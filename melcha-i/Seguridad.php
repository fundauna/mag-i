<?php

/* * *****************************************************************
  CLASE DE NIVEL 1 ENCARGADA DEL MODULO DE SEGURIDAD
  (final SOLO PUEDE SER INSTANCIADA Y NO HEREDADA)
 * ***************************************************************** */

final class Seguridad extends Database {

//
    function AutorizacionesCartasMuestra($_usuario) {
        return $this->_QUERY("SELECT CAR030.id, CAR030.[desc]
		FROM CAR031 INNER JOIN CAR030 ON CAR031.carta = CAR030.id
		WHERE (CAR031.usuario = {$_usuario}) ORDER BY CAR030.[desc];");
    }

    function AutorizacionesCartasIME($_usuario, $_carta, $_accion) {
        $this->_TRANS("DELETE FROM CAR031 WHERE (carta = {$_carta}) AND (usuario = {$_usuario});");
        if ($_accion == 'I')
            $this->_TRANS("INSERT INTO CAR031 VALUES({$_carta}, {$_usuario});");

        $this->Logger('1016');
        return 1;
    }

    function AutorizacionesEquiposIME($_usuario, $_equipo, $_accion) {
        $this->_TRANS("DELETE FROM PER012 WHERE (usuario = {$_usuario}) AND (equipo = {$_equipo});");
        if ($_accion == 'I')
            $this->_TRANS("INSERT INTO PER012 VALUES({$_usuario}, {$_equipo});");

        $this->Logger('1002');
        return 1;
    }

    function AutorizacionesEquiposMuestra($_usuario, $_lid) {
        return $this->_QUERY("SELECT EQU000.id, EQU000.nombre
		FROM EQU000 INNER JOIN PER012 ON EQU000.id = PER012.equipo
		WHERE (EQU000.lid = '{$_lid}') AND (PER012.usuario = {$_usuario});");
    }

    function AutorizacionesInventariosIME($_perfil, $_inventario, $_accion) {
        $this->_TRANS("DELETE FROM PER016 WHERE (perfil = {$_perfil}) AND (tipo_inventario = {$_inventario});");
        if ($_accion == 'I')
            $this->_TRANS("INSERT INTO PER016 VALUES({$_perfil}, {$_inventario});");

        $this->Logger('1003');
        return 1;
    }

    function AutorizacionesInventarioMuestra($_perfil) {
        return $this->_QUERY("SELECT INV000.id, INV000.nombre
		FROM INV000 INNER JOIN PER016 ON INV000.id = PER016.tipo_inventario
		WHERE (PER016.perfil = {$_perfil});");
    }

    function AutorizacionesMetodosIME($_usuario, $_metodo, $_accion) {
        $this->_TRANS("DELETE FROM PER011 WHERE (usuario = {$_usuario}) AND (metodo = {$_metodo});");
        if ($_accion == 'I')
            $this->_TRANS("INSERT INTO PER011 VALUES({$_usuario}, {$_metodo});");

        $this->Logger('1004');
        return 1;
    }

    function AutorizacionesMetodosMuestra($_usuario, $_lid) {
        return $this->_QUERY("SELECT MET000.id, MET000.nombre
		FROM MET000 INNER JOIN PER011 ON MET000.id = PER011.metodo
		WHERE (MET000.lab = '{$_lid}') AND (PER011.usuario = {$_usuario})
		ORDER BY MET000.nombre;");
    }

    function ClientesClave($_actual, $_nueva) {
        $_actual = base64_encode($_actual);
        $_nueva = base64_encode($_nueva);

        $ROW = $this->_QUERY("SELECT 1 FROM MAN002 WHERE (id='{$_SESSION['_UID_']}') AND (clave='{$_actual}');");
        if (!$ROW)
            return false;

        $this->Logger('1005');
        return $this->_TRANS("UPDATE MAN002 SET clave='{$_nueva}' WHERE (id='{$_SESSION['_UID_']}');");
    }

    function ClientesDatosGet() {
        return $this->_QUERY("SELECT email FROM MAN002 WHERE (id='{$_SESSION['_UID_']}');");
    }

    function ClientesDatosSet($_email) {
        $_email = $this->Free($_email);
        $this->_TRANS("UPDATE MAN002 SET email='{$_email}' WHERE (id='{$_SESSION['_UID_']}');");
    }

    function ClientesForgot($_login) {
        $_login = str_replace("'", ' ', $_login);

        $ROW = $this->_QUERY("SELECT email, clave FROM MAN002 WHERE (id='{$_login}') AND (estado <> '2');");
        if ($ROW) {
            $ROW[0]['clave'] = base64_decode($ROW[0]['clave']);
            $cuerpo = "Se�or(a):\n";
            $cuerpo .= "Este es un correo autom�tico con los datos de ingreso al sistema.\n\n";
            $cuerpo .= "Clave: {$ROW[0]['clave']}\n";
            return $this->Email($ROW[0]['email'], $cuerpo);
        } else
            return '-2';
    }

    function ClientesLogin($_login, $_clave) {
        $_login = str_replace("'", '', $_login);
        $_clave = base64_encode($_clave);

        //$ROW = $this->_QUERY("EXECUTE PA_MAG_038 '{$_login}','{$_clave}';");
        $ROW = $this->_QUERY("SELECT id, nombre FROM MAN002 WHERE id = '{$_login}' AND clave = '{$_clave}' AND estado = '1';");

        if ($ROW) {
            $this->SesionSet($ROW[0]['id'], $ROW[0]['id'], $ROW[0]['nombre'], '-1', '-1');
            return 1;
        } else {
            return -2;
        }

        /* if ($ROW[0]['respuesta'] == '1') {
          $this->SesionSet($ROW[0]['UID'], $ROW[0]['UID'], $ROW[0]['nombre'], '-1', '-1');
          } */

        //return $ROW[0]['respuesta'];
    }

    function ClientesHistorialIME($_accion, $_clave = '') {
        if ($_accion == 'I') {
            $_clave = base64_encode($_clave);
            return $this->_TRANS("INSERT INTO MAN015(id,fecha,clave) VALUES('{$_SESSION['_UID_']}',GETDATE(),'$_clave');");
        }
    }

    function ClienteHistorialGet($_token = '') {
        if ($_token == 1) {
            // Ultimas 3
            return $this->_QUERY("SELECT TOP 3 * FROM MAN015 WHERE id='{$_SESSION['_UID_']}' ORDER BY fecha DESC");
        } elseif ($_token == 2) {
            // dias naturales cambio de clave
            return $this->_QUERY("SELECT TOP 1 DATEDIFF (dd,fecha,GETDATE()) as dias FROM MAN015 WHERE id='{$_SESSION['_UID_']}' ORDER BY fecha DESC ;");
        }
    }

    function GetNombreRol($_rol) {
        $ROW = $this->_QUERY("SELECT nombre FROM SEG002 WHERE (id={$_rol});");
        return $ROW[0]['nombre'];
    }

    function NotificacionesGet($_lab, $_yo) {
        $avisar = false;
        $destinos = '';
        //VALIDA NOTIFICACIONES DE TRAMITES
        $tipo = 76;
        $ROW = $this->_QUERY("EXECUTE SENTRY '{$_lab}', '{$tipo}', -1;");
        if ($ROW && $ROW[0]['respuesta'] == '1') {
            for ($x = 0; $x < count($ROW); $x++) {
                //$destinos .= ','.$ROW[$x]['email'];
                $this->TablonSet($tipo, $ROW[$x]['id'], $tipo, false);
            }
            //PONGO BANDERA DE QUE YA AVISE HOY
            $avisar = true;
        }
        //VALIDA NOTIFICACIONES DE CONTRATOS
        $tipo = 77;
        $ROW = $this->_QUERY("EXECUTE SENTRY '{$_lab}', '{$tipo}', -1;");
        if ($ROW && $ROW[0]['respuesta'] == '1') {
            for ($x = 0; $x < count($ROW); $x++) {
                //$destinos .= ','.$ROW[$x]['email'];
                $this->TablonSet($tipo, $ROW[$x]['id'], $tipo, false);
            }
            //PONGO BANDERA DE QUE YA AVISE HOY
            $avisar = true;
        }
        //VALIDA NOTIFICACIONES DE HOJA DE VIDA
        $tipo = 41;
        $ROW = $this->_QUERY("EXECUTE SENTRY '{$_lab}', '{$tipo}', '{$_yo}';");
        if ($ROW && $ROW[0]['respuesta'] == '1') {
            $this->TablonSet($tipo, $_yo, $tipo, false);
        }
        //VALIDA INVENTARIO VENCIDO
        $tipo = 'A6';
        $ROW = $this->_QUERY("EXECUTE SENTRY '{$_lab}', '{$tipo}', -1;");
        if ($ROW && $ROW[0]['respuesta'] == '1') {
            for ($x = 0; $x < count($ROW); $x++) {
                //$destinos .= ','.$ROW[$x]['email'];
                $this->TablonSet($tipo, $ROW[$x]['id'], -1, false);
            }
            //PONGO BANDERA DE QUE YA AVISE HOY
            $avisar = true;
        }
        //VALIDA CRONOGRAMA DE EQUIPO
        $tipo = '64';
        $ROW = $this->_QUERY("EXECUTE SENTRY '{$_lab}', '{$tipo}', -1;");
        if ($ROW && $ROW[0]['respuesta'] == '1') {
            for ($x = 0; $x < count($ROW); $x++) {
                //$destinos .= ','.$ROW[$x]['email'];
                $this->TablonSet($tipo, $ROW[$x]['id'], -1, false);
            }
            //PONGO BANDERA DE QUE YA AVISE HOY
            $avisar = true;
        }
        //VALIDA RECORDATORIO CRONOGRAMA DE EQUIPO
        $tipo = '65';
        $ROW = $this->_QUERY("EXECUTE SENTRY '{$_lab}', '{$tipo}', -1;");
        if ($ROW && $ROW[0]['respuesta'] == '1') {
            for ($x = 0; $x < count($ROW); $x++) {
                //$destinos .= ','.$ROW[$x]['email'];
                $this->TablonSet($tipo, $ROW[$x]['id'], -1, false);
            }
            //PONGO BANDERA DE QUE YA AVISE HOY
            $avisar = true;
        }
        //VALIDA MUESTRAS POR ANALIZAR
        $tipo = '25';
        $ROW = $this->_QUERY("EXECUTE SENTRY '{$_lab}', '{$tipo}', '{$_yo}';");
        if ($ROW && $ROW[0]['respuesta'] == '1') {
            $this->TablonSet($tipo, $ROW[0]['id'], -1, false);
            $avisar = true;
        }
        //
        if ($avisar) {
            $destinos = substr($destinos, 1);
            $this->_TRANS("UPDATE MANPRM SET aviso{$_lab} = GETDATE();");
            //ENVIA EMAIL
            $this->Email($destinos, 'Tiene notificaciones pendientes en el sistema');
        }
    }

    function PaginasMuestra() {
        return $this->_QUERY("SELECT sigla, codigo, nombre, ref, LCC, LDP, LRE FROM SEG008 ORDER BY sigla, codigo;");
    }

    function PerfilesIME($_lab, $_id, $_nombre, $_accion) { //INGRESAR, MODIFICAR, ELIMINAR
        $_nombre = $this->Free($_nombre);
        if ($this->_TRANS("EXECUTE PA_MAG_001 '{$_lab}', '{$_id}', '{$_nombre}', '{$_accion}';")) {
            //
            if ($_accion == 'I')
                $_accion = '1006';
            elseif ($_accion == 'M')
                $_accion = '1007';
            else
                $_accion = '1008';
            $this->Logger($_accion);
            //
            return 1;
        } else
            return 0;
    }

    function PerfilesMuestra($_lab) {
        return $this->_QUERY("SELECT id, nombre FROM SEG002 WHERE (lab = '{$_lab}') AND (id <> 0) ORDER BY nombre;");
    }

    function PermisosIME($_perfil, $_permiso, $_privilegio, $_accion) {
        if ($_accion == 'I') {
            //SI YA EXISTE DA ERROR
            $ROW = $this->_QUERY("SELECT 1 FROM SEG004 WHERE (perfil = '{$_perfil}') AND (permiso = '{$_permiso}');");
            if ($ROW)
                return 0;
        }

        if ($this->_TRANS("EXECUTE PA_MAG_002 '{$_perfil}', '{$_permiso}', '{$_privilegio}', '{$_accion}';")) {
            //
            if ($_accion == 'I')
                $_accion = '1009';
            else
                $_accion = '1010';
            $this->Logger($_accion);
            //
            return 1;
        } else
            return 0;
    }

    function PermisosMuestra($_perfil = 0) {
        if ($_perfil == 0) //MUESTRA TODOS
            return $this->_QUERY("SELECT id, nombre FROM SEG003 ORDER BY modulo, id;");
        else
            return $this->_QUERY("SELECT SEG003.id, SEG003.nombre, SEG004.privilegio
			FROM SEG002 INNER JOIN SEG004 ON SEG002.id = SEG004.perfil INNER JOIN SEG003 ON SEG004.permiso = SEG003.id
			WHERE (SEG002.id = {$_perfil}) AND (SEG002.lab = '{$_SESSION['_LID_']}')
			ORDER BY SEG003.modulo, SEG003.id;");
    }

    function ScriptExe($_codigo) {
        if ($this->_TRANS("{$_codigo};"))
            return 1;
        else
            return 0;
    }

    function SesionAuth() {
        $this->SesionInit();

        if (!isset($_SESSION['_UID_']) or $_SESSION['_UID_'] === '')
            return false;
        return true;
    }

    function SesionDestruye() {
        $this->SesionInit();
        $_SESSION = array();
        if (isset($_COOKIE[session_name()]))
            setcookie(session_name(), '', time() - 42000, '/');
        session_destroy();
    }

    function SesionGet() {
        $this->SesionInit();
        return array('UID' => $_SESSION['_UID_'], 'UCED' => $_SESSION['_UCED_'], 'UNAME' => $_SESSION['_UNAME_'], 'LID' => $_SESSION['_LID_'], 'ROL' => $_SESSION['_ROL_']);
    }

    /* METODOS PRIVADOS */

    private function SesionInit() {
        //session_name('MAG-i');
        if (!isset($_SESSION))
            session_start();
    }

    private function SesionSet($_uid, $_uced, $_name, $_labid, $_perfil) {
        $this->SesionInit();
        $_SESSION['_UID_'] = $_uid;
        $_SESSION['_UCED_'] = $_uced;
        $_SESSION['_UNAME_'] = $_name;
        $_SESSION['_LID_'] = $_labid;
        $_SESSION['_ROL_'] = $_perfil;
    }

    function UsuarioClave($_actual, $_nueva) {
        $_actual = base64_encode($_actual);
        $_nueva = base64_encode($_nueva);

        $ROW = $this->_QUERY("SELECT 1 FROM SEG001 WHERE (id='{$_SESSION['_UID_']}') AND (clave='{$_actual}');");
        if (!$ROW)
            return false;
        //
        $this->Logger('1011');
        //
        return $this->_TRANS("UPDATE SEG001 SET clave='{$_nueva}' WHERE (id='{$_SESSION['_UID_']}');");
    }

    function UsuarioDatosGet() {
        return $this->_QUERY("SELECT nombre, ap1, ap2, email FROM SEG001 WHERE (id='{$_SESSION['_UID_']}');");
    }

    function UsuarioDatosGet2($_id) {
        return $this->_QUERY("SELECT nombre, ap1, ap2, email FROM SEG001 WHERE (id='{$_id}');");
    }

    function UsuarioDatosSet($_nombre, $_ap1, $_ap2, $_email) {
        $_nombre = $this->Free($_nombre);
        $_ap1 = $this->Free($_ap1);
        $_ap2 = $this->Free($_ap2);
        $_email = $this->Free($_email);
        //
        $this->Logger('1012');
        //
        $this->_TRANS("UPDATE SEG001 SET nombre='{$_nombre}', ap1='{$_ap1}', ap2='{$_ap2}', email='{$_email}' WHERE (id='{$_SESSION['_UID_']}');");
    }

    function UsuariosDetalle($_id, $_lab) {
        $ROW = $this->_QUERY("SELECT {$_id} AS id, cedula, nombre, ap1, ap2, email, estado, login, clave FROM SEG001 WHERE (id={$_id});");
        $ROW2 = $this->_QUERY("SELECT SEG007.perfil FROM SEG007 INNER JOIN SEG002 ON SEG007.perfil = SEG002.id WHERE (SEG002.lab = '{$_lab}') AND (SEG007.usuario = {$_id});");
        if ($ROW2)
            $ROW[0]['perfil'] = $ROW2[0]['perfil'];
        else
            $ROW[0]['perfil'] = '';
        return $ROW;
    }

    function UsuariosEstado($_estado) {
        if ($_estado == '0')
            return 'Inactivo';
        elseif ($_estado == '1')
            return 'Activo';
        elseif ($_estado == '2')
            return 'Cond. Especial';
    }

    function UsuarioFin() {
        $this->SesionInit();
        $this->Logger('1001');
        $this->SesionDestruye();
    }

    function UsuarioForgot($_login) {
        $_login = str_replace("'", ' ', $_login);
        $ROW = $this->_QUERY("SELECT email, clave FROM SEG001 WHERE (login='{$_login}');");
        if ($ROW) {
            $ROW[0]['clave'] = base64_decode($ROW[0]['clave']);
            $cuerpo = "Se�or(a):\n";
            $cuerpo .= "Este es un correo autom�tico con los datos de ingreso al sistema.\n\n";
            $cuerpo .= "Clave: {$ROW[0]['clave']}\n";
            return $this->Email($ROW[0]['email'], $cuerpo);
        } else
            return '-2';
    }

    function UsuariosIME($_id, $_cedula, $_nombre, $_ap1, $_ap2, $_email, $_perfil, $_estado, $_login, $_clave, $_accion, $_lab) {
        //PROTECCION EN CASO DE QUE ALGUIEN QUIERA SER ADMINISTRADOR
        if ($_perfil == '0') {
            return 0;
        }

        $_cedula = $this->Free($_cedula);
        $_nombre = $this->Free($_nombre);
        $_ap1 = $this->Free($_ap1);
        $_ap2 = $this->Free($_ap2);
        $_email = $this->Free($_email);
        $_login = $this->Free($_login);
        $_clave = base64_encode($_clave);

        if ($this->_TRANS("EXECUTE PA_MAG_003 '{$_id}', '{$_cedula}', '{$_nombre}', '{$_ap1}', '{$_ap2}', '{$_email}', '{$_perfil}', '{$_estado}', '{$_login}', '{$_clave}', '{$_accion}', '{$_lab}';")) {
            if ($_accion == 'I') {
                $_accion = '1013';
            } elseif ($_accion == 'M') {
                $_accion = '1014';
            } else {
                $_accion = '1015';
            }
            return 1;
        } else {
            return 0;
        }
    }

    function UsuarioLogin($_login, $_clave, $_lab) {
        $_login = str_replace("'", ' ', $_login);
        $_clave = base64_encode($_clave);

        $ROW = $this->_QUERY("EXECUTE PA_MAG_000 '{$_login}', '{$_clave}', '{$_lab}';");

        if ($ROW[0]['respuesta'] != '1') {
            $R = $this->_QUERY("SELECT id FROM SEG001 WHERE login = '{$_login}' AND estado = '1';");
            if ($R) {
                $this->_TRANS("UPDATE SEG001 SET clave = '{$_clave}' WHERE id = '{$R[0]['id']}';");
                $ROW = $this->_QUERY("EXECUTE PA_MAG_000 '{$_login}', '{$_clave}', '{$_lab}';");
            }
        }

        if ($ROW[0]['respuesta'] == '1') {
            $this->SesionSet($ROW[0]['UID'], $ROW[0]['cedula'], $ROW[0]['nombre'], $_lab, $ROW[0]['perfil']);
            $this->Logger('1000');
        }

        return $ROW[0]['respuesta'];
    }

    /* function UsuarioLogin($_login, $_clave, $_lab) {
      $_login = str_replace("'", ' ', $_login);
      $_clave = base64_encode($_clave);

      //$ROW = $this->_QUERY("EXECUTE PA_MAG_000 '{$_login}', '{$_clave}', '{$_lab}';");
      $ROW = $this->_QUERY("SELECT id AS UID, cedula, (nombre + ' ' + ap1 + ' ' + ap2) AS nombre FROM SEG001 WHERE login = '{$_login}' AND estado = '1';");
      if ($ROW) {
      $ROW2 = $this->_QUERY("SELECT SEG007.perfil FROM SEG007 INNER JOIN SEG002 ON SEG007.perfil = SEG002.id WHERE (SEG007.usuario = '{$ROW[0]['id']}') AND (SEG002.lab = '{$_lab}');");
      $this->SesionSet($ROW[0]['UID'], $ROW[0]['cedula'], $ROW[0]['nombre'], $_lab, $ROW2[0]['perfil']);
      $this->Logger('1000');
      return 1;
      } else {
      return -2;
      }
      } */

    function UsuarioMenu($_lab, $_rol) {
        return $this->_QUERY("SELECT SEG004.permiso
		FROM SEG004 INNER JOIN SEG002 ON SEG004.perfil = SEG002.id
		WHERE (SEG002.lab = '{$_lab}') AND (SEG004.perfil = {$_rol});");
    }

    function UsuarioMetodo($_UID, $_metodo) {
        $ROW = $this->_QUERY("SELECT 1 FROM PER011 WHERE (usuario={$_UID}) AND (metodo={$_metodo});");
        if (!$ROW)
            return false;
        return true;
    }

    function UsuariosMuestra($_id = 0, $_lab = '') {
        if ($_lab == '')
            return $this->_QUERY("SELECT id, cedula, nombre, ap1, ap2, estado FROM SEG001 WHERE estado <> '3' ORDER BY nombre;");
        else {
            return $this->_QUERY("SELECT SEG001.id, SEG001.cedula, SEG001.nombre, SEG001.ap1, SEG001.ap2, SEG001.estado
			FROM SEG002 INNER JOIN SEG007 ON SEG002.id = SEG007.perfil INNER JOIN SEG001 ON SEG007.usuario = SEG001.id
			WHERE (SEG002.lab = '{$_lab}') AND (SEG001.estado = '1') AND SEG001.id != '0'  
			UNION
			SELECT SEG001.id, SEG001.cedula, SEG001.nombre, SEG001.ap1, SEG001.ap2, SEG001.estado
			FROM SEG002 INNER JOIN SEG007 ON SEG002.id = SEG007.perfil INNER JOIN SEG001 ON SEG007.usuario = SEG001.id
			WHERE (SEG002.lab = '0') AND (SEG001.estado = '1') AND SEG001.id != '0' ORDER BY nombre;");
        }
    }

    function UsuarioPermiso($_permiso) {
        $ROW = $this->_QUERY("SELECT SEG004.privilegio
		FROM SEG004 INNER JOIN SEG002 ON SEG004.perfil = SEG002.id
		WHERE (SEG002.lab = '{$_SESSION['_LID_']}') AND (SEG004.perfil = {$_SESSION['_ROL_']}) AND (SEG004.permiso = '{$_permiso}');");
        if (!$ROW)
            return false;
        return $ROW[0]['privilegio'];
    }

    function UsuariosVacio() {
        return array(0 => array(
                'id' => '-1',
                'cedula' => '',
                'nombre' => '',
                'ap1' => '',
                'ap2' => '',
                'email' => '',
                'estado' => '',
                'login' => '',
                'perfil' => '',
                'clave' => ''
        ));
    }

    function UsuariosYo($_id) {
        return $this->_QUERY("SELECT id, cedula, nombre, ap1, ap2, estado FROM SEG001 WHERE (id = {$_id});");
    }

//
}

?>
