<?php
/*******************************************************************
CLASE DE NIVEL 1 ENCARGADA DEL MODULO DE VALIDACION DE METODOS
(final SOLO PUEDE SER INSTANCIADA Y NO HEREDADA)
*******************************************************************/
final class Validacion extends Database{
//
	function ConsecutivoGet($_tipo){
		$ROW = $this->_QUERY("SELECT validacion0{$_tipo}_ AS total FROM MAG000;");
		if($_tipo=='2') $_tipo = 'LDP-';
		elseif($_tipo=='3') $_tipo = 'LCC-';
		elseif($_tipo=='1') $_tipo = 'LRE-';
		return $_tipo.$ROW[0]['total'].'-'.date('Y');
	}
	
	function ConsecutivoSet($_tipo){
		$this->_TRANS("UPDATE MAG000 SET validacion0{$_tipo}_=validacion0{$_tipo}_+1;");
	}
	
	function ExisteRepetibilidad($_cs){
		$_cs = str_replace(' ', '', $_cs);
		return $this->_QUERY("SELECT 1 FROM VAL000 WHERE (cs='{$_cs}') AND (tipo='2') AND (lab='3');");
	}
	
	function NotificaJefes($_lab, $_tipo){
		$ROW = $this->_QUERY("SELECT SEG001.id, SEG001.email
		FROM SEG001 INNER JOIN SEG007 ON SEG001.id = SEG007.usuario INNER JOIN SEG002 ON SEG007.perfil = SEG002.id INNER JOIN SEG004 ON SEG002.id = SEG004.perfil
		WHERE (SEG002.lab = '{$_lab}') AND (SEG004.permiso = '{$_tipo}');");

		for($x=0,$destinos='';$x<count($ROW);$x++){
			$destinos .= ','.$ROW[$x]['email'];
			$this->TablonSet($_tipo, $ROW[$x]['id'], $_tipo, false);
		}
		
		$destinos = substr($destinos, 1);
		//ENVIA EMAIL
		$Robot = new Seguridad();
		$Robot->Email($destinos, 'Tiene notificaciones en el sistema');
	}
	
	function ObtieneLineasRepetibilidad($_cs){
		return $this->_QUERY("SELECT MM, MV FROM VAL003 WHERE (validacion = '{$_cs}') AND (X=1) ORDER BY linea;");
	}
	
	function Validacion1EncabezadoGet($_cs){
		return $this->_QUERY("SELECT '{$_cs}' AS cs, CONVERT(VARCHAR, VAL000.fecha, 105) AS fecha, VAL000.estado, VAL000.obs, VAL001.metodo, VAL001.masa, VAL001.pureza, VAL001.volumen, VAL001.estandar, VAL001.balon, VAL001.IA, VAL001.A0, VAL001.A1, VAL001.A2, VAL001.A3, VAL001.C0, VAL001.C1, VAL001.C2, VAL001.C3, EQU000.codigo AS tmp_equipo, EQU000.nombre 
		FROM VAL000 INNER JOIN VAL001 ON VAL000.cs = VAL001.validacion INNER JOIN EQU000 ON VAL001.equipo = EQU000.id
		WHERE (VAL000.cs = '{$_cs}');");
	}
	
	function Validacion1EncabezadoSet($_tipo, $_metodo, $_masa, $_equipo, $_pureza, $_volumen, $_estandar, $_balon, $_obs, $_IA, $_A0, $_A1, $_A2, $_A3, $_B0, $_B1, $_B2, $_B3, $_R, $_LID, $_UID){
		$_metodo = $this->Free($_metodo);
		$_estandar = $this->Free($_estandar);
		$_IA = $this->Free($_IA);
		$_obs = $this->Free($_obs);
		
		$cs = $this->ConsecutivoGet($_LID);
		if($_R>0.99) $_R = 1;
		else $_R = 0;

		if( $this->_TRANS("EXECUTE PA_MAG_112 '{$cs}', '{$_tipo}', '{$_R}', '{$_obs}', '{$_UID}', '{$_LID}';") ){
					
			$this->_TRANS("INSERT INTO VAL001 VALUES('{$cs}',
				'{$_equipo}', 
				'{$_metodo}',
				'{$_masa}', 
				'{$_pureza}', 
				'{$_volumen}', 
				'{$_estandar}', 
				'{$_balon}', 
				'{$_IA}', 
				'{$_A0}', 
				'{$_A1}', 
				'{$_A2}', 
				'{$_A3}', 
				'{$_B0}', 
				'{$_B1}', 
				'{$_B2}', 
				'{$_B3}'
			);");
			
			$cs = $this->ConsecutivoSet($_LID);
			
			return 1;
		}else return 0;
	}
	
	function Validacion1EncabezadoVacio(){
		return array(0=>array(
			'cs'=>'',
			'fecha'=>'',
			'analista'=>'',
			'metodo'=>'',
			'estado'=>'',
			'masa'=>'0',
			'tmp_equipo'=>'',
			'nombre'=>'',
			'pureza'=>'0',
			'volumen'=>'0',
			'estandar'=>'',
			'balon'=>'0',
			'obs'=>'',
			'IA'=>'',
			'A0'=>'0',
			'A1'=>'0',
			'A2'=>'0',
			'A3'=>'0',
			'C0'=>'0',
			'C1'=>'0',
			'C2'=>'0',
			'C3'=>'0')
		);
	}
	
	function Validacion2DetalleGet($_cs, $_tipo){
		if($_tipo==0)
			return $this->_QUERY("SELECT A, B, C, D FROM VAL003 WHERE (validacion = '{$_cs}') ORDER BY linea;");
		else
			return $this->_QUERY("SELECT G, H, I, X FROM VAL003 WHERE (validacion = '{$_cs}') ORDER BY linea;");
	}
	
	function Validacion2EncabezadoGet($_cs){
		return $this->_QUERY("SELECT '{$_cs}' AS cs, CONVERT(VARCHAR, VAL000.fecha, 105) AS fecha, VAL000.estado, VAL000.obs, VAL002.vol_bal, VAL002.mv, VAL002.vol_int, VAL002.ali_mue, VAL002.densidad, VAL002.ali_enr, VAL002.estandar, VAL002.ali_dil, VAL002.vol_bal2, VAL002.masa, VAL002.vol_dil, VAL002.mm, VAL002.pureza, VAL002.unidad AS tipo, VAL002.IA
		FROM VAL000 INNER JOIN VAL002 ON VAL000.cs = VAL002.validacion
		WHERE (VAL000.cs = '{$_cs}');");
	}
	
	function Validacion2EncabezadoSet($_tipo, $_vol_bal, $_mv, $_vol_int, $_ali_mue, $_densidad, $_ali_enr, $_estandar, $_ali_dil, $_vol_bal2, $_masa, $_vol_dil, $_mm, $_pureza, $_unidad, $_IA, $_cumple, $_obs, $_A, $_B, $_C, $_D, $_G, $_H, $_I, $_MM, $_MV, $_X, $_LID, $_UID){
		$_estandar = $this->Free($_estandar);
		$_IA = $this->Free($_IA);
		$_obs = $this->Free($_obs);
	
		$cs = $this->ConsecutivoGet($_LID);
	
		if( $this->_TRANS("EXECUTE PA_MAG_112 '{$cs}', '{$_tipo}', '{$_cumple}', '{$_obs}', '{$_UID}', '{$_LID}';") ){
					
			$this->_TRANS("INSERT INTO VAL002 VALUES('{$cs}', '{$_vol_bal}', '{$_mv}', '{$_vol_int}', '{$_ali_mue}', '{$_densidad}', '{$_ali_enr}', '{$_estandar}', '{$_ali_dil}', '{$_vol_bal2}', '{$_masa}', '{$_vol_dil}', '{$_mm}', '{$_pureza}', '{$_unidad}', '{$_IA}');");
			
			$_A = explode('&', substr($_A, 1));
			$_B = explode('&', substr($_B, 1));
			$_C = explode('&', substr($_C, 1));
			$_D = explode('&', substr($_D, 1));
			$_G = explode('&', substr($_G, 1));
			$_H = explode('&', substr($_H, 1));
			$_I = explode('&', substr($_I, 1));	
			$_MM = explode('&', substr($_MM, 1));	
			$_MV = explode('&', substr($_MV, 1));	
			$_X = explode('&', substr($_X, 1));	
			
			for($i=1;$i<count($_A);$i++){
				if(str_replace(' ', '', $_A[$i]) != ''){
					if($_X[$i]=='1') $_X[$i] = 1;
					else $_X[$i] = 0;
					$this->_TRANS("INSERT INTO VAL003 values('{$cs}', {$i}, {$_A[$i]}, {$_B[$i]}, {$_C[$i]}, {$_D[$i]}, {$_G[$i]}, {$_H[$i]}, {$_I[$i]}, {$_MM[$i]}, {$_MV[$i]}, {$_X[$i]});");
				}
			}
			
			$cs = $this->ConsecutivoSet($_LID);
			
			return 1;
		}else return 0;
	}
	
	function Validacion2EncabezadoVacio(){
		return array(0=>array(
			'cs'=>'',
			'fecha'=>'',
			'tipo'=>'',
			'obs'=>'',
			'IA'=>'',
			'estado'=>'',
			'vol_bal'=>'0',
			'mv'=>'0',
			'vol_int'=>'0',
			'ali_mue'=>'0',
			'densidad'=>'0',
			'ali_enr'=>'0',
			'estandar'=>'',
			'ali_dil'=>'0',
			'vol_bal2'=>'0',
			'masa'=>'0',
			'vol_dil'=>'0',
			'mm'=>'0',
			'pureza'=>'0',
			'analista'=>'')
		);
	}
	
	function Validacion3Detalle1Get($_cs, $_tipo){
		if($_tipo==1) $extra = '(linea < 6)';
		else if($_tipo==2) $extra = '(linea > 5) AND (linea < 11)';
		else if($_tipo==3) $extra = '(linea > 10)';
		return $this->_QUERY("SELECT A, B, C, D, G, H, I, X FROM VAL006 WHERE (validacion = '{$_cs}') AND {$extra} ORDER BY linea;");
	}
	
	function Validacion3Detalle2Get($_cs){
		return $this->_QUERY("SELECT MM, MV, XX FROM VAL007 WHERE (validacion = '{$_cs}') ORDER BY linea;");
	}
	
	function Validacion3DetalleVacio(){
		return array(
			0=>array('A'=>'0','B'=>'0','C'=>'0','D'=>'0','G'=>'0','H'=>'0','I'=>'0','X'=>'1'),
			1=>array('A'=>'0','B'=>'0','C'=>'0','D'=>'0','G'=>'0','H'=>'0','I'=>'0','X'=>'1'),
			2=>array('A'=>'0','B'=>'0','C'=>'0','D'=>'0','G'=>'0','H'=>'0','I'=>'0','X'=>'1'),
			3=>array('A'=>'0','B'=>'0','C'=>'0','D'=>'0','G'=>'0','H'=>'0','I'=>'0','X'=>'1'),
			4=>array('A'=>'0','B'=>'0','C'=>'0','D'=>'0','G'=>'0','H'=>'0','I'=>'0','X'=>'1')
		);
	}
	
	function Validacion3EncabezadoGet($_cs){
		return $this->_QUERY("SELECT '{$_cs}' AS cs, CONVERT(VARCHAR, VAL000.fecha, 105) AS fecha, VAL000.estado, VAL000.obs, VAL004.metodo, VAL004.masa, VAL004.masa1, VAL004.masa2, VAL004.analitico, VAL004.pureza, VAL004.pureza1, VAL004.pureza2, VAL004.vol1, VAL004.vol11, VAL004.vol12, VAL004.ali2, VAL004.ali21, VAL004.ali22, VAL004.vol2, VAL004.vol21, VAL004.vol22, VAL004.IA, VAL004.mue_vol1, VAL004.mue_ali2, VAL004.formulado, VAL004.mue_vol2, VAL004.densidad, VAL004.declarada, VAL004.unidad,
		(SELECT EQU000.codigo FROM VAL005 INNER JOIN EQU000 ON VAL005.equipo = EQU000.id 
			WHERE (VAL005.validacion = VAL000.cs) AND (VAL005.tipo = '1')
		) AS tmp_equipo1,
		(SELECT EQU000.codigo FROM VAL005 INNER JOIN EQU000 ON VAL005.equipo = EQU000.id 
			WHERE (VAL005.validacion = VAL000.cs) AND (VAL005.tipo = '2')
		) AS tmp_equipo2,
		(SELECT EQU000.codigo FROM VAL005 INNER JOIN EQU000 ON VAL005.equipo = EQU000.id 
			WHERE (VAL005.validacion = VAL000.cs) AND (VAL005.tipo = '3')
		) AS tmp_equipo3,
		(SELECT INV002.codigo FROM VAL005 INNER JOIN INV002 ON VAL005.columna = INV002.id 
			WHERE (VAL005.validacion = '{$_cs}') AND (VAL005.tipo = '1')
		) AS tmp_columna1,
		(SELECT INV002.codigo FROM VAL005 INNER JOIN INV002 ON VAL005.columna = INV002.id 
			WHERE (VAL005.validacion = '{$_cs}') AND (VAL005.tipo = '2')
		) AS tmp_columna2,
		(SELECT INV002.codigo FROM VAL005 INNER JOIN INV002 ON VAL005.columna = INV002.id 
			WHERE (VAL005.validacion = '{$_cs}') AND (VAL005.tipo = '3')
		) AS tmp_columna3
		FROM VAL000 INNER JOIN VAL004 ON VAL000.cs = VAL004.validacion
		WHERE (VAL000.cs = '{$_cs}');");
	}
	
	function Validacion3EncabezadoSet($_tipo, $_metodo, $_masa, $_masa1, $_masa2, $_analitico, $_pureza, $_pureza1, $_pureza2, $_equipo1, $_equipo2, $_equipo3, $_columna1, $_columna2, $_columna3, $_vol1, $_vol11, $_vol12, $_ali2, $_ali21, $_ali22, $_vol2, $_vol21, $_vol22, $_IA, $_mue_vol1, $_mue_ali2, $_formulado, $_mue_vol2, $_densidad, $_declarada, $_unidad, $_cumple, $_obs, $_A, $_B, $_C, $_D, $_G, $_H, $_I, $_X, $_MM, $_MV, $_XX, $_LID, $_UID){
		$_metodo = $this->Free($_metodo);
		$_analitico = $this->Free($_analitico);
		$_formulado = $this->Free($_formulado);
		$_IA = $this->Free($_IA);
		$_obs = $this->Free($_obs);
		
		$cs = $this->ConsecutivoGet($_LID);
		
		if( $this->_TRANS("EXECUTE PA_MAG_112 '{$cs}', '{$_tipo}', '{$_cumple}', '{$_obs}', '{$_UID}', '{$_LID}';") ){
					
			$this->_TRANS("INSERT INTO VAL004 VALUES('{$cs}', '{$_metodo}', '{$_masa}', '{$_analitico}', '{$_pureza}', '{$_vol1}', '{$_ali2}', '{$_vol2}', '{$_IA}', '{$_mue_vol1}', '{$_mue_ali2}', '{$_formulado}', '{$_mue_vol2}', '{$_densidad}', '{$_declarada}','{$_unidad}','{$_masa1}','{$_masa2}','{$_pureza1}','{$_pureza2}','{$_vol11}','{$_vol12}','{$_ali21}','{$_ali22}','{$_vol21}','{$_vol22}');");
			
			$_A = explode('&', substr($_A, 1));
			$_B = explode('&', substr($_B, 1));
			$_C = explode('&', substr($_C, 1));
			$_D = explode('&', substr($_D, 1));
			$_G = explode('&', substr($_G, 1));
			$_H = explode('&', substr($_H, 1));
			$_I = explode('&', substr($_I, 1));	
			$_X = explode('&', substr($_X, 1));	
			$_MM = explode('&', substr($_MM, 1));	
			$_MV = explode('&', substr($_MV, 1));
			$_XX = explode('&', substr($_XX, 1));
			
			//INGRESA EQUIPOS
			$this->_TRANS("INSERT INTO VAL005 values('{$cs}', '1', {$_equipo1}, {$_columna1});");
			$this->_TRANS("INSERT INTO VAL005 values('{$cs}', '2', {$_equipo2}, {$_columna2});");
			$this->_TRANS("INSERT INTO VAL005 values('{$cs}', '3', {$_equipo3}, {$_columna3});");
			
			//INGRESA SETS
			for($i=1;$i<count($_A);$i++){
				if(str_replace(' ', '', $_A[$i]) != ''){
					$this->_TRANS("INSERT INTO VAL006 values('{$cs}', {$i}, {$_A[$i]}, {$_B[$i]}, {$_C[$i]}, {$_D[$i]}, {$_G[$i]}, {$_H[$i]}, {$_I[$i]}, {$_X[$i]});");
				}
			}
			//INGRESA IMPORTACIONES
			for($i=1;$i<count($_MM);$i++){
				if(str_replace(' ', '', $_MM[$i]) != ''){
					$this->_TRANS("INSERT INTO VAL007 values('{$cs}', {$i}, {$_MM[$i]}, {$_MV[$i]}, {$_XX[$i]});");
				}
			}
			
			$cs = $this->ConsecutivoSet($_LID);
			
			return 1;
		}else return 0;
	}
	
	function Validacion3EncabezadoVacio(){
		return array(0=>array(
			'cs'=>'',
			'fecha'=>'',
			'metodo'=>'',
			'estado'=>'',
			'masa'=>'0',
			'masa1'=>'0',
			'masa2'=>'0',
			'analitico'=>'',
			'pureza'=>'0',
			'pureza1'=>'0',
			'pureza2'=>'0',
			'tmp_equipo1'=>'',
			'tmp_equipo2'=>'',
			'tmp_equipo3'=>'',
			'tmp_columna1'=>'',
			'tmp_columna2'=>'',
			'tmp_columna3'=>'',
			'vol1'=>'0',
			'vol11'=>'0',
			'vol12'=>'0',
			'ali2'=>'0',
			'ali21'=>'0',
			'ali22'=>'0',
			'vol2'=>'0',
			'vol21'=>'0',
			'vol22'=>'0',
			'IA'=>'',
			'mue_vol1'=>'0',
			'mue_ali2'=>'0',
			'formulado'=>'',
			'mue_vol2'=>'0',
			'densidad'=>'0',
			'declarada'=>'0',
			'unidad'=>'',
			'obs'=>'',
			'analista'=>'')
		);
	}
	
	function Validacion4EncabezadoGet($_cs){
		return $this->_QUERY("SELECT '{$_cs}' AS cs, CONVERT(VARCHAR, VAL000.fecha, 105) AS fecha, VAL008.proce, VAL008.nombre, (SEG001.nombre + ' ' + SEG001.ap1) AS analista
		FROM VAL000 INNER JOIN VAL008 ON VAL000.cs = VAL008.validacion INNER JOIN VAlBIT ON VAL000.cs = VAlBIT.validacion INNER JOIN SEG001 ON VAlBIT.analista = SEG001.id
		WHERE (VAlBIT.accion = '0') AND (VAL000.cs = '{$_cs}');");
	}
	
	function Validacion4EncabezadoSet($_cs, $_proce, $_nombre, $_usuario, $_acc){
		$_proce = $this->Free($_proce);
		$_nombre = $this->Free($_nombre);	
		
		if($_acc == 'I'){
			if( $this->_TRANS("EXECUTE PA_MAG_112 '{$_cs}', '4', '1', NULL, '{$_usuario}', '2';") ){
				$this->_TRANS("INSERT INTO VAL008 values('{$_cs}', '{$_proce}', '{$_nombre}');");
				$this->_TRANS("UPDATE MAG000 SET validacion02_ = validacion02_ + 1;");
				return 1;
			}else return 0;
		}elseif($_acc == 'M'){
			$this->_TRANS("UPDATE VAL008 SET proce = '{$_proce}', nombre = '{$_nombre}' WHERE validacion = '{$_cs}';");
			$this->_TRANS("INSERT INTO VALBIT VALUES($_cs, GETDATE(), '{$_usuario}', 'm');");
			return 1;
		}else{
			if($this->_TRANS("DELETE FROM VALBIT WHERE validacion = '{$_cs}';")){				
				$this->_TRANS("DELETE FROM VAL008 WHERE validacion = '{$_cs}';");
				$this->_TRANS("DELETE FROM VAL000 WHERE cs = '{$_cs}';");
				return 1;
			}else return 0;
		}		
	}
	
	function Validacion4EncabezadoVacio(){
		return array(0=>array(
			'proce'=>'',
			'nombre'=>'')
		);
	}
	
	function Validacion5DetalleGet($_cs){
		return $this->_QUERY("SELECT analito AS ana, pic, teo, exp FROM VAL010 WHERE (validacion = '{$_cs}') ORDER BY linea;");
	}
	
	function Validacion5DetalleVacio(){
		return array(
			0=>array('ana'=>'','pic'=>'0','teo'=>'0','exp'=>'0'),
			1=>array('ana'=>'','pic'=>'0','teo'=>'0','exp'=>'0'),
			2=>array('ana'=>'','pic'=>'0','teo'=>'0','exp'=>'0'),
			3=>array('ana'=>'','pic'=>'0','teo'=>'0','exp'=>'0'),
			4=>array('ana'=>'','pic'=>'0','teo'=>'0','exp'=>'0'),
			5=>array('ana'=>'','pic'=>'0','teo'=>'0','exp'=>'0'),
			6=>array('ana'=>'','pic'=>'0','teo'=>'','exp'=>'0'),
			7=>array('ana'=>'','pic'=>'0','teo'=>'','exp'=>'0'),
			8=>array('ana'=>'','pic'=>'0','teo'=>'','exp'=>'0'),
			9=>array('ana'=>'','pic'=>'0','teo'=>'','exp'=>'0'),
			10=>array('ana'=>'','pic'=>'0','teo'=>'','exp'=>'0'),
			11=>array('ana'=>'','pic'=>'0','teo'=>'','exp'=>'0'),
			12=>array('ana'=>'','pic'=>'0','teo'=>'','exp'=>'0'),
			13=>array('ana'=>'','pic'=>'0','teo'=>'','exp'=>'0'),
			14=>array('ana'=>'','pic'=>'0','teo'=>'','exp'=>'0'),
			15=>array('ana'=>'','pic'=>'0','teo'=>'','exp'=>'0'),
			16=>array('ana'=>'','pic'=>'0','teo'=>'','exp'=>'0')
		);
	}
	
	function Validacion5EncabezadoGet($_cs){
		return $this->_QUERY("SELECT '{$_cs}' AS cs, CONVERT(VARCHAR, VAL000.fecha, 105) AS fecha, VAL000.estado, VAL000.obs, VAR001.id AS matriz, VAR001.nombre AS nommatriz, CONVERT(VARCHAR, VAL009.elaboracion, 105) AS elaboracion, VAL009.asociada, VAL009.bajo, VAL009.medio
		FROM VAL000 INNER JOIN VAL009 ON VAL000.cs = VAL009.validacion INNER JOIN VAR001 ON VAL009.matriz = VAR001.id
		WHERE (VAL000.cs = '{$_cs}');");
	}
	
	function Validacion5EncabezadoSet($_tipo, $_elaboracion, $_asociada, $_matriz, $_bajo, $_medio, $_obs, $_ana, $_pic, $_teo, $_exp, $_analito, $_p1, $_p2, $_p3, $_p4, $_p5, $_bajo1, $_bajo2, $_bajo3, $_alto1, $_alto2, $_alto3, $_final, $_final2, $_cumple, $_LID, $_UID){
		$_asociada = $this->Free($_asociada);
		$_elaboracion = $this->Free($_elaboracion);
		$_obs = $this->Free($_obs);
		
		$cs = $this->ConsecutivoGet($_LID);
		
		if( $this->_TRANS("EXECUTE PA_MAG_112 '{$cs}', '{$_tipo}', '1', '{$_obs}', '{$_UID}', '{$_LID}';") ){

			$this->_TRANS("INSERT INTO VAL009 VALUES('{$cs}', '{$_elaboracion}', '{$_asociada}', {$_matriz}, {$_bajo}, {$_medio});");
			
			$_ana = explode('&', substr($_ana, 1));
			$_pic = explode('&', substr($_pic, 1));
			$_teo = explode('&', substr($_teo, 1));
			$_exp = explode('&', substr($_exp, 1));
			
			//INGRESA PIC, TEO, EXP
			for($i=1;$i<count($_ana);$i++){
				if(str_replace(' ', '', $_ana[$i]) != ''){
					if($_pic[$i]=='N/A') $_pic[$i]='-1';
					if($_teo[$i]=='N/A') $_teo[$i]='-1';
					if($_exp[$i]=='N/A') $_exp[$i]='-1';
					$this->_TRANS("EXECUTE PA_MAG_136 '{$cs}', {$i}, '{$_ana[$i]}', {$_pic[$i]}, {$_teo[$i]}, {$_exp[$i]};");
				}
			}
			
			$_analito = explode('&', substr($_analito, 1));
			$_p1 = explode('&', substr($_p1, 1));
			$_p2 = explode('&', substr($_p2, 1));
			$_p3 = explode('&', substr($_p3, 1));
			$_p4 = explode('&', substr($_p4, 1));
			$_p5 = explode('&', substr($_p5, 1));
			$_bajo1 = explode('&', substr($_bajo1, 1));
			$_bajo2 = explode('&', substr($_bajo2, 1));
			$_bajo3 = explode('&', substr($_bajo3, 1));
			$_alto1 = explode('&', substr($_alto1, 1));
			$_alto2 = explode('&', substr($_alto2, 1));
			$_alto3 = explode('&', substr($_alto3, 1));
			$_final = explode('&', substr($_final, 1));
			$_final2 = explode('&', substr($_final2, 1));
			$_cumple = explode('&', substr($_cumple, 1));
		
			//INGRESA IMPORTACIONES
			for($i=1;$i<count($_p1);$i++){
				if(str_replace(' ', '', $_p1[$i]) != ''){
					$this->_TRANS("EXECUTE PA_MAG_137 '{$cs}', {$i}, 
						'{$_analito[$i]}', 
						 {$_p1[$i]}, 
						 {$_p2[$i]},
						 {$_p3[$i]},
						 {$_p4[$i]},
						 {$_p5[$i]},
						 {$_bajo1[$i]},
						 {$_bajo2[$i]},
						 {$_bajo3[$i]},
						 {$_alto1[$i]},
						 {$_alto2[$i]},
						 {$_alto3[$i]},
						 {$_final[$i]},
						 {$_final2[$i]},
						 {$_cumple[$i]};");
				}
			}
			
			$cs = $this->ConsecutivoSet($_LID);
			
			return 1;
		}else return 0;
	}	
	
	function Validacion5EncabezadoVacio(){
		return array(0=>array(
			'cs'=>'',
			'elaboracion'=>'',
			'asociada'=>'',
			'obs'=>'',
			'estado'=>'',
			'bajo'=>'0.01',
			'medio'=>'0.2',
			'nommatriz'=>'',
			'matriz'=>'')
		);
	}
	
	function Validacion5EstaAprobada($_validacion){
		$_validacion = str_replace(' ', '', $_validacion);
		$_validacion = $this->Free($_validacion);
		
		if( $this->_QUERY("SELECT 1 AS total FROM VAL000 WHERE (tipo = 5) AND (estado = '2') AND (cs = '{$_validacion}');") ) return true;
		else return false;
	}
	
	function Validacion5TemporalDel(){
		$this->_TRANS("DELETE FROM VALTMP;");
	}
	
	function Validacion5TemporalGet(){
		return $this->_QUERY("EXECUTE PA_MAG_135;");
	}
	
	function Validacion5TemporalSet($_i, $_B, $_C, $_D, $_E){
		$_B = str_replace(',', '.', $_B);
		$_C = str_replace(',', '.', $_C);
		$_D = str_replace(',', '.', $_D);
		$_E = str_replace(',', '.', $_E);
		
		$this->_TRANS("EXECUTE PA_MAG_134 {$_i}, '{$_B}', '{$_C}', '{$_D}', '{$_E}';");
	}
	
	function Validacion6DetalleGet($_validacion){
		return $this->_QUERY("SELECT VAL009.bajo, VAL009.medio, VAL011.analito, VAL011.P1, VAL011.P2, VAL011.P3, VAL011.P4, VAL011.P5, VAL011.bajo1, VAL011.bajo2, VAL011.bajo3, VAL011.alto1, VAL011.alto2, VAL011.alto3, VAL011.final, VAL011.final2, VAL011.cumple
		FROM VAL009 INNER JOIN VAL011 ON VAL009.validacion = VAL011.validacion
		WHERE (VAL009.validacion = '{$_validacion}')
		ORDER BY VAL011.linea");
	}
	
	function Validacion6EncabezadoGet($_validacion){
		return $this->_QUERY("EXECUTE PA_MAG_139 '{$_validacion}';");
	}
	
	function Validacion6EncabezadoSet($_accion, $_cs, $_validacion, $_objetivo, $_analitos, $_ambito, $_tipo_metodo, $_cod_metodo, $_precision, $_veracidad, $_limite1, $_limite2, $_linealidad, $_especificidad, $_efecto, $_robustez, $_inc, $_ref, $_modif, $_tecnica, $_interno, $_bitacora, $_pagina, $_equipo, $_otros, $_resumen, $_tratamiento, $_declaracion, $_LID, $_UID){
		$_objetivo = $this->Free($_objetivo);
		$_analitos = $this->Free($_analitos);
		$_ambito = $this->Free($_ambito);
		$_tipo_metodo = $this->Free($_tipo_metodo);
		$_cod_metodo = $this->Free($_cod_metodo);
		$_precision = $this->Free($_precision);
		$_veracidad = $this->Free($_veracidad);
		$_limite1 = $this->Free($_limite1);
		$_limite2 = $this->Free($_limite2);
		$_linealidad = $this->Free($_linealidad);
		$_especificidad = $this->Free($_especificidad);
		$_efecto = $this->Free($_efecto);
		$_robustez = $this->Free($_robustez);
		$_inc = $this->Free($_inc);
		$_ref = $this->Free($_ref);
		$_modif = $this->Free($_modif);
		$_tecnica = $this->Free($_tecnica);
		$_interno = $this->Free($_interno);
		$_bitacora = $this->Free($_bitacora);
		$_pagina = $this->Free($_pagina);
		$_otros = $this->Free($_otros);
		$_resumen = $this->Free($_resumen);
		$_tratamiento = $this->Free($_tratamiento);
		$_declaracion = $this->Free($_declaracion); 
		
		
		if($_accion=='I'){
			$_cs = $this->ConsecutivoGet($_LID);
			$ok = $this->_TRANS("EXECUTE PA_MAG_112 '{$_cs}', '6', '1', '', '{$_UID}', '{$_LID}';");
		}else $ok = true;  
		
		if($ok){
			$this->_TRANS("EXECUTE PA_MAG_138 '{$_accion}', '{$_cs}',
				'{$_validacion}',
				'{$_objetivo}',
				'{$_analitos}',
				'{$_ambito}',
				'{$_tipo_metodo}',
				'{$_cod_metodo}',
				'{$_precision}',
				'{$_veracidad}',
				'{$_limite1}',
				'{$_limite2}',
				'{$_linealidad}',
				'{$_especificidad}',
				'{$_efecto}',
				'{$_robustez}',
				'{$_inc}',
				'{$_ref}',
				'{$_modif}',
				'{$_tecnica}',
				'{$_interno}',
				'{$_bitacora}',
				'{$_pagina}',
				'{$_equipo}',
				'{$_otros}',
				'{$_resumen}',
				'{$_tratamiento}',
				'{$_declaracion}';");
				
			if($_accion=='I') $cs = $this->ConsecutivoSet($_LID);
			
			return 1;
		}else return 0;
	}
	
	function Validacion6EncabezadoVacio(){
		return array(0=>array(
			'cs'=>'',
			'estado'=>'',
			'matriz'=>'',
			'fecha'=>'',
			'validacion'=>'',
			'analitos'=>'Ver p�gina 2 de este informe',
			'objetivo'=>'Determinar con fundamento estad�stico que el m�todo que se desea utilizar es adecuado para los fines previstos',
			'ambito'=>'(0,01 - 0,35) mg/kg',
			'tipo_metodo'=>'Normalizado con modificaciones',
			'cod_metodo'=>'LAB-LRE-Ex-04 AOAC Official Method 2007.01. Pesticide Residues in Foods by Acetonitrile Extraction and Partitioning with Magnesium Sulfate',
			'precision'=>'RSDr% = 20 %',
			'veracidad'=>'En el intervalo (70-120) %',
			'limite1'=>'3/10 LC',
			'limite2'=>'= 0,01 mg/kg',
			'linealidad'=>'Residual = �20 %',
			'especificidad'=>'= 30 % del LC.',
			'efecto'=>'NA',
			'robustez'=>'Recuperaci�n diaria entre (60-140) %',
			'inc'=>'RSDr < 25 %',
			'ref'=>'LAB-LRE-Ex-10 SANCO/12571/2013 "Guidance document on analytical quality control and validation procedures for pesticide residues analysis in food and feed"',
			'modif'=>'Ver los registros LAB-LRE-R-12 Modificaciones realizadas a metodolog�as oficiales de trabajo, R-01-LAB-LRE-PT-01 Variaciones especiales del procedimiento LAB-LRE-PT-01 y R-01-LAB-LRE-PT-02 Variaciones especiales del procedimiento LAB-LRE-PT-02.',
			'tecnica'=>'Cromatograf�a l�quida acoplada a espectrometr�a de masas masas con extracci�n QuEChERS',
			'interno'=>'LAB-LRE-PT-01 An�lisis multiresiduos de plaguicidas por cromatograf�a l�quida o de gases acopladas a espectrometr�a de masas masas',
			'bitacora'=>'',
			'pagina'=>'',
			'equipo'=>'',
			'tmp_equipo'=>'',
			'otros'=>'',
			'resumen'=>'Ver resultados en la p�gina 2 de este informe',
			'tratamiento'=>'Ver los registros R-02-LAB-LRE-PO-05 Obtenci�n de resultados de validaci�n: muestra de c�lculo y R-03-LAB-LRE-PO-05 Hoja de c�lculo para validaciones respectivos de esta validaci�n',
			'declaracion'=>'Una vez realizados y obtenidos los par�metros de desempe�o que se muestran en la p�gina 2 de este informe, se analizaron los resultados y se determin� que el m�todo utilizado cumple con los criterios establecidos y por tanto es apto (conforme) para utilizarse rutinariamente como ensayo del Laboratorios de An�lisis de Residuos de Agroqu�micos')
		);
	}
	
	/*function Validacion6ReportaValidacion($_validacion){
		$this->_TRANS("UPDATE VAL011 SET reportado = 1 WHERE (validacion='{$_validacion}') AND (reportado = 0);");
		return 1;
	}*/
	
	function Validacion7DetalleGet($_validacion, $_num){
		if($_num==1)
			return $this->_QUERY("SELECT masa, area, res FROM VAL013 WHERE (validacion = '{$_validacion}') ORDER BY linea");
		else
			return $this->_QUERY("SELECT mass, are, reso, enri FROM VAL014 WHERE (validacion = '{$_validacion}') ORDER BY linea");
	}
	
	function Validacion7EncabezadoGet($_validacion){
		return $this->_QUERY("SELECT '{$_validacion}' AS cs, CONVERT(VARCHAR, VAL000.fecha, 105) AS fecha, VAL000.estado, VAL000.obs, VAL012.metodo, VAL012.estandar, VAL012.masa, VAL012.analito, VAL012.pureza, VAL012.vol_ext, VAL012.coefM, VAL012.ali_ext, VAL012.coefB, VAL012.vol_fin, VAL012.curva, VAL012._fexp0, VAL012._fexp1, VAL012._fexp2, VAL012._fteo0, VAL012._fteo1, VAL012._fteo2, EQU000.codigo AS tmp_equipo1, EQU000.nombre
		FROM VAL000 INNER JOIN VAL012 ON VAL000.cs = VAL012.validacion INNER JOIN EQU000 ON VAL012.equipo = EQU000.id
		WHERE (VAL000.cs = '{$_validacion}');");
	}
	
	function Validacion7EncabezadoVacio(){
		return array(0=>array(
			'cs'=>'',
			'fecha'=>'',
			'metodo'=>'',
			'tmp_equipo1'=>'',
			'estandar'=>'',
			'nombre'=>'',
			'masa'=>'0',
			'analito'=>'',
			'pureza'=>'0',
			'vol_ext'=>'0',
			'coefM'=>'0',
			'ali_ext'=>'0',
			'coefB'=>'0',
			'vol_fin'=>'0',
			'curva'=>'0',
			'obs'=>'',
			'estado'=>'',
			'_fexp0'=>'0',
			'_fteo0'=>'0',
			'_fexp1'=>'0',
			'_fteo1'=>'0',
			'_fexp2'=>'0',
			'_fteo2'=>'0')
		);
	}
	
	function Validacion7EncabezadoSet($_tipo, $_metodo, $_equipo, $_estandar, $_masa1, $_analito, $_pureza, $_vol_ext, $_coefM, $_ali_ext, $_coefB, $_vol_fin, $_curva, $_obs, $_masa, $_area, $_res, $_mass, $_are, $_reso, $_enri, $_fexp0, $_fexp1, $_fexp2, $_fteo0, $_fteo1, $_fteo2, $_LID, $_UID){
		$_estandar = $this->Free($_estandar);
		$_metodo = $this->Free($_metodo);
		$_obs = $this->Free($_obs);
		$_cumple = 1;
	
		$cs = $this->ConsecutivoGet($_LID);
	
		if( $this->_TRANS("EXECUTE PA_MAG_112 '{$cs}', '{$_tipo}', '{$_cumple}', '{$_obs}', '{$_UID}', '{$_LID}';") ){
			
			$this->_TRANS("INSERT INTO VAL012 VALUES('{$cs}', '{$_metodo}',
				{$_equipo},
				'{$_estandar}',
				{$_masa1},
				{$_analito},
				{$_pureza},
				{$_vol_ext},
				{$_coefM},
				{$_ali_ext},
				{$_coefB},
				{$_vol_fin},
				{$_curva},
				{$_fexp0},
				{$_fexp1},
				{$_fexp2},
				{$_fteo0},
				{$_fteo1},
				{$_fteo2}
			);");
			
			$_masa = explode('&', substr($_masa, 1));
			$_area = explode('&', substr($_area, 1));
			$_res = explode('&', substr($_res, 1));
			$_mass = explode('&', substr($_mass, 1));
			$_are = explode('&', substr($_are, 1));
			$_reso = explode('&', substr($_reso, 1));
			$_enri = explode('&', substr($_enri, 1));	
			
			for($i=1;$i<count($_masa);$i++){
				if(str_replace(' ', '', $_masa[$i]) != '')
					$this->_TRANS("INSERT INTO VAL013 values('{$cs}', {$i}, {$_masa[$i]}, {$_area[$i]}, {$_res[$i]});");
			}
			
			for($i=1;$i<count($_mass);$i++){
				if(str_replace(' ', '', $_mass[$i]) != '')
					$this->_TRANS("INSERT INTO VAL014 values('{$cs}', {$i}, {$_mass[$i]}, {$_are[$i]}, {$_reso[$i]}, {$_enri[$i]});");
			}
			
			$cs = $this->ConsecutivoSet($_LID);
			
			return 1;
		}else return 0;
	}
	
	function Validacion7DetalleVacio($_num){
		if($_num==1){
			return array(
				0=>array('masa'=>'0','area'=>'0','res'=>'0'),
				1=>array('masa'=>'0','area'=>'0','res'=>'0'),
				2=>array('masa'=>'0','area'=>'0','res'=>'0')
			);
		}elseif($_num==2){
			return array(
				0=>array('mass'=>'0','are'=>'0','reso'=>'0','enri'=>'0'),
				1=>array('mass'=>'0','are'=>'0','reso'=>'0','enri'=>'0'),
				2=>array('mass'=>'0','are'=>'0','reso'=>'0','enri'=>'0'),
				3=>array('mass'=>'0','are'=>'0','reso'=>'0','enri'=>'0'),
				4=>array('mass'=>'0','are'=>'0','reso'=>'0','enri'=>'0'),
				5=>array('mass'=>'0','are'=>'0','reso'=>'0','enri'=>'0'),
				6=>array('mass'=>'0','are'=>'0','reso'=>'0','enri'=>'0'),
				7=>array('mass'=>'0','are'=>'0','reso'=>'0','enri'=>'0'),
				8=>array('mass'=>'0','are'=>'0','reso'=>'0','enri'=>'0')
			);
		}
	}
	
	function Validacion8EncabezadoGet($_validacion){
		return $this->_QUERY("SELECT '{$_validacion}' AS cs, CONVERT(VARCHAR, VAL000.fecha, 105) AS fecha, VAL000.estado, VAL000.obs, VAL015.metodo, VAL015.estandar, VAL015.masa, VAL015.analito, VAL015.pureza, VAL015.vol_ext, VAL015.coefM, VAL015.ali_ext, VAL015.coefB, VAL015.vol_fin, VAL015.curva, VAL015.fac1, VAL015.fac2, VAL015.mas1, VAL015.mas2, VAL015.men1, VAL015.men2, VAL015.mass0, VAL015.mass1, VAL015.mass2, VAL015.mass3, VAL015.area0, VAL015.area1, VAL015.area2, VAL015.area3, EQU000.codigo AS tmp_equipo1, EQU000.nombre
		FROM VAL015 INNER JOIN VAL000 ON VAL015.validacion = VAL000.cs INNER JOIN EQU000 ON VAL015.equipo = EQU000.id
		WHERE (VAL000.cs = '{$_validacion}');");
	}
	
	function Validacion8EncabezadoVacio(){
		return array(0=>array(
			'cs'=>'',
			'fecha'=>'',
			'metodo'=>'',
			'tmp_equipo1'=>'',
			'estandar'=>'',
			'nombre'=>'',
			'masa'=>'0',
			'analito'=>'',
			'pureza'=>'0',
			'vol_ext'=>'0',
			'coefM'=>'0',
			'ali_ext'=>'0',
			'coefB'=>'0',
			'vol_fin'=>'0',
			'curva'=>'0',
			'obs'=>'',
			'estado'=>'',
			'fac1'=>'',
			'fac2'=>'',
			'mas1'=>'',
			'mas2'=>'',
			'men1'=>'',
			'men2'=>'',
			'mass0'=>'0',
			'mass1'=>'0',
			'mass2'=>'0',
			'mass3'=>'0',
			'area0'=>'0',
			'area1'=>'0',
			'area2'=>'0',
			'area3'=>'0')
		);
	}
	
	function Validacion8EncabezadoSet($_tipo, $_metodo, $_equipo, $_estandar, $_masa1, $_analito, $_pureza, $_vol_ext, $_coefM, $_ali_ext, $_coefB, $_vol_fin, $_curva, $_obs, $_fac1, $_fac2, $_mas1, $_mas2, $_men1, $_men2, $_mass0, $_mass1, $_mass2, $_mass3, $_area0, $_area1, $_area2, $_area3, $_LID, $_UID){
		$_estandar = $this->Free($_estandar);
		$_metodo = $this->Free($_metodo);
		$_obs = $this->Free($_obs);
		$_fac1 = $this->Free($_fac1);
		$_fac2 = $this->Free($_fac2);
		$_mas1 = $this->Free($_mas1);
		$_mas2 = $this->Free($_mas2);
		$_men1 = $this->Free($_men1);
		$_men2 = $this->Free($_men2);
		$_cumple = 1;
	
		$cs = $this->ConsecutivoGet($_LID);
	
		if( $this->_TRANS("EXECUTE PA_MAG_112 '{$cs}', '{$_tipo}', '{$_cumple}', '{$_obs}', '{$_UID}', '{$_LID}';") ){
			
			$this->_TRANS("INSERT INTO VAL015 VALUES('{$cs}', '{$_metodo}',
				{$_equipo},
				'{$_estandar}',
				{$_masa1},
				{$_analito},
				{$_pureza},
				{$_vol_ext},
				{$_coefM},
				{$_ali_ext},
				{$_coefB},
				{$_vol_fin},
				{$_curva},
				'{$_fac1}',
				'{$_fac2}',
				'{$_mas1}',
				'{$_mas2}',
				'{$_men1}',
				'{$_men2}',
				{$_mass0},
				{$_mass1},
				{$_mass2},
				{$_mass3},
				{$_area0},
				{$_area1},
				{$_area2},
				{$_area3}
			);");
			
			$cs = $this->ConsecutivoSet($_LID);
			
			return 1;
		}else return 0;
	}
	
	function ValidacionAccion($_var){
		if($_var=='0') return 'Realizado por:';
		elseif($_var=='1') return 'Procesado por:';
		elseif($_var=='2') return 'Aprobado por:';
		elseif($_var=='3') return 'Revisada por:';
		elseif($_var=='5') return 'Anulado por:';
		else return 'N/A';
	}
	
	function ValidacionesEstado($_var){
		if($_var=='0') return 'Generada';
		elseif($_var=='1') return 'Pend. Revisi�n';
		elseif($_var=='2') return 'Aprobada';
		elseif($_var=='3') return 'Pend. Aprobaci�n';
		elseif($_var=='5') return 'Anulada';
		else return 'N/A';
	}
	
	function ValidacionesGet($_LAB, $_desde, $_hasta, $_tipo, $_estado){
		if($_tipo == '') $op = '<>';
		else $op = '=';
		
		if($_estado == '') $op2 = '<>';
		else $op2 = '=';
		$_desde = $this->_FECHA($_desde);
		$_hasta = $this->_FECHA($_hasta);
		
		if($_LAB=='1')
			$SQL = "SELECT VAR001.nombre AS matriz, VAL000.cs, VAL000.cs AS validacion, CONVERT(VARCHAR, VAL000.fecha, 105) AS fecha1, VAL000.tipo, VAL000.cumple, VAL000.estado
			FROM VAL000 INNER JOIN VAL009 ON VAL000.cs = VAL009.validacion INNER JOIN VAR001 ON VAL009.matriz = VAR001.id
			WHERE (VAL000.estado {$op2} '{$_estado}')
			UNION
			SELECT '' AS matriz, VAL000.cs, VAL000.cs AS validacion, CONVERT(VARCHAR, VAL000.fecha, 105) AS fecha1, VAL000.tipo, VAL000.cumple, VAL000.estado
			FROM VAL000 
			WHERE (VAL000.estado {$op2} '{$_estado}') AND (tipo = '6');";
		else
			$SQL = "SELECT cs, cs AS validacion, CONVERT(VARCHAR, fecha, 105) AS fecha1, tipo, cumple, estado
			FROM VAL000 
			WHERE (lab = '{$_LAB}') AND (tipo {$op} '{$_tipo}') AND (estado {$op2} '{$_estado}')
			ORDER BY fecha;";
		
		return $this->_QUERY($SQL);
	}
	
	function ValidacionHistorial($_cs){
		return $this->_QUERY("SELECT SEG001.nombre, SEG001.ap1, CONVERT(VARCHAR, VALBIT.fecha, 105) AS fecha1, VALBIT.accion
		FROM VALBIT INNER JOIN SEG001 ON VALBIT.analista = SEG001.id
		WHERE (VALBIT.validacion = '{$_cs}') AND (VALBIT.accion <> '0');");
	}
	
	function ValidacionModificaEstado($_cs, $_estado, $_UID){
		$this->_TRANS("UPDATE VAL000 SET fecha=GETDATE(), estado='{$_estado}' WHERE (cs='{$_cs}');");
		$this->_TRANS("INSERT INTO VALBIT VALUES('{$_cs}', GETDATE(), {$_UID}, '{$_estado}');");
		return 1;
	}	
	
	function ValidacionesTipo($_var){
		if($_var=='1') return 'Linealidad';
		elseif($_var=='2') return 'Repetibilidad';
		elseif($_var=='3') return 'Precisi�n Intermedia';
		elseif($_var=='4') return 'Biolog�a Molecular';
		elseif($_var=='5') return 'C�lculo para validaciones';
		elseif($_var=='6') return 'Informe de validaci�n';
		elseif($_var=='7') return 'Recuperaci�n';
		elseif($_var=='8') return 'Robustez';
		else return 'N/A';
	}		
//
}
?>