<?php

/* * *****************************************************************
  CLASE DE NIVEL 0 UTILIZADA PARA CONEXION A LA BD
  (abstract SOLO PUEDE SER HEREDADA Y NO INSTANCIADA)
 * ***************************************************************** */

abstract class Database
{

    private $_CONN = '';
    private $_DB = '';
    private $_HOST = '';
    private $_OK = '';
    private $_PWD = '';
    private $_STMT = '';
    private $_UID = '';

    //METODOS PRIVADOS
    /* final public function _FECHA($_fecha){
      $_fecha = str_replace('-','/',$_fecha);
      return $_fecha;
      } */

    /* final public function _FECHA($_fecha){
      $_token = explode('-',$_fecha);
      $_fecha = ''.$_token[2].'-'.$_token[1].'-'.$_token[0];
      return $_fecha;
      } */

    final public function _FECHA($_fecha)
    {
        if ($_fecha != '') {
            $_token = explode('-', $_fecha);
            $_fecha = '' . $_token[2] . $_token[1] . $_token[0];
            return $_fecha;
        } else {
            return '';
        }
    }

    final public function _FECHA2($_fecha)
    {
        if ($_fecha != '') {
            $_token = explode('-', $_fecha);
            $_fecha = '' . $_token[2] . $_token[1] . $_token[0];
            return $_fecha;
        } else {
            return '';
        }
    }

    final public function _QUERY($_sql)
    {
        $this->Ini($_sql);
        if ($this->_OK == 0)
            return false;
        //
        $x = 0;
        $a = array();
        if (PHP_OS == 'Linux') {
            while ($row = mssql_fetch_array($this->_STMT)) {
                $a[$x] = $row;
                $x++;
            }
        } else {
            while ($row = sqlsrv_fetch_array($this->_STMT, SQLSRV_FETCH_ASSOC)) {
                $a[$x] = $row;
                $x++;
            }
        }

        $this->Fin();
        return $a;
    }

    final public function _TRANS($_sql)
    {
        $this->Ini($_sql);
        $this->Fin();
        return $this->_OK;
    }

    /* function AlertaUsuarios($_LAB, $_tipo, $uno='TOP(1)'){
      $ROW = $this->_QUERY("SELECT {$uno} SEG001.id, SEG001.email
      FROM SEG001 INNER JOIN SEG007 ON SEG001.id = SEG007.usuario INNER JOIN SEG002 ON SEG007.perfil = SEG002.id INNER JOIN SEG004 ON SEG002.id = SEG004.perfil
      WHERE (SEG002.lab = '{$_LAB}') AND (SEG004.permiso = '{$_tipo}');");

      for($x=0;$x<count($ROW);$x++){
      $this->TablonSet($_tipo, $ROW[$x]['id'], $_tipo, false);
      }
      } */

    /* final public function Email($_destino, $_msj, $_emisor=''){
      $destino = str_replace(' ', '', $_destino);
      $destino = explode(',', $_destino);
      $para = '';
      //SI SON VARIOS
      if( count($destino) > 1 ){
      //SE SALTA EL PRIMERO PARA PONERLO DESPUES COMO ENCABEZADO DEL MENSAJE
      for($x=0;$x<count($destino);$x++){
      if($x == 0)
      $para = '"'.$destino[$x];
      else
      $para .= ','.$destino[$x];
      }
      $para .= '"';
      }else $para = $destino[0];

      @mail($para, 'SFE - Notificaci?n de sistema', $_msj);
      //if($ok) return 1;
      //else return '-3';
      return 1;
      } */

    final public function Email($_destino, $_msj, $_emisor = '')
    {
        if (!isset($mail))
            include('Mail.inc');
        $_destino = str_replace(' ', '', $_destino);
        $_destino = explode(',', $_destino);
        //SI SON VARIOS
        if (count($_destino) > 1) {
            //SE SALTA EL PRIMERO PARA PONERLO DESPUES COMO ENCABEZADO DEL MENSAJE
            for ($x = 1; $x < count($_destino); $x++) {
                $mail->addRecipient($_destino[$x]);
            }
        }
        $_destino = $_destino[0];

        $ok = $mail->send($_emisor, $_destino, 'SFE - Notificacion de sistema', $_msj);
        $mail->clearRecipients();
        //if($ok) return 1;
        //else return '-3';
        return 1;
    }

    private function Fin()
    {
        if (PHP_OS == 'Linux') {
            if (gettype($this->_STMT) != 'boolean')
                mssql_free_result($this->_STMT);
            mssql_close($this->_CONN);
        } else {
            if ($this->_STMT)
                sqlsrv_free_stmt($this->_STMT);
            sqlsrv_close($this->_CONN);
        }

        $this->_STMT = '';
        $this->_CONN = '';
    }

    final public function Free($_VAR)
    {
        //SE USA CON LOS AJAX
        $v1 = array('"', "'", '�', '`', '[', ']', '{', '}');
        $_VAR = str_replace($v1, ' ', $_VAR);
        $_VAR = utf8_decode($_VAR);
        return $_VAR;
    }

    final public function FreeAcento($_VAR)
    {
        //SE USA CON LOS ACENTOS
        return preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml|caron);~i', '$1', htmlentities($_VAR, ENT_COMPAT, 'UTF-8'));
    }

    final public function FreePost($_VAR)
    {
        //SE USA CON LOS GET/POST
        $v1 = array('"', "'", '�', '`', '[', ']', '{', '}');
        $_VAR = str_replace($v1, ' ', $_VAR);
        return $_VAR;
    }

    private function Ini($_sql)
    {
        if ($this->_DB == '') {
            include 'GOD.inc';
        }
        $this->_OK = 1;

        if (PHP_OS == 'Linux') {
            $this->_CONN = mssql_connect("MAG", "sa", "saturno");
            if ($this->_CONN === false) {
                die('Unable to connect.</br>' . print_r(mssql_get_last_message(), true));
            }
            $msdb = mssql_select_db("MAGI", $this->_CONN);
            $this->_STMT = mssql_query($_sql);
        } else {
            $this->_CONN = sqlsrv_connect($this->_HOST, array('UID' => $this->_UID, 'PWD' => $this->_PWD, 'Database' => $this->_DB));
            if ($this->_CONN === false) {
                die('Unable to connect.</br>' . print_r(sqlsrv_errors(), true));
            }
            $this->_STMT = sqlsrv_query($this->_CONN, $_sql);
        }

        if ($this->_STMT === false) {
            $this->_OK = 0;
        }
    }

    final public function Logger($_acc)
    {
        //session_start();
        $this->_TRANS("EXECUTE LOGGER {$_SESSION['_UID_']}, '{$_acc}';");
    }

    final public function obtieneConsecutivo($_columna, $_tabla)
    {
        $Q = $this->_QUERY("SELECT {$_columna} FROM {$_tabla};");
        if ($Q) {
            $this->_TRANS("UPDATE {$_tabla} SET {$_columna} = {$_columna} + 1;");
        } else {
            $Q[0][$_columna] = 0;
        }
        return $Q[0][$_columna];
    }

    /* METODOS FINAL NO PUEDEN SER REDEFINIDOS */

    final public function Rand()
    {
        $letras = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $tam = strlen($letras);
        $str = '';
        for ($i = 0; $i < 20; $i++) {
            $str .= $letras[rand(0, $tam - 1)];
        }
        return $str;
    }

    final public function TablonDel($_id)
    {
        $this->_TRANS("DELETE FROM MAG002 WHERE (id = '{$_id}');");
    }

    final public function TablonDel2($_clasificacion, $_usuario, $_ref)
    {
        $this->_TRANS("DELETE FROM MAG002 WHERE (clasificacion = {$_clasificacion}) AND (usuario = {$_usuario}) AND (referencia = {$_ref});");
    }

    final public function TablonGet($_uid)
    {
        return $this->_QUERY("SELECT SEG000.carpeta AS modulo, MAG001.pagina, MAG001.descripcion, MAG002.id, CONVERT(VARCHAR, MAG002.fecha, 105) AS fecha1
		FROM MAG001 INNER JOIN MAG002 ON MAG001.id = MAG002.clasificacion INNER JOIN SEG003 ON MAG001.permiso = SEG003.id INNER JOIN SEG000 ON SEG003.modulo = SEG000.id
		WHERE (MAG002.usuario = {$_uid})
		ORDER BY MAG002.fecha;");
    }

    final public function TablonSet($_tipo, $_usuario, $_ref, $_sender = true)
    {
        //INSERTA EN EL TABLON
        $cs = $this->Rand();

        $this->_TRANS("EXECUTE BLOG '{$cs}', '{$_tipo}', '{$_usuario}', {$_ref};");
        if ($_sender) {
            $ROW = $this->_QUERY("EXECUTE SENDER '{$_tipo}', '{$_usuario}';");
            if (!$ROW)
                return false;
            //ENVIA NOTIFICACION
            $this->Email($ROW[0]['destino'], $ROW[0]['msj']);
        }
        return 1;
    }

    final public function obtieneEncabezadoPie($_hoja, $_tipo, $_titulo)
    {
        if ($_tipo == 'e') {
            $ROW = $this->_QUERY("SELECT CCA013.hoja, CCA013.codigo, CCA013.version, CCA013.nombre, CCA013.rige, CCA013.encabezado, CCA013.pie, CCA013.enombre, CCA013.epuesto, CCA013.efecha, CCA013.rnombre, CCA013.rpuesto, CCA013.rfecha, CCA013.anombre, CCA013.apuesto, CCA013.afecha, CCA015.detalle FROM CCA013 LEFT JOIN CCA015 ON CCA013.encabezado = CCA015.id WHERE CCA013.hoja = '{$_hoja}';");
        } else {
            $ROW = $this->_QUERY("SELECT CCA013.hoja, CCA013.codigo, CCA013.version, CCA013.nombre, CCA013.rige, CCA013.encabezado, CCA013.pie, CCA013.enombre, CCA013.epuesto, CCA013.efecha, CCA013.rnombre, CCA013.rpuesto, CCA013.rfecha, CCA013.anombre, CCA013.apuesto, CCA013.afecha, CCA015.detalle FROM CCA013 LEFT JOIN CCA015 ON CCA013.pie = CCA015.id WHERE CCA013.hoja = '{$_hoja}';");
        }
        if ($ROW) {
            $detalle = $this->preparaEncabezadoPie($ROW[0]['detalle'], $_tipo);
            if ($_tipo == 'e') {
                $detalle = str_replace('@codigo', $ROW[0]['codigo'], $detalle);
                $detalle = str_replace('@version', $ROW[0]['version'], $detalle);
                $detalle = str_replace('@nombre', $ROW[0]['nombre'], $detalle);
                $detalle = str_replace('@fecha', $ROW[0]['rige'], $detalle);
                $detalle = str_replace('#', 1, $detalle);
                $elaboro = "<b>Elabor&oacute;: </b>".$ROW[0]['enombre']."<br>".$ROW[0]['epuesto']."<br>".$ROW[0]['efecha'];
                $reviso = "<b>Revis&oacute;: </b>".$ROW[0]['rnombre']."<br>".$ROW[0]['rpuesto']."<br>".$ROW[0]['rfecha'];
                $aprobo = "<b>Aprob&oacute;: </b>".$ROW[0]['anombre']."<br>".$ROW[0]['apuesto']."<br>".$ROW[0]['afecha'];
                $detalle = str_replace('@elaboro', $elaboro, $detalle);
                $detalle = str_replace('@reviso', $reviso, $detalle);
                $detalle = str_replace('@aprobo', $aprobo, $detalle);
            }else{
                $elaboro = "<b>Elabor&oacute;: </b>".$ROW[0]['enombre']."<br>".$ROW[0]['epuesto']."<br>".$ROW[0]['efecha'];
                $reviso = "<b>Revis&oacute;: </b>".$ROW[0]['rnombre']."<br>".$ROW[0]['rpuesto']."<br>".$ROW[0]['rfecha'];
                $aprobo = "<b>Aprob&oacute;: </b>".$ROW[0]['anombre']."<br>".$ROW[0]['apuesto']."<br>".$ROW[0]['afecha'];
                $detalle = str_replace('@elaboro', $elaboro, $detalle);
                $detalle = str_replace('@reviso', $reviso, $detalle);
                $detalle = str_replace('@aprobo', $aprobo, $detalle);
            }
            return $detalle;
        } else {
            return '';
        }
    }

    final public function preparaEncabezadoPie($_detalle, $_tipo)
    {
        $_detalle = str_replace('table class="MsoNormalTable"', 'table style="width:98%"', $_detalle);
        $_detalle = str_replace('<table>', '<table style="width:100%" border="1" cellpadding="0" cellspacing="0">', $_detalle);
        return $_detalle;
    }

}

?>