<?php

/* * *****************************************************************
  CLASE DE NIVEL 1 ENCARGADA DEL MODULO DE SERVICIO AL CLIENTE
  (final SOLO PUEDE SER INSTANCIADA Y NO HEREDADA)
 * ***************************************************************** */

final class Sc extends Database
{

//
    private function _RAND()
    {
        $letras = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $tam = strlen($letras);
        $str = '';
        for ($i = 0; $i < 10; $i++)
            $str .= $letras[rand(0, $tam - 1)];
        return $str;
    }

    function ClientesContactosVacio()
    {
        return array(0 => array(
            'contacto' => '',
            'telefono' => '',
            'email' => ''
        ));
    }

    function ClientesContactosGet($_id)
    {
        return $this->_QUERY("SELECT contacto, telefono, email 
		FROM MAN013 
		WHERE (cliente = '{$_id}');");
    }

    function ClientesReenvio($_id, $_email)
    {
        $ROW = $this->_QUERY("SELECT clave FROM MAN002 WHERE (id='{$_id}');");
        $clave = base64_decode($ROW[0]['clave']);

        $Robot = new Seguridad();
        $Robot->Email($_email, "Reenv�o de clave para ingreso al sistema: {$clave}");

        $this->Logger('0500');
        return 1;
    }

    function ClientesDetalleGet($_id)
    {
        return $this->_QUERY("SELECT id, nombre, unidad, representante, tel, email, fax, CONVERT(TEXT, direccion) AS direccion, CONVERT(TEXT, actividad) AS actividad, tipo, LRE, LDP, LCC, estado
		FROM MAN002 WHERE (id='{$_id}') AND (estado <> '2');");
    }

    function ClientesTipoGet($_id)
    {
        $ROW = $this->_QUERY("SELECT tipo FROM MAN002 WHERE (id='{$_id}');");
        return $ROW[0]['tipo'];
    }

    function ClientesIME($_id, $_nombre, $_unidad, $_representante, $_tel, $_email, $_Vcontacto, $_Vtelefonos, $_Vcorreo, $_Vsolicitante, $_Vtel, $_Vsolcorreo, $_fax, $_direccion, $_actividad, $_tipo, $_LRE, $_LDP, $_LCC, $_estado, $_accion)
    {
        $_nombre = $this->Free($_nombre);
        $_unidad = $this->Free($_unidad);
        $_representante = $this->Free($_representante);
        $_email = $this->Free($_email);
        /**/
        $_Vcontacto = str_replace('1=1&', '', $_Vcontacto);
        $_Vcontacto = explode('&', $_Vcontacto);
        $_Vtelefonos = str_replace('1=1&', '', $_Vtelefonos);
        $_Vtelefonos = explode('&', $_Vtelefonos);
        $_Vcorreo = str_replace('1=1&', '', $_Vcorreo);
        $_Vcorreo = explode('&', $_Vcorreo);
        /**/
        $_Vsolicitante = str_replace('1=1&', '', $_Vsolicitante);
        $_Vsolicitante = explode('&', $_Vsolicitante);
        $_Vtel = str_replace('1=1&', '', $_Vtel);
        $_Vtel = explode('&', $_Vtel);
        $_Vsolcorreo = str_replace('1=1&', '', $_Vsolcorreo);
        $_Vsolcorreo = explode('&', $_Vsolcorreo);
        /**/
        $_fax = $this->Free($_fax);
        $_direccion = $this->Free($_direccion);
        $_actividad = $this->Free($_actividad);
        $_clave = base64_encode($this->_RAND());
        $_id = str_replace(' ', '', $_id);

        if ($this->_TRANS("EXECUTE PA_MAG_023 '{$_id}', '{$_nombre}', '{$_unidad}', '{$_representante}', '{$_tel}', '{$_email}', '{$_fax}', '{$_direccion}', '{$_actividad}', '{$_tipo}', '{$_LRE}', '{$_LDP}', '{$_LCC}', '{$_estado}', '{$_clave}', '{$_accion}';")) {
            //CONTACTOS
            if ($_accion == 'I' or $_accion == 'M') {
                $this->_TRANS("DELETE FROM MAN013 WHERE cliente = '{$_id}';");
                $this->_TRANS("DELETE FROM MAN014 WHERE cliente = '{$_id}';");
                for ($i = 0; $i < count($_Vcontacto); $i++) {
                    $_Vcontacto[$i] = $this->Free($_Vcontacto[$i]);
                    $_Vtelefonos[$i] = $this->Free($_Vtelefonos[$i]);

                    if (str_replace(' ', '', $_Vcontacto[$i]) != '') {
                        $this->_TRANS("INSERT INTO MAN013 values('{$_id}', '{$i}', '{$_Vcontacto[$i]}', '{$_Vtelefonos[$i]}', '{$_Vcorreo[$i]}');");
                    }
                }
                for ($i = 0; $i < count($_Vsolicitante); $i++) {
                    $_Vsolicitante[$i] = $this->Free($_Vsolicitante[$i]);
                    $_Vtel[$i] = $this->Free($_Vtel[$i]);

                    if (str_replace(' ', '', $_Vsolicitante[$i]) != '') {
                        $this->_TRANS("INSERT INTO MAN014 values('{$_id}', '{$i}', '{$_Vsolicitante[$i]}', '{$_Vtel[$i]}', '{$_Vsolcorreo[$i]}');");
                    }
                }
            }
            //
            if (isset($_SESSION['_UID_'])) {
                if ($_accion == 'I')
                    $_accion = '0501';
                elseif ($_accion == 'M')
                    $_accion = '0502';
                elseif ($_accion == 'D')
                    $_accion = '0503';
                $this->Logger($_accion);
            }
            //
            return 1;
        } else
            return 0;
    }

    function Clientes_CambiaID($_idActual, $_idNuevo)
    {
        if (!$this->_QUERY("SELECT 1 FROM MAN002 WHERE id='{$_idNuevo}'")) {
            //$this->_TRANS("EXECUTE PA_MAG_147 '{$_idActual}','{$_idNuevo}'");
            //return 1;
            $data = $this->_QUERY("SELECT 
       nombre,
       unidad,
       representante,
       tel,
       email,
       fax,
       direccion,
       actividad,
       tipo,
       lre,
       ldp,
       lcc,
       estado,
       clave
FROM   man002
WHERE  id = '{$_idActual}'");

            return $this->_TRANS(" INSERT INTO man002
            (id,
             nombre,
             unidad,
             representante,
             tel,
             email,
             fax,
             direccion,
             actividad,
             tipo,
             lre,
             ldp,
             lcc,
             estado,
             clave) VALUES (
             '{$_idNuevo}',
             '{$data[0]['nombre']}',
             '{$data[0]['unidad']}',
             '{$data[0]['representante']}',
             '{$data[0]['tel']}',
             '{$data[0]['email']}',
             '{$data[0]['fax']}',
             '{$data[0]['direccion']}',
             '{$data[0]['actividad']}',
             '{$data[0]['tipo']}',
             '{$data[0]['lre']}',
             '{$data[0]['ldp']}',
             '{$data[0]['lcc']}',
             '{$data[0]['estado']}',
             '{$data[0]['clave']}'
             );") &&
                $this->_TRANS("UPDATE cer001
SET    cliente = '{$_idNuevo}'
WHERE  cliente = '{$_idActual}';") &&
                $this->_TRANS("UPDATE cot000
SET    cliente = '{$_idNuevo}'
WHERE  cliente = '{$_idActual}';") &&
                $this->_TRANS("UPDATE cot005
SET    cliente = '{$_idNuevo}'
WHERE  cliente = '{$_idActual}';") &&
                $this->_TRANS("UPDATE man013
SET    cliente = '{$_idNuevo}'
WHERE  cliente = '{$_idActual}';") &&
                $this->_TRANS("UPDATE man014
SET    cliente = '{$_idNuevo}'
WHERE  cliente = '{$_idActual}';") &&
                $this->_TRANS("UPDATE scl004
SET    cliente = '{$_idNuevo}'
WHERE  cliente = '{$_idActual}';") &&
                $this->_TRANS("UPDATE scl007
SET    cliente = '{$_idNuevo}'
WHERE  cliente = '{$_idActual}';") &&
                $this->_TRANS("UPDATE ser001
SET    cliente = '{$_idNuevo}'
WHERE  cliente = '{$_idActual}';") &&
                $this->_TRANS("UPDATE ser003
SET    cliente = '{$_idNuevo}'
WHERE  cliente = '{$_idActual}';") &&
                $this->_TRANS("DELETE FROM man002
WHERE  id = '{$_idActual}';  ");
        } else {
            return 2;
        }
    }

    function ClientesResumenGet($_id, $_nombre, $_filtro, $_lab, $_busqueda = 1, $ordenado = false)
    {
        if ($_busqueda == 1)
            $extra = " (id LIKE '%{$_id}%')";
        else
            $extra = " (nombre LIKE '%{$_nombre}%')";
        //ESTA BUSCANDO POR EL LAB ESPECIFICO
        if ($_filtro == '1') {
            if ($_lab == '1')
                $extra .= " AND (LRE = 1)";
            elseif ($_lab == '2')
                $extra .= " AND (LDP = 1)";
            elseif ($_lab == '3')
                $extra .= " AND (LCC = 1)";
        }

        if ($ordenado)
            $extra2 = 'ORDER BY nombre';
        else
            $extra2 = '';

        return $this->_QUERY("SELECT id, nombre, tel, direccion,
		  estado = CASE estado
			  WHEN '0' THEN 'Inactivo'
			  WHEN '1' THEN 'Activo'
			  ELSE 'N/A'
		  END
		  FROM MAN002 
		  WHERE estado <> '2' AND {$extra} AND (id <> '0              ') {$extra2};");
    }

    function SolicitanteResumenGet($_id, $_nombre, $_filtro, $_cliente, $_busqueda = 1, $ordenado = false)
    {

        return $this->_QUERY("SELECT contacto FROM MAN014 WHERE cliente = '{$_cliente}';");
    }

    function SolicitanteGet($_cliente)
    {
        return $this->_QUERY("SELECT TOP 1 contacto FROM MAN014 WHERE cliente = '{$_cliente}';");
    }

    function ClientesSolicitantesGet($_id)
    {
        return $this->_QUERY("SELECT contacto, telefono, email 
		FROM MAN014 
		WHERE (cliente = '{$_id}');");
    }

    function ClientesSolicitantesVacio()
    {
        return array(0 => array(
            'contacto' => '',
            'telefono' => '',
            'email' => ''
        ));
    }

    function ClientesVacio()
    {
        return array(0 => array(
            'id' => '',
            'nombre' => '',
            'unidad' => '',
            'representante' => '',
            'tel' => '',
            'email' => '',
            'fax' => '',
            'direccion' => '',
            'actividad' => '',
            'tipo' => '',
            'LRE' => '0',
            'LDP' => '0',
            'LCC' => '0',
            'estado' => ''
        ));
    }

    function clienteGet($id, $lab = '')
    {
        $str = "SELECT id,nombre,unidad,representante,tel,email,fax,direccion,actividad,tipo,LRE,LDP,LCC,estado,clave FROM MAN002 WHERE 1=1 ";
        if ($id != "") {
            $str .= "AND id='{$id}'";
        }
        if ($lab != "") {
            if ($lab == "1") {
                $str .= "AND LRE=1";
            } elseif ($lab == "2") {
                $str .= "AND LDP=1";
            } else {
                $str .= "AND LCC=1";
            }
        }
        return $this->_QUERY($str);
    }

    function EncuestaDetalle($_id)
    {
        return $this->_QUERY("SELECT id, nombre, estado FROM SCL001 WHERE (id={$_id});");
    }

    function EncuestaIME($_id, $_nombre, $_estado, $_accion)
    {
        $_nombre = $this->Free($_nombre);

        if ($this->_TRANS("EXECUTE PA_MAG_005 '{$_id}', '{$_nombre}', '{$_estado}', '{$_accion}';")) {
            if ($_accion == 'D')
                Bibliotecario::ZipEliminar("../../docs/sc/encuestas/{$_id}.zip");

            //
            if ($_accion == 'I')
                $_accion = '0504';
            elseif ($_accion == 'M')
                $_accion = '0505';
            else
                $_accion = '0506';
            $this->Logger($_accion);
            //
            return 1;
        } else
            return 0;
    }

    function EncuestaResponde($_usuario, $_tipos, $_preguntas1, $_respuestas1, $_respuestas2)
    {
        $_tipos = explode(';', substr($_tipos, 1));
        $_preguntas1 = explode(';', substr($_preguntas1, 1));
        $_respuestas1 = explode(';', substr($_respuestas1, 1));
        $_respuestas2 = explode(';', substr($_respuestas2, 1));
        //
        $ROW = $this->_QUERY("SELECT respuestas AS cs FROM MAG000;");
        $cs = $ROW[0]['cs'];
        //INGRESA PREGUNTAS NORMALES
        for ($x = 0; $x < count($_preguntas1); $x++) {
            $this->_TRANS("INSERT INTO SCL004 VALUES({$cs}, {$_preguntas1[$x]}, GETDATE(), '{$_usuario}');");
            if ($_tipos[$x] == '1' or $_tipos[$x] == '4') //texto
                $this->_TRANS("INSERT INTO SCL006 VALUES({$cs}, '{$_respuestas1[$x]}');");
            elseif ($_tipos[$x] == '0' or $_tipos[$x] == '2')
                $this->_TRANS("INSERT INTO SCL005 VALUES({$cs}, 0, {$_respuestas1[$x]});");
            $cs++;
        }

        unset($_tipos, $_preguntas1, $_respuestas1);
        //INGRESA PREGUNTAS SELECCION MULTIPLE
        for ($x = 0; $x < count($_respuestas2); $x++) {
            $tmp = explode(':', $_respuestas2[$x]); //SUBDIVIDE LA PREGUNTA Y SUS RESPUESTAS EJE 10:,1,3,5
            $pregunta = $tmp[0];
            $respuestas3 = explode(',', substr($tmp[1], 1)); //QUITO EL PRIMER CARACTER QUE ES UNA COMA
            //
            $this->_TRANS("INSERT INTO SCL004 VALUES({$cs}, {$pregunta}, GETDATE(), '{$_usuario}');");
            for ($y = 0; $y < count($respuestas3); $y++) {
                $this->_TRANS("INSERT INTO SCL005 VALUES({$cs}, {$y}, {$respuestas3[$y]});");
            }
            $cs++;
        }
        //ACTUALIZA CONSECUTIVO
        $this->_TRANS("UPDATE MAG000 SET respuestas = {$cs};");
        //BORRA PETICION DE ENCUESTA
        return $this->_TRANS("DELETE FROM SCL007 WHERE cliente = '{$_usuario}';");
    }

    function EncuestaMuestra($_estado)
    {
        if ($_estado != '')
            return $this->_QUERY("SELECT id, CONVERT(VARCHAR, fecha, 105) AS fecha, nombre, estado 
			FROM SCL001 
			WHERE (estado='{$_estado}');");
        else
            return $this->EncuestaVacio();
    }

    function EncuestasUsuarioGet($_cliente)
    {
        return $this->_QUERY("SELECT SCL001.id, CONVERT(VARCHAR, SCL001.fecha, 105) AS fecha, SCL001.nombre
		FROM SCL001 INNER JOIN SCL007 ON SCL001.id = SCL007.encuesta
		WHERE (SCL007.cliente = '{$_cliente}') AND (SCL001.estado = '1');");
    }

    function EncuestaEnvia($_id, $_clientes)
    {
        $_clientes = str_replace(' ', '', $_clientes);
        $_clientes = substr($_clientes, 1);
        $_clientes = explode(';', $_clientes);
        for ($x = 0, $str = ''; $x < count($_clientes); $x++) {
            $str .= ",'{$_clientes[$x]}'";
            $this->_TRANS("INSERT INTO SCL007 VALUES({$_id}, '{$_clientes[$x]}');");
        }
        $str = substr($str, 1);
        //
        $_clientes = $this->_QUERY("SELECT email FROM MAN002 WHERE id IN({$str});");
        for ($x = 0, $str = ''; $x < count($_clientes); $x++) {
            $str .= ",{$_clientes[$x]['email']}";
        }
        $str = substr($str, 1);

        //PONGO LA ENCUESTA COMO ACTIVA
        $this->_TRANS("UPDATE SCL001 SET estado = '1' WHERE id = {$_id};");
        $this->Logger('0507');
        return $this->Email($str, 'Se le ha enviado una encuesta electr�nica por medio del sistema');
    }

    function EncuestaVacio()
    {
        return array(0 => array(
            'id' => '-1',
            'fecha' => '',
            'nombre' => '',
            'estado' => '')
        );
    }

    function ExportaPaso0($_encuesta)
    { // Se trae las preguntas tipo 0: SI/NO, 1: Respuesta Breve, 4:Desarrollo
        return $this->_QUERY("SELECT MAN002.nombre, SCL002.descri, SCL005.tipo AS respuesta
		FROM MAN002 INNER JOIN SCL004 ON MAN002.id = SCL004.cliente INNER JOIN SCL002 ON SCL004.pregunta = SCL002.id INNER JOIN SCLTMP ON SCL002.id = SCLTMP.pregunta INNER JOIN SCL005 ON SCL004.id = SCL005.respuesta
		WHERE (SCLTMP.encuesta = {$_encuesta}) AND (SCL002.tipo = '0')
		ORDER BY MAN002.nombre DESC;");
    }

    function ExportaPaso1($_encuesta, $_tipo)
    { // Se trae las preguntas tipo 0: SI/NO, 1: Respuesta Breve, 4:Desarrollo
        return $this->_QUERY("SELECT MAN002.nombre, SCL002.tipo, SCL002.descri, SCL006.desarrollo
		FROM MAN002 INNER JOIN SCL004 ON MAN002.id = SCL004.cliente INNER JOIN SCL006 ON SCL004.id = SCL006.respuesta INNER JOIN SCL002 ON SCL004.pregunta = SCL002.id INNER JOIN SCLTMP ON SCL002.id = SCLTMP.pregunta
		WHERE (SCLTMP.encuesta = {$_encuesta}) AND (SCL002.tipo = '{$_tipo}')
		ORDER BY SCL002.tipo, MAN002.nombre DESC;");
    }

    function ExportaPaso2($_encuesta)
    { // Se trae las preguntas tipo 2 y 3:
        return $this->_QUERY("SELECT TOP (100) PERCENT SCL002.descri, SCL003.opcion,
        	(SELECT COUNT(1) AS total
             FROM SCL004 INNER JOIN SCL005 ON SCL004.id = SCL005.respuesta
             WHERE (SCL004.pregunta = SCL003.pregunta) AND (SCL005.tipo = SCL003.linea)) AS total
		FROM SCLTMP INNER JOIN SCL002 ON SCLTMP.pregunta = SCL002.id INNER JOIN SCL003 ON SCL002.id = SCL003.pregunta
		WHERE (SCLTMP.encuesta = {$_encuesta}) AND (SCL002.tipo = '2' OR SCL002.tipo = '3')
		ORDER BY SCL002.tipo");
    }

    function GetEncuesta($_id)
    {
        return $this->_QUERY("SELECT nombre FROM SCL001 WHERE id='{$_id}';");
    }

    function GetOpciones($_id, $_preg)
    {
        return $this->_QUERY("SELECT SCL003.linea, SCL003.opcion  
		FROM SCL002 INNER JOIN SCLTMP ON SCL002.id = SCLTMP.pregunta INNER JOIN SCL001 ON SCLTMP.encuesta = SCL001.id INNER JOIN SCL003 ON SCL002.id = SCL003.pregunta 
		WHERE (SCL001.id = '{$_id}') AND (SCL002.id ='{$_preg}');");
    }

    function GetCheckbox($_id, $_preg, $_cliente)
    {
        return $this->_QUERY("SELECT SCL005.tipo AS respuesta
			FROM SCLTMP INNER JOIN
				SCL002 ON SCLTMP.pregunta = SCL002.id INNER JOIN
				SCL004 ON SCL002.id = SCL004.pregunta INNER JOIN
				SCL005 ON SCL004.id = SCL005.respuesta
			WHERE (SCLTMP.encuesta = '{$_id}') AND (SCL002.id = '{$_preg}') AND (SCL004.cliente = '{$_cliente}');");
    }

    function GetRespuestas($_id, $_cliente)
    {
        return $this->_QUERY("SELECT SCL002.id, SCL002.descri, SCL002.tipo, SCL006.desarrollo, -1 AS respuesta 
			FROM SCLTMP INNER JOIN
				SCL002 ON SCLTMP.pregunta = SCL002.id INNER JOIN
				SCL004 ON SCL002.id = SCL004.pregunta INNER JOIN
				SCL006 ON SCL004.id = SCL006.respuesta
			WHERE (SCLTMP.encuesta = {$_id}) AND (SCL004.cliente = '{$_cliente}')
			UNION
			SELECT SCL002.id, SCL002.descri, SCL002.tipo, '' AS desarrollo, SCL005.tipo AS respuesta
			FROM SCLTMP INNER JOIN
				SCL002 ON SCLTMP.pregunta = SCL002.id INNER JOIN
				SCL004 ON SCL002.id = SCL004.pregunta INNER JOIN
				SCL005 ON SCL004.id = SCL005.respuesta
			WHERE (SCLTMP.encuesta = {$_id}) AND (SCL004.cliente = '{$_cliente}') AND (SCL002.tipo <> 3)
			UNION
			SELECT DISTINCT SCL002.id, SCL002.descri, SCL002.tipo, '' AS desarrollo, '' AS respuesta
			FROM SCLTMP INNER JOIN
				SCL002 ON SCLTMP.pregunta = SCL002.id INNER JOIN
				SCL004 ON SCL002.id = SCL004.pregunta INNER JOIN
				SCL005 ON SCL004.id = SCL005.respuesta
			WHERE (SCLTMP.encuesta = {$_id}) AND (SCL004.cliente = '{$_cliente}') AND (SCL002.tipo = 3)
			ORDER BY SCL002.id;");
    }

    function OpcionesIME($_id, $_linea, $_descripcion, $_accion)
    {
        $_descripcion = $this->Free($_descripcion);
        if ($this->_TRANS("EXECUTE PA_MAG_018 '{$_id}', '{$_linea}', '{$_descripcion}', '{$_accion}';")) {
            return 1;
        } else
            return 0;
    }

    function OpcionesMuestra($_id)
    {
        return $this->_QUERY("SELECT pregunta, linea, opcion FROM SCL003 WHERE (pregunta = '{$_id}');");
    }

    function PreguntasIME($_enc, $_id, $_descripcion, $_tipo, $_accion)
    {
        $_descripcion = $this->Free($_descripcion);
        if ($_accion == 'I') {
            $_id = 99;
        }
        if ($this->_TRANS("EXECUTE PA_MAG_017 '{$_enc}','{$_id}', '{$_descripcion}', '{$_tipo}', '{$_accion}';")) {
            return 1;
        } else
            return 0;
    }

    function PreguntasMuestra($_id)
    {
        return $this->_QUERY("SELECT SCL002.id, SCL002.descri, SCL002.tipo 
		FROM SCL002 INNER JOIN SCLTMP ON SCL002.id = SCLTMP.pregunta
		WHERE (SCLTMP.encuesta = '{$_id}');");
    }

    function QuejasConsecutivo()
    {
        $ROW = $this->_QUERY("SELECT quejas FROM MAG000;");
        return $ROW[0]['quejas'];
    }

    function QuejasDetalleGet($_cs)
    {
        return $this->_QUERY("SELECT cs, codigo, CONVERT(VARCHAR, fec_pre, 105) AS fec_pre, CONVERT(VARCHAR, fec_lim, 105) AS fec_lim, CONVERT(VARCHAR, fec_rec, 105) AS fec_rec, instancia, CONVERT(VARCHAR, fec_res, 105) AS fec_res, acap, CONVERT(VARCHAR, fec_cal, 105) AS fec_cal, CONVERT(VARCHAR, fec_ser, 105) AS fec_ser, CONVERT(VARCHAR, fec_cli, 105) AS fec_cli, CONVERT(TEXT, obs) AS obs, estado
		FROM SCL008
		WHERE (cs = {$_cs});");
    }

    function QuejasDetalleVacio()
    {
        return array(0 => array(
            'cs' => '-1',
            'codigo' => '',
            'fec_pre' => '',
            'fec_lim' => '',
            'fec_rec' => '',
            'instancia' => '',
            'fec_res' => '',
            'acap' => '',
            'fec_cal' => '',
            'fec_ser' => '',
            'fec_cli' => '',
            'tmp' => '',
            'cliente' => '',
            'obs' => '',
            'estado' => '')
        );
    }

    function QuejasIME($_cs, $_codigo, $_fec_pre, $_fec_lim, $_fec_rec, $_instancia, $_usuarioA, $_fec_res, $_acap, $_usuarioB, $_fec_cal, $_fec_ser, $_fec_cli, $_obs, $_estado, $_accion)
    {
        $_codigo = str_replace(' ', '', $_codigo);
        $_acap = $this->FreePost($_acap);
        $_obs = $this->FreePost($_obs);
        //
        if ($_accion == 'I')
            $_log = '0508';
        elseif ($_accion == 'M')
            $_log = '0509';
        else {
            Bibliotecario::ZipEliminar("../../docs/sc/{$_cs}.zip");
            $_log = '0510';
        }
        $this->Logger($_log);
        if ($_accion == 'D') {
            return $this->_TRANS("UPDATE SCL008 SET estado='0' WHERE cs='{$_cs}'");
        }

        $_fec_pre = $this->_FECHA2($_fec_pre);
        $_fec_lim = $this->_FECHA2($_fec_lim);
        $_fec_rec = $this->_FECHA2($_fec_rec);
        $_fec_cal = $this->_FECHA2($_fec_cal);
        $_fec_ser = $this->_FECHA2($_fec_ser);
        $_fec_cli = $this->_FECHA2($_fec_cli);

        $_fec_res = $this->_FECHA2($_fec_res);
        //
        return $this->_TRANS("EXECUTE PA_MAG_036 '{$_cs}', '{$_codigo}', '{$_fec_pre}', '{$_fec_lim}', '{$_fec_rec}', '{$_instancia}', '{$_fec_res}', '{$_acap}', '{$_fec_cal}', '{$_fec_ser}', '{$_fec_cli}', '{$_obs}', '{$_usuarioA}', '{$_usuarioB}', '{$_estado}', '{$_accion}';");
    }

    function QuejasInstancia($_tipo)
    {
        if ($_tipo == '0')
            return 'No procede';
        elseif ($_tipo == '1')
            return 'LRE';
        elseif ($_tipo == '2')
            return 'LDP';
        elseif ($_tipo == '3')
            return 'LCC';
        elseif ($_tipo == '4')
            return 'Jefatura';
    }

    function QuejasResponsablesGet($_cs)
    {
        return $this->_QUERY("EXECUTE PA_MAG_035 '{$_cs}';");
    }

    function QuejasResponsablesVacio()
    {
        return array(0 => array(
            'tmpA' => '',
            'tmpB' => '',
            'usuarioA' => '',
            'usuarioB' => '',
        )
        );
    }

    function QuejasResumenGet($_admin, $_lab, $_desde, $_hasta, $_labs = '')
    {
        if ($_admin == '0') //SI ES ADMIN MUESTRA TODOS LOS TIPOS
            $extra = "1=1";
        else
            $extra = "instancia = '{$_lab}'";
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        $str = "SELECT cs, codigo, CONVERT(VARCHAR, fec_pre, 105) AS fecha, instancia, estado
		FROM SCL008
		WHERE ($extra) AND (fec_pre BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')";

        $str .= $_lab != '' ? " AND instancia='{$_lab}';" : ';';


        return $this->_QUERY($str);
    }

    function VistaPrevia($_id)
    {
        return $this->_QUERY("SELECT SCL002.id, SCL002.descri, SCL002.tipo 
		FROM SCL002 INNER JOIN SCLTMP ON SCL002.id = SCLTMP.pregunta INNER JOIN SCL001 ON SCLTMP.encuesta = SCL001.id 
		WHERE (SCL001.id = '{$_id}');");
    }

//
}

?>
