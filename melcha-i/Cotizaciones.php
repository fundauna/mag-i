<?php
/*******************************************************************
CLASE DE NIVEL 1 ENCARGADA DEL MODULO DE COTIZACIONES
(final SOLO PUEDE SER INSTANCIADA Y NO HEREDADA)
*******************************************************************/
final class Cotizaciones extends Database{
//
	function AperturaAnexa($_apertura, $_solicitud){
		$this->Logger('0100');
	
		return $this->_TRANS("INSERT INTO COT015 VALUES('{$_apertura}', '{$_solicitud}');");
	}
	
	function AperturaAnula($_cs, $_obs){
		$_obs = $this->Free($_obs);

		//ANULA SOLICITUD ASOCIADA
		$this->_TRANS("UPDATE COT000 SET fecha = GETDATE(), obs = '{$_obs}', estado = 'a' 
		WHERE (numero IN (SELECT solicitud FROM COT015 WHERE(apertura = '{$_cs}')) );");
		//
		$this->Logger('0101');
		//
		return $this->_TRANS("UPDATE COT014 SET fecha = GETDATE(), obs = '{$_obs}', estado = '5' WHERE (id = '{$_cs}');");
	}
	
	function AperturaAprueba($_cs){
		$this->Logger('0102');
	
		return $this->_TRANS("UPDATE COT014 SET fecha = GETDATE(), estado = '1' WHERE (id = '{$_cs}');");
	}
	
	function AperturaClienteResumenGet($_cliente, $_desde, $_hasta, $_estado){
		$_desde = $this->_FECHA($_desde);
		$_hasta = $this->_FECHA($_hasta);
		if($_estado == '') $op2 = '<>';
		else $op2 = '=';

		return $this->_QUERY("SELECT id, CONVERT(VARCHAR, fecha, 105) AS fecha1, cultivo, estado
		FROM COT014
		WHERE (cliente = '{$_cliente}') AND (fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00') AND (estado {$op2} '{$_estado}')
		ORDER BY fecha");
	}
	
	function AperturaEncabezadoSet($_cs, $_funcionario, $_cultivo, $_estacion, $_otro, $_obs, $_directa, $_lab){
		$_otro = $this->Free($_otro);
		//
		$this->Logger('0103');
		//
		return $this->_TRANS("EXECUTE PA_MAG_066 '{$_cs}', 
			'{$_funcionario}',
			'{$_cultivo}',
			'{$_estacion}',
			'{$_otro}',
			'{$_obs}',
			'{$_directa}',
			'{$_lab}';");
	}
	
	function AperturaEncabezadoImporta($_apertura){
		$ROW = $this->_QUERY("SELECT funcionario AS solicitante, cultivo, directa FROM COT014 WHERE(id = '{$_apertura}');");
		if($ROW[0]['directa'] == '0'){ //SI TIENE UNA SOLICITUD ASOCIADA, SE JALA EL CLIENTE
			$ROW2 = $this->_QUERY("SELECT MAN002.id AS cliente, MAN002.nombre AS tmp
			FROM COT015 INNER JOIN COT000 ON COT015.solicitud = COT000.numero INNER JOIN MAN002 ON COT000.cliente = MAN002.id
			WHERE (COT015.apertura = '{$_apertura}');");
			$ROW[0]['cliente'] = $ROW2[0]['cliente'];
			$ROW[0]['tmp'] = $ROW2[0]['tmp'];
			$ROW[0]['solicitante'] = '';
			$ROW[0]['cultivo'] = 'wasa';
		}else{
			$ROW[0]['cliente'] = '0              ';
			$ROW[0]['tmp'] = 'Cliente Interno';
		}
		
		$ROW[0]['numero'] = '';
		$ROW[0]['solicitud'] = '';
		$ROW[0]['apertura'] = $_apertura;
		$ROW[0]['fecha'] = '';
		$ROW[0]['estado'] = '0';
		$ROW[0]['moneda'] = '';
		$ROW[0]['monto'] = '0';
		
		return $ROW;
	}
	
	function AperturaDetalleImporta($_cs){
		return $this->_QUERY("SELECT COT016.cantidad, MAN000.id AS codigo, MAN000.nombre AS analisis, MAN000.tarifa, MAN000.costo AS monto, 0 AS total
		FROM COT016 INNER JOIN MAN000 ON COT016.analisis = MAN000.id
		WHERE (COT016.apertura = '{$_cs}');");
	}
	
	function AperturaDetalleGet($_cs){
		return $this->_QUERY("SELECT COT016.dpto, COT016.cantidad, MAN000.nombre, MAN000.tarifa
		FROM COT016 INNER JOIN MAN000 ON COT016.analisis = MAN000.id
		WHERE (COT016.apertura = '{$_cs}');");
	}
	
	function AperturaDetalleSet($cs, $_analisis, $_dpto, $_cant){
		$this->_TRANS("INSERT INTO COT016 VALUES('{$cs}', {$_analisis}, {$_cant}, '{$_dpto}');");
	}
	
	function AperturaEncabezadoGet($_id){
		return $this->_QUERY("SELECT CONVERT(VARCHAR, fecha, 105) AS fecha1, funcionario, cultivo, estacion, otro, obs, estado
		FROM COT014
		WHERE (id = '{$_id}');");
	}
	
	function AperturaExiste($cs, $_lab){
		$ROW = $this->_QUERY("SELECT estado FROM COT014 WHERE (id='{$cs}') AND (lid='{$_lab}');");
		if($ROW && $ROW[0]['estado'] == '1') return 1;
		else return 0;
	}
	
	function AperturaLabPendientes($_lab){
		return $this->_QUERY("SELECT id, CONVERT(VARCHAR, fecha, 105) AS fecha1, cultivo, estado
		FROM COT014
		WHERE (lid = '{$_lab}') AND (estado = '0' OR estado = '3')
		ORDER BY fecha");
	}
	
	function AperturaLabResumenGet($_lab, $_desde, $_hasta, $_estado){
		$_desde = $this->_FECHA($_desde);
		$_hasta = $this->_FECHA($_hasta);
		if($_estado == '') $op2 = '<>';
		else $op2 = '=';

		return $this->_QUERY("SELECT id, CONVERT(VARCHAR, fecha, 105) AS fecha1, cultivo, estado
		FROM COT014
		WHERE (lid = '{$_lab}') AND (fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00') AND (estado {$op2} '{$_estado}')
		ORDER BY fecha");
	}
	
	function AperturaNumeroGet(){
		$ROW = $this->_QUERY("SELECT apertura02_ AS cs FROM MAG000;");
		return "AP-{$ROW[0]['cs']}-".date('Y');
	}
	
	function AperturaNumeroSet(){
		$this->_TRANS("UPDATE MAG000 SET apertura02_ = apertura02_ + 1;");
	}
	
	function CertificacionAccion($consec, $accion){
		//
		$this->Logger('0104');
		//
		if($accion == '0'){
			if($this->_TRANS("UPDATE CER001 SET estado = '5' WHERE id = {$consec};")) return 1;			
			else return 0;
		}else{
			if($this->_TRANS("UPDATE CER001 SET estado = '2' WHERE id = {$consec};")) return 1;			
			else return 0;
		}
	}
	
	function CertificacionesAnula($consec){
		//
		$this->Logger('0105');
		//
		if($this->_TRANS("UPDATE CER001 SET estado = '5' WHERE id = {$consec};")) return 1;			
		else return 0;
	}
	
	function CertificacionesConsecutivoGet(){
		return $this->_QUERY("SELECT certificaciones FROM MAG000;");
	}
	
	function CertificacionesEstado($_var){
		if($_var == '0') return 'Recibida/En Proceso';
		elseif($_var == '2') return 'Finalizada';
		elseif($_var == '5') return 'Anulada';
	}
	
	function CertificacionesGet($_lid, $_desde, $_hasta, $_estado){
		$_desde = $this->_FECHA($_desde);
		$_hasta = $this->_FECHA($_hasta);
		if($_estado == '') $op2 = '<>';
		else $op2 = '=';
			
		return $this->_QUERY("SELECT CER001.id, CONVERT(VARCHAR,CER001.fecha, 105) AS fecha, CER001.cliente, YEAR(fecha) AS ano, CER001.estado, MAN002.nombre
		FROM CER001 INNER JOIN MAN002 ON CER001.cliente = MAN002.id
		WHERE (CER001.lid = '{$_lid}') AND (CER001.estado {$op2} '{$_estado}') AND (CER001.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')");
	}
	
	function CertificacionesIME($consec, $usuario){
		//
		//$this->Logger('0106'); no se graba por que la hace el cliente
		//
		if($this->_TRANS("INSERT INTO CER001 VALUES('{$consec}', GETDATE(), '{$usuario}', '0', '2');")){
			$this->NotificaJefes('2', '-7');
			$this->_TRANS("UPDATE MAG000 SET certificaciones = certificaciones + 1;");
			return 1;
		}else
			return 0;
	}
	
	function CertificacionesMuestra($_cliente, $_desde, $_hasta, $_estado){
		$_desde = $this->_FECHA($_desde);
		$_hasta = $this->_FECHA($_hasta);
		if($_estado == '') $op2 = '<>';
		else $op2 = '=';
			
		return $this->_QUERY("SELECT id, CONVERT(VARCHAR, fecha, 105) AS fecha, YEAR(fecha) AS ano, estado
		FROM CER001
		WHERE (cliente = '{$_cliente}') AND (estado {$op2} '{$_estado}') AND (fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')");
	}
	
	function CotizacionAccion($_var){
		if($_var=='0') return 'Generada';
		elseif($_var=='M') return 'Modificada';
		elseif($_var=='1') return 'Aprobada';
		elseif($_var=='2') return '<strong>Cancelada</strong>';
		elseif($_var=='3') return 'Procesada';
		elseif($_var=='5') return '<strong>Anulada</strong>';
		else return 'N/A';
	}
	
	function CotizacionAgregarEvaluacion($_cs, $_reactivos, $_material, $_personal, $_tiempo, $_equipos, $_viabilidad){
		$this->_TRANS("INSERT INTO COT009 VALUES('{$_cs}', '{$_reactivos}', '{$_material}', '{$_personal}', '{$_tiempo}', '{$_equipos}', '{$_viabilidad}', '');");
	}
	
	function CotizacionClienteResumenGet($_cliente, $_desde, $_hasta){
		$_desde = $this->_FECHA($_desde);
		$_hasta = $this->_FECHA($_hasta);
		return $this->_QUERY("SELECT numero, CONVERT(VARCHAR, fecha, 105) AS fecha1, tipo, estado
		FROM COT005
		WHERE (cliente = '{$_cliente}') AND (fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00') AND (estado IN('1', '2', '5') )
		ORDER BY fecha");
	}
	
	function CotizacionDetalleDel($_cs){
		$this->_TRANS("DELETE FROM COT008 WHERE (cotizacion = '{$_cs}');");
	}
	
	function CotizacionDetalleGet($_cs){
		return $this->_QUERY("SELECT COT008.cantidad, COT008.tarifa, COT008.monto, COT008.total, MAN000.id AS codigo, MAN000.nombre AS analisis, MAN000.descuento
		FROM COT008 INNER JOIN MAN000 ON COT008.analisis = MAN000.id
		WHERE (COT008.cotizacion = '{$_cs}');");
	}
	
	function CotizacionDetalleSet($cs, $_analisis, $_cant, $_tarifa, $_monto, $_total){
		$_monto = str_replace(',', '', $_monto);
		$_total = str_replace(',', '', $_total);
	
		$this->_TRANS("INSERT INTO COT008 VALUES('{$cs}', '{$_analisis}', '{$_cant}', '{$_tarifa}', '{$_monto}', '{$_total}');");
	}
	
	function CotizacionDetalleVacio(){
		return array(0=>array(
			'tarifa'=>'',
			'descuento'=>'0',
			'analisis'=>'',
			'codigo'=>'',
			'cantidad'=>'0',
			'monto'=>'0',
			'total'=>'0')
		);
	}
	
	function CotizacionEncabezadoGetLCC($_cs){
		$ROW = $this->_QUERY("SELECT COT005.numero, CONVERT(VARCHAR, COT005.fecha, 105) AS fecha, COT005.solicitante, COT005.moneda, COT005.monto, COT005.obs, COT005.tipo, COT005.importada, COT005.estado, COT007.tipo2, COT007.insumos, COT007.nombre, MAN002.id AS cliente, MAN002.nombre AS tmp, COT005.muestras 
		FROM COT005 INNER JOIN COT007 ON COT005.numero = COT007.cotizacion INNER JOIN MAN002 ON COT005.cliente = MAN002.id
		WHERE (COT005.numero = '{$_cs}');");
		
		if($ROW[0]['importada']=='1'){
			$ROW2 = $this->_QUERY("SELECT solicitud FROM COT006 WHERE(cotizacion = '{$_cs}');");
			$ROW[0]['solicitud'] = $ROW2[0]['solicitud'];
		}else $ROW[0]['solicitud']='';
		return $ROW;
	}
	
	function CotizacionEncabezadoGetLDP($_cs){
		$ROW = $this->_QUERY("SELECT COT005.numero, CONVERT(VARCHAR, COT005.fecha, 105) AS fecha, COT005.solicitante, COT005.moneda, COT005.monto, COT005.obs, COT005.tipo, COT005.importada, COT005.estado, COT017.descuento, MAN002.id AS cliente, MAN002.nombre AS tmp
		FROM COT005 INNER JOIN COT017 ON COT005.numero = COT017.cotizacion INNER JOIN MAN002 ON COT005.cliente = MAN002.id
		WHERE (COT005.numero = '{$_cs}');");
		
		if($ROW[0]['importada']=='1'){//SOLICITUD
			$ROW2 = $this->_QUERY("SELECT COT012.solicitud, COT012.cultivo
			FROM COT012 INNER JOIN COT000 ON COT012.solicitud = COT000.numero INNER JOIN COT006 ON COT000.numero = COT006.solicitud
			WHERE (COT006.cotizacion = '{$_cs}');");
			$ROW[0]['solicitud'] = $ROW2[0]['solicitud'];
			$ROW[0]['apertura']='';
			$ROW[0]['cultivo'] = $ROW2[0]['cultivo'];
		}elseif($ROW[0]['importada']=='2'){//APERTURA
			$ROW2 = $this->_QUERY("SELECT COT014.id, COT014.cultivo
			FROM COT014 INNER JOIN COT018 ON COT014.id = COT018.apertura
			WHERE(COT018.cotizacion = '{$_cs}');");
			$ROW[0]['solicitud'] = '';
			$ROW[0]['apertura'] = $ROW2[0]['id'];
			$ROW[0]['cultivo'] = $ROW2[0]['cultivo'];
		}else{
			$ROW[0]['solicitud']='';
			$ROW[0]['apertura']='';
		}
		
		return $ROW;
	}
	
	function CotizacionEncabezadoGetLRE($_cs){
		$ROW = $this->_QUERY("SELECT COT005.numero, CONVERT(VARCHAR, COT005.fecha, 105) AS fecha, COT005.solicitante, COT005.moneda, COT005.monto, COT005.obs, COT005.tipo, COT005.importada, COT005.estado, COT005.muestras, COT007.nombre AS comercial, MAN002.id AS cliente, MAN002.nombre AS tmp
		FROM COT005 INNER JOIN COT007 ON COT005.numero = COT007.cotizacion INNER JOIN MAN002 ON COT005.cliente = MAN002.id
		WHERE (COT005.numero = '{$_cs}');");
		
		if($ROW[0]['importada']=='1'){
			$ROW2 = $this->_QUERY("SELECT solicitud FROM COT006 WHERE(cotizacion = '{$_cs}');");
			$ROW[0]['solicitud'] = $ROW2[0]['solicitud'];
		}else $ROW[0]['solicitud']='';
		
		return $ROW;
	}
	
	function CotizacionEncabezadoSetLCC($_cs, $_accion, $_cliente, $_solicitante, $_moneda, $_monto, $_tipo, $_lab, $_solicitud, $_tipo2, $_insumos, $_nombre, $_muestras, $_UID){
		$_solicitante = $this->FreePost($_solicitante);
		$_nombre = $this->FreePost($_nombre);
		$_monto = str_replace(',', '', $_monto);
		$_solicitud = str_replace(' ', '', $_solicitud);
		if($_accion=='I') $acc = '0';
		else $acc = 'M';
		if($this->_TRANS("EXECUTE PA_MAG_063 '{$_cs}',  
			'{$_cliente}',
			'{$_solicitante}',
			'{$_moneda}',
			'{$_monto}',
			'{$_tipo}',
			'{$_lab}',
			'{$_solicitud}',
			'{$_tipo2}',
			'{$_insumos}',
			'{$_nombre}',
			'{$_muestras}',
			'{$_accion}';")){
			
			//INGRESA BITACORA
			$this->_TRANS("INSERT INTO COTBIT VALUES('{$_cs}', GETDATE(), {$_UID}, '{$acc}');");
			return 1;
		}else return 0;
	}
	
	function CotizacionEncabezadoSetLDP($_cs, $_accion, $_cliente, $_solicitante, $_moneda, $_monto, $_lab, $_solicitud, $_apertura, $_descuento, $_UID){
		$_solicitante = $this->FreePost(str_replace(' ', '', $_solicitante));
		$_apertura = $this->FreePost(str_replace(' ', '', $_apertura));
		$_monto = str_replace(',', '', $_monto);
		if($_accion=='I') $acc = '0';
		else $acc = 'M';
		
		if($this->_TRANS("EXECUTE PA_MAG_068 '{$_cs}',  
			'{$_cliente}',
			'{$_solicitante}',
			'{$_moneda}',
			'{$_monto}',
			'{$_lab}',
			'{$_solicitud}',
			'{$_apertura}',
			'{$_descuento}',
			'{$_accion}';")){
			
			//INGRESA BITACORA
			$this->_TRANS("INSERT INTO COTBIT VALUES('{$_cs}', GETDATE(), {$_UID}, '{$acc}');");
			return 1;
		}else return 0;
	}
	
	function CotizacionEncabezadoSetLRE($_cs, $_accion, $_cliente, $_moneda, $_monto, $_tipo, $_lab, $_solicitud, $_nombre, $_UID, $_total){
		$_nombre = $this->FreePost($_nombre);
		$_monto = str_replace(',', '', $_monto);
		$_solicitud = str_replace(' ', '', $_solicitud);
		
		if($_accion=='I') $acc = '0';
		else $acc = 'M';
		
		if($this->_TRANS("EXECUTE PA_MAG_065 '{$_cs}',  
			'{$_cliente}',
			'{$_moneda}',
			'{$_monto}',
			'{$_tipo}',
			'{$_lab}',
			'{$_solicitud}',
			'{$_nombre}',
			'{$_accion}',
			'{$_total}';")){
			
			//INGRESA BITACORA
			$this->_TRANS("INSERT INTO COTBIT VALUES('{$_cs}', GETDATE(), {$_UID}, '{$acc}');");
			return 1;
		}else return 0;
	}
	
	function CotizacionEncabezadoVacioLCC(){
		return array(0=>array(
			'numero'=>'',
			'solicitud'=>'',
			'fecha'=>'',
			'tipo'=>'1',
			'estado'=>'',
			'tmp'=>'',
			'cliente'=>'',
			'comercial'=>'',
			'solicitante'=>'',
			'nombre'=>'',
			'tipo2'=>'',
			'insumos'=>'',
			'muestras'=>'',
			'moneda'=>'',
			'monto'=>'0'
		));
	}
	
	function CotizacionEncabezadoVacioLDP(){
		return array(0=>array(
			'numero'=>'',
			'solicitud'=>'',
			'apertura'=>'',
			'cliente'=>'',
			'tmp'=>'',
			'solicitante'=>'',
			'cultivo'=>'',
			'descuento'=>'',
			'fecha'=>'',
			'estado'=>'0',
			'moneda'=>'',
			'monto'=>'0'
		));
	}
	
	function CotizacionEstado($_var){
		if($_var=='0') return 'Generada';
		elseif($_var=='1') return 'Aprobada';
		elseif($_var=='2') return 'Finalizada';
		elseif($_var=='3') return 'Pendiente';
		elseif($_var=='5') return 'Anulada';
		else return 'N/A';
	}
	
	function CotizacionHistorialGet($_cs){
		return $this->_QUERY("EXECUTE PA_MAG_064 '{$_cs}';");
	}
	
	function ModAprueba($_cs, $_persona){
		$_persona = $this->Free($_persona);	
		if($this->_TRANS("INSERT INTO COT019 VALUES('{$_cs}', GETDATE(), '{$_persona}');"))
			return 1;
		else return 0;
	}
	
	function ModApruebaMuestra($_cs){	
		return $this->_QUERY("SELECT aprobacion FROM COT019 WHERE numero = '{$_cs}' ORDER BY fecha DESC;");
	}
	
	function CotizacionModificarEstado($_cs, $_obs, $_estado, $_UID){
		$_obs = $this->Free($_obs);
	
		if($this->_TRANS("UPDATE COT005 SET fecha = GETDATE(), obs = '{$_obs}', estado = '{$_estado}' WHERE (numero = '{$_cs}');")){
			if($_estado=='1'){
			//ACTUALIZA ESTADO DE LA SOLICITUD DE ORIGEN
				$ROW = $this->_QUERY("SELECT solicitud FROM COT006 WHERE (cotizacion = '{$_cs}');");
				if($ROW) $this->_TRANS("UPDATE COT000 SET estado = 'f' WHERE (numero='{$ROW[0]['solicitud']}');");
			}
			//INGRESA BITACORA
			$this->_TRANS("INSERT INTO COTBIT VALUES('{$_cs}', GETDATE(), {$_UID}, '{$_estado}');");
			return 1;
		}else return 0;
	}
	
	function CotizacionNotificaCliente($_cs, $_msj){
		$ROW = $this->_QUERY("SELECT MAN002.email
		FROM COT005 INNER JOIN MAN002 ON COT005.cliente = MAN002.id
		WHERE (COT005.numero = '{$_cs}');");
		$Robot = new Seguridad();
		$Robot->Email($ROW[0]['email'], "Cotización: {$_cs} se cancela por motivo: {$_msj}");
	}
	
	function CotizacionNumeroGet($_lab, $_tipo){
		$ROW = $this->_QUERY("SELECT cotizacion{$_lab}{$_tipo}_ AS cs FROM MAG000;");
		return $this->Sigla($_tipo)."-{$ROW[0]['cs']}-".date('Y');
	}
	
	function CotizacionNumeroSet($_lab, $_tipo){
		//AUMENTA CONSECUTIVO
		$this->_QUERY("UPDATE MAG000 SET cotizacion{$_lab}{$_tipo}_ = cotizacion{$_lab}{$_tipo}_ + 1;");
	}
	
	function CotizacionPendientesLab($_lab){
		return $this->_QUERY("SELECT ROW_NUMBER() OVER (ORDER BY COT005.fecha DESC, COT005.numero) AS Row, COT005.numero, CONVERT(VARCHAR, COT005.fecha, 105) AS fecha1, COT005.moneda, COT005.monto, COT005.tipo, COT005.estado, MAN002.nombre, CONVERT(INT,SUBSTRING(COT005.numero, 6, CHARINDEX('-', SUBSTRING(COT005.numero, 6, 10))-1)) AS orden1, SUBSTRING(SUBSTRING(COT005.numero, 6, 10), CHARINDEX('-', SUBSTRING(COT005.numero, 6, 10))+1, 4) AS orden2
		FROM COT005 INNER JOIN MAN002 ON COT005.cliente = MAN002.id
		WHERE (COT005.lid = '{$_lab}') AND (COT005.estado = '0' OR COT005.estado = '1' OR COT005.estado = '3')
		ORDER BY orden2 ASC, orden1 ASC;");
	}
	
	function CotizacionResumenGet($_cotizacion, $_cliente, $_desde, $_hasta, $_tipo, $_estado, $_lab){
		$_desde = $this->_FECHA($_desde);
		$_hasta = $this->_FECHA($_hasta);
		if( str_replace(' ','',$_cotizacion) != ''){
			$extra = "(COT005.numero LIKE '%{$_cotizacion}%')";
		}else{
			if($_cliente == '')	$op1 = '<>';
			else $op1 = '=';
			if($_estado == '') $op2 = '<>';
			else $op2 = '=';
			if($_tipo == '') $op3 = '<>';
			else $op3 = '=';
			
			$extra = "(COT005.cliente {$op1} '{$_cliente}') AND (COT005.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00') AND (COT005.tipo {$op3} '{$_tipo}') AND (COT005.estado {$op2} '{$_estado}')";
		}

		return $this->_QUERY("SELECT ROW_NUMBER() OVER (ORDER BY COT005.fecha DESC, COT005.numero) AS Row, COT005.numero, CONVERT(VARCHAR, COT005.fecha, 105) AS fecha1, COT005.moneda, COT005.monto, COT005.tipo, COT005.estado, MAN002.nombre, CONVERT(INT,SUBSTRING(COT005.numero, 6, CHARINDEX('-', SUBSTRING(COT005.numero, 6, 10))-1)) AS orden1, SUBSTRING(SUBSTRING(COT005.numero, 6, 10), CHARINDEX('-', SUBSTRING(COT005.numero, 6, 10))+1, 4) AS orden2
		FROM COT005 INNER JOIN MAN002 ON COT005.cliente = MAN002.id
		WHERE (COT005.lid = '{$_lab}') AND {$extra}
		ORDER BY orden2 ASC, orden1 ASC;");
	}
	
	function MacroAnalisisMuestra($_lab, $_tipo){
		return $this->_QUERY("SELECT id, nombre FROM COT009 WHERE (tipo = '{$_tipo}') AND (lab='{$_lab}') ORDER BY nombre;");
	}
	
	function RegistroGet($_documento){
            $Q = $this->_QUERY("SELECT reactivos, material, personal, tiempo, equipos, viabilidad, CONVERT(TEXT, obs) AS obs FROM COT009 WHERE (id = '{$_documento}');");
            if($Q){
		return $Q;
            }else{
                return array(0=>array(
			'reactivos'=>'',
			'material'=>'',
			'personal'=>'',
			'tiempo'=>'',
			'equipos'=>'',
			'viabilidad'=>'',
			'obs'=>''
		));
            }
	}
	
	function RegistroSet($_documento, $_opciones, $_obs){
		$_obs = $this->Free($_obs);
		list($reactivos, $material, $personal, $tiempo, $equipos, $viabilidad) = explode(':', $_opciones);
		$this->_TRANS("EXECUTE PA_MAG_058 '{$_documento}', '{$reactivos}', '{$material}', '{$personal}', '{$tiempo}', '{$equipos}', '{$viabilidad}', '{$_obs}';");
	}
	
	function NotificaJefes($_lab, $_tipo, $_tipo2=''){
		$ROW = $this->_QUERY("SELECT SEG001.id, SEG001.email
		FROM SEG001 INNER JOIN SEG007 ON SEG001.id = SEG007.usuario INNER JOIN SEG002 ON SEG007.perfil = SEG002.id INNER JOIN SEG004 ON SEG002.id = SEG004.perfil
		WHERE (SEG002.lab = '{$_lab}') AND (SEG004.permiso = '{$_tipo}');");

		for($x=0,$destinos='';$x<count($ROW);$x++){
			$destinos .= ','.$ROW[$x]['email'];
			$this->TablonSet($_tipo, $ROW[$x]['id'], $_tipo, false);
		}
		/*SE USA TIPO 2 CUANDO SE OCUPA MANDAR UNA SOLICITUD + UNA APERTURA*/
		if($_tipo2 != ''){
			$ROW = $this->_QUERY("SELECT SEG001.id, SEG001.email
			FROM SEG001 INNER JOIN SEG007 ON SEG001.id = SEG007.usuario INNER JOIN SEG002 ON SEG007.perfil = SEG002.id INNER JOIN SEG004 ON SEG002.id = SEG004.perfil
			WHERE (SEG002.lab = '{$_lab}') AND (SEG004.permiso = '{$_tipo2}');");
			
			for($x=0,$destinos='';$x<count($ROW);$x++){
				$destinos .= ','.$ROW[$x]['email'];
				$this->TablonSet($_tipo2, $ROW[$x]['id'], $_tipo2, false);
			}
		}
		
		$destinos = substr($destinos, 1);
		//ENVIA EMAIL
		$Robot = new Seguridad();
		$Robot->Email($destinos, 'Tiene documentos por aprobar en el sistema');
	}
	
	function NotificaNuevaCotizacion($_cs){
		$ROW = $this->_QUERY("SELECT id, email FROM SEG001 WHERE (id = '14') OR (id = '16') OR (id = '24');");

		for($x=0,$destinos='';$x<count($ROW);$x++){
			$destinos .= ','.$ROW[$x]['email'];
			$this->TablonSet('-1', $ROW[$x]['id'], '-1', false);
		}
		
		//ENVIA EMAIL
		$Robot = new Seguridad();
		$Robot->Email($destinos, 'Se ha creado la solicitud de cotizacion numero: '.$_cs);
	}
	
	private function Sigla($_tipo){
		if($_tipo==0) return 'LRE';
		elseif($_tipo==3) return 'LDP';
		elseif($_tipo==1 or $_tipo==2) return 'LCC'.$_tipo;
	}
	
	function SolAperturaGet($_cs){
		return $this->_QUERY("SELECT id, CONVERT(VARCHAR, COT014.fecha, 105) AS fecha, COT014.funcionario, COT014.estacion, COT014.otro, COT014.estado
		FROM COT014 INNER JOIN COT015 ON COT014.id = COT015.apertura
		WHERE (COT015.solicitud = '{$_cs}');");
	}
	
	function SolFertilizantesDetalleGet($_cs, $_tipo){
		return $this->_QUERY("SELECT COT002.rango, COT002.quela, COT002.tipo, COT002.fuente, VAR005.nombre
		FROM COT002 INNER JOIN VAR005 ON COT002.analisis = VAR005.id
		WHERE (COT002.solicitud = '{$_cs}') AND (COT002.es_analisis = $_tipo)
		ORDER BY VAR005.nombre;");
	}
	
	function SolFertilizantesDetalleSet($cs, $_analisis, $_rango, $_tipo, $_unidad, $_fuente, $_es_analisis){
		$_rango = $this->FreePost($_rango);
		$_fuente = $this->FreePost($_fuente);
	
		$this->_TRANS("EXECUTE PA_MAG_024 '{$cs}', '{$_analisis}', '{$_rango}', '{$_tipo}', '{$_unidad}', '{$_fuente}', '{$_es_analisis}';");
	}
	
	function SolFertilizantesEncabezadoGet($_cs){
		return $this->_QUERY("SELECT TOP(1) CONVERT(VARCHAR, COT000.fecha, 105) AS fecha, COT000.obs, COT000.nombre_sol, COT000.encargado, COT000.estado, COT001.formula, COT001.comercial, COT001.tipo, COT001.metodo, COT001.mezcla, COT001.aporta, COT001.tipo_impu, COT001.suministro, COT001.discre, COT001.num_sol, COT001.num_mue, COT001.muestras, MAN002.nombre AS nom_cliente, MAN002.representante, MAN002.fax, MAN002.tel
		FROM COT000 INNER JOIN COT001 ON COT000.numero = COT001.solicitud INNER JOIN MAN002 ON COT000.cliente = MAN002.id
		WHERE (COT000.numero = '{$_cs}');");
	}
	
	function SolFertilizantesEncabezadoSet($_cs, $_formula, $_comercial, $_tipo_form, $_metodo, $_mezcla, $_aporta, $_tipo2, $_suministro, $_discre, $_num_sol, $_num_mue, $_total){
		$_formula = $this->FreePost($_formula);
		$_comercial = $this->FreePost($_comercial);
		$_num_sol = $this->FreePost($_num_sol);
		$_num_mue = $this->FreePost($_num_mue);
		
		if($this->_TRANS("INSERT INTO COT001 VALUES('{$_cs}', '{$_formula}', '{$_comercial}', {$_tipo_form}, '{$_metodo}', '{$_mezcla}', '{$_aporta}', '{$_tipo2}', '{$_suministro}', '{$_discre}', '{$_num_sol}', '{$_num_mue}', '{$_total}');")){
			//
			//$this->Logger('0107'); ingresado por clientes
			//
			return 1;
		}else return 0;
	}
	
	function SolicitudAnula($_cs, $_admin, $_obs){
		$_obs = $this->Free($_obs);
		
		if($this->_TRANS("UPDATE COT000 SET fecha = GETDATE(), obs = '{$_obs}', estado = 'a' WHERE (numero = '{$_cs}');")){
			//ANULA APERTURA ASOCIADA
			$this->_TRANS("UPDATE COT014 SET fecha = GETDATE(), obs = '{$_obs}', estado = '5' 
			WHERE (id IN (SELECT apertura FROM COT015 WHERE(solicitud = '{$_cs}')) );");
		
			if($_admin=='1'){
				//NOTIFICA CLIENTE
				$ROW = $this->_QUERY("SELECT MAN002.email
				FROM COT000 INNER JOIN MAN002 ON COT000.cliente = MAN002.id
				WHERE (COT000.numero = '{$_cs}');");
				$Robot = new Seguridad();
				$Robot->Email($ROW[0]['email'], "Solicitud: {$_cs} anulada por: {$_obs}");
				//
				$Securitor = new Seguridad();
				if(!$Securitor->SesionAuth()) return false;
				$ROW = $Securitor->SesionGet();
				$this->_TRANS("INSERT INTO COTBI2 VALUES('{$_cs}', GETDATE(), {$ROW['UID']}, 'a');");
			}
			return 1;
		}else return 0;
	}
	
	function SolicitudAprueba($_cs){
		$Securitor = new Seguridad();
		if(!$Securitor->SesionAuth()) return false;
		$ROW = $Securitor->SesionGet();
			
		$this->_TRANS("UPDATE COT000 SET fecha = GETDATE(), estado = '2' WHERE (numero = '{$_cs}');");
		$this->_TRANS("INSERT INTO COTBI2 VALUES('{$_cs}', GETDATE(), {$ROW['UID']}, '2');");
		return true;
	}
	
	function SolicitudClienteResumenGet($_cliente, $_desde, $_hasta, $_estado){
		$_desde = $this->_FECHA($_desde);
		$_hasta = $this->_FECHA($_hasta);
		if($_estado == '') $op2 = '<>';
		else $op2 = '=';
		
		return $this->_QUERY("SELECT numero, CONVERT(VARCHAR, fecha, 105) AS fecha1, tipo, estado
		FROM COT000
		WHERE (cliente = '{$_cliente}') AND (estado {$op2} '{$_estado}') AND (fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')
		ORDER BY fecha");
	}
	
	function SolicitudEncabezadoSet($_cs, $_cliente, $_tipo, $_obs, $_nombre_sol, $_encargado, $_lab){
		$_obs = $this->FreePost($_obs);
		$_nombre_sol = $this->FreePost($_nombre_sol);
		$_encargado = $this->FreePost($_encargado);
		//
		//$this->Logger('0108'); la hace el cliente
		 
		return $this->_TRANS("INSERT INTO COT000 VALUES('{$_cs}', GETDATE(), '{$_cliente}', '{$_tipo}', '{$_obs}', '{$_nombre_sol}', '{$_encargado}', 'r', '{$_lab}');");
	}
	
	function SolicitudEstado($_var){
		if($_var=='r') return 'Generada';
		elseif($_var=='2') return 'Aprobada';
		elseif($_var=='a') return 'Anulada';
		elseif($_var=='f') return 'Finalizada';
		else return 'N/A';
	}
	
	function SolicitudExiste($cs, $_lab){
		$ROW = $this->_QUERY("SELECT estado FROM COT000 WHERE (numero='{$cs}') AND (lid='{$_lab}');");
		if($ROW && $ROW[0]['estado'] == '2' || $ROW && $ROW[0]['estado'] == 'f') return 1;
		else return 0;
	}
	
	function SolicitudHistorialGet($_cs){
		$ROW = $this->_QUERY("SELECT SEG001.nombre, SEG001.ap1, SEG001.ap2, COTBI2.accion
		FROM COTBI2 INNER JOIN SEG001 ON COTBI2.usuario = SEG001.id
		WHERE(COTBI2.solicitud = '{$_cs}');");
		if($ROW){
			if($ROW[0]['accion']=='a')
				return "<strong>Anulada por: {$ROW[0]['nombre']} {$ROW[0]['ap1']} {$ROW[0]['ap2']}</strong><br />";
			else
				return "<strong>Aprobada por: {$ROW[0]['nombre']} {$ROW[0]['ap1']} {$ROW[0]['ap2']}</strong><br />";
		}else return '';
	}
	
	function SolicitudImportaDetalle($_cs){
		return $this->_QUERY("SELECT COT010.cantidad, MAN000.id AS codigo, MAN000.nombre AS analisis, MAN000.tarifa, MAN000.costo AS monto, MAN000.descuento, 0 AS total
		FROM COT010 INNER JOIN MAN000 ON COT010.analisis = MAN000.id
		WHERE (COT010.solicitud = '{$_cs}');");
	}
	
	function SolicitudImportaDetalleLDP($_cs){
		return $this->_QUERY("SELECT COT011.cantidad, MAN000.id AS codigo, MAN000.nombre AS analisis, MAN000.tarifa, MAN000.costo AS monto, MAN000.descuento, 0 AS total
		FROM COT011 INNER JOIN MAN000 ON COT011.analisis = MAN000.id
		WHERE (COT011.solicitud = '{$_cs}');");
	}
	
	function SolicitudImportaEncabezado($_cs){
		$ROW = $this->_QUERY("SELECT '' AS numero, '' AS fecha, '0' AS estado, '{$_cs}' AS solicitud, '' AS solicitante, COT000.tipo, COT000.nombre_sol AS comercial, MAN002.id AS cliente, MAN002.nombre AS tmp, '' AS tipo2, 0 AS insumos, '' AS moneda, 0 AS monto, '' AS muestras 
		FROM COT000 INNER JOIN MAN002 ON COT000.cliente = MAN002.id
		WHERE (COT000.numero = '{$_cs}');");
                
                if($ROW[0]['tipo']=='1'){//FERTILIZANTES
                    $ROW2 = $this->_QUERY("SELECT formula, muestras, comercial AS nombre FROM COT001 WHERE (solicitud = '{$_cs}');");
                    $ROW[0]['formula'] = $ROW2[0]['formula'];
                    $ROW[0]['nombre'] = $ROW2[0]['nombre'];
                    $ROW[0]['muestras'] = $ROW2[0]['muestras'];
                }elseif($ROW[0]['tipo']=='2'){//PLAGUICIDAS
                    //$ROW2 = $this->_QUERY("SELECT CONVERT(TEXT, concentracion) AS nombre, muestras FROM COT003 WHERE (solicitud = '{$_cs}');");
                    $ROW2 = $this->_QUERY("SELECT muestras FROM COT003 WHERE (solicitud = '{$_cs}');");
                    $ROW[0]['formula'] = '';
                    $ROW[0]['nombre'] = '';//$ROW2[0]['nombre'];
                    $ROW[0]['muestras'] = $ROW2[0]['muestras'];
                }elseif($ROW[0]['tipo']=='3'){//LDP
			$ROW2 = $this->_QUERY("SELECT cultivo FROM COT012 WHERE (solicitud = '{$_cs}');");
			$ROW[0]['cultivo'] = $ROW2[0]['cultivo'];
			$ROW[0]['descuento'] = $ROW[0]['apertura'] = '';
                        $ROW[0]['muestras'] = '';
		}
                
		return $ROW;
	}
	
	function SolicitudLabResumenGet($_solicitud, $_cliente, $_desde, $_hasta, $_tipo, $_estado, $_lab){
		$_desde = $this->_FECHA($_desde);
		$_hasta = $this->_FECHA($_hasta);
		if( str_replace(' ','',$_solicitud) != ''){
			$extra = "(COT000.numero LIKE '%{$_solicitud}%')";
		}else{
			if($_cliente == '')	$op1 = '<>';
			else $op1 = '=';
			if($_estado == '') $op2 = '<>';
			else $op2 = '=';
			if($_tipo == '') $op3 = '<>';
			else $op3 = '=';
			
			$extra = "(COT000.cliente {$op1} '{$_cliente}') AND (COT000.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00') AND (COT000.tipo {$op3} '{$_tipo}') AND (COT000.estado {$op2} '{$_estado}')";
		}

		return $this->_QUERY("SELECT COT000.numero, CONVERT(VARCHAR, COT000.fecha, 105) AS fecha1, COT000.tipo, COT000.estado, MAN002.nombre AS cliente
		FROM COT000 INNER JOIN MAN002 ON COT000.cliente = MAN002.id
		WHERE (COT000.lid = '{$_lab}') AND {$extra}
		ORDER BY COT000.fecha;");
	}
	
	function SolicitudNumeroGet($_lab, $_tipo){
		$ROW = $this->_QUERY("SELECT solicitud{$_lab}{$_tipo}_ AS cs FROM MAG000;");
		return $this->Sigla($_tipo)."-{$ROW[0]['cs']}-".date('Y');
	}
	
	function SolicitudNumeroSet($_lab, $_tipo){
		//AUMENTA CONSECUTIVO
		$this->_QUERY("UPDATE MAG000 SET solicitud{$_lab}{$_tipo}_ = solicitud{$_lab}{$_tipo}_ + 1;");
	}
	
	function SolicitudPendientesLab($_lab){
		return $this->_QUERY("SELECT COT000.numero, CONVERT(VARCHAR, COT000.fecha, 105) AS fecha1, COT000.tipo, COT000.estado, MAN002.nombre AS cliente
		FROM COT000 INNER JOIN MAN002 ON COT000.cliente = MAN002.id
		WHERE (COT000.lid = '{$_lab}') AND (COT000.estado = 'r')
		ORDER BY COT000.fecha;");
	}
	
	function SolicitudTipo($_var){
		if($_var=='1') return 'Fertilizantes';
		elseif($_var=='2') return 'Plaguicidas';
		elseif($_var=='0' or $_var=='3') return 'Análisis';
		else return 'N/A';
	}
	
	function SolLDPAnalisisGet($_cs){
		return $this->_QUERY("SELECT COT011.dpto, COT011.cantidad, MAN000.nombre, MAN000.tarifa
		FROM COT011 INNER JOIN MAN000 ON COT011.analisis = MAN000.id
		WHERE (COT011.solicitud = '{$_cs}');");
	}
	
	function SolLDPDetalleAnalisisSet($cs, $_analisis, $_dpto, $_cant){
		$this->_TRANS("INSERT INTO COT011 VALUES('{$cs}', {$_analisis}, '{$_dpto}', {$_cant});");
	}
	
	function SolLDPDetallePlaguiSet($cs, $_i, $_plag, $_tip, $_frecuencia, $_dosis, $_fecha){
		$_fecha = $this->_FECHA($_fecha);
		$this->_TRANS("INSERT INTO COT013 VALUES('{$cs}', {$_i}, '{$_plag}', '{$_tip}', '{$_frecuencia}', '{$_dosis}', '{$_fecha}');");
	}
	
	function SolLDPEncabezadoGet($_solicitud){
		return $this->_QUERY("SELECT TOP(1) MAN002.nombre AS nom_cliente, MAN002.representante, MAN002.fax, CONVERT(VARCHAR, COT000.fecha, 105) AS fecha, COT000.obs, COT000.estado, COT012.cultivo, 
		COT012.cientifico, COT012.sintomas, COT012.plaga, COT012.tipo, COT012.afectada, COT012.otro, COT012.provincia, 
		COT012.canton, COT012.direccion, COT012.gravedad, COT012.etapa, COT012.area_cultivo, COT012.area_afectada, COT012.aplicado, 
		COT012.riego, COT012.drenaje, COT012.suelo, COT012.programa, COT012.densidad, MAN013.telefono
		FROM COT000 INNER JOIN COT012 ON COT000.numero = COT012.solicitud INNER JOIN MAN002 ON COT000.cliente = MAN002.id INNER JOIN MAN013 ON MAN002.id = MAN013.cliente
		WHERE (COT000.numero = '{$_solicitud}');");
	}
	
	function SolLDPEncabezadoSet($cs, $_cultivo, $_cientifico, $_sintomas, $_plaga, $_tipo, $_afectada, $_otro, $_provincia, $_canton, $_direccion, $_gravedad, $_etapa, $_area_cultivo, $_area_afectada, $_aplicado, $_riego, $_drenaje, $_suelo, $_programa, $_densidad){
		//
		//$this->Logger('0109');
		//
		return $this->_TRANS("EXECUTE PA_MAG_067 '{$cs}', '{$_cultivo}', '{$_cientifico}', '{$_sintomas}', '{$_plaga}', '{$_tipo}', '{$_afectada}', '{$_otro}', '{$_provincia}', '{$_canton}', '{$_direccion}', '{$_gravedad}', '{$_etapa}', '{$_area_cultivo}', '{$_area_afectada}', '{$_aplicado}', '{$_riego}', '{$_drenaje}', '{$_suelo}', '{$_programa}', '{$_densidad}';");
	}
	
	function SolLDPPlaguicidasGet($_cs){
		return $this->_QUERY("SELECT plaguicida, tipo, frecuencia, dosis, CONVERT(VARCHAR, fecha, 105) AS fecha
		FROM COT013
		WHERE (solicitud = '{$_cs}')
		ORDER BY linea");
	}
	
	function SolLFEDetalledoGet($_cs){
		return $this->_QUERY("SELECT COT010.cantidad, MAN000.nombre
		FROM COT010 INNER JOIN MAN000 ON COT010.analisis = MAN000.id
		WHERE (COT010.solicitud = '{$_cs}');");
	}
	
	function SolLFEEncabezadoGet($_cs){
		return $this->_QUERY("SELECT TOP(1) CONVERT(VARCHAR, COT000.fecha, 105) AS fecha, COT000.obs, COT000.nombre_sol AS producto, COT000.estado, MAN002.nombre AS nom_cliente, MAN002.representante, MAN002.email, MAN002.fax, MAN002.tipo, MAN013.telefono
		FROM COT000 INNER JOIN MAN002 ON COT000.cliente = MAN002.id INNER JOIN MAN013 ON MAN002.id = MAN013.cliente
		WHERE (COT000.numero = '{$_cs}');");
	}
	
	function SolLREDetalleSet($cs, $_analisis, $_cant){
		$this->_TRANS("INSERT INTO COT010 VALUES('{$cs}', {$_analisis}, {$_cant});");
	}
	
	function SolPlaguicidasDetalleGet($_cs, $_tipo){
		return $this->_QUERY("SELECT COT004.conc AS rango, COT004.unidad AS tipo, COT004.fuente, VAR005.nombre
		FROM COT004 INNER JOIN VAR005 ON COT004.analisis = VAR005.id
		WHERE (COT004.solicitud = '{$_cs}') AND (COT004.tipo = '{$_tipo}')
		ORDER BY VAR005.nombre;");
	}
	
	function SolPlaguicidasDetalleSet($cs, $_analisis, $_conce, $_unidad, $_fuente, $_tipo){
		$_conce = $this->FreePost($_conce);
		$_fuente = $this->FreePost($_fuente);
		$this->_TRANS("INSERT INTO COT004 VALUES('{$cs}', {$_analisis}, '{$_conce}', '{$_unidad}', '{$_fuente}', '{$_tipo}');");
	}
	
	function SolPlaguicidasEncabezadoGet($_cs){
		return $this->_QUERY("SELECT TOP(1) CONVERT(VARCHAR, COT000.fecha, 105) AS fecha, COT000.obs, COT000.nombre_sol, COT000.encargado, COT000.estado, COT003.contiene, COT003.tipo, COT003.metodo, COT003.aporta, COT003.suministro, COT003.discre, COT003.num_sol, COT003.num_mue, COT003.muestras, COT003.densidad, MAN002.nombre AS nom_cliente, MAN002.representante, MAN002.fax, MAN002.tel FROM COT000 INNER JOIN COT003 ON COT000.numero = COT003.solicitud INNER JOIN MAN002 ON COT000.cliente = MAN002.id WHERE (COT000.numero = '{$_cs}');");
	}
	
	function SolPlaguidicasEncabezadoSet($_cs, $_contiene, $_tipo_form, $_metodo, $_aporta, $_suministro, $_discre, $_num_sol, $_num_mue, $_total, $_densidad){
		$_num_sol = $this->FreePost($_num_sol);
		$_num_mue = $this->FreePost($_num_mue);
		//
		//$this->Logger('0110'); la hace el cliente
		//
		return $this->_TRANS("INSERT INTO COT003 VALUES('{$_cs}', 
			'{$_contiene}',
			'{$_tipo_form}',
			'{$_metodo}', 
			'{$_aporta}',
			'{$_suministro}',
			'{$_discre}',
			'{$_num_sol}',
			'{$_num_mue}',
			'{$_total}',
			'{$_densidad}');");
	}
//
}
?>