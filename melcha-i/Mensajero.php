<?php
/*******************************************************************
CLASE DE NIVEL 2 UTILIZADA PARA INVOCAR RECURSOS 
(abstract SOLO PUEDE SER HEREDADA Y NO INSTANCIADA)
*******************************************************************/
abstract class Mensajero{
	/*CONSTANTES*/
	const __DOCS__ = '../../caspha-i/docs/';
	const __ESTILO__ = '../../caspha-i/css/';
	const __FECHA__ = '2015';
	const __GRAFICOS__ = '../../caspha-i/libchart/temp/';
	const __IMAGENES__ = '../../caspha-i/imagenes/';
	const __JS__ = '../../caspha-i/js/';
	const __NAME__ = 'SFE';
	const __ROOT__ = 'MAG-i/baltha-i/seguridad/';
	const __SHELL__ = '../../caspha-i/shell/'; 
	const __VERSION__ = '1.1';
	/*ELEMENTOS*/
	private $_IP = '';		//IP DE SERVIDOR
	private $_LID = '';		//ID DE CADA LAB
	private $_LNAME = array('Error','Laboratorio de An&aacute;lisis de Residuos de Agroqu&iacute;micos (LRE)', 
								'Laboratorio Central de Diagn&oacute;stico de Plagas (LDP)',
								'Laboratorio de Control de Calidad de Agroqu&iacute;micos (LCC)');
	private $_PRIV = 'E'; 	//PRIVILEGIO DE CADA PAGINA
	
	/*METODOS*/
	final public function Err(){
		include('IP.inc');
		$ruta = 'http://'.$this->_IP.'/'.self::__ROOT__;
		header("location: {$ruta}err.html");
		exit;
	}
	
	function Footer($tipo=''){
		echo "<table id='footer{$tipo}' width='100%'><tr><td width='100%' align='center'><font size='-2'>Documento Normativo Propiedad del SFE. Cualquier versi&oacute;n impresa es una COPIA NO CONTROLADA. Es responsabilidad del personal que utiliza el documento verificar su vigencia. Gesti&oacute;n de Calidad no se hace responsable por el uso de documentos obsoletos</font></td></tr></table>";
	}
	
	final public function Incluir($_nombre, $_tipo, $_version=0){
		$_nombre = str_replace(' ','', strtolower($_nombre));
		$_tipo = str_replace(' ','', strtolower($_tipo));
		
		if($_tipo == 'gif' or $_tipo == 'png' or $_tipo == 'jpg'){
			$ruta = self::__IMAGENES__.$_nombre.'.'.$_tipo;
			file_exists($ruta) or die("Error archivo '{$ruta}' no encontrado");
			echo "<img src='{$ruta}' border='0' />";
		}elseif($_tipo == 'hr'){
			$sigla = strtoupper(substr($_nombre, 0, 1));
			$numero = substr($_nombre, 1);
			$numero = str_pad($numero, 4, '0', STR_PAD_LEFT);
			$_nombre = $sigla.$numero;
			$ruta = self::__IMAGENES__.'dot.png';
			file_exists($ruta) or die("Error archivo '{$ruta}' no encontrado");
			echo "<table id='descbarhr' width='100%' cellpadding='0' cellspacing='0'><tr><td><img src='{$ruta}' border='0' width='16' height='16' /></td><td width='100%' style='font-size:12px;'>&nbsp;{$_version}</td><td align='right'><font size='-2' title='N�mero de pagina: {$_nombre}' style='cursor:help'>[{$_nombre}]</font></td></tr><tr><td colspan='3'><hr /><br /></td></tr></table>";
		}elseif($_tipo == 'fav'){
			$ruta = self::__IMAGENES__.'dot.png';
			file_exists($ruta) or die("Error archivo '{$ruta}' no encontrado");
			echo "<link rel='shortcut icon' type='image/png' href='{$ruta}'/>";
		}elseif($_tipo == 'ico'){
			$ruta = self::__IMAGENES__.'/menu/'.$_nombre.'.png';
			file_exists($ruta) or die("Error archivo '{$ruta}' no encontrado");
			echo "<img src='{$ruta}' border='0' class='icon'/>&nbsp;";
		}elseif($_tipo == 'bkg'){
			/********** IMAGEN PARA BACKGROUND **********/
			$ruta = self::__IMAGENES__.$_nombre.'.png';
			file_exists($ruta) or die("Error archivo '{$ruta}' no encontrado");
			echo $ruta;
		}elseif($_tipo == 'doc'){			
			$ruta = self::__DOCS__.$_version.'/'.$_nombre.'.zip';
			if(file_exists($ruta)) return $ruta;
			else return false;
		}elseif($_tipo == 'swf'){
			$ruta = self::__IMAGENES__.$_nombre.'.swf';
			file_exists($ruta) or die("Error archivo '{$ruta}' no encontrado");
			echo $ruta;
		}elseif($_tipo == 'css'){
			$ruta = self::__ESTILO__.$_nombre.'.css';
			file_exists($ruta) or die("Error archivo '{$ruta}' no encontrado");
			echo "<link rel='stylesheet' type='text/css' href='{$ruta}?version={$_version}'>";
			if($this->_PRIV  != 'E') $this->Oculta();
		}elseif($_tipo == 'ajax'){
			/********** JQUERY **********/
			$ruta = self::__JS__.'jquery-1.9.1.min.js';
			file_exists($ruta) or die("Error archivo 'jquery-1.9.1.min.js' no encontrado");
			echo "<script type='text/javascript' language='javascript' src='{$ruta}?version={$_version}'></script>";
			/********** PAGINA SHELL **********/
			$ruta = self::__SHELL__.$_nombre.'/_'.$_version;
			file_exists($ruta) or die("Error archivo '{$ruta}' no encontrado");
			echo "<script type='text/javascript' language='javascript'>var __SHELL__ = '{$ruta}'</script>";
			/********** JS DE PAGINA **********/
			$ruta = self::__JS__.$_nombre.'/'.$_version.'.js';
			file_exists($ruta) or die("Error archivo '{$ruta}' no encontrado");
			$token = date('dmYh:i:s');
			echo "<script type='text/javascript' language='javascript' src='{$ruta}?tk={$token}&version={$_version}'></script>";
		}elseif($_tipo == 'gfx'){
			$ruta = self::__JS__.'graficador/highcharts.js';
			file_exists($ruta) or die("Error archivo 'graficador/highcharts.js' no encontrado");
			echo "<script type='text/javascript' language='javascript' src='{$ruta}?version={$_version}'></script>";
			//
			$ruta = self::__JS__.'graficador/modules/data.js';
			file_exists($ruta) or die("Error archivo 'graficador/modules/data.js' no encontrado");
			echo "<script type='text/javascript' language='javascript' src='{$ruta}?version={$_version}'></script>";
			//
			$ruta = self::__JS__.'graficador/modules/exporting.js';
			file_exists($ruta) or die("Error archivo 'graficador/modules/exporting.js' no encontrado");
			echo "<script type='text/javascript' language='javascript' src='{$ruta}?version={$_version}'></script>";
		}elseif($_tipo == 'js'){
			$ruta = self::__JS__.$_nombre.'.js';
			file_exists($ruta) or die("Error archivo '{$ruta}' no encontrado");
			if($_nombre == 'window'){
				/********** SI LLAMAMOS AL window.js LLAMAMOS DE UNA VEZ A window.css **********/
				$ruta2 = self::__ESTILO__.'window.css';
				file_exists($ruta2) or die("Error archivo '{$ruta2}' no encontrado");
				echo "<link rel='stylesheet' type='text/css' href='{$ruta2}?version={$_version}'>
				<script type='text/javascript' language='javascript' src='{$ruta}?version={$_version}'></script>";
			}elseif($_nombre == 'calendario'){
				/********** SI LLAMAMOS AL calendario.js LLAMAMOS DE UNA VEZ A LAS IMAGENES DE CALENDARIO **********/
				$ruta2 = self::__IMAGENES__.'/calendario/';
				echo "<script type='text/javascript' language='javascript'>var ruta = '{$ruta2}'</script>
				<script type='text/javascript' language='javascript' src='{$ruta}?version={$_version}'></script>";
			}else
				echo "<script type='text/javascript' language='javascript' src='{$ruta}?version={$_version}'></script>";
		}elseif($_tipo == 'jtables'){
			$ruta = self::__JS__.'jquery.dataTables.min.js';
			file_exists($ruta) or die("Error archivo '{$ruta}' no encontrado");
			$ruta2 = self::__ESTILO__.'jquery-table.css';
			file_exists($ruta2) or die("Error archivo '{$ruta2}' no encontrado");
			echo "<link rel='stylesheet' type='text/css' href='{$ruta2}?version={$_version}'>
			<script type='text/javascript' language='javascript' src='{$ruta}?version={$_version}'></script>";
		}else die('Error de tipo');
	}
	
	final public function Index(){
		include('IP.inc');
		$ruta = 'http://'.$this->_IP.'/'.self::__ROOT__;
		header("location: {$ruta}index.php");
		exit;
	}
	
	final public function LabId(){return $this->_LID;}
	final public function LabName(){return $this->_LNAME[$this->_LID];}
	
	private function Oculta(){
		//SI NO TIENE PERMISOS DE ESCRITURA, ESCONDE LOS BOTONES E IMAGENES
		echo '<style>
		.boton{display:none;}
		.tab{display:none;}
		.tab2{display:none;}
		.blokeado{display:none;}
		</style>';
	}
	
	final public function Portero($_privilegio){
		//ESTABLECE EL PRIVILEGIO DE CADA PAGINA
		if(!$_privilegio) die('Acceso denegado para este laboratorio');
		$this->_PRIV = $_privilegio;
	}
	
	final public function RutaGraficos(){return self::__GRAFICOS__;}
	
	final protected function SetLab($_ID){$this->_LID = $_ID;}
	
	final public function ShellIncluir($_nombre, $_tipo, $_version=0){
		/*USADO CUANDO SE OCUPAN RECURSOS DESDE UNA PAGINA DE SHELL*/
		if($_tipo == 'css'){
			$ruta = self::__ESTILO__.$_nombre.'.css';
			$ruta = str_replace('caspha-i/','',$ruta);
			file_exists($ruta) or die("Error archivo '{$ruta}' no encontrado");
			echo "<link rel='stylesheet' type='text/css' href='{$ruta}?version={$_version}'>";
			if($this->_PRIV  != 'E') $this->Oculta();
			$ruta = self::__JS__.$_nombre.'.js';
			$ruta = str_replace('caspha-i/','',$ruta);
			file_exists($ruta) or die("Error archivo '{$ruta}' no encontrado");
			echo "<script type='text/javascript' language='javascript' src='{$ruta}?version={$_version}'></script>";
		}elseif($_tipo == 'prt'){
			$ruta = self::__ESTILO__.$_nombre.'.css';
			$ruta = str_replace('caspha-i/','',$ruta);
			file_exists($ruta) or die("Error archivo '{$ruta}' no encontrado");
			echo "<link rel='stylesheet' type='text/css' media='print' href='{$ruta}?version={$_version}'>";
		}elseif($_tipo == 'js'){
			$ruta = self::__JS__.$_version.'/'.$_nombre.'.js';
			$ruta = str_replace('caspha-i/','',$ruta);
			file_exists($ruta) or die("Error archivo '{$ruta}' no encontrado");
			$token = date('dmYh:i:s');
			echo "<script type='text/javascript' language='javascript' src='{$ruta}?token={$token}&version={$_version}'></script>";
		}elseif($_tipo == 'doc'){			
			$ruta = self::__DOCS__.$_version.'/'.$_nombre.'.zip';
			$ruta = str_replace('caspha-i/','',$ruta);
			if(file_exists($ruta)) return $ruta;
			else return false;
		}elseif($_tipo == 'png'){
			$ruta = self::__IMAGENES__.$_nombre.'.'.$_tipo;
			$ruta = str_replace('caspha-i/','',$ruta);
			file_exists($ruta) or die("Error archivo '{$ruta}' no encontrado");
			echo "<img src='{$ruta}' border='0' />";
		}else die('Error de tipo');
	}

	final public function Title(){return self::__NAME__;}

	final public function ValidaModal(){
		/*include('IP.inc');
		$ruta = 'http://'.$this->_IP.'/'.self::__ROOT__;
		echo "<script>if(typeof window.dialogArguments=='undefined') location.href='{$ruta}err2.html';</script>";
		*/
	}
	
	final public function VersionDate(){return self::__FECHA__;}
	final public function VersionId(){return self::__VERSION__;}
}
?>