<?php

/* * *****************************************************************
  CLASE DE NIVEL 1 ENCARGADA DEL MODULO DE MANTENIMIENTOS
  (final SOLO PUEDE SER INSTANCIADA Y NO HEREDADA)
 * ***************************************************************** */

final class Mantenimientos extends Database {

//
    function AnalisisExtCs() {
        $ROW = $this->_QUERY("SELECT analisis_e FROM MAG000;");
        return $ROW[0]['analisis_e'];
    }

    function AnalisisExtIME($_id, $_nombre, $_tipo, $_fecha, $_proveedor, $_descripcion, $_lab, $_accion) {
        $_nombre = $this->FreePost($_nombre);
        $_descripcion = $this->FreePost($_descripcion);
        $_fecha = $this->_FECHA($_fecha);

        if ($this->_TRANS("EXECUTE PA_MAG_030 '{$_id}', '{$_nombre}', '{$_tipo}', '{$_fecha}', '{$_proveedor}', '{$_descripcion}', '{$_lab}', '{$_accion}';")) {
            //
            if ($_accion == 'I')
                $_accion = '0700';
            elseif ($_accion == 'M')
                $_accion = '0701';
            else
                $_accion = '0702';
            $this->Logger($_accion);
            //
            return 1;
        } else
            return 0;
    }

    function AnalisisExtMuestra($lab, $ordenado = false) {
        return $this->_QUERY("SELECT MAN004.id, MAN004.nombre, MAN004.tipo, CONVERT(VARCHAR,MAN004.fecha,105) as fecha, MAN004.proveedor as prov, MAN004.descripcion, MAN003.nombre AS proveedor
		FROM MAN003 INNER JOIN MAN004 ON MAN003.cs = MAN004.proveedor 
		WHERE (MAN004.lab='{$lab}');");
    }

    function AnalisisIntIME($_id, $_nombre, $_tarifa, $_costo, $_descuento, $_lab, $_accion) {
        $_nombre = $this->Free($_nombre);
        if ($this->_TRANS("EXECUTE PA_MAG_020 '{$_id}', '{$_nombre}', '{$_tarifa}', '{$_costo}', '{$_descuento}', '{$_lab}', '{$_accion}';")) {
            //
            if ($_accion == 'I')
                $_accion = '0703';
            elseif ($_accion == 'M')
                $_accion = '0704';
            else
                $_accion = '0705';
            $this->Logger($_accion);
            //
            return 1;
        } else
            return 0;
    }

    function AnalisisIntMuestra($lab, $ordenado = false) {
        if ($ordenado)
            $extra = 'ORDER BY nombre';
        else
            $extra = '';
        return $this->_QUERY("SELECT id, nombre, tarifa, costo, descuento FROM MAN000 WHERE (lab='{$lab}') {$extra};");
    }

    function AnalisisIntMuestraLDP($lab, $_tipo, $ordenado = false) {
        if ($ordenado)
            $extra = 'ORDER BY nombre';
        else
            $extra = '';
        //
        if ($_tipo == '0')//INTERNOS
            $extra2 = "AND (tarifa LIKE '8%' OR tarifa LIKE '20%' OR tarifa LIKE '21%')";
        else
            $extra2 = "AND (tarifa LIKE '9%' OR tarifa LIKE '21%')";
        //return $this->_QUERY("SELECT id, nombre, tarifa, costo, descuento FROM MAN000 WHERE (lab='{$lab}') {$extra2} {$extra};");
        return $this->_QUERY("SELECT id, nombre, tarifa, costo, descuento FROM MAN000 WHERE (lab='{$lab}') {$extra};");
    }

    function ContratosConsecutivo() {
        $ROW = $this->_QUERY("SELECT contratos FROM MAG000;");
        return $ROW[0]['contratos'];
    }

    function ContratosDetalleGet($_id) {
        return $this->_QUERY("SELECT MAN003.nombre, MAN008.cs, MAN008.contrato, MAN008.tipo, MAN008.proveedor, CONVERT(VARCHAR, MAN008.fecha, 105) AS fecha, CONVERT(VARCHAR, MAN008.aviso, 105) AS aviso					
		FROM MAN008 INNER JOIN MAN003 ON MAN008.proveedor = MAN003.cs 
		WHERE (MAN008.cs='{$_id}');");
    }

    function ContratosEquiposMuestra($_cs) {
        return $this->_QUERY("SELECT EQU000.id, EQU000.codigo, EQU000.nombre, EQU000.patrimonio, EQU000.serie
		FROM MAN010 INNER JOIN EQU000 ON MAN010.equipo = EQU000.id
		WHERE (MAN010.contrato = {$_cs})
		ORDER BY MAN010.linea");
    }

    function ContratosIME($_cs, $_accion, $_contrato, $_tipo, $_proveedor, $_fecha, $_aviso, $_equipo, $_ano, $_monto, $_moneda, $_obs, $_lab) {
        $_fecha = $this->_FECHA($_fecha);
        if ($_accion == 'I')
            $_log = '0709';
        elseif ($_accion == 'M')
            $_log = '0710';
        else
            $_log = '0711';
        $this->Logger($_log);
        //
        if ($this->_TRANS("EXECUTE PA_MAG_039 '{$_cs}', '{$_accion}', '{$_contrato}', '{$_tipo}', '{$_proveedor}', '{$_fecha}', '{$_aviso}', '{$_lab}';")) {
            if ($_accion == 'M') {
                $this->_TRANS("DELETE FROM MAN010 WHERE (contrato = '{$_cs}');");
                $this->_TRANS("DELETE FROM MAN011 WHERE (contrato = '{$_cs}');");
            }
            if ($_accion == 'I' or $_accion == 'M') {
                if ($_equipo != '') {
                    for ($i = 0; $i < count($_equipo); $i++) {
                        if (str_replace(' ', '', $_equipo[$i]) != '')
                            $this->_TRANS("INSERT INTO MAN010 values('{$_cs}','{$i}','{$_equipo[$i]}');");
                    }
                }
                if ($_monto != '0.00') {
                    for ($i = 0; $i < count($_monto); $i++) {
                        if (str_replace(' ', '', $_monto[$i]) != '0.00') {
                            $_ano[$i] = $this->FreePost($_ano[$i]);
                            $_obs[$i] = $this->FreePost($_obs[$i]);
                            $this->_TRANS("INSERT INTO MAN011 values('{$_cs}','{$i}','{$_ano[$i]}','{$_monto[$i]}','{$_moneda[$i]}','{$_obs[$i]}');");
                        }
                    }
                }
            }
            return 1;
        } else
            return 0;
    }

    function ContratosMontosMuestra($_cs) {
        return $this->_QUERY("SELECT ano, monto, moneda, obs FROM MAN011 WHERE contrato = {$_cs};");
    }

    function ContratosPendientes($_lab) {
        return $this->_QUERY("SELECT MAN003.nombre, MAN008.cs, MAN008.contrato, CONVERT(VARCHAR, MAN008.fecha, 105) AS fecha, (DATEDIFF(DAY, GETDATE(), MAN008.fecha)) AS plazo, MAN009.nombre AS tipo
		FROM MAN008 INNER JOIN MAN003 ON MAN008.proveedor = MAN003.cs INNER JOIN MAN009 ON MAN008.tipo = MAN009.cs
		WHERE (DATEDIFF(DAY, GETDATE(), MAN008.fecha) <= aviso) AND (MAN008.lid='{$_lab}');");
    }

    function ContratosResumenGet($_id, $_nombre, $_lab, $_busqueda, $ordenado = false) {
        if ($_busqueda == 1)
            $extra = "(MAN008.contrato LIKE '%{$_id}%')";
        else
            $extra = "(MAN003.nombre LIKE '%{$_nombre}%')";

        if ($ordenado)
            $extra2 = 'ORDER BY nombre';
        else
            $extra2 = '';

        return $this->_QUERY("SELECT MAN003.nombre, MAN008.cs, MAN008.contrato, CONVERT(VARCHAR, MAN008.fecha, 105) AS fecha, (DATEDIFF(DAY, GETDATE(), MAN008.fecha)) AS plazo, MAN009.nombre AS tipo
		FROM MAN008 INNER JOIN MAN003 ON MAN008.proveedor = MAN003.cs INNER JOIN MAN009 ON MAN008.tipo = MAN009.cs
		WHERE {$extra} AND (MAN008.lid='{$_lab}') {$extra2};");
    }

    function ContratosTiposIME($_cs, $_nombre, $_accion, $_lab) {
        $_nombre = $this->Free($_nombre);

        if ($this->_TRANS("EXECUTE PA_MAG_040 '{$_cs}', '{$_nombre}', '{$_lab}', '{$_accion}';")) {
            //
            if ($_accion == 'I')
                $_accion = '0712';
            elseif ($_accion == 'M')
                $_accion = '0713';
            else
                $_accion = '0714';
            $this->Logger($_accion);
            //
            return 1;
        } else
            return 0;
    }

    function ContratosTiposMuestra($_lab) {
        return $this->_QUERY("SELECT cs, nombre FROM MAN009 WHERE (lid = '{$_lab}') ORDER BY nombre;");
    }

    function ContratosVacio() {
        return array(0 => array(
                'cs' => '',
                'proveedor' => '',
                'contrato' => '',
                'tipo' => '',
                'nombre' => '',
                'equipo' => '',
                'patrimonio' => '',
                'serie' => '',
                'monto' => '0.00',
                'moneda' => '',
                'fecha' => '',
                'aviso' => '',
        ));
    }

    function ParametrosGet() {
        return $this->_QUERY("SELECT CONVERT(VARCHAR, personal1, 105) AS personal1, 
		CONVERT(VARCHAR, personal2, 105) AS personal2, 
		CONVERT(VARCHAR, personal3, 105) AS personal3
		FROM MANPRM;");
    }

    function ParametrosSet($_personal1, $_personal2, $_personal3) {
        //PONE A LOS USUARIOS PENDIENTE DE QUE ACTUALICEN
        $this->_TRANS("UPDATE PER000 SET actualizar = 0");
        return $this->_TRANS("UPDATE MANPRM SET personal1 = '{$_personal1}', 
		personal2 = '{$_personal2}', 
		personal3 = '{$_personal3}'
		FROM MANPRM;");
    }

    function TramitesDetalleGet($_id) {
        return $this->_QUERY("SELECT cs, tipo, descripcion, requisitos, CONVERT(VARCHAR,fecha,105) AS fecha, CONVERT(VARCHAR,aviso,105) AS aviso, vigencia 
		FROM MAN005 WHERE (cs='{$_id}');");
    }

    function TramitesIME($_cs, $_accion, $_tipo, $_descripcion, $_requisitos, $_fecha, $_aviso, $_vigencia, $_lab) {
        $_fecha = $this->_FECHA($_fecha);
        $_aviso = $this->_FECHA($_aviso);
        $_descripcion = $this->Free($_descripcion);
        $_requisitos = $this->Free($_requisitos);
        $_vigencia = $this->Free($_vigencia);

        if ($_accion == 'I') {
            if ($this->_TRANS("INSERT INTO MAN005 VALUES('{$_cs}', '{$_tipo}', '{$_descripcion}', '{$_requisitos}', '{$_fecha}', '{$_aviso}', '{$_vigencia}', '{$_lab}');")) {
                $_accion = '0715';
            }
        } elseif ($_accion == 'M') {
            if ($this->_TRANS("UPDATE MAN005 SET tipo = '{$_tipo}', descripcion = '{$_descripcion}', requisitos = '{$_requisitos}', fecha = '{$_fecha}', aviso = '{$_aviso}', vigencia ='{$_vigencia}' WHERE cs = '{$_cs}');")) {
                $_accion = '0716';
            }
        } elseif ($_accion == 'D') {
            if ($this->_TRANS("DELETE FROM MAN005 WHERE cs = '{$_cs}';")) {
                $_accion = '0717';
            }
        } else {
            return 0;
        }
        $this->Logger($_accion);
        //
        return 1;
    }

    function TramitesPendientes($_lab) {
        return $this->_QUERY("SELECT cs, descripcion, CONVERT(VARCHAR, fecha, 105) AS fecha1, (DATEDIFF(DAY, GETDATE(), fecha)) AS plazo,
			tipoD = CASE tipo
				WHEN '1' THEN 'Permiso sanitario de funcionamiento'
				WHEN '2' THEN 'Precursores ICD'
				WHEN '3' THEN 'Permiso de Circulación'
				WHEN '4' THEN 'Colegio Químicos'
				WHEN '5' THEN 'Préstamo de equipos'
				WHEN '6' THEN 'Renovación de acreditación'
				WHEN '9' THEN 'Otros'
				ELSE 'N/A'
			END 
			FROM MAN005 
			WHERE (aviso < GETDATE()) AND (lid='{$_lab}');");
    }

    function TramitesResumenGet($_desde, $_hasta, $_nombre, $_tipo, $_lab) {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        if ($_tipo == '' || $_tipo == '-99')
            $op = '<>';
        else
            $op = '=';

        return $this->_QUERY("SELECT cs, descripcion, CONVERT(VARCHAR, fecha, 105) AS fecha1, (DATEDIFF(DAY, GETDATE(), fecha)) AS plazo,
			tipoD = CASE tipo
				WHEN '1' THEN 'Permiso sanitario de funcionamiento'
				WHEN '2' THEN 'Precursores ICD'
				WHEN '3' THEN 'Permiso de Circulación'
				WHEN '4' THEN 'Colegio Químicos'
				WHEN '5' THEN 'Préstamo de equipos'
				WHEN '6' THEN 'Renovación de acreditación'
				WHEN '9' THEN 'Otros'
				ELSE 'N/A'
			END 
			FROM MAN005 
			WHERE (fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00') AND (descripcion LIKE '%{$_nombre}%') AND (lid='{$_lab}') AND (tipo $op '{$_tipo}');");
    }

    function TramitesVacio() {
        return array(0 => array(
                'cs' => '',
                'tipo' => '',
                'descripcion' => '',
                'requisitos' => '',
                'fecha' => '',
                'aviso' => '',
                'vigencia' => ''
        ));
    }

//
}

?>