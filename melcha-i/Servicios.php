<?php

/* * *****************************************************************
  CLASE DE NIVEL 1 ENCARGADA DEL MODULO DE SERVICIOS
  (final SOLO PUEDE SER INSTANCIADA Y NO HEREDADA)
 * ***************************************************************** */

final class Servicios extends Database
{

//	
    function AnalisisPendientes($_LAB, $_ROL, $_UID)
    {
        return $this->_QUERY("EXECUTE PA_MAG_091 '{$_LAB}', '{$_ROL}', '{$_UID}';");
    }

    function ApruebaXanalizar($_xanalizar, $_UID)
    {
        $Q = $this->_QUERY("SELECT MUE001.muestra FROM MUE001 INNER JOIN MUE002 ON MUE001.id = MUE002.xanalizar WHERE (MUE002.xanalizar = '{$_xanalizar}');");
        $W = $this->_QUERY("SELECT id FROM MUE001 WHERE (muestra = '{$Q[0]['muestra']}') AND analisis = '228';");
        if ($W) {
            $this->_TRANS("UPDATE MUE001 SET estado = 1 WHERE id = '{$W[0]['id']}';");
        }
        return $this->_TRANS("EXECUTE PA_MAG_099 '{$_xanalizar}', '{$_UID}';");
    }

    function EnsayosEncabezadoGet($_xanalizar)
    {
        return $this->_QUERY("SELECT MUE000.solicitud, MUE000.ref, VAR005.id AS analisis, VAR005.nombre
		FROM MUE000 INNER JOIN MUE001 ON MUE000.id = MUE001.muestra INNER JOIN VAR005 ON MUE001.analisis = VAR005.id
		WHERE (MUE001.id = '{$_xanalizar}');");
    }

    function EnsayosHistorialGeneralGet($_solicitud)
    {
        return $this->_QUERY("SELECT MUE000.ref, MUE002.id, MUE002.xanalizar, MUE002.resultado, MUE002.cumple, MET000.id AS tipo, MET000.nombre, MET000.pagina
		FROM MUE002 INNER JOIN MUE001 ON MUE002.xanalizar = MUE001.id INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MET000 ON MUE002.tipo = MET000.id
		WHERE (MUE000.solicitud = '{$_solicitud}')
		ORDER BY MUE002.fecha;");
    }

    function EnsayosHistorialMuestraGet($_xanalizar)
    {
        return $this->_QUERY("SELECT MUE002.id, MUE002.resultado, MUE002.cumple, MUE002.analista, MET000.id AS ensayo, MET000.nombre, MET000.pagina
		FROM MUE002 INNER JOIN MET000 ON MUE002.tipo = MET000.id
		WHERE (MUE002.xanalizar = '{$_xanalizar}')
		ORDER BY MUE002.fecha;");
    }

    function MuestraAccion($_var)
    {
        if ($_var == '0')
            return 'Recibida:';
        elseif ($_var == '1')
            return 'Finaliz� an�lisis:';
        elseif ($_var == 'B')
            return 'Asignada por:';
        elseif ($_var == 'A')
            return '<strong>Asignada a:</strong>';
        elseif ($_var == '2')
            return 'Reporte:';
        elseif ($_var == '3')
            return 'Por retirar:';
        elseif ($_var == '4')
            return 'Retirada:';
        else
            return 'N/A';
    }

    private function MuestraAsociaAnalisis01($_solicitud)
    {
        $muestras = $this->_QUERY("SELECT id FROM MUE000 WHERE (solicitud='{$_solicitud}') ORDER BY id;");
        $analisis = $this->_QUERY("SELECT analisis FROM SER002 WHERE (solicitud = '{$_solicitud}') ORDER BY codigo;");

        $cs = $this->_QUERY("SELECT xanalizar FROM MAG000;");
        $cont = $cs[0]['xanalizar'];

        for ($x = 0; $x < count($muestras); $x++) {
            $cs = $cont . '-' . date('Y');
            $this->_TRANS("INSERT INTO MUE001 VALUES('{$cs}', '{$muestras[$x]['id']}', {$analisis[$x]['analisis']}, 0);");
            $cont++;
        }

        $this->_TRANS("UPDATE MAG000 SET xanalizar = {$cont};");
    }

    private function MuestraAsociaAnalisis03($_solicitud)
    {
        $this->_TRANS("EXECUTE PA_MAG_128 '{$_solicitud}';");
    }

    function MuestraEstado($_var)
    {
        if ($_var == '0')
            return 'Recepci�n';
        elseif ($_var == '1')
            return 'An�lisis';
        elseif ($_var == '2')
            return 'Reporte';
        elseif ($_var == '3')
            return 'Por retirar';
        elseif ($_var == '4')
            return 'Retirada';
        elseif ($_var == '5')
            return 'Reporte'; //ESTADO TEMPORAL PARA LRE
        else
            return 'N/A';
    }

    function obtieneNombre($_id)
    {
        return $this->_QUERY("SELECT (nombre +' '+ ap1) AS nombre FROM SEG001 WHERE id = '{$_id}';");
    }

    function MuestrasAsignarGet($_lab, $_reasignar = '')
    {
        if ($_reasignar == '') {
            return $this->_QUERY("SELECT MUE000.id, MUE000.solicitud, MUE000.ref, MUE000.estado, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fecha1 FROM MUE000 INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra WHERE (MUE000.id LIKE '{$_lab}-%') AND (estado = '0') ORDER BY MUE000.solicitud, MUE000.id;");
        } else {
            return $this->_QUERY("SELECT MUE000.id, MUE000.solicitud, MUE000.ref, MUEBIT.usuario, MUEBIT.usuario2, MUEBIT.usuario3, MUEBIT.usuario4 FROM MUE000 INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra WHERE (MUE000.id LIKE '{$_lab}-%') AND (MUE000.estado = '1') AND (MUEBIT.accion = 'A') ORDER BY MUE000.solicitud, MUE000.id");
        }
    }

    function MuestrasAsignarSet($_muestra, $_analista, $_analista2, $_analista3, $_analista4, $_UID)
    {
        //LE CAMBIA EL ESTADO A LA MUESTRA
        $this->_TRANS("UPDATE MUE000 SET estado = '1' WHERE (id = '{$_muestra}');");
        //INGRESA EN BITACORA AL JEFE QUE ASIGNO AL ANALISTA
        $this->_TRANS("INSERT INTO MUEBIT VALUES('{$_muestra}', '{$_UID}', 'B', GETDATE(), NULL);");
        //ASOCIA LA MUESTRA A UN ANALISTA
        $this->_TRANS("INSERT INTO MUEBIT VALUES('{$_muestra}', '{$_analista}', 'A', GETDATE(), '{$_analista2}', '{$_analista3}', '{$_analista4}');");
        //NOTIFICA ANALISTA

        //se elimina el envio de correo de notificacion
       /* $this->TablonSet(25, $_analista, 0, true);
        $ROW = $this->_QUERY("SELECT email FROM SEG001 WHERE id = '{$_analista}';");
        $Robot = new Seguridad();
        $Robot->Email($ROW[0]['email'], 'Se le ha asignado la muestra ' . $_muestra);
        if ($_analista2 != null) {
            $this->TablonSet(25, $_analista2, 0, true);
            $ROW2 = $this->_QUERY("SELECT email FROM SEG001 WHERE id = '{$_analista2}';");
            $Robot->Email($ROW2[0]['email'], 'Se le ha asignado la muestra ' . $_muestra);
        }
        if ($_analista3 != null) {
            $this->TablonSet(25, $_analista3, 0, true);
            $ROW2 = $this->_QUERY("SELECT email FROM SEG001 WHERE id = '{$_analista3}';");
            $Robot->Email($ROW2[0]['email'], 'Se le ha asignado la muestra ' . $_muestra);
        }
        if ($_analista4 != null) {
            $this->TablonSet(25, $_analista4, 0, true);
            $ROW2 = $this->_QUERY("SELECT email FROM SEG001 WHERE id = '{$_analista4}';");
            $Robot->Email($ROW2[0]['email'], 'Se le ha asignado la muestra ' . $_muestra);
        }*/
        return 1;
    }

    function MuestrasClienteGet($_cedula)
    {
        return $this->_QUERY("SELECT MUE000.solicitud, MUE000.ref, MUE000.estado, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fecha1
		FROM MUE000 INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra
		WHERE (MUEBIT.accion = '0') AND (MUE000.estado = '0' OR MUE000.estado = '1' OR MUE000.estado = '2' OR MUE000.estado = '3') AND (solicitud IN
			(
				SELECT cs FROM SER001 WHERE (cliente = '{$_cedula}') AND (estado = '2')
				UNION
				SELECT cs FROM SER003 WHERE (cliente = '{$_cedula}') AND (estado = '2')
			)
		)
		ORDER BY MUE000.solicitud;");
    }

    function MuestrasGet($_LAB, $_tipo, $_valor, $_desde, $_hasta, $_estado)
    {
        $_valor = str_replace(' ', '', $_valor);

        if ($_estado == '') {
            $op = '<>';
        } else {
            $op = '=';
        }
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);

        if ($_valor != '') { //ESTA BUSCANDO POR VALOR DIRECTO
            if ($_tipo == '0') { //SOLICITUD
                $extra = 'solicitud';
            } elseif ($_tipo == '1') { //# DE MUESTRA
                $extra = 'ref';
            } else {
                $extra = 'externo';
            }

            $SQL = "SELECT id, externo, solicitud, ref, estado, (SELECT CONVERT(VARCHAR, fecha, 105) FROM MUEBIT WHERE (muestra=id) AND (accion = '0')) AS fecha1
			FROM MUE000 
			WHERE ({$extra} = '{$_valor}') AND (id LIKE '{$_LAB}-%')
			ORDER BY id;";
        } else { //ESTA BUSCANDO POR FECHA Y ESTADO
            $SQL = "SELECT id, externo, solicitud, ref, estado, (SELECT CONVERT(VARCHAR, fecha, 105) FROM MUEBIT WHERE (muestra=id) AND (accion = '0')) AS fecha1
			FROM MUE000 
			WHERE (MUE000.id LIKE '{$_LAB}-%') AND (MUE000.estado {$op} '{$_estado}') AND (MUE000.id IN (
				SELECT muestra FROM MUEBIT WHERE (muestra=id) AND (accion = '0') AND (fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')
				)
			)
			ORDER BY id;";
        }
        return $this->_QUERY($SQL);
    }

    function MuestrasHistorialGet($_cs)
    {
        return $this->_QUERY("SELECT MUEBIT.accion, CONVERT(VARCHAR, MUEBIT.fecha, 100) AS fecha1, SEG001.nombre + ' ' + SEG001.ap1 AS nombre
		FROM MUEBIT INNER JOIN SEG001 ON MUEBIT.usuario = SEG001.id
		WHERE (MUEBIT.muestra = '{$_cs}')
		ORDER BY MUEBIT.fecha;");
    }

    function MuestrasPendientesSet($_seleccionados, $_UID)
    {
        $_seleccionados = str_replace('1=1&', '', $_seleccionados);
        $_seleccionados = explode('&', $_seleccionados);
        $muestra = $estado = '';

        for ($i = 0; $i < count($_seleccionados); $i++) {
            list($muestra, $estado) = explode(';', $_seleccionados[$i]);
            $this->_TRANS("UPDATE MUE000 SET estado = '{$estado}' WHERE (id = '{$muestra}');");
            $this->_TRANS("INSERT INTO MUEBIT values('{$muestra}', {$_UID}, '{$estado}', GETDATE());");
        }
        return 1;
    }

    function MuestrasPendientesGet($_LAB, $_UID)
    {
        return $this->_QUERY("EXECUTE PA_MAG_092 '{$_LAB}', '{$_UID}';");
    }

    function MuestrasReasignar($_muestra, $_analista, $_analista2, $_analista3, $_analista4, $_UID)
    {
        //INGRESA EN BITACORA AL JEFE QUE ASIGNO AL ANALISTA
        $this->_TRANS("UPDATE MUEBIT SET fecha = GETDATE() WHERE (muestra = '{$_muestra}') AND (accion='B');");
        //ASOCIA LA MUESTRA A UN ANALISTA
        $this->_TRANS("UPDATE MUEBIT SET usuario = '{$_analista}', usuario2 = '{$_analista2}', usuario3 = '{$_analista3}', usuario4 = '{$_analista4}' WHERE (muestra = '{$_muestra}') AND (accion='A');");

        //NOTIFICA ANALISTA
        $this->TablonSet(25, $_analista, 0, true);
        $ROW = $this->_QUERY("SELECT email FROM SEG001 WHERE id = '{$_analista}';");
        $Robot = new Seguridad();
        $Robot->Email($ROW[0]['email'], 'Se le ha asignado la muestra ' . $_muestra);
        if ($_analista2 != null) {
            $this->TablonSet(25, $_analista2, 0, true);
            $ROW2 = $this->_QUERY("SELECT email FROM SEG001 WHERE id = '{$_analista2}';");
            $Robot->Email($ROW2[0]['email'], 'Se le ha asignado la muestra ' . $_muestra);
        }
        if ($_analista3 != null) {
            $this->TablonSet(25, $_analista3, 0, true);
            $ROW2 = $this->_QUERY("SELECT email FROM SEG001 WHERE id = '{$_analista3}';");
            $Robot->Email($ROW2[0]['email'], 'Se le ha asignado la muestra ' . $_muestra);
        }
        if ($_analista4 != null) {
            $this->TablonSet(25, $_analista4, 0, true);
            $ROW2 = $this->_QUERY("SELECT email FROM SEG001 WHERE id = '{$_analista4}';");
            $Robot->Email($ROW2[0]['email'], 'Se le ha asignado la muestra ' . $_muestra);
        }
        return 1;
    }

    function NotificaJefes($_LAB, $_tipo, $_msj = '')
    {
        $ROW = $this->_QUERY("SELECT SEG001.id, SEG001.email
		FROM SEG001 INNER JOIN SEG007 ON SEG001.id = SEG007.usuario INNER JOIN SEG002 ON SEG007.perfil = SEG002.id INNER JOIN SEG004 ON SEG002.id = SEG004.perfil
        WHERE (SEG002.lab = '{$_LAB}') AND (SEG004.permiso = '{$_tipo}') and (SEG001.id NOT IN (23, 14, 45) and (SEG001.estado = 1));");

        for ($x = 0, $destinos = ''; $x < count($ROW); $x++) {
            $destinos .= ',' . $ROW[$x]['email'];
            $this->TablonSet($_tipo, $ROW[$x]['id'], $_tipo, false);
        }

        $destinos = substr($destinos, 1);
        //ENVIA EMAIL
        $Robot = new Seguridad();
        if ($_msj == '')
            $Robot->Email($destinos, 'Ha recibido una notificaci�n de sistema');
        else
            $Robot->Email($destinos, $_msj);
    }

    function Solicitud01DetalleGet($_cs)
    {
       /* return $this->_QUERY("SELECT SER002.codigo, SER002.conSolicitud, SER002.presentacion, SER002.empaque, SER002.forma, ser002.masa, ser002.tipo, SER002.observaciones,SER002.obsfrf, SER002.parte, SER002.metodo, SER002.analisis, ser002.fmuestra AS fecha, ser002.hmuestra AS hora, SER002.numInfo as nia, VAR001.id AS matriz, VAR001.nombre, VAR001.grupo, VAR001.acreditado, VAR005.id AS cod_analisis, VAR005.nombre AS nom_analisis
		FROM SER002 INNER JOIN VAR001 ON SER002.matriz = VAR001.id INNER JOIN VAR005 ON SER002.analisis = VAR005.id
		WHERE (SER002.solicitud = '{$_cs}')
		ORDER BY SER002.codigo;");*/



        return $this->_QUERY("SELECT SER002.codigo, SER002.conSolicitud, SER002.presentacion, SER002.empaque, SER002.forma, ser002.masa, ser002.tipo, SER002.observaciones,SER002.obsfrf, SER002.parte, SER002.metodo, SER002.analisis, ser002.fmuestra AS fecha, ser002.hmuestra AS hora, SER002.numInfo as nia, VAR001.id AS matriz, VAR001.nombre, VAR001.grupo, VAR001.acreditado, VAR005.id AS cod_analisis, VAR005.nombre AS nom_analisis , VAR011.nombre as nombreGrupo
		FROM SER002 INNER JOIN VAR001 ON SER002.matriz = VAR001.id INNER JOIN VAR005 ON SER002.analisis = VAR005.id INNER JOIN VAR011 ON VAR001.grupo = VAR011.id
		WHERE (SER002.solicitud = '{$_cs}')
		ORDER BY SER002.codigo;");

    }

    function Solicitud01DetalleGetSustitucion($_cs, $_numInfo)
    {
        return $this->_QUERY("SELECT SER002.codigo, SER002.conSolicitud, SER002.presentacion, SER002.empaque, SER002.forma, ser002.masa, ser002.tipo, SER002.observaciones,SER002.obsfrf, SER002.parte, SER002.metodo, SER002.analisis, ser002.fmuestra AS fecha, ser002.hmuestra AS hora, SER002.numInfo as nia, VAR001.id AS matriz, VAR001.nombre, VAR001.grupo, VAR001.acreditado, VAR005.id AS cod_analisis, VAR005.nombre AS nom_analisis , VAR011.nombre as nombreGrupo
		FROM SER002 INNER JOIN VAR001 ON SER002.matriz = VAR001.id INNER JOIN VAR005 ON SER002.analisis = VAR005.id INNER JOIN VAR011 ON VAR001.grupo = VAR011.id
		WHERE (SER002.numInfo = '{$_numInfo}')
		ORDER BY SER002.codigo;");

    }





    function Solicitud01DetalleVacio()
    {
        return array(0 => array(
            'muestra' => '',
            'linea' => '',
            'codigo' => '',
            'nombre' => '',
            'matriz' => '',
            'tipo' => '',
            'presentacion' => '',
            'empaque' => '',
            'forma' => '',
            'masa' => '',
            'observaciones' => '',
            'obsfrf' => '',
            'nom_analisis' => '',
            'cod_analisis' => '',
            'parte' => '',
            'metodo' => 'QuEChERS'),

        );
    }

    function Solicitud01EncabezadoGet($_cs)
    {

        return $this->_QUERY("SELECT SER001.cs, CONVERT(VARCHAR, SER001.fecha, 105) AS fecha, CONVERT(VARCHAR, fecha, 108) AS hora, SER001.cliente, SER001.nomsolicitante, SER001.entregado, SER001.proposito, SER001.muestreo, SER001.entregacliente, SER001.entregaanalista, SER001.lote, SER001.obs, SER001.estado, SER001.flab, SER001.hlab, SER001.fmuestreo, SER001.hmuestreo, MAN002.nombre AS nomcliente, MAN002.direccion 
		FROM SER001 INNER JOIN MAN002 ON SER001.cliente = MAN002.id 
		WHERE (SER001.cs = '{$_cs}');");
    }

    function Solicitud01EncabezadoVacio()
    {
        return array(0 => array(
            'cs' => '-',
            'fecha' => '',
            'estado' => '',
            'cliente' => '',
            'nomcliente' => '',
            'nomsolicitante' => '',
            'entregado' => '',
            'lote' => '',
            'proposito' => '',
            'muestreo' => 'Realizado por el cliente',
            'fmuestreo' => '' . date('d-m-Y'),
            'hmuestreo' => '' . date('H:i'),
            'flab' => '' . date('d-m-Y'),
            'hlab' => '' . date('H:i'),
            'entregacliente' => '',
            'entregaanalista' => '1',
            'obs' => '')
        );
    }

    function Solicitud01HistorialGet($_cs)
    {
        return $this->_QUERY("SELECT SERBIT.usuario, SERBIT.accion, SEG001.nombre + ' ' + SEG001.ap1 AS nombre, CONVERT(VARCHAR, SERBIT.fecha, 100) AS fecha1
		FROM SERBIT INNER JOIN SEG001 ON SERBIT.usuario = SEG001.id 
		WHERE (SERBIT.solicitud = '{$_cs}') 
		ORDER BY SERBIT.fecha;");
    }

    function obtieneHora($_cs)
    {
        $Q = $this->_QUERY("SELECT CONVERT(VARCHAR, SERBIT.fecha, 108) AS hora FROM SERBIT INNER JOIN SEG001 ON SERBIT.usuario = SEG001.id WHERE (SERBIT.solicitud = 'LRE-2019-4') AND accion = '0';");
        return $Q[0]['hora'];
    }

    function Solicitud01IME($_accion, $_cliente, $_solicitante, $_cs, $_entregado, $_UID, $_proposito, $_muestreo, $_entregacliente, $_entregaanalista, $_obs, $_codigo, $_matriz, $_tipo, $_presentacion, $_empaque, $_forma, $_masa, $_observaciones, $_obsfrf, $_analisis, $_parte, $_metodo, $_flab, $_hlab, $_fmuestra, $_hmuestra, $_nia)
    {
        $_muestreo = $this->Free($_muestreo);
        $_solicitante = $this->Free($_solicitante);
        $_obs = $this->Free($_obs);


        $_codigo = str_replace('1=1&', '', $_codigo);
        $_codigo = $this->Free($_codigo);
        $_codigo = explode('&', $_codigo);
        $_matriz = str_replace('1=1&', '', $_matriz);
        $_matriz = explode('&', $_matriz);
        $_presentacion = str_replace('1=1&', '', $_presentacion);
        $_presentacion = explode('&', $_presentacion);
        $_empaque = str_replace('1=1&', '', $_empaque);
        $_empaque = explode('&', $_empaque);
        $_forma = str_replace('1=1&', '', $_forma);
        $_forma = explode('&', $_forma);

        $_masa = str_replace('1=1&', '', $_masa);
        $_masa = $this->Free($_masa);
        $_masa = explode('&', $_masa);

        $_observaciones = str_replace('1=1&', '', $_observaciones);
        $_observaciones = $this->Free($_observaciones);
        $_observaciones = explode('&', $_observaciones);

        $_obsfrf = str_replace('1=1&', '', $_obsfrf);
        $_obsfrf = $this->Free($_obsfrf);
        $_obsfrf = explode('&', $_obsfrf);

        $_tipo = str_replace('1=1&', '', $_tipo);
        $_tipo = $this->Free($_tipo);
        $_tipo = explode('&', $_tipo);


        $_analisis = str_replace('1=1&', '', $_analisis);
        $_analisis = explode('&', $_analisis);
        $_parte = str_replace('1=1&', '', $_parte);
        $_parte = explode('&', $_parte);
        $_metodo = str_replace('1=1&', '', $_metodo);
        $_metodo = explode('&', $_metodo);


        $_hmuestra = str_replace('1=1&', '', $_hmuestra);
        $_hmuestra = explode('&', $_hmuestra);
        $_fmuestra = str_replace('1=1&', '', $_fmuestra);
        $_fmuestra = explode('&', $_fmuestra);


        $_nia = str_replace('1=1&', '', $_nia);
        //   $_nia = explode('&', $_nia);


        if ($this->_TRANS("EXECUTE PA_MAG_069 '{$_cs}', '{$_accion}', '{$_cliente}', '{$_solicitante}', '{$_entregado}', '{$_UID}', '{$_proposito}', '{$_muestreo}', '{$_entregacliente}', '{$_entregaanalista}', '{$_obs}', '{$_flab}', '{$_hlab}', '0', '0';")) {
            $this->_TRANS("DELETE FROM SER002 WHERE solicitud = '{$_cs}';");
            for ($i = 0; $i < count($_codigo); $i++) {
                //consecutivo de muestra
                $cons= $_cs.'-'.($i+1);

                if (str_replace(' ', '', $_codigo[$i]) != '') {
                    $this->_TRANS("INSERT INTO SER002 values('{$_cs}', '{$_codigo[$i]}', '$cons' , '{$_matriz[$i]}', '{$_tipo[$i]}',  '{$_presentacion[$i]}', '{$_empaque[$i]}', '{$_forma[$i]}', '{$_masa[$i]}', '{$_observaciones[$i]}', '{$_analisis[$i]}', '{$_parte[$i]}', '{$_metodo[$i]}', '{$_obsfrf[$i]}', '{$_fmuestra[$i]}', '{$_hmuestra[$i]}', '');");
                }
            }
            return 1;
        } else {
            return 0;
        }
    }


    function asignaInforme($_cs, $_matriz)
    {
        $_cs = str_replace('1=1&', '', $_cs);
        $_cs = $this->Free($_cs);

    }


    function Solicitud01Modifica($_solicitud, $_obs, $_estado, $_codigo, $_UID)
    {
        $_obs = $this->Free($_obs);
        $_codigo = str_replace('1=1&', '', $_codigo);
        $_codigo = explode('&', $_codigo);

        $this->_TRANS("INSERT INTO SERBIT VALUES('{$_solicitud}', '{$_UID}', '{$_estado}', GETDATE());");
        if ($_estado == '4') {
            $_estado = '0';
        } elseif ($_estado == '2') {
            //SI ES APROBACION, TOMA LAS MUESTRAS DE LA SOLICITUD Y LAS PASA A LA TABLA DE MUESTRAS
            $this->_TRANS("EXECUTE PA_MAG_088 '{$_solicitud}', '{$_UID}';");
            $lote = str_replace('LRE-', '', $_solicitud);
            $this->_TRANS("UPDATE SER001 SET lote = '{$lote}' WHERE (cs = '{$_solicitud}');");
            // $this->_TRANS("UPDATE SER002 SET numInfo = '{$numinfo}' WHERE (cs = '{$_solicitud}');");
            $this->MuestraAsociaAnalisis01($_solicitud);

            //magia del consecutivo del informe
            for ($i = 0; $i < count($_codigo); $i++) {
                $Servitor1 = new Servicios();
                $niaO = $Servitor1->informesConsecutivo();
                $ceros = '0';
                if ($niaO < 10000) {
                    if ($niaO >= 1000) {
                        $ceros = '0';
                    } elseif ($niaO >= 100) {
                        $ceros = '00';
                    } elseif ($niaO >= 10) {
                        $ceros = '000';
                    } else
                        $ceros = '0000';
                }
                $nia = date('y') . '-' . $ceros . $niaO;
                if (str_replace(' ', '', $_codigo[$i]) != '') {
                    $this->_TRANS("update SER002 set numInfo = '$nia' where solicitud = '$_solicitud' and codigo = '$_codigo[$i]'");
                }
                $this->_TRANS("UPDATE MAG000 SET informeAsignado =  informeAsignado + 1;");
            }
        }
        $this->_TRANS("UPDATE SER001 SET fecha = GETDATE(), obs = '{$_obs}', estado = '{$_estado}' WHERE (cs = '{$_solicitud}');");
        return 1;
    }

    function SolicitudAccion($_consec, $_tipo, $_accion, $_usuario)
    {
        //LRE
        if ($_tipo == '1') {
            $this->_TRANS("UPDATE SER001 SET estado = '{$_accion}' WHERE cs = '{$_consec}';");
            $this->_TRANS("INSERT INTO SERBIT VALUES('{$_consec}', '{$_usuario}', '{$_accion}', GETDATE());");
        } elseif ($_tipo == '3') {
            $this->_TRANS("UPDATE SER003 SET estado = '{$_accion}' WHERE cs = '{$_consec}';");
            $this->_TRANS("INSERT INTO SERBI2 VALUES('{$_consec}', '{$_usuario}', '{$_accion}', GETDATE());");
        }
        return 1;
    }

    function SolicitudAnexosGet($_cs)
    {
        return $this->_QUERY("SELECT detalle FROM SER008 WHERE (solicitud='{$_cs}');");
    }

    function SolicitudAnexosSet($_cs, $_detalle)
    {
        $_detalle = $this->FreePost($_detalle);
        $this->SolicitudAnexosDel($_cs, $_detalle);
        return $this->_QUERY("INSERT INTO SER008 VALUES('{$_cs}', '{$_detalle}');");
    }

    function SolicitudAnexosDel($_cs, $_doc)
    {
        $_doc = str_replace(' ', '', $_doc);
        $_doc = str_replace("{$_cs}-", '', $_doc);

        return $this->_TRANS("DELETE FROM SER008 WHERE (solicitud='{$_cs}') AND (detalle = '{$_doc}');");
    }

    function SolicitudImportaDetalle($_cs, $_tipo)
    {
        return $this->_QUERY("SELECT VAR005.id AS codigo, VAR005.nombre AS analisis, COT004.conc AS rango, '' AS quela, COT004.unidad, COT004.fuente
		FROM COT004 INNER JOIN VAR005 ON COT004.analisis = VAR005.id
		WHERE (COT004.solicitud = '{$_cs}') AND (COT004.tipo = '{$_tipo}')
		UNION
		SELECT VAR005.id AS codigo, VAR005.nombre AS analisis, COT002.rango, COT002.quela, COT002.tipo AS unidad, COT002.fuente
		FROM COT002 INNER JOIN VAR005 ON COT002.analisis = VAR005.id
		WHERE (COT002.solicitud = '{$_cs}') AND (COT002.es_analisis = '{$_tipo}')
		ORDER BY VAR005.nombre;");
    }

    function SolicitudImportaElementos($_cs)
    {
        $ROW = $this->_QUERY("SELECT COT002.rango, COT002.quela, COT002.tipo AS unidad, COT002.fuente, VAR005.id AS codigo, VAR005.nombre AS analisis
		FROM COT002 INNER JOIN VAR005 ON COT002.analisis = VAR005.id
		WHERE (COT002.solicitud = '{$_cs}') AND (COT002.es_analisis = '1')
		ORDER BY VAR005.nombre;");
        //SI ES DE PLAGUICIDAS, NO IMPORTO NADA
        if (!$ROW)
            $ROW = $this->Solicitud03ElementosVacio();
        return $ROW;
    }

    function SolicitudImportaEncabezado($_cs)
    {
        $ROW = $this->_QUERY("SELECT COT000.tipo, MAN002.id AS cliente, MAN002.nombre AS tmp, MAN002.direccion
		FROM COT000 INNER JOIN MAN002 ON COT000.cliente = MAN002.id
		WHERE (COT000.numero = '{$_cs}');");

        if ($ROW[0]['tipo'] == '1') {//FERTILIZANTES
            $ROW2 = $this->_QUERY("SELECT comercial AS producto, tipo AS tipo_form, metodo, mezcla, suministro, discre, num_sol, num_mue, muestras
			FROM COT001 WHERE (solicitud = '{$_cs}');");
        } elseif ($ROW[0]['tipo'] == '2') {//PLAGUICIDAS
            $ROW2 = $this->_QUERY("SELECT '' AS producto, tipo AS tipo_form , metodo, '' AS mezcla, suministro, discre, num_sol, num_mue, muestras
			FROM COT003 WHERE (solicitud = '{$_cs}');");
        }
        //
        $ROW[0]['producto'] = $ROW2[0]['producto'];
        $ROW[0]['tipo_form'] = $ROW2[0]['tipo_form'];
        $ROW[0]['suministro'] = $ROW2[0]['suministro'];
        $ROW[0]['discre'] = $ROW2[0]['discre'];
        $ROW[0]['num_sol'] = $ROW2[0]['num_sol'];
        $ROW[0]['num_mue'] = $ROW2[0]['num_mue'];
        $ROW[0]['muestras'] = $ROW2[0]['muestras'];
        $ROW[0]['mezcla'] = $ROW2[0]['mezcla'];
        $ROW[0]['registro'] = '';
        $ROW[0]['dosis'] = '';

        //METODO APORTADO POR CLIENTE
        if ($ROW2[0]['metodo'] == '2')
            $ROW[0]['metodo'] = '1';
        else
            $ROW[0]['metodo'] = '0';
        //RELLENO
        $ROW[0]['cs'] = $ROW[0]['estado'] = $ROW[0]['fecha'] = $ROW[0]['dependencia'] = $ROW[0]['obs'] = $ROW[0]['entregadas'] = $ROW[0]['densidad'] = $ROW[0]['estado'] = '';
        $ROW[0]['solicitud'] = $_cs;
        $ROW[0]['proposito'] = 'Control de calidad';

        return $ROW;
    }

    function Solicitud03AnalisisGet($_cs, $_tipo)
    {
        return $this->_QUERY("SELECT SER005.analisis AS codigo, VAR005.nombre AS analisis
		FROM SER005 INNER JOIN VAR005 ON SER005.analisis = VAR005.id
		WHERE (SER005.solicitud = '$_cs') AND (SER005.es_analisis = {$_tipo})
		ORDER BY VAR005.nombre;");
    }

    function Solicitud03AnalisisSet($_cs, $_analisis, $_tipo)
    {
        $this->_TRANS("DELETE FROM SER005 WHERE (solicitud = '{$_cs}') AND (es_analisis = {$_tipo});");
        //SI NO VIENE NADA SE SALTA TODO
        if ($_analisis != '1=1') {
            $_analisis = str_replace('1=1&', '', $_analisis);
            $_analisis = explode('&', $_analisis);

            for ($i = 0; $i < count($_analisis); $i++) {
                if (str_replace(' ', '', $_analisis[$i]) != '')
                    $this->_TRANS("INSERT INTO SER005 values('{$_cs}', {$_analisis[$i]}, {$_tipo});");
            }
        }
    }

    function Solicitud03AnalisisVacio()
    {
        return array(0 => array(
            'analisis' => '',
            'codigo' => '',
            'declarada' => '',
            'uc' => '',
            'lugar' => '')
        );
    }

    function Solicitud03ElementosGet($_cs, $_tipo)
    {
        return $this->_QUERY("SELECT SER004.analisis AS codigo, SER004.rango, SER004.quela, SER004.unidad, SER004.fuente, VAR005.nombre AS analisis
		FROM VAR005 INNER JOIN SER004 ON VAR005.id = SER004.analisis
		WHERE (SER004.solicitud = '{$_cs}') AND (SER004.tipo = '{$_tipo}')
		ORDER BY VAR005.nombre;");
    }

    function Solicitud03ElementosLimpia($_cs)
    {
        $this->_TRANS("DELETE FROM SER004 WHERE (solicitud = '{$_cs}');");
    }

    function Solicitud03ElementosSet($_cs, $_elementos, $_rango, $_quela, $_unidad, $_fuente, $_tipo, $_formulacion = '')
    {
        $_elementos = str_replace('1=1&', '', $_elementos);
        $_elementos = explode('&', $_elementos);
        $_rango = str_replace('1=1&', '', $_rango);
        $_rango = explode('&', $_rango);
        $_quela = str_replace('1=1&', '', $_quela);
        $_quela = explode('&', $_quela);
        $_unidad = str_replace('1=1&', '', $_unidad);
        $_unidad = explode('&', $_unidad);
        $_fuente = str_replace('1=1&', '', $_fuente);
        $_fuente = explode('&', $_fuente);

        for ($i = 0; $i < count($_elementos); $i++) {
            if (str_replace(' ', '', $_elementos[$i]) != '' && $_elementos[$i] != '1=1') {
                if (!isset($_quela[$i]))
                    $_quela[$i] = '';
                if (!isset($_fuente[$i]))
                    $_fuente[$i] = '';
                isset($_rango[$i]) ? $_rango[$i] : $_rango[$i] = '';
                isset($_unidad[$i]) ? $_unidad[$i] : $_unidad[$i] = '';
                $this->_TRANS("INSERT INTO SER004 values('{$_cs}', {$_elementos[$i]}, '{$_rango[$i]}', '{$_quela[$i]}', '{$_unidad[$i]}', '{$_fuente[$i]}', '{$_tipo}');");
            }
        }
    }

    function Solicitud03ElementosVacio()
    {
        return array(0 => array(
            'analisis' => '',
            'codigo' => '',
            'rango' => '',
            'quela' => '',
            'unidad' => '',
            'fuente' => '')
        );
    }

    function Solicitud03EncabezadoGet($_cs)
    {

        $ROW = $this->_QUERY("SELECT 1 AS muestras, '{$_cs}' AS cs, CONVERT(VARCHAR, SER003.fecha, 105) AS fecha, SER003.tipo, SER003.directa, SER003.proposito, SER003.dependencia, SER003.obs, SER003.cliente, SER003.entregadas, SER003.producto, SER003.tipo_form, SER003.metodo, SER003.registro, SER003.dosis, SER003.mezcla, SER003.densidad, SER003.estado, MAN002.nombre AS tmp, SER003.direccion
		FROM SER003 INNER JOIN MAN002 ON SER003.cliente = MAN002.id
		WHERE (SER003.cs = '{$_cs}');");

        if ($ROW[0]['directa'] == '1')
            $ROW[0]['solicitud'] = '';
        else {
            $ROW2 = $this->_QUERY("SELECT SER007.solicitud, COT000.tipo FROM COT000 INNER JOIN SER007 ON COT000.numero = SER007.solicitud WHERE (SER007.servicios = '{$_cs}');");
            $ROW[0]['solicitud'] = str_replace(' ', '', $ROW2[0]['solicitud']);

            if ($ROW2[0]['tipo'] == '1') {//FERTILIZANTES
                $ROW2 = $this->_QUERY("SELECT suministro, discre, num_sol, num_mue, muestras FROM COT001 WHERE (solicitud = '{$ROW[0]['solicitud']}');");
            } elseif ($ROW2[0]['tipo'] == '2') {//PLAGUICIDAS
                $ROW2 = $this->_QUERY("SELECT suministro, discre, num_sol, num_mue, muestras FROM COT003 WHERE (solicitud = '{$ROW[0]['solicitud']}');");
            }
            //
            $ROW[0]['suministro'] = $ROW2[0]['suministro'];
            $ROW[0]['discre'] = $ROW2[0]['discre'];
            $ROW[0]['num_sol'] = $ROW2[0]['num_sol'];
            $ROW[0]['num_mue'] = $ROW2[0]['num_mue'];
        }
        return $ROW;
    }

    function Solicitud03EncabezadoVacio()
    {
        return array(0 => array(
            'cs' => '',
            'fecha' => '',
            'proposito' => '',
            'estado' => '',
            'solicitud' => '',
            'tipo' => '',
            'dependencia' => '',
            'proposito' => 'Control de calidad',
            'cliente' => '',
            'tmp' => '',
            'direccion' => '',
            'obs' => '',
            'entregadas' => '',
            'producto' => '',
            'tipo_form' => '',
            'metodo' => '',
            'registro' => '',
            'dosis' => '',
            'mezcla' => '',
            'otro' => '',
            'densidad' => '',
            'muestras' => '1')
        );
    }

    function Solicitud03HistorialGet($_cs)
    {
        return $this->_QUERY("SELECT SERBI2.usuario, SERBI2.accion, SEG001.nombre + ' ' + SEG001.ap1 AS nombre, CONVERT(VARCHAR, SERBI2.fecha, 100) AS fecha1
		FROM SERBI2 INNER JOIN SEG001 ON SERBI2.usuario = SEG001.id 
		WHERE (SERBI2.solicitud = '{$_cs}') 
		ORDER BY SERBI2.fecha;");
    }

    function Solicitud03IME($_cs, $_accion, $_solicitud, $_tipo, $_proposito, $_dependencia, $_obs, $_cliente, $_direccion, $_entregadas, $_producto, $_tipo_form, $_metodo, $_registro, $_dosis, $_mezcla, $_densidad, $_UID)
    {
        $_solicitud = str_replace(' ', '', $this->Free($_solicitud));
        $_proposito = $this->Free($_proposito);
        $_obs = $this->Free($_obs);
        $_entregadas = $this->Free($_entregadas);
        $_producto = $this->Free($_producto);
        $_registro = $this->Free($_registro);
        $_dosis = $this->Free($_dosis);

        if ($this->_TRANS("EXECUTE PA_MAG_085 
			'{$_cs}',
			'{$_tipo}',
			'{$_solicitud}',
			'{$_proposito}',
			'{$_dependencia}',
			'{$_obs}',
			'{$_cliente}',
                        '{$_direccion}',
			'{$_entregadas}',
			'{$_producto}',
			'{$_tipo_form}',
			'{$_metodo}',
			
			'{$_registro}',
			'{$_dosis}',
			
			'{$_mezcla}',
			'{$_densidad}',
			'{$_UID}',
			'{$_accion}';")) {

            $this->NotificaJefes('3', '22', 'Se ha creado la solicitud ' . $_cs);
            return 1;
        } else
            return 0;
    }

    function Solicitud03Modifica($_solicitud, $_obs, $_estado, $_UID)
    {
        $_obs = $this->Free($_obs);

        $this->_TRANS("INSERT INTO SERBI2 VALUES('{$_solicitud}', '{$_UID}', '{$_estado}', GETDATE());");
        if ($_estado == '4') {
            $_estado = '0';
        } elseif ($_estado == '1') {
            //SI ES PROCESAMIENTO, AVISA AL JEFE QUE TIENE QUE APROBAR
            $this->NotificaJefes(3, 22, 'Tiene que aprobar la solicitud ' . $_solicitud);
        } elseif ($_estado == '2') {
            //SI ES APROBACION, TOMA LAS MUESTRAS DE LA SOLICITUD Y LAS PASA A LA TABLA DE MUESTRAS
            $this->_TRANS("EXECUTE PA_MAG_086 '{$_solicitud}', '{$_UID}';");
            $this->MuestraAsociaAnalisis03($_solicitud);
            //NOTIFICA JEFES PARA QUE ASIGNEN ANALISTAS
            $this->NotificaJefes(3, 26, 'Tiene muestras por asignar a los analistas, de la solicitud ' . $_solicitud); //el jefe que aprobo deberia saber que tiene que asignar
        } elseif ($_estado == '5') {
            //SI ES ANULACION, BORRA CUALQUIER MUESTRA DE LA TABLA DE MUESTRAS
            $this->_TRANS("EXECUTE PA_MAG_087 '{$_solicitud}';");
        } elseif ($_estado == '6') {
            //SI ES PRE-ANULACION, NOTIFICA JEFE
            $this->NotificaJefes(3, 24, 'Tiene una solicitud de anulaci�n de la solicitud ' . $_solicitud);
        }
        $this->_TRANS("UPDATE SER003 SET fecha = GETDATE(), obs = '{$_obs}', estado = '{$_estado}' WHERE (cs = '{$_solicitud}');");
        return 1;
    }

    function Solicitud03MuestrasGet($_cs)
    {
        return $this->_QUERY("SELECT cod_ext, recipiente, sellado, lote, rechazada, obse
		FROM SER006
		WHERE (solicitud = '{$_cs}')
		ORDER BY cod_ext;");
    }

    function Solicitud03MuestrasSet($_cs, $_cod_ext, $_recipiente, $_sellado, $_lote, $_rechazada, $_obse)
    {
        $this->_TRANS("DELETE FROM SER006 WHERE (solicitud = '{$_cs}');");

        $_cod_ext = $this->FreePost(str_replace('1=1&', '', $_cod_ext));
        $_cod_ext = explode('&', $_cod_ext);
        $_recipiente = str_replace('1=1&', '', $_recipiente);
        $_recipiente = explode('&', $_recipiente);
        $_sellado = str_replace('1=1&', '', $_sellado);
        $_sellado = explode('&', $_sellado);
        $_lote = $this->FreePost(str_replace('1=1&', '', $_lote));
        $_lote = explode('&', $_lote);
        $_rechazada = str_replace('1=1&', '', $_rechazada);
        $_rechazada = explode('&', $_rechazada);
        $_obse = $this->FreePost(str_replace('1=1&', '', $_obse));
        $_obse = explode('&', $_obse);

        for ($i = 0; $i < count($_cod_ext); $i++) {
            if (str_replace(' ', '', $_cod_ext[$i]) != '') {
                $this->_TRANS("INSERT INTO SER006 values('{$_cs}',
					'{$_cod_ext[$i]}',
					'{$_recipiente[$i]}',
					'{$_sellado[$i]}',
					'{$_lote[$i]}',
					'{$_rechazada[$i]}',
					'{$_obse[$i]}');");
            }
        }
    }

    function Solicitud03MuestrasVacio($_muestras)
    {
        $ROW = array();
        for ($x = 0; $x < $_muestras; $x++) {
            $ROW[$x]['cod_ext'] = '';
            $ROW[$x]['recipiente'] = '';
            $ROW[$x]['sellado'] = '';
            $ROW[$x]['lote'] = '';
            $ROW[$x]['rechazada'] = '';
            $ROW[$x]['obse'] = '';
        }
        return $ROW;
    }

    function ServiciosAccion($_var)
    {
        if ($_var == '0')
            return 'Generada';
        elseif ($_var == '1')
            return 'Procesada';
        elseif ($_var == '2')
            return '<strong>Aprobada</strong>';
        elseif ($_var == '3')
            return 'Validada';
        elseif ($_var == '4')
            return 'Rechazada';
        elseif ($_var == '5')
            return '<strong>Anulada</strong>';
        elseif ($_var == '6')
            return 'Solicitud de anulaci�n';
        else
            return 'N/A';
    }

    function SolicitudesConsecutivo($_tipo)
    {
        $ROW = $this->_QUERY("SELECT servicios0{$_tipo}_ AS cs FROM MAG000;");
        return $ROW[0]['cs'];
    }

// numero de informe
    function informesConsecutivo()
    {
        $ROWIA = $this->_QUERY("SELECT informeAsignado AS nia FROM MAG000;");
        return $ROWIA[0]['nia'];
    }


    function ServiciosEstado($_var)
    {
        if ($_var == '0')
            return 'Generada';
        elseif ($_var == '1')
            return 'Pend. Validaci�n';
        elseif ($_var == '2')
            return 'Aprobada';
        elseif ($_var == '3')
            return 'Pend. Aprobaci�n';
        elseif ($_var == '5')
            return 'Anulada';
        elseif ($_var == '6')
            return 'Pend. Anulaci�n';
        elseif ($_var == '7')
            return 'Finalizada';
        else
            return 'N/A';
    }

    function SolicitudesGet($_LAB, $_opcion, $_solicitud, $_desde, $_hasta, $_tipo, $_estado)
    {
        if ($_estado == '')
            $op2 = '<>';
        else
            $op2 = '=';
        if ($_tipo == '')
            $op = '<>';
        else
            $op = '=';

        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);

        if ($_LAB == '1') {
            return $this->_QUERY("SELECT SER001.cs, CONVERT(VARCHAR,SER001.fecha,105) AS fecha, SER001.estado, (SER001.flab +' '+ SER001.hlab) AS flab, MAN002.nombre 
			FROM SER001 INNER JOIN MAN002 ON SER001.cliente = MAN002.id
			WHERE (SER001.estado {$op2} '{$_estado}') AND (SER001.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00');");
        } else {
            if ($_opcion == '1')
                $where = "(SER003.tipo {$op} '{$_tipo}') AND (SER003.estado {$op2} '{$_estado}') AND (SER003.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')";
            else
                $where = "(SER003.cs = '{$_solicitud}')";

            return $this->_QUERY("SELECT SER003.cs, CONVERT(VARCHAR,SER003.fecha,105) AS fecha, SER003.estado, MAN002.nombre
			FROM SER003 INNER JOIN MAN002 ON SER003.cliente = MAN002.id
			WHERE {$where};");
        }
    }

    function SolicitudesPendientes($_tipo)
    {
        if ($_tipo == '1')
            return $this->_QUERY("SELECT SER001.cs, CONVERT(VARCHAR,SER001.fecha,105) as fecha, SER001.estado, (SER001.flab +' '+ SER001.hlab) AS flab, MAN002.nombre 
			FROM SER001 INNER JOIN MAN002 ON SER001.cliente = MAN002.id
			WHERE (SER001.estado = '0' OR SER001.estado = '1');");
        else
            return $this->_QUERY("SELECT SER003.cs, CONVERT(VARCHAR,SER003.fecha,105) AS fecha, SER003.estado, MAN002.nombre
			FROM SER003 INNER JOIN MAN002 ON SER003.cliente = MAN002.id
			WHERE (SER003.estado = '0' OR SER003.estado = '1' OR SER003.estado = '6');");
    }

    function ValidaInvalida($_ensayo, $_estado, $_UID)
    {
        return $this->_TRANS("UPDATE MUE002 SET cumple = {$_estado} WHERE (id='{$_ensayo}');");
    }

//
}

?>