<?php
/*******************************************************************
CLASE DE NIVEL 1 ENCARGADA DEL MODULO DE PERSONAL
(final SOLO PUEDE SER INSTANCIADA Y NO HEREDADA)
*******************************************************************/
final class Personal extends Database{	
	//METODOS GET
	function AutorizacionesGet($_id){
		return $this->_QUERY("SELECT id, formulario, CONVERT(VARCHAR, fecha, 105) AS fecha1, tipo
		FROM PER015 WHERE (persona =  {$_id}) ORDER BY fecha;");
	}
	
	function AutorizacionesDel($_id){
		if($this->_TRANS("DELETE FROM PER015 WHERE (id = {$_id});")){
			Bibliotecario::ZipEliminar("../../docs/personal/ind_{$_id}.zip");
			$this->Logger('0400');
			return 1;
		}else return 0;
	}
	
	function AutorizacionesCs(){
		$ROW = $this->_QUERY("SELECT inducciones FROM MAG000;");
		return $ROW[0]['inducciones'];
	}
	
	function AutorizacionesSet($_cs, $_persona, $_formulario, $_fecha, $_tipo){
		$_formulario = $this->FreePost($_formulario);
		$_fecha = $this->_FECHA($_fecha);
		
		if($this->_TRANS("INSERT INTO PER015 VALUES({$_cs}, {$_persona}, '{$_formulario}', '{$_fecha}', '{$_tipo}');")){
			$this->_TRANS("UPDATE MAG000 SET inducciones = inducciones + 1;");
			$this->Logger('0401');
			return 1;
		}else return 0;
	}
	
	function CitasResumenGet($_id, $_desde, $_hasta){
		if($_id == ''){
			$_id = -1;
			$op = '<>';
		}else $op = '=';
		
		$_desde = $this->_FECHA($_desde);
		$_hasta = $this->_FECHA($_hasta);
		
		return $this->_QUERY("SELECT SEG001.nombre, SEG001.ap1, SEG001.ap2, PER009.id, CONVERT(VARCHAR, PER009.fecha, 105) AS fecha1
		FROM PER009 INNER JOIN SEG001 ON PER009.persona = SEG001.id 
		WHERE (persona {$op} {$_id}) AND (fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00');");
	}
	
	function CitasDetalleGet($_id){		
		return $this->_QUERY("SELECT SEG001.id AS persona, SEG001.nombre, SEG001.ap1, SEG001.ap2, PER009.id, CONVERT(VARCHAR, PER009.fecha, 105) AS fecha, PER009.lugar, PER009.comprobante, CONVERT(TEXT, PER009.obs) AS obs
		FROM PER009 INNER JOIN SEG001 ON PER009.persona = SEG001.id 
		WHERE (PER009.id = '{$_id}');");
	}
	
	function CitasDetalleSet($_id, $_persona, $_fecha, $_lugar, $_comprobante, $_obs, $_accion){	
		$_comprobante = $this->FreePost($_comprobante);
		$_obs = $this->FreePost($_obs);
		$_lugar = $this->FreePost($_lugar);
                $_fecha=$this->_FECHA2($_fecha);
		if($this->_TRANS("EXECUTE PA_MAG_015 '{$_id}', '{$_persona}', '{$_fecha}', '{$_lugar}', '{$_comprobante}', '{$_obs}', '{$_accion}';")){
			//
			if($_accion=='I') $_accion = '0402';
			else $_accion = '0403';
			$this->Logger($_accion);
			//
			return 1;
		}else return 0;
	}
        
        function CitasDetalleUltimoId(){
            return $this->_QUERY("SELECT CONCAT(citas-1,'-',CAST(year(GETDATE()) AS VARCHAR)) as id FROM MAG000");
        }
	
	//METODOS QUE DEVUELVE VECTOR VACIO
	function CitasVacio(){
		return array(0=>array(
			'id'=>'',
			'persona'=>'-1',
			'nombre'=>'',
			'ap1'=>'',
			'ap2'=>'',
			'fecha'=>'',
			'lugar'=>'',
			'comprobante'=>'',
			'obs'=>''
		));
	}
	
	function SugerenciasDetalleGet($_id){		
		return $this->_QUERY("SELECT SEG001.nombre, SEG001.ap1, SEG001.ap2, PER017.id, CONVERT(VARCHAR, PER017.fecha, 105) AS fecha, PER017.obs, PER017.personal, PER017.materiales, PER017.equipos, PER017.presupuesto, PER017.otros, PER017.costo, PER017.beneficio, PER017.propuesta
		FROM PER017 INNER JOIN SEG001 ON PER017.persona = SEG001.id 
		WHERE (PER017.id = '{$_id}');");
	}
	
	function SugerenciasDetalleSet($_id, $_persona, $_obs, $_personal, $_materiales, $_equipos, $_presupuesto, $_otros, $_costo, $_beneficio, $_propuesta, $_accion){	
		$_obs = $this->FreePost($_obs);
		$_personal = $this->FreePost($_personal);
		$_materiales = $this->FreePost($_materiales);
		$_equipos = $this->FreePost($_equipos);
		$_presupuesto = $this->FreePost($_presupuesto);
		$_otros = $this->FreePost($_otros);
		$_costo = $this->FreePost($_costo);
		$_beneficio = $this->FreePost($_beneficio);
		$_propuesta = $this->FreePost($_propuesta);
		if($this->_TRANS("EXECUTE PA_MAG_141 '{$_id}', '{$_persona}', '{$_obs}', '{$_personal}', '{$_materiales}', '{$_equipos}', '{$_presupuesto}', '{$_otros}', '{$_costo}', '{$_beneficio}', '{$_propuesta}', '{$_accion}';")){
			/*
			if($_accion=='I') $_accion = '0402';
			else $_accion = '0403';
			$this->Logger($_accion);
			*/
			return 1;
		}else return 0;
	}
	
	function SugerenciasResumenGet($_id, $_desde, $_hasta){
		if($_id == ''){
			$_id = -1;
			$op = '<>';
		}else $op = '=';
		
		$_desde = $this->_FECHA($_desde);
		$_hasta = $this->_FECHA($_hasta);
		
		return $this->_QUERY("SELECT SEG001.nombre, SEG001.ap1, SEG001.ap2, PER017.id, CONVERT(VARCHAR, PER017.fecha, 105) AS fecha, PER017.obs
		FROM PER017 INNER JOIN SEG001 ON PER017.persona = SEG001.id 
		WHERE (PER017.persona {$op} {$_id}) AND (PER017.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00');");
	}
	
	function SugerenciasVacio(){
		return array(0=>array(
			'id'=>'',
			'nombre'=>'',
			'ap1'=>'',
			'ap2'=>'',
			'fecha'=>'',
			'obs'=>'',
			'personal'=>'',
			'materiales'=>'',
			'equipos'=>'',
			'presupuesto'=>'',
			'otros'=>'',
			'costo'=>'',
			'beneficio'=>'',
			'propuesta'=>''
		));
	}
	
	function InfoPersonalGet($_id){
		return $this->_QUERY("SELECT id, cedula, nombre, ap1, ap2, email FROM SEG001 WHERE (id={$_id});");
	}
	
	function InfoLaboralGet($_id){
		$ROW = $this->_QUERY("SELECT CONVERT(VARCHAR, actualizacion, 105) AS actualizacion, CONVERT(VARCHAR, fnacimiento, 105) AS fnacimiento, genero, nacionalidad, domicilio, tel_hab, tel_cel, 
		CONVERT(VARCHAR, fingreso, 105) AS fingreso, profesion, cargo, clase_puesto, num_puesto, colegio, num_colegiado
		FROM PER000 WHERE (id={$_id});");
		if(!$ROW) $ROW = $this->InfoLaboralVacio();
		return $ROW;
	}
	
	function InfoFormacionGet($_id){
		$ROW = $this->_QUERY("SELECT titulo, institucion, ano
		FROM PER001 WHERE (persona={$_id})
		ORDER BY linea;");
		if(!$ROW) $ROW = $this->InfoFormacionVacio();
		return $ROW;
	}
	
	function InfoExperienciaGet($_id){
		$ROW = $this->_QUERY("SELECT empresa, periodo
		FROM PER002 WHERE (persona={$_id})
		ORDER BY linea;");
		if(!$ROW) $ROW = $this->InfoExperienciaVacio();
		return $ROW;
	}
	
	function InfoIdiomasGet($_id){
		$ROW = $this->_QUERY("SELECT idioma, lee, habla, escribe
		FROM PER003 WHERE (persona={$_id})
		ORDER BY linea;");
		if(!$ROW) $ROW = $this->InfoIdiomasVacio();
		return $ROW;
	}
	
	function InfoPublicacionesGet($_id){
		$ROW = $this->_QUERY("SELECT publicacion, entidad, ano
		FROM PER004 WHERE (persona={$_id})
		ORDER BY linea;");
		if(!$ROW) $ROW = $this->InfoPublicacionesVacio();
		return $ROW;
	}
	
	function InfoAnalisisGet($_id){
		$ROW = $this->_QUERY("SELECT tecnica, empresa, periodo
		FROM PER005 WHERE (persona={$_id})
		ORDER BY linea;");
		if(!$ROW) $ROW = $this->InfoAnalisisVacio();
		return $ROW;
	}
	
	function InfoCalidadGet($_id){
		$ROW = $this->_QUERY("SELECT actividad, empresa, periodo
		FROM PER006 WHERE (persona={$_id})
		ORDER BY linea;");
		if(!$ROW) $ROW = $this->InfoCalidadVacio();
		return $ROW;
	}
	
	function InfoCursosGet($_id){
		$ROW = $this->_QUERY("SELECT tipo, nombre, empresa, duracion, CONVERT(VARCHAR, fecha, 105) AS fecha
		FROM PER007 WHERE (persona={$_id})
		ORDER BY fecha DESC;");
		if(!$ROW) $ROW = $this->InfoCursosVacio();
		return $ROW;
	}
	
	function InfoEquiposGet($_id){
		$ROW = $this->_QUERY("SELECT equipos, empresa, periodo
		FROM PER008 WHERE (persona={$_id})
		ORDER BY linea;");
		if(!$ROW) $ROW = $this->InfoEquiposVacio();
		return $ROW;
	}
	
	function InfoOtrosGet($_id){
		$ROW = $this->_QUERY("SELECT actividad, empresa, periodo
		FROM PER010 WHERE (persona={$_id})
		ORDER BY linea;");
		if(!$ROW) $ROW = $this->InfoOtrosVacio();
		return $ROW;
	}
	
	function InfoUpdate($_id){
		$this->_TRANS("UPDATE PER000 SET actualizar = 1 WHERE (id = {$_id});");
		//ELIMINA DE TABLON
		$this->TablonDel2(41, $_id, 41);
	}
	
	function InfoLaboralSet($_id, $_cedula, $_nombre, $_ap1, $_ap2, $_email, $_fnacimiento, $_genero, $_nacionalidad, $_domicilio, $_tel_hab, $_tel_cel, $_fingreso, $_profesion, $_cargo, $_clase_puesto, $_num_puesto, $_colegio, $_num_colegiado){
		$_cedula = $this->Free($_cedula);
		$_nombre = $this->Free($_nombre);
		$_ap1 = $this->Free($_ap1);
		$_ap2 = $this->Free($_ap2);
		$_email = $this->Free($_email);
		$_nacionalidad = $this->Free($_nacionalidad);
		$_domicilio = $this->Free($_domicilio);
		$_profesion = $this->Free($_profesion);
		$_cargo = $this->Free($_cargo);
		$_clase_puesto = $this->Free($_clase_puesto);
		$_num_puesto = $this->Free($_num_puesto);
		$_colegio = $this->Free($_colegio);
		$_num_colegiado = $this->Free($_num_colegiado);
		$_fnacimiento = $this->_FECHA($_fnacimiento);
		$_fingreso = $this->_FECHA($_fingreso);
		
		if($this->_TRANS("EXECUTE PA_MAG_006 '{$_id}', '{$_cedula}', '{$_nombre}', '{$_ap1}', '{$_ap2}', '{$_email}', '{$_fnacimiento}', '{$_genero}', '{$_nacionalidad}', '{$_domicilio}', '{$_tel_hab}', '{$_tel_cel}', '{$_fingreso}', '{$_profesion}', '{$_cargo}', '{$_clase_puesto}', '{$_num_puesto}', '{$_colegio}', '{$_num_colegiado}';")){
			return 1;
		}else return 0;
	}
	
	function InfoFormacionSet($_id, $_linea, $_titulo, $_institucion, $_ano){
		$_titulo = $this->Free($_titulo);
		$_institucion = $this->Free($_institucion);
		if($_ano != '' and $_titulo != '' and $_institucion != ''){
			if( !$this->_TRANS("EXECUTE PA_MAG_007 '{$_id}', '{$_linea}', '{$_titulo}', '{$_institucion}', '{$_ano}';") )
				return 0;
		}
		return 1;
	}
	
	function InfoExperienciaSet($_id, $_linea, $_empresa, $_periodo){
		$_periodo = $this->Free($_periodo);
		$_empresa = $this->Free($_empresa);
		if($_periodo != '' and $_empresa != ''){
			if( !$this->_TRANS("EXECUTE PA_MAG_008 '{$_id}', '{$_linea}', '{$_empresa}', '{$_periodo}';") )
				return 0;
		}
		return 1;
	}
	
	function InfoIdiomasSet($_id, $_linea, $_idioma, $_lee, $_habla, $_escribe){
		if($_idioma != ''){
			if( !$this->_TRANS("EXECUTE PA_MAG_009 '{$_id}', '{$_linea}', '{$_idioma}', '{$_lee}', '{$_habla}', '{$_escribe}';") )
				return 0;
		}
		return 1;
	}
	
	function InfoPublicacionesSet($_id, $_linea, $_publicacion, $_entidad, $_ano){
		$_publicacion = $this->Free($_publicacion);
		$_entidad = $this->Free($_entidad);
		if($_publicacion != '' and $_entidad != ''and $_ano != ''){
			if( !$this->_TRANS("EXECUTE PA_MAG_010 '{$_id}', '{$_linea}', '{$_publicacion}', '{$_entidad}', '{$_ano}';") )
				return 0;
		}
		return 1;
	}
	
	function InfoAnalisisSet($_id, $_linea, $_tecnica, $_empresa, $_periodo){
		$_tecnica = $this->Free($_tecnica);
		$_empresa = $this->Free($_empresa);
		if($_tecnica != '' and $_empresa != '' and $_periodo != ''){
			if( !$this->_TRANS("EXECUTE PA_MAG_011 '{$_id}', '{$_linea}', '{$_tecnica}', '{$_empresa}', '{$_periodo}';") )
				return 0;
		}
		return 1;
	}
	
	function InfoCalidadSet($_id, $_linea, $_actividad, $_empresa, $_periodo){
		$_actividad = $this->Free($_actividad);
		$_empresa = $this->Free($_empresa);
		$_periodo = $this->Free($_periodo);
		if($_actividad != '' and $_empresa != '' and $_periodo != ''){
			if( !$this->_TRANS("EXECUTE PA_MAG_012 '{$_id}', '{$_linea}', '{$_actividad}', '{$_empresa}', '{$_periodo}';") )
				return 0;
		}
		return 1;
	}
	
	function InfoCursosSet($_id, $_linea, $_tipo, $_nombre, $_empresa, $_duracion, $_fecha){
		$_tipo = $this->Free($_tipo);
		$_nombre = $this->Free($_nombre);
		$_empresa = $this->Free($_empresa);
		$_duracion = $this->Free($_duracion);
		$_fecha = $this->Free($_fecha);
		if($_tipo != '' and $_nombre != '' and $_empresa != '' and $_duracion != '' and $_fecha != ''){
			if( !$this->_TRANS("EXECUTE PA_MAG_013 '{$_id}', '{$_linea}', '{$_tipo}', '{$_nombre}', '{$_empresa}', '{$_duracion}', '{$_fecha}';") )
				return 0;
		}else{
			//BORRA EL ARCHIVO
			Bibliotecario::ZipEliminar("../../docs/personal/cur_{$_id}_{$_linea}.zip");
		}
		return 1;
	}
	
	function InfoEquiposSet($_id, $_linea, $_equipos, $_empresa, $_periodo){
		$_equipos = $this->Free($_equipos);
		$_empresa = $this->Free($_empresa);
		$_periodo = $this->Free($_periodo);
		if($_equipos != '' and $_empresa != '' and $_periodo != ''){
			if( !$this->_TRANS("EXECUTE PA_MAG_014 '{$_id}', '{$_linea}', '{$_equipos}', '{$_empresa}', '{$_periodo}';") )
				return 0;
		}
		return 1;
	}
	
	function InfoOtrosSet($_id, $_linea, $_actividad, $_empresa, $_periodo){
		$_actividad = $this->Free($_actividad);
		$_empresa = $this->Free($_empresa);
		$_periodo = $this->Free($_periodo);
		if($_actividad != '' and $_empresa != '' and $_periodo != ''){
			if( !$this->_TRANS("EXECUTE PA_MAG_041 '{$_id}', '{$_linea}', '{$_actividad}', '{$_empresa}', '{$_periodo}';") )
				return 0;
		}
		return 1;
	}
	//ELIMINA LOS REGISTROS ACTUALES ANTES DE ACTUALIZAR
	function InfoFormacionDel($_id){$this->_TRANS("DELETE FROM PER001 WHERE (persona = {$_id});");}
	function InfoExperienciaDel($_id){$this->_TRANS("DELETE FROM PER002 WHERE (persona = {$_id});");}
	function InfoIdiomasDel($_id){$this->_TRANS("DELETE FROM PER003 WHERE (persona = {$_id});");}
	function InfoPublicacionesDel($_id){$this->_TRANS("DELETE FROM PER004 WHERE (persona = {$_id});");}
	function InfoAnalisisDel($_id){$this->_TRANS("DELETE FROM PER005 WHERE (persona = {$_id});");}
	function InfoCalidadDel($_id){$this->_TRANS("DELETE FROM PER006 WHERE (persona = {$_id});");}
	function InfoCursosDel($_id){$this->_TRANS("DELETE FROM PER007 WHERE (persona = {$_id});");}
	function InfoEquiposDel($_id){$this->_TRANS("DELETE FROM PER008 WHERE (persona = {$_id});");}
	function InfoOtrosDel($_id){$this->_TRANS("DELETE FROM PER010 WHERE (persona = {$_id});");}
	
	function InfoLaboralVacio(){
		return array(0=>array(
			'actualizacion'=>'Pendiente',
			'fnacimiento'=>'',
			'genero'=>'',
			'nacionalidad'=>'',
			'domicilio'=>'',
			'tel_hab'=>'',
			'tel_cel'=>'',
			'fingreso'=>'',
			'profesion'=>'',
			'cargo'=>'',
			'clase_puesto'=>'',
			'num_puesto'=>'',
			'colegio'=>'',
			'num_colegiado'=>''
		));
	}
	
	function InfoFormacionVacio(){
		return array(0=>array(
			'titulo'=>'',
			'institucion'=>'',
			'ano'=>''
		));
	}
	
	function InfoExperienciaVacio(){
		return array(0=>array(
			'empresa'=>'',
			'periodo'=>''
		));
	}
	
	function InfoIdiomasVacio(){
		return array(0=>array(
			'idioma'=>'',
			'lee'=>'',
			'habla'=>'',
			'escribe'=>''
		));
	}
	
	function InfoPublicacionesVacio(){
		return array(0=>array(
			'publicacion'=>'',
			'entidad'=>'',
			'ano'=>''
		));
	}
	
	function InfoAnalisisVacio(){
		return array(0=>array(
			'tecnica'=>'',
			'empresa'=>'',
			'periodo'=>''
		));
	}
	
	function InfoCalidadVacio(){
		return array(0=>array(
			'actividad'=>'',
			'empresa'=>'',
			'periodo'=>''
		));
	}
	
	function InfoCursosVacio(){
		return array(0=>array(
			'tipo'=>'',
			'nombre'=>'',
			'empresa'=>'',
			'duracion'=>'',
			'fecha'=>''
		));
	}
	
	function InfoEquiposVacio(){
		return array(0=>array(
			'equipos'=>'',
			'empresa'=>'',
			'periodo'=>''
		));
	}
	
	function InfoOtrosVacio(){
		return array(0=>array(
			'actividad'=>'',
			'empresa'=>'',
			'periodo'=>''
		));
	}
}
?>