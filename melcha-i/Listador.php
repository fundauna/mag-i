<?php
/*******************************************************************
CLASE DE NIVEL 1 ENCARGADA DEL MOSTRAR LISTAS DESPLEGABLES
(final SOLO PUEDE SER INSTANCIADA Y NO HEREDADA)
*******************************************************************/
final class Listador extends Mensajero{
//
	final public function AlicuotasLista($_modulo, $_linea='', $_lid, $_js=''){
		$this->TableHeader('Al�cuotas disponibles', 2, $_modulo, $_js);
		
		$Robot = new Biologia();
		$ROW = $Robot->AlicuotasDisponibles();
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="AlicuotaEscoge('','<?=$_linea?>')">
			<td align="center" colspan="2">Limpiar</td>
		</tr>
		<tr>
			<th width="10%">C&oacute;digo</th>
			<th width="20%">Nombre</th>
			
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
			$ROW[$x]['id'] = str_replace(' ', '', $ROW[$x]['id']);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="AlicuotaEscoge('<?=$ROW[$x]['id']?>','<?=$_linea?>')">
			<td align="center"><?=$ROW[$x]['id']?></td>
			<td align="center"><?=$ROW[$x]['nombre']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function AnalistasDisponibles($_modulo, $_js='', $_linea='', $_lab, $_muestra, $_campo, $_campo2){
		$this->TableHeader('Analistas disponibles', 2, $_modulo, $_js);
		
		$Robot = new Varios();
		$ROW = $Robot->UsuariosXMetodo($_lab, $_muestra);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="UsuarioEscoge('','', '<?=$_linea?>', '<?=$_campo?>', '<?=$_campo2?>')">
			<td align="center" colspan="2">Limpiar</td>
		</tr>
		<tr>
			<th width="10%">C&eacute;dula</th>
			<th width="20%">Nombre</th>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="UsuarioEscoge('<?=$ROW[$x]['id']?>','<?=$ROW[$x]['nombre']?>', '<?=$_linea?>', '<?=$_campo?>', '<?=$_campo2?>')">
			<td align="center"><?=$ROW[$x]['cedula']?></td>
			<td><?=$ROW[$x]['nombre']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function AnalisisForLista($_modulo, $_lid, $_tipo, $_linea, $_js=''){
		$this->TableHeader('Sub-an�lisis disponibles', 1, $_modulo, $_js);
		
		$Robot = new Varios();
		$ROW = $Robot->AnalisisForMuestra($_tipo);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="AnalisisEscoge('<?=$_linea?>', '','', '0.00')">
			<td align="center">Limpiar</td>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="AnalisisEscoge('<?=$_linea?>', '<?=$ROW[$x]['analisis']?>','<?=$ROW[$x]['nombre']?>', '')">
			<td><?=$ROW[$x]['nombre']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function AnalisisLista($_modulo, $_lid, $_linea, $_js=''){
		$this->TableHeader('Analisis registrados', 3, $_modulo, $_js);
		
		$Robot = new Mantenimientos();
		$ROW = $Robot->AnalisisIntMuestra($_lid, true);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="AnalisisEscoge('<?=$_linea?>', '', '', '0.00')">
			<td align="center" colspan="3">Limpiar</td>
		</tr>
		<tr>
			<th width="5%">Tarifa</th>
			<th width="20%">Nombre</th>
			<th width="10%">Costo</th>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
			$ROW[$x]['costo'] = $this->formato($ROW[$x]['costo']);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="AnalisisEscoge('<?=$_linea?>', '<?=$ROW[$x]['id']?>','<?=$ROW[$x]['nombre']?>', '<?=$ROW[$x]['costo']?>')">
			<td><?=$ROW[$x]['tarifa']?></td>
			<td><?=$ROW[$x]['nombre']?></td>
			<td align="right"><?=$ROW[$x]['costo']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function AnalisisListaLDP($_modulo, $_lid, $_tipo, $_linea, $_js=''){
		$this->TableHeader('Analisis registrados', 3, $_modulo, $_js);
		
		$Robot = new Mantenimientos();
		$ROW = $Robot->AnalisisIntMuestraLDP(2, $_tipo, true);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="AnalisisEscoge('<?=$_linea?>', '',  '', '', '', '0.00')">
			<td align="center" colspan="3">Limpiar</td>
		</tr>
		<tr>
			<th width="5%">Tarifa</th>
			<th width="20%">Nombre</th>
			<th width="10%">Costo</th>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
			$ROW[$x]['costo'] = $this->formato($ROW[$x]['costo']);
			//TIPO DE TARIFA PARA SABER SI REQUIERE APERTURA (codigos 20) U OBSERVACIONES EXTRA (codigos 21)
			$ROW[$x]['tarifa'] = str_replace('', '', $ROW[$x]['tarifa']);
			$ROW[$x]['codigo'] = $ROW[$x]['tarifa'];
			if($ROW[$x]['tarifa']=='21') $ROW[$x]['tarifa'] = '2';
			else{
				$ROW[$x]['tarifa'] = substr($ROW[$x]['tarifa'], 0, 2);
				if($ROW[$x]['tarifa']=='20') $ROW[$x]['tarifa'] = '1';
				else $ROW[$x]['tarifa'] = '0';
			}
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="AnalisisEscoge('<?=$_linea?>', '<?=$ROW[$x]['codigo']?>', '<?=$ROW[$x]['id']?>', '<?=$ROW[$x]['nombre']?>', '<?=$ROW[$x]['tarifa']?>', '<?=$ROW[$x]['costo']?>')">
			<td><?=$ROW[$x]['codigo']?></td>
			<td><?=$ROW[$x]['nombre']?></td>
			<td align="right"><?=$ROW[$x]['costo']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function AnalisisListaTarifa($_modulo, $_lid, $_linea, $_js=''){
		$this->TableHeader('Analisis registrados', 4, $_modulo, $_js);
		
		$Robot = new Mantenimientos();
		$ROW = $Robot->AnalisisIntMuestra($_lid, true);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="AnalisisEscogeTarifa('<?=$_linea?>', '','','','0.00','0')">
			<td align="center" colspan="4">Limpiar</td>
		</tr>
		<tr align="center">
			<th width="5%">Item</th>
			<th width="20%">Nombre</th>
			<th width="10%">Costo</th>
			<th width="5%">Descuento?</th>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
			$ROW[$x]['costo'] = $this->formato($ROW[$x]['costo']);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="AnalisisEscogeTarifa('<?=$_linea?>', '<?=$ROW[$x]['id']?>', '<?=$ROW[$x]['tarifa']?>', '<?=$ROW[$x]['nombre']?>', '<?=$ROW[$x]['costo']?>', '<?=$ROW[$x]['descuento']?>')">
			<td align="center"><?=$ROW[$x]['tarifa']?></td>
			<td align="center"><?=$ROW[$x]['nombre']?></td>
			<td align="right"><?=$ROW[$x]['costo']?></td>
			<td align="center"><?=$ROW[$x]['descuento']=='1' ? '&radic;' : ''?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function BandejasLista($_modulo, $_lid, $_js=''){
		$this->TableHeader('Bandejas registrados', 2, $_modulo, $_js);
		
		$Robot = new Biologia();
		$ROW = $Robot->BandejasResumenGet($_lid);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="CeldaLimpia()">
			<td align="center" colspan="2">Limpiar</td>
		</tr>
		<tr>
			<th width="10%">Id.</th>
			<th width="20%">Dimensiones</th>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="CeldasLista('<?=$ROW[$x]['id']?>')">
			<td align="center"><?=$ROW[$x]['id']?></td>
			<td align="center"><?=$ROW[$x]['dimx'],'x',$ROW[$x]['dimy']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function CamarasLista($_modulo, $_linea='', $_lid, $_js=''){
		$this->TableHeader('Equipos registradas', 4, $_modulo, $_js);
		
		$Variator = new Varios();
		$ROW = $Variator->CamarasResumenGet($_lid, true);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="CamaraEscoge('', '', '', '');">
			<td align="center" colspan="5">Limpiar</td>
		</tr>
		<tr>
			<td width="10%"><strong>C&oacute;digo</strong></td>
			<td width="10%"><strong>Tipo</strong></td>
			<td width="10%"><strong>L&iacute;mites �C</strong></td>
			<td width="10%"><strong>Ubicaci&oacute;n</strong></td>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
			$ROW[$x]['limite1'] = $ROW[$x]['limite1'].' &deg;C a '.$ROW[$x]['limite2'].' &deg;C';
			if($ROW[$x]['tipo'] == 0) $ROW[$x]['tipo'] = 'Refrigeradora'; else $ROW[$x]['tipo'] = 'C�mara';
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="CamaraEscoge('<?=$ROW[$x]['codigo']?>', '<?=$ROW[$x]['tipo']?>', '<?=$ROW[$x]['limite1']?>', '<?=$ROW[$x]['ubicacion']?>');">
			<td><?=$ROW[$x]['codigo']?></td>
			<td><?=$ROW[$x]['tipo']?></td>
			<td><?=$ROW[$x]['limite1']?></td>
			<td><?=$ROW[$x]['ubicacion']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function ClientesLista($_modulo, $_lid, $_js=''){
		$this->TableHeader('Clientes registrados', 2, $_modulo, $_js);
		
		$Robot = new Sc();
		$ROW = $Robot->ClientesResumenGet('','',1,$_lid,1,true);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ClienteEscoge('','','')">
			<td align="center" colspan="2">Limpiar</td>
		</tr>
		<tr>
			<th width="10%">Id.</th>
			<th width="20%">Nombre</th>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ClienteEscoge('<?=$ROW[$x]['id']?>','<?=$ROW[$x]['nombre']?>','<?=$ROW[$x]['direccion']?>')">
			<td align="center"><?=$ROW[$x]['id']?></td>
			<td align="center"><?=$ROW[$x]['nombre']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function ColumnasLista($_modulo, $_lid, $_js='', $l=''){
		$this->TableHeader('Columnas registradas', 3, $_modulo, $_js);
		
		$Robot = new Equipos();
		$ROW = $Robot->EquiposColumnasGet($_lid, $l);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ColumnasEscoge('','','','','')">
			<td align="center" colspan="3">Limpiar</td>
		</tr>
		<tr>
			<th width="10%">Id.</th>
			<th width="10%">Nombre</th>
            <th width="20%">Marca</th>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ColumnasEscoge('<?=$ROW[$x]['id']?>', '<?=$ROW[$x]['codigo']?>','<?=$ROW[$x]['nombre']?>','<?=$ROW[$x]['marca']?>','<?=$ROW[$x]['dimensiones']?>')">
			<td align="center"><?=$ROW[$x]['codigo']?></td>
			<td align="center"><?=$ROW[$x]['nombre']?></td>
            <td align="center"><?=$ROW[$x]['marca']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function SolicitanteLista($_modulo, $_lid, $_cliente, $_js=''){
		$this->TableHeader('Clientes registrados', 2, $_modulo, $_js);
		
		$Robot = new Sc();
		$ROW = $Robot->SolicitanteResumenGet('','',1,$_cliente,1,true);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="SolicitanteEscoge('','')">
			<td align="center" colspan="2">Limpiar</td>
		</tr>
		<tr>
			<th width="20%">Nombre</th>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="SolicitanteEscoge('<?=$ROW[$x]['contacto']?>')">
			<td align="center"><?=$ROW[$x]['contacto']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function ElementosLista($_modulo, $_lid, $_tipo, $_linea, $_js=''){
		$this->TableHeader('Datos disponibles', 1, $_modulo, $_js);
		
		$Robot = new Varios();
		$ROW = $Robot->SubAnalisisGet($_tipo, $_lid);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ElementosEscoge('<?=$_linea?>', '', '')">
			<td align="center">Limpiar</td>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ElementosEscoge('<?=$_linea?>', '<?=$ROW[$x]['id']?>', '<?=$ROW[$x]['nombre']?>')">
			<td><?=$ROW[$x]['nombre']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function EquiposBalanzas($_modulo, $_js='', $_lid=''){
		$this->TableHeader('Equipos registrados', 3, $_modulo, $_js);
		
		$Equipor = new Equipos();
		$ROW = $Equipor->EquiposBalanzas($_lid);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="EquipoEscoge('','')">
			<td align="center" colspan="3">Limpiar</td>
		</tr>
		<tr>
			<td width="10%"><strong>C&oacute;digo</strong></td>
			<td width="20%"><strong>Nombre</strong></td>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
			$ROW[$x]['codigo'] = str_replace(' ','',$ROW[$x]['codigo']);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="EquipoEscoge('<?=$ROW[$x]['id']?>','<?=$ROW[$x]['codigo']?>','<?=$ROW[$x]['nombre']?>')">
			<td><?=$ROW[$x]['codigo']?></td>
			<td><?=$ROW[$x]['nombre']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function EquiposMicropipetas($_modulo, $_js=''){
		$this->TableHeader('Equipos registrados', 3, $_modulo, $_js);
		
		$Equipor = new Equipos();
		$ROW = $Equipor->EquiposMicropipetas();
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="EquipoEscoge('','')">
			<td align="center" colspan="3">Limpiar</td>
		</tr>
		<tr>
			<td width="10%"><strong>C&oacute;digo</strong></td>
			<td width="20%"><strong>Nombre</strong></td>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
			$ROW[$x]['codigo'] = str_replace(' ','',$ROW[$x]['codigo']);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="EquipoEscoge('<?=$ROW[$x]['id']?>','<?=$ROW[$x]['codigo']?>','<?=$ROW[$x]['nombre']?>')">
			<td><?=$ROW[$x]['codigo']?></td>
			<td><?=$ROW[$x]['nombre']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function EquiposCalibrajeLista($_modulo, $_linea='', $_lid, $_js='', $lolo=''){
		$this->TableHeader('Equipos registrados', 3, $_modulo, $_js);
		
		$Equipor = new Equipos();
		$ROW = $Equipor->EquiposCalibrajeGet($_lid, $lolo);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="EquipoEscoge('','','<?=$_linea?>')">
			<td align="center" colspan="3">Limpiar</td>
		</tr>
		<tr>
			<td width="10%"><strong>C&oacute;digo</strong></td>
			<td width="20%"><strong>Nombre</strong></td>
			<td width="10%"><strong>Estado</strong></td>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
			$ROW[$x]['id'] = str_replace(' ','',$ROW[$x]['id']);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="EquipoEscoge('<?=$ROW[$x]['id']?>','<?=$ROW[$x]['codigo']?>','<?=$_linea?>')">
			<td><?=$ROW[$x]['codigo']?></td>
			<td><?=$ROW[$x]['nombre']?></td>
			<td title="<?=$ROW[$x]['justi']?>" <?=($ROW[$x]['estado'] == 'Fuera de uso' ? 'style="color:#FF0000"':'')?>><?=$ROW[$x]['estado']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function EquiposCalibrajeLista2($_modulo, $_linea='', $_lid, $_js='', $lolo=''){
		$this->TableHeader('Equipos registrados', 3, $_modulo, $_js);
		
		$Equipor = new Equipos();
		$ROW = $Equipor->EquiposCalibrajeGet($_lid, $lolo);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="EquipoEscoge('','','','')">
			<td align="center" colspan="3">Limpiar</td>
		</tr>
		<tr>
			<td width="10%"><strong>C&oacute;digo</strong></td>
			<td width="20%"><strong>Nombre</strong></td>
			<td width="10%"><strong>Estado</strong></td>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
			$ROW[$x]['id'] = str_replace(' ','',$ROW[$x]['id']);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="EquipoEscoge('<?=$ROW[$x]['id']?>','<?=$ROW[$x]['codigo']?>','<?=$ROW[$x]['marca']?>','<?=$ROW[$x]['modelo']?>')">
			<td><?=$ROW[$x]['codigo']?></td>
			<td><?=$ROW[$x]['nombre']?></td>
			<td title="<?=$ROW[$x]['justi']?>" <?=($ROW[$x]['estado'] == 'Fuera de uso' ? 'style="color:#FF0000"':'')?>><?=$ROW[$x]['estado']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function EquiposColumnasLista($_modulo, $_linea='', $_lid, $_js=''){
		$this->TableHeader('Columnas registradas', 3, $_modulo, $_js);
		
		$Equipor = new Equipos();
		$ROW = $Equipor->EquiposColumnasGet($_lid);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ColumnaEscoge('','','<?=$_linea?>')">
			<td align="center" colspan="3">Limpiar</td>
		</tr>
		<tr>
			<td width="10%"><strong>C&oacute;digo</strong></td>
			<td width="20%"><strong>Nombre</strong></td>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
			$ROW[$x]['id'] = str_replace(' ','',$ROW[$x]['id']);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ColumnaEscoge('<?=$ROW[$x]['id']?>','<?=$ROW[$x]['codigo']?>','<?=$_linea?>')">
			<td><?=$ROW[$x]['codigo']?></td>
			<td><?=$ROW[$x]['nombre']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function EquiposDisponibles($_modulo, $_linea='', $_UID, $_lid, $_js=''){
		$this->TableHeader('Equipos registrados', 3, $_modulo, $_js);
		
		$Equipor = new Equipos();
		$ROW = $Equipor->EquiposDisponiblesGet($_lid, $_UID);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="DisponibleEscoge('','','','<?=$_linea?>')">
			<td align="center" colspan="3">Limpiar</td>
		</tr>
		<tr>
			<td width="10%"><strong>C&oacute;digo</strong></td>
			<td width="20%"><strong>Nombre</strong></td>
			<td width="10%"><strong>Estado</strong></td>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
			$ROW[$x]['id'] = str_replace(' ','',$ROW[$x]['id']);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="DisponibleEscoge('<?=$ROW[$x]['id']?>','<?=$ROW[$x]['codigo']?>','<?=$ROW[$x]['nombre']?>','<?=$_linea?>','<?=$ROW[$x]['marca']?>','<?=$ROW[$x]['modelo']?>')">
			<td><?=$ROW[$x]['codigo']?></td>
			<td><?=$ROW[$x]['nombre']?></td>
			<td title="<?=$ROW[$x]['justi']?>" <?=($ROW[$x]['estado'] == 'Fuera de uso' ? 'style="color:#FF0000"':'')?>><?=$ROW[$x]['estado']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function EquiposLista($_modulo, $_linea='', $_lid, $_js='', $_tk = '0'){
		$this->TableHeader('Equipos registrados', 4, $_modulo, $_js);
		
		$Equipor = new Equipos();
		$ROW = $Equipor->EquiposResumenGet('', '', $_lid);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="EquipoEscoge('','','<?=$_linea?>')">
			<td align="center" colspan="4">Limpiar</td>
		</tr>
		<tr>
                        <?php if($_tk==1){?><td width="10%"></td><?php }?>
                        <td width="10%"><strong>C&oacute;digo</strong></td>
			<td width="20%"><strong>Nombre</strong></td>
			<td width="10%"><strong>Estado</strong></td>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
			$ROW[$x]['id'] = str_replace(' ','',$ROW[$x]['id']);
		?>
                <?php if($_tk==1){?>
                <tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer">
                    <td><input type="checkbox" id="ck<?=$x?>" onchange="EquipoEscoge('<?=$ROW[$x]['id']?>','<?=$ROW[$x]['codigo']?>','<?=$_linea?>', '<?=$x?>')"></td>
                <?php }else{?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="EquipoEscoge('<?=$ROW[$x]['id']?>','<?=$ROW[$x]['codigo']?>','<?=$_linea?>')">
                <?php }?>    
			<td><?=$ROW[$x]['codigo']?></td>
			<td><?=$ROW[$x]['nombre']?></td>
			<td <?=($ROW[$x]['estado'] == 'Deshabilitado' ? 'style="color:#FF0000"':'')?>><?=$ROW[$x]['estado']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function EstandarLista($_modulo, $_lid, $_js=''){
		$this->TableHeader('Estandar registrados', 2, $_modulo, $_js);
		
		$Robot = new Equipos();
		$ROW = $Robot->EquiposEstandarGet($_lid);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="EstandarEscoge('','')">
			<td align="center" colspan="2">Limpiar</td>
		</tr>
		<tr>
			<th width="10%">Id.</th>
			<th width="20%">Nombre</th>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="EstandarEscoge('<?=$ROW[$x]['id']?>', '<?=$ROW[$x]['codigo']?>','<?=$ROW[$x]['nombre']?>')">
			<td align="center"><?=$ROW[$x]['codigo']?></td>
			<td align="center"><?=$ROW[$x]['nombre']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	private function Formato($_NUMBER){
		return number_format($_NUMBER, 2, '.', ',');
	}
	
	final public function IALista($_modulo, $_lid, $_formulacion, $_linea, $_js=''){
	//LISTADO DE INGREDIENTES ACTIVOS FILTRADOS POR FORMULACION (USADO SOLO CUANDO SE HACEN SOLICITUDES DE COTIZACION Y SERVICIOS)
		$this->TableHeader('I.A. disponibles', 1, $_modulo, $_js);
		
		$Robot = new Varios();
		$ROW = $Robot->SubAnalisisGet(3/*I.A.*/, $_lid, $_formulacion);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ElementosEscoge('<?=$_linea?>', '', '')">
			<td align="center">Limpiar</td>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ElementosEscoge('<?=$_linea?>', '<?=$ROW[$x]['id']?>', '<?=$ROW[$x]['nombre']?>')">
			<td><?=$ROW[$x]['nombre']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function IALista2($_modulo, $_lid, $_formulacion, $_linea, $_js=''){
	//LISTADO DE INGREDIENTES ACTIVOS FILTRADOS POR FORMULACION (USADO SOLO CUANDO SE HACEN SOLICITUDES DE COTIZACION Y SERVICIOS)
		$this->TableHeader('I.A. disponibles', 1, $_modulo, $_js);
		
		$Robot = new Varios();
		$ROW = $Robot->SubAnalisisGet(3/*I.A.*/, $_lid, $_formulacion);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ElementosEscoge('<?=$_linea?>', '', '')">
			<td align="center">Limpiar</td>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
			$n = $Robot->EstN($ROW[$x]['id']);
			$m = $Robot->EstM($ROW[$x]['id']);
			if($n != '' || $m != ''){
				if($n != '') $color = '#CC0';
				if($m != '') $color = '#C00';
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer">
			<td style="color:#FFF; background-color:<?=$color?>;"><?=$ROW[$x]['nombre']?><?=$n?><?=$m?></td>
		</tr>
		<?php
			}else{
		?>		
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ElementosEscoge('<?=$_linea?>', '<?=$ROW[$x]['id']?>', '<?=$ROW[$x]['nombre']?>')">
			<td><?=$ROW[$x]['nombre']?></td>
		</tr>
        <?php
			}
		}//FOR
		$this->TableFooter();
	}
	
	final public function ImpurezasLista($_modulo, $_lid, $_tipo, $_linea, $_js=''){
		$this->TableHeader('Impurezas disponibles', 1, $_modulo, $_js);
		
		$Robot = new Varios();
		$ROW = $Robot->SubAnalisisGet($_tipo, $_lid);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ImpurezasEscoge('<?=$_linea?>', '','')">
			<td align="center">Limpiar</td>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ImpurezasEscoge('<?=$_linea?>', '<?=$ROW[$x]['id']?>','<?=$ROW[$x]['nombre']?>')">
			<td><?=$ROW[$x]['nombre']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function IMPLista($_modulo, $_lid, $_formulacion, $_linea, $_js=''){
	//LISTADO DE IMPUREZAS FILTRADOS POR FORMULACION (USADO SOLO CUANDO SE HACEN SOLICITUDES DE COTIZACION Y SERVICIOS)
		$this->TableHeader('Impurezas disponibles', 1, $_modulo, $_js);
		
		$Robot = new Varios();
		$ROW = $Robot->SubAnalisisGet(0/*I.A.*/, $_lid, $_formulacion);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ImpurezasEscoge('<?=$_linea?>', '', '')">
			<td align="center">Limpiar</td>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ImpurezasEscoge('<?=$_linea?>', '<?=$ROW[$x]['id']?>', '<?=$ROW[$x]['nombre']?>')">
			<td><?=$ROW[$x]['nombre']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function InventarioBuscaTipo($_modulo, $_linea='', $_tipo, $_lid, $_js=''){
		$this->TableHeader($_tipo, 4, $_modulo, $_js);
		
		$Inventor = new Inventario();
		$ROW = $Inventor->InventarioBuscaTipoDesc($_tipo, $_lid);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="InventarioEscoge('','','','','<?=$_linea?>')">
			<td align="center" colspan="4">Limpiar</td>
		</tr>
		<tr>
			<td width="10%"><strong>C&oacute;digo</strong></td>
			<td width="20%"><strong>Nombre</strong></td>
			<td width="5%"><strong>Stock</strong></td>
			<td width="10%"><strong>Estado</strong></td>
		</tr>
		<?php
		$_tipo = str_replace('�','a',$_tipo);
		for($x=0;$x<count($ROW);$x++){
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="InventarioEscoge('<?=$_tipo?>','<?=$ROW[$x]['id']?>','<?=$ROW[$x]['codigo']?>','<?=$ROW[$x]['nombre']?>','<?=$_linea?>')">
			<td><?=$ROW[$x]['codigo']?></td>
			<td><?=$ROW[$x]['nombre']?></td>
			<td><?=$ROW[$x]['stock']?></td>
			<td <?=$ROW[$x]['estado']?>><?=$ROW[$x]['estado']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function InventarioCatalogo($_modulo, $_linea='', $_lid, $_js=''){
		$this->TableHeader('Inventario (Cat�logo)', 2, $_modulo, $_js);
		
		$Robot = new Varios();
		$ROW = $Robot->CatalogoGet($_lid);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="CatalogoEscoge('','','<?=$_linea?>')">
			<td align="center" colspan="2">Limpiar</td>
		</tr>
		<tr>
			<th width="10%">C&oacute;digo</th>
			<th width="20%">Nombre</th>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="CatalogoEscoge('<?=$ROW[$x]['id']?>','<?=$ROW[$x]['nombre']?>','<?=$_linea?>')">
			<td align="center"><?=$ROW[$x]['codigo']?></td>
			<td align="center"><?=$ROW[$x]['nombre']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function InventarioEntradas($_modulo, $_linea='', $_padre='', $_lid, $_js=''){
		$this->TableHeader('Inventario (Entradas)', 4, $_modulo, $_js);
		
		$Robot = new Varios();
		$ROW = $Robot->InventarioEntradasGet($_lid, $_padre);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="CatalogoEscoge('','','<?=$_linea?>')">
			<td align="center" colspan="4">Limpiar</td>
		</tr>
		<tr>
			<th width="10%">C&oacute;digo</th>
			<th width="20%">Nombre</th>
			<th width="10%">Lote</th>
			<th width="10%">Stock</th>
			
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="CatalogoEscoge('<?=$ROW[$x]['id']?>','<?=$ROW[$x]['codigo']?>','<?=$_linea?>')">
			<td align="center"><?=$ROW[$x]['codigo']?></td>
			<td align="center"><?=$ROW[$x]['nombre'], ' (', $ROW[$x]['unidad'], ')'?></td>
			<td align="center"><?=$ROW[$x]['lote']?></td>
			<td align="center"><?=$ROW[$x]['stock1']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function MatrizLista($_modulo, $_lid, $_linea, $_js=''){
		$this->TableHeader('Matrices registradas', 3, $_modulo, $_js);
		
		$Robot = new Varios();
		$ROW = $Robot->MatrizGet();
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="MatrizEscoge('','','<?=$_linea?>')">
			<td align="center" colspan="3">Limpiar</td>
		</tr>
		<tr>
			<th width="10%">Id.</th>
			<th width="20%">Nombre</th>
            <th width="10%">Acreditado</th>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="MatrizEscoge('<?=$ROW[$x]['id']?>','<?=$ROW[$x]['nombre']?>','<?=$_linea?>')">
			<td align="center"><?=$ROW[$x]['id']?></td>
			<td align="center"><?=$ROW[$x]['nombre']?></td>
            <td align="center"><?=$ROW[$x]['acreditado']=='1' ? 'S�' : 'No'?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function MacroAnalisisLista($_modulo, $_lid, $_tipo, $_linea, $_js=''){
		$this->TableHeader('Sub-an�lisis disponibles', 1, $_modulo, $_js);
		
		$Robot = new Varios();
		$ROW = $Robot->SubAnalisisGet($_tipo, $_lid);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="AnalisisEscoge('<?=$_linea?>', '','', '0.00')">
			<td align="center">Limpiar</td>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="AnalisisEscoge('<?=$_linea?>', '<?=$ROW[$x]['id']?>','<?=$ROW[$x]['nombre']?>', '')">
			<td><?=$ROW[$x]['nombre']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function PesasLista($_modulo, $_linea='', $_lid, $_js=''){
		$this->TableHeader('Masas registradas', 5, $_modulo, $_js);
		
		$Variator = new Varios();
		$ROW = $Variator->MasasResumenGet($_lid, true);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="PesaEscoge('','','','<?=$_linea?>')">
			<td align="center" colspan="5">Limpiar</td>
		</tr>
		<tr>
			<td width="10%"><strong>C&oacute;digo</strong></td>
			<td width="5%"><strong>Valor nominal</strong></td>
			<td width="5%"><strong>Valor real</strong></td>
			<td width="5%"><strong>Incertidumbre</strong></td>
			<td width="10%"><strong>Certificado</strong></td>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
			$ROW[$x]['nominal'] = str_replace(' ','', $ROW[$x]['nominal']);
			$ROW[$x]['vreal'] = str_replace(' ','', $ROW[$x]['vreal']);
			$ROW[$x]['inc'] = str_replace(' ','', $ROW[$x]['inc']);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="PesaEscoge('<?=$ROW[$x]['codigo']?>','<?=$ROW[$x]['nominal']?>', '<?=$ROW[$x]['vreal']?>', '<?=$ROW[$x]['inc']?>', '<?=$ROW[$x]['certificado']?>','<?=$_linea?>')">
			<td><?=$ROW[$x]['codigo']?></td>
			<td><?=$ROW[$x]['nominal']?></td>
			<td><?=$ROW[$x]['vreal']?></td>
			<td><?=$ROW[$x]['inc']?></td>
			<td><?=$ROW[$x]['certificado']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function ProcedimientosLista($_modulo, $_js=''){
		$this->TableHeader('Procedimientos registrados', 2, $_modulo, $_js);
		
		$Robot = new Biologia();
		$ROW = $Robot->ProcResumenGet('','');
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ProcsEscoge('')">
			<td align="center" colspan="2">Limpiar</td>
		</tr>
		<tr>
			<th width="10%">C&oacute;digo</th>
			<th width="20%">Nombre</th>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
			$ROW[$x]['cs'] = str_replace('', '', $ROW[$x]['cs']);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ProcsEscoge('<?=$ROW[$x]['cs']?>')">
			<td align="center"><?=$ROW[$x]['cs']?></td>
			<td align="center"><?=$ROW[$x]['nombre']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function ProveedoresLista($_modulo, $_lid, $_js=''){
		$this->TableHeader('Proveedores registrados', 3, $_modulo, $_js);
		
		$Robot = new Proveedores();
		$ROW = $Robot->ProveedoresResumenGet('','',1,'',$_lid,2,true);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ProveedorEscoge('','','')">
			<td align="center" colspan="3">Limpiar</td>
		</tr>
		<tr>
			<th width="10%">Id.</th>
			<th width="20%">Nombre</th>
			<th width="20%">Clasificaci&oacute;n</th>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ProveedorEscoge('<?=$ROW[$x]['cs']?>','<?=$ROW[$x]['nombre']?>','<?=$ROW[$x]['naturaleza']?>')">
			<td align="center"><?=$ROW[$x]['id']?></td>
			<td align="center"><?=$ROW[$x]['nombre']?></td>
			<td align="center"><?=$ROW[$x]['clasificacion']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function ResuspencionesLista($_modulo, $_js=''){
		$this->TableHeader('Resuspenciones', 1, $_modulo, $_js);
		
		$Robot = new Biologia();
		$ROW = $Robot->ResDisponibles();
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ResuspencionEscoge('')">
			<td align="center">Limpiar</td>
		</tr>
		<tr>
			<th width="10%">C&oacute;digo</th>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
			$ROW[$x]['codigo'] = str_replace(' ', '', $ROW[$x]['codigo']);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="ResuspencionEscoge('<?=$ROW[$x]['codigo']?>')">
			<td align="center"><?=$ROW[$x]['codigo']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}

	private function TableFooter(){
		echo '</table></center></body></html>';
	}
	
	private function TableHeader($_titulo, $_columnas, $_modulo, $_js){
		echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>', $this->Title(), '</title>';
		$this->ShellIncluir('estilo', 'css');
		if($_js != ''){
			$_js = substr($_js,1);
			$this->ShellIncluir($_js, 'js', $_modulo);
		}
		echo '</head>
		<body>';
		echo "<center>
		<table class='radius' align='center' style='font-size:12px;'>
		<tr><td class='titulo' colspan='{$_columnas}'>{$_titulo}</td></tr>";
	}
	
	final public function TermosLista($_modulo, $_tipo, $_lid, $_js=''){
		$this->TableHeader('Equipos registradas', 5, $_modulo, $_js);
		
		$Variator = new Varios();
		$ROW = $Variator->TermosResumenGet($_lid, $_tipo, true);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="TermoEscoge('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');">
			<td align="center" colspan="5">Limpiar</td>
		</tr>
		<tr>
			<td width="10%"><strong>C&oacute;digo</strong></td>
			<td width="10%"><strong>Cuarto</strong></td>
			<td width="10%"><strong>L&iacute;mites �C</strong></td>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
			$ROW[$x]['limite1'] = $ROW[$x]['limite1'].' a '.$ROW[$x]['limite2'];
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="TermoEscoge('<?=$ROW[$x]['codigo']?>', '<?=$ROW[$x]['cuarto']?>', '<?=$ROW[$x]['limite1']?>', '<?=$ROW[$x]['inc1']?>', '<?=$ROW[$x]['indicacion1']?>', '<?=$ROW[$x]['error1']?>', '<?=$ROW[$x]['inc2']?>', '<?=$ROW[$x]['indicacion2']?>', '<?=$ROW[$x]['error2']?>', '<?=$ROW[$x]['inc3']?>', '<?=$ROW[$x]['indicacion3']?>', '<?=$ROW[$x]['error3']?>', '<?=$ROW[$x]['inc4']?>', '<?=$ROW[$x]['indicacion4']?>', '<?=$ROW[$x]['error4']?>', '<?=$ROW[$x]['inc5']?>', '<?=$ROW[$x]['indicacion5']?>', '<?=$ROW[$x]['error5']?>', '<?=$ROW[$x]['humedad1']?>', '<?=$ROW[$x]['hr1']?>', '<?=$ROW[$x]['err_hr1']?>', '<?=$ROW[$x]['hr2']?>', '<?=$ROW[$x]['err_hr2']?>', '<?=$ROW[$x]['hr3']?>', '<?=$ROW[$x]['err_hr3']?>');">
			<td><?=$ROW[$x]['codigo']?></td>
			<td><?=$ROW[$x]['cuarto']?></td>
			<td><?=$ROW[$x]['limite1']?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
	
	final public function UsuariosLista($_modulo, $_js='', $_linea=''/*USADO CUANDO HAY VARIAS LINEAS*/, $_lab=''){
		$this->TableHeader('Usuarios registrados', 2, $_modulo, $_js);
		
		$Robot = new Seguridad();
		$ROW = $Robot->UsuariosMuestra(0, $_lab);
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="UsuarioEscoge('','', '<?=$_linea?>')">
			<td align="center" colspan="2">Limpiar</td>
		</tr>
		<tr>
			<th width="10%">C&eacute;dula</th>
			<th width="20%">Nombre</th>
		</tr>
		<?php
		for($x=0;$x<count($ROW);$x++){
			$nombre = $ROW[$x]['nombre'].' '.$ROW[$x]['ap1'].' '.$ROW[$x]['ap2'];
		?>
		<tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" style="cursor:pointer" onclick="UsuarioEscoge('<?=$ROW[$x]['id']?>','<?=$nombre?>', '<?=$_linea?>')">
			<td align="center"><?=$ROW[$x]['cedula']?></td>
			<td><?=$nombre?></td>
		</tr>
		<?php
		}//FOR
		$this->TableFooter();
	}
//
}
?>