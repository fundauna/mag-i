ALTER TABLE [MAGI].[dbo].[PER014] ADD empresa varchar(150);

USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_042]    Script Date: 19/04/2017 15:41:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_042]
/*INGRESA DETALLE CURSOS/INDUCCIONES*/
	@_PERSONA SMALLINT,
	@_TIPO CHAR(1),
	@_FECHA SMALLDATETIME,
	@_HORAS SMALLINT,
	@_NOMBRE VARCHAR(150),
	@_EMPRESA VARCHAR(150)
AS
	DECLARE @linea SMALLINT

	--OBTENGO LA ULTIMA LINEA
	SET @linea = (SELECT COUNT(1) AS total FROM PER014 WHERE (persona = @_PERSONA) ) + 1

	INSERT INTO [PER014]
           ([persona]
           ,[linea]
           ,[tipo]
           ,[fecha]
           ,[horas]
           ,[nombre]
           ,[empresa])
     VALUES
           (@_PERSONA, @linea, @_TIPO, @_FECHA, @_HORAS, @_NOMBRE,@_EMPRESA)

