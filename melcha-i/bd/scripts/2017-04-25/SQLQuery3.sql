/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  TI
 * Created: 20/04/2017
 */

USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_085]    Script Date: 20/04/2017 15:59:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_085]
/*IME SOLICITUD SERVICIOS LCC*/
	@_CS CHAR(13),
	@_tipo CHAR(1),
	@_solicitud CHAR(13),
	@_proposito VARCHAR(50),
	@_dependencia CHAR(1),
	@_obs VARCHAR(200),
	@_cliente CHAR(15),
	@_direccion VARCHAR(500),
	@_entregadas VARCHAR(50),
	@_producto VARCHAR(50),
	@_tipo_form smallint,
	@_metodo BIT,
	@_registro VARCHAR(20),
	@_dosis VARCHAR(500),
	@_mezcla CHAR(1),
	@_densidad CHAR(1),
	--
	@_usuario SMALLINT,
	@_ACCION CHAR(1)

AS
	DECLARE @directa BIT

	IF @_solicitud = '' SET @directa = 1
	ELSE SET @directa = 0
	
	IF @_ACCION = 'I' BEGIN
		INSERT INTO SER003 VALUES(@_CS, GETDATE(), 
			@_tipo,
			@directa,
			@_proposito,
			@_dependencia,
			@_obs,
			@_cliente,
			@_entregadas,
			@_producto,
			@_tipo_form,
			@_metodo,
			@_registro,
			@_dosis,
			@_mezcla,
			@_densidad, 
			'0',
			@_direccion)
		
		INSERT INTO SERBI2 VALUES(@_CS, @_usuario, '0', GETDATE())
			
		IF @@ERROR = 0 
			UPDATE MAG000 SET servicios03_ = servicios03_ + 1			
	END ELSE IF @_ACCION = 'M' BEGIN
		UPDATE SER003 SET tipo = @_tipo,
			proposito = @_proposito,
			dependencia = @_dependencia,
			obs = @_obs,
			cliente = @_cliente,
			direccion = @_direccion,
			entregadas = @_entregadas,
			producto = @_producto,
			tipo_form = @_tipo_form,
			metodo = @_metodo,
			registro = @_registro,
			dosis = @_dosis,
			mezcla = @_mezcla,
			densidad = @_densidad
		WHERE cs = @_CS
	END

	--BORRA ANEXO A SOLICITUD SI EXISTE
	DELETE FROM SER007 WHERE (servicios = @_CS)

	IF @_solicitud != '' INSERT INTO SER007 VALUES(@_CS, @_solicitud)