/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  RRC
 * Created: 19/04/2017
 */

USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_023]    Script Date: 19/04/2017 14:44:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_023]
/*MANTENIMIENTO CLIENTES*/
	@_ID CHAR(15),
	@_NOMBRE VARCHAR(100),
	@_UNIDAD VARCHAR(50),
	@_REPRESENTANTE VARCHAR(60),
	@_TEL VARCHAR(15),
	@_EMAIL VARCHAR(15),
	@_FAX VARCHAR(15),
	@_DIRECCION VARCHAR(500),
	@_ACTIVIDAD VARCHAR(500),
	@_TIPO CHAR(1),
	@_LRE BIT,
	@_LDP BIT,
	@_LCC BIT,
	@_ESTADO CHAR(1),
	@_CLAVE VARCHAR(20),
	@_ACCION CHAR(1)
AS
	IF @_ACCION = 'I'
		INSERT INTO MAN002 VALUES(@_ID,
			@_NOMBRE,
			@_UNIDAD,
			@_REPRESENTANTE,
			@_TEL,
			@_EMAIL,
			@_FAX,
			@_DIRECCION,
			@_ACTIVIDAD,
			@_TIPO,
			@_LRE,
			@_LDP,
			@_LCC,
			@_ESTADO,
			@_CLAVE)
	ELSE IF @_ACCION = 'M'
		UPDATE MAN002 SET nombre = @_NOMBRE,
			unidad = @_UNIDAD,
			REPRESENTANTE = @_REPRESENTANTE,
			tel = @_tel,
			email = @_EMAIL,
			fax = @_FAX,
			direccion = @_DIRECCION,
			actividad = @_ACTIVIDAD,
			tipo = @_TIPO,
			LRE = @_LRE,
			LDP = @_LDP,
			LCC = @_LCC,
			estado = @_ESTADO
			WHERE id = @_ID
	ELSE IF @_ACCION = 'D' BEGIN
		DELETE FROM MAN014 WHERE cliente = @_ID
		DELETE FROM MAN013 WHERE cliente = @_ID
		DELETE FROM MAN002 WHERE id = @_ID
	END