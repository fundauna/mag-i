/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  TI
 * Created: 23/06/2017
 */

USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_047]    Script Date: 23/06/2017 11:17:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_047]
/*REPORTE DE INVENTARIO*/	
	@_ES TINYINT,
	@_TIPO SMALLINT,
	@_LAB CHAR
AS
	IF @_ES = 0
		SELECT INV002.codigo, INV002.nombre, INV002.marca, INV002.stock1, INV002.stock2, (INV002.ubicacion1 + ' ' + INV002.ubicacion2) AS ubicacion
		FROM INV002
		WHERE (es = 0) AND (INV002.familia = @_TIPO) AND (INV002.estado = '1')
		ORDER BY INV002.nombre
	ELSE IF @_ES = 1
		SELECT INV002.nombre, INV002.marca, INV002.presentacion, INV002.ubicacion1, INV002.ubicacion2, INV003.codigo, INV003.bodega1 AS stock1, INV003.bodega2 AS stock2, INV007.lote, INV007.costo
		FROM INV002 INNER JOIN INV003 ON INV002.id = INV003.producto INNER JOIN INV007 ON INV003.id = INV007.ES
		WHERE (INV003.tipo = 'E') AND (INV002.es = 1) AND (INV002.estado = '1') AND (INV002.familia = @_TIPO) AND (INV003.bodega1 > 0 OR INV003.bodega2 > 0)
		UNION --INVENTARIO QUE EMPEZO EN CERO
		SELECT INV002.nombre, INV002.marca, INV002.presentacion, INV002.ubicacion1, INV002.ubicacion2, '' AS codigo, INV002.stock1, INV002.stock2, '' AS lote, 0 AS costo
		FROM INV002 WHERE id NOT IN(
			SELECT distinct producto
			FROM INV003
			WHERE (tipo = 'E') AND (bodega1 > 0 OR bodega2 > 0)
		) AND (INV002.estado = '1')
	ELSE IF @_ES = 2 --VENCIMIENTOS
		SELECT INV002.nombre, INV002.marca, INV002.presentacion, INV002.ubicacion1, INV002.ubicacion2, INV003.codigo, INV003.bodega1 AS stock1, INV003.bodega2 AS stock2, INV007.lote, CONVERT(VARCHAR, INV004.vencimiento, 105) AS vencimiento
		FROM INV002 INNER JOIN INV003 ON INV002.id = INV003.producto INNER JOIN INV007 ON INV003.id = INV007.ES INNER JOIN INV004 ON INV007.ES = INV004.entrada INNER JOIN INV000 ON INV002.familia = INV000.id
		WHERE (INV003.tipo = 'E') AND (INV002.es = 1) AND (INV002.estado = '1') AND (INV000.lab = @_LAB) AND (INV003.bodega1 > 0 OR INV003.bodega2 > 0)
		ORDER BY INV004.vencimiento