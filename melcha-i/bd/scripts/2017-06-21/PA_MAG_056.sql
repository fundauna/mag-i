/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  TI
 * Created: 23/06/2017
 */

USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_056]    Script Date: 23/06/2017 12:05:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_056]
/*INVENTARIO CON ALERTAS*/
	@_LAB CHAR(1)
AS
	--PRODUCTOS CON STOCK MINIMO
	SELECT 1 AS alerta, INV002.id, INV002.codigo, INV002.nombre, INV002.marca, INV002.stock1, INV002.stock2, INV002.estado, INV002.es, INV001.nombre AS unidades
	FROM INV002 INNER JOIN INV001 ON INV002.presentacion = INV001.id INNER JOIN INV000 ON INV002.familia = INV000.id
	WHERE (INV000.lab = @_LAB) AND (INV002.stock1 <= INV002.minimo1 OR INV002.stock2 <= INV002.minimo2) AND (INV002.estado = '1')
	UNION
	--PRODUCTOS CON FECHA DE AVISO ALCANZADA
	SELECT DISTINCT 2 AS alerta, INV002.id, INV002.codigo, INV002.nombre, INV002.marca, INV002.stock1, INV002.stock2, INV002.estado, INV002.es, INV001.nombre AS unidades
	FROM INV002 INNER JOIN INV001 ON INV002.presentacion = INV001.id INNER JOIN INV000 ON INV002.familia = INV000.id
	WHERE (INV000.lab = @_LAB) AND (INV002.estado = '1') AND (INV002.id IN 
		(
			SELECT DISTINCT INV003.producto
			FROM INV004 INNER JOIN INV007 ON INV004.entrada = INV007.ES INNER JOIN INV003 ON INV007.ES = INV003.id
			WHERE (DATEDIFF(DAY, GETDATE(), INV004.aviso) < 1) AND NOT (DATEDIFF(DAY, GETDATE(), INV004.vencimiento) < 1)
			)
		)
	UNION
	--PRODUCTOS VENCIDOS
	SELECT DISTINCT 3 AS alerta, INV002.id, INV002.codigo, INV002.nombre, INV002.marca, INV002.stock1, INV002.stock2, INV002.estado, INV002.es, INV001.nombre AS unidades
	FROM INV002 INNER JOIN INV001 ON INV002.presentacion = INV001.id INNER JOIN INV000 ON INV002.familia = INV000.id
	WHERE (INV000.lab = @_LAB) AND (INV002.estado = '1') AND (INV002.id IN 
			(
			SELECT DISTINCT INV003.producto
			FROM INV004 INNER JOIN INV007 ON INV004.entrada = INV007.ES INNER JOIN INV003 ON INV007.ES = INV003.id
			WHERE (DATEDIFF(DAY, GETDATE(), INV004.vencimiento) < 1) AND (INV004.aviso IS NOT NULL)
			)
		)