/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  TI
 * Created: 23/06/2017
 */

USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_021]    Script Date: 23/06/2017 10:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_021]
/*MANTENIMIENTO INVENTARIO*/
	@_ID SMALLINT,
	@_FAMILIA SMALLINT,
	@_CODIGO VARCHAR(15),
	@_ORDEN VARCHAR(100),
	@_FACTURA VARCHAR(100),
	@_PROVEEDOR VARCHAR(100),
	@_ACTA VARCHAR(45),
	@_NOMBRE VARCHAR(100),
	@_MARCA VARCHAR(20),
	@_PRESENTACION SMALLINT,
	@_ES BIT,
	@_MINIMO1 FLOAT,
	@_MINIMO2 FLOAT,
	@_STOCK1 FLOAT,
	@_STOCK2 FLOAT,
	@_UBICACION1 VARCHAR(100),
	@_UBICACION2 VARCHAR(100),
	@_ACCION CHAR(1)
AS
	IF @_ACCION = 'I' BEGIN
		SET @_ID = (SELECT inventario FROM MAG000)

		INSERT INTO INV002 VALUES(@_ID,
			@_FAMILIA,
			@_CODIGO,
			@_ORDEN,
			@_FACTURA,
			@_PROVEEDOR,
			@_ACTA,
			@_NOMBRE,
			@_MARCA,
			@_PRESENTACION,
			@_ES,
			@_MINIMO1,
			@_MINIMO2,
			@_STOCK1,
			@_STOCK2,
			@_UBICACION1,
			@_UBICACION2,
			1 --ESTADO		
		)

		IF @@ERROR = 0 
			UPDATE MAG000 SET inventario = inventario + 1
	END ELSE IF @_ACCION = 'M'
		UPDATE INV002 SET familia = @_FAMILIA,
			codigo = @_CODIGO,
			orden = @_ORDEN,
			factura = @_FACTURA,
			proveedor = @_PROVEEDOR,
			acta = @_ACTA,
			nombre = @_NOMBRE,
			marca = @_MARCA,
			presentacion = @_PRESENTACION,
			es = @_ES,
			minimo1 = @_MINIMO1,
			minimo2 = @_MINIMO2,
			stock1 = @_STOCK1,
			stock2 = @_STOCK2,
			ubicacion1 = @_UBICACION1,
			ubicacion2 = @_UBICACION2
			WHERE id = @_ID
	ELSE IF @_ACCION = 'D' BEGIN
		UPDATE INV002 SET estado = 0 WHERE id = @_ID
	END