/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  TI
 * Created: 22/06/2017
 */

USE MAGI;
GO
BEGIN TRANSACTION
GO
ALTER TABLE INV002
ALTER COLUMN acta VARCHAR(100);
ALTER TABLE INV002
ADD estado CHAR(1);
GO
COMMIT TRANSACTION
GO
UPDATE INV002 SET estado = 1;