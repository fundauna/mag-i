SELECT VAR005.nombre AS elemento,
       MUE002.declarada,
       MUE002.resultado,
       MUE002.densidad,
       MUE002.incertidumbre,
       MUE002.unidad
FROM MUE001
INNER JOIN MUE002 ON MUE001.id = MUE002.xanalizar
INNER JOIN MUE000 ON MUE001.muestra = MUE000.id
INNER JOIN INF001 ON MUE000.id = INF001.muestra
INNER JOIN VAR005 ON MUE001.analisis = VAR005.id
WHERE (MUE000.id = '3-2017-308-1-1')
  AND (MUE002.cumple = 1)
  AND (MUE002.clasificacion = '0')
  AND (INF001.informe = 'LCC-2017-324')
ORDER BY MUE000.ref,
         VAR005.nombre;

SELECT DISTINCT(ingrediente) FROM MUE002 ORDER BY ingrediente

SELECT * FROM MUE002 WHERE cumple=1 AND clasificacion='0' AND densidad=0 AND incertidumbre=0 AND ingrediente IN('Cd','Pb')

UPDATE MUE002 SET clasificacion='2' WHERE id IN('889-2017','890-2017');
		 
SELECT VAR005.nombre AS elemento,
       MUE002.declarada,
       MUE002.resultado,
       MUE002.densidad,
       MUE002.incertidumbre,
       MUE002.unidad
FROM MUE001
INNER JOIN MUE002 ON MUE001.id = MUE002.xanalizar
INNER JOIN MUE000 ON MUE001.muestra = MUE000.id
INNER JOIN INF001 ON MUE000.id = INF001.muestra
INNER JOIN VAR005 ON MUE001.analisis = VAR005.id
WHERE (MUE000.id = '3-2017-308-1-1')
  AND (MUE002.cumple = 1)
  AND (MUE002.clasificacion = '2')
  AND (INF001.informe = 'LCC-2017-324')
ORDER BY MUE000.ref,
         VAR005.nombre;