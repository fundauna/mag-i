USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_102]    Script Date: 23/5/2017 10:51:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_102]
/*CONVIERTE UN INFORME TEMPORAL A OFICIAL*/
	@_temporal CHAR(14),
	@_nuevo CHAR(14),
	@_obs VARCHAR(500),
	@_usuario SMALLINT,
	@_lab CHAR(1)
AS
	SET NOCOUNT ON

	DECLARE @ref VARCHAR(50), @tipo CHAR(1), @lab CHAR(1), @muestra CHAR(18), @solicitud CHAR(13), @total SMALLINT

	SET @ref = ( SELECT ref FROM INF000 WHERE (id = @_temporal) )
	SET @tipo = ( SELECT tipo FROM INF000 WHERE (id = @_temporal) )
	SET @muestra = ( SELECT TOP(1) muestra FROM INF001 WHERE (informe = @_temporal) )
	SET @solicitud = ( SELECT solicitud FROM MUE000 WHERE (id = @muestra) )

	--INGRESA REPORTE OFICIAL
	INSERT INTO INF000 VALUES(@_nuevo, GETDATE(), @ref, @_obs, '1', @tipo, @_lab)

	IF @@ERROR = 0 BEGIN
		--INGRESA EN BITACORA DE INFORME
		INSERT INTO INFBIT VALUES(@_nuevo, @_usuario, GETDATE(), '1')
		--ACTUALIZA ENLACES
		UPDATE INF001 SET informe = @_nuevo WHERE (informe = @_temporal)
		UPDATE INF005 SET informe = @_nuevo WHERE (informe = @_temporal)
		--BORRA INFORME TEMPOTAL
		DELETE FROM INF000 WHERE (id = @_temporal)
		--CAMBIA ESTADO LA MUESTRA
		UPDATE MUE000 SET estado = '3' WHERE (id IN ( SELECT muestra FROM INF001 WHERE (informe = @_nuevo) ) )
		--INGRESA EN BITACORA DE MUESTRAS
		DECLARE C1 CURSOR FOR SELECT muestra FROM INF001 WHERE (informe = @_temporal)
		OPEN C1
		FETCH NEXT FROM C1 INTO @muestra
		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO MUEBIT VALUES(@muestra, @_usuario, '3', GETDATE(),NULL)
	   		FETCH NEXT FROM C1 INTO @muestra
		END
		CLOSE C1
		DEALLOCATE C1
		--OBTIENE EL NUMERO DE MUESTRAS EN REPORTE PARA VER SI PUEDE FINALIZAR LA SOLICITUD
		SET @total = ( SELECT COUNT(1) AS total FROM MUE000 WHERE (solicitud = @solicitud) AND (estado <> '3') )
		IF(@total = 0) BEGIN--SI TODAS ESTAN EN ESTADO 3, PUEDO BLOQUEAR LA SOLICITUD
			IF(@_LAB = '1')
				UPDATE SER001 SET estado = '7' WHERE (cs = @solicitud)
			ELSE IF(@_LAB = '3')
				UPDATE SER003 SET estado = '7' WHERE (cs = @solicitud)
		END		
	END