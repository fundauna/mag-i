USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_078]    Script Date: 12/5/2017 09:30:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_078]
/*IME CARTA3 PASO1*/
	@_consecutivo CHAR(30),
	@_tipo CHAR(1),
	--
	@_observaciones VARCHAR(MAX),
	@_equipo SMALLINT,
	@_fechavence0 SMALLDATETIME,
	@_fechavence1 SMALLDATETIME,
	@_fechavence2 SMALLDATETIME,
	@_fechavence3 SMALLDATETIME,
	@_fechavence4 SMALLDATETIME,
	@_lote1 VARCHAR(20),
	@_lote2 VARCHAR(20),
	@_lote3 VARCHAR(20),
	@_lote4 VARCHAR(20),
	@_certificado1 VARCHAR(20),
	@_certificado2 VARCHAR(20),
	@_certificado3 VARCHAR(20),
	@_certificado4 VARCHAR(20),
	@_limiteph VARCHAR(11),
	@_limitedesv VARCHAR(11),
	@_limitecoef VARCHAR(11),
	@_limitecond VARCHAR(11),
	@_lecturaC BIT,
	@_pendiente VARCHAR(11),
	@_desviacion VARCHAR(11),
	--
	@_lab CHAR(1),
	@_usuario SMALLINT,
	@_accion CHAR(1),
	@_fecha date
AS
	IF @_accion = 'I' BEGIN
		INSERT INTO CAR000 VALUES(@_consecutivo, @_fecha, @_tipo, '0', @_lab,NULL,NULL)

		INSERT INTO CARBIT VALUES(@_consecutivo, GETDATE(), @_usuario, '0')
	
		INSERT INTO CAR011 VALUES(@_consecutivo,
			@_equipo,
			@_fechavence0,
			@_fechavence1,
			@_fechavence2,
			@_fechavence3,
			@_fechavence4,
			@_lote1,
			@_lote2,
			@_lote3,
			@_lote4,
			@_certificado1,
			@_certificado2,
			@_certificado3,
			@_certificado4,
			@_limiteph,
			@_limitedesv,
			@_limitecoef,
			@_limitecond,
			@_lecturaC,
			@_pendiente,
			@_desviacion,
			@_observaciones)

		UPDATE CAR000 SET 
			codigoalterno=(SELECT CASE WHEN MAX(codigoalterno) IS NULL THEN 1 ELSE MAX(codigoalterno)+1 END FROM CAR000 WHERE codigolab=(SELECT EQU000.codigo FROM CAR011 INNER JOIN EQU000 ON CAR011.equipo = EQU000.id WHERE CAR011.codigo=@_consecutivo)),
			codigolab=(SELECT EQU000.codigo FROM CAR011 INNER JOIN EQU000 ON CAR011.equipo = EQU000.id WHERE CAR011.codigo=@_consecutivo)
			WHERE consecutivo=@_consecutivo

	END ELSE IF @_accion = 'M' BEGIN
		UPDATE CAR000 SET fecha = @_fecha WHERE consecutivo = @_consecutivo

		UPDATE CAR011 SET equipo = @_equipo,
			fechavence0 = @_fechavence0,
			fechavence1 = @_fechavence1,
			fechavence2=@_fechavence2,
			fechavence3=@_fechavence3,
			lote1=@_lote1,
			lote2=@_lote2,
			lote3=@_lote3,
			lote4=@_lote4,
			certificado1=@_certificado1,
			certificado2=@_certificado2,
			certificado3=@_certificado3,
			certificado4=@_certificado4,
			limiteph=@_limiteph,
			limitedesv=@_limitedesv,
			limitecoef=@_limitecoef,
			limitecond=@_limitecond,
			lecturaC=@_lecturaC,
			pendiente=@_pendiente,
			desviacion=@_desviacion,
			observaciones=@_observaciones
			WHERE codigo = @_consecutivo
	END