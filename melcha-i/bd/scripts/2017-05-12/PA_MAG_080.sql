USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_080]    Script Date: 12/5/2017 10:21:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_080]
/*IME CARTA7 PASO1*/
	@_consecutivo CHAR(30),
	@_fecha date,
	@_tipo CHAR(1),
	--
	@_nombre varchar(20),
	@_masa float,
	@_origen varchar(50),
	@_aforado float,
	@_tiene bit,
	@_pureza float,
	@_cod_bal smallint,
	@_inyeccion float,
	@_tipo_bomba char(1),
	@_tipo_inyector char(1),
	@_eluente varchar(20),
	@_temp1 float,
	@_modo char(1),
	@_canalA char(1),
	@_flujoA float,
	@_presiA float,
	@_canalB char(1),
	@_flujoB float,
	@_presiB float,
	@_canalC char(1),
	@_flujoC float,
	@_presiC float,
	@_canalD char(1),
	@_flujoD float,
	@_presiD float,
	@_tipo_col char(1),
	@_cod_col varchar(20),
	@_marcafase varchar(500),
	@_fecha_em smalldatetime,
	@_tipo_detector char(1),
	@_temp2 float,
	@_temp3 float,
	@_dimensiones varchar(500),
	@_longitud float,
	@_otros varchar(500),
	@_bar float,
	@_pat_ali float,
	@_pat_afo float,
	--
	@_lab CHAR(1),
	@_usuario SMALLINT,
	@_accion CHAR(1)
AS
	IF @_fecha_em = '' SET @_fecha_em = NULL

	IF @_accion = 'I' BEGIN
		INSERT INTO CAR000 VALUES(@_consecutivo, @_fecha, @_tipo, '0', @_lab,NULL,NULL)

		INSERT INTO CARBIT VALUES(@_consecutivo, @_fecha, @_usuario, '0')
	
		INSERT INTO CAR016 VALUES(@_consecutivo,
			@_nombre,
			@_masa,
			@_origen,
			@_aforado,
			@_tiene,
			@_pureza,
			@_cod_bal,
			@_inyeccion,
			@_tipo_bomba,
			@_tipo_inyector,
			@_eluente,
			@_temp1,
			@_modo,
			@_canalA,
			@_flujoA,
			@_presiA,
			@_canalB,
			@_flujoB,
			@_presiB,
			@_canalC,
			@_flujoC,
			@_presiC,
			@_canalD,
			@_flujoD,
			@_presiD,
			@_tipo_col,
			@_cod_col,
			@_marcafase,
			@_fecha_em,
			@_tipo_detector,
			@_temp2,
			@_temp3,
			@_dimensiones,
			@_longitud,
			@_otros,
			@_bar,
			@_pat_ali,
			@_pat_afo)

			
		UPDATE CAR000 SET 
			codigoalterno=(SELECT CASE WHEN MAX(codigoalterno) IS NULL THEN 1 ELSE MAX(codigoalterno)+1 END FROM CAR000 WHERE codigolab=(SELECT EQU000.codigo FROM CAR016 INNER JOIN EQU000 ON CAR016.cod_bal = EQU000.id WHERE CAR016.codigo =@_consecutivo)),
			codigolab=(SELECT EQU000.codigo FROM CAR016 INNER JOIN EQU000 ON CAR016.cod_bal = EQU000.id WHERE CAR016.codigo =@_consecutivo)
			WHERE consecutivo=@_consecutivo

	END ELSE IF @_accion = 'M' BEGIN
		UPDATE CAR000 SET fecha = @_fecha WHERE consecutivo = @_consecutivo

		UPDATE CAR016 SET nombre=@_nombre,
			masa=@_masa,
			origen=@_origen,
			aforado=@_aforado,
			tiene=@_tiene,
			pureza=@_pureza,
			cod_bal=@_cod_bal,
			inyeccion=@_inyeccion,
			tipo_bomba=@_tipo_bomba,
			tipo_inyector=@_tipo_inyector,
			eluente=@_eluente,
			temp1=@_temp1,
			modo=@_modo,
			canalA=@_canalA,
			flujoA=@_flujoA,
			presiA=@_presiA,
			canalB=@_canalB,
			flujoB=@_flujoB,
			presiB=@_presiB,
			canalC=@_canalC,
			flujoC=@_flujoC,
			presiC=@_presiC,
			canalD=@_canalD,
			flujoD=@_flujoD,
			presiD=@_presiD,
			tipo_col=@_tipo_col,
			cod_col=@_cod_col,
			marcafase=@_marcafase,
			fecha_em=@_fecha_em,
			tipo_detector=@_tipo_detector,
			temp2=@_temp2,
			temp3=@_temp3,
			dimensiones=@_dimensiones,
			longitud=@_longitud,
			otros=@_otros,
			bar=@_bar,
			pat_ali=@_pat_ali,
			pat_afo=@_pat_afo
			WHERE codigo = @_consecutivo
	END