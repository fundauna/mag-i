USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_074]    Script Date: 12/5/2017 10:04:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_074]
/*IME CARTA 5*/
	@_consecutivo CHAR(10),
	@_tipo CHAR(1),
	@_lab CHAR(1),
	@_usuario SMALLINT,
	--
	@_cod_cam CHAR(20),
	@_cod_ter CHAR(20),
	@_limite1 FLOAT,
	@_limite2 FLOAT,
	@_tipo2 BIT,
	@_ubicacion VARCHAR(50),
	@_inc1 FLOAT,
	@_indicacion1 FLOAT,
	@_error1 FLOAT,
	@_inc2 FLOAT,
	@_indicacion2 FLOAT,
	@_error2 FLOAT,
	@_inc3 FLOAT,
	@_indicacion3 FLOAT,
	@_error3 FLOAT,
	@_inc4 FLOAT,
	@_indicacion4 FLOAT,
	@_error4 FLOAT,
	@_inc5 FLOAT,
	@_indicacion5 FLOAT,
	@_error5 FLOAT,
	--
	@_fecha SMALLDATETIME,
	@_hora CHAR(5),
	@_temp1 FLOAT,
	@_temp2 FLOAT,
	--
	@_accion CHAR(1)
AS
	IF @_accion = 'I' BEGIN
		INSERT INTO CAR000 VALUES(@_consecutivo, GETDATE(), @_tipo, '0', @_lab,NULL,NULL)
	
		INSERT INTO CAR005 VALUES(@_consecutivo,
			@_cod_cam,
			@_cod_ter,
			@_limite1,
			@_limite2,
			@_tipo2,
			@_ubicacion,
			@_inc1,
			@_indicacion1,
			@_error1,
			@_inc2,
			@_indicacion2,
			@_error2,
			@_inc3,
			@_indicacion3,
			@_error3,
			@_inc4,
			@_indicacion4,
			@_error4,
			@_inc5,
			@_indicacion5,
			@_error5)

		UPDATE CAR000 SET 
			codigoalterno=(SELECT CASE WHEN MAX(codigoalterno) IS NULL THEN 1 ELSE MAX(codigoalterno)+1 END FROM CAR000 WHERE codigolab=(SELECT EQU000.codigo FROM CAR005 INNER JOIN EQU000 ON CAR005.cod_cam = EQU000.id WHERE CAR005.codigo=@_consecutivo)),
			codigolab=(SELECT EQU000.codigo FROM CAR005 INNER JOIN EQU000 ON CAR005.cod_cam = EQU000.id WHERE CAR005.codigo=@_consecutivo)
			WHERE consecutivo=@_consecutivo

	END ELSE IF @_accion = 'M' BEGIN
		UPDATE CAR000 SET fecha = GETDATE() WHERE consecutivo = @_consecutivo

		UPDATE CAR005 SET cod_cam=@_cod_cam,
			cod_ter=@_cod_ter,
			limite1=@_limite1,
			limite2=@_limite2,
			tipo=@_tipo,
			ubicacion=@_ubicacion,
			inc1=@_inc1,
			ind1=@_indicacion1,
			factor1=@_error1,
			inc2=@_inc2,
			ind2=@_indicacion2,
			factor2=@_error2,
			inc3=@_inc3,
			ind3=@_indicacion3,
			factor3=@_error3,
			inc4=@_inc4,
			ind4=@_indicacion4,
			factor4=@_error4,
			inc5=@_inc5,
			ind5=@_indicacion5,
			factor5=@_error5 
			WHERE codigo = @_consecutivo
	END

	INSERT INTO CAR006 VALUES(@_consecutivo, @_fecha, @_hora, @_temp1, @_temp2, @_usuario)