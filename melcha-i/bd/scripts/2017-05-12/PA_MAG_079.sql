USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_079]    Script Date: 12/5/2017 10:18:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_079]
/*IME CARTA6 PASO1*/
	@_consecutivo CHAR(30),
	@_tipo CHAR(1),
	@_modalidad CHAR(1),
	--
	@_equipo SMALLINT,
	@_nombre VARCHAR(20),
	@_corriente VARCHAR(11),
	@_origen VARCHAR(50),
	@_longitud VARCHAR(11),
	@_tiene BIT,
	@_ancho VARCHAR(20),
	@_conc FLOAT,
	--
	@_lab CHAR(1),
	@_usuario SMALLINT,
	@_accion CHAR(1),
	@_fecha date
	
AS
	IF @_accion = 'I' BEGIN
		INSERT INTO CAR000 VALUES(@_consecutivo, @_fecha, @_tipo, '0', @_lab,NULL,NULL)
	
		INSERT INTO CARBIT VALUES(@_consecutivo, GETDATE(), @_usuario, '0')

		INSERT INTO CAR013 VALUES(@_consecutivo,
			@_modalidad,
			@_equipo,
			@_nombre,
			@_corriente,
			@_origen,
			@_longitud,
			@_tiene,
			@_ancho,
			@_conc)

		UPDATE CAR000 SET 
			codigoalterno=(SELECT CASE WHEN MAX(codigoalterno) IS NULL THEN 1 ELSE MAX(codigoalterno)+1 END FROM CAR000 WHERE codigolab=(SELECT EQU000.codigo FROM CAR013 INNER JOIN EQU000 ON CAR013.equipo = EQU000.id WHERE CAR013.codigo=@_consecutivo)),
			codigolab=(SELECT EQU000.codigo FROM CAR013 INNER JOIN EQU000 ON CAR013.equipo = EQU000.id WHERE CAR013.codigo=@_consecutivo)
			WHERE consecutivo=@_consecutivo

	END ELSE IF @_accion = 'M' BEGIN
		UPDATE CAR000 SET fecha = @_fecha WHERE consecutivo = @_consecutivo

		UPDATE CAR013 SET modalidad = @_modalidad,
			equipo = @_equipo,
			nombre=@_nombre,
			corriente=@_corriente,
			origen=@_origen,
			longitud=@_longitud,
			tiene=@_tiene,
			ancho=@_ancho,
			conc=@_conc
			WHERE codigo = @_consecutivo
	END