USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_072]    Script Date: 12/5/2017 09:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_072]
/*IME CARTA 0*/
	@_consecutivo CHAR(30),
	@_tipo CHAR(1),
	@_lab CHAR(1),
	--
	@_tipo_bal CHAR(1),
	@_balanza SMALLINT,
	@_rango VARCHAR(20),
	@_pesa1 VARCHAR(20),
	@_nominal1 VARCHAR(7),
	@_cert1 VARCHAR(20),
	@_pesa2 VARCHAR(20),
	@_nominal2 VARCHAR(7),
	@_cert2 VARCHAR(20),
	--
	@_accion CHAR(1)
AS
	IF @_accion = 'I' BEGIN
		INSERT INTO CAR000 VALUES(@_consecutivo, GETDATE(), @_tipo, '0', @_lab,NULL,NULL)
	
		INSERT INTO CAR001 VALUES(@_consecutivo,
			@_tipo_bal,
			@_balanza,
			@_rango,
			@_pesa1,
			@_nominal1,
			@_cert1,
			@_pesa2,
			@_nominal2,
			@_cert2)

		UPDATE CAR000 SET 
		codigoalterno=(SELECT CASE WHEN MAX(codigoalterno) IS NULL THEN 1 ELSE MAX(codigoalterno)+1 END FROM CAR000 WHERE codigolab=(SELECT EQU000.codigo FROM CAR001 INNER JOIN EQU000 ON CAR001.balanza = EQU000.id WHERE CAR001.codigo=@_consecutivo)),
		codigolab=(SELECT EQU000.codigo FROM CAR001 INNER JOIN EQU000 ON CAR001.balanza = EQU000.id WHERE CAR001.codigo=@_consecutivo)
		WHERE consecutivo=@_consecutivo

	END ELSE IF @_accion = 'M' BEGIN
		UPDATE CAR000 SET fecha = GETDATE() WHERE consecutivo = @_consecutivo

		UPDATE CAR001 SET tipo_bal = @_tipo_bal,
			balanza = @_balanza,
			rango = @_rango,
			pesa1 = @_pesa1,
			nominal1 = @_nominal1,
			cert1 = @_cert1,
			pesa2 = @_pesa2,
			nominal2 = @_nominal2,
			cert2 = @_cert2
			WHERE codigo = @_consecutivo
	END