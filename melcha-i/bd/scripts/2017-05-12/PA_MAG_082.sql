USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_082]    Script Date: 12/5/2017 11:22:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_082]
/*IME CARTA9 PASO1*/
	@_consecutivo CHAR(30),
	@_tipo CHAR(1),
	--
	@_equipo SMALLINT,
	@_idColumna SMALLINT,
	@_idEstandar SMALLINT,
	@_connominal varchar(50),
	--
	@_lab CHAR(1),
	@_usuario SMALLINT,
	@_accion CHAR(1),
	@_fecha date

AS
	SET NOCOUNT ON

	IF @_accion = 'I' BEGIN
		INSERT INTO CAR000 VALUES(@_consecutivo, @_fecha, @_tipo, '0', @_lab,NULL,NULL)
		--INGRESA ENCABEZADO
		INSERT INTO CAR023(codigo, equipo, connominal) VALUES(@_consecutivo, @_equipo, @_connominal)
		--INGRESA COLUMNA Y ESTANDAR
		INSERT INTO CAR024 VALUES(@_consecutivo, @_idColumna, 0)
		INSERT INTO CAR024 VALUES(@_consecutivo, @_idEstandar, 1)
		--INGRESA BITACORA
		INSERT INTO CAR025 VALUES(@_consecutivo, @_usuario)

		UPDATE CAR000 SET 
			codigoalterno=(SELECT CASE WHEN MAX(codigoalterno) IS NULL THEN 1 ELSE MAX(codigoalterno)+1 END FROM CAR000 WHERE codigolab=(SELECT EQU000.codigo FROM CAR023 INNER JOIN EQU000 ON CAR023.equipo = EQU000.id WHERE CAR023.codigo = @_consecutivo)),
			codigolab=(SELECT EQU000.codigo FROM CAR023 INNER JOIN EQU000 ON CAR023.equipo = EQU000.id WHERE CAR023.codigo = @_consecutivo)
			WHERE consecutivo=@_consecutivo

	END ELSE IF @_accion = 'M' BEGIN
		UPDATE CAR000 SET fecha = @_fecha WHERE consecutivo = @_consecutivo

		--INGRESA COLUMNA Y ESTANDAR
		DELETE FROM CAR024 WHERE (carta = @_consecutivo)
		INSERT INTO CAR024 VALUES(@_consecutivo, @_idColumna, 0)
		INSERT INTO CAR024 VALUES(@_consecutivo, @_idEstandar, 1)
		--INGRESA BITACORA
		DELETE FROM CAR025 WHERE (carta = @_consecutivo) AND (usuario = @_usuario)
		INSERT INTO CAR025 VALUES(@_consecutivo, @_usuario)

		UPDATE CAR023 SET equipo=@_equipo,
			connominal=@_connominal
			WHERE codigo = @_consecutivo
	END