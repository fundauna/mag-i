USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_081]    Script Date: 12/5/2017 11:14:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_081]
/*IME CARTA8 PASO1*/
	@_consecutivo CHAR(30),
	@_fecha date,
	@_tipo CHAR(1),
	--
	@_nombre varchar(20),
	@_masa float,
	@_origen varchar(50),
	@_aforado float,
	@_tiene bit,
	@_pureza float,
	@_cod_bal smallint,
	@_tipo_inyector char(1),
	@_inyeccion float,
	@_temp1 float,
	@_modo char(1),
	@_tipo_col char(1),
	@_gas1 char(1),
	@_flujo1 float,
	@_presion1 float,	
	@_gas2 char(1),
	@_flujo2 float,
	@_presion2 float,
	@_gas3 char(1),
	@_flujo3 float,
	@_presion3 float,
	@_gas4 char(1),
	@_flujo4 float,
	@_presion4 float,
	@_cod_col varchar(20),
	@_marcafase varchar(500),
	@_fecha_em smalldatetime,
	@_temp2 float,
	@_tipo_detector char(1),
	@_dimensiones varchar(500),
	@_temp3 float,
	@_otros varchar(500),
	--
	@_lab CHAR(1),
	@_usuario SMALLINT,
	@_accion CHAR(1)
AS
	IF @_fecha_em = '' SET @_fecha_em = NULL

	IF @_accion = 'I' BEGIN
		INSERT INTO CAR000 VALUES(@_consecutivo, @_fecha, @_tipo, '0', @_lab,NULL,NULL)

		INSERT INTO CARBIT VALUES(@_consecutivo, @_fecha, @_usuario, '0')
	
		INSERT INTO CAR020 VALUES(@_consecutivo,
			@_nombre,
			@_masa,
			@_origen,
			@_aforado,
			@_tiene,
			@_pureza,
			@_cod_bal,
			@_tipo_inyector,
			@_inyeccion,
			@_temp1,
			@_modo,
			@_tipo_col,
			@_gas1,
			@_flujo1,
			@_presion1,	
			@_gas2,
			@_flujo2,
			@_presion2,
			@_gas3,
			@_flujo3,
			@_presion3,
			@_cod_col,
			@_marcafase,
			@_fecha_em,
			@_temp2,
			@_tipo_detector,
			@_dimensiones,
			@_temp3,
			@_otros,
			@_gas4,
			@_flujo4,
			@_presion4)

		UPDATE CAR000 SET 
			codigoalterno=(SELECT CASE WHEN MAX(codigoalterno) IS NULL THEN 1 ELSE MAX(codigoalterno)+1 END FROM CAR000 WHERE codigolab=(SELECT EQU000.codigo FROM CAR020 INNER JOIN EQU000 ON CAR020.cod_bal = EQU000.id WHERE CAR020.codigo = @_consecutivo)),
			codigolab=(SELECT EQU000.codigo FROM CAR020 INNER JOIN EQU000 ON CAR020.cod_bal = EQU000.id WHERE CAR020.codigo = @_consecutivo)
			WHERE consecutivo=@_consecutivo

	END ELSE IF @_accion = 'M' BEGIN
		UPDATE CAR000 SET fecha = @_fecha WHERE consecutivo = @_consecutivo

		UPDATE CAR020 SET nombre=@_nombre,
			masa=@_masa,
			origen=@_origen,
			aforado=@_aforado,
			tiene=@_tiene,
			pureza=@_pureza,
			cod_bal=@_cod_bal,
			inyeccion=@_inyeccion,
			tipo_inyector=@_tipo_inyector,
			temp1=@_temp1,
			modo=@_modo,
			tipo_col=@_tipo_col,
			gas1=@_gas1,
			flujo1=@_flujo1,
			presion1=@_presion1,	
			gas2=@_gas2,
			flujo2=@_flujo2,
			presion2=@_presion2,
			gas3=@_gas3,
			flujo3=@_flujo3,
			presion3=@_presion3,
			gas4=@_gas4,
			flujo4=@_flujo4,
			presion4=@_presion4,
			cod_col=@_cod_col,
			marcafase=@_marcafase,
			fecha_em=@_fecha_em,
			tipo_detector=@_tipo_detector,
			temp2=@_temp2,
			temp3=@_temp3,
			dimensiones=@_dimensiones,
			otros=@_otros
			WHERE codigo = @_consecutivo
	END