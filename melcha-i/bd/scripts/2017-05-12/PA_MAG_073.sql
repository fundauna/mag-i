USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_073]    Script Date: 12/5/2017 09:57:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_073]
/*IMA CARTA 4*/
	@_consecutivo CHAR(10),
	@_tipo CHAR(1),
	@_lab CHAR(1),
	@_usuario SMALLINT,
	--
	@_cod_ter CHAR(20),
	@_cuarto VARCHAR(20),
	@_limite1 FLOAT,
	@_limite2 FLOAT,
	@_humedad FLOAT,
	@_indicacion1 FLOAT,
	@_error1 FLOAT,
	@_indicacion2 FLOAT,
	@_error2 FLOAT,
	@_indicacion3 FLOAT,
	@_error3 FLOAT,
	@_hr1 FLOAT,
	@_err_hr1 FLOAT,
	@_hr2 FLOAT,
	@_err_hr2 FLOAT,
	@_hr3 FLOAT,
	@_err_hr3 FLOAT,
	@_obs VARCHAR(50),
	--
	@_fecha SMALLDATETIME,
	@_hora CHAR(5),
	@_temp1 FLOAT,
	@_temp2 FLOAT,
	@_hume1 FLOAT,
	@_hume2 FLOAT,
	--
	@_accion CHAR(1)
AS
	IF @_accion = 'I' BEGIN
		INSERT INTO CAR000 VALUES(@_consecutivo, GETDATE(), @_tipo, '0', @_lab,NULL,NULL)
	
		INSERT INTO CAR003 VALUES(@_consecutivo,
			@_cod_ter,
			@_cuarto,
			@_limite1,
			@_limite2,
			@_humedad,
			@_indicacion1,
			@_error1,
			@_indicacion2,
			@_error2,
			@_indicacion3,
			@_error3,
			@_hr1,
			@_err_hr1,
			@_hr2,
			@_err_hr2,
			@_hr3,
			@_err_hr3, 
			@_obs)

		UPDATE CAR000 SET 
			codigoalterno=(SELECT CASE WHEN MAX(codigoalterno) IS NULL THEN 1 ELSE MAX(codigoalterno)+1 END FROM CAR000 WHERE codigolab=(SELECT EQU000.codigo FROM CAR003 INNER JOIN EQU000 ON CAR003.cuarto = EQU000.id WHERE CAR003.codigo = @_consecutivo)),
			codigolab=(SELECT EQU000.codigo FROM CAR003 INNER JOIN EQU000 ON CAR003.cuarto = EQU000.id WHERE CAR003.codigo =@_consecutivo)
			WHERE consecutivo=@_consecutivo

	END ELSE IF @_accion = 'M' BEGIN
		UPDATE CAR000 SET fecha = GETDATE() WHERE consecutivo = @_consecutivo

		UPDATE CAR003 SET cod_ter=@_cod_ter,
			cuarto=@_cuarto,
			limite1=@_limite1,
			limite2=@_limite2,
			humedad=@_humedad,
			indicacion1=@_indicacion1,
			error1=@_error1,
			indicacion2=@_indicacion2,
			error2=@_error2,
			indicacion3=@_indicacion3,
			error3=@_error3,
			hr1=@_hr1,
			err_hr1=@_err_hr1,
			hr2=@_hr2,
			err_hr2=@_err_hr2,
			hr3=@_hr3,
			err_hr3=@_err_hr3, 
			obs=@_obs
			WHERE codigo = @_consecutivo
	END

	INSERT INTO CAR004 VALUES(@_consecutivo, @_fecha, @_hora, @_temp1, @_temp2, @_hume1, @_hume2, @_usuario)