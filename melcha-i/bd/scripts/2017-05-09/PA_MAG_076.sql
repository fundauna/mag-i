USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_076]    Script Date: 9/5/2017 13:24:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_076]
/*IME CARTA1 PASO1*/
	@_consecutivo CHAR(30),
	@_tipo CHAR(1),
	--
	@_tipo_bal CHAR(1),
	@_balanza SMALLINT,
	@_linealidad VARCHAR(11),
	@_repetibilidad VARCHAR(11),
	@_rango VARCHAR(20),
	@_decimales VARCHAR(4),
	@_cert1 VARCHAR(20),
	@_cert2 VARCHAR(20),
	@_cert3 VARCHAR(20),
	@_cert4 VARCHAR(20),
	@_pesa1 VARCHAR(20),
	@_pesa2 VARCHAR(20),
	@_pesa3 VARCHAR(20),
	@_pesa4 VARCHAR(20),
	@_nominal1 VARCHAR(11),
	@_nominal2 VARCHAR(11),
	@_nominal3 VARCHAR(11),
	@_nominal4 VARCHAR(11),	
	@_real1 VARCHAR(11),
	@_real2 VARCHAR(11),
	@_real3 VARCHAR(11),
	@_real4 VARCHAR(11),
	@_inc1 VARCHAR(11),
	@_inc2 VARCHAR(11),
	@_inc3 VARCHAR(11),
	@_inc4 VARCHAR(11),
	--
	@_lab CHAR(1),
	@_usuario SMALLINT,
	@_accion CHAR(1)
AS
	IF @_accion = 'I' BEGIN
		INSERT INTO CAR000 VALUES(@_consecutivo, GETDATE(), @_tipo, '0', @_lab,NULL,NULL)

		INSERT INTO CARBIT VALUES(@_consecutivo, GETDATE(), @_usuario, '0')
	
		INSERT INTO CAR007 VALUES(@_consecutivo,
			@_tipo_bal,
			@_balanza,
			@_linealidad,
			@_repetibilidad,
			@_rango,
			@_decimales,
			@_cert1,
			@_cert2,
			@_cert3,
			@_cert4,
			@_pesa1,
			@_pesa2,
			@_pesa3,
			@_pesa4,
			@_nominal1,
			@_nominal2,
			@_nominal3,
			@_nominal4,	
			@_real1,
			@_real2,
			@_real3,
			@_real4,
			@_inc1,
			@_inc2,
			@_inc3,
			@_inc4,
			NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
			NULL)

			UPDATE CAR000 SET 
			codigoalterno=(SELECT CASE WHEN MAX(codigoalterno) IS NULL THEN 1 ELSE MAX(codigoalterno)+1 END FROM [MAGI].[dbo].[CAR000] WHERE codigolab=(SELECT EQU000.codigo FROM CAR007 INNER JOIN EQU000 ON CAR007.balanza = EQU000.id WHERE CAR007.codigo=@_consecutivo)),
			codigolab=(SELECT EQU000.codigo FROM CAR007 INNER JOIN EQU000 ON CAR007.balanza = EQU000.id WHERE CAR007.codigo=@_consecutivo)
			WHERE consecutivo=@_consecutivo


	END ELSE IF @_accion = 'M' BEGIN
		UPDATE CAR000 SET fecha = GETDATE() WHERE consecutivo = @_consecutivo

		UPDATE CAR007 SET tipo_bal = @_tipo_bal,
			balanza=@_balanza,
			linealidad=@_linealidad,
			repetibilidad=@_repetibilidad,
			rango=@_rango,
			decimales=@_decimales,
			cert1=@_cert1,
			cert2=@_cert2,
			cert3=@_cert3,
			cert4=@_cert4,
			pesa1=@_pesa1,
			pesa2=@_pesa2,
			pesa3=@_pesa3,
			pesa4=@_pesa4,
			nominal1=@_nominal1,
			nominal2=@_nominal2,
			nominal3=@_nominal3,
			nominal4=@_nominal4,	
			real1=@_real1,
			real2=@_real2,
			real3=@_real3,
			real4=@_real4,
			inc1=@_inc1,
			inc2=@_inc2,
			inc3=@_inc3,
			inc4=@_inc4
			WHERE codigo = @_consecutivo
	END