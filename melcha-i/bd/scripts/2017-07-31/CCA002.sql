/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  ronald
 * Created: 31/07/2017
 */

USE MAGI;
GO
ALTER TABLE CCA002 
ALTER COLUMN version CHAR(2);
GO
ALTER TABLE CCA005
DROP CONSTRAINT PK_CCA005;
GO
ALTER TABLE CCA005 
ALTER COLUMN version CHAR(2) NOT NULL;
GO
ALTER TABLE CCA005
ADD CONSTRAINT PK_CCA005 PRIMARY KEY (padre, version);