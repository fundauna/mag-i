
ALTER TABLE MAG000
ADD camaras int DEFAULT 1;

ALTER TABLE MAG000
ADD cajas int DEFAULT 1;

UPDATE MAG000
SET cajas = 1,
    camaras = 1;

CREATE TABLE BAN005 (
  id int NOT NULL,
  descripcion varchar(200),
  estado char(1)
);

ALTER TABLE BAN005
ADD CONSTRAINT PK_BAN005 PRIMARY KEY (id);

CREATE TABLE BAN006 (
  id_camara int NOT NULL,
  linea int NOT NULL,
  descripcion varchar(200),
  dimensionx float,
  dimensiony float,
  rotulox char(1),
  rotuloy char(1)
);

ALTER TABLE BAN006
ADD CONSTRAINT PK_BAN006 PRIMARY KEY (id_camara, linea);

ALTER TABLE BAN006
ADD CONSTRAINT FK_BAN006 FOREIGN KEY (id_camara) REFERENCES BAN005 (id);

CREATE TABLE BAN007 (
  id int NOT NULL,
  id_camara int NOT NULL,
  bandeja_linea int NOT NULL,
  nombre varchar(200),
  ubicacion varchar(200)
);

ALTER TABLE BAN007
ADD CONSTRAINT PK_BAN007 PRIMARY KEY (id);

ALTER TABLE BAN007
ADD CONSTRAINT FK_BAN007_camara_linea FOREIGN KEY (id_camara, bandeja_linea) REFERENCES BAN006 (id_camara, linea);


CREATE TABLE BAN008 (
  id_caja int NOT NULL,
  id_plaga smallint NOT NULL
);

ALTER TABLE BAN008
ADD CONSTRAINT PK_BAN008 PRIMARY KEY (id_caja, id_plaga);

ALTER TABLE BAN008
ADD CONSTRAINT FK_BAN008_caja FOREIGN KEY (id_caja) REFERENCES BAN007 (id);

ALTER TABLE BAN008
ADD CONSTRAINT FK_BAN008_plaga FOREIGN KEY (id_plaga) REFERENCES VAR005 (id);