USE [MAGI]
GO
ALTER TABLE INV062
    ALTER COLUMN descripcion varchar(100) NOT NULL;
GO
ALTER TABLE INV064
    ALTER COLUMN material varchar(100) NOT NULL;
GO
ALTER TABLE INV063
    ALTER COLUMN descripcion varchar(100) NOT NULL;
GO
ALTER TABLE INV063
    ALTER COLUMN orden varchar(100) NOT NULL;
GO
ALTER TABLE INV063
    ALTER COLUMN factura varchar(100) NOT NULL;
GO
ALTER TABLE INV063
    ALTER COLUMN proveedor varchar(100) NOT NULL;
GO