/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  TI
 * Created: 27/04/2017
 */

USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_003]    Script Date: 27/04/2017 10:19:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_003]
/*MANTENIMIENTO USUARIOS*/
	@_ID SMALLINT,
	@_CEDULA CHAR(15), 
	@_NOMBRE VARCHAR(30), 
	@_AP1 VARCHAR(30),
	@_AP2 VARCHAR(30),
	@_EMAIL VARCHAR(50),
	@_PERFIL SMALLINT, 
	@_ESTADO CHAR(1), 
	@_LOGIN VARCHAR(15), 
	@_CLAVE VARCHAR(20),
	@_ACCION CHAR(1),
	@_LAB CHAR(1)
AS
	DECLARE @id SMALLINT

	--BORRA EL PERFIL ACTUAL QUE TENGA PARA ESTE LAB
	DELETE FROM SEG007 WHERE (usuario = @_ID) AND (perfil IN (
		SELECT SEG007.perfil FROM SEG007 INNER JOIN SEG002 ON SEG007.perfil = SEG002.id 
		WHERE (SEG002.lab = @_LAB) AND (SEG007.usuario = @_ID)
	))

	IF @_ACCION = 'I' BEGIN
		SET @id = (SELECT usuarios FROM MAG000)
		INSERT INTO SEG001 VALUES(@id, 
			@_CEDULA, 
			@_NOMBRE,
			@_AP1, @_AP2,
			@_EMAIL,
			@_ESTADO,
			@_LOGIN,
			@_CLAVE)
		
		IF @@ERROR = 0 BEGIN
			UPDATE MAG000 SET usuarios = usuarios + 1			
			--INGRESA PERFIL
			INSERT INTO SEG007 VALUES(@id, @_PERFIL)
		END
	END ELSE IF @_ACCION = 'M' BEGIN
		UPDATE SEG001 SET cedula = @_CEDULA, 
			nombre = @_NOMBRE,
			ap1 = @_AP1, ap2 = @_AP2,
			email = @_EMAIL,
			estado = @_ESTADO,
			[login] = @_LOGIN,
			clave = @_CLAVE
			WHERE id = @_ID

		--INGRESA PERFIL
		IF (@_PERFIL <> '') INSERT INTO SEG007 VALUES(@_ID, @_PERFIL)
	END ELSE IF @_ACCION = 'D' BEGIN
		UPDATE SEG001 SET	estado = @_ESTADO
		WHERE id = @_ID
	END