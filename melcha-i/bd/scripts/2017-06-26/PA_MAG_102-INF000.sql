/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  TI
 * Created: 26/06/2017
 */

USE MAGI;
GO
DECLARE @temporal char(14); 
SELECT @temporal=id FROM INF000 WHERE ref LIKE '%LCC-2016-334%';
EXECUTE PA_MAG_102 @temporal, 'LCC-2016-199', '', '14', '3';
GO
UPDATE INF000 SET estado = '2' WHERE ref LIKE '%LCC-2016-334%';