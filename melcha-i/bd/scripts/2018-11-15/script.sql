USE [MAGI]
GO
ALTER TABLE SER006
DROP CONSTRAINT PK_SER006;
GO
ALTER TABLE SER006
ALTER COLUMN lote varchar(20) NOT NULL; 
GO
ALTER TABLE SER006
ADD CONSTRAINT PK_SER006 PRIMARY KEY (solicitud, cod_ext, lote);
GO