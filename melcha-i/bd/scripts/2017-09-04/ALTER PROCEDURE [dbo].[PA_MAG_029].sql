USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_029]    Script Date: 04/09/2017 11:26:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_029]
/*MANTENIMIENTO PROVEEDORES*/
	@_CS SMALLINT,
	@_ACCION CHAR(1),
	@_TIPO CHAR(1),
	@_NATURALEZA CHAR(1),
	@_ID CHAR(100),
	@_NOMBRE VARCHAR(100),
	@_DIRECCION VARCHAR(200),
	@_TELEFONO VARCHAR(15),
	@_FAX VARCHAR(15),
	@_OTRO VARCHAR(15),
	@_EMAIL VARCHAR(50),
	@_NOTAS VARCHAR(200),
	@_CLASIFICACION SMALLINT,
	@_LRE BIT,
	@_LDP BIT,
	@_LCC BIT,
	@_CRITICO BIT
AS
	IF @_ACCION = 'I' BEGIN
		INSERT INTO MAN003 VALUES(@_CS,
			@_ID,
			@_NOMBRE,
			@_TIPO,
			@_NATURALEZA,
			@_DIRECCION,
			@_TELEFONO,
			@_FAX,
			@_OTRO,
			@_EMAIL,
			@_NOTAS,
			@_CLASIFICACION,			
			@_LRE,
			@_LDP,
			@_LCC,
			@_CRITICO)

		IF @@ERROR = 0 
				UPDATE MAG000 SET proveedores = proveedores + 1
	END ELSE IF @_ACCION = 'M'
		UPDATE MAN003 SET id=@_ID,
			nombre = @_NOMBRE,
			tipo = @_TIPO,
			naturaleza = @_NATURALEZA,
			direccion = @_DIRECCION,
			telefono = @_TELEFONO,
			fax = @_FAX,
			otro = @_OTRO,
			correo = @_EMAIL,
			notas = @_NOTAS,
			clasificacion = @_CLASIFICACION,
			LRE = @_LRE,
			LDP = @_LDP,
			LCC = @_LCC,
			critico = @_CRITICO
			WHERE cs = @_CS
	ELSE IF @_ACCION = 'D' BEGIN
		DELETE FROM MAN006 WHERE proveedor = @_CS
		DELETE FROM MAN003 WHERE cs = @_CS
	END