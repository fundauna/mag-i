DROP PROCEDURE [dbo].[PA_MAG_102]

 CREATE PROCEDURE [dbo].[Pa_mag_102]
  /*CONVIERTE UN INFORME TEMPORAL A OFICIAL*/
  @_temporal CHAR(14),
  @_nuevo    CHAR(14),
  @_obs      VARCHAR(500),
  @_usuario  SMALLINT,
  @_lab      CHAR(1)
AS
    SET nocount ON

    DECLARE @ref       VARCHAR(50),
            @tipo      CHAR(1),
            @lab       CHAR(1),
            @muestra   CHAR(18),
            @solicitud CHAR(13),
            @total     SMALLINT

    SET @ref = (SELECT ref
                FROM   inf000
                WHERE  ( id = @_temporal ))
    SET @tipo = (SELECT tipo
                 FROM   inf000
                 WHERE  ( id = @_temporal ))
    SET @muestra = (SELECT TOP(1) muestra
                    FROM   inf001
                    WHERE  ( informe = @_temporal ))
    SET @solicitud = (SELECT solicitud
                      FROM   mue000
                      WHERE  ( id = @muestra ))

    --INGRESA REPORTE OFICIAL
    -- Verificar si existe informe eliminado
    IF( (SELECT 1
         FROM   inf001
         WHERE  informe = @_nuevo) IS NULL )
      BEGIN
          INSERT INTO inf000
          VALUES     (@_nuevo,
                      Getdate(),
                      @ref,
                      @_obs,
                      '1',
                      @tipo,
                      @_lab)
      END
    ELSE
      BEGIN
          UPDATE inf000
          SET    fecha = Getdate(),
                 [ref] = @ref,
                 obs = @_obs,
                 estado = '1',
                 tipo = @tipo,
                 lab = @_lab
          WHERE  id = @_nuevo;
      END

    IF @@ERROR = 0
      BEGIN
          --INGRESA EN BITACORA DE INFORME
          INSERT INTO infbit
          VALUES     (@_nuevo,
                      @_usuario,
                      Getdate(),
                      '1')

          --ACTUALIZA ENLACES
          IF( (SELECT 1
               FROM   inf001
               WHERE  informe = @_nuevo) IS NULL )
            BEGIN
                UPDATE inf001
                SET    informe = @_nuevo
                WHERE  ( informe = @_temporal );
            END
          ELSE
            BEGIN
                DELETE FROM inf001
                WHERE  ( informe = @_temporal );
            END;

          UPDATE inf005
          SET    informe = @_nuevo
          WHERE  ( informe = @_temporal )

          --BORRA INFORME TEMPOTAL
          DELETE FROM inf000
          WHERE  ( id = @_temporal )

          --CAMBIA ESTADO LA MUESTRA
          UPDATE mue000
          SET    estado = '3'
          WHERE  ( id IN (SELECT muestra
                          FROM   inf001
                          WHERE  ( informe = @_nuevo )) )

          --INGRESA EN BITACORA DE MUESTRAS
          DECLARE c1 CURSOR FOR
            SELECT muestra
            FROM   inf001
            WHERE  ( informe = @_temporal )

          OPEN c1

          FETCH next FROM c1 INTO @muestra

          WHILE @@FETCH_STATUS = 0
            BEGIN
                INSERT INTO muebit
                VALUES     (@muestra,
                            @_usuario,
                            '3',
                            Getdate(),
                            NULL)

                FETCH next FROM c1 INTO @muestra
            END

          CLOSE c1

          DEALLOCATE c1

          --OBTIENE EL NUMERO DE MUESTRAS EN REPORTE PARA VER SI PUEDE FINALIZAR LA SOLICITUD
          SET @total = (SELECT Count(1) AS total
                        FROM   mue000
                        WHERE  ( solicitud = @solicitud )
                               AND ( estado <> '3' ))

          IF( @total = 0 )
            BEGIN--SI TODAS ESTAN EN ESTADO 3, PUEDO BLOQUEAR LA SOLICITUD
                IF( @_LAB = '1' )
                  UPDATE ser001
                  SET    estado = '7'
                  WHERE  ( cs = @solicitud )
                ELSE IF( @_LAB = '3' )
                  UPDATE ser003
                  SET    estado = '7'
                  WHERE  ( cs = @solicitud )
            END
      END;  