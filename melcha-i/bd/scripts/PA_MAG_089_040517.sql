/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  TI
 * Created: 04/05/2017
 */

USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_089]    Script Date: 04/05/2017 15:59:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_089]
/*LISTA DE MUESTRAS POR AIGNAR*/
	@_LAB CHAR(1),
	@_REASIGNAR CHAR(1)
AS
	IF @_REASIGNAR = ''
		SELECT MUE000.id, MUE000.solicitud, MUE000.ref, MUE000.estado, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fecha1
		FROM MUE000 INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra
		WHERE (MUE000.id LIKE @_LAB + '-%') AND (estado = '0')
		ORDER BY MUE000.solicitud, MUE000.id;
	ELSE
		SELECT MUE000.id, MUE000.solicitud, MUE000.ref, S1.id AS actual, (S1.nombre +' '+ S1.ap1) AS analista, S2.id AS actual2, (S2.nombre +' '+ S2.ap1) AS analista2
		FROM MUE000 INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN SEG001 AS S1 ON MUEBIT.usuario = S1.id LEFT JOIN SEG001 AS S2 ON MUEBIT.usuario2 = S2.id
		WHERE (MUE000.id LIKE @_LAB + '-%') AND (MUE000.estado = '1') AND (MUEBIT.accion = 'A')
		ORDER BY MUE000.solicitud, MUE000.id;