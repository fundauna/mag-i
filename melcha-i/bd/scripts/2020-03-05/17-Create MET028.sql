USE [MAGI]
GO

/****** Object:  Table [dbo].[MET028]    Script Date: 11/12/2019 13:51:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MET028]
(
    [ensayo]     [char](10)   NOT NULL,
    [fechaA] [varchar](50) NULL,
[repeticiones] [varchar](50) NULL,
[recipientemasa1] [varchar](50) NULL,
[recipienteerror1] [varchar](50) NULL,
[recipientemasacorregida1] [varchar](50) NULL,
[recipientemasa2] [varchar](50) NULL,
[recipienteerror2] [varchar](50) NULL,
[recipientemasacorregida2] [varchar](50) NULL,
[recipientemuestramasa1] [varchar](50) NULL,
[recipientemuestraerror1] [varchar](50) NULL,
[recipientemuestramasacorregida1] [varchar](50) NULL,
[recipientemuestramasa2] [varchar](50) NULL,
[recipientemuestraerror2] [varchar](50) NULL,
[recipientemuestramasacorregida2] [varchar](50) NULL,
[recipientefinalmasa1] [varchar](50) NULL,
[recipientefinalerror1] [varchar](50) NULL,
[recipientefinalmasacorregida1] [varchar](50) NULL,
[recipientefinalmasa2] [varchar](50) NULL,
[recipientefinalerror2] [varchar](50) NULL,
[recipientefinalmasacorregida2] [varchar](50) NULL,
[masarecipiente1] [varchar](50) NULL,
[masarecipiente2] [varchar](50) NULL,
[masarecipientepromedio] [varchar](50) NULL,
[masainicialrecipiente1] [varchar](50) NULL,
[masainicialrecipiente2] [varchar](50) NULL,
[masainicialrecipientepromedio] [varchar](50) NULL,
[masainicialmuestra1] [varchar](50) NULL,
[masainicialmuestra2] [varchar](50) NULL,
[masainicialmuestrapromedio] [varchar](50) NULL,
[masafinalrecipiente1] [varchar](50) NULL,
[masafinalrecipiente2] [varchar](50) NULL,
[masafinalrecipientepromedio] [varchar](50) NULL,
[masafinalmuestra1] [varchar](50) NULL,
[masafinalmuestra2] [varchar](50) NULL,
[masafinalmuestrapromedio] [varchar](50) NULL,
[humedad1] [varchar](50) NULL,
[humedad2] [varchar](50) NULL,
[humedadpromedio] [varchar](50) NULL,
[promediohumedad] [varchar](50) NULL,
[humedaddesvest] [varchar](50) NULL,
[repetibilidadval] [varchar](50) NULL,
[repetibilidadcomp] [varchar](50) NULL,
[emrval] [varchar](50) NULL,
[emvcomp] [varchar](50) NULL,
[resval] [varchar](50) NULL,
[rescomp] [varchar](50) NULL,
[linealval] [varchar](50) NULL,
[linealcomp] [varchar](50) NULL,
[excentricidadval] [varchar](50) NULL,
[excentricidadcomp] [varchar](50) NULL,
[incindcomp] [varchar](50) NULL,
[incnumeradorcomp] [varchar](50) NULL,
[incpesajecomp] [varchar](50) NULL,
[duplicadoscomp] [varchar](50) NULL,
[incdecomp] [varchar](50) NULL,
[incfinalcombcomp] [varchar](50) NULL,
[inxexp] [varchar](50) NULL,
[obs] [varchar](500) NULL
    CONSTRAINT [PK_MET_028] PRIMARY KEY CLUSTERED
        (
         ensayo ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

