USE [MAGI]
GO

/****** Object:  Table [dbo].[MET027]    Script Date: 11/12/2019 13:51:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MET027POTASIO]
(
    [id]     [smallint]    NOT NULL,
    [potasiocon1] [varchar](50) NULL,
[potasiocon2] [varchar](50) NULL,
[potasiocon3] [varchar](50) NULL,
[potasiocon4] [varchar](50) NULL,
[potasiocon5] [varchar](50) NULL,
[potasioinc1] [varchar](50) NULL,
[potasioinc2] [varchar](50) NULL,
[potasioinc3] [varchar](50) NULL,
[potasioinc4] [varchar](50) NULL,
[potasioinc5] [varchar](50) NULL,
[potasioincrel1] [varchar](50) NULL,
[potasioincrel2] [varchar](50) NULL,
[potasioincrel3] [varchar](50) NULL,
[potasioincrel4] [varchar](50) NULL,
[potasioincrel5] [varchar](50) NULL,
[potasioAA11] [varchar](50) NULL,
[potasioAA12] [varchar](50) NULL,
[potasioAA13] [varchar](50) NULL,
[potasioAA14] [varchar](50) NULL,
[potasioAA15] [varchar](50) NULL,
[potasioA21] [varchar](50) NULL,
[potasioA22] [varchar](50) NULL,
[potasioA23] [varchar](50) NULL,
[potasioA24] [varchar](50) NULL,
[potasioA25] [varchar](50) NULL,
[potasioA31] [varchar](50) NULL,
[potasioA32] [varchar](50) NULL,
[potasioA33] [varchar](50) NULL,
[potasioA34] [varchar](50) NULL,
[potasioA35] [varchar](50) NULL,
[potasioA41] [varchar](50) NULL,
[potasioA42] [varchar](50) NULL,
[potasioA43] [varchar](50) NULL,
[potasioA44] [varchar](50) NULL,
[potasioA45] [varchar](50) NULL,
[potasioCC11] [varchar](50) NULL,
[potasioCC12] [varchar](50) NULL,
[potasioCC13] [varchar](50) NULL,
[potasioCC14] [varchar](50) NULL,
[potasioCC15] [varchar](50) NULL,
[potasioC21] [varchar](50) NULL,
[potasioC22] [varchar](50) NULL,
[potasioC23] [varchar](50) NULL,
[potasioC24] [varchar](50) NULL,
[potasioC25] [varchar](50) NULL,
[potasioC31] [varchar](50) NULL,
[potasioC32] [varchar](50) NULL,
[potasioC33] [varchar](50) NULL,
[potasioC34] [varchar](50) NULL,
[potasioC35] [varchar](50) NULL,
[potasioC41] [varchar](50) NULL,
[potasioC42] [varchar](50) NULL,
[potasioC43] [varchar](50) NULL,
[potasioC44] [varchar](50) NULL,
[potasioC45] [varchar](50) NULL,
[potasiocp1] [varchar](50) NULL,
[potasiocp2] [varchar](50) NULL,
[potasiocp3] [varchar](50) NULL,
[potasiocp4] [varchar](50) NULL,
[potasiocp5] [varchar](50) NULL,
[potasioa1] [varchar](50) NULL,
[potasioa2] [varchar](50) NULL,
[potasioa3] [varchar](50) NULL,
[potasioa4] [varchar](50) NULL,
[potasioa5] [varchar](50) NULL,
[potasioa6] [varchar](50) NULL,
[potasioa7] [varchar](50) NULL,
[potasioa8] [varchar](50) NULL,
[potasioa9] [varchar](50) NULL,
[potasioa10] [varchar](50) NULL,
[potasioa11] [varchar](50) NULL,
[potasioa12] [varchar](50) NULL,
[potasioa13] [varchar](50) NULL,
[potasioa14] [varchar](50) NULL,
[potasioa15] [varchar](50) NULL,
[potasioa16] [varchar](50) NULL,
[potasioa17] [varchar](50) NULL,
[potasioa18] [varchar](50) NULL,
[potasioa19] [varchar](50) NULL,
[potasioa20] [varchar](50) NULL,
[potasioc1] [varchar](50) NULL,
[potasioc2] [varchar](50) NULL,
[potasioc3] [varchar](50) NULL,
[potasioc4] [varchar](50) NULL,
[potasioc5] [varchar](50) NULL,
[potasioc6] [varchar](50) NULL,
[potasioc7] [varchar](50) NULL,
[potasioc8] [varchar](50) NULL,
[potasioc9] [varchar](50) NULL,
[potasioc10] [varchar](50) NULL,
[potasioc11] [varchar](50) NULL,
[potasioc12] [varchar](50) NULL,
[potasioc13] [varchar](50) NULL,
[potasioc14] [varchar](50) NULL,
[potasioc15] [varchar](50) NULL,
[potasioc16] [varchar](50) NULL,
[potasioc17] [varchar](50) NULL,
[potasioc18] [varchar](50) NULL,
[potasioc19] [varchar](50) NULL,
[potasioc20] [varchar](50) NULL,
[potasiomaxinc] [varchar](50) NULL,
[potasiomaxincrel] [varchar](50) NULL,
[potasionummedxmuestra] [varchar](50) NULL,
[potasionummedxpuncurva] [varchar](50) NULL,
[potasionummedcurva] [varchar](50) NULL,
[potasioconmuestra1] [varchar](50) NULL,
[potasioconmuestra2] [varchar](50) NULL,
[potasioprompatcalibracion] [varchar](50) NULL,
[potasiodesvestresidual] [varchar](50) NULL,
[potasiodesvestcurvacali] [varchar](50) NULL,
[potasionp] [varchar](50) NULL,
[potasiolote] [varchar](50) NULL,
[potasiovence] [varchar](50) NULL,
[potasiocc1] [varchar](50) NULL,
[potasiocc2] [varchar](50) NULL,
[potasioest1] [varchar](50) NULL,
[potasioest2] [varchar](50) NULL,
[potasioccest1] [varchar](50) NULL,
[potasioccest2] [varchar](50) NULL,
[potasiovalor] [varchar](50) NULL,
[potasioincexp] [varchar](50) NULL,
[potasiok] [varchar](50) NULL,
[potasioincest] [varchar](50) NULL,
[potasioestandar1] [varchar](50) NULL,
[potasioestandar2] [varchar](50) NULL,
[potasioestandar3] [varchar](50) NULL,
[potasioestandar4] [varchar](50) NULL,
[potasioestandar5] [varchar](50) NULL,
[potasiopipeta1] [varchar](50) NULL,
[potasiopipeta2] [varchar](50) NULL,
[potasiopipeta3] [varchar](50) NULL,
[potasiopipeta4] [varchar](50) NULL,
[potasiopipeta5] [varchar](50) NULL,
[potasiovolcorrpipeta1] [varchar](50) NULL,
[potasiovolcorrpipeta2] [varchar](50) NULL,
[potasiovolcorrpipeta3] [varchar](50) NULL,
[potasiovolcorrpipeta4] [varchar](50) NULL,
[potasiovolcorrpipeta5] [varchar](50) NULL,
[potasioincpip1] [varchar](50) NULL,
[potasioincpip2] [varchar](50) NULL,
[potasioincpip3] [varchar](50) NULL,
[potasioincpip4] [varchar](50) NULL,
[potasioincpip5] [varchar](50) NULL,
[potasiobalon1] [varchar](50) NULL,
[potasiobalon2] [varchar](50) NULL,
[potasiobalon3] [varchar](50) NULL,
[potasiobalon4] [varchar](50) NULL,
[potasiobalon5] [varchar](50) NULL,
[potasiodilinc1] [varchar](50) NULL,
[potasiodilinc2] [varchar](50) NULL,
[potasiodilinc3] [varchar](50) NULL,
[potasiodilinc4] [varchar](50) NULL,
[potasiodilinc5] [varchar](50) NULL,

    [estado] [char](1) NULL,
    CONSTRAINT [PK_MET_027potasio] PRIMARY KEY CLUSTERED
        (
         [id] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

