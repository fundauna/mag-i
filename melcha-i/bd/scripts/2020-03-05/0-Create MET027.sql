USE [MAGI]
GO

/****** Object:  Table [dbo].[MET027]    Script Date: 11/12/2019 13:51:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MET027]
(
    [id]     [smallint]    NOT NULL,
    [fecha] [date] NULL,
    [estado] [char](1) NULL,
    CONSTRAINT [PK_MET_027] PRIMARY KEY CLUSTERED
        (
         [id] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

