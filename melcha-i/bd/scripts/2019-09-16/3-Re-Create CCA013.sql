USE [MAGI]
GO
DROP TABLE [dbo].[CCA013]
GO

/****** Object:  Table [dbo].[CCA013]    Script Date: 9/10/2019 10:57:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CCA013]
(
    [hoja]       [varchar](15)  NOT NULL,
    [codigo]     [varchar](50)  NULL,
    [version]    [varchar](max) NULL,
    [nombre]     [varchar](max) NULL,
    [rige]       [varchar](max) NULL,
    [encabezado] [smallint]     NULL,
    [pie]        [smallint]     NULL,
    [enombre]    [varchar](200) NULL,
    [epuesto]    [varchar](200) NULL,
    [efecha]     [varchar](200) NULL,
    [rnombre]    [varchar](200) NULL,
    [rpuesto]    [varchar](200) NULL,
    [rfecha]     [varchar](200) NULL,
    [anombre]    [varchar](200) NULL,
    [apuesto]    [varchar](200) NULL,
    [afecha]     [varchar](200) NULL,
    CONSTRAINT [PK_CCA013] PRIMARY KEY CLUSTERED
        (
         [hoja] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


