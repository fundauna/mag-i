/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  TI
 * Created: 05/05/2017
 */

USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_091]    Script Date: 05/05/2017 11:06:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_091]
/*ANALISIS PENDIENTES*/
	@_LAB CHAR(1),
	@_ROL SMALLINT,
	@_USUARIO SMALLINT
AS
	IF @_ROL = 0 --SI ES ADMIN TIRA TODAS 
		SELECT MUE000.solicitud, MUE000.ref, MUE001.id AS xanalizar, VAR005.nombre
		FROM MUE000 INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN MUE001 ON MUE000.id = MUE001.muestra INNER JOIN VAR005 ON MUE001.analisis = VAR005.id
		WHERE(VAR005.lab = @_LAB) AND (MUE001.estado = 0) AND (MUEBIT.accion = 'A')
		ORDER BY VAR005.tipo, VAR005.nombre
	ELSE
		SELECT MUE000.solicitud, MUE000.ref, MUE001.id AS xanalizar, VAR005.nombre
		FROM MUE000 INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN MUE001 ON MUE000.id = MUE001.muestra INNER JOIN VAR005 ON MUE001.analisis = VAR005.id
		WHERE(VAR005.lab = @_LAB) AND (MUE001.estado = 0) AND (MUEBIT.usuario = @_USUARIO OR MUEBIT.usuario2 = @_USUARIO) AND (MUEBIT.accion = 'A')
		ORDER BY VAR005.tipo, VAR005.nombre
