/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  TI
 * Created: 17/05/2017
 */

USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_071]    Script Date: 17/05/2017 11:56:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_071]
/*MANTENIMIENTO DE PESAS*/
	@_ocodigo CHAR(20), 
	@_onominal VARCHAR(20), 
	@_codigo CHAR(20), 
	@_codigocer CHAR(20), 
	@_nominal VARCHAR(20),
	@_fecha SMALLDATETIME,
	@_vreal VARCHAR(20),
	@_inc VARCHAR(20),
	@_certificado VARCHAR(20), 
	@_activo BIT,
	@_lab CHAR(1), 
	@_accion CHAR(1)
AS
	IF @_accion = 'I'
		INSERT INTO VAR002 VALUES(@_codigo, @_codigocer, @_nominal, @_fecha, @_vreal, @_inc, @_certificado, @_activo, @_lab)
	ELSE IF @_accion = 'M'
		UPDATE VAR002 SET codigo = @_codigo,		
		codigocer = @_codigocer,
		nominal = @_nominal, 
		fecha = @_fecha,
		vreal = @_vreal,
		inc = @_inc,
		certificado = @_certificado,
		activo = @_activo
		WHERE (codigo = @_ocodigo) AND (nominal = @_onominal)
	ELSE IF @_accion = 'D'
		DELETE FROM VAR002 WHERE (codigo = @_codigo) AND (nominal = @_nominal)