USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_147]    Script Date: 25/04/2017 12:47:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_147]

	@_IdViejo CHAR(15),
	@_Idnuevo CHAR(15)
AS

	BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[MAN002]
           ([id]
           ,[nombre]
           ,[unidad]
           ,[representante]
           ,[tel]
           ,[email]
           ,[fax]
           ,[direccion]
           ,[actividad]
           ,[tipo]
           ,[LRE]
           ,[LDP]
           ,[LCC]
           ,[estado]
           ,[clave])
      
    SELECT  @_Idnuevo
      ,[nombre]
      ,[unidad]
      ,[representante]
      ,[tel]
      ,[email]
      ,[fax]
      ,[direccion]
      ,[actividad]
      ,[tipo]
      ,[LRE]
      ,[LDP]
      ,[LCC]
      ,[estado]
      ,[clave]
  FROM [dbo].[MAN002] WHERE [id]=@_IdViejo;

	UPDATE [dbo].[CER001] SET cliente=@_Idnuevo WHERE cliente=@_IdViejo;
	UPDATE [dbo].[COT000] SET cliente=@_Idnuevo WHERE cliente=@_IdViejo;
        UPDATE [dbo].[COT005] SET cliente=@_Idnuevo WHERE cliente=@_IdViejo;
	UPDATE [dbo].[MAN013] SET cliente=@_Idnuevo WHERE cliente=@_IdViejo;
	UPDATE [dbo].[MAN014] SET cliente=@_Idnuevo WHERE cliente=@_IdViejo;
	UPDATE [dbo].[SCL004] SET cliente=@_Idnuevo WHERE cliente=@_IdViejo;
	UPDATE [dbo].[SCL007] SET cliente=@_Idnuevo WHERE cliente=@_IdViejo;
	UPDATE [dbo].[SER001] SET cliente=@_Idnuevo WHERE cliente=@_IdViejo;
	UPDATE [dbo].[SER003] SET cliente=@_Idnuevo WHERE cliente=@_IdViejo;

	DELETE FROM [dbo].[MAN002] WHERE [id]=@_IdViejo;
   
END
