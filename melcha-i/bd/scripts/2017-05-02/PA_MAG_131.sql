USE [MAGI]
GO
/****** Object:  StoredProcedure [dbo].[PA_MAG_131]    Script Date: 2/5/2017 14:20:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PA_MAG_131]
/*CREA TABLA DE CAMPOS ADICIONALES DE INVENTARIO*/
	@_TEMP CHAR(10),
	@_FICHA SMALLINT
AS
	SET NOCOUNT ON

	DECLARE @nombre VARCHAR(40), @tamano SMALLINT, @tabla CHAR(3), @valor VARCHAR(500),@id smallint
	--BORRA EL CONTENIDO ACTUAL DE LA TABLA
	DELETE FROM INVTMP WHERE temporal = @_TEMP

	DECLARE C1 CURSOR FOR SELECT INVPLA.nombre, INVPLA.tamano, SUBSTRING(INVPLA.tabla, 4, 3) AS tabla,INVPLA.id 
	FROM INV000 INNER JOIN INV002 ON INV000.id = INV002.familia INNER JOIN INVOPC ON INV000.id = INVOPC.familia INNER JOIN INVPLA ON INVOPC.opcional = INVPLA.id
	WHERE(INV002.id = @_FICHA)
	ORDER BY INVPLA.nombre

	OPEN C1
	FETCH NEXT FROM C1 INTO @nombre, @tamano, @tabla,@id
	WHILE @@FETCH_STATUS = 0
	BEGIN
		--TABLAS OPCIONALES
		IF @tabla = '006' SET @valor = (SELECT parte FROM INV006 WHERE producto = @_FICHA)
		ELSE IF @tabla = '005' SET @valor = (SELECT rango FROM INV005 WHERE producto = @_FICHA)
		ELSE IF @tabla = '008' SET @valor = (SELECT polaridad FROM INV008 WHERE producto = @_FICHA)
		ELSE IF @tabla = '009' SET @valor = (SELECT patrimonio FROM INV009 WHERE producto = @_FICHA)
		ELSE IF @tabla = '010' SET @valor = (SELECT dimensiones FROM INV010 WHERE producto = @_FICHA)
		ELSE IF @tabla = '011' SET @valor = (SELECT fase FROM INV011 WHERE producto = @_FICHA)
		ELSE IF @tabla = '012' SET @valor = (SELECT otros FROM INV012 WHERE producto = @_FICHA)
		ELSE IF @tabla = '013' SET @valor = (SELECT peligrosidad FROM INV013 WHERE producto = @_FICHA)
		ELSE IF @tabla = '014' SET @valor = (SELECT especial FROM INV014 WHERE producto = @_FICHA)
		ELSE IF @tabla = '015' SET @valor = (SELECT temperatura FROM INV015 WHERE producto = @_FICHA)
		ELSE IF @tabla = '016' SET @valor = (SELECT serie FROM INV016 WHERE producto = @_FICHA)
		ELSE IF @tabla = '021' SET @valor = (SELECT vencimiento FROM INV021 WHERE producto = @_FICHA)
		ELSE IF @tabla = '022' SET @valor = (SELECT estabilidad FROM INV022 WHERE producto = @_FICHA)
		ELSE IF @tabla = '023' SET @valor = (SELECT metodo FROM INV023 WHERE producto = @_FICHA)
		ELSE IF @tabla = '024' SET @valor = (SELECT procedencia FROM INV024 WHERE producto = @_FICHA)
		ELSE IF @tabla = '025' SET @valor = (SELECT ubicacion FROM INV025 WHERE producto = @_FICHA)
		ELSE IF @tabla = '049' SET @valor = (SELECT formula FROM INV049 WHERE producto = @_FICHA)
		ELSE IF @tabla = '050' SET @valor = (SELECT densidad FROM INV050 WHERE producto = @_FICHA)
		ELSE IF @tabla = '051' SET @valor = (SELECT matriz FROM INV051 WHERE producto = @_FICHA)
		ELSE IF @tabla = '052' SET @valor = (SELECT contenido FROM INV052 WHERE producto = @_FICHA AND id=@id)
		ELSE IF @tabla = '053' SET @valor = (SELECT fecha FROM INV053 WHERE producto = @_FICHA)
		ELSE IF @tabla = '054' SET @valor = (SELECT costo FROM INV054 WHERE producto = @_FICHA)
		ELSE IF @tabla = '055' SET @valor = (SELECT fondo FROM INV055 WHERE producto = @_FICHA)
		ELSE IF @tabla = '056' SET @valor = (SELECT modelo FROM INV056 WHERE producto = @_FICHA)
		ELSE IF @tabla = '057' SET @valor = (SELECT responsable FROM INV057 WHERE producto = @_FICHA)
		ELSE IF @tabla = '058' SET @valor = (SELECT lote FROM INV058 WHERE producto = @_FICHA)
		ELSE IF @tabla = '059' SET @valor = (SELECT requisicion FROM INV059 WHERE producto = @_FICHA)
		ELSE IF @tabla = '060' SET @valor = (SELECT pureza FROM INV060 WHERE producto = @_FICHA)
		ELSE IF @tabla = '061' SET @valor = (SELECT certificado FROM INV061 WHERE producto = @_FICHA)
		--HACE EL INSERT
		INSERT INTO INVTMP VALUES(@_TEMP, @nombre, @tamano, @tabla, @valor)
		--
		FETCH NEXT FROM C1 INTO @nombre, @tamano, @tabla,@id
	END
	
	CLOSE C1
	DEALLOCATE C1