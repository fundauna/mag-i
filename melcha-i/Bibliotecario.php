<?php
/*******************************************************************
CLASE DE NIVEL 0 UTILIZADA PARA SUBIR Y ZIPEAR FICHEROS 
(NO PUEDE SER HEREDADA NI INSTANCIADA)
*******************************************************************/
final class Bibliotecario{
	private function __construct(){}
	
	public static function ZipCrear($_nombre, $_tmp, $_ruta, $_sobrescribe=0){
		$_nombre = str_replace(' ','',$_nombre);
		$_ruta = str_replace(' ','',$_ruta);
		if($_sobrescribe == 0) if(file_exists($_ruta)) unlink($_ruta);
		$zip = new ZipArchive();
		if(($zip->open($_ruta, ZipArchive::CREATE))!==true) return false;			
		$zip->addFile($_tmp, $_nombre);				
		$zip->close();
		return true;
    }
	
	public static function ZipDescargar($_ruta){
		$_ruta = str_replace(' ','',$_ruta);
		if(!file_exists($_ruta)) die('Archivo inexistente');	
		header("Content-type: application/zip");
		header("Content-Disposition: attachment; filename=documento.zip");
		header("Content-Transfer-Encoding: binary");
		readfile($_ruta);
	}
	
	public static function ZipEliminar($_ruta){
		$_ruta = str_replace(' ','',$_ruta);
		@unlink($_ruta);
		return true;
	}
	
	public static function ZipSubir($_tmp, $_ruta){
		$_ruta = str_replace(' ','',$_ruta);
		@move_uploaded_file($_tmp, $_ruta);
		return true;
	}
}
?>