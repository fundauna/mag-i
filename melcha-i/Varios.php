<?php

/*******************************************************************
 * CLASE DE NIVEL 1 ENCARGADA DEL MANTENIMIENTOS VARIOS
 * (final SOLO PUEDE SER INSTANCIADA Y NO HEREDADA)
 *******************************************************************/
final class Varios extends Database
{
//	
    function AnalisisForMuestra($_dato)
    {
        return $this->_QUERY("SELECT VAR009.analisis, VAR005.nombre FROM VAR005 INNER JOIN VAR009 ON VAR005.id = VAR009.analisis WHERE (VAR009.formulacion = {$_dato}) ORDER BY VAR005.nombre;");
    }

    function AnalisisForIME($_formulacion, $_analisis, $_accion)
    {

        if ($_accion == 'I') {
            $this->_TRANS("INSERT INTO VAR009 VALUES({$_formulacion}, {$_analisis});");
            return 1;
        } elseif ($_accion == 'D') {
            $this->_TRANS("DELETE FROM VAR009 WHERE (formulacion = {$_formulacion}) AND (analisis = {$_analisis});");
            return 1;
        } else
            return 0;
    }

    function AnalisisMetodosIME($_analisis, $_metodo, $_accion)
    {
        $this->_TRANS("DELETE FROM MET001 WHERE (analisis = {$_analisis}) AND (metodo = {$_metodo});");
        if ($_accion == 'I')
            $this->_TRANS("INSERT INTO MET001 VALUES({$_analisis}, {$_metodo});");

        $this->Logger('C000');
        return 1;
    }

    function AnalisisMetodosMuestra($_analisis)
    {
        return $this->_QUERY("SELECT MET000.id, MET000.nombre, MET000.pagina
		FROM MET001 INNER JOIN MET000 ON MET001.metodo = MET000.id
		WHERE (MET001.analisis = {$_analisis}) ORDER BY MET000.nombre;");
    }

    function BalanzasCalMuestra()
    {
        return $this->_QUERY("SELECT EQU000.id, EQU000.codigo, EQU000.nombre, EQU010.id AS cs, CONVERT(VARCHAR,EQU010.fecha, 105) AS fecha, EQU010.nominal, EQU010.resultado, EQU010.error, EQU010.inc FROM EQU000 INNER JOIN EQU010 ON EQU000.id = EQU010.equipo;");
    }

    function BalanzasIME($_id, $_equipo, $_nominal, $_resultado, $_error, $_inc, $_accion)
    {
        $_nominal = str_replace(',', '.', $_nominal);
        $_resultado = str_replace(',', '.', $_resultado);
        $_error = str_replace(',', '.', $_error);
        $_inc = str_replace(',', '.', $_inc);

        if ($this->_TRANS("EXECUTE PA_MAG_142 '{$_id}', '{$_equipo}', '{$_nominal}', '{$_resultado}', '{$_error}', '{$_inc}', '{$_accion}';")) {
            //$this->Logger('C004');
            return 1;
        } else return 0;
    }

    function CamarasIME($_codigo, $_fecha, $_tipo, $_limite1, $_limite2, $_ubicacion, $_activo, $_lab, $_accion)
    {
        $_codigo = $this->Free($_codigo);
        $_ubicacion = $this->Free($_ubicacion);
        $_fecha = $this->_FECHA($_fecha);

        if ($this->_TRANS("EXECUTE PA_MAG_126 '{$_codigo}', '{$_fecha}', '{$_tipo}', '{$_limite1}', '{$_limite2}', '{$_ubicacion}', '{$_activo}', '{$_lab}', '{$_accion}';")) {
            $this->Logger('C004');
            return 1;
        } else return 0;
    }

    function CamarasResumenGet($_lab, $_activas = false)
    {
        if ($_activas) $ext = 'AND (activo=1)';
        else $ext = '';

        return $this->_QUERY("SELECT codigo, CONVERT(VARCHAR, fecha, 105) AS fecha, tipo, limite1, limite2, ubicacion, activo
		FROM VAR004
		WHERE (lab = '{$_lab}') {$ext} ORDER BY tipo, activo;");
    }

    function CatalogoGet($_lab)
    {
        return $this->_QUERY("SELECT INV002.id, INV002.codigo, INV002.nombre
		FROM INV000 INNER JOIN INV002 ON INV000.id = INV002.familia
		WHERE (INV000.lab = '{$_lab}') AND (INV002.es=1) AND (INV002.estado='1') ORDER BY INV002.nombre;");
    }

    function CuartoCalMuestra()
    {
        return $this->_QUERY("SELECT EQU012.id, EQU012.cuarto, CONVERT(VARCHAR,EQU012.fecha, 105) AS fecha, EQU012.temp1, EQU012.temp2, EQU012.hum1, EQU012.hum2 FROM EQU012;");
    }

    function CuartoIME($_id, $_cuarto, $_temp1, $_temp2, $_hum1, $_hum2, $_accion)
    {

        if ($this->_TRANS("EXECUTE PA_MAG_144 '{$_id}', '{$_cuarto}', '{$_temp1}', '{$_temp2}', '{$_hum1}', '{$_hum2}', '{$_accion}';")) {
            //$this->Logger('C004');
            return 1;
        } else return 0;
    }

    function EstN($_dato)
    {
        if ($this->_QUERY("SELECT ingrediente FROM VAR010 WHERE ingrediente = {$_dato};"))
            return '';
        else return ' (Sin asignacion)';
    }

    function EstM($_dato)
    {
        $O = $this->_QUERY("SELECT INV021.vencimiento, GETDATE() AS hoy FROM VAR010 INNER JOIN INV002 ON VAR010.estandar = INV002.id INNER JOIN INV021 ON dbo.INV002.id = INV021.producto WHERE VAR010.ingrediente = {$_dato} ORDER BY INV021.vencimiento DESC;");
        /*if($O && ($O[0]['hoy'] > $O[0]['vencimiento']))
            return ' (Vencido)';*/
        //else return '';
        return '';
    }

    function EstIngActMuestra($_dato)
    {
        return $this->_QUERY("SELECT VAR010.estandar, INV002.codigo, INV002.nombre FROM VAR010 INNER JOIN INV002 ON VAR010.estandar = INV002.id WHERE (VAR010.ingrediente = {$_dato}) ORDER BY INV002.nombre;");
    }

    function EstIngActIME($_ingrediente, $_estandar, $_accion)
    {

        if ($_accion == 'I') {
            $this->_TRANS("INSERT INTO VAR010 VALUES({$_ingrediente}, {$_estandar});");
            return 1;
        } elseif ($_accion == 'D') {
            $this->_TRANS("DELETE FROM VAR010 WHERE (ingrediente = {$_ingrediente}) AND (estandar = {$_estandar});");
            return 1;
        } else
            return 0;
    }

    function FormulacionesGet($_tipo)
    {
        return $this->_QUERY("SELECT id, nombre
		FROM VAR007
		WHERE (tipo = '{$_tipo}')
		ORDER BY nombre;");
    }

    function FormulacionesGets($_id)
    {
        $L = $this->_QUERY("SELECT id, nombre FROM VAR007 WHERE (id = '{$_id}');");
        return $L[0]['nombre'];
    }

    function FormulacionesIngredienteIME($_ingrediente, $_formulacion, $_accion)
    {
        $this->_TRANS("DELETE FROM VAR008 WHERE (formulacion = {$_formulacion}) AND (ingrediente = {$_ingrediente});");
        if ($_accion == 'I')
            $this->_TRANS("INSERT INTO VAR008 VALUES({$_formulacion}, {$_ingrediente});");

        //$this->Logger('C000');
        return 1;
    }

    function FormulacionesIngredienteMuestra($_ingrediente)
    {
        return $this->_QUERY("SELECT VAR007.id, VAR007.nombre
		FROM VAR007 INNER JOIN VAR008 ON VAR007.id = VAR008.formulacion
		WHERE(VAR008.ingrediente = {$_ingrediente}) ORDER BY VAR007.nombre;");
    }

    function FormulacionesIME($_id, $_nombre, $_tipo, $_accion)
    {
        $_nombre = $this->Free($_nombre);

        if ($this->_TRANS("EXECUTE PA_MAG_127 '{$_id}', '{$_nombre}', '{$_tipo}', '{$_accion}';"))
            return 1;
        else
            return 0;
    }

    function InventarioEntradasGet($_lab, $_padre)
    {
        if ($_padre == '') $_padre = '-1';
        return $this->_QUERY("EXECUTE PA_MAG_122 '{$_lab}', '{$_padre}';");
    }

    function MasasIME($_ocodigo, $_onominal, $_codigo, $_codigocer, $_fecha, $_nominal, $_real, $_inc, $_certificado, $_activo, $_lab, $_accion)
    {
        $_codigo = $this->Free($_codigo);
        $_codigocer = $this->Free($_codigocer);
        $_certificado = $this->Free($_certificado);
        $_nominal = str_replace(',', '', $_nominal);
        //$_nominal = utf8_decode($_nominal);
        $_real = str_replace(',', '', $_real);
        $_inc = str_replace(',', '', $_inc);
        if ($this->_TRANS("EXECUTE PA_MAG_071 '{$_ocodigo}', '{$_onominal}', '{$_codigo}', '{$_codigocer}', '{$_nominal}', '{$_fecha}', '{$_real}', '{$_inc}', '{$_certificado}', '{$_activo}', '{$_lab}', '{$_accion}';")) {
            $this->Logger('C002');
            return 1;
        } else return 0;
    }

    function MasasResumenGet($_lab, $_activas = false)
    {
        if ($_activas) $ext = 'AND (activo=1)';
        else $ext = '';

        return $this->_QUERY("SELECT codigo, codigocer, nominal, CONVERT(VARCHAR, fecha, 105) AS fecha, vreal, inc, certificado, activo
		FROM VAR002
		WHERE (lab = '{$_lab}') {$ext} ORDER BY activo, nominal;");
    }

    function MatrizGet()
    {
        return $this->_QUERY("SELECT id, nombre, grupo, acreditado FROM VAR001 ORDER BY nombre;");
    }

    function MatrizIME($id, $nombre, $grupo, $acreditado, $accion)
    {
        $nombre = $this->Free($nombre);
        $this->Logger('C001');
        if ($this->_TRANS("EXECUTE PA_MAG_070 '{$id}', '{$nombre}', '{$grupo}', '{$acreditado}', '{$accion}';"))
            return 1;
        else
            return 0;
    }

    function GrupoGet()
    {
        return $this->_QUERY("SELECT id, nombre FROM VAR011 ORDER BY nombre;");
    }

    function FirmantesGet()
    {
        return $this->_QUERY("SELECT id, nombre FROM inf007 where estado = 1;");
    }

    function Firmado($solicitud)
    {
        return $this->_QUERY("select INF007.Nombre as nombre from inf007 inner join  INFORMEFIRMA  on inf007.ID = INFORMEFIRMA.firmante  where informe = '{$solicitud}';");
    }

    function GrupoIME($id, $nombre, $accion)
    {
        $nombre = $this->Free($nombre);
        $this->Logger('C001');
        if ($accion == 'I') {
            $cs = $this->_QUERY("SELECT MAX(id) + 1 AS id FROM VAR011");
            if ($cs[0]['id'] == null) {
                $cs[0]['id'] = 1;
            }
            $this->_TRANS("INSERT INTO VAR011 VALUES('{$cs[0]['id']}', '{$nombre}');");
        }
        if ($accion == 'M') {
            $this->_TRANS("UPDATE VAR011 SET nombre = '{$nombre}' WHERE id = '{$id}';");
        }
        if ($accion == 'D') {
            $this->_TRANS("DELETE FROM VAR011 WHERE id = '{$id}';");
        }
        return 1;
    }

    function MetodosMuestra($lab)
    {
        return $this->_QUERY("SELECT id, nombre FROM MET000 WHERE (lab='{$lab}') ORDER BY nombre;");
    }

    function IngredientesMuestra($lab)
    {
        return $this->_QUERY("SELECT id, nombre FROM VAR005 WHERE (lab='{$lab}') ORDER BY nombre;");
    }

    function MicropipetasCalMuestra()
    {
        return $this->_QUERY("SELECT EQU000.id, EQU000.codigo, EQU000.nombre, EQU011.id AS cs, CONVERT(VARCHAR,EQU011.fecha, 105) AS fecha, EQU011.nominal, EQU011.resultado, EQU011.error, EQU011.inc FROM EQU000 INNER JOIN EQU011 ON EQU000.id = EQU011.equipo;");
    }

    function MicropipetasIME($_id, $_equipo, $_nominal, $_resultado, $_error, $_inc, $_accion)
    {
        $_nominal = str_replace(',', '.', $_nominal);
        $_resultado = str_replace(',', '.', $_resultado);
        $_error = str_replace(',', '.', $_error);
        $_inc = str_replace(',', '.', $_inc);

        if ($this->_TRANS("EXECUTE PA_MAG_143 '{$_id}', '{$_equipo}', '{$_nominal}', '{$_resultado}', '{$_error}', '{$_inc}', '{$_accion}';")) {
            //$this->Logger('C004');
            return 1;
        } else return 0;
    }

    function SubAnalisisGet($_tipo, $_LID, $_formulacion = '')
    {
        if ($_formulacion == '') {
            if ($_tipo == '') array();//$op = '<>';
            //else $op = '=';
            return $this->_QUERY("SELECT id, nombre
			FROM VAR005 
			WHERE (tipo = '{$_tipo}') AND (lab = '{$_LID}')
			ORDER BY nombre;");
        } else {
            //USADO SOLO CUANDO SE PIDE UN I.A. CON UNA FORMULACION ESPECIFICA (LCC PLAGUICIDAS ONLY)
            return $this->_QUERY("SELECT VAR005.id, VAR005.nombre
			FROM VAR008 INNER JOIN VAR005 ON VAR008.ingrediente = VAR005.id
			WHERE (VAR005.lab = '{$_LID}') AND (VAR005.tipo = '{$_tipo}') AND (VAR008.formulacion = {$_formulacion}) ORDER BY nombre;");
        }
    }

    function SubAnalisisGet1($_tipo, $_LID, $_formulacion = '')
    {
        if ($_formulacion == '') {
            if ($_tipo == '') array();//$op = '<>';
            return $this->_QUERY("SELECT id, nombre , tipoCroma FROM VAR005 WHERE (tipo = '{$_tipo}') AND (lab = '{$_LID}') ORDER BY nombre;");
        } else {
            //USADO SOLO CUANDO SE PIDE UN I.A. CON UNA FORMULACION ESPECIFICA (LCC PLAGUICIDAS ONLY)
            return $this->_QUERY("SELECT VAR005.id, VAR005.nombre
			FROM VAR008 INNER JOIN VAR005 ON VAR008.ingrediente = VAR005.id
			WHERE (VAR005.lab = '{$_LID}') AND (VAR005.tipo = '{$_tipo}') AND (VAR008.formulacion = {$_formulacion}) ORDER BY nombre;");
        }
    }


    function SubAnalisisIME($_id, $_nombre, $_tipo, $_lid, $_accion)
    {
        $_nombre = $this->Free($_nombre);

        if ($this->_TRANS("EXECUTE PA_MAG_084 '{$_id}', '{$_nombre}', '{$_tipo}', '{$_lid}', '{$_accion}';"))
            return 1;
        else
            return 0;
    }

    function SubAnalisisIME1($_id, $_nombre, $_tipo, $_lid, $_accion, $_tipoCroma)
    {
        $_nombre = $this->Free($_nombre);

        if ($this->_TRANS("EXECUTE PA_MAG_148 '{$_id}', '{$_nombre}', '{$_tipo}', '{$_lid}', '{$_accion}', '{$_tipoCroma}';"))
            return 1;
        else
            return 0;
    }

    function TermosResumenGet($_lab, $_tipo, $_activas = false)
    {
        if ($_activas) $ext = 'AND (activo=1)';
        else $ext = '';

        return $this->_QUERY("SELECT codigo, codigocer, CONVERT(VARCHAR, fecha, 105) AS fecha, activo, cuarto, limite1, limite2, indicacion1, error1, inc1, indicacion2, error2, inc2, indicacion3, error3, inc3, indicacion4, error4, inc4, indicacion5, error5, inc5, humedad1, humedad2, hr1, err_hr1, hr2, err_hr2, hr3, err_hr3
		FROM VAR003
		WHERE (tipo = {$_tipo}) AND (lab = '{$_lab}') {$ext} ORDER BY activo, codigo;");
    }

    function TermosIME($_codigo, $_codigocer, $_fecha, $_activo, $_cuarto, $_limite1, $_limite2, $_indicacion1, $_error1, $_inc1, $_indicacion2, $_error2, $_inc2, $_indicacion3, $_error3, $_inc3, $_indicacion4, $_error4, $_inc4, $_indicacion5, $_error5, $_inc5, $_humedad, $_humedad2, $_hr1, $_err_hr1, $_hr2, $_err_hr2, $_hr3, $_err_hr3, $_inc6, $_inc7, $_inc8, $_tipo, $_lab, $_accion)
    {
        $_codigo = $this->Free($_codigo);
        $_codigocer = $this->Free($_codigocer);
        $_cuarto = $this->Free($_cuarto);
        $_limite1 = str_replace(',', '', $_limite1);
        $_limite2 = str_replace(',', '', $_limite2);
        $_indicacion1 = str_replace(',', '', $_indicacion1);
        $_indicacion2 = str_replace(',', '', $_indicacion2);
        $_indicacion3 = str_replace(',', '', $_indicacion3);
        $_indicacion4 = str_replace(',', '', $_indicacion4);
        $_indicacion5 = str_replace(',', '', $_indicacion5);
        $_error1 = str_replace(',', '', $_error1);
        $_error2 = str_replace(',', '', $_error2);
        $_error3 = str_replace(',', '', $_error3);
        $_error4 = str_replace(',', '', $_error4);
        $_error5 = str_replace(',', '', $_error5);
        $_inc1 = str_replace(',', '', $_inc1);
        $_inc2 = str_replace(',', '', $_inc2);
        $_inc3 = str_replace(',', '', $_inc3);
        $_inc4 = str_replace(',', '', $_inc4);
        $_inc5 = str_replace(',', '', $_inc5);
        $_inc6 = str_replace(',', '', $_inc3);
        $_inc7 = str_replace(',', '', $_inc4);
        $_inc8 = str_replace(',', '', $_inc5);
        $_humedad1 = str_replace(',', '', $_humedad1);
        $_humedad2 = str_replace(',', '', $_humedad2);
        $_hr1 = str_replace(',', '', $_hr1);
        $_hr2 = str_replace(',', '', $_hr2);
        $_hr3 = str_replace(',', '', $_hr3);
        $_err_hr1 = str_replace(',', '', $_err_hr1);
        $_err_hr2 = str_replace(',', '', $_err_hr2);
        $_err_hr3 = str_replace(',', '', $_err_hr3);
        $_fecha = $this->_FECHA($_fecha);

        if ($this->_TRANS("EXECUTE PA_MAG_075 '{$_codigo}', '{$_codigocer}', '{$_fecha}', '{$_activo}', '{$_cuarto}','{$_limite1}','{$_limite2}','{$_indicacion1}','{$_error1}', '{$_inc1}','{$_indicacion2}','{$_error2}', '{$_inc2}','{$_indicacion3}','{$_error3}', '{$_inc3}', '{$_indicacion4}','{$_error4}', '{$_inc4}', '{$_indicacion5}','{$_error5}', '{$_inc5}','{$_humedad1}', '{$_humedad2}','{$_hr1}','{$_err_hr1}','{$_hr2}','{$_err_hr2}','{$_hr3}','{$_err_hr3}', '{$_inc6}', '{$_inc7}', '{$_inc8}','{$_tipo}','{$_lab}', '{$_accion}';")) {
            $this->Logger('C003');
            return 1;
        } else return 0;
    }

    function UsuarioMetodoXAnalisis($_usuario, $_analisis)
    {
        return $this->_QUERY("SELECT MET000.id, MET000.nombre, MET000.pagina
		FROM MET000 INNER JOIN MET001 ON MET000.id = MET001.metodo INNER JOIN PER011 ON MET000.id = PER011.metodo
		WHERE (PER011.usuario = {$_usuario}) AND (MET001.analisis = {$_analisis}) ORDER BY MET000.nombre;");
    }

    function UsuariosXMetodo($_lab, $_muestra)
    {
        $this->_TRANS("EXECUTE PA_MAG_090 '{$_lab}', '{$_muestra}';");
        return $this->_QUERY("SELECT id, cedula, nombre FROM MUETMP WHERE (lab='{$_lab}') ORDER BY nombre;");
    }

    function XloloX()
    {
        for ($z = 189; $z < 208; $z++) {
            $muestras = $this->_QUERY("SELECT id FROM MUE000 WHERE solicitud = 'LCC-2017-{$z}';");
            for ($x = 0; $x < count($muestras); $x++) {
                $ensayos = $this->_QUERY("SELECT SER004.analisis FROM VAR005 INNER JOIN SER004 ON VAR005.id = SER004.analisis
		WHERE (SER004.solicitud = 'LCC-2017-{$z}');");
                for ($c = 0; $c < count($ensayos); $c++) {
                    $id = $this->_QUERY("SELECT xanalizar FROM MAG000;");
                    $this->_TRANS("UPDATE MAG000 SET xanalizar = xanalizar + 1;");
                    $id = $id[0]['xanalizar'] . '-2017';
                    $this->_TRANS("INSERT INTO MUE001 VALUES('{$id}','{$muestras[$x]['id']}','{$ensayos[$c]['analisis']}','0');");
                }
            }
        }
        return 'Corregido con Exito!';
    }

    function ResultadosDeInforme($_numInforme)
    {
        $nu = substr($_numInforme, 0, 8);
        return $this->_QUERY("EXECUTE usp_Consulta_ResultadosDeInformeFinal  '{$nu}';");
    }
//
}

?>