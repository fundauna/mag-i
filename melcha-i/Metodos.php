<?php

/* * *****************************************************************
  CLASE DE NIVEL 1 ENCARGADA DEL MODULO DE METODOS
  (final SOLO PUEDE SER INSTANCIADA Y NO HEREDADA)
 * ***************************************************************** */

final class Metodos extends Database
{

//
    function AzufreEncabezadoGet($_ensayo)
    {
        return $this->_QUERY("SELECT MUE000.ref, MUE002.tipo, CONVERT(VARCHAR, MUE002.fecha, 105) AS fechaC, MUE002.ingrediente, MUE002.declarada AS rango, MUE002.unidad, CONVERT(VARCHAR, MET003.fechaA, 105) AS fechaA, MET003.tipo_form, MET003.numreactivo, CONVERT(VARCHAR, MET003.fechaP, 105) AS fechaP, MET003.IECB, MET003.linealidad1, 
		MET003.linealidad2, MET003.ampolla1, MET003.ampolla2, MET003.repeti1, MET003.repeti2, MET003.muestra1, MET003.muestra2, 
		MET003.BA, MET003.consumido1, MET003.consumido2, MET003.aforado1, MET003.aforado2, MET003.masa, MET003.alicuota1, 
		MET003.alicuota2, MET003.densidad, MET003.obs, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, (SEG001.nombre +' '+ SEG001.ap1) AS analista
		FROM MUE002 INNER JOIN MET003 ON MUE002.id = MET003.ensayo INNER JOIN MUE001 ON MUE002.xanalizar = MUE001.id INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN SEG001 ON MUE002.analista = SEG001.id
		WHERE (MUE002.id = '{$_ensayo}') AND (MUEBIT.accion = '0');");
    }

    function AzufreComEncabezadoGet($_ensayo)
    {
        return $this->_QUERY("SELECT MUE000.ref, MUE002.tipo, CONVERT(VARCHAR, MUE002.fecha, 105) AS fechaC, MUE002.ingrediente, MUE002.declarada AS rango, MUE002.unidad, CONVERT(VARCHAR, MET021.fechaA, 105) AS fechaA, MET021.rep1, MET021.rep2, MET021.resol1, MET021.resol2, MET021.pres, MET021.inclin, MET021.ccur, MET021.incex, MET021.masa1, MET021.masa2, MET021.masa3, MET021.masaA1, MET021.masaA2, MET021.masaA3, MET021.error1, MET021.error2, MET021.error3, MET021.densidad, MET021.incden, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, (SEG001.nombre +' '+ SEG001.ap1) AS analista
		FROM MUE002 INNER JOIN MET021 ON MUE002.id = MET021.ensayo INNER JOIN MUE001 ON MUE002.xanalizar = MUE001.id INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN SEG001 ON MUE002.analista = SEG001.id
		WHERE (MUE002.id = '{$_ensayo}') AND (MUEBIT.accion = '0');");
    }

    function AzufreEncabezadoSet($_xanalizar, $_tipo, $_ingrediente, $_fechaA, $_rango, $_unidad, $_tipo_form, $_numreactivo, $_fechaP, $_IECB, $_linealidad1, $_linealidad2, $_ampolla1, $_ampolla2, $_repeti1, $_repeti2, $_muestra1, $_muestra2, $_BA, $_consumido1, $_consumido2, $_origen, $_contenido1, $_contenido2, $_contenido3, $_contenido4, $_densidad, $_promedio1, $_incertidumbre1, $_promedio2, $_incertidumbre2, $_obs, $_UID, $_UNAME)
    {
        $_ingrediente = $this->Free($_ingrediente);
        $_rango = $this->Free($_rango);
        $_numreactivo = $this->Free($_numreactivo);
        $_obs = $this->Free($_obs);
        $_fechaA = $this->_FECHA($_fechaA);
        $_fechaP = $this->_FECHA($_fechaP);

        $this->InvalidaAnteriores($_xanalizar, $_tipo);

        $cs = $this->ConsecutivoEnsayoGet();

        if ($_unidad == '0') {
            $_promedio = $_promedio1;
            $_incertidumbre = $_incertidumbre1;
        } else {
            $_promedio = $_promedio2;
            $_incertidumbre = $_incertidumbre2;
        }

        if ($this->_TRANS("EXECUTE PA_MAG_094 '{$cs}',
			'{$_xanalizar}', 
			'{$_tipo}', 
			'{$_ingrediente}', 
			'{$_fechaA}', 
			'{$_rango}', 
			'{$_unidad}', 
			'{$_tipo_form}', 
			'{$_numreactivo}', 
			'{$_fechaP}', 
			'{$_IECB}', 
			'{$_linealidad1}', 
			'{$_linealidad2}', 
			'{$_ampolla1}', 
			'{$_ampolla2}', 
			'{$_repeti1}', 
			'{$_repeti2}', 
			'{$_muestra1}', 
			'{$_muestra2}', 
			'{$_BA}', 
			'{$_consumido1}', 
			'{$_consumido2}', 
			'$_origen', 
			'', 				
			'', 
			'', 
			'', 
			'{$_densidad}',
			'{$_promedio}', 
			'{$_incertidumbre}', 
			'{$_obs}',
			'{$_UID}';")) {
            if ($_unidad == '0') {
                $this->GrafControlIME($_ingrediente, $_rango, $_UNAME, $_xanalizar, $_tipo_form, $_contenido1, $_contenido2);
            } else {
                $this->GrafControlIME($_ingrediente, $_rango, $_UNAME, $_xanalizar, $_tipo_form, $_contenido3, $_contenido4);
            }
            return 1;
        } else
            return 0;
    }

   // function AzufreComEncabezadoSet($_xanalizar, $_tipo, $_ingrediente, $_fechaA, $_rango, $_masa1, $_masa2, $_masa3, $_masaA1, $_masaA2, $_masaA3, $_error1, $_densidad, $_incden, $_rep1, $_rep2, $_resol1, $_resol2, $_pres, $_inclin, $_ccur, $_incex, $_resultado1, $_resultado2, $_UID)
    function AzufreComEncabezadoSet($_xanalizar, $_tipo, $_ingrediente, $_fechaA, $_rango, $_masa1, $_masa2, $_masa3, $_masaA1, $_masaA2, $_masaA3, $_error1, $_error2, $_error3, $_densidad, $_rep1, $_rep2, $_resol1, $_resol2, $_ccur, $_incex, $_resultado1, $_resultado2, $_UID)
    {
        $this->InvalidaAnteriores($_xanalizar, $_tipo);
        $cs = $this->ConsecutivoEnsayoGet();

        if ($this->_TRANS("EXECUTE PA_MAG_140 '{$cs}',
			'{$_xanalizar}', 
			'{$_tipo}', 
			'{$_ingrediente}', 
			'{$this->_FECHA($_fechaA)}',
			'{$_rango}',
			'{$_masa1}', 
			'{$_masa2}', 
			'{$_masa3}', 
			'{$_masaA1}', 
			'{$_masaA2}', 
			'{$_masaA3}', 
			'{$_error1}',			
			'{$_error2}',			
			'{$_error3}',			
			'{$_densidad}',			
			'{$_rep1}', 
			'{$_rep2}', 
			'{$_resol1}', 
			'{$_resol2}',			
			'{$_ccur}', 
			'{$_incex}', 
			'{$_UID}';")) {

            $R = $this->_QUERY("SELECT muestra, analisis FROM MUE001 WHERE id = '{$_xanalizar}';");
            $F = $this->_QUERY("SELECT solicitud fROM MUE000 WHERE id = '{$R[0]['muestra']}';");
            $F = $this->_QUERY("SELECT unidad fROM SER004 WHERE solicitud = '{$F[0]['solicitud']}' AND analisis = '{$R[0]['analisis']}';");
            $this->_TRANS("UPDATE MUE002 SET resultado = 'Cn={$_resultado1}, IC={$_resultado2}', declarada = '{$_rango}' WHERE id = '{$cs}';");
            return 1;
        } else
            return 0;
    }

    function AzufreEncabezadoVacio($_xanalizar)
    {
        $ROW = $this->_QUERY("SELECT MUE000.ref, '' AS ingrediente, '' AS tipo_form, '' AS rango, '' AS unidad, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, MUE000.solicitud, MUE001.analisis
		FROM MUE001 INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra
		WHERE (MUEBIT.accion = '0' OR MUEBIT.accion = 'A') AND (MUE001.id = '{$_xanalizar}');");
        //
        $ROW2 = $this->_QUERY("SELECT SER004.rango, SER004.unidad, VAR007.nombre AS tipo_form, VAR005.nombre AS ingrediente
		FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR007 ON SER003.tipo_form = VAR007.id INNER JOIN VAR005 ON SER004.analisis = VAR005.id
		WHERE (SER003.cs = '{$ROW[0]['solicitud']}') AND (SER004.analisis = {$ROW[0]['analisis']});");
        if ($ROW2) {
            $ROW[0]['ingrediente'] = $ROW2[0]['ingrediente'];
            $ROW[0]['tipo_form'] = $ROW2[0]['tipo_form'];
            $ROW[0]['rango'] = $ROW2[0]['rango'];
            $ROW[0]['unidad'] = $ROW2[0]['unidad'];
        }
        //VACIOS
        $ROW[0]['obs'] = '';
        $ROW[0]['fechaP'] = '';
        $ROW[0]['origen'] = '';
        $ROW[0]['numreactivo'] = '';
        $ROW[0]['fechaA'] = '';
        $ROW[0]['fechaC'] = '';
        $ROW[0]['masa'] = '0';
        $ROW[0]['muestra1'] = '0';
        $ROW[0]['muestra2'] = '0';
        $ROW[0]['consumido1'] = '0';
        $ROW[0]['consumido2'] = '0';
        $ROW[0]['aforado1'] = '';
        $ROW[0]['aforado2'] = '';
        $ROW[0]['alicuota1'] = '';
        $ROW[0]['alicuota2'] = '';
        $ROW[0]['densidad'] = '0';
        $ROW[0]['ampolla1'] = '0';
        $ROW[0]['ampolla2'] = '0';
        $ROW[0]['BA'] = '';
        $ROW[0]['linealidad1'] = '0.0001';
        $ROW[0]['linealidad2'] = '0.0001';
        $ROW[0]['repeti1'] = '0.0007';
        $ROW[0]['repeti2'] = '0.0007';
        $ROW[0]['IECB'] = '0.033';

        return $ROW;
    }

    function AzufreComEncabezadoVacio($_xanalizar)
    {
        $ROW = $this->_QUERY("SELECT MUE000.ref, '' AS ingrediente, '' AS tipo_form, '' AS rango, '' AS unidad, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, MUE000.solicitud, MUE001.analisis
		FROM MUE001 INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra
		WHERE (MUEBIT.accion = '0' OR MUEBIT.accion = 'A') AND (MUE001.id = '{$_xanalizar}');");
        //
        $ROW2 = $this->_QUERY("SELECT SER004.rango, SER004.unidad, VAR007.nombre AS tipo_form, VAR005.nombre AS ingrediente
		FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR007 ON SER003.tipo_form = VAR007.id INNER JOIN VAR005 ON SER004.analisis = VAR005.id
		WHERE (SER003.cs = '{$ROW[0]['solicitud']}') AND (SER004.analisis = {$ROW[0]['analisis']});");
        if ($ROW2) {
            $ROW[0]['ingrediente'] = $ROW2[0]['ingrediente'];
            $ROW[0]['tipo_form'] = $ROW2[0]['tipo_form'];
            $ROW[0]['rango'] = $ROW2[0]['rango'];
            $ROW[0]['unidad'] = $ROW2[0]['unidad'];
        }
        //VACIOS
        $ROW[0]['analista'] = '';
        $ROW[0]['masa1'] = '';
        $ROW[0]['masa2'] = '';
        $ROW[0]['masa3'] = '';
        $ROW[0]['masaA1'] = '';
        $ROW[0]['masaA2'] = '';
        $ROW[0]['masaA3'] = '';
        $ROW[0]['error1'] = '';
        $ROW[0]['error2'] = '';
        $ROW[0]['error3'] = '';
        $ROW[0]['densidad'] = '';
        $ROW[0]['incden'] = '';
        $ROW[0]['fechaA'] = '';
        $ROW[0]['fechaC'] = '';
        $ROW[0]['rep1'] = '';
        $ROW[0]['rep2'] = '';
        $ROW[0]['resol1'] = '';
        $ROW[0]['resol2'] = '';
        $ROW[0]['pres'] = '';
        $ROW[0]['inclin'] = '';
        $ROW[0]['ccur'] = '';
        $ROW[0]['incex'] = '';

        return $ROW;
    }

    function AzufreMasaVacio()
    {
        return array(0 => array(
            'masa' => '',
            'masaA' => '')
        );
    }

    function AzufreMasaGet($_id)
    {
        return $this->_QUERY("SELECT masa, masaA FROM MET022 WHERE ensayo = '{$_id}';");
    }

    function CobreEncabezadoGet($_ensayo)
    {
        return $this->_QUERY("SELECT MUE000.ref, MUE002.tipo, CONVERT(VARCHAR, MUE002.fecha, 105) AS fechaC, MUE002.ingrediente, MUE002.declarada AS rango, MUE002.unidad, CONVERT(VARCHAR, MET018.fechaA, 105) AS fechaA, MET018.tipo_form, MET018.numreactivo, CONVERT(VARCHAR, MET018.fechaP, 105) AS fechaP, MET018.IECB, MET018.linealidad1, 
		MET018.linealidad2, MET018.repeti1, MET018.repeti2, MET018.metodo, MET018.bureta, MET018.ampolla1, MET018.ampolla2, MET018.densidad, MET018.factor, MET018.balon, MET018.masaA, MET018.masaB, MET018.masaC, MET018.masaD, MET018.masaE,
		MET018.masaF, MET018.consuA, MET018.consuB, MET018.consuC, MET018.consuD, MET018.consuE, MET018.consuF, MET018.alicuotaA, MET018.alicuotaB, MET018.alicuotaE, MET018.alicuotaF, MET018.balA, MET018.balB, MET018.balE, MET018.balF, MET018.obs, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, (SEG001.nombre +' '+ SEG001.ap1) AS analista
		FROM MUE002 INNER JOIN MET018 ON MUE002.id = MET018.ensayo INNER JOIN MUE001 ON MUE002.xanalizar = MUE001.id INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN SEG001 ON MUE002.analista = SEG001.id
		WHERE (MUE002.id = '{$_ensayo}') AND (MUEBIT.accion = '0');");
    }

    function CobreEncabezadoSet($_xanalizar, $_tipo, $_ingrediente, $_fechaA, $_rango, $_unidad, $_tipo_form, $_numreactivo, $_fechaP, $_IECB, $_linealidad1, $_linealidad2, $_repeti1, $_repeti2, $_metodo, $_bureta, $_ampolla1, $_ampolla2, $_densidad, $_factor, $_balon, $_masaA, $_masaB, $_masaC, $_masaD, $_masaE, $_masaF, $_consuA, $_consuB, $_consuC, $_consuD, $_consuE, $_consuF, $_alicuotaA, $_alicuotaB, $_alicuotaE, $_alicuotaF, $_balA, $_balB, $_balE, $_balF, $_finA, $_finB, $_finC, $_finD, $_contenido1, $_contenido2, $_obs, $_UID, $_UNAME)
    {
        $_ingrediente = $this->Free($_ingrediente);
        $_rango = $this->Free($_rango);
        $_numreactivo = $this->Free($_numreactivo);
        $_obs = $this->Free($_obs);
        $_fechaA = $this->_FECHA2($_fechaA);
        $_fechaP = $this->_FECHA2($_fechaP);

        $this->InvalidaAnteriores($_xanalizar, $_tipo);

        $cs = $this->ConsecutivoEnsayoGet();

        if ($this->_TRANS("EXECUTE PA_MAG_119 '{$cs}',
			'{$_xanalizar}', 
			'{$_tipo}', 
			'{$_ingrediente}', 
			'{$_fechaA}', 
			'{$_rango}', 
			'{$_unidad}', 
			'{$_tipo_form}', 
			'{$_numreactivo}', 
			'{$_fechaP}', 
			'{$_IECB}', 
			'{$_linealidad1}', 
			'{$_linealidad2}', 
			'{$_repeti1}', 
			'{$_repeti2}', 
			'{$_metodo}',
			'{$_bureta}',
			'{$_ampolla1}',
			'{$_ampolla2}',
			'{$_densidad}',
			'{$_factor}',
			'{$_balon}',
			'{$_masaA}',
			'{$_masaB}',
			'{$_masaC}',
			'{$_masaD}',
			'{$_masaE}',
			'{$_masaF}',
			'{$_consuA}',
			'{$_consuB}',
			'{$_consuC}',
			'{$_consuD}',
			'{$_consuE}',
			'{$_consuF}',
			'{$_alicuotaA}',
			'{$_alicuotaB}',
			'{$_alicuotaE}',
			'{$_alicuotaF}',
			'{$_balA}',
			'{$_balB}',
			'{$_balE}',
			'{$_balF}',
			'{$_finA}',
			'{$_finB}',
			'{$_finC}',
			'{$_finD}',
			'{$_obs}',
			'{$_UID}';")) {
            $this->GrafControlIME($_ingrediente, $_rango, $_UNAME, $_xanalizar, $_tipo_form, $_contenido1, $_contenido2);
            return 1;
        } else
            return 0;
    }

    function CobreEncabezadoVacio($_xanalizar)
    {
        $ROW = $this->_QUERY("SELECT MUE000.ref, '' AS ingrediente, '' AS tipo_form, '' AS rango, '' AS unidad, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, MUE000.solicitud, MUE001.analisis
		FROM MUE001 INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra
		WHERE (MUEBIT.accion = '0' OR MUEBIT.accion = 'A') AND (MUE001.id = '{$_xanalizar}');");
        //
        $ROW2 = $this->_QUERY("SELECT SER004.rango, SER004.unidad, VAR007.nombre AS tipo_form, VAR005.nombre AS ingrediente
		FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR007 ON SER003.tipo_form = VAR007.id INNER JOIN VAR005 ON SER004.analisis = VAR005.id
		WHERE (SER003.cs = '{$ROW[0]['solicitud']}') AND (SER004.analisis = {$ROW[0]['analisis']});");
        if ($ROW2) {
            $ROW[0]['ingrediente'] = $ROW2[0]['ingrediente'];
            $ROW[0]['tipo_form'] = $ROW2[0]['tipo_form'];
            $ROW[0]['rango'] = $ROW2[0]['rango'];
            $ROW[0]['unidad'] = $ROW2[0]['unidad'];
        }
        //VACIOS
        $ROW[0]['fechaI'] = '';
        $ROW[0]['fechaA'] = '';
        $ROW[0]['fechaC'] = '';
        $ROW[0]['metodo'] = '';
        $ROW[0]['numreactivo'] = '';
        $ROW[0]['fechaP'] = '';
        $ROW[0]['bureta'] = '';
        $ROW[0]['ampolla1'] = '0';
        $ROW[0]['ampolla2'] = '0';
        $ROW[0]['densidad'] = '0';
        $ROW[0]['factor'] = '';
        $ROW[0]['balon'] = '';
        $ROW[0]['masaA'] = '0';
        $ROW[0]['masaB'] = '0';
        $ROW[0]['masaC'] = '0';
        $ROW[0]['masaD'] = '0';
        $ROW[0]['masaE'] = '0';
        $ROW[0]['masaF'] = '0';
        $ROW[0]['consuA'] = '0';
        $ROW[0]['consuB'] = '0';
        $ROW[0]['consuC'] = '0';
        $ROW[0]['consuD'] = '0';
        $ROW[0]['consuE'] = '0';
        $ROW[0]['consuF'] = '0';
        $ROW[0]['alicuotaA'] = '';
        $ROW[0]['alicuotaB'] = '';
        $ROW[0]['alicuotaE'] = '';
        $ROW[0]['alicuotaF'] = '';
        $ROW[0]['balA'] = '';
        $ROW[0]['balB'] = '';
        $ROW[0]['balE'] = '';
        $ROW[0]['balF'] = '';
        $ROW[0]['obs'] = '';
        $ROW[0]['linealidad1'] = '0.0001';
        $ROW[0]['linealidad2'] = '0.0001';
        $ROW[0]['repeti1'] = '0.0007';
        $ROW[0]['repeti2'] = '0.0007';
        $ROW[0]['IECB'] = '0.060';

        return $ROW;
    }

    function CromaNEncabezadoGet($_ensayo)
    {
        return $this->_QUERY("EXECUTE PA_MAG_125 '{$_ensayo}';");
    }

    function CromaNEncabezadoSet($_xanalizar, $_tipo, $_ingrediente, $_fechaA, $_rango, $_unidad, $_tipo_form, $_densidad, $_metodo, $_disolvente1, $_validado, $_disolvente2, $_croma, $_disolvente3, $_estandar, $_tiempo1, $_origen1, $_tiempo2, $_certificado1, $_tiempo3, $_pureza1, $_purezas1, $_interno1, $_inyeccion, $_interno2, $_loop, $_origen2, $_metcalibra, $_certificado2, $_pureza2, $_equipo, $_tipo_i, $_temp_i, $_modo_i, $_condicion, $_tipo_c, $_codigo_c, $_marca_c, $_temp_c, $_dim_c, $_gas1, $_flujo1, $_presion1, $_gas2, $_flujo2, $_presion2, $_gas3, $_flujo3, $_presion3, $_tipo_d, $_temp_d, $_long_d, $_fid, $_flujofid1, $_fid1, $_flujofid2, $_fid2, $_flujofid3, $_modo, $_temp1_g, $_temp2_g, $_temp3_g, $_bomba_d, $_elu_d, $_gas4, $_flujo4, $_presion4, $_gas5, $_flujo5, $_presion5, $_gas6, $_flujo6, $_presion6, $_gas7, $_flujo7, $_presion7, $_masa1, $_aforadoA1, $_muestraalicuota1, $_muestraaforadoA2, $_muestraalicuota2, $_muestraaforadoA3, $_masa2, $_aforadoA2, $_plaguicidaalicuota1, $_plaguicidaaforadoA2, $_plaguicidaalicuota2, $_plaguicidaaforadoA3, $_masa3, $_aforadoA3, $_alicuota, $_aforadoB3, $_internoalicuota, $_internoaforadoB3, $_areaA1, $_areaB1, $_areaC1, $_areaA2, $_areaB2, $_areaC2, $_areaA3, $_areaB3, $_areaC3, $_areaA4, $_areaB4, $_areaC4, $_masa4, $_aforadoA4, $_muestraalicuota3, $_muestraaforadoA4, $_muestraalicuota4, $_muestraaforadoA5, $_masa5, $_aforadoA5, $_plaguicidaalicuota3, $_plaguicidaaforadoA4, $_plaguicidaalicuota4, $_plaguicidaaforadoA5, $_masa6, $_aforadoA6, $_alicuota1, $_aforadoB4, $_internoalicuota1, $_internoaforadoB4, $_areaA5, $_areaB5, $_areaC5, $_areaA6, $_areaB6, $_areaC6, $_areaA7, $_areaB7, $_areaC7, $_areaA8, $_areaB8, $_areaC8, $_linealidad1, $_linealidad2, $_repeti1, $_repeti2, $_IECA, $_IECB, $_final_P, $_final_I, $_obs, $_contenido1, $_contenido2, $_contenido3, $_contenido4, $_UID, $_UNAME)
    {

        $_ingrediente = $this->Free($_ingrediente);
        $_metodo = $this->Free($_metodo);
        $_condicion = $this->Free($_condicion);
        $_disolvente1 = $this->Free($_disolvente1);
        $_disolvente2 = $this->Free($_disolvente2);
        $_disolvente3 = $this->Free($_disolvente3);
        $_estandar = $this->Free($_estandar);
        $_origen1 = $this->Free($_origen1);
        $_interno2 = $this->Free($_interno2);
        $_origen2 = $this->Free($_origen2);
        $_codigo_c = $this->Free($_codigo_c);
        $_marca_c = $this->Free($_marca_c);
        $_dim_c = $this->Free($_dim_c);
        $_elu_d = $this->Free($_elu_d);
        $_obs = $this->Free($_obs);
        $_fechaA = $this->_FECHA($_fechaA);

        $this->InvalidaAnteriores($_xanalizar, $_tipo);

        $cs = $this->ConsecutivoEnsayoGet();





        if ($this->_TRANS("EXECUTE PA_MAG_124 '{$cs}',
			'{$_xanalizar}', 
			'{$_tipo}', 
			'{$_ingrediente}', 
			'{$_fechaA}', 
			'{$_rango}', 
			'{$_unidad}', 
			'{$_tipo_form}', 
			'{$_densidad}',
			'{$_metodo}',
			'{$_disolvente1}',
			'{$_validado}',
			'{$_disolvente2}',
			'{$_croma}',
			'{$_disolvente3}',
			'{$_estandar}',
			'{$_tiempo1}',
			'{$_origen1}',
			'{$_tiempo2}',
			'{$_certificado1}',
			'{$_tiempo3}',
			'{$_pureza1}',
			'{$_purezas1}',
			'{$_interno1}',
			'{$_inyeccion}',
			'{$_interno2}',
			'{$_loop}',
			'{$_origen2}',
			'{$_metcalibra}',
			'{$_certificado2}',
			'{$_pureza2}',
			'{$_equipo}',
			'{$_tipo_i}',
			'{$_temp_i}',
			'{$_modo_i}',
			'{$_condicion}',
			'{$_tipo_c}',
			'{$_codigo_c}',
			'{$_marca_c}',
			'{$_temp_c}',
			'{$_dim_c}',
			'{$_gas1}',
			'{$_flujo1}',
			'{$_presion1}',
			'{$_gas2}',
			'{$_flujo2}',
			'{$_presion2}',
			'{$_gas3}',
			'{$_flujo3}',
			'{$_presion3}',
			'{$_tipo_d}',
			'{$_temp_d}',
			'{$_long_d}',
			'{$_fid}',
			'{$_flujofid1}',
			'{$_fid1}',
			'{$_flujofid2}',
			'{$_fid2}',
			'{$_flujofid3}',
			'{$_modo}',
			'{$_temp1_g}',
			'{$_temp2_g}',
			'{$_temp3_g}',
			'{$_bomba_d}',
			'{$_elu_d}',
			'{$_gas4}',
			'{$_flujo4}',
			'{$_presion4}',
			'{$_gas5}',
			'{$_flujo5}',
			'{$_presion5}',
			'{$_gas6}',
			'{$_flujo6}',
			'{$_presion6}',
			'{$_gas7}',
			'{$_flujo7}',
			'{$_presion7}',
			'{$_masa1}',
			'{$_aforadoA1}',
			'{$_muestraalicuota1}',
			'{$_muestraaforadoA2}',
			'{$_muestraalicuota2}',
			'{$_muestraaforadoA3}',
			'{$_masa2}',
			'{$_aforadoA2}',
			'{$_plaguicidaalicuota1}',
			'{$_plaguicidaaforadoA2}',
			'{$_plaguicidaalicuota2}',
			'{$_plaguicidaaforadoA3}',
			'{$_masa3}',
			'{$_aforadoA3}',
			'{$_alicuota}',
			'{$_aforadoB3}',
			'{$_internoalicuota}',
			'{$_internoaforadoB3}',
			'{$_areaA1}',
			'{$_areaB1}',
			'{$_areaC1}',
			'{$_areaA2}',
			'{$_areaB2}',
			'{$_areaC2}',
			'{$_areaA3}',
			'{$_areaB3}',
			'{$_areaC3}',
			'{$_areaA4}',
			'{$_areaB4}',
			'{$_areaC4}',
			'{$_masa4}',
			'{$_aforadoA4}',
			'{$_muestraalicuota3}',
			'{$_muestraaforadoA4}',
			'{$_muestraalicuota4}',
			'{$_muestraaforadoA5}',
			'{$_masa5}',
			'{$_aforadoA5}',
			'{$_plaguicidaalicuota3}',
			'{$_plaguicidaaforadoA4}',
			'{$_plaguicidaalicuota4}',
			'{$_plaguicidaaforadoA5}',
			'{$_masa6}',
			'{$_aforadoA6}',
			'{$_alicuota1}',
			'{$_aforadoB4}',
			'{$_internoalicuota1}',
			'{$_internoaforadoB4}',
			'{$_areaA5}',
			'{$_areaB5}',
			'{$_areaC5}',
			'{$_areaA6}',
			'{$_areaB6}',
			'{$_areaC6}',
			'{$_areaA7}',
			'{$_areaB7}',
			'{$_areaC7}',
			'{$_areaA8}',
			'{$_areaB8}',
			'{$_areaC8}',
			'{$_linealidad1}', 
			'{$_linealidad2}', 
			'{$_repeti1}',
			'{$_repeti2}', 
			'{$_IECA}',
			'{$_IECB}',
			'{$_final_P}',
			'{$_final_I}',
			'{$_obs}',
			'{$_UID}';")) {
            if ($_interno1 == '0') {
                $this->GrafControlIME($_ingrediente, $_rango, $_UNAME, $_xanalizar, $_tipo_form, $_contenido1, $_contenido2);
            } else {
                $this->GrafControlIME($_ingrediente, $_rango, $_UNAME, $_xanalizar, $_tipo_form, $_contenido3, $_contenido4);
            }
            return 1;
        } else
            return 0;
    }

    function CromaNEncabezadoVacio($_xanalizar)
    {
        $ROW = $this->_QUERY("SELECT MUE000.ref, '' AS ingrediente, '' AS tipo_form, '' AS rango, '' AS unidad, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, MUE000.solicitud, MUE001.analisis
		FROM MUE001 INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra
		WHERE (MUEBIT.accion = '0' OR MUEBIT.accion = 'A') AND (MUE001.id = '{$_xanalizar}');");
        //
        $ROW2 = $this->_QUERY("SELECT SER004.rango, SER004.unidad, VAR007.nombre AS tipo_form, VAR005.nombre AS ingrediente
		FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR007 ON SER003.tipo_form = VAR007.id INNER JOIN VAR005 ON SER004.analisis = VAR005.id
		WHERE (SER003.cs = '{$ROW[0]['solicitud']}') AND (SER004.analisis = {$ROW[0]['analisis']});");
        if ($ROW2) {
            $ROW[0]['ingrediente'] = $ROW2[0]['ingrediente'];
            $ROW[0]['tipo_form'] = $ROW2[0]['tipo_form'];
            $ROW[0]['rango'] = $ROW2[0]['rango'];
            $ROW[0]['unidad'] = $ROW2[0]['unidad'];
        }
        //VACIOS
        $ROW[0]['fechaA'] = '';
        $ROW[0]['fechaC'] = '';
        $ROW[0]['impureza'] = '';
        $ROW[0]['tmp_equipo1'] = '';
        $ROW[0]['nombre'] = '';
        $ROW[0]['densidad'] = '0';
        $ROW[0]['metodo'] = '';
        $ROW[0]['condicion'] = '';
        $ROW[0]['disolvente1'] = '';
        $ROW[0]['validado'] = '';
        $ROW[0]['disolvente2'] = '';
        $ROW[0]['croma'] = '';
        $ROW[0]['disolvente3'] = '';
        $ROW[0]['estandar'] = '';
        $ROW[0]['tiempo1'] = '0';
        $ROW[0]['origen1'] = '';
        $ROW[0]['tiempo2'] = '0';
        $ROW[0]['certificado1'] = '';
        $ROW[0]['tiempo3'] = '0';
        $ROW[0]['pureza1'] = '0';
        $ROW[0]['purezas1'] = '0';
        $ROW[0]['interno1'] = '';
        $ROW[0]['inyeccion'] = '0';
        $ROW[0]['interno2'] = '';
        $ROW[0]['loop'] = '0';
        $ROW[0]['origen2'] = '';
        $ROW[0]['metcalibra'] = '';
        $ROW[0]['certificado2'] = '';
        $ROW[0]['pureza2'] = '0';
        $ROW[0]['equipo'] = '';
        $ROW[0]['tipo_i'] = '';
        $ROW[0]['temp_i'] = '0';
        $ROW[0]['modo_i'] = '';
        $ROW[0]['tipo_c'] = '';
        $ROW[0]['codigo_c'] = '';
        $ROW[0]['marca_c'] = '';
        $ROW[0]['temp_c'] = '0';
        $ROW[0]['dim_c'] = '';
        $ROW[0]['gas1'] = '';
        $ROW[0]['flujo1'] = '0';
        $ROW[0]['presion1'] = '0';
        $ROW[0]['gas2'] = '';
        $ROW[0]['flujo2'] = '0';
        $ROW[0]['presion2'] = '0';
        $ROW[0]['gas3'] = '';
        $ROW[0]['flujo3'] = '0';
        $ROW[0]['presion3'] = '0';
        $ROW[0]['tipo_d'] = '';
        $ROW[0]['temp_d'] = '0';
        $ROW[0]['long_d'] = '0';
        $ROW[0]['modo'] = '';
        $ROW[0]['temp1_g'] = '0';
        $ROW[0]['temp2_g'] = '0';
        $ROW[0]['temp3_g'] = '0';
        $ROW[0]['bomba_d'] = '';
        $ROW[0]['elu_d'] = '';
        $ROW[0]['gas4'] = '';
        $ROW[0]['flujo4'] = '0';
        $ROW[0]['presion4'] = '0';
        $ROW[0]['gas5'] = '';
        $ROW[0]['flujo5'] = '0';
        $ROW[0]['presion5'] = '0';
        $ROW[0]['gas6'] = '';
        $ROW[0]['flujo6'] = '0';
        $ROW[0]['presion6'] = '0';
        $ROW[0]['gas7'] = '';
        $ROW[0]['flujo7'] = '0';
        $ROW[0]['presion7'] = '0';
        $ROW[0]['masa1'] = '0';
        $ROW[0]['aforadoA1'] = '';
        $ROW[0]['masa2'] = '0';
        $ROW[0]['aforadoA2'] = '';
        $ROW[0]['masa3'] = '0';
        $ROW[0]['aforadoA3'] = '';
        $ROW[0]['alicuota'] = '';
        $ROW[0]['aforadoB3'] = '';
        $ROW[0]['areaA1'] = '0';
        $ROW[0]['areaB1'] = '0';
        $ROW[0]['areaC1'] = '0';
        $ROW[0]['areaA2'] = '0';
        $ROW[0]['areaB2'] = '0';
        $ROW[0]['areaC2'] = '0';
        $ROW[0]['areaA3'] = '0';
        $ROW[0]['areaB3'] = '0';
        $ROW[0]['areaC3'] = '0';
        $ROW[0]['areaA4'] = '0';
        $ROW[0]['areaB4'] = '0';
        $ROW[0]['areaC4'] = '0';
        $ROW[0]['masa4'] = '0';
        $ROW[0]['aforadoA4'] = '';
        $ROW[0]['masa5'] = '0';
        $ROW[0]['aforadoA5'] = '';
        $ROW[0]['masa6'] = '0';
        $ROW[0]['aforadoA6'] = '';
        $ROW[0]['areaA5'] = '0';
        $ROW[0]['areaB5'] = '0';
        $ROW[0]['areaC5'] = '0';
        $ROW[0]['areaA6'] = '0';
        $ROW[0]['areaB6'] = '0';
        $ROW[0]['areaC6'] = '0';
        $ROW[0]['areaA7'] = '0';
        $ROW[0]['areaB7'] = '0';
        $ROW[0]['areaC7'] = '0';
        $ROW[0]['areaA8'] = '0';
        $ROW[0]['areaB8'] = '0';
        $ROW[0]['areaC8'] = '0';
        $ROW[0]['obs'] = '';
        $ROW[0]['linealidad1'] = '0.0001';
        $ROW[0]['linealidad2'] = '0.0001';
        $ROW[0]['repeti1'] = '0.0007';
        $ROW[0]['repeti2'] = '0.0007';
        $ROW[0]['IECA'] = '0.0007';
        $ROW[0]['IECB'] = '0.0007';
        $ROW[0]['fid'] = '';
        $ROW[0]['fid1'] = '';
        $ROW[0]['fid2'] = '';
        $ROW[0]['flujofid1'] = '';
        $ROW[0]['flujofid2'] = '';
        $ROW[0]['flujofid3'] = '';
        $ROW[0]['muestraalicuota1'] = '';
        $ROW[0]['muestraaforadoA2'] = '';
        $ROW[0]['muestraalicuota2'] = '';
        $ROW[0]['muestraaforadoA3'] = '';
        $ROW[0]['plaguicidaalicuota1'] = '';
        $ROW[0]['plaguicidaaforadoA2'] = '';
        $ROW[0]['plaguicidaalicuota2'] = '';
        $ROW[0]['plaguicidaaforadoA3'] = '';
        $ROW[0]['internoalicuota'] = '';
        $ROW[0]['internoaforadoB3'] = '';
        $ROW[0]['aforado11'] = '';
        $ROW[0]['alicuota11'] = '';
        $ROW[0]['aforado12'] = '';
        $ROW[0]['alicuota12'] = '';
        $ROW[0]['aforado13'] = '';
        $ROW[0]['aforado21'] = '';
        $ROW[0]['alicuota21'] = '';
        $ROW[0]['aforado22'] = '';
        $ROW[0]['alicuota22'] = '';
        $ROW[0]['aforado23'] = '';
        $ROW[0]['alicuota31'] = '';
        $ROW[0]['aforado31'] = '';
        $ROW[0]['alicuota32'] = '';
        $ROW[0]['aforado32'] = '';
        $ROW[0]['concentracion3'] = '';
        $ROW[0]['areaimp3'] = '';
        $ROW[0]['areaint3'] = '';
        $ROW[0]['alicuota41'] = '';
        $ROW[0]['aforado41'] = '';
        $ROW[0]['alicuota42'] = '';
        $ROW[0]['aforado42'] = '';
        $ROW[0]['concentracion4'] = '';
        $ROW[0]['areaimp4'] = '';
        $ROW[0]['areaint4'] = '';
        $ROW[0]['alicuota51'] = '';
        $ROW[0]['aforado51'] = '';
        $ROW[0]['aforado52'] = '';
        $ROW[0]['alicuota52'] = '';
        $ROW[0]['concentracion5'] = '';
        $ROW[0]['areaimp5'] = '';
        $ROW[0]['areaint5'] = '';
        $ROW[0]['alicuota61'] = '';
        $ROW[0]['aforado61'] = '';
        $ROW[0]['alicuota62'] = '';
        $ROW[0]['aforado62'] = '';
        $ROW[0]['concentracion6'] = '';
        $ROW[0]['areaimp6'] = '';
        $ROW[0]['areaint6'] = '';
        $ROW[0]['aforado1'] = '';
        $ROW[0]['muestraaforado2'] = '';
        $ROW[0]['muestraaforado3'] = '';
        $ROW[0]['aforado3'] = '';
        $ROW[0]['muestraalicuota3'] = '';
        $ROW[0]['muestraaforadoA4'] = '';
        $ROW[0]['muestraalicuota4'] = '';
        $ROW[0]['muestraaforadoA5'] = '';
        $ROW[0]['plaguicidaalicuota3'] = '';
        $ROW[0]['plaguicidaaforadoA4'] = '';
        $ROW[0]['plaguicidaalicuota4'] = '';
        $ROW[0]['plaguicidaaforadoA5'] = '';

        return $ROW;
    }

    function ConsecutivoEnsayoGet()
    {
        $ROW = $this->_QUERY("SELECT ensayos_ FROM MAG000;");
        return $ROW[0]['ensayos_'] . '-' . date('Y');
    }

    function ConsecutivoEnsayoSet()
    {
        $this->_TRANS("UPDATE MAG000 SET ensayos_=ensayos_+1;");
    }

    function DilucionEncabezadoGet($_ensayo)
    {
        return $this->_QUERY("SELECT MUE000.ref, MUE002.tipo, CONVERT(VARCHAR, MUE002.fecha, 105) AS fechaC, MUE002.ingrediente, CONVERT(VARCHAR, MET006.fechaA, 105) AS fechaA, MET006.tipo_form, MET006.dosis, CONVERT(VARCHAR, MET006.fechaP, 105) AS fechaP, MET006.maria, 
        MET006.numreactivo, CONVERT(VARCHAR, MET006.fechaD, 105) AS fechaD, MET006.resultado, MET006.cantidad, MET006.volumen, MET006.tiempo, MET006.aceite, MET006.obs, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, (SEG001.nombre +' '+ SEG001.ap1) AS analista
		FROM MUE002 INNER JOIN MET006 ON MUE002.id = MET006.ensayo INNER JOIN MUE001 ON MUE002.xanalizar = MUE001.id INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN SEG001 ON MUE002.analista = SEG001.id
		WHERE (MUE002.id = '{$_ensayo}') AND (MUEBIT.accion = '0');");
    }

    function DilucionEncabezadoSet($_xanalizar, $_tipo, $_ingrediente, $_fechaA, $_tipo_form, $_dosis, $_fechaP, $_maria, $_numreactivo, $_fechaD, $_resultado, $_cantidad, $_volumen, $_tiempo, $_aceite, $_obs, $_UID)
    {
        $_ingrediente = $this->Free($_ingrediente);
        $_dosis = $this->Free($_dosis);
        $_numreactivo = $this->Free($_numreactivo);
        $_obs = $this->Free($_obs);
        $_fechaA = $this->_FECHA($_fechaA);
        $_fechaP = $this->_FECHA($_fechaP);
        $_fechaD = $this->_FECHA($_fechaD);

        $this->InvalidaAnteriores($_xanalizar, $_tipo);

        $cs = $this->ConsecutivoEnsayoGet();

        return $this->_TRANS("EXECUTE PA_MAG_097 '{$cs}',
			'{$_xanalizar}', 
			'{$_tipo}', 
			'{$_ingrediente}',  
			'{$_fechaA}', 
			'{$_tipo_form}', 
			'{$_dosis}', 
			'{$_fechaP}', 
			'{$_maria}',
			'{$_numreactivo}', 
			'{$_fechaD}', 
			'{$_resultado}', 
			'{$_cantidad}',
			'{$_volumen}',  
			'{$_tiempo}', 
			'{$_aceite}',
			'{$_obs}',
			'{$_UID}';");
    }

    function DilucionEncabezadoVacio($_xanalizar)
    {
        $ROW = $this->_QUERY("SELECT MUE000.ref, '' AS ingrediente, '' AS tipo_form, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, MUE000.solicitud, MUE001.analisis
		FROM MUE001 INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra
		WHERE (MUEBIT.accion = '0' OR MUEBIT.accion = 'A') AND (MUE001.id = '{$_xanalizar}');");
        //
        $ROW2 = $this->_QUERY("SELECT SER004.rango, SER004.unidad, VAR007.nombre AS tipo_form, VAR005.nombre AS ingrediente
		FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR007 ON SER003.tipo_form = VAR007.id INNER JOIN VAR005 ON SER004.analisis = VAR005.id
		WHERE (SER003.cs = '{$ROW[0]['solicitud']}') AND (SER004.rango != '');");
        if ($ROW2) {
            $ROW[0]['ingrediente'] = $ROW2[0]['ingrediente'];
            $ROW[0]['tipo_form'] = $ROW2[0]['tipo_form'];
            $ROW[0]['rango'] = $ROW2[0]['rango'];
            $ROW[0]['unidad'] = $ROW2[0]['unidad'];
        }

        $ROW2 = $this->_QUERY("SELECT VAR005.nombre AS ingrediente FROM VAR005 INNER JOIN SER004 ON VAR005.id = SER004.analisis WHERE (SER004.solicitud = '{$ROW[0]['solicitud']}') AND (SER004.tipo = '1');");
        $ROW[0]['ingrediente'] = $ROW2[0]['ingrediente'];
        //VACIOS
        list(
            $ROW[0]['fechaA'],
            $ROW[0]['fechaC'],
            $ROW[0]['dosis'],
            $ROW[0]['metodo'],
            $ROW[0]['fechaP'],
            $ROW[0]['maria'],
            $ROW[0]['numreactivo'],
            $ROW[0]['fechaD'],
            $ROW[0]['resultado'],
            $ROW[0]['cantidad'],
            $ROW[0]['volumen'],
            $ROW[0]['tiempo'],
            $ROW[0]['aceite'],
            $ROW[0]['obs']
            ) = array('', '', '', '', '', '20', '', '', '0', '5', '0', '18', '0', '');

        return $ROW;
    }

    function DitioEncabezadoGet($_ensayo)
    {
        return $this->_QUERY("SELECT MUE000.ref, MUE002.tipo, CONVERT(VARCHAR, MUE002.fecha, 105) AS fechaC, MUE002.ingrediente, MUE002.declarada AS rango, MUE002.unidad, MET004.aceite, CONVERT(VARCHAR, MET004.fechaA, 105) AS fechaA, MET004.tipo_form, MET004.numreactivo, CONVERT(VARCHAR, MET004.fechaP, 105) AS fechaP, MET004.IECB, MET004.linealidad1, 
		MET004.linealidad2, MET004.ampolla1, MET004.ampolla2, MET004.repeti1, MET004.repeti2, MET004.muestra1, MET004.muestra2, 
		MET004.BA, MET004.consumido1, MET004.consumido2, MET004.aforado1, MET004.aforado2, MET004.masa,
		MET004.densidad, MET004.obs, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, (SEG001.nombre +' '+ SEG001.ap1) AS analista
		FROM MUE002 INNER JOIN MET004 ON MUE002.id = MET004.ensayo INNER JOIN MUE001 ON MUE002.xanalizar = MUE001.id INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN SEG001 ON MUE002.analista = SEG001.id
		WHERE (MUE002.id = '{$_ensayo}') AND (MUEBIT.accion = '0');");
    }

    function DitioEncabezadoSet($_xanalizar, $_tipo, $_ingrediente, $_fechaA, $_rango, $_unidad, $_tipo_form, $_aceite, $_numreactivo, $_fechaP, $_IECB, $_linealidad1, $_linealidad2, $_ampolla1, $_ampolla2, $_repeti1, $_repeti2, $_muestra1, $_muestra2, $_BA, $_consumido1, $_consumido2, $_contenido1, $_contenido2, $_contenido3, $_contenido4, $_aforado1, $_aforado2, $_masa, $_densidad, $_promedio, $_incertidumbre, $_obs, $_UID, $_UNAME)
    {
        $_ingrediente = $this->Free($_ingrediente);
        $_rango = $this->Free($_rango);
        $_numreactivo = $this->Free($_numreactivo);
        $_obs = $this->Free($_obs);
        $_fechaP = $this->_FECHA($_fechaP);

        $this->InvalidaAnteriores($_xanalizar, $_tipo);

        $cs = $this->ConsecutivoEnsayoGet();

        if ($this->_TRANS("EXECUTE PA_MAG_095 '{$cs}',
			'{$_xanalizar}', 
			'{$_tipo}', 
			'{$_ingrediente}', 
			'{$_fechaA}', 
			'{$_rango}', 
			'{$_unidad}', 
			'{$_tipo_form}', 
			'{$_aceite}',
			'{$_numreactivo}', 
			'{$_fechaP}', 
			'{$_IECB}', 
			'{$_linealidad1}', 
			'{$_linealidad2}', 
			'{$_ampolla1}', 
			'{$_ampolla2}', 
			'{$_repeti1}', 
			'{$_repeti2}', 
			'{$_muestra1}', 
			'{$_muestra2}', 
			'{$_BA}', 
			'{$_consumido1}', 
			'{$_consumido2}', 
			'{$_aforado1}', 
			'{$_aforado2}', 				
			'{$_masa}', 
			'{$_densidad}',
			'{$_promedio}', 
			'{$_incertidumbre}', 
			'{$_obs}',
			'{$_UID}';")) {
            if ($_unidad == '0') {
                $this->GrafControlIME($_ingrediente, $_rango, $_UNAME, $_xanalizar, $_tipo_form, $_contenido1, $_contenido2);
            } else {
                $this->GrafControlIME($_ingrediente, $_rango, $_UNAME, $_xanalizar, $_tipo_form, $_contenido3, $_contenido4);
            }
            return 1;
        } else
            return 0;
    }

    function DitioEncabezadoVacio($_xanalizar)
    {
        $ROW = $this->_QUERY("SELECT MUE000.ref, '' AS ingrediente, '' AS tipo_form, '' AS rango, '' AS unidad, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, MUE000.solicitud, MUE001.analisis
		FROM MUE001 INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra
		WHERE (MUEBIT.accion = '0' OR MUEBIT.accion = 'A') AND (MUE001.id = '{$_xanalizar}');");
        //
        $ROW2 = $this->_QUERY("SELECT SER004.rango, SER004.unidad, VAR007.nombre AS tipo_form, VAR005.nombre AS ingrediente
		FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR007 ON SER003.tipo_form = VAR007.id INNER JOIN VAR005 ON SER004.analisis = VAR005.id
		WHERE (SER003.cs = '{$ROW[0]['solicitud']}') AND (SER004.analisis = {$ROW[0]['analisis']});");
        if ($ROW2) {
            $ROW[0]['ingrediente'] = $ROW2[0]['ingrediente'];
            $ROW[0]['tipo_form'] = $ROW2[0]['tipo_form'];
            $ROW[0]['rango'] = $ROW2[0]['rango'];
            $ROW[0]['unidad'] = $ROW2[0]['unidad'];
        }
        //VACIOS
        $ROW[0]['obs'] = '';
        $ROW[0]['fechaP'] = '';
        $ROW[0]['numreactivo'] = '';
        $ROW[0]['fechaA'] = '';
        $ROW[0]['fechaC'] = '';
        $ROW[0]['masa'] = '0';
        $ROW[0]['muestra1'] = '0';
        $ROW[0]['muestra2'] = '0';
        $ROW[0]['consumido1'] = '0';
        $ROW[0]['consumido2'] = '0';
        $ROW[0]['aforado1'] = '';
        $ROW[0]['aforado2'] = '';
        $ROW[0]['aceite'] = '';
        $ROW[0]['densidad'] = '0';
        $ROW[0]['ampolla1'] = '0';
        $ROW[0]['ampolla2'] = '0';
        $ROW[0]['BA'] = '';
        $ROW[0]['linealidad1'] = '0.0001';
        $ROW[0]['linealidad2'] = '0.0001';
        $ROW[0]['repeti1'] = '0.00007';
        $ROW[0]['repeti2'] = '0.00007';
        $ROW[0]['IECB'] = '0.033';

        return $ROW;
    }

    function DumasEncabezadoGet($_ensayo)
    {
        return $this->_QUERY("SELECT MUE000.solicitud, MUE000.ref, MUE002.tipo, CONVERT(VARCHAR, MUE002.fecha, 105) AS fechaC, MUE002.declarada AS rango, MUE002.unidad, CONVERT(VARCHAR, MET011.fechaA, 105) AS fechaA, MET011.nitro1, MET011.nitro2, MET011.nitro3, MET011.masa1, MET011.masa2, MET011.masa3, MET011.delta1, MET011.delta2, MET011.repetibilidad1, 
        MET011.resolucion1, MET011.curva, MET011.repetibilidad2, MET011.resolucion2, MET011.inc_cer, MET011.emp, CONVERT(TEXT, MET011.obs) AS obs, VAR005.nombre AS elemento, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI,
       MET_INC.certificado_densidad AS icCert,  MET_INC.resolucion_densidad AS icRes, met_inc.reptibilidad_masa As rep, met_inc.resolucion_masa As resM,
	   met_inc.linealidad_masa As lilM,  met_inc.excentricidad_masa As exM
		FROM MUE000 INNER JOIN MUE001 ON MUE000.id = MUE001.muestra INNER JOIN MUE002 ON MUE001.id = MUE002.xanalizar INNER JOIN MET011 ON MUE002.id = MET011.ensayo INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN VAR005 ON MUE001.analisis = VAR005.id left outer join MET_INC on MET011.ensayo = MET_INC.ensayo
		WHERE (MET011.ensayo = '{$_ensayo}') AND (MUEBIT.accion = '0');");
    }

    //esta funcion inserta en met_inc los valores de las tablas de los componentes de incertidumbre. Se podria utilizar en todos los que lo requieran
    //se debe enviar los parametros en este orden: incertidumbre por certificado de densidad, resolucion, IC combinada, repetiilidad, resolucion, linealidad, excentricidad, ic balanza
  //esta funcion se debe invocar en el boton del metodo donde se desea usar, ejemplo: ver hoja 03_dumas.php (baltha) en la linea 380

    //se debe modificar el innerjoin en cada metodo, para que jale los valores y los pinte

    function inserta($_incCertificado, $_incResolucion, $_ic_combinada, $_repetibilidad, $_resolucion, $_linealidad, $_excentricidad, $_ic_balanza){
        //esta funcion busca el consecutivo. NO ACTUALIZA, SOLO MUESTRA
        $cs = $this->ConsecutivoEnsayoGet();
        error_log("INSERT INTO MET_INC values('{$cs}', '{$_incCertificado}', '{$_incResolucion}', '{$_ic_combinada}', '{$_repetibilidad}', '{$_resolucion}', '{$_linealidad}', '{$_excentricidad}', '{$_ic_balanza}');");
        $this->_TRANS("INSERT INTO MET_INC values('{$cs}', '{$_incCertificado}', '{$_incResolucion}', '{$_ic_combinada}', '{$_repetibilidad}', '{$_resolucion}', '{$_linealidad}', '{$_excentricidad}', '{$_ic_balanza}');");
    }

    function DumasEncabezadoSet($_xanalizar, $_tipo, $_fechaA, $_rango, $_unidad, $_nitro1, $_nitro2, $_nitro3, $_masa1, $_masa2, $_masa3, $_delta1, $_repetibilidad1, $_resolucion1, $_curva, $_repetibilidad2, $_resolucion2, $_inc_cer, $_emp, $_CD, $_IC, $_obs, $_UID)
    {
        $_obs = $this->Free($_obs);
        $_fechaA = $this->_FECHA2($_fechaA);

        $this->InvalidaAnteriores($_xanalizar, $_tipo);

        $cs = $this->ConsecutivoEnsayoGet();

        return $this->_TRANS("EXECUTE PA_MAG_108 '{$cs}',
			'{$_xanalizar}', 
			'{$_tipo}', 
			'{$_fechaA}', 
			'{$_rango}', 
			'{$_unidad}', 
			'{$_nitro1}', 
			'{$_nitro2}',
			'{$_nitro3}',
			'{$_masa1}',
			'{$_masa2}',
			'{$_masa3}',
			'{$_delta1}',			
			'{$_repetibilidad1}',
			'{$_resolucion1}',
			'{$_curva}',
			'{$_repetibilidad2}',
			'{$_resolucion2}',
			'{$_inc_cer}',
			'{$_emp}',
			'{$_CD}', 
			'{$_IC}',
			'{$_obs}',
			'{$_UID}';");
    }

    function DumasEncabezadoVacio($_xanalizar)
    {
        $ROW = $this->_QUERY("SELECT '' AS rango, '' AS unidad, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, MUE000.solicitud, MUE000.ref, MUE001.analisis, VAR005.nombre AS elemento
		FROM MUE001 INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN VAR005 ON MUE001.analisis = VAR005.id
		WHERE (MUEBIT.accion = '0' OR MUEBIT.accion = 'A') AND (MUE001.id = '{$_xanalizar}');");
        //
        $ROW2 = $this->_QUERY("SELECT SER004.rango, SER004.unidad
		FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud
		WHERE (SER003.cs = '{$ROW[0]['solicitud']}') AND (SER004.analisis = {$ROW[0]['analisis']});");
        if ($ROW2) {
            $ROW[0]['rango'] = $ROW2[0]['rango'];
            $ROW[0]['unidad'] = $ROW2[0]['unidad'];
        }

        list(
            $ROW[0]['fechaA'],
            $ROW[0]['fechaC'],
            $ROW[0]['nitro1'],
            $ROW[0]['nitro2'],
            $ROW[0]['nitro3'],
            $ROW[0]['masa1'],
            $ROW[0]['masa2'],
            $ROW[0]['masa3'],
            $ROW[0]['delta1'],
           // $ROW[0]['delta2'],
            $ROW[0]['repetibilidad1'],
            $ROW[0]['resolucion1'],
            $ROW[0]['inc_cer1'],
            $ROW[0]['emp1'],
            $ROW[0]['precision'],
            $ROW[0]['curva'],
            $ROW[0]['rep'],
            $ROW[0]['resM'],
            $ROW[0]['lilM'],
            $ROW[0]['exM'],
            $ROW[0]['obs'],
            $ROW[0]['icCert'],
            $ROW[0]['icRes']
            ) = array('', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0','0');

        return $ROW;
    }

    function EmulsionEncabezadoGet($_ensayo)
    {
        return $this->_QUERY("SELECT MUE000.ref, MUE002.tipo, CONVERT(VARCHAR, MUE002.fecha, 105) AS fechaC, MUE002.ingrediente, CONVERT(VARCHAR, MET015.fechaA, 105) AS fechaA, MET015.tipo_form, MET015.dosis, CONVERT(VARCHAR, MET015.fechaP, 105) AS fechaP, MET015.maria, 
        MET015.numreactivo, CONVERT(VARCHAR, MET015.fechaD, 105) AS fechaD, MET015.resultado, MET015.cantidad, MET015.cremado1,
		MET015.cremado2, MET015.cremado3, MET015.cremado4, MET015.cremado5, MET015.cremado6, MET015.cremado7, MET015.cremado8, MET015.clari1, MET015.espon1, MET015.aceite1, MET015.aceite2, MET015.aceite3, MET015.aceite4, MET015.aceite5, MET015.aceite6, MET015.aceite7, MET015.aceite8, MET015.clari2, MET015.espon2, MET015.obs, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, (SEG001.nombre +' '+ SEG001.ap1) AS analista
		FROM MUE002 INNER JOIN MET015 ON MUE002.id = MET015.ensayo INNER JOIN MUE001 ON MUE002.xanalizar = MUE001.id INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN SEG001 ON MUE002.analista = SEG001.id
		WHERE (MUE002.id = '{$_ensayo}') AND (MUEBIT.accion = '0');");
    }

    function EmulsionEncabezadoSet($_xanalizar, $_tipo, $_ingrediente, $_fechaA, $_tipo_form, $_dosis, $_fechaP, $_maria, $_numreactivo, $_fechaD, $_resultado, $_cantidad, $_cremado1, $_cremado2, $_cremado3, $_cremado4, $_cremado5, $_cremado6, $_cremado7, $_cremado8, $_clari1, $_espon1, $_aceite1, $_aceite2, $_aceite3, $_aceite4, $_aceite5, $_aceite6, $_aceite7, $_aceite8, $_clari2, $_espon2, $_promA2, $_promA3, $_promA4, $_promB2, $_promB3, $_promB4, $_obs, $_UID)
    {
        $_ingrediente = $this->Free($_ingrediente);
        $_dosis = $this->Free($_dosis);
        $_numreactivo = $this->Free($_numreactivo);
        $_obs = $this->Free($_obs);
        $_fechaA = $this->_FECHA($_fechaA);
        $_fechaP = $this->_FECHA($_fechaP);
        $_fechaD = $this->_FECHA($_fechaD);

        $this->InvalidaAnteriores($_xanalizar, $_tipo);

        $cs = $this->ConsecutivoEnsayoGet();

        return $this->_TRANS("EXECUTE PA_MAG_116 '{$cs}',
			'{$_xanalizar}', 
			'{$_tipo}', 
			'{$_ingrediente}',  
			'{$_fechaA}', 
			'{$_tipo_form}', 
			'{$_dosis}', 
			'{$_fechaP}', 
			'{$_maria}',
			'{$_numreactivo}', 
			'{$_fechaD}', 
			'{$_resultado}', 
			'{$_cantidad}',
			'{$_cremado1}',
			'{$_cremado2}',
			'{$_cremado3}',
			'{$_cremado4}',
			'{$_cremado5}',
			'{$_cremado6}',
			'{$_cremado7}',
			'{$_cremado8}',
			'{$_clari1}',
			'{$_espon1}',
			'{$_aceite1}',
			'{$_aceite2}',
			'{$_aceite3}',
			'{$_aceite4}',
			'{$_aceite5}',
			'{$_aceite6}',
			'{$_aceite7}',
			'{$_aceite8}',
			'{$_clari2}',
			'{$_espon2}',
			'{$_promA2}',
			'{$_promA3}',
			'{$_promA4}',
			'{$_promB2}',
			'{$_promB3}',
			'{$_promB4}',
			'{$_obs}',
			'{$_UID}';");
    }

    function EmulsionEncabezadoVacio($_xanalizar)
    {
        $ROW = $this->_QUERY("SELECT MUE000.ref, '' AS ingrediente, '' AS tipo_form, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, MUE000.solicitud, MUE001.analisis
		FROM MUE001 INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra
		WHERE (MUEBIT.accion = '0' OR MUEBIT.accion = 'A') AND (MUE001.id = '{$_xanalizar}');");
        //
        /* $ROW2 = $this->_QUERY("SELECT VAR007.nombre AS tipo_form
          FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR007 ON SER003.tipo_form = VAR007.id INNER JOIN VAR005 ON SER004.analisis = VAR005.id
          WHERE (SER003.cs = '{$ROW[0]['solicitud']}') AND (SER004.analisis = {$ROW[0]['analisis']});");
          if($ROW2){
          $ROW[0]['tipo_form'] = $ROW2[0]['tipo_form'];
          } */
        $ROW2 = $this->_QUERY("SELECT SER004.rango, SER004.unidad, VAR007.nombre AS tipo_form, VAR005.nombre AS ingrediente
		FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR007 ON SER003.tipo_form = VAR007.id INNER JOIN VAR005 ON SER004.analisis = VAR005.id
		WHERE (SER003.cs = '{$ROW[0]['solicitud']}') AND (SER004.rango != '');");
        if ($ROW2) {
            $ROW[0]['ingrediente'] = $ROW2[0]['ingrediente'];
            $ROW[0]['tipo_form'] = $ROW2[0]['tipo_form'];
            $ROW[0]['rango'] = $ROW2[0]['rango'];
            $ROW[0]['unidad'] = $ROW2[0]['unidad'];
        }
        //VACIOS
        $ROW[0]['fechaA'] = '';
        $ROW[0]['fechaC'] = '';
        $ROW[0]['dosis'] = '0';
        $ROW[0]['fechaP'] = '';
        $ROW[0]['maria'] = '30';
        $ROW[0]['numreactivo'] = '';
        $ROW[0]['fechaD'] = '';
        $ROW[0]['resultado'] = '0';
        $ROW[0]['cantidad'] = '5';
        $ROW[0]['clari1'] = '';
        $ROW[0]['clari2'] = '';
        $ROW[0]['espon1'] = '';
        $ROW[0]['espon2'] = '';
        $ROW[0]['cremado1'] = '0';
        $ROW[0]['cremado2'] = '0';
        $ROW[0]['cremado3'] = '0';
        $ROW[0]['cremado4'] = '0';
        $ROW[0]['cremado5'] = '0';
        $ROW[0]['cremado6'] = '0';
        $ROW[0]['cremado7'] = '0';
        $ROW[0]['cremado8'] = '0';
        $ROW[0]['aceite1'] = '0';
        $ROW[0]['aceite2'] = '0';
        $ROW[0]['aceite3'] = '0';
        $ROW[0]['aceite4'] = '0';
        $ROW[0]['aceite5'] = '0';
        $ROW[0]['aceite6'] = '0';
        $ROW[0]['aceite7'] = '0';
        $ROW[0]['aceite8'] = '0';
        $ROW[0]['obs'] = '';

        return $ROW;
    }

    function EnsayoAnalistaGet($_ensayo)
    {
        return $this->_QUERY("SELECT CONVERT(VARCHAR, MUE002.fecha, 105) AS fecha, (SEG001.nombre +' '+ SEG001.ap1 +' '+ SEG001.ap2) AS analista
		FROM MUE002 INNER JOIN SEG001 ON MUE002.analista = SEG001.id
		WHERE (MUE002.id = '{$_ensayo}');");
    }

    function EtefonEncabezadoGet($_ensayo)
    {
        return $this->_QUERY("SELECT MUE000.ref, MUE002.tipo, CONVERT(VARCHAR, MUE002.fecha, 105) AS fechaC, MUE002.ingrediente, MUE002.declarada AS rango, MUE002.unidad, CONVERT(VARCHAR, MET005.fechaA, 105) AS fechaA, MET005.tipo_form, MET005.masa, MET005.muestra1, 
        MET005.muestra2, MET005.numreactivo, MET005.consumido1, MET005.consumido2, CONVERT(VARCHAR, MET005.fechaP, 105) AS fechaP, MET005.IECB, MET005.linealidad1, MET005.linealidad2, MET005.repeti1, MET005.repeti2,
        MET005.yodo, MET005.yodo2, MET005.densidad, MET005.obs, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, (SEG001.nombre +' '+ SEG001.ap1) AS analista
		FROM MUE002 INNER JOIN MET005 ON MUE002.id = MET005.ensayo INNER JOIN MUE001 ON MUE002.xanalizar = MUE001.id INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN SEG001 ON MUE002.analista = SEG001.id
		WHERE (MUE002.id = '{$_ensayo}') AND (MUEBIT.accion = '0');");
    }

    function EtefonEncabezadoSet($_xanalizar, $_tipo, $_ingrediente, $_fechaA, $_rango, $_unidad, $_tipo_form, $_masa, $_muestra1, $_muestra2, $_numreactivo, $_consumido1, $_consumido2, $_fechaP, $_IECB, $_linealidad1, $_linealidad2, $_repeti1, $_repeti2, $_yodo, $_yodo2, $_densidad, $_promedio, $_incertidumbre, $_obs, $_contenido1, $_contenido2, $_UID, $_UNAME)
    {
        $_ingrediente = $this->Free($_ingrediente);
        $_rango = $this->Free($_rango);
        $_numreactivo = $this->Free($_numreactivo);
        $_obs = $this->Free($_obs);
        $_fechaA = $this->_FECHA($_fechaA);
        $_fechaP = $this->_FECHA($_fechaP);

        $this->InvalidaAnteriores($_xanalizar, $_tipo);

        $cs = $this->ConsecutivoEnsayoGet();

        if ($this->_TRANS("EXECUTE PA_MAG_096 '{$cs}',
			'{$_xanalizar}', 
			'{$_tipo}', 
			'{$_ingrediente}',  
			'{$_fechaA}', 
			'{$_rango}', 
			'{$_unidad}', 
			'{$_tipo_form}', 
			'{$_masa}', 
			'{$_muestra1}', 
			'{$_muestra2}', 
			'{$_numreactivo}', 
			'{$_consumido1}', 
			'{$_consumido2}', 
			'{$_fechaP}',  
			'{$_IECB}', 
			'{$_linealidad1}', 
			'{$_linealidad2}', 
			'{$_repeti1}', 
			'{$_repeti2}', 
			'{$_yodo}',
			'{$_yodo2}', 
			'{$_densidad}',
			'{$_promedio}',
			'{$_incertidumbre}',
			'{$_obs}',
			'{$_UID}';")) {
            $this->GrafControlIME($_ingrediente, $_rango, $_UNAME, $_xanalizar, $_tipo_form, $_contenido1, $_contenido2);
            return 1;
        } else
            return 0;
    }

    function EtefonEncabezadoVacio($_xanalizar)
    {
        $ROW = $this->_QUERY("SELECT MUE000.ref, '' AS ingrediente, '' AS tipo_form, '' AS rango, '' AS unidad, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, MUE000.solicitud, MUE001.analisis
		FROM MUE001 INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra
		WHERE (MUEBIT.accion = '0' OR MUEBIT.accion = 'A') AND (MUE001.id = '{$_xanalizar}');");
        //
        $ROW2 = $this->_QUERY("SELECT SER004.rango, SER004.unidad, VAR007.nombre AS tipo_form, VAR005.nombre AS ingrediente
		FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR007 ON SER003.tipo_form = VAR007.id INNER JOIN VAR005 ON SER004.analisis = VAR005.id
		WHERE (SER003.cs = '{$ROW[0]['solicitud']}') AND (SER004.analisis = {$ROW[0]['analisis']});");
        if ($ROW2) {
            $ROW[0]['ingrediente'] = $ROW2[0]['ingrediente'];
            $ROW[0]['tipo_form'] = $ROW2[0]['tipo_form'];
            $ROW[0]['rango'] = $ROW2[0]['rango'];
            $ROW[0]['unidad'] = $ROW2[0]['unidad'];
        }
        //
        $ROW[0]['aceite'] = '';
        $ROW[0]['obs'] = '';
        $ROW[0]['fechaP'] = '';
        $ROW[0]['numreactivo'] = '';
        $ROW[0]['fechaA'] = '';
        $ROW[0]['fechaC'] = '';
        $ROW[0]['masa'] = '144.5';
        $ROW[0]['muestra1'] = '0';
        $ROW[0]['muestra2'] = '0';
        $ROW[0]['consumido1'] = '0';
        $ROW[0]['consumido2'] = '0';
        $ROW[0]['yodo'] = '0';
        $ROW[0]['yodo2'] = '0';
        $ROW[0]['densidad'] = '0';
        $ROW[0]['linealidad1'] = '0.0001';
        $ROW[0]['linealidad2'] = '0.0001';
        $ROW[0]['repeti1'] = '0.0007';
        $ROW[0]['repeti2'] = '0.0007';
        $ROW[0]['IECB'] = '0.033';

        return $ROW;
    }

    function GeneralEncabezadoGet($_ensayo)
    {
        $Q = $this->_QUERY("SELECT MUE000.ref, MUE002.tipo, CONVERT(VARCHAR, MUE002.fecha, 105) AS fechaC, MUE002.ingrediente, MUE002.declarada AS rango, MUE002.unidad, MUE002.clasificacion, CONVERT(VARCHAR, MET020.fechaA, 105) AS fechaA, MET020.tipo_form, MET020.densidad, MET020.incden, MET020.promedio AS ce, MET020.incertidumbre AS ie, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, (SEG001.nombre +' '+ SEG001.ap1) AS analista
		FROM MUE002 INNER JOIN MET020 ON MUE002.id = MET020.ensayo INNER JOIN MUE001 ON MUE002.xanalizar = MUE001.id INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN SEG001 ON MUE002.analista = SEG001.id
		WHERE (MUE002.id = '{$_ensayo}') AND (MUEBIT.accion = '0');");
        if (!$Q) {
            $Q = $this->_QUERY("SELECT MUE000.ref, MUE002.tipo, CONVERT(VARCHAR, MUE002.fecha, 105) AS fechaC, MUE002.ingrediente, MUE002.declarada AS rango, MUE002.unidad, MUE002.clasificacion, '' AS fechaA, '' AS tipo_form, '0' AS densidad, '0' AS incden, 'NC' AS ce, 'NA' AS ie, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, (SEG001.nombre +' '+ SEG001.ap1) AS analista
		FROM MUE002 INNER JOIN MUE001 ON MUE002.xanalizar = MUE001.id INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN SEG001 ON MUE002.analista = SEG001.id
		WHERE (MUE002.id = '{$_ensayo}') AND (MUEBIT.accion = '0');");
        }
        return $Q;
    }

    function GeneralEncabezadoSet($_xanalizar, $_tipo, $_ingrediente, $_fechaA, $_rango, $_unidad, $_tipo_form, $_ce, $_ie, $_densidad, $_incden, $_UID, $_tA)
    {
        $_ingrediente = utf8_encode($this->Free($_ingrediente));
        $_rango = $this->Free($_rango);
        $_fechaA = $this->_FECHA2($_fechaA);
        $this->InvalidaAnteriores($_xanalizar, $_tipo);

        $cs = $this->ConsecutivoEnsayoGet();
        $this->_TRANS("EXECUTE PA_MAG_129 '{$cs}','{$_xanalizar}', '{$_tipo}', '{$_ingrediente}', '{$_fechaA}', '{$_rango}', '{$_unidad}','{$_tipo_form}', '{$_densidad}','{$_incden}',
				'{$_ce}', 
				'{$_ie}', 
				'{$_UID}';");
        return $this->_TRANS("UPDATE MUE002 SET clasificacion = '{$_tA}' WHERE id = '{$cs}';");
    }

    function GeneralEncabezadoVacio($_xanalizar)
    {
        $ROW = $this->_QUERY("SELECT MUE000.ref, '' AS ingrediente, '' AS tipo_form, '' AS rango, '' AS unidad, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, MUE000.solicitud, MUE001.analisis
		FROM MUE001 INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra
		WHERE (MUEBIT.accion = '0' OR MUEBIT.accion = 'A') AND (MUE001.id = '{$_xanalizar}');");
        //VALIDO QUE SEA UNA IMPUREZA
        $ROW2 = $this->_QUERY("SELECT SER004.rango, SER004.unidad, VAR007.nombre AS tipo_form, VAR005.nombre AS ingrediente
		FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR007 ON SER003.tipo_form = VAR007.id INNER JOIN VAR005 ON SER004.analisis = VAR005.id
		WHERE (SER003.cs = '{$ROW[0]['solicitud']}') AND (SER004.analisis = {$ROW[0]['analisis']});");
        if ($ROW2) {
            $ROW[0]['ingrediente'] = $ROW2[0]['ingrediente'];
            $ROW[0]['tipo_form'] = $ROW2[0]['tipo_form'];
            $ROW[0]['rango'] = $ROW2[0]['rango'];
            $ROW[0]['unidad'] = $ROW2[0]['unidad'];
        }
        /* $ROW2 = $this->_QUERY("SELECT nombre AS ingrediente, tipo FROM VAR005 WHERE (id={$ROW[0]['analisis']});");
          $ROW[0]['ingrediente'] = $ROW2[0]['ingrediente'];
          $ROW3 = $this->_QUERY("SELECT rango, unidad FROM SER004 WHERE solicitud = '{$ROW[0]['solicitud']}';");
          $ROW[0]['rango'] = $ROW3[0]['rango'];
          $ROW[0]['unidad'] = $ROW3[0]['unidad'];
          if($ROW2[0]['tipo']=='0'){
          //SI ES IMPUREZA, OBTENGO LA LA FORMULACION, EL CONC. DECLARADA Y LA UNIDAD, DEL ANALISIS LLAMADO IMPUREZA
          $ROW2 = $this->_QUERY("SELECT '{$ROW2[0]['ingrediente']}' AS ingrediente, SER004.rango, SER004.unidad, VAR007.nombre AS tipo_form
          FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR007 ON SER003.tipo_form = VAR007.id INNER JOIN VAR005 ON SER004.analisis = VAR005.id
          WHERE (SER003.cs = '{$ROW[0]['solicitud']}') AND (VAR005.nombre = 'Impurezas');");
          if($ROW2){
          $ROW[0]['ingrediente'] = $ROW2[0]['ingrediente'];
          $ROW[0]['tipo_form'] = $ROW2[0]['tipo_form'];
          $ROW[0]['rango'] = $ROW2[0]['rango'];
          $ROW[0]['unidad'] = $ROW2[0]['unidad'];
          }
          } */

        $ROW[0]['fechaA'] = '';
        $ROW[0]['fechaC'] = '';
        $ROW[0]['ce'] = '0';
        $ROW[0]['ie'] = '0';
        $ROW[0]['densidad'] = '0';
        $ROW[0]['incden'] = '0';

        return $ROW;
    }

    function GrafControlIME($_ingrediente, $_rango, $_analista, $_muestra, $_formulacion, $_resultado1, $_resultado2)
    {
        $B = $this->_QUERY("SELECT id, ingrediente FROM MET023 WHERE ingrediente = '{$_ingrediente}' AND declarada = '{$_rango}' AND formulacion = '{$_formulacion}';");
        if (!$B) {
            $B = $this->_QUERY("SELECT MAX(id+1) AS id FROM MET023;");
            $this->_TRANS("INSERT INTO MET023 values('{$B[0]['id']}', '{$_ingrediente}', '{$_rango}', '{$_formulacion}', '1', '', '1');");
        }
        $C = $this->_QUERY("SELECT MAX(linea+1) AS linea FROM MET024 WHERE ingrediente = '{$B[0]['id']}';");
        $D = $this->_QUERY("SELECT muestra FROM MUE001 WHERE id = '{$_muestra}';");
        $this->_TRANS("INSERT INTO MET024 values('{$B[0]['id']}', '{$C[0]['linea']}', GETDATE(), '{$_analista}', '{$D[0]['muestra']}', '{$_resultado1}', '{$_resultado2}');");
    }

    function GrafControllMuestraEnc($ing = '', $dec = '', $for = '')
    {
        if ($ing == '')
            return $this->_QUERY("SELECT id, ingrediente, declarada, formulacion, version, observaciones, hoja FROM MET023;");
        else
            return $this->_QUERY("SELECT id, ingrediente, declarada, formulacion, version, observaciones, hoja FROM MET023 WHERE ingrediente = '{$ing}' AND declarada = '{$dec}' AND formulacion = '{$for}';");
    }

    function GrafControllMuestraDet($ing)
    {
        return $this->_QUERY("SELECT ingrediente, linea, CONVERT(VARCHAR, fecha, 105) AS fecha, fecha AS fechax, analista, muestra, replica1, replica2 FROM MET024 WHERE ingrediente = '{$ing}' ORDER BY fechax;");
    }

    function GrafControllMuestraObs($ing)
    {
        return $this->_QUERY("SELECT ingrediente, linea, observacion FROM MET025 WHERE ingrediente = '{$ing}';");
    }

    function GrafControllObs($_id, $_obs)
    {
        if ($this->_TRANS("INSERT INTO MET025 VALUES('{$_id}', '1', '{$_obs}');"))
            return 1;
        else
            return 0;
    }

    function Metodo($_id)
    {
        $lolo = $this->_QUERY("SELECT nombre FROM MET000 WHERE id = '{$_id}';");
        return $lolo[0]['nombre'];
    }

    function HumectabilidadEncabezadoGet($_ensayo)
    {
        return $this->_QUERY("SELECT MUE000.ref, MUE002.tipo, CONVERT(VARCHAR, MUE002.fecha, 105) AS fechaC, MUE002.ingrediente, CONVERT(VARCHAR, MET007.fechaA, 105) AS fechaA, MET007.tipo_form, MET007.dosis, CONVERT(VARCHAR, MET007.fechaP, 105) AS fechaP, MET007.maria, 
        MET007.numreactivo, CONVERT(VARCHAR, MET007.fechaD, 105) AS fechaD, MET007.resultado, MET007.cantidad, MET007.volumen, MET007.obs, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, (SEG001.nombre +' '+ SEG001.ap1) AS analista
		FROM MUE002 INNER JOIN MET007 ON MUE002.id = MET007.ensayo INNER JOIN MUE001 ON MUE002.xanalizar = MUE001.id INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN SEG001 ON MUE002.analista = SEG001.id
		WHERE (MUE002.id = '{$_ensayo}') AND (MUEBIT.accion = '0');");
    }

    function HumectabilidadEncabezadoSet($_xanalizar, $_tipo, $_ingrediente, $_fechaA, $_tipo_form, $_dosis, $_fechaP, $_maria, $_numreactivo, $_fechaD, $_resultado, $_cantidad, $_volumen, $_obs, $_UID)
    {
        $_ingrediente = $this->Free($_ingrediente);
        $_dosis = $this->Free($_dosis);
        $_numreactivo = $this->Free($_numreactivo);
        $_obs = $this->Free($_obs);
        $_fechaA = $this->_FECHA($_fechaA);
        $_fechaP = $this->_FECHA($_fechaP);
        $_fechaD = $this->_FECHA($_fechaD);

        $this->InvalidaAnteriores($_xanalizar, $_tipo);

        $cs = $this->ConsecutivoEnsayoGet();

        return $this->_TRANS("EXECUTE PA_MAG_098 '{$cs}',
			'{$_xanalizar}', 
			'{$_tipo}', 
			'{$_ingrediente}',  
			'{$_fechaA}', 
			'{$_tipo_form}', 
			'{$_dosis}', 
			'{$_fechaP}', 
			'{$_maria}',
			'{$_numreactivo}', 
			'{$_fechaD}', 
			'{$_resultado}', 
			'{$_cantidad}',
			'{$_volumen}',  
			'{$_obs}',
			'{$_UID}';");
    }

    function ImpurezasEncabezadoGet($_ensayo)
    {
        return $this->_QUERY("EXECUTE PA_MAG_146 '{$_ensayo}';");
    }

    function ImpurezasEncabezadoSet($_xanalizar, $_tipo, $_ingrediente, $_fechaA, $_impureza, $_rango, $_unidad, $_densidad, $_metodo, $_disolvente1, $_validado, $_disolvente2, $_croma, $_disolvente3, $_estandar, $_tiempo1, $_origen1, $_tiempo2, $_certificado1, $_tiempo3, $_pureza1, $_certificado2, $_interno2, $_inyeccion, $_origen2, $_loop, $_certificado3, $_pureza2, $_equipo, $_tipo_c, $_codigo_c, $_marca_c, $_temp_c, $_dim_c, $_tipo_i, $_temp_i, $_modo_i, $_condicion_i, $_gas1, $_flujo1, $_presion1, $_bomba_d, $_elu_d, $_canal_crom1, $_canal_crom2, $_canal_crom3, $_canal_crom4, $_flujo_crom1, $_flujo_crom2, $_flujo_crom3, $_flujo_crom4, $_presion_crom1, $_presion_crom2, $_presion_crom3, $_presion_crom4, $_tipo_d, $_temp_d, $_long_d, $_flujofid1, $_flujofid2, $_flujofid3, $_modo, $_temp1_g, $_temp2_g, $_temp3_g, $_masa1, $_aforado11, $_alicuota11, $_aforado12, $_alicuota12, $_aforado13, $_masa2, $_aforado21, $_alicuota21, $_aforado22, $_alicuota22, $_aforado23, $_alicuota31, $_aforado31, $_alicuota32, $_aforado32, $_concentracion3, $_areaimp3, $_areaint3, $_alicuota41, $_aforado41, $_alicuota42, $_aforado42, $_concentracion4, $_areaimp4, $_areaint4, $_alicuota51, $_aforado51, $_alicuota52, $_aforado52, $_concentracion5, $_areaimp5, $_areaint5, $_alicuota61, $_aforado61, $_alicuota62, $_aforado62, $_concentracion6, $_areaimp6, $_areaint6, $_muestramasa1, $_muestraaforado11, $_muestraalicuota11, $_muestraaforado12, $_muestraalicuota12, $_muestraaforado13, $_muestramasa2, $_muestraaforado21, $_muestraalicuota21, $_muestraaforado22, $_muestraalicuota22, $_muestraaforado23, $_areaA1, $_areaB1, $_areaA2, $_areaB2, $_muestramasa3, $_muestraaforado31, $_muestraalicuota31, $_muestraaforado32, $_muestraalicuota32, $_muestraaforado33, $_muestramasa4, $_muestraaforado41, $_muestraalicuota41, $_muestraaforado42, $_muestraalicuota42, $_muestraaforado43, $_areaA3, $_areaB3, $_areaA4, $_areaB4, $_CCRmuestramasa, $_CCRmuestradil1, $_CCRmuestraali1, $_CCRmuestradil2, $_CCRmuestraali2, $_CCRmuestradil3, $_CCRinternomasa, $_CCRinternodil1, $_CCRinternoali1, $_CCRinternodil2, $_CCRinternoali2, $_CCRinternodil3, $_CCRenrimasa, $_CCRenridil1, $_CCRenriali1, $_CCRenridil2, $_CCRenriali2, $_CCRenridil3, $_CCRAreaA1, $_CCRAreaB1, $_CCRAreaA2, $_CCRAreaB2, $_final_P, $_final_I, $_obs, $_UID)
    {

        $_ingrediente = $this->Free($_ingrediente);
        $_metodo = $this->Free($_metodo);
        $_disolvente1 = $this->Free($_disolvente1);
        $_disolvente2 = $this->Free($_disolvente2);
        $_disolvente3 = $this->Free($_disolvente3);
        $_estandar = $this->Free($_estandar);
        $_origen1 = $this->Free($_origen1);
        $_interno2 = $this->Free($_interno2);
        $_origen2 = $this->Free($_origen2);
        $_codigo_c = $this->Free($_codigo_c);
        $_marca_c = $this->Free($_marca_c);
        $_dim_c = $this->Free($_dim_c);
        $_elu_d = $this->Free($_elu_d);
        $_obs = $this->Free($_obs);

        $this->InvalidaAnteriores($_xanalizar, $_tipo);

        $cs = $this->ConsecutivoEnsayoGet();

        return $this->_TRANS("EXECUTE PA_MAG_145 '{$cs}',
			'{$_xanalizar}', 
			'{$_tipo}', 
			'{$_ingrediente}', 
			'{$_fechaA}', 
			'{$_impureza}',
			'{$_rango}', 
			'{$_unidad}',  
			'{$_densidad}',
			'{$_metodo}',
			'{$_disolvente1}',
			'{$_validado}',
			'{$_disolvente2}',
			'{$_croma}',
			'{$_disolvente3}',
			'{$_estandar}',
			'{$_tiempo1}',
			'{$_origen1}',
			'{$_tiempo2}',
			'{$_certificado1}',
			'{$_tiempo3}',
			'{$_pureza1}',
			'{$_certificado2}',
			'{$_interno2}',
			'{$_inyeccion}', 
			'{$_origen2}', 
			'{$_loop}', 
			'{$_certificado3}', 
			'{$_pureza2}', 
			'{$_equipo}',
			'{$_tipo_c}',
			'{$_codigo_c}', 
			'{$_marca_c}', 
			'{$_temp_c}', 
			'{$_dim_c}', 
			'{$_tipo_i}', 
			'{$_temp_i}', 
			'{$_modo_i}',
			'{$_condicion_i}',
			'{$_gas1}', 
			'{$_flujo1}', 
			'{$_presion1}', 
			'{$_bomba_d}',
			'{$_elu_d}',
			'{$_canal_crom1}', 
			'{$_canal_crom2}',
			'{$_canal_crom3}',
			'{$_canal_crom4}',
			'{$_flujo_crom1}',
			'{$_flujo_crom2}', 
			'{$_flujo_crom3}',
			'{$_flujo_crom4}',
			'{$_presion_crom1}', 
			'{$_presion_crom2}',
			'{$_presion_crom3}', 
			'{$_presion_crom4}',
			'{$_tipo_d}',
			'{$_temp_d}',
			'{$_long_d}',
			'{$_flujofid1}', 
			'{$_flujofid2}',
			'{$_flujofid3}',
			'{$_modo}',
			'{$_temp1_g}', 
			'{$_temp2_g}',
			'{$_temp3_g}',
			'{$_masa1}',
			'{$_aforado11}', 
			'{$_alicuota11}',
			'{$_aforado12}',
			'{$_alicuota12}',
			'{$_aforado13}',
			'{$_masa2}',
			'{$_aforado21}', 
			'{$_alicuota21}',
			'{$_aforado22}',
			'{$_alicuota22}',
			'{$_aforado23}',
			'{$_alicuota31}',
			'{$_aforado31}',
			'{$_alicuota32}',
			'{$_aforado32}',
			'{$_concentracion3}', 
			'{$_areaimp3}',
			'{$_areaint3}',
			'{$_alicuota41}', 
			'{$_aforado41}',
			'{$_alicuota42}',
			'{$_aforado42}',
			'{$_concentracion4}', 
			'{$_areaimp4}',
			'{$_areaint4}',
			'{$_alicuota51}',
			'{$_aforado51}',
			'{$_alicuota52}',
			'{$_aforado52}',
			'{$_concentracion5}', 
			'{$_areaimp5}',
			'{$_areaint5}',
			'{$_alicuota61}', 
			'{$_aforado61}',
			'{$_alicuota62}',
			'{$_aforado62}',
			'{$_concentracion6}', 
			'{$_areaimp6}',
			'{$_areaint6}',
			'{$_muestramasa1}', 
			'{$_muestraaforado11}', 
			'{$_muestraalicuota11}',
			'{$_muestraaforado12}',
			'{$_muestraalicuota12}',
			'{$_muestraaforado13}',
			'{$_muestramasa2}',
			'{$_muestraaforado21}', 
			'{$_muestraalicuota21}',
			'{$_muestraaforado22}',
			'{$_muestraalicuota22}',
			'{$_muestraaforado23}',
			'{$_areaA1}',
			'{$_areaB1}',
			'{$_areaA2}',
			'{$_areaB2}',
			'{$_muestramasa3}', 
			'{$_muestraaforado31}', 
			'{$_muestraalicuota31}',
			'{$_muestraaforado32}',
			'{$_muestraalicuota32}',
			'{$_muestraaforado33}',
			'{$_muestramasa4}',
			'{$_muestraaforado41}', 
			'{$_muestraalicuota41}',
			'{$_muestraaforado42}',
			'{$_muestraalicuota42}',
			'{$_muestraaforado43}',
			'{$_areaA3}',
			'{$_areaB3}',
			'{$_areaA4}',
			'{$_areaB4}',
			'{$_CCRmuestramasa}', 
			'{$_CCRmuestradil1}',
			'{$_CCRmuestraali1}',
			'{$_CCRmuestradil2}',
			'{$_CCRmuestraali2}',
			'{$_CCRmuestradil3}',
			'{$_CCRinternomasa}',
			'{$_CCRinternodil1}',
			'{$_CCRinternoali1}',
			'{$_CCRinternodil2}',
			'{$_CCRinternoali2}',
			'{$_CCRinternodil3}',
			'{$_CCRenrimasa}',
			'{$_CCRenridil1}',
			'{$_CCRenriali1}',
			'{$_CCRenridil2}',
			'{$_CCRenriali2}',
			'{$_CCRenridil3}',
			'{$_CCRAreaA1}',
			'{$_CCRAreaB1}',
			'{$_CCRAreaA2}',
			'{$_CCRAreaB2}',
			'{$_final_P}',
			'{$_final_I}',
			'{$_obs}',
			'{$_UID}';");
    }

    function ImpurezasEncabezadoVacio($_xanalizar)
    {
        $ROW = $this->_QUERY("SELECT MUE000.ref, '' AS ingrediente, '' AS tipo_form, '' AS rango, '' AS unidad, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, MUE000.solicitud, MUE001.analisis
		FROM MUE001 INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra
		WHERE (MUEBIT.accion = '0' OR MUEBIT.accion = 'A') AND (MUE001.id = '{$_xanalizar}');");
        //
        $ROW2 = $this->_QUERY("SELECT tipo_form FROM SER003 WHERE (cs = '{$ROW[0]['solicitud']}');");
        if ($ROW2)
            $ROW[0]['tipo_form'] = $ROW2[0]['tipo_form'];
        //
        $ROW2 = $this->_QUERY("SELECT analisis, rango, unidad FROM SER004 WHERE (solicitud = '{$ROW[0]['solicitud']}') AND rango != '';");
        if ($ROW2) {
            $ROW[0]['rango'] = $ROW2[0]['rango'];
            $ROW[0]['unidad'] = $ROW2[0]['unidad'];
        }
        //
        $ROW2 = $this->_QUERY("SELECT nombre FROM VAR005 WHERE (id = '{$ROW2[0]['analisis']}');");
        if ($ROW2)
            $ROW[0]['ingrediente'] = $ROW2[0]['nombre'];
        //
        /* $ROW2 = $this->_QUERY("SELECT SER004.rango, SER004.unidad, VAR007.nombre AS tipo_form, VAR005.nombre AS ingrediente
          FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR007 ON SER003.tipo_form = VAR007.id INNER JOIN VAR005 ON SER004.analisis = VAR005.id WHERE (SER003.cs = '{$ROW[0]['solicitud']}');"); */
        $ROW[0]['impureza'] = '';
        $ROW2 = $this->_QUERY("SELECT VAR005.id AS analisis, VAR005.nombre FROM MUE000 INNER JOIN MUE001 ON MUE000.id = MUE001.muestra INNER JOIN VAR005 ON MUE001.analisis = VAR005.id WHERE (MUE001.id = '{$_xanalizar}');;");
        if ($ROW2)
            $ROW[0]['impureza'] = $ROW2[0]['analisis'];

        //VACIOS
        $ROW[0]['fechaA'] = '';
        $ROW[0]['fechaC'] = '';
        $ROW[0]['densidad'] = '1';
        $ROW[0]['metodo'] = '';
        $ROW[0]['disolvente1'] = '';
        $ROW[0]['validado'] = '';
        $ROW[0]['disolvente2'] = '';
        $ROW[0]['croma'] = '';
        $ROW[0]['disolvente3'] = '';
        $ROW[0]['estandar'] = '';
        $ROW[0]['tiempo1'] = '0';
        $ROW[0]['origen1'] = '';
        $ROW[0]['tiempo2'] = '0';
        $ROW[0]['certificado1'] = '';
        $ROW[0]['tiempo3'] = '0';
        $ROW[0]['pureza1'] = '0';
        $ROW[0]['certificado2'] = '';
        $ROW[0]['interno2'] = '';
        $ROW[0]['inyeccion'] = '0';
        $ROW[0]['origen2'] = '';
        $ROW[0]['loop'] = '0';
        $ROW[0]['certificado3'] = '';
        $ROW[0]['pureza2'] = '0';
        $ROW[0]['tmp_equipo1'] = '';
        $ROW[0]['equipo'] = '';
        $ROW[0]['marca'] = '';
        $ROW[0]['modelo'] = '';
        $ROW[0]['tipo_c'] = '';
        $ROW[0]['codigo_c'] = '';
        $ROW[0]['marca_c'] = '';
        $ROW[0]['temp_c'] = '0';
        $ROW[0]['dim_c'] = '';
        $ROW[0]['tipo_i'] = '';
        $ROW[0]['temp_i'] = '0';
        $ROW[0]['modo_i'] = '';
        $ROW[0]['condicion_i'] = '';
        $ROW[0]['gas1'] = '';
        $ROW[0]['flujo1'] = '0';
        $ROW[0]['presion1'] = '0';
        $ROW[0]['bomba_d'] = '';
        $ROW[0]['elu_d'] = '';
        $ROW[0]['canal_crom1'] = '';
        $ROW[0]['flujo_crom1'] = '';
        $ROW[0]['presion_crom1'] = '';
        $ROW[0]['canal_crom2'] = '';
        $ROW[0]['flujo_crom2'] = '';
        $ROW[0]['presion_crom2'] = '';
        $ROW[0]['canal_crom3'] = '';
        $ROW[0]['flujo_crom3'] = '';
        $ROW[0]['presion_crom3'] = '';
        $ROW[0]['canal_crom4'] = '';
        $ROW[0]['flujo_crom4'] = '';
        $ROW[0]['presion_crom4'] = '';
        $ROW[0]['tipo_d'] = '0';
        $ROW[0]['temp_d'] = '';
        $ROW[0]['long_d'] = '';
        $ROW[0]['flujofid1'] = '';
        $ROW[0]['flujofid2'] = '';
        $ROW[0]['flujofid3'] = '';
        $ROW[0]['modo'] = '';
        $ROW[0]['temp1_g'] = '';
        $ROW[0]['temp2_g'] = '';
        $ROW[0]['temp3_g'] = '';
        $ROW[0]['masa1'] = '';
        $ROW[0]['aforado11'] = '';
        $ROW[0]['alicuota11'] = '';
        $ROW[0]['aforado12'] = '';
        $ROW[0]['alicuota12'] = '';
        $ROW[0]['aforado13'] = '';
        $ROW[0]['masa2'] = '';
        $ROW[0]['aforado21'] = '';
        $ROW[0]['alicuota21'] = '';
        $ROW[0]['aforado22'] = '';
        $ROW[0]['alicuota22'] = '';
        $ROW[0]['aforado23'] = '';
        $ROW[0]['alicuota31'] = '';
        $ROW[0]['aforado31'] = '';
        $ROW[0]['alicuota32'] = '';
        $ROW[0]['aforado32'] = '';
        $ROW[0]['concentracion3'] = '';
        $ROW[0]['areaimp3'] = '';
        $ROW[0]['areaint3'] = '';
        $ROW[0]['alicuota41'] = '';
        $ROW[0]['aforado41'] = '';
        $ROW[0]['alicuota42'] = '';
        $ROW[0]['aforado42'] = '';
        $ROW[0]['concentracion4'] = '';
        $ROW[0]['areaimp4'] = '';
        $ROW[0]['areaint4'] = '';
        $ROW[0]['alicuota51'] = '';
        $ROW[0]['aforado51'] = '';
        $ROW[0]['alicuota52'] = '';
        $ROW[0]['aforado52'] = '';
        $ROW[0]['concentracion5'] = '';
        $ROW[0]['areaimp5'] = '';
        $ROW[0]['areaint5'] = '';
        $ROW[0]['alicuota61'] = '';
        $ROW[0]['aforado61'] = '';
        $ROW[0]['alicuota62'] = '';
        $ROW[0]['aforado62'] = '';
        $ROW[0]['concentracion6'] = '';
        $ROW[0]['areaimp6'] = '';
        $ROW[0]['areaint6'] = '';
        $ROW[0]['muestramasa1'] = '';
        $ROW[0]['muestraaforado11'] = '';
        $ROW[0]['muestraalicuota11'] = '';
        $ROW[0]['muestraaforado12'] = '';
        $ROW[0]['muestraalicuota12'] = '';
        $ROW[0]['muestraaforado13'] = '';
        $ROW[0]['muestramasa2'] = '';
        $ROW[0]['muestraaforado21'] = '';
        $ROW[0]['muestraalicuota21'] = '';
        $ROW[0]['muestraaforado22'] = '';
        $ROW[0]['muestraalicuota22'] = '';
        $ROW[0]['muestraaforado23'] = '';
        $ROW[0]['areaA1'] = '';
        $ROW[0]['areaA2'] = '';
        $ROW[0]['areaB1'] = '';
        $ROW[0]['areaB2'] = '';
        $ROW[0]['muestramasa3'] = '';
        $ROW[0]['muestraaforado31'] = '';
        $ROW[0]['muestraalicuota31'] = '';
        $ROW[0]['muestraaforado32'] = '';
        $ROW[0]['muestraalicuota32'] = '';
        $ROW[0]['muestraaforado33'] = '';
        $ROW[0]['muestramasa4'] = '';
        $ROW[0]['muestraaforado41'] = '';
        $ROW[0]['muestraalicuota41'] = '';
        $ROW[0]['muestraaforado42'] = '';
        $ROW[0]['muestraalicuota42'] = '';
        $ROW[0]['muestraaforado43'] = '';
        $ROW[0]['areaA3'] = '';
        $ROW[0]['areaA4'] = '';
        $ROW[0]['areaB3'] = '';
        $ROW[0]['areaB4'] = '';
        $ROW[0]['CCRmuestramasa'] = '';
        $ROW[0]['CCRmuestradil1'] = '';
        $ROW[0]['CCRmuestraali1'] = '';
        $ROW[0]['CCRmuestradil2'] = '';
        $ROW[0]['CCRmuestraali2'] = '';
        $ROW[0]['CCRmuestradil3'] = '';
        $ROW[0]['CCRinternomasa'] = '';
        $ROW[0]['CCRinternodil1'] = '';
        $ROW[0]['CCRinternoali1'] = '';
        $ROW[0]['CCRinternodil2'] = '';
        $ROW[0]['CCRinternoali2'] = '';
        $ROW[0]['CCRinternodil3'] = '';
        $ROW[0]['CCRenrimasa'] = '';
        $ROW[0]['CCRenridil1'] = '';
        $ROW[0]['CCRenriali1'] = '';
        $ROW[0]['CCRenridil2'] = '';
        $ROW[0]['CCRenriali2'] = '';
        $ROW[0]['CCRenridil3'] = '';
        $ROW[0]['CCRAreaA1'] = '';
        $ROW[0]['CCRAreaB1'] = '';
        $ROW[0]['CCRAreaA2'] = '';
        $ROW[0]['CCRAreaB2'] = '';

        return $ROW;
    }

    private function InvalidaAnteriores($_xanalizar, $_tipo)
    {
        //INVALIDA CUALQUIER METODO DEL MISMO TIPO REALIZADO ANTERIORMENTE
        $TK = $this->_QUERY("SELECT ensayos_ FROM MAG000;");
        $lol = $TK[0]['ensayos_'] . '-' . date('Y');
        $TK2 = $this->_QUERY("SELECT id FROM MUE002 WHERE id = '{$lol}';");
        if ($TK2) {
            $this->_TRANS("UPDATE MAG000 SET ensayos_ = ensayos_ + 1;");
        }
        $this->_TRANS("UPDATE MUE002 SET cumple = 0 WHERE (xanalizar='{$_xanalizar}') AND (tipo={$_tipo});");
    }

    function KjeldahlEncabezadoGet($_ensayo)
    {
        return $this->_QUERY("SELECT MUE000.solicitud, MUE000.ref, MUE002.tipo, CONVERT(VARCHAR, MUE002.fecha, 105) AS fechaC, MUE002.declarada AS rango, MUE002.unidad, CONVERT(VARCHAR, MET012.fechaA, 105) AS fechaA, MET012.na, MET012.molar, MET012.valorado, MET012.densidad, MET012.alicuota, MET012.balon, MET012.inc2, MET012.inc3, 
        MET012.inc5, MET012.repetibilidad2, MET012.resolucion2, MET012.inc_cer, MET012.emp, MET012.masaA1, MET012.masaA2, MET012.masaB1, MET012.masaB2, MET012.des1, MET012.val6, MET012.des6, CONVERT(TEXT, MET012.obs) AS obs, VAR005.nombre AS elemento, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI,
		MET_INC.certificado_densidad AS icCert,  MET_INC.resolucion_densidad AS icRes, met_inc.reptibilidad_masa As rep, met_inc.resolucion_masa As resM, met_inc.linealidad_masa As lilM,  met_inc.excentricidad_masa As exM		
		FROM MUE000 INNER JOIN MUE001 ON MUE000.id = MUE001.muestra INNER JOIN MUE002 ON MUE001.id = MUE002.xanalizar INNER JOIN MET012 ON MUE002.id = MET012.ensayo INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN VAR005 ON MUE001.analisis = VAR005.id left outer join MET_INC on MET012.ensayo = MET_INC.ensayo
		WHERE (MET012.ensayo = '{$_ensayo}') AND (MUEBIT.accion = '0');");
    }

    function KjeldahlEncabezadoSet($_xanalizar, $_tipo, $_fechaA, $_rango, $_unidad, $_na, $_molar, $_valorado, $_densidad, $_alicuota, $_balon, $_inc2, $_inc3, $_inc5, $_repetibilidad2, $_resolucion2, $_inc_cer, $_emp, $_masaA1, $_masaA2, $_masaB1, $_masaB2, $_des1, $_val6, $_des6, $_CD, $_IC, $_obs, $_UID)
    {
        $_rango = $this->Free($_rango);
        $_obs = $this->Free($_obs);
        $_fechaA = $this->_FECHA2($_fechaA);
        $this->InvalidaAnteriores($_xanalizar, $_tipo);

        $cs = $this->ConsecutivoEnsayoGet();

        return $this->_TRANS("EXECUTE PA_MAG_109 '{$cs}',
			'{$_xanalizar}', 
			'{$_tipo}', 
			'{$_fechaA}', 
			'{$_rango}', 
			'{$_unidad}', 
			'{$_na}',  
			'{$_molar}',  
			'{$_valorado}',  
			'{$_densidad}',  
			'{$_alicuota}',  
			'{$_balon}',  
			'{$_inc2}',  
			'{$_inc3}',  
			'{$_inc5}',  
			'{$_repetibilidad2}',  
			'{$_resolucion2}',  
			'{$_inc_cer}',  
			'{$_emp}',  
			'{$_masaA1}',  
			'{$_masaA2}',  
			'{$_masaB1}',  
			'{$_masaB2}',  
			'{$_des1}',  
			'{$_val6}',  
			'{$_des6}',  
			'{$_CD}',  
			'{$_IC}', 
			'{$_obs}',
			'{$_UID}';");
    }

    function KjeldahlEncabezadoVacio($_xanalizar)
    {
        $ROW = $this->_QUERY("SELECT '' AS rango, '' AS unidad, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, MUE000.solicitud, MUE000.ref, MUE001.analisis, VAR005.nombre AS elemento
		FROM MUE001 INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN VAR005 ON MUE001.analisis = VAR005.id
		WHERE (MUEBIT.accion = '0' OR MUEBIT.accion = 'A') AND (MUE001.id = '{$_xanalizar}');");
        //


        $ROW2 = $this->_QUERY("SELECT SER004.rango, SER004.unidad
		FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud
		WHERE (SER003.cs = '{$ROW[0]['solicitud']}') AND (SER004.analisis = {$ROW[0]['analisis']});");
        if ($ROW2) {
            $ROW[0]['rango'] = $ROW2[0]['rango'];
            $ROW[0]['unidad'] = $ROW2[0]['unidad'];
        }

        list(
            $ROW[0]['fechaA'],
            $ROW[0]['fechaC'],
            $ROW[0]['na'],
            $ROW[0]['molar'],
            $ROW[0]['valorado'],
            $ROW[0]['densidad'],
            $ROW[0]['alicuota'],
            $ROW[0]['balon'],
            $ROW[0]['inc2'],
            $ROW[0]['inc3'],
            $ROW[0]['inc5'],
            $ROW[0]['repetibilidad2'],
            $ROW[0]['resolucion2'],
            $ROW[0]['inc_cer'],
            $ROW[0]['emp'],
            $ROW[0]['masaA1'],
            $ROW[0]['masaB1'],
            $ROW[0]['masaA2'],
            $ROW[0]['masaB2'],
            $ROW[0]['val6'],
            $ROW[0]['des6'],
            $ROW[0]['des1'],
            $ROW[0]['obs']
            ) = array('', '', '0', '204.2180', '0', '0', '', '', '0.0001', '0.05', '0', '0', '0', '0', '0', '0', '0', '0', '0', '14.007', '0.001', '0.025', '');

        return $ROW;
    }

    function MacroEncabezadoGet($_ensayo)
    {

        $ROW = $this->_QUERY("SELECT MUE000.solicitud, MUE000.ref, MUE002.tipo, CONVERT(VARCHAR, MUE002.fecha, 105) AS fechaC, MUE002.declarada AS rango, MUE002.unidad, CONVERT(VARCHAR, MET010.fechaA, 105) AS fechaA, MET010.balon1, MET010.balon2, 
        MET010.pipetaD1, MET010.pipetaD2, MET010.pipetaD3, MET010.balonD1, MET010.balonD2, MET010.balonD3, MET010.repetibilidad, 
        MET010.resolucion, MET010.inc_cer, MET010.emp, MET010.masa1, MET010.masa2, MET010.cn1, MET010.cn2, MET010.dumbre1, 
 		MET010.dumbre2, MET010.delta1, MET010.delta2, MET010.certi1, MET010.certi2, CONVERT(TEXT, MET010.obs) AS obs, VAR005.nombre AS elemento, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI,
		 met_inc.reptibilidad_masa As repetibilidad_MET_INC, met_inc.resolucion_masa As resolucion_MET_INC,
	   met_inc.linealidad_masa As inc_cer_MET_INC,  met_inc.excentricidad_masa As emp_MET_INC
		FROM MUE000 INNER JOIN MUE001 ON MUE000.id = MUE001.muestra INNER JOIN MUE002 ON MUE001.id = MUE002.xanalizar INNER JOIN MET010 ON MUE002.id = MET010.ensayo INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN VAR005 ON MUE001.analisis = VAR005.id left outer join MET_INC on MET010.ensayo = MET_INC.ensayo
		WHERE (MET010.ensayo = '{$_ensayo}') AND (MUEBIT.accion = '0');");

        //OBTIENE EL SIMBOLO DEL ELEMENTO
        $elemento = strtolower($ROW[0]['elemento']);
        $pelao= substr($elemento, 0,1);
        if($pelao == 'f') {
            $elemento = "fosforo";
        }


        if (strpos($elemento, 'boro') === false) {
            if (strpos($elemento, 'calcio') === false) {
                if (strpos($elemento, 'cobre') === false) {
                    if (strpos($elemento, 'fosforo') === false) {
                       if (strpos($elemento, "f" . "\xf3" . "sforo" . " (p<sub>2</sub>o<sub>5</sub>)") === false) {
                            if (strpos($elemento, 'hierro') === false) {
                                if (strpos($elemento, 'magnesio') === false) {
                                    if (strpos($elemento, 'manganeso') === false) {
                                        if (strpos($elemento, 'potasio') === false) {
                                            if (strpos($elemento, 'zinc') === false) {

                                            } else
                                                $ROW[0]['base'] = 'zinc';
                                        } else
                                            $ROW[0]['base'] = 'potasio';
                                    } else
                                        $ROW[0]['base'] = 'manganeso';
                                } else
                                    $ROW[0]['base'] = 'magnesio';
                            } else
                                $ROW[0]['base'] = 'hierro';
                        } else
                            $ROW[0]['base'] = 'fosforo';
                    } else
                        $ROW[0]['base'] = 'fósforo';
                } else
                    $ROW[0]['base'] = 'cobre';
            } else
                $ROW[0]['base'] = 'calcio';
        } else
            $ROW[0]['base'] = 'boro';

        return $ROW;
    }

    function MacroEncabezadoSet($_xanalizar, $_tipo, $_fechaA, $_rango, $_unidad, $_balon1, $_balon2, $_pipetaD1, $_pipetaD2, $_pipetaD3, $_balonD1, $_balonD2, $_balonD3, $_repetibilidad, $_resolucion, $_inc_cer, $_emp, $_masa1, $_masa2, $_cn1, $_cn2, $_dumbre1, $_dumbre2, $_delta1, $_delta2, $_certi1, $_certi2, $_prom_masa, $_prom_volu, $_final, $_obs, $_UID)
    {
        $_rango = $this->Free($_rango);
        $_obs = $this->Free($_obs);
        $_fechaA = $this->_FECHA2($_fechaA);
        $this->InvalidaAnteriores($_xanalizar, $_tipo);

        $cs = $this->ConsecutivoEnsayoGet();

        return $this->_TRANS("EXECUTE PA_MAG_103 '{$cs}',
			'{$_xanalizar}', 
			'{$_tipo}', 
			'{$_fechaA}', 
			'{$_rango}', 
			'{$_unidad}', 
			'{$_balon1}', 
			'{$_balon2}', 	
			'{$_pipetaD1}', 
			'{$_pipetaD2}', 
			'{$_pipetaD3}', 
			'{$_balonD1}', 
			'{$_balonD2}', 
			'{$_balonD3}', 
			'{$_repetibilidad}', 
			'{$_resolucion}', 
			'{$_inc_cer}', 
			'{$_emp}', 
			'{$_masa1}', 
			'{$_masa2}', 
			'{$_cn1}', 
			'{$_cn2}', 
			'{$_dumbre1}', 
			'{$_dumbre2}', 
			'{$_delta1}', 
			'{$_delta2}', 
			'{$_certi1}', 
			'{$_certi2}', 
			'{$_prom_masa}', 
			'{$_prom_volu}', 
			'{$_final}', 
			'{$_obs}', 
			'{$_UID}';");
    }

    function MacroEncabezadoVacio($_xanalizar)
    {

        $ROW = $this->_QUERY("SELECT '' AS obs, '' AS rango, '' AS unidad, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, MUE000.solicitud, MUE000.ref, MUE001.analisis, VAR005.nombre AS elemento
		FROM MUE001 INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN VAR005 ON MUE001.analisis = VAR005.id
		WHERE (MUEBIT.accion = '0' OR MUEBIT.accion = 'A') AND (MUE001.id = '{$_xanalizar}');");

        //
        $ROW2 = $this->_QUERY("SELECT SER004.rango, SER004.unidad
		FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud
		WHERE (SER003.cs = '{$ROW[0]['solicitud']}') AND (SER004.analisis = {$ROW[0]['analisis']});");


        if ($ROW2) {
            $ROW[0]['rango'] = $ROW2[0]['rango'];
            $ROW[0]['unidad'] = $ROW2[0]['unidad'];
        }
        //OBTIENE EL SIMBOLO DEL ELEMENTO



        $elemento = strtolower($ROW[0]['elemento']);

        $pelao= substr($elemento, 0,1);
      if($pelao == 'f') {
          $elemento = "fosforo";
      }

        if (strpos($elemento, 'boro') === false) {
            if (strpos($elemento, 'calcio') === false) {
                if (strpos($elemento, 'cobre') === false) {
                    if (strpos($elemento, 'fosforo') === false) {
                       // if (strpos($elemento, "f" . "\xf3" . "sforo" . " (p<sub>2</sub>o<sub>5</sub>)") === false) {
                            if (strpos($elemento, 'hierro') === false) {
                                if (strpos($elemento, 'magnesio') === false) {
                                    if (strpos($elemento, 'manganeso') === false) {
                                        if (strpos($elemento, 'potasio') === false) {
                                            if (strpos($elemento, 'zinc') === false) {
                                                $ROW[0]['base'] = '';
                                            } else
                                                $ROW[0]['base'] = 'zinc';
                                        } else
                                            $ROW[0]['base'] = 'potasio';
                                    } else
                                        $ROW[0]['base'] = 'manganeso';
                                } else
                                    $ROW[0]['base'] = 'magnesio';
                            } else
                                $ROW[0]['base'] = 'hierro';
                       /* } else
                            $ROW[0]['base'] = 'fósforo';*/
                    } else
                        $ROW[0]['base'] = 'fosforo';
                } else
                    $ROW[0]['base'] = 'cobre';
            } else
                $ROW[0]['base'] = 'calcio';
        } else
            $ROW[0]['base'] = 'boro';

        //VACIOS
        list(
            $ROW[0]['fechaA'],
            $ROW[0]['fechaC'],
            $ROW[0]['balon1'],
            $ROW[0]['balon2'],
            $ROW[0]['pipetaD1'],
            $ROW[0]['balonD1'],
            $ROW[0]['pipetaD2'],
            $ROW[0]['balonD2'],
            $ROW[0]['pipetaD3'],
            $ROW[0]['balonD3'],
            $ROW[0]['repetibilidad'],
            $ROW[0]['resolucion'],
            $ROW[0]['inc_cer'],
            $ROW[0]['emp'],
            $ROW[0]['cn1'],
            $ROW[0]['cn2'],
            $ROW[0]['dumbre1'],
            $ROW[0]['dumbre2'],
            $ROW[0]['delta1'],
            $ROW[0]['delta2'],
            $ROW[0]['certi1'],
            $ROW[0]['certi2'],
            $ROW[0]['masa1'],
            $ROW[0]['masa2'],
            ) = array('', '', '', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

        return $ROW;
    }

    function Metodo0EncabezadoVacio()
    {
        return array(0 => array(
            'cs' => '',
            'fecha' => '',
            'tipo_bal' => '',
            'nom_bal' => '',
            'cod_bal' => '',
            'rango' => '',
            'pesa1' => '',
            'nominal1' => '',
            'cert1' => '',
            'pesa2' => '',
            'nominal2' => '',
            'cert2' => '')
        );
    }

    function ParaquatEncabezadoGet($_ensayo)
    {
        return $this->_QUERY("SELECT MUE000.ref, MUE002.tipo, CONVERT(VARCHAR, MUE002.fecha, 105) AS fechaC, MUE002.ingrediente, MUE002.declarada AS rango, MUE002.unidad, CONVERT(VARCHAR, MET014.fechaA, 105) AS fechaA, MET014.tipo_form, MET014.rango2, MET014.unidad2, MET014.origen, CONVERT(VARCHAR, MET014.fechaP, 105) AS fechaP, MET014.pureza, MET014.factor, MET014.existe, MET014.densidad, MET014.linealidad1, MET014.linealidad2, MET014.repeti1, MET014.repeti2, MET014.masa, MET014.bal1, MET014.vol2, MET014.bal2, MET014.vol3, MET014.bal3, MET014.abs3, MET014.vol4, MET014.bal4, MET014.abs4, MET014.vol5, MET014.bal5, MET014.abs5, MET014.vol6, MET014.bal6, MET014.abs6, MET014.mm1, MET014.bb1, MET014.aa2, MET014.bb2, MET014.aa3, MET014.bb3, MET014.cur3, 
		MET014.sor3, MET014.cur4, MET014.sor4, MET014.cur5, MET014.sor5, MET014.mm6, MET014.bb6, MET014.aa7, MET014.bb7, MET014.aa8, MET014.bb8, MET014.cur8, MET014.sor8, MET014.cur9, MET014.sor9, MET014.curA, MET014.sorA, MET014.IC, MET014.EX, MET014.obs, MET014.obs, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, (SEG001.nombre +' '+ SEG001.ap1) AS analista
		FROM MUE002 INNER JOIN MET014 ON MUE002.id = MET014.ensayo INNER JOIN MUE001 ON MUE002.xanalizar = MUE001.id INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN SEG001 ON MUE002.analista = SEG001.id
		WHERE (MUE002.id = '{$_ensayo}') AND (MUEBIT.accion = '0');");
    }

    function ParaquatEncabezadoSet($_xanalizar, $_tipo, $_ingrediente, $_fechaA, $_rango, $_unidad, $_tipo_form, $_rango2, $_unidad2, $_origen, $_fechaP, $_pureza, $_factor, $_existe, $_densidad, $_linealidad1, $_linealidad2, $_repeti1, $_repeti2, $_masa, $_bal1, $_vol2, $_bal2, $_vol3, $_bal3, $_abs3, $_vol4, $_bal4, $_abs4, $_vol5, $_bal5, $_abs5, $_vol6, $_bal6, $_abs6, $_mm1, $_bb1, $_aa2, $_bb2, $_aa3, $_bb3, $_cur3, $_sor3, $_cur4, $_sor4, $_cur5, $_sor5, $_mm6, $_bb6, $_aa7, $_bb7, $_aa8, $_bb8, $_cur8, $_sor8, $_cur9, $_sor9, $_curA, $_sorA, $_IC, $_EX, $_obs, $_contenido1, $_contenido2, $_UID, $_UNAME)
    {
        $_ingrediente = $this->Free($_ingrediente);
        $_rango = $this->Free($_rango);
        $_rango2 = $this->Free($_rango2);
        $_origen = $this->Free($_origen);
        $_obs = $this->Free($_obs);
        $_fechaA = $this->_FECHA($_fechaA);
        $_fechaP = $this->_FECHA($_fechaP);

        $this->InvalidaAnteriores($_xanalizar, $_tipo);

        $cs = $this->ConsecutivoEnsayoGet();

        if ($this->_TRANS("EXECUTE PA_MAG_115 '{$cs}',
			'{$_xanalizar}', 
			'{$_tipo}', 
			'{$_ingrediente}', 
			'{$_fechaA}', 
			'{$_rango}', 
			'{$_unidad}', 
			'{$_tipo_form}', 
			'{$_rango2}',
			'{$_unidad2}',
			'{$_origen}',
			'{$_fechaP}',
			'{$_pureza}',
			'{$_factor}',
			'{$_existe}',
			'{$_densidad}',
			'{$_linealidad1}',
			'{$_linealidad2}',
			'{$_repeti1}',
			'{$_repeti2}',
			'{$_masa}',
			'{$_bal1}',
			'{$_vol2}',
			'{$_bal2}',
			'{$_vol3}',
			'{$_bal3}',
			'{$_abs3}',
			'{$_vol4}',
			'{$_bal4}',
			'{$_abs4}',
			'{$_vol5}',
			'{$_bal5}',
			'{$_abs5}',
			'{$_vol6}',
			'{$_bal6}',
			'{$_abs6}',
			'{$_mm1}',
			'{$_bb1}',
			'{$_aa2}',
			'{$_bb2}',
			'{$_aa3}',
			'{$_bb3}',
			'{$_cur3}',
			'{$_sor3}',
			'{$_cur4}',
			'{$_sor4}',
			'{$_cur5}',
			'{$_sor5}',
			'{$_mm6}',
			'{$_bb6}',
			'{$_aa7}',
			'{$_bb7}',
			'{$_aa8}',
			'{$_bb8}',
			'{$_cur8}',
			'{$_sor8}',
			'{$_cur9}',
			'{$_sor9}',
			'{$_curA}',
			'{$_sorA}',
			'{$_IC}',
			'{$_EX}',
			'{$_obs}', 
			'{$_UID}';")) {
            $this->GrafControlIME($_ingrediente, $_rango, $_UNAME, $_xanalizar, $_tipo_form, $_contenido1, $_contenido2);
            return 1;
        } else
            return 0;
    }

    function ParaquatEncabezadoVacio($_xanalizar)
    {
        $ROW = $this->_QUERY("SELECT MUE000.ref, '' AS ingrediente, '' AS tipo_form, '' AS rango, '' AS unidad, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, MUE000.solicitud, MUE001.analisis
		FROM MUE001 INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra
		WHERE (MUEBIT.accion = '0' OR MUEBIT.accion = 'A') AND (MUE001.id = '{$_xanalizar}');");
        //
        $ROW2 = $this->_QUERY("SELECT SER004.rango, SER004.unidad, VAR007.nombre AS tipo_form, VAR005.nombre AS ingrediente
		FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR007 ON SER003.tipo_form = VAR007.id INNER JOIN VAR005 ON SER004.analisis = VAR005.id
		WHERE (SER003.cs = '{$ROW[0]['solicitud']}') AND (SER004.analisis = {$ROW[0]['analisis']});");
        if ($ROW2) {
            $ROW[0]['ingrediente'] = $ROW2[0]['ingrediente'];
            $ROW[0]['tipo_form'] = $ROW2[0]['tipo_form'];
            $ROW[0]['rango'] = $ROW2[0]['rango'];
            $ROW[0]['unidad'] = $ROW2[0]['unidad'];
        }
        $ROW[0]['fechaA'] = '';
        $ROW[0]['fechaC'] = '';
        $ROW[0]['rango2'] = '';
        $ROW[0]['unidad2'] = '';
        $ROW[0]['origen'] = '';
        $ROW[0]['fechaP'] = '';
        $ROW[0]['pureza'] = '';
        $ROW[0]['factor'] = '';
        $ROW[0]['existe'] = '';
        $ROW[0]['densidad'] = '';
        $ROW[0]['linealidad1'] = '0.0001';
        $ROW[0]['linealidad2'] = '0.0001';
        $ROW[0]['repeti1'] = '0.0007';
        $ROW[0]['repeti2'] = '0.0007';
        $ROW[0]['masa'] = '0';
        $ROW[0]['bal1'] = '500';
        $ROW[0]['vol2'] = '50';
        $ROW[0]['bal2'] = '250';
        $ROW[0]['vol3'] = '5';
        $ROW[0]['bal3'] = '100';
        $ROW[0]['abs3'] = '0';
        $ROW[0]['vol4'] = '10';
        $ROW[0]['bal4'] = '100';
        $ROW[0]['abs4'] = '0';
        $ROW[0]['vol5'] = '15';
        $ROW[0]['bal5'] = '100';
        $ROW[0]['abs5'] = '0';
        $ROW[0]['vol6'] = '20';
        $ROW[0]['bal6'] = '100';
        $ROW[0]['abs6'] = '0';
        $ROW[0]['mm1'] = '0';
        $ROW[0]['mm6'] = '0';
        $ROW[0]['aa2'] = '10';
        $ROW[0]['aa3'] = '10';
        $ROW[0]['aa7'] = '10';
        $ROW[0]['aa8'] = '10';
        $ROW[0]['bb1'] = '500';
        $ROW[0]['bb2'] = '100';
        $ROW[0]['bb3'] = '100';
        $ROW[0]['bb6'] = '500';
        $ROW[0]['bb7'] = '100';
        $ROW[0]['bb8'] = '100';
        $ROW[0]['cur3'] = '0';
        $ROW[0]['cur4'] = '0';
        $ROW[0]['cur5'] = '0';
        $ROW[0]['cur8'] = '0';
        $ROW[0]['cur9'] = '0';
        $ROW[0]['curA'] = '0';
        $ROW[0]['sor3'] = '0';
        $ROW[0]['sor4'] = '0';
        $ROW[0]['sor5'] = '0';
        $ROW[0]['sor8'] = '0';
        $ROW[0]['sor9'] = '0';
        $ROW[0]['sorA'] = '0';
        $ROW[0]['obs'] = '';

        return $ROW;
    }

    function PbcdEncabezadoGet($_ensayo)
    {
        return $this->_QUERY("SELECT MUE000.solicitud, MUE000.ref, MUE002.tipo, CONVERT(VARCHAR, MUE002.fecha, 105) AS fechaC, CONVERT(VARCHAR, MET008.fechaA, 105) AS fechaA, MET008.balon1, MET008.balon2, 
        MET008.pipetaD1, MET008.pipetaD2, MET008.pipetaD3, MET008.balonD1, MET008.balonD2, MET008.balonD3, MET008.repetibilidad, 
        MET008.resolucion, MET008.inc_cer, MET008.emp, MET008.masa1, MET008.masa2, MET008.cn1, MET008.cn2, MET008.dumbre1, 
 		MET008.dumbre2, MET008.delta1, MET008.delta2, MET008.certi1, MET008.certi2, CONVERT(TEXT, MET008.obs) AS obs, VAR005.nombre AS elemento, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI
		FROM MUE000 INNER JOIN MUE001 ON MUE000.id = MUE001.muestra INNER JOIN MUE002 ON MUE001.id = MUE002.xanalizar INNER JOIN MET008 ON MUE002.id = MET008.ensayo INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN VAR005 ON MUE001.analisis = VAR005.id
		WHERE (MET008.ensayo = '{$_ensayo}') AND (MUEBIT.accion = '0');");
    }

    function PbcdEncabezadoSet($_xanalizar, $_tipo, $_fechaA, $_balon1, $_balon2, $_pipetaD1, $_pipetaD2, $_pipetaD3, $_balonD1, $_balonD2, $_balonD3, $_repetibilidad, $_resolucion, $_inc_cer, $_emp, $_masa1, $_masa2, $_cn1, $_cn2, $_dumbre1, $_dumbre2, $_delta1, $_delta2, $_certi1, $_certi2, $_prom_kilo, $_prom_inc, $_obs, $_UID)
    {

        $_obs = $this->Free($_obs);
        $_fechaA = $this->_FECHA2($_fechaA);

        $this->InvalidaAnteriores($_xanalizar, $_tipo);

        $cs = $this->ConsecutivoEnsayoGet();

        return $this->_TRANS("EXECUTE PA_MAG_106 '{$cs}',
			'{$_xanalizar}', 
			'{$_tipo}', 
			'{$_fechaA}', 
			'{$_balon1}', 
			'{$_balon2}', 	
			'{$_pipetaD1}', 
			'{$_pipetaD2}', 
			'{$_pipetaD3}', 
			'{$_balonD1}', 
			'{$_balonD2}', 
			'{$_balonD3}', 
			'{$_repetibilidad}', 
			'{$_resolucion}', 
			'{$_inc_cer}', 
			'{$_emp}', 
			'{$_masa1}', 
			'{$_masa2}', 
			'{$_cn1}', 
			'{$_cn2}', 
			'{$_dumbre1}', 
			'{$_dumbre2}', 
			'{$_delta1}', 
			'{$_delta2}', 
			'{$_certi1}', 
			'{$_certi2}', 
			'{$_prom_kilo}', 
			'{$_prom_inc}', 
			'{$_obs}', 
			'{$_UID}';");
    }

    function PbcdEncabezadoVacio($_xanalizar)
    {
        $ROW = $this->_QUERY("SELECT CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, MUE000.solicitud, MUE000.ref, MUE001.analisis, VAR005.nombre AS elemento
		FROM MUE001 INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN VAR005 ON MUE001.analisis = VAR005.id
		WHERE (MUEBIT.accion = '0' OR MUEBIT.accion = 'A') AND (MUE001.id = '{$_xanalizar}');");

        //VACIOS
        list(
            $ROW[0]['fechaA'],
            $ROW[0]['fechaC'],
            $ROW[0]['balon1'],
            $ROW[0]['balon2'],
            $ROW[0]['pipetaD1'],
            $ROW[0]['balonD1'],
            $ROW[0]['pipetaD2'],
            $ROW[0]['balonD2'],
            $ROW[0]['pipetaD3'],
            $ROW[0]['balonD3'],
            $ROW[0]['repetibilidad'],
            $ROW[0]['resolucion'],
            $ROW[0]['inc_cer'],
            $ROW[0]['emp'],
            $ROW[0]['cn1'],
            $ROW[0]['cn2'],
            $ROW[0]['dumbre1'],
            $ROW[0]['dumbre2'],
            $ROW[0]['delta1'],
            $ROW[0]['delta2'],
            $ROW[0]['certi1'],
            $ROW[0]['certi2'],
            $ROW[0]['masa1'],
            $ROW[0]['masa2'],
            $ROW[0]['obs']
            ) = array('', '', '', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');

        return $ROW;
    }

    function PotasioEncabezadoGet($_ensayo)
    {
        return $this->_QUERY("SELECT MUE000.solicitud, MUE000.ref, MUE002.tipo, CONVERT(VARCHAR, MUE002.fecha, 105) AS fechaC, MUE002.declarada AS rango, MUE002.unidad, CONVERT(VARCHAR, MET009.fechaA, 105) AS fechaA, MET009.reactivos, MET009.factor1, MET009.volumen1, MET009.densidad, MET009.volumen2, MET009.masaA1, MET009.masaA2, 
        MET009.masaB1, MET009.masaB2, MET009.des1, MET009.des3, MET009.des4, CONVERT(TEXT, MET009.obs) AS obs, VAR005.nombre AS elemento, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI,
        MET_INC.certificado_densidad AS icCert,  MET_INC.resolucion_densidad AS icRes
        FROM MUE000 INNER JOIN MUE001 ON MUE000.id = MUE001.muestra INNER JOIN MUE002 ON MUE001.id = MUE002.xanalizar INNER JOIN MET009 ON MUE002.id = MET009.ensayo INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN VAR005 ON MUE001.analisis = VAR005.id left outer join MET_INC on MET009.ensayo = MET_INC.ensayo
		WHERE (MET009.ensayo = '{$_ensayo}') AND (MUEBIT.accion = '0');");
    }

    function PotasioEncabezadoSet($_xanalizar, $_tipo, $_fechaA, $_rango, $_unidad, $_reactivos, $_factor1, $_volumen1, $_densidad, $_volumen2, $_masaA1, $_masaA2, $_masaB1, $_masaB2, $_des1, $_des3, $_des4, $_CD, $_IC, $_obs, $_UID)
    {
        $_rango = $this->Free($_rango);
        $_obs = $this->Free($_obs);
        $_fechaA = $this->_FECHA2($_fechaA);

        $this->InvalidaAnteriores($_xanalizar, $_tipo);

        $cs = $this->ConsecutivoEnsayoGet();

        return $this->_TRANS("EXECUTE PA_MAG_107 '{$cs}',
			'{$_xanalizar}', 
			'{$_tipo}', 
			'{$_fechaA}', 
			'{$_rango}', 
			'{$_unidad}', 
			'{$_reactivos}',  
			'{$_factor1}',  
			'{$_volumen1}',  
			'{$_densidad}',  
			'{$_volumen2}',  
			'{$_masaA1}',  
			'{$_masaA2}',  
			'{$_masaB1}',  
			'{$_masaB2}',  
			'{$_des1}',  
			'{$_des3}',  
			'{$_des4}',  
			'{$_CD}',  
			'{$_IC}',
			'{$_obs}',
			'{$_UID}';");
    }

    function PotasioEncabezadoVacio($_xanalizar)
    {
        $ROW = $this->_QUERY("SELECT '' AS rango, '' AS unidad, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, MUE000.solicitud, MUE000.ref, MUE001.analisis, VAR005.nombre AS elemento
		FROM MUE001 INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN VAR005 ON MUE001.analisis = VAR005.id
		WHERE (MUEBIT.accion = '0' OR MUEBIT.accion = 'A') AND (MUE001.id = '{$_xanalizar}');");
        //
        $ROW2 = $this->_QUERY("SELECT SER004.rango, SER004.unidad
		FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud
		WHERE (SER003.cs = '{$ROW[0]['solicitud']}') AND (SER004.analisis = {$ROW[0]['analisis']});");
        if ($ROW2) {
            $ROW[0]['rango'] = $ROW2[0]['rango'];
            $ROW[0]['unidad'] = $ROW2[0]['unidad'];
        }

        list(
            $ROW[0]['fechaA'],
            $ROW[0]['fechaC'],
            $ROW[0]['reactivos'],
            $ROW[0]['factor1'],
            $ROW[0]['volumen1'],
            $ROW[0]['volumen2'],
            $ROW[0]['masaA1'],
            $ROW[0]['masaB1'],
            $ROW[0]['masaA2'],
            $ROW[0]['masaB2'],
            $ROW[0]['densidad'],
            $ROW[0]['des1'],
            $ROW[0]['des4'],
            $ROW[0]['des3'],
            $ROW[0]['obs']
            ) = array('', '', '0', '34.61', '43', '0', '0', '0', '0', '0', '1.09969', '0.05', '0.00015', '0.05', '');

        return $ROW;
    }

    function PulverulenciaEncabezadoGet($_ensayo)
    {
        return $this->_QUERY("SELECT MUE000.ref, MUE002.tipo, CONVERT(VARCHAR, MUE002.fecha, 105) AS fechaC, MUE002.ingrediente, MUE002.declarada AS rango, MUE002.unidad, CONVERT(VARCHAR, MET013.fechaA, 105) AS fechaA, MET013.tipo_form, MET013.masa, MET013.volumen, MET013.tamiz1, MET013.tamiz2, MET013.tamiz3, MET013.tamiz4, MET013.tamiz5, MET013.obs, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, (SEG001.nombre +' '+ SEG001.ap1) AS analista
		FROM MUE002 INNER JOIN MET013 ON MUE002.id = MET013.ensayo INNER JOIN MUE001 ON MUE002.xanalizar = MUE001.id INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN SEG001 ON MUE002.analista = SEG001.id
		WHERE (MUE002.id = '{$_ensayo}') AND (MUEBIT.accion = '0');");
    }

    function PulverulenciaEncabezadoSet($_xanalizar, $_tipo, $_ingrediente, $_fechaA, $_tipo_form, $_masa, $_volumen, $_tamiz1, $_tamiz2, $_tamiz3, $_tamiz4, $_tamiz5, $_acumulativo, $_fraccion, $_obs, $_UID)
    {
        $_ingrediente = $this->Free($_ingrediente);
        $_obs = $this->Free($_obs);
        $_fechaA = $this->_FECHA($_fechaA);

        $this->InvalidaAnteriores($_xanalizar, $_tipo);

        $cs = $this->ConsecutivoEnsayoGet();

        return $this->_TRANS("EXECUTE PA_MAG_114 '{$cs}',
			'{$_xanalizar}', 
			'{$_tipo}', 
			'{$_ingrediente}',  
			'{$_fechaA}', 
			'{$_tipo_form}', 
			'{$_masa}', 
			'{$_volumen}',
			'{$_tamiz1}',
			'{$_tamiz2}',
			'{$_tamiz3}',
			'{$_tamiz4}',
			'{$_tamiz5}',
			'{$_acumulativo}', 
			'{$_fraccion}', 
			'{$_obs}',
			'{$_UID}';");
    }

    function PulverulenciaEncabezadoVacio($_xanalizar)
    {
        $ROW = $this->_QUERY("SELECT MUE000.ref, '' AS ingrediente, '' AS tipo_form, '' AS rango, '' AS unidad, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, MUE000.solicitud, MUE001.analisis
		FROM MUE001 INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra
		WHERE (MUEBIT.accion = '0' OR MUEBIT.accion = 'A') AND (MUE001.id = '{$_xanalizar}');");
        //
        $ROW2 = $this->_QUERY("SELECT VAR007.nombre AS tipo_form
		FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR007 ON SER003.tipo_form = VAR007.id INNER JOIN VAR005 ON SER004.analisis = VAR005.id
		WHERE (SER003.cs = '{$ROW[0]['solicitud']}') AND (SER004.analisis = {$ROW[0]['analisis']});");
        if ($ROW2) {
            $ROW[0]['tipo_form'] = $ROW2[0]['tipo_form'];
        }
        //VACIOS
        $ROW[0]['ingrediente'] = '';
        $ROW[0]['fechaA'] = '';
        $ROW[0]['fechaC'] = '';
        $ROW[0]['masa'] = '0';
        $ROW[0]['volumen'] = '0';
        $ROW[0]['tamiz1'] = '0';
        $ROW[0]['tamiz2'] = '0';
        $ROW[0]['tamiz3'] = '0';
        $ROW[0]['tamiz4'] = '0';
        $ROW[0]['tamiz5'] = '0';
        $ROW[0]['obs'] = '';

        return $ROW;
    }

    function QuechersDetalleGet($_ensayo)
    {
        return $this->_QUERY("SELECT MET002.analito, MET002.tecnica, MET002.conc, MET002.reportar, VAR001.nombre
		FROM MET002 INNER JOIN VAR001 ON MET002.matriz = VAR001.id
		WHERE (MET002.ensayo = '{$_ensayo}');");
    }

    function QuechersDetalleSet($_ensayo, $_analito, $_matriz, $_id, $_conc, $_reportar, $_tecnica)
    {
        $_analito = str_replace('1=1&', '', $_analito);
        $_analito = explode('&', $_analito);
        $_conc = str_replace('1=1&', '', $_conc);
        $_conc = explode('&', $_conc);
        $_reportar = str_replace('1=1&', '', $_reportar);
        $_reportar = explode('&', $_reportar);

        $_tecnica = str_replace('1=1&', '', $_tecnica);
        $_tecnica = explode('&', $_tecnica);

        $_id = str_replace('1=1&', '', $_id);
        $_id = explode('&', $_id);

        for ($x = 0; $x < count($_analito); $x++) {

            $tildes = array("'", '@', '&', '�', '�', '�', '�', '�','Á');
            $solas = array('', '', '', 'a', 'e', 'i', 'o', 'u','A','E', 'I','O','U');
            //$_analito[$x] = strtolower($this->Free($_analito[$x]));

           $_analito[$x] = ($this->Free($_analito[$x]));
            $_analito[$x] = str_replace($tildes, $solas, $_analito[$x]);

            //cambio  $_conc[$x] = str_replace(',', '', $_conc[$x]);
            $_conc[$x] = str_replace(',', '.', $_conc[$x]);
            $_conc[$x] = strtolower($_conc[$x]);

            $_tecnica[$x] = str_replace(',', '', $_tecnica[$x]);
            $_tecnica[$x] = strtolower($_tecnica[$x]);

            $_id[$x] = str_replace(',', '', $_id[$x]);
            $_id[$x] = strtolower($_id[$x]);

            if ($_conc[$x] == 'nd')
                $_conc[$x] = '-1';
            elseif ($_conc[$x] == 'nc')
                $_conc[$x] = '-2';

            $this->_TRANS("INSERT INTO MET002 VALUES('{$_ensayo}', '{$_analito[$x]}', '{$_id[$x]}', {$_tecnica[$x]}, {$_matriz}, {$_conc[$x]}, {$_reportar[$x]});");
        }
    }

    function QuechersEncabezadoGet($_xanalizar)
    {
        $ROW = $this->_QUERY("SELECT MUE000.id, MUE000.solicitud, MUE000.ref
		FROM MUE000 INNER JOIN MUE001 ON MUE000.id = MUE001.muestra
		WHERE (MUE001.id = '{$_xanalizar}');");
        // $ROW[0]['ref'] = str_replace(' ', '', $ROW[0]['ref']);

        //OBTIENE LA MATRIZ O CULTIVO QUE CORRESPONDE
        $mat = str_replace(' ', '', $ROW[0]['ref']);
        $mat = str_replace('--', '-', $mat);
        $ROW2 = $this->_QUERY("SELECT VAR001.id AS matriz, VAR001.nombre
		FROM SER002 INNER JOIN VAR001 ON SER002.matriz = VAR001.id 
		WHERE (SER002.conSolicitud = '{$mat}');");

        for ($x = 0; $x < count($ROW2); $x++) {
            $cod_int = str_replace('LRE-', '', $ROW[0]['solicitud']) . '-' . ($x + 1);
            $cod_int = str_replace(' ', '', $cod_int);


            $ROW[0]['matriz'] = $ROW2[$x]['matriz'];
            $ROW[0]['nombre'] = $ROW2[$x]['nombre'];
            break;
            //}
        }
        return $ROW;
    }

    function QuechersEncabezadoSet($_ensayo, $_xanalizar, $_tipo, $_UID)
    {
        $this->InvalidaAnteriores($_xanalizar, $_tipo);
        return $this->_TRANS("INSERT INTO MUE002 VALUES('{$_ensayo}', '{$_xanalizar}', {$_tipo}, GETDATE(), NULL, NULL, 'Registro exitoso', NULL, NULL, NULL, 1, {$_UID}, NULL);");
    }

    function SuspensibilidadAzEncabezadoGet($_ensayo)
    {
        return $this->_QUERY("SELECT MUE000.ref, MUE002.tipo, CONVERT(VARCHAR, MUE002.fecha, 105) AS fechaC, MUE002.ingrediente, CONVERT(VARCHAR, MET017.fechaA, 105) AS fechaA, MET017.tipo_form, MET017.dosis, CONVERT(VARCHAR, MET017.fechaP, 105) AS fechaP, MET017.maria, 
        MET017.numreactivo, CONVERT(VARCHAR, MET017.fechaD, 105) AS fechaD, MET017.resultado,  MET017.masa AS valA1, MET017.muestra AS valA2, MET017.encontrado AS valA3, MET017.yodo AS valA4, MET017.consumido AS valA5, MET017.factor AS valB1, MET017.obs, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, (SEG001.nombre +' '+ SEG001.ap1) AS analista
		FROM MUE002 INNER JOIN MET017 ON MUE002.id = MET017.ensayo INNER JOIN MUE001 ON MUE002.xanalizar = MUE001.id INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN SEG001 ON MUE002.analista = SEG001.id
		WHERE (MUE002.id = '{$_ensayo}') AND (MUEBIT.accion = '0');");
    }

    function SuspensibilidadAzEncabezadoSet($_xanalizar, $_tipo, $_ingrediente, $_fechaA, $_tipo_form, $_dosis, $_fechaP, $_maria, $_numreactivo, $_fechaD, $_resultado, $_masa, $_muestra, $_encontrado, $_yodo, $_consumido, $_factor, $_IC, $_obs, $_UID)
    {
        $_ingrediente = $this->Free($_ingrediente);
        $_dosis = $this->Free($_dosis);
        $_numreactivo = $this->Free($_numreactivo);
        $_obs = $this->Free($_obs);
        $_fechaA = $this->_FECHA($_fechaA);
        $_fechaP = $this->_FECHA($_fechaP);
        $_fechaD = $this->_FECHA($_fechaD);

        $this->InvalidaAnteriores($_xanalizar, $_tipo);

        $cs = $this->ConsecutivoEnsayoGet();

        return $this->_TRANS("EXECUTE PA_MAG_118 '{$cs}',
			'{$_xanalizar}', 
			'{$_tipo}', 
			'{$_ingrediente}',  
			'{$_fechaA}', 
			'{$_tipo_form}', 
			'{$_dosis}', 
			'{$_fechaP}', 
			'{$_maria}',
			'{$_numreactivo}', 
			'{$_fechaD}', 
			'{$_resultado}', 
			'{$_masa}',
			'{$_muestra}',
			'{$_encontrado}',
			'{$_yodo}',
			'{$_consumido}',
			'{$_factor}',
			'{$_IC}',
			'{$_obs}',
			'{$_UID}';");
    }

    function SuspensibilidadDiEncabezadoGet($_ensayo)
    {
        return $this->_QUERY("SELECT MUE000.ref, MUE002.tipo, CONVERT(VARCHAR, MUE002.fecha, 105) AS fechaC, MUE002.ingrediente, CONVERT(VARCHAR, MET017.fechaA, 105) AS fechaA, MET017.tipo_form, MET017.dosis, CONVERT(VARCHAR, MET017.fechaP, 105) AS fechaP, MET017.maria, 
        MET017.numreactivo, CONVERT(VARCHAR, MET017.fechaD, 105) AS fechaD, MET017.resultado,  MET017.masa AS valA1, MET017.muestra AS valA2, MET017.encontrado AS valA3, MET017.yodo AS valA4, MET017.consumido AS valA5, MET017.obs, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, (SEG001.nombre +' '+ SEG001.ap1) AS analista
		FROM MUE002 INNER JOIN MET017 ON MUE002.id = MET017.ensayo INNER JOIN MUE001 ON MUE002.xanalizar = MUE001.id INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN SEG001 ON MUE002.analista = SEG001.id
		WHERE (MUE002.id = '{$_ensayo}') AND (MUEBIT.accion = '0');");
    }

    function SuspensibilidadEncabezadoVacio($_xanalizar)
    {
        $ROW = $this->_QUERY("SELECT MUE000.ref, '' AS ingrediente, '' AS tipo_form, '' AS rango, '' AS unidad, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, MUE000.solicitud, MUE001.analisis
		FROM MUE001 INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra
		WHERE (MUEBIT.accion = '0' OR MUEBIT.accion = 'A') AND (MUE001.id = '{$_xanalizar}');");
        //
        $ROW2 = $this->_QUERY("SELECT SER004.rango, SER004.unidad, VAR007.nombre AS tipo_form, VAR005.nombre AS ingrediente
		FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR007 ON SER003.tipo_form = VAR007.id INNER JOIN VAR005 ON SER004.analisis = VAR005.id
		WHERE (SER003.cs = '{$ROW[0]['solicitud']}') AND (SER004.analisis = {$ROW[0]['analisis']});");
        if ($ROW2) {
            $ROW[0]['ingrediente'] = $ROW2[0]['ingrediente'];
            $ROW[0]['tipo_form'] = $ROW2[0]['tipo_form'];
            $ROW[0]['rango'] = $ROW2[0]['rango'];
            $ROW[0]['unidad'] = $ROW2[0]['unidad'];
        }
        $ROW2 = $this->_QUERY("SELECT VAR005.nombre FROM VAR005 INNER JOIN SER004 ON VAR005.id = dbo.SER004.analisis WHERE SER004.solicitud = '{$ROW[0]['solicitud']}' AND SER004.rango != '';");
        $ROW[0]['ingrediente'] = $ROW2[0]['nombre'];
        //VACIOS
        $ROW[0]['fechaA'] = '';
        $ROW[0]['fechaC'] = '';
        $ROW[0]['dosis'] = '';
        $ROW[0]['maria'] = '30';
        $ROW[0]['numreactivo'] = '';
        $ROW[0]['fechaP'] = '';
        $ROW[0]['fechaD'] = '';
        $ROW[0]['resultado'] = '0';
        $ROW[0]['origen'] = '';
        $ROW[0]['1A'] = $ROW[0]['1B'] = $ROW[0]['2A'] = $ROW[0]['2B'] = '0';
        $ROW[0]['masa'] = '0';
        $ROW[0]['pureza'] = '0';
        $ROW[0]['existe'] = '';
        $ROW[0]['disol'] = '';
        $ROW[0]['muestra'] = '0';
        $ROW[0]['encontrado'] = '0';
        $ROW[0]['valA1'] = '25';
        $ROW[0]['valA2'] = $ROW[0]['valA3'] = $ROW[0]['valA4'] = $ROW[0]['valA5'] = '0';
        $ROW[0]['valB1'] = $ROW[0]['valB2'] = $ROW[0]['valB3'] = $ROW[0]['valB4'] = $ROW[0]['valB5'] = '0';
        $ROW[0]['obs'] = '';

        return $ROW;
    }

    function SuspensibilidadIAEncabezadoGet($_ensayo)
    {
        return $this->_QUERY("SELECT MUE000.ref, MUE002.tipo, CONVERT(VARCHAR, MUE002.fecha, 105) AS fechaC, MUE002.ingrediente, CONVERT(VARCHAR, MET016.fechaA, 105) AS fechaA, MET016.tipo_form, MET016.dosis, CONVERT(VARCHAR, MET016.fechaP, 105) AS fechaP, MET016.maria, 
        MET016.numreactivo, CONVERT(VARCHAR, MET016.fechaD, 105) AS fechaD, MET016.resultado, MET016.origen, MET016.[1A], MET016.[1B], MET016.[2A], MET016.[2B], MET016.masa, MET016.pureza, MET016.existe, MET016.disol, MET016.muestra, MET016.encontrado, MET016.valA1, MET016.valA2, MET016.valA3, MET016.valA4, MET016.valA5, MET016.valB1, MET016.valB2, MET016.valB3, MET016.valB4, MET016.valB5, MET016.obs, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, (SEG001.nombre +' '+ SEG001.ap1) AS analista
		FROM MUE002 INNER JOIN MET016 ON MUE002.id = MET016.ensayo INNER JOIN MUE001 ON MUE002.xanalizar = MUE001.id INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN SEG001 ON MUE002.analista = SEG001.id
		WHERE (MUE002.id = '{$_ensayo}') AND (MUEBIT.accion = '0');");
    }

    function SuspensibilidadIAEncabezadoSet($_xanalizar, $_tipo, $_ingrediente, $_fechaA, $_tipo_form, $_dosis, $_fechaP, $_maria, $_numreactivo, $_fechaD, $_resultado, $_origen, $_1A, $_1B, $_2A, $_2B, $_masa, $_pureza, $_existe, $_disol, $_muestra, $_encontrado, $_valA1, $_valA2, $_valA3, $_valA4, $_valA5, $_valB1, $_valB2, $_valB3, $_valB4, $_valB5, $_IC, $_obs, $_UID)
    {
        $_ingrediente = $this->Free($_ingrediente);
        $_dosis = $this->Free($_dosis);
        $_origen = $this->Free($_origen);
        $_numreactivo = $this->Free($_numreactivo);
        $_disol = $this->Free($_disol);
        $_obs = $this->Free($_obs);
        $_fechaA = $this->_FECHA($_fechaA);
        $_fechaP = $this->_FECHA($_fechaP);
        $_fechaD = $this->_FECHA($_fechaD);

        $this->InvalidaAnteriores($_xanalizar, $_tipo);

        $cs = $this->ConsecutivoEnsayoGet();

        return $this->_TRANS("EXECUTE PA_MAG_117 '{$cs}',
			'{$_xanalizar}', 
			'{$_tipo}', 
			'{$_ingrediente}',  
			'{$_fechaA}', 
			'{$_tipo_form}', 
			'{$_dosis}', 
			'{$_fechaP}', 
			'{$_maria}',
			'{$_numreactivo}', 
			'{$_fechaD}', 
			'{$_resultado}', 
			'{$_origen}',
			'{$_1A}',
			'{$_1B}',
			'{$_2A}',
			'{$_2B}',
			'{$_masa}',
			'{$_pureza}',
			'{$_existe}',
			'{$_disol}',
			'{$_muestra}',
			'{$_encontrado}',
			'{$_valA1}',
			'{$_valA2}',
			'{$_valA3}',
			'{$_valA4}',
			'{$_valA5}',
			'{$_valB1}',
			'{$_valB2}',
			'{$_valB3}',
			'{$_valB4}',
			'{$_valB5}',
			'{$_IC}',
			'{$_obs}',
			'{$_UID}';");
    }

    function IncccEncabezadoVacio()
    {
        return array(0 => array(
            'borocon1' => '',
            'borocon2' => '',
            'borocon3' => '',
            'borocon4' => '',
            'borocon5' => '',
            'boroinc1' => '',
            'boroinc2' => '',
            'boroinc3' => '',
            'boroinc4' => '',
            'boroinc5' => '',
            'boroincrel1' => '',
            'boroincrel2' => '',
            'boroincrel3' => '',
            'boroincrel4' => '',
            'boroincrel5' => '',
            'boroAA11' => '',
            'boroAA12' => '',
            'boroAA13' => '',
            'boroAA14' => '',
            'boroAA15' => '',
            'boroA21' => '',
            'boroA22' => '',
            'boroA23' => '',
            'boroA24' => '',
            'boroA25' => '',
            'boroA31' => '',
            'boroA32' => '',
            'boroA33' => '',
            'boroA34' => '',
            'boroA35' => '',
            'boroA41' => '',
            'boroA42' => '',
            'boroA43' => '',
            'boroA44' => '',
            'boroA45' => '',
            'boroCC11' => '',
            'boroCC12' => '',
            'boroCC13' => '',
            'boroCC14' => '',
            'boroCC15' => '',
            'boroC21' => '',
            'boroC22' => '',
            'boroC23' => '',
            'boroC24' => '',
            'boroC25' => '',
            'boroC31' => '',
            'boroC32' => '',
            'boroC33' => '',
            'boroC34' => '',
            'boroC35' => '',
            'boroC41' => '',
            'boroC42' => '',
            'boroC43' => '',
            'boroC44' => '',
            'boroC45' => '',
            'borocp1' => '',
            'borocp2' => '',
            'borocp3' => '',
            'borocp4' => '',
            'borocp5' => '',
            'boroa1' => '',
            'boroa2' => '',
            'boroa3' => '',
            'boroa4' => '',
            'boroa5' => '',
            'boroa6' => '',
            'boroa7' => '',
            'boroa8' => '',
            'boroa9' => '',
            'boroa10' => '',
            'boroa11' => '',
            'boroa12' => '',
            'boroa13' => '',
            'boroa14' => '',
            'boroa15' => '',
            'boroa16' => '',
            'boroa17' => '',
            'boroa18' => '',
            'boroa19' => '',
            'boroa20' => '',
            'boroc1' => '',
            'boroc2' => '',
            'boroc3' => '',
            'boroc4' => '',
            'boroc5' => '',
            'boroc6' => '',
            'boroc7' => '',
            'boroc8' => '',
            'boroc9' => '',
            'boroc10' => '',
            'boroc11' => '',
            'boroc12' => '',
            'boroc13' => '',
            'boroc14' => '',
            'boroc15' => '',
            'boroc16' => '',
            'boroc17' => '',
            'boroc18' => '',
            'boroc19' => '',
            'boroc20' => '',
            'boromaxinc' => '',
            'boromaxincrel' => '',
            'boronummedxmuestra' => '',
            'boronummedxpuncurva' => '',
            'boronummedcurva' => '',
            'boroconmuestra1' => '',
            'boroconmuestra2' => '',
            'boroprompatcalibracion' => '',
            'borodesvestresidual' => '',
            'borodesvestcurvacali' => '',
            'boronp' => '',
            'borolote' => '',
            'borovence' => '',
            'borocc1' => '',
            'borocc2' => '',
            'boroest1' => '',
            'boroest2' => '',
            'boroccest1' => '',
            'boroccest2' => '',
            'borovalor' => '',
            'boroincexp' => '',
            'borok' => '',
            'boroincest' => '',
            'boroestandar1' => '',
            'boroestandar2' => '',
            'boroestandar3' => '',
            'boroestandar4' => '',
            'boroestandar5' => '',
            'boropipeta1' => '',
            'boropipeta2' => '',
            'boropipeta3' => '',
            'boropipeta4' => '',
            'boropipeta5' => '',
            'borovolcorrpipeta1' => '',
            'borovolcorrpipeta2' => '',
            'borovolcorrpipeta3' => '',
            'borovolcorrpipeta4' => '',
            'borovolcorrpipeta5' => '',
            'boroincpip1' => '',
            'boroincpip2' => '',
            'boroincpip3' => '',
            'boroincpip4' => '',
            'boroincpip5' => '',
            'borobalon1' => '',
            'borobalon2' => '',
            'borobalon3' => '',
            'borobalon4' => '',
            'borobalon5' => '',
            'borodilinc1' => '',
            'borodilinc2' => '',
            'borodilinc3' => '',
            'borodilinc4' => '',
            'borodilinc5' => '',
            'calciocon1' => '',
            'calciocon2' => '',
            'calciocon3' => '',
            'calciocon4' => '',
            'calciocon5' => '',
            'calcioinc1' => '',
            'calcioinc2' => '',
            'calcioinc3' => '',
            'calcioinc4' => '',
            'calcioinc5' => '',
            'calcioincrel1' => '',
            'calcioincrel2' => '',
            'calcioincrel3' => '',
            'calcioincrel4' => '',
            'calcioincrel5' => '',
            'calcioAA11' => '',
            'calcioAA12' => '',
            'calcioAA13' => '',
            'calcioAA14' => '',
            'calcioAA15' => '',
            'calcioA21' => '',
            'calcioA22' => '',
            'calcioA23' => '',
            'calcioA24' => '',
            'calcioA25' => '',
            'calcioA31' => '',
            'calcioA32' => '',
            'calcioA33' => '',
            'calcioA34' => '',
            'calcioA35' => '',
            'calcioA41' => '',
            'calcioA42' => '',
            'calcioA43' => '',
            'calcioA44' => '',
            'calcioA45' => '',
            'calcioCC11' => '',
            'calcioCC12' => '',
            'calcioCC13' => '',
            'calcioCC14' => '',
            'calcioCC15' => '',
            'calcioC21' => '',
            'calcioC22' => '',
            'calcioC23' => '',
            'calcioC24' => '',
            'calcioC25' => '',
            'calcioC31' => '',
            'calcioC32' => '',
            'calcioC33' => '',
            'calcioC34' => '',
            'calcioC35' => '',
            'calcioC41' => '',
            'calcioC42' => '',
            'calcioC43' => '',
            'calcioC44' => '',
            'calcioC45' => '',
            'calciocp1' => '',
            'calciocp2' => '',
            'calciocp3' => '',
            'calciocp4' => '',
            'calciocp5' => '',
            'calcioa1' => '',
            'calcioa2' => '',
            'calcioa3' => '',
            'calcioa4' => '',
            'calcioa5' => '',
            'calcioa6' => '',
            'calcioa7' => '',
            'calcioa8' => '',
            'calcioa9' => '',
            'calcioa10' => '',
            'calcioa11' => '',
            'calcioa12' => '',
            'calcioa13' => '',
            'calcioa14' => '',
            'calcioa15' => '',
            'calcioa16' => '',
            'calcioa17' => '',
            'calcioa18' => '',
            'calcioa19' => '',
            'calcioa20' => '',
            'calcioc1' => '',
            'calcioc2' => '',
            'calcioc3' => '',
            'calcioc4' => '',
            'calcioc5' => '',
            'calcioc6' => '',
            'calcioc7' => '',
            'calcioc8' => '',
            'calcioc9' => '',
            'calcioc10' => '',
            'calcioc11' => '',
            'calcioc12' => '',
            'calcioc13' => '',
            'calcioc14' => '',
            'calcioc15' => '',
            'calcioc16' => '',
            'calcioc17' => '',
            'calcioc18' => '',
            'calcioc19' => '',
            'calcioc20' => '',
            'calciomaxinc' => '',
            'calciomaxincrel' => '',
            'calcionummedxmuestra' => '',
            'calcionummedxpuncurva' => '',
            'calcionummedcurva' => '',
            'calcioconmuestra1' => '',
            'calcioconmuestra2' => '',
            'calcioprompatcalibracion' => '',
            'calciodesvestresidual' => '',
            'calciodesvestcurvacali' => '',
            'calcionp' => '',
            'calciolote' => '',
            'calciovence' => '',
            'calciocc1' => '',
            'calciocc2' => '',
            'calcioest1' => '',
            'calcioest2' => '',
            'calcioccest1' => '',
            'calcioccest2' => '',
            'calciovalor' => '',
            'calcioincexp' => '',
            'calciok' => '',
            'calcioincest' => '',
            'calcioestandar1' => '',
            'calcioestandar2' => '',
            'calcioestandar3' => '',
            'calcioestandar4' => '',
            'calcioestandar5' => '',
            'calciopipeta1' => '',
            'calciopipeta2' => '',
            'calciopipeta3' => '',
            'calciopipeta4' => '',
            'calciopipeta5' => '',
            'calciovolcorrpipeta1' => '',
            'calciovolcorrpipeta2' => '',
            'calciovolcorrpipeta3' => '',
            'calciovolcorrpipeta4' => '',
            'calciovolcorrpipeta5' => '',
            'calcioincpip1' => '',
            'calcioincpip2' => '',
            'calcioincpip3' => '',
            'calcioincpip4' => '',
            'calcioincpip5' => '',
            'calciobalon1' => '',
            'calciobalon2' => '',
            'calciobalon3' => '',
            'calciobalon4' => '',
            'calciobalon5' => '',
            'calciodilinc1' => '',
            'calciodilinc2' => '',
            'calciodilinc3' => '',
            'calciodilinc4' => '',
            'calciodilinc5' => '',
            'cobrecon1' => '',
            'cobrecon2' => '',
            'cobrecon3' => '',
            'cobrecon4' => '',
            'cobrecon5' => '',
            'cobreinc1' => '',
            'cobreinc2' => '',
            'cobreinc3' => '',
            'cobreinc4' => '',
            'cobreinc5' => '',
            'cobreincrel1' => '',
            'cobreincrel2' => '',
            'cobreincrel3' => '',
            'cobreincrel4' => '',
            'cobreincrel5' => '',
            'cobreAA11' => '',
            'cobreAA12' => '',
            'cobreAA13' => '',
            'cobreAA14' => '',
            'cobreAA15' => '',
            'cobreA21' => '',
            'cobreA22' => '',
            'cobreA23' => '',
            'cobreA24' => '',
            'cobreA25' => '',
            'cobreA31' => '',
            'cobreA32' => '',
            'cobreA33' => '',
            'cobreA34' => '',
            'cobreA35' => '',
            'cobreA41' => '',
            'cobreA42' => '',
            'cobreA43' => '',
            'cobreA44' => '',
            'cobreA45' => '',
            'cobreCC11' => '',
            'cobreCC12' => '',
            'cobreCC13' => '',
            'cobreCC14' => '',
            'cobreCC15' => '',
            'cobreC21' => '',
            'cobreC22' => '',
            'cobreC23' => '',
            'cobreC24' => '',
            'cobreC25' => '',
            'cobreC31' => '',
            'cobreC32' => '',
            'cobreC33' => '',
            'cobreC34' => '',
            'cobreC35' => '',
            'cobreC41' => '',
            'cobreC42' => '',
            'cobreC43' => '',
            'cobreC44' => '',
            'cobreC45' => '',
            'cobrecp1' => '',
            'cobrecp2' => '',
            'cobrecp3' => '',
            'cobrecp4' => '',
            'cobrecp5' => '',
            'cobrea1' => '',
            'cobrea2' => '',
            'cobrea3' => '',
            'cobrea4' => '',
            'cobrea5' => '',
            'cobrea6' => '',
            'cobrea7' => '',
            'cobrea8' => '',
            'cobrea9' => '',
            'cobrea10' => '',
            'cobrea11' => '',
            'cobrea12' => '',
            'cobrea13' => '',
            'cobrea14' => '',
            'cobrea15' => '',
            'cobrea16' => '',
            'cobrea17' => '',
            'cobrea18' => '',
            'cobrea19' => '',
            'cobrea20' => '',
            'cobrec1' => '',
            'cobrec2' => '',
            'cobrec3' => '',
            'cobrec4' => '',
            'cobrec5' => '',
            'cobrec6' => '',
            'cobrec7' => '',
            'cobrec8' => '',
            'cobrec9' => '',
            'cobrec10' => '',
            'cobrec11' => '',
            'cobrec12' => '',
            'cobrec13' => '',
            'cobrec14' => '',
            'cobrec15' => '',
            'cobrec16' => '',
            'cobrec17' => '',
            'cobrec18' => '',
            'cobrec19' => '',
            'cobrec20' => '',
            'cobremaxinc' => '',
            'cobremaxincrel' => '',
            'cobrenummedxmuestra' => '',
            'cobrenummedxpuncurva' => '',
            'cobrenummedcurva' => '',
            'cobreconmuestra1' => '',
            'cobreconmuestra2' => '',
            'cobreprompatcalibracion' => '',
            'cobredesvestresidual' => '',
            'cobredesvestcurvacali' => '',
            'cobrenp' => '',
            'cobrelote' => '',
            'cobrevence' => '',
            'cobrecc1' => '',
            'cobrecc2' => '',
            'cobreest1' => '',
            'cobreest2' => '',
            'cobreccest1' => '',
            'cobreccest2' => '',
            'cobrevalor' => '',
            'cobreincexp' => '',
            'cobrek' => '',
            'cobreincest' => '',
            'cobreestandar1' => '',
            'cobreestandar2' => '',
            'cobreestandar3' => '',
            'cobreestandar4' => '',
            'cobreestandar5' => '',
            'cobrepipeta1' => '',
            'cobrepipeta2' => '',
            'cobrepipeta3' => '',
            'cobrepipeta4' => '',
            'cobrepipeta5' => '',
            'cobrevolcorrpipeta1' => '',
            'cobrevolcorrpipeta2' => '',
            'cobrevolcorrpipeta3' => '',
            'cobrevolcorrpipeta4' => '',
            'cobrevolcorrpipeta5' => '',
            'cobreincpip1' => '',
            'cobreincpip2' => '',
            'cobreincpip3' => '',
            'cobreincpip4' => '',
            'cobreincpip5' => '',
            'cobrebalon1' => '',
            'cobrebalon2' => '',
            'cobrebalon3' => '',
            'cobrebalon4' => '',
            'cobrebalon5' => '',
            'cobredilinc1' => '',
            'cobredilinc2' => '',
            'cobredilinc3' => '',
            'cobredilinc4' => '',
            'cobredilinc5' => '',
            'fosforocon1' => '',
            'fosforocon2' => '',
            'fosforocon3' => '',
            'fosforocon4' => '',
            'fosforocon5' => '',
            'fosforoinc1' => '',
            'fosforoinc2' => '',
            'fosforoinc3' => '',
            'fosforoinc4' => '',
            'fosforoinc5' => '',
            'fosforoincrel1' => '',
            'fosforoincrel2' => '',
            'fosforoincrel3' => '',
            'fosforoincrel4' => '',
            'fosforoincrel5' => '',
            'fosforoAA11' => '',
            'fosforoAA12' => '',
            'fosforoAA13' => '',
            'fosforoAA14' => '',
            'fosforoAA15' => '',
            'fosforoA21' => '',
            'fosforoA22' => '',
            'fosforoA23' => '',
            'fosforoA24' => '',
            'fosforoA25' => '',
            'fosforoA31' => '',
            'fosforoA32' => '',
            'fosforoA33' => '',
            'fosforoA34' => '',
            'fosforoA35' => '',
            'fosforoA41' => '',
            'fosforoA42' => '',
            'fosforoA43' => '',
            'fosforoA44' => '',
            'fosforoA45' => '',
            'fosforoCC11' => '',
            'fosforoCC12' => '',
            'fosforoCC13' => '',
            'fosforoCC14' => '',
            'fosforoCC15' => '',
            'fosforoC21' => '',
            'fosforoC22' => '',
            'fosforoC23' => '',
            'fosforoC24' => '',
            'fosforoC25' => '',
            'fosforoC31' => '',
            'fosforoC32' => '',
            'fosforoC33' => '',
            'fosforoC34' => '',
            'fosforoC35' => '',
            'fosforoC41' => '',
            'fosforoC42' => '',
            'fosforoC43' => '',
            'fosforoC44' => '',
            'fosforoC45' => '',
            'fosforocp1' => '',
            'fosforocp2' => '',
            'fosforocp3' => '',
            'fosforocp4' => '',
            'fosforocp5' => '',
            'fosforoa1' => '',
            'fosforoa2' => '',
            'fosforoa3' => '',
            'fosforoa4' => '',
            'fosforoa5' => '',
            'fosforoa6' => '',
            'fosforoa7' => '',
            'fosforoa8' => '',
            'fosforoa9' => '',
            'fosforoa10' => '',
            'fosforoa11' => '',
            'fosforoa12' => '',
            'fosforoa13' => '',
            'fosforoa14' => '',
            'fosforoa15' => '',
            'fosforoa16' => '',
            'fosforoa17' => '',
            'fosforoa18' => '',
            'fosforoa19' => '',
            'fosforoa20' => '',
            'fosforoc1' => '',
            'fosforoc2' => '',
            'fosforoc3' => '',
            'fosforoc4' => '',
            'fosforoc5' => '',
            'fosforoc6' => '',
            'fosforoc7' => '',
            'fosforoc8' => '',
            'fosforoc9' => '',
            'fosforoc10' => '',
            'fosforoc11' => '',
            'fosforoc12' => '',
            'fosforoc13' => '',
            'fosforoc14' => '',
            'fosforoc15' => '',
            'fosforoc16' => '',
            'fosforoc17' => '',
            'fosforoc18' => '',
            'fosforoc19' => '',
            'fosforoc20' => '',
            'fosforomaxinc' => '',
            'fosforomaxincrel' => '',
            'fosforonummedxmuestra' => '',
            'fosforonummedxpuncurva' => '',
            'fosforonummedcurva' => '',
            'fosforoconmuestra1' => '',
            'fosforoconmuestra2' => '',
            'fosforoprompatcalibracion' => '',
            'fosforodesvestresidual' => '',
            'fosforodesvestcurvacali' => '',
            'fosforonp' => '',
            'fosforolote' => '',
            'fosforovence' => '',
            'fosforocc1' => '',
            'fosforocc2' => '',
            'fosforoest1' => '',
            'fosforoest2' => '',
            'fosforoccest1' => '',
            'fosforoccest2' => '',
            'fosforovalor' => '',
            'fosforoincexp' => '',
            'fosforok' => '',
            'fosforoincest' => '',
            'fosforoestandar1' => '',
            'fosforoestandar2' => '',
            'fosforoestandar3' => '',
            'fosforoestandar4' => '',
            'fosforoestandar5' => '',
            'fosforopipeta1' => '',
            'fosforopipeta2' => '',
            'fosforopipeta3' => '',
            'fosforopipeta4' => '',
            'fosforopipeta5' => '',
            'fosforovolcorrpipeta1' => '',
            'fosforovolcorrpipeta2' => '',
            'fosforovolcorrpipeta3' => '',
            'fosforovolcorrpipeta4' => '',
            'fosforovolcorrpipeta5' => '',
            'fosforoincpip1' => '',
            'fosforoincpip2' => '',
            'fosforoincpip3' => '',
            'fosforoincpip4' => '',
            'fosforoincpip5' => '',
            'fosforobalon1' => '',
            'fosforobalon2' => '',
            'fosforobalon3' => '',
            'fosforobalon4' => '',
            'fosforobalon5' => '',
            'fosforodilinc1' => '',
            'fosforodilinc2' => '',
            'fosforodilinc3' => '',
            'fosforodilinc4' => '',
            'fosforodilinc5' => '',
            'hierrocon1' => '',
            'hierrocon2' => '',
            'hierrocon3' => '',
            'hierrocon4' => '',
            'hierrocon5' => '',
            'hierroinc1' => '',
            'hierroinc2' => '',
            'hierroinc3' => '',
            'hierroinc4' => '',
            'hierroinc5' => '',
            'hierroincrel1' => '',
            'hierroincrel2' => '',
            'hierroincrel3' => '',
            'hierroincrel4' => '',
            'hierroincrel5' => '',
            'hierroAA11' => '',
            'hierroAA12' => '',
            'hierroAA13' => '',
            'hierroAA14' => '',
            'hierroAA15' => '',
            'hierroA21' => '',
            'hierroA22' => '',
            'hierroA23' => '',
            'hierroA24' => '',
            'hierroA25' => '',
            'hierroA31' => '',
            'hierroA32' => '',
            'hierroA33' => '',
            'hierroA34' => '',
            'hierroA35' => '',
            'hierroA41' => '',
            'hierroA42' => '',
            'hierroA43' => '',
            'hierroA44' => '',
            'hierroA45' => '',
            'hierroCC11' => '',
            'hierroCC12' => '',
            'hierroCC13' => '',
            'hierroCC14' => '',
            'hierroCC15' => '',
            'hierroC21' => '',
            'hierroC22' => '',
            'hierroC23' => '',
            'hierroC24' => '',
            'hierroC25' => '',
            'hierroC31' => '',
            'hierroC32' => '',
            'hierroC33' => '',
            'hierroC34' => '',
            'hierroC35' => '',
            'hierroC41' => '',
            'hierroC42' => '',
            'hierroC43' => '',
            'hierroC44' => '',
            'hierroC45' => '',
            'hierrocp1' => '',
            'hierrocp2' => '',
            'hierrocp3' => '',
            'hierrocp4' => '',
            'hierrocp5' => '',
            'hierroa1' => '',
            'hierroa2' => '',
            'hierroa3' => '',
            'hierroa4' => '',
            'hierroa5' => '',
            'hierroa6' => '',
            'hierroa7' => '',
            'hierroa8' => '',
            'hierroa9' => '',
            'hierroa10' => '',
            'hierroa11' => '',
            'hierroa12' => '',
            'hierroa13' => '',
            'hierroa14' => '',
            'hierroa15' => '',
            'hierroa16' => '',
            'hierroa17' => '',
            'hierroa18' => '',
            'hierroa19' => '',
            'hierroa20' => '',
            'hierroc1' => '',
            'hierroc2' => '',
            'hierroc3' => '',
            'hierroc4' => '',
            'hierroc5' => '',
            'hierroc6' => '',
            'hierroc7' => '',
            'hierroc8' => '',
            'hierroc9' => '',
            'hierroc10' => '',
            'hierroc11' => '',
            'hierroc12' => '',
            'hierroc13' => '',
            'hierroc14' => '',
            'hierroc15' => '',
            'hierroc16' => '',
            'hierroc17' => '',
            'hierroc18' => '',
            'hierroc19' => '',
            'hierroc20' => '',
            'hierromaxinc' => '',
            'hierromaxincrel' => '',
            'hierronummedxmuestra' => '',
            'hierronummedxpuncurva' => '',
            'hierronummedcurva' => '',
            'hierroconmuestra1' => '',
            'hierroconmuestra2' => '',
            'hierroprompatcalibracion' => '',
            'hierrodesvestresidual' => '',
            'hierrodesvestcurvacali' => '',
            'hierronp' => '',
            'hierrolote' => '',
            'hierrovence' => '',
            'hierrocc1' => '',
            'hierrocc2' => '',
            'hierroest1' => '',
            'hierroest2' => '',
            'hierroccest1' => '',
            'hierroccest2' => '',
            'hierrovalor' => '',
            'hierroincexp' => '',
            'hierrok' => '',
            'hierroincest' => '',
            'hierroestandar1' => '',
            'hierroestandar2' => '',
            'hierroestandar3' => '',
            'hierroestandar4' => '',
            'hierroestandar5' => '',
            'hierropipeta1' => '',
            'hierropipeta2' => '',
            'hierropipeta3' => '',
            'hierropipeta4' => '',
            'hierropipeta5' => '',
            'hierrovolcorrpipeta1' => '',
            'hierrovolcorrpipeta2' => '',
            'hierrovolcorrpipeta3' => '',
            'hierrovolcorrpipeta4' => '',
            'hierrovolcorrpipeta5' => '',
            'hierroincpip1' => '',
            'hierroincpip2' => '',
            'hierroincpip3' => '',
            'hierroincpip4' => '',
            'hierroincpip5' => '',
            'hierrobalon1' => '',
            'hierrobalon2' => '',
            'hierrobalon3' => '',
            'hierrobalon4' => '',
            'hierrobalon5' => '',
            'hierrodilinc1' => '',
            'hierrodilinc2' => '',
            'hierrodilinc3' => '',
            'hierrodilinc4' => '',
            'hierrodilinc5' => '',
            'magnesiocon1' => '',
            'magnesiocon2' => '',
            'magnesiocon3' => '',
            'magnesiocon4' => '',
            'magnesiocon5' => '',
            'magnesioinc1' => '',
            'magnesioinc2' => '',
            'magnesioinc3' => '',
            'magnesioinc4' => '',
            'magnesioinc5' => '',
            'magnesioincrel1' => '',
            'magnesioincrel2' => '',
            'magnesioincrel3' => '',
            'magnesioincrel4' => '',
            'magnesioincrel5' => '',
            'magnesioAA11' => '',
            'magnesioAA12' => '',
            'magnesioAA13' => '',
            'magnesioAA14' => '',
            'magnesioAA15' => '',
            'magnesioA21' => '',
            'magnesioA22' => '',
            'magnesioA23' => '',
            'magnesioA24' => '',
            'magnesioA25' => '',
            'magnesioA31' => '',
            'magnesioA32' => '',
            'magnesioA33' => '',
            'magnesioA34' => '',
            'magnesioA35' => '',
            'magnesioA41' => '',
            'magnesioA42' => '',
            'magnesioA43' => '',
            'magnesioA44' => '',
            'magnesioA45' => '',
            'magnesioCC11' => '',
            'magnesioCC12' => '',
            'magnesioCC13' => '',
            'magnesioCC14' => '',
            'magnesioCC15' => '',
            'magnesioC21' => '',
            'magnesioC22' => '',
            'magnesioC23' => '',
            'magnesioC24' => '',
            'magnesioC25' => '',
            'magnesioC31' => '',
            'magnesioC32' => '',
            'magnesioC33' => '',
            'magnesioC34' => '',
            'magnesioC35' => '',
            'magnesioC41' => '',
            'magnesioC42' => '',
            'magnesioC43' => '',
            'magnesioC44' => '',
            'magnesioC45' => '',
            'magnesiocp1' => '',
            'magnesiocp2' => '',
            'magnesiocp3' => '',
            'magnesiocp4' => '',
            'magnesiocp5' => '',
            'magnesioa1' => '',
            'magnesioa2' => '',
            'magnesioa3' => '',
            'magnesioa4' => '',
            'magnesioa5' => '',
            'magnesioa6' => '',
            'magnesioa7' => '',
            'magnesioa8' => '',
            'magnesioa9' => '',
            'magnesioa10' => '',
            'magnesioa11' => '',
            'magnesioa12' => '',
            'magnesioa13' => '',
            'magnesioa14' => '',
            'magnesioa15' => '',
            'magnesioa16' => '',
            'magnesioa17' => '',
            'magnesioa18' => '',
            'magnesioa19' => '',
            'magnesioa20' => '',
            'magnesioc1' => '',
            'magnesioc2' => '',
            'magnesioc3' => '',
            'magnesioc4' => '',
            'magnesioc5' => '',
            'magnesioc6' => '',
            'magnesioc7' => '',
            'magnesioc8' => '',
            'magnesioc9' => '',
            'magnesioc10' => '',
            'magnesioc11' => '',
            'magnesioc12' => '',
            'magnesioc13' => '',
            'magnesioc14' => '',
            'magnesioc15' => '',
            'magnesioc16' => '',
            'magnesioc17' => '',
            'magnesioc18' => '',
            'magnesioc19' => '',
            'magnesioc20' => '',
            'magnesiomaxinc' => '',
            'magnesiomaxincrel' => '',
            'magnesionummedxmuestra' => '',
            'magnesionummedxpuncurva' => '',
            'magnesionummedcurva' => '',
            'magnesioconmuestra1' => '',
            'magnesioconmuestra2' => '',
            'magnesioprompatcalibracion' => '',
            'magnesiodesvestresidual' => '',
            'magnesiodesvestcurvacali' => '',
            'magnesionp' => '',
            'magnesiolote' => '',
            'magnesiovence' => '',
            'magnesiocc1' => '',
            'magnesiocc2' => '',
            'magnesioest1' => '',
            'magnesioest2' => '',
            'magnesioccest1' => '',
            'magnesioccest2' => '',
            'magnesiovalor' => '',
            'magnesioincexp' => '',
            'magnesiok' => '',
            'magnesioincest' => '',
            'magnesioestandar1' => '',
            'magnesioestandar2' => '',
            'magnesioestandar3' => '',
            'magnesioestandar4' => '',
            'magnesioestandar5' => '',
            'magnesiopipeta1' => '',
            'magnesiopipeta2' => '',
            'magnesiopipeta3' => '',
            'magnesiopipeta4' => '',
            'magnesiopipeta5' => '',
            'magnesiovolcorrpipeta1' => '',
            'magnesiovolcorrpipeta2' => '',
            'magnesiovolcorrpipeta3' => '',
            'magnesiovolcorrpipeta4' => '',
            'magnesiovolcorrpipeta5' => '',
            'magnesioincpip1' => '',
            'magnesioincpip2' => '',
            'magnesioincpip3' => '',
            'magnesioincpip4' => '',
            'magnesioincpip5' => '',
            'magnesiobalon1' => '',
            'magnesiobalon2' => '',
            'magnesiobalon3' => '',
            'magnesiobalon4' => '',
            'magnesiobalon5' => '',
            'magnesiodilinc1' => '',
            'magnesiodilinc2' => '',
            'magnesiodilinc3' => '',
            'magnesiodilinc4' => '',
            'magnesiodilinc5' => '',
            'manganesocon1' => '',
            'manganesocon2' => '',
            'manganesocon3' => '',
            'manganesocon4' => '',
            'manganesocon5' => '',
            'manganesoinc1' => '',
            'manganesoinc2' => '',
            'manganesoinc3' => '',
            'manganesoinc4' => '',
            'manganesoinc5' => '',
            'manganesoincrel1' => '',
            'manganesoincrel2' => '',
            'manganesoincrel3' => '',
            'manganesoincrel4' => '',
            'manganesoincrel5' => '',
            'manganesoAA11' => '',
            'manganesoAA12' => '',
            'manganesoAA13' => '',
            'manganesoAA14' => '',
            'manganesoAA15' => '',
            'manganesoA21' => '',
            'manganesoA22' => '',
            'manganesoA23' => '',
            'manganesoA24' => '',
            'manganesoA25' => '',
            'manganesoA31' => '',
            'manganesoA32' => '',
            'manganesoA33' => '',
            'manganesoA34' => '',
            'manganesoA35' => '',
            'manganesoA41' => '',
            'manganesoA42' => '',
            'manganesoA43' => '',
            'manganesoA44' => '',
            'manganesoA45' => '',
            'manganesoCC11' => '',
            'manganesoCC12' => '',
            'manganesoCC13' => '',
            'manganesoCC14' => '',
            'manganesoCC15' => '',
            'manganesoC21' => '',
            'manganesoC22' => '',
            'manganesoC23' => '',
            'manganesoC24' => '',
            'manganesoC25' => '',
            'manganesoC31' => '',
            'manganesoC32' => '',
            'manganesoC33' => '',
            'manganesoC34' => '',
            'manganesoC35' => '',
            'manganesoC41' => '',
            'manganesoC42' => '',
            'manganesoC43' => '',
            'manganesoC44' => '',
            'manganesoC45' => '',
            'manganesocp1' => '',
            'manganesocp2' => '',
            'manganesocp3' => '',
            'manganesocp4' => '',
            'manganesocp5' => '',
            'manganesoa1' => '',
            'manganesoa2' => '',
            'manganesoa3' => '',
            'manganesoa4' => '',
            'manganesoa5' => '',
            'manganesoa6' => '',
            'manganesoa7' => '',
            'manganesoa8' => '',
            'manganesoa9' => '',
            'manganesoa10' => '',
            'manganesoa11' => '',
            'manganesoa12' => '',
            'manganesoa13' => '',
            'manganesoa14' => '',
            'manganesoa15' => '',
            'manganesoa16' => '',
            'manganesoa17' => '',
            'manganesoa18' => '',
            'manganesoa19' => '',
            'manganesoa20' => '',
            'manganesoc1' => '',
            'manganesoc2' => '',
            'manganesoc3' => '',
            'manganesoc4' => '',
            'manganesoc5' => '',
            'manganesoc6' => '',
            'manganesoc7' => '',
            'manganesoc8' => '',
            'manganesoc9' => '',
            'manganesoc10' => '',
            'manganesoc11' => '',
            'manganesoc12' => '',
            'manganesoc13' => '',
            'manganesoc14' => '',
            'manganesoc15' => '',
            'manganesoc16' => '',
            'manganesoc17' => '',
            'manganesoc18' => '',
            'manganesoc19' => '',
            'manganesoc20' => '',
            'manganesomaxinc' => '',
            'manganesomaxincrel' => '',
            'manganesonummedxmuestra' => '',
            'manganesonummedxpuncurva' => '',
            'manganesonummedcurva' => '',
            'manganesoconmuestra1' => '',
            'manganesoconmuestra2' => '',
            'manganesoprompatcalibracion' => '',
            'manganesodesvestresidual' => '',
            'manganesodesvestcurvacali' => '',
            'manganesonp' => '',
            'manganesolote' => '',
            'manganesovence' => '',
            'manganesocc1' => '',
            'manganesocc2' => '',
            'manganesoest1' => '',
            'manganesoest2' => '',
            'manganesoccest1' => '',
            'manganesoccest2' => '',
            'manganesovalor' => '',
            'manganesoincexp' => '',
            'manganesok' => '',
            'manganesoincest' => '',
            'manganesoestandar1' => '',
            'manganesoestandar2' => '',
            'manganesoestandar3' => '',
            'manganesoestandar4' => '',
            'manganesoestandar5' => '',
            'manganesopipeta1' => '',
            'manganesopipeta2' => '',
            'manganesopipeta3' => '',
            'manganesopipeta4' => '',
            'manganesopipeta5' => '',
            'manganesovolcorrpipeta1' => '',
            'manganesovolcorrpipeta2' => '',
            'manganesovolcorrpipeta3' => '',
            'manganesovolcorrpipeta4' => '',
            'manganesovolcorrpipeta5' => '',
            'manganesoincpip1' => '',
            'manganesoincpip2' => '',
            'manganesoincpip3' => '',
            'manganesoincpip4' => '',
            'manganesoincpip5' => '',
            'manganesobalon1' => '',
            'manganesobalon2' => '',
            'manganesobalon3' => '',
            'manganesobalon4' => '',
            'manganesobalon5' => '',
            'manganesodilinc1' => '',
            'manganesodilinc2' => '',
            'manganesodilinc3' => '',
            'manganesodilinc4' => '',
            'manganesodilinc5' => '',
            'potasiocon1' => '',
            'potasiocon2' => '',
            'potasiocon3' => '',
            'potasiocon4' => '',
            'potasiocon5' => '',
            'potasioinc1' => '',
            'potasioinc2' => '',
            'potasioinc3' => '',
            'potasioinc4' => '',
            'potasioinc5' => '',
            'potasioincrel1' => '',
            'potasioincrel2' => '',
            'potasioincrel3' => '',
            'potasioincrel4' => '',
            'potasioincrel5' => '',
            'potasioAA11' => '',
            'potasioAA12' => '',
            'potasioAA13' => '',
            'potasioAA14' => '',
            'potasioAA15' => '',
            'potasioA21' => '',
            'potasioA22' => '',
            'potasioA23' => '',
            'potasioA24' => '',
            'potasioA25' => '',
            'potasioA31' => '',
            'potasioA32' => '',
            'potasioA33' => '',
            'potasioA34' => '',
            'potasioA35' => '',
            'potasioA41' => '',
            'potasioA42' => '',
            'potasioA43' => '',
            'potasioA44' => '',
            'potasioA45' => '',
            'potasioCC11' => '',
            'potasioCC12' => '',
            'potasioCC13' => '',
            'potasioCC14' => '',
            'potasioCC15' => '',
            'potasioC21' => '',
            'potasioC22' => '',
            'potasioC23' => '',
            'potasioC24' => '',
            'potasioC25' => '',
            'potasioC31' => '',
            'potasioC32' => '',
            'potasioC33' => '',
            'potasioC34' => '',
            'potasioC35' => '',
            'potasioC41' => '',
            'potasioC42' => '',
            'potasioC43' => '',
            'potasioC44' => '',
            'potasioC45' => '',
            'potasiocp1' => '',
            'potasiocp2' => '',
            'potasiocp3' => '',
            'potasiocp4' => '',
            'potasiocp5' => '',
            'potasioa1' => '',
            'potasioa2' => '',
            'potasioa3' => '',
            'potasioa4' => '',
            'potasioa5' => '',
            'potasioa6' => '',
            'potasioa7' => '',
            'potasioa8' => '',
            'potasioa9' => '',
            'potasioa10' => '',
            'potasioa11' => '',
            'potasioa12' => '',
            'potasioa13' => '',
            'potasioa14' => '',
            'potasioa15' => '',
            'potasioa16' => '',
            'potasioa17' => '',
            'potasioa18' => '',
            'potasioa19' => '',
            'potasioa20' => '',
            'potasioc1' => '',
            'potasioc2' => '',
            'potasioc3' => '',
            'potasioc4' => '',
            'potasioc5' => '',
            'potasioc6' => '',
            'potasioc7' => '',
            'potasioc8' => '',
            'potasioc9' => '',
            'potasioc10' => '',
            'potasioc11' => '',
            'potasioc12' => '',
            'potasioc13' => '',
            'potasioc14' => '',
            'potasioc15' => '',
            'potasioc16' => '',
            'potasioc17' => '',
            'potasioc18' => '',
            'potasioc19' => '',
            'potasioc20' => '',
            'potasiomaxinc' => '',
            'potasiomaxincrel' => '',
            'potasionummedxmuestra' => '',
            'potasionummedxpuncurva' => '',
            'potasionummedcurva' => '',
            'potasioconmuestra1' => '',
            'potasioconmuestra2' => '',
            'potasioprompatcalibracion' => '',
            'potasiodesvestresidual' => '',
            'potasiodesvestcurvacali' => '',
            'potasionp' => '',
            'potasiolote' => '',
            'potasiovence' => '',
            'potasiocc1' => '',
            'potasiocc2' => '',
            'potasioest1' => '',
            'potasioest2' => '',
            'potasioccest1' => '',
            'potasioccest2' => '',
            'potasiovalor' => '',
            'potasioincexp' => '',
            'potasiok' => '',
            'potasioincest' => '',
            'potasioestandar1' => '',
            'potasioestandar2' => '',
            'potasioestandar3' => '',
            'potasioestandar4' => '',
            'potasioestandar5' => '',
            'potasiopipeta1' => '',
            'potasiopipeta2' => '',
            'potasiopipeta3' => '',
            'potasiopipeta4' => '',
            'potasiopipeta5' => '',
            'potasiovolcorrpipeta1' => '',
            'potasiovolcorrpipeta2' => '',
            'potasiovolcorrpipeta3' => '',
            'potasiovolcorrpipeta4' => '',
            'potasiovolcorrpipeta5' => '',
            'potasioincpip1' => '',
            'potasioincpip2' => '',
            'potasioincpip3' => '',
            'potasioincpip4' => '',
            'potasioincpip5' => '',
            'potasiobalon1' => '',
            'potasiobalon2' => '',
            'potasiobalon3' => '',
            'potasiobalon4' => '',
            'potasiobalon5' => '',
            'potasiodilinc1' => '',
            'potasiodilinc2' => '',
            'potasiodilinc3' => '',
            'potasiodilinc4' => '',
            'potasiodilinc5' => '',
            'zinccon1' => '',
            'zinccon2' => '',
            'zinccon3' => '',
            'zinccon4' => '',
            'zinccon5' => '',
            'zincinc1' => '',
            'zincinc2' => '',
            'zincinc3' => '',
            'zincinc4' => '',
            'zincinc5' => '',
            'zincincrel1' => '',
            'zincincrel2' => '',
            'zincincrel3' => '',
            'zincincrel4' => '',
            'zincincrel5' => '',
            'zincAA11' => '',
            'zincAA12' => '',
            'zincAA13' => '',
            'zincAA14' => '',
            'zincAA15' => '',
            'zincA21' => '',
            'zincA22' => '',
            'zincA23' => '',
            'zincA24' => '',
            'zincA25' => '',
            'zincA31' => '',
            'zincA32' => '',
            'zincA33' => '',
            'zincA34' => '',
            'zincA35' => '',
            'zincA41' => '',
            'zincA42' => '',
            'zincA43' => '',
            'zincA44' => '',
            'zincA45' => '',
            'zincCC11' => '',
            'zincCC12' => '',
            'zincCC13' => '',
            'zincCC14' => '',
            'zincCC15' => '',
            'zincC21' => '',
            'zincC22' => '',
            'zincC23' => '',
            'zincC24' => '',
            'zincC25' => '',
            'zincC31' => '',
            'zincC32' => '',
            'zincC33' => '',
            'zincC34' => '',
            'zincC35' => '',
            'zincC41' => '',
            'zincC42' => '',
            'zincC43' => '',
            'zincC44' => '',
            'zincC45' => '',
            'zinccp1' => '',
            'zinccp2' => '',
            'zinccp3' => '',
            'zinccp4' => '',
            'zinccp5' => '',
            'zinca1' => '',
            'zinca2' => '',
            'zinca3' => '',
            'zinca4' => '',
            'zinca5' => '',
            'zinca6' => '',
            'zinca7' => '',
            'zinca8' => '',
            'zinca9' => '',
            'zinca10' => '',
            'zinca11' => '',
            'zinca12' => '',
            'zinca13' => '',
            'zinca14' => '',
            'zinca15' => '',
            'zinca16' => '',
            'zinca17' => '',
            'zinca18' => '',
            'zinca19' => '',
            'zinca20' => '',
            'zincc1' => '',
            'zincc2' => '',
            'zincc3' => '',
            'zincc4' => '',
            'zincc5' => '',
            'zincc6' => '',
            'zincc7' => '',
            'zincc8' => '',
            'zincc9' => '',
            'zincc10' => '',
            'zincc11' => '',
            'zincc12' => '',
            'zincc13' => '',
            'zincc14' => '',
            'zincc15' => '',
            'zincc16' => '',
            'zincc17' => '',
            'zincc18' => '',
            'zincc19' => '',
            'zincc20' => '',
            'zincmaxinc' => '',
            'zincmaxincrel' => '',
            'zincnummedxmuestra' => '',
            'zincnummedxpuncurva' => '',
            'zincnummedcurva' => '',
            'zincconmuestra1' => '',
            'zincconmuestra2' => '',
            'zincprompatcalibracion' => '',
            'zincdesvestresidual' => '',
            'zincdesvestcurvacali' => '',
            'zincnp' => '',
            'zinclote' => '',
            'zincvence' => '',
            'zinccc1' => '',
            'zinccc2' => '',
            'zincest1' => '',
            'zincest2' => '',
            'zincccest1' => '',
            'zincccest2' => '',
            'zincvalor' => '',
            'zincincexp' => '',
            'zinck' => '',
            'zincincest' => '',
            'zincestandar1' => '',
            'zincestandar2' => '',
            'zincestandar3' => '',
            'zincestandar4' => '',
            'zincestandar5' => '',
            'zincpipeta1' => '',
            'zincpipeta2' => '',
            'zincpipeta3' => '',
            'zincpipeta4' => '',
            'zincpipeta5' => '',
            'zincvolcorrpipeta1' => '',
            'zincvolcorrpipeta2' => '',
            'zincvolcorrpipeta3' => '',
            'zincvolcorrpipeta4' => '',
            'zincvolcorrpipeta5' => '',
            'zincincpip1' => '',
            'zincincpip2' => '',
            'zincincpip3' => '',
            'zincincpip4' => '',
            'zincincpip5' => '',
            'zincbalon1' => '',
            'zincbalon2' => '',
            'zincbalon3' => '',
            'zincbalon4' => '',
            'zincbalon5' => '',
            'zincdilinc1' => '',
            'zincdilinc2' => '',
            'zincdilinc3' => '',
            'zincdilinc4' => '',
            'zincdilinc5' => '',
            'nitrogenocon1' => '',
            'nitrogenocon2' => '',
            'nitrogenocon3' => '',
            'nitrogenocon4' => '',
            'nitrogenocon5' => '',
            'nitrogenoinc1' => '',
            'nitrogenoinc2' => '',
            'nitrogenoinc3' => '',
            'nitrogenoinc4' => '',
            'nitrogenoinc5' => '',
            'nitrogenoincrel1' => '',
            'nitrogenoincrel2' => '',
            'nitrogenoincrel3' => '',
            'nitrogenoincrel4' => '',
            'nitrogenoincrel5' => '',
            'nitrogenoAA11' => '',
            'nitrogenoAA12' => '',
            'nitrogenoAA13' => '',
            'nitrogenoAA14' => '',
            'nitrogenoAA15' => '',
            'nitrogenoA21' => '',
            'nitrogenoA22' => '',
            'nitrogenoA23' => '',
            'nitrogenoA24' => '',
            'nitrogenoA25' => '',
            'nitrogenoA31' => '',
            'nitrogenoA32' => '',
            'nitrogenoA33' => '',
            'nitrogenoA34' => '',
            'nitrogenoA35' => '',
            'nitrogenoA41' => '',
            'nitrogenoA42' => '',
            'nitrogenoA43' => '',
            'nitrogenoA44' => '',
            'nitrogenoA45' => '',
            'nitrogenoCC11' => '',
            'nitrogenoCC12' => '',
            'nitrogenoCC13' => '',
            'nitrogenoCC14' => '',
            'nitrogenoCC15' => '',
            'nitrogenoC21' => '',
            'nitrogenoC22' => '',
            'nitrogenoC23' => '',
            'nitrogenoC24' => '',
            'nitrogenoC25' => '',
            'nitrogenoC31' => '',
            'nitrogenoC32' => '',
            'nitrogenoC33' => '',
            'nitrogenoC34' => '',
            'nitrogenoC35' => '',
            'nitrogenoC41' => '',
            'nitrogenoC42' => '',
            'nitrogenoC43' => '',
            'nitrogenoC44' => '',
            'nitrogenoC45' => '',
            'nitrogenocp1' => '',
            'nitrogenocp2' => '',
            'nitrogenocp3' => '',
            'nitrogenocp4' => '',
            'nitrogenocp5' => '',
            'nitrogenoa1' => '',
            'nitrogenoa2' => '',
            'nitrogenoa3' => '',
            'nitrogenoa4' => '',
            'nitrogenoa5' => '',
            'nitrogenoa6' => '',
            'nitrogenoa7' => '',
            'nitrogenoa8' => '',
            'nitrogenoa9' => '',
            'nitrogenoa10' => '',
            'nitrogenoa11' => '',
            'nitrogenoa12' => '',
            'nitrogenoa13' => '',
            'nitrogenoa14' => '',
            'nitrogenoa15' => '',
            'nitrogenoa16' => '',
            'nitrogenoa17' => '',
            'nitrogenoa18' => '',
            'nitrogenoa19' => '',
            'nitrogenoa20' => '',
            'nitrogenoc1' => '',
            'nitrogenoc2' => '',
            'nitrogenoc3' => '',
            'nitrogenoc4' => '',
            'nitrogenoc5' => '',
            'nitrogenoc6' => '',
            'nitrogenoc7' => '',
            'nitrogenoc8' => '',
            'nitrogenoc9' => '',
            'nitrogenoc10' => '',
            'nitrogenoc11' => '',
            'nitrogenoc12' => '',
            'nitrogenoc13' => '',
            'nitrogenoc14' => '',
            'nitrogenoc15' => '',
            'nitrogenoc16' => '',
            'nitrogenoc17' => '',
            'nitrogenoc18' => '',
            'nitrogenoc19' => '',
            'nitrogenoc20' => '',
            'nitrogenomaxinc' => '',
            'nitrogenomaxincrel' => '',
            'nitrogenonummedxmuestra' => '',
            'nitrogenonummedxpuncurva' => '',
            'nitrogenonummedcurva' => '',
            'nitrogenoconmuestra1' => '',
            'nitrogenoconmuestra2' => '',
            'nitrogenoprompatcalibracion' => '',
            'nitrogenodesvestresidual' => '',
            'nitrogenodesvestcurvacali' => '',
            'nitrogenonp' => '',
            'nitrogenolote' => '',
            'nitrogenovence' => '',
            'nitrogenocc1' => '',
            'nitrogenocc2' => '',
            'nitrogenoest1' => '',
            'nitrogenoest2' => '',
            'nitrogenoccest1' => '',
            'nitrogenoccest2' => '',
            'nitrogenovalor' => '',
            'nitrogenoincexp' => '',
            'nitrogenok' => '',
            'nitrogenoincest' => '',
            'nitrogenoestandar1' => '',
            'nitrogenoestandar2' => '',
            'nitrogenoestandar3' => '',
            'nitrogenoestandar4' => '',
            'nitrogenoestandar5' => '',
            'nitrogenopipeta1' => '',
            'nitrogenopipeta2' => '',
            'nitrogenopipeta3' => '',
            'nitrogenopipeta4' => '',
            'nitrogenopipeta5' => '',
            'nitrogenovolcorrpipeta1' => '',
            'nitrogenovolcorrpipeta2' => '',
            'nitrogenovolcorrpipeta3' => '',
            'nitrogenovolcorrpipeta4' => '',
            'nitrogenovolcorrpipeta5' => '',
            'nitrogenoincpip1' => '',
            'nitrogenoincpip2' => '',
            'nitrogenoincpip3' => '',
            'nitrogenoincpip4' => '',
            'nitrogenoincpip5' => '',
            'nitrogenobalon1' => '',
            'nitrogenobalon2' => '',
            'nitrogenobalon3' => '',
            'nitrogenobalon4' => '',
            'nitrogenobalon5' => '',
            'nitrogenodilinc1' => '',
            'nitrogenodilinc2' => '',
            'nitrogenodilinc3' => '',
            'nitrogenodilinc4' => '',
            'nitrogenodilinc5' => '',
            'azufrecon1' => '',
            'azufrecon2' => '',
            'azufrecon3' => '',
            'azufrecon4' => '',
            'azufrecon5' => '',
            'azufreinc1' => '',
            'azufreinc2' => '',
            'azufreinc3' => '',
            'azufreinc4' => '',
            'azufreinc5' => '',
            'azufreincrel1' => '',
            'azufreincrel2' => '',
            'azufreincrel3' => '',
            'azufreincrel4' => '',
            'azufreincrel5' => '',
            'azufreAA11' => '',
            'azufreAA12' => '',
            'azufreAA13' => '',
            'azufreAA14' => '',
            'azufreAA15' => '',
            'azufreA21' => '',
            'azufreA22' => '',
            'azufreA23' => '',
            'azufreA24' => '',
            'azufreA25' => '',
            'azufreA31' => '',
            'azufreA32' => '',
            'azufreA33' => '',
            'azufreA34' => '',
            'azufreA35' => '',
            'azufreA41' => '',
            'azufreA42' => '',
            'azufreA43' => '',
            'azufreA44' => '',
            'azufreA45' => '',
            'azufreCC11' => '',
            'azufreCC12' => '',
            'azufreCC13' => '',
            'azufreCC14' => '',
            'azufreCC15' => '',
            'azufreC21' => '',
            'azufreC22' => '',
            'azufreC23' => '',
            'azufreC24' => '',
            'azufreC25' => '',
            'azufreC31' => '',
            'azufreC32' => '',
            'azufreC33' => '',
            'azufreC34' => '',
            'azufreC35' => '',
            'azufreC41' => '',
            'azufreC42' => '',
            'azufreC43' => '',
            'azufreC44' => '',
            'azufreC45' => '',
            'azufrecp1' => '',
            'azufrecp2' => '',
            'azufrecp3' => '',
            'azufrecp4' => '',
            'azufrecp5' => '',
            'azufrea1' => '',
            'azufrea2' => '',
            'azufrea3' => '',
            'azufrea4' => '',
            'azufrea5' => '',
            'azufrea6' => '',
            'azufrea7' => '',
            'azufrea8' => '',
            'azufrea9' => '',
            'azufrea10' => '',
            'azufrea11' => '',
            'azufrea12' => '',
            'azufrea13' => '',
            'azufrea14' => '',
            'azufrea15' => '',
            'azufrea16' => '',
            'azufrea17' => '',
            'azufrea18' => '',
            'azufrea19' => '',
            'azufrea20' => '',
            'azufrec1' => '',
            'azufrec2' => '',
            'azufrec3' => '',
            'azufrec4' => '',
            'azufrec5' => '',
            'azufrec6' => '',
            'azufrec7' => '',
            'azufrec8' => '',
            'azufrec9' => '',
            'azufrec10' => '',
            'azufrec11' => '',
            'azufrec12' => '',
            'azufrec13' => '',
            'azufrec14' => '',
            'azufrec15' => '',
            'azufrec16' => '',
            'azufrec17' => '',
            'azufrec18' => '',
            'azufrec19' => '',
            'azufrec20' => '',
            'azufremaxinc' => '',
            'azufremaxincrel' => '',
            'azufrenummedxmuestra' => '',
            'azufrenummedxpuncurva' => '',
            'azufrenummedcurva' => '',
            'azufreconmuestra1' => '',
            'azufreconmuestra2' => '',
            'azufreprompatcalibracion' => '',
            'azufredesvestresidual' => '',
            'azufredesvestcurvacali' => '',
            'azufrenp' => '',
            'azufrelote' => '',
            'azufrevence' => '',
            'azufrecc1' => '',
            'azufrecc2' => '',
            'azufreest1' => '',
            'azufreest2' => '',
            'azufreccest1' => '',
            'azufreccest2' => '',
            'azufrevalor' => '',
            'azufreincexp' => '',
            'azufrek' => '',
            'azufreincest' => '',
            'azufreestandar1' => '',
            'azufreestandar2' => '',
            'azufreestandar3' => '',
            'azufreestandar4' => '',
            'azufreestandar5' => '',
            'azufrepipeta1' => '',
            'azufrepipeta2' => '',
            'azufrepipeta3' => '',
            'azufrepipeta4' => '',
            'azufrepipeta5' => '',
            'azufrevolcorrpipeta1' => '',
            'azufrevolcorrpipeta2' => '',
            'azufrevolcorrpipeta3' => '',
            'azufrevolcorrpipeta4' => '',
            'azufrevolcorrpipeta5' => '',
            'azufreincpip1' => '',
            'azufreincpip2' => '',
            'azufreincpip3' => '',
            'azufreincpip4' => '',
            'azufreincpip5' => '',
            'azufrebalon1' => '',
            'azufrebalon2' => '',
            'azufrebalon3' => '',
            'azufrebalon4' => '',
            'azufrebalon5' => '',
            'azufredilinc1' => '',
            'azufredilinc2' => '',
            'azufredilinc3' => '',
            'azufredilinc4' => '',
            'azufredilinc5' => '',
            'arsenicocon1' => '',
            'arsenicocon2' => '',
            'arsenicocon3' => '',
            'arsenicocon4' => '',
            'arsenicocon5' => '',
            'arsenicoinc1' => '',
            'arsenicoinc2' => '',
            'arsenicoinc3' => '',
            'arsenicoinc4' => '',
            'arsenicoinc5' => '',
            'arsenicoincrel1' => '',
            'arsenicoincrel2' => '',
            'arsenicoincrel3' => '',
            'arsenicoincrel4' => '',
            'arsenicoincrel5' => '',
            'arsenicoAA11' => '',
            'arsenicoAA12' => '',
            'arsenicoAA13' => '',
            'arsenicoAA14' => '',
            'arsenicoAA15' => '',
            'arsenicoA21' => '',
            'arsenicoA22' => '',
            'arsenicoA23' => '',
            'arsenicoA24' => '',
            'arsenicoA25' => '',
            'arsenicoA31' => '',
            'arsenicoA32' => '',
            'arsenicoA33' => '',
            'arsenicoA34' => '',
            'arsenicoA35' => '',
            'arsenicoA41' => '',
            'arsenicoA42' => '',
            'arsenicoA43' => '',
            'arsenicoA44' => '',
            'arsenicoA45' => '',
            'arsenicoCC11' => '',
            'arsenicoCC12' => '',
            'arsenicoCC13' => '',
            'arsenicoCC14' => '',
            'arsenicoCC15' => '',
            'arsenicoC21' => '',
            'arsenicoC22' => '',
            'arsenicoC23' => '',
            'arsenicoC24' => '',
            'arsenicoC25' => '',
            'arsenicoC31' => '',
            'arsenicoC32' => '',
            'arsenicoC33' => '',
            'arsenicoC34' => '',
            'arsenicoC35' => '',
            'arsenicoC41' => '',
            'arsenicoC42' => '',
            'arsenicoC43' => '',
            'arsenicoC44' => '',
            'arsenicoC45' => '',
            'arsenicocp1' => '',
            'arsenicocp2' => '',
            'arsenicocp3' => '',
            'arsenicocp4' => '',
            'arsenicocp5' => '',
            'arsenicoa1' => '',
            'arsenicoa2' => '',
            'arsenicoa3' => '',
            'arsenicoa4' => '',
            'arsenicoa5' => '',
            'arsenicoa6' => '',
            'arsenicoa7' => '',
            'arsenicoa8' => '',
            'arsenicoa9' => '',
            'arsenicoa10' => '',
            'arsenicoa11' => '',
            'arsenicoa12' => '',
            'arsenicoa13' => '',
            'arsenicoa14' => '',
            'arsenicoa15' => '',
            'arsenicoa16' => '',
            'arsenicoa17' => '',
            'arsenicoa18' => '',
            'arsenicoa19' => '',
            'arsenicoa20' => '',
            'arsenicoc1' => '',
            'arsenicoc2' => '',
            'arsenicoc3' => '',
            'arsenicoc4' => '',
            'arsenicoc5' => '',
            'arsenicoc6' => '',
            'arsenicoc7' => '',
            'arsenicoc8' => '',
            'arsenicoc9' => '',
            'arsenicoc10' => '',
            'arsenicoc11' => '',
            'arsenicoc12' => '',
            'arsenicoc13' => '',
            'arsenicoc14' => '',
            'arsenicoc15' => '',
            'arsenicoc16' => '',
            'arsenicoc17' => '',
            'arsenicoc18' => '',
            'arsenicoc19' => '',
            'arsenicoc20' => '',
            'arsenicomaxinc' => '',
            'arsenicomaxincrel' => '',
            'arseniconummedxmuestra' => '',
            'arseniconummedxpuncurva' => '',
            'arseniconummedcurva' => '',
            'arsenicoconmuestra1' => '',
            'arsenicoconmuestra2' => '',
            'arsenicoprompatcalibracion' => '',
            'arsenicodesvestresidual' => '',
            'arsenicodesvestcurvacali' => '',
            'arseniconp' => '',
            'arsenicolote' => '',
            'arsenicovence' => '',
            'arsenicocc1' => '',
            'arsenicocc2' => '',
            'arsenicoest1' => '',
            'arsenicoest2' => '',
            'arsenicoccest1' => '',
            'arsenicoccest2' => '',
            'arsenicovalor' => '',
            'arsenicoincexp' => '',
            'arsenicok' => '',
            'arsenicoincest' => '',
            'arsenicoestandar1' => '',
            'arsenicoestandar2' => '',
            'arsenicoestandar3' => '',
            'arsenicoestandar4' => '',
            'arsenicoestandar5' => '',
            'arsenicopipeta1' => '',
            'arsenicopipeta2' => '',
            'arsenicopipeta3' => '',
            'arsenicopipeta4' => '',
            'arsenicopipeta5' => '',
            'arsenicovolcorrpipeta1' => '',
            'arsenicovolcorrpipeta2' => '',
            'arsenicovolcorrpipeta3' => '',
            'arsenicovolcorrpipeta4' => '',
            'arsenicovolcorrpipeta5' => '',
            'arsenicoincpip1' => '',
            'arsenicoincpip2' => '',
            'arsenicoincpip3' => '',
            'arsenicoincpip4' => '',
            'arsenicoincpip5' => '',
            'arsenicobalon1' => '',
            'arsenicobalon2' => '',
            'arsenicobalon3' => '',
            'arsenicobalon4' => '',
            'arsenicobalon5' => '',
            'arsenicodilinc1' => '',
            'arsenicodilinc2' => '',
            'arsenicodilinc3' => '',
            'arsenicodilinc4' => '',
            'arsenicodilinc5' => '',
            'cadmiocon1' => '',
            'cadmiocon2' => '',
            'cadmiocon3' => '',
            'cadmiocon4' => '',
            'cadmiocon5' => '',
            'cadmioinc1' => '',
            'cadmioinc2' => '',
            'cadmioinc3' => '',
            'cadmioinc4' => '',
            'cadmioinc5' => '',
            'cadmioincrel1' => '',
            'cadmioincrel2' => '',
            'cadmioincrel3' => '',
            'cadmioincrel4' => '',
            'cadmioincrel5' => '',
            'cadmioAA11' => '',
            'cadmioAA12' => '',
            'cadmioAA13' => '',
            'cadmioAA14' => '',
            'cadmioAA15' => '',
            'cadmioA21' => '',
            'cadmioA22' => '',
            'cadmioA23' => '',
            'cadmioA24' => '',
            'cadmioA25' => '',
            'cadmioA31' => '',
            'cadmioA32' => '',
            'cadmioA33' => '',
            'cadmioA34' => '',
            'cadmioA35' => '',
            'cadmioA41' => '',
            'cadmioA42' => '',
            'cadmioA43' => '',
            'cadmioA44' => '',
            'cadmioA45' => '',
            'cadmioCC11' => '',
            'cadmioCC12' => '',
            'cadmioCC13' => '',
            'cadmioCC14' => '',
            'cadmioCC15' => '',
            'cadmioC21' => '',
            'cadmioC22' => '',
            'cadmioC23' => '',
            'cadmioC24' => '',
            'cadmioC25' => '',
            'cadmioC31' => '',
            'cadmioC32' => '',
            'cadmioC33' => '',
            'cadmioC34' => '',
            'cadmioC35' => '',
            'cadmioC41' => '',
            'cadmioC42' => '',
            'cadmioC43' => '',
            'cadmioC44' => '',
            'cadmioC45' => '',
            'cadmiocp1' => '',
            'cadmiocp2' => '',
            'cadmiocp3' => '',
            'cadmiocp4' => '',
            'cadmiocp5' => '',
            'cadmioa1' => '',
            'cadmioa2' => '',
            'cadmioa3' => '',
            'cadmioa4' => '',
            'cadmioa5' => '',
            'cadmioa6' => '',
            'cadmioa7' => '',
            'cadmioa8' => '',
            'cadmioa9' => '',
            'cadmioa10' => '',
            'cadmioa11' => '',
            'cadmioa12' => '',
            'cadmioa13' => '',
            'cadmioa14' => '',
            'cadmioa15' => '',
            'cadmioa16' => '',
            'cadmioa17' => '',
            'cadmioa18' => '',
            'cadmioa19' => '',
            'cadmioa20' => '',
            'cadmioc1' => '',
            'cadmioc2' => '',
            'cadmioc3' => '',
            'cadmioc4' => '',
            'cadmioc5' => '',
            'cadmioc6' => '',
            'cadmioc7' => '',
            'cadmioc8' => '',
            'cadmioc9' => '',
            'cadmioc10' => '',
            'cadmioc11' => '',
            'cadmioc12' => '',
            'cadmioc13' => '',
            'cadmioc14' => '',
            'cadmioc15' => '',
            'cadmioc16' => '',
            'cadmioc17' => '',
            'cadmioc18' => '',
            'cadmioc19' => '',
            'cadmioc20' => '',
            'cadmiomaxinc' => '',
            'cadmiomaxincrel' => '',
            'cadmionummedxmuestra' => '',
            'cadmionummedxpuncurva' => '',
            'cadmionummedcurva' => '',
            'cadmioconmuestra1' => '',
            'cadmioconmuestra2' => '',
            'cadmioprompatcalibracion' => '',
            'cadmiodesvestresidual' => '',
            'cadmiodesvestcurvacali' => '',
            'cadmionp' => '',
            'cadmiolote' => '',
            'cadmiovence' => '',
            'cadmiocc1' => '',
            'cadmiocc2' => '',
            'cadmioest1' => '',
            'cadmioest2' => '',
            'cadmioccest1' => '',
            'cadmioccest2' => '',
            'cadmiovalor' => '',
            'cadmioincexp' => '',
            'cadmiok' => '',
            'cadmioincest' => '',
            'cadmioestandar1' => '',
            'cadmioestandar2' => '',
            'cadmioestandar3' => '',
            'cadmioestandar4' => '',
            'cadmioestandar5' => '',
            'cadmiopipeta1' => '',
            'cadmiopipeta2' => '',
            'cadmiopipeta3' => '',
            'cadmiopipeta4' => '',
            'cadmiopipeta5' => '',
            'cadmiovolcorrpipeta1' => '',
            'cadmiovolcorrpipeta2' => '',
            'cadmiovolcorrpipeta3' => '',
            'cadmiovolcorrpipeta4' => '',
            'cadmiovolcorrpipeta5' => '',
            'cadmioincpip1' => '',
            'cadmioincpip2' => '',
            'cadmioincpip3' => '',
            'cadmioincpip4' => '',
            'cadmioincpip5' => '',
            'cadmiobalon1' => '',
            'cadmiobalon2' => '',
            'cadmiobalon3' => '',
            'cadmiobalon4' => '',
            'cadmiobalon5' => '',
            'cadmiodilinc1' => '',
            'cadmiodilinc2' => '',
            'cadmiodilinc3' => '',
            'cadmiodilinc4' => '',
            'cadmiodilinc5' => '',
            'cromocon1' => '',
            'cromocon2' => '',
            'cromocon3' => '',
            'cromocon4' => '',
            'cromocon5' => '',
            'cromoinc1' => '',
            'cromoinc2' => '',
            'cromoinc3' => '',
            'cromoinc4' => '',
            'cromoinc5' => '',
            'cromoincrel1' => '',
            'cromoincrel2' => '',
            'cromoincrel3' => '',
            'cromoincrel4' => '',
            'cromoincrel5' => '',
            'cromoAA11' => '',
            'cromoAA12' => '',
            'cromoAA13' => '',
            'cromoAA14' => '',
            'cromoAA15' => '',
            'cromoA21' => '',
            'cromoA22' => '',
            'cromoA23' => '',
            'cromoA24' => '',
            'cromoA25' => '',
            'cromoA31' => '',
            'cromoA32' => '',
            'cromoA33' => '',
            'cromoA34' => '',
            'cromoA35' => '',
            'cromoA41' => '',
            'cromoA42' => '',
            'cromoA43' => '',
            'cromoA44' => '',
            'cromoA45' => '',
            'cromoCC11' => '',
            'cromoCC12' => '',
            'cromoCC13' => '',
            'cromoCC14' => '',
            'cromoCC15' => '',
            'cromoC21' => '',
            'cromoC22' => '',
            'cromoC23' => '',
            'cromoC24' => '',
            'cromoC25' => '',
            'cromoC31' => '',
            'cromoC32' => '',
            'cromoC33' => '',
            'cromoC34' => '',
            'cromoC35' => '',
            'cromoC41' => '',
            'cromoC42' => '',
            'cromoC43' => '',
            'cromoC44' => '',
            'cromoC45' => '',
            'cromocp1' => '',
            'cromocp2' => '',
            'cromocp3' => '',
            'cromocp4' => '',
            'cromocp5' => '',
            'cromoa1' => '',
            'cromoa2' => '',
            'cromoa3' => '',
            'cromoa4' => '',
            'cromoa5' => '',
            'cromoa6' => '',
            'cromoa7' => '',
            'cromoa8' => '',
            'cromoa9' => '',
            'cromoa10' => '',
            'cromoa11' => '',
            'cromoa12' => '',
            'cromoa13' => '',
            'cromoa14' => '',
            'cromoa15' => '',
            'cromoa16' => '',
            'cromoa17' => '',
            'cromoa18' => '',
            'cromoa19' => '',
            'cromoa20' => '',
            'cromoc1' => '',
            'cromoc2' => '',
            'cromoc3' => '',
            'cromoc4' => '',
            'cromoc5' => '',
            'cromoc6' => '',
            'cromoc7' => '',
            'cromoc8' => '',
            'cromoc9' => '',
            'cromoc10' => '',
            'cromoc11' => '',
            'cromoc12' => '',
            'cromoc13' => '',
            'cromoc14' => '',
            'cromoc15' => '',
            'cromoc16' => '',
            'cromoc17' => '',
            'cromoc18' => '',
            'cromoc19' => '',
            'cromoc20' => '',
            'cromomaxinc' => '',
            'cromomaxincrel' => '',
            'cromonummedxmuestra' => '',
            'cromonummedxpuncurva' => '',
            'cromonummedcurva' => '',
            'cromoconmuestra1' => '',
            'cromoconmuestra2' => '',
            'cromoprompatcalibracion' => '',
            'cromodesvestresidual' => '',
            'cromodesvestcurvacali' => '',
            'cromonp' => '',
            'cromolote' => '',
            'cromovence' => '',
            'cromocc1' => '',
            'cromocc2' => '',
            'cromoest1' => '',
            'cromoest2' => '',
            'cromoccest1' => '',
            'cromoccest2' => '',
            'cromovalor' => '',
            'cromoincexp' => '',
            'cromok' => '',
            'cromoincest' => '',
            'cromoestandar1' => '',
            'cromoestandar2' => '',
            'cromoestandar3' => '',
            'cromoestandar4' => '',
            'cromoestandar5' => '',
            'cromopipeta1' => '',
            'cromopipeta2' => '',
            'cromopipeta3' => '',
            'cromopipeta4' => '',
            'cromopipeta5' => '',
            'cromovolcorrpipeta1' => '',
            'cromovolcorrpipeta2' => '',
            'cromovolcorrpipeta3' => '',
            'cromovolcorrpipeta4' => '',
            'cromovolcorrpipeta5' => '',
            'cromoincpip1' => '',
            'cromoincpip2' => '',
            'cromoincpip3' => '',
            'cromoincpip4' => '',
            'cromoincpip5' => '',
            'cromobalon1' => '',
            'cromobalon2' => '',
            'cromobalon3' => '',
            'cromobalon4' => '',
            'cromobalon5' => '',
            'cromodilinc1' => '',
            'cromodilinc2' => '',
            'cromodilinc3' => '',
            'cromodilinc4' => '',
            'cromodilinc5' => '',
            'plomocon1' => '',
            'plomocon2' => '',
            'plomocon3' => '',
            'plomocon4' => '',
            'plomocon5' => '',
            'plomoinc1' => '',
            'plomoinc2' => '',
            'plomoinc3' => '',
            'plomoinc4' => '',
            'plomoinc5' => '',
            'plomoincrel1' => '',
            'plomoincrel2' => '',
            'plomoincrel3' => '',
            'plomoincrel4' => '',
            'plomoincrel5' => '',
            'plomoAA11' => '',
            'plomoAA12' => '',
            'plomoAA13' => '',
            'plomoAA14' => '',
            'plomoAA15' => '',
            'plomoA21' => '',
            'plomoA22' => '',
            'plomoA23' => '',
            'plomoA24' => '',
            'plomoA25' => '',
            'plomoA31' => '',
            'plomoA32' => '',
            'plomoA33' => '',
            'plomoA34' => '',
            'plomoA35' => '',
            'plomoA41' => '',
            'plomoA42' => '',
            'plomoA43' => '',
            'plomoA44' => '',
            'plomoA45' => '',
            'plomoCC11' => '',
            'plomoCC12' => '',
            'plomoCC13' => '',
            'plomoCC14' => '',
            'plomoCC15' => '',
            'plomoC21' => '',
            'plomoC22' => '',
            'plomoC23' => '',
            'plomoC24' => '',
            'plomoC25' => '',
            'plomoC31' => '',
            'plomoC32' => '',
            'plomoC33' => '',
            'plomoC34' => '',
            'plomoC35' => '',
            'plomoC41' => '',
            'plomoC42' => '',
            'plomoC43' => '',
            'plomoC44' => '',
            'plomoC45' => '',
            'plomocp1' => '',
            'plomocp2' => '',
            'plomocp3' => '',
            'plomocp4' => '',
            'plomocp5' => '',
            'plomoa1' => '',
            'plomoa2' => '',
            'plomoa3' => '',
            'plomoa4' => '',
            'plomoa5' => '',
            'plomoa6' => '',
            'plomoa7' => '',
            'plomoa8' => '',
            'plomoa9' => '',
            'plomoa10' => '',
            'plomoa11' => '',
            'plomoa12' => '',
            'plomoa13' => '',
            'plomoa14' => '',
            'plomoa15' => '',
            'plomoa16' => '',
            'plomoa17' => '',
            'plomoa18' => '',
            'plomoa19' => '',
            'plomoa20' => '',
            'plomoc1' => '',
            'plomoc2' => '',
            'plomoc3' => '',
            'plomoc4' => '',
            'plomoc5' => '',
            'plomoc6' => '',
            'plomoc7' => '',
            'plomoc8' => '',
            'plomoc9' => '',
            'plomoc10' => '',
            'plomoc11' => '',
            'plomoc12' => '',
            'plomoc13' => '',
            'plomoc14' => '',
            'plomoc15' => '',
            'plomoc16' => '',
            'plomoc17' => '',
            'plomoc18' => '',
            'plomoc19' => '',
            'plomoc20' => '',
            'plomomaxinc' => '',
            'plomomaxincrel' => '',
            'plomonummedxmuestra' => '',
            'plomonummedxpuncurva' => '',
            'plomonummedcurva' => '',
            'plomoconmuestra1' => '',
            'plomoconmuestra2' => '',
            'plomoprompatcalibracion' => '',
            'plomodesvestresidual' => '',
            'plomodesvestcurvacali' => '',
            'plomonp' => '',
            'plomolote' => '',
            'plomovence' => '',
            'plomocc1' => '',
            'plomocc2' => '',
            'plomoest1' => '',
            'plomoest2' => '',
            'plomoccest1' => '',
            'plomoccest2' => '',
            'plomovalor' => '',
            'plomoincexp' => '',
            'plomok' => '',
            'plomoincest' => '',
            'plomoestandar1' => '',
            'plomoestandar2' => '',
            'plomoestandar3' => '',
            'plomoestandar4' => '',
            'plomoestandar5' => '',
            'plomopipeta1' => '',
            'plomopipeta2' => '',
            'plomopipeta3' => '',
            'plomopipeta4' => '',
            'plomopipeta5' => '',
            'plomovolcorrpipeta1' => '',
            'plomovolcorrpipeta2' => '',
            'plomovolcorrpipeta3' => '',
            'plomovolcorrpipeta4' => '',
            'plomovolcorrpipeta5' => '',
            'plomoincpip1' => '',
            'plomoincpip2' => '',
            'plomoincpip3' => '',
            'plomoincpip4' => '',
            'plomoincpip5' => '',
            'plomobalon1' => '',
            'plomobalon2' => '',
            'plomobalon3' => '',
            'plomobalon4' => '',
            'plomobalon5' => '',
            'plomodilinc1' => '',
            'plomodilinc2' => '',
            'plomodilinc3' => '',
            'plomodilinc4' => '',
            'plomodilinc5' => '',
            'mercuriocon1' => '',
            'mercuriocon2' => '',
            'mercuriocon3' => '',
            'mercuriocon4' => '',
            'mercuriocon5' => '',
            'mercurioinc1' => '',
            'mercurioinc2' => '',
            'mercurioinc3' => '',
            'mercurioinc4' => '',
            'mercurioinc5' => '',
            'mercurioincrel1' => '',
            'mercurioincrel2' => '',
            'mercurioincrel3' => '',
            'mercurioincrel4' => '',
            'mercurioincrel5' => '',
            'mercurioAA11' => '',
            'mercurioAA12' => '',
            'mercurioAA13' => '',
            'mercurioAA14' => '',
            'mercurioAA15' => '',
            'mercurioA21' => '',
            'mercurioA22' => '',
            'mercurioA23' => '',
            'mercurioA24' => '',
            'mercurioA25' => '',
            'mercurioA31' => '',
            'mercurioA32' => '',
            'mercurioA33' => '',
            'mercurioA34' => '',
            'mercurioA35' => '',
            'mercurioA41' => '',
            'mercurioA42' => '',
            'mercurioA43' => '',
            'mercurioA44' => '',
            'mercurioA45' => '',
            'mercurioCC11' => '',
            'mercurioCC12' => '',
            'mercurioCC13' => '',
            'mercurioCC14' => '',
            'mercurioCC15' => '',
            'mercurioC21' => '',
            'mercurioC22' => '',
            'mercurioC23' => '',
            'mercurioC24' => '',
            'mercurioC25' => '',
            'mercurioC31' => '',
            'mercurioC32' => '',
            'mercurioC33' => '',
            'mercurioC34' => '',
            'mercurioC35' => '',
            'mercurioC41' => '',
            'mercurioC42' => '',
            'mercurioC43' => '',
            'mercurioC44' => '',
            'mercurioC45' => '',
            'mercuriocp1' => '',
            'mercuriocp2' => '',
            'mercuriocp3' => '',
            'mercuriocp4' => '',
            'mercuriocp5' => '',
            'mercurioa1' => '',
            'mercurioa2' => '',
            'mercurioa3' => '',
            'mercurioa4' => '',
            'mercurioa5' => '',
            'mercurioa6' => '',
            'mercurioa7' => '',
            'mercurioa8' => '',
            'mercurioa9' => '',
            'mercurioa10' => '',
            'mercurioa11' => '',
            'mercurioa12' => '',
            'mercurioa13' => '',
            'mercurioa14' => '',
            'mercurioa15' => '',
            'mercurioa16' => '',
            'mercurioa17' => '',
            'mercurioa18' => '',
            'mercurioa19' => '',
            'mercurioa20' => '',
            'mercurioc1' => '',
            'mercurioc2' => '',
            'mercurioc3' => '',
            'mercurioc4' => '',
            'mercurioc5' => '',
            'mercurioc6' => '',
            'mercurioc7' => '',
            'mercurioc8' => '',
            'mercurioc9' => '',
            'mercurioc10' => '',
            'mercurioc11' => '',
            'mercurioc12' => '',
            'mercurioc13' => '',
            'mercurioc14' => '',
            'mercurioc15' => '',
            'mercurioc16' => '',
            'mercurioc17' => '',
            'mercurioc18' => '',
            'mercurioc19' => '',
            'mercurioc20' => '',
            'mercuriomaxinc' => '',
            'mercuriomaxincrel' => '',
            'mercurionummedxmuestra' => '',
            'mercurionummedxpuncurva' => '',
            'mercurionummedcurva' => '',
            'mercurioconmuestra1' => '',
            'mercurioconmuestra2' => '',
            'mercurioprompatcalibracion' => '',
            'mercuriodesvestresidual' => '',
            'mercuriodesvestcurvacali' => '',
            'mercurionp' => '',
            'mercuriolote' => '',
            'mercuriovence' => '',
            'mercuriocc1' => '',
            'mercuriocc2' => '',
            'mercurioest1' => '',
            'mercurioest2' => '',
            'mercurioccest1' => '',
            'mercurioccest2' => '',
            'mercuriovalor' => '',
            'mercurioincexp' => '',
            'mercuriok' => '',
            'mercurioincest' => '',
            'mercurioestandar1' => '',
            'mercurioestandar2' => '',
            'mercurioestandar3' => '',
            'mercurioestandar4' => '',
            'mercurioestandar5' => '',
            'mercuriopipeta1' => '',
            'mercuriopipeta2' => '',
            'mercuriopipeta3' => '',
            'mercuriopipeta4' => '',
            'mercuriopipeta5' => '',
            'mercuriovolcorrpipeta1' => '',
            'mercuriovolcorrpipeta2' => '',
            'mercuriovolcorrpipeta3' => '',
            'mercuriovolcorrpipeta4' => '',
            'mercuriovolcorrpipeta5' => '',
            'mercurioincpip1' => '',
            'mercurioincpip2' => '',
            'mercurioincpip3' => '',
            'mercurioincpip4' => '',
            'mercurioincpip5' => '',
            'mercuriobalon1' => '',
            'mercuriobalon2' => '',
            'mercuriobalon3' => '',
            'mercuriobalon4' => '',
            'mercuriobalon5' => '',
            'mercuriodilinc1' => '',
            'mercuriodilinc2' => '',
            'mercuriodilinc3' => '',
            'mercuriodilinc4' => '',
            'mercuriodilinc5' => '')
        );
    }

    function IncccEncabezadoSet($_borocon1, $_borocon2, $_borocon3, $_borocon4, $_borocon5, $_boroinc1, $_boroinc2, $_boroinc3, $_boroinc4, $_boroinc5, $_boroincrel1, $_boroincrel2, $_boroincrel3, $_boroincrel4, $_boroincrel5, $_boroA11, $_boroA12, $_boroA13, $_boroA14, $_boroA15, $_boroA21, $_boroA22, $_boroA23, $_boroA24, $_boroA25, $_boroA31, $_boroA32, $_boroA33, $_boroA34, $_boroA35, $_boroA41, $_boroA42, $_boroA43, $_boroA44, $_boroA45, $_boroC11, $_boroC12, $_boroC13, $_boroC14, $_boroC15, $_boroC21, $_boroC22, $_boroC23, $_boroC24, $_boroC25, $_boroC31, $_boroC32, $_boroC33, $_boroC34, $_boroC35, $_boroC41, $_boroC42, $_boroC43, $_boroC44, $_boroC45, $_borocp1, $_borocp2, $_borocp3, $_borocp4, $_borocp5, $_boroa1, $_boroa2, $_boroa3, $_boroa4, $_boroa5, $_boroa6, $_boroa7, $_boroa8, $_boroa9, $_boroa10, $_boroa11, $_boroa12, $_boroa13, $_boroa14, $_boroa15, $_boroa16, $_boroa17, $_boroa18, $_boroa19, $_boroa20, $_boroc1, $_boroc2, $_boroc3, $_boroc4, $_boroc5, $_boroc6, $_boroc7, $_boroc8, $_boroc9, $_boroc10, $_boroc11, $_boroc12, $_boroc13, $_boroc14, $_boroc15, $_boroc16, $_boroc17, $_boroc18, $_boroc19, $_boroc20, $_boromaxinc, $_boromaxincrel, $_boronummedxmuestra, $_boronummedxpuncurva, $_boronummedcurva, $_boroconmuestra1, $_boroconmuestra2, $_boroprompatcalibracion, $_borodesvestresidual, $_borodesvestcurvacali, $_boronp, $_borolote, $_borovence, $_borocc1, $_borocc2, $_boroest1, $_boroest2, $_boroccest1, $_boroccest2, $_borovalor, $_boroincexp, $_borok, $_boroincest, $_boroestandar1, $_boroestandar2, $_boroestandar3, $_boroestandar4, $_boroestandar5, $_boropipeta1, $_boropipeta2, $_boropipeta3, $_boropipeta4, $_boropipeta5, $_borovolcorrpipeta1, $_borovolcorrpipeta2, $_borovolcorrpipeta3, $_borovolcorrpipeta4, $_borovolcorrpipeta5, $_boroincpip1, $_boroincpip2, $_boroincpip3, $_boroincpip4, $_boroincpip5, $_borobalon1, $_borobalon2, $_borobalon3, $_borobalon4, $_borobalon5, $_borodilinc1, $_borodilinc2, $_borodilinc3, $_borodilinc4, $_borodilinc5, $_calciocon1, $_calciocon2, $_calciocon3, $_calciocon4, $_calciocon5, $_calcioinc1, $_calcioinc2, $_calcioinc3, $_calcioinc4, $_calcioinc5, $_calcioincrel1, $_calcioincrel2, $_calcioincrel3, $_calcioincrel4, $_calcioincrel5, $_calcioA11, $_calcioA12, $_calcioA13, $_calcioA14, $_calcioA15, $_calcioA21, $_calcioA22, $_calcioA23, $_calcioA24, $_calcioA25, $_calcioA31, $_calcioA32, $_calcioA33, $_calcioA34, $_calcioA35, $_calcioA41, $_calcioA42, $_calcioA43, $_calcioA44, $_calcioA45, $_calcioC11, $_calcioC12, $_calcioC13, $_calcioC14, $_calcioC15, $_calcioC21, $_calcioC22, $_calcioC23, $_calcioC24, $_calcioC25, $_calcioC31, $_calcioC32, $_calcioC33, $_calcioC34, $_calcioC35, $_calcioC41, $_calcioC42, $_calcioC43, $_calcioC44, $_calcioC45, $_calciocp1, $_calciocp2, $_calciocp3, $_calciocp4, $_calciocp5, $_calcioa1, $_calcioa2, $_calcioa3, $_calcioa4, $_calcioa5, $_calcioa6, $_calcioa7, $_calcioa8, $_calcioa9, $_calcioa10, $_calcioa11, $_calcioa12, $_calcioa13, $_calcioa14, $_calcioa15, $_calcioa16, $_calcioa17, $_calcioa18, $_calcioa19, $_calcioa20, $_calcioc1, $_calcioc2, $_calcioc3, $_calcioc4, $_calcioc5, $_calcioc6, $_calcioc7, $_calcioc8, $_calcioc9, $_calcioc10, $_calcioc11, $_calcioc12, $_calcioc13, $_calcioc14, $_calcioc15, $_calcioc16, $_calcioc17, $_calcioc18, $_calcioc19, $_calcioc20, $_calciomaxinc, $_calciomaxincrel, $_calcionummedxmuestra, $_calcionummedxpuncurva, $_calcionummedcurva, $_calcioconmuestra1, $_calcioconmuestra2, $_calcioprompatcalibracion, $_calciodesvestresidual, $_calciodesvestcurvacali, $_calcionp, $_calciolote, $_calciovence, $_calciocc1, $_calciocc2, $_calcioest1, $_calcioest2, $_calcioccest1, $_calcioccest2, $_calciovalor, $_calcioincexp, $_calciok, $_calcioincest, $_calcioestandar1, $_calcioestandar2, $_calcioestandar3, $_calcioestandar4, $_calcioestandar5, $_calciopipeta1, $_calciopipeta2, $_calciopipeta3, $_calciopipeta4, $_calciopipeta5, $_calciovolcorrpipeta1, $_calciovolcorrpipeta2, $_calciovolcorrpipeta3, $_calciovolcorrpipeta4, $_calciovolcorrpipeta5, $_calcioincpip1, $_calcioincpip2, $_calcioincpip3, $_calcioincpip4, $_calcioincpip5, $_calciobalon1, $_calciobalon2, $_calciobalon3, $_calciobalon4, $_calciobalon5, $_calciodilinc1, $_calciodilinc2, $_calciodilinc3, $_calciodilinc4, $_calciodilinc5, $_cobrecon1, $_cobrecon2, $_cobrecon3, $_cobrecon4, $_cobrecon5, $_cobreinc1, $_cobreinc2, $_cobreinc3, $_cobreinc4, $_cobreinc5, $_cobreincrel1, $_cobreincrel2, $_cobreincrel3, $_cobreincrel4, $_cobreincrel5, $_cobreA11, $_cobreA12, $_cobreA13, $_cobreA14, $_cobreA15, $_cobreA21, $_cobreA22, $_cobreA23, $_cobreA24, $_cobreA25, $_cobreA31, $_cobreA32, $_cobreA33, $_cobreA34, $_cobreA35, $_cobreA41, $_cobreA42, $_cobreA43, $_cobreA44, $_cobreA45, $_cobreC11, $_cobreC12, $_cobreC13, $_cobreC14, $_cobreC15, $_cobreC21, $_cobreC22, $_cobreC23, $_cobreC24, $_cobreC25, $_cobreC31, $_cobreC32, $_cobreC33, $_cobreC34, $_cobreC35, $_cobreC41, $_cobreC42, $_cobreC43, $_cobreC44, $_cobreC45, $_cobrecp1, $_cobrecp2, $_cobrecp3, $_cobrecp4, $_cobrecp5, $_cobrea1, $_cobrea2, $_cobrea3, $_cobrea4, $_cobrea5, $_cobrea6, $_cobrea7, $_cobrea8, $_cobrea9, $_cobrea10, $_cobrea11, $_cobrea12, $_cobrea13, $_cobrea14, $_cobrea15, $_cobrea16, $_cobrea17, $_cobrea18, $_cobrea19, $_cobrea20, $_cobrec1, $_cobrec2, $_cobrec3, $_cobrec4, $_cobrec5, $_cobrec6, $_cobrec7, $_cobrec8, $_cobrec9, $_cobrec10, $_cobrec11, $_cobrec12, $_cobrec13, $_cobrec14, $_cobrec15, $_cobrec16, $_cobrec17, $_cobrec18, $_cobrec19, $_cobrec20, $_cobremaxinc, $_cobremaxincrel, $_cobrenummedxmuestra, $_cobrenummedxpuncurva, $_cobrenummedcurva, $_cobreconmuestra1, $_cobreconmuestra2, $_cobreprompatcalibracion, $_cobredesvestresidual, $_cobredesvestcurvacali, $_cobrenp, $_cobrelote, $_cobrevence, $_cobrecc1, $_cobrecc2, $_cobreest1, $_cobreest2, $_cobreccest1, $_cobreccest2, $_cobrevalor, $_cobreincexp, $_cobrek, $_cobreincest, $_cobreestandar1, $_cobreestandar2, $_cobreestandar3, $_cobreestandar4, $_cobreestandar5, $_cobrepipeta1, $_cobrepipeta2, $_cobrepipeta3, $_cobrepipeta4, $_cobrepipeta5, $_cobrevolcorrpipeta1, $_cobrevolcorrpipeta2, $_cobrevolcorrpipeta3, $_cobrevolcorrpipeta4, $_cobrevolcorrpipeta5, $_cobreincpip1, $_cobreincpip2, $_cobreincpip3, $_cobreincpip4, $_cobreincpip5, $_cobrebalon1, $_cobrebalon2, $_cobrebalon3, $_cobrebalon4, $_cobrebalon5, $_cobredilinc1, $_cobredilinc2, $_cobredilinc3, $_cobredilinc4, $_cobredilinc5, $_fosforocon1, $_fosforocon2, $_fosforocon3, $_fosforocon4, $_fosforocon5, $_fosforoinc1, $_fosforoinc2, $_fosforoinc3, $_fosforoinc4, $_fosforoinc5, $_fosforoincrel1, $_fosforoincrel2, $_fosforoincrel3, $_fosforoincrel4, $_fosforoincrel5, $_fosforoA11, $_fosforoA12, $_fosforoA13, $_fosforoA14, $_fosforoA15, $_fosforoA21, $_fosforoA22, $_fosforoA23, $_fosforoA24, $_fosforoA25, $_fosforoA31, $_fosforoA32, $_fosforoA33, $_fosforoA34, $_fosforoA35, $_fosforoA41, $_fosforoA42, $_fosforoA43, $_fosforoA44, $_fosforoA45, $_fosforoC11, $_fosforoC12, $_fosforoC13, $_fosforoC14, $_fosforoC15, $_fosforoC21, $_fosforoC22, $_fosforoC23, $_fosforoC24, $_fosforoC25, $_fosforoC31, $_fosforoC32, $_fosforoC33, $_fosforoC34, $_fosforoC35, $_fosforoC41, $_fosforoC42, $_fosforoC43, $_fosforoC44, $_fosforoC45, $_fosforocp1, $_fosforocp2, $_fosforocp3, $_fosforocp4, $_fosforocp5, $_fosforoa1, $_fosforoa2, $_fosforoa3, $_fosforoa4, $_fosforoa5, $_fosforoa6, $_fosforoa7, $_fosforoa8, $_fosforoa9, $_fosforoa10, $_fosforoa11, $_fosforoa12, $_fosforoa13, $_fosforoa14, $_fosforoa15, $_fosforoa16, $_fosforoa17, $_fosforoa18, $_fosforoa19, $_fosforoa20, $_fosforoc1, $_fosforoc2, $_fosforoc3, $_fosforoc4, $_fosforoc5, $_fosforoc6, $_fosforoc7, $_fosforoc8, $_fosforoc9, $_fosforoc10, $_fosforoc11, $_fosforoc12, $_fosforoc13, $_fosforoc14, $_fosforoc15, $_fosforoc16, $_fosforoc17, $_fosforoc18, $_fosforoc19, $_fosforoc20, $_fosforomaxinc, $_fosforomaxincrel, $_fosforonummedxmuestra, $_fosforonummedxpuncurva, $_fosforonummedcurva, $_fosforoconmuestra1, $_fosforoconmuestra2, $_fosforoprompatcalibracion, $_fosforodesvestresidual, $_fosforodesvestcurvacali, $_fosforonp, $_fosforolote, $_fosforovence, $_fosforocc1, $_fosforocc2, $_fosforoest1, $_fosforoest2, $_fosforoccest1, $_fosforoccest2, $_fosforovalor, $_fosforoincexp, $_fosforok, $_fosforoincest, $_fosforoestandar1, $_fosforoestandar2, $_fosforoestandar3, $_fosforoestandar4, $_fosforoestandar5, $_fosforopipeta1, $_fosforopipeta2, $_fosforopipeta3, $_fosforopipeta4, $_fosforopipeta5, $_fosforovolcorrpipeta1, $_fosforovolcorrpipeta2, $_fosforovolcorrpipeta3, $_fosforovolcorrpipeta4, $_fosforovolcorrpipeta5, $_fosforoincpip1, $_fosforoincpip2, $_fosforoincpip3, $_fosforoincpip4, $_fosforoincpip5, $_fosforobalon1, $_fosforobalon2, $_fosforobalon3, $_fosforobalon4, $_fosforobalon5, $_fosforodilinc1, $_fosforodilinc2, $_fosforodilinc3, $_fosforodilinc4, $_fosforodilinc5, $_hierrocon1, $_hierrocon2, $_hierrocon3, $_hierrocon4, $_hierrocon5, $_hierroinc1, $_hierroinc2, $_hierroinc3, $_hierroinc4, $_hierroinc5, $_hierroincrel1, $_hierroincrel2, $_hierroincrel3, $_hierroincrel4, $_hierroincrel5, $_hierroA11, $_hierroA12, $_hierroA13, $_hierroA14, $_hierroA15, $_hierroA21, $_hierroA22, $_hierroA23, $_hierroA24, $_hierroA25, $_hierroA31, $_hierroA32, $_hierroA33, $_hierroA34, $_hierroA35, $_hierroA41, $_hierroA42, $_hierroA43, $_hierroA44, $_hierroA45, $_hierroC11, $_hierroC12, $_hierroC13, $_hierroC14, $_hierroC15, $_hierroC21, $_hierroC22, $_hierroC23, $_hierroC24, $_hierroC25, $_hierroC31, $_hierroC32, $_hierroC33, $_hierroC34, $_hierroC35, $_hierroC41, $_hierroC42, $_hierroC43, $_hierroC44, $_hierroC45, $_hierrocp1, $_hierrocp2, $_hierrocp3, $_hierrocp4, $_hierrocp5, $_hierroa1, $_hierroa2, $_hierroa3, $_hierroa4, $_hierroa5, $_hierroa6, $_hierroa7, $_hierroa8, $_hierroa9, $_hierroa10, $_hierroa11, $_hierroa12, $_hierroa13, $_hierroa14, $_hierroa15, $_hierroa16, $_hierroa17, $_hierroa18, $_hierroa19, $_hierroa20, $_hierroc1, $_hierroc2, $_hierroc3, $_hierroc4, $_hierroc5, $_hierroc6, $_hierroc7, $_hierroc8, $_hierroc9, $_hierroc10, $_hierroc11, $_hierroc12, $_hierroc13, $_hierroc14, $_hierroc15, $_hierroc16, $_hierroc17, $_hierroc18, $_hierroc19, $_hierroc20, $_hierromaxinc, $_hierromaxincrel, $_hierronummedxmuestra, $_hierronummedxpuncurva, $_hierronummedcurva, $_hierroconmuestra1, $_hierroconmuestra2, $_hierroprompatcalibracion, $_hierrodesvestresidual, $_hierrodesvestcurvacali, $_hierronp, $_hierrolote, $_hierrovence, $_hierrocc1, $_hierrocc2, $_hierroest1, $_hierroest2, $_hierroccest1, $_hierroccest2, $_hierrovalor, $_hierroincexp, $_hierrok, $_hierroincest, $_hierroestandar1, $_hierroestandar2, $_hierroestandar3, $_hierroestandar4, $_hierroestandar5, $_hierropipeta1, $_hierropipeta2, $_hierropipeta3, $_hierropipeta4, $_hierropipeta5, $_hierrovolcorrpipeta1, $_hierrovolcorrpipeta2, $_hierrovolcorrpipeta3, $_hierrovolcorrpipeta4, $_hierrovolcorrpipeta5, $_hierroincpip1, $_hierroincpip2, $_hierroincpip3, $_hierroincpip4, $_hierroincpip5, $_hierrobalon1, $_hierrobalon2, $_hierrobalon3, $_hierrobalon4, $_hierrobalon5, $_hierrodilinc1, $_hierrodilinc2, $_hierrodilinc3, $_hierrodilinc4, $_hierrodilinc5, $_magnesiocon1, $_magnesiocon2, $_magnesiocon3, $_magnesiocon4, $_magnesiocon5, $_magnesioinc1, $_magnesioinc2, $_magnesioinc3, $_magnesioinc4, $_magnesioinc5, $_magnesioincrel1, $_magnesioincrel2, $_magnesioincrel3, $_magnesioincrel4, $_magnesioincrel5, $_magnesioA11, $_magnesioA12, $_magnesioA13, $_magnesioA14, $_magnesioA15, $_magnesioA21, $_magnesioA22, $_magnesioA23, $_magnesioA24, $_magnesioA25, $_magnesioA31, $_magnesioA32, $_magnesioA33, $_magnesioA34, $_magnesioA35, $_magnesioA41, $_magnesioA42, $_magnesioA43, $_magnesioA44, $_magnesioA45, $_magnesioC11, $_magnesioC12, $_magnesioC13, $_magnesioC14, $_magnesioC15, $_magnesioC21, $_magnesioC22, $_magnesioC23, $_magnesioC24, $_magnesioC25, $_magnesioC31, $_magnesioC32, $_magnesioC33, $_magnesioC34, $_magnesioC35, $_magnesioC41, $_magnesioC42, $_magnesioC43, $_magnesioC44, $_magnesioC45, $_magnesiocp1, $_magnesiocp2, $_magnesiocp3, $_magnesiocp4, $_magnesiocp5, $_magnesioa1, $_magnesioa2, $_magnesioa3, $_magnesioa4, $_magnesioa5, $_magnesioa6, $_magnesioa7, $_magnesioa8, $_magnesioa9, $_magnesioa10, $_magnesioa11, $_magnesioa12, $_magnesioa13, $_magnesioa14, $_magnesioa15, $_magnesioa16, $_magnesioa17, $_magnesioa18, $_magnesioa19, $_magnesioa20, $_magnesioc1, $_magnesioc2, $_magnesioc3, $_magnesioc4, $_magnesioc5, $_magnesioc6, $_magnesioc7, $_magnesioc8, $_magnesioc9, $_magnesioc10, $_magnesioc11, $_magnesioc12, $_magnesioc13, $_magnesioc14, $_magnesioc15, $_magnesioc16, $_magnesioc17, $_magnesioc18, $_magnesioc19, $_magnesioc20, $_magnesiomaxinc, $_magnesiomaxincrel, $_magnesionummedxmuestra, $_magnesionummedxpuncurva, $_magnesionummedcurva, $_magnesioconmuestra1, $_magnesioconmuestra2, $_magnesioprompatcalibracion, $_magnesiodesvestresidual, $_magnesiodesvestcurvacali, $_magnesionp, $_magnesiolote, $_magnesiovence, $_magnesiocc1, $_magnesiocc2, $_magnesioest1, $_magnesioest2, $_magnesioccest1, $_magnesioccest2, $_magnesiovalor, $_magnesioincexp, $_magnesiok, $_magnesioincest, $_magnesioestandar1, $_magnesioestandar2, $_magnesioestandar3, $_magnesioestandar4, $_magnesioestandar5, $_magnesiopipeta1, $_magnesiopipeta2, $_magnesiopipeta3, $_magnesiopipeta4, $_magnesiopipeta5, $_magnesiovolcorrpipeta1, $_magnesiovolcorrpipeta2, $_magnesiovolcorrpipeta3, $_magnesiovolcorrpipeta4, $_magnesiovolcorrpipeta5, $_magnesioincpip1, $_magnesioincpip2, $_magnesioincpip3, $_magnesioincpip4, $_magnesioincpip5, $_magnesiobalon1, $_magnesiobalon2, $_magnesiobalon3, $_magnesiobalon4, $_magnesiobalon5, $_magnesiodilinc1, $_magnesiodilinc2, $_magnesiodilinc3, $_magnesiodilinc4, $_magnesiodilinc5, $_manganesocon1, $_manganesocon2, $_manganesocon3, $_manganesocon4, $_manganesocon5, $_manganesoinc1, $_manganesoinc2, $_manganesoinc3, $_manganesoinc4, $_manganesoinc5, $_manganesoincrel1, $_manganesoincrel2, $_manganesoincrel3, $_manganesoincrel4, $_manganesoincrel5, $_manganesoA11, $_manganesoA12, $_manganesoA13, $_manganesoA14, $_manganesoA15, $_manganesoA21, $_manganesoA22, $_manganesoA23, $_manganesoA24, $_manganesoA25, $_manganesoA31, $_manganesoA32, $_manganesoA33, $_manganesoA34, $_manganesoA35, $_manganesoA41, $_manganesoA42, $_manganesoA43, $_manganesoA44, $_manganesoA45, $_manganesoC11, $_manganesoC12, $_manganesoC13, $_manganesoC14, $_manganesoC15, $_manganesoC21, $_manganesoC22, $_manganesoC23, $_manganesoC24, $_manganesoC25, $_manganesoC31, $_manganesoC32, $_manganesoC33, $_manganesoC34, $_manganesoC35, $_manganesoC41, $_manganesoC42, $_manganesoC43, $_manganesoC44, $_manganesoC45, $_manganesocp1, $_manganesocp2, $_manganesocp3, $_manganesocp4, $_manganesocp5, $_manganesoa1, $_manganesoa2, $_manganesoa3, $_manganesoa4, $_manganesoa5, $_manganesoa6, $_manganesoa7, $_manganesoa8, $_manganesoa9, $_manganesoa10, $_manganesoa11, $_manganesoa12, $_manganesoa13, $_manganesoa14, $_manganesoa15, $_manganesoa16, $_manganesoa17, $_manganesoa18, $_manganesoa19, $_manganesoa20, $_manganesoc1, $_manganesoc2, $_manganesoc3, $_manganesoc4, $_manganesoc5, $_manganesoc6, $_manganesoc7, $_manganesoc8, $_manganesoc9, $_manganesoc10, $_manganesoc11, $_manganesoc12, $_manganesoc13, $_manganesoc14, $_manganesoc15, $_manganesoc16, $_manganesoc17, $_manganesoc18, $_manganesoc19, $_manganesoc20, $_manganesomaxinc, $_manganesomaxincrel, $_manganesonummedxmuestra, $_manganesonummedxpuncurva, $_manganesonummedcurva, $_manganesoconmuestra1, $_manganesoconmuestra2, $_manganesoprompatcalibracion, $_manganesodesvestresidual, $_manganesodesvestcurvacali, $_manganesonp, $_manganesolote, $_manganesovence, $_manganesocc1, $_manganesocc2, $_manganesoest1, $_manganesoest2, $_manganesoccest1, $_manganesoccest2, $_manganesovalor, $_manganesoincexp, $_manganesok, $_manganesoincest, $_manganesoestandar1, $_manganesoestandar2, $_manganesoestandar3, $_manganesoestandar4, $_manganesoestandar5, $_manganesopipeta1, $_manganesopipeta2, $_manganesopipeta3, $_manganesopipeta4, $_manganesopipeta5, $_manganesovolcorrpipeta1, $_manganesovolcorrpipeta2, $_manganesovolcorrpipeta3, $_manganesovolcorrpipeta4, $_manganesovolcorrpipeta5, $_manganesoincpip1, $_manganesoincpip2, $_manganesoincpip3, $_manganesoincpip4, $_manganesoincpip5, $_manganesobalon1, $_manganesobalon2, $_manganesobalon3, $_manganesobalon4, $_manganesobalon5, $_manganesodilinc1, $_manganesodilinc2, $_manganesodilinc3, $_manganesodilinc4, $_manganesodilinc5, $_potasiocon1, $_potasiocon2, $_potasiocon3, $_potasiocon4, $_potasiocon5, $_potasioinc1, $_potasioinc2, $_potasioinc3, $_potasioinc4, $_potasioinc5, $_potasioincrel1, $_potasioincrel2, $_potasioincrel3, $_potasioincrel4, $_potasioincrel5, $_potasioA11, $_potasioA12, $_potasioA13, $_potasioA14, $_potasioA15, $_potasioA21, $_potasioA22, $_potasioA23, $_potasioA24, $_potasioA25, $_potasioA31, $_potasioA32, $_potasioA33, $_potasioA34, $_potasioA35, $_potasioA41, $_potasioA42, $_potasioA43, $_potasioA44, $_potasioA45, $_potasioC11, $_potasioC12, $_potasioC13, $_potasioC14, $_potasioC15, $_potasioC21, $_potasioC22, $_potasioC23, $_potasioC24, $_potasioC25, $_potasioC31, $_potasioC32, $_potasioC33, $_potasioC34, $_potasioC35, $_potasioC41, $_potasioC42, $_potasioC43, $_potasioC44, $_potasioC45, $_potasiocp1, $_potasiocp2, $_potasiocp3, $_potasiocp4, $_potasiocp5, $_potasioa1, $_potasioa2, $_potasioa3, $_potasioa4, $_potasioa5, $_potasioa6, $_potasioa7, $_potasioa8, $_potasioa9, $_potasioa10, $_potasioa11, $_potasioa12, $_potasioa13, $_potasioa14, $_potasioa15, $_potasioa16, $_potasioa17, $_potasioa18, $_potasioa19, $_potasioa20, $_potasioc1, $_potasioc2, $_potasioc3, $_potasioc4, $_potasioc5, $_potasioc6, $_potasioc7, $_potasioc8, $_potasioc9, $_potasioc10, $_potasioc11, $_potasioc12, $_potasioc13, $_potasioc14, $_potasioc15, $_potasioc16, $_potasioc17, $_potasioc18, $_potasioc19, $_potasioc20, $_potasiomaxinc, $_potasiomaxincrel, $_potasionummedxmuestra, $_potasionummedxpuncurva, $_potasionummedcurva, $_potasioconmuestra1, $_potasioconmuestra2, $_potasioprompatcalibracion, $_potasiodesvestresidual, $_potasiodesvestcurvacali, $_potasionp, $_potasiolote, $_potasiovence, $_potasiocc1, $_potasiocc2, $_potasioest1, $_potasioest2, $_potasioccest1, $_potasioccest2, $_potasiovalor, $_potasioincexp, $_potasiok, $_potasioincest, $_potasioestandar1, $_potasioestandar2, $_potasioestandar3, $_potasioestandar4, $_potasioestandar5, $_potasiopipeta1, $_potasiopipeta2, $_potasiopipeta3, $_potasiopipeta4, $_potasiopipeta5, $_potasiovolcorrpipeta1, $_potasiovolcorrpipeta2, $_potasiovolcorrpipeta3, $_potasiovolcorrpipeta4, $_potasiovolcorrpipeta5, $_potasioincpip1, $_potasioincpip2, $_potasioincpip3, $_potasioincpip4, $_potasioincpip5, $_potasiobalon1, $_potasiobalon2, $_potasiobalon3, $_potasiobalon4, $_potasiobalon5, $_potasiodilinc1, $_potasiodilinc2, $_potasiodilinc3, $_potasiodilinc4, $_potasiodilinc5, $_zinccon1, $_zinccon2, $_zinccon3, $_zinccon4, $_zinccon5, $_zincinc1, $_zincinc2, $_zincinc3, $_zincinc4, $_zincinc5, $_zincincrel1, $_zincincrel2, $_zincincrel3, $_zincincrel4, $_zincincrel5, $_zincA11, $_zincA12, $_zincA13, $_zincA14, $_zincA15, $_zincA21, $_zincA22, $_zincA23, $_zincA24, $_zincA25, $_zincA31, $_zincA32, $_zincA33, $_zincA34, $_zincA35, $_zincA41, $_zincA42, $_zincA43, $_zincA44, $_zincA45, $_zincC11, $_zincC12, $_zincC13, $_zincC14, $_zincC15, $_zincC21, $_zincC22, $_zincC23, $_zincC24, $_zincC25, $_zincC31, $_zincC32, $_zincC33, $_zincC34, $_zincC35, $_zincC41, $_zincC42, $_zincC43, $_zincC44, $_zincC45, $_zinccp1, $_zinccp2, $_zinccp3, $_zinccp4, $_zinccp5, $_zinca1, $_zinca2, $_zinca3, $_zinca4, $_zinca5, $_zinca6, $_zinca7, $_zinca8, $_zinca9, $_zinca10, $_zinca11, $_zinca12, $_zinca13, $_zinca14, $_zinca15, $_zinca16, $_zinca17, $_zinca18, $_zinca19, $_zinca20, $_zincc1, $_zincc2, $_zincc3, $_zincc4, $_zincc5, $_zincc6, $_zincc7, $_zincc8, $_zincc9, $_zincc10, $_zincc11, $_zincc12, $_zincc13, $_zincc14, $_zincc15, $_zincc16, $_zincc17, $_zincc18, $_zincc19, $_zincc20, $_zincmaxinc, $_zincmaxincrel, $_zincnummedxmuestra, $_zincnummedxpuncurva, $_zincnummedcurva, $_zincconmuestra1, $_zincconmuestra2, $_zincprompatcalibracion, $_zincdesvestresidual, $_zincdesvestcurvacali, $_zincnp, $_zinclote, $_zincvence, $_zinccc1, $_zinccc2, $_zincest1, $_zincest2, $_zincccest1, $_zincccest2, $_zincvalor, $_zincincexp, $_zinck, $_zincincest, $_zincestandar1, $_zincestandar2, $_zincestandar3, $_zincestandar4, $_zincestandar5, $_zincpipeta1, $_zincpipeta2, $_zincpipeta3, $_zincpipeta4, $_zincpipeta5, $_zincvolcorrpipeta1, $_zincvolcorrpipeta2, $_zincvolcorrpipeta3, $_zincvolcorrpipeta4, $_zincvolcorrpipeta5, $_zincincpip1, $_zincincpip2, $_zincincpip3, $_zincincpip4, $_zincincpip5, $_zincbalon1, $_zincbalon2, $_zincbalon3, $_zincbalon4, $_zincbalon5, $_zincdilinc1, $_zincdilinc2, $_zincdilinc3, $_zincdilinc4, $_zincdilinc5, $_nitrogenocon1, $_nitrogenocon2, $_nitrogenocon3, $_nitrogenocon4, $_nitrogenocon5, $_nitrogenoinc1, $_nitrogenoinc2, $_nitrogenoinc3, $_nitrogenoinc4, $_nitrogenoinc5, $_nitrogenoincrel1, $_nitrogenoincrel2, $_nitrogenoincrel3, $_nitrogenoincrel4, $_nitrogenoincrel5, $_nitrogenoA11, $_nitrogenoA12, $_nitrogenoA13, $_nitrogenoA14, $_nitrogenoA15, $_nitrogenoA21, $_nitrogenoA22, $_nitrogenoA23, $_nitrogenoA24, $_nitrogenoA25, $_nitrogenoA31, $_nitrogenoA32, $_nitrogenoA33, $_nitrogenoA34, $_nitrogenoA35, $_nitrogenoA41, $_nitrogenoA42, $_nitrogenoA43, $_nitrogenoA44, $_nitrogenoA45, $_nitrogenoC11, $_nitrogenoC12, $_nitrogenoC13, $_nitrogenoC14, $_nitrogenoC15, $_nitrogenoC21, $_nitrogenoC22, $_nitrogenoC23, $_nitrogenoC24, $_nitrogenoC25, $_nitrogenoC31, $_nitrogenoC32, $_nitrogenoC33, $_nitrogenoC34, $_nitrogenoC35, $_nitrogenoC41, $_nitrogenoC42, $_nitrogenoC43, $_nitrogenoC44, $_nitrogenoC45, $_nitrogenocp1, $_nitrogenocp2, $_nitrogenocp3, $_nitrogenocp4, $_nitrogenocp5, $_nitrogenoa1, $_nitrogenoa2, $_nitrogenoa3, $_nitrogenoa4, $_nitrogenoa5, $_nitrogenoa6, $_nitrogenoa7, $_nitrogenoa8, $_nitrogenoa9, $_nitrogenoa10, $_nitrogenoa11, $_nitrogenoa12, $_nitrogenoa13, $_nitrogenoa14, $_nitrogenoa15, $_nitrogenoa16, $_nitrogenoa17, $_nitrogenoa18, $_nitrogenoa19, $_nitrogenoa20, $_nitrogenoc1, $_nitrogenoc2, $_nitrogenoc3, $_nitrogenoc4, $_nitrogenoc5, $_nitrogenoc6, $_nitrogenoc7, $_nitrogenoc8, $_nitrogenoc9, $_nitrogenoc10, $_nitrogenoc11, $_nitrogenoc12, $_nitrogenoc13, $_nitrogenoc14, $_nitrogenoc15, $_nitrogenoc16, $_nitrogenoc17, $_nitrogenoc18, $_nitrogenoc19, $_nitrogenoc20, $_nitrogenomaxinc, $_nitrogenomaxincrel, $_nitrogenonummedxmuestra, $_nitrogenonummedxpuncurva, $_nitrogenonummedcurva, $_nitrogenoconmuestra1, $_nitrogenoconmuestra2, $_nitrogenoprompatcalibracion, $_nitrogenodesvestresidual, $_nitrogenodesvestcurvacali, $_nitrogenonp, $_nitrogenolote, $_nitrogenovence, $_nitrogenocc1, $_nitrogenocc2, $_nitrogenoest1, $_nitrogenoest2, $_nitrogenoccest1, $_nitrogenoccest2, $_nitrogenovalor, $_nitrogenoincexp, $_nitrogenok, $_nitrogenoincest, $_nitrogenoestandar1, $_nitrogenoestandar2, $_nitrogenoestandar3, $_nitrogenoestandar4, $_nitrogenoestandar5, $_nitrogenopipeta1, $_nitrogenopipeta2, $_nitrogenopipeta3, $_nitrogenopipeta4, $_nitrogenopipeta5, $_nitrogenovolcorrpipeta1, $_nitrogenovolcorrpipeta2, $_nitrogenovolcorrpipeta3, $_nitrogenovolcorrpipeta4, $_nitrogenovolcorrpipeta5, $_nitrogenoincpip1, $_nitrogenoincpip2, $_nitrogenoincpip3, $_nitrogenoincpip4, $_nitrogenoincpip5, $_nitrogenobalon1, $_nitrogenobalon2, $_nitrogenobalon3, $_nitrogenobalon4, $_nitrogenobalon5, $_nitrogenodilinc1, $_nitrogenodilinc2, $_nitrogenodilinc3, $_nitrogenodilinc4, $_nitrogenodilinc5, $_azufrecon1, $_azufrecon2, $_azufrecon3, $_azufrecon4, $_azufrecon5, $_azufreinc1, $_azufreinc2, $_azufreinc3, $_azufreinc4, $_azufreinc5, $_azufreincrel1, $_azufreincrel2, $_azufreincrel3, $_azufreincrel4, $_azufreincrel5, $_azufreA11, $_azufreA12, $_azufreA13, $_azufreA14, $_azufreA15, $_azufreA21, $_azufreA22, $_azufreA23, $_azufreA24, $_azufreA25, $_azufreA31, $_azufreA32, $_azufreA33, $_azufreA34, $_azufreA35, $_azufreA41, $_azufreA42, $_azufreA43, $_azufreA44, $_azufreA45, $_azufreC11, $_azufreC12, $_azufreC13, $_azufreC14, $_azufreC15, $_azufreC21, $_azufreC22, $_azufreC23, $_azufreC24, $_azufreC25, $_azufreC31, $_azufreC32, $_azufreC33, $_azufreC34, $_azufreC35, $_azufreC41, $_azufreC42, $_azufreC43, $_azufreC44, $_azufreC45, $_azufrecp1, $_azufrecp2, $_azufrecp3, $_azufrecp4, $_azufrecp5, $_azufrea1, $_azufrea2, $_azufrea3, $_azufrea4, $_azufrea5, $_azufrea6, $_azufrea7, $_azufrea8, $_azufrea9, $_azufrea10, $_azufrea11, $_azufrea12, $_azufrea13, $_azufrea14, $_azufrea15, $_azufrea16, $_azufrea17, $_azufrea18, $_azufrea19, $_azufrea20, $_azufrec1, $_azufrec2, $_azufrec3, $_azufrec4, $_azufrec5, $_azufrec6, $_azufrec7, $_azufrec8, $_azufrec9, $_azufrec10, $_azufrec11, $_azufrec12, $_azufrec13, $_azufrec14, $_azufrec15, $_azufrec16, $_azufrec17, $_azufrec18, $_azufrec19, $_azufrec20, $_azufremaxinc, $_azufremaxincrel, $_azufrenummedxmuestra, $_azufrenummedxpuncurva, $_azufrenummedcurva, $_azufreconmuestra1, $_azufreconmuestra2, $_azufreprompatcalibracion, $_azufredesvestresidual, $_azufredesvestcurvacali, $_azufrenp, $_azufrelote, $_azufrevence, $_azufrecc1, $_azufrecc2, $_azufreest1, $_azufreest2, $_azufreccest1, $_azufreccest2, $_azufrevalor, $_azufreincexp, $_azufrek, $_azufreincest, $_azufreestandar1, $_azufreestandar2, $_azufreestandar3, $_azufreestandar4, $_azufreestandar5, $_azufrepipeta1, $_azufrepipeta2, $_azufrepipeta3, $_azufrepipeta4, $_azufrepipeta5, $_azufrevolcorrpipeta1, $_azufrevolcorrpipeta2, $_azufrevolcorrpipeta3, $_azufrevolcorrpipeta4, $_azufrevolcorrpipeta5, $_azufreincpip1, $_azufreincpip2, $_azufreincpip3, $_azufreincpip4, $_azufreincpip5, $_azufrebalon1, $_azufrebalon2, $_azufrebalon3, $_azufrebalon4, $_azufrebalon5, $_azufredilinc1, $_azufredilinc2, $_azufredilinc3, $_azufredilinc4, $_azufredilinc5, $_arsenicocon1, $_arsenicocon2, $_arsenicocon3, $_arsenicocon4, $_arsenicocon5, $_arsenicoinc1, $_arsenicoinc2, $_arsenicoinc3, $_arsenicoinc4, $_arsenicoinc5, $_arsenicoincrel1, $_arsenicoincrel2, $_arsenicoincrel3, $_arsenicoincrel4, $_arsenicoincrel5, $_arsenicoA11, $_arsenicoA12, $_arsenicoA13, $_arsenicoA14, $_arsenicoA15, $_arsenicoA21, $_arsenicoA22, $_arsenicoA23, $_arsenicoA24, $_arsenicoA25, $_arsenicoA31, $_arsenicoA32, $_arsenicoA33, $_arsenicoA34, $_arsenicoA35, $_arsenicoA41, $_arsenicoA42, $_arsenicoA43, $_arsenicoA44, $_arsenicoA45, $_arsenicoC11, $_arsenicoC12, $_arsenicoC13, $_arsenicoC14, $_arsenicoC15, $_arsenicoC21, $_arsenicoC22, $_arsenicoC23, $_arsenicoC24, $_arsenicoC25, $_arsenicoC31, $_arsenicoC32, $_arsenicoC33, $_arsenicoC34, $_arsenicoC35, $_arsenicoC41, $_arsenicoC42, $_arsenicoC43, $_arsenicoC44, $_arsenicoC45, $_arsenicocp1, $_arsenicocp2, $_arsenicocp3, $_arsenicocp4, $_arsenicocp5, $_arsenicoa1, $_arsenicoa2, $_arsenicoa3, $_arsenicoa4, $_arsenicoa5, $_arsenicoa6, $_arsenicoa7, $_arsenicoa8, $_arsenicoa9, $_arsenicoa10, $_arsenicoa11, $_arsenicoa12, $_arsenicoa13, $_arsenicoa14, $_arsenicoa15, $_arsenicoa16, $_arsenicoa17, $_arsenicoa18, $_arsenicoa19, $_arsenicoa20, $_arsenicoc1, $_arsenicoc2, $_arsenicoc3, $_arsenicoc4, $_arsenicoc5, $_arsenicoc6, $_arsenicoc7, $_arsenicoc8, $_arsenicoc9, $_arsenicoc10, $_arsenicoc11, $_arsenicoc12, $_arsenicoc13, $_arsenicoc14, $_arsenicoc15, $_arsenicoc16, $_arsenicoc17, $_arsenicoc18, $_arsenicoc19, $_arsenicoc20, $_arsenicomaxinc, $_arsenicomaxincrel, $_arseniconummedxmuestra, $_arseniconummedxpuncurva, $_arseniconummedcurva, $_arsenicoconmuestra1, $_arsenicoconmuestra2, $_arsenicoprompatcalibracion, $_arsenicodesvestresidual, $_arsenicodesvestcurvacali, $_arseniconp, $_arsenicolote, $_arsenicovence, $_arsenicocc1, $_arsenicocc2, $_arsenicoest1, $_arsenicoest2, $_arsenicoccest1, $_arsenicoccest2, $_arsenicovalor, $_arsenicoincexp, $_arsenicok, $_arsenicoincest, $_arsenicoestandar1, $_arsenicoestandar2, $_arsenicoestandar3, $_arsenicoestandar4, $_arsenicoestandar5, $_arsenicopipeta1, $_arsenicopipeta2, $_arsenicopipeta3, $_arsenicopipeta4, $_arsenicopipeta5, $_arsenicovolcorrpipeta1, $_arsenicovolcorrpipeta2, $_arsenicovolcorrpipeta3, $_arsenicovolcorrpipeta4, $_arsenicovolcorrpipeta5, $_arsenicoincpip1, $_arsenicoincpip2, $_arsenicoincpip3, $_arsenicoincpip4, $_arsenicoincpip5, $_arsenicobalon1, $_arsenicobalon2, $_arsenicobalon3, $_arsenicobalon4, $_arsenicobalon5, $_arsenicodilinc1, $_arsenicodilinc2, $_arsenicodilinc3, $_arsenicodilinc4, $_arsenicodilinc5, $_cadmiocon1, $_cadmiocon2, $_cadmiocon3, $_cadmiocon4, $_cadmiocon5, $_cadmioinc1, $_cadmioinc2, $_cadmioinc3, $_cadmioinc4, $_cadmioinc5, $_cadmioincrel1, $_cadmioincrel2, $_cadmioincrel3, $_cadmioincrel4, $_cadmioincrel5, $_cadmioA11, $_cadmioA12, $_cadmioA13, $_cadmioA14, $_cadmioA15, $_cadmioA21, $_cadmioA22, $_cadmioA23, $_cadmioA24, $_cadmioA25, $_cadmioA31, $_cadmioA32, $_cadmioA33, $_cadmioA34, $_cadmioA35, $_cadmioA41, $_cadmioA42, $_cadmioA43, $_cadmioA44, $_cadmioA45, $_cadmioC11, $_cadmioC12, $_cadmioC13, $_cadmioC14, $_cadmioC15, $_cadmioC21, $_cadmioC22, $_cadmioC23, $_cadmioC24, $_cadmioC25, $_cadmioC31, $_cadmioC32, $_cadmioC33, $_cadmioC34, $_cadmioC35, $_cadmioC41, $_cadmioC42, $_cadmioC43, $_cadmioC44, $_cadmioC45, $_cadmiocp1, $_cadmiocp2, $_cadmiocp3, $_cadmiocp4, $_cadmiocp5, $_cadmioa1, $_cadmioa2, $_cadmioa3, $_cadmioa4, $_cadmioa5, $_cadmioa6, $_cadmioa7, $_cadmioa8, $_cadmioa9, $_cadmioa10, $_cadmioa11, $_cadmioa12, $_cadmioa13, $_cadmioa14, $_cadmioa15, $_cadmioa16, $_cadmioa17, $_cadmioa18, $_cadmioa19, $_cadmioa20, $_cadmioc1, $_cadmioc2, $_cadmioc3, $_cadmioc4, $_cadmioc5, $_cadmioc6, $_cadmioc7, $_cadmioc8, $_cadmioc9, $_cadmioc10, $_cadmioc11, $_cadmioc12, $_cadmioc13, $_cadmioc14, $_cadmioc15, $_cadmioc16, $_cadmioc17, $_cadmioc18, $_cadmioc19, $_cadmioc20, $_cadmiomaxinc, $_cadmiomaxincrel, $_cadmionummedxmuestra, $_cadmionummedxpuncurva, $_cadmionummedcurva, $_cadmioconmuestra1, $_cadmioconmuestra2, $_cadmioprompatcalibracion, $_cadmiodesvestresidual, $_cadmiodesvestcurvacali, $_cadmionp, $_cadmiolote, $_cadmiovence, $_cadmiocc1, $_cadmiocc2, $_cadmioest1, $_cadmioest2, $_cadmioccest1, $_cadmioccest2, $_cadmiovalor, $_cadmioincexp, $_cadmiok, $_cadmioincest, $_cadmioestandar1, $_cadmioestandar2, $_cadmioestandar3, $_cadmioestandar4, $_cadmioestandar5, $_cadmiopipeta1, $_cadmiopipeta2, $_cadmiopipeta3, $_cadmiopipeta4, $_cadmiopipeta5, $_cadmiovolcorrpipeta1, $_cadmiovolcorrpipeta2, $_cadmiovolcorrpipeta3, $_cadmiovolcorrpipeta4, $_cadmiovolcorrpipeta5, $_cadmioincpip1, $_cadmioincpip2, $_cadmioincpip3, $_cadmioincpip4, $_cadmioincpip5, $_cadmiobalon1, $_cadmiobalon2, $_cadmiobalon3, $_cadmiobalon4, $_cadmiobalon5, $_cadmiodilinc1, $_cadmiodilinc2, $_cadmiodilinc3, $_cadmiodilinc4, $_cadmiodilinc5, $_cromocon1, $_cromocon2, $_cromocon3, $_cromocon4, $_cromocon5, $_cromoinc1, $_cromoinc2, $_cromoinc3, $_cromoinc4, $_cromoinc5, $_cromoincrel1, $_cromoincrel2, $_cromoincrel3, $_cromoincrel4, $_cromoincrel5, $_cromoA11, $_cromoA12, $_cromoA13, $_cromoA14, $_cromoA15, $_cromoA21, $_cromoA22, $_cromoA23, $_cromoA24, $_cromoA25, $_cromoA31, $_cromoA32, $_cromoA33, $_cromoA34, $_cromoA35, $_cromoA41, $_cromoA42, $_cromoA43, $_cromoA44, $_cromoA45, $_cromoC11, $_cromoC12, $_cromoC13, $_cromoC14, $_cromoC15, $_cromoC21, $_cromoC22, $_cromoC23, $_cromoC24, $_cromoC25, $_cromoC31, $_cromoC32, $_cromoC33, $_cromoC34, $_cromoC35, $_cromoC41, $_cromoC42, $_cromoC43, $_cromoC44, $_cromoC45, $_cromocp1, $_cromocp2, $_cromocp3, $_cromocp4, $_cromocp5, $_cromoa1, $_cromoa2, $_cromoa3, $_cromoa4, $_cromoa5, $_cromoa6, $_cromoa7, $_cromoa8, $_cromoa9, $_cromoa10, $_cromoa11, $_cromoa12, $_cromoa13, $_cromoa14, $_cromoa15, $_cromoa16, $_cromoa17, $_cromoa18, $_cromoa19, $_cromoa20, $_cromoc1, $_cromoc2, $_cromoc3, $_cromoc4, $_cromoc5, $_cromoc6, $_cromoc7, $_cromoc8, $_cromoc9, $_cromoc10, $_cromoc11, $_cromoc12, $_cromoc13, $_cromoc14, $_cromoc15, $_cromoc16, $_cromoc17, $_cromoc18, $_cromoc19, $_cromoc20, $_cromomaxinc, $_cromomaxincrel, $_cromonummedxmuestra, $_cromonummedxpuncurva, $_cromonummedcurva, $_cromoconmuestra1, $_cromoconmuestra2, $_cromoprompatcalibracion, $_cromodesvestresidual, $_cromodesvestcurvacali, $_cromonp, $_cromolote, $_cromovence, $_cromocc1, $_cromocc2, $_cromoest1, $_cromoest2, $_cromoccest1, $_cromoccest2, $_cromovalor, $_cromoincexp, $_cromok, $_cromoincest, $_cromoestandar1, $_cromoestandar2, $_cromoestandar3, $_cromoestandar4, $_cromoestandar5, $_cromopipeta1, $_cromopipeta2, $_cromopipeta3, $_cromopipeta4, $_cromopipeta5, $_cromovolcorrpipeta1, $_cromovolcorrpipeta2, $_cromovolcorrpipeta3, $_cromovolcorrpipeta4, $_cromovolcorrpipeta5, $_cromoincpip1, $_cromoincpip2, $_cromoincpip3, $_cromoincpip4, $_cromoincpip5, $_cromobalon1, $_cromobalon2, $_cromobalon3, $_cromobalon4, $_cromobalon5, $_cromodilinc1, $_cromodilinc2, $_cromodilinc3, $_cromodilinc4, $_cromodilinc5, $_plomocon1, $_plomocon2, $_plomocon3, $_plomocon4, $_plomocon5, $_plomoinc1, $_plomoinc2, $_plomoinc3, $_plomoinc4, $_plomoinc5, $_plomoincrel1, $_plomoincrel2, $_plomoincrel3, $_plomoincrel4, $_plomoincrel5, $_plomoA11, $_plomoA12, $_plomoA13, $_plomoA14, $_plomoA15, $_plomoA21, $_plomoA22, $_plomoA23, $_plomoA24, $_plomoA25, $_plomoA31, $_plomoA32, $_plomoA33, $_plomoA34, $_plomoA35, $_plomoA41, $_plomoA42, $_plomoA43, $_plomoA44, $_plomoA45, $_plomoC11, $_plomoC12, $_plomoC13, $_plomoC14, $_plomoC15, $_plomoC21, $_plomoC22, $_plomoC23, $_plomoC24, $_plomoC25, $_plomoC31, $_plomoC32, $_plomoC33, $_plomoC34, $_plomoC35, $_plomoC41, $_plomoC42, $_plomoC43, $_plomoC44, $_plomoC45, $_plomocp1, $_plomocp2, $_plomocp3, $_plomocp4, $_plomocp5, $_plomoa1, $_plomoa2, $_plomoa3, $_plomoa4, $_plomoa5, $_plomoa6, $_plomoa7, $_plomoa8, $_plomoa9, $_plomoa10, $_plomoa11, $_plomoa12, $_plomoa13, $_plomoa14, $_plomoa15, $_plomoa16, $_plomoa17, $_plomoa18, $_plomoa19, $_plomoa20, $_plomoc1, $_plomoc2, $_plomoc3, $_plomoc4, $_plomoc5, $_plomoc6, $_plomoc7, $_plomoc8, $_plomoc9, $_plomoc10, $_plomoc11, $_plomoc12, $_plomoc13, $_plomoc14, $_plomoc15, $_plomoc16, $_plomoc17, $_plomoc18, $_plomoc19, $_plomoc20, $_plomomaxinc, $_plomomaxincrel, $_plomonummedxmuestra, $_plomonummedxpuncurva, $_plomonummedcurva, $_plomoconmuestra1, $_plomoconmuestra2, $_plomoprompatcalibracion, $_plomodesvestresidual, $_plomodesvestcurvacali, $_plomonp, $_plomolote, $_plomovence, $_plomocc1, $_plomocc2, $_plomoest1, $_plomoest2, $_plomoccest1, $_plomoccest2, $_plomovalor, $_plomoincexp, $_plomok, $_plomoincest, $_plomoestandar1, $_plomoestandar2, $_plomoestandar3, $_plomoestandar4, $_plomoestandar5, $_plomopipeta1, $_plomopipeta2, $_plomopipeta3, $_plomopipeta4, $_plomopipeta5, $_plomovolcorrpipeta1, $_plomovolcorrpipeta2, $_plomovolcorrpipeta3, $_plomovolcorrpipeta4, $_plomovolcorrpipeta5, $_plomoincpip1, $_plomoincpip2, $_plomoincpip3, $_plomoincpip4, $_plomoincpip5, $_plomobalon1, $_plomobalon2, $_plomobalon3, $_plomobalon4, $_plomobalon5, $_plomodilinc1, $_plomodilinc2, $_plomodilinc3, $_plomodilinc4, $_plomodilinc5, $_mercuriocon1, $_mercuriocon2, $_mercuriocon3, $_mercuriocon4, $_mercuriocon5, $_mercurioinc1, $_mercurioinc2, $_mercurioinc3, $_mercurioinc4, $_mercurioinc5, $_mercurioincrel1, $_mercurioincrel2, $_mercurioincrel3, $_mercurioincrel4, $_mercurioincrel5, $_mercurioA11, $_mercurioA12, $_mercurioA13, $_mercurioA14, $_mercurioA15, $_mercurioA21, $_mercurioA22, $_mercurioA23, $_mercurioA24, $_mercurioA25, $_mercurioA31, $_mercurioA32, $_mercurioA33, $_mercurioA34, $_mercurioA35, $_mercurioA41, $_mercurioA42, $_mercurioA43, $_mercurioA44, $_mercurioA45, $_mercurioC11, $_mercurioC12, $_mercurioC13, $_mercurioC14, $_mercurioC15, $_mercurioC21, $_mercurioC22, $_mercurioC23, $_mercurioC24, $_mercurioC25, $_mercurioC31, $_mercurioC32, $_mercurioC33, $_mercurioC34, $_mercurioC35, $_mercurioC41, $_mercurioC42, $_mercurioC43, $_mercurioC44, $_mercurioC45, $_mercuriocp1, $_mercuriocp2, $_mercuriocp3, $_mercuriocp4, $_mercuriocp5, $_mercurioa1, $_mercurioa2, $_mercurioa3, $_mercurioa4, $_mercurioa5, $_mercurioa6, $_mercurioa7, $_mercurioa8, $_mercurioa9, $_mercurioa10, $_mercurioa11, $_mercurioa12, $_mercurioa13, $_mercurioa14, $_mercurioa15, $_mercurioa16, $_mercurioa17, $_mercurioa18, $_mercurioa19, $_mercurioa20, $_mercurioc1, $_mercurioc2, $_mercurioc3, $_mercurioc4, $_mercurioc5, $_mercurioc6, $_mercurioc7, $_mercurioc8, $_mercurioc9, $_mercurioc10, $_mercurioc11, $_mercurioc12, $_mercurioc13, $_mercurioc14, $_mercurioc15, $_mercurioc16, $_mercurioc17, $_mercurioc18, $_mercurioc19, $_mercurioc20, $_mercuriomaxinc, $_mercuriomaxincrel, $_mercurionummedxmuestra, $_mercurionummedxpuncurva, $_mercurionummedcurva, $_mercurioconmuestra1, $_mercurioconmuestra2, $_mercurioprompatcalibracion, $_mercuriodesvestresidual, $_mercuriodesvestcurvacali, $_mercurionp, $_mercuriolote, $_mercuriovence, $_mercuriocc1, $_mercuriocc2, $_mercurioest1, $_mercurioest2, $_mercurioccest1, $_mercurioccest2, $_mercuriovalor, $_mercurioincexp, $_mercuriok, $_mercurioincest, $_mercurioestandar1, $_mercurioestandar2, $_mercurioestandar3, $_mercurioestandar4, $_mercurioestandar5, $_mercuriopipeta1, $_mercuriopipeta2, $_mercuriopipeta3, $_mercuriopipeta4, $_mercuriopipeta5, $_mercuriovolcorrpipeta1, $_mercuriovolcorrpipeta2, $_mercuriovolcorrpipeta3, $_mercuriovolcorrpipeta4, $_mercuriovolcorrpipeta5, $_mercurioincpip1, $_mercurioincpip2, $_mercurioincpip3, $_mercurioincpip4, $_mercurioincpip5, $_mercuriobalon1, $_mercuriobalon2, $_mercuriobalon3, $_mercuriobalon4, $_mercuriobalon5, $_mercuriodilinc1, $_mercuriodilinc2, $_mercuriodilinc3, $_mercuriodilinc4, $_mercuriodilinc5)
    {

        $this->_TRANS("UPDATE MET027 SET estado = '0' WHERE 1=1;");

        $cs = $this->_QUERY("SELECT MAX(id)+1 AS id FROM MET027 WHERE 1=1");

        $this->_TRANS("INSERT INTO MET027 VALUES('{$cs[0]['id']}', GETDATE(), '1');");
        $this->_TRANS("INSERT INTO MET027boro VALUES('{$cs[0]['id']}', '{$_borocon1}', '{$_borocon2}', '{$_borocon3}', '{$_borocon4}', '{$_borocon5}', '{$_boroinc1}', '{$_boroinc2}', '{$_boroinc3}', '{$_boroinc4}', '{$_boroinc5}', '{$_boroincrel1}', '{$_boroincrel2}', '{$_boroincrel3}', '{$_boroincrel4}', '{$_boroincrel5}', '{$_boroA11}', '{$_boroA12}', '{$_boroA13}', '{$_boroA14}', '{$_boroA15}', '{$_boroA21}', '{$_boroA22}', '{$_boroA23}', '{$_boroA24}', '{$_boroA25}', '{$_boroA31}', '{$_boroA32}', '{$_boroA33}', '{$_boroA34}', '{$_boroA35}', '{$_boroA41}', '{$_boroA42}', '{$_boroA43}', '{$_boroA44}', '{$_boroA45}', '{$_boroC11}', '{$_boroC12}', '{$_boroC13}', '{$_boroC14}', '{$_boroC15}', '{$_boroC21}', '{$_boroC22}', '{$_boroC23}', '{$_boroC24}', '{$_boroC25}', '{$_boroC31}', '{$_boroC32}', '{$_boroC33}', '{$_boroC34}', '{$_boroC35}', '{$_boroC41}', '{$_boroC42}', '{$_boroC43}', '{$_boroC44}', '{$_boroC45}', '{$_borocp1}', '{$_borocp2}', '{$_borocp3}', '{$_borocp4}', '{$_borocp5}', '{$_boroa1}', '{$_boroa2}', '{$_boroa3}', '{$_boroa4}', '{$_boroa5}', '{$_boroa6}', '{$_boroa7}', '{$_boroa8}', '{$_boroa9}', '{$_boroa10}', '{$_boroa11}', '{$_boroa12}', '{$_boroa13}', '{$_boroa14}', '{$_boroa15}', '{$_boroa16}', '{$_boroa17}', '{$_boroa18}', '{$_boroa19}', '{$_boroa20}', '{$_boroc1}', '{$_boroc2}', '{$_boroc3}', '{$_boroc4}', '{$_boroc5}', '{$_boroc6}', '{$_boroc7}', '{$_boroc8}', '{$_boroc9}', '{$_boroc10}', '{$_boroc11}', '{$_boroc12}', '{$_boroc13}', '{$_boroc14}', '{$_boroc15}', '{$_boroc16}', '{$_boroc17}', '{$_boroc18}', '{$_boroc19}', '{$_boroc20}', '{$_boromaxinc}', '{$_boromaxincrel}', '{$_boronummedxmuestra}', '{$_boronummedxpuncurva}', '{$_boronummedcurva}', '{$_boroconmuestra1}', '{$_boroconmuestra2}', '{$_boroprompatcalibracion}', '{$_borodesvestresidual}', '{$_borodesvestcurvacali}', '{$_boronp}', '{$_borolote}', '{$_borovence}', '{$_borocc1}', '{$_borocc2}', '{$_boroest1}', '{$_boroest2}', '{$_boroccest1}', '{$_boroccest2}', '{$_borovalor}', '{$_boroincexp}', '{$_borok}', '{$_boroincest}', '{$_boroestandar1}', '{$_boroestandar2}', '{$_boroestandar3}', '{$_boroestandar4}', '{$_boroestandar5}', '{$_boropipeta1}', '{$_boropipeta2}', '{$_boropipeta3}', '{$_boropipeta4}', '{$_boropipeta5}', '{$_borovolcorrpipeta1}', '{$_borovolcorrpipeta2}', '{$_borovolcorrpipeta3}', '{$_borovolcorrpipeta4}', '{$_borovolcorrpipeta5}', '{$_boroincpip1}', '{$_boroincpip2}', '{$_boroincpip3}', '{$_boroincpip4}', '{$_boroincpip5}', '{$_borobalon1}', '{$_borobalon2}', '{$_borobalon3}', '{$_borobalon4}', '{$_borobalon5}', '{$_borodilinc1}', '{$_borodilinc2}', '{$_borodilinc3}', '{$_borodilinc4}', '{$_borodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027calcio VALUES('{$cs[0]['id']}', '{$_calciocon1}', '{$_calciocon2}', '{$_calciocon3}', '{$_calciocon4}', '{$_calciocon5}', '{$_calcioinc1}', '{$_calcioinc2}', '{$_calcioinc3}', '{$_calcioinc4}', '{$_calcioinc5}', '{$_calcioincrel1}', '{$_calcioincrel2}', '{$_calcioincrel3}', '{$_calcioincrel4}', '{$_calcioincrel5}', '{$_calcioA11}', '{$_calcioA12}', '{$_calcioA13}', '{$_calcioA14}', '{$_calcioA15}', '{$_calcioA21}', '{$_calcioA22}', '{$_calcioA23}', '{$_calcioA24}', '{$_calcioA25}', '{$_calcioA31}', '{$_calcioA32}', '{$_calcioA33}', '{$_calcioA34}', '{$_calcioA35}', '{$_calcioA41}', '{$_calcioA42}', '{$_calcioA43}', '{$_calcioA44}', '{$_calcioA45}', '{$_calcioC11}', '{$_calcioC12}', '{$_calcioC13}', '{$_calcioC14}', '{$_calcioC15}', '{$_calcioC21}', '{$_calcioC22}', '{$_calcioC23}', '{$_calcioC24}', '{$_calcioC25}', '{$_calcioC31}', '{$_calcioC32}', '{$_calcioC33}', '{$_calcioC34}', '{$_calcioC35}', '{$_calcioC41}', '{$_calcioC42}', '{$_calcioC43}', '{$_calcioC44}', '{$_calcioC45}', '{$_calciocp1}', '{$_calciocp2}', '{$_calciocp3}', '{$_calciocp4}', '{$_calciocp5}', '{$_calcioa1}', '{$_calcioa2}', '{$_calcioa3}', '{$_calcioa4}', '{$_calcioa5}', '{$_calcioa6}', '{$_calcioa7}', '{$_calcioa8}', '{$_calcioa9}', '{$_calcioa10}', '{$_calcioa11}', '{$_calcioa12}', '{$_calcioa13}', '{$_calcioa14}', '{$_calcioa15}', '{$_calcioa16}', '{$_calcioa17}', '{$_calcioa18}', '{$_calcioa19}', '{$_calcioa20}', '{$_calcioc1}', '{$_calcioc2}', '{$_calcioc3}', '{$_calcioc4}', '{$_calcioc5}', '{$_calcioc6}', '{$_calcioc7}', '{$_calcioc8}', '{$_calcioc9}', '{$_calcioc10}', '{$_calcioc11}', '{$_calcioc12}', '{$_calcioc13}', '{$_calcioc14}', '{$_calcioc15}', '{$_calcioc16}', '{$_calcioc17}', '{$_calcioc18}', '{$_calcioc19}', '{$_calcioc20}', '{$_calciomaxinc}', '{$_calciomaxincrel}', '{$_calcionummedxmuestra}', '{$_calcionummedxpuncurva}', '{$_calcionummedcurva}', '{$_calcioconmuestra1}', '{$_calcioconmuestra2}', '{$_calcioprompatcalibracion}', '{$_calciodesvestresidual}', '{$_calciodesvestcurvacali}', '{$_calcionp}', '{$_calciolote}', '{$_calciovence}', '{$_calciocc1}', '{$_calciocc2}', '{$_calcioest1}', '{$_calcioest2}', '{$_calcioccest1}', '{$_calcioccest2}', '{$_calciovalor}', '{$_calcioincexp}', '{$_calciok}', '{$_calcioincest}', '{$_calcioestandar1}', '{$_calcioestandar2}', '{$_calcioestandar3}', '{$_calcioestandar4}', '{$_calcioestandar5}', '{$_calciopipeta1}', '{$_calciopipeta2}', '{$_calciopipeta3}', '{$_calciopipeta4}', '{$_calciopipeta5}', '{$_calciovolcorrpipeta1}', '{$_calciovolcorrpipeta2}', '{$_calciovolcorrpipeta3}', '{$_calciovolcorrpipeta4}', '{$_calciovolcorrpipeta5}', '{$_calcioincpip1}', '{$_calcioincpip2}', '{$_calcioincpip3}', '{$_calcioincpip4}', '{$_calcioincpip5}', '{$_calciobalon1}', '{$_calciobalon2}', '{$_calciobalon3}', '{$_calciobalon4}', '{$_calciobalon5}', '{$_calciodilinc1}', '{$_calciodilinc2}', '{$_calciodilinc3}', '{$_calciodilinc4}', '{$_calciodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027cobre VALUES('{$cs[0]['id']}', '{$_cobrecon1}', '{$_cobrecon2}', '{$_cobrecon3}', '{$_cobrecon4}', '{$_cobrecon5}', '{$_cobreinc1}', '{$_cobreinc2}', '{$_cobreinc3}', '{$_cobreinc4}', '{$_cobreinc5}', '{$_cobreincrel1}', '{$_cobreincrel2}', '{$_cobreincrel3}', '{$_cobreincrel4}', '{$_cobreincrel5}', '{$_cobreA11}', '{$_cobreA12}', '{$_cobreA13}', '{$_cobreA14}', '{$_cobreA15}', '{$_cobreA21}', '{$_cobreA22}', '{$_cobreA23}', '{$_cobreA24}', '{$_cobreA25}', '{$_cobreA31}', '{$_cobreA32}', '{$_cobreA33}', '{$_cobreA34}', '{$_cobreA35}', '{$_cobreA41}', '{$_cobreA42}', '{$_cobreA43}', '{$_cobreA44}', '{$_cobreA45}', '{$_cobreC11}', '{$_cobreC12}', '{$_cobreC13}', '{$_cobreC14}', '{$_cobreC15}', '{$_cobreC21}', '{$_cobreC22}', '{$_cobreC23}', '{$_cobreC24}', '{$_cobreC25}', '{$_cobreC31}', '{$_cobreC32}', '{$_cobreC33}', '{$_cobreC34}', '{$_cobreC35}', '{$_cobreC41}', '{$_cobreC42}', '{$_cobreC43}', '{$_cobreC44}', '{$_cobreC45}', '{$_cobrecp1}', '{$_cobrecp2}', '{$_cobrecp3}', '{$_cobrecp4}', '{$_cobrecp5}', '{$_cobrea1}', '{$_cobrea2}', '{$_cobrea3}', '{$_cobrea4}', '{$_cobrea5}', '{$_cobrea6}', '{$_cobrea7}', '{$_cobrea8}', '{$_cobrea9}', '{$_cobrea10}', '{$_cobrea11}', '{$_cobrea12}', '{$_cobrea13}', '{$_cobrea14}', '{$_cobrea15}', '{$_cobrea16}', '{$_cobrea17}', '{$_cobrea18}', '{$_cobrea19}', '{$_cobrea20}', '{$_cobrec1}', '{$_cobrec2}', '{$_cobrec3}', '{$_cobrec4}', '{$_cobrec5}', '{$_cobrec6}', '{$_cobrec7}', '{$_cobrec8}', '{$_cobrec9}', '{$_cobrec10}', '{$_cobrec11}', '{$_cobrec12}', '{$_cobrec13}', '{$_cobrec14}', '{$_cobrec15}', '{$_cobrec16}', '{$_cobrec17}', '{$_cobrec18}', '{$_cobrec19}', '{$_cobrec20}', '{$_cobremaxinc}', '{$_cobremaxincrel}', '{$_cobrenummedxmuestra}', '{$_cobrenummedxpuncurva}', '{$_cobrenummedcurva}', '{$_cobreconmuestra1}', '{$_cobreconmuestra2}', '{$_cobreprompatcalibracion}', '{$_cobredesvestresidual}', '{$_cobredesvestcurvacali}', '{$_cobrenp}', '{$_cobrelote}', '{$_cobrevence}', '{$_cobrecc1}', '{$_cobrecc2}', '{$_cobreest1}', '{$_cobreest2}', '{$_cobreccest1}', '{$_cobreccest2}', '{$_cobrevalor}', '{$_cobreincexp}', '{$_cobrek}', '{$_cobreincest}', '{$_cobreestandar1}', '{$_cobreestandar2}', '{$_cobreestandar3}', '{$_cobreestandar4}', '{$_cobreestandar5}', '{$_cobrepipeta1}', '{$_cobrepipeta2}', '{$_cobrepipeta3}', '{$_cobrepipeta4}', '{$_cobrepipeta5}', '{$_cobrevolcorrpipeta1}', '{$_cobrevolcorrpipeta2}', '{$_cobrevolcorrpipeta3}', '{$_cobrevolcorrpipeta4}', '{$_cobrevolcorrpipeta5}', '{$_cobreincpip1}', '{$_cobreincpip2}', '{$_cobreincpip3}', '{$_cobreincpip4}', '{$_cobreincpip5}', '{$_cobrebalon1}', '{$_cobrebalon2}', '{$_cobrebalon3}', '{$_cobrebalon4}', '{$_cobrebalon5}', '{$_cobredilinc1}', '{$_cobredilinc2}', '{$_cobredilinc3}', '{$_cobredilinc4}', '{$_cobredilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027fosforo VALUES('{$cs[0]['id']}', '{$_fosforocon1}', '{$_fosforocon2}', '{$_fosforocon3}', '{$_fosforocon4}', '{$_fosforocon5}', '{$_fosforoinc1}', '{$_fosforoinc2}', '{$_fosforoinc3}', '{$_fosforoinc4}', '{$_fosforoinc5}', '{$_fosforoincrel1}', '{$_fosforoincrel2}', '{$_fosforoincrel3}', '{$_fosforoincrel4}', '{$_fosforoincrel5}', '{$_fosforoA11}', '{$_fosforoA12}', '{$_fosforoA13}', '{$_fosforoA14}', '{$_fosforoA15}', '{$_fosforoA21}', '{$_fosforoA22}', '{$_fosforoA23}', '{$_fosforoA24}', '{$_fosforoA25}', '{$_fosforoA31}', '{$_fosforoA32}', '{$_fosforoA33}', '{$_fosforoA34}', '{$_fosforoA35}', '{$_fosforoA41}', '{$_fosforoA42}', '{$_fosforoA43}', '{$_fosforoA44}', '{$_fosforoA45}', '{$_fosforoC11}', '{$_fosforoC12}', '{$_fosforoC13}', '{$_fosforoC14}', '{$_fosforoC15}', '{$_fosforoC21}', '{$_fosforoC22}', '{$_fosforoC23}', '{$_fosforoC24}', '{$_fosforoC25}', '{$_fosforoC31}', '{$_fosforoC32}', '{$_fosforoC33}', '{$_fosforoC34}', '{$_fosforoC35}', '{$_fosforoC41}', '{$_fosforoC42}', '{$_fosforoC43}', '{$_fosforoC44}', '{$_fosforoC45}', '{$_fosforocp1}', '{$_fosforocp2}', '{$_fosforocp3}', '{$_fosforocp4}', '{$_fosforocp5}', '{$_fosforoa1}', '{$_fosforoa2}', '{$_fosforoa3}', '{$_fosforoa4}', '{$_fosforoa5}', '{$_fosforoa6}', '{$_fosforoa7}', '{$_fosforoa8}', '{$_fosforoa9}', '{$_fosforoa10}', '{$_fosforoa11}', '{$_fosforoa12}', '{$_fosforoa13}', '{$_fosforoa14}', '{$_fosforoa15}', '{$_fosforoa16}', '{$_fosforoa17}', '{$_fosforoa18}', '{$_fosforoa19}', '{$_fosforoa20}', '{$_fosforoc1}', '{$_fosforoc2}', '{$_fosforoc3}', '{$_fosforoc4}', '{$_fosforoc5}', '{$_fosforoc6}', '{$_fosforoc7}', '{$_fosforoc8}', '{$_fosforoc9}', '{$_fosforoc10}', '{$_fosforoc11}', '{$_fosforoc12}', '{$_fosforoc13}', '{$_fosforoc14}', '{$_fosforoc15}', '{$_fosforoc16}', '{$_fosforoc17}', '{$_fosforoc18}', '{$_fosforoc19}', '{$_fosforoc20}', '{$_fosforomaxinc}', '{$_fosforomaxincrel}', '{$_fosforonummedxmuestra}', '{$_fosforonummedxpuncurva}', '{$_fosforonummedcurva}', '{$_fosforoconmuestra1}', '{$_fosforoconmuestra2}', '{$_fosforoprompatcalibracion}', '{$_fosforodesvestresidual}', '{$_fosforodesvestcurvacali}', '{$_fosforonp}', '{$_fosforolote}', '{$_fosforovence}', '{$_fosforocc1}', '{$_fosforocc2}', '{$_fosforoest1}', '{$_fosforoest2}', '{$_fosforoccest1}', '{$_fosforoccest2}', '{$_fosforovalor}', '{$_fosforoincexp}', '{$_fosforok}', '{$_fosforoincest}', '{$_fosforoestandar1}', '{$_fosforoestandar2}', '{$_fosforoestandar3}', '{$_fosforoestandar4}', '{$_fosforoestandar5}', '{$_fosforopipeta1}', '{$_fosforopipeta2}', '{$_fosforopipeta3}', '{$_fosforopipeta4}', '{$_fosforopipeta5}', '{$_fosforovolcorrpipeta1}', '{$_fosforovolcorrpipeta2}', '{$_fosforovolcorrpipeta3}', '{$_fosforovolcorrpipeta4}', '{$_fosforovolcorrpipeta5}', '{$_fosforoincpip1}', '{$_fosforoincpip2}', '{$_fosforoincpip3}', '{$_fosforoincpip4}', '{$_fosforoincpip5}', '{$_fosforobalon1}', '{$_fosforobalon2}', '{$_fosforobalon3}', '{$_fosforobalon4}', '{$_fosforobalon5}', '{$_fosforodilinc1}', '{$_fosforodilinc2}', '{$_fosforodilinc3}', '{$_fosforodilinc4}', '{$_fosforodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027hierro VALUES('{$cs[0]['id']}', '{$_hierrocon1}', '{$_hierrocon2}', '{$_hierrocon3}', '{$_hierrocon4}', '{$_hierrocon5}', '{$_hierroinc1}', '{$_hierroinc2}', '{$_hierroinc3}', '{$_hierroinc4}', '{$_hierroinc5}', '{$_hierroincrel1}', '{$_hierroincrel2}', '{$_hierroincrel3}', '{$_hierroincrel4}', '{$_hierroincrel5}', '{$_hierroA11}', '{$_hierroA12}', '{$_hierroA13}', '{$_hierroA14}', '{$_hierroA15}', '{$_hierroA21}', '{$_hierroA22}', '{$_hierroA23}', '{$_hierroA24}', '{$_hierroA25}', '{$_hierroA31}', '{$_hierroA32}', '{$_hierroA33}', '{$_hierroA34}', '{$_hierroA35}', '{$_hierroA41}', '{$_hierroA42}', '{$_hierroA43}', '{$_hierroA44}', '{$_hierroA45}', '{$_hierroC11}', '{$_hierroC12}', '{$_hierroC13}', '{$_hierroC14}', '{$_hierroC15}', '{$_hierroC21}', '{$_hierroC22}', '{$_hierroC23}', '{$_hierroC24}', '{$_hierroC25}', '{$_hierroC31}', '{$_hierroC32}', '{$_hierroC33}', '{$_hierroC34}', '{$_hierroC35}', '{$_hierroC41}', '{$_hierroC42}', '{$_hierroC43}', '{$_hierroC44}', '{$_hierroC45}', '{$_hierrocp1}', '{$_hierrocp2}', '{$_hierrocp3}', '{$_hierrocp4}', '{$_hierrocp5}', '{$_hierroa1}', '{$_hierroa2}', '{$_hierroa3}', '{$_hierroa4}', '{$_hierroa5}', '{$_hierroa6}', '{$_hierroa7}', '{$_hierroa8}', '{$_hierroa9}', '{$_hierroa10}', '{$_hierroa11}', '{$_hierroa12}', '{$_hierroa13}', '{$_hierroa14}', '{$_hierroa15}', '{$_hierroa16}', '{$_hierroa17}', '{$_hierroa18}', '{$_hierroa19}', '{$_hierroa20}', '{$_hierroc1}', '{$_hierroc2}', '{$_hierroc3}', '{$_hierroc4}', '{$_hierroc5}', '{$_hierroc6}', '{$_hierroc7}', '{$_hierroc8}', '{$_hierroc9}', '{$_hierroc10}', '{$_hierroc11}', '{$_hierroc12}', '{$_hierroc13}', '{$_hierroc14}', '{$_hierroc15}', '{$_hierroc16}', '{$_hierroc17}', '{$_hierroc18}', '{$_hierroc19}', '{$_hierroc20}', '{$_hierromaxinc}', '{$_hierromaxincrel}', '{$_hierronummedxmuestra}', '{$_hierronummedxpuncurva}', '{$_hierronummedcurva}', '{$_hierroconmuestra1}', '{$_hierroconmuestra2}', '{$_hierroprompatcalibracion}', '{$_hierrodesvestresidual}', '{$_hierrodesvestcurvacali}', '{$_hierronp}', '{$_hierrolote}', '{$_hierrovence}', '{$_hierrocc1}', '{$_hierrocc2}', '{$_hierroest1}', '{$_hierroest2}', '{$_hierroccest1}', '{$_hierroccest2}', '{$_hierrovalor}', '{$_hierroincexp}', '{$_hierrok}', '{$_hierroincest}', '{$_hierroestandar1}', '{$_hierroestandar2}', '{$_hierroestandar3}', '{$_hierroestandar4}', '{$_hierroestandar5}', '{$_hierropipeta1}', '{$_hierropipeta2}', '{$_hierropipeta3}', '{$_hierropipeta4}', '{$_hierropipeta5}', '{$_hierrovolcorrpipeta1}', '{$_hierrovolcorrpipeta2}', '{$_hierrovolcorrpipeta3}', '{$_hierrovolcorrpipeta4}', '{$_hierrovolcorrpipeta5}', '{$_hierroincpip1}', '{$_hierroincpip2}', '{$_hierroincpip3}', '{$_hierroincpip4}', '{$_hierroincpip5}', '{$_hierrobalon1}', '{$_hierrobalon2}', '{$_hierrobalon3}', '{$_hierrobalon4}', '{$_hierrobalon5}', '{$_hierrodilinc1}', '{$_hierrodilinc2}', '{$_hierrodilinc3}', '{$_hierrodilinc4}', '{$_hierrodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027magnesio VALUES('{$cs[0]['id']}', '{$_magnesiocon1}', '{$_magnesiocon2}', '{$_magnesiocon3}', '{$_magnesiocon4}', '{$_magnesiocon5}', '{$_magnesioinc1}', '{$_magnesioinc2}', '{$_magnesioinc3}', '{$_magnesioinc4}', '{$_magnesioinc5}', '{$_magnesioincrel1}', '{$_magnesioincrel2}', '{$_magnesioincrel3}', '{$_magnesioincrel4}', '{$_magnesioincrel5}', '{$_magnesioA11}', '{$_magnesioA12}', '{$_magnesioA13}', '{$_magnesioA14}', '{$_magnesioA15}', '{$_magnesioA21}', '{$_magnesioA22}', '{$_magnesioA23}', '{$_magnesioA24}', '{$_magnesioA25}', '{$_magnesioA31}', '{$_magnesioA32}', '{$_magnesioA33}', '{$_magnesioA34}', '{$_magnesioA35}', '{$_magnesioA41}', '{$_magnesioA42}', '{$_magnesioA43}', '{$_magnesioA44}', '{$_magnesioA45}', '{$_magnesioC11}', '{$_magnesioC12}', '{$_magnesioC13}', '{$_magnesioC14}', '{$_magnesioC15}', '{$_magnesioC21}', '{$_magnesioC22}', '{$_magnesioC23}', '{$_magnesioC24}', '{$_magnesioC25}', '{$_magnesioC31}', '{$_magnesioC32}', '{$_magnesioC33}', '{$_magnesioC34}', '{$_magnesioC35}', '{$_magnesioC41}', '{$_magnesioC42}', '{$_magnesioC43}', '{$_magnesioC44}', '{$_magnesioC45}', '{$_magnesiocp1}', '{$_magnesiocp2}', '{$_magnesiocp3}', '{$_magnesiocp4}', '{$_magnesiocp5}', '{$_magnesioa1}', '{$_magnesioa2}', '{$_magnesioa3}', '{$_magnesioa4}', '{$_magnesioa5}', '{$_magnesioa6}', '{$_magnesioa7}', '{$_magnesioa8}', '{$_magnesioa9}', '{$_magnesioa10}', '{$_magnesioa11}', '{$_magnesioa12}', '{$_magnesioa13}', '{$_magnesioa14}', '{$_magnesioa15}', '{$_magnesioa16}', '{$_magnesioa17}', '{$_magnesioa18}', '{$_magnesioa19}', '{$_magnesioa20}', '{$_magnesioc1}', '{$_magnesioc2}', '{$_magnesioc3}', '{$_magnesioc4}', '{$_magnesioc5}', '{$_magnesioc6}', '{$_magnesioc7}', '{$_magnesioc8}', '{$_magnesioc9}', '{$_magnesioc10}', '{$_magnesioc11}', '{$_magnesioc12}', '{$_magnesioc13}', '{$_magnesioc14}', '{$_magnesioc15}', '{$_magnesioc16}', '{$_magnesioc17}', '{$_magnesioc18}', '{$_magnesioc19}', '{$_magnesioc20}', '{$_magnesiomaxinc}', '{$_magnesiomaxincrel}', '{$_magnesionummedxmuestra}', '{$_magnesionummedxpuncurva}', '{$_magnesionummedcurva}', '{$_magnesioconmuestra1}', '{$_magnesioconmuestra2}', '{$_magnesioprompatcalibracion}', '{$_magnesiodesvestresidual}', '{$_magnesiodesvestcurvacali}', '{$_magnesionp}', '{$_magnesiolote}', '{$_magnesiovence}', '{$_magnesiocc1}', '{$_magnesiocc2}', '{$_magnesioest1}', '{$_magnesioest2}', '{$_magnesioccest1}', '{$_magnesioccest2}', '{$_magnesiovalor}', '{$_magnesioincexp}', '{$_magnesiok}', '{$_magnesioincest}', '{$_magnesioestandar1}', '{$_magnesioestandar2}', '{$_magnesioestandar3}', '{$_magnesioestandar4}', '{$_magnesioestandar5}', '{$_magnesiopipeta1}', '{$_magnesiopipeta2}', '{$_magnesiopipeta3}', '{$_magnesiopipeta4}', '{$_magnesiopipeta5}', '{$_magnesiovolcorrpipeta1}', '{$_magnesiovolcorrpipeta2}', '{$_magnesiovolcorrpipeta3}', '{$_magnesiovolcorrpipeta4}', '{$_magnesiovolcorrpipeta5}', '{$_magnesioincpip1}', '{$_magnesioincpip2}', '{$_magnesioincpip3}', '{$_magnesioincpip4}', '{$_magnesioincpip5}', '{$_magnesiobalon1}', '{$_magnesiobalon2}', '{$_magnesiobalon3}', '{$_magnesiobalon4}', '{$_magnesiobalon5}', '{$_magnesiodilinc1}', '{$_magnesiodilinc2}', '{$_magnesiodilinc3}', '{$_magnesiodilinc4}', '{$_magnesiodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027manganeso VALUES('{$cs[0]['id']}', '{$_manganesocon1}', '{$_manganesocon2}', '{$_manganesocon3}', '{$_manganesocon4}', '{$_manganesocon5}', '{$_manganesoinc1}', '{$_manganesoinc2}', '{$_manganesoinc3}', '{$_manganesoinc4}', '{$_manganesoinc5}', '{$_manganesoincrel1}', '{$_manganesoincrel2}', '{$_manganesoincrel3}', '{$_manganesoincrel4}', '{$_manganesoincrel5}', '{$_manganesoA11}', '{$_manganesoA12}', '{$_manganesoA13}', '{$_manganesoA14}', '{$_manganesoA15}', '{$_manganesoA21}', '{$_manganesoA22}', '{$_manganesoA23}', '{$_manganesoA24}', '{$_manganesoA25}', '{$_manganesoA31}', '{$_manganesoA32}', '{$_manganesoA33}', '{$_manganesoA34}', '{$_manganesoA35}', '{$_manganesoA41}', '{$_manganesoA42}', '{$_manganesoA43}', '{$_manganesoA44}', '{$_manganesoA45}', '{$_manganesoC11}', '{$_manganesoC12}', '{$_manganesoC13}', '{$_manganesoC14}', '{$_manganesoC15}', '{$_manganesoC21}', '{$_manganesoC22}', '{$_manganesoC23}', '{$_manganesoC24}', '{$_manganesoC25}', '{$_manganesoC31}', '{$_manganesoC32}', '{$_manganesoC33}', '{$_manganesoC34}', '{$_manganesoC35}', '{$_manganesoC41}', '{$_manganesoC42}', '{$_manganesoC43}', '{$_manganesoC44}', '{$_manganesoC45}', '{$_manganesocp1}', '{$_manganesocp2}', '{$_manganesocp3}', '{$_manganesocp4}', '{$_manganesocp5}', '{$_manganesoa1}', '{$_manganesoa2}', '{$_manganesoa3}', '{$_manganesoa4}', '{$_manganesoa5}', '{$_manganesoa6}', '{$_manganesoa7}', '{$_manganesoa8}', '{$_manganesoa9}', '{$_manganesoa10}', '{$_manganesoa11}', '{$_manganesoa12}', '{$_manganesoa13}', '{$_manganesoa14}', '{$_manganesoa15}', '{$_manganesoa16}', '{$_manganesoa17}', '{$_manganesoa18}', '{$_manganesoa19}', '{$_manganesoa20}', '{$_manganesoc1}', '{$_manganesoc2}', '{$_manganesoc3}', '{$_manganesoc4}', '{$_manganesoc5}', '{$_manganesoc6}', '{$_manganesoc7}', '{$_manganesoc8}', '{$_manganesoc9}', '{$_manganesoc10}', '{$_manganesoc11}', '{$_manganesoc12}', '{$_manganesoc13}', '{$_manganesoc14}', '{$_manganesoc15}', '{$_manganesoc16}', '{$_manganesoc17}', '{$_manganesoc18}', '{$_manganesoc19}', '{$_manganesoc20}', '{$_manganesomaxinc}', '{$_manganesomaxincrel}', '{$_manganesonummedxmuestra}', '{$_manganesonummedxpuncurva}', '{$_manganesonummedcurva}', '{$_manganesoconmuestra1}', '{$_manganesoconmuestra2}', '{$_manganesoprompatcalibracion}', '{$_manganesodesvestresidual}', '{$_manganesodesvestcurvacali}', '{$_manganesonp}', '{$_manganesolote}', '{$_manganesovence}', '{$_manganesocc1}', '{$_manganesocc2}', '{$_manganesoest1}', '{$_manganesoest2}', '{$_manganesoccest1}', '{$_manganesoccest2}', '{$_manganesovalor}', '{$_manganesoincexp}', '{$_manganesok}', '{$_manganesoincest}', '{$_manganesoestandar1}', '{$_manganesoestandar2}', '{$_manganesoestandar3}', '{$_manganesoestandar4}', '{$_manganesoestandar5}', '{$_manganesopipeta1}', '{$_manganesopipeta2}', '{$_manganesopipeta3}', '{$_manganesopipeta4}', '{$_manganesopipeta5}', '{$_manganesovolcorrpipeta1}', '{$_manganesovolcorrpipeta2}', '{$_manganesovolcorrpipeta3}', '{$_manganesovolcorrpipeta4}', '{$_manganesovolcorrpipeta5}', '{$_manganesoincpip1}', '{$_manganesoincpip2}', '{$_manganesoincpip3}', '{$_manganesoincpip4}', '{$_manganesoincpip5}', '{$_manganesobalon1}', '{$_manganesobalon2}', '{$_manganesobalon3}', '{$_manganesobalon4}', '{$_manganesobalon5}', '{$_manganesodilinc1}', '{$_manganesodilinc2}', '{$_manganesodilinc3}', '{$_manganesodilinc4}', '{$_manganesodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027potasio VALUES('{$cs[0]['id']}', '{$_potasiocon1}', '{$_potasiocon2}', '{$_potasiocon3}', '{$_potasiocon4}', '{$_potasiocon5}', '{$_potasioinc1}', '{$_potasioinc2}', '{$_potasioinc3}', '{$_potasioinc4}', '{$_potasioinc5}', '{$_potasioincrel1}', '{$_potasioincrel2}', '{$_potasioincrel3}', '{$_potasioincrel4}', '{$_potasioincrel5}', '{$_potasioA11}', '{$_potasioA12}', '{$_potasioA13}', '{$_potasioA14}', '{$_potasioA15}', '{$_potasioA21}', '{$_potasioA22}', '{$_potasioA23}', '{$_potasioA24}', '{$_potasioA25}', '{$_potasioA31}', '{$_potasioA32}', '{$_potasioA33}', '{$_potasioA34}', '{$_potasioA35}', '{$_potasioA41}', '{$_potasioA42}', '{$_potasioA43}', '{$_potasioA44}', '{$_potasioA45}', '{$_potasioC11}', '{$_potasioC12}', '{$_potasioC13}', '{$_potasioC14}', '{$_potasioC15}', '{$_potasioC21}', '{$_potasioC22}', '{$_potasioC23}', '{$_potasioC24}', '{$_potasioC25}', '{$_potasioC31}', '{$_potasioC32}', '{$_potasioC33}', '{$_potasioC34}', '{$_potasioC35}', '{$_potasioC41}', '{$_potasioC42}', '{$_potasioC43}', '{$_potasioC44}', '{$_potasioC45}', '{$_potasiocp1}', '{$_potasiocp2}', '{$_potasiocp3}', '{$_potasiocp4}', '{$_potasiocp5}', '{$_potasioa1}', '{$_potasioa2}', '{$_potasioa3}', '{$_potasioa4}', '{$_potasioa5}', '{$_potasioa6}', '{$_potasioa7}', '{$_potasioa8}', '{$_potasioa9}', '{$_potasioa10}', '{$_potasioa11}', '{$_potasioa12}', '{$_potasioa13}', '{$_potasioa14}', '{$_potasioa15}', '{$_potasioa16}', '{$_potasioa17}', '{$_potasioa18}', '{$_potasioa19}', '{$_potasioa20}', '{$_potasioc1}', '{$_potasioc2}', '{$_potasioc3}', '{$_potasioc4}', '{$_potasioc5}', '{$_potasioc6}', '{$_potasioc7}', '{$_potasioc8}', '{$_potasioc9}', '{$_potasioc10}', '{$_potasioc11}', '{$_potasioc12}', '{$_potasioc13}', '{$_potasioc14}', '{$_potasioc15}', '{$_potasioc16}', '{$_potasioc17}', '{$_potasioc18}', '{$_potasioc19}', '{$_potasioc20}', '{$_potasiomaxinc}', '{$_potasiomaxincrel}', '{$_potasionummedxmuestra}', '{$_potasionummedxpuncurva}', '{$_potasionummedcurva}', '{$_potasioconmuestra1}', '{$_potasioconmuestra2}', '{$_potasioprompatcalibracion}', '{$_potasiodesvestresidual}', '{$_potasiodesvestcurvacali}', '{$_potasionp}', '{$_potasiolote}', '{$_potasiovence}', '{$_potasiocc1}', '{$_potasiocc2}', '{$_potasioest1}', '{$_potasioest2}', '{$_potasioccest1}', '{$_potasioccest2}', '{$_potasiovalor}', '{$_potasioincexp}', '{$_potasiok}', '{$_potasioincest}', '{$_potasioestandar1}', '{$_potasioestandar2}', '{$_potasioestandar3}', '{$_potasioestandar4}', '{$_potasioestandar5}', '{$_potasiopipeta1}', '{$_potasiopipeta2}', '{$_potasiopipeta3}', '{$_potasiopipeta4}', '{$_potasiopipeta5}', '{$_potasiovolcorrpipeta1}', '{$_potasiovolcorrpipeta2}', '{$_potasiovolcorrpipeta3}', '{$_potasiovolcorrpipeta4}', '{$_potasiovolcorrpipeta5}', '{$_potasioincpip1}', '{$_potasioincpip2}', '{$_potasioincpip3}', '{$_potasioincpip4}', '{$_potasioincpip5}', '{$_potasiobalon1}', '{$_potasiobalon2}', '{$_potasiobalon3}', '{$_potasiobalon4}', '{$_potasiobalon5}', '{$_potasiodilinc1}', '{$_potasiodilinc2}', '{$_potasiodilinc3}', '{$_potasiodilinc4}', '{$_potasiodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027zinc VALUES('{$cs[0]['id']}', '{$_zinccon1}', '{$_zinccon2}', '{$_zinccon3}', '{$_zinccon4}', '{$_zinccon5}', '{$_zincinc1}', '{$_zincinc2}', '{$_zincinc3}', '{$_zincinc4}', '{$_zincinc5}', '{$_zincincrel1}', '{$_zincincrel2}', '{$_zincincrel3}', '{$_zincincrel4}', '{$_zincincrel5}', '{$_zincA11}', '{$_zincA12}', '{$_zincA13}', '{$_zincA14}', '{$_zincA15}', '{$_zincA21}', '{$_zincA22}', '{$_zincA23}', '{$_zincA24}', '{$_zincA25}', '{$_zincA31}', '{$_zincA32}', '{$_zincA33}', '{$_zincA34}', '{$_zincA35}', '{$_zincA41}', '{$_zincA42}', '{$_zincA43}', '{$_zincA44}', '{$_zincA45}', '{$_zincC11}', '{$_zincC12}', '{$_zincC13}', '{$_zincC14}', '{$_zincC15}', '{$_zincC21}', '{$_zincC22}', '{$_zincC23}', '{$_zincC24}', '{$_zincC25}', '{$_zincC31}', '{$_zincC32}', '{$_zincC33}', '{$_zincC34}', '{$_zincC35}', '{$_zincC41}', '{$_zincC42}', '{$_zincC43}', '{$_zincC44}', '{$_zincC45}', '{$_zinccp1}', '{$_zinccp2}', '{$_zinccp3}', '{$_zinccp4}', '{$_zinccp5}', '{$_zinca1}', '{$_zinca2}', '{$_zinca3}', '{$_zinca4}', '{$_zinca5}', '{$_zinca6}', '{$_zinca7}', '{$_zinca8}', '{$_zinca9}', '{$_zinca10}', '{$_zinca11}', '{$_zinca12}', '{$_zinca13}', '{$_zinca14}', '{$_zinca15}', '{$_zinca16}', '{$_zinca17}', '{$_zinca18}', '{$_zinca19}', '{$_zinca20}', '{$_zincc1}', '{$_zincc2}', '{$_zincc3}', '{$_zincc4}', '{$_zincc5}', '{$_zincc6}', '{$_zincc7}', '{$_zincc8}', '{$_zincc9}', '{$_zincc10}', '{$_zincc11}', '{$_zincc12}', '{$_zincc13}', '{$_zincc14}', '{$_zincc15}', '{$_zincc16}', '{$_zincc17}', '{$_zincc18}', '{$_zincc19}', '{$_zincc20}', '{$_zincmaxinc}', '{$_zincmaxincrel}', '{$_zincnummedxmuestra}', '{$_zincnummedxpuncurva}', '{$_zincnummedcurva}', '{$_zincconmuestra1}', '{$_zincconmuestra2}', '{$_zincprompatcalibracion}', '{$_zincdesvestresidual}', '{$_zincdesvestcurvacali}', '{$_zincnp}', '{$_zinclote}', '{$_zincvence}', '{$_zinccc1}', '{$_zinccc2}', '{$_zincest1}', '{$_zincest2}', '{$_zincccest1}', '{$_zincccest2}', '{$_zincvalor}', '{$_zincincexp}', '{$_zinck}', '{$_zincincest}', '{$_zincestandar1}', '{$_zincestandar2}', '{$_zincestandar3}', '{$_zincestandar4}', '{$_zincestandar5}', '{$_zincpipeta1}', '{$_zincpipeta2}', '{$_zincpipeta3}', '{$_zincpipeta4}', '{$_zincpipeta5}', '{$_zincvolcorrpipeta1}', '{$_zincvolcorrpipeta2}', '{$_zincvolcorrpipeta3}', '{$_zincvolcorrpipeta4}', '{$_zincvolcorrpipeta5}', '{$_zincincpip1}', '{$_zincincpip2}', '{$_zincincpip3}', '{$_zincincpip4}', '{$_zincincpip5}', '{$_zincbalon1}', '{$_zincbalon2}', '{$_zincbalon3}', '{$_zincbalon4}', '{$_zincbalon5}', '{$_zincdilinc1}', '{$_zincdilinc2}', '{$_zincdilinc3}', '{$_zincdilinc4}', '{$_zincdilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027nitrogeno VALUES('{$cs[0]['id']}', '{$_nitrogenocon1}', '{$_nitrogenocon2}', '{$_nitrogenocon3}', '{$_nitrogenocon4}', '{$_nitrogenocon5}', '{$_nitrogenoinc1}', '{$_nitrogenoinc2}', '{$_nitrogenoinc3}', '{$_nitrogenoinc4}', '{$_nitrogenoinc5}', '{$_nitrogenoincrel1}', '{$_nitrogenoincrel2}', '{$_nitrogenoincrel3}', '{$_nitrogenoincrel4}', '{$_nitrogenoincrel5}', '{$_nitrogenoA11}', '{$_nitrogenoA12}', '{$_nitrogenoA13}', '{$_nitrogenoA14}', '{$_nitrogenoA15}', '{$_nitrogenoA21}', '{$_nitrogenoA22}', '{$_nitrogenoA23}', '{$_nitrogenoA24}', '{$_nitrogenoA25}', '{$_nitrogenoA31}', '{$_nitrogenoA32}', '{$_nitrogenoA33}', '{$_nitrogenoA34}', '{$_nitrogenoA35}', '{$_nitrogenoA41}', '{$_nitrogenoA42}', '{$_nitrogenoA43}', '{$_nitrogenoA44}', '{$_nitrogenoA45}', '{$_nitrogenoC11}', '{$_nitrogenoC12}', '{$_nitrogenoC13}', '{$_nitrogenoC14}', '{$_nitrogenoC15}', '{$_nitrogenoC21}', '{$_nitrogenoC22}', '{$_nitrogenoC23}', '{$_nitrogenoC24}', '{$_nitrogenoC25}', '{$_nitrogenoC31}', '{$_nitrogenoC32}', '{$_nitrogenoC33}', '{$_nitrogenoC34}', '{$_nitrogenoC35}', '{$_nitrogenoC41}', '{$_nitrogenoC42}', '{$_nitrogenoC43}', '{$_nitrogenoC44}', '{$_nitrogenoC45}', '{$_nitrogenocp1}', '{$_nitrogenocp2}', '{$_nitrogenocp3}', '{$_nitrogenocp4}', '{$_nitrogenocp5}', '{$_nitrogenoa1}', '{$_nitrogenoa2}', '{$_nitrogenoa3}', '{$_nitrogenoa4}', '{$_nitrogenoa5}', '{$_nitrogenoa6}', '{$_nitrogenoa7}', '{$_nitrogenoa8}', '{$_nitrogenoa9}', '{$_nitrogenoa10}', '{$_nitrogenoa11}', '{$_nitrogenoa12}', '{$_nitrogenoa13}', '{$_nitrogenoa14}', '{$_nitrogenoa15}', '{$_nitrogenoa16}', '{$_nitrogenoa17}', '{$_nitrogenoa18}', '{$_nitrogenoa19}', '{$_nitrogenoa20}', '{$_nitrogenoc1}', '{$_nitrogenoc2}', '{$_nitrogenoc3}', '{$_nitrogenoc4}', '{$_nitrogenoc5}', '{$_nitrogenoc6}', '{$_nitrogenoc7}', '{$_nitrogenoc8}', '{$_nitrogenoc9}', '{$_nitrogenoc10}', '{$_nitrogenoc11}', '{$_nitrogenoc12}', '{$_nitrogenoc13}', '{$_nitrogenoc14}', '{$_nitrogenoc15}', '{$_nitrogenoc16}', '{$_nitrogenoc17}', '{$_nitrogenoc18}', '{$_nitrogenoc19}', '{$_nitrogenoc20}', '{$_nitrogenomaxinc}', '{$_nitrogenomaxincrel}', '{$_nitrogenonummedxmuestra}', '{$_nitrogenonummedxpuncurva}', '{$_nitrogenonummedcurva}', '{$_nitrogenoconmuestra1}', '{$_nitrogenoconmuestra2}', '{$_nitrogenoprompatcalibracion}', '{$_nitrogenodesvestresidual}', '{$_nitrogenodesvestcurvacali}', '{$_nitrogenonp}', '{$_nitrogenolote}', '{$_nitrogenovence}', '{$_nitrogenocc1}', '{$_nitrogenocc2}', '{$_nitrogenoest1}', '{$_nitrogenoest2}', '{$_nitrogenoccest1}', '{$_nitrogenoccest2}', '{$_nitrogenovalor}', '{$_nitrogenoincexp}', '{$_nitrogenok}', '{$_nitrogenoincest}', '{$_nitrogenoestandar1}', '{$_nitrogenoestandar2}', '{$_nitrogenoestandar3}', '{$_nitrogenoestandar4}', '{$_nitrogenoestandar5}', '{$_nitrogenopipeta1}', '{$_nitrogenopipeta2}', '{$_nitrogenopipeta3}', '{$_nitrogenopipeta4}', '{$_nitrogenopipeta5}', '{$_nitrogenovolcorrpipeta1}', '{$_nitrogenovolcorrpipeta2}', '{$_nitrogenovolcorrpipeta3}', '{$_nitrogenovolcorrpipeta4}', '{$_nitrogenovolcorrpipeta5}', '{$_nitrogenoincpip1}', '{$_nitrogenoincpip2}', '{$_nitrogenoincpip3}', '{$_nitrogenoincpip4}', '{$_nitrogenoincpip5}', '{$_nitrogenobalon1}', '{$_nitrogenobalon2}', '{$_nitrogenobalon3}', '{$_nitrogenobalon4}', '{$_nitrogenobalon5}', '{$_nitrogenodilinc1}', '{$_nitrogenodilinc2}', '{$_nitrogenodilinc3}', '{$_nitrogenodilinc4}', '{$_nitrogenodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027azufre VALUES('{$cs[0]['id']}', '{$_azufrecon1}', '{$_azufrecon2}', '{$_azufrecon3}', '{$_azufrecon4}', '{$_azufrecon5}', '{$_azufreinc1}', '{$_azufreinc2}', '{$_azufreinc3}', '{$_azufreinc4}', '{$_azufreinc5}', '{$_azufreincrel1}', '{$_azufreincrel2}', '{$_azufreincrel3}', '{$_azufreincrel4}', '{$_azufreincrel5}', '{$_azufreA11}', '{$_azufreA12}', '{$_azufreA13}', '{$_azufreA14}', '{$_azufreA15}', '{$_azufreA21}', '{$_azufreA22}', '{$_azufreA23}', '{$_azufreA24}', '{$_azufreA25}', '{$_azufreA31}', '{$_azufreA32}', '{$_azufreA33}', '{$_azufreA34}', '{$_azufreA35}', '{$_azufreA41}', '{$_azufreA42}', '{$_azufreA43}', '{$_azufreA44}', '{$_azufreA45}', '{$_azufreC11}', '{$_azufreC12}', '{$_azufreC13}', '{$_azufreC14}', '{$_azufreC15}', '{$_azufreC21}', '{$_azufreC22}', '{$_azufreC23}', '{$_azufreC24}', '{$_azufreC25}', '{$_azufreC31}', '{$_azufreC32}', '{$_azufreC33}', '{$_azufreC34}', '{$_azufreC35}', '{$_azufreC41}', '{$_azufreC42}', '{$_azufreC43}', '{$_azufreC44}', '{$_azufreC45}', '{$_azufrecp1}', '{$_azufrecp2}', '{$_azufrecp3}', '{$_azufrecp4}', '{$_azufrecp5}', '{$_azufrea1}', '{$_azufrea2}', '{$_azufrea3}', '{$_azufrea4}', '{$_azufrea5}', '{$_azufrea6}', '{$_azufrea7}', '{$_azufrea8}', '{$_azufrea9}', '{$_azufrea10}', '{$_azufrea11}', '{$_azufrea12}', '{$_azufrea13}', '{$_azufrea14}', '{$_azufrea15}', '{$_azufrea16}', '{$_azufrea17}', '{$_azufrea18}', '{$_azufrea19}', '{$_azufrea20}', '{$_azufrec1}', '{$_azufrec2}', '{$_azufrec3}', '{$_azufrec4}', '{$_azufrec5}', '{$_azufrec6}', '{$_azufrec7}', '{$_azufrec8}', '{$_azufrec9}', '{$_azufrec10}', '{$_azufrec11}', '{$_azufrec12}', '{$_azufrec13}', '{$_azufrec14}', '{$_azufrec15}', '{$_azufrec16}', '{$_azufrec17}', '{$_azufrec18}', '{$_azufrec19}', '{$_azufrec20}', '{$_azufremaxinc}', '{$_azufremaxincrel}', '{$_azufrenummedxmuestra}', '{$_azufrenummedxpuncurva}', '{$_azufrenummedcurva}', '{$_azufreconmuestra1}', '{$_azufreconmuestra2}', '{$_azufreprompatcalibracion}', '{$_azufredesvestresidual}', '{$_azufredesvestcurvacali}', '{$_azufrenp}', '{$_azufrelote}', '{$_azufrevence}', '{$_azufrecc1}', '{$_azufrecc2}', '{$_azufreest1}', '{$_azufreest2}', '{$_azufreccest1}', '{$_azufreccest2}', '{$_azufrevalor}', '{$_azufreincexp}', '{$_azufrek}', '{$_azufreincest}', '{$_azufreestandar1}', '{$_azufreestandar2}', '{$_azufreestandar3}', '{$_azufreestandar4}', '{$_azufreestandar5}', '{$_azufrepipeta1}', '{$_azufrepipeta2}', '{$_azufrepipeta3}', '{$_azufrepipeta4}', '{$_azufrepipeta5}', '{$_azufrevolcorrpipeta1}', '{$_azufrevolcorrpipeta2}', '{$_azufrevolcorrpipeta3}', '{$_azufrevolcorrpipeta4}', '{$_azufrevolcorrpipeta5}', '{$_azufreincpip1}', '{$_azufreincpip2}', '{$_azufreincpip3}', '{$_azufreincpip4}', '{$_azufreincpip5}', '{$_azufrebalon1}', '{$_azufrebalon2}', '{$_azufrebalon3}', '{$_azufrebalon4}', '{$_azufrebalon5}', '{$_azufredilinc1}', '{$_azufredilinc2}', '{$_azufredilinc3}', '{$_azufredilinc4}', '{$_azufredilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027arsenico VALUES('{$cs[0]['id']}', '{$_arsenicocon1}', '{$_arsenicocon2}', '{$_arsenicocon3}', '{$_arsenicocon4}', '{$_arsenicocon5}', '{$_arsenicoinc1}', '{$_arsenicoinc2}', '{$_arsenicoinc3}', '{$_arsenicoinc4}', '{$_arsenicoinc5}', '{$_arsenicoincrel1}', '{$_arsenicoincrel2}', '{$_arsenicoincrel3}', '{$_arsenicoincrel4}', '{$_arsenicoincrel5}', '{$_arsenicoA11}', '{$_arsenicoA12}', '{$_arsenicoA13}', '{$_arsenicoA14}', '{$_arsenicoA15}', '{$_arsenicoA21}', '{$_arsenicoA22}', '{$_arsenicoA23}', '{$_arsenicoA24}', '{$_arsenicoA25}', '{$_arsenicoA31}', '{$_arsenicoA32}', '{$_arsenicoA33}', '{$_arsenicoA34}', '{$_arsenicoA35}', '{$_arsenicoA41}', '{$_arsenicoA42}', '{$_arsenicoA43}', '{$_arsenicoA44}', '{$_arsenicoA45}', '{$_arsenicoC11}', '{$_arsenicoC12}', '{$_arsenicoC13}', '{$_arsenicoC14}', '{$_arsenicoC15}', '{$_arsenicoC21}', '{$_arsenicoC22}', '{$_arsenicoC23}', '{$_arsenicoC24}', '{$_arsenicoC25}', '{$_arsenicoC31}', '{$_arsenicoC32}', '{$_arsenicoC33}', '{$_arsenicoC34}', '{$_arsenicoC35}', '{$_arsenicoC41}', '{$_arsenicoC42}', '{$_arsenicoC43}', '{$_arsenicoC44}', '{$_arsenicoC45}', '{$_arsenicocp1}', '{$_arsenicocp2}', '{$_arsenicocp3}', '{$_arsenicocp4}', '{$_arsenicocp5}', '{$_arsenicoa1}', '{$_arsenicoa2}', '{$_arsenicoa3}', '{$_arsenicoa4}', '{$_arsenicoa5}', '{$_arsenicoa6}', '{$_arsenicoa7}', '{$_arsenicoa8}', '{$_arsenicoa9}', '{$_arsenicoa10}', '{$_arsenicoa11}', '{$_arsenicoa12}', '{$_arsenicoa13}', '{$_arsenicoa14}', '{$_arsenicoa15}', '{$_arsenicoa16}', '{$_arsenicoa17}', '{$_arsenicoa18}', '{$_arsenicoa19}', '{$_arsenicoa20}', '{$_arsenicoc1}', '{$_arsenicoc2}', '{$_arsenicoc3}', '{$_arsenicoc4}', '{$_arsenicoc5}', '{$_arsenicoc6}', '{$_arsenicoc7}', '{$_arsenicoc8}', '{$_arsenicoc9}', '{$_arsenicoc10}', '{$_arsenicoc11}', '{$_arsenicoc12}', '{$_arsenicoc13}', '{$_arsenicoc14}', '{$_arsenicoc15}', '{$_arsenicoc16}', '{$_arsenicoc17}', '{$_arsenicoc18}', '{$_arsenicoc19}', '{$_arsenicoc20}', '{$_arsenicomaxinc}', '{$_arsenicomaxincrel}', '{$_arseniconummedxmuestra}', '{$_arseniconummedxpuncurva}', '{$_arseniconummedcurva}', '{$_arsenicoconmuestra1}', '{$_arsenicoconmuestra2}', '{$_arsenicoprompatcalibracion}', '{$_arsenicodesvestresidual}', '{$_arsenicodesvestcurvacali}', '{$_arseniconp}', '{$_arsenicolote}', '{$_arsenicovence}', '{$_arsenicocc1}', '{$_arsenicocc2}', '{$_arsenicoest1}', '{$_arsenicoest2}', '{$_arsenicoccest1}', '{$_arsenicoccest2}', '{$_arsenicovalor}', '{$_arsenicoincexp}', '{$_arsenicok}', '{$_arsenicoincest}', '{$_arsenicoestandar1}', '{$_arsenicoestandar2}', '{$_arsenicoestandar3}', '{$_arsenicoestandar4}', '{$_arsenicoestandar5}', '{$_arsenicopipeta1}', '{$_arsenicopipeta2}', '{$_arsenicopipeta3}', '{$_arsenicopipeta4}', '{$_arsenicopipeta5}', '{$_arsenicovolcorrpipeta1}', '{$_arsenicovolcorrpipeta2}', '{$_arsenicovolcorrpipeta3}', '{$_arsenicovolcorrpipeta4}', '{$_arsenicovolcorrpipeta5}', '{$_arsenicoincpip1}', '{$_arsenicoincpip2}', '{$_arsenicoincpip3}', '{$_arsenicoincpip4}', '{$_arsenicoincpip5}', '{$_arsenicobalon1}', '{$_arsenicobalon2}', '{$_arsenicobalon3}', '{$_arsenicobalon4}', '{$_arsenicobalon5}', '{$_arsenicodilinc1}', '{$_arsenicodilinc2}', '{$_arsenicodilinc3}', '{$_arsenicodilinc4}', '{$_arsenicodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027cadmio VALUES('{$cs[0]['id']}', '{$_cadmiocon1}', '{$_cadmiocon2}', '{$_cadmiocon3}', '{$_cadmiocon4}', '{$_cadmiocon5}', '{$_cadmioinc1}', '{$_cadmioinc2}', '{$_cadmioinc3}', '{$_cadmioinc4}', '{$_cadmioinc5}', '{$_cadmioincrel1}', '{$_cadmioincrel2}', '{$_cadmioincrel3}', '{$_cadmioincrel4}', '{$_cadmioincrel5}', '{$_cadmioA11}', '{$_cadmioA12}', '{$_cadmioA13}', '{$_cadmioA14}', '{$_cadmioA15}', '{$_cadmioA21}', '{$_cadmioA22}', '{$_cadmioA23}', '{$_cadmioA24}', '{$_cadmioA25}', '{$_cadmioA31}', '{$_cadmioA32}', '{$_cadmioA33}', '{$_cadmioA34}', '{$_cadmioA35}', '{$_cadmioA41}', '{$_cadmioA42}', '{$_cadmioA43}', '{$_cadmioA44}', '{$_cadmioA45}', '{$_cadmioC11}', '{$_cadmioC12}', '{$_cadmioC13}', '{$_cadmioC14}', '{$_cadmioC15}', '{$_cadmioC21}', '{$_cadmioC22}', '{$_cadmioC23}', '{$_cadmioC24}', '{$_cadmioC25}', '{$_cadmioC31}', '{$_cadmioC32}', '{$_cadmioC33}', '{$_cadmioC34}', '{$_cadmioC35}', '{$_cadmioC41}', '{$_cadmioC42}', '{$_cadmioC43}', '{$_cadmioC44}', '{$_cadmioC45}', '{$_cadmiocp1}', '{$_cadmiocp2}', '{$_cadmiocp3}', '{$_cadmiocp4}', '{$_cadmiocp5}', '{$_cadmioa1}', '{$_cadmioa2}', '{$_cadmioa3}', '{$_cadmioa4}', '{$_cadmioa5}', '{$_cadmioa6}', '{$_cadmioa7}', '{$_cadmioa8}', '{$_cadmioa9}', '{$_cadmioa10}', '{$_cadmioa11}', '{$_cadmioa12}', '{$_cadmioa13}', '{$_cadmioa14}', '{$_cadmioa15}', '{$_cadmioa16}', '{$_cadmioa17}', '{$_cadmioa18}', '{$_cadmioa19}', '{$_cadmioa20}', '{$_cadmioc1}', '{$_cadmioc2}', '{$_cadmioc3}', '{$_cadmioc4}', '{$_cadmioc5}', '{$_cadmioc6}', '{$_cadmioc7}', '{$_cadmioc8}', '{$_cadmioc9}', '{$_cadmioc10}', '{$_cadmioc11}', '{$_cadmioc12}', '{$_cadmioc13}', '{$_cadmioc14}', '{$_cadmioc15}', '{$_cadmioc16}', '{$_cadmioc17}', '{$_cadmioc18}', '{$_cadmioc19}', '{$_cadmioc20}', '{$_cadmiomaxinc}', '{$_cadmiomaxincrel}', '{$_cadmionummedxmuestra}', '{$_cadmionummedxpuncurva}', '{$_cadmionummedcurva}', '{$_cadmioconmuestra1}', '{$_cadmioconmuestra2}', '{$_cadmioprompatcalibracion}', '{$_cadmiodesvestresidual}', '{$_cadmiodesvestcurvacali}', '{$_cadmionp}', '{$_cadmiolote}', '{$_cadmiovence}', '{$_cadmiocc1}', '{$_cadmiocc2}', '{$_cadmioest1}', '{$_cadmioest2}', '{$_cadmioccest1}', '{$_cadmioccest2}', '{$_cadmiovalor}', '{$_cadmioincexp}', '{$_cadmiok}', '{$_cadmioincest}', '{$_cadmioestandar1}', '{$_cadmioestandar2}', '{$_cadmioestandar3}', '{$_cadmioestandar4}', '{$_cadmioestandar5}', '{$_cadmiopipeta1}', '{$_cadmiopipeta2}', '{$_cadmiopipeta3}', '{$_cadmiopipeta4}', '{$_cadmiopipeta5}', '{$_cadmiovolcorrpipeta1}', '{$_cadmiovolcorrpipeta2}', '{$_cadmiovolcorrpipeta3}', '{$_cadmiovolcorrpipeta4}', '{$_cadmiovolcorrpipeta5}', '{$_cadmioincpip1}', '{$_cadmioincpip2}', '{$_cadmioincpip3}', '{$_cadmioincpip4}', '{$_cadmioincpip5}', '{$_cadmiobalon1}', '{$_cadmiobalon2}', '{$_cadmiobalon3}', '{$_cadmiobalon4}', '{$_cadmiobalon5}', '{$_cadmiodilinc1}', '{$_cadmiodilinc2}', '{$_cadmiodilinc3}', '{$_cadmiodilinc4}', '{$_cadmiodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027cromo VALUES('{$cs[0]['id']}', '{$_cromocon1}', '{$_cromocon2}', '{$_cromocon3}', '{$_cromocon4}', '{$_cromocon5}', '{$_cromoinc1}', '{$_cromoinc2}', '{$_cromoinc3}', '{$_cromoinc4}', '{$_cromoinc5}', '{$_cromoincrel1}', '{$_cromoincrel2}', '{$_cromoincrel3}', '{$_cromoincrel4}', '{$_cromoincrel5}', '{$_cromoA11}', '{$_cromoA12}', '{$_cromoA13}', '{$_cromoA14}', '{$_cromoA15}', '{$_cromoA21}', '{$_cromoA22}', '{$_cromoA23}', '{$_cromoA24}', '{$_cromoA25}', '{$_cromoA31}', '{$_cromoA32}', '{$_cromoA33}', '{$_cromoA34}', '{$_cromoA35}', '{$_cromoA41}', '{$_cromoA42}', '{$_cromoA43}', '{$_cromoA44}', '{$_cromoA45}', '{$_cromoC11}', '{$_cromoC12}', '{$_cromoC13}', '{$_cromoC14}', '{$_cromoC15}', '{$_cromoC21}', '{$_cromoC22}', '{$_cromoC23}', '{$_cromoC24}', '{$_cromoC25}', '{$_cromoC31}', '{$_cromoC32}', '{$_cromoC33}', '{$_cromoC34}', '{$_cromoC35}', '{$_cromoC41}', '{$_cromoC42}', '{$_cromoC43}', '{$_cromoC44}', '{$_cromoC45}', '{$_cromocp1}', '{$_cromocp2}', '{$_cromocp3}', '{$_cromocp4}', '{$_cromocp5}', '{$_cromoa1}', '{$_cromoa2}', '{$_cromoa3}', '{$_cromoa4}', '{$_cromoa5}', '{$_cromoa6}', '{$_cromoa7}', '{$_cromoa8}', '{$_cromoa9}', '{$_cromoa10}', '{$_cromoa11}', '{$_cromoa12}', '{$_cromoa13}', '{$_cromoa14}', '{$_cromoa15}', '{$_cromoa16}', '{$_cromoa17}', '{$_cromoa18}', '{$_cromoa19}', '{$_cromoa20}', '{$_cromoc1}', '{$_cromoc2}', '{$_cromoc3}', '{$_cromoc4}', '{$_cromoc5}', '{$_cromoc6}', '{$_cromoc7}', '{$_cromoc8}', '{$_cromoc9}', '{$_cromoc10}', '{$_cromoc11}', '{$_cromoc12}', '{$_cromoc13}', '{$_cromoc14}', '{$_cromoc15}', '{$_cromoc16}', '{$_cromoc17}', '{$_cromoc18}', '{$_cromoc19}', '{$_cromoc20}', '{$_cromomaxinc}', '{$_cromomaxincrel}', '{$_cromonummedxmuestra}', '{$_cromonummedxpuncurva}', '{$_cromonummedcurva}', '{$_cromoconmuestra1}', '{$_cromoconmuestra2}', '{$_cromoprompatcalibracion}', '{$_cromodesvestresidual}', '{$_cromodesvestcurvacali}', '{$_cromonp}', '{$_cromolote}', '{$_cromovence}', '{$_cromocc1}', '{$_cromocc2}', '{$_cromoest1}', '{$_cromoest2}', '{$_cromoccest1}', '{$_cromoccest2}', '{$_cromovalor}', '{$_cromoincexp}', '{$_cromok}', '{$_cromoincest}', '{$_cromoestandar1}', '{$_cromoestandar2}', '{$_cromoestandar3}', '{$_cromoestandar4}', '{$_cromoestandar5}', '{$_cromopipeta1}', '{$_cromopipeta2}', '{$_cromopipeta3}', '{$_cromopipeta4}', '{$_cromopipeta5}', '{$_cromovolcorrpipeta1}', '{$_cromovolcorrpipeta2}', '{$_cromovolcorrpipeta3}', '{$_cromovolcorrpipeta4}', '{$_cromovolcorrpipeta5}', '{$_cromoincpip1}', '{$_cromoincpip2}', '{$_cromoincpip3}', '{$_cromoincpip4}', '{$_cromoincpip5}', '{$_cromobalon1}', '{$_cromobalon2}', '{$_cromobalon3}', '{$_cromobalon4}', '{$_cromobalon5}', '{$_cromodilinc1}', '{$_cromodilinc2}', '{$_cromodilinc3}', '{$_cromodilinc4}', '{$_cromodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027plomo VALUES('{$cs[0]['id']}', '{$_plomocon1}', '{$_plomocon2}', '{$_plomocon3}', '{$_plomocon4}', '{$_plomocon5}', '{$_plomoinc1}', '{$_plomoinc2}', '{$_plomoinc3}', '{$_plomoinc4}', '{$_plomoinc5}', '{$_plomoincrel1}', '{$_plomoincrel2}', '{$_plomoincrel3}', '{$_plomoincrel4}', '{$_plomoincrel5}', '{$_plomoA11}', '{$_plomoA12}', '{$_plomoA13}', '{$_plomoA14}', '{$_plomoA15}', '{$_plomoA21}', '{$_plomoA22}', '{$_plomoA23}', '{$_plomoA24}', '{$_plomoA25}', '{$_plomoA31}', '{$_plomoA32}', '{$_plomoA33}', '{$_plomoA34}', '{$_plomoA35}', '{$_plomoA41}', '{$_plomoA42}', '{$_plomoA43}', '{$_plomoA44}', '{$_plomoA45}', '{$_plomoC11}', '{$_plomoC12}', '{$_plomoC13}', '{$_plomoC14}', '{$_plomoC15}', '{$_plomoC21}', '{$_plomoC22}', '{$_plomoC23}', '{$_plomoC24}', '{$_plomoC25}', '{$_plomoC31}', '{$_plomoC32}', '{$_plomoC33}', '{$_plomoC34}', '{$_plomoC35}', '{$_plomoC41}', '{$_plomoC42}', '{$_plomoC43}', '{$_plomoC44}', '{$_plomoC45}', '{$_plomocp1}', '{$_plomocp2}', '{$_plomocp3}', '{$_plomocp4}', '{$_plomocp5}', '{$_plomoa1}', '{$_plomoa2}', '{$_plomoa3}', '{$_plomoa4}', '{$_plomoa5}', '{$_plomoa6}', '{$_plomoa7}', '{$_plomoa8}', '{$_plomoa9}', '{$_plomoa10}', '{$_plomoa11}', '{$_plomoa12}', '{$_plomoa13}', '{$_plomoa14}', '{$_plomoa15}', '{$_plomoa16}', '{$_plomoa17}', '{$_plomoa18}', '{$_plomoa19}', '{$_plomoa20}', '{$_plomoc1}', '{$_plomoc2}', '{$_plomoc3}', '{$_plomoc4}', '{$_plomoc5}', '{$_plomoc6}', '{$_plomoc7}', '{$_plomoc8}', '{$_plomoc9}', '{$_plomoc10}', '{$_plomoc11}', '{$_plomoc12}', '{$_plomoc13}', '{$_plomoc14}', '{$_plomoc15}', '{$_plomoc16}', '{$_plomoc17}', '{$_plomoc18}', '{$_plomoc19}', '{$_plomoc20}', '{$_plomomaxinc}', '{$_plomomaxincrel}', '{$_plomonummedxmuestra}', '{$_plomonummedxpuncurva}', '{$_plomonummedcurva}', '{$_plomoconmuestra1}', '{$_plomoconmuestra2}', '{$_plomoprompatcalibracion}', '{$_plomodesvestresidual}', '{$_plomodesvestcurvacali}', '{$_plomonp}', '{$_plomolote}', '{$_plomovence}', '{$_plomocc1}', '{$_plomocc2}', '{$_plomoest1}', '{$_plomoest2}', '{$_plomoccest1}', '{$_plomoccest2}', '{$_plomovalor}', '{$_plomoincexp}', '{$_plomok}', '{$_plomoincest}', '{$_plomoestandar1}', '{$_plomoestandar2}', '{$_plomoestandar3}', '{$_plomoestandar4}', '{$_plomoestandar5}', '{$_plomopipeta1}', '{$_plomopipeta2}', '{$_plomopipeta3}', '{$_plomopipeta4}', '{$_plomopipeta5}', '{$_plomovolcorrpipeta1}', '{$_plomovolcorrpipeta2}', '{$_plomovolcorrpipeta3}', '{$_plomovolcorrpipeta4}', '{$_plomovolcorrpipeta5}', '{$_plomoincpip1}', '{$_plomoincpip2}', '{$_plomoincpip3}', '{$_plomoincpip4}', '{$_plomoincpip5}', '{$_plomobalon1}', '{$_plomobalon2}', '{$_plomobalon3}', '{$_plomobalon4}', '{$_plomobalon5}', '{$_plomodilinc1}', '{$_plomodilinc2}', '{$_plomodilinc3}', '{$_plomodilinc4}', '{$_plomodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027mercurio VALUES('{$cs[0]['id']}', '{$_mercuriocon1}', '{$_mercuriocon2}', '{$_mercuriocon3}', '{$_mercuriocon4}', '{$_mercuriocon5}', '{$_mercurioinc1}', '{$_mercurioinc2}', '{$_mercurioinc3}', '{$_mercurioinc4}', '{$_mercurioinc5}', '{$_mercurioincrel1}', '{$_mercurioincrel2}', '{$_mercurioincrel3}', '{$_mercurioincrel4}', '{$_mercurioincrel5}', '{$_mercurioA11}', '{$_mercurioA12}', '{$_mercurioA13}', '{$_mercurioA14}', '{$_mercurioA15}', '{$_mercurioA21}', '{$_mercurioA22}', '{$_mercurioA23}', '{$_mercurioA24}', '{$_mercurioA25}', '{$_mercurioA31}', '{$_mercurioA32}', '{$_mercurioA33}', '{$_mercurioA34}', '{$_mercurioA35}', '{$_mercurioA41}', '{$_mercurioA42}', '{$_mercurioA43}', '{$_mercurioA44}', '{$_mercurioA45}', '{$_mercurioC11}', '{$_mercurioC12}', '{$_mercurioC13}', '{$_mercurioC14}', '{$_mercurioC15}', '{$_mercurioC21}', '{$_mercurioC22}', '{$_mercurioC23}', '{$_mercurioC24}', '{$_mercurioC25}', '{$_mercurioC31}', '{$_mercurioC32}', '{$_mercurioC33}', '{$_mercurioC34}', '{$_mercurioC35}', '{$_mercurioC41}', '{$_mercurioC42}', '{$_mercurioC43}', '{$_mercurioC44}', '{$_mercurioC45}', '{$_mercuriocp1}', '{$_mercuriocp2}', '{$_mercuriocp3}', '{$_mercuriocp4}', '{$_mercuriocp5}', '{$_mercurioa1}', '{$_mercurioa2}', '{$_mercurioa3}', '{$_mercurioa4}', '{$_mercurioa5}', '{$_mercurioa6}', '{$_mercurioa7}', '{$_mercurioa8}', '{$_mercurioa9}', '{$_mercurioa10}', '{$_mercurioa11}', '{$_mercurioa12}', '{$_mercurioa13}', '{$_mercurioa14}', '{$_mercurioa15}', '{$_mercurioa16}', '{$_mercurioa17}', '{$_mercurioa18}', '{$_mercurioa19}', '{$_mercurioa20}', '{$_mercurioc1}', '{$_mercurioc2}', '{$_mercurioc3}', '{$_mercurioc4}', '{$_mercurioc5}', '{$_mercurioc6}', '{$_mercurioc7}', '{$_mercurioc8}', '{$_mercurioc9}', '{$_mercurioc10}', '{$_mercurioc11}', '{$_mercurioc12}', '{$_mercurioc13}', '{$_mercurioc14}', '{$_mercurioc15}', '{$_mercurioc16}', '{$_mercurioc17}', '{$_mercurioc18}', '{$_mercurioc19}', '{$_mercurioc20}', '{$_mercuriomaxinc}', '{$_mercuriomaxincrel}', '{$_mercurionummedxmuestra}', '{$_mercurionummedxpuncurva}', '{$_mercurionummedcurva}', '{$_mercurioconmuestra1}', '{$_mercurioconmuestra2}', '{$_mercurioprompatcalibracion}', '{$_mercuriodesvestresidual}', '{$_mercuriodesvestcurvacali}', '{$_mercurionp}', '{$_mercuriolote}', '{$_mercuriovence}', '{$_mercuriocc1}', '{$_mercuriocc2}', '{$_mercurioest1}', '{$_mercurioest2}', '{$_mercurioccest1}', '{$_mercurioccest2}', '{$_mercuriovalor}', '{$_mercurioincexp}', '{$_mercuriok}', '{$_mercurioincest}', '{$_mercurioestandar1}', '{$_mercurioestandar2}', '{$_mercurioestandar3}', '{$_mercurioestandar4}', '{$_mercurioestandar5}', '{$_mercuriopipeta1}', '{$_mercuriopipeta2}', '{$_mercuriopipeta3}', '{$_mercuriopipeta4}', '{$_mercuriopipeta5}', '{$_mercuriovolcorrpipeta1}', '{$_mercuriovolcorrpipeta2}', '{$_mercuriovolcorrpipeta3}', '{$_mercuriovolcorrpipeta4}', '{$_mercuriovolcorrpipeta5}', '{$_mercurioincpip1}', '{$_mercurioincpip2}', '{$_mercurioincpip3}', '{$_mercurioincpip4}', '{$_mercurioincpip5}', '{$_mercuriobalon1}', '{$_mercuriobalon2}', '{$_mercuriobalon3}', '{$_mercuriobalon4}', '{$_mercuriobalon5}', '{$_mercuriodilinc1}', '{$_mercuriodilinc2}', '{$_mercuriodilinc3}', '{$_mercuriodilinc4}', '{$_mercuriodilinc5}', '1')");
        return 1;
    }

    function IncccEncabezadoGet($_id = '')
    {
        $extra = "1=1";
        if ($_id != '') {
            $extra = 'id = ' . $_id;
        }
        return $this->_QUERY("SELECT id, CONVERT(VARCHAR, fecha, 105) AS fecha, estado FROM MET027 WHERE {$extra};");
    }

    function IncccDataGet($_id)
    {
        return $this->_QUERY("SELECT borocon1, borocon2, borocon3, borocon4, borocon5, boroinc1, boroinc2, boroinc3, boroinc4, boroinc5, boroincrel1, boroincrel2, boroincrel3, boroincrel4, boroincrel5, boroAA11, boroAA12, boroAA13, boroAA14, boroAA15, boroA21, boroA22, boroA23, boroA24, boroA25, boroA31, boroA32, boroA33, boroA34, boroA35, boroA41, boroA42, boroA43, boroA44, boroA45, boroCC11, boroCC12, boroCC13, boroCC14, boroCC15, boroC21, boroC22, boroC23, boroC24, boroC25, boroC31, boroC32, boroC33, boroC34, boroC35, boroC41, boroC42, boroC43, boroC44, boroC45, borocp1, borocp2, borocp3, borocp4, borocp5, boroa1, boroa2, boroa3, boroa4, boroa5, boroa6, boroa7, boroa8, boroa9, boroa10, boroa11, boroa12, boroa13, boroa14, boroa15, boroa16, boroa17, boroa18, boroa19, boroa20, boroc1, boroc2, boroc3, boroc4, boroc5, boroc6, boroc7, boroc8, boroc9, boroc10, boroc11, boroc12, boroc13, boroc14, boroc15, boroc16, boroc17, boroc18, boroc19, boroc20, boromaxinc, boromaxincrel, boronummedxmuestra, boronummedxpuncurva, boronummedcurva, boroconmuestra1, boroconmuestra2, boroprompatcalibracion, borodesvestresidual, borodesvestcurvacali, boronp, borolote, borovence, borocc1, borocc2, boroest1, boroest2, boroccest1, boroccest2, borovalor, boroincexp, borok, boroincest, boroestandar1, boroestandar2, boroestandar3, boroestandar4, boroestandar5, boropipeta1, boropipeta2, boropipeta3, boropipeta4, boropipeta5, borovolcorrpipeta1, borovolcorrpipeta2, borovolcorrpipeta3, borovolcorrpipeta4, borovolcorrpipeta5, boroincpip1, boroincpip2, boroincpip3, boroincpip4, boroincpip5, borobalon1, borobalon2, borobalon3, borobalon4, borobalon5, borodilinc1, borodilinc2, borodilinc3, borodilinc4, borodilinc5, calciocon1, calciocon2, calciocon3, calciocon4, calciocon5, calcioinc1, calcioinc2, calcioinc3, calcioinc4, calcioinc5, calcioincrel1, calcioincrel2, calcioincrel3, calcioincrel4, calcioincrel5, calcioAA11, calcioAA12, calcioAA13, calcioAA14, calcioAA15, calcioA21, calcioA22, calcioA23, calcioA24, calcioA25, calcioA31, calcioA32, calcioA33, calcioA34, calcioA35, calcioA41, calcioA42, calcioA43, calcioA44, calcioA45, calcioCC11, calcioCC12, calcioCC13, calcioCC14, calcioCC15, calcioC21, calcioC22, calcioC23, calcioC24, calcioC25, calcioC31, calcioC32, calcioC33, calcioC34, calcioC35, calcioC41, calcioC42, calcioC43, calcioC44, calcioC45, calciocp1, calciocp2, calciocp3, calciocp4, calciocp5, calcioa1, calcioa2, calcioa3, calcioa4, calcioa5, calcioa6, calcioa7, calcioa8, calcioa9, calcioa10, calcioa11, calcioa12, calcioa13, calcioa14, calcioa15, calcioa16, calcioa17, calcioa18, calcioa19, calcioa20, calcioc1, calcioc2, calcioc3, calcioc4, calcioc5, calcioc6, calcioc7, calcioc8, calcioc9, calcioc10, calcioc11, calcioc12, calcioc13, calcioc14, calcioc15, calcioc16, calcioc17, calcioc18, calcioc19, calcioc20, calciomaxinc, calciomaxincrel, calcionummedxmuestra, calcionummedxpuncurva, calcionummedcurva, calcioconmuestra1, calcioconmuestra2, calcioprompatcalibracion, calciodesvestresidual, calciodesvestcurvacali, calcionp, calciolote, calciovence, calciocc1, calciocc2, calcioest1, calcioest2, calcioccest1, calcioccest2, calciovalor, calcioincexp, calciok, calcioincest, calcioestandar1, calcioestandar2, calcioestandar3, calcioestandar4, calcioestandar5, calciopipeta1, calciopipeta2, calciopipeta3, calciopipeta4, calciopipeta5, calciovolcorrpipeta1, calciovolcorrpipeta2, calciovolcorrpipeta3, calciovolcorrpipeta4, calciovolcorrpipeta5, calcioincpip1, calcioincpip2, calcioincpip3, calcioincpip4, calcioincpip5, calciobalon1, calciobalon2, calciobalon3, calciobalon4, calciobalon5, calciodilinc1, calciodilinc2, calciodilinc3, calciodilinc4, calciodilinc5, cobrecon1, cobrecon2, cobrecon3, cobrecon4, cobrecon5, cobreinc1, cobreinc2, cobreinc3, cobreinc4, cobreinc5, cobreincrel1, cobreincrel2, cobreincrel3, cobreincrel4, cobreincrel5, cobreAA11, cobreAA12, cobreAA13, cobreAA14, cobreAA15, cobreA21, cobreA22, cobreA23, cobreA24, cobreA25, cobreA31, cobreA32, cobreA33, cobreA34, cobreA35, cobreA41, cobreA42, cobreA43, cobreA44, cobreA45, cobreCC11, cobreCC12, cobreCC13, cobreCC14, cobreCC15, cobreC21, cobreC22, cobreC23, cobreC24, cobreC25, cobreC31, cobreC32, cobreC33, cobreC34, cobreC35, cobreC41, cobreC42, cobreC43, cobreC44, cobreC45, cobrecp1, cobrecp2, cobrecp3, cobrecp4, cobrecp5, cobrea1, cobrea2, cobrea3, cobrea4, cobrea5, cobrea6, cobrea7, cobrea8, cobrea9, cobrea10, cobrea11, cobrea12, cobrea13, cobrea14, cobrea15, cobrea16, cobrea17, cobrea18, cobrea19, cobrea20, cobrec1, cobrec2, cobrec3, cobrec4, cobrec5, cobrec6, cobrec7, cobrec8, cobrec9, cobrec10, cobrec11, cobrec12, cobrec13, cobrec14, cobrec15, cobrec16, cobrec17, cobrec18, cobrec19, cobrec20, cobremaxinc, cobremaxincrel, cobrenummedxmuestra, cobrenummedxpuncurva, cobrenummedcurva, cobreconmuestra1, cobreconmuestra2, cobreprompatcalibracion, cobredesvestresidual, cobredesvestcurvacali, cobrenp, cobrelote, cobrevence, cobrecc1, cobrecc2, cobreest1, cobreest2, cobreccest1, cobreccest2, cobrevalor, cobreincexp, cobrek, cobreincest, cobreestandar1, cobreestandar2, cobreestandar3, cobreestandar4, cobreestandar5, cobrepipeta1, cobrepipeta2, cobrepipeta3, cobrepipeta4, cobrepipeta5, cobrevolcorrpipeta1, cobrevolcorrpipeta2, cobrevolcorrpipeta3, cobrevolcorrpipeta4, cobrevolcorrpipeta5, cobreincpip1, cobreincpip2, cobreincpip3, cobreincpip4, cobreincpip5, cobrebalon1, cobrebalon2, cobrebalon3, cobrebalon4, cobrebalon5, cobredilinc1, cobredilinc2, cobredilinc3, cobredilinc4, cobredilinc5, fosforocon1, fosforocon2, fosforocon3, fosforocon4, fosforocon5, fosforoinc1, fosforoinc2, fosforoinc3, fosforoinc4, fosforoinc5, fosforoincrel1, fosforoincrel2, fosforoincrel3, fosforoincrel4, fosforoincrel5, fosforoAA11, fosforoAA12, fosforoAA13, fosforoAA14, fosforoAA15, fosforoA21, fosforoA22, fosforoA23, fosforoA24, fosforoA25, fosforoA31, fosforoA32, fosforoA33, fosforoA34, fosforoA35, fosforoA41, fosforoA42, fosforoA43, fosforoA44, fosforoA45, fosforoCC11, fosforoCC12, fosforoCC13, fosforoCC14, fosforoCC15, fosforoC21, fosforoC22, fosforoC23, fosforoC24, fosforoC25, fosforoC31, fosforoC32, fosforoC33, fosforoC34, fosforoC35, fosforoC41, fosforoC42, fosforoC43, fosforoC44, fosforoC45, fosforocp1, fosforocp2, fosforocp3, fosforocp4, fosforocp5, fosforoa1, fosforoa2, fosforoa3, fosforoa4, fosforoa5, fosforoa6, fosforoa7, fosforoa8, fosforoa9, fosforoa10, fosforoa11, fosforoa12, fosforoa13, fosforoa14, fosforoa15, fosforoa16, fosforoa17, fosforoa18, fosforoa19, fosforoa20, fosforoc1, fosforoc2, fosforoc3, fosforoc4, fosforoc5, fosforoc6, fosforoc7, fosforoc8, fosforoc9, fosforoc10, fosforoc11, fosforoc12, fosforoc13, fosforoc14, fosforoc15, fosforoc16, fosforoc17, fosforoc18, fosforoc19, fosforoc20, fosforomaxinc, fosforomaxincrel, fosforonummedxmuestra, fosforonummedxpuncurva, fosforonummedcurva, fosforoconmuestra1, fosforoconmuestra2, fosforoprompatcalibracion, fosforodesvestresidual, fosforodesvestcurvacali, fosforonp, fosforolote, fosforovence, fosforocc1, fosforocc2, fosforoest1, fosforoest2, fosforoccest1, fosforoccest2, fosforovalor, fosforoincexp, fosforok, fosforoincest, fosforoestandar1, fosforoestandar2, fosforoestandar3, fosforoestandar4, fosforoestandar5, fosforopipeta1, fosforopipeta2, fosforopipeta3, fosforopipeta4, fosforopipeta5, fosforovolcorrpipeta1, fosforovolcorrpipeta2, fosforovolcorrpipeta3, fosforovolcorrpipeta4, fosforovolcorrpipeta5, fosforoincpip1, fosforoincpip2, fosforoincpip3, fosforoincpip4, fosforoincpip5, fosforobalon1, fosforobalon2, fosforobalon3, fosforobalon4, fosforobalon5, fosforodilinc1, fosforodilinc2, fosforodilinc3, fosforodilinc4, fosforodilinc5, hierrocon1, hierrocon2, hierrocon3, hierrocon4, hierrocon5, hierroinc1, hierroinc2, hierroinc3, hierroinc4, hierroinc5, hierroincrel1, hierroincrel2, hierroincrel3, hierroincrel4, hierroincrel5, hierroAA11, hierroAA12, hierroAA13, hierroAA14, hierroAA15, hierroA21, hierroA22, hierroA23, hierroA24, hierroA25, hierroA31, hierroA32, hierroA33, hierroA34, hierroA35, hierroA41, hierroA42, hierroA43, hierroA44, hierroA45, hierroCC11, hierroCC12, hierroCC13, hierroCC14, hierroCC15, hierroC21, hierroC22, hierroC23, hierroC24, hierroC25, hierroC31, hierroC32, hierroC33, hierroC34, hierroC35, hierroC41, hierroC42, hierroC43, hierroC44, hierroC45, hierrocp1, hierrocp2, hierrocp3, hierrocp4, hierrocp5, hierroa1, hierroa2, hierroa3, hierroa4, hierroa5, hierroa6, hierroa7, hierroa8, hierroa9, hierroa10, hierroa11, hierroa12, hierroa13, hierroa14, hierroa15, hierroa16, hierroa17, hierroa18, hierroa19, hierroa20, hierroc1, hierroc2, hierroc3, hierroc4, hierroc5, hierroc6, hierroc7, hierroc8, hierroc9, hierroc10, hierroc11, hierroc12, hierroc13, hierroc14, hierroc15, hierroc16, hierroc17, hierroc18, hierroc19, hierroc20, hierromaxinc, hierromaxincrel, hierronummedxmuestra, hierronummedxpuncurva, hierronummedcurva, hierroconmuestra1, hierroconmuestra2, hierroprompatcalibracion, hierrodesvestresidual, hierrodesvestcurvacali, hierronp, hierrolote, hierrovence, hierrocc1, hierrocc2, hierroest1, hierroest2, hierroccest1, hierroccest2, hierrovalor, hierroincexp, hierrok, hierroincest, hierroestandar1, hierroestandar2, hierroestandar3, hierroestandar4, hierroestandar5, hierropipeta1, hierropipeta2, hierropipeta3, hierropipeta4, hierropipeta5, hierrovolcorrpipeta1, hierrovolcorrpipeta2, hierrovolcorrpipeta3, hierrovolcorrpipeta4, hierrovolcorrpipeta5, hierroincpip1, hierroincpip2, hierroincpip3, hierroincpip4, hierroincpip5, hierrobalon1, hierrobalon2, hierrobalon3, hierrobalon4, hierrobalon5, hierrodilinc1, hierrodilinc2, hierrodilinc3, hierrodilinc4, hierrodilinc5, magnesiocon1, magnesiocon2, magnesiocon3, magnesiocon4, magnesiocon5, magnesioinc1, magnesioinc2, magnesioinc3, magnesioinc4, magnesioinc5, magnesioincrel1, magnesioincrel2, magnesioincrel3, magnesioincrel4, magnesioincrel5, magnesioAA11, magnesioAA12, magnesioAA13, magnesioAA14, magnesioAA15, magnesioA21, magnesioA22, magnesioA23, magnesioA24, magnesioA25, magnesioA31, magnesioA32, magnesioA33, magnesioA34, magnesioA35, magnesioA41, magnesioA42, magnesioA43, magnesioA44, magnesioA45, magnesioCC11, magnesioCC12, magnesioCC13, magnesioCC14, magnesioCC15, magnesioC21, magnesioC22, magnesioC23, magnesioC24, magnesioC25, magnesioC31, magnesioC32, magnesioC33, magnesioC34, magnesioC35, magnesioC41, magnesioC42, magnesioC43, magnesioC44, magnesioC45, magnesiocp1, magnesiocp2, magnesiocp3, magnesiocp4, magnesiocp5, magnesioa1, magnesioa2, magnesioa3, magnesioa4, magnesioa5, magnesioa6, magnesioa7, magnesioa8, magnesioa9, magnesioa10, magnesioa11, magnesioa12, magnesioa13, magnesioa14, magnesioa15, magnesioa16, magnesioa17, magnesioa18, magnesioa19, magnesioa20, magnesioc1, magnesioc2, magnesioc3, magnesioc4, magnesioc5, magnesioc6, magnesioc7, magnesioc8, magnesioc9, magnesioc10, magnesioc11, magnesioc12, magnesioc13, magnesioc14, magnesioc15, magnesioc16, magnesioc17, magnesioc18, magnesioc19, magnesioc20, magnesiomaxinc, magnesiomaxincrel, magnesionummedxmuestra, magnesionummedxpuncurva, magnesionummedcurva, magnesioconmuestra1, magnesioconmuestra2, magnesioprompatcalibracion, magnesiodesvestresidual, magnesiodesvestcurvacali, magnesionp, magnesiolote, magnesiovence, magnesiocc1, magnesiocc2, magnesioest1, magnesioest2, magnesioccest1, magnesioccest2, magnesiovalor, magnesioincexp, magnesiok, magnesioincest, magnesioestandar1, magnesioestandar2, magnesioestandar3, magnesioestandar4, magnesioestandar5, magnesiopipeta1, magnesiopipeta2, magnesiopipeta3, magnesiopipeta4, magnesiopipeta5, magnesiovolcorrpipeta1, magnesiovolcorrpipeta2, magnesiovolcorrpipeta3, magnesiovolcorrpipeta4, magnesiovolcorrpipeta5, magnesioincpip1, magnesioincpip2, magnesioincpip3, magnesioincpip4, magnesioincpip5, magnesiobalon1, magnesiobalon2, magnesiobalon3, magnesiobalon4, magnesiobalon5, magnesiodilinc1, magnesiodilinc2, magnesiodilinc3, magnesiodilinc4, magnesiodilinc5, manganesocon1, manganesocon2, manganesocon3, manganesocon4, manganesocon5, manganesoinc1, manganesoinc2, manganesoinc3, manganesoinc4, manganesoinc5, manganesoincrel1, manganesoincrel2, manganesoincrel3, manganesoincrel4, manganesoincrel5, manganesoAA11, manganesoAA12, manganesoAA13, manganesoAA14, manganesoAA15, manganesoA21, manganesoA22, manganesoA23, manganesoA24, manganesoA25, manganesoA31, manganesoA32, manganesoA33, manganesoA34, manganesoA35, manganesoA41, manganesoA42, manganesoA43, manganesoA44, manganesoA45, manganesoCC11, manganesoCC12, manganesoCC13, manganesoCC14, manganesoCC15, manganesoC21, manganesoC22, manganesoC23, manganesoC24, manganesoC25, manganesoC31, manganesoC32, manganesoC33, manganesoC34, manganesoC35, manganesoC41, manganesoC42, manganesoC43, manganesoC44, manganesoC45, manganesocp1, manganesocp2, manganesocp3, manganesocp4, manganesocp5, manganesoa1, manganesoa2, manganesoa3, manganesoa4, manganesoa5, manganesoa6, manganesoa7, manganesoa8, manganesoa9, manganesoa10, manganesoa11, manganesoa12, manganesoa13, manganesoa14, manganesoa15, manganesoa16, manganesoa17, manganesoa18, manganesoa19, manganesoa20, manganesoc1, manganesoc2, manganesoc3, manganesoc4, manganesoc5, manganesoc6, manganesoc7, manganesoc8, manganesoc9, manganesoc10, manganesoc11, manganesoc12, manganesoc13, manganesoc14, manganesoc15, manganesoc16, manganesoc17, manganesoc18, manganesoc19, manganesoc20, manganesomaxinc, manganesomaxincrel, manganesonummedxmuestra, manganesonummedxpuncurva, manganesonummedcurva, manganesoconmuestra1, manganesoconmuestra2, manganesoprompatcalibracion, manganesodesvestresidual, manganesodesvestcurvacali, manganesonp, manganesolote, manganesovence, manganesocc1, manganesocc2, manganesoest1, manganesoest2, manganesoccest1, manganesoccest2, manganesovalor, manganesoincexp, manganesok, manganesoincest, manganesoestandar1, manganesoestandar2, manganesoestandar3, manganesoestandar4, manganesoestandar5, manganesopipeta1, manganesopipeta2, manganesopipeta3, manganesopipeta4, manganesopipeta5, manganesovolcorrpipeta1, manganesovolcorrpipeta2, manganesovolcorrpipeta3, manganesovolcorrpipeta4, manganesovolcorrpipeta5, manganesoincpip1, manganesoincpip2, manganesoincpip3, manganesoincpip4, manganesoincpip5, manganesobalon1, manganesobalon2, manganesobalon3, manganesobalon4, manganesobalon5, manganesodilinc1, manganesodilinc2, manganesodilinc3, manganesodilinc4, manganesodilinc5, potasiocon1, potasiocon2, potasiocon3, potasiocon4, potasiocon5, potasioinc1, potasioinc2, potasioinc3, potasioinc4, potasioinc5, potasioincrel1, potasioincrel2, potasioincrel3, potasioincrel4, potasioincrel5, potasioAA11, potasioAA12, potasioAA13, potasioAA14, potasioAA15, potasioA21, potasioA22, potasioA23, potasioA24, potasioA25, potasioA31, potasioA32, potasioA33, potasioA34, potasioA35, potasioA41, potasioA42, potasioA43, potasioA44, potasioA45, potasioCC11, potasioCC12, potasioCC13, potasioCC14, potasioCC15, potasioC21, potasioC22, potasioC23, potasioC24, potasioC25, potasioC31, potasioC32, potasioC33, potasioC34, potasioC35, potasioC41, potasioC42, potasioC43, potasioC44, potasioC45, potasiocp1, potasiocp2, potasiocp3, potasiocp4, potasiocp5, potasioa1, potasioa2, potasioa3, potasioa4, potasioa5, potasioa6, potasioa7, potasioa8, potasioa9, potasioa10, potasioa11, potasioa12, potasioa13, potasioa14, potasioa15, potasioa16, potasioa17, potasioa18, potasioa19, potasioa20, potasioc1, potasioc2, potasioc3, potasioc4, potasioc5, potasioc6, potasioc7, potasioc8, potasioc9, potasioc10, potasioc11, potasioc12, potasioc13, potasioc14, potasioc15, potasioc16, potasioc17, potasioc18, potasioc19, potasioc20, potasiomaxinc, potasiomaxincrel, potasionummedxmuestra, potasionummedxpuncurva, potasionummedcurva, potasioconmuestra1, potasioconmuestra2, potasioprompatcalibracion, potasiodesvestresidual, potasiodesvestcurvacali, potasionp, potasiolote, potasiovence, potasiocc1, potasiocc2, potasioest1, potasioest2, potasioccest1, potasioccest2, potasiovalor, potasioincexp, potasiok, potasioincest, potasioestandar1, potasioestandar2, potasioestandar3, potasioestandar4, potasioestandar5, potasiopipeta1, potasiopipeta2, potasiopipeta3, potasiopipeta4, potasiopipeta5, potasiovolcorrpipeta1, potasiovolcorrpipeta2, potasiovolcorrpipeta3, potasiovolcorrpipeta4, potasiovolcorrpipeta5, potasioincpip1, potasioincpip2, potasioincpip3, potasioincpip4, potasioincpip5, potasiobalon1, potasiobalon2, potasiobalon3, potasiobalon4, potasiobalon5, potasiodilinc1, potasiodilinc2, potasiodilinc3, potasiodilinc4, potasiodilinc5, zinccon1, zinccon2, zinccon3, zinccon4, zinccon5, zincinc1, zincinc2, zincinc3, zincinc4, zincinc5, zincincrel1, zincincrel2, zincincrel3, zincincrel4, zincincrel5, zincAA11, zincAA12, zincAA13, zincAA14, zincAA15, zincA21, zincA22, zincA23, zincA24, zincA25, zincA31, zincA32, zincA33, zincA34, zincA35, zincA41, zincA42, zincA43, zincA44, zincA45, zincCC11, zincCC12, zincCC13, zincCC14, zincCC15, zincC21, zincC22, zincC23, zincC24, zincC25, zincC31, zincC32, zincC33, zincC34, zincC35, zincC41, zincC42, zincC43, zincC44, zincC45, zinccp1, zinccp2, zinccp3, zinccp4, zinccp5, zinca1, zinca2, zinca3, zinca4, zinca5, zinca6, zinca7, zinca8, zinca9, zinca10, zinca11, zinca12, zinca13, zinca14, zinca15, zinca16, zinca17, zinca18, zinca19, zinca20, zincc1, zincc2, zincc3, zincc4, zincc5, zincc6, zincc7, zincc8, zincc9, zincc10, zincc11, zincc12, zincc13, zincc14, zincc15, zincc16, zincc17, zincc18, zincc19, zincc20, zincmaxinc, zincmaxincrel, zincnummedxmuestra, zincnummedxpuncurva, zincnummedcurva, zincconmuestra1, zincconmuestra2, zincprompatcalibracion, zincdesvestresidual, zincdesvestcurvacali, zincnp, zinclote, zincvence, zinccc1, zinccc2, zincest1, zincest2, zincccest1, zincccest2, zincvalor, zincincexp, zinck, zincincest, zincestandar1, zincestandar2, zincestandar3, zincestandar4, zincestandar5, zincpipeta1, zincpipeta2, zincpipeta3, zincpipeta4, zincpipeta5, zincvolcorrpipeta1, zincvolcorrpipeta2, zincvolcorrpipeta3, zincvolcorrpipeta4, zincvolcorrpipeta5, zincincpip1, zincincpip2, zincincpip3, zincincpip4, zincincpip5, zincbalon1, zincbalon2, zincbalon3, zincbalon4, zincbalon5, zincdilinc1, zincdilinc2, zincdilinc3, zincdilinc4, zincdilinc5, nitrogenocon1, nitrogenocon2, nitrogenocon3, nitrogenocon4, nitrogenocon5, nitrogenoinc1, nitrogenoinc2, nitrogenoinc3, nitrogenoinc4, nitrogenoinc5, nitrogenoincrel1, nitrogenoincrel2, nitrogenoincrel3, nitrogenoincrel4, nitrogenoincrel5, nitrogenoAA11, nitrogenoAA12, nitrogenoAA13, nitrogenoAA14, nitrogenoAA15, nitrogenoA21, nitrogenoA22, nitrogenoA23, nitrogenoA24, nitrogenoA25, nitrogenoA31, nitrogenoA32, nitrogenoA33, nitrogenoA34, nitrogenoA35, nitrogenoA41, nitrogenoA42, nitrogenoA43, nitrogenoA44, nitrogenoA45, nitrogenoCC11, nitrogenoCC12, nitrogenoCC13, nitrogenoCC14, nitrogenoCC15, nitrogenoC21, nitrogenoC22, nitrogenoC23, nitrogenoC24, nitrogenoC25, nitrogenoC31, nitrogenoC32, nitrogenoC33, nitrogenoC34, nitrogenoC35, nitrogenoC41, nitrogenoC42, nitrogenoC43, nitrogenoC44, nitrogenoC45, nitrogenocp1, nitrogenocp2, nitrogenocp3, nitrogenocp4, nitrogenocp5, nitrogenoa1, nitrogenoa2, nitrogenoa3, nitrogenoa4, nitrogenoa5, nitrogenoa6, nitrogenoa7, nitrogenoa8, nitrogenoa9, nitrogenoa10, nitrogenoa11, nitrogenoa12, nitrogenoa13, nitrogenoa14, nitrogenoa15, nitrogenoa16, nitrogenoa17, nitrogenoa18, nitrogenoa19, nitrogenoa20, nitrogenoc1, nitrogenoc2, nitrogenoc3, nitrogenoc4, nitrogenoc5, nitrogenoc6, nitrogenoc7, nitrogenoc8, nitrogenoc9, nitrogenoc10, nitrogenoc11, nitrogenoc12, nitrogenoc13, nitrogenoc14, nitrogenoc15, nitrogenoc16, nitrogenoc17, nitrogenoc18, nitrogenoc19, nitrogenoc20, nitrogenomaxinc, nitrogenomaxincrel, nitrogenonummedxmuestra, nitrogenonummedxpuncurva, nitrogenonummedcurva, nitrogenoconmuestra1, nitrogenoconmuestra2, nitrogenoprompatcalibracion, nitrogenodesvestresidual, nitrogenodesvestcurvacali, nitrogenonp, nitrogenolote, nitrogenovence, nitrogenocc1, nitrogenocc2, nitrogenoest1, nitrogenoest2, nitrogenoccest1, nitrogenoccest2, nitrogenovalor, nitrogenoincexp, nitrogenok, nitrogenoincest, nitrogenoestandar1, nitrogenoestandar2, nitrogenoestandar3, nitrogenoestandar4, nitrogenoestandar5, nitrogenopipeta1, nitrogenopipeta2, nitrogenopipeta3, nitrogenopipeta4, nitrogenopipeta5, nitrogenovolcorrpipeta1, nitrogenovolcorrpipeta2, nitrogenovolcorrpipeta3, nitrogenovolcorrpipeta4, nitrogenovolcorrpipeta5, nitrogenoincpip1, nitrogenoincpip2, nitrogenoincpip3, nitrogenoincpip4, nitrogenoincpip5, nitrogenobalon1, nitrogenobalon2, nitrogenobalon3, nitrogenobalon4, nitrogenobalon5, nitrogenodilinc1, nitrogenodilinc2, nitrogenodilinc3, nitrogenodilinc4, nitrogenodilinc5, azufrecon1, azufrecon2, azufrecon3, azufrecon4, azufrecon5, azufreinc1, azufreinc2, azufreinc3, azufreinc4, azufreinc5, azufreincrel1, azufreincrel2, azufreincrel3, azufreincrel4, azufreincrel5, azufreAA11, azufreAA12, azufreAA13, azufreAA14, azufreAA15, azufreA21, azufreA22, azufreA23, azufreA24, azufreA25, azufreA31, azufreA32, azufreA33, azufreA34, azufreA35, azufreA41, azufreA42, azufreA43, azufreA44, azufreA45, azufreCC11, azufreCC12, azufreCC13, azufreCC14, azufreCC15, azufreC21, azufreC22, azufreC23, azufreC24, azufreC25, azufreC31, azufreC32, azufreC33, azufreC34, azufreC35, azufreC41, azufreC42, azufreC43, azufreC44, azufreC45, azufrecp1, azufrecp2, azufrecp3, azufrecp4, azufrecp5, azufrea1, azufrea2, azufrea3, azufrea4, azufrea5, azufrea6, azufrea7, azufrea8, azufrea9, azufrea10, azufrea11, azufrea12, azufrea13, azufrea14, azufrea15, azufrea16, azufrea17, azufrea18, azufrea19, azufrea20, azufrec1, azufrec2, azufrec3, azufrec4, azufrec5, azufrec6, azufrec7, azufrec8, azufrec9, azufrec10, azufrec11, azufrec12, azufrec13, azufrec14, azufrec15, azufrec16, azufrec17, azufrec18, azufrec19, azufrec20, azufremaxinc, azufremaxincrel, azufrenummedxmuestra, azufrenummedxpuncurva, azufrenummedcurva, azufreconmuestra1, azufreconmuestra2, azufreprompatcalibracion, azufredesvestresidual, azufredesvestcurvacali, azufrenp, azufrelote, azufrevence, azufrecc1, azufrecc2, azufreest1, azufreest2, azufreccest1, azufreccest2, azufrevalor, azufreincexp, azufrek, azufreincest, azufreestandar1, azufreestandar2, azufreestandar3, azufreestandar4, azufreestandar5, azufrepipeta1, azufrepipeta2, azufrepipeta3, azufrepipeta4, azufrepipeta5, azufrevolcorrpipeta1, azufrevolcorrpipeta2, azufrevolcorrpipeta3, azufrevolcorrpipeta4, azufrevolcorrpipeta5, azufreincpip1, azufreincpip2, azufreincpip3, azufreincpip4, azufreincpip5, azufrebalon1, azufrebalon2, azufrebalon3, azufrebalon4, azufrebalon5, azufredilinc1, azufredilinc2, azufredilinc3, azufredilinc4, azufredilinc5, arsenicocon1, arsenicocon2, arsenicocon3, arsenicocon4, arsenicocon5, arsenicoinc1, arsenicoinc2, arsenicoinc3, arsenicoinc4, arsenicoinc5, arsenicoincrel1, arsenicoincrel2, arsenicoincrel3, arsenicoincrel4, arsenicoincrel5, arsenicoAA11, arsenicoAA12, arsenicoAA13, arsenicoAA14, arsenicoAA15, arsenicoA21, arsenicoA22, arsenicoA23, arsenicoA24, arsenicoA25, arsenicoA31, arsenicoA32, arsenicoA33, arsenicoA34, arsenicoA35, arsenicoA41, arsenicoA42, arsenicoA43, arsenicoA44, arsenicoA45, arsenicoCC11, arsenicoCC12, arsenicoCC13, arsenicoCC14, arsenicoCC15, arsenicoC21, arsenicoC22, arsenicoC23, arsenicoC24, arsenicoC25, arsenicoC31, arsenicoC32, arsenicoC33, arsenicoC34, arsenicoC35, arsenicoC41, arsenicoC42, arsenicoC43, arsenicoC44, arsenicoC45, arsenicocp1, arsenicocp2, arsenicocp3, arsenicocp4, arsenicocp5, arsenicoa1, arsenicoa2, arsenicoa3, arsenicoa4, arsenicoa5, arsenicoa6, arsenicoa7, arsenicoa8, arsenicoa9, arsenicoa10, arsenicoa11, arsenicoa12, arsenicoa13, arsenicoa14, arsenicoa15, arsenicoa16, arsenicoa17, arsenicoa18, arsenicoa19, arsenicoa20, arsenicoc1, arsenicoc2, arsenicoc3, arsenicoc4, arsenicoc5, arsenicoc6, arsenicoc7, arsenicoc8, arsenicoc9, arsenicoc10, arsenicoc11, arsenicoc12, arsenicoc13, arsenicoc14, arsenicoc15, arsenicoc16, arsenicoc17, arsenicoc18, arsenicoc19, arsenicoc20, arsenicomaxinc, arsenicomaxincrel, arseniconummedxmuestra, arseniconummedxpuncurva, arseniconummedcurva, arsenicoconmuestra1, arsenicoconmuestra2, arsenicoprompatcalibracion, arsenicodesvestresidual, arsenicodesvestcurvacali, arseniconp, arsenicolote, arsenicovence, arsenicocc1, arsenicocc2, arsenicoest1, arsenicoest2, arsenicoccest1, arsenicoccest2, arsenicovalor, arsenicoincexp, arsenicok, arsenicoincest, arsenicoestandar1, arsenicoestandar2, arsenicoestandar3, arsenicoestandar4, arsenicoestandar5, arsenicopipeta1, arsenicopipeta2, arsenicopipeta3, arsenicopipeta4, arsenicopipeta5, arsenicovolcorrpipeta1, arsenicovolcorrpipeta2, arsenicovolcorrpipeta3, arsenicovolcorrpipeta4, arsenicovolcorrpipeta5, arsenicoincpip1, arsenicoincpip2, arsenicoincpip3, arsenicoincpip4, arsenicoincpip5, arsenicobalon1, arsenicobalon2, arsenicobalon3, arsenicobalon4, arsenicobalon5, arsenicodilinc1, arsenicodilinc2, arsenicodilinc3, arsenicodilinc4, arsenicodilinc5, cadmiocon1, cadmiocon2, cadmiocon3, cadmiocon4, cadmiocon5, cadmioinc1, cadmioinc2, cadmioinc3, cadmioinc4, cadmioinc5, cadmioincrel1, cadmioincrel2, cadmioincrel3, cadmioincrel4, cadmioincrel5, cadmioAA11, cadmioAA12, cadmioAA13, cadmioAA14, cadmioAA15, cadmioA21, cadmioA22, cadmioA23, cadmioA24, cadmioA25, cadmioA31, cadmioA32, cadmioA33, cadmioA34, cadmioA35, cadmioA41, cadmioA42, cadmioA43, cadmioA44, cadmioA45, cadmioCC11, cadmioCC12, cadmioCC13, cadmioCC14, cadmioCC15, cadmioC21, cadmioC22, cadmioC23, cadmioC24, cadmioC25, cadmioC31, cadmioC32, cadmioC33, cadmioC34, cadmioC35, cadmioC41, cadmioC42, cadmioC43, cadmioC44, cadmioC45, cadmiocp1, cadmiocp2, cadmiocp3, cadmiocp4, cadmiocp5, cadmioa1, cadmioa2, cadmioa3, cadmioa4, cadmioa5, cadmioa6, cadmioa7, cadmioa8, cadmioa9, cadmioa10, cadmioa11, cadmioa12, cadmioa13, cadmioa14, cadmioa15, cadmioa16, cadmioa17, cadmioa18, cadmioa19, cadmioa20, cadmioc1, cadmioc2, cadmioc3, cadmioc4, cadmioc5, cadmioc6, cadmioc7, cadmioc8, cadmioc9, cadmioc10, cadmioc11, cadmioc12, cadmioc13, cadmioc14, cadmioc15, cadmioc16, cadmioc17, cadmioc18, cadmioc19, cadmioc20, cadmiomaxinc, cadmiomaxincrel, cadmionummedxmuestra, cadmionummedxpuncurva, cadmionummedcurva, cadmioconmuestra1, cadmioconmuestra2, cadmioprompatcalibracion, cadmiodesvestresidual, cadmiodesvestcurvacali, cadmionp, cadmiolote, cadmiovence, cadmiocc1, cadmiocc2, cadmioest1, cadmioest2, cadmioccest1, cadmioccest2, cadmiovalor, cadmioincexp, cadmiok, cadmioincest, cadmioestandar1, cadmioestandar2, cadmioestandar3, cadmioestandar4, cadmioestandar5, cadmiopipeta1, cadmiopipeta2, cadmiopipeta3, cadmiopipeta4, cadmiopipeta5, cadmiovolcorrpipeta1, cadmiovolcorrpipeta2, cadmiovolcorrpipeta3, cadmiovolcorrpipeta4, cadmiovolcorrpipeta5, cadmioincpip1, cadmioincpip2, cadmioincpip3, cadmioincpip4, cadmioincpip5, cadmiobalon1, cadmiobalon2, cadmiobalon3, cadmiobalon4, cadmiobalon5, cadmiodilinc1, cadmiodilinc2, cadmiodilinc3, cadmiodilinc4, cadmiodilinc5, cromocon1, cromocon2, cromocon3, cromocon4, cromocon5, cromoinc1, cromoinc2, cromoinc3, cromoinc4, cromoinc5, cromoincrel1, cromoincrel2, cromoincrel3, cromoincrel4, cromoincrel5, cromoAA11, cromoAA12, cromoAA13, cromoAA14, cromoAA15, cromoA21, cromoA22, cromoA23, cromoA24, cromoA25, cromoA31, cromoA32, cromoA33, cromoA34, cromoA35, cromoA41, cromoA42, cromoA43, cromoA44, cromoA45, cromoCC11, cromoCC12, cromoCC13, cromoCC14, cromoCC15, cromoC21, cromoC22, cromoC23, cromoC24, cromoC25, cromoC31, cromoC32, cromoC33, cromoC34, cromoC35, cromoC41, cromoC42, cromoC43, cromoC44, cromoC45, cromocp1, cromocp2, cromocp3, cromocp4, cromocp5, cromoa1, cromoa2, cromoa3, cromoa4, cromoa5, cromoa6, cromoa7, cromoa8, cromoa9, cromoa10, cromoa11, cromoa12, cromoa13, cromoa14, cromoa15, cromoa16, cromoa17, cromoa18, cromoa19, cromoa20, cromoc1, cromoc2, cromoc3, cromoc4, cromoc5, cromoc6, cromoc7, cromoc8, cromoc9, cromoc10, cromoc11, cromoc12, cromoc13, cromoc14, cromoc15, cromoc16, cromoc17, cromoc18, cromoc19, cromoc20, cromomaxinc, cromomaxincrel, cromonummedxmuestra, cromonummedxpuncurva, cromonummedcurva, cromoconmuestra1, cromoconmuestra2, cromoprompatcalibracion, cromodesvestresidual, cromodesvestcurvacali, cromonp, cromolote, cromovence, cromocc1, cromocc2, cromoest1, cromoest2, cromoccest1, cromoccest2, cromovalor, cromoincexp, cromok, cromoincest, cromoestandar1, cromoestandar2, cromoestandar3, cromoestandar4, cromoestandar5, cromopipeta1, cromopipeta2, cromopipeta3, cromopipeta4, cromopipeta5, cromovolcorrpipeta1, cromovolcorrpipeta2, cromovolcorrpipeta3, cromovolcorrpipeta4, cromovolcorrpipeta5, cromoincpip1, cromoincpip2, cromoincpip3, cromoincpip4, cromoincpip5, cromobalon1, cromobalon2, cromobalon3, cromobalon4, cromobalon5, cromodilinc1, cromodilinc2, cromodilinc3, cromodilinc4, cromodilinc5, plomocon1, plomocon2, plomocon3, plomocon4, plomocon5, plomoinc1, plomoinc2, plomoinc3, plomoinc4, plomoinc5, plomoincrel1, plomoincrel2, plomoincrel3, plomoincrel4, plomoincrel5, plomoAA11, plomoAA12, plomoAA13, plomoAA14, plomoAA15, plomoA21, plomoA22, plomoA23, plomoA24, plomoA25, plomoA31, plomoA32, plomoA33, plomoA34, plomoA35, plomoA41, plomoA42, plomoA43, plomoA44, plomoA45, plomoCC11, plomoCC12, plomoCC13, plomoCC14, plomoCC15, plomoC21, plomoC22, plomoC23, plomoC24, plomoC25, plomoC31, plomoC32, plomoC33, plomoC34, plomoC35, plomoC41, plomoC42, plomoC43, plomoC44, plomoC45, plomocp1, plomocp2, plomocp3, plomocp4, plomocp5, plomoa1, plomoa2, plomoa3, plomoa4, plomoa5, plomoa6, plomoa7, plomoa8, plomoa9, plomoa10, plomoa11, plomoa12, plomoa13, plomoa14, plomoa15, plomoa16, plomoa17, plomoa18, plomoa19, plomoa20, plomoc1, plomoc2, plomoc3, plomoc4, plomoc5, plomoc6, plomoc7, plomoc8, plomoc9, plomoc10, plomoc11, plomoc12, plomoc13, plomoc14, plomoc15, plomoc16, plomoc17, plomoc18, plomoc19, plomoc20, plomomaxinc, plomomaxincrel, plomonummedxmuestra, plomonummedxpuncurva, plomonummedcurva, plomoconmuestra1, plomoconmuestra2, plomoprompatcalibracion, plomodesvestresidual, plomodesvestcurvacali, plomonp, plomolote, plomovence, plomocc1, plomocc2, plomoest1, plomoest2, plomoccest1, plomoccest2, plomovalor, plomoincexp, plomok, plomoincest, plomoestandar1, plomoestandar2, plomoestandar3, plomoestandar4, plomoestandar5, plomopipeta1, plomopipeta2, plomopipeta3, plomopipeta4, plomopipeta5, plomovolcorrpipeta1, plomovolcorrpipeta2, plomovolcorrpipeta3, plomovolcorrpipeta4, plomovolcorrpipeta5, plomoincpip1, plomoincpip2, plomoincpip3, plomoincpip4, plomoincpip5, plomobalon1, plomobalon2, plomobalon3, plomobalon4, plomobalon5, plomodilinc1, plomodilinc2, plomodilinc3, plomodilinc4, plomodilinc5, mercuriocon1, mercuriocon2, mercuriocon3, mercuriocon4, mercuriocon5, mercurioinc1, mercurioinc2, mercurioinc3, mercurioinc4, mercurioinc5, mercurioincrel1, mercurioincrel2, mercurioincrel3, mercurioincrel4, mercurioincrel5, mercurioAA11, mercurioAA12, mercurioAA13, mercurioAA14, mercurioAA15, mercurioA21, mercurioA22, mercurioA23, mercurioA24, mercurioA25, mercurioA31, mercurioA32, mercurioA33, mercurioA34, mercurioA35, mercurioA41, mercurioA42, mercurioA43, mercurioA44, mercurioA45, mercurioCC11, mercurioCC12, mercurioCC13, mercurioCC14, mercurioCC15, mercurioC21, mercurioC22, mercurioC23, mercurioC24, mercurioC25, mercurioC31, mercurioC32, mercurioC33, mercurioC34, mercurioC35, mercurioC41, mercurioC42, mercurioC43, mercurioC44, mercurioC45, mercuriocp1, mercuriocp2, mercuriocp3, mercuriocp4, mercuriocp5, mercurioa1, mercurioa2, mercurioa3, mercurioa4, mercurioa5, mercurioa6, mercurioa7, mercurioa8, mercurioa9, mercurioa10, mercurioa11, mercurioa12, mercurioa13, mercurioa14, mercurioa15, mercurioa16, mercurioa17, mercurioa18, mercurioa19, mercurioa20, mercurioc1, mercurioc2, mercurioc3, mercurioc4, mercurioc5, mercurioc6, mercurioc7, mercurioc8, mercurioc9, mercurioc10, mercurioc11, mercurioc12, mercurioc13, mercurioc14, mercurioc15, mercurioc16, mercurioc17, mercurioc18, mercurioc19, mercurioc20, mercuriomaxinc, mercuriomaxincrel, mercurionummedxmuestra, mercurionummedxpuncurva, mercurionummedcurva, mercurioconmuestra1, mercurioconmuestra2, mercurioprompatcalibracion, mercuriodesvestresidual, mercuriodesvestcurvacali, mercurionp, mercuriolote, mercuriovence, mercuriocc1, mercuriocc2, mercurioest1, mercurioest2, mercurioccest1, mercurioccest2, mercuriovalor, mercurioincexp, mercuriok, mercurioincest, mercurioestandar1, mercurioestandar2, mercurioestandar3, mercurioestandar4, mercurioestandar5, mercuriopipeta1, mercuriopipeta2, mercuriopipeta3, mercuriopipeta4, mercuriopipeta5, mercuriovolcorrpipeta1, mercuriovolcorrpipeta2, mercuriovolcorrpipeta3, mercuriovolcorrpipeta4, mercuriovolcorrpipeta5, mercurioincpip1, mercurioincpip2, mercurioincpip3, mercurioincpip4, mercurioincpip5, mercuriobalon1, mercuriobalon2, mercuriobalon3, mercuriobalon4, mercuriobalon5, mercuriodilinc1, mercuriodilinc2, mercuriodilinc3, mercuriodilinc4, mercuriodilinc5 FROM MET027boro INNER JOIN MET027calcio ON MET027boro.id = MET027calcio.id INNER JOIN MET027cobre ON MET027boro.id = MET027cobre.id INNER JOIN MET027fosforo ON MET027boro.id = MET027fosforo.id INNER JOIN MET027hierro ON MET027boro.id = MET027hierro.id INNER JOIN MET027magnesio ON MET027boro.id = MET027magnesio.id INNER JOIN MET027manganeso ON MET027boro.id = MET027manganeso.id INNER JOIN MET027potasio ON MET027boro.id = MET027potasio.id INNER JOIN MET027zinc ON MET027boro.id = MET027zinc.id INNER JOIN MET027nitrogeno ON MET027boro.id = MET027nitrogeno.id INNER JOIN MET027azufre ON MET027boro.id = MET027azufre.id INNER JOIN MET027arsenico ON MET027boro.id = MET027arsenico.id INNER JOIN MET027cadmio ON MET027boro.id = MET027cadmio.id INNER JOIN MET027cromo ON MET027boro.id = MET027cromo.id INNER JOIN MET027plomo ON MET027boro.id = MET027plomo.id INNER JOIN MET027mercurio ON MET027boro.id = MET027mercurio.id WHERE MET027boro.id = '{$_id}'");
    }

    function HumedadEncabezadoSet($_borocon1, $_borocon2, $_borocon3, $_borocon4, $_borocon5, $_boroinc1, $_boroinc2, $_boroinc3, $_boroinc4, $_boroinc5, $_boroincrel1, $_boroincrel2, $_boroincrel3, $_boroincrel4, $_boroincrel5, $_boroA11, $_boroA12, $_boroA13, $_boroA14, $_boroA15, $_boroA21, $_boroA22, $_boroA23, $_boroA24, $_boroA25, $_boroA31, $_boroA32, $_boroA33, $_boroA34, $_boroA35, $_boroA41, $_boroA42, $_boroA43, $_boroA44, $_boroA45, $_boroC11, $_boroC12, $_boroC13, $_boroC14, $_boroC15, $_boroC21, $_boroC22, $_boroC23, $_boroC24, $_boroC25, $_boroC31, $_boroC32, $_boroC33, $_boroC34, $_boroC35, $_boroC41, $_boroC42, $_boroC43, $_boroC44, $_boroC45, $_borocp1, $_borocp2, $_borocp3, $_borocp4, $_borocp5, $_boroa1, $_boroa2, $_boroa3, $_boroa4, $_boroa5, $_boroa6, $_boroa7, $_boroa8, $_boroa9, $_boroa10, $_boroa11, $_boroa12, $_boroa13, $_boroa14, $_boroa15, $_boroa16, $_boroa17, $_boroa18, $_boroa19, $_boroa20, $_boroc1, $_boroc2, $_boroc3, $_boroc4, $_boroc5, $_boroc6, $_boroc7, $_boroc8, $_boroc9, $_boroc10, $_boroc11, $_boroc12, $_boroc13, $_boroc14, $_boroc15, $_boroc16, $_boroc17, $_boroc18, $_boroc19, $_boroc20, $_boromaxinc, $_boromaxincrel, $_boronummedxmuestra, $_boronummedxpuncurva, $_boronummedcurva, $_boroconmuestra1, $_boroconmuestra2, $_boroprompatcalibracion, $_borodesvestresidual, $_borodesvestcurvacali, $_boronp, $_borolote, $_borovence, $_borocc1, $_borocc2, $_boroest1, $_boroest2, $_boroccest1, $_boroccest2, $_borovalor, $_boroincexp, $_borok, $_boroincest, $_boroestandar1, $_boroestandar2, $_boroestandar3, $_boroestandar4, $_boroestandar5, $_boropipeta1, $_boropipeta2, $_boropipeta3, $_boropipeta4, $_boropipeta5, $_borovolcorrpipeta1, $_borovolcorrpipeta2, $_borovolcorrpipeta3, $_borovolcorrpipeta4, $_borovolcorrpipeta5, $_boroincpip1, $_boroincpip2, $_boroincpip3, $_boroincpip4, $_boroincpip5, $_borobalon1, $_borobalon2, $_borobalon3, $_borobalon4, $_borobalon5, $_borodilinc1, $_borodilinc2, $_borodilinc3, $_borodilinc4, $_borodilinc5, $_calciocon1, $_calciocon2, $_calciocon3, $_calciocon4, $_calciocon5, $_calcioinc1, $_calcioinc2, $_calcioinc3, $_calcioinc4, $_calcioinc5, $_calcioincrel1, $_calcioincrel2, $_calcioincrel3, $_calcioincrel4, $_calcioincrel5, $_calcioA11, $_calcioA12, $_calcioA13, $_calcioA14, $_calcioA15, $_calcioA21, $_calcioA22, $_calcioA23, $_calcioA24, $_calcioA25, $_calcioA31, $_calcioA32, $_calcioA33, $_calcioA34, $_calcioA35, $_calcioA41, $_calcioA42, $_calcioA43, $_calcioA44, $_calcioA45, $_calcioC11, $_calcioC12, $_calcioC13, $_calcioC14, $_calcioC15, $_calcioC21, $_calcioC22, $_calcioC23, $_calcioC24, $_calcioC25, $_calcioC31, $_calcioC32, $_calcioC33, $_calcioC34, $_calcioC35, $_calcioC41, $_calcioC42, $_calcioC43, $_calcioC44, $_calcioC45, $_calciocp1, $_calciocp2, $_calciocp3, $_calciocp4, $_calciocp5, $_calcioa1, $_calcioa2, $_calcioa3, $_calcioa4, $_calcioa5, $_calcioa6, $_calcioa7, $_calcioa8, $_calcioa9, $_calcioa10, $_calcioa11, $_calcioa12, $_calcioa13, $_calcioa14, $_calcioa15, $_calcioa16, $_calcioa17, $_calcioa18, $_calcioa19, $_calcioa20, $_calcioc1, $_calcioc2, $_calcioc3, $_calcioc4, $_calcioc5, $_calcioc6, $_calcioc7, $_calcioc8, $_calcioc9, $_calcioc10, $_calcioc11, $_calcioc12, $_calcioc13, $_calcioc14, $_calcioc15, $_calcioc16, $_calcioc17, $_calcioc18, $_calcioc19, $_calcioc20, $_calciomaxinc, $_calciomaxincrel, $_calcionummedxmuestra, $_calcionummedxpuncurva, $_calcionummedcurva, $_calcioconmuestra1, $_calcioconmuestra2, $_calcioprompatcalibracion, $_calciodesvestresidual, $_calciodesvestcurvacali, $_calcionp, $_calciolote, $_calciovence, $_calciocc1, $_calciocc2, $_calcioest1, $_calcioest2, $_calcioccest1, $_calcioccest2, $_calciovalor, $_calcioincexp, $_calciok, $_calcioincest, $_calcioestandar1, $_calcioestandar2, $_calcioestandar3, $_calcioestandar4, $_calcioestandar5, $_calciopipeta1, $_calciopipeta2, $_calciopipeta3, $_calciopipeta4, $_calciopipeta5, $_calciovolcorrpipeta1, $_calciovolcorrpipeta2, $_calciovolcorrpipeta3, $_calciovolcorrpipeta4, $_calciovolcorrpipeta5, $_calcioincpip1, $_calcioincpip2, $_calcioincpip3, $_calcioincpip4, $_calcioincpip5, $_calciobalon1, $_calciobalon2, $_calciobalon3, $_calciobalon4, $_calciobalon5, $_calciodilinc1, $_calciodilinc2, $_calciodilinc3, $_calciodilinc4, $_calciodilinc5, $_cobrecon1, $_cobrecon2, $_cobrecon3, $_cobrecon4, $_cobrecon5, $_cobreinc1, $_cobreinc2, $_cobreinc3, $_cobreinc4, $_cobreinc5, $_cobreincrel1, $_cobreincrel2, $_cobreincrel3, $_cobreincrel4, $_cobreincrel5, $_cobreA11, $_cobreA12, $_cobreA13, $_cobreA14, $_cobreA15, $_cobreA21, $_cobreA22, $_cobreA23, $_cobreA24, $_cobreA25, $_cobreA31, $_cobreA32, $_cobreA33, $_cobreA34, $_cobreA35, $_cobreA41, $_cobreA42, $_cobreA43, $_cobreA44, $_cobreA45, $_cobreC11, $_cobreC12, $_cobreC13, $_cobreC14, $_cobreC15, $_cobreC21, $_cobreC22, $_cobreC23, $_cobreC24, $_cobreC25, $_cobreC31, $_cobreC32, $_cobreC33, $_cobreC34, $_cobreC35, $_cobreC41, $_cobreC42, $_cobreC43, $_cobreC44, $_cobreC45, $_cobrecp1, $_cobrecp2, $_cobrecp3, $_cobrecp4, $_cobrecp5, $_cobrea1, $_cobrea2, $_cobrea3, $_cobrea4, $_cobrea5, $_cobrea6, $_cobrea7, $_cobrea8, $_cobrea9, $_cobrea10, $_cobrea11, $_cobrea12, $_cobrea13, $_cobrea14, $_cobrea15, $_cobrea16, $_cobrea17, $_cobrea18, $_cobrea19, $_cobrea20, $_cobrec1, $_cobrec2, $_cobrec3, $_cobrec4, $_cobrec5, $_cobrec6, $_cobrec7, $_cobrec8, $_cobrec9, $_cobrec10, $_cobrec11, $_cobrec12, $_cobrec13, $_cobrec14, $_cobrec15, $_cobrec16, $_cobrec17, $_cobrec18, $_cobrec19, $_cobrec20, $_cobremaxinc, $_cobremaxincrel, $_cobrenummedxmuestra, $_cobrenummedxpuncurva, $_cobrenummedcurva, $_cobreconmuestra1, $_cobreconmuestra2, $_cobreprompatcalibracion, $_cobredesvestresidual, $_cobredesvestcurvacali, $_cobrenp, $_cobrelote, $_cobrevence, $_cobrecc1, $_cobrecc2, $_cobreest1, $_cobreest2, $_cobreccest1, $_cobreccest2, $_cobrevalor, $_cobreincexp, $_cobrek, $_cobreincest, $_cobreestandar1, $_cobreestandar2, $_cobreestandar3, $_cobreestandar4, $_cobreestandar5, $_cobrepipeta1, $_cobrepipeta2, $_cobrepipeta3, $_cobrepipeta4, $_cobrepipeta5, $_cobrevolcorrpipeta1, $_cobrevolcorrpipeta2, $_cobrevolcorrpipeta3, $_cobrevolcorrpipeta4, $_cobrevolcorrpipeta5, $_cobreincpip1, $_cobreincpip2, $_cobreincpip3, $_cobreincpip4, $_cobreincpip5, $_cobrebalon1, $_cobrebalon2, $_cobrebalon3, $_cobrebalon4, $_cobrebalon5, $_cobredilinc1, $_cobredilinc2, $_cobredilinc3, $_cobredilinc4, $_cobredilinc5, $_fosforocon1, $_fosforocon2, $_fosforocon3, $_fosforocon4, $_fosforocon5, $_fosforoinc1, $_fosforoinc2, $_fosforoinc3, $_fosforoinc4, $_fosforoinc5, $_fosforoincrel1, $_fosforoincrel2, $_fosforoincrel3, $_fosforoincrel4, $_fosforoincrel5, $_fosforoA11, $_fosforoA12, $_fosforoA13, $_fosforoA14, $_fosforoA15, $_fosforoA21, $_fosforoA22, $_fosforoA23, $_fosforoA24, $_fosforoA25, $_fosforoA31, $_fosforoA32, $_fosforoA33, $_fosforoA34, $_fosforoA35, $_fosforoA41, $_fosforoA42, $_fosforoA43, $_fosforoA44, $_fosforoA45, $_fosforoC11, $_fosforoC12, $_fosforoC13, $_fosforoC14, $_fosforoC15, $_fosforoC21, $_fosforoC22, $_fosforoC23, $_fosforoC24, $_fosforoC25, $_fosforoC31, $_fosforoC32, $_fosforoC33, $_fosforoC34, $_fosforoC35, $_fosforoC41, $_fosforoC42, $_fosforoC43, $_fosforoC44, $_fosforoC45, $_fosforocp1, $_fosforocp2, $_fosforocp3, $_fosforocp4, $_fosforocp5, $_fosforoa1, $_fosforoa2, $_fosforoa3, $_fosforoa4, $_fosforoa5, $_fosforoa6, $_fosforoa7, $_fosforoa8, $_fosforoa9, $_fosforoa10, $_fosforoa11, $_fosforoa12, $_fosforoa13, $_fosforoa14, $_fosforoa15, $_fosforoa16, $_fosforoa17, $_fosforoa18, $_fosforoa19, $_fosforoa20, $_fosforoc1, $_fosforoc2, $_fosforoc3, $_fosforoc4, $_fosforoc5, $_fosforoc6, $_fosforoc7, $_fosforoc8, $_fosforoc9, $_fosforoc10, $_fosforoc11, $_fosforoc12, $_fosforoc13, $_fosforoc14, $_fosforoc15, $_fosforoc16, $_fosforoc17, $_fosforoc18, $_fosforoc19, $_fosforoc20, $_fosforomaxinc, $_fosforomaxincrel, $_fosforonummedxmuestra, $_fosforonummedxpuncurva, $_fosforonummedcurva, $_fosforoconmuestra1, $_fosforoconmuestra2, $_fosforoprompatcalibracion, $_fosforodesvestresidual, $_fosforodesvestcurvacali, $_fosforonp, $_fosforolote, $_fosforovence, $_fosforocc1, $_fosforocc2, $_fosforoest1, $_fosforoest2, $_fosforoccest1, $_fosforoccest2, $_fosforovalor, $_fosforoincexp, $_fosforok, $_fosforoincest, $_fosforoestandar1, $_fosforoestandar2, $_fosforoestandar3, $_fosforoestandar4, $_fosforoestandar5, $_fosforopipeta1, $_fosforopipeta2, $_fosforopipeta3, $_fosforopipeta4, $_fosforopipeta5, $_fosforovolcorrpipeta1, $_fosforovolcorrpipeta2, $_fosforovolcorrpipeta3, $_fosforovolcorrpipeta4, $_fosforovolcorrpipeta5, $_fosforoincpip1, $_fosforoincpip2, $_fosforoincpip3, $_fosforoincpip4, $_fosforoincpip5, $_fosforobalon1, $_fosforobalon2, $_fosforobalon3, $_fosforobalon4, $_fosforobalon5, $_fosforodilinc1, $_fosforodilinc2, $_fosforodilinc3, $_fosforodilinc4, $_fosforodilinc5, $_hierrocon1, $_hierrocon2, $_hierrocon3, $_hierrocon4, $_hierrocon5, $_hierroinc1, $_hierroinc2, $_hierroinc3, $_hierroinc4, $_hierroinc5, $_hierroincrel1, $_hierroincrel2, $_hierroincrel3, $_hierroincrel4, $_hierroincrel5, $_hierroA11, $_hierroA12, $_hierroA13, $_hierroA14, $_hierroA15, $_hierroA21, $_hierroA22, $_hierroA23, $_hierroA24, $_hierroA25, $_hierroA31, $_hierroA32, $_hierroA33, $_hierroA34, $_hierroA35, $_hierroA41, $_hierroA42, $_hierroA43, $_hierroA44, $_hierroA45, $_hierroC11, $_hierroC12, $_hierroC13, $_hierroC14, $_hierroC15, $_hierroC21, $_hierroC22, $_hierroC23, $_hierroC24, $_hierroC25, $_hierroC31, $_hierroC32, $_hierroC33, $_hierroC34, $_hierroC35, $_hierroC41, $_hierroC42, $_hierroC43, $_hierroC44, $_hierroC45, $_hierrocp1, $_hierrocp2, $_hierrocp3, $_hierrocp4, $_hierrocp5, $_hierroa1, $_hierroa2, $_hierroa3, $_hierroa4, $_hierroa5, $_hierroa6, $_hierroa7, $_hierroa8, $_hierroa9, $_hierroa10, $_hierroa11, $_hierroa12, $_hierroa13, $_hierroa14, $_hierroa15, $_hierroa16, $_hierroa17, $_hierroa18, $_hierroa19, $_hierroa20, $_hierroc1, $_hierroc2, $_hierroc3, $_hierroc4, $_hierroc5, $_hierroc6, $_hierroc7, $_hierroc8, $_hierroc9, $_hierroc10, $_hierroc11, $_hierroc12, $_hierroc13, $_hierroc14, $_hierroc15, $_hierroc16, $_hierroc17, $_hierroc18, $_hierroc19, $_hierroc20, $_hierromaxinc, $_hierromaxincrel, $_hierronummedxmuestra, $_hierronummedxpuncurva, $_hierronummedcurva, $_hierroconmuestra1, $_hierroconmuestra2, $_hierroprompatcalibracion, $_hierrodesvestresidual, $_hierrodesvestcurvacali, $_hierronp, $_hierrolote, $_hierrovence, $_hierrocc1, $_hierrocc2, $_hierroest1, $_hierroest2, $_hierroccest1, $_hierroccest2, $_hierrovalor, $_hierroincexp, $_hierrok, $_hierroincest, $_hierroestandar1, $_hierroestandar2, $_hierroestandar3, $_hierroestandar4, $_hierroestandar5, $_hierropipeta1, $_hierropipeta2, $_hierropipeta3, $_hierropipeta4, $_hierropipeta5, $_hierrovolcorrpipeta1, $_hierrovolcorrpipeta2, $_hierrovolcorrpipeta3, $_hierrovolcorrpipeta4, $_hierrovolcorrpipeta5, $_hierroincpip1, $_hierroincpip2, $_hierroincpip3, $_hierroincpip4, $_hierroincpip5, $_hierrobalon1, $_hierrobalon2, $_hierrobalon3, $_hierrobalon4, $_hierrobalon5, $_hierrodilinc1, $_hierrodilinc2, $_hierrodilinc3, $_hierrodilinc4, $_hierrodilinc5, $_magnesiocon1, $_magnesiocon2, $_magnesiocon3, $_magnesiocon4, $_magnesiocon5, $_magnesioinc1, $_magnesioinc2, $_magnesioinc3, $_magnesioinc4, $_magnesioinc5, $_magnesioincrel1, $_magnesioincrel2, $_magnesioincrel3, $_magnesioincrel4, $_magnesioincrel5, $_magnesioA11, $_magnesioA12, $_magnesioA13, $_magnesioA14, $_magnesioA15, $_magnesioA21, $_magnesioA22, $_magnesioA23, $_magnesioA24, $_magnesioA25, $_magnesioA31, $_magnesioA32, $_magnesioA33, $_magnesioA34, $_magnesioA35, $_magnesioA41, $_magnesioA42, $_magnesioA43, $_magnesioA44, $_magnesioA45, $_magnesioC11, $_magnesioC12, $_magnesioC13, $_magnesioC14, $_magnesioC15, $_magnesioC21, $_magnesioC22, $_magnesioC23, $_magnesioC24, $_magnesioC25, $_magnesioC31, $_magnesioC32, $_magnesioC33, $_magnesioC34, $_magnesioC35, $_magnesioC41, $_magnesioC42, $_magnesioC43, $_magnesioC44, $_magnesioC45, $_magnesiocp1, $_magnesiocp2, $_magnesiocp3, $_magnesiocp4, $_magnesiocp5, $_magnesioa1, $_magnesioa2, $_magnesioa3, $_magnesioa4, $_magnesioa5, $_magnesioa6, $_magnesioa7, $_magnesioa8, $_magnesioa9, $_magnesioa10, $_magnesioa11, $_magnesioa12, $_magnesioa13, $_magnesioa14, $_magnesioa15, $_magnesioa16, $_magnesioa17, $_magnesioa18, $_magnesioa19, $_magnesioa20, $_magnesioc1, $_magnesioc2, $_magnesioc3, $_magnesioc4, $_magnesioc5, $_magnesioc6, $_magnesioc7, $_magnesioc8, $_magnesioc9, $_magnesioc10, $_magnesioc11, $_magnesioc12, $_magnesioc13, $_magnesioc14, $_magnesioc15, $_magnesioc16, $_magnesioc17, $_magnesioc18, $_magnesioc19, $_magnesioc20, $_magnesiomaxinc, $_magnesiomaxincrel, $_magnesionummedxmuestra, $_magnesionummedxpuncurva, $_magnesionummedcurva, $_magnesioconmuestra1, $_magnesioconmuestra2, $_magnesioprompatcalibracion, $_magnesiodesvestresidual, $_magnesiodesvestcurvacali, $_magnesionp, $_magnesiolote, $_magnesiovence, $_magnesiocc1, $_magnesiocc2, $_magnesioest1, $_magnesioest2, $_magnesioccest1, $_magnesioccest2, $_magnesiovalor, $_magnesioincexp, $_magnesiok, $_magnesioincest, $_magnesioestandar1, $_magnesioestandar2, $_magnesioestandar3, $_magnesioestandar4, $_magnesioestandar5, $_magnesiopipeta1, $_magnesiopipeta2, $_magnesiopipeta3, $_magnesiopipeta4, $_magnesiopipeta5, $_magnesiovolcorrpipeta1, $_magnesiovolcorrpipeta2, $_magnesiovolcorrpipeta3, $_magnesiovolcorrpipeta4, $_magnesiovolcorrpipeta5, $_magnesioincpip1, $_magnesioincpip2, $_magnesioincpip3, $_magnesioincpip4, $_magnesioincpip5, $_magnesiobalon1, $_magnesiobalon2, $_magnesiobalon3, $_magnesiobalon4, $_magnesiobalon5, $_magnesiodilinc1, $_magnesiodilinc2, $_magnesiodilinc3, $_magnesiodilinc4, $_magnesiodilinc5, $_manganesocon1, $_manganesocon2, $_manganesocon3, $_manganesocon4, $_manganesocon5, $_manganesoinc1, $_manganesoinc2, $_manganesoinc3, $_manganesoinc4, $_manganesoinc5, $_manganesoincrel1, $_manganesoincrel2, $_manganesoincrel3, $_manganesoincrel4, $_manganesoincrel5, $_manganesoA11, $_manganesoA12, $_manganesoA13, $_manganesoA14, $_manganesoA15, $_manganesoA21, $_manganesoA22, $_manganesoA23, $_manganesoA24, $_manganesoA25, $_manganesoA31, $_manganesoA32, $_manganesoA33, $_manganesoA34, $_manganesoA35, $_manganesoA41, $_manganesoA42, $_manganesoA43, $_manganesoA44, $_manganesoA45, $_manganesoC11, $_manganesoC12, $_manganesoC13, $_manganesoC14, $_manganesoC15, $_manganesoC21, $_manganesoC22, $_manganesoC23, $_manganesoC24, $_manganesoC25, $_manganesoC31, $_manganesoC32, $_manganesoC33, $_manganesoC34, $_manganesoC35, $_manganesoC41, $_manganesoC42, $_manganesoC43, $_manganesoC44, $_manganesoC45, $_manganesocp1, $_manganesocp2, $_manganesocp3, $_manganesocp4, $_manganesocp5, $_manganesoa1, $_manganesoa2, $_manganesoa3, $_manganesoa4, $_manganesoa5, $_manganesoa6, $_manganesoa7, $_manganesoa8, $_manganesoa9, $_manganesoa10, $_manganesoa11, $_manganesoa12, $_manganesoa13, $_manganesoa14, $_manganesoa15, $_manganesoa16, $_manganesoa17, $_manganesoa18, $_manganesoa19, $_manganesoa20, $_manganesoc1, $_manganesoc2, $_manganesoc3, $_manganesoc4, $_manganesoc5, $_manganesoc6, $_manganesoc7, $_manganesoc8, $_manganesoc9, $_manganesoc10, $_manganesoc11, $_manganesoc12, $_manganesoc13, $_manganesoc14, $_manganesoc15, $_manganesoc16, $_manganesoc17, $_manganesoc18, $_manganesoc19, $_manganesoc20, $_manganesomaxinc, $_manganesomaxincrel, $_manganesonummedxmuestra, $_manganesonummedxpuncurva, $_manganesonummedcurva, $_manganesoconmuestra1, $_manganesoconmuestra2, $_manganesoprompatcalibracion, $_manganesodesvestresidual, $_manganesodesvestcurvacali, $_manganesonp, $_manganesolote, $_manganesovence, $_manganesocc1, $_manganesocc2, $_manganesoest1, $_manganesoest2, $_manganesoccest1, $_manganesoccest2, $_manganesovalor, $_manganesoincexp, $_manganesok, $_manganesoincest, $_manganesoestandar1, $_manganesoestandar2, $_manganesoestandar3, $_manganesoestandar4, $_manganesoestandar5, $_manganesopipeta1, $_manganesopipeta2, $_manganesopipeta3, $_manganesopipeta4, $_manganesopipeta5, $_manganesovolcorrpipeta1, $_manganesovolcorrpipeta2, $_manganesovolcorrpipeta3, $_manganesovolcorrpipeta4, $_manganesovolcorrpipeta5, $_manganesoincpip1, $_manganesoincpip2, $_manganesoincpip3, $_manganesoincpip4, $_manganesoincpip5, $_manganesobalon1, $_manganesobalon2, $_manganesobalon3, $_manganesobalon4, $_manganesobalon5, $_manganesodilinc1, $_manganesodilinc2, $_manganesodilinc3, $_manganesodilinc4, $_manganesodilinc5, $_potasiocon1, $_potasiocon2, $_potasiocon3, $_potasiocon4, $_potasiocon5, $_potasioinc1, $_potasioinc2, $_potasioinc3, $_potasioinc4, $_potasioinc5, $_potasioincrel1, $_potasioincrel2, $_potasioincrel3, $_potasioincrel4, $_potasioincrel5, $_potasioA11, $_potasioA12, $_potasioA13, $_potasioA14, $_potasioA15, $_potasioA21, $_potasioA22, $_potasioA23, $_potasioA24, $_potasioA25, $_potasioA31, $_potasioA32, $_potasioA33, $_potasioA34, $_potasioA35, $_potasioA41, $_potasioA42, $_potasioA43, $_potasioA44, $_potasioA45, $_potasioC11, $_potasioC12, $_potasioC13, $_potasioC14, $_potasioC15, $_potasioC21, $_potasioC22, $_potasioC23, $_potasioC24, $_potasioC25, $_potasioC31, $_potasioC32, $_potasioC33, $_potasioC34, $_potasioC35, $_potasioC41, $_potasioC42, $_potasioC43, $_potasioC44, $_potasioC45, $_potasiocp1, $_potasiocp2, $_potasiocp3, $_potasiocp4, $_potasiocp5, $_potasioa1, $_potasioa2, $_potasioa3, $_potasioa4, $_potasioa5, $_potasioa6, $_potasioa7, $_potasioa8, $_potasioa9, $_potasioa10, $_potasioa11, $_potasioa12, $_potasioa13, $_potasioa14, $_potasioa15, $_potasioa16, $_potasioa17, $_potasioa18, $_potasioa19, $_potasioa20, $_potasioc1, $_potasioc2, $_potasioc3, $_potasioc4, $_potasioc5, $_potasioc6, $_potasioc7, $_potasioc8, $_potasioc9, $_potasioc10, $_potasioc11, $_potasioc12, $_potasioc13, $_potasioc14, $_potasioc15, $_potasioc16, $_potasioc17, $_potasioc18, $_potasioc19, $_potasioc20, $_potasiomaxinc, $_potasiomaxincrel, $_potasionummedxmuestra, $_potasionummedxpuncurva, $_potasionummedcurva, $_potasioconmuestra1, $_potasioconmuestra2, $_potasioprompatcalibracion, $_potasiodesvestresidual, $_potasiodesvestcurvacali, $_potasionp, $_potasiolote, $_potasiovence, $_potasiocc1, $_potasiocc2, $_potasioest1, $_potasioest2, $_potasioccest1, $_potasioccest2, $_potasiovalor, $_potasioincexp, $_potasiok, $_potasioincest, $_potasioestandar1, $_potasioestandar2, $_potasioestandar3, $_potasioestandar4, $_potasioestandar5, $_potasiopipeta1, $_potasiopipeta2, $_potasiopipeta3, $_potasiopipeta4, $_potasiopipeta5, $_potasiovolcorrpipeta1, $_potasiovolcorrpipeta2, $_potasiovolcorrpipeta3, $_potasiovolcorrpipeta4, $_potasiovolcorrpipeta5, $_potasioincpip1, $_potasioincpip2, $_potasioincpip3, $_potasioincpip4, $_potasioincpip5, $_potasiobalon1, $_potasiobalon2, $_potasiobalon3, $_potasiobalon4, $_potasiobalon5, $_potasiodilinc1, $_potasiodilinc2, $_potasiodilinc3, $_potasiodilinc4, $_potasiodilinc5, $_zinccon1, $_zinccon2, $_zinccon3, $_zinccon4, $_zinccon5, $_zincinc1, $_zincinc2, $_zincinc3, $_zincinc4, $_zincinc5, $_zincincrel1, $_zincincrel2, $_zincincrel3, $_zincincrel4, $_zincincrel5, $_zincA11, $_zincA12, $_zincA13, $_zincA14, $_zincA15, $_zincA21, $_zincA22, $_zincA23, $_zincA24, $_zincA25, $_zincA31, $_zincA32, $_zincA33, $_zincA34, $_zincA35, $_zincA41, $_zincA42, $_zincA43, $_zincA44, $_zincA45, $_zincC11, $_zincC12, $_zincC13, $_zincC14, $_zincC15, $_zincC21, $_zincC22, $_zincC23, $_zincC24, $_zincC25, $_zincC31, $_zincC32, $_zincC33, $_zincC34, $_zincC35, $_zincC41, $_zincC42, $_zincC43, $_zincC44, $_zincC45, $_zinccp1, $_zinccp2, $_zinccp3, $_zinccp4, $_zinccp5, $_zinca1, $_zinca2, $_zinca3, $_zinca4, $_zinca5, $_zinca6, $_zinca7, $_zinca8, $_zinca9, $_zinca10, $_zinca11, $_zinca12, $_zinca13, $_zinca14, $_zinca15, $_zinca16, $_zinca17, $_zinca18, $_zinca19, $_zinca20, $_zincc1, $_zincc2, $_zincc3, $_zincc4, $_zincc5, $_zincc6, $_zincc7, $_zincc8, $_zincc9, $_zincc10, $_zincc11, $_zincc12, $_zincc13, $_zincc14, $_zincc15, $_zincc16, $_zincc17, $_zincc18, $_zincc19, $_zincc20, $_zincmaxinc, $_zincmaxincrel, $_zincnummedxmuestra, $_zincnummedxpuncurva, $_zincnummedcurva, $_zincconmuestra1, $_zincconmuestra2, $_zincprompatcalibracion, $_zincdesvestresidual, $_zincdesvestcurvacali, $_zincnp, $_zinclote, $_zincvence, $_zinccc1, $_zinccc2, $_zincest1, $_zincest2, $_zincccest1, $_zincccest2, $_zincvalor, $_zincincexp, $_zinck, $_zincincest, $_zincestandar1, $_zincestandar2, $_zincestandar3, $_zincestandar4, $_zincestandar5, $_zincpipeta1, $_zincpipeta2, $_zincpipeta3, $_zincpipeta4, $_zincpipeta5, $_zincvolcorrpipeta1, $_zincvolcorrpipeta2, $_zincvolcorrpipeta3, $_zincvolcorrpipeta4, $_zincvolcorrpipeta5, $_zincincpip1, $_zincincpip2, $_zincincpip3, $_zincincpip4, $_zincincpip5, $_zincbalon1, $_zincbalon2, $_zincbalon3, $_zincbalon4, $_zincbalon5, $_zincdilinc1, $_zincdilinc2, $_zincdilinc3, $_zincdilinc4, $_zincdilinc5, $_nitrogenocon1, $_nitrogenocon2, $_nitrogenocon3, $_nitrogenocon4, $_nitrogenocon5, $_nitrogenoinc1, $_nitrogenoinc2, $_nitrogenoinc3, $_nitrogenoinc4, $_nitrogenoinc5, $_nitrogenoincrel1, $_nitrogenoincrel2, $_nitrogenoincrel3, $_nitrogenoincrel4, $_nitrogenoincrel5, $_nitrogenoA11, $_nitrogenoA12, $_nitrogenoA13, $_nitrogenoA14, $_nitrogenoA15, $_nitrogenoA21, $_nitrogenoA22, $_nitrogenoA23, $_nitrogenoA24, $_nitrogenoA25, $_nitrogenoA31, $_nitrogenoA32, $_nitrogenoA33, $_nitrogenoA34, $_nitrogenoA35, $_nitrogenoA41, $_nitrogenoA42, $_nitrogenoA43, $_nitrogenoA44, $_nitrogenoA45, $_nitrogenoC11, $_nitrogenoC12, $_nitrogenoC13, $_nitrogenoC14, $_nitrogenoC15, $_nitrogenoC21, $_nitrogenoC22, $_nitrogenoC23, $_nitrogenoC24, $_nitrogenoC25, $_nitrogenoC31, $_nitrogenoC32, $_nitrogenoC33, $_nitrogenoC34, $_nitrogenoC35, $_nitrogenoC41, $_nitrogenoC42, $_nitrogenoC43, $_nitrogenoC44, $_nitrogenoC45, $_nitrogenocp1, $_nitrogenocp2, $_nitrogenocp3, $_nitrogenocp4, $_nitrogenocp5, $_nitrogenoa1, $_nitrogenoa2, $_nitrogenoa3, $_nitrogenoa4, $_nitrogenoa5, $_nitrogenoa6, $_nitrogenoa7, $_nitrogenoa8, $_nitrogenoa9, $_nitrogenoa10, $_nitrogenoa11, $_nitrogenoa12, $_nitrogenoa13, $_nitrogenoa14, $_nitrogenoa15, $_nitrogenoa16, $_nitrogenoa17, $_nitrogenoa18, $_nitrogenoa19, $_nitrogenoa20, $_nitrogenoc1, $_nitrogenoc2, $_nitrogenoc3, $_nitrogenoc4, $_nitrogenoc5, $_nitrogenoc6, $_nitrogenoc7, $_nitrogenoc8, $_nitrogenoc9, $_nitrogenoc10, $_nitrogenoc11, $_nitrogenoc12, $_nitrogenoc13, $_nitrogenoc14, $_nitrogenoc15, $_nitrogenoc16, $_nitrogenoc17, $_nitrogenoc18, $_nitrogenoc19, $_nitrogenoc20, $_nitrogenomaxinc, $_nitrogenomaxincrel, $_nitrogenonummedxmuestra, $_nitrogenonummedxpuncurva, $_nitrogenonummedcurva, $_nitrogenoconmuestra1, $_nitrogenoconmuestra2, $_nitrogenoprompatcalibracion, $_nitrogenodesvestresidual, $_nitrogenodesvestcurvacali, $_nitrogenonp, $_nitrogenolote, $_nitrogenovence, $_nitrogenocc1, $_nitrogenocc2, $_nitrogenoest1, $_nitrogenoest2, $_nitrogenoccest1, $_nitrogenoccest2, $_nitrogenovalor, $_nitrogenoincexp, $_nitrogenok, $_nitrogenoincest, $_nitrogenoestandar1, $_nitrogenoestandar2, $_nitrogenoestandar3, $_nitrogenoestandar4, $_nitrogenoestandar5, $_nitrogenopipeta1, $_nitrogenopipeta2, $_nitrogenopipeta3, $_nitrogenopipeta4, $_nitrogenopipeta5, $_nitrogenovolcorrpipeta1, $_nitrogenovolcorrpipeta2, $_nitrogenovolcorrpipeta3, $_nitrogenovolcorrpipeta4, $_nitrogenovolcorrpipeta5, $_nitrogenoincpip1, $_nitrogenoincpip2, $_nitrogenoincpip3, $_nitrogenoincpip4, $_nitrogenoincpip5, $_nitrogenobalon1, $_nitrogenobalon2, $_nitrogenobalon3, $_nitrogenobalon4, $_nitrogenobalon5, $_nitrogenodilinc1, $_nitrogenodilinc2, $_nitrogenodilinc3, $_nitrogenodilinc4, $_nitrogenodilinc5, $_azufrecon1, $_azufrecon2, $_azufrecon3, $_azufrecon4, $_azufrecon5, $_azufreinc1, $_azufreinc2, $_azufreinc3, $_azufreinc4, $_azufreinc5, $_azufreincrel1, $_azufreincrel2, $_azufreincrel3, $_azufreincrel4, $_azufreincrel5, $_azufreA11, $_azufreA12, $_azufreA13, $_azufreA14, $_azufreA15, $_azufreA21, $_azufreA22, $_azufreA23, $_azufreA24, $_azufreA25, $_azufreA31, $_azufreA32, $_azufreA33, $_azufreA34, $_azufreA35, $_azufreA41, $_azufreA42, $_azufreA43, $_azufreA44, $_azufreA45, $_azufreC11, $_azufreC12, $_azufreC13, $_azufreC14, $_azufreC15, $_azufreC21, $_azufreC22, $_azufreC23, $_azufreC24, $_azufreC25, $_azufreC31, $_azufreC32, $_azufreC33, $_azufreC34, $_azufreC35, $_azufreC41, $_azufreC42, $_azufreC43, $_azufreC44, $_azufreC45, $_azufrecp1, $_azufrecp2, $_azufrecp3, $_azufrecp4, $_azufrecp5, $_azufrea1, $_azufrea2, $_azufrea3, $_azufrea4, $_azufrea5, $_azufrea6, $_azufrea7, $_azufrea8, $_azufrea9, $_azufrea10, $_azufrea11, $_azufrea12, $_azufrea13, $_azufrea14, $_azufrea15, $_azufrea16, $_azufrea17, $_azufrea18, $_azufrea19, $_azufrea20, $_azufrec1, $_azufrec2, $_azufrec3, $_azufrec4, $_azufrec5, $_azufrec6, $_azufrec7, $_azufrec8, $_azufrec9, $_azufrec10, $_azufrec11, $_azufrec12, $_azufrec13, $_azufrec14, $_azufrec15, $_azufrec16, $_azufrec17, $_azufrec18, $_azufrec19, $_azufrec20, $_azufremaxinc, $_azufremaxincrel, $_azufrenummedxmuestra, $_azufrenummedxpuncurva, $_azufrenummedcurva, $_azufreconmuestra1, $_azufreconmuestra2, $_azufreprompatcalibracion, $_azufredesvestresidual, $_azufredesvestcurvacali, $_azufrenp, $_azufrelote, $_azufrevence, $_azufrecc1, $_azufrecc2, $_azufreest1, $_azufreest2, $_azufreccest1, $_azufreccest2, $_azufrevalor, $_azufreincexp, $_azufrek, $_azufreincest, $_azufreestandar1, $_azufreestandar2, $_azufreestandar3, $_azufreestandar4, $_azufreestandar5, $_azufrepipeta1, $_azufrepipeta2, $_azufrepipeta3, $_azufrepipeta4, $_azufrepipeta5, $_azufrevolcorrpipeta1, $_azufrevolcorrpipeta2, $_azufrevolcorrpipeta3, $_azufrevolcorrpipeta4, $_azufrevolcorrpipeta5, $_azufreincpip1, $_azufreincpip2, $_azufreincpip3, $_azufreincpip4, $_azufreincpip5, $_azufrebalon1, $_azufrebalon2, $_azufrebalon3, $_azufrebalon4, $_azufrebalon5, $_azufredilinc1, $_azufredilinc2, $_azufredilinc3, $_azufredilinc4, $_azufredilinc5, $_arsenicocon1, $_arsenicocon2, $_arsenicocon3, $_arsenicocon4, $_arsenicocon5, $_arsenicoinc1, $_arsenicoinc2, $_arsenicoinc3, $_arsenicoinc4, $_arsenicoinc5, $_arsenicoincrel1, $_arsenicoincrel2, $_arsenicoincrel3, $_arsenicoincrel4, $_arsenicoincrel5, $_arsenicoA11, $_arsenicoA12, $_arsenicoA13, $_arsenicoA14, $_arsenicoA15, $_arsenicoA21, $_arsenicoA22, $_arsenicoA23, $_arsenicoA24, $_arsenicoA25, $_arsenicoA31, $_arsenicoA32, $_arsenicoA33, $_arsenicoA34, $_arsenicoA35, $_arsenicoA41, $_arsenicoA42, $_arsenicoA43, $_arsenicoA44, $_arsenicoA45, $_arsenicoC11, $_arsenicoC12, $_arsenicoC13, $_arsenicoC14, $_arsenicoC15, $_arsenicoC21, $_arsenicoC22, $_arsenicoC23, $_arsenicoC24, $_arsenicoC25, $_arsenicoC31, $_arsenicoC32, $_arsenicoC33, $_arsenicoC34, $_arsenicoC35, $_arsenicoC41, $_arsenicoC42, $_arsenicoC43, $_arsenicoC44, $_arsenicoC45, $_arsenicocp1, $_arsenicocp2, $_arsenicocp3, $_arsenicocp4, $_arsenicocp5, $_arsenicoa1, $_arsenicoa2, $_arsenicoa3, $_arsenicoa4, $_arsenicoa5, $_arsenicoa6, $_arsenicoa7, $_arsenicoa8, $_arsenicoa9, $_arsenicoa10, $_arsenicoa11, $_arsenicoa12, $_arsenicoa13, $_arsenicoa14, $_arsenicoa15, $_arsenicoa16, $_arsenicoa17, $_arsenicoa18, $_arsenicoa19, $_arsenicoa20, $_arsenicoc1, $_arsenicoc2, $_arsenicoc3, $_arsenicoc4, $_arsenicoc5, $_arsenicoc6, $_arsenicoc7, $_arsenicoc8, $_arsenicoc9, $_arsenicoc10, $_arsenicoc11, $_arsenicoc12, $_arsenicoc13, $_arsenicoc14, $_arsenicoc15, $_arsenicoc16, $_arsenicoc17, $_arsenicoc18, $_arsenicoc19, $_arsenicoc20, $_arsenicomaxinc, $_arsenicomaxincrel, $_arseniconummedxmuestra, $_arseniconummedxpuncurva, $_arseniconummedcurva, $_arsenicoconmuestra1, $_arsenicoconmuestra2, $_arsenicoprompatcalibracion, $_arsenicodesvestresidual, $_arsenicodesvestcurvacali, $_arseniconp, $_arsenicolote, $_arsenicovence, $_arsenicocc1, $_arsenicocc2, $_arsenicoest1, $_arsenicoest2, $_arsenicoccest1, $_arsenicoccest2, $_arsenicovalor, $_arsenicoincexp, $_arsenicok, $_arsenicoincest, $_arsenicoestandar1, $_arsenicoestandar2, $_arsenicoestandar3, $_arsenicoestandar4, $_arsenicoestandar5, $_arsenicopipeta1, $_arsenicopipeta2, $_arsenicopipeta3, $_arsenicopipeta4, $_arsenicopipeta5, $_arsenicovolcorrpipeta1, $_arsenicovolcorrpipeta2, $_arsenicovolcorrpipeta3, $_arsenicovolcorrpipeta4, $_arsenicovolcorrpipeta5, $_arsenicoincpip1, $_arsenicoincpip2, $_arsenicoincpip3, $_arsenicoincpip4, $_arsenicoincpip5, $_arsenicobalon1, $_arsenicobalon2, $_arsenicobalon3, $_arsenicobalon4, $_arsenicobalon5, $_arsenicodilinc1, $_arsenicodilinc2, $_arsenicodilinc3, $_arsenicodilinc4, $_arsenicodilinc5, $_cadmiocon1, $_cadmiocon2, $_cadmiocon3, $_cadmiocon4, $_cadmiocon5, $_cadmioinc1, $_cadmioinc2, $_cadmioinc3, $_cadmioinc4, $_cadmioinc5, $_cadmioincrel1, $_cadmioincrel2, $_cadmioincrel3, $_cadmioincrel4, $_cadmioincrel5, $_cadmioA11, $_cadmioA12, $_cadmioA13, $_cadmioA14, $_cadmioA15, $_cadmioA21, $_cadmioA22, $_cadmioA23, $_cadmioA24, $_cadmioA25, $_cadmioA31, $_cadmioA32, $_cadmioA33, $_cadmioA34, $_cadmioA35, $_cadmioA41, $_cadmioA42, $_cadmioA43, $_cadmioA44, $_cadmioA45, $_cadmioC11, $_cadmioC12, $_cadmioC13, $_cadmioC14, $_cadmioC15, $_cadmioC21, $_cadmioC22, $_cadmioC23, $_cadmioC24, $_cadmioC25, $_cadmioC31, $_cadmioC32, $_cadmioC33, $_cadmioC34, $_cadmioC35, $_cadmioC41, $_cadmioC42, $_cadmioC43, $_cadmioC44, $_cadmioC45, $_cadmiocp1, $_cadmiocp2, $_cadmiocp3, $_cadmiocp4, $_cadmiocp5, $_cadmioa1, $_cadmioa2, $_cadmioa3, $_cadmioa4, $_cadmioa5, $_cadmioa6, $_cadmioa7, $_cadmioa8, $_cadmioa9, $_cadmioa10, $_cadmioa11, $_cadmioa12, $_cadmioa13, $_cadmioa14, $_cadmioa15, $_cadmioa16, $_cadmioa17, $_cadmioa18, $_cadmioa19, $_cadmioa20, $_cadmioc1, $_cadmioc2, $_cadmioc3, $_cadmioc4, $_cadmioc5, $_cadmioc6, $_cadmioc7, $_cadmioc8, $_cadmioc9, $_cadmioc10, $_cadmioc11, $_cadmioc12, $_cadmioc13, $_cadmioc14, $_cadmioc15, $_cadmioc16, $_cadmioc17, $_cadmioc18, $_cadmioc19, $_cadmioc20, $_cadmiomaxinc, $_cadmiomaxincrel, $_cadmionummedxmuestra, $_cadmionummedxpuncurva, $_cadmionummedcurva, $_cadmioconmuestra1, $_cadmioconmuestra2, $_cadmioprompatcalibracion, $_cadmiodesvestresidual, $_cadmiodesvestcurvacali, $_cadmionp, $_cadmiolote, $_cadmiovence, $_cadmiocc1, $_cadmiocc2, $_cadmioest1, $_cadmioest2, $_cadmioccest1, $_cadmioccest2, $_cadmiovalor, $_cadmioincexp, $_cadmiok, $_cadmioincest, $_cadmioestandar1, $_cadmioestandar2, $_cadmioestandar3, $_cadmioestandar4, $_cadmioestandar5, $_cadmiopipeta1, $_cadmiopipeta2, $_cadmiopipeta3, $_cadmiopipeta4, $_cadmiopipeta5, $_cadmiovolcorrpipeta1, $_cadmiovolcorrpipeta2, $_cadmiovolcorrpipeta3, $_cadmiovolcorrpipeta4, $_cadmiovolcorrpipeta5, $_cadmioincpip1, $_cadmioincpip2, $_cadmioincpip3, $_cadmioincpip4, $_cadmioincpip5, $_cadmiobalon1, $_cadmiobalon2, $_cadmiobalon3, $_cadmiobalon4, $_cadmiobalon5, $_cadmiodilinc1, $_cadmiodilinc2, $_cadmiodilinc3, $_cadmiodilinc4, $_cadmiodilinc5, $_cromocon1, $_cromocon2, $_cromocon3, $_cromocon4, $_cromocon5, $_cromoinc1, $_cromoinc2, $_cromoinc3, $_cromoinc4, $_cromoinc5, $_cromoincrel1, $_cromoincrel2, $_cromoincrel3, $_cromoincrel4, $_cromoincrel5, $_cromoA11, $_cromoA12, $_cromoA13, $_cromoA14, $_cromoA15, $_cromoA21, $_cromoA22, $_cromoA23, $_cromoA24, $_cromoA25, $_cromoA31, $_cromoA32, $_cromoA33, $_cromoA34, $_cromoA35, $_cromoA41, $_cromoA42, $_cromoA43, $_cromoA44, $_cromoA45, $_cromoC11, $_cromoC12, $_cromoC13, $_cromoC14, $_cromoC15, $_cromoC21, $_cromoC22, $_cromoC23, $_cromoC24, $_cromoC25, $_cromoC31, $_cromoC32, $_cromoC33, $_cromoC34, $_cromoC35, $_cromoC41, $_cromoC42, $_cromoC43, $_cromoC44, $_cromoC45, $_cromocp1, $_cromocp2, $_cromocp3, $_cromocp4, $_cromocp5, $_cromoa1, $_cromoa2, $_cromoa3, $_cromoa4, $_cromoa5, $_cromoa6, $_cromoa7, $_cromoa8, $_cromoa9, $_cromoa10, $_cromoa11, $_cromoa12, $_cromoa13, $_cromoa14, $_cromoa15, $_cromoa16, $_cromoa17, $_cromoa18, $_cromoa19, $_cromoa20, $_cromoc1, $_cromoc2, $_cromoc3, $_cromoc4, $_cromoc5, $_cromoc6, $_cromoc7, $_cromoc8, $_cromoc9, $_cromoc10, $_cromoc11, $_cromoc12, $_cromoc13, $_cromoc14, $_cromoc15, $_cromoc16, $_cromoc17, $_cromoc18, $_cromoc19, $_cromoc20, $_cromomaxinc, $_cromomaxincrel, $_cromonummedxmuestra, $_cromonummedxpuncurva, $_cromonummedcurva, $_cromoconmuestra1, $_cromoconmuestra2, $_cromoprompatcalibracion, $_cromodesvestresidual, $_cromodesvestcurvacali, $_cromonp, $_cromolote, $_cromovence, $_cromocc1, $_cromocc2, $_cromoest1, $_cromoest2, $_cromoccest1, $_cromoccest2, $_cromovalor, $_cromoincexp, $_cromok, $_cromoincest, $_cromoestandar1, $_cromoestandar2, $_cromoestandar3, $_cromoestandar4, $_cromoestandar5, $_cromopipeta1, $_cromopipeta2, $_cromopipeta3, $_cromopipeta4, $_cromopipeta5, $_cromovolcorrpipeta1, $_cromovolcorrpipeta2, $_cromovolcorrpipeta3, $_cromovolcorrpipeta4, $_cromovolcorrpipeta5, $_cromoincpip1, $_cromoincpip2, $_cromoincpip3, $_cromoincpip4, $_cromoincpip5, $_cromobalon1, $_cromobalon2, $_cromobalon3, $_cromobalon4, $_cromobalon5, $_cromodilinc1, $_cromodilinc2, $_cromodilinc3, $_cromodilinc4, $_cromodilinc5, $_plomocon1, $_plomocon2, $_plomocon3, $_plomocon4, $_plomocon5, $_plomoinc1, $_plomoinc2, $_plomoinc3, $_plomoinc4, $_plomoinc5, $_plomoincrel1, $_plomoincrel2, $_plomoincrel3, $_plomoincrel4, $_plomoincrel5, $_plomoA11, $_plomoA12, $_plomoA13, $_plomoA14, $_plomoA15, $_plomoA21, $_plomoA22, $_plomoA23, $_plomoA24, $_plomoA25, $_plomoA31, $_plomoA32, $_plomoA33, $_plomoA34, $_plomoA35, $_plomoA41, $_plomoA42, $_plomoA43, $_plomoA44, $_plomoA45, $_plomoC11, $_plomoC12, $_plomoC13, $_plomoC14, $_plomoC15, $_plomoC21, $_plomoC22, $_plomoC23, $_plomoC24, $_plomoC25, $_plomoC31, $_plomoC32, $_plomoC33, $_plomoC34, $_plomoC35, $_plomoC41, $_plomoC42, $_plomoC43, $_plomoC44, $_plomoC45, $_plomocp1, $_plomocp2, $_plomocp3, $_plomocp4, $_plomocp5, $_plomoa1, $_plomoa2, $_plomoa3, $_plomoa4, $_plomoa5, $_plomoa6, $_plomoa7, $_plomoa8, $_plomoa9, $_plomoa10, $_plomoa11, $_plomoa12, $_plomoa13, $_plomoa14, $_plomoa15, $_plomoa16, $_plomoa17, $_plomoa18, $_plomoa19, $_plomoa20, $_plomoc1, $_plomoc2, $_plomoc3, $_plomoc4, $_plomoc5, $_plomoc6, $_plomoc7, $_plomoc8, $_plomoc9, $_plomoc10, $_plomoc11, $_plomoc12, $_plomoc13, $_plomoc14, $_plomoc15, $_plomoc16, $_plomoc17, $_plomoc18, $_plomoc19, $_plomoc20, $_plomomaxinc, $_plomomaxincrel, $_plomonummedxmuestra, $_plomonummedxpuncurva, $_plomonummedcurva, $_plomoconmuestra1, $_plomoconmuestra2, $_plomoprompatcalibracion, $_plomodesvestresidual, $_plomodesvestcurvacali, $_plomonp, $_plomolote, $_plomovence, $_plomocc1, $_plomocc2, $_plomoest1, $_plomoest2, $_plomoccest1, $_plomoccest2, $_plomovalor, $_plomoincexp, $_plomok, $_plomoincest, $_plomoestandar1, $_plomoestandar2, $_plomoestandar3, $_plomoestandar4, $_plomoestandar5, $_plomopipeta1, $_plomopipeta2, $_plomopipeta3, $_plomopipeta4, $_plomopipeta5, $_plomovolcorrpipeta1, $_plomovolcorrpipeta2, $_plomovolcorrpipeta3, $_plomovolcorrpipeta4, $_plomovolcorrpipeta5, $_plomoincpip1, $_plomoincpip2, $_plomoincpip3, $_plomoincpip4, $_plomoincpip5, $_plomobalon1, $_plomobalon2, $_plomobalon3, $_plomobalon4, $_plomobalon5, $_plomodilinc1, $_plomodilinc2, $_plomodilinc3, $_plomodilinc4, $_plomodilinc5, $_mercuriocon1, $_mercuriocon2, $_mercuriocon3, $_mercuriocon4, $_mercuriocon5, $_mercurioinc1, $_mercurioinc2, $_mercurioinc3, $_mercurioinc4, $_mercurioinc5, $_mercurioincrel1, $_mercurioincrel2, $_mercurioincrel3, $_mercurioincrel4, $_mercurioincrel5, $_mercurioA11, $_mercurioA12, $_mercurioA13, $_mercurioA14, $_mercurioA15, $_mercurioA21, $_mercurioA22, $_mercurioA23, $_mercurioA24, $_mercurioA25, $_mercurioA31, $_mercurioA32, $_mercurioA33, $_mercurioA34, $_mercurioA35, $_mercurioA41, $_mercurioA42, $_mercurioA43, $_mercurioA44, $_mercurioA45, $_mercurioC11, $_mercurioC12, $_mercurioC13, $_mercurioC14, $_mercurioC15, $_mercurioC21, $_mercurioC22, $_mercurioC23, $_mercurioC24, $_mercurioC25, $_mercurioC31, $_mercurioC32, $_mercurioC33, $_mercurioC34, $_mercurioC35, $_mercurioC41, $_mercurioC42, $_mercurioC43, $_mercurioC44, $_mercurioC45, $_mercuriocp1, $_mercuriocp2, $_mercuriocp3, $_mercuriocp4, $_mercuriocp5, $_mercurioa1, $_mercurioa2, $_mercurioa3, $_mercurioa4, $_mercurioa5, $_mercurioa6, $_mercurioa7, $_mercurioa8, $_mercurioa9, $_mercurioa10, $_mercurioa11, $_mercurioa12, $_mercurioa13, $_mercurioa14, $_mercurioa15, $_mercurioa16, $_mercurioa17, $_mercurioa18, $_mercurioa19, $_mercurioa20, $_mercurioc1, $_mercurioc2, $_mercurioc3, $_mercurioc4, $_mercurioc5, $_mercurioc6, $_mercurioc7, $_mercurioc8, $_mercurioc9, $_mercurioc10, $_mercurioc11, $_mercurioc12, $_mercurioc13, $_mercurioc14, $_mercurioc15, $_mercurioc16, $_mercurioc17, $_mercurioc18, $_mercurioc19, $_mercurioc20, $_mercuriomaxinc, $_mercuriomaxincrel, $_mercurionummedxmuestra, $_mercurionummedxpuncurva, $_mercurionummedcurva, $_mercurioconmuestra1, $_mercurioconmuestra2, $_mercurioprompatcalibracion, $_mercuriodesvestresidual, $_mercuriodesvestcurvacali, $_mercurionp, $_mercuriolote, $_mercuriovence, $_mercuriocc1, $_mercuriocc2, $_mercurioest1, $_mercurioest2, $_mercurioccest1, $_mercurioccest2, $_mercuriovalor, $_mercurioincexp, $_mercuriok, $_mercurioincest, $_mercurioestandar1, $_mercurioestandar2, $_mercurioestandar3, $_mercurioestandar4, $_mercurioestandar5, $_mercuriopipeta1, $_mercuriopipeta2, $_mercuriopipeta3, $_mercuriopipeta4, $_mercuriopipeta5, $_mercuriovolcorrpipeta1, $_mercuriovolcorrpipeta2, $_mercuriovolcorrpipeta3, $_mercuriovolcorrpipeta4, $_mercuriovolcorrpipeta5, $_mercurioincpip1, $_mercurioincpip2, $_mercurioincpip3, $_mercurioincpip4, $_mercurioincpip5, $_mercuriobalon1, $_mercuriobalon2, $_mercuriobalon3, $_mercuriobalon4, $_mercuriobalon5, $_mercuriodilinc1, $_mercuriodilinc2, $_mercuriodilinc3, $_mercuriodilinc4, $_mercuriodilinc5)
    {

        $this->_TRANS("UPDATE MET027 SET estado = '0' WHERE 1=1;");

        $cs = $this->_QUERY("SELECT MAX(id)+1 AS id FROM MET027 WHERE 1=1");

        $this->_TRANS("INSERT INTO MET027 VALUES('{$cs[0]['id']}', GETDATE(), '1');");
        $this->_TRANS("INSERT INTO MET027boro VALUES('{$cs[0]['id']}', '{$_borocon1}', '{$_borocon2}', '{$_borocon3}', '{$_borocon4}', '{$_borocon5}', '{$_boroinc1}', '{$_boroinc2}', '{$_boroinc3}', '{$_boroinc4}', '{$_boroinc5}', '{$_boroincrel1}', '{$_boroincrel2}', '{$_boroincrel3}', '{$_boroincrel4}', '{$_boroincrel5}', '{$_boroA11}', '{$_boroA12}', '{$_boroA13}', '{$_boroA14}', '{$_boroA15}', '{$_boroA21}', '{$_boroA22}', '{$_boroA23}', '{$_boroA24}', '{$_boroA25}', '{$_boroA31}', '{$_boroA32}', '{$_boroA33}', '{$_boroA34}', '{$_boroA35}', '{$_boroA41}', '{$_boroA42}', '{$_boroA43}', '{$_boroA44}', '{$_boroA45}', '{$_boroC11}', '{$_boroC12}', '{$_boroC13}', '{$_boroC14}', '{$_boroC15}', '{$_boroC21}', '{$_boroC22}', '{$_boroC23}', '{$_boroC24}', '{$_boroC25}', '{$_boroC31}', '{$_boroC32}', '{$_boroC33}', '{$_boroC34}', '{$_boroC35}', '{$_boroC41}', '{$_boroC42}', '{$_boroC43}', '{$_boroC44}', '{$_boroC45}', '{$_borocp1}', '{$_borocp2}', '{$_borocp3}', '{$_borocp4}', '{$_borocp5}', '{$_boroa1}', '{$_boroa2}', '{$_boroa3}', '{$_boroa4}', '{$_boroa5}', '{$_boroa6}', '{$_boroa7}', '{$_boroa8}', '{$_boroa9}', '{$_boroa10}', '{$_boroa11}', '{$_boroa12}', '{$_boroa13}', '{$_boroa14}', '{$_boroa15}', '{$_boroa16}', '{$_boroa17}', '{$_boroa18}', '{$_boroa19}', '{$_boroa20}', '{$_boroc1}', '{$_boroc2}', '{$_boroc3}', '{$_boroc4}', '{$_boroc5}', '{$_boroc6}', '{$_boroc7}', '{$_boroc8}', '{$_boroc9}', '{$_boroc10}', '{$_boroc11}', '{$_boroc12}', '{$_boroc13}', '{$_boroc14}', '{$_boroc15}', '{$_boroc16}', '{$_boroc17}', '{$_boroc18}', '{$_boroc19}', '{$_boroc20}', '{$_boromaxinc}', '{$_boromaxincrel}', '{$_boronummedxmuestra}', '{$_boronummedxpuncurva}', '{$_boronummedcurva}', '{$_boroconmuestra1}', '{$_boroconmuestra2}', '{$_boroprompatcalibracion}', '{$_borodesvestresidual}', '{$_borodesvestcurvacali}', '{$_boronp}', '{$_borolote}', '{$_borovence}', '{$_borocc1}', '{$_borocc2}', '{$_boroest1}', '{$_boroest2}', '{$_boroccest1}', '{$_boroccest2}', '{$_borovalor}', '{$_boroincexp}', '{$_borok}', '{$_boroincest}', '{$_boroestandar1}', '{$_boroestandar2}', '{$_boroestandar3}', '{$_boroestandar4}', '{$_boroestandar5}', '{$_boropipeta1}', '{$_boropipeta2}', '{$_boropipeta3}', '{$_boropipeta4}', '{$_boropipeta5}', '{$_borovolcorrpipeta1}', '{$_borovolcorrpipeta2}', '{$_borovolcorrpipeta3}', '{$_borovolcorrpipeta4}', '{$_borovolcorrpipeta5}', '{$_boroincpip1}', '{$_boroincpip2}', '{$_boroincpip3}', '{$_boroincpip4}', '{$_boroincpip5}', '{$_borobalon1}', '{$_borobalon2}', '{$_borobalon3}', '{$_borobalon4}', '{$_borobalon5}', '{$_borodilinc1}', '{$_borodilinc2}', '{$_borodilinc3}', '{$_borodilinc4}', '{$_borodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027calcio VALUES('{$cs[0]['id']}', '{$_calciocon1}', '{$_calciocon2}', '{$_calciocon3}', '{$_calciocon4}', '{$_calciocon5}', '{$_calcioinc1}', '{$_calcioinc2}', '{$_calcioinc3}', '{$_calcioinc4}', '{$_calcioinc5}', '{$_calcioincrel1}', '{$_calcioincrel2}', '{$_calcioincrel3}', '{$_calcioincrel4}', '{$_calcioincrel5}', '{$_calcioA11}', '{$_calcioA12}', '{$_calcioA13}', '{$_calcioA14}', '{$_calcioA15}', '{$_calcioA21}', '{$_calcioA22}', '{$_calcioA23}', '{$_calcioA24}', '{$_calcioA25}', '{$_calcioA31}', '{$_calcioA32}', '{$_calcioA33}', '{$_calcioA34}', '{$_calcioA35}', '{$_calcioA41}', '{$_calcioA42}', '{$_calcioA43}', '{$_calcioA44}', '{$_calcioA45}', '{$_calcioC11}', '{$_calcioC12}', '{$_calcioC13}', '{$_calcioC14}', '{$_calcioC15}', '{$_calcioC21}', '{$_calcioC22}', '{$_calcioC23}', '{$_calcioC24}', '{$_calcioC25}', '{$_calcioC31}', '{$_calcioC32}', '{$_calcioC33}', '{$_calcioC34}', '{$_calcioC35}', '{$_calcioC41}', '{$_calcioC42}', '{$_calcioC43}', '{$_calcioC44}', '{$_calcioC45}', '{$_calciocp1}', '{$_calciocp2}', '{$_calciocp3}', '{$_calciocp4}', '{$_calciocp5}', '{$_calcioa1}', '{$_calcioa2}', '{$_calcioa3}', '{$_calcioa4}', '{$_calcioa5}', '{$_calcioa6}', '{$_calcioa7}', '{$_calcioa8}', '{$_calcioa9}', '{$_calcioa10}', '{$_calcioa11}', '{$_calcioa12}', '{$_calcioa13}', '{$_calcioa14}', '{$_calcioa15}', '{$_calcioa16}', '{$_calcioa17}', '{$_calcioa18}', '{$_calcioa19}', '{$_calcioa20}', '{$_calcioc1}', '{$_calcioc2}', '{$_calcioc3}', '{$_calcioc4}', '{$_calcioc5}', '{$_calcioc6}', '{$_calcioc7}', '{$_calcioc8}', '{$_calcioc9}', '{$_calcioc10}', '{$_calcioc11}', '{$_calcioc12}', '{$_calcioc13}', '{$_calcioc14}', '{$_calcioc15}', '{$_calcioc16}', '{$_calcioc17}', '{$_calcioc18}', '{$_calcioc19}', '{$_calcioc20}', '{$_calciomaxinc}', '{$_calciomaxincrel}', '{$_calcionummedxmuestra}', '{$_calcionummedxpuncurva}', '{$_calcionummedcurva}', '{$_calcioconmuestra1}', '{$_calcioconmuestra2}', '{$_calcioprompatcalibracion}', '{$_calciodesvestresidual}', '{$_calciodesvestcurvacali}', '{$_calcionp}', '{$_calciolote}', '{$_calciovence}', '{$_calciocc1}', '{$_calciocc2}', '{$_calcioest1}', '{$_calcioest2}', '{$_calcioccest1}', '{$_calcioccest2}', '{$_calciovalor}', '{$_calcioincexp}', '{$_calciok}', '{$_calcioincest}', '{$_calcioestandar1}', '{$_calcioestandar2}', '{$_calcioestandar3}', '{$_calcioestandar4}', '{$_calcioestandar5}', '{$_calciopipeta1}', '{$_calciopipeta2}', '{$_calciopipeta3}', '{$_calciopipeta4}', '{$_calciopipeta5}', '{$_calciovolcorrpipeta1}', '{$_calciovolcorrpipeta2}', '{$_calciovolcorrpipeta3}', '{$_calciovolcorrpipeta4}', '{$_calciovolcorrpipeta5}', '{$_calcioincpip1}', '{$_calcioincpip2}', '{$_calcioincpip3}', '{$_calcioincpip4}', '{$_calcioincpip5}', '{$_calciobalon1}', '{$_calciobalon2}', '{$_calciobalon3}', '{$_calciobalon4}', '{$_calciobalon5}', '{$_calciodilinc1}', '{$_calciodilinc2}', '{$_calciodilinc3}', '{$_calciodilinc4}', '{$_calciodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027cobre VALUES('{$cs[0]['id']}', '{$_cobrecon1}', '{$_cobrecon2}', '{$_cobrecon3}', '{$_cobrecon4}', '{$_cobrecon5}', '{$_cobreinc1}', '{$_cobreinc2}', '{$_cobreinc3}', '{$_cobreinc4}', '{$_cobreinc5}', '{$_cobreincrel1}', '{$_cobreincrel2}', '{$_cobreincrel3}', '{$_cobreincrel4}', '{$_cobreincrel5}', '{$_cobreA11}', '{$_cobreA12}', '{$_cobreA13}', '{$_cobreA14}', '{$_cobreA15}', '{$_cobreA21}', '{$_cobreA22}', '{$_cobreA23}', '{$_cobreA24}', '{$_cobreA25}', '{$_cobreA31}', '{$_cobreA32}', '{$_cobreA33}', '{$_cobreA34}', '{$_cobreA35}', '{$_cobreA41}', '{$_cobreA42}', '{$_cobreA43}', '{$_cobreA44}', '{$_cobreA45}', '{$_cobreC11}', '{$_cobreC12}', '{$_cobreC13}', '{$_cobreC14}', '{$_cobreC15}', '{$_cobreC21}', '{$_cobreC22}', '{$_cobreC23}', '{$_cobreC24}', '{$_cobreC25}', '{$_cobreC31}', '{$_cobreC32}', '{$_cobreC33}', '{$_cobreC34}', '{$_cobreC35}', '{$_cobreC41}', '{$_cobreC42}', '{$_cobreC43}', '{$_cobreC44}', '{$_cobreC45}', '{$_cobrecp1}', '{$_cobrecp2}', '{$_cobrecp3}', '{$_cobrecp4}', '{$_cobrecp5}', '{$_cobrea1}', '{$_cobrea2}', '{$_cobrea3}', '{$_cobrea4}', '{$_cobrea5}', '{$_cobrea6}', '{$_cobrea7}', '{$_cobrea8}', '{$_cobrea9}', '{$_cobrea10}', '{$_cobrea11}', '{$_cobrea12}', '{$_cobrea13}', '{$_cobrea14}', '{$_cobrea15}', '{$_cobrea16}', '{$_cobrea17}', '{$_cobrea18}', '{$_cobrea19}', '{$_cobrea20}', '{$_cobrec1}', '{$_cobrec2}', '{$_cobrec3}', '{$_cobrec4}', '{$_cobrec5}', '{$_cobrec6}', '{$_cobrec7}', '{$_cobrec8}', '{$_cobrec9}', '{$_cobrec10}', '{$_cobrec11}', '{$_cobrec12}', '{$_cobrec13}', '{$_cobrec14}', '{$_cobrec15}', '{$_cobrec16}', '{$_cobrec17}', '{$_cobrec18}', '{$_cobrec19}', '{$_cobrec20}', '{$_cobremaxinc}', '{$_cobremaxincrel}', '{$_cobrenummedxmuestra}', '{$_cobrenummedxpuncurva}', '{$_cobrenummedcurva}', '{$_cobreconmuestra1}', '{$_cobreconmuestra2}', '{$_cobreprompatcalibracion}', '{$_cobredesvestresidual}', '{$_cobredesvestcurvacali}', '{$_cobrenp}', '{$_cobrelote}', '{$_cobrevence}', '{$_cobrecc1}', '{$_cobrecc2}', '{$_cobreest1}', '{$_cobreest2}', '{$_cobreccest1}', '{$_cobreccest2}', '{$_cobrevalor}', '{$_cobreincexp}', '{$_cobrek}', '{$_cobreincest}', '{$_cobreestandar1}', '{$_cobreestandar2}', '{$_cobreestandar3}', '{$_cobreestandar4}', '{$_cobreestandar5}', '{$_cobrepipeta1}', '{$_cobrepipeta2}', '{$_cobrepipeta3}', '{$_cobrepipeta4}', '{$_cobrepipeta5}', '{$_cobrevolcorrpipeta1}', '{$_cobrevolcorrpipeta2}', '{$_cobrevolcorrpipeta3}', '{$_cobrevolcorrpipeta4}', '{$_cobrevolcorrpipeta5}', '{$_cobreincpip1}', '{$_cobreincpip2}', '{$_cobreincpip3}', '{$_cobreincpip4}', '{$_cobreincpip5}', '{$_cobrebalon1}', '{$_cobrebalon2}', '{$_cobrebalon3}', '{$_cobrebalon4}', '{$_cobrebalon5}', '{$_cobredilinc1}', '{$_cobredilinc2}', '{$_cobredilinc3}', '{$_cobredilinc4}', '{$_cobredilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027fosforo VALUES('{$cs[0]['id']}', '{$_fosforocon1}', '{$_fosforocon2}', '{$_fosforocon3}', '{$_fosforocon4}', '{$_fosforocon5}', '{$_fosforoinc1}', '{$_fosforoinc2}', '{$_fosforoinc3}', '{$_fosforoinc4}', '{$_fosforoinc5}', '{$_fosforoincrel1}', '{$_fosforoincrel2}', '{$_fosforoincrel3}', '{$_fosforoincrel4}', '{$_fosforoincrel5}', '{$_fosforoA11}', '{$_fosforoA12}', '{$_fosforoA13}', '{$_fosforoA14}', '{$_fosforoA15}', '{$_fosforoA21}', '{$_fosforoA22}', '{$_fosforoA23}', '{$_fosforoA24}', '{$_fosforoA25}', '{$_fosforoA31}', '{$_fosforoA32}', '{$_fosforoA33}', '{$_fosforoA34}', '{$_fosforoA35}', '{$_fosforoA41}', '{$_fosforoA42}', '{$_fosforoA43}', '{$_fosforoA44}', '{$_fosforoA45}', '{$_fosforoC11}', '{$_fosforoC12}', '{$_fosforoC13}', '{$_fosforoC14}', '{$_fosforoC15}', '{$_fosforoC21}', '{$_fosforoC22}', '{$_fosforoC23}', '{$_fosforoC24}', '{$_fosforoC25}', '{$_fosforoC31}', '{$_fosforoC32}', '{$_fosforoC33}', '{$_fosforoC34}', '{$_fosforoC35}', '{$_fosforoC41}', '{$_fosforoC42}', '{$_fosforoC43}', '{$_fosforoC44}', '{$_fosforoC45}', '{$_fosforocp1}', '{$_fosforocp2}', '{$_fosforocp3}', '{$_fosforocp4}', '{$_fosforocp5}', '{$_fosforoa1}', '{$_fosforoa2}', '{$_fosforoa3}', '{$_fosforoa4}', '{$_fosforoa5}', '{$_fosforoa6}', '{$_fosforoa7}', '{$_fosforoa8}', '{$_fosforoa9}', '{$_fosforoa10}', '{$_fosforoa11}', '{$_fosforoa12}', '{$_fosforoa13}', '{$_fosforoa14}', '{$_fosforoa15}', '{$_fosforoa16}', '{$_fosforoa17}', '{$_fosforoa18}', '{$_fosforoa19}', '{$_fosforoa20}', '{$_fosforoc1}', '{$_fosforoc2}', '{$_fosforoc3}', '{$_fosforoc4}', '{$_fosforoc5}', '{$_fosforoc6}', '{$_fosforoc7}', '{$_fosforoc8}', '{$_fosforoc9}', '{$_fosforoc10}', '{$_fosforoc11}', '{$_fosforoc12}', '{$_fosforoc13}', '{$_fosforoc14}', '{$_fosforoc15}', '{$_fosforoc16}', '{$_fosforoc17}', '{$_fosforoc18}', '{$_fosforoc19}', '{$_fosforoc20}', '{$_fosforomaxinc}', '{$_fosforomaxincrel}', '{$_fosforonummedxmuestra}', '{$_fosforonummedxpuncurva}', '{$_fosforonummedcurva}', '{$_fosforoconmuestra1}', '{$_fosforoconmuestra2}', '{$_fosforoprompatcalibracion}', '{$_fosforodesvestresidual}', '{$_fosforodesvestcurvacali}', '{$_fosforonp}', '{$_fosforolote}', '{$_fosforovence}', '{$_fosforocc1}', '{$_fosforocc2}', '{$_fosforoest1}', '{$_fosforoest2}', '{$_fosforoccest1}', '{$_fosforoccest2}', '{$_fosforovalor}', '{$_fosforoincexp}', '{$_fosforok}', '{$_fosforoincest}', '{$_fosforoestandar1}', '{$_fosforoestandar2}', '{$_fosforoestandar3}', '{$_fosforoestandar4}', '{$_fosforoestandar5}', '{$_fosforopipeta1}', '{$_fosforopipeta2}', '{$_fosforopipeta3}', '{$_fosforopipeta4}', '{$_fosforopipeta5}', '{$_fosforovolcorrpipeta1}', '{$_fosforovolcorrpipeta2}', '{$_fosforovolcorrpipeta3}', '{$_fosforovolcorrpipeta4}', '{$_fosforovolcorrpipeta5}', '{$_fosforoincpip1}', '{$_fosforoincpip2}', '{$_fosforoincpip3}', '{$_fosforoincpip4}', '{$_fosforoincpip5}', '{$_fosforobalon1}', '{$_fosforobalon2}', '{$_fosforobalon3}', '{$_fosforobalon4}', '{$_fosforobalon5}', '{$_fosforodilinc1}', '{$_fosforodilinc2}', '{$_fosforodilinc3}', '{$_fosforodilinc4}', '{$_fosforodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027hierro VALUES('{$cs[0]['id']}', '{$_hierrocon1}', '{$_hierrocon2}', '{$_hierrocon3}', '{$_hierrocon4}', '{$_hierrocon5}', '{$_hierroinc1}', '{$_hierroinc2}', '{$_hierroinc3}', '{$_hierroinc4}', '{$_hierroinc5}', '{$_hierroincrel1}', '{$_hierroincrel2}', '{$_hierroincrel3}', '{$_hierroincrel4}', '{$_hierroincrel5}', '{$_hierroA11}', '{$_hierroA12}', '{$_hierroA13}', '{$_hierroA14}', '{$_hierroA15}', '{$_hierroA21}', '{$_hierroA22}', '{$_hierroA23}', '{$_hierroA24}', '{$_hierroA25}', '{$_hierroA31}', '{$_hierroA32}', '{$_hierroA33}', '{$_hierroA34}', '{$_hierroA35}', '{$_hierroA41}', '{$_hierroA42}', '{$_hierroA43}', '{$_hierroA44}', '{$_hierroA45}', '{$_hierroC11}', '{$_hierroC12}', '{$_hierroC13}', '{$_hierroC14}', '{$_hierroC15}', '{$_hierroC21}', '{$_hierroC22}', '{$_hierroC23}', '{$_hierroC24}', '{$_hierroC25}', '{$_hierroC31}', '{$_hierroC32}', '{$_hierroC33}', '{$_hierroC34}', '{$_hierroC35}', '{$_hierroC41}', '{$_hierroC42}', '{$_hierroC43}', '{$_hierroC44}', '{$_hierroC45}', '{$_hierrocp1}', '{$_hierrocp2}', '{$_hierrocp3}', '{$_hierrocp4}', '{$_hierrocp5}', '{$_hierroa1}', '{$_hierroa2}', '{$_hierroa3}', '{$_hierroa4}', '{$_hierroa5}', '{$_hierroa6}', '{$_hierroa7}', '{$_hierroa8}', '{$_hierroa9}', '{$_hierroa10}', '{$_hierroa11}', '{$_hierroa12}', '{$_hierroa13}', '{$_hierroa14}', '{$_hierroa15}', '{$_hierroa16}', '{$_hierroa17}', '{$_hierroa18}', '{$_hierroa19}', '{$_hierroa20}', '{$_hierroc1}', '{$_hierroc2}', '{$_hierroc3}', '{$_hierroc4}', '{$_hierroc5}', '{$_hierroc6}', '{$_hierroc7}', '{$_hierroc8}', '{$_hierroc9}', '{$_hierroc10}', '{$_hierroc11}', '{$_hierroc12}', '{$_hierroc13}', '{$_hierroc14}', '{$_hierroc15}', '{$_hierroc16}', '{$_hierroc17}', '{$_hierroc18}', '{$_hierroc19}', '{$_hierroc20}', '{$_hierromaxinc}', '{$_hierromaxincrel}', '{$_hierronummedxmuestra}', '{$_hierronummedxpuncurva}', '{$_hierronummedcurva}', '{$_hierroconmuestra1}', '{$_hierroconmuestra2}', '{$_hierroprompatcalibracion}', '{$_hierrodesvestresidual}', '{$_hierrodesvestcurvacali}', '{$_hierronp}', '{$_hierrolote}', '{$_hierrovence}', '{$_hierrocc1}', '{$_hierrocc2}', '{$_hierroest1}', '{$_hierroest2}', '{$_hierroccest1}', '{$_hierroccest2}', '{$_hierrovalor}', '{$_hierroincexp}', '{$_hierrok}', '{$_hierroincest}', '{$_hierroestandar1}', '{$_hierroestandar2}', '{$_hierroestandar3}', '{$_hierroestandar4}', '{$_hierroestandar5}', '{$_hierropipeta1}', '{$_hierropipeta2}', '{$_hierropipeta3}', '{$_hierropipeta4}', '{$_hierropipeta5}', '{$_hierrovolcorrpipeta1}', '{$_hierrovolcorrpipeta2}', '{$_hierrovolcorrpipeta3}', '{$_hierrovolcorrpipeta4}', '{$_hierrovolcorrpipeta5}', '{$_hierroincpip1}', '{$_hierroincpip2}', '{$_hierroincpip3}', '{$_hierroincpip4}', '{$_hierroincpip5}', '{$_hierrobalon1}', '{$_hierrobalon2}', '{$_hierrobalon3}', '{$_hierrobalon4}', '{$_hierrobalon5}', '{$_hierrodilinc1}', '{$_hierrodilinc2}', '{$_hierrodilinc3}', '{$_hierrodilinc4}', '{$_hierrodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027magnesio VALUES('{$cs[0]['id']}', '{$_magnesiocon1}', '{$_magnesiocon2}', '{$_magnesiocon3}', '{$_magnesiocon4}', '{$_magnesiocon5}', '{$_magnesioinc1}', '{$_magnesioinc2}', '{$_magnesioinc3}', '{$_magnesioinc4}', '{$_magnesioinc5}', '{$_magnesioincrel1}', '{$_magnesioincrel2}', '{$_magnesioincrel3}', '{$_magnesioincrel4}', '{$_magnesioincrel5}', '{$_magnesioA11}', '{$_magnesioA12}', '{$_magnesioA13}', '{$_magnesioA14}', '{$_magnesioA15}', '{$_magnesioA21}', '{$_magnesioA22}', '{$_magnesioA23}', '{$_magnesioA24}', '{$_magnesioA25}', '{$_magnesioA31}', '{$_magnesioA32}', '{$_magnesioA33}', '{$_magnesioA34}', '{$_magnesioA35}', '{$_magnesioA41}', '{$_magnesioA42}', '{$_magnesioA43}', '{$_magnesioA44}', '{$_magnesioA45}', '{$_magnesioC11}', '{$_magnesioC12}', '{$_magnesioC13}', '{$_magnesioC14}', '{$_magnesioC15}', '{$_magnesioC21}', '{$_magnesioC22}', '{$_magnesioC23}', '{$_magnesioC24}', '{$_magnesioC25}', '{$_magnesioC31}', '{$_magnesioC32}', '{$_magnesioC33}', '{$_magnesioC34}', '{$_magnesioC35}', '{$_magnesioC41}', '{$_magnesioC42}', '{$_magnesioC43}', '{$_magnesioC44}', '{$_magnesioC45}', '{$_magnesiocp1}', '{$_magnesiocp2}', '{$_magnesiocp3}', '{$_magnesiocp4}', '{$_magnesiocp5}', '{$_magnesioa1}', '{$_magnesioa2}', '{$_magnesioa3}', '{$_magnesioa4}', '{$_magnesioa5}', '{$_magnesioa6}', '{$_magnesioa7}', '{$_magnesioa8}', '{$_magnesioa9}', '{$_magnesioa10}', '{$_magnesioa11}', '{$_magnesioa12}', '{$_magnesioa13}', '{$_magnesioa14}', '{$_magnesioa15}', '{$_magnesioa16}', '{$_magnesioa17}', '{$_magnesioa18}', '{$_magnesioa19}', '{$_magnesioa20}', '{$_magnesioc1}', '{$_magnesioc2}', '{$_magnesioc3}', '{$_magnesioc4}', '{$_magnesioc5}', '{$_magnesioc6}', '{$_magnesioc7}', '{$_magnesioc8}', '{$_magnesioc9}', '{$_magnesioc10}', '{$_magnesioc11}', '{$_magnesioc12}', '{$_magnesioc13}', '{$_magnesioc14}', '{$_magnesioc15}', '{$_magnesioc16}', '{$_magnesioc17}', '{$_magnesioc18}', '{$_magnesioc19}', '{$_magnesioc20}', '{$_magnesiomaxinc}', '{$_magnesiomaxincrel}', '{$_magnesionummedxmuestra}', '{$_magnesionummedxpuncurva}', '{$_magnesionummedcurva}', '{$_magnesioconmuestra1}', '{$_magnesioconmuestra2}', '{$_magnesioprompatcalibracion}', '{$_magnesiodesvestresidual}', '{$_magnesiodesvestcurvacali}', '{$_magnesionp}', '{$_magnesiolote}', '{$_magnesiovence}', '{$_magnesiocc1}', '{$_magnesiocc2}', '{$_magnesioest1}', '{$_magnesioest2}', '{$_magnesioccest1}', '{$_magnesioccest2}', '{$_magnesiovalor}', '{$_magnesioincexp}', '{$_magnesiok}', '{$_magnesioincest}', '{$_magnesioestandar1}', '{$_magnesioestandar2}', '{$_magnesioestandar3}', '{$_magnesioestandar4}', '{$_magnesioestandar5}', '{$_magnesiopipeta1}', '{$_magnesiopipeta2}', '{$_magnesiopipeta3}', '{$_magnesiopipeta4}', '{$_magnesiopipeta5}', '{$_magnesiovolcorrpipeta1}', '{$_magnesiovolcorrpipeta2}', '{$_magnesiovolcorrpipeta3}', '{$_magnesiovolcorrpipeta4}', '{$_magnesiovolcorrpipeta5}', '{$_magnesioincpip1}', '{$_magnesioincpip2}', '{$_magnesioincpip3}', '{$_magnesioincpip4}', '{$_magnesioincpip5}', '{$_magnesiobalon1}', '{$_magnesiobalon2}', '{$_magnesiobalon3}', '{$_magnesiobalon4}', '{$_magnesiobalon5}', '{$_magnesiodilinc1}', '{$_magnesiodilinc2}', '{$_magnesiodilinc3}', '{$_magnesiodilinc4}', '{$_magnesiodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027manganeso VALUES('{$cs[0]['id']}', '{$_manganesocon1}', '{$_manganesocon2}', '{$_manganesocon3}', '{$_manganesocon4}', '{$_manganesocon5}', '{$_manganesoinc1}', '{$_manganesoinc2}', '{$_manganesoinc3}', '{$_manganesoinc4}', '{$_manganesoinc5}', '{$_manganesoincrel1}', '{$_manganesoincrel2}', '{$_manganesoincrel3}', '{$_manganesoincrel4}', '{$_manganesoincrel5}', '{$_manganesoA11}', '{$_manganesoA12}', '{$_manganesoA13}', '{$_manganesoA14}', '{$_manganesoA15}', '{$_manganesoA21}', '{$_manganesoA22}', '{$_manganesoA23}', '{$_manganesoA24}', '{$_manganesoA25}', '{$_manganesoA31}', '{$_manganesoA32}', '{$_manganesoA33}', '{$_manganesoA34}', '{$_manganesoA35}', '{$_manganesoA41}', '{$_manganesoA42}', '{$_manganesoA43}', '{$_manganesoA44}', '{$_manganesoA45}', '{$_manganesoC11}', '{$_manganesoC12}', '{$_manganesoC13}', '{$_manganesoC14}', '{$_manganesoC15}', '{$_manganesoC21}', '{$_manganesoC22}', '{$_manganesoC23}', '{$_manganesoC24}', '{$_manganesoC25}', '{$_manganesoC31}', '{$_manganesoC32}', '{$_manganesoC33}', '{$_manganesoC34}', '{$_manganesoC35}', '{$_manganesoC41}', '{$_manganesoC42}', '{$_manganesoC43}', '{$_manganesoC44}', '{$_manganesoC45}', '{$_manganesocp1}', '{$_manganesocp2}', '{$_manganesocp3}', '{$_manganesocp4}', '{$_manganesocp5}', '{$_manganesoa1}', '{$_manganesoa2}', '{$_manganesoa3}', '{$_manganesoa4}', '{$_manganesoa5}', '{$_manganesoa6}', '{$_manganesoa7}', '{$_manganesoa8}', '{$_manganesoa9}', '{$_manganesoa10}', '{$_manganesoa11}', '{$_manganesoa12}', '{$_manganesoa13}', '{$_manganesoa14}', '{$_manganesoa15}', '{$_manganesoa16}', '{$_manganesoa17}', '{$_manganesoa18}', '{$_manganesoa19}', '{$_manganesoa20}', '{$_manganesoc1}', '{$_manganesoc2}', '{$_manganesoc3}', '{$_manganesoc4}', '{$_manganesoc5}', '{$_manganesoc6}', '{$_manganesoc7}', '{$_manganesoc8}', '{$_manganesoc9}', '{$_manganesoc10}', '{$_manganesoc11}', '{$_manganesoc12}', '{$_manganesoc13}', '{$_manganesoc14}', '{$_manganesoc15}', '{$_manganesoc16}', '{$_manganesoc17}', '{$_manganesoc18}', '{$_manganesoc19}', '{$_manganesoc20}', '{$_manganesomaxinc}', '{$_manganesomaxincrel}', '{$_manganesonummedxmuestra}', '{$_manganesonummedxpuncurva}', '{$_manganesonummedcurva}', '{$_manganesoconmuestra1}', '{$_manganesoconmuestra2}', '{$_manganesoprompatcalibracion}', '{$_manganesodesvestresidual}', '{$_manganesodesvestcurvacali}', '{$_manganesonp}', '{$_manganesolote}', '{$_manganesovence}', '{$_manganesocc1}', '{$_manganesocc2}', '{$_manganesoest1}', '{$_manganesoest2}', '{$_manganesoccest1}', '{$_manganesoccest2}', '{$_manganesovalor}', '{$_manganesoincexp}', '{$_manganesok}', '{$_manganesoincest}', '{$_manganesoestandar1}', '{$_manganesoestandar2}', '{$_manganesoestandar3}', '{$_manganesoestandar4}', '{$_manganesoestandar5}', '{$_manganesopipeta1}', '{$_manganesopipeta2}', '{$_manganesopipeta3}', '{$_manganesopipeta4}', '{$_manganesopipeta5}', '{$_manganesovolcorrpipeta1}', '{$_manganesovolcorrpipeta2}', '{$_manganesovolcorrpipeta3}', '{$_manganesovolcorrpipeta4}', '{$_manganesovolcorrpipeta5}', '{$_manganesoincpip1}', '{$_manganesoincpip2}', '{$_manganesoincpip3}', '{$_manganesoincpip4}', '{$_manganesoincpip5}', '{$_manganesobalon1}', '{$_manganesobalon2}', '{$_manganesobalon3}', '{$_manganesobalon4}', '{$_manganesobalon5}', '{$_manganesodilinc1}', '{$_manganesodilinc2}', '{$_manganesodilinc3}', '{$_manganesodilinc4}', '{$_manganesodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027potasio VALUES('{$cs[0]['id']}', '{$_potasiocon1}', '{$_potasiocon2}', '{$_potasiocon3}', '{$_potasiocon4}', '{$_potasiocon5}', '{$_potasioinc1}', '{$_potasioinc2}', '{$_potasioinc3}', '{$_potasioinc4}', '{$_potasioinc5}', '{$_potasioincrel1}', '{$_potasioincrel2}', '{$_potasioincrel3}', '{$_potasioincrel4}', '{$_potasioincrel5}', '{$_potasioA11}', '{$_potasioA12}', '{$_potasioA13}', '{$_potasioA14}', '{$_potasioA15}', '{$_potasioA21}', '{$_potasioA22}', '{$_potasioA23}', '{$_potasioA24}', '{$_potasioA25}', '{$_potasioA31}', '{$_potasioA32}', '{$_potasioA33}', '{$_potasioA34}', '{$_potasioA35}', '{$_potasioA41}', '{$_potasioA42}', '{$_potasioA43}', '{$_potasioA44}', '{$_potasioA45}', '{$_potasioC11}', '{$_potasioC12}', '{$_potasioC13}', '{$_potasioC14}', '{$_potasioC15}', '{$_potasioC21}', '{$_potasioC22}', '{$_potasioC23}', '{$_potasioC24}', '{$_potasioC25}', '{$_potasioC31}', '{$_potasioC32}', '{$_potasioC33}', '{$_potasioC34}', '{$_potasioC35}', '{$_potasioC41}', '{$_potasioC42}', '{$_potasioC43}', '{$_potasioC44}', '{$_potasioC45}', '{$_potasiocp1}', '{$_potasiocp2}', '{$_potasiocp3}', '{$_potasiocp4}', '{$_potasiocp5}', '{$_potasioa1}', '{$_potasioa2}', '{$_potasioa3}', '{$_potasioa4}', '{$_potasioa5}', '{$_potasioa6}', '{$_potasioa7}', '{$_potasioa8}', '{$_potasioa9}', '{$_potasioa10}', '{$_potasioa11}', '{$_potasioa12}', '{$_potasioa13}', '{$_potasioa14}', '{$_potasioa15}', '{$_potasioa16}', '{$_potasioa17}', '{$_potasioa18}', '{$_potasioa19}', '{$_potasioa20}', '{$_potasioc1}', '{$_potasioc2}', '{$_potasioc3}', '{$_potasioc4}', '{$_potasioc5}', '{$_potasioc6}', '{$_potasioc7}', '{$_potasioc8}', '{$_potasioc9}', '{$_potasioc10}', '{$_potasioc11}', '{$_potasioc12}', '{$_potasioc13}', '{$_potasioc14}', '{$_potasioc15}', '{$_potasioc16}', '{$_potasioc17}', '{$_potasioc18}', '{$_potasioc19}', '{$_potasioc20}', '{$_potasiomaxinc}', '{$_potasiomaxincrel}', '{$_potasionummedxmuestra}', '{$_potasionummedxpuncurva}', '{$_potasionummedcurva}', '{$_potasioconmuestra1}', '{$_potasioconmuestra2}', '{$_potasioprompatcalibracion}', '{$_potasiodesvestresidual}', '{$_potasiodesvestcurvacali}', '{$_potasionp}', '{$_potasiolote}', '{$_potasiovence}', '{$_potasiocc1}', '{$_potasiocc2}', '{$_potasioest1}', '{$_potasioest2}', '{$_potasioccest1}', '{$_potasioccest2}', '{$_potasiovalor}', '{$_potasioincexp}', '{$_potasiok}', '{$_potasioincest}', '{$_potasioestandar1}', '{$_potasioestandar2}', '{$_potasioestandar3}', '{$_potasioestandar4}', '{$_potasioestandar5}', '{$_potasiopipeta1}', '{$_potasiopipeta2}', '{$_potasiopipeta3}', '{$_potasiopipeta4}', '{$_potasiopipeta5}', '{$_potasiovolcorrpipeta1}', '{$_potasiovolcorrpipeta2}', '{$_potasiovolcorrpipeta3}', '{$_potasiovolcorrpipeta4}', '{$_potasiovolcorrpipeta5}', '{$_potasioincpip1}', '{$_potasioincpip2}', '{$_potasioincpip3}', '{$_potasioincpip4}', '{$_potasioincpip5}', '{$_potasiobalon1}', '{$_potasiobalon2}', '{$_potasiobalon3}', '{$_potasiobalon4}', '{$_potasiobalon5}', '{$_potasiodilinc1}', '{$_potasiodilinc2}', '{$_potasiodilinc3}', '{$_potasiodilinc4}', '{$_potasiodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027zinc VALUES('{$cs[0]['id']}', '{$_zinccon1}', '{$_zinccon2}', '{$_zinccon3}', '{$_zinccon4}', '{$_zinccon5}', '{$_zincinc1}', '{$_zincinc2}', '{$_zincinc3}', '{$_zincinc4}', '{$_zincinc5}', '{$_zincincrel1}', '{$_zincincrel2}', '{$_zincincrel3}', '{$_zincincrel4}', '{$_zincincrel5}', '{$_zincA11}', '{$_zincA12}', '{$_zincA13}', '{$_zincA14}', '{$_zincA15}', '{$_zincA21}', '{$_zincA22}', '{$_zincA23}', '{$_zincA24}', '{$_zincA25}', '{$_zincA31}', '{$_zincA32}', '{$_zincA33}', '{$_zincA34}', '{$_zincA35}', '{$_zincA41}', '{$_zincA42}', '{$_zincA43}', '{$_zincA44}', '{$_zincA45}', '{$_zincC11}', '{$_zincC12}', '{$_zincC13}', '{$_zincC14}', '{$_zincC15}', '{$_zincC21}', '{$_zincC22}', '{$_zincC23}', '{$_zincC24}', '{$_zincC25}', '{$_zincC31}', '{$_zincC32}', '{$_zincC33}', '{$_zincC34}', '{$_zincC35}', '{$_zincC41}', '{$_zincC42}', '{$_zincC43}', '{$_zincC44}', '{$_zincC45}', '{$_zinccp1}', '{$_zinccp2}', '{$_zinccp3}', '{$_zinccp4}', '{$_zinccp5}', '{$_zinca1}', '{$_zinca2}', '{$_zinca3}', '{$_zinca4}', '{$_zinca5}', '{$_zinca6}', '{$_zinca7}', '{$_zinca8}', '{$_zinca9}', '{$_zinca10}', '{$_zinca11}', '{$_zinca12}', '{$_zinca13}', '{$_zinca14}', '{$_zinca15}', '{$_zinca16}', '{$_zinca17}', '{$_zinca18}', '{$_zinca19}', '{$_zinca20}', '{$_zincc1}', '{$_zincc2}', '{$_zincc3}', '{$_zincc4}', '{$_zincc5}', '{$_zincc6}', '{$_zincc7}', '{$_zincc8}', '{$_zincc9}', '{$_zincc10}', '{$_zincc11}', '{$_zincc12}', '{$_zincc13}', '{$_zincc14}', '{$_zincc15}', '{$_zincc16}', '{$_zincc17}', '{$_zincc18}', '{$_zincc19}', '{$_zincc20}', '{$_zincmaxinc}', '{$_zincmaxincrel}', '{$_zincnummedxmuestra}', '{$_zincnummedxpuncurva}', '{$_zincnummedcurva}', '{$_zincconmuestra1}', '{$_zincconmuestra2}', '{$_zincprompatcalibracion}', '{$_zincdesvestresidual}', '{$_zincdesvestcurvacali}', '{$_zincnp}', '{$_zinclote}', '{$_zincvence}', '{$_zinccc1}', '{$_zinccc2}', '{$_zincest1}', '{$_zincest2}', '{$_zincccest1}', '{$_zincccest2}', '{$_zincvalor}', '{$_zincincexp}', '{$_zinck}', '{$_zincincest}', '{$_zincestandar1}', '{$_zincestandar2}', '{$_zincestandar3}', '{$_zincestandar4}', '{$_zincestandar5}', '{$_zincpipeta1}', '{$_zincpipeta2}', '{$_zincpipeta3}', '{$_zincpipeta4}', '{$_zincpipeta5}', '{$_zincvolcorrpipeta1}', '{$_zincvolcorrpipeta2}', '{$_zincvolcorrpipeta3}', '{$_zincvolcorrpipeta4}', '{$_zincvolcorrpipeta5}', '{$_zincincpip1}', '{$_zincincpip2}', '{$_zincincpip3}', '{$_zincincpip4}', '{$_zincincpip5}', '{$_zincbalon1}', '{$_zincbalon2}', '{$_zincbalon3}', '{$_zincbalon4}', '{$_zincbalon5}', '{$_zincdilinc1}', '{$_zincdilinc2}', '{$_zincdilinc3}', '{$_zincdilinc4}', '{$_zincdilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027nitrogeno VALUES('{$cs[0]['id']}', '{$_nitrogenocon1}', '{$_nitrogenocon2}', '{$_nitrogenocon3}', '{$_nitrogenocon4}', '{$_nitrogenocon5}', '{$_nitrogenoinc1}', '{$_nitrogenoinc2}', '{$_nitrogenoinc3}', '{$_nitrogenoinc4}', '{$_nitrogenoinc5}', '{$_nitrogenoincrel1}', '{$_nitrogenoincrel2}', '{$_nitrogenoincrel3}', '{$_nitrogenoincrel4}', '{$_nitrogenoincrel5}', '{$_nitrogenoA11}', '{$_nitrogenoA12}', '{$_nitrogenoA13}', '{$_nitrogenoA14}', '{$_nitrogenoA15}', '{$_nitrogenoA21}', '{$_nitrogenoA22}', '{$_nitrogenoA23}', '{$_nitrogenoA24}', '{$_nitrogenoA25}', '{$_nitrogenoA31}', '{$_nitrogenoA32}', '{$_nitrogenoA33}', '{$_nitrogenoA34}', '{$_nitrogenoA35}', '{$_nitrogenoA41}', '{$_nitrogenoA42}', '{$_nitrogenoA43}', '{$_nitrogenoA44}', '{$_nitrogenoA45}', '{$_nitrogenoC11}', '{$_nitrogenoC12}', '{$_nitrogenoC13}', '{$_nitrogenoC14}', '{$_nitrogenoC15}', '{$_nitrogenoC21}', '{$_nitrogenoC22}', '{$_nitrogenoC23}', '{$_nitrogenoC24}', '{$_nitrogenoC25}', '{$_nitrogenoC31}', '{$_nitrogenoC32}', '{$_nitrogenoC33}', '{$_nitrogenoC34}', '{$_nitrogenoC35}', '{$_nitrogenoC41}', '{$_nitrogenoC42}', '{$_nitrogenoC43}', '{$_nitrogenoC44}', '{$_nitrogenoC45}', '{$_nitrogenocp1}', '{$_nitrogenocp2}', '{$_nitrogenocp3}', '{$_nitrogenocp4}', '{$_nitrogenocp5}', '{$_nitrogenoa1}', '{$_nitrogenoa2}', '{$_nitrogenoa3}', '{$_nitrogenoa4}', '{$_nitrogenoa5}', '{$_nitrogenoa6}', '{$_nitrogenoa7}', '{$_nitrogenoa8}', '{$_nitrogenoa9}', '{$_nitrogenoa10}', '{$_nitrogenoa11}', '{$_nitrogenoa12}', '{$_nitrogenoa13}', '{$_nitrogenoa14}', '{$_nitrogenoa15}', '{$_nitrogenoa16}', '{$_nitrogenoa17}', '{$_nitrogenoa18}', '{$_nitrogenoa19}', '{$_nitrogenoa20}', '{$_nitrogenoc1}', '{$_nitrogenoc2}', '{$_nitrogenoc3}', '{$_nitrogenoc4}', '{$_nitrogenoc5}', '{$_nitrogenoc6}', '{$_nitrogenoc7}', '{$_nitrogenoc8}', '{$_nitrogenoc9}', '{$_nitrogenoc10}', '{$_nitrogenoc11}', '{$_nitrogenoc12}', '{$_nitrogenoc13}', '{$_nitrogenoc14}', '{$_nitrogenoc15}', '{$_nitrogenoc16}', '{$_nitrogenoc17}', '{$_nitrogenoc18}', '{$_nitrogenoc19}', '{$_nitrogenoc20}', '{$_nitrogenomaxinc}', '{$_nitrogenomaxincrel}', '{$_nitrogenonummedxmuestra}', '{$_nitrogenonummedxpuncurva}', '{$_nitrogenonummedcurva}', '{$_nitrogenoconmuestra1}', '{$_nitrogenoconmuestra2}', '{$_nitrogenoprompatcalibracion}', '{$_nitrogenodesvestresidual}', '{$_nitrogenodesvestcurvacali}', '{$_nitrogenonp}', '{$_nitrogenolote}', '{$_nitrogenovence}', '{$_nitrogenocc1}', '{$_nitrogenocc2}', '{$_nitrogenoest1}', '{$_nitrogenoest2}', '{$_nitrogenoccest1}', '{$_nitrogenoccest2}', '{$_nitrogenovalor}', '{$_nitrogenoincexp}', '{$_nitrogenok}', '{$_nitrogenoincest}', '{$_nitrogenoestandar1}', '{$_nitrogenoestandar2}', '{$_nitrogenoestandar3}', '{$_nitrogenoestandar4}', '{$_nitrogenoestandar5}', '{$_nitrogenopipeta1}', '{$_nitrogenopipeta2}', '{$_nitrogenopipeta3}', '{$_nitrogenopipeta4}', '{$_nitrogenopipeta5}', '{$_nitrogenovolcorrpipeta1}', '{$_nitrogenovolcorrpipeta2}', '{$_nitrogenovolcorrpipeta3}', '{$_nitrogenovolcorrpipeta4}', '{$_nitrogenovolcorrpipeta5}', '{$_nitrogenoincpip1}', '{$_nitrogenoincpip2}', '{$_nitrogenoincpip3}', '{$_nitrogenoincpip4}', '{$_nitrogenoincpip5}', '{$_nitrogenobalon1}', '{$_nitrogenobalon2}', '{$_nitrogenobalon3}', '{$_nitrogenobalon4}', '{$_nitrogenobalon5}', '{$_nitrogenodilinc1}', '{$_nitrogenodilinc2}', '{$_nitrogenodilinc3}', '{$_nitrogenodilinc4}', '{$_nitrogenodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027azufre VALUES('{$cs[0]['id']}', '{$_azufrecon1}', '{$_azufrecon2}', '{$_azufrecon3}', '{$_azufrecon4}', '{$_azufrecon5}', '{$_azufreinc1}', '{$_azufreinc2}', '{$_azufreinc3}', '{$_azufreinc4}', '{$_azufreinc5}', '{$_azufreincrel1}', '{$_azufreincrel2}', '{$_azufreincrel3}', '{$_azufreincrel4}', '{$_azufreincrel5}', '{$_azufreA11}', '{$_azufreA12}', '{$_azufreA13}', '{$_azufreA14}', '{$_azufreA15}', '{$_azufreA21}', '{$_azufreA22}', '{$_azufreA23}', '{$_azufreA24}', '{$_azufreA25}', '{$_azufreA31}', '{$_azufreA32}', '{$_azufreA33}', '{$_azufreA34}', '{$_azufreA35}', '{$_azufreA41}', '{$_azufreA42}', '{$_azufreA43}', '{$_azufreA44}', '{$_azufreA45}', '{$_azufreC11}', '{$_azufreC12}', '{$_azufreC13}', '{$_azufreC14}', '{$_azufreC15}', '{$_azufreC21}', '{$_azufreC22}', '{$_azufreC23}', '{$_azufreC24}', '{$_azufreC25}', '{$_azufreC31}', '{$_azufreC32}', '{$_azufreC33}', '{$_azufreC34}', '{$_azufreC35}', '{$_azufreC41}', '{$_azufreC42}', '{$_azufreC43}', '{$_azufreC44}', '{$_azufreC45}', '{$_azufrecp1}', '{$_azufrecp2}', '{$_azufrecp3}', '{$_azufrecp4}', '{$_azufrecp5}', '{$_azufrea1}', '{$_azufrea2}', '{$_azufrea3}', '{$_azufrea4}', '{$_azufrea5}', '{$_azufrea6}', '{$_azufrea7}', '{$_azufrea8}', '{$_azufrea9}', '{$_azufrea10}', '{$_azufrea11}', '{$_azufrea12}', '{$_azufrea13}', '{$_azufrea14}', '{$_azufrea15}', '{$_azufrea16}', '{$_azufrea17}', '{$_azufrea18}', '{$_azufrea19}', '{$_azufrea20}', '{$_azufrec1}', '{$_azufrec2}', '{$_azufrec3}', '{$_azufrec4}', '{$_azufrec5}', '{$_azufrec6}', '{$_azufrec7}', '{$_azufrec8}', '{$_azufrec9}', '{$_azufrec10}', '{$_azufrec11}', '{$_azufrec12}', '{$_azufrec13}', '{$_azufrec14}', '{$_azufrec15}', '{$_azufrec16}', '{$_azufrec17}', '{$_azufrec18}', '{$_azufrec19}', '{$_azufrec20}', '{$_azufremaxinc}', '{$_azufremaxincrel}', '{$_azufrenummedxmuestra}', '{$_azufrenummedxpuncurva}', '{$_azufrenummedcurva}', '{$_azufreconmuestra1}', '{$_azufreconmuestra2}', '{$_azufreprompatcalibracion}', '{$_azufredesvestresidual}', '{$_azufredesvestcurvacali}', '{$_azufrenp}', '{$_azufrelote}', '{$_azufrevence}', '{$_azufrecc1}', '{$_azufrecc2}', '{$_azufreest1}', '{$_azufreest2}', '{$_azufreccest1}', '{$_azufreccest2}', '{$_azufrevalor}', '{$_azufreincexp}', '{$_azufrek}', '{$_azufreincest}', '{$_azufreestandar1}', '{$_azufreestandar2}', '{$_azufreestandar3}', '{$_azufreestandar4}', '{$_azufreestandar5}', '{$_azufrepipeta1}', '{$_azufrepipeta2}', '{$_azufrepipeta3}', '{$_azufrepipeta4}', '{$_azufrepipeta5}', '{$_azufrevolcorrpipeta1}', '{$_azufrevolcorrpipeta2}', '{$_azufrevolcorrpipeta3}', '{$_azufrevolcorrpipeta4}', '{$_azufrevolcorrpipeta5}', '{$_azufreincpip1}', '{$_azufreincpip2}', '{$_azufreincpip3}', '{$_azufreincpip4}', '{$_azufreincpip5}', '{$_azufrebalon1}', '{$_azufrebalon2}', '{$_azufrebalon3}', '{$_azufrebalon4}', '{$_azufrebalon5}', '{$_azufredilinc1}', '{$_azufredilinc2}', '{$_azufredilinc3}', '{$_azufredilinc4}', '{$_azufredilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027arsenico VALUES('{$cs[0]['id']}', '{$_arsenicocon1}', '{$_arsenicocon2}', '{$_arsenicocon3}', '{$_arsenicocon4}', '{$_arsenicocon5}', '{$_arsenicoinc1}', '{$_arsenicoinc2}', '{$_arsenicoinc3}', '{$_arsenicoinc4}', '{$_arsenicoinc5}', '{$_arsenicoincrel1}', '{$_arsenicoincrel2}', '{$_arsenicoincrel3}', '{$_arsenicoincrel4}', '{$_arsenicoincrel5}', '{$_arsenicoA11}', '{$_arsenicoA12}', '{$_arsenicoA13}', '{$_arsenicoA14}', '{$_arsenicoA15}', '{$_arsenicoA21}', '{$_arsenicoA22}', '{$_arsenicoA23}', '{$_arsenicoA24}', '{$_arsenicoA25}', '{$_arsenicoA31}', '{$_arsenicoA32}', '{$_arsenicoA33}', '{$_arsenicoA34}', '{$_arsenicoA35}', '{$_arsenicoA41}', '{$_arsenicoA42}', '{$_arsenicoA43}', '{$_arsenicoA44}', '{$_arsenicoA45}', '{$_arsenicoC11}', '{$_arsenicoC12}', '{$_arsenicoC13}', '{$_arsenicoC14}', '{$_arsenicoC15}', '{$_arsenicoC21}', '{$_arsenicoC22}', '{$_arsenicoC23}', '{$_arsenicoC24}', '{$_arsenicoC25}', '{$_arsenicoC31}', '{$_arsenicoC32}', '{$_arsenicoC33}', '{$_arsenicoC34}', '{$_arsenicoC35}', '{$_arsenicoC41}', '{$_arsenicoC42}', '{$_arsenicoC43}', '{$_arsenicoC44}', '{$_arsenicoC45}', '{$_arsenicocp1}', '{$_arsenicocp2}', '{$_arsenicocp3}', '{$_arsenicocp4}', '{$_arsenicocp5}', '{$_arsenicoa1}', '{$_arsenicoa2}', '{$_arsenicoa3}', '{$_arsenicoa4}', '{$_arsenicoa5}', '{$_arsenicoa6}', '{$_arsenicoa7}', '{$_arsenicoa8}', '{$_arsenicoa9}', '{$_arsenicoa10}', '{$_arsenicoa11}', '{$_arsenicoa12}', '{$_arsenicoa13}', '{$_arsenicoa14}', '{$_arsenicoa15}', '{$_arsenicoa16}', '{$_arsenicoa17}', '{$_arsenicoa18}', '{$_arsenicoa19}', '{$_arsenicoa20}', '{$_arsenicoc1}', '{$_arsenicoc2}', '{$_arsenicoc3}', '{$_arsenicoc4}', '{$_arsenicoc5}', '{$_arsenicoc6}', '{$_arsenicoc7}', '{$_arsenicoc8}', '{$_arsenicoc9}', '{$_arsenicoc10}', '{$_arsenicoc11}', '{$_arsenicoc12}', '{$_arsenicoc13}', '{$_arsenicoc14}', '{$_arsenicoc15}', '{$_arsenicoc16}', '{$_arsenicoc17}', '{$_arsenicoc18}', '{$_arsenicoc19}', '{$_arsenicoc20}', '{$_arsenicomaxinc}', '{$_arsenicomaxincrel}', '{$_arseniconummedxmuestra}', '{$_arseniconummedxpuncurva}', '{$_arseniconummedcurva}', '{$_arsenicoconmuestra1}', '{$_arsenicoconmuestra2}', '{$_arsenicoprompatcalibracion}', '{$_arsenicodesvestresidual}', '{$_arsenicodesvestcurvacali}', '{$_arseniconp}', '{$_arsenicolote}', '{$_arsenicovence}', '{$_arsenicocc1}', '{$_arsenicocc2}', '{$_arsenicoest1}', '{$_arsenicoest2}', '{$_arsenicoccest1}', '{$_arsenicoccest2}', '{$_arsenicovalor}', '{$_arsenicoincexp}', '{$_arsenicok}', '{$_arsenicoincest}', '{$_arsenicoestandar1}', '{$_arsenicoestandar2}', '{$_arsenicoestandar3}', '{$_arsenicoestandar4}', '{$_arsenicoestandar5}', '{$_arsenicopipeta1}', '{$_arsenicopipeta2}', '{$_arsenicopipeta3}', '{$_arsenicopipeta4}', '{$_arsenicopipeta5}', '{$_arsenicovolcorrpipeta1}', '{$_arsenicovolcorrpipeta2}', '{$_arsenicovolcorrpipeta3}', '{$_arsenicovolcorrpipeta4}', '{$_arsenicovolcorrpipeta5}', '{$_arsenicoincpip1}', '{$_arsenicoincpip2}', '{$_arsenicoincpip3}', '{$_arsenicoincpip4}', '{$_arsenicoincpip5}', '{$_arsenicobalon1}', '{$_arsenicobalon2}', '{$_arsenicobalon3}', '{$_arsenicobalon4}', '{$_arsenicobalon5}', '{$_arsenicodilinc1}', '{$_arsenicodilinc2}', '{$_arsenicodilinc3}', '{$_arsenicodilinc4}', '{$_arsenicodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027cadmio VALUES('{$cs[0]['id']}', '{$_cadmiocon1}', '{$_cadmiocon2}', '{$_cadmiocon3}', '{$_cadmiocon4}', '{$_cadmiocon5}', '{$_cadmioinc1}', '{$_cadmioinc2}', '{$_cadmioinc3}', '{$_cadmioinc4}', '{$_cadmioinc5}', '{$_cadmioincrel1}', '{$_cadmioincrel2}', '{$_cadmioincrel3}', '{$_cadmioincrel4}', '{$_cadmioincrel5}', '{$_cadmioA11}', '{$_cadmioA12}', '{$_cadmioA13}', '{$_cadmioA14}', '{$_cadmioA15}', '{$_cadmioA21}', '{$_cadmioA22}', '{$_cadmioA23}', '{$_cadmioA24}', '{$_cadmioA25}', '{$_cadmioA31}', '{$_cadmioA32}', '{$_cadmioA33}', '{$_cadmioA34}', '{$_cadmioA35}', '{$_cadmioA41}', '{$_cadmioA42}', '{$_cadmioA43}', '{$_cadmioA44}', '{$_cadmioA45}', '{$_cadmioC11}', '{$_cadmioC12}', '{$_cadmioC13}', '{$_cadmioC14}', '{$_cadmioC15}', '{$_cadmioC21}', '{$_cadmioC22}', '{$_cadmioC23}', '{$_cadmioC24}', '{$_cadmioC25}', '{$_cadmioC31}', '{$_cadmioC32}', '{$_cadmioC33}', '{$_cadmioC34}', '{$_cadmioC35}', '{$_cadmioC41}', '{$_cadmioC42}', '{$_cadmioC43}', '{$_cadmioC44}', '{$_cadmioC45}', '{$_cadmiocp1}', '{$_cadmiocp2}', '{$_cadmiocp3}', '{$_cadmiocp4}', '{$_cadmiocp5}', '{$_cadmioa1}', '{$_cadmioa2}', '{$_cadmioa3}', '{$_cadmioa4}', '{$_cadmioa5}', '{$_cadmioa6}', '{$_cadmioa7}', '{$_cadmioa8}', '{$_cadmioa9}', '{$_cadmioa10}', '{$_cadmioa11}', '{$_cadmioa12}', '{$_cadmioa13}', '{$_cadmioa14}', '{$_cadmioa15}', '{$_cadmioa16}', '{$_cadmioa17}', '{$_cadmioa18}', '{$_cadmioa19}', '{$_cadmioa20}', '{$_cadmioc1}', '{$_cadmioc2}', '{$_cadmioc3}', '{$_cadmioc4}', '{$_cadmioc5}', '{$_cadmioc6}', '{$_cadmioc7}', '{$_cadmioc8}', '{$_cadmioc9}', '{$_cadmioc10}', '{$_cadmioc11}', '{$_cadmioc12}', '{$_cadmioc13}', '{$_cadmioc14}', '{$_cadmioc15}', '{$_cadmioc16}', '{$_cadmioc17}', '{$_cadmioc18}', '{$_cadmioc19}', '{$_cadmioc20}', '{$_cadmiomaxinc}', '{$_cadmiomaxincrel}', '{$_cadmionummedxmuestra}', '{$_cadmionummedxpuncurva}', '{$_cadmionummedcurva}', '{$_cadmioconmuestra1}', '{$_cadmioconmuestra2}', '{$_cadmioprompatcalibracion}', '{$_cadmiodesvestresidual}', '{$_cadmiodesvestcurvacali}', '{$_cadmionp}', '{$_cadmiolote}', '{$_cadmiovence}', '{$_cadmiocc1}', '{$_cadmiocc2}', '{$_cadmioest1}', '{$_cadmioest2}', '{$_cadmioccest1}', '{$_cadmioccest2}', '{$_cadmiovalor}', '{$_cadmioincexp}', '{$_cadmiok}', '{$_cadmioincest}', '{$_cadmioestandar1}', '{$_cadmioestandar2}', '{$_cadmioestandar3}', '{$_cadmioestandar4}', '{$_cadmioestandar5}', '{$_cadmiopipeta1}', '{$_cadmiopipeta2}', '{$_cadmiopipeta3}', '{$_cadmiopipeta4}', '{$_cadmiopipeta5}', '{$_cadmiovolcorrpipeta1}', '{$_cadmiovolcorrpipeta2}', '{$_cadmiovolcorrpipeta3}', '{$_cadmiovolcorrpipeta4}', '{$_cadmiovolcorrpipeta5}', '{$_cadmioincpip1}', '{$_cadmioincpip2}', '{$_cadmioincpip3}', '{$_cadmioincpip4}', '{$_cadmioincpip5}', '{$_cadmiobalon1}', '{$_cadmiobalon2}', '{$_cadmiobalon3}', '{$_cadmiobalon4}', '{$_cadmiobalon5}', '{$_cadmiodilinc1}', '{$_cadmiodilinc2}', '{$_cadmiodilinc3}', '{$_cadmiodilinc4}', '{$_cadmiodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027cromo VALUES('{$cs[0]['id']}', '{$_cromocon1}', '{$_cromocon2}', '{$_cromocon3}', '{$_cromocon4}', '{$_cromocon5}', '{$_cromoinc1}', '{$_cromoinc2}', '{$_cromoinc3}', '{$_cromoinc4}', '{$_cromoinc5}', '{$_cromoincrel1}', '{$_cromoincrel2}', '{$_cromoincrel3}', '{$_cromoincrel4}', '{$_cromoincrel5}', '{$_cromoA11}', '{$_cromoA12}', '{$_cromoA13}', '{$_cromoA14}', '{$_cromoA15}', '{$_cromoA21}', '{$_cromoA22}', '{$_cromoA23}', '{$_cromoA24}', '{$_cromoA25}', '{$_cromoA31}', '{$_cromoA32}', '{$_cromoA33}', '{$_cromoA34}', '{$_cromoA35}', '{$_cromoA41}', '{$_cromoA42}', '{$_cromoA43}', '{$_cromoA44}', '{$_cromoA45}', '{$_cromoC11}', '{$_cromoC12}', '{$_cromoC13}', '{$_cromoC14}', '{$_cromoC15}', '{$_cromoC21}', '{$_cromoC22}', '{$_cromoC23}', '{$_cromoC24}', '{$_cromoC25}', '{$_cromoC31}', '{$_cromoC32}', '{$_cromoC33}', '{$_cromoC34}', '{$_cromoC35}', '{$_cromoC41}', '{$_cromoC42}', '{$_cromoC43}', '{$_cromoC44}', '{$_cromoC45}', '{$_cromocp1}', '{$_cromocp2}', '{$_cromocp3}', '{$_cromocp4}', '{$_cromocp5}', '{$_cromoa1}', '{$_cromoa2}', '{$_cromoa3}', '{$_cromoa4}', '{$_cromoa5}', '{$_cromoa6}', '{$_cromoa7}', '{$_cromoa8}', '{$_cromoa9}', '{$_cromoa10}', '{$_cromoa11}', '{$_cromoa12}', '{$_cromoa13}', '{$_cromoa14}', '{$_cromoa15}', '{$_cromoa16}', '{$_cromoa17}', '{$_cromoa18}', '{$_cromoa19}', '{$_cromoa20}', '{$_cromoc1}', '{$_cromoc2}', '{$_cromoc3}', '{$_cromoc4}', '{$_cromoc5}', '{$_cromoc6}', '{$_cromoc7}', '{$_cromoc8}', '{$_cromoc9}', '{$_cromoc10}', '{$_cromoc11}', '{$_cromoc12}', '{$_cromoc13}', '{$_cromoc14}', '{$_cromoc15}', '{$_cromoc16}', '{$_cromoc17}', '{$_cromoc18}', '{$_cromoc19}', '{$_cromoc20}', '{$_cromomaxinc}', '{$_cromomaxincrel}', '{$_cromonummedxmuestra}', '{$_cromonummedxpuncurva}', '{$_cromonummedcurva}', '{$_cromoconmuestra1}', '{$_cromoconmuestra2}', '{$_cromoprompatcalibracion}', '{$_cromodesvestresidual}', '{$_cromodesvestcurvacali}', '{$_cromonp}', '{$_cromolote}', '{$_cromovence}', '{$_cromocc1}', '{$_cromocc2}', '{$_cromoest1}', '{$_cromoest2}', '{$_cromoccest1}', '{$_cromoccest2}', '{$_cromovalor}', '{$_cromoincexp}', '{$_cromok}', '{$_cromoincest}', '{$_cromoestandar1}', '{$_cromoestandar2}', '{$_cromoestandar3}', '{$_cromoestandar4}', '{$_cromoestandar5}', '{$_cromopipeta1}', '{$_cromopipeta2}', '{$_cromopipeta3}', '{$_cromopipeta4}', '{$_cromopipeta5}', '{$_cromovolcorrpipeta1}', '{$_cromovolcorrpipeta2}', '{$_cromovolcorrpipeta3}', '{$_cromovolcorrpipeta4}', '{$_cromovolcorrpipeta5}', '{$_cromoincpip1}', '{$_cromoincpip2}', '{$_cromoincpip3}', '{$_cromoincpip4}', '{$_cromoincpip5}', '{$_cromobalon1}', '{$_cromobalon2}', '{$_cromobalon3}', '{$_cromobalon4}', '{$_cromobalon5}', '{$_cromodilinc1}', '{$_cromodilinc2}', '{$_cromodilinc3}', '{$_cromodilinc4}', '{$_cromodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027plomo VALUES('{$cs[0]['id']}', '{$_plomocon1}', '{$_plomocon2}', '{$_plomocon3}', '{$_plomocon4}', '{$_plomocon5}', '{$_plomoinc1}', '{$_plomoinc2}', '{$_plomoinc3}', '{$_plomoinc4}', '{$_plomoinc5}', '{$_plomoincrel1}', '{$_plomoincrel2}', '{$_plomoincrel3}', '{$_plomoincrel4}', '{$_plomoincrel5}', '{$_plomoA11}', '{$_plomoA12}', '{$_plomoA13}', '{$_plomoA14}', '{$_plomoA15}', '{$_plomoA21}', '{$_plomoA22}', '{$_plomoA23}', '{$_plomoA24}', '{$_plomoA25}', '{$_plomoA31}', '{$_plomoA32}', '{$_plomoA33}', '{$_plomoA34}', '{$_plomoA35}', '{$_plomoA41}', '{$_plomoA42}', '{$_plomoA43}', '{$_plomoA44}', '{$_plomoA45}', '{$_plomoC11}', '{$_plomoC12}', '{$_plomoC13}', '{$_plomoC14}', '{$_plomoC15}', '{$_plomoC21}', '{$_plomoC22}', '{$_plomoC23}', '{$_plomoC24}', '{$_plomoC25}', '{$_plomoC31}', '{$_plomoC32}', '{$_plomoC33}', '{$_plomoC34}', '{$_plomoC35}', '{$_plomoC41}', '{$_plomoC42}', '{$_plomoC43}', '{$_plomoC44}', '{$_plomoC45}', '{$_plomocp1}', '{$_plomocp2}', '{$_plomocp3}', '{$_plomocp4}', '{$_plomocp5}', '{$_plomoa1}', '{$_plomoa2}', '{$_plomoa3}', '{$_plomoa4}', '{$_plomoa5}', '{$_plomoa6}', '{$_plomoa7}', '{$_plomoa8}', '{$_plomoa9}', '{$_plomoa10}', '{$_plomoa11}', '{$_plomoa12}', '{$_plomoa13}', '{$_plomoa14}', '{$_plomoa15}', '{$_plomoa16}', '{$_plomoa17}', '{$_plomoa18}', '{$_plomoa19}', '{$_plomoa20}', '{$_plomoc1}', '{$_plomoc2}', '{$_plomoc3}', '{$_plomoc4}', '{$_plomoc5}', '{$_plomoc6}', '{$_plomoc7}', '{$_plomoc8}', '{$_plomoc9}', '{$_plomoc10}', '{$_plomoc11}', '{$_plomoc12}', '{$_plomoc13}', '{$_plomoc14}', '{$_plomoc15}', '{$_plomoc16}', '{$_plomoc17}', '{$_plomoc18}', '{$_plomoc19}', '{$_plomoc20}', '{$_plomomaxinc}', '{$_plomomaxincrel}', '{$_plomonummedxmuestra}', '{$_plomonummedxpuncurva}', '{$_plomonummedcurva}', '{$_plomoconmuestra1}', '{$_plomoconmuestra2}', '{$_plomoprompatcalibracion}', '{$_plomodesvestresidual}', '{$_plomodesvestcurvacali}', '{$_plomonp}', '{$_plomolote}', '{$_plomovence}', '{$_plomocc1}', '{$_plomocc2}', '{$_plomoest1}', '{$_plomoest2}', '{$_plomoccest1}', '{$_plomoccest2}', '{$_plomovalor}', '{$_plomoincexp}', '{$_plomok}', '{$_plomoincest}', '{$_plomoestandar1}', '{$_plomoestandar2}', '{$_plomoestandar3}', '{$_plomoestandar4}', '{$_plomoestandar5}', '{$_plomopipeta1}', '{$_plomopipeta2}', '{$_plomopipeta3}', '{$_plomopipeta4}', '{$_plomopipeta5}', '{$_plomovolcorrpipeta1}', '{$_plomovolcorrpipeta2}', '{$_plomovolcorrpipeta3}', '{$_plomovolcorrpipeta4}', '{$_plomovolcorrpipeta5}', '{$_plomoincpip1}', '{$_plomoincpip2}', '{$_plomoincpip3}', '{$_plomoincpip4}', '{$_plomoincpip5}', '{$_plomobalon1}', '{$_plomobalon2}', '{$_plomobalon3}', '{$_plomobalon4}', '{$_plomobalon5}', '{$_plomodilinc1}', '{$_plomodilinc2}', '{$_plomodilinc3}', '{$_plomodilinc4}', '{$_plomodilinc5}', '1')");
        $this->_TRANS("INSERT INTO MET027mercurio VALUES('{$cs[0]['id']}', '{$_mercuriocon1}', '{$_mercuriocon2}', '{$_mercuriocon3}', '{$_mercuriocon4}', '{$_mercuriocon5}', '{$_mercurioinc1}', '{$_mercurioinc2}', '{$_mercurioinc3}', '{$_mercurioinc4}', '{$_mercurioinc5}', '{$_mercurioincrel1}', '{$_mercurioincrel2}', '{$_mercurioincrel3}', '{$_mercurioincrel4}', '{$_mercurioincrel5}', '{$_mercurioA11}', '{$_mercurioA12}', '{$_mercurioA13}', '{$_mercurioA14}', '{$_mercurioA15}', '{$_mercurioA21}', '{$_mercurioA22}', '{$_mercurioA23}', '{$_mercurioA24}', '{$_mercurioA25}', '{$_mercurioA31}', '{$_mercurioA32}', '{$_mercurioA33}', '{$_mercurioA34}', '{$_mercurioA35}', '{$_mercurioA41}', '{$_mercurioA42}', '{$_mercurioA43}', '{$_mercurioA44}', '{$_mercurioA45}', '{$_mercurioC11}', '{$_mercurioC12}', '{$_mercurioC13}', '{$_mercurioC14}', '{$_mercurioC15}', '{$_mercurioC21}', '{$_mercurioC22}', '{$_mercurioC23}', '{$_mercurioC24}', '{$_mercurioC25}', '{$_mercurioC31}', '{$_mercurioC32}', '{$_mercurioC33}', '{$_mercurioC34}', '{$_mercurioC35}', '{$_mercurioC41}', '{$_mercurioC42}', '{$_mercurioC43}', '{$_mercurioC44}', '{$_mercurioC45}', '{$_mercuriocp1}', '{$_mercuriocp2}', '{$_mercuriocp3}', '{$_mercuriocp4}', '{$_mercuriocp5}', '{$_mercurioa1}', '{$_mercurioa2}', '{$_mercurioa3}', '{$_mercurioa4}', '{$_mercurioa5}', '{$_mercurioa6}', '{$_mercurioa7}', '{$_mercurioa8}', '{$_mercurioa9}', '{$_mercurioa10}', '{$_mercurioa11}', '{$_mercurioa12}', '{$_mercurioa13}', '{$_mercurioa14}', '{$_mercurioa15}', '{$_mercurioa16}', '{$_mercurioa17}', '{$_mercurioa18}', '{$_mercurioa19}', '{$_mercurioa20}', '{$_mercurioc1}', '{$_mercurioc2}', '{$_mercurioc3}', '{$_mercurioc4}', '{$_mercurioc5}', '{$_mercurioc6}', '{$_mercurioc7}', '{$_mercurioc8}', '{$_mercurioc9}', '{$_mercurioc10}', '{$_mercurioc11}', '{$_mercurioc12}', '{$_mercurioc13}', '{$_mercurioc14}', '{$_mercurioc15}', '{$_mercurioc16}', '{$_mercurioc17}', '{$_mercurioc18}', '{$_mercurioc19}', '{$_mercurioc20}', '{$_mercuriomaxinc}', '{$_mercuriomaxincrel}', '{$_mercurionummedxmuestra}', '{$_mercurionummedxpuncurva}', '{$_mercurionummedcurva}', '{$_mercurioconmuestra1}', '{$_mercurioconmuestra2}', '{$_mercurioprompatcalibracion}', '{$_mercuriodesvestresidual}', '{$_mercuriodesvestcurvacali}', '{$_mercurionp}', '{$_mercuriolote}', '{$_mercuriovence}', '{$_mercuriocc1}', '{$_mercuriocc2}', '{$_mercurioest1}', '{$_mercurioest2}', '{$_mercurioccest1}', '{$_mercurioccest2}', '{$_mercuriovalor}', '{$_mercurioincexp}', '{$_mercuriok}', '{$_mercurioincest}', '{$_mercurioestandar1}', '{$_mercurioestandar2}', '{$_mercurioestandar3}', '{$_mercurioestandar4}', '{$_mercurioestandar5}', '{$_mercuriopipeta1}', '{$_mercuriopipeta2}', '{$_mercuriopipeta3}', '{$_mercuriopipeta4}', '{$_mercuriopipeta5}', '{$_mercuriovolcorrpipeta1}', '{$_mercuriovolcorrpipeta2}', '{$_mercuriovolcorrpipeta3}', '{$_mercuriovolcorrpipeta4}', '{$_mercuriovolcorrpipeta5}', '{$_mercurioincpip1}', '{$_mercurioincpip2}', '{$_mercurioincpip3}', '{$_mercurioincpip4}', '{$_mercurioincpip5}', '{$_mercuriobalon1}', '{$_mercuriobalon2}', '{$_mercuriobalon3}', '{$_mercuriobalon4}', '{$_mercuriobalon5}', '{$_mercuriodilinc1}', '{$_mercuriodilinc2}', '{$_mercuriodilinc3}', '{$_mercuriodilinc4}', '{$_mercuriodilinc5}', '1')");
        return 1;
    }

    function HumedadEncabezadoGet($_id = '')
    {
        $extra = "1=1";
        if ($_id != '') {
            $extra = 'id = ' . $_id;
        }
        return $this->_QUERY("SELECT id, CONVERT(VARCHAR, fecha, 105) AS fecha, estado FROM MET027 WHERE {$extra};");
    }

    function HumedadEncabezadoVacio($_xanalizar)
    {
        $ROW = $this->_QUERY("SELECT '' AS rango, '' AS unidad, CONVERT(VARCHAR, MUEBIT.fecha, 105) AS fechaI, MUE000.solicitud, MUE000.ref, MUE001.analisis, VAR005.nombre AS elemento
		FROM MUE001 INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN MUEBIT ON MUE000.id = MUEBIT.muestra INNER JOIN VAR005 ON MUE001.analisis = VAR005.id
		WHERE (MUEBIT.accion = '0' OR MUEBIT.accion = 'A') AND (MUE001.id = '{$_xanalizar}');");
        //
        $ROW2 = $this->_QUERY("SELECT SER004.rango, SER004.unidad
		FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud
		WHERE (SER003.cs = '{$ROW[0]['solicitud']}') AND (SER004.analisis = {$ROW[0]['analisis']});");
        if ($ROW2) {
            $ROW[0]['rango'] = $ROW2[0]['rango'];
            $ROW[0]['unidad'] = $ROW2[0]['unidad'];
        }


        $ROW[0]['fechaA'] = '';
        $ROW[0]['fechaC'] = '';
        $ROW[0]['repeticiones'] = '0';
        $ROW[0]['recipientemasa1'] = '0';
        $ROW[0]['recipientemasa2'] = '0';
        $ROW[0]['recipienteerror1'] = '0.01';
        $ROW[0]['recipienteerror2'] = '0.01';
        $ROW[0]['recipientemasacorregida1'] = '0';
        $ROW[0]['recipientemasacorregida2'] = '0';
        $ROW[0]['recipientemuestramasa1'] = '0';
        $ROW[0]['recipientemuestramasa2'] = '0';
        $ROW[0]['recipientemuestraerror1'] = '0.01';
        $ROW[0]['recipientemuestraerror2'] = '0.01';
        $ROW[0]['recipientemuestramasacorregida1'] = '0';
        $ROW[0]['recipientemuestramasacorregida2'] = '0';
        $ROW[0]['recipientefinalmasa1'] = '0';
        $ROW[0]['recipientefinalmasa2'] = '0';
        $ROW[0]['recipientefinalerror1'] = '0.01';
        $ROW[0]['recipientefinalerror2'] = '0.01';
        $ROW[0]['recipientefinalmasacorregida1'] = '0';
        $ROW[0]['recipientefinalmasacorregida2'] = '0';
        $ROW[0]['masarecipiente1'] = '0';
        $ROW[0]['masarecipiente2'] = '0';
        $ROW[0]['masarecipientepromedio'] = '0';
        $ROW[0]['masainicialrecipiente1'] = '0';
        $ROW[0]['masainicialrecipiente2'] = '0';
        $ROW[0]['masainicialrecipientepromedio'] = '0';
        $ROW[0]['masainicialmuestra1'] = '0';
        $ROW[0]['masainicialmuestra2'] = '0';
        $ROW[0]['masainicialmuestrapromedio'] = '0';
        $ROW[0]['masafinalrecipiente1'] = '0';
        $ROW[0]['masafinalrecipiente2'] = '0';
        $ROW[0]['masafinalrecipientepromedio'] = '0';
        $ROW[0]['masafinalmuestra1'] = '0';
        $ROW[0]['masafinalmuestra2'] = '0';
        $ROW[0]['masafinalmuestrapromedio'] = '0';
        $ROW[0]['humedad1'] = '0';
        $ROW[0]['humedad2'] = '0';
        $ROW[0]['humedadpromedio'] = '0';
        $ROW[0]['promediohumedad'] = '0';
        $ROW[0]['humedaddesvest'] = '0';
        $ROW[0]['repetibilidadval'] = '0';
        $ROW[0]['repetibilidadcomp'] = '0';
        $ROW[0]['emrval'] = '0';
        $ROW[0]['emvcomp'] = '0';
        $ROW[0]['resval'] = '0';
        $ROW[0]['rescomp'] = '0';
        $ROW[0]['linealval'] = '0';
        $ROW[0]['linealcomp'] = '0';
        $ROW[0]['excentricidadval'] = '0';
        $ROW[0]['excentricidadcomp'] = '0';
        $ROW[0]['incindcomp'] = '0';
        $ROW[0]['incnumeradorcomp'] = '0';
        $ROW[0]['incpesajecomp'] = '0';
        $ROW[0]['duplicadoscomp'] = '0';
        $ROW[0]['incdecomp'] = '0';
        $ROW[0]['incfinalcombcomp'] = '0';
        $ROW[0]['inxexp'] = '0';
        $ROW[0]['duplicadoscomp'] = '0';
        $ROW[0]['duplicadoscomp'] = '0';
        $ROW[0]['resulhumedad'] = '0';
        $ROW[0]['resulincexp'] = '0';
        $ROW[0]['obs'] = '';


        return $ROW;
    }
//
}

?>