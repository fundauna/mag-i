<?php
/*******************************************************************
CLASE DE NIVEL 1 ENCARGADA DEL MODULO DE CARTAS/HOJAS DE VERIFICACION
(final SOLO PUEDE SER INSTANCIADA Y NO HEREDADA)

DELETE FROM CAR028;DELETE FROM CAR027;
DELETE FROM CAR026;DELETE FROM CAR025;DELETE FROM CAR024;DELETE FROM CAR023;DELETE FROM CAR022;DELETE FROM CAR021;DELETE FROM CAR020;
DELETE FROM CAR019;DELETE FROM CAR018;DELETE FROM CAR017;DELETE FROM CAR016;DELETE FROM CAR015;DELETE FROM CAR014;DELETE FROM CAR013;
DELETE FROM CAR012;DELETE FROM CAR011;DELETE FROM CAR010;DELETE FROM CAR009;DELETE FROM CAR008;DELETE FROM CAR007;DELETE FROM CAR006;
DELETE FROM CAR005;DELETE FROM CAR004;DELETE FROM CAR003;DELETE FROM CAR002;DELETE FROM CAR001;DELETE FROM CAR000;DELETE FROM CARBIT;
*******************************************************************/
final class Cartas extends Database{
//
	function CartaADetalleColumnaGet($_tipo, $_desde, $_hasta){
		$_desde = $this->_FECHA($_desde);
		$_hasta = $this->_FECHA($_hasta);	
		$ROW = $this->_QUERY("EXECUTE PA_MAG_093 '{$_tipo}', '{$_desde}', '{$_hasta} 23:59:00';");
		
		$str = '<table class="radius" width="600px">
		<tr>
			<td><strong>Fecha</strong></td>
			<td><strong>% Recuperaci&oacute;n</strong></td>
			<td><strong>Analito</strong></td>
			<td><strong>Matriz</strong></td>
			<td><strong>Analista</strong></td>
			<td><strong>Obs.</strong></td>
		</tr>';
		for($x=0;$x<count($ROW);$x++){
			$temp = number_format($ROW[$x]['conc'], 2, '.', '');
			if(str_replace(' ', '', $ROW[$x]['obs']) != ''){
				$title = "title='{$ROW[$x]['obs']}' style='cursor:help'";
				$ROW[$x]['obs'] = '**';
			}else $title = '';
			if($temp < 60 or $temp > 140) $color = 'style="color:#FF0000"';
			else $color = '';
			
			$str .= "<tr>
				<td>{$ROW[$x]['fecha1']}</td>
				<td {$color}>{$temp}</td>
				<td>{$ROW[$x]['analito']}</td>
				<td>{$ROW[$x]['matriz']}</td>
				<td>{$ROW[$x]['analista']}</td>
				<td {$title}>{$ROW[$x]['obs']}</td>
			</tr>";
		}
	
		$str .= '</table><table id="tabla" style="display:none;"><tr><td>Fecha</td><td>% Recup.</td></tr>';
		for($x=0;$x<count($ROW);$x++){
			$temp = number_format($ROW[$x]['conc'], 2, '.', '');
			$str .= "<tr><td>{$ROW[$x]['fecha1']}</td><td>{$temp}</td></tr>";
		}
		$str .= "</table>";
		return $str;
	}
	
	function Carta0DetalleColumnaGet($_cs, $_tipo, $_desde, $_hasta){
		$_desde = $this->_FECHA2($_desde);
		$_hasta = $this->_FECHA2($_hasta);
                
                $ROW = $this->_QUERY("SELECT CONVERT(VARCHAR, fecha, 6) AS fecha1, masa{$_tipo} AS masa
		FROM CAR002 
		WHERE (carta = '{$_cs}') AND (fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')
		ORDER BY fecha");
		
		$valores = array();
		$str = '<table id="tabla"><tr><td>Fecha</td><td>Masa</td></tr>';
		for($x=0;$x<count($ROW);$x++){
			$valores[$x] = $ROW[$x]['masa'];
			$str .= "<tr><td>{$ROW[$x]['fecha1']}</td><td>{$ROW[$x]['masa']}</td></tr>";
		}
		
		$valores = number_format($this->DesvEst($valores), 2, '.', '');
		$str .= "</table><input type='hidden' id='estandar' value='{$valores}' />";
		return $str;
	}
	
	function Carta0DetalleElimina($_carta, $_fecha){
		return $this->_TRANS("DELETE FROM CAR002 WHERE (carta = '{$_carta}') AND (fecha = '{$_fecha}');");
	}
	
	function Carta0DetalleGet($_cs){
		return $this->_QUERY("SELECT CONVERT(VARCHAR, CAR002.fecha, 105) AS fecha1, CONVERT(VARCHAR, CAR002.fecha, 100) AS fecha2, CAR002.masaA, CAR002.masaB, SEG001.nombre, SEG001.ap1, SEG001.ap2
		FROM CAR002 INNER JOIN SEG001 ON CAR002.funcionario = SEG001.id
		WHERE (CAR002.carta = '{$_cs}') ORDER BY fecha;");
	}
	
	function Carta0EncabezadoGet($_cs){
		return $this->_QUERY("SELECT CONVERT(VARCHAR, CAR000.fecha, 105) AS fecha, CAR000.estado, CAR001.codigo AS cs, CAR001.tipo_bal, CAR001.rango, CAR001.pesa1, CAR001.nominal1, CAR001.cert1, CAR001.pesa2, CAR001.nominal2, CAR001.cert2, EQU000.id AS cod_bal, EQU000.codigo AS nom_bal, (EQU000.marca + ' / ' + EQU000.modelo) AS nombre
		FROM CAR000 INNER JOIN CAR001 ON CAR000.consecutivo = CAR001.codigo INNER JOIN EQU000 ON CAR001.balanza = EQU000.id
		WHERE (CAR000.consecutivo = '{$_cs}');");
	}
	
	function Carta0EncabezadoVacio(){
		return array(0=>array(
			'cs'=>'',
			'fecha'=>'',
			'estado'=>'',
			'tipo_bal'=>'',
			'nom_bal'=>'',
			'cod_bal'=>'',
			'nombre'=>'',
			'rango'=>'',
			'pesa1'=>'',
			'nominal1'=>'',
			'cert1'=>'',
			'pesa2'=>'',
			'nominal2'=>'',
			'cert2'=>'')
		);
	}
	
	function Carta0IME($_cs, $_accion, $_tipo_bal, $_cod_bal, $_rango, $_pesa1, $_nominal1, $_certificado1, $_pesa2, $_nominal2, $_certificado2, $_fecha, $_masaA, $_masaB, $_LID, $_UID){
		$_fecha = $this->_FECHA($_fecha).' '.date('H:i:s');
		$tipo = 0;
		$_rango = $this->Free($_rango);
		$_masaA = str_replace(',', '', $_masaA);
		$_masaB = str_replace(',', '', $_masaB);
		//
		if($_accion == 'I') $_cs = $this->CartaConsecutivoGet($tipo);

		if($this->_TRANS("EXECUTE PA_MAG_072 '{$_cs}', 
			'{$tipo}', 
			'{$_LID}', 
			'{$_tipo_bal}', 
			'{$_cod_bal}', 
			'{$_rango}', 
			'{$_pesa1}', '{$_nominal1}', '{$_certificado1}', 
			'{$_pesa2}', '{$_nominal2}', '{$_certificado2}', 
			'{$_accion}';")){
			
                            $this->_TRANS("INSERT INTO CAR002 values('{$_cs}', '{$_fecha}', '{$_masaA}', '{$_masaB}', '{$_UID}');");	
			
                            $this->CartaConsecutivoSet($tipo);
			
                            return $_cs;
		}else return 0;
	}
	
	function Carta1DetalleGet($_cs){
		return $this->_QUERY("SELECT valA, valB, valC, valD
		FROM CAR008 WHERE (carta = '{$_cs}') ORDER BY pto;");
	}
	
	function Carta1DetalleVacio(){
		return array(0=>array(
			'valA'=>'0',
			'valB'=>'0',
			'valC'=>'0',
			'valD'=>'0')
		);
	}
	
	function Carta1EncabezadoGet($_cs){
		return $this->_QUERY("SELECT '{$_cs}' AS cs, CONVERT(VARCHAR, CAR000.fecha, 105) AS fecha, CAR000.estado, CAR000.codigoalterno, CAR007.tipo_bal, CAR007.balanza AS cod_bal, CAR007.linealidad, CAR007.repetibilidad, CAR007.rango, CAR007.decimales, 
	    CAR007.cert1, CAR007.cert2, CAR007.cert3, CAR007.cert4, CAR007.pesa1, CAR007.pesa2, CAR007.pesa3, CAR007.pesa4, 
	    CAR007.nominal1, CAR007.nominal2, CAR007.nominal3, CAR007.nominal4, CAR007.real1, CAR007.real2, CAR007.real3, 
	    CAR007.real4, CAR007.inc1, CAR007.inc2, CAR007.inc3, CAR007.inc4, CAR007.pto1_ind_asc, CAR007.pto2_ind_asc, 
	    CAR007.pto3_ind_asc, CAR007.pto4_ind_asc, CAR007.pto1_ind_dsc, CAR007.pto2_ind_dsc, CAR007.pto3_ind_dsc, CAR007.pto4_ind_dsc, 
	    CAR007.platillo, CAR007.pesa5, CAR007.nominal5, CAR007.real5, CAR007.sensibilidad, CAR007.pos0, CAR007.pos1, 
	    CAR007.pos2, CAR007.pos3, CAR007.pos4, CAR007.pos5, CAR007.obs, EQU000.codigo AS nom_bal, (EQU000.marca + ' / ' + EQU000.modelo) AS nombre
		FROM CAR007 INNER JOIN EQU000 ON CAR007.balanza = EQU000.id INNER JOIN CAR000 ON CAR007.codigo = CAR000.consecutivo
		WHERE (CAR000.consecutivo = '{$_cs}');");
	}
	
	function Carta1EncabezadoVacio(){
		return array(0=>array(
			'cs'=>'',
                        'codigoalterno'=>'',
			'fecha'=>'',
			'estado'=>'',
			'obs'=>'',
			'tipo_bal'=>'',
			'nom_bal'=>'',
			'cod_bal'=>'',
			'nombre'=>'',
			'linealidad'=>'0',
			'repetibilidad'=>'0',
			'sensibilidad'=>'0',
			'rango'=>'',
			'platillo'=>'',
			'decimales'=>'5555',
			//
			'cert1'=>'','pesa1'=>'','nominal1'=>'','real1'=>'','inc1'=>'',
			'cert2'=>'','pesa2'=>'','nominal2'=>'','real2'=>'','inc2'=>'',
			'cert3'=>'','pesa3'=>'','nominal3'=>'','real3'=>'','inc3'=>'',
			'cert4'=>'','pesa4'=>'','nominal4'=>'','real4'=>'','inc4'=>'',
						'pesa5'=>'','nominal5'=>'','real5'=>'',
			//
			'pos0'=>'','pos1'=>'','pos2'=>'','pos3'=>'','pos4'=>'','pos5'=>'',
			//
			'pto1_ind_asc'=>'0', 'pto1_ind_dsc'=>'0',
			'pto2_ind_asc'=>'0', 'pto2_ind_dsc'=>'0',
			'pto3_ind_asc'=>'0', 'pto3_ind_dsc'=>'0',
			'pto4_ind_asc'=>'0', 'pto4_ind_dsc'=>'0'
			)
		);
	}
	
	function Carta1Paso1($_cs, $_accion, $_tipo_bal, $_cod_bal, $_linealidad, $_repetibilidad, $_rango, $_decimalesA, $_decimalesB, $_decimalesC, $_decimalesD, $_cert1, $_cert2, $_cert3, $_cert4, $_pesa1, $_pesa2, $_pesa3, $_pesa4, $_nominal1, $_nominal2, $_nominal3, $_nominal4, $_real1, $_real2, $_real3, $_real4, $_inc1, $_inc2, $_inc3, $_inc4, $_LID, $_UID){
		$tipo = 1;
		$_linealidad = str_replace(',', '', $_linealidad);
		$_repetibilidad = str_replace(',', '', $_repetibilidad);
		$_nominal1 = str_replace(',', '', $_nominal1);
		$_nominal2 = str_replace(',', '', $_nominal2);
		$_nominal3 = str_replace(',', '', $_nominal3);
		$_nominal4 = str_replace(',', '', $_nominal4);
		$_real1 = str_replace(',', '', $_real1);
		$_real2 = str_replace(',', '', $_real2);
		$_real3 = str_replace(',', '', $_real3);
		$_real4 = str_replace(',', '', $_real4);
		$_inc1 = str_replace(',', '', $_inc1);
		$_inc2 = str_replace(',', '', $_inc2);
		$_inc3 = str_replace(',', '', $_inc3);
		$_inc4 = str_replace(',', '', $_inc4);
		
		if($_accion == 'I') $_cs = $this->CartaConsecutivoGet($tipo);
		
		if($this->_TRANS("EXECUTE PA_MAG_076 '{$_cs}', 
			'{$tipo}',
			'{$_tipo_bal}',
			'{$_cod_bal}',
			'{$_linealidad}',
			'{$_repetibilidad}',
			'{$_rango}',
			'{$_decimalesA}{$_decimalesB}{$_decimalesC}{$_decimalesD}',
			'{$_cert1}',
			'{$_cert2}',
			'{$_cert3}',
			'{$_cert4}',
			'{$_pesa1}',
			'{$_pesa2}',
			'{$_pesa3}',
			'{$_pesa4}',
			'{$_nominal1}',
			'{$_nominal2}',
			'{$_nominal3}',
			'{$_nominal4}',	
			'{$_real1}',
			'{$_real2}',
			'{$_real3}',
			'{$_real4}',
			'{$_inc1}',
			'{$_inc2}',
			'{$_inc3}',
			'{$_inc4}',
			'{$_LID}', 
			'{$_UID}', 
			'{$_accion}';")){
			
			$this->CartaConsecutivoSet($tipo);
			
			return $_cs;
		}else return 0;
	}
	
	function Carta1Paso2($_cs, $_obs, $_pto1_ind_asc, $_pto2_ind_asc, $_pto3_ind_asc, $_pto4_ind_asc, $_pto1_ind_dsc, $_pto2_ind_dsc, $_pto3_ind_dsc, $_pto4_ind_dsc, $_platillo, $_pesa5, $_nominal5, $_real5, $_sensibilidad, $_pos0, $_pos1, $_pos2, $_pos3, $_pos4, $_pos5){
		$_obs = $this->Free($_obs);
		$_pto1_ind_asc = str_replace(',', '', $_pto1_ind_asc);
		$_pto2_ind_asc = str_replace(',', '', $_pto2_ind_asc);
		$_pto3_ind_asc = str_replace(',', '', $_pto3_ind_asc);
		$_pto4_ind_asc = str_replace(',', '', $_pto4_ind_asc);
		$_pto1_ind_dsc = str_replace(',', '', $_pto1_ind_dsc);
		$_pto2_ind_dsc = str_replace(',', '', $_pto2_ind_dsc);
		$_pto3_ind_dsc = str_replace(',', '', $_pto3_ind_dsc);
		$_pto4_ind_dsc = str_replace(',', '', $_pto4_ind_dsc);
		//
		$_nominal5 = str_replace(',', '', $_nominal5);
		$_real5 = str_replace(',', '', $_real5);
		$_sensibilidad = str_replace(',', '', $_sensibilidad);
		$_pos0 = str_replace(',', '', $_pos0);
		$_pos1 = str_replace(',', '', $_pos1);
		$_pos2 = str_replace(',', '', $_pos2);
		$_pos3 = str_replace(',', '', $_pos3);
		$_pos4 = str_replace(',', '', $_pos4);
		$_pos5 = str_replace(',', '', $_pos5);
		
		if($this->_TRANS("UPDATE CAR007 SET
			pto1_ind_asc='{$_pto1_ind_asc}',
			pto2_ind_asc='{$_pto2_ind_asc}',
			pto3_ind_asc='{$_pto3_ind_asc}',
			pto4_ind_asc='{$_pto4_ind_asc}',
			pto1_ind_dsc='{$_pto1_ind_dsc}',
			pto2_ind_dsc='{$_pto2_ind_dsc}',
			pto3_ind_dsc='{$_pto3_ind_dsc}',
			pto4_ind_dsc='{$_pto4_ind_dsc}',
			platillo='{$_platillo}',
			pesa5='{$_pesa5}',
			nominal5='{$_nominal5}',
			real5='{$_real5}',
			sensibilidad='{$_sensibilidad}',
			pos0='{$_pos0}',
			pos1='{$_pos1}',
			pos2='{$_pos2}',
			pos3='{$_pos3}',
			pos4='{$_pos4}',
			pos5='{$_pos5}',
			obs='{$_obs}'
			WHERE (codigo = '{$_cs}');")){
		
			return $_cs;
		}else return 0;
	}
	
	function Carta1Paso3($_cs, $_valA, $_valB, $_valC, $_valD){
		$_valA = str_replace(',', '', $_valA);
		$_valB = str_replace(',', '', $_valB);
		$_valC = str_replace(',', '', $_valC);
		$_valD = str_replace(',', '', $_valD);
		$_valA = explode('&', substr($_valA, 1));
		$_valB = explode('&', substr($_valB, 1));
		$_valC = explode('&', substr($_valC, 1));
		$_valD = explode('&', substr($_valD, 1));
		
		$this->_TRANS("DELETE FROM CAR008 WHERE (carta = '{$_cs}');");		
		for($i=1;$i<count($_valA);$i++){
			if(str_replace(' ', '', $_valA[$i]) != ''){
				$this->_TRANS("INSERT INTO CAR008 values('{$_cs}', {$i}, {$_valA[$i]}, {$_valB[$i]}, {$_valC[$i]}, {$_valD[$i]});");					
			}
		}

		return 1;
	}
	
	function Carta2DetalleGet($_cs){
		return $this->_QUERY("SELECT ph4, ph7, ph10
		FROM CAR010 WHERE (carta = '{$_cs}') ORDER BY linea;");
	}
	
	function Carta2DetalleVacio(){
		return array(
			0=>array('ph4'=>'0','ph7'=>'0','ph10'=>'0'),
			1=>array('ph4'=>'0','ph7'=>'0','ph10'=>'0'),
			2=>array('ph4'=>'0','ph7'=>'0','ph10'=>'0'),
			3=>array('ph4'=>'0','ph7'=>'0','ph10'=>'0'),
			4=>array('ph4'=>'0','ph7'=>'0','ph10'=>'0'),
			5=>array('ph4'=>'0','ph7'=>'0','ph10'=>'0'),
			6=>array('ph4'=>'0','ph7'=>'0','ph10'=>'0'),
			7=>array('ph4'=>'0','ph7'=>'0','ph10'=>'0'),
			8=>array('ph4'=>'0','ph7'=>'0','ph10'=>'0'),
			9=>array('ph4'=>'0','ph7'=>'0','ph10'=>'0')
		);
	}
	
	function Carta2EncabezadoGet($_cs){
		return $this->_QUERY("SELECT '{$_cs}' AS cs, CONVERT(VARCHAR, CAR000.fecha, 105) AS fecha, CAR000.estado,CAR000.codigoalterno, CAR009.equipo, CONVERT(VARCHAR, CAR009.fechavence1, 105) AS fechavence1, CONVERT(VARCHAR, CAR009.fechavence2, 105) AS fechavence2, CONVERT(VARCHAR, CAR009.fechavence3, 105) AS fechavence3, CAR009.lote1, CAR009.lote2, 
        CAR009.lote3, CAR009.certificado1, CAR009.certificado2, CAR009.certificado3, CAR009.electrodo, CAR009.limiteph, CAR009.limitedesv, 
        CAR009.limitecoef, CAR009.limitemv, EQU000.codigo AS nomequipo, (EQU000.marca + ' / ' + EQU000.modelo) AS marca
		FROM CAR009 INNER JOIN CAR000 ON CAR009.codigo = CAR000.consecutivo INNER JOIN EQU000 ON CAR009.equipo = EQU000.id
		WHERE (CAR000.consecutivo = '{$_cs}');");
	}
	
	function Carta2EncabezadoVacio(){
		return array(0=>array(
			'cs'=>'',
                        'codigoalterno'=>'',
			'fecha'=>'',
			'estado'=>'',
			'nomequipo'=>'',
			'equipo'=>'',
			'marca'=>'',
			'fechavence1'=>'',	
			'fechavence2'=>'',
			'fechavence3'=>'',	
			'lote1'=>'',
			'lote2'=>'',
			'lote3'=>'',
			'certificado1'=>'',
			'certificado2'=>'',
			'certificado3'=>'',
			'electrodo'=>'0',
			'limiteph'=>'0.02',
			'limitedesv'=>'0.02',
			'limitecoef'=>'0.25',
			'limitemv'=>'35')
		);
	}
	
	function Carta2Paso1($_cs, $_accion, $_fecha, $_equipo, $_fechavence1, $_fechavence2, $_fechavence3, $_lote1, $_lote2, $_lote3, $_certificado1, $_certificado2, $_certificado3, $_electrodo, $_limiteph, $_limitedesv, $_limitecoef, $_limitemv, $_UID, $_LID){
		$_fechavence1 = $this->_FECHA($_fechavence1);
		$_fechavence2 = $this->_FECHA($_fechavence2);
		$_fechavence3 = $this->_FECHA($_fechavence3);
		$tipo = 2;
		$_electrodo = str_replace(',', '', $_electrodo);
		$_limiteph = str_replace(',', '', $_limiteph);
		$_limitedesv = str_replace(',', '', $_limitedesv);
		$_limitecoef = str_replace(',', '', $_limitecoef);
		$_limitemv = str_replace(',', '', $_limitemv);
		//
		if($_accion == 'I') $_cs = $this->CartaConsecutivoGet($tipo);
		
		if($this->_TRANS("EXECUTE PA_MAG_077 '{$_cs}', 
			'{$tipo}',
			'{$_equipo}', 
			'{$_fechavence1}', 
			'{$_fechavence2}', 
			'{$_fechavence3}', 
			'{$_lote1}', 
			'{$_lote2}', 
			'{$_lote3}', 
			'{$_certificado1}', 
			'{$_certificado2}', 
			'{$_certificado3}', 
			'{$_electrodo}', 
			'{$_limiteph}', 
			'{$_limitedesv}', 
			'{$_limitecoef}', 
			'{$_limitemv}',
			'{$_LID}', 
			'{$_UID}', 
			'{$_accion}',
                        '{$_fecha}';")){    
						
			$this->CartaConsecutivoSet($tipo);

			return $_cs;
		}else return 0;
	}
	
	function Carta2Paso2($_cs, $_valA, $_valB, $_valC){
		$_valA = str_replace(',', '', $_valA);
		$_valB = str_replace(',', '', $_valB);
		$_valC = str_replace(',', '', $_valC);
		$_valA = explode('&', substr($_valA, 1));
		$_valB = explode('&', substr($_valB, 1));
		$_valC = explode('&', substr($_valC, 1));
		
		$this->_TRANS("DELETE FROM CAR010 WHERE (carta = '{$_cs}');");		
		for($i=1;$i<count($_valA);$i++){
			if(str_replace(' ', '', $_valA[$i]) != ''){
				$this->_TRANS("INSERT INTO CAR010 values('{$_cs}', {$i}, {$_valA[$i]}, {$_valB[$i]}, {$_valC[$i]});");					
			}
		}

		return 1;
	}
	
	function Carta3DetalleGet($_cs){
		return $this->_QUERY("SELECT ph4, ph7, ph10, phcond
		FROM CAR012 WHERE (carta = '{$_cs}') ORDER BY linea;");
	}
	
	function Carta3DetalleVacio(){
		return array(
			0=>array('ph4'=>'0','ph7'=>'0','ph10'=>'0','phcond'=>'0'),
			1=>array('ph4'=>'0','ph7'=>'0','ph10'=>'0','phcond'=>'0'),
			2=>array('ph4'=>'0','ph7'=>'0','ph10'=>'0','phcond'=>'0'),
			3=>array('ph4'=>'0','ph7'=>'0','ph10'=>'0','phcond'=>'0'),
			4=>array('ph4'=>'0','ph7'=>'0','ph10'=>'0','phcond'=>'0'),
			5=>array('ph4'=>'0','ph7'=>'0','ph10'=>'0','phcond'=>'0'),
			6=>array('ph4'=>'0','ph7'=>'0','ph10'=>'0','phcond'=>'0'),
			7=>array('ph4'=>'0','ph7'=>'0','ph10'=>'0','phcond'=>'0'),
			8=>array('ph4'=>'0','ph7'=>'0','ph10'=>'0','phcond'=>'0'),
			9=>array('ph4'=>'0','ph7'=>'0','ph10'=>'0','phcond'=>'0')
		);
	}
	
	function Carta3EncabezadoGet($_cs){
		return $this->_QUERY("SELECT '{$_cs}' AS cs, CONVERT(VARCHAR, CAR000.fecha, 105) AS fecha, CAR000.estado,CAR000.codigoalterno, CAR011.equipo, CONVERT(VARCHAR, CAR011.fechavence0, 105) AS fechavence0, CONVERT(VARCHAR, CAR011.fechavence1, 105) AS fechavence1, CONVERT(VARCHAR, CAR011.fechavence2, 105) AS fechavence2, CONVERT(VARCHAR, CAR011.fechavence3, 105) AS fechavence3, CONVERT(VARCHAR, CAR011.fechavence4, 105) AS fechavence4, CAR011.lote1, CAR011.lote2, 
        CAR011.lote3, CAR011.lote4, CAR011.certificado1, CAR011.certificado2, CAR011.certificado3, CAR011.certificado4, CAR011.limiteph, CAR011.limitedesv, 
        CAR011.limitecoef, CAR011.limitecond, CAR011.lecturaC, CAR011.pendiente, CAR011.desviacion, CAR011.observaciones, EQU000.codigo AS nomequipo, (EQU000.marca + ' / ' + EQU000.modelo) AS marca
		FROM CAR011 INNER JOIN CAR000 ON CAR011.codigo = CAR000.consecutivo INNER JOIN EQU000 ON CAR011.equipo = EQU000.id
		WHERE (CAR000.consecutivo = '{$_cs}');");
	}
	
	function Carta3EncabezadoVacio(){
		return array(0=>array(
			'cs'=>'',
                        'codigoalterno'=>'',
			'fecha'=>'',
			'estado'=>'',
			'nomequipo'=>'',
			'equipo'=>'',
			'marca'=>'',
			'usuario'=>'',
			'fechavence0'=>'',
			'fechavence1'=>'',	
			'fechavence2'=>'',
			'fechavence3'=>'',	
			'fechavence4'=>'',	
			'lote1'=>'',
			'lote2'=>'',
			'lote3'=>'',
			'lote4'=>'',
			'certificado1'=>'',
			'certificado2'=>'',
			'certificado3'=>'',
			'certificado4'=>'',
			'limiteph'=>'0.02',
			'limitedesv'=>'0.02',
			'limitecoef'=>'0.2',
			'limitecond'=>'10',
			'lecturaC'=>'',
			'pendiente'=>'',
			'desviacion'=>'',
			'observaciones'=>'')
		);
	}
	
	function Carta3Paso1($_cs, $_accion, $_fecha, $_observaciones, $_equipo, $_fechavence0, $_fechavence1, $_fechavence2, $_fechavence3, $_fechavence4, $_lote1, $_lote2, $_lote3, $_lote4, $_certificado1, $_certificado2, $_certificado3, $_certificado4, $_limiteph, $_limitedesv, $_limitecoef, $_limitecond, $_lecturaC, $_pendiente, $_desviacion, $_UID, $_LID){
		$_fechavence0 = $this->_FECHA($_fechavence0);
		$_fechavence1 = $this->_FECHA($_fechavence1);
		$_fechavence2 = $this->_FECHA($_fechavence2);
		$_fechavence3 = $this->_FECHA($_fechavence3);
		$_fechavence4 = $this->_FECHA($_fechavence4);
		$tipo = 3;
		$_limiteph = str_replace(',', '', $_limiteph);
		$_limitedesv = str_replace(',', '', $_limitedesv);
		$_limitecoef = str_replace(',', '', $_limitecoef);
		$_limitecond = str_replace(',', '', $_limitecond);
		$_pendiente = str_replace(',', '', $_pendiente);
		$_desviacion = str_replace(',', '', $_desviacion);
		//
		if($_accion == 'I') $_cs = $this->CartaConsecutivoGet($tipo);
		
		if($this->_TRANS("EXECUTE PA_MAG_078 '{$_cs}', 
			'{$tipo}',
			'{$_observaciones}',
			'{$_equipo}', 
			'{$_fechavence0}', 
			'{$_fechavence1}', 
			'{$_fechavence2}', 
			'{$_fechavence3}',
			'{$_fechavence4}', 
			'{$_lote1}', 
			'{$_lote2}', 
			'{$_lote3}', 
			'{$_lote4}', 
			'{$_certificado1}', 
			'{$_certificado2}', 
			'{$_certificado3}',
			'{$_certificado4}',  
			'{$_limiteph}', 
			'{$_limitedesv}', 
			'{$_limitecoef}', 
			'{$_limitecond}',
			'{$_lecturaC}', 
			'{$_pendiente}', 
			'{$_desviacion}',
			'{$_LID}', 
			'{$_UID}', 
			'{$_accion}',
                        '{$_fecha}';")){    
						
			$this->CartaConsecutivoSet($tipo);

			return $_cs;
		}else return 0;
	}
	
	function Carta3Paso2($_cs, $_valA, $_valB, $_valC, $_valD){
		$_valA = str_replace(',', '', $_valA);
		$_valB = str_replace(',', '', $_valB);
		$_valC = str_replace(',', '', $_valC);
		$_valD = str_replace(',', '', $_valD);
		$_valA = explode('&', substr($_valA, 1));
		$_valB = explode('&', substr($_valB, 1));
		$_valC = explode('&', substr($_valC, 1));
		$_valD = explode('&', substr($_valD, 1));
		
		$this->_TRANS("DELETE FROM CAR012 WHERE (carta = '{$_cs}');");		
		for($i=1;$i<count($_valA);$i++){
			if(str_replace(' ', '', $_valA[$i]) != ''){
				$this->_TRANS("INSERT INTO CAR012 values('{$_cs}', {$i}, {$_valA[$i]}, {$_valB[$i]}, {$_valC[$i]}, {$_valD[$i]});");					
			}
		}

		return 1;
	}
	
	function Carta4DetalleColumnaGet($_cs, $_tipo, $_desde, $_hasta){	
		$_desde = $this->_FECHA($_desde);
		$_hasta = $this->_FECHA($_hasta);	
		$ROW = $this->_QUERY("SELECT CONVERT(VARCHAR, fecha, 6) AS fecha1, {$_tipo} AS masa
		FROM CAR004
		WHERE (carta = '{$_cs}') AND (fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')
		ORDER BY fecha");
		
		$str = '<table id="tabla"><tr><td></td><td>Fecha</td></tr>';
		for($x=0;$x<count($ROW);$x++){
			$str .= "<tr><td>{$ROW[$x]['fecha1']}</td><td>{$ROW[$x]['masa']}</td></tr>";
		}
		
		$str .= "</table>";
		return $str;
	}
	
	function Carta4DetalleElimina($_carta, $_fecha, $_hora){
		$_fecha = $this->_FECHA($_fecha);
		return $this->_TRANS("DELETE FROM CAR004 WHERE (carta = '{$_carta}') AND (fecha = '{$_fecha}') AND (hora = '{$_hora}');");
	}
	
	function Carta4DetalleGet($_cs){
		return $this->_QUERY("SELECT CONVERT(VARCHAR, CAR004.fecha, 105) AS fecha1, CAR004.hora, CAR004.temp1, CAR004.temp2, CAR004.hume1, CAR004.hume2, SEG001.nombre, SEG001.ap1, SEG001.ap2
		FROM CAR004 INNER JOIN SEG001 ON CAR004.funcionario = SEG001.id
		WHERE (CAR004.carta = '{$_cs}') ORDER BY fecha;");
	}
	
	function Carta4EncabezadoGet($_cs){
		return $this->_QUERY("SELECT CAR000.consecutivo AS cs, CONVERT(VARCHAR, CAR000.fecha, 105) AS fecha, CAR000.estado,CAR000.codigoalterno, CAR003.cod_ter, CAR003.cuarto, CAR003.limite1, CAR003.limite2, CAR003.humedad, 
		CAR003.indicacion1, CAR003.error1, CAR003.indicacion2, CAR003.error2, CAR003.indicacion3, CAR003.error3, CAR003.hr1, 
		CAR003.err_hr1, CAR003.hr2, CAR003.err_hr2, CAR003.hr3, CAR003.err_hr3, CAR003.obs
		FROM CAR003 INNER JOIN CAR000 ON CAR003.codigo = CAR000.consecutivo
		WHERE (CAR000.consecutivo = '{$_cs}');");
	}
	
	function Carta4EncabezadoVacio(){
		return array(0=>array(
			'cs'=>'',
			'fecha'=>'',
			'estado'=>'',
			'cod_ter'=>'',
			'cuarto'=>'',
			'limite1'=>'',
			'limite2'=>'',
			'humedad'=>'',
			'indicacion1'=>'...',
			'error1'=>'...',
			'indicacion2'=>'...',
			'error2'=>'...',
			'indicacion3'=>'...',
			'error3'=>'...',
			'hr1'=>'...',
			'err_hr1'=>'...',
			'hr2'=>'...',
			'err_hr2'=>'...',
			'hr3'=>'...',
			'err_hr3'=>'...',
			'obs'=>'')
		);
	}
	
	function Carta4IME($_cs, $_accion, $_cod_ter, $_obs, $_cuarto, $_limites, $_humedad, $_indicacion1, $_error1, $_indicacion2, $_error2, $_indicacion3, $_error3, $_hr1, $_err_hr1, $_hr2, $_err_hr2, $_hr3, $_err_hr3, $_fecha, $_hora, $_temp1, $_temp2, $_hume1, $_hume2, $_LID, $_UID){
		$_fecha = $this->_FECHA($_fecha);
		$tipo = 4;
		$_cod_ter = str_replace(' ', '', $_cod_ter);
		$_obs = $this->Free($_obs);
		$_limites = explode(' a ', $_limites);
		//
		$_humedad = str_replace(',', '', $_humedad);
		$_indicacion1 = str_replace(',', '', $_indicacion1);
		$_error1 = str_replace(',', '', $_error1);
		$_indicacion2 = str_replace(',', '', $_indicacion2);
		$_error2 = str_replace(',', '', $_error2);
		$_indicacion3 = str_replace(',', '', $_indicacion3);
		$_error3 = str_replace(',', '', $_error3);
		//
		$_hr1 = str_replace(',', '', $_hr1);
		$_err_hr1 = str_replace(',', '', $_err_hr1);
		$_hr2 = str_replace(',', '', $_hr2);
		$_err_hr2 = str_replace(',', '', $_err_hr2);
		$_hr3 = str_replace(',', '', $_hr3);
		$_err_hr3 = str_replace(',', '', $_err_hr3);
		//
		$_temp1 = str_replace(',', '', $_temp1);
		$_temp2 = str_replace(',', '', $_temp2);
		$_hume1 = str_replace(',', '', $_hume1);
		$_hume2 = str_replace(',', '', $_hume2);
		//
		if($_accion == 'I') $_cs = $this->CartaConsecutivoGet($tipo);
		//
		if($this->_TRANS("EXECUTE PA_MAG_073 '{$_cs}', 
			'{$tipo}', 
			'{$_LID}', 
			'{$_UID}', 
			'{$_cod_ter}', 
			'{$_cuarto}', 
			'{$_limites[0]}', 
			'{$_limites[1]}', 
			'{$_humedad}', 
			'{$_indicacion1}','{$_error1}',
			'{$_indicacion2}','{$_error2}',
			'{$_indicacion3}','{$_error3}',
			'{$_hr1}','{$_err_hr1}',
			'{$_hr2}','{$_err_hr2}',
			'{$_hr3}','{$_err_hr3}',
			'{$_obs}',
			'{$_fecha}',
			'{$_hora}',
			'{$_temp1}',
			'{$_temp2}',
			'{$_hume1}',
			'{$_hume2}', 
			'{$_accion}';")){
						
			$this->CartaConsecutivoSet($tipo);

			return $_cs;
		}else return 0;
	}
	
	function Carta5DetalleColumnaGet($_cs, $_tipo, $_desde, $_hasta){		
		$ROW = $this->_QUERY("SELECT CONVERT(VARCHAR, fecha, 6) AS fecha1, temp1 AS masa
		FROM CAR006
		WHERE (carta = '{$_cs}') AND (fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')
		ORDER BY fecha");
		
		$str = '<table id="tabla"><tr><td></td><td>Fecha</td></tr>';
		for($x=0;$x<count($ROW);$x++){
			$str .= "<tr><td>{$ROW[$x]['fecha1']}</td><td>{$ROW[$x]['masa']}</td></tr>";
		}
		
		$str .= "</table>";
		return $str;
	}
	
	function Carta5DetalleElimina($_carta, $_fecha, $_hora){
		$_fecha = $this->_FECHA($_fecha);
		return $this->_TRANS("DELETE FROM CAR006 WHERE (carta = '{$_carta}') AND (fecha = '{$_fecha}') AND (hora = '{$_hora}');");
	}
	
	function Carta5DetalleGet($_cs){
		return $this->_QUERY("SELECT CONVERT(VARCHAR, CAR006.fecha, 105) AS fecha1, CAR006.hora, CAR006.temp1, CAR006.temp2, SEG001.nombre, SEG001.ap1, SEG001.ap2
		FROM CAR006 INNER JOIN SEG001 ON CAR006.funcionario = SEG001.id
		WHERE (CAR006.carta = '{$_cs}') ORDER BY fecha;");
	}
	
	function Carta5EncabezadoGet($_cs){
		return $this->_QUERY("SELECT CAR000.consecutivo AS cs, CONVERT(VARCHAR, CAR000.fecha, 105) AS fecha, CAR000.estado,CAR000.codigoalterno, CAR005.cod_cam, CAR005.cod_ter, CAR005.limite1, CAR005.limite2, CAR005.tipo, CAR005.ubicacion, CAR005.inc1, CAR005.ind1, CAR005.factor1, CAR005.inc2, CAR005.ind2, CAR005.factor2, CAR005.inc3, CAR005.ind3, CAR005.factor3, CAR005.inc, CAR005.ind4, CAR005.factor4, CAR005.inc5, CAR005.ind5, CAR005.factor5
		FROM CAR000 INNER JOIN CAR005 ON CAR000.consecutivo = CAR005.codigo
		WHERE (CAR000.consecutivo = '{$_cs}');");
	}
	
	function Carta5EncabezadoVacio(){
		return array(0=>array(
			'cs'=>'',
			'fecha'=>'',
			'estado'=>'',
			'cod_ter'=>'',
			'cod_cam'=>'',
			'limite1'=>'',
			'limite2'=>'',
			'tipo'=>'',
			'inc1'=>'...',
			'ind1'=>'...',
			'factor1'=>'...',
			'inc2'=>'...',
			'ind2'=>'...',
			'factor2'=>'...',
			'inc3'=>'...',
			'ind3'=>'...',
			'factor3'=>'...',
			'inc4'=>'...',
			'ind4'=>'...',
			'factor4'=>'...',
			'inc5'=>'...',
			'ind5'=>'...',
			'factor5'=>'...',
			'ubicacion'=>'')
		);
	}
	
	function Carta5IME($_cs, $_accion, $_cod_cam, $_cod_ter, $_limites, $_tipo2, $_ubicacion, $_inc1, $_ind1, $_factor1, $_inc2, $_ind2, $_factor2, $_inc3,$_ind3, $_factor3, $_inc4,$_ind4, $_factor4, $_inc5, $_ind5, $_factor5, $_fecha, $_hora, $_temp1, $_temp2, $_LID, $_UID){
		$_fecha = $this->_FECHA($_fecha);
		$tipo = 5;
		$_cod_cam = str_replace(' ', '', $_cod_cam);
		$_cod_ter = str_replace(' ', '', $_cod_ter);
		$_limites = explode(' a ', $_limites);
		$_ubicacion = $this->Free($_ubicacion);
		//
		$_inc1 = str_replace(',', '', $_inc1);
		$_ind1 = str_replace(',', '', $_ind1);
		$_factor1 = str_replace(',', '', $_factor1);
		$_inc2 = str_replace(',', '', $_inc2);
		$_ind2 = str_replace(',', '', $_ind2);
		$_factor2 = str_replace(',', '', $_factor2);
		$_inc3 = str_replace(',', '', $_inc3);
		$_ind3 = str_replace(',', '', $_ind3);
		$_factor3 = str_replace(',', '', $_factor3);
		$_inc4 = str_replace(',', '', $_inc4);
		$_ind4 = str_replace(',', '', $_ind4);
		$_factor4 = str_replace(',', '', $_factor4);
		$_inc5 = str_replace(',', '', $_inc5);
		$_ind5 = str_replace(',', '', $_ind5);
		$_factor5 = str_replace(',', '', $_factor5);
		//
		$_temp1 = str_replace(',', '', $_temp1);
		$_temp2 = str_replace(',', '', $_temp2);
		//
		if($_tipo2 == 'Refrigeradora') $_tipo2 = '0';
		else $_tipo2 = '1';
		//
		if($_accion == 'I') $_cs = $this->CartaConsecutivoGet($tipo);
		//
		if($this->_TRANS("EXECUTE PA_MAG_074 '{$_cs}', 
			'{$tipo}', 
			'{$_LID}', 
			'{$_UID}', 
			'{$_cod_cam}', 
			'{$_cod_ter}',
			'{$_limites[0]}', 
			'{$_limites[1]}', 
			'{$_tipo2}',
			'{$_ubicacion}',  
			'{$_inc1}','{$_ind1}','{$_factor1}',
			'{$_inc2}','{$_ind2}','{$_factor2}',
			'{$_inc3}','{$_ind3}','{$_factor3}',
			'{$_inc4}','{$_ind4}','{$_factor4}',
			'{$_inc5}','{$_ind5}','{$_factor5}',
			'{$_fecha}',
			'{$_hora}',
			'{$_temp1}',
			'{$_temp2}',
			'{$_accion}';")){
						
			$this->CartaConsecutivoSet($tipo);
			return $_cs;
		}else return 0;
	}
	
	function Carta6DetalleGet($_cs){
		return $this->_QUERY("SELECT blanco, abs1, abs2, abs3, abs4, abs5
		FROM CAR015 WHERE (carta = '{$_cs}') ORDER BY linea;");
	}
	
	function Carta6DetalleVacio(){
		return array(0=>array(
			'blanco'=>'0.00',
			'abs1'=>'0.00',
			'abs2'=>'0.00',
			'abs3'=>'0.00',
			'abs4'=>'0.00',
			'abs5'=>'0.00')
		);
	}
	
	function Carta6EncabezadoGet($_cs){
		return $this->_QUERY("SELECT CAR000.consecutivo AS cs, CONVERT(VARCHAR, CAR000.fecha, 105) AS fecha, CAR000.estado,CAR000.codigoalterno, CAR013.modalidad, CAR013.equipo AS cod_bal, CAR013.nombre, CAR013.corriente, CAR013.origen, CAR013.longitud, CAR013.tiene, CAR013.ancho, 
        CAR013.conc, EQU000.codigo AS nom_bal, (EQU000.marca + ' / ' + EQU000.modelo) AS marca
		FROM CAR013 INNER JOIN CAR000 ON CAR013.codigo = CAR000.consecutivo INNER JOIN EQU000 ON CAR013.equipo = EQU000.id
		WHERE (CAR000.consecutivo = '{$_cs}');");
	}
	
	function Carta6EncabezadoVacio(){
		return array(0=>array(
			'cs'=>'',
			'modalidad'=>'',
			'fecha'=>'',
			'estado'=>'',
                        'codigoalterno'=>'',
			'nom_bal'=>'',
			'cod_bal'=>'',
			'nombre'=>'',
			'corriente'=>'',
			'origen'=>'',
			'longitud'=>'',
			'tiene'=>'',
			'ancho'=>'',
			'conc'=>'',
			'marca'=>'')
		);
	}
	
	function Carta6Paso1($_cs, $_fecha, $_accion, $_modalidad, $_equipo, $_nombre, $_corriente, $_origen, $_longitud, $_tiene, $_ancho, $_conc, $_volA, $_volB, $_LID, $_UID){
		$tipo = 6;
		$_conc = str_replace(',', '', $_conc);		
		$_volA = str_replace(',', '', $_volA);
		$_volB = str_replace(',', '', $_volB);
		$_volA = explode('&', substr($_volA, 1));
		$_volB = explode('&', substr($_volB, 1));
                $_fecha = $this->_FECHA($_fecha);
		//
		if($_accion == 'I') $_cs = $this->CartaConsecutivoGet($tipo);

		if($this->_TRANS("EXECUTE PA_MAG_079 '{$_cs}', 
			'{$tipo}',
			'{$_modalidad}',
			'{$_equipo}', 
			'{$_nombre}', 
			'{$_corriente}', 
			'{$_origen}', 
			'{$_longitud}', 
			'{$_tiene}', 
			'{$_ancho}', 
			'{$_conc}', 
			'{$_LID}', 
			'{$_UID}', 
			'{$_accion}',
                        '{$_fecha}';")){
			
			//INGRESA VOLUMENES
			$this->_TRANS("DELETE FROM CAR014 WHERE (carta = '{$_cs}');");		
			for($i=1;$i<count($_volA);$i++){
				if(str_replace(' ', '', $_volA[$i]) != ''){
					$this->_TRANS("INSERT INTO CAR014 values('{$_cs}', {$i}, {$_volA[$i]}, {$_volB[$i]});");					
				}
			}
			
			$this->CartaConsecutivoSet($tipo);

			return $_cs;
		}else return 0;
	}
	
	function Carta6Paso2($_cs, $_blanco, $_abs1, $_abs2, $_abs3, $_abs4, $_abs5){
		$_blanco = str_replace(',', '', $_blanco);
		$_abs1 = str_replace(',', '', $_abs1);
		$_abs2 = str_replace(',', '', $_abs2);
		$_abs3 = str_replace(',', '', $_abs3);
		$_abs4 = str_replace(',', '', $_abs4);
		$_abs5 = str_replace(',', '', $_abs5);

		$_blanco = explode('&', substr($_blanco, 1));
		$_abs1 = explode('&', substr($_abs1, 1));
		$_abs2 = explode('&', substr($_abs2, 1));
		$_abs3 = explode('&', substr($_abs3, 1));
		$_abs4 = explode('&', substr($_abs4, 1));
		$_abs5 = explode('&', substr($_abs5, 1));
		
		$this->_TRANS("DELETE FROM CAR015 WHERE (carta = '{$_cs}');");		
		for($i=1;$i<count($_abs5);$i++){
			if(str_replace(' ', '', $_abs5[$i]) != ''){
                            if(isset($_blanco[$i])){
				$this->_TRANS("INSERT INTO CAR015 values('{$_cs}', {$i}, {$_blanco[$i]}, {$_abs1[$i]}, {$_abs2[$i]}, {$_abs3[$i]}, {$_abs4[$i]}, {$_abs5[$i]});");
                            }else{
                                $this->_TRANS("INSERT INTO CAR015 values('{$_cs}', {$i}, '', '', '', '', '', {$_abs5[$i]});");
                            }
			}
		}

		return 1;
	}
	
	function Carta6VolumenesGet($_cs){
		return $this->_QUERY("SELECT volA, volB
		FROM CAR014 WHERE (carta = '{$_cs}') ORDER BY linea;");
	}
	
	function Carta6VolumenesVacio(){
		return array(0=>array('volA'=>'0.00','volB'=>'0.00','volC'=>'0.00','volP'=>'0.00'),
		1=>array('volA'=>'0.00','volB'=>'0.00','volC'=>'0.00','volP'=>'0.00'),
		2=>array('volA'=>'0.00','volB'=>'0.00','volC'=>'0.00','volP'=>'0.00'),
		3=>array('volA'=>'0.00','volB'=>'0.00','volC'=>'0.00','volP'=>'0.00'),
		4=>array('volA'=>'0.00','volB'=>'0.00','volC'=>'0.00','volP'=>'0.00'),
		5=>array('volA'=>'0.00','volB'=>'0.00','volC'=>'0.00','volP'=>'0.00')
		);
	}
	
	function Carta7DetalleGet($_cs){
		return $this->_QUERY("SELECT t, mass
		FROM CAR017 WHERE (carta = '{$_cs}') ORDER BY linea");
	}
	
	function Carta7DetalleVacio(){
		return array(0=>array(
			't'=>'0',
			'mass'=>'0')
		);
	}
	
	function Carta7EncabezadoGet($_cs){
		return $this->_QUERY("SELECT '{$_cs}' AS cs, CONVERT(VARCHAR, CAR000.fecha, 105) AS fecha, CAR000.estado,CAR000.codigoalterno, CAR016.nombre, CAR016.masa, CAR016.origen, CAR016.aforado, CAR016.tiene, 
        CAR016.pureza, CAR016.cod_bal, CAR016.inyeccion, CAR016.tipo_bomba, CAR016.tipo_inyector, CAR016.eluente, CAR016.temp1, CAR016.modo, CAR016.canalA, CAR016.flujoA, CAR016.presiA, CAR016.canalB, CAR016.flujoB, CAR016.presiB, CAR016.canalC, CAR016.flujoC, CAR016.presiC, CAR016.canalD, CAR016.flujoD, CAR016.presiD, CAR016.tipo_col, CAR016.cod_col, 
        CONVERT(TEXT, CAR016.marcafase) AS marcafase, CONVERT(VARCHAR, CAR016.fecha_em, 105) AS fecha_em, CAR016.tipo_detector, CAR016.temp2, CAR016.temp3, CONVERT(TEXT, CAR016.dimensiones) AS dimensiones, CAR016.longitud, CONVERT(TEXT, CAR016.otros) AS otros, CAR016.bar, CAR016.pat_ali, CAR016.pat_afo, EQU000.codigo AS nom_bal, (EQU000.marca + ' / ' + EQU000.modelo) AS marca
		FROM CAR000 INNER JOIN CAR016 ON CAR000.consecutivo = CAR016.codigo INNER JOIN EQU000 ON CAR016.cod_bal = EQU000.id
		WHERE (CAR000.consecutivo = '{$_cs}');");
	}
	
	function Carta7EncabezadoVacio(){
		return array(0=>array(
			'cs'=>'',
                        'codigoalterno'=>'',
			'fecha'=>'',
			'estado'=>'',
			'nombre'=>'',
			'masa'=>'',
			'origen'=>'',
			'aforado'=>'',
			'tiene'=>'',
			'pureza'=>'',
			'nom_bal'=>'',
			'cod_bal'=>'',
			'marca'=>'',
			'inyeccion'=>'0',
			'tipo_bomba'=>'',
			'tipo_inyector'=>'',
			'eluente'=>'',
			'temp1'=>'0',
			'modo'=>'',
			'canalA'=>'',
			'flujoA'=>'0',
			'presiA'=>'0',
			'canalB'=>'',
			'flujoB'=>'0',
			'presiB'=>'0',
			'canalC'=>'',
			'flujoC'=>'0',
			'presiC'=>'0',
			'canalD'=>'',
			'flujoD'=>'0',
			'presiD'=>'0',
			'tipo_col'=>'',
			'cod_col'=>'',
			'marcafase'=>'',
			'fecha_em'=>'',
			'tipo_detector'=>'',
			'temp2'=>'0',
			'temp3'=>'0',
			'dimensiones'=>'',
			'longitud'=>'0',
			'otros'=>'',
			'bar'=>'0',
			'pat_ali'=>'',
			'pat_afo'=>'')
		);
	}
	
	function Carta7LinealidadGet($_cs){
		return $this->_QUERY("SELECT vol_ali, vol_afo, area
		FROM CAR018 WHERE (carta = '{$_cs}') ORDER BY linea");
	}
	
	function Carta7LinealidadVacio(){
		return array(0=>array('vol_ali'=>'0.00','vol_afo'=>'0.00','area'=>'0.00'),
		1=>array('vol_ali'=>'0.00','vol_afo'=>'0.00','area'=>'0.00'),
		2=>array('vol_ali'=>'0.00','vol_afo'=>'0.00','area'=>'0.00'),
		3=>array('vol_ali'=>'0.00','vol_afo'=>'0.00','area'=>'0.00')
		);
	}
	
	function Carta7Paso1($_cs, $_fecha, $_accion, $_nombre, $_masa, $_origen, $_aforado, $_tiene, $_pureza, $_cod_bal, $_inyeccion, $_tipo_bomba, $_tipo_inyector, $_eluente, $_temp1, $_modo, $_canalA, $_flujoA, $_presiA, $_canalB, $_flujoB, $_presiB, $_canalC, $_flujoC, $_presiC, $_canalD, $_flujoD, $_presiD, $_tipo_col, $_cod_col, $_marcafase, $_fecha_em, $_tipo_detector, $_temp2, $_temp3, $_dimensiones, $_longitud, $_otros, $_bar, $_pat_ali, $_pat_afo, $_LID, $_UID){
		$_fecha_em = $this->_FECHA($_fecha_em);
		$tipo = 7;
		$_nombre = $this->Free($_nombre);
		$_origen = $this->Free($_origen);
		$_eluente = $this->Free($_eluente);
		$_cod_col = $this->Free($_cod_col);
		$_marcafase = $this->Free($_marcafase);
		$_dimensiones = $this->Free($_dimensiones);
		$_otros = $this->Free($_otros);
		//
		$_masa = str_replace(',', '', $_masa);
		$_pureza = str_replace(',', '', $_pureza);
		$_inyeccion = str_replace(',', '', $_inyeccion);
		$_temp1 = str_replace(',', '', $_temp1);
		$_flujoA = str_replace(',', '', $_flujoA);$_presiA = str_replace(',', '', $_presiA);
		$_flujoB = str_replace(',', '', $_flujoB);$_presiB = str_replace(',', '', $_presiB);
		$_flujoC = str_replace(',', '', $_flujoC);$_presiC = str_replace(',', '', $_presiC);
		$_flujoD = str_replace(',', '', $_flujoD);$_presiD = str_replace(',', '', $_presiD);
		$_temp2 = str_replace(',', '', $_temp2);
		$_temp3 = str_replace(',', '', $_temp3);
		$_longitud = str_replace(',', '', $_longitud);
		$_bar = str_replace(',', '', $_bar);
		//
		if($_accion == 'I') $_cs = $this->CartaConsecutivoGet($tipo);
		
		if($this->_TRANS("EXECUTE PA_MAG_080 '{$_cs}', '{$_fecha}', 
			'{$tipo}', 
			'{$_nombre}', 
			'{$_masa}',
			'{$_origen}', 
			'{$_aforado}',
			'{$_tiene}',
			'{$_pureza}',
			'{$_cod_bal}',
			'{$_inyeccion}', 
			'{$_tipo_bomba}',
			'{$_tipo_inyector}', 
			'{$_eluente}',
			'{$_temp1}',
			'{$_modo}',
			'{$_canalA}', '{$_flujoA}', '{$_presiA}', 
			'{$_canalB}', '{$_flujoB}', '{$_presiB}', 
			'{$_canalC}', '{$_flujoC}', '{$_presiC}', 
			'{$_canalD}', '{$_flujoD}', '{$_presiD}', 
			'{$_tipo_col}', 
			'{$_cod_col}',
			'{$_marcafase}', 
			'{$_fecha_em}',
			'{$_tipo_detector}', 
			'{$_temp2}',
			'{$_temp3}',
			'{$_dimensiones}', 
			'{$_longitud}',
			'{$_otros}',
			'{$_bar}',
			'{$_pat_ali}', 
			'{$_pat_afo}',
			'{$_LID}',
			'{$_UID}',
			'{$_accion}';")){
			
			$this->CartaConsecutivoSet($tipo);
			
			//$this->Logger("{$_tipo}{$_accion}");
			return $_cs;
		}else return 0;
	}
	
	function Carta7Paso2($_cs, $_t, $_mass){
		$_t = str_replace(',', '', $_t);
		$_mass = str_replace(',', '', $_mass);

		$_t = explode('&', substr($_t, 1));
		$_mass = explode('&', substr($_mass, 1));
		
		$this->_TRANS("DELETE FROM CAR017 WHERE (carta = '{$_cs}');");		
		for($i=1;$i<count($_t);$i++){
			if(str_replace(' ', '', $_t[$i]) != ''){
				$this->_TRANS("INSERT INTO CAR017 values('{$_cs}', {$i}, {$_t[$i]}, {$_mass[$i]});");					
			}
		}

		return $_cs;
	}
	
	function Carta7Paso3($_cs, $_vol_ali, $_vol_afo, $_area){
		$_vol_ali = str_replace(',', '', $_vol_ali);
		$_vol_afo = str_replace(',', '', $_vol_afo);
		$_area = str_replace(',', '', $_area);

		$_vol_ali = explode('&', substr($_vol_ali, 1));
		$_vol_afo = explode('&', substr($_vol_afo, 1));
		$_area = explode('&', substr($_area, 1));
		
		$this->_TRANS("DELETE FROM CAR018 WHERE (carta = '{$_cs}');");		
		for($i=1;$i<count($_vol_ali);$i++){
			if(str_replace(' ', '', $_vol_ali[$i]) != ''){
				$this->_TRANS("INSERT INTO CAR018 values('{$_cs}', {$i}, {$_vol_ali[$i]}, {$_vol_afo[$i]}, {$_area[$i]});");					
			}
		}

		return $_cs;
	}
	
	function Carta7Paso4($_cs, $_are, $_tr, $_alt){
		$_are = str_replace(',', '', $_are);
		$_tr = str_replace(',', '', $_tr);
		$_alt = str_replace(',', '', $_alt);

		$_are = explode('&', substr($_are, 1));
		$_tr = explode('&', substr($_tr, 1));
		$_alt = explode('&', substr($_alt, 1));
		
		$this->_TRANS("DELETE FROM CAR019 WHERE (carta = '{$_cs}');");		
		for($i=1;$i<count($_are);$i++){
			if(str_replace(' ', '', $_are[$i]) != ''){
				$this->_TRANS("INSERT INTO CAR019 values('{$_cs}', {$i}, {$_are[$i]}, {$_tr[$i]}, {$_alt[$i]});");					
			}
		}

		return 1;
	}
	
	function Carta7ReproGet($_cs){
		return $this->_QUERY("SELECT are, tr, alt
		FROM CAR019 WHERE (carta = '{$_cs}')
		ORDER BY linea;");
	}
	
	function Carta7ReproVacio(){
		return array(0=>array(
			'are'=>'0',
			'tr'=>'0',
			'alt'=>'0')
		);
	}
	
	function Carta8EncabezadoGet($_cs){
		return $this->_QUERY("SELECT '{$_cs}' AS cs, CONVERT(VARCHAR, CAR000.fecha, 105) AS fecha, CAR000.estado,CAR000.codigoalterno, CAR020.nombre, CAR020.masa, CAR020.origen, CAR020.aforado, CAR020.tiene, CAR020.pureza, CAR020.cod_bal, CAR020.inyeccion, CAR020.tipo_inyector, CAR020.temp1, CAR020.modo, CAR020.tipo_col, CAR020.gas1, CAR020.flujo1, CAR020.presion1, CAR020.gas2, CAR020.flujo2, CAR020.presion2, CAR020.gas3, CAR020.flujo3, CAR020.presion3, CAR020.cod_col, 
        CONVERT(TEXT, CAR020.marcafase) AS marcafase, CONVERT(VARCHAR, CAR020.fecha_em, 105) AS fecha_em, CAR020.tipo_detector, CAR020.temp2, CAR020.temp3, CONVERT(TEXT, CAR020.dimensiones) AS dimensiones, CONVERT(TEXT, CAR020.otros) AS otros, CAR020.gas4, CAR020.flujo4, CAR020.presion4, EQU000.codigo AS nom_bal, (EQU000.marca + ' / ' + EQU000.modelo) AS marca
		FROM CAR000 INNER JOIN CAR020 ON CAR000.consecutivo = CAR020.codigo INNER JOIN EQU000 ON CAR020.cod_bal = EQU000.id
		WHERE (CAR000.consecutivo = '{$_cs}');");
	}
	
	function Carta8EncabezadoVacio(){
		return array(0=>array(
			'cs'=>'',
                        'codigoalterno'=>'',
			'fecha'=>'',
			'estado'=>'',
			'nombre'=>'',
			'masa'=>'',
			'origen'=>'',
			'aforado'=>'',
			'tiene'=>'',
			'pureza'=>'',
			'nom_bal'=>'',
			'cod_bal'=>'',
			'marca'=>'',
			'inyeccion'=>'0',
			'tipo_inyector'=>'',
			'temp1'=>'0',
			'modo'=>'',
			'cod_col'=>'',
			'tipo_col'=>'',
			'gas1'=>'',
			'gas2'=>'',
			'gas3'=>'',
			'gas4'=>'',
			'flujo1'=>'0',
			'flujo2'=>'0',
			'flujo3'=>'0',
			'flujo4'=>'0',
			'presion1'=>'0',
			'presion2'=>'0',
			'presion3'=>'0',
			'presion4'=>'0',
			'marcafase'=>'',
			'fecha_em'=>'',
			'tipo_detector'=>'',
			'temp2'=>'0',
			'temp3'=>'0',
			'dimensiones'=>'',
			'otros'=>'')
		);
	}
	
	function Carta8LinealidadGet($_cs){
		return $this->_QUERY("SELECT vol_ali, vol_afo, area
		FROM CAR021 WHERE (carta = '{$_cs}') ORDER BY linea");
	}
	
	function Carta8Paso1($_cs, $_fecha, $_accion, $_nombre, $_masa, $_origen, $_aforado, $_tiene, $_pureza, $_cod_bal, $_tipo_inyector, $_inyeccion, $_temp1, $_modo, $_tipo_col, $_gas1, $_flujo1, $_presion1, $_gas2, $_flujo2, $_presion2, $_gas3, $_flujo3, $_presion3, $_gas4, $_flujo4, $_presion4, $_cod_col, $_marcafase, $_fecha_em, $_temp2, $_tipo_detector, $_dimensiones, $_temp3, $_otros, $_LID, $_UID){
		$_fecha_em = $this->_FECHA($_fecha_em);
		$tipo = 8;
		$_nombre = $this->Free($_nombre);
		$_origen = $this->Free($_origen);
		$_cod_col = $this->Free($_cod_col);
		$_marcafase = $this->Free($_marcafase);
		$_dimensiones = $this->Free($_dimensiones);
		$_otros = $this->Free($_otros);
		//
		$_masa = str_replace(',', '', $_masa);
		$_pureza = str_replace(',', '', $_pureza);
		$_inyeccion = str_replace(',', '', $_inyeccion);
		$_temp1 = str_replace(',', '', $_temp1);
		$_presion1 = str_replace(',', '', $_presion1);$_gas1 = str_replace(',', '', $_gas1);
		$_presion2 = str_replace(',', '', $_presion2);$_gas2 = str_replace(',', '', $_gas2);
		$_presion3 = str_replace(',', '', $_presion3);$_gas3 = str_replace(',', '', $_gas3);
		$_temp2 = str_replace(',', '', $_temp2);
		$_temp3 = str_replace(',', '', $_temp3);
		//
		if($_accion == 'I') $_cs = $this->CartaConsecutivoGet($tipo);
		
		if($this->_TRANS("EXECUTE PA_MAG_081 '{$_cs}', 
			'{$_fecha}', 
			'{$tipo}', 
			'{$_nombre}', 
			'{$_masa}',
			'{$_origen}', 
			'{$_aforado}',
			'{$_tiene}',
			'{$_pureza}',
			'{$_cod_bal}',
			'{$_tipo_inyector}', 
			'{$_inyeccion}', 
			'{$_temp1}',
			'{$_modo}',
			'{$_tipo_col}',
			'{$_gas1}', 
			'{$_flujo1}',
			'{$_presion1}',
			'{$_gas2}', 
			'{$_flujo2}',
			'{$_presion2}',
			'{$_gas3}', 
			'{$_flujo3}',
			'{$_presion3}',
			'{$_gas4}', 
			'{$_flujo4}',
			'{$_presion4}', 
			'{$_cod_col}',
			'{$_marcafase}', 
			'{$_fecha_em}',
			'{$_temp2}',
			'{$_tipo_detector}', 
			'{$_dimensiones}', 
			'{$_temp3}',
			'{$_otros}',
			'{$_LID}',
			'{$_UID}',
			'{$_accion}';")){
			
			$this->CartaConsecutivoSet($tipo);
			
			//$this->Logger("{$_tipo}{$_accion}");
			return $_cs;
		}else return 0;
	}
	
	function Carta8Paso3($_cs, $_vol_ali, $_vol_afo, $_area){
		$_vol_ali = str_replace(',', '', $_vol_ali);
		$_vol_afo = str_replace(',', '', $_vol_afo);
		$_area = str_replace(',', '', $_area);

		$_vol_ali = explode('&', substr($_vol_ali, 1));
		$_vol_afo = explode('&', substr($_vol_afo, 1));
		$_area = explode('&', substr($_area, 1));
		
		$this->_TRANS("DELETE FROM CAR021 WHERE (carta = '{$_cs}');");		
		for($i=1;$i<count($_vol_ali);$i++){
			if(str_replace(' ', '', $_vol_ali[$i]) != ''){
				$this->_TRANS("INSERT INTO CAR021 values('{$_cs}', {$i}, {$_vol_ali[$i]}, {$_vol_afo[$i]}, {$_area[$i]});");
			}
		}

		return $_cs;
	}
	
	function Carta8Paso4($_cs, $_are, $_tr, $_alt){
		$_are = str_replace(',', '', $_are);
		$_tr = str_replace(',', '', $_tr);
		$_alt = str_replace(',', '', $_alt);

		$_are = explode('&', substr($_are, 1));
		$_tr = explode('&', substr($_tr, 1));
		$_alt = explode('&', substr($_alt, 1));
		
		$this->_TRANS("DELETE FROM CAR022 WHERE (carta = '{$_cs}');");		
		for($i=1;$i<count($_are);$i++){
			if(str_replace(' ', '', $_are[$i]) != ''){
				$this->_TRANS("INSERT INTO CAR022 values('{$_cs}', {$i}, {$_are[$i]}, {$_tr[$i]}, {$_alt[$i]});");					
			}
		}

		return 1;
	}
	
	function Carta8ReproGet($_cs){
		return $this->_QUERY("SELECT are, tr, alt
		FROM CAR022 WHERE (carta = '{$_cs}')
		ORDER BY linea;");
	}
	
	function Carta9BarridoScanGet($_cs){
		$ROW = $this->_QUERY("SELECT tr, resol, qual, asim, platos
		FROM CAR026 WHERE (carta = '{$_cs}')
		ORDER BY senal;");
		if(!$ROW)
			return array(
				0=>array('tr'=>'0.00','resol'=>'0.00','qual'=>'0','asim'=>'0.00','platos'=>'0.00'),
				1=>array('tr'=>'0.00','resol'=>'0.00','qual'=>'0','asim'=>'0.00','platos'=>'0.00'),
				2=>array('tr'=>'0.00','resol'=>'0.00','qual'=>'0','asim'=>'0.00','platos'=>'0.00'),
				3=>array('tr'=>'0.00','resol'=>'0.00','qual'=>'0','asim'=>'0.00','platos'=>'0.00'),
				4=>array('tr'=>'0.00','resol'=>'0.00','qual'=>'0','asim'=>'0.00','platos'=>'0.00'),
				5=>array('tr'=>'0.00','resol'=>'0.00','qual'=>'0','asim'=>'0.00','platos'=>'0.00'),
				6=>array('tr'=>'0.00','resol'=>'0.00','qual'=>'0','asim'=>'0.00','platos'=>'0.00'),
				7=>array('tr'=>'0.00','resol'=>'0.00','qual'=>'0','asim'=>'0.00','platos'=>'0.00'),
				8=>array('tr'=>'0.00','resol'=>'0.00','qual'=>'0','asim'=>'0.00','platos'=>'0.00'),
				9=>array('tr'=>'0.00','resol'=>'0.00','qual'=>'0','asim'=>'0.00','platos'=>'0.00'),
				10=>array('tr'=>'0.00','resol'=>'0.00','qual'=>'0','asim'=>'0.00','platos'=>'0.00'),
				11=>array('tr'=>'0.00','resol'=>'0.00','qual'=>'0','asim'=>'0.00','platos'=>'0.00')
			);
		else return $ROW;
	}
	
	function Carta9BarridoSimGet($_cs){
		$ROW = $this->_QUERY("SELECT tr, area
		FROM CAR027 WHERE (carta = '{$_cs}')
		ORDER BY corrida;");
		if(!$ROW)
			return array(
				0=>array('tr'=>'0.00','area'=>'0'),
				1=>array('tr'=>'0.00','area'=>'0'),
				2=>array('tr'=>'0.00','area'=>'0'),
				3=>array('tr'=>'0.00','area'=>'0'),
				4=>array('tr'=>'0.00','area'=>'0'),
				5=>array('tr'=>'0.00','area'=>'0'),
				6=>array('tr'=>'0.00','area'=>'0'),
				7=>array('tr'=>'0.00','area'=>'0'),
				8=>array('tr'=>'0.00','area'=>'0'),
				9=>array('tr'=>'0.00','area'=>'0'),
				10=>array('tr'=>'0.00','area'=>'0'),
				11=>array('tr'=>'0.00','area'=>'0'),
				12=>array('tr'=>'0.00','area'=>'0'),
				13=>array('tr'=>'0.00','area'=>'0'),
				14=>array('tr'=>'0.00','area'=>'0'),
				15=>array('tr'=>'0.00','area'=>'0'),
				16=>array('tr'=>'0.00','area'=>'0'),
				17=>array('tr'=>'0.00','area'=>'0'),
				18=>array('tr'=>'0.00','area'=>'0'),
				19=>array('tr'=>'0.00','area'=>'0'),
				20=>array('tr'=>'0.00','area'=>'0')
			);
		else return $ROW;
	}
	
	function Carta9ChequeoGet($_cs){
		$ROW = $this->_QUERY("SELECT v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15
		FROM CAR023 WHERE (codigo = '{$_cs}');");
		if(!$ROW)
			return array(0=>array(
				'v1'=>'',
				'v2'=>'',
				'v3'=>'',
				'v4'=>'',
				'v5'=>'',			
				'v6'=>'',
				'v7'=>'',
				'v8'=>'',
				'v9'=>'',
				'v10'=>'',
				'v11'=>'',
				'v12'=>'',
				'v13'=>'',
				'v14'=>'',
				'v15'=>'')
			);
			
		else return $ROW;
	}
	
	function Carta9EncabezadoGet($_cs){
		$ROW = $this->_QUERY("SELECT '{$_cs}' AS cs, CONVERT(VARCHAR, CAR000.fecha, 105) AS fecha, CAR000.estado, CAR023.equipo, CAR023.connominal, EQU000.codigo AS nomequipo, (EQU000.marca + ' / ' + EQU000.modelo) AS marca
		FROM CAR000 INNER JOIN CAR023 ON CAR000.consecutivo = CAR023.codigo INNER JOIN EQU000 ON CAR023.equipo = EQU000.id
		WHERE (CAR000.consecutivo = '{$_cs}');");
		//
		$ROW2 = $this->_QUERY("SELECT INV002.id, INV002.codigo, INV002.nombre
		FROM CAR024 INNER JOIN INV002 ON CAR024.articulo = INV002.id
		WHERE (CAR024.carta = '{$_cs}')
		ORDER BY CAR024.tipo");
		//
		$ROW[0]['idcolumna'] = $ROW2[0]['id'];
		$ROW[0]['codcolumna'] = $ROW2[0]['codigo'];
		$ROW[0]['nomcolumna'] = $ROW2[0]['nombre'];
		$ROW[0]['idestandar'] = $ROW2[1]['id'];
		$ROW[0]['codestandar'] = $ROW2[1]['codigo'];
		$ROW[0]['nomestandar'] = $ROW2[1]['nombre'];
		//
		return $ROW;
	}
	
	function Carta9EncabezadoVacio(){
		return array(0=>array(
			'cs'=>'',
			'fecha'=>'',
			'estado'=>'',
			'marca'=>'',
			'id'=>'',
			'nomequipo'=>'',
			'equipo'=>'',
			'nomcolumna'=>'',
			'idcolumna'=>'',
			'codcolumna'=>'',
			'nomestandar'=>'',
			'idestandar'=>'',
			'codestandar'=>'',
			'connominal'=>'',
			'usuario'=>'')
		);
	}
	
	function Carta9Involucra($_cs, $_usuario){
		$this->_TRANS("DELETE FROM CAR025 WHERE (carta = '{$_cs}') AND (usuario = {$_usuario});");
		$this->_TRANS("INSERT INTO CAR025 VALUES('{$_cs}', {$_usuario});");
		return 1;
	}
	
	function Carta9LinealidadGet($_cs){
		$ROW = $this->_QUERY("SELECT tr, area, conc
		FROM CAR028 WHERE (carta = '{$_cs}')
		ORDER BY linea;");
		if(!$ROW)
			return array(
				0=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				1=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				2=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				3=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				4=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				5=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				6=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				7=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				8=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				9=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				10=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				11=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				12=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				13=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				14=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				15=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				16=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				17=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				18=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				19=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				20=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				21=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				22=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				23=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				24=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				25=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				26=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				27=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				28=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				29=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				30=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				31=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				32=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				33=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				34=>array('tr'=>'0','area'=>'0','conc'=>'0'),
				35=>array('tr'=>'0','area'=>'0','conc'=>'0')
			);
		else return $ROW;
	}
	
	function Carta9OtrosGet($_cs){
		$ROW = $this->_QUERY("SELECT vresol, vasimetria, vresol2, vasimetria2, dimensiones, coef, CONVERT(TEXT, obs) AS obs,cumple4
		FROM CAR023 WHERE (codigo = '{$_cs}');");
		if(!$ROW)
			return array(0=>array(
				'vresol'=>'0.00',
				'vasimetria'=>'0.00',
				'vresol2'=>'0.00',
				'vasimetria2'=>'0.00',
				'dimensiones'=>'',
				'coef'=>'',	
				'obs'=>'',
                                'cumple4'=>''
                            )
			);
		else return $ROW;
	}
	
	function Carta9Paso1($_cs, $_accion, $_fecha, $_equipo, $_idColumna, $_idEstandar, $_connominal, $_LID, $_UID){
		$tipo = 9;
		$_connominal = $this->Free($_connominal);
                $_fecha=$this->_FECHA2($_fecha);
		//
                if($_accion == 'I'){ $_cs = $this->CartaConsecutivoGet($tipo);} 
		
		$this->_TRANS("EXECUTE PA_MAG_082 '{$_cs}', 
			'{$tipo}', 
			'{$_equipo}', 
			'{$_idColumna}',
			'{$_idEstandar}',
			'{$_connominal}',
			'{$_LID}',
			'{$_UID}',
			'{$_accion}',
                        '{$_fecha}';");
			
			$this->CartaConsecutivoSet($tipo);
			
			return $_cs;
	}
	
	function Carta9Paso2($_cs,$_v1,$_v2,$_v3,$_v4,$_v5,$_v6,$_v7,$_v8,$_v9,$_v10,$_v11,$_v12,$_v13,$_v14,$_v15,$_UID){
		$_v1 = $this->Free($_v1);
		$_v2 = $this->Free($_v2);
		$_v3 = $this->Free($_v3);
		$_v9 = $this->Free($_v9);
		//
		$_v4 = str_replace(',', '', $_v4);
		$_v5 = str_replace(',', '', $_v5);
		$_v6 = str_replace(',', '', $_v6);
		$_v7 = str_replace(',', '', $_v7);
		$_v8 = str_replace(',', '', $_v8);
		$_v10 = str_replace(',', '', $_v10);
		$_v11 = str_replace(',', '', $_v11);
		$_v12 = str_replace(',', '', $_v12);
		$_v13 = str_replace(',', '', $_v13);
		$_v14 = str_replace(',', '', $_v14);
		$_v15 = str_replace(',', '', $_v15);
		//ACTUALIZA INFO
		$this->_TRANS("UPDATE CAR023 SET v1='{$_v1}',v2='{$_v2}',v3='{$_v3}',v4='{$_v4}',v5='{$_v5}',v6='{$_v6}',v7='{$_v7}',v8='{$_v8}',v9='{$_v9}',v10='{$_v10}',v11='{$_v11}',v12='{$_v12}',v13='{$_v13}',v14='{$_v14}',v15='{$_v15}'
		WHERE (codigo = '{$_cs}');");
		
		return 1;
	}
	
	function Carta9Paso3($_cs, $_dimensiones, $_tr, $_resol, $_qual, $_asim, $_platos,$_cumple4, $_UID){
		$_dimensiones = $this->Free($_dimensiones);
		//
		$_tr = str_replace(',', '', $_tr);
		$_resol = str_replace(',', '', $_resol);
		$_qual = str_replace(',', '', $_qual);
		$_asim = str_replace(',', '', $_asim);
		$_platos = str_replace(',', '', $_platos);

		$_tr = explode('&', substr($_tr, 1));
		$_resol = explode('&', substr($_resol, 1));
		$_qual = explode('&', substr($_qual, 1));
		$_asim = explode('&', substr($_asim, 1));
		$_platos = explode('&', substr($_platos, 1));
		
		//ACTUALIZA INFO
		$this->_TRANS("UPDATE CAR023 SET dimensiones='{$_dimensiones}',cumple4='{$_cumple4}' WHERE (codigo = '{$_cs}');");
		//ACTUALIZA DETALLE
		$this->_TRANS("DELETE FROM CAR026 WHERE (carta = '{$_cs}');");		
		for($i=1;$i<count($_tr);$i++){
			if(str_replace(' ', '', $_tr[$i]) != ''){
				$this->_TRANS("INSERT INTO CAR026 values('{$_cs}', {$i}, {$_tr[$i]}, {$_resol[$i]}, {$_qual[$i]}, {$_asim[$i]}, {$_platos[$i]});");					
			}
		}

		return 1;
	}
	
	function Carta9Paso4($_cs, $_vresol, $_vasimetria, $_vresol2, $_vasimetria2, $_tr, $_area, $_UID){
		$_vresol = str_replace(',', '', $_vresol);
		$_vasimetria = str_replace(',', '', $_vasimetria);
		$_vresol2 = str_replace(',', '', $_vresol2);
		$_vasimetria2 = str_replace(',', '', $_vasimetria2);
		
                $_tr = str_replace('1=1&', '', $_tr);
                $_tr = explode('&', $_tr);

                $_area = str_replace('1=1&', '', $_area);
                $_area = explode('&', $_area);


        //ACTUALIZA INFO
		$this->_TRANS("UPDATE CAR023 SET vresol={$_vresol}, vasimetria={$_vasimetria}, vresol2={$_vresol2}, vasimetria2={$_vasimetria2} WHERE (codigo = '{$_cs}');");
		//ACTUALIZA DETALLE
		$this->_TRANS("DELETE FROM CAR027 WHERE (carta = '{$_cs}');");		
		for($i=0;$i<count($_tr);$i++){
			if($_tr[$i] != ''){
				$this->_TRANS("INSERT INTO CAR027 values('{$_cs}', {$i}, {$_tr[$i]}, {$_area[$i]});");					
			}
		}

		return 1;
	}
	
	function Carta9Paso5($_cs, $_coef, $_obs, $_tr, $_area, $_conc, $_UID){
		$_obs = $this->Free($_obs);
		$_tr = str_replace(',', '', $_tr);
		$_area = str_replace(',', '', $_area);
		$_conc = str_replace(',', '', $_conc);

		$_tr = explode('&', substr($_tr, 1));
		$_area = explode('&', substr($_area, 1));
		$_conc = explode('&', substr($_conc, 1));
		
		//ACTUALIZA INFO
		$this->_TRANS("UPDATE CAR023 SET coef={$_coef}, obs='{$_obs}' WHERE (codigo = '{$_cs}');");
		//ACTUALIZA DETALLE
		$this->_TRANS("DELETE FROM CAR028 WHERE (carta = '{$_cs}');");		
		for($i=1;$i<count($_tr);$i++){
			if(str_replace(' ', '', $_tr[$i]) != ''){
				$this->_TRANS("INSERT INTO CAR028 values('{$_cs}', {$i}, {$_tr[$i]}, {$_area[$i]}, {$_conc[$i]});");					
			}
		}

		return 1;
	}
	
	function CartaActualiza($_cs, $_UID, $_accion){
		return $this->_TRANS("EXECUTE PA_MAG_083 '{$_cs}', '{$_UID}', '{$_accion}';");
	}
	
	function CartaConsecutivoGet($_tipo){
		$ROW = $this->_QUERY("SELECT carta{$_tipo}_ AS cs FROM MAG000;");
		//return $_tipo.'-'.$ROW[0]['cs'].'-'.date('Y');
                return $_tipo.'-'.$ROW[0]['cs'];
	}
	
	function CartaConsecutivoSet($_tipo){
		$this->_TRANS("UPDATE MAG000 SET carta{$_tipo}_ = carta{$_tipo}_ + 1;");
	}
	
	function CartaHistorial($_cs, $_carta9=''){
		if($_carta9==''){
			return $this->_QUERY("SELECT SEG001.id, SEG001.nombre, SEG001.ap1, CONVERT(VARCHAR, CARBIT.fecha, 105) AS fecha1, CARBIT.accion
			FROM CARBIT INNER JOIN SEG001 ON CARBIT.usuario = SEG001.id
			WHERE (CARBIT.carta = '{$_cs}') ORDER BY fecha;");
		}else{
			return $this->_QUERY("SELECT SEG001.id, SEG001.nombre, SEG001.ap1, CONVERT(VARCHAR, CARBIT.fecha, 105) AS fecha1, CARBIT.accion
			FROM CARBIT INNER JOIN SEG001 ON CARBIT.usuario = SEG001.id
			WHERE (CARBIT.carta = '{$_cs}') 
			UNION
			SELECT SEG001.id, SEG001.nombre, SEG001.ap1, '' AS fecha1, '0' AS accion
			FROM CAR025 INNER JOIN SEG001 ON CAR025.usuario =  SEG001.id
			WHERE (CAR025.carta = '{$_cs}')
			ORDER BY accion");
		}
	}
	
	function CartasAccion($_estado){
		if($_estado=='0') return 'Realizada por: ';
		elseif($_estado=='1') return 'Procesada por: ';
		elseif($_estado=='2') return 'Aprobada por: ';
		elseif($_estado=='5') return 'Anulada por: ';
	}
	
	function CartasCatalogo($_LID=''){
		if($_LID == '') $op = '<>';
		else $op = '=';
		
		return $this->_QUERY("SELECT id, [desc]
		FROM CAR030
		WHERE (lab $op '{$_LID}') ORDER BY [desc];");
	}
	
	function CartasEstado($_estado){
		if($_estado=='0') return 'Creada';
		elseif($_estado=='1') return 'Pend. Aprobaci�n';
		elseif($_estado=='2') return 'Aprobada';
		elseif($_estado=='5') return 'Anulada';
	}
	
	function CartasResumenGet($_desde, $_hasta, $_tipo, $_estado, $_lid){
		$_desde = $this->_FECHA($_desde);
		$_hasta = $this->_FECHA($_hasta);
		if($_tipo == '') $op = '<>';
		else $op = '=';
		
		if($_estado == '') $op2 = '<>';
		else $op2 = '=';
		
		$Q = $this->_QUERY("SELECT CAR000.codigoalterno,CAR000.consecutivo, CONVERT(VARCHAR, CAR000.fecha, 105) AS fecha1, CAR000.tipo, CAR030.id, CAR030.[desc], CAR000.estado
		FROM CAR000 INNER JOIN CAR030 ON CAR000.tipo = CAR030.id
		WHERE (CAR000.lab = '{$_lid}') AND (CAR000.tipo {$op} '{$_tipo}') AND (CAR000.estado {$op2} '{$_estado}') AND (CAR000.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00') ORDER BY CAR000.codigolab ASC;");
		for($q=0;$q<count($Q);$q++){
			$Q[$q]['equipo'] = $this->CartasResumenGetEquipo($Q[$q]['id'], $Q[$q]['consecutivo']);
		}
		return $Q;
	}
	
	function CartasResumenGetEquipo($_id, $_carta){		
		if($_id == '0'){
			$W = $this->_QUERY("SELECT EQU000.codigo, EQU000.nombre FROM CAR001 INNER JOIN EQU000 ON CAR001.balanza = EQU000.id WHERE CAR001.codigo = '{$_carta}';");
			if(count($W)>0)
                        return $W[0]['codigo'].' | '.$W[0]['nombre'];
		}elseif($_id == '1'){
			$W = $this->_QUERY("SELECT EQU000.codigo, EQU000.nombre FROM CAR007 INNER JOIN EQU000 ON CAR007.balanza = EQU000.id WHERE CAR007.codigo = '{$_carta}';");
			if(count($W)>0)
                        return $W[0]['codigo'].' | '.$W[0]['nombre'];
		}elseif($_id == '2'){
			$W = $this->_QUERY("SELECT EQU000.codigo, EQU000.nombre FROM CAR009 INNER JOIN EQU000 ON CAR009.equipo = EQU000.id WHERE CAR009.codigo = '{$_carta}';");
			if(count($W)>0)
                        return $W[0]['codigo'].' | '.$W[0]['nombre'];
		}elseif($_id == '3'){
			$W = $this->_QUERY("SELECT EQU000.codigo, EQU000.nombre FROM CAR011 INNER JOIN EQU000 ON CAR011.equipo = EQU000.id WHERE CAR011.codigo = '{$_carta}';");
			return $W[0]['codigo'].' | '.$W[0]['nombre'];
		}elseif($_id == '4'){
			$W = $this->_QUERY("SELECT EQU000.codigo, EQU000.nombre FROM CAR003 INNER JOIN EQU000 ON CAR003.cuarto = EQU000.id WHERE CAR003.codigo = '{$_carta}';");
			if(count($W)>0)
                        return $W[0]['codigo'].' | '.$W[0]['nombre'];
		}elseif($_id == '5'){
			$W = $this->_QUERY("SELECT EQU000.codigo, EQU000.nombre FROM CAR005 INNER JOIN EQU000 ON CAR005.cod_cam = EQU000.id WHERE CAR005.codigo = '{$_carta}';");
			if(count($W)>0)
                        return $W[0]['codigo'].' | '.$W[0]['nombre'];
		}elseif($_id == '6'){
			$W = $this->_QUERY("SELECT EQU000.codigo, EQU000.nombre FROM CAR013 INNER JOIN EQU000 ON CAR013.equipo = EQU000.id WHERE CAR013.codigo = '{$_carta}';");
			if(count($W)>0)
                        return $W[0]['codigo'].' | '.$W[0]['nombre'];
		}elseif($_id == '7'){
			$W = $this->_QUERY("SELECT EQU000.codigo, EQU000.nombre FROM CAR016 INNER JOIN EQU000 ON CAR016.cod_bal = EQU000.id WHERE CAR016.codigo = '{$_carta}';");
			if(count($W)>0)
                        return $W[0]['codigo'].' | '.$W[0]['nombre'];
		}elseif($_id == '8'){
			$W = $this->_QUERY("SELECT EQU000.codigo, EQU000.nombre FROM CAR020 INNER JOIN EQU000 ON CAR020.cod_bal = EQU000.id WHERE CAR020.codigo = '{$_carta}';");
			if(count($W)>0)
                        return $W[0]['codigo'].' | '.$W[0]['nombre'];
		}elseif($_id == '9'){
			$W = $this->_QUERY("SELECT EQU000.codigo, EQU000.nombre FROM CAR023 INNER JOIN EQU000 ON CAR023.equipo = EQU000.id WHERE CAR023.codigo = '{$_carta}';");
			if(count($W)>0)
                        return $W[0]['codigo'].' | '.$W[0]['nombre'];	
		}else return '';
	}
	
	private function DesvEst($aValues){
            $fMean = array_sum($aValues) / count($aValues);
            $fVariance = 0.0;
            foreach ($aValues as $i){
                    $fVariance += pow($i - $fMean, 2);
            }       
            $size = count($aValues) - 1;
            return (float) sqrt($fVariance)/sqrt($size);
	}
	
	function QuechersDetalleSet($_recu, $_matriz, $_analito, $_analista, $_obs){
		$tildes = array("'",'@','&','�','�','�','�','�');
		$solas = array('','','','a','e','i','o','u');
		$_matriz = $this->Free($_matriz);
		$_analista = $this->Free($_analista);
		$_obs = $this->Free($_obs);
		$_analito = strtolower($this->Free($_analito));
		$_analito = str_replace($tildes, $solas, $_analito);
		$_recu = str_replace(',', '', $_recu);
		
		$this->_TRANS("INSERT INTO CAR029 VALUES(GETDATE(), '{$_analito}', '{$_matriz}', {$_recu}, '{$_analista}', '{$_obs}');");
	}
//
}
?>