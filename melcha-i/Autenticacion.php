<?php

class Autenticacion {

    const PROTOCOLO = "https";
    const DOMINIO = "app.sfe.go.cr";

    static function encriptarString($string) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://app2.sfe.go.cr/ws_dllEncriptar/wsEncriptar.svc/EncriptarStringPOST",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => '{"strTexto":"'.$string.'"}',
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        } else {
            return $response;
        }
    }

    static function login($_usuario, $_clave) {
        $_strUsuarioAd = str_replace('"', '', stripslashes(self::encriptarString($_usuario)));
        $_strClaveAd = str_replace('"', '', stripslashes(self::encriptarString($_clave)));
        if ($_strUsuarioAd && $_strClaveAd) {
            $xml = self::verificaUsuarioXML($_usuario, $_strUsuarioAd, $_strClaveAd);
            $url = self::PROTOCOLO . "://" . self::DOMINIO . "/ws_autenticacion/autenticacion.asmx";
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $xml,
                CURLOPT_HTTPHEADER => array(
                    "Cache-Control: no-cache",
                    "Content-Type: text/xml"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                // Error al consultar el usuario
                return -3;
            } else {
                return strpos($response, 'verificarUsuarioSFEResult') !== false ? 1 : -1;
            }
        } else {
            // Error al encriptar
            return -2;
        }
    }

    static function verificaUsuarioXML($_strUsuario, $_strUsuarioAd, $_strClaveAd) {
        ob_start();
        ?>
        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <soap:Body>
                <verificarUsuarioSFE xmlns="http://www.sfe.go.cr/Autenticacion">
                    <strUsuario><?= $_strUsuario ?></strUsuario>
                    <strUsuarioAd><?= $_strUsuarioAd ?></strUsuarioAd>
                    <strClaveAd><?= $_strClaveAd ?></strClaveAd>
                </verificarUsuarioSFE>
            </soap:Body>
        </soap:Envelope>
        <?php
        return '<?xml version="1.0" encoding="utf-8"?> ' . ob_get_clean();
    }

}


