<?php

/*******************************************************************
 * CLASE DE NIVEL 1 ENCARGADA DEL MODULO DE CALIDAD
 * (final SOLO PUEDE SER INSTANCIADA Y NO HEREDADA)
 *******************************************************************/
final class Calidad extends Database
{
//
    function AccionesConsecutivo()
    {
        $ROW = $this->_QUERY("SELECT acap FROM MAG000;");
        return $ROW[0]['acap'];
    }

    function AccionesDetalleGet($_cs)
    {
        $ROW = $this->_QUERY("SELECT cs, tipo, codigo, CONVERT(VARCHAR, fec_pre, 105) AS fec_pre, hallazgo, correccion, CONVERT(VARCHAR, fecha_corr, 105) AS fecha_corr, estado, '' AS tmpA, '' AS usuarioA
		FROM CCA009
		WHERE (cs = {$_cs});");

        if ($ROW) {
            $ROW2 = $this->_QUERY("SELECT SEG001.id, (SEG001.nombre + ' ' +SEG001.ap1) AS usuario
			FROM SEG001 INNER JOIN CCA012 ON SEG001.id = CCA012.usuario
			WHERE (CCA012.acap = {$_cs});");
            if ($ROW2) {
                $ROW[0]['usuarioA'] = $ROW2[0]['id'];
                $ROW[0]['tmpA'] = $ROW2[0]['usuario'];
            }
        }
        return $ROW;
    }

    function AccionesElimina($_yo, $_cs)
    {
        $this->_TRANS("EXECUTE PA_MAG_032 '{$_cs}', NULL, NULL, NULL, NULL, 'D';");

        //ELIMINA TABLON
        $this->TablonDel2(94, $_yo, $_cs);
        $this->Logger('0900');
        //$this->Logger("K{$_accion}");
        return 1;
    }

    function AccionesEncabezadoVacio()
    {
        return array(0 => array(
            'cs' => '-1',
            'tipo' => '',
            'codigo' => '',
            'fec_pre' => '',
            'hallazgo' => '',
            'correccion' => '',
            'fecha_corr' => '',
            'tmpA' => '',
            'usuarioA' => '',
            'estado' => 'A')
        );
    }

    function AccionesEstadoSet($_cs, $_estado)
    {
        $this->_TRANS("UPDATE CCA009 SET estado = '{$_estado}' WHERE (cs = {$_cs});");
    }

    function AccionesLineasDel($_cs)
    {
        $this->_TRANS("DELETE FROM CCA011 WHERE (hallazgo IN (SELECT numero FROM CCA010 WHERE acap = {$_cs}));");
        $this->_TRANS("DELETE FROM CCA010 WHERE (acap = {$_cs});");
    }

    function AccionesLineasGet($_acap)
    {
        return $this->_QUERY("SELECT numero, CONVERT(VARCHAR, fec_acc, 105) AS fec_acc, CONVERT(VARCHAR, fec_seg, 105) AS fec_seg, CONVERT(VARCHAR, fec_rea, 105) AS fec_rea, CONVERT(VARCHAR, fec_cie, 105) AS fec_cie, estado
		FROM CCA010
		WHERE (acap = '{$_acap}') 
		ORDER BY numero;");
    }

    function AccionesLineasSet($_numero, $_acap, $_fec_acc, $_fec_seg, $_fec_rea, $_fec_cie, $_estado, $_usuarioB, $_usuarioC, $_usuarioD)
    {
        $_fec_acc = $this->_FECHA($_fec_acc);
        $_fec_seg = $this->_FECHA($_fec_seg);
        $_fec_rea = $this->_FECHA($_fec_rea);
        $_fec_cie = $this->_FECHA($_fec_cie);
        $_numero = $this->FreePost($_numero);
        $_numero = str_replace(' ', '', $_numero);

        return $this->_TRANS("EXECUTE PA_MAG_033 '{$_numero}', '{$_acap}', '{$_fec_acc}', '{$_fec_seg}', '{$_fec_rea}', '{$_fec_cie}', '{$_estado}', '{$_usuarioB}', '{$_usuarioC}', '{$_usuarioD}';");
    }

    function AccionesLineasVacio()
    {
        return array(0 => array(
            'numero' => '',
            'fec_acc' => '',
            'fec_seg' => '',
            'fec_rea' => '',
            'estado' => '',
            'fec_cie' => '')
        );
    }

    function AccionesResponsablesGet($_numero)
    {
        return $this->_QUERY("EXECUTE PA_MAG_034 '{$_numero}';");
    }

    function AccionesResponsablesVacio()
    {
        return array(0 => array(
            'tmpB' => '',
            'tmpC' => '',
            'tmpD' => '',
            'usuarioB' => '',
            'usuarioC' => '',
            'usuarioD' => '')
        );
    }

    function AccionesResumenGet($_tipo, $_desde, $_hasta, $_estado)
    {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        if ($_tipo == '') $op1 = '<>';
        else $op1 = '=';

        if ($_estado == '') $op2 = '<>';
        else $op2 = '=';

        return $this->_QUERY("SELECT cs, tipo, codigo, CONVERT(VARCHAR, fec_pre, 105) AS fecha, estado
		FROM CCA009
		WHERE (tipo {$op1} '{$_tipo}') AND (estado {$op2} '{$_estado}') AND (fec_pre BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00');");
    }

    function AccionesResumenSet($_cs, $_tipo, $_codigo, $_fec_pre, $_hallazgo, $_correccion, $_fec_corr, $_usuarioA, $_accion)
    {
        $_fec_pre = $this->_FECHA($_fec_pre);
        $_fec_corr = $this->_FECHA($_fec_corr);
        $_codigo = $this->FreePost($_codigo);
        $_codigo = str_replace(' ', '', $_codigo);

        return $this->_TRANS("EXECUTE PA_MAG_032 '{$_cs}', '{$_tipo}', '{$_codigo}', '{$_fec_pre}', '{$_hallazgo}', '{$_correccion}', '{$_fec_corr}', '{$_usuarioA}', '{$_accion}';");
    }

    function CartasMuestra($_lab)
    {
        return $this->_QUERY("SELECT id, [desc] FROM CAR030 WHERE (lab = '{$_lab}');");
    }

    function ClasificacionesIME($_id, $_sigla, $_nombre, $_descripcion, $_tipo, $_accion)
    {
        $_sigla = str_replace(' ', '', $this->Free($_sigla));
        $_nombre = $this->Free($_nombre);
        $_descripcion = $this->Free($_descripcion);

        if ($this->_TRANS("EXECUTE PA_MAG_004 '{$_id}', '{$_sigla}', '{$_nombre}', '{$_descripcion}', '{$_tipo}', '{$_accion}';")) {
            //
            $this->Logger('0901');
            //
            return 1;
        } elseif ($_accion == 'D') {
            return 2;
        } else
            return 0;

    }

    function ClasificacionesMuestra($_tipo)
    {
        if ($_tipo == '') return $this->ClasificacionesVacio();
        if ($_tipo == 1) return $this->_QUERY("SELECT id, sigla, nombre FROM CCA001;");
        if ($_tipo == 2) return $this->_QUERY("SELECT id, nombre, descripcion FROM CCA003;");
        if ($_tipo == 3) return $this->_QUERY("SELECT id, nombre, descripcion, tipo FROM CCA004;");
    }

    function ClasificacionesVacio()
    {
        return array(0 => array(
            'id' => '',
            'sigla' => '',
            'nombre' => '',
            'descripcion' => '')
        );
    }

    function DocumentosDetalleGet($_id)
    {
        return $this->_QUERY("SELECT CCA001.nombre, CCA002.cs, CCA002.lid, CCA002.codigo, CCA002.descripcion, CCA002.tipo, CCA002.modulo, CCA002.naturaleza, CCA002.version, CCA002.revision, CCA002.estado
		FROM CCA001 INNER JOIN CCA002 ON CCA001.id = CCA002.tipo
		WHERE (CCA002.cs = {$_id});");
    }

    function DocumentosEstado($_extra = "tipo <> '2'")
    {
        return $this->_QUERY("SELECT id, nombre, descripcion FROM CCA004 WHERE ({$_extra}) ORDER BY nombre;");
    }

    function DocumentosFiltro()
    {
        return $this->_QUERY("SELECT id, sigla, nombre FROM CCA001 ORDER BY nombre;");
    }

    function DocumentosFirmas($_padre)
    {
        return $this->_QUERY("SELECT SEG001.nombre, SEG001.ap1, CONVERT(VARCHAR, CCA007.fecha, 100) AS fecha1, CCA007.accion, '' AS jus
		FROM CCA007 INNER JOIN SEG001 ON CCA007.usuario = SEG001.id
		WHERE (CCA007.documento = {$_padre}) AND (CCA007.accion <> '-1')
		UNION
		SELECT SEG001.nombre, SEG001.ap1, CONVERT(VARCHAR, CCA007.fecha, 100) AS fecha1, CCA007.accion, CCA008.just
		FROM CCA008 INNER JOIN CCA002 ON CCA008.documento = CCA002.cs INNER JOIN CCA007 ON CCA002.cs = CCA007.documento INNER JOIN SEG001 ON CCA007.usuario = SEG001.id
		WHERE (CCA007.documento = {$_padre}) AND  (CCA007.accion = '-1')
		ORDER BY fecha1;");
    }

    function DocumentosIME($_cs, $_version, $_accion)
    {
        //
        $this->Logger('0902');
        //
        if ($_accion == 1) {//ANULADO DESDE PAGINA DE documentos
            $this->_TRANS("EXECUTE PA_MAG_025 {$_cs};");
        } elseif ($_accion == 2) { //ELIMINADO VERSION UNICA
            $ROW = $this->_QUERY("SELECT COUNT(1) AS total FROM CCA005 WHERE (padre={$_cs}) AND (version='{$_version}');");
            $ROW2 = $this->_QUERY("SELECT MAX(version) AS maxversion FROM CCA005 WHERE (padre={$_cs});");
            if ($ROW[0]['total'] == 1) {
                $this->_TRANS("DELETE FROM CCA005 WHERE (padre = {$_cs}) AND (version='{$_version}');");
                if ($ROW2[0]['maxversion'] == $_version) {
                    $ROW3 = $this->_QUERY("SELECT version FROM CCA005 WHERE version = (SELECT MAX(version) FROM CCA005 WHERE padre={$_cs});");
                    $this->_TRANS("UPDATE CCA002 SET version = '{$ROW3[0]['version']}' WHERE (cs={$_cs});");
                }
                Bibliotecario::ZipEliminar("../../docs/calidad/{$_cs}-{$_version}.zip");
            }
        } elseif ($_accion == 3) { //ELIMINADO DESDE PAGINA DE documentos
            $ROW = $this->_QUERY("SELECT MAX(version) AS maxversion FROM CCA005 WHERE (padre={$_cs});");
            $this->_TRANS("DELETE FROM CCA005 WHERE (padre = {$_cs});");
            $this->_TRANS("DELETE FROM CCA002 WHERE (CS = {$_cs});");
            for ($x = 1; $x <= $ROW[0]['maxversion']; $x++) {
                if (strlen($x) <= 1) {
                    Bibliotecario::ZipEliminar("../../docs/calidad/{$_cs}-0{$x}.zip");
                } else {
                    Bibliotecario::ZipEliminar("../../docs/calidad/{$_cs}-{$x}.zip");
                }

            }
        }
        return 1;
    }

    function DocumentosModulo()
    {
        return $this->_QUERY("SELECT id, nombre FROM SEG000 ORDER BY nombre;");
    }

    function DocumentosMuestra($_tipo, $_codigo, $_nombre, $_lolo, $_rol, $_lab = '')
    {
        $extra1 = $extra2 = $extra3 = $extra4 = $extra5 = '';
        if ($_tipo != '') $extra1 = " AND (CCA002.tipo='{$_tipo}')";
        if ($_nombre != '') $extra2 = " AND (CCA002.descripcion LIKE '%{$_nombre}%')";
        if ($_codigo != '') $extra3 = " AND (CCA002.codigo='{$_codigo}')";
        if ($_lab != '') {
            $extra5 = " AND (CCA002.lid='{$_lab}')";
        }
        //SI NO ES ADMINISTRADOR, NO MUESTRA LOS DOCUMENTOS CON PROPIEDAD NO VISIBILIDAD
        if ($_rol != '0') $extra4 = " AND (CCA004.tipo <> 0)";

        return $this->_QUERY("SELECT CCA002.cs, CCA002.codigo, CCA002.descripcion, CCA002.version, CCA002.revision, CCA004.nombre AS estado, CCA004.tipo AS invisible
		FROM CCA002 INNER JOIN CCA004 ON CCA002.estado = CCA004.id
		WHERE (cs <> 0) {$extra1}{$extra2}{$extra3}{$extra4}{$extra5};");
    }

    function DocumentosNaturaleza()
    {
        return $this->_QUERY("SELECT id, nombre, descripcion FROM CCA003 ORDER BY nombre;");
    }

    function DocumentosNotificaciones()
    {
        return $this->DocumentosEstado("tipo = '2'");
    }

    function DocumentosPendientes($_iud)
    {
        return $this->_QUERY("SELECT CCA002.cs, CCA002.codigo, CCA002.descripcion, CCA002.version, CCA002.revision, CCA007.accion
		FROM CCA002 INNER JOIN CCA007 ON CCA002.cs = CCA007.documento
		WHERE (CCA007.usuario = {$_iud}) AND (CCA007.accion = '93' OR CCA007.accion = '99');");
    }

    function DocumentosQuery()
    {
        return $this->_QUERY("SELECT cs_doc FROM MAG000;");
    }

    function DocumentosReferencias($_tipo, $_lid)
    {
        if ($_tipo != 77)
            return $this->_QUERY("SELECT CCA002.cs, CCA002.codigo, CCA002.descripcion, CCA002.version
		FROM CCA001 INNER JOIN CCA002 ON CCA001.id = CCA002.tipo 
		WHERE (CCA002.modulo = '{$_tipo}') AND (CCA002.naturaleza = '1') AND (CCA002.estado = '1') AND (CCA002.lid = '{$_lid}' OR CCA002.lid = '4');");
        else
            return $this->_QUERY("SELECT CCA002.cs, CCA002.codigo, CCA002.descripcion, CCA002.version
		FROM CCA001 INNER JOIN CCA002 ON CCA001.id = CCA002.tipo 
		WHERE (CCA002.naturaleza = '1') AND (CCA002.estado = '1') AND (CCA002.lid = '{$_lid}' OR CCA002.lid = '4');");
    }

    function DocumentosTrans($_cs_doc, $_lid, $_cod, $_descripcion, $_tipo, $_modulo, $_naturaleza, $_estado, $_version, $_revision, $_accion, $_token)
    {
        $_descripcion = $this->FreePost($_descripcion);
        if ($_accion == 'I') {
            if ($this->_TRANS("INSERT INTO CCA002 VALUES({$_cs_doc}, '$_lid', '$_cod',GETDATE(), '{$_descripcion}', '{$_tipo}', '{$_modulo}', '{$_naturaleza}', '{$_version}', '{$_revision}', '{$_estado}');")) {
                $this->_TRANS("INSERT INTO CCA005 VALUES({$_cs_doc}, '{$_version}', '{$_revision}', GETDATE());");
                $this->_TRANS("UPDATE MAG000 SET cs_doc = cs_doc + 1;");
                $this->Logger('0903');
                return 1;
            } else
                return 0;
        } else {
            $this->Logger('0904');
            if ($_token == 1) {
                if ($this->_TRANS("UPDATE CCA002 SET cs={$_cs_doc}, tipo='{$_tipo}', lid='$_lid', fecha=GETDATE(), descripcion='{$_descripcion}', modulo='{$_modulo}', naturaleza='{$_naturaleza}', version='{$_version}', revision='{$_revision}', estado='{$_estado}' WHERE (cs={$_cs_doc});")) {
                    return 1;
                } else
                    return 0;
            } else {
                if ($this->_TRANS("UPDATE CCA002 SET cs={$_cs_doc}, tipo='{$_tipo}', lid='$_lid', fecha=GETDATE(), descripcion='{$_descripcion}', modulo='{$_modulo}', naturaleza='{$_naturaleza}', version='{$_version}', revision='{$_revision}', estado='{$_estado}' WHERE (cs={$_cs_doc});")) {
                    $this->_TRANS("INSERT INTO CCA005 VALUES({$_cs_doc}, '{$_version}', '{$_revision}', GETDATE());");
                    return 1;
                } else
                    return 0;
            }
        }
    }

    function DocumentosTraslada($_documento, $_usuario, $_peticion, $_detalle, $_yo)
    {
        //ENVIA NOTIFICACI�N
        $_detalle = $this->Free($_detalle);
        $ROW = $this->_QUERY("SELECT email FROM SEG001 WHERE (id={$_usuario});");
        //$this->Email($ROW[0]['email'], $_detalle);
        //ENVIA EMAIL
        //$Robot = new Seguridad();
        //$Robot->Email($ROW[0]['email'], 'Ha recibido una solicitud de anulacion');
        //return 1;
        //
        if ($this->_TRANS("EXECUTE PA_MAG_027 '{$_documento}', '{$_usuario}', '{$_peticion}', '{$_yo}';")) {
            //INGRESA TABLON
            if ($this->TablonSet($_peticion, $_usuario, $_documento)) return 1;
            else return 0;
        } else return 0;
    }

    function DocumentosUnidad()
    {
        return $this->_QUERY("SELECT cs_doc FROM MAG000;");
    }

    function DocumentosVacio()
    {
        return array(0 => array(
            'cs' => '-1',
            'lid' => '',
            'codigo' => '',
            'descripcion' => '',
            'tipo' => '',
            'modulo' => '',
            'naturaleza' => '',
            'version' => '01',
            'revision' => '1',
            'padre' => '',
            'versionP' => '',
            'estado' => '')
        );
    }

    function DocumentosVersiones($_id)
    {
        return $this->_QUERY("SELECT version AS versionP, revision, CONVERT(VARCHAR, fecha,105) AS fecha
		FROM CCA005 
		WHERE (padre= {$_id}) ORDER BY version;");
    }

    function DocumentosVinculadosGet($_padre, $_lock = 0 /*SI ESTA EN 1 SOLO PUEDE VER LOS PERMITIDOS*/)
    {
        if ($_lock == 1) $extra = "AND (CCA002.naturaleza = '1') AND (CCA002.estado = '1')";
        else $extra = '';

        return $this->_QUERY("SELECT CCA002.cs, CCA002.codigo, CCA002.descripcion, CCA002.version
		FROM CCA002 INNER JOIN CCA006 ON CCA002.cs = CCA006.vinculo
		WHERE (CCA006.padre = {$_padre}) {$extra} ORDER BY CCA002.codigo;");
    }

    function DocumentosVinculadosIME($_padre, $_vinculo, $_linea, $_accion)
    {
        $_padre = str_replace(' ', '', $_padre);
        $_vinculo = str_replace(' ', '', $_vinculo);
        $_linea = str_replace(' ', '', $_linea);

        if ($_accion == 'I') {
            //VALIDA SI EL VINCULO NO EXISTE
            $ROW = $this->_QUERY("SELECT cs FROM CCA002 WHERE (codigo = '{$_vinculo}')");
            if (!$ROW) return 2;
            $_vinculo = $ROW[0]['cs'];//SI EXISTE EL VINCULO, OBTENGO EL CS
        }

        if ($this->_TRANS("EXECUTE PA_MAG_026 '{$_padre}', '{$_vinculo}', '{$_linea}', '{$_accion}';"))
            return 1;
        else
            return 0;
    }

    function Encabezado()
    {
        return $this->_QUERY("SELECT encabezado FROM MAG000;");
    }

    function PlantillaEncabezadosMuestra($_id = '', $_lab = '', $_tipo = '')
    {
        if ($_id == '') {
            $extra = '1=1';
            if ($_lab != '-1') {
                $extra = "lab = '" . $_lab . "'";
            }
            return $this->_QUERY("SELECT id, tipo, nombre, detalle, lab FROM CCA015 WHERE {$extra};");
        } else
            if ($_tipo == '') {
                return $this->_QUERY("SELECT id, tipo, nombre, detalle, lab FROM CCA015 WHERE (id= {$_id});");
            } else {
                return $this->_QUERY("SELECT id, tipo, nombre, detalle, lab FROM CCA015 WHERE tipo = '{$_tipo}';");
            }
    }

    function PlantillaEncpieMuestraVacio()
    {
        return array(0 => array(
            'id' => '',
            'tipo' => '',
            'nombre' => '',
            'detalle' => '',
            'lab' => ''
        ));
    }

    function PlantillaEncpieIME($_accion, $_id, $_tipo, $_nombre, $_detalle, $_lab)
    {
        if ($_accion == 'I') {
            if ($this->_TRANS("INSERT INTO CCA015 VALUES('{$_id}', '{$_tipo}', '{$_nombre}', '{$_detalle}', '{$_lab}');")) {
                $this->_TRANS("UPDATE MAG000 SET encabezado = encabezado + 1;");
                return 1;
            } else
                return 0;
        }

        if ($_accion == 'M') {
            if ($this->_TRANS("UPDATE CCA015 SET tipo = '{$_tipo}', nombre = '{$_nombre}', detalle = '{$_detalle}', lab = '{$_lab}' WHERE id = '{$_id}';"))
                return 1;
            else
                return 0;
        }
    }

    function EncabezadosMuestra($_id = '')
    {
        if ($_id == '') {
            return $this->_QUERY("SELECT hoja, codigo, version, nombre, rige, encabezado, pie, enombre, epuesto, efecha, rnombre, rpuesto, rfecha, anombre, apuesto, afecha FROM CCA013;");
        } else {
            return $this->_QUERY("SELECT hoja, codigo, version, nombre, rige, encabezado, pie, enombre, epuesto, efecha, rnombre, rpuesto, rfecha, anombre, apuesto, afecha FROM CCA013 WHERE (hoja= '{$_id}');");
        }
    }

    function EncabezadoMuestra($_sigla = '', $_codigo = '')
    {
        if ($_sigla == '' && $_codigo == '')
            return $this->_QUERY("SELECT sigla, codigo, encabezado, pie FROM CCA014;");
        else
            return $this->_QUERY("SELECT sigla, codigo, encabezado, pie FROM CCA014 WHERE (sigla= '{$_sigla}') AND (codigo= '{$_codigo}');");
    }

    function EncabezadoModifica($_hoja, $_encabezado, $_pie)
    {
        if ($this->_TRANS("UPDATE CCA014 SET encabezado = '{$_encabezado}', pie = '{$_pie}' WHERE hoja = '{$_hoja}';"))
            return 1;
        else
            return 0;
    }

    function EncabezadosMuestraVacio()
    {
        return array(0 => array(
            'id' => '',
            'encabezado' => '',
            'pie' => '',
            'codigo' => '',
            'version' => '',
            'nombre' => '',
            'rige' => '',
            'enombre' => '',
            'epuesto' => '',
            'efecha' => '',
            'rnombre' => '',
            'rpuesto' => '',
            'rfecha' => '',
            'anombre' => '',
            'apuesto' => '',
            'afecha' => '',
        ));
    }

    function EncabezadosIME($_id, $_codigo, $_version, $_nombre, $_rige, $_encabezado, $_pie, $_enombre, $_epuesto, $_efecha, $_rnombre, $_rpuesto, $_rfecha, $_anombre, $_apuesto, $_afecha)
    {
        $Q = $this->_QUERY("SELECT hoja FROM CCA013 WHERE hoja = '{$_id}';");
        $cod = str_replace('0', '', $_id);
        $cod = substr($cod, 1);
        if ($cod == '') {
            $cod = '0';
        }
        if (!$Q) {
            if ($this->_TRANS("INSERT INTO CCA013 VALUES('{$_id}', '{$_codigo}', '{$_version}', '{$_nombre}', '{$_rige}', '{$_encabezado}', '{$_pie}', '{$_enombre}', '{$_epuesto}', '{$_efecha}', '{$_rnombre}', '{$_rpuesto}', '{$_rfecha}','{$_anombre}', '{$_apuesto}', '{$_afecha}');")) {
                $this->_TRANS("UPDATE CCA014 SET encabezado = '{$_encabezado}', pie = '{$_pie}' WHERE sigla = '{$_id[0]}' AND codigo = '{$cod}';");
                return 1;
            } else {
                return 0;
            }
        } else {
            if ($this->_TRANS("UPDATE CCA013 SET codigo = '{$_codigo}', version = '{$_version}', nombre = '{$_nombre}', rige = '{$_rige}', encabezado = '{$_encabezado}', pie = '{$_pie}', enombre = '{$_enombre}', epuesto = '{$_epuesto}', efecha = '{$_efecha}', rnombre = '{$_rnombre}', rpuesto = '{$_rpuesto}', rfecha = '{$_rfecha}', anombre = '{$_anombre}', apuesto = '{$_apuesto}', afecha = '{$_afecha}' WHERE hoja = '{$_id}';")) {
                $this->_TRANS("UPDATE CCA014 SET encabezado = '{$_encabezado}', pie = '{$_pie}' WHERE sigla = '{$_id[0]}' AND codigo = '{$cod}';");
                return 1;
            } else {
                return 0;
            }
        }
    }

    function FirmasIME($_tablon, $_documento, $_usuario, $_jus, $_accion)
    {
        $_jus = $this->Free($_jus);

        $this->_TRANS("EXECUTE PA_MAG_028 '{$_documento}', '{$_usuario}', '{$_jus}', '{$_accion}';");

        //NOTIFICO A LA PERSONA QUE ME NOTIFIC�
        $ROW = $this->_QUERY("SELECT SEG001.email AS 'gestor', 
		(SELECT nombre + ' ' + ap1 + ' ' + ap2 FROM SEG001 WHERE id = {$_usuario}) AS 'aprobador',
		(SELECT codigo FROM CCA002 WHERE cs = {$_documento}) AS 'documento'
		FROM SEG001 INNER JOIN CCA007 ON SEG001.id = CCA007.usuario
		WHERE (CCA007.documento = {$_documento}) AND (CCA007.accion = '00');");
        if ($ROW) {
            if ($_accion == 'D') $msj = "El usuario: {$ROW[0]['aprobador']} ha denegado el documento: {$ROW[0]['documento']} por el siguiente motivo: {$_jus}.";
            else $msj = "El usuario: {$ROW[0]['aprobador']} ha tramitado el documento: {$ROW[0]['documento']}.";

            $this->Email($ROW[0]['gestor'], $msj);
        }

        //ELIMINA TABLON
        $this->TablonDel($_tablon);

        return 1;
    }

    function FirmasTipo($_tipo)
    {
        if ($_tipo == '00') return 'Envi� notificaci�n';
        elseif ($_tipo == '99') return 'Pendiente de revisar';
        elseif ($_tipo == '09') return 'Revisado';
        elseif ($_tipo == '93') return 'Pendiente de aprobar';
        elseif ($_tipo == '03') return '<strong>Aprobado</strong>';
        elseif ($_tipo == '-1') return 'Rechaz� el documento';
    }

    function InduccionesDetalleGet($_persona, $_tipo = '', $_desde = '01/01/1900', $_hasta = '01/01/2070')
    {
        if ($_tipo == '') $op = '<>';
        else $op = '=';

        $ROW = $this->_QUERY("SELECT linea, tipo,empresa, CONVERT(VARCHAR, fecha, 105) AS fecha1, horas, nombre
		FROM PER014
		WHERE (persona = {$_persona}) AND (tipo {$op} '{$_tipo}') AND (fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')
		ORDER BY linea, tipo;");

        return $ROW;
    }

    function InduccionesDetalleLineaGet($_persona, $_linea)
    {
        return $this->_QUERY("SELECT linea, tipo, CONVERT(VARCHAR, fecha, 105) AS fecha1, horas, nombre
		FROM PER014
		WHERE (persona = {$_persona}) AND (linea = {$_linea});");
    }

    function InduccionesDetalleLineaSet($_persona, $_linea, $_tipo, $_nombre, $_fecha, $_horas)
    {
        $_fecha = $this->_FECHA($_fecha);
        $_nombre = $this->Free($_nombre);

        if ($_tipo != '' and $_nombre != '' and $_fecha != '' and $_horas != '') {
            $this->_TRANS("UPDATE PER014 SET tipo = '{$_tipo}', fecha = '{$_fecha}', horas = {$_horas}, nombre = '{$_nombre}'
			WHERE (persona = {$_persona}) AND (linea = {$_linea});");
            $this->Logger('0905');
        } else {
            $this->_TRANS("DELETE FROM PER014 WHERE (persona = {$_persona}) AND (linea = {$_linea});");
            $this->Logger('0906');
        }
    }

    function InduccionesDetalleSet($_id, $_tipo, $_nombre, $_empresa, $_fecha, $_horas)
    {
        $_fecha = $this->_FECHA($_fecha);
        $_nombre = $this->Free($_nombre);
        $_empresa = $this->Free($_empresa);
        if ($_tipo != '' and $_nombre != '' and $_fecha != '' and $_horas != '') {
            if (!$this->_TRANS("EXECUTE PA_MAG_042 '{$_id}', '{$_tipo}', '{$_fecha}', {$_horas}, '{$_nombre}','{$_empresa}';"))
                return 0;
        }
        return 1;
    }

    function InduccionesDetalleVacio()
    {
        return array(0 => array(
            'tipo' => '',
            'fecha1' => '',
            'horas' => '',
            'nombre' => ''
        ));
    }

    function InduccionesEncabezadoGet($_id)
    {
        $ROW = $this->_QUERY("SELECT SEG001.id, SEG001.nombre, SEG001.ap1, SEG001.ap2, PER013.siglas, CONVERT(VARCHAR, PER013.fecha, 105) AS fecha, CONVERT(VARCHAR, PER013.prox, 105) AS prox, PER013.req_cedula, PER013.req_titulo, PER013.req_colegio, PER013.req_hv, PER013.req_compromiso, PER013.req_competente, PER013.req_perfil, PER013.req_clase, PER013.req_especialidad, PER013.req_induccion, PER013.req_autorizacion
		FROM PER013 INNER JOIN SEG001 ON PER013.persona = SEG001.id
		WHERE (SEG001.id = {$_id});");
        if (!$ROW) {
            $ROW = $this->_QUERY("SELECT nombre, ap1, ap2 FROM SEG001 WHERE (id = {$_id});");
            $ROW = $this->InduccionesEncabezadoVacio($_id, $ROW[0]['nombre'], $ROW[0]['ap1'], $ROW[0]['ap2']);
        }
        return $ROW;
    }

    function InduccionesEncabezadoSet($_id, $_siglas, $_prox, $_req_cedula, $_req_titulo, $_req_colegio, $_req_hv, $_req_compromiso, $_req_competente, $_req_perfil, $_req_clase, $_req_especialidad, $_req_induccion, $_req_autorizacion)
    {
        $this->_TRANS("DELETE FROM PER013 WHERE (persona = {$_id});");
        $this->_TRANS("INSERT INTO PER013 VALUES({$_id}, '{$_siglas}', GETDATE(), '{$_prox}', 
			'{$_req_cedula}', 
			'{$_req_titulo}', 
			'{$_req_colegio}', 
			'{$_req_hv}', 
			'{$_req_compromiso}', 
			'{$_req_competente}', 
			'{$_req_perfil}', 
			'{$_req_clase}', 
			'{$_req_especialidad}', 
			'{$_req_induccion}', 
			'{$_req_autorizacion}');");
        return 1;
    }

    function InduccionesEncabezadoVacio($_id, $_nombre, $_ap1, $_ap2)
    {
        return array(0 => array(
            'id' => $_id,
            'nombre' => $_nombre,
            'ap1' => $_ap1,
            'ap2' => $_ap2,
            'siglas' => '',
            'fecha' => 'N/A',
            'prox' => '',
            'req_cedula' => '',
            'req_titulo' => '',
            'req_colegio' => '',
            'req_hv' => '',
            'req_compromiso' => '',
            'req_competente' => '',
            'req_perfil' => '',
            'req_clase' => '',
            'req_especialidad' => '',
            'req_induccion' => '',
            'req_autorizacion' => ''
        ));
    }

    function InduccionesFechaFormato($_fecha)
    {
        $_fecha = explode('-', $_fecha);
        if ($_fecha[1] == '01') $_fecha[1] = 'Enero';
        elseif ($_fecha[1] == '02') $_fecha[1] = 'Febrero';
        elseif ($_fecha[1] == '03') $_fecha[1] = 'Marzo';
        elseif ($_fecha[1] == '04') $_fecha[1] = 'Abril';
        elseif ($_fecha[1] == '05') $_fecha[1] = 'Mayo';
        elseif ($_fecha[1] == '06') $_fecha[1] = 'Junio';
        elseif ($_fecha[1] == '07') $_fecha[1] = 'Julio';
        elseif ($_fecha[1] == '08') $_fecha[1] = 'Agosto';
        elseif ($_fecha[1] == '09') $_fecha[1] = 'Setiembre';
        elseif ($_fecha[1] == '10') $_fecha[1] = 'Octubre';
        elseif ($_fecha[1] == '11') $_fecha[1] = 'Noviembre';
        elseif ($_fecha[1] == '12') $_fecha[1] = 'Diciembre';
        return "{$_fecha[1]}, {$_fecha[2]}";
    }

    function InduccionesIndexa($_persona)
    {
        //REPARA LOS HUECOS EN LAS LINEAS
        $this->_TRANS("EXECUTE PA_MAG_043 '{$_persona}';");
    }

    function InduccionesTipo($_tipo)
    {
        if ($_tipo == 'S') return 'SGC';
        elseif ($_tipo == 'T') return 'T�cnica';
        elseif ($_tipo == 'O') return 'Otras';
        elseif ($_tipo == 'I') return 'Internacionales';
        elseif ($_tipo == 'X') return '<strong>Total horas</strong>';
    }
//
}

?>