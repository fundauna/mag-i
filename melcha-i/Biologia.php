<?php

/* * *****************************************************************
  CLASE DE NIVEL 1 ENCARGADA DE LA SECCION DE BIOLOGIA MOLECULAR
  (final SOLO PUEDE SER INSTANCIADA Y NO HEREDADA)
 * ***************************************************************** */

final class Biologia extends Database {

//		
    function AliDetalleVacio() {
        return array(0 => array(
                'tipo' => '',
                'nombre' => '',
                'estado' => '',
                'obs' => '',
                'fecha' => '',
                'analista' => ''
        ));
    }

    function AliDetalleGet($_id) {
        return $this->_QUERY("SELECT INV027.tipo, INV027.nombre, INV027.estado, INV027.obs, CONVERT(VARCHAR, INV027.fecha, 105) AS fecha, (SEG001.nombre +' '+ SEG001.ap1) AS analista
		FROM INV027 INNER JOIN SEG001 ON INV027.analista = SEG001.id
		WHERE (INV027.alicuota = '{$_id}')
		ORDER BY INV027.linea");
    }

    function AlicuotasDisponibles() {
        return $this->_QUERY("SELECT (alicuota +'-'+ CONVERT(VARCHAR, linea) ) AS id, nombre 
		FROM INV027 WHERE (estado='1') ORDER BY alicuota;");
    }

    function AliEncabezadoGet($_id) {
        return $this->_QUERY("SELECT alicuota AS cs FROM INV027 WHERE (alicuota = '{$_id}');");
    }

    function AliEncabezadoVacio() {
        return array(0 => array(
                'cs' => ''
        ));
    }

    function AliIME($_accion, $_cs, $_tipo, $_nombre, $_estado, $_observaciones, $_UID) {
        $this->_TRANS("DELETE FROM INV027 WHERE (alicuota = '{$_cs}');");

        if ($_accion != 'D') {
            $_tipo = explode('&', substr($_tipo, 1));
            $_nombre = explode('&', substr($_nombre, 1));
            $_estado = explode('&', substr($_estado, 1));
            $_observaciones = explode('&', substr($_observaciones, 1));

            for ($i = 0; $i < count($_tipo); $i++) {
                if (str_replace(' ', '', $_tipo[$i]) != '') {
                    $_nombre[$i] = $this->Free($_nombre[$i]);
                    $_observaciones[$i] = $this->Free($_observaciones[$i]);
                    $x = $i + 1;

                    $this->_TRANS("INSERT INTO INV027 VALUES('{$_cs}', {$x}, 
						'{$_tipo[$i]}',
						'{$_nombre[$i]}',
						'{$_estado[$i]}',
						'{$_observaciones[$i]}',
						GETDATE(),
						{$_UID}
					);");
                }
            }//for
        }
        return 1;
    }

    function AliResumenGet($_desde, $_hasta, $_codigo) {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        $_codigo = str_replace(' ', '', $_codigo);
        if ($_codigo != '')
            $extra = "alicuota LIKE '%{$_codigo}%'";
        else
            $extra = "fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00'";

        return $this->_QUERY("SELECT alicuota, linea, CONVERT(VARCHAR, fecha, 105) AS fecha, tipo, estado
		FROM INV027
		WHERE ({$extra})
		ORDER BY alicuota, linea;");
    }

    function AliTipo($_var) {
        if ($_var == '0')
            return 'Agua';
        elseif ($_var == '1')
            return 'Controles';
        elseif ($_var == '2')
            return 'dNTP';
        elseif ($_var == '3')
            return 'Primer';
        elseif ($_var == '4')
            return 'Sonda';
        elseif ($_var == '5')
            return 'Otro';
    }

    function ADNoAnalisisGet($_cs, $_paso) {
        return $this->_QUERY("SELECT VAR005.id, VAR005.nombre
		FROM INV046 INNER JOIN VAR005 ON INV046.analisis = VAR005.id
		WHERE (INV046.muestra = '{$_cs}') AND (INV046.paso = '{$_paso}')
		ORDER BY VAR005.nombre;");
    }

    function ADNoDetalleGet($_cs) {
        return $this->_QUERY("SELECT VAR005.id, VAR005.nombre, INV047.resultado, INV047.obs
		FROM INV047 INNER JOIN VAR005 ON INV047.analisis = VAR005.id
		WHERE (INV047.muestra = '{$_cs}') ORDER BY VAR005.nombre");
    }

    function ADNoDetalleVacio() {
        return array(0 => array(
                'id' => '',
                'nombre' => '',
                'tecnica' => '',
                'resultado' => '',
                'ficha' => '',
                'reporte' => '',
                'obs' => ''
        ));
    }

    function ADNoEncabezadoGet($_cs) {
        return $this->_QUERY("SELECT INV045.id, INV045.analito, INV045.bandeja, INV045.hoja, INV045.cultivo, INV045.cuanti, CONVERT(VARCHAR, INV045.fechaC, 105) AS fechaC, CONVERT(VARCHAR, INV045.fechaA, 105) AS fechaA, INV045.obs, INV045.estado, (SEG001.nombre + ' ' + SEG001.nombre) AS analista
		FROM INV045 INNER JOIN SEG001 ON INV045.analista = SEG001.id
		WHERE (INV045.id = '{$_cs}');");
    }

    function ADNoEncabezadoVacio() {
        return array(0 => array(
                'id' => '',
                'bandeja' => '',
                'analito' => '',
                'cultivo' => '',
                'cuanti' => '0',
                'hoja' => '',
                'fechaC' => '',
                'fechaR' => '',
                'fechaA' => '',
                'obs' => '',
                'analista' => '',
                'estado' => ''
        ));
    }

    function ADNoE($_cs, $_UID) {
        if ($this->_TRANS("DELETE FROM BAN002 WHERE valor1 = '{$_cs}';")) {
            $this->_TRANS("UPDATE INV045 SET estado = 'o' WHERE id = '{$_cs}';");
            return 1;
        } else
            return 0;
    }

    function ADNoIME($_accion, $_cs, $_analito, $_rotulo, $_bandeja, $_x, $_y, $_cultivo, $_cuanti, $_hoja, $_fechaA, $_observaciones, $_A1cod_analisis, $_A2cod_analisis, $_A3cod_analisis, $_resultado, $_obs, $_estado, $_UID) {
        $_fechaA = $this->_FECHA($_fechaA);
        $_cs = $this->Free($_cs);
        $_cultivo = $this->Free($_cultivo);
        $_hoja = $this->Free($_hoja);
        $_observaciones = $this->Free($_observaciones);

        if ($this->_TRANS("EXECUTE PA_MAG_111 '{$_cs}', '{$_analito}', '{$_rotulo}', '{$_bandeja}', '{$_x}', '{$_y}', '{$_hoja}', '{$_cultivo}', '{$_cuanti}', '{$_fechaA}', '{$_UID}', '{$_observaciones}', '{$_estado}', '{$_accion}';")) {

            if ($_accion != 'D') {
                $_A1cod_analisis = explode('&', substr($_A1cod_analisis, 1));
                $_A2cod_analisis = explode('&', substr($_A2cod_analisis, 1));
                $_A3cod_analisis = explode('&', substr($_A3cod_analisis, 1));
                $_resultado = explode('&', substr($_resultado, 1));
                $_obs = explode('&', substr($_obs, 1));

                $this->_TRANS("DELETE FROM INV047 WHERE (muestra = '{$_cs}');");
                $this->_TRANS("DELETE FROM INV046 WHERE (muestra = '{$_cs}');");

                for ($i = 0; $i < count($_A1cod_analisis); $i++) {
                    if (str_replace(' ', '', $_A1cod_analisis[$i]) != '')
                        $this->_TRANS("INSERT INTO INV046 VALUES('{$_cs}', '1', {$_A1cod_analisis[$i]});");
                }
                for ($i = 0; $i < count($_A2cod_analisis); $i++) {
                    if (str_replace(' ', '', $_A2cod_analisis[$i]) != '')
                        $this->_TRANS("INSERT INTO INV046 VALUES('{$_cs}', '2', {$_A2cod_analisis[$i]});");
                }
                //
                for ($i = 0; $i < count($_A3cod_analisis); $i++) {
                    if (str_replace(' ', '', $_A3cod_analisis[$i]) != '') {
                        if (!isset($_obs[$i]))
                            $_obs[$i] = '';
                        $_obs[$i] = $this->Free($_obs[$i]);

                        $this->_TRANS("INSERT INTO INV047 VALUES('{$_cs}', 
							{$_A3cod_analisis[$i]},
						   '{$_resultado[$i]}',
						   '{$_obs[$i]}'
						);");
                    }
                }
            }

            return 1;
        } else
            return 0;
    }

    function ADNoPendientesGet() {
        return $this->_QUERY("SELECT id, CONVERT(VARCHAR, fechaC, 105) AS fecha, estado
		FROM INV045 WHERE (estado='p') ORDER BY fechaC;");
    }

    function ADNoResumenGet($_desde, $_hasta, $_codigo) {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        if ($_codigo != '') { //ESTA BUSCANDO POR VALOR DIRECTO
            $SQL = "SELECT id, CONVERT(VARCHAR, fechaC, 105) AS fecha, estado
			FROM INV045
			WHERE (id = '{$_codigo}');";
        } else { //ESTA BUSCANDO POR FECHA
            $SQL = "SELECT id, CONVERT(VARCHAR, fechaC, 105) AS fecha, estado
			FROM INV045
			WHERE (fechaC BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00') ORDER BY fechaC;";
        }

        return $this->_QUERY($SQL);
    }

    function BandejasIME($_id, $_dimx, $_dimy, $_labelx, $_labely, $_lab, $_accion) {
        $_id = $this->Free($_id);

        if ($this->_TRANS("EXECUTE PA_MAG_055 '{$_id}', '{$_dimx}', '{$_dimy}', '{$_labelx}', '{$_labely}', '{$_lab}', '{$_accion}';")) {
            //
            $this->Logger('A700');
            //
            return 1;
        } else
            return 0;
    }

    function BandejasMuestra($_id) {
        return $this->_QUERY("SELECT id, dimx, dimy, labelx, labely
		FROM BAN001
		WHERE (id = '{$_id}')
		ORDER BY id;");
    }

    function BandejasMuestraCajas($_id) {
        return $this->_QUERY("SELECT x, y, valor1, valor2
		FROM BAN002
		WHERE (bandeja = '{$_id}');");
    }

    function BandejasResumenGet($_lab) {
        return $this->_QUERY("SELECT id, dimx, dimy, labelx, labely
		FROM BAN001
		WHERE (lab = '{$_lab}')
		ORDER BY id;");
    }

    function CajasIME($_id) {
        $_id = $this->Free($_id);

        if ($this->_TRANS("INSERT INTO BAN003 VALUES('{$_id}', '2');")) {
            return 1;
        } else
            return 0;
    }

    function CajasResumenGet($_lab) {
        return $this->_QUERY("SELECT id FROM BAN003 WHERE (lab = '{$_lab}')
		ORDER BY id;");
    }

    function VaciaTemporal() {
        return $this->_TRANS("DELETE FROM BANTMP2;");
    }

    function CajasResumenGetPlaga($_plaga) {
        $Q = $this->_QUERY("SELECT muestra FROM INV047 WHERE analisis = '{$_plaga}';");
        for ($z = 0; $z < count($Q); $z++) {
            $W = $this->_QUERY("SELECT bandeja FROM BAN002 WHERE valor1 = '{$Q[$z]['muestra']}';");
            if ($W) {
                $E = $this->_QUERY("SELECT caja FROM BAN004 WHERE bandeja = '{$W[0]['bandeja']}';");
                $this->_TRANS("INSERT INTO BANTMP2 VALUES('{$W[0]['bandeja']}', '{$E[0]['caja']}', '{$_plaga}');");
            }
        }
        return $this->_QUERY("SELECT DISTINCT bandeja FROM BANTMP2 WHERE plaga = '{$_plaga}';");
    }

    function CamarasIME($_caja, $_bandeja, $_sigla) {
        if ($_sigla == 'I') {
            if ($this->_TRANS("INSERT INTO BAN004 VALUES('{$_caja}', '{$_bandeja}');")) {
                return 1;
            } else
                return 0;
        }elseif ($_sigla == 'D') {
            if ($this->_TRANS("DELETE FROM BAN004 WHERE caja = '{$_caja}' AND bandeja = '{$_bandeja}';")) {
                return 1;
            } else
                return 0;
        }
    }

    function CamarasResumenGet($_caja) {
        return $this->_QUERY("SELECT bandeja FROM BAN004 WHERE (caja = '{$_caja}') ORDER BY bandeja;");
    }

    function CamarasResumenGet2($_caja, $_plaga) {
        $Q = $this->_QUERY("SELECT muestra FROM INV047 WHERE analisis = '{$_plaga}';");
        $this->_TRANS("DELETE FROM BANTMP1;");
        for ($z = 0; $z < count($Q); $z++) {
            $W = $this->_QUERY("SELECT bandeja FROM BAN002 WHERE valor1 = '{$Q[$z]['muestra']}';");
            $this->_TRANS("INSERT INTO BANTMP1 VALUES('{$Q[$z]['muestra']}', '{$W[0]['bandeja']}');");
        }
        return $this->_QUERY("SELECT DISTINCT bandeja FROM BANTMP1;");
    }

    function DiluDetalleGet($_cs) {
        return $this->_QUERY("SELECT INV017.observaciones, INV017.cod_disolvente, INV017.concentracionI, INV017.disolvente, INV017.concentracionF, INV017.med1, INV017.med2, INV017.med3, CONVERT(VARCHAR, INV017.fecha, 105) AS fecha, (SEG001.nombre +' '+ SEG001.ap1) AS analista
		FROM INV017 INNER JOIN SEG001 ON INV017.analista = SEG001.id
		WHERE (INV017.dilucion = '{$_cs}')
		ORDER BY INV017.linea");
    }

    function DiluDetalleVacio() {
        return array(0 => array(
                'cod_disolvente' => '',
                'concentracionI' => '0.00',
                'disolvente' => '0.00',
                'concentracionF' => '1',
                'observaciones' => '',
                'med1' => '',
                'med2' => '',
                'med3' => '',
                'fecha' => '',
                'analista' => ''
        ));
    }

    function DiluEncabezadoGet($_cs) {
        $_cs = str_replace(' ', '', $_cs);
        return $this->_QUERY("SELECT cs AS codigo, reactivo FROM INV020 WHERE (cs = '{$_cs}');");
    }

    function DiluEncabezadoVacio() {
        return array(0 => array(
                'codigo' => '',
                'reactivo' => ''
        ));
    }

    function DiluIme($_codigo, $_reactivo, $_observaciones, $_cod_disolvente, $_concentracionI, $_disolvente, $_concentracionF, $_med1, $_med2, $_med3, $_UID, $_accion) {
        $_codigo = $this->Free($_codigo);
        $_reactivo = $this->Free($_reactivo);
        $_concentracionI = str_replace(',', '', explode('&', substr($_concentracionI, 1)));
        $_disolvente = str_replace(',', '', explode('&', substr($_disolvente, 1)));
        $_cod_disolvente = explode('&', substr($_cod_disolvente, 1));
        $_concentracionF = str_replace(',', '', explode('&', substr($_concentracionF, 1)));
        $_observaciones = explode('&', substr($_observaciones, 1));
        $_med1 = explode('&', substr($_med1, 1));
        $_med2 = explode('&', substr($_med2, 1));
        $_med3 = explode('&', substr($_med3, 1));

        if ($this->_TRANS("EXECUTE PA_MAG_054 '{$_codigo}', '{$_reactivo}', '{$_accion}';")) {

            if ($_accion != 'D') {
                $this->_TRANS("DELETE FROM INV017 WHERE (dilucion = '{$_codigo}');");

                for ($i = 0, $x = 0; $i < count($_cod_disolvente); $i++) {
                    if (str_replace(' ', '', $_cod_disolvente[$i]) != '') {
                        $x++;
                        $_observaciones[$i] = $this->Free($_observaciones[$i]);
                        $_cod_disolvente[$i] = $this->Free($_cod_disolvente[$i]);

                        $this->_TRANS("INSERT INTO INV017 VALUES('{$_codigo}', '{$x}', 
							'{$_observaciones[$i]}',
							'{$_cod_disolvente[$i]}',
							 {$_concentracionI[$i]}, 
							 {$_disolvente[$i]}, 							
							 {$_concentracionF[$i]},
							'{$_med1[$i]}',
							'{$_med2[$i]}',
							'{$_med3[$i]}',
							 GETDATE(),
							 {$_UID}
						);");
                    }
                }
            }
            //$this->Logger("M{$_accion}");
            return 1;
        } else
            return 0;
    }

    function DiluResumenGet($_desde, $_hasta, $_codigo) {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        $_codigo = str_replace(' ', '', $_codigo);
        if ($_codigo != '')
            $extra = "INV020.cs LIKE '%{$_codigo}%'";
        else
            $extra = "INV017.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00'";

        return $this->_QUERY("SELECT INV020.reactivo, CONVERT(VARCHAR, INV017.fecha, 105) AS fecha, INV017.dilucion, INV017.linea
		FROM INV017 INNER JOIN INV020 ON INV017.dilucion = INV020.cs
		WHERE ({$extra});");
    }

    function ProcAnalisisGet($_cs, $_paso) {
        return $this->_QUERY("SELECT VAR005.id, VAR005.nombre
		FROM INV031 INNER JOIN VAR005 ON INV031.analisis = VAR005.id
		WHERE (INV031.proce = '{$_cs}') AND (INV031.paso = '{$_paso}')
		ORDER BY VAR005.nombre;");
    }

    function ProcEquiposGet($_cs, $_paso) {
        if ($_paso == 'X')
            $_paso = 'F';
        elseif ($_paso == 'F')
            $_paso = 'G';
        elseif ($_paso == 'G')
            $_paso = 'H';
        return $this->_QUERY("SELECT EQU000.id, EQU000.codigo, EQU000.nombre
		FROM INV030 INNER JOIN EQU000 ON INV030.equipo = EQU000.id
		WHERE (INV030.proce = '{$_cs}') AND (INV030.paso = '{$_paso}')
		ORDER BY EQU000.nombre;");
    }

    function ProcEncabezadoGet($_cs) {
        return $this->_QUERY("SELECT codigo AS cs, nombre, Dnormalizada, Danalito, Dunidad, Eprograma, Ftiempo, Fvoltaje, Gamplicon, Gprograma, Htiempo, Hvoltaje
		FROM INV028
		WHERE (codigo = '{$_cs}');");
    }

    function ProcEncabezadoVacio() {
        return array(0 => array(
                'cs' => '',
                'nombre' => '',
                'Dnormalizada' => '0',
                'Danalito' => '',
                'Dunidad' => '',
                'Eprograma' => '',
                'Ftiempo' => '0',
                'Fvoltaje' => '0',
                'Gamplicon' => '0',
                'Gprograma' => '',
                'Htiempo' => '0',
                'Hvoltaje' => '0'
        ));
    }

    function ProcIME($_accion, $_cs, $_nombre, $_A1cod_analisis, $_A2cod_analisis, $_A3cod_analisis, $_C1cod_insumos, $_C1total, $_C2cod_equipos, $_Dnormalizada, $_Danalito, $_Dunidad, $_D1cod_insumos, $_D1total, $_D2cod_equipos, $_E1tipo, $_E1descr, $_E1concS, $_E1concF, $_E1volumen, $_E2cod_equipos, $_Eprograma, $_F1cod_insumos, $_F1total, $_F2cod_equipos, $_Ftiempo, $_Fvoltaje, $_G1tipo, $_G1descr, $_G1volumen, $_Gamplicon, $_G2cod_equipos, $_Gprograma, $_H1cod_insumos, $_H1total, $_H2cod_equipos, $_Htiempo, $_Hvoltaje) {
        $_cs = $this->Free($_cs);
        $_nombre = $this->FreePost($_nombre);
        $_Eprograma = $this->FreePost($_Eprograma);
        $_Gprograma = $this->FreePost($_Gprograma);

        if ($this->_TRANS("EXECUTE PA_MAG_120 '{$_cs}', '{$_nombre}', '{$_Dnormalizada}', '{$_Danalito}', '{$_Dunidad}', '{$_Eprograma}', '{$_Ftiempo}', '{$_Fvoltaje}', '{$_Gamplicon}', '{$_Gprograma}', '{$_Htiempo}', '{$_Hvoltaje}', '{$_accion}';")) {
            if ($_accion != 'D') {
                //ANALISIS
                for ($i = 0; $i < count($_A1cod_analisis); $i++) {
                    if ($_A1cod_analisis != '' and str_replace(' ', '', $_A1cod_analisis[$i]) != '')
                        $this->_TRANS("INSERT INTO INV031 VALUES('{$_cs}', '1', '{$_A1cod_analisis[$i]}');");
                }
                for ($i = 0; $i < count($_A2cod_analisis); $i++) {
                    if ($_A2cod_analisis != '' and str_replace(' ', '', $_A2cod_analisis[$i]) != '')
                        $this->_TRANS("INSERT INTO INV031 VALUES('{$_cs}', '2', '{$_A2cod_analisis[$i]}');");
                }
                for ($i = 0; $i < count($_A3cod_analisis); $i++) {
                    if ($_A3cod_analisis != '' and str_replace(' ', '', $_A3cod_analisis[$i]) != '')
                        $this->_TRANS("INSERT INTO INV031 VALUES('{$_cs}', '3', '{$_A3cod_analisis[$i]}');");
                }
                //INSUMOS
                for ($i = 0; $i < count($_C1cod_insumos); $i++) {
                    if ($_C1cod_insumos != '' and str_replace(' ', '', $_C1cod_insumos[$i]) != '')
                        $this->_TRANS("INSERT INTO INV029 VALUES('{$_cs}', 'C', '{$_C1cod_insumos[$i]}', '{$_C1total[$i]}');");
                }
                for ($i = 0; $i < count($_D1cod_insumos); $i++) {
                    if ($_D1cod_insumos != '' and str_replace(' ', '', $_D1cod_insumos[$i]) != '')
                        $this->_TRANS("INSERT INTO INV029 VALUES('{$_cs}', 'D', '{$_D1cod_insumos[$i]}', '{$_D1total[$i]}');");
                }
                for ($i = 0; $i < count($_F1cod_insumos); $i++) {
                    if ($_F1cod_insumos != '' and str_replace(' ', '', $_F1cod_insumos[$i]) != '')
                        $this->_TRANS("INSERT INTO INV029 VALUES('{$_cs}', 'F', '{$_F1cod_insumos[$i]}', '{$_F1total[$i]}');");
                }
                for ($i = 0; $i < count($_H1cod_insumos); $i++) {
                    if ($_H1cod_insumos != '' and str_replace(' ', '', $_H1cod_insumos[$i]) != '')
                        $this->_TRANS("INSERT INTO INV029 VALUES('{$_cs}', 'H', '{$_H1cod_insumos[$i]}', '{$_H1total[$i]}');");
                }
                //EQUIPOS
                for ($i = 0; $i < count($_C2cod_equipos); $i++) {
                    if ($_C2cod_equipos != '' and str_replace(' ', '', $_C2cod_equipos[$i]) != '')
                        $this->_TRANS("INSERT INTO INV030 VALUES('{$_cs}', 'C', '{$_C2cod_equipos[$i]}');");
                }
                for ($i = 0; $i < count($_D2cod_equipos); $i++) {
                    if ($_D2cod_equipos != '' and str_replace(' ', '', $_D2cod_equipos[$i]) != '')
                        $this->_TRANS("INSERT INTO INV030 VALUES('{$_cs}', 'D', '{$_D2cod_equipos[$i]}');");
                }
                for ($i = 0; $i < count($_E2cod_equipos); $i++) {
                    if ($_E2cod_equipos != '' and str_replace(' ', '', $_E2cod_equipos[$i]) != '')
                        $this->_TRANS("INSERT INTO INV030 VALUES('{$_cs}', 'E', '{$_E2cod_equipos[$i]}');");
                }
                for ($i = 0; $i < count($_F2cod_equipos); $i++) {
                    if ($_F2cod_equipos != '' and str_replace(' ', '', $_F2cod_equipos[$i]) != '')
                        $this->_TRANS("INSERT INTO INV030 VALUES('{$_cs}', 'F', '{$_F2cod_equipos[$i]}');");
                }
                for ($i = 0; $i < count($_G2cod_equipos); $i++) {
                    if ($_G2cod_equipos != '' and str_replace(' ', '', $_G2cod_equipos[$i]) != '')
                        $this->_TRANS("INSERT INTO INV030 VALUES('{$_cs}', 'G', '{$_G2cod_equipos[$i]}');");
                }
                for ($i = 0; $i < count($_H2cod_equipos); $i++) {
                    if ($_H2cod_equipos != '' and str_replace(' ', '', $_H2cod_equipos[$i]) != '')
                        $this->_TRANS("INSERT INTO INV030 VALUES('{$_cs}', 'H', '{$_H2cod_equipos[$i]}');");
                }
                //PLANTILLA 1
                for ($i = 0; $i < count($_E1tipo); $i++) {
                    if ($_E1tipo != '' and str_replace(' ', '', $_E1tipo[$i]) != '')
                        $this->_TRANS("INSERT INTO INV040 VALUES('{$_cs}', {$i}, '{$_E1tipo[$i]}', '{$_E1descr[$i]}', '{$_E1concS[$i]}', '{$_E1concF[$i]}', {$_E1volumen[$i]});");
                }
                //PLANTILLA 2
                for ($i = 0; $i < count($_G1tipo); $i++) {
                    if ($_G1tipo != '' and str_replace(' ', '', $_G1tipo[$i]) != '')
                        $this->_TRANS("INSERT INTO INV041 VALUES('{$_cs}', {$i}, '{$_G1tipo[$i]}', '{$_G1descr[$i]}', {$_G1volumen[$i]});");
                }
            }
            //
            if ($_accion == 'I')
                $_accion = 'A701';
            elseif ($_accion == 'M')
                $_accion = 'A702';
            else
                $_accion = 'A703';
            $this->Logger($_accion);
            //
            return 1;
        } else
            return 0;
    }

    function ProcInsumosGet($_cs, $_paso) {
        return $this->_QUERY("SELECT INV002.id, INV002.codigo, INV002.nombre, INV029.cantidad
		FROM INV029 INNER JOIN INV002 ON INV029.insumo = INV002.id
		WHERE (INV029.proce = '{$_cs}') AND (INV029.paso = '{$_paso}')
		ORDER BY INV002.nombre;");
    }

    function ProcInsumosImporta($_cs, $_paso) {
        if ($_paso == 'X')
            $_paso = 'F';
        elseif ($_paso == 'G')
            $_paso = 'H';
        return $this->_QUERY("SELECT '' AS id, INV002.id AS padre, ('...' + INV002.nombre) AS nombre, INV029.cantidad
		FROM INV029 INNER JOIN INV002 ON INV029.insumo = INV002.id
		WHERE (INV029.proce = '{$_cs}') AND (INV029.paso = '{$_paso}')
		ORDER BY INV002.nombre;");
    }

    function ProcOtrosGet($_cs) {
        return $this->_QUERY("SELECT Dnormalizada, Danalito, Dunidad, Eprograma, Ftiempo, Fvoltaje, Gamplicon, Gprograma, Htiempo, Hvoltaje
		FROM INV028 WHERE (codigo='{$_cs}');");
    }

    function ProcPlantillaGet($_cs, $_tipo) {
        if ($_tipo == '1')
            return $this->_QUERY("SELECT tipo, descr, concS, concF, volumen
			FROM INV040
			WHERE (proce = '{$_cs}')
			ORDER BY linea;");
        else
            return $this->_QUERY("SELECT tipo, descr, volumen
			FROM INV041
			WHERE (proce = '{$_cs}')
			ORDER BY linea;");
    }

    function ProcResumenGet($_codigo, $_nombre) {
        if ($_codigo == '')
            $extra = "(nombre LIKE '%{$_nombre}%')";
        else
            $extra = "(codigo = '{$_codigo}') AND (nombre LIKE '%{$_nombre}%')";

        return $this->_QUERY("SELECT codigo AS cs, nombre 
		FROM INV028 
		WHERE {$extra};");
    }

    function ResDetalleGet($_cs) {
        return $this->_QUERY("SELECT observaciones, estado FROM INV019 WHERE (resuspension = '{$_cs}');");
    }

    function ResDetalleVacio() {
        return array(0 => array(
                'estado' => '',
                'observaciones' => ''
        ));
    }

    function ResDisponibles() {
        return $this->_QUERY("SELECT codigo FROM INV019 WHERE (estado = '1') ORDER BY codigo;");
    }

    function ResEncabezadoGet($_cs) {
        return $this->_QUERY("SELECT INV018.codigo, INV018.reactivo, CONVERT(VARCHAR, INV018.fecha_r, 105) AS fecha_r, INV018.disolvente, INV018.concentracionI, INV018.vol_disolvente, INV018.concentracionM, INV018.med1, INV018.med2, INV018.med3, (SEG001.nombre +' '+ SEG001.ap1) AS analista
		FROM INV018 INNER JOIN SEG001 ON INV018.analista = SEG001.id WHERE (INV018.codigo = '{$_cs}');");
    }

    function ResEncabezadoVacio() {
        return array(0 => array(
                'codigo' => '',
                'fecha' => '',
                'reactivo' => '',
                'fecha_r' => '',
                'disolvente' => '',
                'analista' => '',
                'concentracionI' => '1',
                'vol_disolvente' => '0',
                'concentracionM' => '0',
                'med1' => '',
                'med2' => '',
                'med3' => ''
        ));
    }

    function ResIME($_accion, $_codigo, $_reactivo, $_fecha_r, $_disolvente, $_concentracionI, $_vol_disolvente, $_concentracionM, $_med1, $_med2, $_med3, $_observaciones, $_estado, $_UID) {
        $_codigo = str_replace(' ', '', $this->Free($_codigo));
        $_reactivo = $this->Free($_reactivo);
        $_disolvente = $this->Free($_disolvente);
        $_observaciones = explode('&', substr($_observaciones, 1));
        $_estado = explode('&', substr($_estado, 1));
        $_fecha_r = $this->_FECHA($_fecha_r);

        if ($this->_TRANS("EXECUTE PA_MAG_059 '{$_codigo}', '{$_reactivo}', '{$_fecha_r}', '{$_disolvente}', '{$_concentracionI}', '{$_vol_disolvente}', '{$_concentracionM}', '{$_med1}', '{$_med2}', '{$_med3}', '{$_UID}', '{$_accion}';")) {

            if ($_accion != 'D') {
                $this->_TRANS("DELETE FROM INV019 WHERE (resuspension = '{$_codigo}');");
                for ($i = 0; $i < count($_observaciones); $i++) {
                    $hijo = $_codigo . '-' . ($i + 1);
                    $_observaciones[$i] = $this->Free($_observaciones[$i]);
                    $this->_TRANS("INSERT INTO INV019 VALUES('{$hijo}', '{$_codigo}', '{$_observaciones[$i]}', {$_estado[$i]});");
                }
            }
            //$this->Logger("M{$_accion}");
            return 1;
        } else
            return 0;
    }

    function ResResumenGet($_desde, $_hasta, $_codigo) {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        $_codigo = str_replace(' ', '', $_codigo);
        if ($_codigo != '')
            $extra = "codigo LIKE '%{$_codigo}%'";
        else
            $extra = "fecha_r BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00'";

        return $this->_QUERY("SELECT codigo, reactivo, CONVERT(VARCHAR, fecha_r, 105) AS fecha
		FROM INV018 WHERE ($extra) ORDER BY fecha_r;");
    }

    function TrabajoAnalisisGet($_id, $_paso) {
        return $this->_QUERY("SELECT VAR005.id, VAR005.nombre
		FROM INV033 INNER JOIN VAR005 ON INV033.analisis = VAR005.id
		WHERE (INV033.hoja = '{$_id}') AND (INV033.paso = '{$_paso}')
		ORDER BY VAR005.nombre;");
    }

    function TrabajoCsGet() {
        $ROW = $this->_QUERY("SELECT hoja02_ AS cs FROM MAG000;");
        return 'BM-' . $ROW[0]['cs'] . '-' . date('Y');
    }

    function TrabajoAnula($_cs, $_UID) {
        $this->_TRANS("INSERT INTO INV035 VALUES ('{$_cs}', 'N', {$_UID}, GETDATE());");
        $this->_TRANS("UPDATE INV032 SET estado = 'a' WHERE (id = '{$_cs}');");
        return 1;
    }

    function TrabajoControlesGet($_cs, $_tipo) {
        if ($_tipo == 1)
            return $this->_QUERY("SELECT control FROM INV026 WHERE (hoja = '{$_cs}') ORDER BY control;");
        else
            return $this->_QUERY("SELECT control, cumple FROM INV038 WHERE (hoja = '{$_cs}') ORDER BY control;");
    }

    function TrabajoEncabezadoGet($_id) {
        return $this->_QUERY("EXECUTE PA_MAG_123 '{$_id}';");
    }

    function TrabajoEncabezadoVacio($_proc) {
        return array(0 => array(
                'cs' => '',
                'procedimiento' => $_proc,
                'analistaA' => '',
                'fechaA' => '',
                'obsA' => '',
                'estado' => ''
        ));
    }

    function TrabajoEquiposGet($_cs, $_paso) {
        return $this->_QUERY("SELECT EQU000.id, EQU000.codigo, EQU000.nombre
		FROM INV036 INNER JOIN EQU000 ON INV036.equipo = EQU000.id
		WHERE (INV036.hoja = '{$_cs}') AND (INV036.paso = '{$_paso}')
		ORDER BY EQU000.nombre;");
    }

    function TrabajoInsumosGet($_id, $_paso) {
        return $this->_QUERY("SELECT (INV002.nombre+'('+INV001.nombre+')') AS nombre, INV003.id, INV003.producto AS padre, INV003.codigo, INV037.cantidad
		FROM INV002 INNER JOIN INV003 ON INV002.id = INV003.producto INNER JOIN INV037 ON INV003.id = INV037.inventario INNER JOIN INV001 ON INV002.presentacion = INV001.id
		WHERE (INV037.hoja = '{$_id}') AND (INV037.paso = '{$_paso}');");
    }

    function TrabajoEstado($_var) {
        if ($_var == 'r')
            return 'Pendiente';
        elseif ($_var == 'a')
            return 'Anulada';
        elseif ($_var == 'f')
            return 'Finalizada';
        else
            return 'N/A';
    }

    function TrabajoMuestrasGet($_id, $_tipo = '-1') {
        if ($_tipo == '-1')
            $op = '<>';
        else
            $op = '=';

        return $this->_QUERY("SELECT muestra AS codigo
		FROM INV034
		WHERE (hoja = '{$_id}') AND (tipo {$op} {$_tipo})
		ORDER BY muestra;");
    }

    function TrabajoMuestrasVacio() {
        return array(0 => array(
                'codigo' => ''
        ));
    }

    function TrabajoPasoASet($_accion, $_cs, $_procedimiento, $_obsA, $_A1cod_analisis, $_A2cod_analisis, $_A3cod_analisis, $_muestras, $_UID) {
        $_obsA = $this->Free($_obsA);
        if ($_accion == 'R')
            $_accion = 'I';
        if ($_accion == 'I')
            $_cs = $this->TrabajoCsGet();

        if ($this->_TRANS("EXECUTE PA_MAG_121 '{$_cs}', '{$_procedimiento}', '{$_obsA}', '{$_UID}', '{$_accion}';")) {
            if ($_accion != 'D') {
                $_A1cod_analisis = explode('&', substr($_A1cod_analisis, 1));
                $_A2cod_analisis = explode('&', substr($_A2cod_analisis, 1));
                $_A3cod_analisis = explode('&', substr($_A3cod_analisis, 1));
                $_muestras = explode('&', substr($_muestras, 1));

                for ($i = 0; $i < count($_A1cod_analisis); $i++) {
                    if (str_replace(' ', '', $_A1cod_analisis[$i]) != '')
                        $this->_TRANS("INSERT INTO INV033 VALUES('{$_cs}', '1', '{$_A1cod_analisis[$i]}');");
                }
                for ($i = 0; $i < count($_A2cod_analisis); $i++) {
                    if (str_replace(' ', '', $_A2cod_analisis[$i]) != '')
                        $this->_TRANS("INSERT INTO INV033 VALUES('{$_cs}', '2', '{$_A2cod_analisis[$i]}');");
                }
                for ($i = 0; $i < count($_A3cod_analisis); $i++) {
                    if (str_replace(' ', '', $_A3cod_analisis[$i]) != '')
                        $this->_TRANS("INSERT INTO INV033 VALUES('{$_cs}', '3', '{$_A3cod_analisis[$i]}');");
                }
                for ($i = 0; $i < count($_muestras); $i++) {
                    if (str_replace(' ', '', $_muestras[$i]) != '')
                        $this->_TRANS("INSERT INTO INV034 VALUES('{$_cs}', '{$_muestras[$i]}', 0);");
                }
            }
            return 1;
        } else
            return 0;
    }

    function TrabajoPasoBSet($_cs, $_fechaB, $_tempB, $_humedadB, $_obsB, $_UID) {
        $_fechaB = $this->_FECHA($_fechaB);
        $_obsB = $this->Free($_obsB);
        $this->_TRANS("UPDATE INV032 SET fechaB = '{$_fechaB}', tempB = '{$_tempB}', humedadB = '{$_humedadB}', obsB = '{$_obsB}' WHERE (id = '{$_cs}');");
        $this->_TRANS("DELETE FROM INV035 WHERE (hoja='{$_cs}') AND (paso='B');");
        $this->_TRANS("INSERT INTO INV035 VALUES ('{$_cs}', 'B', {$_UID}, GETDATE());");
        return 1;
    }

    function TrabajoPasoCSet($_cs, $_tempC, $_humedadC, $_obsC, $_C1cod_insumos, $_C1total, $_C2cod_equipos, $_C3cod_insumos, $_UID) {
        $_obsC = $this->Free($_obsC);
        $this->_TRANS("UPDATE INV032 SET tempC = '{$_tempC}', humedadC = '{$_humedadC}', obsC = '{$_obsC}' WHERE (id = '{$_cs}');");
        $this->_TRANS("DELETE FROM INV026 WHERE (hoja='{$_cs}');");
        $this->_TRANS("DELETE FROM INV035 WHERE (hoja='{$_cs}') AND (paso='C');");
        $this->_TRANS("DELETE FROM INV036 WHERE (hoja='{$_cs}') AND (paso='C');");
        $this->_TRANS("DELETE FROM INV037 WHERE (hoja='{$_cs}') AND (paso='C');");
        $_C1cod_insumos = explode('&', substr($_C1cod_insumos, 1));
        $_C1total = explode('&', substr($_C1total, 1));
        $_C2cod_equipos = explode('&', substr($_C2cod_equipos, 1));
        $_C3cod_insumos = explode('&', substr($_C3cod_insumos, 1));

        for ($i = 0; $i < count($_C1cod_insumos); $i++) {
            if (str_replace(' ', '', $_C1cod_insumos[$i]) != '')
                $this->_TRANS("INSERT INTO INV037 VALUES('{$_cs}', 'C', {$_C1cod_insumos[$i]}, {$_C1total[$i]});");
        }
        for ($i = 0; $i < count($_C2cod_equipos); $i++) {
            if (str_replace(' ', '', $_C2cod_equipos[$i]) != '')
                $this->_TRANS("INSERT INTO INV036 VALUES('{$_cs}', 'C', {$_C2cod_equipos[$i]});");
        }
        for ($i = 0; $i < count($_C3cod_insumos); $i++) {
            if (str_replace(' ', '', $_C3cod_insumos[$i]) != '')
                $this->_TRANS("INSERT INTO INV026 VALUES('{$_cs}', '{$_C3cod_insumos[$i]}');");
        }
        $this->_TRANS("INSERT INTO INV035 VALUES ('{$_cs}', 'C', {$_UID}, GETDATE());");

        return 1;
    }

    function TrabajoPasoDSet($_cs, $_tempD, $_humedadD, $_normalizada, $_analito, $_unidad, $_obsD, $_Dmuestra, $_CAN, $_coef1, $_coef2, $_vol1, $_D1cod_insumos, $_D1total, $_D2cod_equipos, $_UID) {
        $_obsD = $this->Free($_obsD);
        $this->_TRANS("UPDATE INV032 SET tempD = '{$_tempD}', humedadD = '{$_humedadD}', normalizada = {$_normalizada}, analito = '{$_analito}', unidad = '{$_unidad}', obsD = '{$_obsD}' WHERE (id = '{$_cs}');");
        $this->_TRANS("DELETE FROM INV035 WHERE (hoja='{$_cs}') AND (paso='D');");
        $this->_TRANS("DELETE FROM INV036 WHERE (hoja='{$_cs}') AND (paso='D');");
        $this->_TRANS("DELETE FROM INV037 WHERE (hoja='{$_cs}') AND (paso='D');");
        $this->_TRANS("DELETE FROM INV044 WHERE (hoja='{$_cs}');");
        //
        $_Dmuestra = explode('&', substr($_Dmuestra, 1));
        $_CAN = explode('&', substr($_CAN, 1));
        $_coef1 = explode('&', substr($_coef1, 1));
        $_coef2 = explode('&', substr($_coef2, 1));
        $_vol1 = explode('&', substr($_vol1, 1));
        $_D1cod_insumos = explode('&', substr($_D1cod_insumos, 1));
        $_D1total = explode('&', substr($_D1total, 1));
        $_D2cod_equipos = explode('&', substr($_D2cod_equipos, 1));
        //
        for ($i = 0; $i < count($_Dmuestra); $i++) {
            $this->_TRANS("INSERT INTO INV044 VALUES('{$_cs}', '{$_Dmuestra[$i]}', {$_CAN[$i]}, {$_coef1[$i]}, {$_coef2[$i]}, {$_vol1[$i]});");
        }
        //
        for ($i = 0; $i < count($_D1cod_insumos); $i++) {
            if (str_replace(' ', '', $_D1cod_insumos[$i]) != '')
                $this->_TRANS("INSERT INTO INV037 VALUES('{$_cs}', 'D', {$_D1cod_insumos[$i]}, {$_D1total[$i]});");
        }
        for ($i = 0; $i < count($_D2cod_equipos); $i++) {
            if (str_replace(' ', '', $_D2cod_equipos[$i]) != '')
                $this->_TRANS("INSERT INTO INV036 VALUES('{$_cs}', 'D', {$_D2cod_equipos[$i]});");
        }
        //
        $this->_TRANS("INSERT INTO INV035 VALUES ('{$_cs}', 'D', {$_UID}, GETDATE());");
        return 1;
    }

    function TrabajoPasoESet($_cs, $_fechaE, $_tempE, $_humedadE, $_reacciones, $_programaE, $_nombreE, $_obsE, $_E1tipo, $_E1descr, $_E1insumos, $_E1concS, $_E1concF, $_E1volumen, $_E1reacciones, $_E2muestras, $_E2cod_equipos, $_control, $_UID) {
        $_fechaE = $this->_FECHA($_fechaE);
        $_programaE = $this->Free($_programaE);
        $_nombreE = $this->Free($_nombreE);
        $_obsE = $this->Free($_obsE);

        $this->_TRANS("UPDATE INV032 SET fechaPcr = '{$_fechaE}', tempE = '{$_tempE}', humedadE = '{$_humedadE}', reaccionesE = {$_reacciones}, programaE = '{$_programaE}', nombreE = '{$_nombreE}', obsE = '{$_obsE}' WHERE (id = '{$_cs}');");
        $this->_TRANS("DELETE FROM INV034 WHERE (hoja='{$_cs}') AND (tipo=1);"); //muestras adicionales
        $this->_TRANS("DELETE FROM INV035 WHERE (hoja='{$_cs}') AND (paso='E');"); //bitacora
        $this->_TRANS("DELETE FROM INV036 WHERE (hoja='{$_cs}') AND (paso='E');"); //equipos
        $this->_TRANS("DELETE FROM INV038 WHERE (hoja='{$_cs}');"); //controles
        $this->_TRANS("DELETE FROM INV043 WHERE (hoja='{$_cs}') AND (paso='E');"); //plantilla
        //
		$_E1tipo = explode('&', substr($_E1tipo, 1));
        $_E1descr = explode('&', substr($_E1descr, 1));
        $_E1insumos = explode('&', substr($_E1insumos, 1));
        $_E1concS = explode('&', substr($_E1concS, 1));
        $_E1concF = explode('&', substr($_E1concF, 1));
        $_E1volumen = explode('&', substr($_E1volumen, 1));
        $_E1reacciones = explode('&', substr($_E1reacciones, 1));
        $_E2muestras = explode('&', substr($_E2muestras, 1));
        $_E2cod_equipos = explode('&', substr($_E2cod_equipos, 1));
        $_control = explode('&', substr($_control, 1));

        for ($i = 0; $i < count($_E1tipo); $i++) {
            if (str_replace(' ', '', $_E1tipo[$i]) != '' && str_replace(' ', '', $_E1insumos[$i]) != '') {
                $_E1descr[$i] = $this->Free($_E1descr[$i]);
                $_E1concS[$i] = $this->Free($_E1concS[$i]);
                $_E1concF[$i] = $this->Free($_E1concF[$i]);
                $this->_TRANS("INSERT INTO INV043 VALUES('{$_cs}', {$i}, '{$_E1tipo[$i]}', '{$_E1descr[$i]}', '{$_E1insumos[$i]}', '{$_E1concS[$i]}', '{$_E1concF[$i]}', '{$_E1volumen[$i]}', '{$_E1reacciones[$i]}', 'E');");
            }
        }
        for ($i = 0; $i < count($_E2muestras); $i++) {
            if (str_replace(' ', '', $_E2muestras[$i]) != '')
                $this->_TRANS("INSERT INTO INV034 VALUES('{$_cs}', '{$_E2muestras[$i]}', NULL, 1);");
        }
        for ($i = 0; $i < count($_E2cod_equipos); $i++) {
            if (str_replace(' ', '', $_E2cod_equipos[$i]) != '')
                $this->_TRANS("INSERT INTO INV036 VALUES('{$_cs}', 'E', {$_E2cod_equipos[$i]});");
        }
        for ($i = 0; $i < count($_control); $i++) {
            $_control[$i] = str_replace(' ', '', $_control[$i]);
            if ($_control[$i] != '')
                $this->_TRANS("INSERT INTO INV038 VALUES('{$_cs}', '{$_control[$i]}', '');");
        }
        //
        $this->_TRANS("INSERT INTO INV035 VALUES ('{$_cs}', 'E', {$_UID}, GETDATE());");
        return 1;
    }

    function TrabajoPasoFSet($_cs, $_reaccionesF, $_Famplicon1, $_Famplicon2, $_programaF, $_obsF, $_F1tipo, $_F1descr, $_F1insumos, $_F1volumen, $_F1reacciones, $_F2cod_equipos, $_UID) {
        $_programaF = $this->Free($_programaF);
        $_obsF = $this->Free($_obsF);
        $this->_TRANS("UPDATE INV032 SET reaccionesF = {$_reaccionesF}, Famplicon1 = '{$_Famplicon1}', Famplicon2 = '{$_Famplicon2}', programaF = '{$_programaF}', obsF = '{$_obsF}' WHERE (id = '{$_cs}');");
        $this->_TRANS("DELETE FROM INV035 WHERE (hoja='{$_cs}') AND (paso='F');"); //bitacora
        $this->_TRANS("DELETE FROM INV036 WHERE (hoja='{$_cs}') AND (paso='F');"); //equipos
        $this->_TRANS("DELETE FROM INV043 WHERE (hoja='{$_cs}') AND (paso='F');"); //plantilla
        //
		$_F1tipo = explode('&', substr($_F1tipo, 1));
        $_F1descr = explode('&', substr($_F1descr, 1));
        $_F1insumos = explode('&', substr($_F1insumos, 1));
        $_F1volumen = explode('&', substr($_F1volumen, 1));
        $_F1reacciones = explode('&', substr($_F1reacciones, 1));
        $_F2cod_equipos = explode('&', substr($_F2cod_equipos, 1));

        for ($i = 0; $i < count($_F1tipo); $i++) {
            if (str_replace(' ', '', $_F1tipo[$i]) != '' && str_replace(' ', '', $_F1insumos[$i]) != '') {
                $_F1descr[$i] = $this->Free($_F1descr[$i]);
                $this->_TRANS("INSERT INTO INV043 VALUES('{$_cs}', {$i}, '{$_F1tipo[$i]}', '{$_F1descr[$i]}', '{$_F1insumos[$i]}', NULL, NULL, '{$_F1volumen[$i]}', '{$_F1reacciones[$i]}', 'F');");
            }
        }
        for ($i = 0; $i < count($_F2cod_equipos); $i++) {
            if (str_replace(' ', '', $_F2cod_equipos[$i]) != '')
                $this->_TRANS("INSERT INTO INV036 VALUES('{$_cs}', 'F', {$_F2cod_equipos[$i]});");
        }

        $this->_TRANS("INSERT INTO INV035 VALUES ('{$_cs}', 'F', {$_UID}, GETDATE());");
        //
        return 1;
    }

    function TrabajoPasoGSet($_cs, $_tempG, $_humedadG, $_tiempoG, $_voltajeG, $_obsG, $_G1cod_insumos, $_G1total, $_G2cod_equipos, $_UID) {
        $_obsG = $this->Free($_obsG);
        $this->_TRANS("UPDATE INV032 SET tempG = '{$_tempG}', humedadG = '{$_humedadG}', tiempoG = '{$_tiempoG}', voltajeG = '{$_voltajeG}', obsG = '{$_obsG}' WHERE (id = '{$_cs}');");
        $this->_TRANS("DELETE FROM INV035 WHERE (hoja='{$_cs}') AND (paso='G');");
        $this->_TRANS("DELETE FROM INV036 WHERE (hoja='{$_cs}') AND (paso='G');");
        $this->_TRANS("DELETE FROM INV037 WHERE (hoja='{$_cs}') AND (paso='G');");
        $_G1cod_insumos = explode('&', substr($_G1cod_insumos, 1));
        $_G1total = explode('&', substr($_G1total, 1));
        $_G2cod_equipos = explode('&', substr($_G2cod_equipos, 1));

        for ($i = 0; $i < count($_G1cod_insumos); $i++) {
            if (str_replace(' ', '', $_G1cod_insumos[$i]) != '')
                $this->_TRANS("INSERT INTO INV037 VALUES('{$_cs}', 'G', {$_G1cod_insumos[$i]}, {$_G1total[$i]});");
        }
        for ($i = 0; $i < count($_G2cod_equipos); $i++) {
            if (str_replace(' ', '', $_G2cod_equipos[$i]) != '')
                $this->_TRANS("INSERT INTO INV036 VALUES('{$_cs}', 'G', {$_G2cod_equipos[$i]});");
        }
        $this->_TRANS("INSERT INTO INV035 VALUES ('{$_cs}', 'G', {$_UID}, GETDATE());");
        //
        return 1;
    }

    function TrabajoPasoRSet($_cs, $_obsR, $_control, $_cumple, $_muestra, $_analisis, $_result, $_incluir, $_UID) {
        $_obsR = $this->FreePost($_obsR);
        $estado = 'f';

        $this->_TRANS("DELETE FROM INV038 WHERE (hoja='{$_cs}');");
        $this->_TRANS("DELETE FROM INV039 WHERE (hoja='{$_cs}');");

        if ($_control != '') {
            for ($i = 0; $i < count($_control); $i++) {
                //if($_cumple[$i]=='0') $estado = 'a';
                $this->_TRANS("INSERT INTO INV038 VALUES('{$_cs}', '{$_control[$i]}', '{$_cumple[$i]}');");
            }
        }
        for ($i = 0; $i < count($_muestra); $i++) {
            $this->_TRANS("INSERT INTO INV039 VALUES('{$_cs}', '{$_muestra[$i]}', {$_analisis[$i]}, '{$_result[$i]}');");
        }

        //MUESTRAS PARA ADNoteca
        if ($_incluir != '') {
            $exportar = count($_incluir);
            $ROW = $this->_QUERY("SELECT analito FROM INV032 WHERE (id='{$_cs}');");
            $analito = $ROW[0]['analito'];
            //ANALISIS
            $ROW = $this->TrabajoAnalisisGet($_cs, 1);
            for ($i = 0, $A1 = ''; $i < count($ROW); $i++)
                $A1 .= "&{$ROW[$i]['id']}";
            $ROW = $this->TrabajoAnalisisGet($_cs, 2);
            for ($i = 0, $A2 = ''; $i < count($ROW); $i++)
                $A2 .= "&{$ROW[$i]['id']}";
            $ROW = $this->TrabajoResultadosGet($_cs);

            for ($i = 0; $i < $exportar; $i++) {
                for ($x = 0, $A3 = '', $A4 = ''; $x < count($ROW); $x++) {
                    if ($ROW[$x]['muestra'] == $_incluir[$i]) { //SOLO BUSCA RESULTADOS DE LA MUESTRA SELECCIONADA
                        $A3 .= "&{$ROW[$x]['id']}";
                        $A4 .= "&{$ROW[$x]['resultado']}";
                    }
                }

                $this->ADNoIME('I', $_incluir[$i], $analito, '', '', '-1', '0', '', '0', $_cs, '', '', $A1, $A2, $A3, $A4, '', 'p', $_UID);
            }
        }
        //
        if ($this->_QUERY("SELECT 1 FROM INV035 WHERE (hoja='{$_cs}') AND (paso='R');")) {
            $this->_TRANS("DELETE FROM INV035 WHERE (hoja='{$_cs}') AND (paso='M');");
            $this->_TRANS("INSERT INTO INV035 VALUES ('{$_cs}', 'M', {$_UID}, GETDATE());");
        } else {
            $this->_TRANS("INSERT INTO INV035 VALUES ('{$_cs}', 'R', {$_UID}, GETDATE());");
        }

        $this->_TRANS("UPDATE INV032 SET obsR = '{$_obsR}', estado = '{$estado}' WHERE (id = '{$_cs}');");
        //
        return 1;
    }

    function TrabajoPasoWSet($_cs, $_tiempoW, $_tempW, $_W2cod_equipos, $_obsW, $_UID) {
        $_obsW = $this->Free($_obsW);
        $this->_TRANS("UPDATE INV032 SET tiempoW = '{$_tiempoW}', tempW = '{$_tempW}', obsW = '{$_obsW}' WHERE (id = '{$_cs}');");
        $this->_TRANS("DELETE FROM INV035 WHERE (hoja='{$_cs}') AND (paso='W');");
        $this->_TRANS("DELETE FROM INV036 WHERE (hoja='{$_cs}') AND (paso='W');");
        $_W2cod_equipos = explode('&', substr($_W2cod_equipos, 1));

        for ($i = 0; $i < count($_W2cod_equipos); $i++) {
            if (str_replace(' ', '', $_W2cod_equipos[$i]) != '')
                $this->_TRANS("INSERT INTO INV036 VALUES('{$_cs}', 'W', {$_W2cod_equipos[$i]});");
        }

        $this->_TRANS("INSERT INTO INV035 VALUES ('{$_cs}', 'W', {$_UID}, GETDATE());");

        return 1;
    }

    function TrabajoPasoXSet($_cs, $_tempX, $_humedadX, $_tiempoX, $_voltajeX, $_X1cod_insumos, $_X1total, $_X2cod_equipos, $_obsX, $_UID) {
        $_obsX = $this->Free($_obsX);
        $this->_TRANS("UPDATE INV032 SET tempX = '{$_tempX}', humedadX = '{$_humedadX}', tiempoX = '{$_tiempoX}', voltajeX = '{$_voltajeX}', obsX = '{$_obsX}' WHERE (id = '{$_cs}');");
        $this->_TRANS("DELETE FROM INV035 WHERE (hoja='{$_cs}') AND (paso='X');");
        $this->_TRANS("DELETE FROM INV036 WHERE (hoja='{$_cs}') AND (paso='X');");
        $this->_TRANS("DELETE FROM INV037 WHERE (hoja='{$_cs}') AND (paso='X');");
        $_X1cod_insumos = explode('&', substr($_X1cod_insumos, 1));
        $_X1total = explode('&', substr($_X1total, 1));
        $_X2cod_equipos = explode('&', substr($_X2cod_equipos, 1));

        for ($i = 0; $i < count($_X1cod_insumos); $i++) {
            if (str_replace(' ', '', $_X1cod_insumos[$i]) != '')
                $this->_TRANS("INSERT INTO INV037 VALUES('{$_cs}', 'X', {$_X1cod_insumos[$i]}, {$_X1total[$i]});");
        }
        for ($i = 0; $i < count($_X2cod_equipos); $i++) {
            if (str_replace(' ', '', $_X2cod_equipos[$i]) != '')
                $this->_TRANS("INSERT INTO INV036 VALUES('{$_cs}', 'X', {$_X2cod_equipos[$i]});");
        }

        $this->_TRANS("INSERT INTO INV035 VALUES ('{$_cs}', 'X', {$_UID}, GETDATE());");

        return 1;
    }

    function TrabajoPlantillaGet($_cs, $_paso) {
        if ($_paso == 'D') {
            $ROW = $this->_QUERY("SELECT muestra, CAN, coef1, coef2, vol1
			FROM INV044 WHERE (hoja = '{$_cs}') ORDER BY muestra;");
            if ($ROW)
                return $ROW;
            //SI NO SE HAN METIDO DATOS DE DILUCION, SE DEVUELVE EL VECTOR DE MUESTRAS ORIGINALES
            return $this->_QUERY("SELECT muestra, 0 AS CAN, 0 AS coef1, 0 AS coef2, 0 AS vol1
			FROM INV034 WHERE (hoja = '{$_cs}') ORDER BY muestra;");
        } else
            return $this->_QUERY("SELECT tipo, descr, codigo, concS, concF, volumen, reacciones
			FROM INV043
			WHERE (hoja = '{$_cs}') AND (paso='{$_paso}')
			ORDER BY tipo, codigo;");
    }

    function TrabajoResumenGet($_estado, $_proc) {
        if ($_estado == '')
            $op = '<>';
        else
            $op = '=';

        return $this->_QUERY("SELECT INV032.id, INV032.procedimiento, INV032.estado, INV035.analista
		FROM INV032 INNER JOIN INV035 ON INV032.id = INV035.hoja
		WHERE (INV035.paso = 'A') AND (INV032.procedimiento LIKE '%{$_proc}%') AND (INV032.estado {$op} '{$_estado}')
		ORDER BY id;");
    }

    function TrabajoResultadosGet($_cs) {
        return $this->_QUERY("SELECT INV039.muestra, INV039.analisis AS id, INV039.resultado, VAR005.nombre AS analisis
		FROM INV039 INNER JOIN VAR005 ON INV039.analisis = VAR005.id
		WHERE (INV039.hoja = '{$_cs}')
		ORDER BY INV039.muestra, VAR005.nombre;");
    }

    function TrabajoResultadosVacio($_cs) {
        $ROW = array();
        $ROW1 = $this->TrabajoMuestrasGet($_cs);
        $ROW2 = $this->TrabajoAnalisisGet($_cs, '3');
        for ($x = 0, $i = 0; $x < count($ROW1); $x++) {
            for ($y = 0; $y < count($ROW2); $y++) {
                $ROW[$i] = array('muestra' => $ROW1[$x]['codigo'], 'id' => $ROW2[$y]['id'], 'analisis' => $ROW2[$y]['nombre'], 'resultado' => '');
                $i++;
            }
        }
        return $ROW;
    }

    function TrabajoTemporalDel($_cs) {
        return $this->_TRANS("DELETE FROM INV044 WHERE (hoja='{$_cs}');");
    }

    function TrabajoTemporalSet($_cs, $_Dmuestra, $_CAN, $_coef1, $_coef2) {
        $this->_TRANS("INSERT INTO INV044 VALUES('{$_cs}', '{$_Dmuestra}', {$_CAN}, {$_coef1}, {$_coef2}, 0);");
    }

    function camaras_IME($_accion, $_id, $_descripcion, $_estado) {
        if ($_accion == 'I') {
            return $this->_TRANS("INSERT INTO BAN005 (id, descripcion, estado) VALUES({$_id}, '{$_descripcion}', '{$_estado}');");
        } else if ($_accion == 'M') {
            return $this->_TRANS("UPDATE BAN005 SET descripcion='{$_descripcion}', estado='{$_estado}' WHERE id={$_id};");
        }else if($_accion=='E'){
            
        }
    }

    function camaras_get($_id = '') {
        $str = "SELECT id, descripcion, estado FROM BAN005 WHERE 1=1";
        if ($_id != '') {
            $str .= " AND id={$_id}";
        }
        return $this->_QUERY($str);
    }

    function camaras_get_vacio() {
        return [['id' => '', 'descripcion' => '', 'estado' => '1']];
    }

    function bandeja_ime($_accion, $_id_camara, $_linea = '', $_descripcion = '', $_dimensionx = '', $_dimensiony = '', $_rotulox = '', $_rotuloy = '') {
        if ($_accion == "I") {
            return $this->_TRANS("INSERT INTO BAN006 (id_camara, linea, descripcion, dimensionx, dimensiony, rotulox, rotuloy) VALUES($_id_camara, $_linea, '$_descripcion', $_dimensionx, $_dimensiony, '$_rotulox', '$_rotuloy');");
        } elseif ($_accion == "E") {
            $cajas=$this->_QUERY("SELECT id FROM BAN007 WHERE id_camara=$_id_camara AND bandeja_linea=$_linea");
            foreach ($cajas as $caja){
                $this->_TRANS("DELETE FROM BAN008 WHERE id_caja={$caja['id']}");
                $this->_TRANS("DELETE FROM BAN007 WHERE id={$caja['id']}");
            }
            return $this->_TRANS("DELETE FROM BAN006 WHERE id_camara=$_id_camara AND linea=$_linea");
        } elseif($_accion=='M'){
            return $this->_TRANS("UPDATE BAN006 SET descripcion='{$_descripcion}', dimensionx={$_dimensionx}, dimensiony={$_dimensiony}, rotulox='{$_rotulox}', rotuloy='{$_rotuloy}' WHERE id_camara={$_id_camara} AND linea={$_linea};");
        }
    }
    
    function bandeja_max($_id_camara){
        return $this->_QUERY("SELECT  MAX(linea)+1 as linea FROM BAN006 WHERE id_camara={$_id_camara}");
    }

    function bandeja_get($_id_camara) {
        $str = "SELECT id_camara, linea, descripcion, dimensionx, dimensiony, rotulox, rotuloy FROM BAN006 WHERE id_camara=$_id_camara;";
        return $this->_QUERY($str);
    }

    function caja_ime($_accion, $_id, $_id_camara, $_bandeja_linea, $_nombre,$_ubicacion) {
        if ($_accion == 'I') {
            return $this->_TRANS("INSERT INTO BAN007(id, id_camara, bandeja_linea, nombre,ubicacion) VALUES({$_id}, {$_id_camara}, {$_bandeja_linea}, '{$_nombre}','{$_ubicacion}');");
        } else if ($_accion == 'M') {
            return $this->_TRANS("UPDATE BAN007 SET nombre='{$_nombre}', ubicacion='{$_ubicacion}' WHERE id={$_id};");
        }
    }

    function caja_get($_id='',$id_camara='',$_bandeja_linea='') {
        $str = "SELECT id, id_camara, bandeja_linea, nombre,ubicacion FROM BAN007 WHERE 1=1";
        if($_id!=''){
            $str.=" AND id={$_id}";
        }
        if($id_camara!=''){
            $str.=" AND id_camara={$id_camara}";
        }
        if($_bandeja_linea!=''){
            $str.=" AND bandeja_linea={$_bandeja_linea}";
        }
        return $this->_QUERY($str);
    }

    function caja_linea_get($_id) {
        $str = "SELECT BAN008.id_plaga, VAR005.nombre FROM BAN008 INNER JOIN VAR005 ON BAN008.id_plaga=VAR005.id WHERE BAN008.id_caja={$_id}";
        return $this->_QUERY($str);
    }
    
    function plajas_cajas_get(){
       $str="SELECT BAN008.id_caja, VAR005.id, VAR005.nombre,BAN007.nombre as nombrecaja  FROM VAR005 
                    LEFT JOIN  BAN008  ON BAN008.id_plaga=VAR005.id
                    LEFT JOIN BAN007 ON BAN008.id_caja=BAN007.id
                    WHERE VAR005.tipo='6' AND VAR005.lab='2'
                    ORDER BY VAR005.id DESC"; 
        return $this->_QUERY($str);
    }

    function caja_linea_IME($_accion, $_id_caja, $_id_plaga) {
        if ($_accion == 'I') {
            return $this->_TRANS("INSERT INTO BAN008 (id_caja, id_plaga) VALUES({$_id_caja}, {$_id_plaga});");
        } else if ($_accion == 'E') {
            return $this->_TRANS("DELETE FROM BAN008 WHERE id_caja={$_id_caja} AND id_plaga={$_id_plaga}");
        }
    }

    function plaga_get($_tipo, $_LID, $_idcaja) {
        return $this->_QUERY("SELECT id, nombre
			FROM VAR005 
			WHERE (tipo = '{$_tipo}') AND (lab = '{$_LID}') AND id NOT IN (SELECT id_plaga FROM BAN008 WHERE id_caja={$_idcaja})
			ORDER BY nombre;");
    }

    function consecutivo($_campo) {
        $consecutivo = $this->_QUERY("SELECT {$_campo} FROM MAG000;");
        $this->_TRANS("UPDATE MAG000 SET {$_campo}={$_campo}+1;");
        return $consecutivo[0][$_campo];
    }

//
}

?>