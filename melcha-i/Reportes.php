<?php

//CLASE DUMMY REQUERIDA PARA TRAER RECURSOS DE MENSAJERO
class dummy extends Mensajero
{

    function Bienvenida($_ID)
    {
        parent::SetLab($_ID);
    }

}

/* * *****************************************************************
  CLASE DE NIVEL 1 ENCARGADA DEL MODULO DE REPORTES
  (final SOLO PUEDE SER INSTANCIADA Y NO HEREDADA)
 * ***************************************************************** */

final class Reportes extends Database
{

//
    private $_LID = '';
    private $_LNAME = '';

    function __construct()
    {
        //OBTENGO LA SESION DEL LAB ID
        $Robot = new Seguridad();
        if (!$Robot->SesionAuth())
            $this->Err();
        $ROW = $Robot->SesionGet();
        $this->_LID = $ROW['LID'];
        unset($Robot);
        //OBTENGO LA SESION DEL LNAME
        $Robot = new dummy();
        $Robot->Bienvenida($this->_LID);
        $this->_LNAME = $Robot->LabName();
    }

    function AnalisisCategorias($_var)
    {
        if ($_var == '0')
            return 'Impurezas plaguicidas';
        elseif ($_var == '1')
            return 'Elementos fertilizantes';
        elseif ($_var == '2')
            return 'Sub-an�lisis plaguicidas';
        elseif ($_var == '3')
            return 'Ingredientes activos';
        elseif ($_var == '4')
            return 'Impurezas fertilizantes';
        //elseif($_var=='8') return 'Analitos';
        elseif ($_var == '9')
            return 'An�lisis';
    }

    function Mant_Jefe($_id, $_estado = '')
    {
        if ($_id == '') {
            if ($_estado != '') {
                return $this->_QUERY("SELECT id, nombre, cargo, estado, obs FROM INF006 WHERE estado = '{$_estado}';");
            } else {
                return $this->_QUERY("SELECT id, nombre, cargo, estado, obs FROM INF006 WHERE 1=1;");
            }
        } else {
            return $this->_QUERY("SELECT id, nombre, cargo, estado, obs FROM INF006 WHERE id = '{$_id}';");
        }
    }

    function Mant_JefeIME($_id, $_accion, $_nombre, $_cargo, $_estado, $_obs)
    {
        if ($_accion == 'I') {
            $Q = $this->_QUERY("SELECT MAX(id)+1 AS cs FROM INF006");
            $this->_TRANS("UPDATE INF006 SET estado = '0';");
            $this->_TRANS("INSERT INTO INF006 VALUES('{$Q[0]['cs']}', '{$_nombre}', '{$_cargo}', '{$_estado}', '{$_obs}');");
            //
        } elseif ($_accion == 'M') {
            $this->_TRANS("UPDATE INF006 SET nombre = '{$_nombre}', cargo = '{$_cargo}', estado = '{$_estado}', '{$_obs}' WHERE id = '{$_id}';");
        } else {
            $this->_TRANS("DELETE FROM INF006 WHERE id = '{$_id}';");
        }
        if ($_accion == 'I') {
            $_accion = 'A003';
        } elseif ($_accion == 'M')
            $_accion = 'A004';
        else {
            $_accion = 'A005';
        }
        $this->Logger($_accion);
        //
        return 1;
    }

    function Mant_JefeVacio()
    {
        return array(0 => array(
            'id' => '0',
            'nombre' => '',
            'cargo' => '',
            'estado' => '',
            'obs' => ''
        ));
    }

    function AnalisisExtLab($_lab, $_tipo, $_proveedor)
    {
        if ($_tipo == '')
            $op1 = '<>';
        else
            $op1 = '=';

        return $this->_QUERY("SELECT MAN004.nombre, MAN004.tipo, MAN004.descripcion, MAN003.nombre AS proveedor
		FROM MAN003 INNER JOIN MAN004 ON MAN003.cs = MAN004.proveedor 
		WHERE (MAN004.lab='{$_lab}') AND (MAN004.tipo {$op1} '{$_tipo}') AND (MAN003.nombre LIKE '%{$_proveedor}%')
		ORDER BY MAN004.tipo;");
    }

    function AnalisisExtTipo($_var)
    {
        if ($_var == '1')
            return 'An�lisis Ambiental';
        elseif ($_var == '2')
            return 'Salud Ocupacional';
        elseif ($_var == '3')
            return 'Otro';
    }

    function AnalisisHistorial($_lab, $_desde, $_hasta, $_analisis, $_estado = '')
    {
        if ($_analisis == '-1' || $_analisis == '-2' || $_analisis == '-3')
            $op1 = '<>';
        else
            $op1 = '=';
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        $sql_extra = '';


        if ($_analisis == '25' || $_analisis == '24') {
            $_analisis = $_analisis == '25' ? 'cadmio' : 'plomo';
            return $this->_QUERY("SELECT '9' AS categoria,MUE002.ingrediente AS analisis,MUE000.solicitud,SER003.tipo,CONVERT(VARCHAR, MUE002.fecha, 105) AS fecha1,MUE001.estado,MUE001.muestra FROM MUE002 
INNER JOIN MUE001 ON MUE002.xanalizar=MUE001.id
INNER JOIN MUE000 ON MUE001.muestra=MUE000.id
INNER JOIN SER003 ON SER003.cs=MUE000.solicitud WHERE MUE002.ingrediente  = '{$_analisis}' AND (MUE002.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')");
        }


        if ($_lab == '1') {
            if ($_estado != '') {
                $sql_extra .= " AND SER003.estado = '{$_estado}'";
            }
            return $this->_QUERY("SELECT '' AS tipo, SER001.cs AS solicitud, CONVERT(VARCHAR, SER001.fecha, 105) AS fecha1, SER001.estado, SER002.analisis AS examen, VAR005.nombre AS analisis, VAR005.tipo AS categoria
                    FROM SER001 INNER JOIN SER002 ON SER001.cs = SER002.solicitud INNER JOIN VAR005 ON SER002.analisis = VAR005.id
                    WHERE (VAR005.lab = '{$_lab}') AND (SER002.analisis {$op1} {$_analisis}) AND (SER001.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')" . $sql_extra .
                "ORDER BY VAR005.nombre, SER001.fecha, SER001.cs;");
        } elseif ($_lab == '3') {
            if ($_estado != '') {
                $sql_extra .= " AND SER003.estado = '{$_estado}'";
            }
            if ($_analisis == '-2') {
                return $this->_QUERY("SELECT SER003.cs AS solicitud, CONVERT(VARCHAR, SER003.fecha, 105) AS fecha1, SER003.tipo, SER003.estado, SER004.analisis AS examen, VAR005.nombre AS analisis, VAR005.tipo AS categoria
			FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR005 ON SER004.analisis = VAR005.id
			WHERE (VAR005.lab = '{$_lab}') AND (SER004.analisis {$op1} {$_analisis}) AND (SER003.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00') AND SER003.tipo = '1'" . $sql_extra .
                    "ORDER BY VAR005.nombre, SER003.fecha, SER003.cs;");
            } elseif ($_analisis == '-3') {
                return $this->_QUERY("SELECT SER003.cs AS solicitud, CONVERT(VARCHAR, SER003.fecha, 105) AS fecha1, SER003.tipo, SER003.estado, SER004.analisis AS examen, VAR005.nombre AS analisis, VAR005.tipo AS categoria
			FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR005 ON SER004.analisis = VAR005.id
			WHERE (VAR005.lab = '{$_lab}') AND (SER004.analisis {$op1} {$_analisis}) AND (SER003.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00') AND SER003.tipo = '2'" . $sql_extra .
                    "ORDER BY VAR005.nombre, SER003.fecha, SER003.cs;");
            } else {
                return $this->_QUERY("SELECT SER003.cs AS solicitud, CONVERT(VARCHAR, SER003.fecha, 105) AS fecha1, SER003.tipo, SER003.estado, SER004.analisis AS examen, VAR005.nombre AS analisis, VAR005.tipo AS categoria
			FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR005 ON SER004.analisis = VAR005.id
			WHERE (VAR005.lab = '{$_lab}') AND (SER004.analisis {$op1} {$_analisis}) AND (SER003.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')" . $sql_extra .
                    "ORDER BY VAR005.nombre, SER003.fecha, SER003.cs;");
            }
        }
    }

    function AnalisisHistorial2($_lab, $_desde, $_hasta, $_analisis)
    {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        if ($_lab == '3') {
            if ($_analisis == '-2') {
                return $this->_QUERY("SELECT SER003.cs AS solicitud, CONVERT(VARCHAR, SER003.fecha, 105) AS fecha1, SER003.tipo, SER003.estado, SER004.analisis AS examen, VAR005.nombre AS analisis, VAR005.tipo AS categoria
			FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR005 ON SER004.analisis = VAR005.id
			WHERE (VAR005.lab = '{$_lab}') AND (VAR005.tipo != 8) AND (VAR005.tipo != 2) AND (SER003.tipo = '1') AND (SER003.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00') AND (SER003.estado = '7')
			ORDER BY VAR005.nombre, SER003.fecha, SER003.cs;");
            } elseif ($_analisis == '-3') {
                return $this->_QUERY("SELECT SER003.cs AS solicitud, CONVERT(VARCHAR, SER003.fecha, 105) AS fecha1, SER003.tipo, SER003.estado, SER004.analisis AS examen, VAR005.nombre AS analisis, VAR005.tipo AS categoria
			FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR005 ON SER004.analisis = VAR005.id
			WHERE (VAR005.lab = '{$_lab}') AND (VAR005.tipo != 8) AND (VAR005.tipo != 2) AND (SER003.tipo = '2') AND (SER003.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00') AND (SER003.estado = '7')
			ORDER BY VAR005.nombre, SER003.fecha, SER003.cs;");
            } else {
                return $this->_QUERY("SELECT SER003.cs AS solicitud, CONVERT(VARCHAR, SER003.fecha, 105) AS fecha1, SER003.tipo, SER003.estado, SER004.analisis AS examen, VAR005.nombre AS analisis, VAR005.tipo AS categoria
			FROM SER003 INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR005 ON SER004.analisis = VAR005.id
			WHERE (VAR005.lab = '{$_lab}') AND (SER004.analisis = {$_analisis}) AND (SER003.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00') AND (SER003.estado = '7')
			ORDER BY VAR005.nombre, SER003.fecha, SER003.cs;");
            }
        }
    }

    function BDMuestras($_desde, $_hasta)
    {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        return $this->_QUERY("EXECUTE PA_MAG_113 '{$_desde}', '{$_hasta} 23:59:00';");
    }

    function BitacoraAccionesGet()
    {
        return $this->_QUERY("SELECT id, nombre FROM SEG005 ORDER BY id;");
    }

    function BitacoraHistorialGet($_usuario, $_id, $_desde, $_hasta)
    {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        if ($_usuario != '')
            $extra1 = "AND (SEG006.usuario = {$_usuario})";
        else
            $extra1 = '';

        if ($_id != '')
            $extra2 = "AND (SEG005.id = '{$_id}')";
        else
            $extra2 = '';

        return $this->_QUERY("SELECT CONVERT(VARCHAR, SEG006.fecha, 105) AS fecha1, SEG005.nombre AS accion, SEG001.cedula, SEG001.nombre, (SEG001.ap1+' '+SEG001.ap2) AS apellidos
		FROM SEG005 INNER JOIN SEG006 ON SEG005.id = SEG006.accion INNER JOIN SEG001 ON SEG006.usuario = SEG001.id
		WHERE (SEG006.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00') {$extra1} {$extra2} ORDER BY SEG006.fecha;");
    }

    function ContratosLab($_lab, $_tipo, $_desde, $_hasta)
    {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        if ($_tipo == '-1')
            $op1 = '<>';
        else
            $op1 = '=';

        return $this->_QUERY("SELECT MAN003.nombre, MAN008.contrato, CONVERT(VARCHAR, MAN008.fecha, 105) AS fecha, (DATEDIFF(DAY, GETDATE(), MAN008.fecha)) AS plazo, MAN009.nombre AS tipo
		FROM MAN008 INNER JOIN MAN003 ON MAN008.proveedor = MAN003.cs INNER JOIN MAN009 ON MAN008.tipo = MAN009.cs
		WHERE (MAN008.lid='{$_lab}') AND (MAN008.tipo {$op1} {$_tipo}) AND (MAN008.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')
		ORDER BY MAN008.fecha, MAN003.nombre;");
    }

    private function CsTemporal()
    {
        $letras = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $tam = strlen($letras);
        $str = '';
        for ($i = 0; $i < 10; $i++)
            $str .= $letras[rand(0, $tam - 1)];
        return $str;
    }

    private function CsTemporal123($_solicitud, $cod_int)
    {
        $rowia = $this->_QUERY("SELECT numInfo as numero FROM SER002 WHERE solicitud ='{$_solicitud}' and conSolicitud like  '%-$cod_int';");
        return $rowia[0]['numero'];
    }


    function CotizacionEstado($_var)
    {
        if ($_var == '0')
            return 'Generada';
        elseif ($_var == '1')
            return 'Aprobada';
        elseif ($_var == '2')
            return 'Cancelada';
        elseif ($_var == '3')
            return 'Pendiente';
        elseif ($_var == '5')
            return 'Anulada';
        else
            return 'N/A';
    }

    function CotizacionesXcliente($_lab, $_cliente, $_desde, $_hasta, $_estado)
    {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        $_cliente = $this->FreePost($_cliente);

        if ($_estado == '')
            $op1 = '<>';
        else
            $op1 = '=';

        return $this->_QUERY("SELECT COT005.numero, CONVERT(VARCHAR, COT005.fecha, 105) AS fecha1, COT005.solicitante, COT005.moneda, COT005.monto, COT005.tipo, COT005.estado, MAN002.nombre
		FROM COT005 INNER JOIN MAN002 ON COT005.cliente = MAN002.id
		WHERE (COT005.lid = '{$_lab}') AND (COT005.cliente LIKE '%{$_cliente}%') AND (COT005.estado {$op1} '{$_estado}') AND (COT005.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')
		ORDER BY COT005.fecha;");
    }

    function EnsayosMuestra($_lid)
    {
        return $this->_QUERY("SELECT id, nombre FROM MET000 WHERE (lab='{$_lid}') ORDER BY nombre;");
    }

    function obtieneTipo($_solicitud)
    {
        return $this->_QUERY("SELECT tipo FROM SER003 WHERE cs = '{$_solicitud}';");
    }

    function tip($_solicitud)
    {
        return $this->_QUERY("SELECT tipo FROM SER002 WHERE solicitud = '{$_solicitud}';");
    }

    function EnsayosHistorial($_lab, $_desde, $_hasta, $_ensayo, $_tipo, $_cumple)
    {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        if ($_ensayo == '-1')
            $op1 = '<>';
        else
            $op1 = '=';
        if ($_tipo == '')
            $op2 = '<>';
        else
            $op2 = '=';
        if ($_cumple == '') {
            $op3 = 'IS NOT';
            $_cumple = 'NULL';
        } else
            $op3 = '=';

        return $this->_QUERY("SELECT MUE000.solicitud, MET000.nombre, CONVERT(VARCHAR, MUE002.fecha, 105) AS fecha1, MUE002.resultado, MUE002.cumple, (SEG001.nombre +' '+ SEG001.ap1 +' '+ SEG001.ap2) AS analista
		FROM MET000 INNER JOIN MUE002 ON MET000.id = MUE002.tipo INNER JOIN MUE001 ON MUE002.xanalizar = MUE001.id INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN SEG001 ON MUE002.analista = SEG001.id
		WHERE (MET000.lab = '{$_lab}') AND (MET000.id $op1 {$_ensayo}) AND (MET000.dpto {$op2} '{$_tipo}') AND (MUE002.cumple $op3 {$_cumple}) AND (MUE002.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')
		ORDER BY MET000.nombre, MUE002.fecha, MUE000.solicitud;");
    }

    function EquiposContratosGet($_codigo, $_nombre)
    {
        $_lab = $this->_LID;
        $_codigo = $this->FreePost($_codigo);
        $_nombre = $this->FreePost($_nombre);

        return $this->_QUERY("SELECT EQU000.codigo, EQU000.nombre, EQU000.marca, EQU000.serie, MAN008.contrato, MAN009.nombre AS tipo, MAN003.nombre AS proveedor, MAN003.telefono
		FROM EQU000 INNER JOIN MAN010 ON EQU000.id = MAN010.equipo INNER JOIN MAN008 ON MAN010.contrato = MAN008.cs INNER JOIN MAN009 ON MAN008.tipo = MAN009.cs INNER JOIN MAN003 ON MAN008.proveedor = MAN003.cs
		WHERE (EQU000.codigo LIKE '%{$_codigo}%') AND (EQU000.nombre LIKE '%{$_nombre}%')
		ORDER BY EQU000.nombre;");
    }

    function EquiposCronograma($_desde, $_hasta)
    {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        $_lab = $this->_LID;
        return $this->_QUERY("SELECT CONVERT(VARCHAR, EQU006.fecha, 105) AS fecha1, EQU000.codigo, EQU000.nombre, tipo = CASE EQU006.tipo
			WHEN '0' THEN 'Verificaci�n'
			WHEN '1' THEN 'Mantenimiento'
			WHEN '2' THEN 'Calibraci�n'
		END, EQU006.deshabilita, EQU006.observaciones
		FROM EQU006 INNER JOIN EQU000 ON EQU006.equipo = EQU000.id
		WHERE (EQU006.lid = '{$_lab}') AND (EQU006.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')
		ORDER BY EQU006.fecha, EQU006.tipo, EQU000.codigo");
    }

    function EquiposGeneralGet($_nombre, $_control_metro, $_estado)
    {
        $_nombre = $this->FreePost($_nombre);

        $_lab = $this->_LID;
        if ($_control_metro == '-1')
            $op1 = '<>';
        else
            $op1 = '=';
        if ($_estado == '')
            $op2 = '<>';
        else
            $op2 = '=';

        return $this->_QUERY("SELECT EQU000.codigo, EQU000.nombre, EQU000.patrimonio, EQU000.marca, EQU000.modelo, EQU000.serie, EQU000.ubi_equipo, EQU000.control_metro, estado = CASE EQU000.estado
				WHEN '0' THEN 'Deshabilitado'
				WHEN '1' THEN 'Habilitado'
				WHEN '2' THEN 'Almacenado'
				ELSE 'N/A'
			END, MAN003.nombre AS proveedor
		FROM EQU000 INNER JOIN MAN003 ON EQU000.proveedor = MAN003.cs
		WHERE (EQU000.lid = '{$_lab}') AND (EQU000.nombre LIKE '%{$_nombre}%') AND (EQU000.control_metro {$op1} {$_control_metro}) AND (EQU000.estado {$op2} '{$_estado}')
		ORDER BY codigo");
    }

    function EquiposProveedoresGet($_codigo, $_nombre, $_proveedor)
    {
        $_lab = $this->_LID;
        $_codigo = $this->FreePost($_codigo);
        $_nombre = $this->FreePost($_nombre);
        $_proveedor = $this->FreePost($_proveedor);

        return $this->_QUERY("SELECT EQU000.codigo, EQU000.nombre, EQU000.marca, EQU000.serie, MAN003.id, MAN003.nombre AS proveedor, MAN003.telefono, MAN003.fax, MAN003.correo
		FROM EQU000 INNER JOIN MAN003 ON EQU000.proveedor = MAN003.cs
		WHERE (EQU000.codigo LIKE '%{$_codigo}%') AND (EQU000.nombre LIKE '%{$_nombre}%') AND (MAN003.nombre LIKE '%{$_proveedor}%')
		ORDER BY MAN003.nombre");
    }

    function EvaluacionesLab($_lab, $_desde, $_hasta, $_proveedor)
    {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        $_proveedor = $this->FreePost($_proveedor);

        return $this->_QUERY("SELECT MAN003.nombre, CONVERT(VARCHAR, MAN012.fecha, 105) AS fecha1, MAN012.orden, MAN012.calificacion
		FROM MAN012 INNER JOIN MAN003 ON MAN012.proveedor = MAN003.cs
		WHERE (MAN003.lid = '{$_lab}') AND (MAN003.nombre LIKE '%{$_proveedor}%') AND (MAN012.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')
		ORDER BY MAN003.nombre, MAN012.fecha;");
    }

    function Formato($_NUMBER)
    {
        return number_format($_NUMBER, 2, '.', ',');
    }

    function GeneraCombos($_tipo, $_LID)
    {
        $ROW = $this->_QUERY("SELECT id, texto FROM VAR006 WHERE ((clasificacion = '{$_tipo}') AND (lab = '{$_LID}') OR id=15);");
        $str = '<select onchange="muestraMetodosOtros()" name="metodo" style="width:90px"><option value="">....</option>';
        for ($x = 0; $x < count($ROW); $x++) {
            $str .= "<option value='{$ROW[$x]['id']}'>{$ROW[$x]['texto']}</option>";
        }
        $str .= '</select>';
        return $str;
    }
    /*
        function Informe01DetalleGet($_ensayo)
        {
          return $this->_QUERY("SELECT analito, conc, tecnica FROM MET002 WHERE (ensayo='{$_ensayo}') AND (reportar=1) ORDER BY analito;");
        }*/

    /*function Informe01DetalleGet($_ensayo)
    {
    return $this->_QUERY("SELECT MET002.analito, MET002.conc, VAR005.tipoCroma FROM MET002 CROSS JOIN VAR005 where MET002.analito = VAR005.nombre and (ensayo='{$_ensayo}') AND (reportar=1) ORDER BY analito;");
    }*/

    function Informe01DetalleGet($_ensayo, $_idm)
    {
        return $this->_QUERY("SELECT  MET002.analito, MET002.conc, MET002.tecnica, INV065.cuantificador FROM MET002 INNER JOIN INV065 ON MET002.analito = INV065.analito  where  (ensayo='{$_ensayo}')  and  idm = '{$_idm}' and INV065.ida= MET002.id and met002.reportar = 1 ORDER BY analito;");
    }


    function Solicitud01EncabeGet($_cs)
    {

        return $this->_QUERY("SELECT SER001.cs, CONVERT(VARCHAR, SER001.fecha, 105) AS fecha, CONVERT(VARCHAR, fecha, 108) AS hora, SER001.cliente, SER001.nomsolicitante, SER001.entregado, SER001.proposito, SER001.muestreo, SER001.entregacliente, SER001.entregaanalista, SER001.lote, SER001.obs, SER001.estado, SER001.flab, SER001.hlab, SER001.fmuestreo, SER001.hmuestreo, MAN002.nombre AS nomcliente, MAN002.direccion 
		FROM SER001 INNER JOIN MAN002 ON SER001.cliente = MAN002.id 
		WHERE (SER001.cs = '{$_cs}');");
    }


    function Informe01EncabezadoGet($_ID, $_LAB)
    {
        return $this->_QUERY("EXECUTE PA_MAG_101 '{$_ID}', '{$_LAB}';");
    }

    function Informe03DetalleGet($_informe, $_muestra, $_clasificacion)
    {
        

        return $this->_QUERY("SELECT VAR005.nombre AS elemento, MUE002.declarada, MUE002.resultado, MUE002.densidad, MUE002.incertidumbre, MUE002.unidad
		FROM MUE001 INNER JOIN MUE002 ON MUE001.id = MUE002.xanalizar INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN INF001 ON MUE000.id = INF001.muestra INNER JOIN VAR005 ON MUE001.analisis = VAR005.id
		WHERE (MUE000.id = '{$_muestra}') AND (MUE002.cumple = 1) AND (MUE002.clasificacion = '{$_clasificacion}') AND (INF001.informe = '{$_informe}')
		ORDER BY MUE000.ref, VAR005.nombre;");
        /* return $this->_QUERY("SELECT DISTINCT(VAR005.nombre) AS elemento, MUE002.declarada, MUE002.resultado, MUE002.densidad, MUE002.incertidumbre, MUE002.unidad
          FROM MUE001 INNER JOIN MUE002 ON MUE001.id = MUE002.xanalizar INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN INF001 ON MUE000.id = INF001.muestra INNER JOIN VAR005 ON MUE001.analisis = VAR005.id
          WHERE (MUE000.id = '{$_muestra}') AND (MUE002.cumple = 1) AND (MUE002.clasificacion = '{$_clasificacion}') AND (INF001.informe = '{$_informe}');"); */
    }

    function Informe03DetalleGetM($_informe)
    {
        return $this->_QUERY("SELECT metodos FROM INF004 WHERE informe = '{$_informe}' AND linea = '1';");
    }

    function Informe03PlagasDetalleGet($_informe, $_muestra, $_clasificacion)
    {
        return $this->_QUERY("SELECT MUE002.tipo, MUE002.ingrediente, MUE002.declarada, MUE002.resultado, MUE002.densidad, MUE002.incertidumbre, MUE002.unidad, MET000.tecnico, MET000.metodo
		FROM MUE001 INNER JOIN MUE002 ON MUE001.id = MUE002.xanalizar INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN INF001 ON MUE000.id = INF001.muestra INNER JOIN MET000 ON MUE002.tipo = MET000.id
		WHERE (MUE000.id = '{$_muestra}') AND (MUE002.cumple = 1) AND (MUE002.clasificacion = '{$_clasificacion}') AND (INF001.informe = '{$_informe}');");
    }

    function Informe03EncabezadoGet($_ID, $_LAB)
    {
        //error_log("EXECUTE PA_MAG_105 '{$_ID}', '{$_LAB}';");
        return $this->_QUERY("EXECUTE PA_MAG_105 '{$_ID}', '{$_LAB}';");
    }

    function Informe03EquiposGet($_id)
    {
        $_id = 'Informe: ' . str_replace(' ', '', $_id);
        return $this->_QUERY("SELECT EQU000.codigo, EQU000.nombre
		FROM EQU000 INNER JOIN EQUBIT ON EQU000.id = EQUBIT.equipo
		WHERE (EQUBIT.ref = '{$_id}') AND (EQUBIT.accion = 'i');");
    }

    function Informe03MetodosFertilizantesGet($_id)
    {
        return $this->_QUERY("SELECT INF002.linea, INF002.metodo, VAR006.texto, VAR006.resumen
		FROM INF000 INNER JOIN INF002 ON INF000.id = INF002.informe INNER JOIN VAR006 ON INF002.metodo = VAR006.id
		WHERE (INF000.id = '{$_id}')
		ORDER BY INF002.linea;");
    }

    function Informe03MetodosFertilizantesIME($_id, $_metodos, $_metodosotros, $_UID)
    {
        $this->_TRANS("DELETE FROM INF002_Otros WHERE (informe = '{$_id}');");
        $this->_TRANS("DELETE FROM INF002 WHERE (informe = '{$_id}');");
        $j = 1;
        for ($i = 0; $i < count($_metodos); $i++) {
            $this->_TRANS("INSERT INTO INF002(informe,linea,metodo,metodos) VALUES('{$_id}',{$j},{$_metodos[$i]},{$_metodos[$i]})");
            if ($_metodos[$i] == '15') {
                $this->_TRANS("INSERT INTO INF002_Otros(informe,linea,descripcion) VALUES('{$_id}',{$j},'{$_metodosotros[$i]}');");
            }
            $j++;
        }
        $this->_TRANS("INSERT INTO INFBIT VALUES('{$_id}', '{$_UID}', GETDATE(), '3');");
        return 1;
    }

    function Informe03MetodosFertilizantesGetTodos()
    {
        return $this->_QUERY("SELECT id,clasificacion,texto,resumen,lab FROM VAR006 ");
    }

    function Informe03MetodosOtrosGet($_informe, $_linea)
    {
        return $this->_QUERY("SELECT * FROM INF002_Otros WHERE informe='{$_informe}' AND linea={$_linea}");
    }

    function Informe03MetodosPlaguicidasGet($_id)
    {
        return $this->_QUERY("SELECT linea, metodo FROM INF004 WHERE (informe = '{$_id}');");
    }

    function Informe03MetodosPlaguicidasIME($_id, $_metodo, $_metodos, $_UID)
    {
        $this->_TRANS("DELETE FROM INF004 WHERE (informe = '{$_id}');");
        for ($i = 0, $j = 1; $i < count($_metodo); $i++, $j++) {
            $_metodos_actual = ($i < count($_metodos) ? $this->Free($_metodos[$i]) : '');
            $_metodo[$i] = $this->Free($_metodo[$i]);
            $this->_TRANS("INSERT INTO INF004 (informe,linea,metodo,metodos) VALUES ('{$_id}',{$j},'{$_metodo[$i]}','{$_metodos_actual}')");
        }
        $this->_TRANS("INSERT INTO INFBIT VALUES('{$_id}', '{$_UID}', GETDATE(), '3');");
        return 1;
    }

    function Informe03ObservacionesGet($_id)
    {
        return $this->_QUERY("SELECT linea, obs FROM INF003 WHERE (informe = '{$_id}');");
    }

    function InformeAccion($_var)
    {
        if ($_var == '1')
            return 'Generado por:';
        elseif ($_var == '2')
            return 'Aprobado por:';
        elseif ($_var == '3')
            return 'M&eacutetodos actualizados por:';
        elseif ($_var == '4')
            return 'Eliminado por:';
        elseif ($_var == '5')
            return 'Anulado por:';
        else
            return 'N/A';
    }

    function InformeEstado($_var)
    {
        if ($_var == 't')
            return 'Preliminar';
        elseif ($_var == '1')
            return 'Generado';
        elseif ($_var == '2')
            return 'Aprobado';
        elseif ($_var == '5')
            return 'Anulado';
        elseif ($_var == '9')
            return 'Firmado';
        elseif ($_var == 's')
            return 'Sustituido';
        else
            return 'N/A';
    }

    function InformeDisponiblesGet($_lid)
    {
        if ($this->_LID == '1') {
            //LRE

            return $this->_QUERY("SELECT MUE000.id AS muestra, MUE000.solicitud AS cs, CONVERT(VARCHAR, SER001.fecha, 105) AS fecha1, ('Muestra: ' + MUE000.ref) AS obs
			FROM MUE000 INNER JOIN SER001 ON MUE000.solicitud = SER001.cs
			WHERE MUE000.estado = '2'
			ORDER BY MUE000.id;");
        } elseif ($this->_LID == '3') {
            //LCC
            $ROW = $this->_QUERY("SELECT '' AS muestra, cs, CONVERT(VARCHAR, fecha, 105) AS fecha1, '' AS obs
			FROM SER003
			WHERE (estado = '2') AND (cs NOT IN (
				SELECT solicitud FROM MUE000 WHERE (estado!='2') AND (estado!='3')
			) ) ORDER BY fecha;");
            //OBTIENE EL DETALLE
            for ($x = 0; $x < count($ROW); $x++) {
                $ROW2 = $this->_QUERY("EXECUTE PA_MAG_060 '{$ROW[$x]['cs']}';");
                $ROW[$x]['obs'] = $ROW2[0]['obs'];
            }
            return $ROW;
        }
    }

    function InformeHistorial($_id)
    {
        return $this->_QUERY("SELECT SEG001.nombre, SEG001.ap1, CONVERT(VARCHAR, INFBIT.fecha, 105) AS fecha1, INFBIT.accion, SEG002.nombre AS rol
		FROM INFBIT INNER JOIN SEG001 ON INFBIT.usuario = SEG001.id INNER JOIN SEG007 ON SEG001.id = SEG007.usuario INNER JOIN SEG002 ON SEG007.perfil = SEG002.id
		WHERE (INFBIT.informe = '{$_id}');");
    }

    function InformesHistorialGet($_LAB, $_tipo, $_valor, $_desde, $_hasta, $_estado)
    {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        $_valor = str_replace(' ', '', $_valor);

        if ($_estado == '')
            $op = '<>';
        else
            $op = '=';

        if ($_valor != '') { //ESTA BUSCANDO POR VALOR DIRECTO
            if ($_tipo == '0') { //INFORME
                $SQL = "SELECT id, CONVERT(VARCHAR, fecha, 105) AS fecha1, ref, estado, lab
				FROM INF000
				WHERE (lab = '{$_LAB}') AND (id = '{$_valor}');";
            } elseif ($_tipo == '1') { //SOLICITUD 
                $SQL = "SELECT INF000.id, CONVERT(VARCHAR, INF000.fecha, 105) AS fecha1, INF000.ref, INF000.estado, INF000.lab
				FROM INF000 INNER JOIN INF001 ON INF000.id = INF001.informe INNER JOIN MUE000 ON INF001.muestra = MUE000.id
				WHERE (INF000.lab = '{$_LAB}') AND (MUE000.solicitud = '{$_valor}');";
            } elseif ($_tipo == '2') { //MUESTRA
                $SQL = "SELECT INF000.id, CONVERT(VARCHAR, INF000.fecha, 105) AS fecha1, INF000.ref, INF000.estado, INF000.lab
				FROM INF000 INNER JOIN INF001 ON INF000.id = INF001.informe INNER JOIN MUE000 ON INF001.muestra = MUE000.id
				WHERE (INF000.lab = '{$_LAB}') AND (MUE000.ref LIKE '%{$_valor}%');";
            } elseif ($_LAB == '3') { //LCC
                if ($_tipo == '3') { //# DE REGISTRO DE LA MUESTRA
                    $SQL = "SELECT INF000.id, CONVERT(VARCHAR, INF000.fecha, 105) AS fecha1, INF000.ref, INF000.estado, INF000.lab
					FROM INF000 INNER JOIN INF001 ON INF000.id = INF001.informe INNER JOIN MUE000 ON INF001.muestra = MUE000.id INNER JOIN SER003 ON MUE000.solicitud = SER003.cs
					WHERE (INF000.lab = '{$_LAB}') AND (SER003.registro LIKE '%{$_valor}%');";
                } elseif ($_tipo == '4') { //# DE LOTE
                    $SQL = "SELECT INF000.id, CONVERT(VARCHAR, INF000.fecha, 105) AS fecha1, INF000.ref, INF000.estado, INF000.lab
					FROM INF000 INNER JOIN INF001 ON INF000.id = INF001.informe INNER JOIN MUE000 ON INF001.muestra = MUE000.id INNER JOIN SER003 ON MUE000.solicitud = SER003.cs INNER JOIN SER006 ON SER003.cs = SER006.solicitud
					WHERE (INF000.lab = '{$_LAB}') AND (SER006.lote LIKE '%{$_valor}%');";
                } elseif ($_tipo == '5' or $_tipo == '6' or $_tipo == '7') { //ANALISIS
                    $SQL = "SELECT INF000.id, CONVERT(VARCHAR, INF000.fecha, 105) AS fecha1, INF000.ref, INF000.estado, INF000.lab
					FROM INF000 INNER JOIN INF001 ON INF000.id = INF001.informe INNER JOIN MUE000 ON INF001.muestra = MUE000.id INNER JOIN SER003 ON MUE000.solicitud = SER003.cs INNER JOIN SER004 ON SER003.cs = SER004.solicitud INNER JOIN VAR005 ON SER004.analisis = VAR005.id
					WHERE (INF000.lab = '{$_LAB}') AND (VAR005.nombre LIKE '%{$_valor}%');";
                } elseif ($_tipo == '8') { //IMPUREZAS
                    $SQL = "SELECT INF000.id, CONVERT(VARCHAR, INF000.fecha, 105) AS fecha1, INF000.ref, INF000.estado, INF000.lab
					FROM INF000 INNER JOIN INF001 ON INF000.id = INF001.informe INNER JOIN MUE000 ON INF001.muestra = MUE000.id INNER JOIN SER003 ON MUE000.solicitud = SER003.cs INNER JOIN SER005 ON SER003.cs = SER005.solicitud INNER JOIN VAR005 ON SER005.analisis = VAR005.id
					WHERE (INF000.lab = '{$_LAB}') AND (VAR005.nombre LIKE '%{$_valor}%');";
                }
            } elseif ($_LAB == '1') { //LRE
                if ($_tipo == '4') { //# DE LOTE
                    $SQL = "SELECT INF000.id, CONVERT(VARCHAR, INF000.fecha, 105) AS fecha1, INF000.ref, INF000.estado, INF000.lab
					FROM INF000 INNER JOIN INF001 ON INF000.id = INF001.informe INNER JOIN MUE000 ON INF001.muestra = MUE000.id INNER JOIN SER001 ON MUE000.solicitud = SER001.cs
					WHERE (INF000.lab = '{$_LAB}') AND (SER001.lote LIKE '%{$_valor}%');";
                }
            }
        } else { //ESTA BUSCANDO POR FECHA Y ESTADO
            $SQL = "SELECT id, CONVERT(VARCHAR, fecha, 105) AS fecha1, ref, estado, lab
			FROM INF000 
			WHERE (lab = '{$_LAB}') AND (estado {$op} '{$_estado}') AND (fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')
			ORDER BY fecha;";
        }
        return $this->_QUERY($SQL);
    }

    function InformesReporteGet($_tipo, $_valor, $_desde, $_hasta, $_estado, $_tipos = '')
    {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        $_valor = str_replace(' ', '', $_valor);

        if ($_estado == '') {
            $op = '<>';
        } else {
            $op = '=';
        }

        if ($_tipos == '') {
            $op3 = '<>';
        } else {
            $op3 = '=';
        }

        $SQL = "SELECT INF000.id AS informe, CONVERT(VARCHAR, INF000.fecha, 105) AS fecha1, INF000.lab, MUE000.solicitud, MUE000.ref AS muestra, SER003.producto, VAR007.nombre AS formulacion, SER003.tipo, SER003.registro, SER006.lote, VAR005.nombre AS analisis, MUE002.resultado
		FROM INF000 INNER JOIN INF001 ON INF000.id = INF001.informe INNER JOIN MUE000 ON INF001.muestra = MUE000.id INNER JOIN SER003 ON MUE000.solicitud = SER003.cs INNER JOIN SER005 ON SER003.cs = SER005.solicitud INNER JOIN VAR005 ON SER005.analisis = VAR005.id INNER JOIN VAR007 ON SER003.tipo_form = VAR007.id INNER JOIN SER006 ON SER003.cs = SER006.solicitud INNER JOIN MUE001 ON MUE000.id = MUE001.muestra INNER JOIN MUE002 ON MUE001.id = MUE002.xanalizar
		WHERE (INF000.lab = '3') AND (INF000.estado {$op} '{$_estado}') AND (INF000.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00') AND SER003.tipo {$op3} '{$_tipos}'";

        if ($_valor != '') { //ESTA BUSCANDO POR VALOR DIRECTO
            if ($_tipo == '0') { //INFORME
                $SQL .= "AND (INF000.id = '{$_valor}')";
            } elseif ($_tipo == '1') { //SOLICITUD 
                $SQL .= "AND (MUE000.solicitud = '{$_valor}')";
            } elseif ($_tipo == '2') { //MUESTRA
                $SQL .= "AND (MUE000.ref LIKE '%{$_valor}%')";
            } elseif ($_tipo == '3') { //# DE REGISTRO DE LA MUESTRA
                $SQL .= "AND (SER003.registro LIKE '%{$_valor}%')";
            } elseif ($_tipo == '4') { //# DE LOTE
                $SQL .= "AND (SER006.lote LIKE '%{$_valor}%')";
            } elseif ($_tipo == '5') { //ANALISIS
                $SQL .= "AND (VAR005.nombre LIKE '%{$_valor}%')";
            }
        }

        $SQL .= " ORDER BY INF000.fecha;";
        return $this->_QUERY($SQL);
    }

    function InformeModificaEstado($_id, $_obs, $_UID, $_estado, $_cliente)
    {
        $_obs = $this->Free($_obs);
        //MODIFICA ESTADO 
        $this->_TRANS("UPDATE INF000 SET fecha = GETDATE(), obs = '{$_obs}', estado = '{$_estado}' WHERE (id='{$_id}');");
        //INGRESA BITACORA
        $this->_TRANS("INSERT INTO INFBIT VALUES('{$_id}', '{$_UID}', GETDATE(), '{$_estado}');");
        //SI ES APROBACION, NOTIFICA AL CLIENTE SOBRE RECOGER LAS MUESTRAS
        if ($_estado == '2') {
            $ROW = $this->_QUERY("SELECT SER003.cs AS solicitud, MAN002.email
			FROM SER003 INNER JOIN MAN002 ON SER003.cliente = MAN002.id
			WHERE (SER003.cs = (
						SELECT TOP(1) MUE000.solicitud
						FROM INF001 INNER JOIN INF000 ON INF001.informe = INF000.id INNER JOIN MUE000 ON INF001.muestra = MUE000.id 
						WHERE (INF000.id='{$_id}')
					)
				)
			");

            //  $this->Email($ROW[0]['email'], "Estimado cliente: El Servicio Fitosanitario del Estado le informa que puede venir a retirar las muestras relativas a la solicitud: {$ROW[0]['solicitud']}");
        }
        return 1;
    }

    function sustituyeInforme($_numInfo, $_cliente, $_id_cliente, $_solicitante, $_codExterno, $_frf, $_metodo, $_observaciones, $_usuario, $_numSolicitud)
    {
        $_numInfo = $this->Free($_numInfo);
        $_cliente = $this->Free($_cliente);
        $_id_cliente = $this->Free($_id_cliente);
        $_solicitante = $this->Free($_solicitante);
        $_codExterno = $this->Free($_codExterno);
        $_frf = $this->Free($_frf);
        $_metodo = $this->Free($_metodo);
        $_observaciones = $this->Free($_observaciones);
        $_usuario = $this->Free($_usuario);
        $_numSolicitud = $this->Free($_numSolicitud);

        $_cliente = trim($_cliente);
        $_numSolicitud = trim($_numSolicitud);
        $_id_cliente = trim($_id_cliente);
        $reg = '';
        $conse = '';
        $nu = "";

        $ROW = $this->_QUERY("select numInfo from INF008 where numInfo like '{$_numInfo}%';");
        for ($i = 0; $i < count($ROW); $i++) {
            $reg++;
        }

        if ($reg > 1) {
            $conse = $reg + 1;
        } else {
            $conse = 2;
        }
        $nu = $_numInfo . "-" . $conse;

        $this->_TRANS("INSERT INTO inf008 VALUES('{$_numInfo}', '{$_id_cliente}', '{$_cliente}',  '{$_solicitante}', '{$_codExterno}', '{$_frf}', '{$_metodo}', '{$_observaciones}', '{$_usuario}', '{$nu}' , GETDATE(), '1');");
        $this->_TRANS("UPDATE INF000 SET estado = 's' where id = '{$_numInfo}' ");
        //actualiza el nombre del cliente
        $this->_TRANS("UPDATE ser001 SET cliente  = '{$_id_cliente}', nomSolicitante = '{$_solicitante}'  where cs = '{$_numSolicitud}' ");
        //actualizada el detalle
        $this->_TRANS("UPDATE ser002 SET codigo  = '{$_codExterno}', obsfrf = '{$_frf}', metodo = '{$_metodo}', observaciones  = '{$_observaciones}'  where numInfo = '{$_numInfo}' ");

        return '1';
    }


    function FirmarInforme($_id, $_firmante, $_UID, $_estado)
    {        //INGRESA BITACORA
        $this->_TRANS("INSERT INTO INFBIT VALUES('{$_id}', '{$_UID}', GETDATE(), '{$_estado}');");
        $this->_TRANS("INSERT INTO INFORMEFIRMA VALUES('{$_id}', '{$_firmante}', '{$_UID}', GETDATE(), '{$_estado}');");
        $this->_TRANS("UPDATE INF000 SET fecha = GETDATE(), estado = '{$_estado}' WHERE (id='{$_id}');");
        return 1;
    }

    function EliminaFirma($_id, $_UID, $_estado)
    {        //INGRESA BITACORA 1 para ponerlo en estado para firmar
        $this->_TRANS("DELETE FROM INFORMEFIRMA WHERE informe ='{$_id}';");
        $this->_TRANS("INSERT INTO INFBIT VALUES('{$_id}', '{$_UID}', GETDATE(), '{$_estado}');");
        $this->_TRANS("UPDATE INF000 SET fecha = GETDATE(), estado = 1 WHERE (id='{$_id}');");
        return 1;
    }

    function InformesPendientesGet($_LAB)
    {
        return $this->_QUERY("SELECT id, CONVERT(VARCHAR, fecha, 105) AS fecha1, ref, estado, lab
		FROM INF000 
		WHERE (estado = 't' OR estado = '1' OR estado = '9' ) AND (lab = '{$_LAB}')
		ORDER BY fecha;");
    }

    function InformeRequiereEquipo($_id)
    {
        return $this->_QUERY("SELECT 1
		FROM MUE000 INNER JOIN MUE001 ON MUE000.id = MUE001.muestra INNER JOIN MUE002 ON MUE001.id = MUE002.xanalizar INNER JOIN  MET000 ON MUE002.tipo = MET000.id INNER JOIN INF001 ON MUE000.id = INF001.muestra
		WHERE (INF001.informe = '{$_id}') AND (MET000.equipo = '1')");
    }

    function InformeTemporalConvierte($_temp, $_obs, $_obser, $_muestra, $_metodo, $_metodo1, $_metodosotros, $_equipos, $_analistas, $_horas, $_UID, $_LAB, $_plaguicidas = false)
    {
        $_obs = $this->Free($_obs);
        $_obser = $this->Free($_obser);
        $_siglas = '';
        if ($_LAB == '1')
            $siglas = 'LRE-';
        elseif ($_LAB == '2')
            $siglas = 'LDP-';
        elseif ($_LAB == '3')
            $siglas = 'LCC-';
        //$_LAB = 3;
        //$siglas = 'LCC-';

        if ($_LAB == '3') {
            $existente = $this->_QUERY("SELECT informe FROM INF001 WHERE muestra LIKE '%{$_muestra}%' AND INFORME LIKE '%LCC%'");
        } else {
            $existente = false;
        }
        if ($existente) {
            $nuevo1 = $existente[0]['informe'];
        } else {
            $nuevo1 = $this->_QUERY("SELECT informe0{$_LAB}_ AS cs FROM MAG000;");
            $nuevo1 = $siglas . date('Y') . '-' . $nuevo1[0]['cs'];
            $nuevo = $_temp;

        }
        $this->_TRANS("DELETE FROM INF001 WHERE informe = '{$nuevo1}'; DELETE FROM INFBIT WHERE informe = '{$nuevo1}'; DELETE FROM INF000 WHERE id = '{$nuevo1}';");
        //error_log("EXECUTE PA_MAG_102 '{$_temp}', '{$nuevo1}', '{$_obs}', '{$_UID}', '{$_LAB}';");
        if ($this->_TRANS("EXECUTE PA_MAG_102 '{$_temp}', '{$nuevo1}', '{$_obs}', '{$_UID}', '{$_LAB}';")) {
            if (!$existente) {
                $this->_TRANS("UPDATE MAG000 SET informe0{$_LAB}_ = informe0{$_LAB}_ + 1;");
            }
            if ($_LAB == '3') {
                //INSERTA OBS
                if ($_obser != '') {
                    $_obser = explode('&', $_obser);
                    for ($i = 1; $i < count($_obser); $i++) {
                        if (str_replace(' ', '', $_obser[$i]) != '') {
                            list($linea, $tmp) = explode('^', $_obser[$i]);
                            $this->_TRANS("INSERT INTO INF003 values('{$nuevo}', {$linea}, '{$tmp}');");
                        }
                    }
                }
                //INSERTA METODOS
                
                if ($_metodo != '') {
                    if (!$_plaguicidas)
                        $tabla = 'INF002'; //metodos para fertilizantes
                    else
                        $tabla = 'INF004'; //metodos para plaguicidas

                    $_metodo = explode('&', $_metodo);
                    $_metodo1 = explode('&', $_metodo1);
                    $_metodosotros = explode('&', $_metodosotros); 
                    for ($i = 1; $i < count($_metodo); $i++) {
                        if (str_replace(' ', '', $_metodo[$i]) != '') {
                            $_metodo[$i] = $this->Free($_metodo[$i]);
                            if (!isset($_metodo1[$i])) {
                                $_metodo1[$i] = '';
                            }
                            //error_log("INSERT INTO {$tabla} values('{$nuevo1}', {$i}, '{$_metodo[$i]}', '{$_metodo1[$i]}');");
                            $this->_TRANS("INSERT INTO {$tabla} values('{$nuevo1}', {$i}, '{$_metodo[$i]}', '{$_metodo1[$i]}');");
                            if ($_metodo[$i] == '15') {
                                $this->_TRANS("INSERT INTO INF002_Otros(informe,linea,descripcion) VALUES('{$nuevo}',{$i},'{$_metodosotros[$i]}');");
                            }
                        }
                    }
                }
                //INSERTA EQUIPOS
                if ($_equipos != '') {
                    $_equipos = explode('&', $_equipos);
                    $_analistas = explode('&', $_analistas);
                    $_horas = explode('&', $_horas);
                    for ($i = 1; $i < count($_equipos); $i++) {
                        if (str_replace(' ', '', $_equipos[$i]) != '') {
                            $this->_TRANS("EXECUTE PA_MAG_110 {$_equipos[$i]}, {$_analistas[$i]}, {$_horas[$i]}, 'Informe: {$nuevo1}', 'i';");
                        }
                    }
                }
            } else if ($_LAB == '1') {
                //ACTUALIZA PROCEDIMIENTO UTILIZADO
                //  $this->_TRANS("UPDATE INF005 SET procedimiento = '{$_obser}' WHERE (informe = '{$nuevo}');");
                $this->_TRANS("UPDATE INF005 SET procedimiento = 'Extracci�n QuEChERS, seg�n procedimiento t�cnico LAB-LRE-PT-01' WHERE (informe = '{$nuevo}');");
            }
            //NOTIFICA AL JEFE PARA QUE APRUEBE
            /*  $ROW = $this->_QUERY("SELECT SEG001.id, SEG001.email
              FROM SEG001 INNER JOIN SEG007 ON SEG001.id = SEG007.usuario INNER JOIN SEG002 ON SEG007.perfil = SEG002.id INNER JOIN SEG004 ON SEG002.id = SEG004.perfil
              WHERE (SEG002.lab = '{$_LAB}') AND (SEG004.permiso = '87');");

              for ($x = 0, $destinos = ''; $x < count($ROW); $x++) {
                  $destinos .= ',' . $ROW[$x]['email'];
                  $this->TablonSet(87, $ROW[$x]['id'], 87, false);
              }

              $destinos = substr($destinos, 1);
              ;
              //ENVIA EMAIL
              $Robot = new Seguridad();
              $Robot->Email($destinos, 'Ha recibido una solicitud de aprobacion');
              //*/
            return 1;
        } else
            return 0;
    }

    /* function InformeFinalElimina($_id){

      $ROW = $this->_QUERY("SELECT muestra FROM INF001 WHERE (informe='{$_id}');");
      $muestras = explode('-',$ROW[0]['muestra']);
      $muestra = $muestras[1].'-'.$muestras[2];
      //PARA LCC PONE LAS MUESTRAS DE ESTADO 1 DE NUEVO A 0
      $this->_TRANS("UPDATE MUE000 SET estado = '1' WHERE (id like '%{$muestra}%');");
      $this->_TRANS("UPDATE MUE001 SET estado = '0' WHERE (muestra like '%{$muestra}%');");
      //ACTUALIZA LA SOLICITUD PARA RECIBIR DE NUEVO ENSAYOS
      $ROW = $this->_QUERY("SELECT solicitud FROM MUE000 WHERE (id = '{$ROW[0]['muestra']}');");
      $this->_TRANS("UPDATE SER003 SET estado = '2' WHERE (cs = '{$ROW[0]['solicitud']}');");
      //ELIMINA RELACIONES
      $this->_TRANS("DELETE FROM INF001 WHERE (informe = '{$_id}');");
      $this->_TRANS("DELETE FROM INFBIT WHERE (informe = '{$_id}');");
      $this->_TRANS("DELETE FROM INF005 WHERE (informe = '{$_id}');");
      $this->_TRANS("DELETE FROM INF004 WHERE (informe = '{$_id}');");
      $this->_TRANS("DELETE FROM INF003 WHERE (informe = '{$_id}');");
      $this->_TRANS("DELETE FROM INF002 WHERE (informe = '{$_id}');");
      $this->_TRANS("DELETE FROM INF000 WHERE (id = '{$_id}');");
      return 1;
      } */

    function InformeFinalElimina($_id, $_usuario = 0)
    {

        $ROW = $this->_QUERY("SELECT muestra FROM INF001 WHERE (informe='{$_id}');");
        $muestras = explode('-', $ROW[0]['muestra']);
        $muestra = $muestras[1] . '-' . $muestras[2];
        //PARA LCC PONE LAS MUESTRAS DE ESTADO 1 DE NUEVO A 0
        $this->_TRANS("UPDATE MUE000 SET estado = '1' WHERE (id like '%{$muestra}%');");
        $this->_TRANS("UPDATE MUE001 SET estado = '0' WHERE (muestra like '%{$muestra}%');");
        //ACTUALIZA LA SOLICITUD PARA RECIBIR DE NUEVO ENSAYOS
        $ROW = $this->_QUERY("SELECT solicitud FROM MUE000 WHERE (id = '{$ROW[0]['muestra']}');");
        $this->_TRANS("UPDATE SER003 SET estado = '2' WHERE (cs = '{$ROW[0]['solicitud']}');");
        //ELIMINA RELACIONES
        $this->_TRANS("INSERT INTO INFBIT (informe, usuario, fecha, accion) VALUES('{$_id}', {$_usuario}, GETDATE(), '4');");
        //$this->_TRANS("DELETE FROM INF001 WHERE (informe = '{$_id}');");
        //$this->_TRANS("DELETE FROM INFBIT WHERE (informe = '{$_id}');");
        $this->_TRANS("DELETE FROM INF005 WHERE (informe = '{$_id}');");
        $this->_TRANS("DELETE FROM INF004 WHERE (informe = '{$_id}');");
        $this->_TRANS("DELETE FROM INF003 WHERE (informe = '{$_id}');");
        $this->_TRANS("DELETE FROM INF002 WHERE (informe = '{$_id}');");
        //$this->_TRANS("DELETE FROM INF000 WHERE (id = '{$_id}');");
        $this->_TRANS("UPDATE INF000 SET estado='-1' WHERE id='{$_id}';");
        return 1;
    }

    function InformeTemporalElimina($_id)
    {

        $ROW = $this->_QUERY("SELECT muestra FROM INF001 WHERE (informe='{$_id}');");
        if ($this->_LID == '1') {
            //PARA LRE PONE LAS MUESTRAS DE ESTADO 5 DE NUEVO A 2			
            $this->_TRANS("UPDATE MUE000 SET estado = '2' WHERE (id='{$ROW[0]['muestra']}') AND (estado='5');");
            $this->_TRANS("DELETE FROM INF005 WHERE (informe='{$_id}');");
        }
        if ($this->_LID == '3') {
            //PARA LCC PONE LAS MUESTRAS DE ESTADO 1 DE NUEVO A 0
            $this->_TRANS("UPDATE MUE000 SET estado = '1' WHERE (id = '{$ROW[0]['muestra']}');");
            $this->_TRANS("UPDATE MUE001 SET estado = '0' WHERE (muestra = '{$ROW[0]['muestra']}');");
        }
        //ELIMINA RELACIONES
        $this->_TRANS("DELETE FROM INF001 WHERE (informe='{$_id}');");
        $this->_TRANS("DELETE FROM INF000 WHERE (id='{$_id}');");
        return 1;
    }

    function InformeTemporalFromSolicitud($_solicitud, $_muestra = '')
    {
        $lab = $this->_LID;

        if ($lab == '1') { //LRE
            //INGRESA ENCABEZADO INFORME

            $cod_int = substr_replace($_muestra, '', 0, 9);
            //borra espacios en blanco
            $cod_int = trim($cod_int, $characters = " \t\n\r\0\x0B");
            //$cs = $this->_QUERY("SELECT numInfo FROM SER002 WHERE solicitud ='{$_solicitud}' and conSolicitud like  '%-$cod_int');");
            $cs = $this->CsTemporal123($_solicitud, $cod_int);

            $this->_TRANS("EXECUTE PA_MAG_100 '{$cs}', '{$_solicitud}', '{$_muestra}';");
        } elseif ($lab == '3') { //LCC
            //OBTIENE EL TIPO DE SOLICITUD 1:FERTILIZANTES, 2:PLAGUICIDAS
            $ROW = $this->_QUERY("SELECT tipo FROM SER003 WHERE (cs='{$_solicitud}');");
            $tipo = $ROW[0]['tipo'];
            $cs = $this->CsTemporal();
            //INGRESA ENCABEZADO INFORME
            if ($this->_TRANS("EXECUTE PA_MAG_104 '{$cs}', '{$_solicitud}', '{$tipo}', '{$lab}';")) {
                //OBTIENE VECTOR CON MUESTRAS
                $ROW = $this->_QUERY("SELECT id FROM MUE000 WHERE (solicitud='{$_solicitud}') AND (estado='2');");
                for ($i = 0; $i < count($ROW); $i++) {
                    $this->_TRANS("INSERT INTO INF001 VALUES('{$cs}', '{$ROW[$i]['id']}');");
                }
            } else
                return 0;
        }

        return 1;
    }

    function InformeValidaSolicitud($_solicitud)
    {
        //VALIDA QUE EXISTE Y QUE ESTE APROBADA
        if ($this->_LID == '1') //LRE
            if (!$this->_QUERY("SELECT 1 FROM SER001 WHERE (cs='{$_solicitud}') AND (estado='2');"))
                return 0;
            elseif ($this->_LID == '3') //LCC
                if (!$this->_QUERY("SELECT 1 FROM SER003 WHERE (cs='{$_solicitud}') AND (estado='2');"))
                    return 0;

        //VALIDA QUE TODAS LAS MUESTRAS ESTEN ACEPTADAS
        if ($this->_QUERY("SELECT 1 FROM MUE000 WHERE (solicitud='{$_solicitud}') AND (estado!='2') AND (estado!='3');"))
            return 1;

        return 2;
    }

    function InventarioGeneral($_es, $_tipo)
    {
        $_lab = $this->_LID;
        //return $this->_QUERY("EXECUTE PA_MAG_047 '{$_es}', '{$_tipo}', '{$_lab}';");
        if ($_tipo == -99) {
            return $this->_QUERY("SELECT INV002.codigo, INV002.nombre, INV002.marca, INV002.presentacion, INV001.nombre AS unidad, INV002.stock1, INV002.stock2, (INV002.ubicacion1 + ' ' + INV002.ubicacion2) AS ubicacion
                    FROM INV000 INNER JOIN INV002 ON INV000.id = INV002.familia AND INV002.estado = '1' INNER JOIN INV001 ON INV002.presentacion = INV001.id WHERE (INV000.lab = '{$_lab}') ORDER BY INV002.nombre;");
        } elseif ($_es == '0') {
            return $this->_QUERY("SELECT INV002.codigo, INV002.nombre, INV002.marca, INV002.presentacion, INV001.nombre AS unidad, INV002.stock1, INV002.stock2, (INV002.ubicacion1 + ' ' + INV002.ubicacion2) AS ubicacion
                    FROM INV002
                    INNER JOIN INV001 ON INV002.presentacion = INV001.id
                    WHERE (es = 0) AND (INV002.familia = '{$_tipo}') AND (estado = '1')
		ORDER BY INV002.nombre;");
        } else {
//			return $this->_QUERY("SELECT INV002.nombre, INV002.marca, INV002.presentacion, INV001.nombre AS unidad, INV002.ubicacion1, INV002.ubicacion2, INV003.codigo, INV003.bodega1 AS stock1, INV003.bodega2 AS stock2, INV007.lote, INV007.costo
//                    FROM INV002 INNER JOIN INV003 ON INV002.id = INV003.producto INNER JOIN INV007 ON INV003.id = INV007.ES INNER JOIN INV001 ON INV002.presentacion = INV001.id
//                    WHERE (INV003.tipo = 'E') AND (INV002.es = 1) AND (INV002.estado = '1') AND (INV002.familia = '{$_tipo}') AND (INV003.bodega1 > 0 OR INV003.bodega2 > 0)
//                    UNION
//                    SELECT INV002.nombre, INV002.marca, INV002.presentacion, INV001.nombre AS unidad, INV002.ubicacion1, INV002.ubicacion2, '' AS codigo, INV002.stock1, INV002.stock2, '' AS lote, 0 AS costo
//                    FROM INV002 INNER JOIN INV001 ON INV002.presentacion = INV001.id WHERE es = '1' AND familia = '{$_tipo}' AND estado = '1';");
            return $this->_QUERY("SELECT INV_PRINCIPAL.id,
                                INV_PRINCIPAL.nombre,
                                INV_PRINCIPAL.marca,
                                INV_PRINCIPAL.presentacion,
                                INV_PRINCIPAL.unidad,
                                INV_PRINCIPAL.ubicacion1,
                                INV_PRINCIPAL.ubicacion2,
                                INV_PRINCIPAL.codigo,
                                ( INV_PRINCIPAL.stock1 + INV_ENTRADAS.stock1 ) AS stock1,
                                ( INV_PRINCIPAL.stock2 + INV_ENTRADAS.stock2 ) AS stock2,
                                INV_PRINCIPAL.lote,
                                INV_PRINCIPAL.costo
                         FROM   (SELECT inv002.id,
                                        inv002.nombre,
                                        inv002.marca,
                                        inv002.presentacion,
                                        inv001.nombre  AS unidad,
                                        inv002.ubicacion1,
                                        inv002.ubicacion2,
                                        inv003.codigo,
                                        inv003.bodega1 AS stock1,
                                        inv003.bodega2 AS stock2,
                                        inv007.lote,
                                        inv007.costo
                                 FROM   inv002
                                        INNER JOIN inv003
                                                ON inv002.id = inv003.producto
                                        INNER JOIN inv007
                                                ON inv003.id = inv007.es
                                        INNER JOIN inv001
                                                ON inv002.presentacion = inv001.id
                                 WHERE  ( inv003.tipo = 'E' )
                                        AND ( inv002.es = 1 )
                                        AND ( inv002.estado = '1' )
                                        AND ( inv002.familia = '{$_tipo}' )
                                        AND ( inv003.bodega1 > 0
                                               OR inv003.bodega2 > 0 )) AS INV_PRINCIPAL
                                INNER JOIN (SELECT id,
                                                   Sum(stock1) AS stock1,
                                                   Sum(stock2) AS stock2
                                            FROM   inv002
                                            GROUP  BY id) AS INV_ENTRADAS
               ON INV_ENTRADAS.id = INV_PRINCIPAL.id");
        }
    }

    function InventarioVencimientos()
    {
        $_lab = $this->_LID;
        return $this->_QUERY("EXECUTE PA_MAG_047 '2', '0', '{$_lab}';");
    }

    function LabTipo($_var)
    {
        if ($_var == '1')
            return 'LRE';
        elseif ($_var == '2')
            return 'LDP';
        elseif ($_var == '3')
            return 'LCC';
    }

    function MatrizXanalito($_tipo, $_desde, $_hasta)
    {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        if ($_tipo == '0') {
            $colA = 'campoB';
            $colB = 'campoA';
            $orden = 'MET002.matriz';
        } else {
            $colA = 'campoA';
            $colB = 'campoB';
            $orden = 'MET002.analito';
        }

        return $this->_QUERY("SELECT MET002.analito AS {$colA}, VAR001.nombre AS {$colB}, MUE000.ref, MET002.conc
		FROM MET002 INNER JOIN MUE002 ON MET002.ensayo = MUE002.id INNER JOIN MUE001 ON MUE002.xanalizar = MUE001.id INNER JOIN MUE000 ON MUE001.muestra = MUE000.id INNER JOIN INF001 ON MUE000.id = INF001.muestra INNER JOIN INF000 ON INF001.informe = INF000.id INNER JOIN VAR001 ON MET002.matriz = VAR001.id
		WHERE (INF000.estado = '1' OR INF000.estado = '2') AND (INF000.lab = '1') AND (INF000.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')
		ORDER BY {$orden};");
    }

    function MuestrasEstado($_var)
    {
        $Robot = new Servicios();
        return $Robot->MuestraEstado($_var);
    }

    function MuestrasLab($_lab, $_cliente, $_desde, $_hasta, $_tipo = '')
    {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        $_cliente = $this->FreePost($_cliente);

        if ($_tipo != '') {
            return $this->_QUERY("SELECT CONVERT(VARCHAR, INF000.fecha, 105) AS fecha1, INF001.informe, MUE000.externo, MUE000.solicitud, MUE000.ref, MUE000.estado, MAN002.nombre, SER003.tipo AS tipo
		FROM INF000 INNER JOIN INF001 INNER JOIN MUE000 ON INF001.muestra = MUE000.id ON INF000.id = INF001.informe INNER JOIN SER003 ON MUE000.solicitud = SER003.cs INNER JOIN MAN002 ON SER003.cliente = MAN002.id
		WHERE (INF000.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00') AND (MAN002.nombre LIKE '%{$_cliente}%' AND SER003.tipo = '{$_tipo}')
		ORDER BY MUE000.ref;");
        } else {
            return $this->_QUERY("SELECT CONVERT(VARCHAR, INF000.fecha, 105) AS fecha1, INF001.informe, MUE000.externo, MUE000.solicitud, MUE000.ref, MUE000.estado, MAN002.nombre, SER003.tipo AS tipo
		FROM INF000 INNER JOIN INF001 INNER JOIN MUE000 ON INF001.muestra = MUE000.id ON INF000.id = INF001.informe INNER JOIN SER003 ON MUE000.solicitud = SER003.cs INNER JOIN MAN002 ON SER003.cliente = MAN002.id
		WHERE (INF000.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00') AND (MAN002.nombre LIKE '%{$_cliente}%')
		ORDER BY MUE000.ref;");
        }
    }

    function MuestrasXcliente($_LAB, $_cliente, $_desde, $_hasta, $_tipo = '')
    {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        if ($_cliente == '')
            $op1 = '<>';
        else
            $op1 = '=';

        if ($_LAB == '1') {
            return $this->_QUERY("SELECT SER001.cs, CONVERT(VARCHAR, SER001.fecha, 105) AS fecha, '' AS tipo, SER001.estado, SER002.codigo AS cod_ext, MAN002.nombre AS cliente
			FROM SER001 INNER JOIN SER002 ON SER001.cs = SER002.solicitud INNER JOIN MAN002 ON SER001.cliente = MAN002.id
			WHERE (MAN002.id {$op1} '{$_cliente}') AND (SER001.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')
			ORDER BY SER001.cs;");
        } else {
            if ($_tipo == '') {
                $op3 = '<>';
            } else {
                $op3 = '=';
            }
            return $this->_QUERY("SELECT SER003.cs, CONVERT(VARCHAR,SER003.fecha,105) AS fecha, ('-' + SER003.tipo) AS tipo, SER003.estado, SER006.cod_ext, MAN002.nombre AS cliente
			FROM SER003 INNER JOIN SER006 ON SER003.cs = SER006.solicitud INNER JOIN MAN002 ON SER003.cliente = MAN002.id
			WHERE (MAN002.id {$op1} '{$_cliente}') AND (SER003.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00') AND SER003.tipo {$op3} '{$_tipo}'
			ORDER BY SER003.cs;");
        }
    }

    function ObtieneVencimientos()
    {
        return $this->_QUERY("SELECT id, analito, bandeja, hoja, cultivo, cuanti, CONVERT(VARCHAR, fechaC, 105) AS fechaC, CONVERT(VARCHAR, fechaA, 105) AS fechaA, obs FROM INV045 WHERE (estado = 'o');");
    }

    function ObtieneVencimientos1($_muestra)
    {
        return $this->_QUERY("SELECT paso, analisis FROM INV046 WHERE (muestra = '{$_muestra}');");
    }

    function ObtieneVencimientos2()
    {
        return $this->_QUERY("SELECT analisis, resultado, obs FROM INV047 WHERE (muestra = '{$_muestra}');");
    }

    function obtieneFechas($_id)
    {
        return $this->_QUERY("SELECT flab, hlab, fmuestreo, hmuestreo FROM SER001 WHERE cs = '{$_id}';");
    }

    function obtieneFechaMuestreo($_id)
    {
        return $this->_QUERY("SELECT fmuestra, hmuestra FROM SER002 WHERE solicitud = '{$_id}';");
    }

    function obtieneobservaciones($_id)
    {
        return $this->_QUERY("SELECT observaciones FROM SER002 WHERE numInfo = '{$_id}';");
    }


    function obtieneFRF($_id)
    {
        /*revisar*/
        return $this->_QUERY("SELECT obsfrf, presentacion FROM SER002 WHERE solicitud = '{$_id}';");
        // return $Q[0]['obsfrf'];
    }


    function PerfilesLab($_lab)
    {
        if ($_lab == '')
            $op1 = '<>';
        else
            $op1 = '=';

        return $this->_QUERY("SELECT SEG002.nombre AS perfil, SEG002.lab, SEG004.privilegio, SEG003.nombre AS permiso, SEG000.nombre AS modulo
		FROM SEG002 INNER JOIN SEG004 ON SEG002.id = SEG004.perfil INNER JOIN SEG003 ON SEG004.permiso = SEG003.id INNER JOIN SEG000 ON SEG003.modulo = SEG000.id
		WHERE (SEG002.lab {$op1} '{$_lab}')
		ORDER BY SEG002.lab, modulo, perfil, permiso;");
    }

    function PersonaTablaCapacitacionesDel($_persona)
    {
        $this->_TRANS("DELETE FROM PERTMP WHERE (persona = {$_persona});");
    }

    function PersonaTablaCapacitacionesGet($_persona)
    {
        return $this->_QUERY("SELECT tipo, actual, menos1, menos2, menos3, menos4
		FROM PERTMP
		WHERE (persona = {$_persona})
		ORDER BY tipo;");
    }

    function PersonaTablaCapacitacionesSet($_persona, $_anos)
    {
        $this->_TRANS("EXECUTE PA_MAG_044 {$_persona}, {$_anos};");
    }

    function ProveedoresLab($_lab, $_tipo, $_clasificacion)
    {
        if ($_tipo == '')
            $op1 = '<>';
        else
            $op1 = '=';
        if ($_clasificacion == '-1')
            $op2 = '<>';
        else
            $op2 = '=';

        $lolo = "MAN003.LCC != ''";
        if ($_lab == '1')
            $lolo = "MAN003.LRE = '1'";
        if ($_lab == '2')
            $lolo = "MAN003.LDP = '1'";
        if ($_lab == '3')
            $lolo = "MAN003.LCC = '1'";

        return $this->_QUERY("SELECT MAN003.id, MAN003.nombre, MAN003.tipo, MAN003.direccion, MAN003.telefono, MAN003.fax, MAN003.correo, MAN007.nombre AS clasificacion
		FROM MAN003 INNER JOIN MAN007 ON MAN003.clasificacion = MAN007.cs
		WHERE ({$lolo}) AND (MAN003.tipo {$op1} '{$_tipo}') AND (MAN003.clasificacion {$op2} {$_clasificacion})
		ORDER BY MAN003.tipo, MAN007.nombre, MAN003.nombre;");
    }

    function QuejasLab($_instancia, $_estado, $_desde, $_hasta)
    {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        if ($_instancia == '')
            $op = '<>';
        else
            $op = '=';
        if ($_estado == '')
            $op2 = '<>';
        else
            $op2 = '=';

        return $this->_QUERY("SELECT SCL008.codigo, CONVERT(VARCHAR, SCL008.fec_pre, 105) AS fec_pre, CONVERT(VARCHAR, SCL008.fec_lim, 105) AS fec_lim, CONVERT(VARCHAR, SCL008.fec_rec, 105) AS fec_rec, SCL008.instancia, SCL008.acap, SCL008.estado, (SEG001.nombre +' '+SEG001.ap1) AS responsable
		FROM SCL008 INNER JOIN SCL009 ON cs = SCL009.queja INNER JOIN SEG001 ON SCL009.usuario = SEG001.id
		WHERE (SCL008.instancia {$op} '{$_instancia}') AND (SCL008.estado {$op2} '{$_estado}') AND (SCL008.fec_pre BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00') AND (SCL009.tipo = 'A')
		ORDER BY SCL008.fec_pre;");
    }

    function QuejasTipo($_tipo)
    {
        $Servitor = new Sc();
        return $Servitor->QuejasInstancia($_tipo);
    }

    function SolicitudEstado($_var)
    {
        $Servitor = new Servicios();
        return $Servitor->ServiciosEstado($_var);
    }

    function SolicitudTipo($_var)
    {
        if ($_var == '')
            return 'An�lisis';
        elseif ($_var == '1')
            return 'Fertilizantes';
        elseif ($_var == '2')
            return 'Plaguicidas';
    }

    function SolicitudMuestra($_solicitud, $_analisis)
    {
        $Q = $this->_QUERY("SELECT MUE001.muestra FROM MUE000 INNER JOIN MUE001 ON MUE000.id = MUE001.muestra WHERE MUE001.analisis = $_analisis AND MUE000.solicitud = '{$_solicitud}';");
        if ($Q) {
            return $Q[0]['muestra'];
        } else {
            $Q = $this->_QUERY("SELECT MUE001.muestra FROM MUE000 INNER JOIN MUE001 ON MUE000.id = MUE001.muestra WHERE MUE000.solicitud = '{$_solicitud}';");
            if ($Q) {
                return $Q[0]['muestra'];
            } else {
                return '-';
            }
        }
    }

    function SolicitudesXcliente($_LAB, $_cliente, $_desde, $_hasta, $_tipo, $_estado)
    {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        if ($_cliente == '') {
            $op1 = '<>';
        } else {
            $op1 = '=';
        }
        if ($_estado == '') {
            $op2 = '<>';
        } else {
            $op2 = '=';
        }
        if ($_tipo == '') {
            $op3 = '<>';
        } else {
            $op3 = '=';
        }

        if ($_LAB == '1') {
            return $this->_QUERY("SELECT SER001.cs, CONVERT(VARCHAR,SER001.fecha,105) AS fecha, '' AS tipo, SER001.estado, MAN002.nombre AS cliente
			FROM SER001 INNER JOIN MAN002 ON SER001.cliente = MAN002.id
			WHERE (MAN002.id {$op1} '{$_cliente}') AND (SER001.estado {$op2} '{$_estado}') AND (SER001.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')
			ORDER BY SER001.cs;");
        } else {
            return $this->_QUERY("SELECT SER003.cs, CONVERT(VARCHAR,SER003.fecha,105) AS fecha, SER003.tipo, SER003.estado, MAN002.nombre AS cliente
			FROM SER003 INNER JOIN MAN002 ON SER003.cliente = MAN002.id
			WHERE (SER003.tipo {$op3} '{$_tipo}') AND (MAN002.id {$op1} '{$_cliente}') AND (SER003.estado {$op2} '{$_estado}') AND (SER003.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')
			ORDER BY SER003.cs;");
        }
    }

    function SubAnalisisMuestra($_lid)
    {
        return $this->_QUERY("SELECT id, nombre, tipo FROM VAR005 WHERE (lab='{$_lid}') AND (tipo != 8) ORDER BY tipo, nombre;");
    }

    function SubAnalisisMuestra2($_lid)
    {
        return $this->_QUERY("SELECT id, nombre, tipo FROM VAR005 WHERE (lab='{$_lid}') AND (tipo != 8) AND (tipo != 2) ORDER BY tipo, nombre;");
    }

    function TablaCapacitacionesDel()
    {
        $this->_TRANS("DELETE FROM PERTMP1;");
    }

    function TablaCapacitacionesGet()
    {
        return $this->_QUERY("SELECT nombre, actual, menos1, menos2, menos3, menos4, CONVERT(VARCHAR, ingreso, 105) AS ingreso
		FROM PERTMP1
		ORDER BY nombre;");
    }

    function TablaCapacitacionesSet($_unidad, $_ano, $_rango, $_tipo = '')
    {
        if ($_tipo == '')
            $this->_TRANS("EXECUTE PA_MAG_R01 '{$_unidad}', '{$_ano}', '{$_rango}';");
        else
            $this->_TRANS("EXECUTE PA_MAG_R02 '{$_unidad}', '{$_ano}', '{$_rango}', '{$_tipo}';");
    }

    function TableHeader($_titulo, $_tipo = 1)
    {
        $robot = new dummy();
        $lname = $this->_LNAME;
        if ($_tipo == 1) {
            echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>', $robot->Title(), '</title>';
            '<style type="text/css">
                    input, select, textarea{
                        font-family:Calibri;
                        font-size:12px;
                    }

                    body{
                        font-family:Calibri;
                        font-size:12px;
                    }

                    #footer, #footer2{
                        display: none;
                    }
                </style>';
            $robot->ShellIncluir('print', 'prt');
            echo '</head>
		<body style="font-size:12px;">';
            echo "<center>
		<table class='radius' align='center' width='100%'>
		<tr align='center'>
			<td width='25%'>", $robot->ShellIncluir('mag', 'png'), "</td>
			<td width='50%'><strong>Ministerio de Agricultura y Ganader&iacute;a<br />Servicio Fitosanitario del Estado<br />{$lname}</strong></td>
			<td width='25%'>", $robot->ShellIncluir('agro', 'png'), "</td>
		</tr>
		<tr align='center'>
			<td>", date('d/m/Y'), "</td>
			<td>{$_titulo}</td>
			<td>", $robot->Title(), "</td>
		<tr>
		</table><br>";
        } else {
            echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>', $robot->Title(), '</title>';


            echo '</head>
		<body style="font-size:12px;">';
            echo "<center>
		<table class='radius' align='center' width='100%'>
		<tr align='center'>
			<td width='25%'></td>
			<td width='50%'><strong>Ministerio de Agricultura y Ganader&iacute;a<br />Servicio Fitosanitario del Estado<br />{$lname}</strong></td>
			<td width='25%'></td>
		</tr>
		<tr align='center'>
			<td>", date('d/m/Y'), "</td>
			<td>{$_titulo}</td>
			<td>", $robot->Title(), "</td>
		<tr>
		</table><br>";
        }
    }

    function TableFooter()
    {
        $footer = "<table id='footer2' width='100%'><tr><td width='100%' align='center'><font size='-2'>Documento Normativo Propiedad del SFE. Cualquier versi&oacute;n impresa es una COPIA NO CONTROLADA. Es responsabilidad del personal que utiliza el documento verificar su vigencia. Gesti&oacute;n de Calidad no se hace responsable por el uso de documentos obsoletos</font></td></tr></table>";
        echo '<br><input type="button" value="Imprimir" class="boton" onclick="window.print()" /></center>', $footer, '</body></html>';
    }

    function TramitesLab($_lab, $_tipo, $_desde, $_hasta)
    {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        if ($_tipo == '')
            $op = '<>';
        else
            $op = '=';

        return $this->_QUERY("SELECT descripcion, CONVERT(VARCHAR, fecha, 105) AS fecha1, (DATEDIFF(DAY, GETDATE(), fecha)) AS plazo,
			tipoD = CASE tipo
				WHEN '1' THEN 'Permiso Sanitario'
				WHEN '2' THEN 'Precursores ICD'
				WHEN '3' THEN 'Permiso de Circulaci�n'
				WHEN '4' THEN 'Colegio Qu�micos'
				WHEN '5' THEN 'Pr�stamo de equipos'
				WHEN '9' THEN 'Otros'
				ELSE 'N/A'
			END 
			FROM MAN005 
			WHERE (lid='{$_lab}') AND (tipo $op '{$_tipo}') AND (fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')
			ORDER BY tipoD, fecha;");
    }

    function UsuarioEstado($_var)
    {
        if ($_var == '0')
            return 'Inactivo';
        elseif ($_var == '1')
            return 'Activo';
        elseif ($_var == '2')
            return 'Cond. Especial';
    }

    function UsuariosLab($_lab, $_usuario, $_estado)
    {
        if ($_lab == '')
            $op1 = '<>';
        else
            $op1 = '=';
        if ($_usuario == '') {
            $op2 = '<>';
            $_usuario = '-1';
        } else
            $op2 = '=';
        if ($_estado == '')
            $op3 = '<>';
        else
            $op3 = '=';

        return $this->_QUERY("SELECT SEG002.lab, (SEG001.cedula + ' ' + SEG001.nombre + ' ' + SEG001.ap1 + ' ' + SEG001.ap2) AS usuario, SEG001.estado, SEG002.nombre AS perfil, SEG003.nombre AS permiso, SEG004.privilegio
		FROM SEG004 INNER JOIN SEG002 ON SEG004.perfil = SEG002.id INNER JOIN SEG007 ON SEG002.id = SEG007.perfil INNER JOIN SEG001 ON SEG007.usuario = SEG001.id INNER JOIN SEG003 ON SEG004.permiso = SEG003.id
		WHERE (SEG001.id {$op2} {$_usuario}) AND (SEG002.lab {$op1} '{$_lab}') AND (SEG001.estado {$op3} '{$_estado}')
		ORDER BY SEG002.lab, SEG001.id, perfil, permiso;");
    }

    function ValidacionesEstado($_var)
    {
        $Robot = new Validacion();
        return $Robot->ValidacionesEstado($_var);
    }

    function ValidacionesLab($_lab, $_tipo, $_desde, $_hasta)
    {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        if ($_tipo == '')
            $op = '<>';
        else
            $op = '=';

        return $this->_QUERY("SELECT VAL000.cs, CONVERT(VARCHAR, VAL000.fecha, 105) AS fecha1, VAL000.tipo, VAL000.cumple, VAL000.estado, VAL000.obs, (SEG001.nombre +' '+ SEG001.ap1) AS analista
		FROM VAL000 INNER JOIN VAlBIT ON VAL000.cs = VAlBIT.validacion INNER JOIN SEG001 ON VAlBIT.analista = SEG001.id
		WHERE (VAL000.lab = '{$_lab}') AND (VAL000.tipo {$op} '{$_tipo}') AND (VAL000.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')
		ORDER BY VAL000.tipo, VAL000.fecha;");
    }

    function ValidacionesLRE($_desde, $_hasta)
    {
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        return $this->_QUERY("SELECT CONVERT(VARCHAR, VAL000.fecha, 105) AS fecha1, VAL011.validacion, VAL011.analito, VAL011.P1, VAL011.P2, VAL011.P3, VAL011.P4, VAL011.P5, VAL011.bajo2, VAL011.bajo3, VAL011.alto2, VAL011.alto3, VAL011.final, VAL011.cumple
		FROM VAL011 INNER JOIN VAL009 ON VAL011.validacion = VAL009.validacion INNER JOIN VAL000 ON VAL009.validacion = VAL000.cs
		WHERE (VAL011.reportado = 1) AND (VAL000.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00')
		ORDER BY VAL000.fecha, VAL011.validacion;");
    }

    function ValidacionesTipo($_var)
    {
        $Robot = new Validacion();
        return $Robot->ValidacionesTipo($_var);
    }

    function obtieneIngredientesCentinela()
    {
        return $this->_QUERY("SELECT DISTINCT ingrediente FROM CENTIPLAG UNION SELECT DISTINCT ingrediente FROM CENTIFERT");
    }

    function obtieneProductosCentinela()
    {
        return $this->_QUERY("SELECT DISTINCT producto FROM CENTIPLAG UNION SELECT DISTINCT producto FROM CENTIFERT");
    }

    function obtieneGeneralCentinela($_desde, $_hasta, $_registro, $_elemento, $_solicitud, $_informe, $_ingrediente, $_tipo)
    {

        $extra = '';
        $extra1 = '';
        if ($_tipo != '-1') {
            if ($_tipo == 'FERT') {
                $extra .= " AND CENTIINF.id_lab = 2";
            } else {
                $extra .= " AND CENTIINF.id_lab = 3";
            }
        }
        if ($_desde != '' && $_hasta != '') {
            if ($_tipo != '-1') {
                $extra .= " AND CENTIINF.f_conclusion BETWEEN '" . $this->_FECHA($_desde) . "' AND '" . $this->_FECHA($_hasta) . "'";
            } else {
                $extra .= " AND CENTIINF.f_conclusion BETWEEN '" . $this->_FECHA($_desde) . "' AND '" . $this->_FECHA($_hasta) . "'";
                $extra1 .= " AND CENTIINF.f_conclusion BETWEEN '" . $this->_FECHA($_desde) . "' AND '" . $this->_FECHA($_hasta) . "'";
            }
        }
        if ($_ingrediente != '-1') {
            if ($_tipo != '-1') {
                $extra .= " AND CENTI" . $_tipo . ".ingrediente = '" . $_ingrediente . "'";
            } else {
                $extra .= " AND CENTIFERT.ingrediente = '" . $_ingrediente . "'";
                $extra1 .= " AND CENTIPLAG.ingrediente = '" . $_ingrediente . "'";
            }
        }
        if ($_elemento != '-1') {
            if ($_tipo != '-1') {
                $extra .= " AND CENTI" . $_tipo . ".producto = '" . $_elemento . "'";
            } else {
                $extra .= " AND CENTIFERT.producto = '" . $_elemento . "'";
                $extra1 .= " AND CENTIPLAG.producto = '" . $_elemento . "'";
            }
        }
        if ($_registro != '') {
            if ($_tipo != '-1') {
                $extra .= " AND CENTI" . $_tipo . ".registro = '" . $_registro . "'";
            } else {
                $extra .= " AND CENTIFERT.registro = '" . $_registro . "'";
                $extra1 .= " AND CENTIPLAG.registro = '" . $_registro . "'";
            }
        }
        if ($_solicitud != '') {
            if ($_tipo != '-1') {
                $extra .= " AND CENTIINF.id_solicitud = '" . $_solicitud . "'";
            } else {
                $extra .= " AND CENTIINF.id_solicitud = '" . $_solicitud . "'";
                $extra1 .= " AND CENTIINF.id_solicitud = '" . $_solicitud . "'";
            }
        }
        if ($_informe != '') {
            if ($_tipo != '-1') {
                $extra .= " AND CENTIINF.cod_informe = '" . $_informe . "'";
            } else {
                $extra .= " AND CENTIINF.cod_informe = '" . $_informe . "'";
                $extra1 .= " AND CENTIINF.cod_informe = '" . $_informe . "'";
            }
        }
        if ($_tipo == '-1') {
            return $this->_QUERY("SELECT CENTIINF.id, CENTIINF.id_solicitud, CENTIINF.id_lab, CENTIINF.cod_informe, CENTIINF.inf_lote, CONVERT(VARCHAR, CENTIINF.f_conclusion, 107) AS f_conclusion, CENTIFERT.id_muestra, CENTIFERT.incertidumbre, CENTIFERT.encontrada, CENTIFERT.declarada, CENTIFERT.limite_inferior, CENTIFERT.limite_superior, CENTIFERT.producto, CENTIFERT.codigo_interno, CENTIFERT.lote, CENTIFERT.impurezas, CENTIFERT.acta, CENTIFERT.ingrediente, CENTIFERT.formulacion, CENTIFERT.registro, CENTIFERT.codigo_externo FROM CENTIINF INNER JOIN CENTIFERT ON CENTIINF.id = CENTIFERT.id_informe WHERE 1=1 {$extra} UNION  SELECT CENTIINF.id, CENTIINF.id_solicitud, CENTIINF.id_lab, CENTIINF.cod_informe, CENTIINF.inf_lote, CONVERT(VARCHAR, CENTIINF.f_conclusion, 107) AS f_conclusion, CENTIPLAG.id_mst_plag AS id_muestra, CENTIPLAG.incertidumbre, CENTIPLAG.encontrada, CENTIPLAG.declarada, CENTIPLAG.limite_inferior, CENTIPLAG.limite_superior, CENTIPLAG.producto, CENTIPLAG.codigo_interno, CENTIPLAG.lote, CENTIPLAG.impurezas, CENTIPLAG.acta, CENTIPLAG.ingrediente, CENTIPLAG.formulacion, CENTIPLAG.registro, CENTIPLAG.codigo_externo FROM CENTIINF INNER JOIN CENTIPLAG ON CENTIINF.id = CENTIPLAG.id_informe WHERE 1=1 {$extra1}");
        } else {
            if ($_tipo == 'FERT') {
                return $this->_QUERY("SELECT CENTIINF.id, CENTIINF.id_solicitud, CENTIINF.id_lab, CENTIINF.cod_informe, CENTIINF.inf_lote, CONVERT(VARCHAR, CENTIINF.f_conclusion, 107) AS f_conclusion, CENTI{$_tipo}.id_muestra, CENTI{$_tipo}.incertidumbre, CENTI{$_tipo}.encontrada, CENTI{$_tipo}.declarada, CENTI{$_tipo}.limite_inferior, CENTI{$_tipo}.limite_superior, CENTI{$_tipo}.producto, CENTI{$_tipo}.codigo_interno, CENTI{$_tipo}.lote, CENTI{$_tipo}.impurezas, CENTI{$_tipo}.acta, CENTI{$_tipo}.ingrediente, CENTI{$_tipo}.formulacion, CENTI{$_tipo}.registro, CENTI{$_tipo}.codigo_externo FROM CENTIINF INNER JOIN CENTI{$_tipo} ON CENTIINF.id = CENTI{$_tipo}.id_informe WHERE 1=1 {$extra} ORDER BY CENTI{$_tipo}.codigo_interno;");
            } else {
                return $this->_QUERY("SELECT CENTIINF.id, CENTIINF.id_solicitud, CENTIINF.id_lab, CENTIINF.cod_informe, CENTIINF.inf_lote, CONVERT(VARCHAR, CENTIINF.f_conclusion, 107) AS f_conclusion, CENTI{$_tipo}.id_mst_plag AS id_muestra, CENTI{$_tipo}.incertidumbre, CENTI{$_tipo}.encontrada, CENTI{$_tipo}.declarada, CENTI{$_tipo}.limite_inferior, CENTI{$_tipo}.limite_superior, CENTI{$_tipo}.producto, CENTI{$_tipo}.codigo_interno, CENTI{$_tipo}.lote, CENTI{$_tipo}.impurezas, CENTI{$_tipo}.acta, CENTI{$_tipo}.ingrediente, CENTI{$_tipo}.formulacion, CENTI{$_tipo}.registro, CENTI{$_tipo}.codigo_externo FROM CENTIINF INNER JOIN CENTI{$_tipo} ON CENTIINF.id = CENTI{$_tipo}.id_informe WHERE 1=1 {$extra} ORDER BY CENTI{$_tipo}.codigo_interno;");
            }
        }
    }

    function obtieneEspecificosCentinela($_solicitud, $_informe)
    {
        if ($_solicitud != '') {
            return $this->_QUERY("SELECT id_solicitud AS 'solicitud', cod_informe AS 'informe', inf_lab AS 'tipo' FROM CENTIINF WHERE id_solicitud = '{$_solicitud}';");
        } else {
            return $this->_QUERY("SELECT id_solicitud AS 'solicitud', cod_informe AS 'informe', inf_lab AS 'tipo' FROM CENTIINF WHERE cod_informe = '{$_informe}';");
        }
    }

    function obtieneInformeCentinela($_informe, $_tipo)
    {
        return $this->_QUERY("SELECT id, id_informe_anterior, id_solicitud, id_lab, id_prioridad, prioridad, id_dependencia, dependencia, id_modo_notificacion, modo_notificacion, id_tipo_pago, tipo_pago, id_formato_informe, formato_informe, nombre_archivo, copias_impresas, telefono, fax, lote, correo, direccion, solicitante, entrega, propuesta, propietario, observaciones, receptor, id_usuario, miembro_lab, login, clave, usuario, nom_receptor, entrega_muestras, proposito_analisis, nombre_solicitante, inf_copias_impresas, inf_lab, valor_colones, CONVERT(VARCHAR, f_conclusion, 107) AS f_conclusion, cod_informe, inf_lote, num_hoja_trabajo, num_cotizacion, inf_observaciones, observaciones_internas, inf_proposito, normas_fisicas, normas_quimicas, impurezas FROM CENTIINF WHERE cod_informe = '{$_informe}' AND inf_lab = '{$_tipo}';");
    }

    function obtieneMuestrasFertTotalCentinela($_informe)
    {
        return $this->_QUERY("SELECT DISTINCT id_muestra FROM CENTIFERT WHERE id_informe = '{$_informe}';");
    }

    function obtieneMuestrasPlagTotalCentinela($_informe)
    {
        return $this->_QUERY("SELECT DISTINCT id_mst_plag AS id_muestra FROM CENTIPLAG WHERE id_informe = '{$_informe}';");
    }

    function obtieneMuestrasFertCentinela($_informe, $_muestra)
    {
        return $this->_QUERY("SELECT id, id_ingFer, id_informe, id_muestra, id_ingrediente, id_metrica, incertidumbre, encontrada, declarada, limite_inferior, limite_superior, cumple, observacion, metodo, metrica, id_padre, analizar, usar_metodo_lab, masa_masa, masa_volumen, gramos_litro, gramos_kilo, partes_millon, ing_fert_observacion, id_solicitud, id_formulacion, id_recipiente, rechazada, sello, prueba_densidad, registro, producto, codigo_interno, codigo_externo, lote, impurezas, mst_fert_observacion, motivo_rechazo, acta, ingrediente, lab_resi, lab_fert, lab_plag, analizar_fertilizante, metodo_fertilizante, recipiente, formulacion, id_usuario, id_lab, miembro_lab, login, password, usuario, receptor, entrega, proposito, solicitante, notificacion, formato, id_prioridad, id_dependencia, sol_id_lab, sol_id_prioridad, prioridad, sol_id_dependencia, dependencia, sol_id_notificacion, modo_notificacion, id_pago, tipo_pago, id_formato_informe, nombre_formato, nombre_archivo, copias_impresas, telefono, fax, numero_lote, email, direccion, sol_solicitante, entrega_muestras, proposito_analisis, propietario, sol_observacion, sol_receptor FROM CENTIFERT WHERE id_informe = '{$_informe}' AND id_muestra = '{$_muestra}';");
    }

    function obtieneMuestrasPlagCentinela($_informe, $_muestra)
    {
        return $this->_QUERY("SELECT id, id_ing_plag, id_informe, id_mst_plag, id_ingrediente, id_metrica, encontrada, declarada, limite_inferior, limite_superior, incertidumbre, cumple_norma, se_analizo, observacion, metodo, metrica, id_padre, metodo_lab, masa_masa, masa_volumen, gramos_litro, gramos_kilo, partes_millon, ing_plag_observacion, ingrediente, lab_resi, lab_fert, lab_plag, analiza_fert, metodo_fert, id_solicitud, id_formulacion, id_recipiente, rechazada, sello, registro, producto, codigo_interno, codigo_externo, lote, mst_plag_observacion, dosis, motivo_rechazo, impurezas, acta, formulacion, descripcion, recipiente, usuario, miembro_lab, login, clave, nom_usuario, receptor, entrega, proposito_analisis, solicitante, id_modo_notificacion, id_formato_informe, id_prioridad, id_dependencia, id_lab, sol_id_prioridad, prioridad, sol_id_dependencia, dependencia, sol_id_modo_notificacion, sol_modo_notificacion, id_pago, pago, sol_id_formato_informe, formato, archivo, copias_impresas, telefono, fax, numero_lote, correo, direccion, nom_solicitante, entrega_muestras, proposito, propietario, sol_observacion, sol_receptor FROM CENTIPLAG WHERE id_informe = '{$_informe}' AND id_mst_plag = '{$_muestra}';");
    }

    function obtieneDensidadCentinela($_informe, $_muestra)
    {
        return $this->_QUERY("SELECT id, id_mst_fert_plag, id_informe, tipo, metrica, densidad, observacion, metodo FROM CENTIDENSI WHERE id_informe = '{$_informe}' AND id_mst_fert_plag = '{$_muestra}';");
    }

    function obtieneImpurezasCentinela($_informe, $_muestra)
    {
        return $this->_QUERY("SELECT i_IdPk, i_IdMstFert, tipo, i_IdInforme, i_IdIngrediente, ti_IdMetrica, ti_CumpleNorma, dc_Incertidumbre, dc_CtcEncontrada, dc_LimiteMaxPermitido, vc_Metodo, vc_Observacion, c_NmbMetrica, c_NmbIngrediente FROM CENTIIMPUR WHERE i_IdInforme = '{$_informe}' AND i_IdMstFert = '{$_muestra}' ORDER BY i_IdPk DESC;");
    }

    function obtieneEstadoCentinela($_informe)
    {
        return $this->_QUERY("SELECT i_IdPk, i_IdInforme, CONVERT(VARCHAR, d_Estado, 107) AS d_Estado, ti_IdEstado, vc_Motivo FROM CENTIESTADO WHERE i_IdInforme = '{$_informe}' ORDER BY d_Estado DESC;");
    }

    function obtieneFechaIngresoCentinela($_solicitud)
    {
        return $this->_QUERY("SELECT i_IdPk, i_IdSolicitud, CONVERT(VARCHAR, d_Estado, 107) AS d_Estado, ti_IdEstado, vc_Motivo FROM CENTISOLESTADO WHERE i_IdSolicitud = '{$_solicitud}' ORDER BY d_Estado DESC;");
    }

    function obtieneDisolucionCentinela($_informe, $_muestra)
    {
        return $this->_QUERY("SELECT i_IdPk, i_IdMstPlag, i_IdInforme, dc_Incertidumbre, dc_SeparacionAceite, dc_SeparacionSedimento, b_HayParticulasVisibles, b_CumpleNorma, b_SeAnalizo, vc_Observacion, vc_Metodo FROM CENTIESTABACUOSA WHERE i_IdInforme = '{$_informe}' AND i_IdMstPlag = '{$_muestra}';");
    }

    function obtieneSuspensibilidadCentinela($_informe, $_muestra)
    {
        return $this->_QUERY("SELECT i_IdPk, i_IdMstPlag, i_IdInforme, i_IdIngrediente, ti_IdMetrica, b_CumpleNorma, b_SeAnalizo, dc_MinimoPermitido, dc_Incertidumbre, dc_Suspensibilidad, vc_Observacion, vc_Metodo, metrica, ingrediente FROM CENTISUSPENPLAG WHERE i_IdInforme = '{$_informe}' AND i_IdMstPlag = '{$_muestra}' ORDER BY dc_Suspensibilidad;");
    }

    function obtieneHumectabilidadCentinela($_informe, $_muestra)
    {
        return $this->_QUERY("SELECT i_IdPk, i_IdMstPlag, i_IdInforme, dc_Incertidumbre, si_Humectabilidad, b_CumpleNorma, b_SeAnalizo, vc_Observacion, vc_Metodo FROM CENTIHUMPLAG WHERE i_IdInforme = '{$_informe}' AND i_IdMstPlag = '{$_muestra}';");
    }

    function obtieneEmulsionCentinela($_informe, $_muestra)
    {
        return $this->_QUERY("SELECT i_IdPk, i_IdMstPlag, i_IdInforme, dc_Incertidumbre, dc_30minCremado, dc_30minAceite, dc_120minCremado, dc_120minAceite, dc_24hrsCremado, dc_24hrsAceite, b_HayEspontaneidad, b_CumpleNorma, b_SeAnalizo, vc_Observacion, vc_Metodo FROM CENTIESTABEMUL WHERE i_IdInforme = '{$_informe}' AND i_IdMstPlag = '{$_muestra}';");
    }

    function obtieneTamizCentinela($_informe, $_muestra)
    {
        return $this->_QUERY("SELECT i_IdPk, i_IdMstPlag, i_IdInforme, dc_Incertidumbre, dc_Acumulativo, dc_Receptor, b_CumpleNorma, b_SeAnalizo, vc_Observacion, vc_Metodo FROM CENTITAMIZPLAG WHERE i_IdInforme = '{$_informe}' AND i_IdMstPlag = '{$_muestra}';");
    }

//
}

?>
