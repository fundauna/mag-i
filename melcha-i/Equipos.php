<?php

/* * *****************************************************************
  CLASE DE NIVEL 1 ENCARGADA DEL MODULO DE EQUIPOS
  (final SOLO PUEDE SER INSTANCIADA Y NO HEREDADA)
 * ***************************************************************** */

final class Equipos extends Database {

//
    function CalibracionesResumenGet($_equipo, $_lid, $_desde, $_hasta) {
        if ($_equipo == '')
            $op = '<>';
        else
            $op = '=';
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);
        return $this->_QUERY("SELECT EQU000.nombre, EQU000.codigo AS codequipo, EQU003.id, EQU003.codigo, CONVERT(VARCHAR, EQU003.fecha, 105) AS fecha, MAN003.nombre AS ente
		FROM EQU000 INNER JOIN EQU003 ON EQU000.id = EQU003.equipo INNER JOIN MAN003 ON EQU003.proveedor = MAN003.cs
		WHERE (EQU000.id {$op} '{$_equipo}') AND (EQU000.lid = '{$_lid}') AND (EQU003.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00') ORDER BY EQU003.fecha;");
    }

    function CalisConsecutivoGet() {
        $ROW = $this->_QUERY("SELECT calibraciones FROM MAG000;");
        return $ROW[0]['calibraciones'];
    }

    function CalisDetalleGet($_id) {
        return $this->_QUERY("SELECT EQU000.id AS equipo, EQU000.codigo, EQU000.nombre, EQU003.id, EQU003.maestro, EQU003.codigo AS certificado, CONVERT(VARCHAR, EQU003.fecha, 105) AS fecha, EQU003.tolerancia, EQU003.tolerancia2, EQU003.conforme, CONVERT(TEXT, EQU003.justi) AS jusficacion, CONVERT(TEXT, EQU003.obs) AS obs, SEG001.id AS usuario, (SEG001.nombre + ' ' + SEG001.ap1) AS tmp, MAN003.cs AS prov, MAN003.nombre AS proveedor
		FROM EQU000 INNER JOIN EQU003 ON EQU000.id = EQU003.equipo INNER JOIN SEG001 ON EQU003.encargado = SEG001.id INNER JOIN MAN003 ON EQU003.proveedor = MAN003.cs
		WHERE (EQU003.id = {$_id});");
    }

    function CalisIME($_id, $_maestro, $_codigo, $_equipo, $_fecha, $_proveedor, $_tolerancia, $_tolerancia2, $_conforme, $_justi, $_usuario, $_obs, $_accion) {
        $_maestro = $this->FreePost($_maestro);
        $_codigo = $this->FreePost($_codigo);
        $_justi = $this->FreePost($_justi);
        $_obs = $this->FreePost($_obs);
        $_fecha = $this->_FECHA($_fecha);

        if ($this->_TRANS("EXECUTE PA_MAG_057 '{$_id}', '{$_maestro}', '{$_codigo}', '{$_equipo}', '{$_fecha}', '{$_proveedor}', '{$_tolerancia}', '{$_tolerancia2}', '{$_conforme}', '{$_justi}', '{$_usuario}', '{$_obs}', '{$_accion}';")) {
            //
            if ($_accion == 'I')
                $_accion = '0600';
            else
                $_accion = '0601';
            $this->Logger($_accion);
            //
            return 1;
        } else
            return 0;
    }

    function CalisE($_id) {

        if ($this->_TRANS("DELETE FROM EQU003 WHERE id = '{$_id}';")) {
            return 1;
        } else
            return 0;
    }

    function CalisVacio() {
        return array(0 => array(
                'id' => '',
                'maestro' => '',
                'equipo' => '',
                'nombre' => '',
                'conforme' => '',
                'tmp' => '',
                'usuario' => '',
                'tolerancia' => '0',
                'tolerancia2' => '0',
                'certificado' => '',
                'codigo' => '',
                'fecha' => '',
                'prov' => '',
                'proveedor' => '',
                'jusficacion' => '',
                'obs' => ''
        ));
    }

    function CatalogoHistorialIME($_id, $_accion, $_linea, $_fecha, $_descripcion, $_usuario) {
        $_fecha = $this->_FECHA($_fecha);
        if ($_accion == 'I') {
            $ROW = $this->_QUERY("SELECT MAX(linea)+1 AS linea FROM EQU013 WHERE (equipo = '{$_id}');");
            if (!$ROW || $ROW[0]['linea'] == null || $ROW[0]['linea'] == '')
                $ROW[0]['linea'] = 0;
            if ($this->_TRANS("INSERT INTO EQU013 VALUES('{$_id}', '{$ROW[0]['linea']}', '{$_fecha}', '{$_descripcion}', '{$_usuario}');"))
                return 1;
            else
                return 0;
        }elseif ($_accion == 'M') {
            if ($this->_TRANS("UPDATE EQU013 SET fecha = '{$_fecha}', descripcion = '{$_descripcion}', usuario = '{$_usuario}' WHERE equipo = '{$_id}' AND linea = '{$_linea}';"))
                return 1;
            else
                return 0;
        }else {
            if ($this->_TRANS("DELETE FROM EQU013 WHERE equipo = '{$_id}' AND linea = '{$_linea}';"))
                return 1;
            else
                return 0;
        }
    }

    function CatalogoHistorialMuestra($_equipo, $_linea = '') {
        if ($_linea == '')
            return $this->_QUERY("SELECT equipo, linea, CONVERT(VARCHAR,fecha,105) AS fecha, descripcion, usuario FROM EQU013 WHERE (equipo = {$_equipo});");
        else
            return $this->_QUERY("SELECT equipo, linea, CONVERT(VARCHAR,fecha,105) AS fecha, descripcion, usuario FROM EQU013 WHERE (equipo = {$_equipo}) AND linea = '{$_linea}';");
    }

    function CatalogoHistorialMuestraVacio() {
        return array(0 => array(
                'equipo' => '',
                'linea' => '',
                'fecha' => '',
                'descripcion' => '',
                'usuario' => ''
        ));
    }

    function CronoDetalleGet($dia, $mes, $ano) {
        $fecha = '' . $dia . '-' . $mes . '-' . $ano;
        $fecha = $this->_FECHA($fecha);
        return $this->_QUERY("SELECT EQU006.cs, CONVERT(VARCHAR,EQU006.fecha,105) AS fecha, CONVERT(VARCHAR,EQU006.final,105) AS final, CONVERT(VARCHAR,EQU006.aviso,105) AS aviso, EQU006.equipo, EQU006.responsable, EQU006.tipo, EQU006.deshabilita, EQU006.observaciones, EQU000.codigo, EQU000.nombre
		FROM EQU000 INNER JOIN EQU006 ON EQU000.id = EQU006.equipo 
		WHERE (EQU006.fecha = '{$fecha}');");
    }
    
    function CronoDetalleGetEquipos($_cs) {
        return $this->_QUERY("SELECT EQU000.codigo, EQU000.nombre, EQU014.cs, EQU014.equipo FROM EQU014 INNER JOIN EQU000 ON EQU014.equipo = EQU000.id WHERE EQU014.cs = '{$_cs}';");
    }

    function CronoGet($mes, $ano, $_lid) {
        $fechaini = '01/' . $mes . '/' . $ano;
        if ($mes == 2)
            $fechafin = '1/' . $mes . '/' . $ano;
        elseif ($mes == 4 || $mes == 6 || $mes == 9 || $mes == 11)
            $fechafin = '30/' . $mes . '/' . $ano;
        else
            $fechafin = '31/' . $mes . '/' . $ano;

        return $this->_QUERY("SELECT DISTINCT CONVERT(VARCHAR, fecha, 105) AS fecha 
		FROM EQU006 
		WHERE (fecha BETWEEN '{$fechaini}' AND '{$fechafin}') AND lid = '{$_lid}';");
    }

    function CronoGetTipos($fecha) {
        return $this->_QUERY("SELECT DISTINCT tipo FROM EQU006 WHERE fecha = '{$fecha}' ORDER BY tipo;");
    }

    function CronoIME($_cs, $_fecha, $_nueva, $_final, $_aviso, $_equipo, $_responsable, $_tipo, $_deshabilita, $_observaciones, $_accion, $_LID) {
        //SI ES UNA MODIFICACION, FORMATEO LA FECHA NUEVA
        /* if($_accion=='M'){
          list($dia,$mes,) = explode('/',$_nueva);
          if($dia<10) $dia = substr($dia,1,1); //LE QUITAMOS EL CERO
          if($mes<10) $mes = substr($mes,1,1); //LE QUITAMOS EL CERO
          } */
        $_fecha = $this->_FECHA($_fecha);
        $_final = $this->_FECHA($_final);
        $_nueva = $this->_FECHA($_nueva);
        $_aviso = $this->_FECHA($_aviso);

        $aequipos = explode(',', $_equipo);
        if ($_accion == 'I') {
            $Q = $this->_QUERY("SELECT cronograma FROM MAG000;");
            if ($this->_TRANS("INSERT INTO EQU006 VALUES('{$Q[0]['cronograma']}', '{$_fecha}', '{$_final}', '{$_aviso}', '{$aequipos[1]}', '{$_responsable}', '{$_tipo}', '{$_deshabilita}', '{$_observaciones}', '{$_LID}');")) {
                $this->_TRANS("UPDATE MAG000 SET cronograma = cronograma + 1;");
                foreach ($aequipos as $ae) {
                    $this->_TRANS("INSERT INTO EQU014 VALUES ('{$Q[0]['cronograma']}', '{$ae}');");
                }
                
            } else {
                return 0;
            }
        } elseif ($_accion == 'M') {
            if ($this->_TRANS("UPDATE EQU006 SET fecha = '{$_nueva}', final = '{$_final}', equipo = '{$aequipos[0]}', responsable = '{$_responsable}', aviso = '{$_aviso}', tipo = '{$_tipo}', deshabilita = '{$_deshabilita}', observaciones = '{$_observaciones}' WHERE cs = '{$_cs}';")) {
                $this->_TRANS("DELETE FROM EQU014 WHERE cs = '{$_cs}';");
                foreach ($aequipos as $ae) {
                    $this->_TRANS("INSERT INTO EQU014 VALUES ('{$_cs}', '{$ae}');");
                }
            }else{
                return 0;
            }
        } elseif ($_accion == 'D') {
            $this->_TRANS("DELETE FROM EQU014 WHERE cs = '{$_cs}';");
            if (!$this->_TRANS("DELETE FROM EQU006 WHERE cs = '{$_cs}';")) {
                return 0;
            }
        } else {
            return 0;
        }
        $this->Logger($_accion);
        return 1;
    }

    function EquiposAlertaGet($_lab) {
        //EQUIPOS DESHABILITADOS
        //0->'Fuera de uso', 1->'En uso', 2->'Almacenado', 3->'Eliminado
        return $this->_QUERY("SELECT id, codigo, nombre, estado = CASE estado
			WHEN '0' THEN 'Fuera de uso'
			WHEN '1' THEN 'En uso'
                        WHEN '2' THEN 'Almacenado'
			ELSE 'N/A'
		END
		FROM EQU000 WHERE (estado = '0') AND (lid = '{$_lab}');");
    }

    function EquiposCalibrajeGet($_lid, $token = '') {
        if ($token = '1')
            return $this->_QUERY("SELECT id, codigo, nombre, marca, modelo, 'En uso' AS estado, justi FROM EQU000 WHERE (lid='{$_lid}') AND (estado <> '3') ORDER BY codigo;");
        else
            return $this->_QUERY("SELECT id, codigo, nombre, marca, modelo, 'En uso' AS estado, justi
		FROM EQU000 
		WHERE (lid='{$_lid}') AND (control_metro = 1) AND (EQU000.estado = '1') ORDER BY codigo;");
    }

    function EquiposCertificadosDel($_equipo, $_doc) {
        $_doc = str_replace(' ', '', $_doc);
        $_doc = str_replace("{$_equipo}-", '', $_doc);

        $this->Logger('0604');
        return $this->_TRANS("DELETE FROM EQU001 WHERE (equipo={$_equipo}) AND (detalle = '{$_doc}');");
    }

    function EquiposCertificadosGet($_id) {
        return $this->_QUERY("SELECT detalle FROM EQU001 WHERE (equipo='{$_id}');");
    }

    function EquiposCertificadosSet($_id, $_detalle) {
        $_detalle = $this->FreePost($_detalle);
        $this->EquiposCertificadosDel($_id, $_detalle);

        $this->Logger('0605');
        return $this->_QUERY("INSERT INTO EQU001 VALUES('{$_id}', '{$_detalle}');");
    }

    function EquiposColumnasGet($_lid, $tk = '') {
        if ($tk == '')
            return $this->_QUERY("SELECT id, codigo, nombre, '' AS marca, '' AS dimensiones FROM INV002 WHERE (familia = '18') AND (estado = '1') ORDER BY codigo;");
        else
            return $this->_QUERY("SELECT INV002.id, INV002.codigo, INV002.nombre, INV002.marca, INV010.dimensiones FROM INV002 INNER JOIN INV010 ON INV002.id = INV010.producto WHERE (INV002.familia = 18) AND (INV002.estado = '1') ORDER BY INV002.codigo");
    }

    function EquiposComponentesGet($_equipo) {
        return $this->_QUERY("SELECT c_codigo, c_nombre, c_patrimonio, c_marca, c_modelo, c_serie
		FROM EQU004
		WHERE (equipo = {$_equipo});");
    }

    function EquiposComponentesVacio() {
        return array(0 => array(
                'c_codigo' => '',
                'c_nombre' => '',
                'c_patrimonio' => '',
                'c_marca' => '',
                'c_modelo' => '',
                'c_serie' => ''
        ));
    }

    function EquiposComponenteElimina($_c_codigo) {
        if ($this->_TRANS("DELETE FROM EQU004 WHERE (c_codigo = '{$_c_codigo}');")) {
            return 1;
        }
        return 0;
    }

    function EquiposDisponiblesGet($_lid, $_UID) {
        if ($_UID == 0) //ADMINISTRADOR MUESTRA TODOS
            return $this->_QUERY("SELECT id, codigo, nombre, marca, modelo, '' AS justi, estado = CASE estado
			WHEN '1' THEN 'En uso'
			WHEN '2' THEN 'Almacenado'
			END
			FROM EQU000 WHERE (estado = '1' OR estado = '2') AND (lid='{$_lid}')
			ORDER BY codigo;");
        else
        /* return $this->_QUERY("SELECT EQU000.id, EQU000.codigo, EQU000.nombre, estado = CASE EQU000.estado
          WHEN '1' THEN 'En uso'
          WHEN '0' THEN 'Fuera de uso'
          END, EQU000.justi
          FROM EQU000 INNER JOIN PER012 ON EQU000.id = PER012.equipo
          WHERE (estado = '1' OR estado = '2') AND (lid='{$_lid}') AND (PER012.usuario = {$_UID})
          ORDER BY EQU000.nombre;"); */
            return $this->_QUERY("SELECT id, codigo, nombre, marca, modelo, '' AS justi, estado = CASE estado
			WHEN '1' THEN 'En uso'
			WHEN '2' THEN 'Almacenado'
			END
			FROM EQU000 WHERE (estado = '1' OR estado = '2') AND (lid='{$_lid}')
			ORDER BY codigo;");
    }

    function EquiposDetalleGet($_id) {
        return $this->_QUERY("SELECT EQU000.id, EQU000.codigo, EQU000.nombre, EQU000.patrimonio, EQU000.marca, EQU000.modelo, EQU000.serie, CONVERT(TEXT, EQU000.ubi_fabricante) AS ubi_fabricante, 
        CONVERT(VARCHAR, EQU000.fecha, 105) AS fecha, EQU000.temp1, EQU000.temp2, EQU000.hume1, EQU000.hume2, CONVERT(TEXT, EQU000.ubi_equipo) AS ubi_equipo, EQU000.garantia, EQU000.proveedor, EQU000.control_metro, EQU000.tipo_cali, EQU000.tipo_veri, EQU000.tipo_mant, 
        EQU000.otro, EQU000.unidad, EQU000.estado, EQU000.justi, MAN003.nombre AS tmp_proveedor, MAN003.telefono, MAN003.correo
		FROM EQU000 INNER JOIN MAN003 ON EQU000.proveedor = MAN003.cs
		WHERE (EQU000.id = {$_id}) AND (EQU000.estado <> '3');");
    }

    function EquiposErroresGet($_equipo) {
        return $this->_QUERY("SELECT valor, error
		FROM EQU005
		WHERE (equipo = {$_equipo})
		ORDER BY punto;");
    }

    function EquiposErroresVacio() {
        return array(0 => array(
                'valor' => '',
                'error' => ''
        ));
    }

    function EquiposEstandarGet($_lid) {
        return $this->_QUERY("SELECT id, codigo, nombre FROM INV002 WHERE (familia = '4') AND (estado = '1') ORDER BY nombre;");
    }

    function EquiposHistorialGet($_equipo) {
        return $this->_QUERY("SELECT EQU007.id, CONVERT(VARCHAR, EQU007.fecha, 105) AS fecha1, EQU007.descr, EQU000.codigo, EQU000.nombre, (SEG001.nombre +' '+ SEG001.ap1) AS usuario
		FROM EQU007 INNER JOIN EQU000 ON EQU007.equipo = EQU000.id INNER JOIN SEG001 ON EQU007.usuario = SEG001.id
		WHERE (equipo = {$_equipo})
		ORDER BY EQU007.fecha DESC;");
    }

    function EquiposHistorialIME($_id, $_equipo, $_accion, $_fecha, $_descr, $_usuario) {
        $_descr = $this->FreePost($_descr);
        $_fecha = $this->_FECHA($_fecha);
        $_fecha = $this->_FECHA($_fecha);

        if ($this->_TRANS("EXECUTE PA_MAG_061 '{$_id}', '{$_equipo}', '{$_fecha}', '{$_descr}', '{$_usuario}', '{$_accion}';")) {
            if ($_accion == 'I')
                $_accion = '0606';
            else
                $_accion = '0607';
            $this->Logger($_accion);
            return 1;
        } else
            return 0;
    }

    function EquiposIME($_id, $_accion, $_codigo, $_nombre, $_patrimonio, $_marca, $_modelo, $_serie, $_ubi_fabricante, $_fecha, $_temp1, $_temp2, $_hume1, $_hume2, $_ubi_equipo, $_garantia, $_proveedor, $_control_metro, $_tipo_cali, $_tipo_veri, $_tipo_mant, $_otro, $_unidad, $_rango1, $_rango2, $_resolucion, $_error, $_estado, $_justi, $_lab, $_c_codigo, $_c_nombre, $_c_patrimonio, $_c_marca, $_c_modelo, $_c_serie) {
        $_codigo = $this->Free($_codigo);
        $_nombre = $this->Free($_nombre);
        $_patrimonio = $this->Free($_patrimonio);
        $_marca = $this->Free($_marca);
        $_modelo = $this->Free($_modelo);
        $_serie = $this->Free($_serie);
        $_ubi_fabricante = $this->Free($_ubi_fabricante);
        $_ubi_equipo = $this->Free($_ubi_equipo);
        $_garantia = $this->Free($_garantia);
        $_justi = $this->Free($_justi);
        $_otro = $this->Free($_otro);
        //
        $_rango1 = explode('&', substr($_rango1, 1));
        $_rango2 = explode('&', substr($_rango2, 1));
        $_resolucion = explode('&', substr($_resolucion, 1));
        $_error = str_replace(',', '', $_error);
        //
        $_c_codigo = explode('&', substr($_c_codigo, 1));
        $_c_nombre = explode('&', substr($_c_nombre, 1));
        $_c_patrimonio = explode('&', substr($_c_patrimonio, 1));
        $_c_marca = explode('&', substr($_c_marca, 1));
        $_c_modelo = explode('&', substr($_c_modelo, 1));
        $_c_serie = explode('&', substr($_c_serie, 1));
        //
        //$_valor = explode('&', substr($_valor, 1));
        $_error = explode('&', substr($_error, 1));

        $_fecha = $this->_FECHA2($_fecha);

        if ($_accion == 'I') {
            $ROW = $this->_QUERY("SELECT equipos FROM MAG000;");
            $_id = $ROW[0]['equipos'];
        }

        if ($this->_TRANS("EXECUTE PA_MAG_016 '{$_id}', '{$_accion}', '{$_codigo}', '{$_nombre}', '{$_patrimonio}', '{$_marca}', '{$_modelo}', '{$_serie}', '{$_ubi_fabricante}', '{$_fecha}',  '{$_temp1}', '{$_temp2}', '{$_hume1}', '{$_hume2}', '{$_ubi_equipo}', '{$_garantia}', '{$_proveedor}', '{$_control_metro}', '{$_tipo_cali}', '{$_tipo_veri}', '{$_tipo_mant}', '{$_otro}', '{$_unidad}', '{$_estado}', '{$_justi}', '{$_lab}';")) {
            if ($_accion != 'D') {
                $this->_TRANS("DELETE FROM EQU004 WHERE (equipo = {$_id});");
                $this->_TRANS("DELETE FROM EQU005 WHERE (equipo = {$_id});");
                $this->_TRANS("DELETE FROM EQU008 WHERE (equipo = {$_id});");
                for ($i = 0; $i < count($_c_codigo); $i++) {
                    if (str_replace(' ', '', $_c_codigo[$i]) != '')
                        $this->_TRANS("INSERT INTO EQU004 values('{$_id}', '{$_c_codigo[$i]}', '{$_c_nombre[$i]}', '{$_c_patrimonio[$i]}', '{$_c_marca[$i]}', '{$_c_modelo[$i]}', '{$_c_serie[$i]}');");
                }
                /* for($i=0;$i<count($_valor);$i++){
                  if(str_replace(' ', '', $_valor[$i]) != '' and $_valor[$i] != '0' and $_error[$i] != '0' )
                  $this->_TRANS("INSERT INTO EQU005 values('{$_id}', '{$i}', '{$_valor[$i]}', '{$_error[$i]}');");
                  } */
                for ($i = 0; $i < count($_rango1); $i++) {
                    if (str_replace(' ', '', $_rango1[$i]) != '')
                        $this->_TRANS("INSERT INTO EQU008 values('{$_id}', '{$i}', '{$_rango1[$i]}', '{$_rango2[$i]}', '{$_resolucion[$i]}', '{$_error[$i]}');");
                }
            }
            //
            if ($_accion == 'I')
                $_accion = '0608';
            elseif ($_accion == 'M')
                $_accion = '0609';
            else
                $_accion = '0610';
            $this->Logger($_accion);
            //
            return 1;
        } else
            return 0;
    }

    function EquiposProveedoresHistorial($_equipo) {
        $ROW = $this->_QUERY("SELECT EQU002.id, CONVERT(VARCHAR,EQU002.fecha,105) AS fecha, EQU002.tipo, EQU002.detalle, MAN003.nombre FROM EQU002 INNER JOIN
                         EQU009 ON EQU002.id = EQU009.mantenimiento INNER JOIN MAN003 ON EQU009.proveedor = MAN003.cs WHERE (EQU002.equipo = {$_equipo}) ORDER BY EQU002.fecha");

        $str = '<table width="100%"><tr>
			<td><strong>Proveedor</strong></td>
			<td><strong>Tipo Mantenimiento</strong></td>
			<td><strong>Fecha</strong></td>
			<td><strong>Descripci&oacute;n</strong></td>
			<td><strong>Adjunto</strong></td>
		</tr>';
        for ($x = 0; $x < count($ROW); $x++) {
            if ($ROW[$x]['tipo'] == '0')
                $ROW[$x]['tipo'] = 'Correctivo';
            if ($ROW[$x]['tipo'] == '1')
                $ROW[$x]['tipo'] = 'Preventivo';
            if ($ROW[$x]['tipo'] == '2')
                $ROW[$x]['tipo'] = 'Rutinario';
            $str .= "<tr>
				<td>{$ROW[$x]['nombre']}</td>
				<td>{$ROW[$x]['tipo']}</td>
				<td>{$ROW[$x]['fecha']}</td>
				<td>{$ROW[$x]['detalle']}</td>
				<td><a href='../../caspha-i/docs/equipos/man/{$ROW[$x]['id']}.zip'>Descargar</a></td>
			</tr>";
        }
        $str .= '</table>';
        echo $str;
    }

    function EquiposRangosGet($_equipo) {
        return $this->_QUERY("SELECT rango1, rango2, resolucion, error
		FROM EQU008
		WHERE (equipo = {$_equipo})
		ORDER BY linea;");
    }

    function EquiposBalanzas($_lid = '') {
        if ($_lid == '')
            return $this->_QUERY("SELECT id, codigo, nombre FROM EQU000 WHERE (nombre like '%balanza%') AND (estado <> '3');");
        else
            return $this->_QUERY("SELECT id, codigo, nombre FROM EQU000 WHERE (nombre like '%balanza%') AND (estado <> '3') AND lid = '{$_lid}';");
    }

    function EquiposMicropipetas() {
        return $this->_QUERY("SELECT id, codigo, nombre FROM EQU000 WHERE (nombre like '%micropipeta%') AND (estado <> '3');");
    }

    function EquiposRangosVacio() {
        return array(0 => array(
                'rango1' => '',
                'rango2' => '',
                'resolucion' => '',
                'error' => ''
        ));
    }

    function EquiposResumenGet($_id, $_nombre, $_lid, $_busqueda = 1) {
        if ($_busqueda == 1)
            $extra = " WHERE (codigo LIKE '%{$_id}%')";
        else
            $extra = " WHERE (nombre LIKE '%{$_nombre}%')";
        //0->'Fuera de uso', 1->'En uso', 2->'Almacenado', 3->'Eliminado
        return $this->_QUERY("SELECT id, codigo, nombre, estado = CASE estado
				WHEN '0' THEN 'Fuera de uso'
				WHEN '1' THEN 'En uso'
				WHEN '2' THEN 'Almacenado'
				ELSE 'N/A'
			END
			FROM EQU000 {$extra} AND estado <> '3' AND (lid='{$_lid}');");
    }

    function EquiposVacio() {
        return array(0 => array(
                'id' => '',
                'codigo' => '',
                'nombre' => '',
                'patrimonio' => '',
                'marca' => '',
                'modelo' => '',
                'serie' => '',
                'ubi_fabricante' => '',
                'fecha' => '',
                'temp1' => '',
                'temp2' => '',
                'hume1' => '',
                'hume2' => '',
                'ubi_equipo' => '',
                'garantia' => '',
                'proveedor' => '',
                'tmp_proveedor' => '',
                'control_metro' => '',
                'tipo_cali' => '',
                'tipo_veri' => '',
                'tipo_mant' => '',
                'otro' => '',
                'unidad' => '',
                'rango1' => '0',
                'rango2' => '0',
                'resolucion' => '0',
                'estado' => '',
                'justi' => ''
        ));
    }

    function ManteConsecutivoGet($_tipo) {
        $ROW = $this->_QUERY("SELECT mantenimientos FROM MAG000;");
        return $ROW[0]['mantenimientos'];
    }

    function MantesDetalleGet($_id) {
        $ROW = $this->_QUERY("SELECT EQU000.codigo AS tmp_equipo, EQU000.nombre, EQU002.id, EQU002.maestro, EQU002.codigo AS reporte, EQU002.equipo, CONVERT(VARCHAR, EQU002.fecha, 105) AS fecha, EQU002.tipo, EQU002.detalle, SEG001.id AS usuario, (SEG001.nombre + ' ' + SEG001.ap1) AS tmp, '' AS prov, '' AS proveedor
		FROM EQU000 INNER JOIN EQU002 ON EQU000.id = EQU002.equipo INNER JOIN SEG001 ON EQU002.encargado = SEG001.id
		WHERE (EQU002.id = {$_id});");

        $ROW2 = $this->_QUERY("SELECT MAN003.cs AS prov, MAN003.nombre AS proveedor
		FROM MAN003 INNER JOIN EQU009 ON MAN003.cs = EQU009.proveedor
		WHERE (EQU009.mantenimiento = {$_id});");
        if ($ROW2) {
            $ROW[0]['prov'] = $ROW2[0]['prov'];
            $ROW[0]['proveedor'] = $ROW2[0]['proveedor'];
        }
        return $ROW;
    }

    function MantesIME($_id, $_maestro, $_codigo, $_equipo, $_fecha, $_tipo, $_detalle, $_usuario, $_prov, $_accion) {
        $_id = str_replace(' ', '', $_id);
        $_maestro = $this->FreePost($_maestro);
        $_detalle = $this->FreePost($_detalle);
        $_fecha = $this->_FECHA($_fecha);

        if ($this->_TRANS("EXECUTE PA_MAG_019 '{$_id}', '{$_maestro}', '{$_codigo}', '{$_equipo}', '{$_fecha}', '{$_tipo}', '{$_detalle}', '{$_usuario}', '{$_prov}', '{$_accion}';")) {
            //
            if ($_accion == 'I')
                $_accion = '0611';
            else
                $_accion = '0612';
            $this->Logger($_accion);
            //
            return 1;
        } else
            return 0;
    }

    function MantesE($_id) {
        if ($this->_TRANS("DELETE FROM EQU009 WHERE mantenimiento = '{$_id}';")) {
            $this->_TRANS("DELETE FROM EQU002 WHERE id = '{$_id}';");
            return 1;
        } else
            return 0;
    }

    function MantesResumenGet($_equipo, $_lid, $_desde, $_hasta) {
        if ($_equipo == '')
            $op = '<>';
        else
            $op = '=';
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);

        return $this->_QUERY("SELECT EQU000.nombre, EQU002.id, EQU002.codigo, CONVERT(VARCHAR, EQU002.fecha, 105) AS fecha, tipo =
		CASE EQU002.tipo
			WHEN '0' THEN 'Correctivo'
			WHEN '1' THEN 'Preventivo'
			WHEN '2' THEN 'Rutinario'
		END
		FROM EQU000 INNER JOIN EQU002 ON EQU000.id = EQU002.equipo
		WHERE (EQU000.id {$op} '{$_equipo}') AND (EQU000.lid = '{$_lid}') AND (EQU002.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00');");
    }

    function MantesVacio() {
        return array(0 => array(
                'id' => '',
                'maestro' => '',
                'equipo' => '',
                'prov' => '',
                'usuario' => '',
                'tmp_equipo' => '',
                'fecha' => '',
                'tipo' => '',
                'tmp' => '',
                'proveedor' => '',
                'reporte' => '',
                'detalle' => ''
        ));
    }

    function UsoDetalleGet($_equipo, $_usuario, $_fecha, $_accion) {
        $_fecha = $this->_FECHA($_fecha);
        return $this->_QUERY("SELECT EQU000.codigo AS equipo, EQU000.nombre AS tmp_equipo, EQUBIT.usuario, EQUBIT.fecha, EQUBIT.horas, EQUBIT.ref, EQUBIT.accion, (SEG001.nombre + ' '+ SEG001.ap1) AS analista
		FROM EQU000 INNER JOIN EQUBIT ON EQU000.id = EQUBIT.equipo INNER JOIN SEG001 ON EQUBIT.usuario = SEG001.id
		WHERE (EQUBIT.equipo = '{$_equipo}') AND (EQUBIT.usuario = '{$_usuario}') AND (EQUBIT.fecha BETWEEN '{$_fecha}' AND '{$_fecha} 23:59:00') AND (EQUBIT.accion = '{$_accion}');");
    }

    function UsoIME($_equipo, $_horas, $_referencia, $_accion, $_usuario) {
        if ($this->_TRANS("EXECUTE PA_MAG_110 '{$_equipo}', '{$_usuario}', '{$_horas}', '{$_referencia}', '{$_accion}';")) {
            $this->Logger('0613');
            return 1;
        } else
            return 0;
    }

    function UsoResumenGet($_equipo, $_lid, $_desde, $_hasta) {
        if ($_equipo == '')
            $op = '<>';
        else
            $op = '=';
        $_desde = $this->_FECHA($_desde);
        $_hasta = $this->_FECHA($_hasta);

        return $this->_QUERY("SELECT EQU000.id, EQU000.codigo, EQU000.nombre, EQUBIT.usuario, CONVERT(VARCHAR, EQUBIT.fecha, 105) AS fecha, EQUBIT.horas, EQUBIT.accion
		FROM EQU000 INNER JOIN EQUBIT ON EQU000.id = EQUBIT.equipo
		WHERE (EQU000.id {$op} '{$_equipo}') AND (EQU000.lid = '{$_lid}') AND (EQUBIT.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00');");
    }

    function UsoVacio() {
        return array(0 => array(
                'equipo' => '',
                'tmp_equipo' => '',
                'horas' => '',
                'ref' => '',
                'accion' => ''
        ));
    }

//	
}

?>
