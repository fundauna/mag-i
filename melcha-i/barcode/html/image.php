<?php
$class_dir = '..'.DIRECTORY_SEPARATOR.'class';
require_once($class_dir.DIRECTORY_SEPARATOR.'BCGColor.php');
require_once($class_dir.DIRECTORY_SEPARATOR.'BCGBarcode.php');
require_once($class_dir.DIRECTORY_SEPARATOR.'BCGDrawing.php');
include_once($class_dir.DIRECTORY_SEPARATOR.'BCGcode39.barcode.php');
include_once('config'.DIRECTORY_SEPARATOR.'BCGBarcode1D.php');

$color_white = new BCGColor(255, 255, 255);
$code_generated = new BCGcode39();
$code_generated->setScale(1);
$code_generated->parse($_GET['text']);    
$drawing = new BCGDrawing('', $color_white);
$drawing->setBarcode($code_generated);
$drawing->setRotationAngle(0);
$drawing->setDPI(300);
$drawing->draw();
$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
?>