<?php
/*******************************************************************
CLASE DE NIVEL 1 ENCARGADA DEL MODULO DE PROVEEDORES
(final SOLO PUEDE SER INSTANCIADA Y NO HEREDADA)
*******************************************************************/
final class Proveedores extends Database{
//	
	function ClasificacionesIME($_cs, $_nombre, $_accion, $_lab){	
		$_nombre = $this->Free($_nombre);
				
		if($this->_TRANS("EXECUTE PA_MAG_037 '{$_cs}', '{$_nombre}', '{$_lab}', '{$_accion}';")){
			//
			if($_accion=='I') $_accion = 'B000';
			elseif($_accion=='M') $_accion = 'B001';
			else $_accion = 'B002';
			$this->Logger($_accion);
			//
			return 1;
		}else return 0;
	}
	
	function ClasificacionesMuestra($_lab=''){
		if($_lab=='') $op = '<>';
		else $op = '=';
		return $this->_QUERY("SELECT cs, nombre FROM MAN007 WHERE (lid {$op} '{$_lab}');");
	}
	
	function EvaIME($_id, $_accion, $_prov, $_orden, $_lineas, $_total, $_yo, $_precio1, $_precio2, $_porc, $_calif1, $_calif2, $_calif3, $_calif4, $_calif5, $_calif6, $_calif7, $_calif8){
		$_orden = $this->Free($_orden);
		$_lineas = $this->Free($_lineas);
		if($_calif1 == '') $_calif1 = 0;
		if($_calif2 == '') $_calif2 = 0;
		if($_calif3 == '') $_calif3 = 0;
		if($_calif4 == '') $_calif4 = 0;
		if($_calif5 == '') $_calif5 = 0;
		if($_calif6 == '') $_calif6 = 0;
		if($_calif7 == '') $_calif7 = 0;
		if($_calif8 == '') $_calif8 = 0;
		
		if($this->_TRANS("EXECUTE PA_MAG_052 '{$_id}', '{$_accion}', '{$_prov}', '{$_orden}', '{$_lineas}', '{$_total}', '{$_yo}', '{$_precio1}', '{$_precio2}', '{$_porc}', '{$_calif1}', '{$_calif2}', '{$_calif3}', '{$_calif4}', '{$_calif5}', '{$_calif6}', '{$_calif7}', '{$_calif8}';")){
			//
			if($_accion=='I') $_accion = 'B003';
			else $_accion = 'B004';
			$this->Logger($_accion);
			//
			return 1;
		}else return 0;
	}
	
	function EvasDetalleGet($_id){
		return $this->_QUERY("SELECT MAN012.id, MAN012.proveedor, CONVERT(VARCHAR, MAN012.fecha, 105) AS fecha, MAN012.orden, MAN012.lineas, MAN012.calificacion, MAN012.funcionario, MAN012.precio1, MAN012.precio2, MAN012.porc, MAN012.calif1, MAN012.calif2, MAN012.calif3, MAN012.calif4, MAN012.calif5, MAN012.calif6, MAN012.calif7, MAN012.calif8, MAN003.nombre, MAN003.naturaleza AS tipo_prov, (SEG001.nombre +' '+ SEG001.ap1 +' '+ SEG001.ap2) AS funcionario
		FROM MAN012 INNER JOIN MAN003 ON MAN012.proveedor = MAN003.cs INNER JOIN SEG001 ON MAN012.funcionario = SEG001.id
		WHERE (MAN012.id = {$_id});");
	}
	
	function EvasResumenGet($_desde, $_hasta, $_nombre){
		$_desde = $this->_FECHA($_desde);
		$_hasta = $this->_FECHA($_hasta);
		return $this->_QUERY("SELECT MAN003.nombre, MAN012.id, CONVERT(VARCHAR, MAN012.fecha, 105) AS fecha1, MAN012.orden, MAN012.calificacion
		FROM MAN012 INNER JOIN MAN003 ON MAN012.proveedor = MAN003.cs
		WHERE (MAN003.nombre LIKE '%{$_nombre}%') AND (MAN012.fecha BETWEEN '{$_desde}' AND '{$_hasta} 23:59:00');");
	}
	
	function EvaVacio(){
		return array(0=>array(	
			'id'=>'',
			'proveedor'=>'',
			'tipo_prov'=>'',
			'nombre'=>'',
			'orden'=>'',
			'lineas'=>'',
			'fecha'=>'',
			'funcionario'=>'',
			'precio1'=>'0.00',
			'precio2'=>'0.00',
			'porc'=>'0',
			'calif1'=>'',
			'calif2'=>'',
			'calif3'=>'',
			'calif4'=>'',
			'calif5'=>'',
			'calif6'=>'',
			'calif7'=>'',
			'calif8'=>''	
		));
	}
	
	function ProveedoresConsecutivo(){
		$ROW = $this->_QUERY("SELECT proveedores FROM MAG000;");
		return $ROW[0]['proveedores'];
	}
	
	function ProveedoresContactosGet($_id){
		return $this->_QUERY("SELECT contacto, area, telefono, extension, directo, Otelefono, email, Oemail 
		FROM MAN006 
		WHERE (proveedor = {$_id});");
	}
	
	function ProveedoresContactosVacio(){
		return array(0=>array(	
			'contacto'=>'',		
			'area'=>'',
			'telefono'=>'',
			'extension'=>'',
			'directo'=>'',
			'Otelefono'=>'',
			'email'=>'',
			'Ocorreo'=>'',
                        'Oemail'=>''
		));
	}
	
	function ProveedoresDetalleGet($_id){
		return $this->_QUERY("SELECT cs, id, nombre, tipo, naturaleza, direccion, telefono, fax, otro, correo, notas, clasificacion, LRE, LDP, LCC, critico
		FROM MAN003
		WHERE (cs = {$_id});");
	}
	
	function ProveedoresIME($_cs, $_accion, $_tipo, $_naturaleza, $_id, $_nombre, $_direccion, $_contacto, $_area, $_telefonos, $_extension, $_directo, $_Otelefono, $_correo, $_Ocorreo, $_telefono, $_fax, $_otro, $_email, $_notas, $_clasificacion, $_LRE, $_LDP, $_LCC, $_critico){	
		$_contacto = str_replace('1=1&','',$_contacto);
		$_contacto = explode('&',$_contacto);
		$_area = str_replace('1=1&','',$_area);
		$_area = explode('&',$_area);
		$_telefonos = str_replace('1=1&','',$_telefonos);
		$_telefonos = explode('&',$_telefonos);
		$_extension = str_replace('1=1&','',$_extension);
		$_extension = explode('&',$_extension);
		$_directo = str_replace('1=1&','',$_directo);
		$_directo = explode('&',$_directo);
		$_Otelefono = str_replace('1=1&','',$_Otelefono);
		$_Otelefono = explode('&',$_Otelefono);
		$_correo = str_replace('1=1&','',$_correo);
		$_correo = explode('&',$_correo);
		$_Ocorreo = str_replace('1=1&','',$_Ocorreo);
		$_Ocorreo = explode('&',$_Ocorreo);
		$_nombre = $this->Free($_nombre);		
		$_direccion = $this->Free($_direccion);
		$_notas = $this->Free($_notas);
		
		if($_accion == 'I') $_cs = $this->ProveedoresConsecutivo();
		
		if($this->_TRANS("EXECUTE PA_MAG_029 '{$_cs}', '{$_accion}', '{$_tipo}', '{$_naturaleza}', '{$_id}', '{$_nombre}', '{$_direccion}', '{$_telefono}', '{$_fax}', '{$_otro}', '{$_email}', '{$_notas}', '{$_clasificacion}', '{$_LRE}', '{$_LDP}', '{$_LCC}', '{$_critico}';")){	
			if($_accion == 'I'){		
				for($i=0;$i<count($_contacto);$i++){
					$_contacto[$i] = $this->Free($_contacto[$i]);
					$_area[$i] = $this->Free($_area[$i]);
					$_telefonos[$i] = $this->Free($_telefonos[$i]);
					$this->_TRANS("INSERT INTO MAN006 values('{$_cs}','{$i}','{$_contacto[$i]}','{$_area[$i]}','{$_telefonos[$i]}', '{$_extension[$i]}','{$_directo[$i]}','{$_Otelefono[$i]}','{$_correo[$i]}', '{$_Ocorreo[$i]}');");	
				}
			}elseif($_accion == 'M'){
				$this->_TRANS("DELETE FROM MAN006 WHERE proveedor = '{$_cs}';");				
				for($i=0;$i<count($_contacto);$i++){
					$_contacto[$i] = $this->Free($_contacto[$i]);
					$_area[$i] = $this->Free($_area[$i]);
					$_telefonos[$i] = $this->Free($_telefonos[$i]);
					
					if(str_replace(' ','',$_contacto[$i])!='')					
						$this->_TRANS("INSERT INTO MAN006 values('{$_cs}','{$i}','{$_contacto[$i]}','{$_area[$i]}','{$_telefonos[$i]}', '{$_extension[$i]}','{$_directo[$i]}','{$_Otelefono[$i]}','{$_correo[$i]}', '{$_Ocorreo[$i]}');");		
				}
			}			
			//
			if($_accion=='I') $_accion = 'B005';
			elseif($_accion=='M') $_accion = 'B006';
			else $_accion = 'B007';
			$this->Logger($_accion);
			//
			return 1;
		}else return 0;
	}
        
        function ProveedoresContactos_IME($_cs, $_accion,$_contacto,$_area,$_telefono,$_extension,$_directo,$_Otelefono,$_correo,$_Ocorreo,$borrar=-1){
                $_contacto = str_replace('1=1&','',$_contacto);
		$_contacto = explode('&',$_contacto);
		$_area = str_replace('1=1&','',$_area);
		$_area = explode('&',$_area);
		$_telefono = str_replace('1=1&','',$_telefono);
		$_telefono = explode('&',$_telefono);
		$_extension = str_replace('1=1&','',$_extension);
		$_extension = explode('&',$_extension);
		$_directo = str_replace('1=1&','',$_directo);
		$_directo = explode('&',$_directo);
		$_Otelefono = str_replace('1=1&','',$_Otelefono);
		$_Otelefono = explode('&',$_Otelefono);
		$_correo = str_replace('1=1&','',$_correo);
		$_correo = explode('&',$_correo);
		$_Ocorreo = str_replace('1=1&','',$_Ocorreo);
		$_Ocorreo = explode('&',$_Ocorreo);
            if($_accion=='M'){
                $this->_TRANS("DELETE FROM MAN006 WHERE proveedor = '{$_cs}';");
                for($i=0;$i<count($_contacto);$i++){
                    if ($borrar != $i) {
                    $this->_TRANS("INSERT INTO MAN006 values('{$_cs}','{$i}','{$_contacto[$i]}','{$_area[$i]}','{$_telefono[$i]}', '{$_extension[$i]}','{$_directo[$i]}','{$_Otelefono[$i]}','{$_correo[$i]}', '{$_Ocorreo[$i]}');");
                }
            }
                return 1;
            }
        }
	
	function ProveedoresResumenGet($_id, $_nombre, $_filtro, $_tipo, $_lab, $_busqueda=1, $ordenado=false){
		if($_busqueda == 1) $extra = " (MAN003.id LIKE '%{$_id}%')";
		else $extra = " (MAN003.nombre LIKE '%{$_nombre}%')";
		//ESTA BUSCANDO POR EL LAB ESPECIFICO
		if($_filtro=='1'){
			if($_lab=='1') $extra .= " AND (MAN003.LRE = 1)";
			elseif($_lab=='2') $extra .= " AND (MAN003.LDP = 1)";
			elseif($_lab=='3') $extra .= " AND (MAN003.LCC = 1)";
		}
		
		if($_tipo == '') $op = '<>';
		else $op = '=';
		
		if($ordenado) $extra2 = 'ORDER BY MAN003.nombre';
		else $extra2 = '';
		
		return $this->_QUERY("SELECT MAN003.cs, MAN003.id, MAN003.nombre, MAN003.naturaleza, MAN003.telefono, MAN003.fax, MAN007.nombre AS clasificacion
		FROM MAN003 INNER JOIN MAN007 ON MAN003.clasificacion = MAN007.cs
		WHERE {$extra} AND (MAN003.clasificacion {$op} '{$_tipo}') {$extra2};");
	}
	
	function ProveedoresVacio(){
		return array(0=>array(	
			'cs'=>'',		
			'id'=>'',
			'nombre'=>'',
			'tipo'=>'',
			'naturaleza'=>'',
			'direccion'=>'',
			'contacto'=>'',
			'telefono'=>'',
			'fax'=>'',
			'otro'=>'',
			'correo'=>'',			
			'clasificacion'=>'',
			'notas'=>'',
			'area'=>'',
			'tel'=>'',
			'email'=>'',
			'LRE'=>'0',
			'LDP'=>'0',
			'LCC'=>'0',
                        'critico'=>'0',
		));
	}
//
}
?>