<?php

/* * *****************************************************************
  CLASE DE NIVEL 1 ENCARGADA DEL MODULO DE INVENTARIO
  (final SOLO PUEDE SER INSTANCIADA Y NO HEREDADA)

  CATALOGO DE OBJETOS RELACIONADOS A LOS CAMPOS OPCIONALES DEL INVENTARIO
  P�ginas: 	inventaro_detalle.php, js y shell
  M�todos: 	InventarioOpcionalesIME
  Proced.:	PA_MAG_50, 131
  Tablas:		INVPLA
 * ***************************************************************** */

final class Inventario extends Database
{

//
    function ArqueoIME($_analisis, $_consumible, $_cantidad, $_obs, $_accion)
    {
        $_obs = $this->Free($_obs);

        if ($this->_TRANS("EXECUTE PA_MAG_022 '{$_analisis}', '{$_consumible}', '{$_cantidad}', '{$_obs}', '{$_accion}';")) {
            $this->Logger('A000');
            return 1;
        } else
            return 0;
    }

    function ArqueoRequerido($_analisis)
    {
        return $this->_QUERY("SELECT INV002.id, INV002.codigo, INV002.nombre, INV002.stock1, INV002.stock2, INV048.cantidad, INV048.obs, INV001.nombre AS unidades
		FROM INV002 INNER JOIN  INV048 ON INV002.id = INV048.inventario INNER JOIN INV001 ON INV002.presentacion = INV001.id
		WHERE (INV048.metodo = {$_analisis})
		ORDER BY INV002.codigo");
    }

    function General_inventario($_id = '', $_codigo = '', $_descripcion = '', $_parte = '', $_estado = '')
    {
        $extra = '';
        if ($_id != '') {
            $extra .= " AND INV062.id = '{$_id}'";
        }
        if ($_codigo != '') {
            $extra .= " AND (INV062.codigo like '%{$_codigo}%')";
        }
        if ($_descripcion != '') {
            $extra .= " AND (INV062.descripcion like '%{$_descripcion}%')";
        }
        if ($_parte != '') {
            $extra .= " AND (INV062.parte like '%{$_parte}%')";
        }
        if ($_estado != '') {
            $extra .= " AND (INV062.estado like '%{$_estado}%')";
        }

        return $this->_QUERY("SELECT id, codigo, descripcion, ubicacion, cantidadlcc, cantidadpavas, cantidadtotal, cantidadtransito, nivel, costounitariod, costounitarioc, costototal, presentacion, parte, equipo, ingreso, analisis, consumoanalisis, cantidadanalisis, consumoanual, medida, seguridadmax, reorden, vencimiento, estado, pedir, abc, obs, encargado FROM INV062 WHERE 1=1 {$extra};");
    }

    function General_inventarioIME($_id, $_accion, $_codigo, $_descripcion, $_ubicacion, $_cantidadlcc, $_cantidadpavas, $_cantidadtotal, $_cantidadtransito, $_nivel, $_costounitariod, $_costounitarioc, $_costototal, $_presentacion, $_parte, $_equipo, $_ingreso, $_analisis, $_consumoanalisis, $_cantidadanalisis, $_consumoanual, $_medida, $_seguridadmax, $_reorden, $_vencimiento, $_estado, $_pedir, $_abc, $_obs, $_encargado)
    {
        $_codigo = $this->Free($_codigo);
        $_descripcion = $this->Free($_descripcion);
        $_ubicacion = $this->Free($_ubicacion);
        $_obs = $this->Free($_obs);
        $_encargado = $this->Free($_encargado);
        if ($_accion == 'I') {
            $Q = $this->_QUERY("SELECT MAX(id)+1 AS cs FROM INV062");
            $this->_TRANS("INSERT INTO INV062 VALUES('{$Q[0]['cs']}', '{$_codigo}', '{$_descripcion}', '{$_ubicacion}', '{$_cantidadlcc}', '{$_cantidadpavas}', '{$_cantidadtotal}', '{$_cantidadtransito}', '{$_nivel}','{$_costounitariod}', '{$_costounitarioc}', '{$_costototal}', '{$_presentacion}', '{$_parte}', '{$_equipo}', '{$_ingreso}', '{$_analisis}', '{$_consumoanalisis}', '{$_cantidadanalisis}', '{$_consumoanual}', '{$_medida}', '{$_seguridadmax}', '{$_reorden}', '{$_vencimiento}', '{$_estado}', '{$_pedir}', '{$_abc}', '{$_obs}', '{$_encargado}');");
            //
        } elseif ($_accion == 'M') {
            $this->_TRANS("UPDATE INV062 SET codigo = '{$_codigo}', descripcion = '{$_descripcion}', ubicacion = '{$_ubicacion}', cantidadlcc = '{$_cantidadlcc}', cantidadpavas = '{$_cantidadpavas}', cantidadtotal = '{$_cantidadtotal}', cantidadtransito = '{$_cantidadtransito}', nivel = '{$_nivel}', costounitariod = '{$_costounitariod}', costounitarioc = '{$_costounitarioc}', costototal = '{$_costototal}', presentacion = '{$_presentacion}', parte = '{$_parte}', equipo = '{$_equipo}', ingreso = '{$_ingreso}', analisis = '{$_analisis}', consumoanalisis = '{$_consumoanalisis}', cantidadanalisis = '{$_cantidadanalisis}', consumoanual = '{$_consumoanual}', medida = '{$_medida}', seguridadmax = '{$_seguridadmax}', reorden = '{$_reorden}', vencimiento = '{$_vencimiento}', estado = '{$_estado}', pedir = '{$_pedir}', abc = '{$_abc}', obs= '{$_obs}', encargado = '{$_encargado}' WHERE id = '{$_id}';");
        } else {
            $this->_TRANS("DELETE FROM INV062 WHERE id = '{$_id}';");
        }
        if ($_accion == 'I') {
            $_accion = 'A003';
        } elseif ($_accion == 'M')
            $_accion = 'A004';
        else {
            $_accion = 'A005';
        }
        $this->Logger($_accion);
        //
        return 1;
    }

    function General_inventarioVacio()
    {
        return array(0 => array(
            'id' => '0',
            'codigo' => '',
            'descripcion' => '',
            'ubicacion' => '',
            'cantidadlcc' => '',
            'cantidadpavas' => '',
            'cantidadtotal' => '',
            'cantidadtransito' => '',
            'nivel' => '',
            'costounitariod' => '',
            'costounitarioc' => '',
            'costototal' => '',
            'presentacion' => '',
            'parte' => '',
            'equipo' => '',
            'ingreso' => '',
            'analisis' => '',
            'consumoanalisis' => '',
            'cantidadanalisis' => '',
            'consumoanual' => '',
            'medida' => '',
            'seguridadmax' => '',
            'reorden' => '',
            'vencimiento' => '',
            'estado' => '',
            'pedir' => '',
            'abc' => '',
            'obs' => '',
            'encargado' => ''
        ));
    }

    function IngresoSalida($_id = '', $_tipo = '', $_codigo = '', $_descripcion = '', $_orden = '', $_factura = '', $_proveedor = '', $_desde = '', $_hasta = '')
    {
        $extra = '';
        if ($_id != '') {
            $extra .= " AND INV063.id = '{$_id}'";
        }
        if ($_tipo != '') {
            $extra .= " AND (INV063.tipo = '{$_tipo}')";
        }
        if ($_codigo != '') {
            $extra .= " AND (INV063.codigo like '%{$_codigo}%')";
        }
        if ($_descripcion != '') {
            $extra .= " AND (INV063.descripcion like '%{$_descripcion}%')";
        }
        if ($_orden != '') {
            $extra .= " AND (INV063.orden like '%{$_orden}%')";
        }
        if ($_factura != '') {
            $extra .= " AND (INV063.factura like '%{$_factura}%')";
        }
        if ($_proveedor != '') {
            $extra .= " AND (INV063.proveedor like '%{$_proveedor}%')";
        }
        if ($_desde != '' && $_hasta != '') {
            $_desde = $this->_FECHA($_desde);
            $_hasta = $this->_FECHA($_hasta);
            $extra .= " AND (INV063.ingresolcc BETWEEN '{$_desde}' AND '{$_hasta} 23:59:59')";
        }
        return $this->_QUERY("SELECT id, tipo, codigo, descripcion, orden, factura, proveedor, costo, CONVERT(VARCHAR, ingresobi, 105) AS ingresobi, CONVERT(VARCHAR, ingresolcc, 105) AS ingresolcc, salida, cantidad, unidad, obs FROM INV063 WHERE 1=1 {$extra};");
    }

    function IngresoSalidaIME($_id, $_accion, $_tipo, $_codigo, $_descripcion, $_orden, $_factura, $_proveedor, $_costo, $_ingresobi, $_ingresolcc, $_salida, $_cantidad, $_unidad, $_obs)
    {
        $_codigo = $this->Free($_codigo);
        $_descripcion = $this->Free($_descripcion);
        $_proveedor = $this->Free($_proveedor);
        $_obs = $this->Free($_obs);
        $_ingresobi = $this->_FECHA($_ingresobi);
        $_ingresolcc = $this->_FECHA($_ingresolcc);
        if ($_accion == 'I') {
            $Q = $this->_QUERY("SELECT MAX(id)+1 AS cs FROM INV063");
            $this->_TRANS("INSERT INTO INV063 VALUES('{$Q[0]['cs']}', '{$_tipo}', '{$_codigo}', '{$_descripcion}', '{$_orden}', '{$_factura}', '{$_proveedor}', '{$_costo}', '{$_ingresobi}', '{$_ingresolcc}','{$_salida}', '{$_cantidad}', '{$_unidad}', '{$_obs}');");
            //
        } elseif ($_accion == 'M') {
            $this->_TRANS("UPDATE INV063 SET tipo = '{$_tipo}', codigo = '{$_codigo}', descripcion = '{$_descripcion}', orden = '{$_orden}', factura = '{$_factura}', proveedor = '{$_proveedor}', costo = '{$_costo}', ingresobi = '{$_ingresobi}', ingresolcc = '{$_ingresolcc}', salida = '{$_salida}', cantidad = '{$_cantidad}', unidad = '{$_unidad}', obs = '{$_obs}' WHERE id = '{$_id}';");
        } else {
            $this->_TRANS("DELETE FROM INV063 WHERE id = '{$_id}';");
        }
        if ($_accion == 'I') {
            $_accion = 'A003';
        } elseif ($_accion == 'M')
            $_accion = 'A004';
        else {
            $_accion = 'A005';
        }
        $this->Logger($_accion);
        //
        return 1;
    }

    function IngresoSalidaVacio()
    {
        return array(0 => array(
            'id' => '0',
            'tipo' => '',
            'codigo' => '',
            'descripcion' => '',
            'orden' => '',
            'factura' => '',
            'proveedor' => '',
            'costo' => '',
            'ingresobi' => '',
            'ingresolcc' => '',
            'salida' => '',
            'cantidad' => '',
            'unidad' => '',
            'obs' => ''
        ));
    }

    function MaterialVencido($_id = '', $_codigo = '', $_material = '', $_costo = '', $_vencimiento = '')
    {
        $extra = '';
        if ($_id != '') {
            $extra .= " AND id = '{$_id}'";
        }
        if ($_codigo != '') {
            $extra .= " AND (codigo like '%{$_codigo}%')";
        }
        if ($_material != '') {
            $extra .= " AND (material like '%{$_material}%')";
        }
        if ($_costo != '') {
            $extra .= " AND (costo like '%{$_costo}%')";
        }
        if ($_vencimiento != '') {
            $extra .= " AND (vencimiento like '%{$_vencimiento}%')";
        }
        return $this->_QUERY("SELECT id, codigo, material, costo, vencimiento, cantidad, unidad, total, obs FROM INV064 WHERE 1=1 {$extra};");
    }

    function MaterialVencidoIME($_id, $_accion, $_codigo, $_material, $_costo, $_vencimiento, $_cantidad, $_unidad, $_total, $_obs)
    {
        $_codigo = $this->Free($_codigo);
        $_material = $this->Free($_material);
        $_obs = $this->Free($_obs);
        if ($_accion == 'I') {
            $Q = $this->_QUERY("SELECT MAX(id)+1 AS cs FROM INV064");
            $this->_TRANS("INSERT INTO INV064 VALUES('{$Q[0]['cs']}', '{$_codigo}', '{$_material}', '{$_costo}', '{$_vencimiento}', '{$_cantidad}', '{$_unidad}', '{$_total}', '{$_obs}');");
            //
        } elseif ($_accion == 'M') {
            $this->_TRANS("UPDATE INV064 SET codigo = '{$_codigo}', material = '{$_material}', costo = '{$_costo}', vencimiento = '{$_vencimiento}', cantidad = '{$_cantidad}', unidad = '{$_unidad}', total = '{$_total}', obs = '{$_obs}' WHERE id = '{$_id}';");
        } else {
            $this->_TRANS("DELETE FROM INV064 WHERE id = '{$_id}';");
        }
        if ($_accion == 'I') {
            $_accion = 'A003';
        } elseif ($_accion == 'M')
            $_accion = 'A004';
        else {
            $_accion = 'A005';
        }
        $this->Logger($_accion);
        //
        return 1;
    }

    function MaterialVencidoVacio()
    {
        return array(0 => array(
            'id' => '0',
            'codigo' => '',
            'material' => '',
            'costo' => '',
            'vencimiento' => '',
            'cantidad' => '',
            'unidad' => '',
            'total' => '',
            'obs' => ''
        ));
    }

    function InventarioAlertaGet($_lab)
    {
        return $this->_QUERY("EXECUTE PA_MAG_056 '{$_lab}';");
    }

    function InventarioBuscaTipoDesc($_tipo, $_lab)
    {
        //BUSCA INVENTARIO POR EL NOMBRE DE LA CLASIFICACION, EJE: Columnas o Estandar
        return $this->_QUERY("SELECT INV002.id, INV002.codigo, INV002.nombre, INV002.stock, INV005.nombre AS estado
		FROM INV000 INNER JOIN INV002 ON INV000.id = INV002.familia INNER JOIN INV005 ON INV002.estado = INV005.id
		WHERE (INV000.nombre = '{$_tipo}') AND (INV000.lab = '{$_lab}')");
    }

    function InventarioConsecutivo()
    {
        $this->_TRANS("UPDATE MAG000 SET inventario=inventario+1");
        $ROW = $this->_QUERY("SELECT inventario FROM MAG000;");
        return $ROW[0]['inventario'];
    }

    function InventarioDetalleGet($_id, $_lab = '')
    {
        if ($_lab != '') {
            return $this->_QUERY("SELECT INV002.id, INV002.familia, INV002.codigo, INV002.orden, INV002.factura, INV002.proveedor, INV002.acta, INV002.nombre, INV002.marca, INV002.marcacomercial, INV002.presentacion, INV002.es, INV002.minimo1, INV002.minimo2, INV002.stock1, INV002.stock2, INV002.ubicacion1, INV002.ubicacion2 FROM INV002 INNER JOIN INV000 ON INV002.familia = INV000.id	WHERE (INV000.lab = {$_lab});");
        } else {
            return $this->_QUERY("SELECT id, familia, codigo, orden, factura, proveedor, acta, nombre, marca,marcacomercial, presentacion, es, minimo1, minimo2, stock1, stock2, ubicacion1, ubicacion2
		FROM INV002
		WHERE (id = {$_id});");
        }
    }

    function InventarioDisponiblesGet($_producto)
    {
        return $this->_QUERY("SELECT id, CONVERT(VARCHAR, fecha, 105) AS fecha1, codigo, bodega1, bodega2
		FROM INV003
		WHERE (producto = {$_producto}) AND (tipo = 'E') AND (bodega1 > 0 OR bodega2 > 0)
		ORDER BY fecha;");
    }

    function InventarioEncabezado($_id)
    {
        return $this->_QUERY("SELECT INV002.codigo, INV002.nombre, INV002.stock1, INV002.stock2, INV001.nombre AS unids, INV002.marca, INV002.marcacomercial
		FROM INV002 INNER JOIN INV001 ON INV002.presentacion = INV001.id
		WHERE (INV002.id = {$_id});");
    }

    /* function InventarioESGet($_producto, $_tipo){
      return $this->_QUERY("EXECUTE PA_MAG_053 '{$_producto}', '{$_tipo}';");
      } */

    function InventarioEntradaGet($_id)
    {
        $ROW = $this->_QUERY("SELECT INV002.nombre AS producto, INV003.codigo, (SEG001.nombre + ' ' + SEG001.ap1) AS usuario, INV007.lote, INV007.oc_inicio, INV007.tramite, INV007.oc, INV007.requisicion, INV007.factura, INV007.stock1, INV007.stock2, INV007.costo, INV007.vence, INV007.certificado, INV007.pureza, MAN003.nombre AS proveedor
		FROM INV007 INNER JOIN INV003 ON INV007.ES = INV003.id INNER JOIN INV002 ON INV003.producto = INV002.id INNER JOIN SEG001 ON INV003.usuario = SEG001.id INNER JOIN MAN003 ON INV007.proveedor = MAN003.cs
		WHERE (INV003.id = {$_id});");

        if ($ROW) {
            $ROW2 = $this->_QUERY("SELECT CONVERT(VARCHAR, vencimiento, 105) AS vencimiento, CONVERT(VARCHAR, aviso, 105) AS aviso FROM INV004 WHERE (entrada={$_id});");
            if ($ROW2) {
                $ROW[0]['vencimiento'] = $ROW2[0]['vencimiento'];
                $ROW[0]['aviso'] = $ROW2[0]['aviso'];
            } else {
                $ROW[0]['vencimiento'] = '';
                $ROW[0]['aviso'] = '';
            }
        }

        return $ROW;
    }

    function InventarioEntradaSet($_cs, $_producto, $_codigo, $_lote, $_oc_inicio, $_tramite, $_oc, $_req, $_factura, $_proveedor, $_stock1, $_stock2, $_costo, $_vence, $_vencimiento, $_aviso, $_certificado, $_pureza, $_UID)
    {
        $_codigo = $this->Free($_codigo);
        $_lote = $this->Free($_lote);
        $_costo = str_replace(',', '', $_costo);
        $_oc_inicio = $this->Free($_oc_inicio);
        $_tramite = $this->Free($_tramite);
        $_req = $this->Free($_req);
        $_oc = $this->Free($_oc);
        $_factura = $this->Free($_factura);
        $_stock1 = str_replace(',', '', $_stock1);
        $_stock2 = str_replace(',', '', $_stock2);
        $_costo = str_replace(',', '', $_costo);

        if ($this->InventarioMovimientoSet($_cs, $_producto, 'E', $_codigo, $_stock1, $_stock2, $_UID)) {
            $this->_TRANS("EXECUTE PA_MAG_051 '{$_cs}', '{$_producto}', '{$_lote}', '{$_oc_inicio}', '{$_tramite}', '{$_oc}', '{$_req}', '{$_factura}', '{$_proveedor}', '{$_stock1}', '{$_stock2}', '{$_costo}', '{$_vence}', '{$_vencimiento}', '{$_aviso}', '{$_certificado}', '{$_pureza}';");
            $this->Logger('A002');
            return 1;
        } else
            return 0;
    }

    function InventarioESValidaCodigo($_lab, $_codigo, $_valida = true)
    {
        $ROW = $this->_QUERY("SELECT id FROM INV003 WHERE (codigo = '{$_codigo}') AND (tipo = 'E');");
        if (!$ROW)
            return false;
        if ($_valida)
            return true;
        else
            return $ROW[0]['id'];
    }

    function InventarioHistorialDetalle($_id)
    {
        $ROW = $this->_QUERY("SELECT INV003.codigo, INV003.obs, INV007.vence, INV007.certificado, INV007.oc_inicio, INV007.tramite, INV007.requisicion, INV007.oc, INV007.factura, INV007.pureza, INV007.ubicacion, MAN003.nombre AS proveedor
		FROM INV007 INNER JOIN INV003 ON INV007.ES = INV003.id INNER JOIN MAN003 ON INV007.proveedor = MAN003.cs
		WHERE (INV003.id = {$_id});");
        if (isset($ROW) && $ROW[0]['vence'] == '1') {
            $ROW2 = $this->_QUERY("SELECT CONVERT(VARCHAR, vencimiento, 105) AS vencimiento, CONVERT(VARCHAR, aviso, 105) AS aviso FROM INV004 WHERE (entrada = {$_id})");
            $ROW[0]['vencimiento'] = $ROW2[0]['vencimiento'];
            $ROW[0]['aviso'] = $ROW2[0]['aviso'];
        } else {
            $ROW[0]['vencimiento'] = $ROW[0]['aviso'] = '';
        }
        return $ROW;
    }

    function inventarioAdicionalIME($_accion, $_idcat, $_idinv, $_valor)
    {
        if ($_accion == 'I') {
            return $this->_TRANS("INSERT INTO INVA VALUES('{$_idcat}','{$_idinv}','{$_valor}');");
        } else if ($_accion == 'E') {
            return $this->_TRANS("DELETE FROM INVA WHERE idinv='{$_idinv}'");
        }
    }

    function InventarioIME($_id, $_tipo, $_codigo, $_orden, $_factura, $_proveedor, $_acta, $_nombre, $_marca, $_marcacomercial, $_presentacion, $_es, $_minimo1, $_minimo2, $_stock1, $_stock2, $_ubicacion1, $_ubicacion2, $_adicional, $_accion, $_valorinv, $_idcat)
    {
        $_codigo = str_replace(' ', '', $_codigo);
        $_codigo = $this->Free($_codigo);
        $_nombre = $this->Free($_nombre);
        $_marca = $this->Free($_marca);
        $_ubicacion1 = $this->Free($_ubicacion1);
        $_ubicacion2 = $this->Free($_ubicacion2);

        $this->InventarioOpcionalesDel($_id);
        if ($this->_TRANS("EXECUTE PA_MAG_021 '{$_id}', '{$_tipo}', '{$_codigo}', '{$_orden}', '{$_factura}', '{$_proveedor}', '{$_acta}', '{$_nombre}', '{$_marca}','{$_marcacomercial}', '{$_presentacion}', '{$_es}', '{$_minimo1}', '{$_minimo2}', '{$_stock1}', '{$_stock2}', '{$_ubicacion1}', '{$_ubicacion2}', '{$_accion}';")) {
            if ($_accion != 'D') {
                $this->inventarioAdicionalIME('E', '', $_id, '');
                for ($i = 0; $i < count($_idcat); $i++) {
                    $this->inventarioAdicionalIME('I', $_idcat[$i], $_id, $_valorinv[$i]);
                }
                // $this->InventarioOpcionalesIME($_id, $_adicional, $_accion, $_ids_052);
            }
            //
            if ($_accion == 'I') {
                $_accion = 'A003';
            } elseif ($_accion == 'M')
                $_accion = 'A004';
            else {
                $_accion = 'A005';
            }
            $this->Logger($_accion);
            //
            return 1;
        } else {
            return 0;
        }
    }

    function inventarioAdicionalcatalogoConsecutivo($_nombre = '')
    {
        if ($_nombre == '') {
            $this->_TRANS("UPDATE MAG000 SET INVACAT = INVACAT + 1;");
            $b = $this->_QUERY("SELECT INVACAT FROM MAG000;");
            return $b[0]['INVACAT'];
        } else {
            $b = $this->_QUERY("SELECT id FROM INVACAT WHERE nombre = '{$_nombre}';");
            if ($b) {
                return -2;
            } else {
                $this->_TRANS("UPDATE MAG000 SET INVACAT = INVACAT + 1;");
                $b = $this->_QUERY("SELECT INVACAT FROM MAG000;");
                return $b[0]['INVACAT'];
            }
        }
    }

    function inventarioAdicionalcatalogoIME($_id, $_nombre, $_tipo)
    {
        return $this->_TRANS("INSERT INTO INVACAT VALUES('{$_id}','$_nombre','{$_tipo}')");
    }

    function InventarioEncuentraxCatalogo($_id, $_familia)
    {
        return $this->_QUERY("SELECT id FROM INV002 WHERE codigo = '{$_id}' AND familia = '{$_familia}' AND estado <> 0");
    }

    function InventarioActualizaEstado($_id)
    {
        return $this->_TRANS("UPDATE INV002 SET estado='1' WHERE id='{$_id}'");
    }

    function InventarioMovimientoCs()
    {
        $ROW = $this->_QUERY("SELECT inventario_ES FROM MAG000;");
        return $ROW[0]['inventario_ES'];
    }

    function InventarioMovimientoGet($_producto, $_tipo, $_desde, $_hasta)
    {
        $_desde = $this->_FECHA2($_desde);
        $_hasta = $this->_FECHA2($_hasta);
        return $this->_QUERY("EXECUTE PA_MAG_048 '{$_producto}', '{$_tipo}', '{$_desde}', '{$_hasta} 23:59:00';");
    }

    private function InventarioMovimientoSet($_id, $_producto, $_tipo, $_codigo, $_stock1, $_stock2, $_usuario)
    {
        return $this->_TRANS("EXECUTE PA_MAG_132 '{$_id}', '{$_producto}', '{$_tipo}', '{$_codigo}', '{$_stock1}', '{$_stock2}', '{$_usuario}';");
    }

    function InventarioMovimientoTipo($_tipo)
    {
        if ($_tipo == 'E')
            return 'Entrada';
        elseif ($_tipo == 'T')
            return 'Traslado';
        elseif ($_tipo == '0')
            return 'Consumo';
        elseif ($_tipo == '1')
            return 'Da�o';
        elseif ($_tipo == '2')
            return 'Donaci�n';
        elseif ($_tipo == '3')
            return 'Mantenimiento';
        elseif ($_tipo == '4')
            return 'Obsolescencia';
        elseif ($_tipo == '5')
            return 'Pr�stamo';
    }

    private function InventarioOpcionalesIME($_producto, $_adicional, $_accion, $_ids_052 = '')
    {
        if ($_accion == 'I' or $_accion == 'M') {
            $_adicional = substr($_adicional, 1);
            $ROW = explode(';', $_adicional);

            // j tiene el index de ids_052
            for ($x = 0, $j = 0; $x < count($ROW); $x++) {
                if ($ROW[$x] != '') {
                    list($tabla, $valor) = explode(':', $ROW[$x]);
                    $valor = $this->Free($valor);

                    if ($tabla == '052' && !empty($_ids_052)) {
                        $this->_TRANS("DELETE FROM INV{$tabla} WHERE producto={$_producto} AND id='{$_ids_052[$j]}';");
                        $this->_TRANS("INSERT INTO INV{$tabla} VALUES({$_producto}, '{$valor}','{$_ids_052[$j++]}');");
                    } else {
                        $this->_TRANS("DELETE FROM INV{$tabla} WHERE (producto={$_producto});");
                        if ($tabla == '052') {
                            $this->_TRANS("INSERT INTO INV052(producto,contenido) VALUES({$_producto}, '{$valor}')");
                        } else {
                            $this->_TRANS("INSERT INTO INV{$tabla} VALUES({$_producto}, '{$valor}');");
                        }
                    }
                }
            }
        }
    }

    private function InventarioOpcionalesTemporal()
    {
        $letras = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $tam = strlen($letras);
        $str = '';
        for ($i = 0; $i < 10; $i++)
            $str .= $letras[rand(0, $tam - 1)];
        return $str;
    }

    function InventarioOpcionalesGet($_id)
    {
        $cs = $this->InventarioOpcionalesTemporal();
        //GENERA TABLA TEMPORAL
        $this->_TRANS("EXECUTE PA_MAG_131 '{$cs}', '{$_id}';");
        //CREA MARCO TABLA
        $ROW = $this->_QUERY("SELECT nombre, tamano, tabla, CONVERT(TEXT, valor) AS valor FROM INVTMP WHERE (temporal='{$cs}');");
        $str = '<table width="100%">';
        for ($x = 0; $x < count($ROW); $x++) {

            $str .= "<tr><td>{$ROW[$x]['nombre']}</td><td>";
            if ($ROW[$x]['tabla'] == '052') {
                $valor = $this->_QUERY("SELECT id FROM INVPLA WHERE nombre='{$ROW[$x]['nombre']}' ");
                if ($valor) {
                    $str .= "<input type='hidden' name='idcampo[]' value='" . $valor[0]['id'] . "' />";
                }
            }
            if ($ROW[$x]['tamano'] > 99) {
                $str .= "<textarea ondblclick='Blank(this)' name='adicional' id='{$ROW[$x]['tabla']}'>{$ROW[$x]['valor']}</textarea></td></tr>";
            } else {

                $str .= "<input ondblclick='Blank(this)' type='text' name='adicional' id='{$ROW[$x]['tabla']}' size='{$ROW[$x]['tamano']}' maxlength='{$ROW[$x]['tamano']}' value='{$ROW[$x]['valor']}'/></td></tr>";
            }
        }
        $str .= '</table>';
        echo $str;
        //LIMPIA TABLA TEMPORAL
        $this->_TRANS("DELETE FROM INVTMP WHERE (temporal='{$cs}');");
    }

    private function InventarioOpcionalesDel($_id)
    {
        return $this->_TRANS("EXECUTE PA_MAG_050 '{$_id}';");
    }

    function InventarioResumenGet($_id, $_nombre, $_familia, $_lab, $ordenado = false, $_parte = '', $_estado = '1')
    {
        if ($_familia == -99) {
            return $this->_QUERY("SELECT INV002.id, INV002.codigo, INV002.nombre, INV002.marca, INV002.es, INV002.minimo1, INV002.minimo2, INV002.stock1, INV002.stock2, INV002.estado, INV001.nombre AS unidades
			FROM INV002 INNER JOIN INV001 ON INV002.presentacion = INV001.id INNER JOIN INV000 ON INV002.familia = INV000.id
			WHERE (INV000.lab = '{$_lab}') AND (INV002.estado = '{$_estado}');");
        } elseif ($_parte == '') {
            if ($_id != '')
                $extra = "AND INV002.codigo = '{$_id}'";
            elseif ($_nombre != '')
                $extra = "AND (INV002.nombre LIKE '%{$_nombre}%')";
            elseif ($_familia != '')
                $extra = "AND (INV002.familia = {$_familia})";
            else
                $extra = '';

            if ($ordenado)
                $extra2 = 'ORDER BY nombre';
            else
                $extra2 = '';
            $str = "SELECT INV002.id, INV002.codigo, INV002.nombre, INV002.marca, INV002.es, INV002.minimo1, INV002.minimo2, INV002.stock1, INV002.stock2, INV002.estado, INV001.nombre AS unidades
			FROM INV002 INNER JOIN INV001 ON INV002.presentacion = INV001.id INNER JOIN INV000 ON INV002.familia = INV000.id
			WHERE (INV000.lab = '{$_lab}') AND (INV002.estado = '{$_estado}') {$extra} {$extra2};";
            return $this->_QUERY($str);
        } else {
            if ($ordenado)
                $extra2 = 'ORDER BY nombre';
            else
                $extra2 = '';

            $TEMP = $this->_QUERY("SELECT producto FROM INV006 WHERE parte like '%{$_parte}%';");
            if (!$TEMP) {
                $TEMP = $this->_QUERY("SELECT idinv AS producto FROM INVA WHERE valor like '%{$_parte}%';");
            }
            return $this->_QUERY("SELECT INV002.id, INV002.codigo, INV002.nombre, INV002.marca, INV002.es, INV002.minimo1, INV002.minimo2, INV002.stock1, INV002.stock2, INV002.estado, INV001.nombre AS unidades FROM INV002 INNER JOIN INV001 ON INV002.presentacion = INV001.id INNER JOIN INV000 ON INV002.familia = INV000.id WHERE INV002.estado = '{$_estado}' AND INV002.id = '{$TEMP[0]['producto']}' {$extra2};");
        }
    }

    function inventarioAdicionalCanpos($_familia)
    {
        return $this->_QUERY("SELECT INVACAT.id,INVACAT.nombre FROM INVOPC INNER JOIN INVACAT ON INVACAT.id=INVOPC.opcional WHERE INVOPC.familia={$_familia};");
    }

    function inventarioAdicionalGet($_idinv, $_idfamilia = '')
    {
        if (empty($_idfamilia)) {
            return $this->_QUERY("SELECT INVA.idcat,INVA.idinv,INVA.valor,INVACAT.nombre,INVACAT.tipo FROM INVA INNER JOIN INVACAT ON INVA.idcat=INVACAT.id WHERE INVA.idinv='{$_idinv}';");
        } else {
            return $this->_QUERY("SELECT INVA.idcat,INVA.valor,T1.nombre FROM (SELECT INVACAT.id,INVACAT.nombre FROM INVOPC INNER JOIN INVACAT ON INVACAT.id=INVOPC.opcional WHERE INVOPC.familia='{$_idfamilia}') T1 LEFT JOIN INVA ON INVA.idcat=T1.id AND INVA.idinv='{$_idinv}' ORDER BY T1.nombre;");
        }
    }

    function InventarioSalidaSet($_id, $_tipo, $_tipo_tras, $_cant_tras, $_stock1, $_stock2, $_UID, $_producto = '')
    {
        $cs = $this->InventarioMovimientoCs();

        if ($_id == '') {
            $ROW[0]['producto'] = $_producto;
            $ROW[0]['codigo'] = date('Y') . '-' . $cs;
        } else {
            //OBTIENE DATOS DE LA ENTRADA
            $ROW = $this->_QUERY("SELECT producto, codigo FROM INV003 WHERE (id = {$_id});");
            if (!$ROW)
                return 0;
            //
        }
        if ($this->InventarioMovimientoSet($cs, $ROW[0]['producto'], $_tipo, $ROW[0]['codigo'], $_stock1, $_stock2, $_UID)) {
            $this->_TRANS("EXECUTE PA_MAG_133 '{$cs}', '{$_id}', '{$ROW[0]['producto']}', '{$_tipo}', '{$_tipo_tras}', '{$_cant_tras}', '{$_stock1}', '{$_stock2}';");
            $this->Logger('A001');
            return 1;
        } else
            return 0;
    }

    function InventarioVacio()
    {
        return array(0 => array(
            'id' => '0',
            'familia' => '',
            'codigo' => '',
            'orden' => '',
            'factura' => '',
            'proveedor' => '',
            'nombre' => '',
            'acta' => '',
            'presentacion' => '',
            'marca' => '',
            'marcacomercial' => '',
            'es' => '',
            'minimo1' => '',
            'minimo2' => '',
            'stock1' => '',
            'stock2' => '',
            'ubicacion1' => '',
            'ubicacion2' => ''
        ));
    }

    function InventarioVencimientoAtender($_es)
    {
        if ($this->_TRANS("UPDATE INV004 SET aviso = NULL WHERE (entrada = {$_es});")) {
            return 1;
        } else
            return 0;
    }

    function OpcionalesAgrega($_nombre)
    {
        $ROW = $this->_QUERY("SELECT MAX(id)+1 AS id FROM INVPLA;");
        if ($this->_TRANS("INSERT INTO INVPLA VALUES ('{$ROW[0]['id']}', '{$_nombre}', '75', 'INV052', '1');")) {
            return 1;
        } else
            return 0;
    }

    function OpcionalesGetSiguiente()
    {
        $ROW = $this->_QUERY("SELECT COUNT(1) AS cuenta FROM INVPLA;");
        return $ROW[0]['cuenta'];
    }

    function obtieneIdGrupo($_matriz = '', $nombre = '')
    {
        if ($nombre != '') {
            $Q = $this->_QUERY("SELECT grupo FROM VAR001 WHERE nombre = '{$nombre}';");
        } else {
            $Q = $this->_QUERY("SELECT grupo FROM VAR001 WHERE id = '{$_matriz}';");
        }

        return $Q[0]['grupo'];
    }

    function MAAsignados($_matriz)
    {
           return $this->_QUERY("SELECT ida, analito, idm, matriz FROM INV065 WHERE idm = '{$_matriz}' ORDER BY analito;");
    }

    function MAAsignados1($_matriz)
    {
        return $this->_QUERY("SELECT ida, analito, idm, matriz, cuantificador, tipoCroma FROM INV065 WHERE idm = '{$_matriz}' ORDER BY analito;");
    }

    function MAAsignados2($_matriz)
    {
      return $this->_QUERY("select ida, var005.nombre as analito, matriz, cuantificador, var005.tipoCroma from inv065 inner join VAR005 on VAR005.id = INV065.ida  where idm = '{$_matriz}'  ORDER BY analito;");
    }


    function MAIME($_idm, $_matriz, $_ida, $_analito, $_cuantificador, $_accion)
    {
        if ($_accion == 'I') {
            //SI YA EXISTE DA ERROR
            $ROW = $this->_QUERY("SELECT 1 FROM INV065 WHERE (ida = {$_ida}) AND (idm = {$_idm});");
            $tip = $this->_QUERY("SELECT tipoCroma FROM var005 WHERE (id = {$_ida})");
            for ($x = 0; $x < count($tip); $x++) {
                $tipoCroma = $tip[$x]['tipoCroma'];
            }
            if ($ROW) {
                return 0;
            } else {
                return $this->_TRANS("INSERT INTO INV065 VALUES('{$_ida}', '{$_analito}', '{$_idm}', '{$_matriz}', '{$_cuantificador}', '$tipoCroma');");
            }
        }
    }


    function MAIMEDEL($_ida, $_matriz)
    {
        return $this->_TRANS("DELETE FROM INV065 WHERE ida = '{$_ida}' AND idm = '{$_matriz}';");
    }


    function OpcionalesAsignados($_familia)
    {
        return $this->_QUERY("SELECT INVACAT.id, INVACAT.nombre FROM INVOPC INNER JOIN INVACAT ON INVOPC.opcional = INVACAT.id WHERE (INVOPC.familia = '{$_familia}') ORDER BY INVACAT.nombre;");
    }

    function OpcionalesIME($_familia, $_opcional, $_accion)
    {
        if ($_accion == 'I') {
            //SI YA EXISTE DA ERROR
            $ROW = $this->_QUERY("SELECT 1 FROM INVOPC WHERE (familia = {$_familia}) AND (opcional = {$_opcional});");
            if ($ROW) {
                return 0;
            }
        }

        return $this->_TRANS("EXECUTE PA_MAG_130 '{$_familia}', '{$_opcional}', '{$_accion}';");
    }

    function OpcionalesMuestra()
    {
        return $this->_QUERY("SELECT id, nombre FROM INVACAT ORDER BY nombre;");
    }

    function OpcionalesPlantilla($_familia)
    {
        //REGRESA LOS CAMPOS OPCIONALES EN FORMA DE TABLA
        $ROW = $this->OpcionalesAsignados($_familia);
        $str = '<table width="100%">';
        for ($x = 0; $x < count($ROW); $x++) {
            //$ROW[$x]['nombre'] = utf8_encode($ROW[$x]['nombre']);
            $ROW[$x]['tabla'] = substr($ROW[$x]['tabla'], 3);
            $str .= "<tr><td>{$ROW[$x]['nombre']}</td><td>";
            if ($ROW[$x]['tamano'] > 99)
                $str .= "<textarea ondblclick='Blank(this)' name='adicional' id='{$ROW[$x]['tabla']}'></textarea></td></tr>";
            else
                $str .= "<input ondblclick='Blank(this)' type='text' name='adicional' id='{$ROW[$x]['tabla']}' size='{$ROW[$x]['tamano']}' maxlength='{$ROW[$x]['tamano']}' /></td></tr>";
        }
        $str .= '</table>';
        return $str;
    }

    function PresentacionesIME($_cs, $_nombre, $_accion)
    {
        $_nombre = $this->Free($_nombre);

        if ($this->_TRANS("EXECUTE PA_MAG_046 '{$_cs}', '{$_nombre}', '{$_accion}';")) {
            //
            if ($_accion == 'I')
                $_accion = 'A006';
            elseif ($_accion == 'M')
                $_accion = 'A007';
            else
                $_accion = 'A008';
            $this->Logger($_accion);
            //
            return 1;
        } else
            return 0;
    }

    function PresentacionesMuestra()
    {
        return $this->_QUERY("SELECT id AS cs, nombre FROM INV001 ORDER BY nombre;");
    }

    function TiposIME($_cs, $_nombre, $_accion, $_lab)
    {
        $_nombre = $this->Free($_nombre);

        if ($this->_TRANS("EXECUTE PA_MAG_045 '{$_cs}', '{$_nombre}', '{$_lab}', '{$_accion}';")) {
            //
            if ($_accion == 'I')
                $_accion = 'A009';
            elseif ($_accion == 'M')
                $_accion = 'A010';
            else
                $_accion = 'A011';
            $this->Logger($_accion);
            //
            return 1;
        } else
            return 0;
    }

    function TiposMuestra($_lab, $_rol)
    {
        return $this->_QUERY("SELECT id AS cs, nombre FROM INV000 WHERE (lab = '{$_lab}') ORDER BY nombre;");
        /* if($_rol == '0') //ADMINISTRADOR
          return $this->_QUERY("SELECT id AS cs, nombre FROM INV000 WHERE (lab = '{$_lab}') ORDER BY nombre;");
          else
          return $this->_QUERY("SELECT id AS cs, nombre FROM INV000 WHERE id IN(
          SELECT INV000.id
          FROM INV000 INNER JOIN PER016 ON INV000.ID = PER016.TIPO_INVENTARIO
          WHERE (PER016.PERFIL = {$_rol})
          ) AND (lab = '{$_lab}')
          ORDER BY nombre;"); */
    }

    function ObtienePatrimonio($_codigo)
    {
        return $this->_QUERY("SELECT valor FROM INVA WHERE idcat = 7 AND idinv = {$_codigo};");
    }
}

?>