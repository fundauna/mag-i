function DocumentosZip(_ARCHIVO, _MODULO){
	var ruta = "../seguridad/documentos.php?ARCHIVO=" + _ARCHIVO + "&MODULO=" + _MODULO + "&ACCESO=43";
	window.open(ruta,"","width=100,height=100,scrollbars=yes,status=no");
	//window.showModalDialog(ruta, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}

function datos(btn){
	if( Mal(3, $('#codigo').val()) ){
		OMEGA('Debe indicar el n�mero de formulario');
		return;
	}
	
	if( Mal(1, $('#fecha').val()) ){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	if($('#archivo').val()==''){
		OMEGA('Debe adjuntar un documento');
		return;
	}

	if(!confirm('Modificar datos?')) return;
	btn.disabled = true;
	ALFA('Por favor espere....');
	document.form.submit();
}

function DocumentosElimina(_CS){
	if(!confirm('Eliminar registro?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'cs' : _CS
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					OMEGA('Transaccion finalizada');
					location.reload();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}