function datos(){
		
	var parametros = {
		'_AJAX' : 1,
		'id' : $('#id').val(),
		'accion' : $('#accion').val(),
		'obs' : $('#obs').val(),
		'personal' : $('#personal').val(),
		'materiales' : $('#materiales').val(),
		'equipos' : $('#equipos').val(),
		'presupuesto' : $('#presupuesto').val(),
		'otros' : $('#otros').val(),
		'costo' : $('#costo').val(),
		'beneficio' : $('#beneficio').val(),
		'propuesta' : $('#propuesta').val(),
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesión expirada [Err:0]');break;
				case '-1':alert('Error en el envío de parámetros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					DELTA('Transaccion finalizada');
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function UsuariosLista(){
	window.open(__SHELL__ + "?list=1","","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function UsuarioEscoge(id, nombre){
	opener.document.getElementById('usuario').value=id;
	opener.document.getElementById('tmp').value=nombre;
	window.close();
}