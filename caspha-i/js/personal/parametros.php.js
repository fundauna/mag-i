function datos(){	
	
	if( Mal(2, $('#personal1').val()) ){
		OMEGA('Debe indicar una fecha v�lida');
		return;
	}
	
	if( !_HOY( document.getElementById('personal1') ) ){
		OMEGA('La fecha debe ser superior a hoy');
		return;
	}
	
	if( Mal(2, $('#personal2').val()) ){
		OMEGA('Debe indicar una fecha v�lida');
		return;
	}
	
	if( !_HOY( document.getElementById('personal2') ) ){
		OMEGA('La fecha debe ser superior a hoy');
		return;
	}
	
	if( Mal(2, $('#personal3').val()) ){
		OMEGA('Debe indicar una fecha v�lida');
		return;
	}
	
	if( !_HOY( document.getElementById('personal3') ) ){
		OMEGA('La fecha debe ser superior a hoy');
		return;
	}
	
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'personal1' : $('#personal1').val(),	
		'personal2' : $('#personal2').val(),
		'personal3' : $('#personal3').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					OMEGA("Transaccion finalizada");
					$('#btn').disabled = false;
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}