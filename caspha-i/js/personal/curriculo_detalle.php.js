/*******************************************************************
MISC
*******************************************************************/
var img1 = ''; //ruta de las imagenes de mostrar/ocultar
var img2 = '';

//PARA SABER QUE YA LA INFO SE CARG�
var paso2 = false;
var paso3 = false;
var paso4 = false;
var paso51 = false;
var paso52 = false;
var paso53 = false;
var paso54 = false;
var paso55 = false;
var paso6 = false;

jQuery(function($){
	$("#tel_hab").mask("9999-9999");
   	$("#tel_cel").mask("9999-9999");
});

function SecccionesBloquea(){
	//document.getElementById('ac').style.display = 'none';
	document.getElementById('btn1').style.display = 'none';
	document.getElementById('btn2').style.display = 'none';
	document.getElementById('btn3').style.display = 'none';
	document.getElementById('btn4').style.display = 'none';
	document.getElementById('btn51').style.display = 'none';
	document.getElementById('btn52').style.display = 'none';
	document.getElementById('btn53').style.display = 'none';
	document.getElementById('btn54').style.display = 'none';
	document.getElementById('btn55').style.display = 'none';
	document.getElementById('lolo1').style.display = 'none';
	document.getElementById('lolo2').style.display = 'none';
	document.getElementById('lolo3').style.display = 'none';
	document.getElementById('lolo4').style.display = 'none';
	document.getElementById('lolo5').style.display = 'none';
	document.getElementById('lolo6').style.display = 'none';
	document.getElementById('lolo7').style.display = 'none';
	document.getElementById('lolo8').style.display = 'none';
}

function DocumentosSubir(_USUARIO, _LINEA){
	window.open("docs.php?usuario="+ _USUARIO + "&linea=" + _LINEA,"","width=390,height=65,scrollbars=yes,status=no");
	//window.showModalDialog("docs.php?usuario="+ _USUARIO + "&linea=" + _LINEA, this.window, "dialogWidth:390px;dialogHeight:65px;status:no;");
}

function Buscar(num){
	var parametros = {
		'_VER' : num,
		'id' : $('#id').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response){
			BETA();
			document.getElementById('t'+num).innerHTML = _response;
			Mascara(num);
			return;
		}
	});
}

function Oculta(obj){
	if(document.getElementById('t'+obj).style.display == 'none'){
		document.getElementById('t'+obj).style.display = '';
		document.getElementById('f'+obj).style.display = '';
		document.getElementById('i_'+obj).src = img2;
	}else{
		document.getElementById('t'+obj).style.display = 'none';
		document.getElementById('f'+obj).style.display = 'none';
		document.getElementById('i_'+obj).src = img1;
	}
	
	if(obj==2 && !paso2) Buscar(obj);
	if(obj==3 && !paso3) Buscar(obj);
	if(obj==4 && !paso4) Buscar(obj);
	if(obj==51 && !paso51) Buscar(obj);
	if(obj==52 && !paso52) Buscar(obj);
	if(obj==53 && !paso53) Buscar(obj);
	if(obj==54 && !paso54) Buscar(obj);
	if(obj==55 && !paso55) Buscar(obj);
	if(obj==6 && !paso6) Buscar(obj);
}

//LE PONE MASCARA A LOS CAMPOS
function Mascara(tipo){
	if(tipo==2){
		paso2 = true;
		var control = document.getElementsByName("2ano");
		for(i=0;i<control.length;i++) $("#2ano" + i).mask("9999");
	}else if(tipo==3){
		paso3 = true;
	}else if(tipo==4){
		paso4 = true;
		var control = document.getElementsByName("4ano");
		for(i=0;i<control.length;i++) $("#4ano" + i).mask("9999");
	}else if(tipo==51){
		paso51 = true;
		var control = document.getElementsByName("51periodo");
		for(i=0;i<control.length;i++) $("#51periodo" + i).mask("9999-9999");
	}else if(tipo==52){
		paso52 = true;
		var control = document.getElementsByName("52periodo");
		for(i=0;i<control.length;i++) $("#52periodo" + i).mask("9999-9999");
	}else if(tipo==53){
		paso53 = true;
		var control = document.getElementsByName("53periodo");
		for(i=0;i<control.length;i++) $("#53periodo" + i).mask("9999-9999");
	}else if(tipo==54){
		paso54 = true;
		var control = document.getElementsByName("54periodo");
		for(i=0;i<control.length;i++) $("#54periodo" + i).mask("9999-9999");
	}else if(tipo==55){
		paso55 = true;
		var control = document.getElementsByName("55periodo");
		for(i=0;i<control.length;i++) $("#55periodo" + i).mask("9999-9999");
	}else if(tipo==6){
		paso6 = true;
	}
}

function Vector(ctrl){
	var str = "";
	var control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		str += ";" + control[i].value.replace(/&/g, ''); 
	}
	return str.substring(1);
}

function DocumentosZip(_ARCHIVO, _MODULO){
	var ruta = "../seguridad/documentos.php?ARCHIVO=" + _ARCHIVO + "&MODULO=" + _MODULO + "&ACCESO=00";
	window.open(ruta,"","width=100,height=100,scrollbars=yes,status=no");
	//window.showModalDialog(ruta, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}

/*******************************************************************
AGREGAR LINEAS
*******************************************************************/
function ExperienciaMas(){
	var linea = document.getElementsByName("51empresa").length;
	var fila = document.createElement("tr");
	var colum = new Array(2);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	
	colum[0].innerHTML = '<input type="text" id="51empresa' + linea + '" name="51empresa" size="50" maxlength="50">';
	colum[1].innerHTML = '<input type="text" id="51periodo' + linea + '" name="51periodo" size="9" maxlength="9" title="eje: 1990-2005">';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	
	document.getElementById('t51').appendChild(fila);
	$("#51periodo" + linea).mask("9999-9999");
}

function FormacionMas(){
	var linea = document.getElementsByName("2titulo").length;
	var fila = document.createElement("tr");
	var colum = new Array(3);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	
	colum[0].innerHTML = '<input type="text" id="2titulo' + linea + '" name="2titulo" size="50" maxlength="100">';
	colum[1].innerHTML = '<input type="text" id="2institucion' + linea + '" name="2institucion" size="40" maxlength="100">';
	colum[2].innerHTML = '<input type="text" id="2ano' + linea + '" name="2ano" maxlength="4" class="ano">';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]); 	
	
	document.getElementById('t2').appendChild(fila);
	$("#2ano" + linea).mask("9999");
}

function IdiomasMas(){
	var linea = document.getElementsByName("3idioma").length;
	var fila = document.createElement("tr");
	var colum = new Array(4);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	
	colum[0].innerHTML = '<select id="3idioma' + linea + '" name="3idioma"><option value="">N/A</option><option value="I">Ingl�s</option><option value="F">Franc�s</option><option value="A">Alem�n</option><option value="P">Portugu�s</option><option value="J">Japon�s</option><option value="S">Sueco</option><option value="C">Canton�s</option><option value="M">Mandar�n</option><option value="H">Holand�s</option><option value="R">Ruso</option></select>';
	colum[1].innerHTML = '<input id="3lee' + linea + '" name="3lee" size="3" maxlength="3" onblur="_INT(this)" />';
	colum[2].innerHTML = '<input id="3habla' + linea + '" name="3habla" size="3" maxlength="3" onblur="_INT(this)" />';
	colum[3].innerHTML = '<input id="3escribe' + linea + '" name="3escribe" size="3" maxlength="3" onblur="_INT(this)" />';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]); 	
	
	document.getElementById('t3').appendChild(fila);
}

function PublicacionesMas(){
	var linea = document.getElementsByName("4publicacion").length;
	var fila = document.createElement("tr");
	var colum = new Array(3);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	
	colum[0].innerHTML = '<input type="text" id="4publicacion' + linea + '" name="4publicacion" size="50" maxlength="100">';
	colum[1].innerHTML = '<input type="text" id="4entidad' + linea + '" name="4entidad" size="40" maxlength="50">';
	colum[2].innerHTML = '<input type="text" id="4ano' + linea + '" name="4ano" size="4" maxlength="4">';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	
	document.getElementById('t4').appendChild(fila);
	$("#4ano" + linea).mask("9999");
}

function AnalisisMas(){
	var linea = document.getElementsByName("52tecnica").length;
	var fila = document.createElement("tr");
	var colum = new Array(3);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	
	colum[0].innerHTML = '<input type="text" id="52tecnica' + linea + '" name="52tecnica" size="50" maxlength="100">';
	colum[1].innerHTML = '<input type="text" id="52empresa' + linea + '" name="52empresa" size="40" maxlength="50">';
	colum[2].innerHTML = '<input type="text" id="52periodo' + linea + '" name="52periodo" size="7" maxlength="9" title="eje: 1990-2005">';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	
	document.getElementById('t52').appendChild(fila);
	$("#52periodo" + linea).mask("9999-9999");
}

function EquiposMas(){
	var linea = document.getElementsByName("53equipos").length;
	var fila = document.createElement("tr");	
	var colum = new Array(3);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	
	colum[0].innerHTML = '<input type="text" id="53equipos' + linea + '" name="53equipos" size="50" maxlength="100">';
	colum[1].innerHTML = '<input type="text" id="53empresa' + linea + '" name="53empresa" size="40" maxlength="50">';
	colum[2].innerHTML = '<input type="text" id="53periodo' + linea + '" name="53periodo" size="7" maxlength="9" title="eje: 1990-2005">';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	
	document.getElementById('t53').appendChild(fila);
	$("#53periodo" + linea).mask("9999-9999");
}

function CalidadMas(){
	var linea = document.getElementsByName("54actividad").length;
	var fila = document.createElement("tr");
	var colum = new Array(3);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	
	colum[0].innerHTML = '<input type="text" id="54actividad' + linea + '" name="54actividad" size="50" maxlength="100">';
	colum[1].innerHTML = '<input type="text" id="54empresa' + linea + '" name="54empresa" size="40" maxlength="50">';
	colum[2].innerHTML = '<input type="text" id="54periodo' + linea + '" name="54periodo" size="7" maxlength="9" title="eje: 1990-2005">';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	
	document.getElementById('t54').appendChild(fila);
	$("#54periodo" + linea).mask("9999-9999");
}

function OtrosMas(){
	var linea = document.getElementsByName("55actividad").length;
	var fila = document.createElement("tr");
	var colum = new Array(3);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	
	colum[0].innerHTML = '<input type="text" id="55actividad' + linea + '" name="55actividad" size="50" maxlength="100">';
	colum[1].innerHTML = '<input type="text" id="55empresa' + linea + '" name="55empresa" size="40" maxlength="50">';
	colum[2].innerHTML = '<input type="text" id="55periodo' + linea + '" name="55periodo" size="7" maxlength="9" title="eje: 1990-2005">';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	
	document.getElementById('t55').appendChild(fila);
	$("#55periodo" + linea).mask("9999-9999");
}

function CursosMas(_usuario){
	var linea = document.getElementsByName("6tipo").length;
	var fila = document.createElement("tr");
	var colum = new Array(7);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	colum[4] = document.createElement("td");
	colum[5] = document.createElement("td");
	colum[6] = document.createElement("td");
	
	colum[0].innerHTML = '<img src="../../caspha-i/imagenes/bajar2.png" title="No disponible" class="tab2"/>';
	colum[1].innerHTML = '<img src="../../caspha-i/imagenes/subir.png" title="Subir" onclick="DocumentosSubir(' + _usuario + ',' + linea + ')" class="tab2"/>';
	colum[2].innerHTML = '<input type="text" id="6tipo' + linea + '" name="6tipo" size="20" maxlength="20">';
	colum[3].innerHTML = '<input type="text" id="6nombre' + linea + '" name="6nombre" size="50" maxlength="200">';
	colum[4].innerHTML = '<input type="text" id="6empresa' + linea + '" name="6empresa" size="30" maxlength="50">';
	colum[5].innerHTML = '<input type="text" id="6duracion' + linea + '" name="6duracion">';
	colum[6].innerHTML = '<input type="text" id="6fecha' + linea + '" name="6fecha" placeholder="MM/YYYY">';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]); 	
	
	document.getElementById('t6').appendChild(fila);
}

/*******************************************************************
VALIDACIONES
*******************************************************************/
function Paso1(){
	if( Mal(9, $('#cedula').val()) ){
		OMEGA('Debe indicar la c�dula');
		return;
	}
	
	if( Mal(2, $('#nombre').val()) ){
		OMEGA('Debe indicar el nombre');
		return;
	}
	
	if( Mal(4, $('#ap1').val()) ){
		OMEGA('Debe indicar el apellido 1');
		return;
	}
	
	if( Mal(4, $('#ap2').val()) ){
		OMEGA('Debe indicar el apellido 2');
		return;
	}	
	
	if( Mal(4, $('#email').val()) ){
		OMEGA('Debe indicar el email');
		return;
	}
	
	if( Mal(4, $('#fnacimiento').val()) ){
		OMEGA('Debe indicar la fecha de nacimiento');
		return;
	}
	
	if( Mal(1, $('#genero').val()) ){
		OMEGA('Debe indicar el g�nero');
		return;
	}
	
	if( Mal(3, $('#nacionalidad').val()) ){
		OMEGA('Debe indicar la nacionalidad');
		return;
	}
	
	if( Mal(4, $('#fingreso').val()) ){
		OMEGA('Debe indicar la fecha de ingreso al SFE');
		return;
	}
	
	if( Mal(1, $('#cargo').val()) ){
		OMEGA('Debe indicar el cargo');
		return;
	}

	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso': 1,
		'id' : $('#id').val(),
		'cedula' : $('#cedula').val(),
		'nombre' : $('#nombre').val(),
		'ap1' : $('#ap1').val(),
		'ap2' : $('#ap2').val(),
		'email' : $('#email').val(),
		'fnacimiento' : $('#fnacimiento').val(),
		'genero' : $('#genero').val(),
		'nacionalidad' : $('#nacionalidad').val(),
		'domicilio' : $('#domicilio').val(),
		'tel_hab' : $('#tel_hab').val(),
		'tel_cel' : $('#tel_cel').val(),
		'fingreso' : $('#fingreso').val(),
		'profesion' : $('#profesion').val(),
		'cargo' : $('#cargo').val(),
		'clase_puesto' : $('#clase_puesto').val(),
		'num_puesto' : $('#num_puesto').val(),
		'colegio' : $('#colegio').val(),
		'num_colegiado' : $('#num_colegiado').val(),
		'otros' : $('#otros').val(),
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn2').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn2').disabled = false;
					break;
				case '1':
					OMEGA('Transaccion finalizada');
					$('#btn2').disabled = false;
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Paso2(){
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso': 2,
		'id' : $('#id').val(),
		'titulo' : Vector('2titulo'),
		'institucion' : Vector('2institucion'),
		'ano' : Vector('2ano')
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn2').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn2').disabled = false;
					break;
				case '1':
					OMEGA('Transaccion finalizada');
					$('#btn2').disabled = false;
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Paso3(){
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso': 3,
		'id' : $('#id').val(),
		'idioma' : Vector('3idioma'),
		'lee' : Vector('3lee'),
		'habla' : Vector('3habla'),
		'escribe' : Vector('3escribe')
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn3').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn3').disabled = false;
					break;
				case '1':
					OMEGA('Transaccion finalizada');
					$('#btn3').disabled = false;
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Paso4(){
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso': 4,
		'id' : $('#id').val(),
		'publicacion' : Vector('4publicacion'),
		'entidad' : Vector('4entidad'),
		'ano' : Vector('4ano')
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn4').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn4').disabled = false;
					break;
				case '1':
					OMEGA('Transaccion finalizada');
					$('#btn4').disabled = false;
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Paso51(){
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso': 51,
		'id' : $('#id').val(),
		'empresa' : Vector('51empresa'),
		'periodo' : Vector('51periodo')
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn51').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn51').disabled = false;
					break;
				case '1':
					OMEGA('Transaccion finalizada');
					$('#btn51').disabled = false;
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Paso52(){
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso': 52,
		'id' : $('#id').val(),
		'tecnica' : Vector('52tecnica'),
		'empresa' : Vector('52empresa'),
		'periodo' : Vector('52periodo')
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn52').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn52').disabled = false;
					break;
				case '1':
					OMEGA('Transaccion finalizada');
					$('#btn52').disabled = false;
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Paso53(){
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso': 53,
		'id' : $('#id').val(),
		'equipos' : Vector('53equipos'),
		'empresa' : Vector('53empresa'),
		'periodo' : Vector('53periodo')
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn53').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '1':
					OMEGA('Transaccion finalizada');
					$('#btn53').disabled = false;
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}


function Paso54(){
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso': 54,
		'id' : $('#id').val(),
		'actividad' : Vector('54actividad'),
		'empresa' : Vector('54empresa'),
		'periodo' : Vector('54periodo')
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn54').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '1':
					OMEGA('Transaccion finalizada');
					$('#btn54').disabled = false;
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Paso55(){
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso': 55,
		'id' : $('#id').val(),
		'actividad' : Vector('55actividad'),
		'empresa' : Vector('55empresa'),
		'periodo' : Vector('55periodo')
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn55').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '1':
					OMEGA('Transaccion finalizada');
					$('#btn55').disabled = false;
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Paso6(){
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso': 6,
		'id' : $('#id').val(),
		'tipo' : Vector('6tipo'),
		'nombre' : Vector('6nombre'),
		'empresa' : Vector('6empresa'),
		'duracion' : Vector('6duracion'),
		'fecha' : Vector('6fecha')
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn6').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '1':
					OMEGA('Transaccion finalizada');
					$('#btn6').disabled = false;
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}