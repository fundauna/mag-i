function datos() {
    if (Mal(1, $('#usuario').val())) {
        OMEGA('Debe indicar el usuario');
        return;
    }

    if (Mal(2, $('#fecha').val())) {
        OMEGA('Debe indicar la fecha');
        return;
    }

    if (!confirm('Modificar datos?'))
        return;
    $('#btn').disabled = true;
    ALFA('Por favor espere....');
    document.form.submit();
}

function UsuariosLista() {
    window.open(__SHELL__ + "?list=1", "", "width=400,height=200,scrollbars=yes,status=no");
}

function UsuarioEscoge(id, nombre) {
    opener.document.getElementById('usuario').value = id;
    opener.document.getElementById('tmp').value = nombre;
    window.close();
}

function DocElimina(id) {
    if (!confirm('Desea eliminar el Adjunto!?'))
        return;
    var parametros = {
        '_AJAX': 1,
        'id': id
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    break;
                case '1':
                    OMEGA('Transaccion finalizada');
                    location.reload();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}