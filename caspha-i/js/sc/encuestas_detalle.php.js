function datos(accion){	
	if( Mal(1, $('#nombre').val()) ){
		OMEGA('Debe indicar el nombre de la encuesta');
		return;
	}	
	
	if( $('#estado').val()=='' ){
		OMEGA('Debe indicar el estado de la encuesta');
		return;
	}

	if(accion=='I'){
		if(!confirm('Ingresar datos?')) return;
		btn.disabled = true;
		ALFA('Por favor espere....');
		document.form.submit();
	}else{
		if(!confirm('Modificar datos?')) return;
		btn.disabled = true;
		ALFA('Por favor espere....');
		document.form.submit();
	}
	/*var parametros = {
		'_AJAX' : 1,
		'id' : $('#id').val(),
		'accion' : $('#accion').val(),		
		'nombre' : $('#nombre').val(),		
		'estado' : $('#estado').val()		
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					OMEGA('Transaccion finalizada');
					window.dialogArguments.location.reload();
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});*/
}

function DocumentosZip(_ARCHIVO, _MODULO){
	var ruta = "../seguridad/documentos.php?ARCHIVO=" + _ARCHIVO + "&MODULO=" + _MODULO + "&ACCESO=52";
	window.open(ruta,"","width=100,height=100,scrollbars=yes,status=no");
	//window.showModalDialog(ruta, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}

function EncuestaAnula(_CS,_VER,_ACC){
	if(!confirm('Eliminar documento?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'cs' : _CS,
		'version' : _VER,
		'accion' : _ACC,
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					alert('Transaccion finalizada');
					window.close();
					opener.location.reload();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}