$(document).ready(function () {
    oTable = $('#example').dataTable({
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        /*"sScrollX": "100%",*/
        "sScrollXInner": "110%",
        "bScrollCollapse": true,
        "bFilter": true,
        "oLanguage": {
            "sEmptyTable": "No hay datos que mostrar",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sLoadingRecords": "Cargando...",
            "sProcessing": "Procesando...",
            "sSearch": "Buscar:",
            "sZeroRecords": "No hay datos que mostrar",
            "sInfoFiltered": "(filtro de _MAX_ registros)",
            "sInfoEmpty": "Mostrando 0 registros",
            "oPaginate": {
                "sFirst": "Primer",
                "sLast": "Fin",
                "sNext": "Sig.",
                "sPrevious": "Prev."
            }
        }
    });
});

function QuejasBuscar(btn) {
    if (Mal(1, $('#desde').val())) {
        OMEGA('Debe indicar la fecha');
        return;
    }

    if (Mal(1, $('#hasta').val())) {
        OMEGA('Debe indicar la fecha');
        return;
    }

    btn.disabled = true;
    btn.form.submit();
}

function QuejasAgregar() {
    window.open("queja_detalle.php?acc=I", "", "width=700,height=480,scrollbars=yes,status=no");
    //window.showModalDialog("quejas_detalle.php?acc=I", this.window, "dialogWidth:800px;dialogHeight:450px;status:no;");
}

function QuejasModificar(_ID) {
    window.open("queja_detalle.php?acc=M&ID=" + _ID, "", "width=700,height=480,scrollbars=yes,status=no");
    //window.showModalDialog("quejas_detalle.php?acc=M&ID="+_ID, this.window, "dialogWidth:800px;dialogHeight:450px;status:no;");
}

function QuejasElimina(_CS) {
    if (!confirm('Cerrar queja?')) return;

    var parametros = {
        '_AJAX': 1,
        'cs': _CS
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    GAMA('Transaccion finalizada');
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}