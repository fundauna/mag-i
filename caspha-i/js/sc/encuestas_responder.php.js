function datos(){
	var control = document.getElementsByName('pregunta');
	var respuestas1 = '';
	var respuestas2 = '';
	
	for(i=0;i<control.length;i++){
		if(control[i].getAttribute('tipo') == '0'){ //solo si no es checkbox
			if( Mal(1, control[i].value) ){
				OMEGA('Debe responder la pregunta '+ (i+1));
				return;
			}
			respuestas1 += ';' + control[i].value;
		}else{
			//obtiene todas las opciones del check
			var control2 = document.getElementsByName('check'+ control[i].id);
			respuestas2 += ';' + control[i].id + ':';
			for(x=0;x<control2.length;x++){
				if(control2[x].checked){
					respuestas2 += ',' + control2[x].value;
				}
			}
		}
	}

	if(!confirm('Ingresar datos?')) return;

	var parametros = {
		'_AJAX' : 1,
		'tipos' : $('#tipos').val(),
		'preguntas1' : $('#preguntas1').val(),
		'respuestas1' : respuestas1,
		'respuestas2' : respuestas2
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					/*window.dialogArguments*/opener.location.reload();
					DELTA('Transaccion finalizada');
					//window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}