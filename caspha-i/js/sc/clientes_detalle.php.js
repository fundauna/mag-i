function ClaveReenvia() {
    if ($('#email').val() == '') {
        OMEGA('Debe indicar el email de notificaciones');
        return;
    }

    if (!confirm('Reenviar clave al cliente?'))
        return;

    var parametros = {
        '_REENVIAR': 1,
        'id': $('#id').val(),
        'email': $('#email').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            console.log(_response);
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    break;
                case '1':
                    alert('Transaccion finalizada');
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function datos() {
    if (Mal(3, $('#id').val())) {
        OMEGA('Debe indicar la identificaci�n');
        return;
    }

    if (Mal(2, $('#nombre').val())) {
        OMEGA('Debe indicar el nombre');
        return;
    }

    var control = document.getElementsByName('contacto');
    var control3 = document.getElementsByName('telefono');
    var control4 = document.getElementsByName('correo');
    var hay = 0;

    for (i = 0; i < control.length; i++) {
        if (!Mal(1, control[i].value)) {

            if (Mal(1, control3[i].value)) {
                OMEGA('Debe indicar el tel�fono del contacto');
                return;
            }

            if (Mal(1, control4[i].value)) {
                OMEGA('Debe indicar el correo electr�nico del contacto');
                return;
            }

            hay++;
        }
    }

    if (hay < 1) {
        OMEGA('No ha indicado ning�n contacto');
        return;
    }

    var control = document.getElementsByName('solicitante');
    var control3 = document.getElementsByName('soltel');
    var control4 = document.getElementsByName('solemail');
    var hay = 0;

    for (i = 0; i < control.length; i++) {
        if (!Mal(1, control[i].value)) {

            if (Mal(1, control3[i].value)) {
                OMEGA('Debe indicar el tel�fono del solicitante');
                return;
            }

            if (Mal(1, control4[i].value)) {
                OMEGA('Debe indicar el correo electr�nico del solicitante');
                return;
            }

            hay++;
        }
    }

    if (hay < 1) {
        OMEGA('No ha indicado ning�n solicitante');
        return;
    }

    if (!document.getElementById('LRE').checked && !document.getElementById('LDP').checked && !document.getElementById('LCC').checked) {
        OMEGA('Debe indicar los laboratorios a los que pertenece el cliente');
        return;
    }

    if (!confirm('Modificar datos?'))
        return;

    var parametros = {
        '_AJAX': 1,
        'accion': $('#accion').val(),
        'tipo': $('#tipo').val(),
        'id': $('#id').val(),
        'nombre': $('#nombre').val(),
        'unidad': $('#unidad').val(),
        'representante': $('#representante').val(),
        'tel': $('#tel').val(),
        'email': $('#email').val(),
        /**/
        'contacto': vector("contacto"),
        'telefonos': vector("telefono"),
        'correo': vector("correo"),
        /**/
        'solicitante': vector("solicitante"),
        'soltel': vector("soltel"),
        'solemail': vector("solemail"),
        /**/
        'fax': $('#fax').val(),
        'direccion': $('#direccion').val(),
        'actividad': $('#actividad').val(),
        'LRE': document.getElementById('LRE').checked ? '1' : '0',
        'LDP': document.getElementById('LDP').checked ? '1' : '0',
        'LCC': document.getElementById('LCC').checked ? '1' : '0',
        'estado': $('#estado').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            $('#btn').disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    //window.dialogArguments.location.reload();
                    opener.location.reload();
                    alert('Transaccion finalizada');
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function ContactoAgregar() {
    var linea = document.getElementsByName("contacto").length;
    var fila = document.createElement("tr");
    var colum = new Array(4);

    colum[0] = document.createElement("td");
    colum[1] = document.createElement("td");
    colum[2] = document.createElement("td");
    colum[3] = document.createElement("td");

    colum[0].innerHTML = (linea + 1) + '.';
    colum[1].innerHTML = '<input type="text" name="contacto" onblur="ValidaLinea()" maxlength="60"/>';
    colum[2].innerHTML = '<input type="text" name="telefono" size="9" maxlength="15"/>';
    colum[3].innerHTML = '<input type="text" name="correo" maxlength="50"/>';

    for (i = 0; i < colum.length; i++)
        fila.appendChild(colum[i]);

    document.getElementById('lolo').appendChild(fila);
}

function SolicitanteAgregar() {
    var linea = document.getElementsByName("solicitante").length;
    var fila = document.createElement("tr");
    var colum = new Array(4);

    colum[0] = document.createElement("td");
    colum[1] = document.createElement("td");
    colum[2] = document.createElement("td");
    colum[3] = document.createElement("td");

    colum[0].innerHTML = (linea + 1) + '.';
    colum[1].innerHTML = '<input type="text" name="solicitante" onblur="ValidaLinea()" maxlength="60"/>';
    colum[2].innerHTML = '<input type="text" name="soltel" size="9" maxlength="15"/>';
    colum[3].innerHTML = '<input type="text" name="solemail" maxlength="50"/>';

    for (i = 0; i < colum.length; i++)
        fila.appendChild(colum[i]);

    document.getElementById('lolo1').appendChild(fila);
}

function vector(ctrl) {
    var str = "1=1";
    control = document.getElementsByName(ctrl);
    for (i = 0; i < control.length; i++) {
        str += "&" + control[i].value.replace(/&/g, '');
    }
    return str;
}

function ValidaLinea() {
    control = document.getElementsByName("contacto");
    control3 = document.getElementsByName("telefono");
    control4 = document.getElementsByName("correo");
    for (i = 0; i < control.length; i++) {
        if (control[i].value == "") {
            control3[i].value = "";
            control4[i].value = "";
        }
    }
}

jQuery(function ($) {
    $("#btn_actualizarid").click(function () {
        $("#btn_guardarid").show();
        $(this).hide();
        $("#id").prop('readonly', false);
    });
    $("#btn_guardarid").click(function () {
        if (Mal(3, $('#id').val())) {
            OMEGA('Debe indicar la identificaci�n');
            return;
        }
        var parametros = {
            '_AJAX': 2,
            'idActual': $('#idActual').val(),
            'idNuevo': $('#id').val()
        };

        $.ajax({
            data: parametros,
            url: __SHELL__,
            type: 'post',
            beforeSend: function () {
                ALFA('Por favor espere....');
            },
            success: function (_response) {
                switch (_response) {
                    case '-0':
                        alert('Sesi�n expirada [Err:0]');
                        break;
                    case '-1':
                        alert('Error en el env�o de par�metros [Err:-1]');
                        break;
                    case '0':
                        OMEGA('Error transaccional');
                        break;
                    case '1':
                        alert('Identificaci�n actualizada');
                        window.location = 'clientes_detalle.php?acc=M&ID=' + $('#id').val();
                        break;
                    case '2':
                        OMEGA('Identificaci�n Existente');
                        break;
                    default:
                        alert('Tiempo de espera agotado');
                        break;
                }
            }
        });
    });

});
