function datos(accion,pregunta,linea){	
	if(accion=='I'){
		if( Mal(2, $('#descr').val()) ){
			OMEGA('Debe indicar el nombre de la opci�n');
			return;
		}	
		var desc = document.getElementById('descr').value;
	}else{
		var desc = document.getElementById('descr'+linea).value;
	}
	if(!confirm('Modificar datos?')) return;	
	var parametros = {
		'_AJAX' : 1,
		'id' : pregunta,
		'linea' : linea,		
		'descr' : desc,		
		'accion' : accion					
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					OMEGA('Transaccion finalizada');
					location.reload();					
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function DocumentosZip(_ARCHIVO){
	window.open("documentos_zip.php?ARCHIVO="+_ARCHIVO,"","width=100,height=100,scrollbars=yes,status=no");
	//window.showModalDialog("documentos_zip.php?ARCHIVO="+_ARCHIVO, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}

function EncuestaAnula(_CS,_VER,_ACC){
	if(!confirm('Eliminar documento?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'cs' : _CS,
		'version' : _VER,
		'accion' : _ACC,
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					alert('Transaccion finalizada');
					window.close();
					opener.location.reload();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}