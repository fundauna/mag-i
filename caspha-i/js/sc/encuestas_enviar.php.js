$(document).ready(function () {
    oTable = $('#example').dataTable({
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        /*"sScrollX": "100%",*/
        "sScrollXInner": "110%",
        "bScrollCollapse": true,
        "bFilter": true,
        "oLanguage": {
            "sEmptyTable": "No hay datos que mostrar",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sLoadingRecords": "Cargando...",
            "sProcessing": "Procesando...",
            "sSearch": "Buscar:",
            "sZeroRecords": "No hay datos que mostrar",
            "sInfoFiltered": "(filtro de _MAX_ registros)",
            "sInfoEmpty": "Mostrando 0 registros",
            "oPaginate": {
                "sFirst": "Primer",
                "sLast": "Fin",
                "sNext": "Sig.",
                "sPrevious": "Prev."
            }
        }
    });
});

function datos() {
    var str = '';
    var ok = false;
    var control = document.getElementsByName('id');

    for (i = 0; i < control.length; i++) {
        if (control[i].checked) {
            str += ";" + control[i].value;
            ok = true;
        }
    }

    if (!ok) {
        OMEGA("No tiene clientes seleccionados");
        return;
    }

    if (!confirm('Enviar encuesta?'))
        return;

    var parametros = {
        '_AJAX': 1,
        'id': $('#id').val(),
        'clientes': str
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '-3':
                    OMEGA('Error al enviar el email');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    /*window.dialogArguments*/opener.location.reload();
                    alert('Transacci�n finalizada');
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function todos(_ID, _VALOR) {
    if (_VALOR == 'N') {
        _VALOR = 'T';
    } else {
        _VALOR = 'N';
    }
    location.href = 'encuestas_enviar.php?ID=' + _ID + '&T=' + _VALOR;
}