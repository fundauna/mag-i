function desmarca(){
	document.getElementById('add').disabled = false;
	document.getElementById('mod').disabled = true;
	document.getElementById('del').disabled = true;
	document.getElementById('new').disabled = true;
	//document.getElementById('temp').value = '';
	//document.getElementById('id').value = '';
	document.getElementById('descripcion').value = '';
	document.getElementById('tipo').value = '';
	//document.getElementById('sigla').value = '';
}

function marca(control){
	if(control.selectedIndex!= -1){		
		document.getElementById('id').value = control.options[control.selectedIndex].getAttribute('id');
		document.getElementById('descripcion').value = control.options[control.selectedIndex].getAttribute('descri');
		document.getElementById('tipo').value = control.options[control.selectedIndex].getAttribute('tipo');	
		if(control.options[control.selectedIndex].getAttribute('tipo') == 2 || control.options[control.selectedIndex].getAttribute('tipo') == 3){
			document.getElementById('tr1').style="display:;"		
		}else{
			document.getElementById('tr1').style="display: none;"
		}
		if(document.getElementById('id').value == '0'){
			document.getElementById('mod').disabled = true;
			document.getElementById('del').disabled = true;
		}else{
			document.getElementById('mod').disabled = false;
			document.getElementById('del').disabled = false;
		}
		document.getElementById('add').disabled = true;
		document.getElementById('new').disabled = false;
	}
}

function modificar(accion){
	if( Mal(1, document.getElementById('descripcion').value) ){
		OMEGA('Debe digitar la descripcion de la pregunta');
		return;
	}
	
	if(document.getElementById('tipo').value==''){
		OMEGA('Debe digitar el tipo de pregunta');
		return;
	}
	
	if(confirm('Modificar Datos?')){
		var parametros = {
			'_AJAX' : 1,
			'enc' : $('#enc').val(),
			'id' : $('#id').val(),
			'descri' : $('#descripcion').val(),			
			'tipo' : $('#tipo').val(),
			'accion' : accion
		};
		
		$.ajax({
			data:  parametros,
			url:   __SHELL__,
			type:  'post',
			beforeSend: function () {
				document.getElementById('add').disabled = true;
				document.getElementById('mod').disabled = true;
				document.getElementById('del').disabled = true;
				document.getElementById('new').disabled = true;	
				ALFA('Por favor espere....');
			},
			success: function (_response) {
				switch(_response){
					case '-0':alert('Sesi�n expirada [Err:0]');break;
					case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
					case '0':
						OMEGA('Error transaccional');
						document.getElementById('add').disabled = false;
						break;
					case '1':
						GAMA('Transaccion finalizada');
						document.getElementById('add').disabled = false;
						break;
					default:alert('Tiempo de espera agotado');break;
				}
			}
		});
	}	
}

function OpcionIM(_ID){
	window.open("preguntas_detalle.php?ID="+_ID,"","width=530,height=450,scrollbars=yes,status=no");
	//window.showModalDialog("preguntas_detalle.php?ID="+_ID, this.window, "dialogWidth:530px;dialogHeight:450px;status:no;");
}