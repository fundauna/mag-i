$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		//"sScrollX": "100%",
		"sScrollXInner": "110%",
		"bScrollCollapse": true,
		"bFilter": true,
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function EncuestaExporta(_ID){
	if(!confirm('Desea exportar los resultados?')) return;
	window.open("encuestas_exporta.php?ID="+_ID+"&export=1","","width=800,height=600,scrollbars=yes,status=no");
	//window.showModalDialog("encuestas_exporta.php?ID="+_ID+"&export=1", this.window, "dialogWidth:800px;dialogHeight:600px;status:no;");
}

function EncuestaAgrega(){
	window.open("encuestas_detalle.php?acc=I&ID=","","width=530,height=450,scrollbars=yes,status=no");
	//window.showModalDialog("encuestas_detalle.php?acc=I", this.window, "dialogWidth:530px;dialogHeight:450px;status:no;");
}

function EncuestaMuestra(_ID){
	if(!confirm('Generar vista previa?')) return;
	window.open("encuestas_vista.php?ID="+_ID,"","width=800,height=600,scrollbars=yes,status=no");
	//window.showModalDialog("encuestas_vista.php?ID="+_ID, this.window, "dialogWidth:800px;dialogHeight:600px;status:no;");
}

function EncuestaVer(_ID){
	if(!confirm('Ver respuestas de clientes?')) return;
	window.open("encuestas_ver.php?ID="+_ID,"","width=800,height=600,scrollbars=yes,status=no");
	//window.showModalDialog("encuestas_ver.php?ID="+_ID, this.window, "dialogWidth:800px;dialogHeight:600px;status:no;");
}

function EncuestaEnvia(_ID){
	window.open("encuestas_enviar.php?ID="+_ID,"","width=800,height=600,scrollbars=yes,status=no");
	//window.showModalDialog("encuestas_enviar.php?ID="+_ID, this.window, "dialogWidth:800px;dialogHeight:600px;status:no;");
}

function EncuestaModifica(_ID){
	window.open("encuestas_detalle.php?acc=M&ID="+_ID,"","width=530,height=450,scrollbars=yes,status=no");
	//window.showModalDialog("encuestas_detalle.php?acc=M&ID="+_ID, this.window, "dialogWidth:530px;dialogHeight:450px;status:no;");
}

function EncuestaElimina(_ID){
	if(!confirm('Eliminar encuesta?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'id' : _ID,
		'accion' : 'D',
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					GAMA('Transacci�n finalizada');
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}