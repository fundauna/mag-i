function DocumentosZip(_ARCHIVO, _MODULO){
	var ruta = "../seguridad/documentos.php?ARCHIVO=" + _ARCHIVO + "&MODULO=" + _MODULO + "&ACCESO=72";
	window.open(ruta,"","width=100,height=100,scrollbars=yes,status=no");
	//window.showModalDialog(ruta, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}

function datos(accion){	
	if( Mal(2, $('#contrato').val()) ){
		OMEGA('Debe indicar un n�mero de contrato');
		return;
	}
	
	if( $('#tipo').val() == ''){
		OMEGA('Debe indicar un tipo de contrato');
		return;
	}
	
	if( Mal(2, $('#proveedor').val()) ){
		OMEGA('Debe seleccionar un proveedor');
		return;
	}
	
	if( $('#fecha').val() == '' ){
		OMEGA('Debe indicar una fecha v�lida');
		return;
	}
	
	if( $('#aviso').val() == '' ){
		OMEGA('Debe indicar una fecha de aviso');
		return;
	}
	
	if(!confirm('Modificar datos?')) return;
	ALFA('Por favor espere....');
	document.form.submit();
}

function ProveedoresLista(){
	window.open(__SHELL__ + "?list=1","","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function ProveedorEscoge(cs, nombre){
	opener.document.getElementById('prov').value=cs;
	opener.document.getElementById('proveedor').value=nombre;
	window.close();
}

function ValidaExiste(_id){
	if(_id=='') return false;
	
	var control = document.getElementsByName('equipo[]');
	for(i=0;i<control.length;i++){
		if(document.getElementById('equipo'+i).value == _id) return true
	}
	return false;
}

function EquiposLista(linea){
	window.open(__SHELL__ + "?list=1&tipo=1&linea=" + linea,"","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1&tipo=1&linea=" + linea, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function EquipoEscoge(cs, codigo, linea){
	if( opener.ValidaExiste(cs) ){
		alert('El equipo ya se encuentra en el contrato');
		return;
	}
	
	opener.document.getElementById('equipo'+linea).value=cs;
	opener.document.getElementById('tmp_equipo'+linea).value=codigo;
	window.close();
}

function ActivoAgregar(){
	var linea = document.getElementsByName("equipo[]").length;
	var fila = document.createElement("tr");
	var colum = new Array(5);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	colum[4] = document.createElement("td");
	
	colum[0].innerHTML = (linea+1) + '.';
	colum[1].innerHTML = '<input type="text" id="tmp_equipo'+linea+'" class="lista" readonly onclick="EquiposLista('+linea+')"/><input type="hidden" id="equipo'+linea+'" name="equipo[]"/>';
	colum[2].innerHTML = '';
	colum[3].innerHTML = '';
	colum[4].innerHTML = '';
	
	for(i=0;i<colum.length;i++)
		fila.appendChild(colum[i]);
	
	document.getElementById('lolo').appendChild(fila);
}

function MontoAgregar(){
	var linea = document.getElementsByName("monto[]").length;
	var fila = document.createElement("tr");
	var colum = new Array(4);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	
	colum[0].innerHTML = '<input type="text" name="ano[]" size="4" maxlength="4"/>';
	colum[1].innerHTML = '<input type="text" name="monto[]" value="" onblur="_FLOAT(this);" class="monto"/>';
	colum[2].innerHTML = '<select name="moneda[]"><option value="">...</option><option value="0">Colones</option><option value="1">D&oacute;lares</option></select>';
	colum[3].innerHTML = '<input type="text" name="obs[]" maxlength="50"/>';
	
	for(i=0;i<colum.length;i++)
		fila.appendChild(colum[i]);
	
	document.getElementById('lolo1').appendChild(fila);
}

function vector(ctrl){
	var str = "1=1";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		//if(control[i].value != ""){
			str += "&" + control[i].value.replace(/&/g, ''); 
		//}
	}
	return str;
}