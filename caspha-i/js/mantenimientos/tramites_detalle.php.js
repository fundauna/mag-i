jQuery(function ($) {
    $("#tel").mask("9999-9999");
    $("#idp").mask("9-9999-9999");
    $("#ide").mask("9-999-99999");
});

function datos() {

    if ($('#tipo').val() == '') {
        OMEGA('Debe indicar un tipo de tr�mite');
        return;
    }

    if (Mal(2, $('#descripcion').val())) {
        OMEGA('Debe indicar una descripci�n');
        return;
    }

    if (Mal(2, $('#fecha').val())) {
        OMEGA('Debe indicar una fecha v�lida');
        return;
    }

    if (!_HOY(document.getElementById('fecha'))) {
        OMEGA('La fecha debe ser superior a hoy');
        return;
    }

    if (Mal(2, $('#fecha').val())) {
        OMEGA('Debe indicar una fecha de aviso');
        return;
    }

    if ($('#vigencia').val() == '') {
        OMEGA('Debe indicar una vigencia v�lida');
        return;
    }

    if (!confirm('Modificar datos?'))
        return;

    var file_data = $("#archivo").prop("files")[0];
    var form_data = new FormData();
    
    form_data.append("_AJAX", 1);
    form_data.append("archivo", file_data);
    form_data.append("cs", $("#cs").val());
    form_data.append("accion", $("#accion").val());
    form_data.append("tipo", $("#tipo").val());
    form_data.append("descripcion", $("#descripcion").val());
    form_data.append("requisitos", $("#requisitos").val());
    form_data.append("fecha", $("#fecha").val());
    form_data.append("aviso", $("#aviso").val());
    form_data.append("vigencia", $("#vigencia").val());

    /*var parametros = {
        '_AJAX': 1,
        'cs': $('#cs').val(),
        'accion': $('#accion').val(),
        'tipo': $('#tipo').val(),
        'descripcion': $('#descripcion').val(),
        'requisitos': $('#requisitos').val(),
        'fecha': $('#fecha').val(),
        'aviso': $('#aviso').val(),
        'vigencia': $('#vigencia').val()
    };*/

    $.ajax({
        data: form_data,
        url: __SHELL__,
        type: 'post',
        dataType: 'script',
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {
            $('#btn').disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    alert("Transaccion finalizada");
                    opener.location.reload();
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}