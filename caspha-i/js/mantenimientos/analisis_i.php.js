function desmarca(){
	document.getElementById('add').disabled = false;
	document.getElementById('mod').disabled = true;
	document.getElementById('del').disabled = true;
	document.getElementById('new').disabled = true;
	document.getElementById('id').value = '';
	document.getElementById('nombre').value = '';
	document.getElementById('tarifa').value = '';
	document.getElementById('costo').value = '0.00';
	document.getElementById('descuento').value = '';
}

function marca(control){
	if(control.selectedIndex!= -1){
		document.getElementById('id').value = control.options[control.selectedIndex].getAttribute('id');
		document.getElementById('nombre').value = control.options[control.selectedIndex].getAttribute('nombre');
		document.getElementById('tarifa').value = control.options[control.selectedIndex].getAttribute('tarifa');
		document.getElementById('costo').value = control.options[control.selectedIndex].getAttribute('costo');
		document.getElementById('descuento').value = control.options[control.selectedIndex].getAttribute('descuento');
		
		if(document.getElementById('id').value == '0'){
			document.getElementById('mod').disabled = true;
			document.getElementById('del').disabled = true;
		}else{
			document.getElementById('mod').disabled = false;
			document.getElementById('del').disabled = false;
		}
		document.getElementById('add').disabled = true;
		document.getElementById('new').disabled = false;
	}
}

function modificar(accion){
	if( Mal(1, document.getElementById('id').value) ){
		OMEGA('Debe digitar el c�digo');
		return;
	}
	
	if( Mal(2, document.getElementById('nombre').value) ){
		OMEGA('Debe digitar el nombre');
		return;
	}
	
	if( Mal(2, document.getElementById('tarifa').value) ){
		OMEGA('Debe digitar el codigo de tarifa');
		return;
	}
	
	if( _FVAL(document.getElementById('costo').value) < 1 ){
		OMEGA('Debe digitar el costo');
		return;
	}
	
	if( Mal(1, document.getElementById('descuento').value) ){
		OMEGA('Debe indicar si tiene descuento');
		return;
	}
	
	if(confirm('Modificar Datos?')){
		var parametros = {
			'_AJAX' : 1,
			'id' : $('#id').val(),
			'nombre' : $('#nombre').val(),
			'tarifa' : $('#tarifa').val(),
			'costo' : _FVAL($('#costo').val()),
			'descuento' : $('#descuento').val(),
			'accion' : accion
		};
		
		$.ajax({
			data:  parametros,
			url:   __SHELL__,
			type:  'post',
			beforeSend: function () {
				document.getElementById('add').disabled = true;
				document.getElementById('mod').disabled = true;
				document.getElementById('del').disabled = true;
				document.getElementById('new').disabled = true;	
				ALFA('Modificando datos....');
			},
			success: function (_response) {
				switch(_response){
					case '-0':alert('Sesi�n expirada [Err:0]');break;
					case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
					case '0':
						OMEGA('Error transaccional');
						document.getElementById('add').disabled = false;
						break;
					case '1':
						GAMA('Datos modificados');
						document.getElementById('add').disabled = false;
						break;
					default:alert('Tiempo de espera agotado');break;
					//default:alert(_response);break;
				}
			}
		});
	}
}