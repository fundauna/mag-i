/***************FUNCIONES WINDOW***************/
var _MODAL;
function BETA(){
	if (_MODAL===undefined) return;
	
	if(_MODAL != ''){
		document.body.removeChild(_MODAL);
		_MODAL = '';
	}
}

function ALFA(_msg){
	_MODAL = document.createElement('div');
	_MODAL.innerHTML = '<table align="center" class="tabla" cellpadding="0" cellspacing="0"><tr><td class="alert_nw"></td><td class="alert_n"></td><td class="alert_ne"></td></tr><tr><td class="alert_e"></td><td class="alert_message" id="alert_message">'+_msg+'</td><td class="alert_w"></td></tr><tr><td class="alert_e"></td><td class="loader"></td><td class="alert_w"></td></tr><tr><td class="alert_sw"></td><td class="alert_s"></td><td class="alert_se"></td></tr></table>';
	_MODAL.setAttribute('class', 'overlay');
	document.body.appendChild(_MODAL);
}

function GAMA(_msg){
	BETA();
	_MODAL = document.createElement('div');
	_MODAL.innerHTML = '<table align="center" class="tabla" cellpadding="0" cellspacing="0"><tr><td class="alert_nw"></td><td class="alert_n"></td><td class="alert_ne"></td></tr><tr><td class="alert_e"></td><td class="alert_message" id="alert_message">'+_msg+'</td><td class="alert_w"></td></tr><tr><td class="alert_e"></td><td class="alert_btn"><input type="button" value="Aceptar" class="boton2" onclick="BETA();location.reload()" /></td><td class="alert_w"></td></tr><tr><td class="alert_sw"></td><td class="alert_s"></td><td class="alert_se"></td></tr></table>';
	_MODAL.setAttribute('class', 'overlay');
	document.body.appendChild(_MODAL);
}

function DELTA(_msg){
	BETA();
	_MODAL = document.createElement('div');
	_MODAL.innerHTML = '<table align="center" class="tabla" cellpadding="0" cellspacing="0"><tr><td class="alert_nw"></td><td class="alert_n"></td><td class="alert_ne"></td></tr><tr><td class="alert_e"></td><td class="alert_message" id="alert_message">'+_msg+'</td><td class="alert_w"></td></tr><tr><td class="alert_e"></td><td class="alert_btn"><input type="button" value="Aceptar" class="boton2" onclick="BETA();window.close()" /></td><td class="alert_w"></td></tr><tr><td class="alert_sw"></td><td class="alert_s"></td><td class="alert_se"></td></tr></table>';
	_MODAL.setAttribute('class', 'overlay');
	document.body.appendChild(_MODAL);
}

function OMEGA(_msg){
	BETA();
	_MODAL = document.createElement('div');
	_MODAL.innerHTML = '<table align="center" class="tabla" cellpadding="0" cellspacing="0"><tr><td class="alert_nw"></td><td class="alert_n"></td><td class="alert_ne"></td></tr><tr><td class="alert_e"></td><td class="alert_message" id="alert_message">'+_msg+'</td><td class="alert_w"></td></tr><tr><td class="alert_e"></td><td class="alert_btn"><input type="button" value="Aceptar" class="boton2" onclick="BETA()" /></td><td class="alert_w"></td></tr><tr><td class="alert_sw"></td><td class="alert_s"></td><td class="alert_se"></td></tr></table>';
	_MODAL.setAttribute('class', 'overlay');
	document.body.appendChild(_MODAL);
}