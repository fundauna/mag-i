function Alphanum(texto) {
	var numeros = "0123456789";
	var letras = "abcdefghijklmn�opqrstuvwxyz";
	for(i=0; i<texto.length; i++){
		if(numeros.indexOf(texto.charAt(i),0)!=-1){
			/* valida letras*/
			texto = texto.toLowerCase();
			for(i=0; i<texto.length; i++){
				if(letras.indexOf(texto.charAt(i),0)!=-1){
					return 1;
				}
			}
			return 0;
			/* fin letras */
		}
	}
	return 0;
}

function _FVAL(num){
	var str = new String();
	str = num.toString();
    str = str.replace(/([^0-9\.\-])/g, '') * 1;
	if(isNaN(str)) str = 0;
	return str;
}

function _FLOAT(obj, prefix){
	num = obj.value.replace(/,/g, ''); 
	num = _FVAL(num);
    prefix = prefix || '';
    num += '';
    var splitStr = num.split('.');
    var splitLeft = splitStr[0];
    var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '.00';
    var regx = /(\d+)(\d{3})/;
    while (regx.test(splitLeft)) {
    	splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
    }
   splitRight += '0';
   obj.value = prefix + splitLeft + splitRight.substr(0, 3);
   if(obj.value == ""/* || _FVAL(obj.value) < 0*/) obj.value = "0.00";
}

function _HOY(control){
	var ano2 = (control.value).substr(6, 4);
    var mes2 = ((control.value).substr(3, 2)) * 1 - 1;
    var dia2 = (control.value).substr(0, 2);
	
    var hoy = new Date();
    var Fecha_Fin = new Date(ano2, mes2, dia2);

    if(hoy > Fecha_Fin) return false;
	return true;
}

function MalComparaFecha(vencimiento, aviso){
	var ano1 = (vencimiento.value).substr(6, 4);
    var mes1 = ((vencimiento.value).substr(3, 2)) * 1 - 1;
    var dia1 = (vencimiento.value).substr(0, 2);
	//
	var ano2 = (aviso.value).substr(6, 4);
    var mes2 = ((aviso.value).substr(3, 2)) * 1 - 1;
    var dia2 = (aviso.value).substr(0, 2);
	
	var fecha_ven = new Date(ano1, mes1, dia1);
	var fecha_avi = new Date(ano2, mes2, dia2);
	
	if(fecha_avi > fecha_ven) return true;
	return false;
}

function _INT(control){
	var str = "";
	str = control.value;
	if(isNaN(parseInt(str))){
		control.value = "";
	}else{
		control.value = parseInt(str);
	}
}

function Mal(cantidad, valor){	
	if (valor.length < cantidad){
		return true;
	}
	
	var p1 = valor;
  	var espacios = true;
  	var cont = 0;
	
	while (espacios && (cont < p1.length)) {
   		if (p1.charAt(cont) != " ") {
    		espacios = false;
   		}
   		cont++;
  	}
   
  	if (espacios) {
   		return true;
  	}
	
	return false;
}

function _RED(_txt, _decimales){ //para campos de texto
	var _CANTIDAD = parseFloat( _txt.value.replace(/,/g, '.') );
	var _decimales = parseFloat(_decimales);
	_decimales = (!_decimales ? 2 : _decimales);
	_decimales = Math.round(_CANTIDAD * Math.pow(10, _decimales)) / Math.pow(10, _decimales);
	if(isNaN(_decimales) || _decimales == Infinity || _decimales == -Infinity) _txt.value = "0";
	else _txt.value = _decimales;
}

function _RED2(_valor, _decimales){ //para valores directos
	var valor = "" + _valor;
	var _CANTIDAD = parseFloat( valor.replace(/,/g, '.') );
	var _decimales = parseFloat(_decimales);
	_decimales = (!_decimales ? 2 : _decimales);
	_decimales = Math.round(_CANTIDAD * Math.pow(10, _decimales)) / Math.pow(10, _decimales);
	if(isNaN(_decimales) || _decimales == Infinity || _decimales == -Infinity) return 0;
	else return _decimales;
}