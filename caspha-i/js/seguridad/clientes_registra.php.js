function atras() {
    window.location.href = 'clientes.php';
}

function validaUsuario() {

    var valor = $('#id').val().replace(/_/g, '');
    var valor = valor.replace(/-/g, '');

    if (Mal(9, valor)) {
        OMEGA('La identificaci�n debe ser mayor a 9 caracteres');
        $('#id').val('');
        return;
    }

    var parametros = {
        '_AJAX': 2,
        'id': $('#id').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            if (_response == '1') {
                OMEGA('Ya se encuentra registrado');
                setTimeout(function () {
                    window.location.href = "clientes.php";
                }, 1500);
            } else {
                BETA();
            }
        }
    });
}

function datos() {
    if (Mal(3, $('#id').val())) {
        OMEGA('Debe indicar la identificaci�n');
        return;
    }

    if (Mal(2, $('#nombre').val())) {
        OMEGA('Debe indicar el nombre');
        return;
    }

    var control = document.getElementsByName('contacto');
    var control3 = document.getElementsByName('telefono');
    var control4 = document.getElementsByName('correo');
    var hay = 0;

    for (i = 0; i < control.length; i++) {
        if (!Mal(1, control[i].value)) {

            if (Mal(1, control3[i].value)) {
                OMEGA('Debe indicar el tel�fono del contacto');
                return;
            }

            if (Mal(1, control4[i].value)) {
                OMEGA('Debe indicar el correo electr�nico del contacto');
                return;
            }

            hay++;
        }
    }

    if (hay < 1) {
        OMEGA('No ha indicado ning�n contacto');
        return;
    }

    var control = document.getElementsByName('solicitante');
    var control3 = document.getElementsByName('soltel');
    var control4 = document.getElementsByName('solemail');
    var hay = 0;

    for (i = 0; i < control.length; i++) {
        if (!Mal(1, control[i].value)) {

            if (Mal(1, control3[i].value)) {
                OMEGA('Debe indicar el tel�fono del solicitante');
                return;
            }

            if (Mal(1, control4[i].value)) {
                OMEGA('Debe indicar el correo electr�nico del solicitante');
                return;
            }

            hay++;
        }
    }

    if (hay < 1) {
        OMEGA('No ha indicado ning�n solicitante');
        return;
    }


    if (!confirm('Modificar datos?'))
        return;

    var parametros = {
        '_AJAX': 1,
        'accion': $('#accion').val(),
        'tipo': '1',
        'id': $('#id').val(),
        'nombre': $('#nombre').val(),
        'unidad': '',
        'representante': $('#representante').val(),
        'tel': $('#tel').val(),
        'email': $('#email').val(),
        /**/
        'contacto': vector("contacto"),
        'telefonos': vector("telefono"),
        'correo': vector("correo"),
        /**/
        'solicitante': vector("solicitante"),
        'soltel': vector("soltel"),
        'solemail': vector("solemail"),
        /**/
        'fax': $('#fax').val(),
        'direccion': $('#direccion').val(),
        'actividad': '',
        'LRE': '1',
        'LDP': '1',
        'LCC': '1',
        'estado': '1'
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            $('#btn').disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            try {
                _response = JSON.parse(_response);
                if (_response.length > 0) {
                    alert('Datos guardados, su clave es: ' + _response[0]);
                    setTimeout(function () {
                        window.location.href = "clientes.php";
                    }, 1500);
                } else {
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                }
            } catch (error) {
                console.error(error);
                OMEGA('Error transaccional');
            }
        }
    });
}

function capturekey(e) {
    var key = (typeof event != 'undefined') ? window.event.keyCode : e.keyCode;
    if (key == '13' && document.getElementById('btn') != undefined) {
        document.getElementById('btn').click();
    }
}

function vector(ctrl) {
    var str = "1=1";
    control = document.getElementsByName(ctrl);
    for (i = 0; i < control.length; i++) {
        str += "&" + control[i].value.replace(/&/g, '');
    }
    return str;
}

function ContactoAgregar() {
    var linea = document.getElementsByName("contacto").length;
    var fila = document.createElement("tr");
    var colum = new Array(4);

    colum[0] = document.createElement("td");
    colum[1] = document.createElement("td");
    colum[2] = document.createElement("td");
    colum[3] = document.createElement("td");

    colum[0].innerHTML = (linea + 1) + '.';
    colum[1].innerHTML = '<input type="text" name="contacto" onblur="ValidaLinea()" maxlength="60"/>';
    colum[2].innerHTML = '<input type="text" name="telefono" size="9" maxlength="15"/>';
    colum[3].innerHTML = '<input type="text" name="correo" maxlength="50"/>';

    for (i = 0; i < colum.length; i++)
        fila.appendChild(colum[i]);

    document.getElementById('lolo').appendChild(fila);
}

function SolicitanteAgregar() {
    var linea = document.getElementsByName("solicitante").length;
    var fila = document.createElement("tr");
    var colum = new Array(4);

    colum[0] = document.createElement("td");
    colum[1] = document.createElement("td");
    colum[2] = document.createElement("td");
    colum[3] = document.createElement("td");

    colum[0].innerHTML = (linea + 1) + '.';
    colum[1].innerHTML = '<input type="text" name="solicitante" onblur="ValidaLinea()" maxlength="60"/>';
    colum[2].innerHTML = '<input type="text" name="soltel" size="9" maxlength="15"/>';
    colum[3].innerHTML = '<input type="text" name="solemail" maxlength="50"/>';

    for (i = 0; i < colum.length; i++)
        fila.appendChild(colum[i]);

    document.getElementById('lolo1').appendChild(fila);
}
if (navigator.appName != "Mozilla") {
    document.onkeyup = capturekey;
} else {
    document.addEventListener("keypress", capturekey, true);
}

function cambiaMascara() {
    var valor = document.getElementById("tipo").value;

    if (valor == 0) {
        $("#id").mask("9-9999-9999");
    }
    if (valor == 1) {
        $("#id").mask("9-999-999999");
    }
    if (valor == 2) {
        $("#id").mask("***************");
    }
}