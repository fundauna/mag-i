$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		/*"sScrollX": "100%",*/
		"sScrollXInner": "110%",
		"bScrollCollapse": true,
		"bFilter": true,
		"bSort": false,
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function Redirige(_id, _modulo, _pagina){
	location.href = "../" + _modulo + "/" + _pagina + "?ver=1&tablon="+_id;
}

function Elimina(_ID){
	if(!confirm('Eliminar notificacion?')) return;

	var parametros = {
		'_AJAX' : 1,
		'id' : _ID
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesión expirada [Err:0]');break;
				case '-1':alert('Error en el envío de parámetros [Err:-1]');break;
				case '0':
					GAMA('Error transaccional');
					break;
				case '1':
					GAMA('Transaccion finalizada');
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}