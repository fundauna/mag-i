function registrar() {
    window.location.href = 'clientes_registra.php';
}

function login() {
    if (Mal(4, $('#usuario').val())) {
        OMEGA('Debe indicar el usuario');
        return;
    }

    if (Mal(4, $('#clave').val())) {
        OMEGA('Debe indicar la clave');
        return;
    }

    var parametros = {
        "_AJAX": 1,
        "usuario": $('#usuario').val(),
        "clave": $('#clave').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            $('#btn').disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '-2':
                    OMEGA('Usuario y/o<br>contrase�a incorrecta');
                    $('#usuario').val('');
                    $('#clave').val('');
                    $('#btn').disabled = false;
                    break;
                case '-3':
                    OMEGA('El usuario no tiene permisos de entrada en este laboratorio');
                    break;
                case '-4':
                    OMEGA('La clave actual se ha caducado, debe cambiarla!');
                    setTimeout(function () {
                        location.href = 'menu3.php';
                    }, 3000);
                    break;
                case '1':
                    BETA();
                    location.href = 'menu2.php';
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function capturekey(e) {
    var key = (typeof event != 'undefined') ? window.event.keyCode : e.keyCode;
    if (key == '13' && document.getElementById('btn') != undefined) {
        document.getElementById('btn').click();
    }
}

if (navigator.appName != "Mozilla") {
    document.onkeyup = capturekey;
} else {
    document.addEventListener("keypress", capturekey, true);
}