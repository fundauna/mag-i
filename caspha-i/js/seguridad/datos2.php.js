function clave(chk) {
    $('#nueva').val('');
    $('#confirmacion').val('');
    $('#actual').val('');
    if (chk.checked) {
        $('#nueva').prop('disabled', false);
        $('#confirmacion').prop('disabled', false);
        $('#actual').prop('disabled', false);
    } else {
        $('#nueva').prop('disabled', true);
        $('#confirmacion').prop('disabled', true);
        $('#actual').prop('disabled', true);
    }
}

function datos() {
    if (Mal(4, $('#email').val())) {
        OMEGA('Debe indicar el email');
        return;
    }

    if ($('#checkbox').is(':checked')) {
        if (Mal(8, $('#actual').val())) {
            OMEGA('Debe indicar la clave actual<br>(Min 8 caracteres)');
            return;
        }

        if (Mal(8, $('#nueva').val())) {
            OMEGA('Debe indicar la clave nueva<br>(Min 8 caracteres)');
            return;
        }

        if (Alphanum($('#nueva').val()) == 0) {
            OMEGA('La nueva clave debe<br>ser alfanum�rica');
            return;
        }
        
        if(!(/[A-Z]/.test($('#nueva').val()))){
            OMEGA('La nueva clave tener al menos una may�scula');
            return;
        }

        if (Mal(8, $('#confirmacion').val())) {
            OMEGA('Debe indicar confirmacion<br>(Min 8 caracteres)');
            return;
        }

        if (Mal(8, $('#confirmacion').val())) {
            OMEGA('Debe indicar confirmacion<br>(Min 8 caracteres)');
            return;
        }

        if ($('#confirmacion').val() != $('#nueva').val()) {
            OMEGA('La clave de confirmaci�n es diferente');
            return;
        }
    }

    if (!confirm('Modificar datos?'))
        return;

    if ($('#checkbox').is(':checked')) {
        var parametros = {
            '_AJAX': 1,
            'email': $('#email').val(),
            'clave': 1,
            'actual': $('#actual').val(),
            'nueva': $('#nueva').val()
        };
    } else {
        var parametros = {
            '_AJAX': 1,
            'email': $('#email').val(),
            'clave': 0
        };
    }

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            $('#btn').disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            console.log(_response);
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '-2':
                    OMEGA('Contrase�a incorrecta');
                    $('#btn').disabled = false;
                    break;
                case '-3':
                    alert('Error! La clave ya se ha utilizado!');
                    $('#btn').disabled = false;
                    location.reload();
                    break;
                case '1':
                    OMEGA('Transaccion finalizada');
                    $('#btn').disabled = false;
                    parent.window.location.href="menu2.php";
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
                    //default:alert(_response);break;
            }
        }
    });
}

function capturekey(e) {
    var key = (typeof event != 'undefined') ? window.event.keyCode : e.keyCode;
    if (key == '13' && document.getElementById('btn') != undefined) {
        document.getElementById('btn').click();
    }
}

if (navigator.appName != 'Mozilla') {
    document.onkeyup = capturekey;
} else {
    document.addEventListener('keypress', capturekey, true);
}