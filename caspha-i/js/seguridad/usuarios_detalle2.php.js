function datos() {
    if (Mal(9, $('#cedula').val())) {
        OMEGA('Debe indicar la c�dula');
        return;
    }

    if (Mal(2, $('#nombre').val())) {
        OMEGA('Debe indicar el nombre');
        return;
    }

    if (Mal(4, $('#ap1').val())) {
        OMEGA('Debe indicar el apellido 1');
        return;
    }

    if (Mal(4, $('#ap2').val())) {
        OMEGA('Debe indicar el apellido 2');
        return;
    }

    if (Mal(4, $('#email').val())) {
        OMEGA('Debe indicar el email');
        return;
    }
    
    if (!confirm('Registrarse?'))
        return;

    var parametros = {
        '_AJAX': 1,
        'id': $('#id').val(),
        'accion': $('#accion').val(),
        'cedula': $('#cedula').val(),
        'nombre': $('#nombre').val(),
        'ap1': $('#ap1').val(),
        'ap2': $('#ap2').val(),
        'email': $('#email').val(),
        'login': $('#login').val(),
        'clave': $('#clave').val(),
        'LID':$('#LID').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            $('#btn').disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    alert('Registro Completado');
                    window.location.href='login.php';
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
                    //default:alert(_response);break;
            }
        }
    });
}