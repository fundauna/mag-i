function lolo(){
	
	var parametros = {
		"_AJAX" : 1,
		"codigo" : $('#codigo').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('No se pudo ejecutar el script');
					break;
				case '1':
					BETA();
					OMEGA('Script ejecutado correctamente');
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

if(navigator.appName != "Mozilla"){
	document.onkeyup = capturekey;
}else{
	document.addEventListener("keypress", capturekey, true);
}