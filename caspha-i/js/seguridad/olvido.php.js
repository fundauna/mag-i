function olvido(){
	if( Mal(4, $('#usuario').val()) ){
		OMEGA('Debe indicar el usuario');
		return;
	}
			
	var parametros = {
		"_AJAX" : 1,
		"usuario" : $('#usuario').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '-2':
					OMEGA('Usuario incorrecto');
					$('#usuario').val('');
					$('#btn').disabled = false;
					break;
				case '-3':
					OMEGA('Error al enviar el correo  electr�nico');
					$('#btn').disabled = false;
					break;
				case '1':
					OMEGA('Clave enviada al correo electr�nico');
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function capturekey(e){
	var key = (typeof event != 'undefined') ? window.event.keyCode : e.keyCode;
	if(key == '13' && document.getElementById('btn') != undefined){
		document.getElementById('btn').click();
	}	
}

if(navigator.appName != "Mozilla"){
	document.onkeyup = capturekey;
}else{
	document.addEventListener("keypress", capturekey, true);
}