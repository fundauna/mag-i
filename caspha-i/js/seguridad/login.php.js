function login() {
    if (Mal(4, $('#usuario').val())) {
        OMEGA('Debe indicar el usuario');
        return;
    }

    if (Mal(4, $('#clave').val())) {
        OMEGA('Debe indicar la clave');
        return;
    }

    var parametros = {
        "_AJAX": 1,
        "usuario": $('#usuario').val(),
        "clave": $('#clave').val(),
        "LID": $('#LID').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            $('#btn').disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '2':
                     alert('Usuario Encontrado! \n Para continuar debe completar sus datos');
                     usuario2($('#usuario').val(),$('#clave').val());
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '-2':
                    OMEGA('Usuario y/o<br>contrase�a incorrecta');
                    $('#usuario').val('');
                    $('#clave').val('');
                    $('#btn').disabled = false;
                    break;
                case '-3':
                    OMEGA('El usuario no tiene permisos de entrada en este laboratorio');
                    break;
                case '1':
                    BETA();
                    location.href = 'menu.php';
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function usuario2(usuario,cl){
    $('#usuario2').val(usuario);
    $('#clave2').val(cl);
    document.getElementById('frm1').submit();
}

function capturekey(e) {
    var key = (typeof event != 'undefined') ? window.event.keyCode : e.keyCode;
    if (key == '13' && document.getElementById('btn') != undefined) {
        document.getElementById('btn').click();
    }
}

if (navigator.appName != "Mozilla") {
    document.onkeyup = capturekey;
} else {
    document.addEventListener("keypress", capturekey, true);
}