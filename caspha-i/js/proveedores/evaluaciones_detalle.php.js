function CambiaTipo(){
	var tipoProv = $('#tipo_prov').val();
	
	if(tipoProv == ''){
		VisibleGeneral('none');
		VisibleBienes('none');
		VisibleServicios('none');
		document.getElementById('lbl_tipo').innerHTML = '';
	}else if(tipoProv == '0'){
		VisibleGeneral('');
		VisibleBienes('');
		VisibleServicios('none');
		document.getElementById('lbl_tipo').innerHTML = 'Bienes y suministros';
		$('#porc').val('30');
		Calcula();
	}else{
		VisibleGeneral('');
		VisibleBienes('none');
		VisibleServicios('');
		document.getElementById('lbl_tipo').innerHTML = 'Servicios';
		$('#porc').val('45');
		Calcula();
	}
}

function Calcula(){
	var tipoProv = $('#tipo_prov').val();
	if(tipoProv == '') return;
	
	var total = E18 = E28 = E40 = E50 = E63 = 0;
	var precio1 = _FVAL($('#precio1').val());
	var precio2 = _FVAL($('#precio2').val());
	var porc = parseInt($('#porc').val());
	var calif1 = parseInt($('#calif1').val());
	var calif2 = parseInt($('#calif2').val());
	var calif3 = parseInt($('#calif3').val());
	var calif4 = parseInt($('#calif4').val());
	var calif5 = parseInt($('#calif5').val());
	var calif6 = parseInt($('#calif6').val());
	var calif7 = parseInt($('#calif7').val());
	var calif8 = parseInt($('#calif8').val());
	var lbl = '';
	//
	E18 = (1-((precio1-precio2)/precio2))*porc;
	E50 = calif7 * 0.25;
	//
	if(tipoProv == '0'){
		E28 = calif1 * 0.25;
		E63 = calif8 * 0.2;
	}else{
		E40 = calif2 + calif3 + calif4 + calif5 + calif6;
	}
	total = parseFloat(E18) + parseFloat(E28) + parseFloat(E40) + parseFloat(E50) + parseFloat(E63);
	if(total > 100) total = 100;
	//
	$('#puntos1').val(E18); _FLOAT(document.getElementById('puntos1'));
	$('#puntos2').val(E28); _FLOAT(document.getElementById('puntos2'));
	$('#puntos3').val(E40); _FLOAT(document.getElementById('puntos3'));
	$('#puntos4').val(E50); _FLOAT(document.getElementById('puntos4'));
	$('#puntos5').val(E63); _FLOAT(document.getElementById('puntos5'));
	$('#total').val(total); _FLOAT(document.getElementById('total'));
	//
	if(total == 100) lbl = 'Excelente';
	else if(total < 100 && total >= 90) lbl = 'Muy Bueno';
	else if(total < 90 && total >= 80) lbl = 'Bueno';
	else if(total < 80 && total >= 60) lbl = 'Regular';
	else lbl = 'Malo';
	document.getElementById('lbl_calificacion').innerHTML = lbl;
}

function VisibleGeneral(_modo){
	document.getElementById('tgeneral0').style.display = _modo;
	document.getElementById('tgeneral1').style.display = _modo;
	document.getElementById('tgeneral2').style.display = _modo;
}

function VisibleBienes(_modo){
	document.getElementById('tbienes1').style.display = _modo;
	document.getElementById('tbienes2').style.display = _modo;
}

function VisibleServicios(_modo){
	document.getElementById('tservicios').style.display = _modo;
}

function datos(){
	if( Mal(1, $('#prov').val()) ){
		OMEGA('Debe seleccionar un proveedor');
		return;
	}
	
	if( Mal(5, $('#orden').val()) ){
		OMEGA('Debe indicar el n�mero de orden que se evalua');
		return;
	}

	if( _FVAL(document.getElementById('precio1').value) < 1){
		OMEGA('Debe indicar el precio total ofrecido');
		return;
	}
	
	if( _FVAL(document.getElementById('precio2').value) < 1){
		OMEGA('Debe indicar el precio total de la oferta');
		return;
	}
	
	if( $('#calif7').val() == ''){
		OMEGA('Debe indicar el criterio de plazo');
		return;
	}
		
	if($('#tipo_prov').val() == '0'){
		if( $('#calif1').val() == ''){
			OMEGA('Debe indicar el criterio de calidad');
			return;
		}
		
		if( $('#calif8').val() == ''){
			OMEGA('Debe indicar el criterio de<br>cumplimiento de cantidad');
			return;
		}
	}else{
		if( $('#calif2').val() == '' || $('#calif3').val() == '' ||$('#calif4').val() == '' ||$('#calif5').val() == '' ||$('#calif6').val() == ''){
			OMEGA('Debe indicar el criterio de calidad');
			return;
		}
	}
	
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'id' : $('#id').val(),
		'accion' : $('#accion').val(),
		'prov' : $('#prov').val(),
		'orden' : $('#orden').val(),
		'lineas' : $('#lineas').val(),	
		'total' : $('#total').val(),
		'precio1' : _FVAL(document.getElementById('precio1').value),
		'precio2' : _FVAL(document.getElementById('precio2').value),
		'porc' : $('#porc').val(),
		'calif1' : $('#calif1').val(),
		'calif2' : $('#calif2').val(),
		'calif3' : $('#calif3').val(),
		'calif4' : $('#calif4').val(),
		'calif5' : $('#calif5').val(),
		'calif6' : $('#calif6').val(),
		'calif7' : $('#calif7').val(),
		'calif8' : $('#calif8').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					alert("Transaccion finalizada");
					opener.location.reload();
					window.close();				
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function ProveedoresLista(){
	window.open(__SHELL__ + "?list=1","","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function ProveedorEscoge(cs, nombre, tipo){
	opener.document.getElementById('prov').value=cs;
	opener.document.getElementById('proveedor').value=nombre;
	opener.document.getElementById('tipo_prov').value=tipo;
	opener.CambiaTipo();
	window.close();
}