jQuery(function ($) {
    $("#tel").mask("9999-9999");
    $("#fax").mask("9999-9999");
});

function datos(tipo) {
    if ($('#tipo').val() == '') {
        OMEGA('Debe indicar un tipo de proveeedor');
        return;
    }

    if ($('#naturaleza').val() == '') {
        OMEGA('Debe indicar la naturaleza');
        return;
    }


    if (Mal(2, $('#nombre').val())) {
        OMEGA('Debe indicar el nombre');
        return;
    }

    if (Mal(2, $('#direccion').val())) {
        OMEGA('Debe indicar una direcci�n v�lida');
        return;
    }

    if (Mal(2, $('#tel').val())) {
        OMEGA('Debe indicar el tel�fono');
        return;
    }

    if (Mal(2, $('#email').val())) {
        OMEGA('Debe indicar un correo electr�nico v�lido');
        return;
    }

    var control = document.getElementsByName('contacto');
    var control2 = document.getElementsByName('area');
    var control3 = document.getElementsByName('telefono');
    var control4 = document.getElementsByName('correo');
    var hay = 0;

    for (i = 0; i < control.length; i++) {
        if (!Mal(1, control[i].value)) {
            if (Mal(1, control2[i].value)) {
                OMEGA('Debe indicar el �rea al cual pertenece el contacto');
                return;
            }

            if (Mal(1, control3[i].value)) {
                OMEGA('Debe indicar el tel�fono del contacto');
                return;
            }

            if (Mal(1, control4[i].value)) {
                OMEGA('Debe indicar el correo electr�nico del contacto');
                return;
            }

            hay++;
        }
    }

    if (hay < 1) {
        OMEGA('No ha indicado ning�n contacto');
        return;
    }

    if (!document.getElementById('LRE').checked && !document.getElementById('LDP').checked && !document.getElementById('LCC').checked) {
        OMEGA('Debe indicar los laboratorios a los que pertenece el proveedor');
        return;
    }

    if ($('#clasificacion').val() == '') {
        OMEGA('Debe indicar una clasificaci�n v�lida');
        return;
    }

    if (!confirm('Modificar datos?'))
        return;

    var temp;
    if (tipo == '0') {
        temp = "idp";
    } else {
        temp = "ide";
    }
    var parametros = {
        '_AJAX': 1,
        'cs': $('#cs').val(),
        'accion': $('#accion').val(),
        'tipo': $('#tipo').val(),
        'naturaleza': $('#naturaleza').val(),
        'id': $('#' + temp).val(),
        'nombre': $('#nombre').val(),
        'direccion': $('#direccion').val(),
        'telefono': $('#tel').val(),
        'fax': $('#fax').val(),
        'otro': $('#otro').val(),
        'email': $('#email').val(),
        'contacto': vector("contacto"),
        'area': vector("area"),
        'telefonos': vector("telefono"),
        'extension': vector("extension"),
        'directo': vector("directo"),
        'Otelefono': vector("Otelefono"),
        'correo': vector("correo"),
        'Ocorreo': vector("Ocorreo"),
        'notas': $('#notas').val(),
        'clasificacion': $('#clasificacion').val(),
        'LRE': document.getElementById('LRE').checked ? '1' : '0',
        'LDP': document.getElementById('LDP').checked ? '1' : '0',
        'LCC': document.getElementById('LCC').checked ? '1' : '0',
        'critico': document.getElementById('critico').checked ? '1' : '0'
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            $('#btn').disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    alert("Transaccion finalizada");
                    opener.location.reload();
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function ContactoAgregar() {
    var linea = document.getElementsByName("contacto").length;
    var fila = document.createElement("tr");
    var colum = new Array(5);

    colum[0] = document.createElement("td");
    colum[1] = document.createElement("td");
    colum[2] = document.createElement("td");
    colum[3] = document.createElement("td");
    colum[4] = document.createElement("td");
    colum[5] = document.createElement("td");
    colum[6] = document.createElement("td");
    colum[7] = document.createElement("td");
    colum[8] = document.createElement("td");

    colum[0].innerHTML = (linea + 1) + '.';
    colum[1].innerHTML = '<input type="text" name="contacto" onblur="ValidaLinea()" maxlength="60"/>';
    colum[2].innerHTML = '<input type="text" name="area" maxlength="60"/>';
    colum[3].innerHTML = '<input type="text" name="telefono" size="9" maxlength="15"/>';
    colum[4].innerHTML = '<input type="text" name="extension" value="" size="9" maxlength="15">';
    colum[5].innerHTML = '<input type="text" name="directo" value="" size="9" maxlength="15">';
    colum[6].innerHTML = '<input type="text" name="Otelefono" value="" size="9" maxlength="15">';
    colum[7].innerHTML = '<input type="text" name="correo" value="" maxlength="50">';
    colum[8].innerHTML = '<input type="text" name="Ocorreo" value="" maxlength="50">';

    for (i = 0; i < colum.length; i++)
        fila.appendChild(colum[i]);

    document.getElementById('lolo').appendChild(fila);
}

function vector(ctrl) {
    var str = "1=1";
    control = document.getElementsByName(ctrl);
    for (i = 0; i < control.length; i++) {
        //if(control[i].value != ""){
        str += "&" + control[i].value.replace(/&/g, '');
        //}
    }
    return str;
}

function ValidaLinea() {
    control = document.getElementsByName("contacto");
    control2 = document.getElementsByName("area");
    control3 = document.getElementsByName("telefono");
    control4 = document.getElementsByName("correo");
    for (i = 0; i < control.length; i++) {
        if (control[i].value == "") {
            control2[i].value = "";
            control3[i].value = "";
            control4[i].value = "";
        }
    }
}

function EliminaLinea(i) {
    if (confirm("Desea eliminar este Contacto?")) {
        var parametros = {
            '_AJAX': 2,
            'cs': $('#cs').val(),
            'accion': $('#accion').val(),
            'contacto': vector("contacto"),
            'area': vector("area"),
            'telefono': vector("telefono"),
            'extension': vector("extension"),
            'directo': vector("directo"),
            'Otelefono': vector("Otelefono"),
            'correo': vector("correo"),
            'Ocorreo': vector("Ocorreo"),
            'borrar':i
        };

        $.ajax({
            data: parametros,
            url: __SHELL__,
            type: 'post',
            beforeSend: function () {
                $('#btn').disabled = true;
                ALFA('Por favor espere....');
            },
            success: function (_response) {
                switch (_response) {
                    case '-0':
                        alert('Sesi�n expirada [Err:0]');
                        break;
                    case '-1':
                        alert('Error en el env�o de par�metros [Err:-1]');
                        break;
                    case '0':
                        OMEGA('Error transaccional');
                        $('#btn').disabled = false;
                        break;
                    case '1':
                        location.reload();
                        break;
                    default:
                        alert('Tiempo de espera agotado');
                        break;
                }
            }
        });
    }
}