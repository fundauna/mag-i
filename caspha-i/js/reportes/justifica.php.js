function ClienteEscoge(id, nombre) {

    opener.document.getElementById('id_cliente').value = id;
    opener.document.getElementById('nomcliente').value = nombre;
    window.close();
    /*
        strBusqueda = nombre.toLowerCase();
        if (strBusqueda.search('estacion') == 0 || strBusqueda.search('estaci?n') == 0) {
            opener.document.getElementById('entregacliente').value = 3;
        } else {
            opener.document.getElementById('entregacliente').value = 10;
        }*/
    window.opener.getSolicitante();
    //opener.document.getElementById('nomsolicitante').value=contacto;
}

function ClientesLista() {
    window.open(__SHELL__ + "?list=1", "", "width=400,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function valida() {
    // var numInfo =  $("#numInfo").val();
    var form_data = new FormData();
    var file = $("#archivo").prop("files")[0];
    form_data.append("_AJAX", 10);
    form_data.append("numInfo", $("#numInfo").val());
    form_data.append("file", file);
    var ext = document.getElementById('archivo').value;
    //obtiene extencion
    ext = (ext.split('.').pop());

    if (document.getElementById('archivo').value == '') {
        swal('Alerta!', 'Debe adjuntar un archivo', 'error');
        return;
    }

    if (ext != 'pdf') {
        swal('Alerta!', 'Debe adjuntar un archivo de tipo pdf', 'error');
        return;
    }
    $.ajax({
        data: form_data,
        url: __SHELL__,
        type: 'post',
        dataType: 'script',
        cache: false,
        contentType: false,
        processData: false,

        beforeSend: function () {
            alert({
                title: "Por favor espere!",
                text: "Se esta procesando la informacion!",
                type: "info",
                allowOutsideClick: false,
                showConfirmButton: false
            });
        },

        success: function (_response) {
            switch (_response) {

                case '-1':
                    swal('Error!', 'Error en el envio de parametros [Err:-1].', 'error');
                    break;
                case '0':
                    swal('Error!', 'Se genero un error.', 'error');
                    break;
                case '10':
                    swal({
                        title: "Exito!",
                        text: "Se Guardo el documento correctamente!",
                        type: "success",
                        allowOutsideClick: false,
                        timer: 1500,
                        showConfirmButton: false
                    });
                    setTimeout(function () {
                        opener.location.reload();
                        window.close();
                        //location.reload();
                    }, 1500);
                    break;
                default:
                    swal('Error!', 'Tiempo de espera agotado.', 'error');
                    break;
            }
        }
    });
}

function salvar(){

    if($('#solicita').val() == ''){
        swal('Error!', 'Debe digitar un solicitante.', 'error');
        return;
    }
    if($('#codigo').val() == ''){
        swal('Error!', 'Debe digitar un codigo externo.', 'error');
        return;
    }
    if($('#metodo').val() == ''){
        swal('Error!', 'Debe digitar un metodo.', 'error');
        return;
    }

    var parametros = {
        '_AJAX': 2,
        'numInfo': $('#numInfo').val(),
        'cliente':  $('#nomcliente').val(),
        'id_cliente':  $('#id_cliente').val(),
        'solicitante':  $('#solicita').val(),
        'codExterno':  $('#codigo').val(),
        'frf':  $('#obsfrf').val(),
        'metodo':  $('#metodo').val(),
        'observaciones':  $('#observaciones').val(),
        'usuario':  $('#usuario').val(),
        'numSolicitud':  $('#numSolicitud').val(),
    }

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            $('#btn').disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {

            switch (_response) {
                case '-0':
                    swal('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    swal('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    swal('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    swal({
                        title: 'Transaccion Finalizada',
                        text: "Se han guardado los datos",
                        type: 'save',
                        timer: 3000,
                    });
                    setTimeout("self.close()",2000)

                    break;
                default:
                    swal('Tiempo de espera agotado');
                    break;
            }
        }
    });



}