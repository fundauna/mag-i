var avisa = false;

function ActivaSello() {
    document.getElementById('eca1').style.display = '';
    document.getElementById('eca0').style.display = 'none';
}

function Eliminar() {
    if (!confirm('Desechar informe?')) return;
    _Modificar(4);
}

function Eliminarlo() {
    window.open("justifica.php?acc=I&ID=LRE-2019-9", "", "width=550,height=450,scrollbars=yes,status=no");
}

function Convertir() {
    if (!confirm('Convertir preliminar en informe oficial?')) return;
    _Modificar(0);
}

function Aprobar() {
    if (!confirm('Aprobar informe?')) return;
    _Modificar(2);


}

function CambiaFirma() {
    if (!confirm('Eliminar firma actual?')) return;
    _Modificar(8);
}

//firma
function Firmar() {
    var firma = $('#firmante').val();

    if (firma == '') {
        alert('Debe seleccionar quien firmara el documento');
    } else {

        if (!confirm('Firmar informe?')) return;
        _Modificar(9);
    }
}

function Anular() {
    if (!avisa) {
        $('#obs').val('');
        avisa = true;
    }

    if (Mal(3, $('#obs').val())) {
        alert('Debe indicar la justificaci�n en observaciones');
        return;
    }

    if (!confirm('Anular informe?')) return;
    _Modificar(5);
}

function _Modificar(_estado) {

    var parametros = {
        '_AJAX': 1,
        'id': $('#id').val(),
        'cliente': $('#cliente').val(),
        'procedimiento': $('#proc').val(),
        'obs': $('#obs').val(),
        'firmante': $('#firmante').val(),
        'estado': _estado
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    alert("Transaccion finalizada");
                    opener.location.reload();
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function imprimirlo() {
    var cs = $('#id').val();
    cs = cs.trim(cs);
    window.open("https://app.sfe.go.cr/ws_ReporteSILAB/frmreportesolicitud?solicitud=" + cs + "&formulario=6", "", "width=550,height=450,scrollbars=yes,status=no");
}

