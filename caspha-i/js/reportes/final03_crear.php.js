var avisa = false;

function ValidaExiste(_id){
	var control = document.getElementsByName('equipo');
	for(i=0;i<control.length;i++){
		if(document.getElementById('equipo'+i).value == _id) return true
	}
	return false;
}

function LineasMas(){
	var linea = document.getElementsByName("equipo").length;
	var fila = document.createElement("tr");
	var colum = new Array(4);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	
	colum[0].innerHTML = (linea+1) + '.';
	colum[1].innerHTML = '<input type="text" id="tmp_equipo'+linea+'" name="tmp_equipo" class="lista2" readonly onclick="EquiposLista('+linea+')"/><input type="hidden" id="equipo'+linea+'" name="equipo"/>';
	colum[2].innerHTML = '<input type="text" id="tmp_usuario'+linea+'" name="tmp_usuario" class="lista2" readonly onclick="UsuariosLista('+linea+')" /><input type="hidden" id="usuario'+linea+'" name="usuario"/></td>';
	colum[3].innerHTML = '<input type="text" name="horas" class="monto2" onblur="_FLOAT(this)" />';

	for(i=0;i<colum.length;i++)
		fila.appendChild(colum[i]);
	
	document.getElementById('lolo').appendChild(fila);
}

function UsuariosLista(_linea){
	window.open(__SHELL__ + "?list2=" + _linea,"","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list2=" + _linea, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function UsuarioEscoge(id, nombre, linea){
	opener.document.getElementById('usuario'+linea).value=id;
	opener.document.getElementById('tmp_usuario'+linea).value=nombre;
	window.close();
}

function EquiposLista(_linea){
	window.open(__SHELL__ + "?list=" + _linea,"","width=500,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=" + _linea, this.window, "dialogWidth:500px;dialogHeight:200px;status:no;");
}

function EquipoEscoge(cs, codigo, linea){
	if( opener.ValidaExiste(cs) ){
		alert('El equipo seleccionado ya se encuentra en la lista');
		return;
	}
	
	opener.document.getElementById('equipo'+linea).value=cs;
	opener.document.getElementById('tmp_equipo'+linea).value=codigo;
	window.close();
}

function ObsMuestra(_linea){
	if(document.getElementById('tr_'+_linea).style.display == 'none'){
		document.getElementById('tr_'+_linea).style.display = '';
	}else{
		document.getElementById('tr_'+_linea).style.display = 'none';
		document.getElementById('obs'+_linea).value = '';
	}
}

function Eliminar(){
	if(!confirm('Desechar informe?')) return;
	_Modificar(4);
}

function Convertir(){
	var vector1 = document.getElementsByName("metodo");
	
	for(i=0;i<vector1.length;i++){
		if( Mal(1, vector1[i].value) ){
			alert('Debe indicar el m�todo en la l�nea ' + (i+1));
			return;
		}
	}
	
	/*var vector1 = document.getElementsByName("equipo");
	var vector2 = document.getElementsByName("usuario");
	var vector3 = document.getElementsByName("horas");
	
	var ok = false;
	for(i=0;i<vector1.length;i++){
		if( !Mal(1, vector1[i].value) ){
			if( Mal(1, vector2[i].value) ){
				alert('Debe indicar el analista');
				return;
			}
			
			if( Mal(1, vector3[i].value) || vector3[i].value=='0.00' ){
				alert('Debe indicar las horas de uso del equipo');
				return;
			}
			
			ok = true;
			break;
		}
	}
	
	if(!ok) if(!confirm("No ha indicado ning�n equipo, desea continuar?")) return;*/
	//alert(vector('metodo'));
	if(!confirm('Convertir preliminar en informe oficial?')) return;
	
	_Modificar2();
}

function Anular(){
	if(!avisa){
		$('#obs').val('');
		avisa = true;
	}
	
	if( Mal(3, $('#obs').val()) ){
		alert('Debe indicar la justificaci�n en observaciones');
		return;
	}
	
	if(!confirm('Anular informe?')) return;
	_Modificar(5);
}

function _Modificar2(){
	var parametros = {
		'_AJAX' : 1,
		'id':$('#id').val(),
		'obs':$('#obs').val(),
		'obser': vector2('obs'), 
        'muestra':$('#muestra').val(),
		'metodos':vector('metodo'),
        'metodosotros':vector('metodootro'),
		'equipos':vector('equipo'),
		'analistas':vector('usuario'),
		'horas':vector('horas'),
		'estado': '0'
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					alert("Transaccion finalizada");
					opener.location.reload();
					window.close();				
					break;
				default:alert('Tiempo de espera agotado. '+_response+'.obs '+$('#muestra').val());break;
			}
		}
	});
}

function _Modificar(_estado){
	var parametros = {
		'_AJAX' : 1,
		'id' :$('#id').val(),
		'cliente' :$('#cliente').val(),
		'obs' :$('#obs').val(),
		'estado' : _estado,
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					alert("Transaccion finalizada");
					opener.location.reload();
					window.close();				
					break;
				default:alert('Tiempo de espera agotado. ' +_response);break;
			}
		}
	});
}

function vector(ctrl){
	var str = "1=1";
	var control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		//if(control[i].value != ""){
			str += "&" + control[i].value.replace(/&/g, ''); 
		//}
	}
	//error_log("vector:"&str);
	return str;
}

function vector2(ctrl){
	var str = "1=1";
	var control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		if(control[i].value.replace(/ /g, '') != ""){
			str += "&" + (i+1) + "^" + control[i].value.replace(/&/g, ''); 
		}
	}
	//error_log("vector2:"&str);
	return str;
}

function muestraMetodosOtros() {
    metodos = document.getElementsByName("metodo");
    metodosOtros = document.getElementsByName("metodootro");
    for (ix = 0; ix < metodos.length; ix++) {
        metodosOtros[ix].hidden=metodos[ix].value!='15';
    }
}