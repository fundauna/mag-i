function datos() {

    if (Mal(3, $('#nombre').val())) {
        OMEGA('Debe indicar el nombre');
        return;
    }

    if (Mal(3, $('#cargo').val())) {
        OMEGA('Debe indicar el cargo');
        return;
    }

    var txt = 'Ingresar';
    if ($('#accion').val() == 'M') {
        txt = 'Modificar';
    }
    if (!confirm(txt + ' datos?'))
        return;

    var parametros = {
        '_AJAX': 1,
        'id': $('#id').val(),
        'accion': $('#accion').val(),
        'nombre': $('#nombre').val(),
        'cargo': $('#cargo').val(),
        'estado': $('#estado').val(),
        'obs': $('#obs').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    break;
                case '1':
                    opener.location.reload();
                    DELTA('Transaccion finalizada');
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}