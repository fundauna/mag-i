$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"sScrollX": "100%",
		/*"sScrollXInner": "110%",*/
		"bScrollCollapse": true,
		"bFilter": true,
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function SolicitudesBuscar(btn){
	if( Mal(1, $('#valor').val()) ){
		if( Mal(1, $('#desde').val()) || Mal(1, $('#hasta').val()) ){
			OMEGA('Debe indicar la fecha');
			return;
		}
	}
	
	btn.disabled = true;
	btn.form.submit();
}

function InformeDetalle(_id, _estado, _lab){
	if(_estado=='t' && _lab != '1') _estado = 'crear';
	else _estado = 'detalle';
	window.open("final0"+_lab+"_"+_estado+".php?ID="+_id,"","width=850,height=500,scrollbars=yes,status=no");
	//window.showModalDialog("final0"+_lab+"_"+_estado+".php?ID="+_id, this.window, "dialogWidth:850px;dialogHeight:500px;status:no;");
}

function Anexar(_id){
	window.open("final_anexar.php?ID="+_id,"","width=400,height=300,scrollbars=yes,status=no");
	//window.showModalDialog("final_anexar.php?ID="+_id, this.window, "dialogWidth:400px;dialogHeight:300px;status:no;");
}

function Sustituir(_id){
	window.open("justifica.php?acc=I&ID="+_id,"","width=600,height=800,scrollbars=yes,status=no");
}


function DocumentosZip(_ARCHIVO, _MODULO){
	var ruta = "../seguridad/documentos.php?ARCHIVO=" + _ARCHIVO + "&MODULO=" + _MODULO + "&ACCESO=88";
	window.open(ruta,"","width=100,height=100,scrollbars=yes,status=no");
	//window.showModalDialog(ruta, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}

function DocElimina(doc){
	if(confirm('Eliminar documento?')){
		var parametros = {
			'_AJAX' : 1,
			'id' : doc,
		};
		
		$.ajax({
			data:  parametros,
			url:   __SHELL__,
			type:  'post',
			beforeSend: function () {
				ALFA('Por favor espere....');
			},
			success: function (_response) {
				switch(_response){
					case '-0':alert('Sesi�n expirada [Err:0]');break;
					case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
					case '1':
						OMEGA('Anexo eliminado');
						break;
					default:alert('Tiempo de espera agotado');break;
				}
			}
		});
	}
}