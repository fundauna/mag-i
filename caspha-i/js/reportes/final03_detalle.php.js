var avisa = false;

function EscribeMetodos(_linea, _id){
	//document.getElementById('metodo'+_linea).value = _resumen;
        $('#metodo'+_linea).val(_id);
}

function EscribeMetodosOtros(_linea, _id){
	//document.getElementById('metodo'+_linea).value = _resumen;
        $('#metodootro'+_linea).val(_id);
}

function EscribeObs(_linea, _resumen){
	document.getElementById('tr_'+_linea).style.display = '';
	document.getElementById('obs'+_linea).innerHTML = 'Observaciones: ' + _resumen + '<br>&nbsp;';
}

function Aprobar(){
	if(!confirm('Aprobar informe?')) return;
	_Modificar(2);
}

function Eliminar(){
	if(!confirm('Desechar informe?')) return;
	_Modificar(4);
}

function Guardar() {

    metodos = document.getElementsByName("metodo");
    metodosarray = new Array();
    for (i = 0; i < metodos.length; i++) {
        metodosarray.push(metodos[i].value);
    }
    
    metodosotros = document.getElementsByName("metodootro");
    metodosotrosarray = new Array();
    for (i = 0; i < metodosotros.length; i++) {
        metodosotrosarray.push(metodosotros[i].value);
    }
    var parametros = {
        '_AJAX': 2,
        'id': $('#id').val(),
        'metodos': metodosarray,
        'metodosotros': metodosotrosarray
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    alert("Transaccion finalizada");
                    opener.location.reload();
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function Anular(){
	if(!avisa){
		$('#obs').val('');
		avisa = true;
	}
	
	if( Mal(3, $('#obs').val()) ){
		alert('Debe indicar la justificaci�n en observaciones');
		return;
	}
	
	if(!confirm('Anular informe?')) return;
	_Modificar(5);
}

function _Modificar(_estado){
	var parametros = {
		'_AJAX' : 1,
		'id' :$('#id').val(),
		'cliente' :$('#cliente').val(),
		'obs' :$('#obs').val(),
		'estado' : _estado,
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					alert("Transaccion finalizada");
					opener.location.reload();
					window.close();				
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function muestraMetodosOtros() {
    metodos = document.getElementsByName("metodo");
    metodosOtros = document.getElementsByName("metodootro");
    for (ix = 0; ix < metodos.length; ix++) {
        metodosOtros[ix].hidden=metodos[ix].value!='15';
    }
}