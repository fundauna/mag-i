$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"sScrollX": "100%",
		/*"sScrollXInner": "110%",*/
		"bScrollCollapse": true,
		"bFilter": true,
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function SolicitudesImprime(_ID){
	var tipo = document.getElementById('LAB').value;
	if (tipo==1)//LRE
	{
		window.open("https://app.sfe.go.cr/ws_ReporteSILAB/frmreportesolicitud?solicitud=" + _ID + "&formulario=3", "", "width='100%',height='100%',scrollbars=yes,status=no");
	}
	else
	{ 
		window.open("../servicios/solicitud0" + tipo + "_imprimir.php?ID="+_ID,"","width=900,height=750,scrollbars=yes,status=no");
	}
	
	
	//window.showModalDialog("../servicios/solicitud0" + tipo + "_imprimir.php?ID="+_ID, this.window, "dialogWidth:900px;dialogHeight:750px;status:no;");
}

function Generar(_solicitud, _muestra){
	if(!confirm("Generar informe preliminar?")) return;
	
	var parametros = {
		'_AJAX' : 1,
		'solicitud' : _solicitud,
		'muestra' : _muestra
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':OMEGA('Solicitud inexistente o no se encuentra aprobada');break;
				case '1':
					alert('Informe generado');
					location.href = 'final_historial.php';
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}