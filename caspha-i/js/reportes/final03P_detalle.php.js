var avisa = false;

function EscribeMetodos(_linea, _resumen) {
    document.getElementById('metodo' + _linea).value = _resumen;
}

function EscribeObs(_linea, _resumen) {
    document.getElementById('tr_' + _linea).style.display = '';
    document.getElementById('obs' + _linea).innerHTML = 'Observaciones: ' + _resumen + '<br>&nbsp;';
}

function Aprobar() {
    if (!confirm('Aprobar informe?'))
        return;
    _Modificar(2);
}

function Eliminar() {
    if (!confirm('Desechar informe?'))
        return;
    _Modificar(4);
}

function Anular() {
    if (!avisa) {
        $('#obs').val('');
        avisa = true;
    }

    if (Mal(3, $('#obs').val())) {
        alert('Debe indicar la justificaci�n en observaciones');
        return;
    }

    if (!confirm('Anular informe?'))
        return;
    _Modificar(5);
}

function Guardar() {

    metodo = document.getElementsByName("metodo");
    metodoarray = new Array();
    for (i = 0; i < metodo.length; i++) {
        metodoarray.push(metodo[i].value);
    }

    metodos = document.getElementsByName("metodo1");
    metodosarray = new Array();
    for (i = 0; i < metodos.length; i++) {
        metodosarray.push(metodos[i].value);
    }

    var parametros = {
        '_AJAX': 2,
        'id': $('#id').val(),
        'metodo': metodoarray,
        'metodos': metodosarray
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    alert("Transaccion finalizada");
                    opener.location.reload();
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function _Modificar(_estado) {
    var parametros = {
        '_AJAX': 1,
        'id': $('#id').val(),
        'cliente': $('#cliente').val(),
        'obs': $('#obs').val(),
        'estado': _estado,
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            console.log(_response);
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    alert("Transaccion finalizada");
                    opener.location.reload();
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function ajusta(textArea) {
    alert(textArea);
    if (navigator.appName.indexOf("Microsoft Internet Explorer") == 0) {
        textArea.style.overflow = 'visible';
    }
    var diferencia = (textArea.offsetHeight - textArea.scrollHeight) * 2;
    textArea.rows = textArea.rows + diferencia;
    return;
}