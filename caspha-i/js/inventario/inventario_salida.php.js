$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		/*"sScrollX": "100%",*/
		"sScrollXInner": "110%",
		"bScrollCollapse": true,
		"bFilter": true,
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function datos(ID){
	if( Mal(1, $('#tipo').val()) ){
		OMEGA('Debe indicar el tipo de salida');
		return;
	}
	
	if( $('#tipo').val()=='T' ){ //TRASLADO
		if( Mal(1, $('#cant_tras').val()) || $('#cant_tras').val() == '0'){
			OMEGA('Debe indicar la cantidad a trasladar');
			return;
		}
		
		var maximo = 0;
		if( $('#tipo_tras').val()=='A' ) maximo =  parseFloat( $('#bodega2').val() );// DE AUXILIAR A CENTRAL
		else maximo =  parseFloat( $('#bodega1').val() );
		
		if( parseFloat($('#cant_tras').val()) > maximo ){
			OMEGA('Debe la cantidad m&aacute;xima trasladar es ' + maximo);
			return;
		}
	}else{ //SALIDAS
		var total = parseFloat( $('#cant_bod1').val() ) + parseFloat( $('#cant_bod2').val() );
		
		if(total < 0){
			OMEGA('Debe indicar la cantidad a rebajar');
			return;
		}
		
		var maximo =  parseFloat( $('#bodega1').val() );
		if( parseFloat($('#cant_bod1').val()) > maximo ){
			OMEGA('Debe la cantidad m&aacute;xima a rebajar de la bodega del laboratorio es ' + maximo);
			return;
		}
		
		var maximo =  parseFloat( $('#bodega2').val() );
		if( parseFloat($('#cant_bod2').val()) > maximo ){
			OMEGA('Debe la cantidad m&aacute;xima a rebajar de la bodega auxiliar es ' + maximo);
			return;
		}
	}
	
	if(!confirm('Ingresar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'id' : $('#id').val(),
		'tipo' : $('#tipo').val(),
		'producto' : ID,
		'tipo_tras' : $('#tipo_tras').val(),
		'cant_tras' : $('#cant_tras').val(),
		'cant_bod1' : $('#cant_bod1').val(),
		'cant_bod2' : $('#cant_bod2').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesión expirada [Err:0]');break;
				case '-1':alert('Error en el envío de parámetros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					DELTA('Transaccion finalizada');
					window.opener.location.reload();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Marca(_id, _codigo, _bodega1, _bodega2){
	$('#id').val(_id);
	$('#bodega1').val(_bodega1);
	$('#bodega2').val(_bodega2);
	//
	document.getElementById('tabla').style.display = '';
	document.getElementById('ref').innerHTML = '<strong>Codigo interno: </strong>' + _codigo;
}

function CambiaTipo(_tipo){
	if(_tipo=='T'){
		document.getElementById('tr_boton').style.display = '';
		document.getElementById('tr_translado').style.display = '';
		document.getElementById('tr_salida').style.display = 'none';
	}else if(_tipo != ''){
		document.getElementById('tr_boton').style.display = '';
		document.getElementById('tr_salida').style.display = '';
		document.getElementById('tr_translado').style.display = 'none';
	}else{
		document.getElementById('tr_boton').style.display = 'none';
		document.getElementById('tr_translado').style.display = 'none';
		document.getElementById('tr_salida').style.display = 'none';
	}
}

function Entero(_campo){
	_campo.value = _campo.value.replace(/,/g, '.');
}

/*function Entero(_campo){
	if(_campo.value == ''){
		_campo.value = '0';
		return; 
	}
	_INT(_campo);
}*/