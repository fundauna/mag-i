function DocumentosZip(_ARCHIVO, _MODULO){
	var ruta = "../seguridad/documentos.php?ARCHIVO=" + _ARCHIVO + "&MODULO=" + _MODULO + "&ACCESO=A5";
	window.open(ruta,"","width=100,height=100,scrollbars=yes,status=no");
	//window.showModalDialog(ruta, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}

function Atender(){
	if(!confirm('Marcar vencimiento como atendido?')) return;

	var parametros = {
		'_AJAX' : 1,
		'ES' : $('#ES').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			//$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					//$('#btn').disabled = false;
					break;
				case '1':
					opener.location.reload();
					//window.dialogArguments.location.reload();
					alert('Transaccion finalizada');
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}