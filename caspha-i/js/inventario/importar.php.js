function InventarioImportar() {
    if (document.getElementById('archivo').value == '') {
        alert('Debe seleccionar un archivo a importar');
        return;
    }

    if (document.getElementById('inventario3').checked == true) {
        document.getElementById("formulario").action = "../../caspha-i/shell/inventario/_importa_vencido.php";
    } else if (document.getElementById('inventario1').checked == true) {
        document.getElementById("formulario").action = "../../caspha-i/shell/inventario/_importa_general.php";
    } else if (document.getElementById('inventario2').checked == true) {
        document.getElementById("formulario").action = "../../caspha-i/shell/inventario/_importa_io.php";
    }

    formulario.submit();
}