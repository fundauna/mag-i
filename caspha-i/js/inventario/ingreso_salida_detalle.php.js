function cambia() {
    if (document.getElementById('tipo').value == '1') {
        $("#e1").show();
        $("#e2").show();
        $("#e3").show();
        $("#e4").show();
        $("#e5").show();
        $("#e6").show();
        $("#s1").hide();
    } else {
        $("#e1").hide();
        $("#e2").hide();
        $("#e3").hide();
        $("#e4").hide();
        $("#e5").hide();
        $("#e6").hide();
        $("#s1").show();
    }
}

function datos() {

    if ($('#tipo').val() == '') {
        OMEGA('Debe indicar el tipo de movimiento');
        return;
    }

    if (Mal(3, $('#codigo').val())) {
        OMEGA('Debe indicar el codigo');
        return;
    }

    if (Mal(3, $('#descripcion').val())) {
        OMEGA('Debe indicar la descripcion');
        return;
    }
    if ($('#tipo').val() == '1') {
        if (Mal(3, $('#orden').val())) {
            OMEGA('Debe indicar el numero de orden de compra');
            return;
        }

        if (Mal(3, $('#factura').val())) {
            OMEGA('Debe indicar el numero de factura');
            return;
        }
        if (Mal(1, $('#proveedor').val())) {
            OMEGA('Debe indicar el nombre del proveedor');
            return;
        }
        if (Mal(1, $('#costo').val())) {
            OMEGA('Debe indicar el costo');
            return;
        }
        if (Mal(1, $('#ingresobi').val())) {
            OMEGA('Debe indicar la fecha de ingreso BI');
            return;
        }
        if (Mal(1, $('#ingresolcc').val())) {
            OMEGA('Debe indicar la fecha de ingreso al LCC');
            return;
        }
    }
    if ($('#tipo').val() == '2') {
        if (Mal(1, $('#salida').val())) {
            OMEGA('Debe indicar la fecha de salida');
            return;
        }
    }
    if (Mal(1, $('#cantidad').val())) {
        OMEGA('Debe indicar la cantidad');
        return;
    }
    if (Mal(1, $('#unidad').val())) {
        OMEGA('Debe indicar la unidad');
        return;
    }

    var txt = 'Ingresar';
    if ($('#accion').val() == 'M') {
        txt = 'Modificar';
    }
    if (!confirm(txt + ' datos?'))
        return;

    var parametros = {
        '_AJAX': 1,
        'id': $('#id').val(),
        'accion': $('#accion').val(),
        'tipo': $('#tipo').val(),
        'codigo': $('#codigo').val(),
        'descripcion': $('#descripcion').val(),
        'orden': $('#orden').val(),
        'factura': $('#factura').val(),
        'proveedor': $('#proveedor').val(),
        'costo': $('#costo').val(),
        'ingresobi': $('#ingresobi').val(),
        'ingresolcc': $('#ingresolcc').val(),
        'salida': $('#salida').val(),
        'cantidad': $('#cantidad').val(),
        'unidad': $('#unidad').val(),
        'obs': $('#obs').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    break;
                case '1':
                    opener.location.reload();
                    DELTA('Transaccion finalizada');
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}