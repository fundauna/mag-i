function total(_valor) {
    if (document.getElementById('cantidadlcc').value != '' && document.getElementById('cantidadlcc').value != '') {
        _FLOAT(document.getElementById('cantidadlcc'));
        _FLOAT(document.getElementById('cantidadpavas'));
        document.getElementById('cantidadtotal').value = parseFloat(_FVAL(document.getElementById('cantidadlcc').value)) + parseFloat(_FVAL(document.getElementById('cantidadpavas').value));
        _FLOAT(document.getElementById('cantidadtotal'));
    }
    if (document.getElementById('cantidadtransito').value != '') {
        _FLOAT(document.getElementById('cantidadtransito'));
        document.getElementById('nivel').value = parseFloat(_FVAL(document.getElementById('cantidadtotal').value)) + parseFloat(_FVAL(document.getElementById('cantidadtransito').value));
        _FLOAT(document.getElementById('nivel'));
    }
    if (document.getElementById('consumoanual').value != '') {
        _FLOAT(document.getElementById('consumoanual'));
        document.getElementById('pedir').value = parseFloat(_FVAL(document.getElementById('consumoanual').value)) * 1.05;
        _FLOAT(document.getElementById('pedir'));
    }
    if (document.getElementById('seguridadmax').value != '') {
        _FLOAT(document.getElementById('seguridadmax'));
        document.getElementById('reorden').value = _FVAL(document.getElementById('consumoanual').value) * _FVAL(document.getElementById('seguridadmax').value) * 1.05;
        if (_FVAL(document.getElementById('nivel').value) > _FVAL(document.getElementById('reorden').value)) {
            document.getElementById('estado').value = 'Bien';
        } else {
            document.getElementById('estado').value = 'Solicitar';
        }
        _FLOAT(document.getElementById('reorden'));
    }
}

function datos() {

    if (Mal(3, $('#codigo').val())) {
        OMEGA('Debe indicar el codigo');
        return;
    }

    if (Mal(3, $('#descripcion').val())) {
        OMEGA('Debe indicar la descripcion');
        return;
    }

    if (Mal(3, $('#ubicacion').val())) {
        OMEGA('Debe indicar la ubicacion');
        return;
    }
    if (Mal(1, $('#cantidadlcc').val())) {
        OMEGA('Debe indicar la cantidad en LCC');
        return;
    }
    if (Mal(1, $('#cantidadpavas').val())) {
        OMEGA('Debe indicar la cantidad en Pavas');
        return;
    }
    if (Mal(1, $('#cantidadtransito').val())) {
        OMEGA('Debe indicar la cantidad en transito');
        return;
    }
    if (Mal(1, $('#costounitariod').val())) {
        OMEGA('Debe indicar el costo unitario en USD');
        return;
    }
    if (Mal(1, $('#costounitarioc').val())) {
        OMEGA('Debe indicar el costo unitario en CRC');
        return;
    }
    if (Mal(1, $('#costototal').val())) {
        OMEGA('Debe indicar el costo total');
        return;
    }
    if (Mal(1, $('#presentacion').val())) {
        OMEGA('Debe indicar la presentacion');
        return;
    }
    if (Mal(1, $('#parte').val())) {
        OMEGA('Debe indicar el numero de parte o catalogo');
        return;
    }
    if (Mal(1, $('#analisis').val())) {
        OMEGA('Debe indicar el analisis en el que se emplea');
        return;
    }
    if (Mal(1, $('#consumoanalisis').val())) {
        OMEGA('Debe indicar el consumo por analisis (unidades)');
        return;
    }
    if (Mal(1, $('#cantidadanalisis').val())) {
        OMEGA('Debe indicar la cantidad de analisis anual');
        return;
    }
    if (Mal(1, $('#consumoanual').val())) {
        OMEGA('Debe indicar el consumo anual (unidades)');
        return;
    }
    if (Mal(1, $('#medida').val())) {
        OMEGA('Debe indicar la unidad de medida');
        return;
    }
    if (Mal(1, $('#seguridadmax').val())) {
        OMEGA('Debe indicar el inventario de seguridad max');
        return;
    }
    if (Mal(1, $('#abc').val())) {
        OMEGA('Debe indicar la clasificacion ABC para control periodico');
        return;
    }
    if (Mal(1, $('#encargado').val())) {
        OMEGA('Debe indicar el encargado');
        return;
    }
    var txt = 'Ingresar';
    if ($('#accion').val() == 'M') {
        txt = 'Modificar';
    }
    if (!confirm(txt + ' datos?'))
        return;

    var parametros = {
        '_AJAX': 1,
        'id': $('#id').val(),
        'accion': $('#accion').val(),
        'codigo': $('#codigo').val(),
        'descripcion': $('#descripcion').val(),
        'ubicacion': $('#ubicacion').val(),
        'cantidadlcc': $('#cantidadlcc').val(),
        'cantidadpavas': $('#cantidadpavas').val(),
        'cantidadtotal': $('#cantidadtotal').val(),
        'cantidadtransito': $('#cantidadtransito').val(),
        'nivel': $('#nivel').val(),
        'costounitariod': $('#costounitariod').val(),
        'costounitarioc': $('#costounitarioc').val(),
        'costototal': $('#costototal').val(),
        'presentacion': $('#presentacion').val(),
        'parte': $('#parte').val(),
        'equipo': $('#equipo').val(),
        'ingreso': $('#ingreso').val(),
        'analisis': $('#analisis').val(),
        'consumoanalisis': $('#consumoanalisis').val(),
        'cantidadanalisis': $('#cantidadanalisis').val(),
        'consumoanual': $('#consumoanual').val(),
        'medida': $('#medida').val(),
        'seguridadmax': $('#seguridadmax').val(),
        'reorden': $('#reorden').val(),
        'vencimiento': $('#vencimiento').val(),
        'estado': $('#estado').val(),
        'pedir': $('#pedir').val(),
        'abc': $('#abc').val(),
        'obs': $('#obs').val(),
        'encargado': $('#encargado').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    break;
                case '1':
                    opener.location.reload();
                    DELTA('Transaccion finalizada');
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}