function CambiaCertificado(_valor){
	if(_valor == '1'){
		document.getElementById('tr1').style.display = '';
		document.getElementById('tr2').style.display = '';
	}else{
		document.getElementById('tr1').style.display = 'none';
		document.getElementById('tr2').style.display = 'none';
	}
}

function ValidaVencimiento(_valor){
	if(_valor == '0'){
		document.getElementById('tvencimiento1').style.display = 'none';
		document.getElementById('tvencimiento2').style.display = 'none';
	}else{
		document.getElementById('tvencimiento1').style.display = '';
		document.getElementById('tvencimiento2').style.display = '';
	}
}

function ProveedoresLista(){
	window.open(__SHELL__ + "?list=1","","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function ProveedorEscoge(cs, nombre){
	opener.document.getElementById('prov').value=cs;
	opener.document.getElementById('proveedor').value=nombre;
	window.close();
}

function Total(){
	_FLOAT(document.form.stock1);
	_FLOAT(document.form.stock2);
	_FLOAT(document.form.costo);

	var cantidad = parseFloat( $('#stock1').val() ) + parseFloat( $('#stock2').val() );
		 
	if( cantidad > 0 && _FVAL( $('#costo').val() ) > 0){
		document.getElementById('unidad').value = _FVAL(document.form.costo.value) / cantidad;
		_FLOAT(document.getElementById('unidad'));
	}
}

function ValidaPureza(txt){
	_INT(txt);
	if(txt.value < 0 || txt.value > 100){
		alert("% de pureza incorrecto");
		txt.value = "";
	}
}

function datos(){
	if( Mal(1, $('#codigo').val()) ){
		OMEGA('Debe indicar el c�digo interno');
		return;
	}
	
	if( Mal(1, $('#oc').val()) ){
		OMEGA('Debe indicar el n�mero de orden de compra');
		return;
	}
	
	if( Mal(1, $('#factura').val()) ){
		OMEGA('Debe indicar el n�mero de factura');
		return;
	}
	
	if( Mal(1, $('#prov').val()) ){
		OMEGA('Debe seleccionar el proveedor');
		return;
	}
	//
	var cantidad = parseFloat( $('#stock1').val() ) + parseFloat( $('#stock2').val() );
	if( cantidad < 1 ){
		OMEGA('Debe indicar la cantidad de entrada');
		return;
	}
	
	if( _FVAL(document.getElementById('costo').value) < 1 ){
		OMEGA('Debe indicar el costo');
		return;
	}
	//
	if( Mal(1, $('#vence').val()) ){
		OMEGA('Debe indicar si el inventario vence');
		return;
	}
	
	if( $('#vence').val() == '1' ){
		if( Mal(1, $('#vencimiento').val()) ){
			OMEGA('Debe indicar la fecha de vencimiento');
			return;
		}
		
		if( Mal(1, $('#aviso').val()) ){
			OMEGA('Debe indicar la fecha de aviso sobre vencimiento');
			return;
		}
		
		if( !_HOY( document.getElementById('vencimiento')) || !_HOY( document.getElementById('aviso')) ){
			OMEGA('Las fecha deben ser superior a hoy');
			return;
		}
		
		if( MalComparaFecha( document.getElementById('vencimiento'), document.getElementById('aviso') ) ){
			OMEGA('Las fecha de aviso no puede ser superior a la de vencimiento');
			return;
		}
	}
	//
	if( Mal(1, $('#certificado').val()) ){
		OMEGA('Debe indicar si el inventario es certificado');
		return;
	}
	
	if( $('#certificado').val() == '1' ){
		/*if( Mal(1, $('#pureza').val()) ){
			OMEGA('Debe indicar el % de pureza');
			return;
		}*/
		
		if( Mal(1, $('#archivo').val()) ){
			OMEGA('Debe adjuntar el certificado');
			return;
		}
	}

	if(!confirm('Ingresar datos?')) return;
	ALFA('Por favor espere....');
	document.form.submit();
}

function ValidaCodigo(_codigo){
	/*if( Mal(1, _codigo) ) return;
	
	var parametros = {
		'_AJAX' : 1,
		'codigo' : _codigo
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '-4':
					OMEGA('El c�digo interno digitado ya existe');
					$('#codigo').val('');
					break;
				case '1':
					BETA();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});*/
}