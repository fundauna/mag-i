function cambia() {
    if (document.getElementById('tipo').value == '1') {
        $("#e1").show();
        $("#e2").show();
        $("#e3").show();
        $("#e4").show();
        $("#e5").show();
        $("#e6").show();
        $("#s1").hide();
    } else {
        $("#e1").hide();
        $("#e2").hide();
        $("#e3").hide();
        $("#e4").hide();
        $("#e5").hide();
        $("#e6").hide();
        $("#s1").show();
    }
}

function datos() {

    if (Mal(3, $('#codigo').val())) {
        OMEGA('Debe indicar el codigo');
        return;
    }

    if (Mal(3, $('#material').val())) {
        OMEGA('Debe indicar un material');
        return;
    }

    if (Mal(1, $('#costo').val())) {
        OMEGA('Debe indicar un costo unitario');
        return;
    }
    if (Mal(1, $('#vencimiento').val())) {
        OMEGA('Debe indicar la fecha de vencimiento');
        return;
    }

    if (Mal(1, $('#cantidad').val())) {
        OMEGA('Debe indicar la cantidad');
        return;
    }
    if (Mal(1, $('#unidad').val())) {
        OMEGA('Debe indicar la unidad');
        return;
    }

    if (Mal(1, $('#total').val())) {
        OMEGA('Debe indicar el costo total');
        return;
    }

    var txt = 'Ingresar';
    if ($('#accion').val() == 'M') {
        txt = 'Modificar';
    }
    if (!confirm(txt + ' datos?'))
        return;

    var parametros = {
        '_AJAX': 1,
        'id': $('#id').val(),
        'accion': $('#accion').val(),
        'codigo': $('#codigo').val(),
        'material': $('#material').val(),
        'costo': $('#costo').val(),
        'vencimiento': $('#vencimiento').val(),
        'cantidad': $('#cantidad').val(),
        'unidad': $('#unidad').val(),
        'total': $('#total').val(),
        'obs': $('#obs').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    break;
                case '1':
                    opener.location.reload();
                    DELTA('Transaccion finalizada');
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}