function Agregar() {

    if (document.getElementById('nombre').value == '') {
        OMEGA('Digite un nombre para el nuevo campo');
        return;
    }

    var parametros = {
        '_AJAX': 1,
        'Agrega': 1,
        'nombre': $('#nombre').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;                    
                case '-2':
                    BETA();
                    alert('Adicional ya se encuentra registrado!');
                    break;
                case '0':
                    BETA();
                    alert('Error al incluir');
                    break;
                case '1':
                    BETA();
                    opener.location.reload();
                    window.close();
                    break;
                default:
                    alert(_response);
                    break;
            }
        }
    });
}