var valor = label = '', indice;
var apreto2 = false;

function LimpiaDisponibles(){
	document.getElementById('td_disponibles').innerHTML = '<select style="width:340px;" size="10" disabled></select>';
	document.getElementById('btn_agregar').disabled = true;
}

function LimpiaAsignados(){
	document.getElementById('td_asignados').innerHTML = '<select style="width:340px;" size="10" disabled></select>';
	document.getElementById('btn_eliminar').disabled = true;
}

function habilita(bool){
	document.getElementById('btn_buscar1').disabled = !bool;
	document.getElementById('btn_buscar2').disabled = !bool;
	document.getElementById('btn_agregar').disabled = !bool;
	document.getElementById('btn_eliminar').disabled = !bool;
}

function ProductoBuscarTipos(){
	var parametros = {
		'_AJAX' : 1,
		'ProductoBuscarTipo' : 1,
		'tipo' : $('#tipo').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			habilita(false);
			ALFA('Buscando....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				default:
					BETA();
					habilita(true);
					document.getElementById('td_disponibles').innerHTML = _response;
                                        ProductoBuscarAsignados();
					break;
			}
		}
	});
}

function ProductoBuscarAsignados(){
	var parametros = {
		'_AJAX' : 1,
		'ProductoBuscarAsignados' : 1,
		'analisis' : $('#analisis').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			habilita(false);
			ALFA('Buscando....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				default:
					BETA();
					habilita(true);
					apreto2 = true;
					document.getElementById('td_asignados').innerHTML = _response;
					break;
			}
		}
	});
}

function ProductoAgrega(){
	if(!apreto2){
		OMEGA('Debe presionar buscar en las hojas de c�lculo registradas');
		return;
	}
	
	var control = document.getElementById('productos');
        var control2 = document.getElementById('analisis');
	if(control.selectedIndex == -1){
		OMEGA('Seleccione una opci�n del inventario disponible');
		return;
	}
	
	if( Mal(1, document.getElementById('cantidad').value) ){
		OMEGA('Debe indicar la cantidad');
		return;
	}
	
	valor = control.options[control.selectedIndex].value;
	label = control.options[control.selectedIndex].text;
	
	if(document.getElementById('td_asignados').innerHTML == '<select style="width:340px;" size="10" disabled></select>' ){
		OMEGA('Debe buscar el inventario asignado de ' + document.getElementById('analisis').options[control.selectedIndex].text );
		return;
	}
	
	if(!confirm("Agregar " + label + " a " + document.getElementById('analisis').options[control2.selectedIndex].text + "?" )) return;
	habilita(false);
	
	var parametros = {
		'_AJAX' : 1,
		'ProductoAgrega' : 1,
		'analisis' : $('#analisis').val(),
		'producto' : valor,
		'cantidad' : _FVAL($('#cantidad').val()),
		'obs' : $('#obs').val(),
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			habilita(false);
			ALFA('Modificando....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					BETA();
					habilita(true);
					break;
				case '1':
					BETA();
					habilita(true);
					/**/
					var oOption = document.createElement('OPTION');
					oOption.value = valor;
					oOption.text = document.getElementById('cantidad').value + 'x' + label;
					var combo = document.getElementById('asignados');
					combo.options[combo.options.length] = new Option(oOption.text, oOption.value);
					$('#cantidad').val('');
					$('#obs').val('');
					/**/
					break;
				default:alert(_response);break;
			}
		}
	});
}

function ProductoElimina(){
	var control = document.getElementById('asignados');
	if(control.selectedIndex == -1){
		OMEGA('Seleccione la l�nea a eliminar');
		return;
	}
	
	habilita(false);
	indice = control.selectedIndex;
	valor = control.options[control.selectedIndex].value;
	
	var parametros = {
		'_AJAX' : 1,
		'ProductoElimina' : 1,
		'analisis' : $('#analisis').val(),
		'producto' : valor
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			habilita(false);
			ALFA('Modificando....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '1':
					BETA();
					habilita(true);
					/**/
					document.getElementById('asignados').remove(indice);
					$('#cantidad').val('');
					habilita(true);
					/**/
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}