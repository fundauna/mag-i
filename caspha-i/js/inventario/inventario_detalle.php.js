function CambiaTipo(_valor) {
    if (_valor == '') {
        document.getElementById('pantalla_opcionales').innerHTML = '';
        return;
    }

    //CARGA PLANTILLA
    var parametros = {
        '_AJAX': 3,
        'familia': _valor
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {

            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '':
                    OMEGA('Error transaccional');
                    break;
                default:
                    BETA();
                    document.getElementById('pantalla_opcionales').innerHTML = _response;
                    break;
            }
        }
    });
}

function CambiaEs(_valor) {
    if (_valor == '0') {
        $('#minimo1').val('-1');
        $('#minimo2').val('-1');
    } else {
        $('#minimo1').val('');
        $('#minimo2').val('');
    }
}

function ValidaEs() {
    if (document.getElementById('es').value == '0') {
        if (parseFloat(document.getElementById('minimo1').value) != -1) {
            $('#minimo1').val('-1');
            OMEGA('El m�nimo de un item que no registra E/S debe ser -1');
            return;
        }
        if (parseFloat(document.getElementById('minimo2').value) != -1) {
            $('#minimo2').val('-1');
            OMEGA('El m�nimo de un item que no registra E/S debe ser -1');
            return;
        }
    }
}

function Blank(_campo) {
    if (_campo.value == '')
        _campo.value = '.';
}

idcat_datos = [];
valorinv_datos = [];

function dataAdicional() {
    idcat_datos = [];
    valorinv_datos = [];
    valorinv = document.getElementsByName('valorinv[]');
    idcat = document.getElementsByName('idcat[]');
    for (i = 0; i < valorinv.length; i++) {
        if (valorinv[i] != "") {
            idcat_datos.push(idcat[i].value);
            valorinv_datos.push(valorinv[i].value);
        }
    }

}


function datos() {
    if (Mal(1, $('#tipo').val())) {
        OMEGA('Debe escoger el tipo');
        return;
    }

    if (Mal(3, $('#codigo').val())) {
        OMEGA('Debe indicar el codigo');
        return;
    }

    if (Mal(2, $('#nombre').val())) {
        OMEGA('Debe indicar el nombre');
        return;
    }

    if (Mal(1, $('#presentacion').val())) {
        OMEGA('Debe escoger la presentaci�n');
        return;
    }

    $('#minimo1').val($('#minimo1').val().replace(',', '.'));
    $('#minimo2').val($('#minimo2').val().replace(',', '.'));
    if (Mal(1, $('#minimo1').val())) {
        OMEGA('Debe indicar las cantidades m�nimas');
        return;
    }

    $('#stock1').val($('#stock1').val().replace(',', '.'));
    $('#stock2').val($('#stock2').val().replace(',', '.'));
    if (Mal(1, $('#stock1').val())) {
        OMEGA('Debe indicar el stock en la bodega del laboratorio');
        return;
    }

    if (Mal(1, $('#ubicacion1').val())) {
        OMEGA('Debe indicar la ubicaci�n en la bodega del laboratorio');
        return;
    }
    //VALIDACION CAMPOS OPCIONALES
    dataAdicional();
    var vector = document.getElementsByName('adicional');
    var str = '';
    for (i = 0; i < vector.length; i++) {
        if (Mal(1, vector[i].value)) {
            OMEGA('Debe indicar todos los datos adicionales ');
            return;
        }
        str += ';' + vector[i].id + ':' + vector[i].value.replace(/:/g, '');
    }

    if (!confirm('Modificar datos?'))
        return;

    id052 = new Array();
    ids = $("[name='idcampo[]']");
    for (i = 0; i < ids.length; i++) {
        id052.push(ids[i].value);
    }
    var parametros = {
        '_AJAX': 1,
        'id': $('#id').val(),
        'accion': $('#accion').val(),
        'tipo': $('#tipo').val(),
        'codigo': $('#codigo').val(),
        'orden': $('#orden').val(),
        'factura': $('#factura').val(),
        'proveedor': $('#proveedor').val(),
        'acta': $('#acta').val(),
        'nombre': $('#nombre').val(),
        'marca': $('#marca').val(),
        'marcacomercial': $('#marcacomercial').val(),
        'presentacion': $('#presentacion').val(),
        'es': $('#es').val(),
        'minimo1': $('#minimo1').val(),
        'minimo2': $('#minimo2').val(),
        'stock1': $('#stock1').val(),
        'stock2': $('#stock2').val(),
        'ubicacion1': $('#ubicacion1').val(),
        'ubicacion2': $('#ubicacion2').val(),
        'adicional': str,
        'ids052': id052,
        'valorinv': valorinv_datos,
        'idcat': idcat_datos
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            console.log(_response);
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    break;
                case '1':
                    opener.location.reload();
                    DELTA('Transaccion finalizada');
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function verificaCatalogo(valor) {
    if ($('#tipo').val() == '') {
        OMEGA('Debe seleccionar el tipo de inventario!');
        return;
    }
    var parametros = {
        '_AJAX': 2,
        'id': valor,
        'familia': $("#tipo").val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            if (_response == '0') {
                OMEGA('Nuevo Registro: ' + valor);

            } else {
                window.location.href = "../inventario/inventario_detalle.php?acc=M&ID=" + _response;
            }

        }
    });
}

function ProveeedorLista() {
    window.open(__SHELL__ + "?list=1", "", "width=400,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function ProveedorEscoge(id, nombre, naturaleza) {
    opener.document.getElementById('proveedor').value = nombre;
    window.close();
}