$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		/*"sScrollX": "100%",*/
		"sScrollXInner": "110%",
		"bScrollCollapse": true,
		"bFilter": true,
		"aaSorting": [[ 0, "desc" ]],
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function InventarioAgrega(){
	window.open("material_vencido_detalle.php?acc=I","","width=550,height=450,scrollbars=yes,status=no");
	//window.showModalDialog("inventario_detalle.php?acc=I", this.window, "dialogWidth:600px;dialogHeight:450px;status:no;");
}

function InventarioBuscar(btn){	
	btn.disabled = true;
	btn.form.submit();
}

function InventarioModifica(_ID){
	window.open("material_vencido_detalle.php?acc=M&ID="+_ID,"","width=600,height=450,scrollbars=yes,status=no");
	//window.showModalDialog("inventario_detalle.php?acc=M&ID="+_ID, this.window, "dialogWidth:600px;dialogHeight:450px;status:no;");
}

function InventarioElimina(_ID){
	if(!confirm('Eliminar inventario?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'id' : _ID,
		'accion' : 'D'
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
                    console.log(_response);
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					GAMA('Transacci�n finalizada');
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function marca(valor){
	if(valor==1){
		document.getElementById('codigo').readOnly = false;
		document.getElementById('nombre').readOnly = true;
		document.getElementById('parte').readOnly = true;
                document.getElementById('estado2').disabled = true;
		document.getElementById('nombre').value = '';
		document.getElementById('parte').value = '';
                document.getElementById('estado').value = '1';
                document.getElementById('estado2').value = '';
	}if(valor==2){
		document.getElementById('nombre').readOnly = false;
		document.getElementById('codigo').readOnly = true;
		document.getElementById('parte').readOnly = true;
                document.getElementById('estado2').disabled = true;
		document.getElementById('codigo').value = '';
		document.getElementById('parte').value = '';
                document.getElementById('estado').value = '1';
                document.getElementById('estado2').value = '';
	}if(valor==3){
		document.getElementById('parte').readOnly = false;
		document.getElementById('codigo').readOnly = true;
		document.getElementById('nombre').readOnly = true;
                document.getElementById('estado2').disabled = true;                
		document.getElementById('codigo').value = '';
		document.getElementById('nombre').value = '';
                document.getElementById('estado').value = '1';
                document.getElementById('estado2').value = '';
	}if(valor==4){
		document.getElementById('estado2').disabled = false;
		document.getElementById('codigo').readOnly = true;
		document.getElementById('nombre').readOnly = true;
                document.getElementById('parte').readOnly = true;                
		document.getElementById('codigo').value = '';
		document.getElementById('nombre').value = '';
                document.getElementById('parte').value = '';
	}
}

function refrescaEstado(valor){
	document.getElementById('estado').value = valor;
}