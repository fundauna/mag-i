var _decimales = 4;
var promA = promB = res1 = res2 = res3 = res4 = res5 = res6 = 0;

function __lolo(){
	document.getElementById('masa').value = '1';
	document.getElementById('muestra').value = '7';
	document.getElementById('encontrado').value = '5';
	document.getElementById('yodo').value = '0.1';
	document.getElementById('consumido').value = '13.00';
	__calcula();
}

function __calcula(){
	res1 = document.getElementById('muestra').value*document.getElementById('encontrado').value/100;
	res2 = document.getElementById('yodo').value*document.getElementById('consumido').value*document.getElementById('factor').value/100;
	res3 = 111*(res1-res2)/res1;
	
	document.getElementById('res1').innerHTML = _RED2(res1, 4);
	document.getElementById('res2').innerHTML = _RED2(res2, 2);
	document.getElementById('res3').innerHTML = _RED2(res3, 2);
}

function datos(){	
	if($('#fechaA').val()==''){
		OMEGA('Debe indicar la fecha del an�lisis');
		return;
	}
	
	if($('#tipo_form').val()==''){
		OMEGA('Debe seleccionar el tipo de formulaci�n');
		return;
	}
	
	if($('#ingrediente').val()==''){
		OMEGA('Debe indicar el ingrediente activo');
		return;
	}
	
	if($('#fechaP').val()==''){
		OMEGA('Debe indicar la fecha de preparaci�n');
		return;
	}
	
	if($('#fechaD').val()==''){
		OMEGA('Debe indicar la fecha de comprobaci�n');
		return;
	}
	
	if($('#numreactivo').val()==''){
		OMEGA('Debe indicar el n�mero de reactivo');
		return;
	}
	
	if(!confirm('Ingresar ensayo?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'xanalizar' : $('#xanalizar').val(),
		'tipo' : $('#tipo').val(),
		'ingrediente' : $('#ingrediente').val(),
		'fechaA' : $('#fechaA').val(),
		'tipo_form' : $('#tipo_form').val(),
		'dosis' : $('#dosis').val(),
		'metodo' : $('#metodo').val(),
		'fechaP' : $('#fechaP').val(),
		'maria' : $('#maria').val(),
		'numreactivo' : $('#numreactivo').val(),
		'fechaD' : $('#fechaD').val(),
		'resultado' : $('#resultado').val(),
		'masa' : $('#masa').val(),
		'muestra' : $('#muestra').val(),
		'encontrado' : $('#encontrado').val(),
		'yodo' : $('#yodo').val(),
		'consumido' : $('#consumido').val(),
		'factor' : $('#factor').val(),
		'IC' : document.getElementById('res3').innerHTML,
		'obs' : $('#obs').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					opener.location.reload();
					alert("Transaccion finalizada");
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Redondear(txt){
	_RED(txt, _decimales);
	__calcula();
}