var masa_normal1 = masa_doble1 = masa_delta1 = masa_normal2 = masa_doble2 = masa_delta2 = volu_normal1 = volu_doble1 = volu_delta1 = volu_normal2 = volu_doble2 = volu_delta2 = 0;
var _IC = 0;
var simbolA = simbolB = simbolC = simbolD = incFC = 0;

function __lolo() {
    document.getElementById('balon1').selectedIndex = 3;
    document.getElementById('balon2').selectedIndex = 3;
    document.getElementById('rango').value = '20';
    document.getElementById('unidad').selectedIndex = 1;
    document.getElementById('repetibilidad').value = '0.000125';
    document.getElementById('resolucion').value = '0.0001';
    document.getElementById('inc_cer').value = '0.00017';
    document.getElementById('emp').value = '0.000093';
    document.getElementById('masa1').value = '0.0001';
    document.getElementById('cn1').value = '11';
    document.getElementById('dumbre1').value = '22';
    document.getElementById('delta1').value = '2';
    document.getElementById('certi1').value = '0.028';
    document.getElementById('masa2').value = '0.4956';
    document.getElementById('cn2').value = '33';
    document.getElementById('dumbre2').value = '66';
    document.getElementById('delta2').value = '3';
    document.getElementById('certi2').value = '0.029';
    __calcula();
}

function datos() {
    if ($('#fechaA').val() == '') {
        OMEGA('Debe indicar la fecha del an�lisis');
        return;
    }

    if ($('#rango').val() == '') {
        OMEGA('Debe indicar la concentraci�n declarada');
        return;
    }

    if ($('#unidad').val() == '') {
        OMEGA('Debe seleccionar la unidad');
        return;
    }

    if (!confirm('Ingresar ensayo?')) return;

    if (document.getElementById('unidad').value == '0') var lbl = 'm/m';
    else var lbl = 'm/v';

    var parametros = {
        '_AJAX': 1,
        'xanalizar': $('#xanalizar').val(),
        'tipo': $('#tipo').val(),
        'fechaA': $('#fechaA').val(),
        'rango': $('#rango').val(),
        'unidad': $('#unidad').val(),
        'balon1': $('#balon1').val(),
        'balon2': $('#balon2').val(),
        'pipetaD1': $('#pipetaD1').val(),
        'pipetaD2': $('#pipetaD2').val(),
        'pipetaD3': $('#pipetaD3').val(),
        'balonD1': $('#balonD1').val(),
        'balonD2': $('#balonD2').val(),
        'balonD3': $('#balonD3').val(),
        'repetibilidad': $('#repetibilidad').val(),
        'resolucion': $('#resolucion').val(),
        'inc_cer': $('#inc_cer').val(),
        'emp': $('#emp').val(),
        'masa1': $('#masa1').val(),
        'masa2': $('#masa2').val(),
        'cn1': $('#cn1').val(),
        'cn2': $('#cn2').val(),
        'dumbre1': $('#dumbre1').val(),
        'dumbre2': $('#dumbre2').val(),
        'delta1': $('#delta1').val(),
        'delta2': $('#delta2').val(),
        'certi1': $('#certi1').val(),
        'certi2': $('#certi2').val(),
        'prom_masa': document.getElementById('prom_masa').innerHTML,
        'prom_volu': document.getElementById('prom_volu').innerHTML,
        'final': document.getElementById('final').innerHTML,
        'obs': $('#obs').val(),
        'repetibilidad': $('#repetibilidad').val(),
        'resolucion': $('#resolucion').val(),
        'inc_cer': $('#inc_cer').val(),
        'emp': $('#emp').val()
    }

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    break;
                case '1':
                    opener.location.reload();
                    alert("Transaccion finalizada");
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function Redondear(txt) {
    _RED(txt, 6);
    __calcula();
}

function __calcula() {
    var joker = 0;
    if (document.getElementById('unidad').value == '') return;

    __factores();
    __combos();
    //SACA LOS DATOS DE LA BALANZA
    N4 = document.getElementById('repetibilidad').value / Math.sqrt(10);
    document.getElementById('rep_comp').innerHTML = _RED2(N4, 5);
    //
    N5 = document.getElementById('resolucion').value / Math.sqrt(12);
    document.getElementById('res_comp').innerHTML = _RED2(N5, 5);
    //
    N6 = document.getElementById('inc_cer').value / 2;
    document.getElementById('cer_comp').innerHTML = _RED2(N6, 6);
    //
    N7 = document.getElementById('emp').value / 2;
    document.getElementById('emp_comp').innerHTML = _RED2(N7, 5);
    //
    _IC = Math.sqrt(parseFloat(Math.pow(N4, 2)) + parseFloat(Math.pow(N5, 2)) + parseFloat(Math.pow(N6, 2)) + parseFloat(Math.pow(N7, 2)));
    document.getElementById('inc_balanza').innerHTML = _RED2(_IC, 5);
    //RESULTADOS PARCIALES

    pipetaD1 = document.getElementById('pipetaD1').value;
    pipetaD2 = document.getElementById('pipetaD2').value;
    pipetaD3 = document.getElementById('pipetaD3').value;

    pipetaD1 == '0' ? pipetaD1 = 0.5 : '';
    pipetaD2 == '0' ? pipetaD2 = 0.5 : '';
    pipetaD3 == '0' ? pipetaD3 = 0.5 : '';

    //masa_normal1 = (document.getElementById('cn1').value*document.getElementById('balon1').value*(document.getElementById('balonD1').value/pipetaD1)*(document.getElementById('balonD2').value/pipetaD2)*(document.getElementById('balonD3').value/pipetaD3))/(10000*document.getElementById('masa1').value);
    var sumaDiv = (document.getElementById('balonD1').value / pipetaD1) * (document.getElementById('balonD2').value / pipetaD2) * (document.getElementById('balonD3').value / pipetaD3)


    masa_normal1 = (document.getElementById('cn1').value * document.getElementById('balon1').value) * sumaDiv / (10000 * document.getElementById('masa1').value);
    masa_normal2 = (document.getElementById('cn2').value * document.getElementById('balon2').value * (document.getElementById('balonD1').value / pipetaD1) * (document.getElementById('balonD2').value / pipetaD2) * (document.getElementById('balonD3').value / pipetaD3)) / (10000 * document.getElementById('masa2').value);
    volu_normal1 = masa_normal1 * document.getElementById('delta1').value;
    volu_normal2 = masa_normal2 * document.getElementById('delta2').value;


    //masa_doble1 = masa_normal1*simbolD;
    masa_doble1 = masa_normal1 * simbolD;
    masa_doble2 = masa_normal2 * simbolD;
    volu_doble1 = volu_normal1 * simbolD;
    volu_doble2 = volu_normal2 * simbolD;

    /*var dom2;
    var dov1;
    var dov2;*/



    if ($('#base').val() == 'boro') __BORO_CALCIO(6);//listo**15-11
    else if ($('#base').val() == 'calcio') __OXIDOS(6);//**15-11
    else if ($('#base').val() == 'cobre') __COBRE(6);//listo**15-11
   // else if ($('#base').val() == "f" + "\xf3" + "sforo") __FOSFORO(6);//15/11/2021
    else if ($('#base').val() == "fosforo") __FOSFORO(6);//15/11/2021
    else if ($('#base').val() == 'hierro') __COBRE(6);//listo**15-11
    else if ($('#base').val() == 'magnesio') __OXIDOS(6);//**15-11
    else if ($('#base').val() == 'manganeso') __COBRE(6);//listo**15-11
    else if ($('#base').val() == 'potasio') __OXIDOS(6);//***15-11
    else if ($('#base').val() == 'zinc') __BORO_CALCIO(6);//**15-11




    //PROMEDIO
    if ($('#base').val() == 'boro' || $('#base').val() == 'cobre' || $('#base').val() == 'hierro' || $('#base').val() == 'manganeso' || $('#base').val() == 'zinc') {

        var prom_masa = (masa_normal1 + masa_normal2) / 2;
        var prom_volu = (volu_normal1 + volu_normal2) / 2;
    } else {
        var prom_masa = (masa_doble1 + masa_doble2) / 2;
        var prom_volu = (volu_doble1 + volu_doble2) / 2;
    }


    //DESVIACIONES ESTANDAR
    var fVarianceA = 0;

    if (document.getElementById('unidad').value == '0') {
        //if( $('#base').val()=='boro' || $('#base').val()=='cobre' || $('#base').val()=='f�sforo' || $('#base').val()=='hierro' || $('#base').val()=='manganeso' || $('#base').val()=='zinc'){
        //fVarianceA += parseFloat(Math.pow(volu_normal1 - prom_volu, 2));
        fVarianceA += parseFloat(Math.pow(masa_normal1 - prom_masa, 2));
        fVarianceA += parseFloat(Math.pow(masa_normal2 - prom_masa, 2));

    } else {
        fVarianceA += parseFloat(Math.pow(volu_doble1 - prom_volu, 2));
        fVarianceA += parseFloat(Math.pow(volu_doble2 - prom_volu, 2));

    }


    var desv = Math.sqrt(fVarianceA) / Math.sqrt(1);
    //DE
    var DE = desv / Math.sqrt(2);
    //FINAL
    var inCalcu1 = masa_delta1;
    var inCalcu2 = masa_delta2;
    var inCalcu1V = volu_delta1;
    var inCalcu2V = volu_delta2;

    var mayor = Math.max(inCalcu1, inCalcu2);
    var mayorV = Math.max(inCalcu1V, inCalcu2V);

    //valida que sea solo oxidos
    if ($('#base').val() == 'calcio' || $('#base').val() == 'magnesio' || $('#base').val() == 'potasio' /*||  $('#base').val() == "f"+"\xf3"+"sforo"*/) {
        var inCalcu1O = dom1;
        var inCalcu20 = dom2;
        var inCalcu1V0 = dov1;
        var inCalcu2V0 = dov2;

        var mayorO = Math.max(inCalcu1O, inCalcu20);
        var mayorVO = Math.max(inCalcu1V0, inCalcu2V0);


        if (document.getElementById('unidad').value == '0') {//m/m
            var final = Math.sqrt(Math.pow(mayor / Math.sqrt(2), 2) + Math.pow(DE, 2));
            var finalOxidos = Math.sqrt(Math.pow(mayorO / Math.sqrt(2), 2) + Math.pow(DE, 2));
        } else { //m/v
            var final = Math.sqrt(Math.pow(mayorV / Math.sqrt(2), 2) + Math.pow(DE, 2));
            var finalOxidos = Math.sqrt(Math.pow(mayorVO / Math.sqrt(2), 2) + Math.pow(DE, 2));
            ///aqui se debe revisar el calculo para la final
        }

    } else {
        if (document.getElementById('unidad').value == '0') {//m/m
            var final = Math.sqrt(Math.pow(mayor / Math.sqrt(2), 2) + Math.pow(DE, 2));
        } else { //m/v
            var final = Math.sqrt(Math.pow(mayorV / Math.sqrt(2), 2) + Math.pow(DE, 2));
        }
    }


    if (final >= 1 && final < 10) {
        final = _RED2(final, 3);
        prom_masa = _RED2(prom_masa, 3);
        prom_volu = _RED2(prom_volu, 3);
        desv = _RED2(desv, 3);
        DE = _RED2(DE, 3);
    } else {
        if (final > 10) {
            final = Math.round(final);
            prom_masa = Math.round(prom_masa);
            prom_volu = Math.round(prom_volu);
            desv = Math.round(desv);
            DE = Math.round(DE);
        } else {
            final = _RED2(final, 3);
            prom_masa = _RED2(prom_masa, 3);
            prom_volu = _RED2(prom_volu, 3);
            desv = _RED2(desv, 3);
            DE = _RED2(DE, 3);
            decimals = Math.pow(10, 2);
            var intPart = Math.floor(final);
            var fracPart = (final % 1) * decimals;
            if (fracPart == '10' || fracPart == '20' || fracPart == '30' || fracPart == '40' || fracPart == '50' || fracPart == '60' || fracPart == '70' || fracPart == '80' || fracPart == '90')
                joker = '1';
        }
    }
    //estos se pintan en las primeras 2 comunas
    document.getElementById('masa_normal1').innerHTML = _RED2(masa_normal1, 3);
    document.getElementById('masa_normal2').innerHTML = _RED2(masa_normal2, 3);
    document.getElementById('volu_normal1').innerHTML = _RED2(volu_normal1, 3);
    document.getElementById('volu_normal2').innerHTML = _RED2(volu_normal2, 3);


    if ($('#base').val() == 'calcio' || $('#base').val() == 'magnesio' || $('#base').val() == 'potasio') {

        document.getElementById('masa_doble1').innerHTML = _RED2(masa_doble1, 3);
        document.getElementById('masa_doble2').innerHTML = _RED2(masa_doble2, 3);
        document.getElementById('volu_doble1').innerHTML = _RED2(volu_doble1, 3);
        document.getElementById('volu_doble2').innerHTML = _RED2(volu_doble2, 3);
        document.getElementById('delta_oxido_masa1').innerHTML = _RED2(dom1, 3);
        document.getElementById('delta_oxido_masa2').innerHTML = _RED2(dom2, 3);
        document.getElementById('delta_oxido_volumen1').innerHTML = _RED2(dov1, 3);
        document.getElementById('delta_oxido_volumen2').innerHTML = _RED2(dov2, 3);
        document.getElementById('finalOxidos').innerHTML = _RED2(finalOxidos, 3);

    }

    if (($('#base').val() == "fosforo")) {

        document.getElementById('prom_masa').innerHTML = _RED2(prommm, 3);
        document.getElementById('prom_volu').innerHTML = _RED2(prommv, 3);
        document.getElementById('delta_oxido_masa1').innerHTML = _RED2(deltammp1, 3);
        document.getElementById('delta_oxido_volumen1').innerHTML = _RED2(deltamvp1, 3);
        document.getElementById('delta_oxido_masa2').innerHTML = _RED2(deltammp2, 3);
        document.getElementById('delta_oxido_volumen2').innerHTML = _RED2(deltamvp2, 3);
        var fVarianceP = 0;
        if (document.getElementById('unidad').value == '0') {
            fVarianceP += parseFloat(Math.pow(masa_normal1 - prommm, 2));
            fVarianceP += parseFloat(Math.pow(masa_normal2 - prommm, 2));
        } else {
            fVarianceP += parseFloat(Math.pow(volu_normal1 - prommv, 2));
            fVarianceP += parseFloat(Math.pow(volu_normal2 - prommv, 2));
        }
//DE
        var desvP = Math.sqrt(fVarianceP) / Math.sqrt(1);
        //inc x de
        var DEP = desvP / Math.sqrt(2);
        document.getElementById('desvP').innerHTML = _RED2(desvP, 3);
        document.getElementById('DEP').innerHTML = _RED2(DEP, 3);
        var maymm = Math.max(deltammp1, deltammp2);
        var maymv = Math.max(deltamvp1, deltamvp2);
        if (document.getElementById('unidad').value == '0') {//m/m
            var final = Math.sqrt(Math.pow(maymm / Math.sqrt(2), 2) + Math.pow(DEP, 2));
        } else { //m/v
            var final = Math.sqrt(Math.pow(maymv / Math.sqrt(2), 2) + Math.pow(DEP, 2));
        }
        document.getElementById('final').innerHTML = _RED2(final, 2);
    }


    document.getElementById('masa_delta1').innerHTML = _RED2(masa_delta1, 3);
    document.getElementById('masa_delta2').innerHTML = _RED2(masa_delta2, 3);
    document.getElementById('volu_delta1').innerHTML = _RED2(volu_delta1, 3);
    document.getElementById('volu_delta2').innerHTML = _RED2(volu_delta2, 3);
    /*document.getElementById('desv').innerHTML = _RED2(desv, 5);
    document.getElementById('DE').innerHTML = _RED2(DE, 5);*/
    if (joker == '1') {
        document.getElementById('prom_masa').innerHTML = '' + prom_masa + '0';
        document.getElementById('prom_volu').innerHTML = '' + prom_volu + '0';
        document.getElementById('desv').innerHTML = '' + desv + '0';
        document.getElementById('DE').innerHTML = '' + DE + '0';
        document.getElementById('final').innerHTML = '' + final + '0';
    } else {
        document.getElementById('prom_masa').innerHTML = prom_masa;
        document.getElementById('prom_volu').innerHTML = prom_volu;
        document.getElementById('desv').innerHTML = desv;
        document.getElementById('DE').innerHTML = DE;
        document.getElementById('final').innerHTML = final;
    }
}

function __factores() {

    if ($('#base').val() == 'calcio') {
        simbolA = 'Ca a CaO:';
        simbolB = 40.078;
        simbolC = 56.078;
        simbolD = simbolC / simbolB;
        tmpB = simbolB;
        tmpC = 56.078;
        tmpD = 1.39922;
        incFC = 0.00017;
    } else if ($('#base').val() == 'magnesio') {
        simbolA = 'Mg a MgO:';
        simbolB = 24.306;
        simbolC = 40.306;
        simbolD = simbolC / simbolB;
        tmpB = simbolB;
        tmpC = 40.306;
        tmpD = 1.658274;
        incFC = 0.000090;
    } else if ($('#base').val() == 'potasio') {
        simbolA = 'K a K<sub>2</sub>O:';
        simbolB = 39.098;
        simbolC = 94.196;
        simbolD = simbolC / (2 * simbolB);
        tmpB = simbolB;
        tmpC = 94.196;
        tmpD = 1.204614;
        incFC = 0.000038;
        /*} else if ($('#base').val() == 'zinc') {
            simbolA = 'Zn a ZnO:';
            simbolB = 65.38;
            simbolC = 81.38;
            simbolD = simbolC / simbolB;
            tmpB = simbolB;
            tmpC = 81.38;
            tmpD = 1.24472;
            incFC = 0.00049;*/
        //Agregado BAAG revisar
        /*}else if($('#base').val()=='hierro'){
            simbolA = 'Fe a FeO<sub>2</sub>O<sub>3</sub>:';
            simbolB = 55.845;
            simbolC = 159.69;
            simbolD =  (simbolC/2)/simbolB;
            tmpB = simbolB;
            tmpC = 159.69;
            tmpD = 1.429761;
        }else if($('#base').val()=='cobre'){
            simbolA = 'Cu a CuO:';
            simbolB = 63.546;
            simbolC = 79.546;
            simbolD =  (simbolC/simbolB);
            tmpB = simbolB;
            tmpC = 79.546;
            tmpD = 1.251786;*/
    } else if (($('#base').val() == "fosforo")) {

        simbolA = 'P a P<sub>2</sub>O<sub>5</sub>:';
        simbolB = 30.974;
        simbolC = 141.948;
        simbolD = (simbolC / 2) / simbolB;
        tmpB = simbolB;
        tmpC = 141.948;
        tmpD = 2.291406;
        incFC = 0.000085;
        /*  }else if($('#base').val()=='manganeso'){
              simbolA = 'Mn a MnO<sub>2</sub>:';
              simbolB = 54.938;
              simbolC = 86.938;
              simbolD =  (simbolC/simbolB);
              tmpB = simbolB;
              tmpC = 86.938;
              tmpD = 1.582475;*/
        //hasta aqui se agregaron
    } else {

        simbolA = '';
        simbolB = '';
        simbolC = '';
        simbolD = 1;
        tmpB = '';
        tmpC = '';
        tmpD = '';
        incFC = '';
    }
    document.getElementById('simbolA').innerHTML = simbolA;
    document.getElementById('simbolB').innerHTML = tmpB;
    document.getElementById('simbolC').innerHTML = tmpC;
    document.getElementById('simbolD').innerHTML = tmpD;
    document.getElementById('incFC').innerHTML = incFC;
}

function __combos() {
    var control;
    //OBTIENE INC DEL PRIMER BALON
    control = document.getElementById('balon1');
    var inc = control.options[control.selectedIndex].getAttribute('inc');
    document.getElementById('inc_balon').innerHTML = inc;
    //OBTIENE INC DE LA PIPETA D1
    control = document.getElementById('pipetaD1');
    var inc = control.options[control.selectedIndex].getAttribute('inc');
    document.getElementById('inc_pipD1').innerHTML = inc;
    //OBTIENE INC DE LA PIPETA D2
    control = document.getElementById('pipetaD2');
    var inc = control.options[control.selectedIndex].getAttribute('inc');
    document.getElementById('inc_pipD2').innerHTML = inc;
    //OBTIENE INC DE LA PIPETA D3
    control = document.getElementById('pipetaD3');
    var inc = control.options[control.selectedIndex].getAttribute('inc');
    document.getElementById('inc_pipD3').innerHTML = inc;
    //OBTIENE INC DEL BALON D1
    control = document.getElementById('balonD1');
    var inc = control.options[control.selectedIndex].getAttribute('inc');
    document.getElementById('inc_balD1').innerHTML = inc;
    //OBTIENE INC DEL BALON D2
    control = document.getElementById('balonD2');
    var inc = control.options[control.selectedIndex].getAttribute('inc');
    document.getElementById('inc_balD2').innerHTML = inc;
    //OBTIENE INC DEL BALON D3
    control = document.getElementById('balonD3');
    var inc = control.options[control.selectedIndex].getAttribute('inc');
    document.getElementById('inc_balD3').innerHTML = inc;
}

function __FOSFORO(_temp) {

    pipetaD1 = document.getElementById('pipetaD1').value;
    pipetaD2 = document.getElementById('pipetaD2').value;
    pipetaD3 = document.getElementById('pipetaD3').value;

    pipetaD1 == '0' ? pipetaD1 = 0.5 : '';
    pipetaD2 == '0' ? pipetaD2 = 0.5 : '';
    pipetaD3 == '0' ? pipetaD3 = 0.5 : '';
    var tmpA1 = Math.pow((document.getElementById('dumbre1').value / document.getElementById('cn1').value), 2);
    var tmpA2 = Math.pow((document.getElementById('dumbre2').value / document.getElementById('cn2').value), 2);


    var tmp2 = Math.pow((document.getElementById('inc_balon').innerHTML / Math.sqrt(_temp)) / document.getElementById('balon1').value, 2);

    var tmp3 = Math.pow((document.getElementById('inc_pipD1').innerHTML / Math.sqrt(_temp)) / pipetaD1, 2);

    var tmp4 = Math.pow((document.getElementById('inc_balD1').innerHTML / Math.sqrt(_temp)) / document.getElementById('balonD1').value, 2);
    var tmp5 = Math.pow((document.getElementById('inc_pipD2').innerHTML / Math.sqrt(_temp)) / pipetaD2, 2);
    var tmp6 = Math.pow((document.getElementById('inc_balD2').innerHTML / Math.sqrt(_temp)) / document.getElementById('balonD2').value, 2);
    var tmp7 = Math.pow((document.getElementById('inc_pipD3').innerHTML / Math.sqrt(_temp)) / pipetaD3, 2);
    var tmp8 = Math.pow((document.getElementById('inc_balD3').innerHTML / Math.sqrt(_temp)) / document.getElementById('balonD3').value, 2);


    var tmpB1 = Math.pow(_IC / document.getElementById('masa1').value, 2);
    var tmpB2 = Math.pow(_IC / document.getElementById('masa2').value, 2);

    //
    var tmpZ1 = Math.pow((document.getElementById('certi1').value / 2) / document.getElementById('delta1').value, 2);
    var tmpZ2 = Math.pow((document.getElementById('certi2').value / 2) / document.getElementById('delta2').value, 2);
    //

    var sumaDiv = (document.getElementById('balonD1').value / pipetaD1) * (document.getElementById('balonD2').value / pipetaD2) * (document.getElementById('balonD3').value / pipetaD3)


    var men = ((document.getElementById('cn1').value * document.getElementById('balon1').value) * sumaDiv / (10000 * document.getElementById('masa1').value));
    var men2 = ((document.getElementById('cn2').value * document.getElementById('balon2').value) * sumaDiv / (10000 * document.getElementById('masa2').value));

    var vol = masa_normal1 * document.getElementById('delta1').value;
    var vol2 = masa_normal2 * document.getElementById('delta2').value;

    var res = (Math.sqrt((parseFloat(tmpA1) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB1))));


    prommm = (masa_normal1 + masa_normal2) / 2;
    prommv = (volu_normal1 + volu_normal2) / 2;
    deltammp1 = masa_normal1 * (Math.sqrt((parseFloat(tmpA1) + parseFloat(tmp2) + parseFloat(tmp3) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB1))));
    deltammp2 = masa_normal2 * (Math.sqrt((parseFloat(tmpA2) + parseFloat(tmp2) + parseFloat(tmp3) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB2))));
    deltamvp1 = volu_normal1 * (Math.sqrt((parseFloat(tmpA1) + parseFloat(tmp2) + parseFloat(tmp3) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB1) + parseFloat(tmpZ1))));
    deltamvp2 = volu_normal2 * (Math.sqrt((parseFloat(tmpA2) + parseFloat(tmp2) + parseFloat(tmp3) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB1) + parseFloat(tmpZ2))))


}

function __COBRE(_temp) {

    var incFC = document.getElementById('incFC').value;
    pipetaD1 = document.getElementById('pipetaD1').value;
    pipetaD2 = document.getElementById('pipetaD2').value;
    pipetaD3 = document.getElementById('pipetaD3').value;

    pipetaD1 == '0' ? pipetaD1 = 0.5 : '';
    pipetaD2 == '0' ? pipetaD2 = 0.5 : '';
    pipetaD3 == '0' ? pipetaD3 = 0.5 : '';
    var tmpA1 = Math.pow((document.getElementById('dumbre1').value / document.getElementById('cn1').value), 2);
    var tmpA2 = Math.pow((document.getElementById('dumbre2').value / document.getElementById('cn2').value), 2);


    var tmp2 = Math.pow((document.getElementById('inc_balon').innerHTML / Math.sqrt(_temp)) / document.getElementById('balon1').value, 2);

    var tmp3 = Math.pow((document.getElementById('inc_pipD1').innerHTML / Math.sqrt(_temp)) / pipetaD1, 2);

    var tmp4 = Math.pow((document.getElementById('inc_balD1').innerHTML / Math.sqrt(_temp)) / document.getElementById('balonD1').value, 2);
    var tmp5 = Math.pow((document.getElementById('inc_pipD2').innerHTML / Math.sqrt(_temp)) / pipetaD2, 2);
    var tmp6 = Math.pow((document.getElementById('inc_balD2').innerHTML / Math.sqrt(_temp)) / document.getElementById('balonD2').value, 2);
    var tmp7 = Math.pow((document.getElementById('inc_pipD3').innerHTML / Math.sqrt(_temp)) / pipetaD3, 2);
    var tmp8 = Math.pow((document.getElementById('inc_balD3').innerHTML / Math.sqrt(_temp)) / document.getElementById('balonD3').value, 2);


    var tmpB1 = Math.pow(_IC / document.getElementById('masa1').value, 2);
    var tmpB2 = Math.pow(_IC / document.getElementById('masa2').value, 2);

    //
    var tmpZ1 = Math.pow((document.getElementById('certi1').value / 2) / document.getElementById('delta1').value, 2);
    var tmpZ2 = Math.pow((document.getElementById('certi2').value / 2) / document.getElementById('delta2').value, 2);
    //

    var sumaDiv = (document.getElementById('balonD1').value / pipetaD1) * (document.getElementById('balonD2').value / pipetaD2) * (document.getElementById('balonD3').value / pipetaD3)

    var men = ((document.getElementById('cn1').value * document.getElementById('balon1').value) * sumaDiv / (10000 * document.getElementById('masa1').value));
    var men2 = ((document.getElementById('cn2').value * document.getElementById('balon2').value) * sumaDiv / (10000 * document.getElementById('masa2').value));

    var vol = masa_normal1 * document.getElementById('delta1').value;
    var vol2 = masa_normal2 * document.getElementById('delta2').value;

    var res = (Math.sqrt((parseFloat(tmpA1) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB1))));


    masa_delta1 = men * (Math.sqrt((parseFloat(tmpA1) + parseFloat(tmp2) + parseFloat(tmp3) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB1))));
    masa_delta2 = men2 * (Math.sqrt((parseFloat(tmpA2) + parseFloat(tmp2) + parseFloat(tmp3) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB2))));
    volu_delta1 = vol * (Math.sqrt((parseFloat(tmpA1) + parseFloat(tmp2) + parseFloat(tmp3) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB1) + parseFloat(tmpZ1))));
    volu_delta2 = vol2 * (Math.sqrt((parseFloat(tmpA2) + parseFloat(tmp2) + parseFloat(tmp3) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB2) + parseFloat(tmpZ2))));

}

function __BORO_CALCIO(_temp) {


    var incFC = document.getElementById('incFC').value;
    pipetaD1 = document.getElementById('pipetaD1').value;
    pipetaD2 = document.getElementById('pipetaD2').value;
    pipetaD3 = document.getElementById('pipetaD3').value;

    pipetaD1 == '0' ? pipetaD1 = 0.5 : '';
    pipetaD2 == '0' ? pipetaD2 = 0.5 : '';
    pipetaD3 == '0' ? pipetaD3 = 0.5 : '';
    var tmpA1 = Math.pow((document.getElementById('dumbre1').value / document.getElementById('cn1').value), 2);
    var tmpA2 = Math.pow((document.getElementById('dumbre2').value / document.getElementById('cn2').value), 2);


    var tmp2 = Math.pow((document.getElementById('inc_balon').innerHTML / Math.sqrt(_temp)) / document.getElementById('balon1').value, 2);

    var tmp3 = Math.pow((document.getElementById('inc_pipD1').innerHTML / Math.sqrt(_temp)) / pipetaD1, 2);

    var tmp4 = Math.pow((document.getElementById('inc_balD1').innerHTML / Math.sqrt(_temp)) / document.getElementById('balonD1').value, 2);
    var tmp5 = Math.pow((document.getElementById('inc_pipD2').innerHTML / Math.sqrt(_temp)) / pipetaD2, 2);
    var tmp6 = Math.pow((document.getElementById('inc_balD2').innerHTML / Math.sqrt(_temp)) / document.getElementById('balonD2').value, 2);
    var tmp7 = Math.pow((document.getElementById('inc_pipD3').innerHTML / Math.sqrt(_temp)) / pipetaD3, 2);
    var tmp8 = Math.pow((document.getElementById('inc_balD3').innerHTML / Math.sqrt(_temp)) / document.getElementById('balonD3').value, 2);


    var tmpB1 = Math.pow(_IC / document.getElementById('masa1').value, 2);
    var tmpB2 = Math.pow(_IC / document.getElementById('masa2').value, 2);

    //
    var tmpZ1 = Math.pow((document.getElementById('certi1').value / 2) / document.getElementById('delta1').value, 2);
    var tmpZ2 = Math.pow((document.getElementById('certi2').value / 2) / document.getElementById('delta2').value, 2);
    //

    var sumaDiv = (document.getElementById('balonD1').value / pipetaD1) * (document.getElementById('balonD2').value / pipetaD2) * (document.getElementById('balonD3').value / pipetaD3)

    var men = ((document.getElementById('cn1').value * document.getElementById('balon1').value) * sumaDiv / (10000 * document.getElementById('masa1').value));
    var men2 = ((document.getElementById('cn2').value * document.getElementById('balon2').value) * sumaDiv / (10000 * document.getElementById('masa2').value));

    var vol = masa_normal1 * document.getElementById('delta1').value;
    var vol2 = masa_normal2 * document.getElementById('delta2').value;

    var res = (Math.sqrt((parseFloat(tmpA1) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB1))));

    masa_delta1 = men * (Math.sqrt((parseFloat(tmpA1) + parseFloat(tmp2) + parseFloat(tmp3) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB1))));
    masa_delta2 = men2 * (Math.sqrt((parseFloat(tmpA2) + parseFloat(tmp2) + parseFloat(tmp3) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB2))));
    volu_delta1 = vol * (Math.sqrt((parseFloat(tmpA1) + parseFloat(tmp2) + parseFloat(tmp3) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB1) + parseFloat(tmpZ1))));
    volu_delta2 = vol2 * (Math.sqrt((parseFloat(tmpA2) + parseFloat(tmp2) + parseFloat(tmp3) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB2) + parseFloat(tmpZ2))));

}

function __OXIDOS(_temp) {

    //var incFC = document.getElementById('incFC').value;
    pipetaD1 = document.getElementById('pipetaD1').value;
    pipetaD2 = document.getElementById('pipetaD2').value;
    pipetaD3 = document.getElementById('pipetaD3').value;

    pipetaD1 == '0' ? pipetaD1 = 0.5 : '';
    pipetaD2 == '0' ? pipetaD2 = 0.5 : '';
    pipetaD3 == '0' ? pipetaD3 = 0.5 : '';
    var tmpA1 = Math.pow((document.getElementById('dumbre1').value / document.getElementById('cn1').value), 2);
    var tmpA2 = Math.pow((document.getElementById('dumbre2').value / document.getElementById('cn2').value), 2);


    var tmp2 = Math.pow((document.getElementById('inc_balon').innerHTML / Math.sqrt(_temp)) / document.getElementById('balon1').value, 2);

    var tmp3 = Math.pow((document.getElementById('inc_pipD1').innerHTML / Math.sqrt(_temp)) / pipetaD1, 2);

    var tmp4 = Math.pow((document.getElementById('inc_balD1').innerHTML / Math.sqrt(_temp)) / document.getElementById('balonD1').value, 2);
    var tmp5 = Math.pow((document.getElementById('inc_pipD2').innerHTML / Math.sqrt(_temp)) / pipetaD2, 2);
    var tmp6 = Math.pow((document.getElementById('inc_balD2').innerHTML / Math.sqrt(_temp)) / document.getElementById('balonD2').value, 2);
    var tmp7 = Math.pow((document.getElementById('inc_pipD3').innerHTML / Math.sqrt(_temp)) / pipetaD3, 2);
    var tmp8 = Math.pow((document.getElementById('inc_balD3').innerHTML / Math.sqrt(_temp)) / document.getElementById('balonD3').value, 2);


    var tmpB1 = Math.pow(_IC / document.getElementById('masa1').value, 2);
    var tmpB2 = Math.pow(_IC / document.getElementById('masa2').value, 2);

    //
    var tmpZ1 = Math.pow((document.getElementById('certi1').value / 2) / document.getElementById('delta1').value, 2);
    var tmpZ2 = Math.pow((document.getElementById('certi2').value / 2) / document.getElementById('delta2').value, 2);
    //

    var sumaDiv = (document.getElementById('balonD1').value / pipetaD1) * (document.getElementById('balonD2').value / pipetaD2) * (document.getElementById('balonD3').value / pipetaD3)

    var men = ((document.getElementById('cn1').value * document.getElementById('balon1').value) * sumaDiv / (10000 * document.getElementById('masa1').value));
    var men2 = ((document.getElementById('cn2').value * document.getElementById('balon2').value) * sumaDiv / (10000 * document.getElementById('masa2').value));

    var vol = masa_normal1 * document.getElementById('delta1').value;
    var vol2 = masa_normal2 * document.getElementById('delta2').value;

    var res = (Math.sqrt((parseFloat(tmpA1) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB1))));

    masa_delta1 = men * (Math.sqrt((parseFloat(tmpA1) + parseFloat(tmp2) + parseFloat(tmp3) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB1))));
    masa_delta2 = men2 * (Math.sqrt((parseFloat(tmpA2) + parseFloat(tmp2) + parseFloat(tmp3) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB2))));
    volu_delta1 = vol * (Math.sqrt((parseFloat(tmpA1) + parseFloat(tmp2) + parseFloat(tmp3) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB1) + parseFloat(tmpZ1))));
    volu_delta2 = vol2 * (Math.sqrt((parseFloat(tmpA2) + parseFloat(tmp2) + parseFloat(tmp3) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB2) + parseFloat(tmpZ2))));

//calculo de las deltas de oxidos
    dom1 = masa_doble1 * (Math.sqrt((incFC / tmpD) * (incFC / tmpD) + (parseFloat(tmpA1) + parseFloat(tmp2) + parseFloat(tmp3) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB1))));
    dom2 = masa_doble2 * (Math.sqrt((incFC / tmpD) * (incFC / tmpD) + (parseFloat(tmpA2) + parseFloat(tmp2) + parseFloat(tmp3) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB2))));
    dov1 = volu_doble1 * (Math.sqrt((incFC / tmpD) * (incFC / tmpD) + (parseFloat(tmpA1) + parseFloat(tmp2) + parseFloat(tmp3) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB1) + parseFloat(tmpZ1))));
    dov2 = volu_doble2 * (Math.sqrt((incFC / tmpD) * (incFC / tmpD) + (parseFloat(tmpA2) + parseFloat(tmp2) + parseFloat(tmp3) + parseFloat(tmp4) + parseFloat(tmp5) + parseFloat(tmp6) + parseFloat(tmp7) + parseFloat(tmp8) + parseFloat(tmpB2) + parseFloat(tmpZ2))));


}