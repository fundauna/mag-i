function __lolo() {
    document.getElementById('impureza').selectedIndex = 9;
    $('#densidad').val(1.3601);
    document.getElementById('unidad').selectedIndex = 2;
    $('#rango').val(72);
    ////////////////////////////////////////////////////
    $('#metodo').val("LCC-P-0024");
    document.getElementById('validado').selectedIndex = 2;
    document.getElementById('croma').selectedIndex = 5;
    $('#tiempo1').val("7.34");
    $('#tiempo2').val("7.34");
    $('#inyeccion').val("1");
    $('#disolvente1').val("Acetona:tolueno 40:60");
    $('#disolvente2').val("Tolueno");
    $('#estandar').val("Hexachlorobenzene");
    $('#orgen1').val("13-423");
    document.getElementById('certificado1').selectedIndex = 2;
    $('#pureza1').val(99.9);
    document.getElementById('certificado2').selectedIndex = 1;
    document.getElementById('certificado3').selectedIndex = 1;
    ////////////////////////////////////////////////////
    document.getElementById('tipo_c').selectedIndex = 1;    
    ////////////////////////////////////////////////////
    document.getElementById('tipo_i').selectedIndex = 3;
    $('#temp_i').val(300);
    document.getElementById('modo_i').selectedIndex = 3;
    ////////////////////////////////////////////////////
    document.getElementById('gas1').selectedIndex = 3;
    $('#flujo1').val(1.10);
    ////////////////////////////////////////////////////
    document.getElementById('tipo_d').selectedIndex = 4;
    ////////////////////////////////////////////////////
    document.getElementById('modo').selectedIndex = 1;
    $('#temp1_g').val(150);
    $('#temp2_g').val(230);
    $('#temp3_g').val('282,284,286');
    ////////////////////////////////////////////////////
    
    
    $('#masa1').val(12.64);
    $('#aforado11').val(50);
    //$('#alicuota11').val(1);
    //$('#aforado12').val(10);
    //$('#masa2').val(9.95);	
    //$('#aforado21').val(25);
    //$('#alicuota21').val(0.50);
    //$('#aforado22').val(100);
    $('#alicuota31').val(63);
    $('#aforado31').val(25);
    $('#alicuota32').val(1);
    $('#aforado32').val(10);
    $('#alicuota41').val(88);
    $('#aforado41').val(25);
    $('#alicuota42').val(1);
    $('#aforado42').val(10);
    $('#alicuota51').val(13);
    $('#aforado51').val(25);
    $('#alicuota61').val(88);
    $('#aforado61').val(25);
    $('#alicuota62').val(2.5);
    $('#aforado62').val(10);
    $('#areaimp3').val(190563);
    $('#areaimp4').val(270238);
    $('#areaimp5').val(375770);
    $('#areaimp6').val(689286);
    /*$('#areaint3').val(5292308);
     $('#areaint4').val(5351024);
     $('#areaint5').val(5492860);
     $('#areaint6').val(5753582);*/
    $('#muestramasa1').val(257.1);
    $('#muestraaforado11').val(50);
    /*$('#muestraalicuota11').val(4);
     $('#muestraaforado12').val(2);
     $('#muestramasa2').val(10);
     $('#muestraaforado21').val(25);
     $('#muestraalicuota21').val(0.5);
     $('#muestraaforado22').val(100);
     $('#muestramasa3').val(2015);
     $('#muestraaforado31').val(10);
     $('#muestraalicuota31').val(4);
     $('#muestraaforado32').val(10);
     $('#muestramasa4').val(10);
     $('#muestraaforado41').val(25);
     $('#muestraalicuota41').val(0.50);
     $('#muestraaforado42').val(100);*/
    $('#areaA1').val(288014);
    $('#areaB1').val(307503);
    $('#areaA2').val(5579741);
    $('#areaB2').val(5400281);
    $('#areaA3').val(1541561);
    $('#areaB3').val(1278389);
    $('#areaA4').val(5468710);
    $('#areaB4').val(4522100);
    $('#CCRmuestramasa').val(255.9);
    $('#CCRmuestradil1').val(50);
    /*$('#CCRmuestraali1').val(4);
     $('#CCRmuestradil2').val(2);
     $('#CCRinternomasa').val(10);
     $('#CCRinternodil1').val(25);
     $('#CCRinternoali1').val(0.5);
     $('#CCRinternodil2').val(100);*/
    $('#CCRenrimasa').val(12.64);
    $('#CCRenridil1').val(50);
    $('#CCRenriali1').val(1);
    $('#CCRenridil2').val(25);
    $('#CCRenriali2').val(0.300);
    $('#CCRenridil3').val(50);
    $('#CCRAreaA1').val(460909);
    $('#CCRAreaB1').val(475289);
    /*$('#CCRAreaA2').val(475289);
     $('#CCRAreaB2').val(5302042);*/
    __calcula();
}

function datos() {

    if ($('#fechaA').val() == '') {
        OMEGA('Debe indicar la fecha del an�lisis');
        return;
    }

    if ($('#pureza1').val() == '') {
        OMEGA('Debe indicar la pureza del estandar de impureza');
        return;
    }

    if ($('#certificado2').val() == '') {
        OMEGA('Debe seleccionar si utiliza estandar interno');
        return;
    }

    if ($('#tipo_form').val() == '') {
        OMEGA('Debe seleccionar el tipo de formulaci�n');
        return;
    }

    if ($('#equipo').val() == '') {
        OMEGA('Debe seleccionar el equipo');
        return;
    }

    if ($('#modo_i').val() == '1' && $('#condicion').val() == '') {
        OMEGA('Debe indicar la condici�n del inyector splitless');
        return;
    }

    if (!confirm('Ingresar ensayo?'))
        return;

    var parametros = {
        '_AJAX': 1,
        'xanalizar': $('#xanalizar').val(),
        'tipo': $('#tipo').val(),
        'ingrediente': $('#ingrediente').val(),
        'fechaA': $('#fechaA').val(),
        'impureza': $('#impureza').val(),
        'rango': $('#rango').val(),
        'unidad': $('#unidad').val(),
        'densidad': $('#densidad').val(),
        'metodo': $('#metodo').val(),
        'disolvente1': $('#disolvente1').val(),
        'validado': $('#validado').val(),
        'disolvente2': $('#disolvente2').val(),
        'croma': $('#croma').val(),
        'disolvente3': $('#disolvente3').val(),
        'estandar': $('#estandar').val(),
        'tiempo1': $('#tiempo1').val(),
        'origen1': $('#origen1').val(),
        'tiempo2': $('#tiempo2').val(),
        'certificado1': $('#certificado1').val(),
        'tiempo3': $('#tiempo3').val(),
        'pureza1': $('#pureza1').val(),
        'certificado2': $('#certificado2').val(),
        'interno2': $('#interno2').val(),
        'inyeccion': $('#inyeccion').val(),
        'origen2': $('#origen2').val(),
        'loop': $('#loop').val(),
        'certificado3': $('#certificado3').val(),
        'pureza2': $('#pureza2').val(),
        'equipo': $('#equipo').val(),
        'tipo_c': $('#tipo_c').val(),
        'codigo_c': $('#codigo_c').val(),
        'marca_c': $('#marca_c').val(),
        'temp_c': $('#temp_c').val(),
        'dim_c': $('#dim_c').val(),
        'tipo_i': $('#tipo_i').val(),
        'temp_i': $('#temp_i').val(),
        'modo_i': $('#modo_i').val(),
        'condicion_i': $('#condicion_i').val(),
        'gas1': $('#gas1').val(),
        'flujo1': $('#flujo1').val(),
        'presion1': $('#presion1').val(),
        'bomba_d': $('#bomba_d').val(),
        'elu_d': $('#elu_d').val(),
        'canal_crom1': $('#canal_crom1').val(),
        'canal_crom2': $('#canal_crom2').val(),
        'canal_crom3': $('#canal_crom3').val(),
        'canal_crom4': $('#canal_crom4').val(),
        'flujo_crom1': $('#flujo_crom1').val(),
        'flujo_crom2': $('#flujo_crom2').val(),
        'flujo_crom3': $('#flujo_crom3').val(),
        'flujo_crom4': $('#flujo_crom4').val(),
        'presion_crom1': $('#presion_crom1').val(),
        'presion_crom2': $('#presion_crom2').val(),
        'presion_crom3': $('#presion_crom3').val(),
        'presion_crom4': $('#presion_crom4').val(),
        'tipo_d': $('#tipo_d').val(),
        'temp_d': $('#temp_d').val(),
        'long_d': $('#long_d').val(),
        'flujofid1': $('#flujofid1').val(),
        'flujofid2': $('#flujofid2').val(),
        'flujofid3': $('#flujofid3').val(),
        'modo': $('#modo').val(),
        'temp1_g': $('#temp1_g').val(),
        'temp2_g': $('#temp2_g').val(),
        'temp3_g': $('#temp3_g').val(),
        'masa1': $('#masa1').val(),
        'aforado11': $('#aforado11').val(),
        'alicuota11': $('#alicuota11').val(),
        'aforado12': $('#aforado12').val(),
        'alicuota12': $('#alicuota12').val(),
        'aforado13': $('#aforado13').val(),
        'masa2': $('#masa2').val(),
        'aforado21': $('#aforado21').val(),
        'alicuota21': $('#alicuota21').val(),
        'aforado22': $('#aforado22').val(),
        'alicuota22': $('#alicuota22').val(),
        'aforado23': $('#aforado23').val(),
        'alicuota31': $('#alicuota31').val(),
        'aforado31': $('#aforado31').val(),
        'alicuota32': $('#alicuota32').val(),
        'aforado32': $('#aforado32').val(),
        'concentracion3': $('#concentracion3').val(),
        'areaimp3': $('#areaimp3').val(),
        'areaint3': $('#areaint3').val(),
        'alicuota41': $('#alicuota41').val(),
        'aforado41': $('#aforado41').val(),
        'alicuota42': $('#alicuota42').val(),
        'aforado42': $('#aforado42').val(),
        'concentracion4': $('#concentracion4').val(),
        'areaimp4': $('#areaimp4').val(),
        'areaint4': $('#areaint4').val(),
        'alicuota51': $('#alicuota51').val(),
        'aforado51': $('#aforado51').val(),
        'alicuota52': $('#alicuota52').val(),
        'aforado52': $('#aforado52').val(),
        'concentracion5': $('#concentracion5').val(),
        'areaimp5': $('#areaimp5').val(),
        'areaint5': $('#areaint5').val(),
        'alicuota61': $('#alicuota61').val(),
        'aforado61': $('#aforado61').val(),
        'alicuota62': $('#alicuota62').val(),
        'aforado62': $('#aforado62').val(),
        'concentracion6': $('#concentracion6').val(),
        'areaimp6': $('#areaimp6').val(),
        'areaint6': $('#areaint6').val(),
        'muestramasa1': $('#muestramasa1').val(),
        'muestraaforado11': $('#muestraaforado11').val(),
        'muestraalicuota11': $('#muestraalicuota11').val(),
        'muestraaforado12': $('#muestraaforado12').val(),
        'muestraalicuota12': $('#muestraalicuota12').val(),
        'muestraaforado13': $('#muestraaforado13').val(),
        'muestramasa2': $('#muestramasa2').val(),
        'muestraaforado21': $('#muestraaforado21').val(),
        'muestraalicuota21': $('#muestraalicuota21').val(),
        'muestraaforado22': $('#muestraaforado22').val(),
        'muestraalicuota22': $('#muestraalicuota22').val(),
        'muestraaforado23': $('#muestraaforado23').val(),
        'areaA1': $('#areaA1').val(),
        'areaB1': $('#areaB1').val(),
        'areaA2': $('#areaA2').val(),
        'areaB2': $('#areaB2').val(),
        'muestramasa3': $('#muestramasa3').val(),
        'muestraaforado31': $('#muestraaforado31').val(),
        'muestraalicuota31': $('#muestraalicuota31').val(),
        'muestraaforado32': $('#muestraaforado32').val(),
        'muestraalicuota32': $('#muestraalicuota32').val(),
        'muestraaforado33': $('#muestraaforado33').val(),
        'muestramasa4': $('#muestramasa4').val(),
        'muestraaforado41': $('#muestraaforado41').val(),
        'muestraalicuota41': $('#muestraalicuota41').val(),
        'muestraaforado42': $('#muestraaforado42').val(),
        'muestraalicuota42': $('#muestraalicuota42').val(),
        'muestraaforado43': $('#muestraaforado43').val(),
        'areaA3': $('#areaA3').val(),
        'areaB3': $('#areaB3').val(),
        'areaA4': $('#areaA4').val(),
        'areaB4': $('#areaB4').val(),
        'CCRmuestramasa': $('#CCRmuestramasa').val(),
        'CCRmuestradil1': $('#CCRmuestradil1').val(),
        'CCRmuestraali1': $('#CCRmuestraali1').val(),
        'CCRmuestradil2': $('#CCRmuestradil2').val(),
        'CCRmuestraali2': $('#CCRmuestraali2').val(),
        'CCRmuestradil3': $('#CCRmuestradil3').val(),
        'CCRinternomasa': $('#CCRinternomasa').val(),
        'CCRinternodil1': $('#CCRinternodil1').val(),
        'CCRinternoali1': $('#CCRinternoali1').val(),
        'CCRinternodil2': $('#CCRinternodil2').val(),
        'CCRinternoali2': $('#CCRinternoali2').val(),
        'CCRinternodil3': $('#CCRinternodil3').val(),
        'CCRenrimasa': $('#CCRenrimasa').val(),
        'CCRenridil1': $('#CCRenridil1').val(),
        'CCRenriali1': $('#CCRenriali1').val(),
        'CCRenridil2': $('#CCRenridil2').val(),
        'CCRenriali2': $('#CCRenriali2').val(),
        'CCRenridil3': $('#CCRenridil3').val(),
        'CCRAreaA1': $('#CCRAreaA1').val(),
        'CCRAreaB1': $('#CCRAreaB1').val(),
        'CCRAreaA2': $('#CCRAreaA2').val(),
        'CCRAreaB2': $('#CCRAreaB2').val(),
        'final_P': $('#final_P').val(),
        'final_I': $('#final_I').val(),
        'obs': $('#obs').val()
    }

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    break;
                case '1':
                    opener.location.reload();
                    alert("Transaccion finalizada");
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function Redondear(txt) {
    if (txt.value != '')
        _RED(txt, 4);
    __calcula();
}

function __calcula() {
    if (document.getElementById('unidad').value == '')
        return;

    /*******************************************/
    /********INICIO CURVA DE CALIBRACION********/
    /*******************************************/

    var token = 1;

    if ($('#alicuota11').val() != '' && $('#aforado12').val() != '')
        token = $('#alicuota11').val() / $('#aforado12').val();
    if ($('#alicuota12').val() != '' && $('#aforado13').val() != '')
        token *= $('#alicuota12').val() / $('#aforado13').val();

    var con1 = ($('#masa1').val() / $('#aforado11').val()) * ($('#pureza1').val() / 100) * token;

    token = 1;

    if ($('#alicuota21').val() != '' && $('#aforado22').val() != '')
        token = $('#alicuota21').val() / $('#aforado22').val();
    if ($('#alicuota22').val() != '' && $('#aforado23').val() != '')
        token *= $('#alicuota22').val() / $('#aforado23').val();

    var con2 = ($('#masa2').val() / $('#aforado21').val()) * ($('#pureza2').val() / 100) * token;

    /***************************************************************************************************/

    token = 1;

    if ($('#alicuota31').val() != '' && $('#aforado31').val() != '')
        token = ($('#alicuota31').val() / 1000) / $('#aforado31').val();
    if ($('#alicuota32').val() != '' && $('#aforado32').val() != '')
        token *= $('#alicuota32').val() / $('#aforado32').val();

    var con3 = (con1 * 1000) * token;

    token = 1;

    if ($('#alicuota41').val() != '' && $('#aforado41').val() != '')
        token = ($('#alicuota41').val() / 1000) / $('#aforado41').val();
    if ($('#alicuota42').val() != '' && $('#aforado42').val() != '')
        token *= $('#alicuota42').val() / $('#aforado42').val();

    var con4 = (con1 * 1000) * token;

    token = 1;

    if ($('#alicuota51').val() != '' && $('#aforado51').val() != '')
        token = ($('#alicuota51').val() / 1000) / $('#aforado51').val();
    if ($('#alicuota52').val() != '' && $('#aforado52').val() != '')
        token *= $('#alicuota52').val() / $('#aforado52').val();

    var con5 = (con1 * 1000) * token;

    token = 1;

    if ($('#alicuota61').val() != '' && $('#aforado61').val() != '')
        token = ($('#alicuota61').val() / 1000) / $('#aforado61').val();
    if ($('#alicuota62').val() != '' && $('#aforado62').val() != '')
        token *= $('#alicuota62').val() / $('#aforado62').val();

    var con6 = (con1 * 1000) * token;

    /***************************************************************************************************/

    if ($('#areaimp3').val() != '' && $('#areaint3').val() != '')
        var con7 = $('#areaimp3').val() / $('#areaint3').val();
    else
        var con7 = 0;

    if ($('#areaimp4').val() != '' && $('#areaint4').val() != '')
        var con8 = $('#areaimp4').val() / $('#areaint4').val();
    else
        var con8 = 0;

    if ($('#areaimp5').val() != '' && $('#areaint5').val() != '')
        var con9 = $('#areaimp5').val() / $('#areaint5').val();
    else
        var con9 = 0;

    if ($('#areaimp6').val() != '' && $('#areaint6').val() != '')
        var con10 = $('#areaimp6').val() / $('#areaint6').val();
    else
        var con10 = 0;

    /***************************************************************************************************/
    var sx = con3 + con4 + con5 + con6;
    var sx2 = (con3 * con3) + (con4 * con4) + (con5 * con5) + (con6 * con6);
    var sy = (parseFloat($('#areaimp3').val()) + parseFloat($('#areaimp4').val()) + parseFloat($('#areaimp5').val()) + parseFloat($('#areaimp6').val()));
    var pxy = (con3 * parseFloat($('#areaimp3').val())) + (con4 * parseFloat($('#areaimp4').val())) + (con5 * parseFloat($('#areaimp5').val())) + (con6 * parseFloat($('#areaimp6').val()));
    var mediaX = sx / 4;
    var mediaY = sy / 4;
    var cpxy = ((con3 - mediaX) * (parseFloat($('#areaimp3').val()) - mediaY)) + ((con4 - mediaX) * (parseFloat($('#areaimp4').val()) - mediaY)) + ((con5 - mediaX) * (parseFloat($('#areaimp5').val()) - mediaY)) + ((con6 - mediaX) * (parseFloat($('#areaimp6').val()) - mediaY));
    var csx2 = (con3 - mediaX) * (con3 - mediaX) + (con4 - mediaX) * (con4 - mediaX) + (con5 - mediaX) * (con5 - mediaX) + (con6 - mediaX) * (con6 - mediaX);
    var csy2 = ((parseFloat($('#areaimp3').val()) - mediaY) * (parseFloat($('#areaimp3').val()) - mediaY)) + ((parseFloat($('#areaimp4').val()) - mediaY) * (parseFloat($('#areaimp4').val()) - mediaY)) + ((parseFloat($('#areaimp5').val()) - mediaY) * (parseFloat($('#areaimp5').val()) - mediaY)) + ((parseFloat($('#areaimp6').val()) - mediaY) * (parseFloat($('#areaimp6').val()) - mediaY));
    if ($('#certificado2').val() == '1') {
        sy = con7 + con8 + con9 + con10;
        pxy = (con3 * con7) + (con4 * con8) + (con5 * con9) + (con6 * con10);
        cpxy = (con3 - mediaX) * (con7 - mediaY) + (con4 - mediaX) * (con8 - mediaY) + (con5 - mediaX) * (con9 - mediaY) + (con6 - mediaX) * (con10 - mediaY);
        csy2 = (con7 - mediaY) * (con7 - mediaY) + (con8 - mediaY) * (con8 - mediaY) + (con9 - mediaY) * (con9 - mediaY) + (con10 - mediaY) * (con10 - mediaY);
    }

    var pendiente = (4 * pxy - sx * sy) / (4 * sx2 - sx * sx);
    var intercepto = (sy - pendiente * sx) / 4;
    var coerel = cpxy / Math.sqrt(csx2 * csy2);

    /***************************************************************************************************/
    document.getElementById('con1').innerHTML = con1.toFixed(5);
    document.getElementById('con2').innerHTML = con2.toFixed(5);
    document.getElementById('concentracion3').value = con3.toFixed(5);
    document.getElementById('concentracion4').value = con4.toFixed(5);
    document.getElementById('concentracion5').value = con5.toFixed(5);
    document.getElementById('concentracion6').value = con6.toFixed(5);
    document.getElementById('area3').innerHTML = _RED2(con7, 6);
    document.getElementById('area4').innerHTML = _RED2(con8, 6);
    document.getElementById('area5').innerHTML = _RED2(con9, 6);
    document.getElementById('area6').innerHTML = _RED2(con10, 6);
    document.getElementById('m').innerHTML = _RED2(pendiente, 4);
    document.getElementById('b').innerHTML = _RED2(intercepto, 4);
    document.getElementById('r').innerHTML = _RED2(coerel, 4);

    if ($('#certificado2').val() == '0') {
        document.getElementById('con2').innerHTML = 'No Aplica';
        document.getElementById('area3').innerHTML = 'No Aplica';
        document.getElementById('area4').innerHTML = 'No Aplica';
        document.getElementById('area5').innerHTML = 'No Aplica';
        document.getElementById('area6').innerHTML = 'No Aplica';
    }

    /*******************************************/
    /********FIN CURVA DE CALIBRACION********/
    /*******************************************/

    /*******************************************/
    /********INICIO DATOS DE ANALISIS***********/
    /*******************************************/
    var token = 1;

    if ($('#muestraalicuota11').val() != '' && $('#muestraaforado12').val() != '')
        token = $('#muestraalicuota11').val() / $('#muestraaforado12').val();
    if ($('#muestraalicuota12').val() != '' && $('#muestraaforado13').val() != '')
        token *= $('#muestraalicuota12').val() / $('#muestraaforado13').val();

    var con1 = ($('#muestramasa1').val() / $('#muestraaforado11').val()) * token;

    token = 1;

    if ($('#muestraalicuota21').val() != '' && $('#muestraaforado22').val() != '')
        token = $('#muestraalicuota21').val() / $('#muestraaforado22').val();
    if ($('#muestraalicuota22').val() != '' && $('#muestraaforado23').val() != '')
        token *= $('#muestraalicuota22').val() / $('#muestraaforado23').val();

    var con2 = ($('#muestramasa2').val() / $('#muestraaforado21').val()) * ($('#pureza2').val() / 100) * token;

    var token = 1;

    if ($('#muestraalicuota31').val() != '' && $('#muestraaforado32').val() != '')
        token = $('#muestraalicuota31').val() / $('#muestraaforado32').val();
    if ($('#muestraalicuota32').val() != '' && $('#muestraaforado33').val() != '')
        token *= $('#muestraalicuota32').val() / $('#muestraaforado33').val();

    var con3 = ($('#muestramasa3').val() / $('#muestraaforado31').val()) * token;

    token = 1;

    if ($('#muestraalicuota41').val() != '' && $('#muestraaforado42').val() != '')
        token = $('#muestraalicuota41').val() / $('#muestraaforado42').val();
    if ($('#muestraalicuota42').val() != '' && $('#muestraaforado43').val() != '')
        token *= $('#muestraalicuota42').val() / $('#muestraaforado43').val();

    var con4 = ($('#muestramasa4').val() / $('#muestraaforado41').val()) * ($('#pureza2').val() / 100) * token;

    /*if($('#certificado2').val() == '0'){
     $('#areaA2').val() = 1;
     $('#areaB2').val() = 1;
     $('#areaA4').val() = 1;
     $('#areaB4').val() = 1;
     }*/
    if ($('#areaA3').val() == '0')
        $('#areaA3').val('');
    if ($('#areaA3').val() == '0')
        $('#areaB3').val('');
    if ($('#areaA3').val() == '0')
        $('#areaA4').val('');
    if ($('#areaA3').val() == '0')
        $('#areaB4').val('');
    var promArea1 = (parseFloat($('#areaA1').val()) + parseFloat($('#areaB1').val())) / 2;
    var promArea2 = (parseFloat($('#areaA2').val()) + parseFloat($('#areaB2').val())) / 2;
    var promArea3 = (parseFloat($('#areaA3').val()) + parseFloat($('#areaB3').val())) / 2;
    var promArea4 = (parseFloat($('#areaA4').val()) + parseFloat($('#areaB4').val())) / 2;


    var varianza = 0;
    varianza += Math.pow(parseFloat($('#areaA1').val()) - promArea1, 2) + Math.pow(parseFloat($('#areaB1').val()) - promArea1, 2);
    var desv1 = Math.sqrt(varianza);
    var cv1 = (desv1 / promArea1) * 100;

    var varianza1 = 0;
    varianza1 += Math.pow(parseFloat($('#areaA2').val()) - promArea2, 2) + Math.pow(parseFloat($('#areaB2').val()) - promArea2, 2);
    var desv2 = Math.sqrt(varianza1);
    var cv2 = (desv2 / promArea2) * 100;

    var varianza2 = 0;
    varianza2 += Math.pow(parseFloat($('#areaA3').val()) - promArea3, 2) + Math.pow(parseFloat($('#areaB3').val()) - promArea3, 2);
    var desv3 = Math.sqrt(varianza2);
    var cv3 = (desv3 / promArea3) * 100;

    var varianza3 = 0;
    varianza3 += Math.pow(parseFloat($('#areaA4').val()) - promArea4, 2) + Math.pow(parseFloat($('#areaB4').val()) - promArea4, 2);
    var desv4 = Math.sqrt(varianza3);
    var cv4 = (desv4 / promArea4) * 100;

    var concurArea1 = ((parseFloat($('#areaA1').val()) - intercepto) / pendiente) / 1000;
    var concurArea2 = ((parseFloat($('#areaB1').val()) - intercepto) / pendiente) / 1000;
    var concurProm1 = (concurArea1 + concurArea2) / 2;

    var varianza4 = 0;
    varianza4 += Math.pow(concurArea1 - concurProm1, 2) + Math.pow(concurArea2 - concurProm1, 2);
    var concurdesv = Math.sqrt(varianza4);
    var concurcv = (concurdesv / concurProm1) * 100;

    var concurIArea1 = (((parseFloat($('#areaA1').val()) / parseFloat($('#areaA2').val())) - intercepto) / pendiente) / 1000;
    var concurIArea2 = (((parseFloat($('#areaB1').val()) / parseFloat($('#areaB2').val())) - intercepto) / pendiente) / 1000;
    var concurIProm1 = (concurIArea1 + concurIArea2) / 2;

    var varianza5 = 0;
    varianza5 += Math.pow(concurIArea1 - concurIProm1, 2) + Math.pow(concurIArea2 - concurIProm1, 2);
    var concurIdesv = Math.sqrt(varianza5);
    var concurIcv = (concurIdesv / concurIProm1) * 100;

    var concurArea3 = ((parseFloat($('#areaA3').val()) - intercepto) / pendiente) / 1000;
    var concurArea4 = ((parseFloat($('#areaB3').val()) - intercepto) / pendiente) / 1000;
    var concurProm2 = (concurArea3 + concurArea4) / 2;

    var varianza6 = 0;
    varianza6 += Math.pow(concurArea3 - concurProm2, 2) + Math.pow(concurArea4 - concurProm2, 2);
    var concurdesv2 = Math.sqrt(varianza6);
    var concurcv2 = (concurdesv2 / concurProm2) * 100;

    var concurIArea3 = (((parseFloat($('#areaA3').val()) / parseFloat($('#areaA4').val())) - intercepto) / pendiente) / 1000;
    var concurIArea4 = (((parseFloat($('#areaB3').val()) / parseFloat($('#areaB4').val())) - intercepto) / pendiente) / 1000;
    var concurIProm2 = (concurIArea3 + concurIArea4) / 2;

    var varianza7 = 0;
    varianza7 += Math.pow(concurIArea3 - concurIProm2, 2) + Math.pow(concurIArea4 - concurIProm2, 2);
    var concurIdesv2 = Math.sqrt(varianza7);
    var concurIcv2 = (concurIdesv2 / concurIProm2) * 100;

    /***************************************************************************************************/
    document.getElementById('mcon1').innerHTML = _RED2(con1, 5);
    document.getElementById('mcon2').innerHTML = _RED2(con2, 5);
    document.getElementById('mcon3').innerHTML = _RED2(con3, 5);
    document.getElementById('mcon4').innerHTML = _RED2(con4, 5);
    document.getElementById('promArea1').innerHTML = promArea1;
    document.getElementById('promArea2').innerHTML = promArea2;
    document.getElementById('promArea3').innerHTML = promArea3;
    document.getElementById('promArea4').innerHTML = promArea4;
    document.getElementById('desvArea1').innerHTML = _RED2(desv1, 3);
    document.getElementById('cvArea1').innerHTML = _RED2(cv1, 3);
    document.getElementById('desvArea2').innerHTML = _RED2(desv2, 3);
    document.getElementById('cvArea2').innerHTML = _RED2(cv2, 3);
    document.getElementById('desvArea3').innerHTML = _RED2(desv3, 3);
    document.getElementById('cvArea3').innerHTML = _RED2(cv3, 3);
    document.getElementById('desvArea4').innerHTML = _RED2(desv4, 3);
    document.getElementById('cvArea4').innerHTML = _RED2(cv4, 3);
    document.getElementById('concurArea1').innerHTML = _RED2(concurArea1, 5);
    document.getElementById('concurArea2').innerHTML = _RED2(concurArea2, 5);
    document.getElementById('concurProm1').innerHTML = _RED2(concurProm1, 5);
    document.getElementById('concurdesv').innerHTML = _RED2(concurdesv, 3);
    document.getElementById('concurcv').innerHTML = _RED2(concurcv, 3);
    document.getElementById('concurArea3').innerHTML = _RED2(concurArea3, 5);
    document.getElementById('concurArea4').innerHTML = _RED2(concurArea4, 5);
    document.getElementById('concurProm2').innerHTML = _RED2(concurProm2, 5);
    document.getElementById('concurdesv2').innerHTML = _RED2(concurdesv2, 3);
    document.getElementById('concurcv2').innerHTML = _RED2(concurcv2, 3);
    document.getElementById('concurIArea1').innerHTML = _RED2(concurIArea1, 6);
    document.getElementById('concurIArea2').innerHTML = _RED2(concurIArea2, 6);
    document.getElementById('concurIProm1').innerHTML = _RED2(concurIProm1, 5);
    document.getElementById('concurIdesv').innerHTML = _RED2(concurIdesv, 3);
    document.getElementById('concurIcv').innerHTML = _RED2(concurIcv, 3);
    document.getElementById('concurIArea3').innerHTML = _RED2(concurIArea3, 6);
    document.getElementById('concurIArea4').innerHTML = _RED2(concurIArea4, 6);
    document.getElementById('concurIProm2').innerHTML = _RED2(concurIProm2, 5);
    document.getElementById('concurIdesv2').innerHTML = _RED2(concurIdesv2, 3);
    document.getElementById('concurIcv2').innerHTML = _RED2(concurIcv2, 3);
    if ($('#certificado2').val() == '0') {
        document.getElementById('mcon2').innerHTML = 'No Aplica';
        document.getElementById('promArea2').innerHTML = 'No Aplica';
        document.getElementById('promArea4').innerHTML = 'No Aplica';
        document.getElementById('desvArea2').innerHTML = 'No Aplica';
        document.getElementById('desvArea4').innerHTML = 'No Aplica';
        document.getElementById('cvArea2').innerHTML = 'No Aplica';
        document.getElementById('cvArea4').innerHTML = 'No Aplica';
        document.getElementById('concurIArea1').innerHTML = 'No Aplica';
        document.getElementById('concurIArea2').innerHTML = 'No Aplica';
        document.getElementById('concurIProm1').innerHTML = 'No Aplica';
        document.getElementById('concurIdesv').innerHTML = 'No Aplica';
        document.getElementById('concurIcv').innerHTML = 'No Aplica';
        document.getElementById('concurIArea3').innerHTML = 'No Aplica';
        document.getElementById('concurIArea4').innerHTML = 'No Aplica';
        document.getElementById('concurIProm2').innerHTML = 'No Aplica';
        document.getElementById('concurIdesv2').innerHTML = 'No Aplica';
        document.getElementById('concurIcv2').innerHTML = 'No Aplica';
    }
    if ($('#certificado2').val() == '1') {
        document.getElementById('concurArea1').innerHTML = 'No Aplica';
        document.getElementById('concurArea2').innerHTML = 'No Aplica';
        document.getElementById('concurProm1').innerHTML = 'No Aplica';
        document.getElementById('concurdesv').innerHTML = 'No Aplica';
        document.getElementById('concurcv').innerHTML = 'No Aplica';
        document.getElementById('concurArea3').innerHTML = 'No Aplica';
        document.getElementById('concurArea4').innerHTML = 'No Aplica';
        document.getElementById('concurProm2').innerHTML = 'No Aplica';
        document.getElementById('concurdesv2').innerHTML = 'No Aplica';
        document.getElementById('concurcv2').innerHTML = 'No Aplica';
    }
    /*******************************************/
    /***********FIN DATOS DE ANALISIS***********/
    /*******************************************/

    /*******************************************/
    /********INICIO REPORTE DE RESULTADOS*******/
    /*******************************************/

    var mue1A = concurProm1 * (1 / parseFloat(document.getElementById('mcon1').innerHTML)) * 100;
    var mue1B = concurProm1 * (1 / parseFloat(document.getElementById('mcon1').innerHTML)) * parseFloat(document.getElementById('densidad').value) * 100;
    var mue1C = concurIProm1 * (1 / parseFloat(document.getElementById('mcon1').innerHTML)) * 100;
    var mue1D = concurIProm1 * (1 / parseFloat(document.getElementById('mcon1').innerHTML)) * parseFloat(document.getElementById('densidad').value) * 100;
    var mue2A = concurProm2 * (1 / parseFloat(document.getElementById('mcon3').innerHTML)) * 100;
    var mue2B = concurProm2 * (1 / parseFloat(document.getElementById('mcon3').innerHTML)) * parseFloat(document.getElementById('densidad').value) * 100;
    var mue2C = concurIProm2 * (1 / parseFloat(document.getElementById('mcon3').innerHTML)) * 100;
    var mue2D = concurIProm2 * (1 / parseFloat(document.getElementById('mcon3').innerHTML)) * parseFloat(document.getElementById('densidad').value) * 100;
    var mue1AProm = (mue1A + mue2A) / 2;
    var mue1BProm = (mue1B + mue2B) / 2;
    var mue1CProm = (mue1C + mue2C) / 2;
    var mue1DProm = (mue1D + mue2D) / 2;
    if (!mue2A)
        mue1AProm = mue1A;
    if (!mue2B)
        mue1BProm = mue1B;
    if (!mue2C)
        mue1CProm = mue1C;
    if (!mue2D)
        mue1DProm = mue1D;


    var densidad = $('#densidad').val();
    if ($('#certificado2').val() == '0' && $('#unidad').val() == '0') {
        var cvt = 0.67 * Math.pow(2, 1 - 0.5 * Math.log10(mue1A / 100));
        var cvt2 = 0.67 * Math.pow(2, 1 - 0.5 * Math.log10(mue2A / 100));
    }
    if ($('#certificado2').val() == '1' && $('#unidad').val() == '0') {
        var cvt = 0.67 * Math.pow(2, 1 - 0.5 * Math.log10(mue1C / 100));
        var cvt2 = 0.67 * Math.pow(2, 1 - 0.5 * Math.log10(mue2C / 100));
    }
    if ($('#certificado2').val() == '0' && $('#unidad').val() == '1') {
        var cvt = 0.67 * Math.pow(2, 1 - 0.5 * Math.log10(mue1B / densidad / 100));
        var cvt2 = 0.67 * Math.pow(2, 1 - 0.5 * Math.log10(mue2B / densidad / 100));
    }
    if ($('#certificado2').val() == '1' && $('#unidad').val() == '1') {
        var cvt = 0.67 * Math.pow(2, 1 - 0.5 * Math.log10(mue1D / densidad / 100));
        var cvt2 = 0.67 * Math.pow(2, 1 - 0.5 * Math.log10(mue2D / densidad / 100));
    }



    var tkvalprom = concurProm1 * 1000;
    if (!concurProm2)
        concurProm2 = 1;
    var tkvalprom2 = concurProm2 * 1000;
    var tkYarea1 = parseFloat($('#areaimp3').val());
    var tkYarea2 = parseFloat($('#areaimp4').val());
    var tkYarea3 = parseFloat($('#areaimp5').val());
    var tkYarea4 = parseFloat($('#areaimp6').val());
    if ($('#certificado2').val() == '1') {
        tkvalprom = concurIProm1 * 1000;
        tkvalprom2 = concurIProm2 * 1000;
        var tkYarea1 = con7;
        var tkYarea2 = con8;
        var tkYarea3 = con9;
        var tkYarea4 = con10;
    }
    var tkYareaP1 = pendiente * parseFloat(document.getElementById('concentracion3').value) + intercepto;
    var tkYareaP2 = pendiente * parseFloat(document.getElementById('concentracion4').value) + intercepto;
    var tkYareaP3 = pendiente * parseFloat(document.getElementById('concentracion5').value) + intercepto;
    var tkYareaP4 = pendiente * parseFloat(document.getElementById('concentracion6').value) + intercepto;
    var tkYdiff1 = Math.abs(tkYarea1 - tkYareaP1);
    var tkYdiff2 = Math.abs(tkYarea2 - tkYareaP2);
    var tkYdiff3 = Math.abs(tkYarea3 - tkYareaP3);
    var tkYdiff4 = Math.abs(tkYarea4 - tkYareaP4);
    var tkYdiff12 = Math.pow(tkYdiff1, 2);
    var tkYdiff22 = Math.pow(tkYdiff2, 2);
    var tkYdiff32 = Math.pow(tkYdiff3, 2);
    var tkYdiff42 = Math.pow(tkYdiff4, 2);
    var tkYdiffSum = tkYdiff12 + tkYdiff22 + tkYdiff32 + tkYdiff42;
    var tkSYX = Math.sqrt(tkYdiffSum / 2);
    var tkPromCon = (parseFloat(document.getElementById('concentracion3').value) + parseFloat(document.getElementById('concentracion4').value) + parseFloat(document.getElementById('concentracion5').value) + parseFloat(document.getElementById('concentracion6').value)) / 4;
    var tkXdiff1 = parseFloat(document.getElementById('concentracion3').value) - tkPromCon;
    var tkXdiff2 = parseFloat(document.getElementById('concentracion4').value) - tkPromCon;
    var tkXdiff3 = parseFloat(document.getElementById('concentracion5').value) - tkPromCon;
    var tkXdiff4 = parseFloat(document.getElementById('concentracion6').value) - tkPromCon;
    var tkXdiff12 = Math.pow(tkXdiff1, 2);
    var tkXdiff22 = Math.pow(tkXdiff2, 2);
    var tkXdiff32 = Math.pow(tkXdiff3, 2);
    var tkXdiff42 = Math.pow(tkXdiff4, 2);
    var tkXdiffSum = tkXdiff12 + tkXdiff22 + tkXdiff32 + tkXdiff42;
    var tkUMuestra1 = (tkSYX / pendiente) * Math.sqrt(0.75 + (Math.pow(tkvalprom - tkPromCon, 2) / tkXdiffSum));
    var tkUMuestra2 = (tkSYX / pendiente) * Math.sqrt(0.75 + (Math.pow(tkvalprom2 - tkPromCon, 2) / tkXdiffSum));
    var tkincrel1 = tkUMuestra1 / tkvalprom;
    var tkincrel2 = tkUMuestra2 / tkvalprom2;
    var tkincestcom = Math.sqrt(Math.pow(0.07, 2) + Math.pow((0.1 / Math.sqrt(3)), 2));
    var tkinctk1 = tkincestcom / parseFloat(document.getElementById('muestramasa1').value);
    var tkincestcomm1 = parseFloat(document.getElementById('mcon1').innerHTML) * Math.sqrt(Math.pow(tkinctk1, 2));
    var tkincestcomm2 = parseFloat(document.getElementById('mcon3').innerHTML) * Math.sqrt(Math.pow(tkinctk1, 2));
    var tkincrel11 = tkincestcomm1 / parseFloat(document.getElementById('mcon1').innerHTML);
    var tkincrel12 = tkincestcomm2 / parseFloat(document.getElementById('mcon3').innerHTML);
    if (!tkincrel12)
        tkincrel12 = 0;
    var tkinccomf = Math.sqrt(Math.pow(tkincrel1, 2) + Math.pow(tkincrel11, 2));
    var tkinccomf2 = Math.sqrt(Math.pow(tkincrel2, 2) + Math.pow(tkincrel12, 2));
    var tkincrelf = tkinccomf * mue1C;
    var tkincrelf2 = tkinccomf2 * mue2C;
    var tkincexpf = tkincrelf * 2;
    var tkincexpf2 = tkincrelf2 * 2;
    var mue1AInc = (tkincexpf + tkincexpf2) / 2;

    if ($('#unidad').val() == '1' && $('#certificado2').val() == '0') {
        mue1AInc = ((tkUMuestra1 / (concurProm1 * 1000)) * mue1B) * 2;
    }


    /***************************************************************************************************/
    document.getElementById('mue1A').innerHTML = _RED2(mue1A, 4);
    document.getElementById('mue1B').innerHTML = _RED2(mue1B, 4);
    document.getElementById('mue1C').innerHTML = _RED2(mue1C, 6);
    document.getElementById('mue1D').innerHTML = _RED2(mue1D, 6);
    document.getElementById('mue2A').innerHTML = _RED2(mue2A, 4);
    document.getElementById('mue2B').innerHTML = _RED2(mue2B, 4);
    document.getElementById('mue2C').innerHTML = _RED2(mue2C, 6);
    document.getElementById('mue2D').innerHTML = _RED2(mue2D, 6);
    document.getElementById('mue1AProm').innerHTML = _RED2(mue1AProm, 4);
    document.getElementById('mue1BProm').innerHTML = _RED2(mue1BProm, 4);
    document.getElementById('mue1CProm').innerHTML = _RED2(mue1CProm, 4);
    document.getElementById('mue1DProm').innerHTML = _RED2(mue1DProm, 4);
    document.getElementById('mue1AInc').innerHTML = _RED2(mue1AInc, 4);
    document.getElementById('mue1BInc').innerHTML = _RED2(mue1AInc, 4);
    document.getElementById('mue1CInc').innerHTML = _RED2(mue1AInc, 4);
    document.getElementById('mue1DInc').innerHTML = _RED2(mue1AInc, 4);
    document.getElementById('cvt').innerHTML = _RED2(cvt, 3);
    document.getElementById('cvt2').innerHTML = _RED2(cvt2, 3);
    document.getElementById('CCRCVT').innerHTML = _RED2(cvt, 3);
    if ($('#certificado2').val() == '0') {
        document.getElementById('mue1C').innerHTML = 'No Aplica';
        document.getElementById('mue1D').innerHTML = 'No Aplica';
        document.getElementById('mue2C').innerHTML = 'No Aplica';
        document.getElementById('mue2D').innerHTML = 'No Aplica';
        document.getElementById('mue1CProm').innerHTML = 'No Aplica';
        document.getElementById('mue1DProm').innerHTML = 'No Aplica';
        document.getElementById('mue1CProm').innerHTML = 'No Aplica';
        document.getElementById('mue1DProm').innerHTML = 'No Aplica';
        document.getElementById('mue1CInc').innerHTML = 'No Aplica';
        document.getElementById('mue1DInc').innerHTML = 'No Aplica';
        document.getElementById('final_P').value = _RED2(mue1AProm, 4);
        document.getElementById('final_I').value = _RED2(mue1AInc, 4);
    }
    if ($('#certificado2').val() == '1') {
        document.getElementById('mue1A').innerHTML = 'No Aplica';
        document.getElementById('mue1B').innerHTML = 'No Aplica';
        document.getElementById('mue2A').innerHTML = 'No Aplica';
        document.getElementById('mue2B').innerHTML = 'No Aplica';
        document.getElementById('mue1AProm').innerHTML = 'No Aplica';
        document.getElementById('mue1BProm').innerHTML = 'No Aplica';
        document.getElementById('mue1AProm').innerHTML = 'No Aplica';
        document.getElementById('mue1BProm').innerHTML = 'No Aplica';
        document.getElementById('mue1AInc').innerHTML = 'No Aplica';
        document.getElementById('mue1BInc').innerHTML = 'No Aplica';
        document.getElementById('final_P').value = _RED2(mue1DProm, 4);
        document.getElementById('final_I').value = _RED2(mue1AInc, 4);
    }
    if ($('#unidad').val() == '0') {
        document.getElementById('mue1B').innerHTML = 'No Aplica';
        document.getElementById('mue1D').innerHTML = 'No Aplica';
        document.getElementById('mue2B').innerHTML = 'No Aplica';
        document.getElementById('mue2D').innerHTML = 'No Aplica';
        document.getElementById('mue1BProm').innerHTML = 'No Aplica';
        document.getElementById('mue1DProm').innerHTML = 'No Aplica';
        document.getElementById('mue1BInc').innerHTML = 'No Aplica';
        document.getElementById('mue1DInc').innerHTML = 'No Aplica';
        document.getElementById('final_P').value = _RED2(mue1CProm, 4);
        document.getElementById('final_I').value = _RED2(mue1AInc, 4);
    }
    if ($('#unidad').val() == '1') {
        document.getElementById('mue1A').innerHTML = 'No Aplica';
        document.getElementById('mue2A').innerHTML = 'No Aplica';
        document.getElementById('mue1C').innerHTML = 'No Aplica';
        document.getElementById('mue2C').innerHTML = 'No Aplica';
        document.getElementById('mue1AProm').innerHTML = 'No Aplica';
        document.getElementById('mue1CProm').innerHTML = 'No Aplica';
        document.getElementById('mue1AInc').innerHTML = 'No Aplica';
        document.getElementById('mue1CInc').innerHTML = 'No Aplica';
        document.getElementById('final_P').value = _RED2(mue1BProm, 4);
        document.getElementById('final_I').value = _RED2(mue1AInc, 4);
    }
    /*******************************************/
    /**********FIN REPORTE DE RESULTADOS********/
    /*******************************************/
    /*******************************************/
    /**INICIO CONTROL CALIDAD ENRIQUECIMIENTOS**/
    /*******************************************/
    var token = 1;

    if ($('#CCRmuestraali1').val() != '' && $('#CCRmuestradil2').val() != '')
        token = $('#CCRmuestraali1').val() / $('#CCRmuestradil2').val();
    if ($('#CCRmuestraali2').val() != '' && $('#CCRmuestradil3').val() != '')
        token *= $('#CCRmuestraali2').val() / $('#CCRmuestradil3').val();

    var CCRcon1 = ($('#CCRmuestramasa').val() / $('#CCRmuestradil1').val()) * token;

    token = 1;

    if ($('#CCRinternoali1').val() != '' && $('#CCRinternodil2').val() != '')
        token = $('#CCRinternoali1').val() / $('#CCRinternodil2').val();
    if ($('#CCRinternoali2').val() != '' && $('#CCRinternodil3').val() != '')
        token *= $('#CCRinternoali2').val() / $('#CCRinternodil3').val();

    var CCRcon2 = ($('#CCRinternomasa').val() / $('#CCRinternodil1').val()) * ($('#pureza2').val() / 100) * token;

    token = 1;

    if ($('#CCRenriali1').val() != '' && $('#CCRenridil2').val() != '')
        token = $('#CCRenriali1').val() / $('#CCRenridil2').val();
    if ($('#CCRenriali2').val() != '' && $('#CCRenridil3').val() != '')
        token *= $('#CCRenriali2').val() / $('#CCRenridil3').val();

    var CCRcon3 = ($('#CCRenrimasa').val() * ($('#pureza1').val() / 100)) / $('#CCRenridil1').val() * token;


    var CCRProm1 = (parseFloat($('#CCRAreaA1').val()) + parseFloat($('#CCRAreaB1').val())) / 2;
    var CCRProm2 = (parseFloat($('#CCRAreaA2').val()) + parseFloat($('#CCRAreaB2').val())) / 2;
    var CCRProm3 = (parseFloat($('#areaA3').val()) + parseFloat($('#areaB3').val())) / 2;
    var CCRProm4 = (parseFloat($('#areaA4').val()) + parseFloat($('#areaB4').val())) / 2;

    var varianza = 0;
    varianza += Math.pow(parseFloat($('#CCRAreaA1').val()) - CCRProm1, 2) + Math.pow(parseFloat($('#CCRAreaB1').val()) - CCRProm1, 2);
    var CCRDesv1 = Math.sqrt(varianza);
    var CCRCV1 = (CCRDesv1 / CCRProm1) * 100;

    var varianza = 0;
    varianza += Math.pow(parseFloat($('#CCRAreaA2').val()) - CCRProm2, 2) + Math.pow(parseFloat($('#CCRAreaB2').val()) - CCRProm2, 2);
    var CCRDesv2 = Math.sqrt(varianza);
    var CCRCV2 = (CCRDesv2 / CCRProm2) * 100;

    var CCRAreaA3 = ((parseFloat($('#CCRAreaA1').val()) - intercepto) / pendiente) / 1000;
    var CCRAreaB3 = ((parseFloat($('#CCRAreaB1').val()) - intercepto) / pendiente) / 1000;
    var CCRProm3 = (CCRAreaA3 + CCRAreaB3) / 2;

    var varianza = 0;
    varianza += Math.pow(CCRAreaA3 - CCRProm3, 2) + Math.pow(CCRAreaB3 - CCRProm3, 2);
    var CCRDesv3 = Math.sqrt(varianza);
    var CCRCV3 = (CCRDesv3 / CCRProm3) * 100;

    var CCRAreaA4 = (((parseFloat($('#CCRAreaA1').val()) / parseFloat($('#CCRAreaA2').val())) - intercepto) / pendiente) / 1000;
    var CCRAreaB4 = (((parseFloat($('#CCRAreaB1').val()) / parseFloat($('#CCRAreaB2').val())) - intercepto) / pendiente) / 1000;
    var CCRProm4 = (CCRAreaA4 + CCRAreaB4) / 2;

    var varianza = 0;
    varianza += Math.pow(CCRAreaA4 - CCRProm4, 2) + Math.pow(CCRAreaB4 - CCRProm4, 2);
    var CCRDesv4 = Math.sqrt(varianza);
    var CCRCV4 = (CCRDesv4 / CCRProm4) * 100;

    var CCRmue1A = CCRProm3 * (1 / CCRcon1) * 100;
    var CCRmue1B = CCRProm3 * (1 / CCRcon1) * parseFloat(document.getElementById('densidad').value) * 100;
    var CCRmue1C = CCRProm4 * (1 / CCRcon1) * 100;
    var CCRmue1D = CCRProm4 * (1 / CCRcon1) * parseFloat(document.getElementById('densidad').value) * 100;
    /*var CCRmue2A = CCRmue1A-mue1AProm;
     var CCRmue2B = CCRmue1B-mue1BProm;
     var CCRmue2C = CCRmue1C-mue1CProm;
     var CCRmue2D = CCRmue1D-mue1DProm;*/
    if (mue2B == '')
        var CCRmue2A = CCRmue1B - mue1A;
    else
        var CCRmue2A = CCRmue1B - mue1AProm;
    if (mue2B == '')
        var CCRmue2B = CCRmue1B - mue1B;
    else
        var CCRmue2B = CCRmue1B - mue1BProm;
    if (mue2B == '')
        var CCRmue2C = CCRmue1B - mue1C;
    else
        var CCRmue2C = CCRmue1B - mue1CProm;
    if (mue2B == '')
        var CCRmue2D = CCRmue1B - mue1D;
    else
        var CCRmue2D = CCRmue1B - mue1DProm;

    if ($('#CCRenridil3').val() == '')
        lolo = $('#CCRenridil2').val();
    else
        lolo = $('#CCRenridil3').val();

    var CCRmue3A = CCRmue2A / (lolo * CCRcon3 * 100 / $('#CCRmuestramasa').val()) * 100;
    var CCRmue3B = CCRmue2B / (lolo * CCRcon3 * 100 * $('#densidad').val() / $('#CCRmuestramasa').val()) * 100;
    var CCRmue3C = CCRmue2C / (lolo * CCRcon3 * 100 / $('#CCRmuestramasa').val()) * 100;
    var CCRmue3D = CCRmue2D / (lolo * CCRcon3 * 100 * $('#densidad').val() / $('#CCRmuestramasa').val()) * 100;

    /***************************************************************************************************/
    document.getElementById('CCRcon1').innerHTML = _RED2(CCRcon1, 4);
    document.getElementById('CCRcon2').innerHTML = _RED2(CCRcon2, 4);
    document.getElementById('CCRcon3').innerHTML = _RED2(CCRcon3, 5);
    document.getElementById('CCRProm1').innerHTML = _RED2(CCRProm1, 5);
    document.getElementById('CCRProm2').innerHTML = _RED2(CCRProm2, 5);
    document.getElementById('CCRProm3').innerHTML = _RED2(CCRProm3, 5);
    document.getElementById('CCRProm4').innerHTML = _RED2(CCRProm4, 5);
    document.getElementById('CCRDesv1').innerHTML = _RED2(CCRDesv1, 3);
    document.getElementById('CCRCV1').innerHTML = _RED2(CCRCV1, 3);
    document.getElementById('CCRDesv2').innerHTML = _RED2(CCRDesv2, 3);
    document.getElementById('CCRCV2').innerHTML = _RED2(CCRCV2, 3);
    document.getElementById('CCRAreaA3').innerHTML = _RED2(CCRAreaA3, 5);
    document.getElementById('CCRAreaB3').innerHTML = _RED2(CCRAreaB3, 5);
    document.getElementById('CCRProm3').innerHTML = _RED2(CCRProm3, 6);
    document.getElementById('CCRDesv3').innerHTML = _RED2(CCRDesv3, 6);
    document.getElementById('CCRCV3').innerHTML = _RED2(CCRCV3, 4);
    document.getElementById('CCRAreaA4').innerHTML = _RED2(CCRAreaA4, 5);
    document.getElementById('CCRAreaB4').innerHTML = _RED2(CCRAreaB4, 5);
    document.getElementById('CCRProm4').innerHTML = _RED2(CCRProm4, 6);
    document.getElementById('CCRDesv4').innerHTML = _RED2(CCRDesv4, 6);
    document.getElementById('CCRCV4').innerHTML = _RED2(CCRCV4, 4);
    document.getElementById('CCRmue1A').innerHTML = _RED2(CCRmue1A, 4);
    document.getElementById('CCRmue1B').innerHTML = _RED2(CCRmue1B, 4);
    document.getElementById('CCRmue1C').innerHTML = _RED2(CCRmue1C, 6);
    document.getElementById('CCRmue1D').innerHTML = _RED2(CCRmue1D, 6);
    document.getElementById('CCRmue2A').innerHTML = _RED2(CCRmue2A, 4);
    document.getElementById('CCRmue2B').innerHTML = _RED2(CCRmue2B, 4);
    document.getElementById('CCRmue2C').innerHTML = _RED2(CCRmue2C, 6);
    document.getElementById('CCRmue2D').innerHTML = _RED2(CCRmue2D, 6);
    document.getElementById('CCRmue3A').innerHTML = _RED2(CCRmue3A, 1) + '%';
    document.getElementById('CCRmue3B').innerHTML = _RED2(CCRmue3B, 1) + '%';
    document.getElementById('CCRmue3C').innerHTML = _RED2(CCRmue3C, 1) + '%';
    document.getElementById('CCRmue3D').innerHTML = _RED2(CCRmue3D, 1) + '%';
    if ($('#certificado2').val() == '0') {
        document.getElementById('CCRcon2').innerHTML = 'No Aplica';
        document.getElementById('CCRProm2').innerHTML = 'No Aplica';
        document.getElementById('CCRDesv2').innerHTML = 'No Aplica';
        document.getElementById('CCRCV2').innerHTML = 'No Aplica';
        document.getElementById('CCRAreaA4').innerHTML = 'No Aplica';
        document.getElementById('CCRAreaB4').innerHTML = 'No Aplica';
        document.getElementById('CCRProm4').innerHTML = 'No Aplica';
        document.getElementById('CCRDesv4').innerHTML = 'No Aplica';
        document.getElementById('CCRCV4').innerHTML = 'No Aplica';
        document.getElementById('CCRmue1C').innerHTML = 'No Aplica';
        document.getElementById('CCRmue1D').innerHTML = 'No Aplica';
        document.getElementById('CCRmue2C').innerHTML = 'No Aplica';
        document.getElementById('CCRmue2D').innerHTML = 'No Aplica';
        document.getElementById('CCRmue3C').innerHTML = 'No Aplica';
        document.getElementById('CCRmue3D').innerHTML = 'No Aplica';
    }
    if ($('#certificado2').val() == '1') {
        document.getElementById('CCRAreaA3').innerHTML = 'No Aplica';
        document.getElementById('CCRAreaB3').innerHTML = 'No Aplica';
        document.getElementById('CCRProm3').innerHTML = 'No Aplica';
        document.getElementById('CCRDesv3').innerHTML = 'No Aplica';
        document.getElementById('CCRCV3').innerHTML = 'No Aplica';
        document.getElementById('CCRmue1A').innerHTML = 'No Aplica';
        document.getElementById('CCRmue1B').innerHTML = 'No Aplica';
        document.getElementById('CCRmue2A').innerHTML = 'No Aplica';
        document.getElementById('CCRmue2B').innerHTML = 'No Aplica';
        document.getElementById('CCRmue3A').innerHTML = 'No Aplica';
        document.getElementById('CCRmue3B').innerHTML = 'No Aplica';
    }
    if ($('#unidad').val() == '0') {
        document.getElementById('CCRmue1B').innerHTML = 'No Aplica';
        document.getElementById('CCRmue1D').innerHTML = 'No Aplica';
        document.getElementById('CCRmue2B').innerHTML = 'No Aplica';
        document.getElementById('CCRmue2D').innerHTML = 'No Aplica'
        document.getElementById('CCRmue3B').innerHTML = 'No Aplica';
        document.getElementById('CCRmue3D').innerHTML = 'No Aplica';
    }
    if ($('#unidad').val() == '1') {
        document.getElementById('CCRmue1A').innerHTML = 'No Aplica';
        document.getElementById('CCRmue2A').innerHTML = 'No Aplica';
        document.getElementById('CCRmue3A').innerHTML = 'No Aplica';
    }

    /*******************************************/
    /****FIN CONTROL CALIDAD ENRIQUECIMIENTOS***/
    /*******************************************/
}

function RAIZ(_num) {
    return Math.sqrt(_num);
}

function EquiposLista(_linea) {
    window.open(__SHELL__ + "?list=" + _linea, "", "width=500,height=200,scrollbars=yes,status=no");
}

function EquipoEscoge(cs, codigo, marca, modelo) {
    opener.document.getElementById('equipo').value = cs;
    opener.document.getElementById('tmp_equipo1').value = codigo;
    opener.document.getElementById('nommarmo').innerHTML = '' + marca + ' / ' + modelo;
    window.close();
}

function escondeCroma(opc) {
    if (opc == 0 || opc == 3) {
        document.getElementById('cromaLiq').style.visibility = '';
        document.getElementById('cromaGas').style.visibility = 'hidden';
        document.getElementById('cromaIon').style.visibility = 'hidden';
    }
    if (opc == 1 || opc == 4) {
        document.getElementById('cromaGas').style.visibility = '';
        document.getElementById('cromaLiq').style.visibility = 'hidden';
        document.getElementById('cromaIon').style.visibility = 'hidden';
    }
    if (opc == 2) {
        document.getElementById('cromaLiq').style.visibility = '';
        document.getElementById('cromaIon').style.visibility = '';
        document.getElementById('cromaGas').style.visibility = 'hidden';
    }
}

function escondeDetector(opc) {
    if (opc == 0) {
        document.getElementById('tempTrab').style.visibility = '';
        document.getElementById('LongOnda').style.visibility = '';
        document.getElementById('condicionesFID').style.visibility = 'hidden';
        document.getElementById('condicionesFID1').style.visibility = 'hidden';
        document.getElementById('condicionesFID2').style.visibility = 'hidden';
        document.getElementById('condicionesFID3').style.visibility = 'hidden';
    }
    if (opc == 1) {
        document.getElementById('tempTrab').style.visibility = '';
        document.getElementById('LongOnda').style.visibility = 'hidden';
        document.getElementById('condicionesFID').style.visibility = 'hidden';
        document.getElementById('condicionesFID1').style.visibility = 'hidden';
        document.getElementById('condicionesFID2').style.visibility = 'hidden';
        document.getElementById('condicionesFID3').style.visibility = 'hidden';
    }
    if (opc == 2) {
        document.getElementById('tempTrab').style.visibility = '';
        document.getElementById('LongOnda').style.visibility = 'hidden';
        document.getElementById('condicionesFID').style.visibility = '';
        document.getElementById('condicionesFID1').style.visibility = '';
        document.getElementById('condicionesFID2').style.visibility = '';
        document.getElementById('condicionesFID3').style.visibility = '';
    }
}

function ColumnasLista() {
    window.open(__SHELL__ + "?list=2", "", "width=400,height=200,scrollbars=yes,status=no");
}

function ColumnasEscoge(id, codigo, nombre, marca, dimensiones) {
    opener.document.getElementById('codigo_c').value = codigo;
    opener.document.getElementById('marca_c').value = '' + marca + ' , ' + nombre;
    opener.document.getElementById('dim_c').value = dimensiones;
    window.close();
}

function Generar() {
    if ($('#certificado2').val() == '0') {
        document.getElementById('td_tabla').innerHTML = '<table id="tabla"><tr><td>X</td><td>Y</td></tr><tr><td>' + $('#concentracion3').val() + '</td><td>' + $('#areaimp3').val() + '</td></tr><tr><td>' + $('#concentracion4').val() + '</td><td>' + $('#areaimp4').val() + '</td></tr><tr><td>' + $('#concentracion5').val() + '</td><td>' + $('#areaimp5').val() + '</td></tr><tr><td>' + $('#concentracion6').val() + '</td><td>' + $('#areaimp6').val() + '</td></tr></table>';
    } else {
        document.getElementById('td_tabla').innerHTML = '<table id="tabla"><tr><td>X</td><td>Y</td></tr><tr><td>' + $('#concentracion3').val() + '</td><td>' + document.getElementById('area3').innerHTML + '</td></tr><tr><td>' + $('#concentracion4').val() + '</td><td>' + document.getElementById('area4').innerHTML + '</td></tr><tr><td>' + $('#concentracion5').val() + '</td><td>' + document.getElementById('area5').innerHTML + '</td></tr><tr><td>' + $('#concentracion6').val() + '</td><td>' + document.getElementById('area6').innerHTML + '</td></tr></table>';
    }
    GeneraGrafico();
}

function GeneraGrafico() {
    document.getElementById('tr_grafico').style.display = '';

    $(function () {
        $('#container').highcharts({
            data: {
                table: 'td_tabla'
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            title: {
                text: 'Curva de calibraci�n para la determinaci�n de la impureza en el plaguicida',
            },
            xAxis: {
                title: {
                    text: 'Concentracion'
                }
            },
            yAxis: {
                title: {
                    text: 'Area'
                },
            }
        });
    });
}