var _decimales = 4;
var contenido1 = contenido1 = 0;

function __lolo(){
	document.getElementById('rango').value = '85';
	document.getElementById('unidad').selectedIndex=1;
	document.getElementById('aceite').selectedIndex=1;
	document.getElementById('numreactivo').value = 'Reactivo 1';
	document.getElementById('fechaA').value = '01/01/2001';
	document.getElementById('fechaP').value = '02/01/2001';
	document.getElementById('ampolla1').value = '0.1005';
	document.getElementById('ampolla2').value = '0.00021';
	document.getElementById('BA').selectedIndex=2;
	document.getElementById('muestra1').value = '0.2009';
	document.getElementById('muestra2').value = '0.2057';
	document.getElementById('consumido1').value = '12.55';
	document.getElementById('consumido2').value = '12.93';
	document.getElementById('aforado1').selectedIndex=2;
	document.getElementById('aforado2').selectedIndex=2;
	__calcula();
}

function datos(){	
	if($('#ingrediente').val()==''){
		OMEGA('Debe indicar el ingrediente activo');
		return;
	}
	
	if($('#fechaA').val()==''){
		OMEGA('Debe indicar la fecha del an�lisis');
		return;
	}
	
	if($('#rango').val()==''){
		OMEGA('Debe indicar la concentraci�n declarada');
		return;
	}
	
	if($('#unidad').val()==''){
		OMEGA('Debe seleccionar la unidad');
		return;
	}
	
	if($('#tipo_form').val()==''){
		OMEGA('Debe seleccionar el tipo de formulaci�n');
		return;
	}
	
	if($('#fechaP').val()==''){
		OMEGA('Debe indicar la fecha de preparaci�n');
		return;
	}
	
	if($('#incertidumbre').val()==''){
		OMEGA('Debe indicar la incertidumbre expandida');
		return;
	}
	
	if(!confirm('Ingresar ensayo?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'xanalizar' : $('#xanalizar').val(),
		'tipo' : $('#tipo').val(),
		'ingrediente' : $('#ingrediente').val(),
		'fechaA' : $('#fechaA').val(),
		'rango' : $('#rango').val(),
		'unidad' : $('#unidad').val(),
		'tipo_form' : $('#tipo_form').val(),
		'aceite' : $('#aceite').val(),
		'numreactivo' : $('#numreactivo').val(),
		'fechaP' : $('#fechaP').val(),
		'IECB' : $('#IECB').val(),
		'linealidad1' : $('#linealidad1').val(),
		'linealidad2' : $('#linealidad2').val(),
		'ampolla1' : $('#ampolla1').val(),
		'ampolla2' : $('#ampolla2').val(),
		'repeti1' : $('#repeti1').val(),
		'repeti2' : $('#repeti2').val(),
		'muestra1' : $('#muestra1').val(),
		'muestra2' : $('#muestra2').val(),
		'BA' : $('#BA').val(),
		'consumido1' : $('#consumido1').val(),
		'consumido2' : $('#consumido2').val(),
		'aforado1' : $('#aforado1').val(),
		'aforado2' : $('#aforado2').val(),
		'masa' : $('#masa').val(),
		'densidad' : $('#densidad').val(),
		'promedio' : $('#promedio').val(),
		'incertidumbre' : $('#incertidumbre').val(),
		'obs' : $('#obs').val()	
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					opener.location.reload();
					alert("Transaccion finalizada");
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Redondear(txt){
	_RED(txt, _decimales);
	__calcula();
}

function __calcula(){
	if(document.getElementById('unidad').value == '') return;
	
	_conc = $('#ampolla1').val()/(document.getElementById('BA').value/1000);
	document.getElementById('_conc').innerHTML =  _RED2(_conc, 4);
	
	if(document.getElementById('unidad').value == '0'){ //m/m
		document.getElementById('lbl1').innerHTML = '<strong>Contenido de '+document.getElementById('ingrediente').value+' (% m/m)</strong>';
		document.getElementById('lbl2').innerHTML = '<strong>(% m/m)</strong>';
		//
		contenido1 = ((((document.getElementById('consumido1').value/1000)*document.getElementById('ampolla1').value)*document.getElementById('aforado1').value)*100)/document.getElementById('muestra1').value;
		contenido2 = ((((document.getElementById('consumido2').value/1000)*document.getElementById('ampolla1').value)*document.getElementById('aforado2').value)*100)/document.getElementById('muestra2').value;
	}else{
		document.getElementById('lbl1').innerHTML = '<strong>Contenido de '+document.getElementById('ingrediente').value+' (% m/v)</strong>';
		document.getElementById('lbl2').innerHTML = '<strong>(% m/v)</strong>';
		//
		contenido1 = document.getElementById('consumido1').value/1000*document.getElementById('ampolla1').value*document.getElementById('aforado1').value*document.getElementById('densidad').value*100/document.getElementById('muestra1').value;
		contenido2 = document.getElementById('consumido2').value/1000*document.getElementById('ampolla1').value*document.getElementById('aforado2').value*document.getElementById('densidad').value*100/document.getElementById('muestra2').value;
	}
	var promedio = (contenido1 + contenido2) / 2;
	document.getElementById('contenido1').value = _RED2(contenido1, 2);
	document.getElementById('contenido2').value = _RED2(contenido2, 2);
	document.getElementById('promedio').value = _RED2(promedio, 2);
	
	__calcula2();
}

function __calcula2(){
	var tmp1 = $('#linealidad1').val()/Math.sqrt(3);
	var tmp2 = $('#linealidad2').val()/Math.sqrt(3);

	var IEC1 = Math.sqrt(Math.pow($('#repeti1').val(), 2) + Math.pow(tmp1, 2));
	var IEC2 = Math.sqrt(Math.pow($('#repeti2').val(), 2) + Math.pow(tmp2, 2));
	var IR1 = IEC1/$('#muestra1').val();
	var IR2 = IEC2/$('#muestra2').val();
	//
	var _tol1 = $('#ampolla2').val()/Math.sqrt(3);
	_tol1 = RAIZ(Math.pow(_tol1, 2));
	_tol1 = _tol1/$('#ampolla1').val();
	//
	var control = document.getElementById('BA');
	var BAIR = control.options[control.selectedIndex].getAttribute('IR');
	var CYIE = RAIZ(Math.pow(_tol1, 2) + Math.pow(BAIR, 2));
	var CYIR = CYIE/_conc;
	//
	//var control = document.getElementById('aforado1');
	//var BAEC = control.options[control.selectedIndex].getAttribute('IEC');
	//var PIEC = PipetasIEC($('#alicuota1').val());
	//var factor = RAIZ(Math.pow(BAEC/$('#aforado1').val(), 2) + Math.pow(PIEC/$('#alicuota1').val(), 2));
	//_tmp = $('#aforado1').val()/$('#alicuota1').val();
	//factor = factor / _tmp;
	factor = 0.0000660;

	var YIR1 = $('#IECB').val()/$('#consumido1').val();
	var YIR2 = $('#IECB').val()/$('#consumido2').val();
	alert(YIR1);
	if(document.getElementById('unidad').value == '0'){ //m/m
		var IRDEN = 0;
	}else{
		var IRDEN = Math.pow(0.000058/$('#densidad').val(), 2);
	}
	
	var PRE1 = RAIZ(Math.pow(IR1, 2) + Math.pow(CYIR, 2) + Math.pow(factor, 2) + Math.pow(YIR1, 2) + IRDEN);
	var PRE2 = RAIZ(Math.pow(IR2, 2) + Math.pow(CYIR, 2) + Math.pow(factor, 2) + Math.pow(YIR2, 2) + IRDEN);
		alert(PRE2);
	var MED1 = PRE1 * contenido1;
	var MED2 = PRE2 * contenido2;
	var FIN = (MED1+MED2)/2;
	
	document.getElementById('incertidumbre').value = _RED2(FIN*2, 2);
}

function RAIZ(_num){
	return Math.sqrt(_num);
}