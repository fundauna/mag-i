////ARREGLAR FACTORES
var _IE = _IC = _MM1 = _MM2 = _MV1 = _MV2 = 0;

function __lolo() {
    document.getElementById('recipientemasa1').value = '28.7228';
    document.getElementById('recipientemasa2').value = '28.1156';
    document.getElementById('recipientemuestramasa1').value = '30.7306';
    document.getElementById('recipientemuestramasa2').value = '30.1253';
    document.getElementById('recipientefinalmasa1').value = '30.68';
    document.getElementById('recipientefinalmasa2').value = '30.0747';

    document.getElementById('repetibilidadval').value = '0.000125';
    document.getElementById('emrval').value = '';
    document.getElementById('resval').value = '0.001';
    document.getElementById('linealval').value = '0.00017';
    document.getElementById('excentricidadval').value = '0.000093';
    __calcula();
}

function datos() {
    if ($('#fechaA').val() == '') {
        OMEGA('Debe indicar la fecha del an�lisis');
        return;
    }

    if ($('#rango').val() == '') {
        OMEGA('Debe indicar la concentraci�n declarada');
        return;
    }

    if ($('#unidad').val() == '') {
        OMEGA('Debe seleccionar la unidad');
        return;
    }

    if (!confirm('Ingresar ensayo?')) return;

    if (document.getElementById('unidad').value == '0') {
        var lbl = 'm/m';
        var CD = document.getElementById('prom1').innerHTML;
        var IC = document.getElementById('prom2').innerHTML;
    } else {
        var lbl = 'm/v';
        var CD = document.getElementById('prom4').innerHTML;
        var IC = document.getElementById('prom5').innerHTML;
    }

    var parametros = {
        '_AJAX': 1,
        'xanalizar': $('#xanalizar').val(),
        'tipo': $('#tipo').val(),
        'fechaA': $('#fechaA').val(),
        'rango': $('#rango').val(),
        'unidad': $('#unidad').val(),
        'nitro1': $('#nitro1').val(),
        'nitro2': $('#nitro2').val(),
        'masa1': $('#masa1').val(),
        'masa2': $('#masa2').val(),
        'delta1': $('#delta1').val(),
        'delta2': $('#delta2').val(),
        'repetibilidad1': $('#repetibilidad1').val(),
        'resolucion1': $('#resolucion1').val(),
        'curva': $('#curva').val(),
        'repetibilidad2': $('#repetibilidad2').val(),
        'resolucion2': $('#resolucion2').val(),
        'inc_cer': $('#inc_cer').val(),
        'emp': $('#emp').val(),
        'CD': CD,
        'IC': IC,
        'obs': $('#obs').val()
    }

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    break;
                case '1':
                    opener.location.reload();
                    alert("Transaccion finalizada");
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function Redondear(txt) {
    _RED(txt, 4);
    __calcula();
}

function __calcula() {
    if (document.getElementById('unidad').value == '') return;


    let corregida1 = document.getElementById('recipientemasa1').value * (1 - (document.getElementById('recipienteerror1').value / 100));
    let corregida2 = document.getElementById('recipientemasa2').value * (1 - (document.getElementById('recipienteerror2').value / 100));
    let corregida3 = document.getElementById('recipientemuestramasa1').value * (1 - (document.getElementById('recipientemuestraerror1').value / 100));
    let corregida4 = document.getElementById('recipientemuestramasa2').value * (1 - (document.getElementById('recipientemuestraerror2').value / 100));
    let corregida5 = document.getElementById('recipientefinalmasa1').value * (1 - (document.getElementById('recipientefinalerror1').value / 100));
    let corregida6 = document.getElementById('recipientefinalmasa2').value * (1 - (document.getElementById('recipientefinalerror2').value / 100));

    document.getElementById('recipientemasacorregida1').value = corregida1.toFixed(4);
    document.getElementById('recipientemasacorregida2').value = corregida2.toFixed(4);
    document.getElementById('recipientemuestramasacorregida1').value = corregida3.toFixed(4);
    document.getElementById('recipientemuestramasacorregida2').value = corregida4.toFixed(4);
    document.getElementById('recipientefinalmasacorregida1').value = corregida5.toFixed(4);
    document.getElementById('recipientefinalmasacorregida2').value = corregida6.toFixed(4);

    document.getElementById('masarecipiente1').value = corregida1.toFixed(4);
    document.getElementById('masarecipiente2').value = corregida2.toFixed(4);
    document.getElementById('masarecipientepromedio').value = ((parseFloat(corregida1) + parseFloat(corregida2)) / 2).toFixed(4);
    document.getElementById('masainicialrecipiente1').value = corregida3.toFixed(4);
    document.getElementById('masainicialrecipiente2').value = corregida4.toFixed(4);
    document.getElementById('masainicialrecipientepromedio').value = ((parseFloat(corregida3) + parseFloat(corregida4)) / 2).toFixed(4);
    document.getElementById('masainicialmuestra1').value = document.getElementById('masainicialrecipiente1').value - document.getElementById('masarecipiente1').value;
    document.getElementById('masainicialmuestra2').value = document.getElementById('masainicialrecipiente2').value - document.getElementById('masarecipiente2').value;
    document.getElementById('masainicialmuestrapromedio').value = ((parseFloat(document.getElementById('masainicialmuestra1').value) + parseFloat(document.getElementById('masainicialmuestra2').value)) / 2).toFixed(4);
    document.getElementById('masafinalrecipiente1').value = corregida5.toFixed(4);
    document.getElementById('masafinalrecipiente2').value = corregida6.toFixed(4);
    document.getElementById('masafinalrecipientepromedio').value = ((parseFloat(corregida5) + parseFloat(corregida6)) / 2).toFixed(4);
    document.getElementById('masafinalmuestra1').value = document.getElementById('masafinalrecipiente1').value - document.getElementById('masarecipiente1').value;
    document.getElementById('masafinalmuestra2').value = document.getElementById('masafinalrecipiente2').value - document.getElementById('masarecipiente2').value;
    document.getElementById('masafinalmuestrapromedio').value = ((parseFloat(document.getElementById('masafinalmuestra1').value) + parseFloat(document.getElementById('masafinalmuestra2').value)) / 2).toFixed(4);
    document.getElementById('humedad1').value = (1 - (document.getElementById('masafinalmuestra1').value / document.getElementById('masainicialmuestra1').value)) * 100;
    document.getElementById('humedad2').value = (1 - (document.getElementById('masafinalmuestra2').value / document.getElementById('masainicialmuestra2').value)) * 100;
    document.getElementById('humedadpromedio').value = ((parseFloat(document.getElementById('humedad1').value) + parseFloat(document.getElementById('humedad2').value)) / 2).toFixed(4);
    document.getElementById('promediohumedad').value = ((parseFloat(document.getElementById('humedad1').value) + parseFloat(document.getElementById('humedad2').value)) / 2).toFixed(4);
    document.getElementById('humedaddesvest').value = 0;

    document.getElementById('repetibilidadcomp').value = document.getElementById('repetibilidadval').value / Math.sqrt(10);
    document.getElementById('rescomp').value = document.getElementById('resval').value / Math.sqrt(12);
    document.getElementById('linealcomp').value = document.getElementById('linealval').value / 2;
    document.getElementById('excentricidadcomp').value = document.getElementById('excentricidadval').value / 2;

}