////ARREGLAR FACTORES
var _IC = _CON = _CON2 = _INC4 = _INC7 = _MMA1 = _MMB1 = _MMA2 = _MMB2 = _PROM1 = _PROM2 = _DESVA = _DESVB = 0;

function __lolo() {
    document.getElementById('repetibilidad2').value = '10';
    document.getElementById('resolucion2').value = '8.12';
    document.getElementById('inc_cer').value = '0.00017';
    document.getElementById('emp').value = '0.000093';
    document.getElementById('na').value = '0.1015';
    document.getElementById('valorado').value = '5.02';
    document.getElementById('alicuota').value = '25';
    document.getElementById('balon').value = '100';
    document.getElementById('inc2').value = '0.0001';
    document.getElementById('inc3').value = '0.05';
    document.getElementById('inc5').value = '0.00015';
    document.getElementById('masaA1').value = '0.3564';
    document.getElementById('masaB1').value = '6.15';
    document.getElementById('masaA2').value = '0.3714';
    document.getElementById('masaB2').value = '6.23';
    __calcula();
}

function datos() {
    if ($('#fechaA').val() == '') {
        OMEGA('Debe indicar la fecha del an�lisis');
        return;
    }

    if ($('#rango').val() == '') {
        OMEGA('Debe indicar la concentraci�n declarada');
        return;
    }

    if ($('#unidad').val() == '') {
        OMEGA('Debe seleccionar la unidad');
        return;
    }

    if (!confirm('Ingresar ensayo?')) return;

    if (document.getElementById('unidad').value == '0') {
        var CD = document.getElementById('promA').innerHTML;
        var IC = document.getElementById('suma3').innerHTML;
    } else {
        var CD = document.getElementById('promB').innerHTML;
        var IC = document.getElementById('suma4').innerHTML;
    }

    var ic_balanza = document.getElementById('inc_balanza').innerHTML;
    var ic_combinada = document.getElementById('inc_Combinada').innerHTML;

    var parametros = {
        '_AJAX': 1,
        'xanalizar': $('#xanalizar').val(),
        'tipo': $('#tipo').val(),
        'fechaA': $('#fechaA').val(),
        'rango': $('#rango').val(),
        'unidad': $('#unidad').val(),
        'na': $('#na').val(),
        'molar': $('#molar').val(),
        'valorado': $('#valorado').val(),
        'densidad': $('#densidad').val(),
        'alicuota': $('#alicuota').val(),
        'balon': $('#balon').val(),
        'inc2': $('#inc2').val(),
        'inc3': $('#inc3').val(),
        'inc5': $('#inc5').val(),
        'repetibilidad2': $('#repetibilidad2').val(),
        'resolucion2': $('#resolucion2').val(),
        'inc_cer': $('#inc_cer').val(),
        'emp': $('#emp').val(),
        'masaA1': $('#masaA1').val(),
        'masaA2': $('#masaA2').val(),
        'masaB1': $('#masaB1').val(),
        'masaB2': $('#masaB2').val(),
        'des1': $('#des1').val(),
        'val6': $('#val6').val(),
        'des6': $('#des6').val(),
        'CD': CD,
        'IC': IC,
        'obs': $('#obs').val(),
        'ic_balanza': ic_balanza,
        'ic_combinada': ic_combinada,
        'certificadoD': $('#certificado').val(),
        'resolucionD': $('#resolucion1').val(),
        'repetibilidadM': $('#repetibilidad').val(),
        'resolucionM': $('#resolucion').val(),
        'linealidad': $('#inc_cer').val(),
        'excentricidad': $('#emp').val(),
    }

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    break;
                case '1':
                    opener.location.reload();
                    alert("Transaccion finalizada");
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function Redondear(txt) {
    _RED(txt, 6);
    __calcula();
}

function combos() {
    var inc3 = document.getElementById(cbinc3).value;
}


function __calcula() {
    if (document.getElementById('unidad').value == '') return;
    //
    //__combos();
    //__balanza();
    //
    //SACA LOS DATOS DE LA BALANZA
    N4 = document.getElementById('repetibilidad').value / Math.sqrt(10);
    document.getElementById('rep_comp').innerHTML = _RED2(N4, 5);
    //
    N5 = document.getElementById('resolucion').value / Math.sqrt(12);
    document.getElementById('res_comp').innerHTML = _RED2(N5, 5);
    //
    N6 = document.getElementById('inc_cer').value / 2;
    document.getElementById('cer_comp').innerHTML = _RED2(N6, 6);
    //
    N7 = document.getElementById('emp').value / 2;
    document.getElementById('emp_comp').innerHTML = _RED2(N7, 4);
    //
    _IC = Math.sqrt(parseFloat(Math.pow(N4, 2)) + parseFloat(Math.pow(N5, 2)) + parseFloat(Math.pow(N6, 2)) + parseFloat(Math.pow(N7, 2)));
    document.getElementById('inc_balanza').innerHTML = _RED2(_IC, 6);


    N8 = document.getElementById('certificado').value / 2;
    document.getElementById('lcertificado').innerHTML = _RED2(N8, 5);
    N9 = document.getElementById('resolucion1').value / Math.sqrt(12);
    document.getElementById('lresolucion1').innerHTML = _RED2(N8, 5);

    _IC_Combinada = Math.sqrt(parseFloat(Math.pow(N8, 2)) + parseFloat(Math.pow(N9, 2)));
    document.getElementById('inc_Combinada').innerHTML = _RED2(_IC_Combinada, 6);


    //calcula normalidad naoh
    _CON = document.getElementById('na').value / document.getElementById('molar').value / document.getElementById('valorado').value * 1000;

    // calcula normalidad acido sulfurico h2so4
    _CON2 = _CON * document.getElementById('repetibilidad2').value / document.getElementById('resolucion2').value;

    _valor = '';
    _inceVol = $('#cbinc3').val();
    if (_inceVol == 10) {
        _valor = '0.025';
    } else if (_inceVol == 25) {
        _valor = '0.03';
    } else if (_inceVol == 50) {
        _valor = '0.05';
    } else if (_inceVol == 100) {
        _valor = '0.10';
    }

    /*
        if($('#cbinc3').val() == 10) {
            document.getElementById('inc3').innerHTML = '0.025';
        }else if($('#cbinc3').val() == 25){
            document.getElementById('inc3').innerHTML = '0.03';
        } else if($('#cbinc3').val() == 50)  {
            document.getElementById('inc3').innerHTML = '0.05';
        } else if($('#cbinc3').val() == 100)  {
            document.getElementById('inc3').innerHTML = '0.10';
        }
        */


    _incePip = '';
    _inceValorado = $('#repetibilidad2').val();

    if (_inceValorado <= 10) {
        _incePip = '0.025';
        document.getElementById('inc5').innerHTML = _incePip;
    } else if ((_inceValorado > 10) && (_inceValorado <= 25)) {
        _incePip = '0.03';
        document.getElementById('inc5').innerHTML = _incePip;
    } else if ((_inceValorado > 25) && (_inceValorado <= 50)) {
        _incePip = '0.05';
        document.getElementById('inc5').innerHTML = _incePip;
    } else if ((_inceValorado > 50) && (_inceValorado <= 100)) {
        _incePip = '0.10';
        document.getElementById('inc5').innerHTML = _incePip;
    }


    _valorVolumenConsumido = '';
    _inceCons = $('#cbinc6').val();

    if (_inceCons == 10) {
        _valorVolumenConsumido = '0.025';
    } else if (_inceCons == 25) {
        _valorVolumenConsumido = '0.03';
    } else if (_inceCons == 50) {
        _valorVolumenConsumido = '0.05';
    } else if (_inceCons == 100) {
        _valorVolumenConsumido = '0.10';
    }


    //incertidumbre de la normalidad del naoh                       //=RAIZ(POTENCIA(N4;2)+POTENCIA(N5;2)+POTENCIA(N6;2)+POTENCIA(N7;2))
    _INC4 = _CON * Math.sqrt(parseFloat(Math.pow(_IC / document.getElementById('na').value, 2)) + parseFloat(Math.pow(0.0036 / document.getElementById('molar').value, 2)) + Math.pow(_valor / (Math.sqrt(6) * document.getElementById('valorado').value), 2));


    // raiz (  0.000053�2/raiz10   +   0.0001�2   raiz 12         + 0,000065/2)
    // _INC4 = _CON * Math.sqrt(parseFloat(Math.pow(0.00011 / document.getElementById('na').value, 2)) + parseFloat(Math.pow(0.0036 / document.getElementById('molar').value, 2)) + Math.pow(0.05 / (Math.sqrt(6) * document.getElementById('valorado').value), 2));


    _INC7 = _CON2 * Math.sqrt(parseFloat(Math.pow(_INC4 / _CON, 2)) + parseFloat(Math.pow(_incePip / document.getElementById('repetibilidad2').value / Math.sqrt(6), 2)) + parseFloat(Math.pow(_valorVolumenConsumido / document.getElementById('resolucion2').value / Math.sqrt(6), 2)));


    _MMA1 = (document.getElementById('masaB1').value * _CON2 * 14.007 * 100 * document.getElementById('balon').value / document.getElementById('alicuota').value) / (document.getElementById('masaA1').value * 1000);
    _MMB1 = (document.getElementById('masaB1').value * _CON2 * 14.007 * 100 * document.getElementById('balon').value / document.getElementById('alicuota').value * document.getElementById('densidad').value) / (document.getElementById('masaA1').value * 1000);
    _MMA2 = (document.getElementById('masaB2').value * _CON2 * 14.007 * 100 * document.getElementById('balon').value / document.getElementById('alicuota').value) / (document.getElementById('masaA2').value * 1000);
    _MMB2 = (document.getElementById('masaB2').value * _CON2 * 14.007 * 100 * document.getElementById('balon').value / document.getElementById('alicuota').value * document.getElementById('densidad').value) / (document.getElementById('masaA2').value * 1000);
    _PROM1 = (_MMA1 + _MMA2) / 2;
    _PROM2 = (_MMB1 + _MMB2) / 2;
    //
    var fVarianceA = fVarianceB = 0;
    fVarianceA += parseFloat(Math.pow(_MMA1 - _PROM1, 2));
    fVarianceA += parseFloat(Math.pow(_MMA2 - _PROM1, 2));
    fVarianceB += parseFloat(Math.pow(_MMB1 - _PROM2, 2));
    fVarianceB += parseFloat(Math.pow(_MMB2 - _PROM2, 2));
    _DESVA = Math.sqrt(fVarianceA) / Math.sqrt(1);
    _DESVB = Math.sqrt(fVarianceB) / Math.sqrt(1);

    document.getElementById('con').innerHTML = _RED2(_CON, 5);
    document.getElementById('con2').innerHTML = _RED2(_CON2, 4);
    document.getElementById('inc1').innerHTML = _RED2(_IC, 6);
    document.getElementById('inc2').innerHTML = '0.0036';

    ///********************************************


    if ($('#cbinc3').val() == 10) {
        document.getElementById('inc3').innerHTML = '0.025';
    } else if ($('#cbinc3').val() == 25) {
        document.getElementById('inc3').innerHTML = '0.03';
    } else if ($('#cbinc3').val() == 50) {
        document.getElementById('inc3').innerHTML = '0.05';
    } else if ($('#cbinc3').val() == 100) {
        document.getElementById('inc3').innerHTML = '0.10';
    }


    if ($('#cbinc5').val() == 10) {
        document.getElementById('inc5').innerHTML = '0.025';
    } else if ($('#cbinc5').val() == 25) {
        document.getElementById('inc5').innerHTML = '0.03';
    } else if ($('#cbinc5').val() == 50) {
        document.getElementById('inc5').innerHTML = '0.05';
    } else if ($('#cbinc5').val() == 100) {
        document.getElementById('inc5').innerHTML = '0.10';
    }

    if ($('#cbinc6').val() == 10) {
        document.getElementById('inc6').innerHTML = '0.025';
    } else if ($('#cbinc6').val() == 25) {
        document.getElementById('inc6').innerHTML = '0.03';
    } else if ($('#cbinc6').val() == 50) {
        document.getElementById('inc6').innerHTML = '0.05';
    } else if ($('#cbinc6').val() == 100) {
        document.getElementById('inc6').innerHTML = '0.10';
    }


    /*if(document.getElementById('valorado').value <= 10 ){
        document.getElementById('inc3').innerHTML = '0.025';
    } else if((document.getElementById('valorado').value > 10) && (document.getElementById('valorado').value <= 25)) {
        document.getElementById('inc3').innerHTML = '0.03';
    } else if((document.getElementById('valorado').value > 25) && (document.getElementById('valorado').value <= 50)) {
        document.getElementById('inc3').innerHTML = '0.05';
    } else if((document.getElementById('valorado').value > 50) && (document.getElementById('valorado').value <= 100)) {
        document.getElementById('inc3').innerHTML = '0.10';
    } else {
        document.getElementById('inc3').innerHTML = 'error';
    }*/

///////////////*************************************

    // document.getElementById('inc3').innerHTML = '0.05';
    document.getElementById('inc4').innerHTML = _RED2(_INC4, 5);

    /*
        if(document.getElementById('repetibilidad2').value < 1 ){
            document.getElementById('inc5').innerHTML = '0.006';
        } else if(document.getElementById('repetibilidad2').value == 1)  {
            document.getElementById('inc5').innerHTML = '0.005';
        } else if((document.getElementById('repetibilidad2').value >= 2) &&(document.getElementById('repetibilidad2').value <= 5)) {
            document.getElementById('inc5').innerHTML = '0.01';
        } else if((document.getElementById('repetibilidad2').value >= 6) &&(document.getElementById('repetibilidad2').value <= 10)) {
            document.getElementById('inc5').innerHTML = '0.02';
        } else if((document.getElementById('repetibilidad2').value >= 11) &&(document.getElementById('repetibilidad2').value <= 25)) {
            document.getElementById('inc5').innerHTML = '0.03';
        } else if((document.getElementById('repetibilidad2').value >= 26) &&(document.getElementById('repetibilidad2').value <= 50)) {
            document.getElementById('inc5').innerHTML = '0.05';
        } else if((document.getElementById('repetibilidad2').value >= 51) &&(document.getElementById('repetibilidad2').value <= 100))  {
            document.getElementById('inc5').innerHTML = '0.08';
        } else if((document.getElementById('repetibilidad2').value > 100))  {
            document.getElementById('inc5').innerHTML = 'error';
        }


        if(document.getElementById('resolucion2').value <= 10 ){
            document.getElementById('inc6').innerHTML = '0.025';
        } else if((document.getElementById('resolucion2').value > 10) && (document.getElementById('resolucion2').value <= 25)) {
        document.getElementById('inc6').innerHTML = '0.03';
        } else if((document.getElementById('resolucion2').value > 25) && (document.getElementById('resolucion2').value <= 50)) {
            document.getElementById('inc6').innerHTML = '0.05';
        } else if((document.getElementById('resolucion2').value > 50) && (document.getElementById('resolucion2').value <= 100)) {
            document.getElementById('inc6').innerHTML = '0.10';
        } else {
            document.getElementById('inc6').innerHTML = 'error';
        }
    */

    document.getElementById('inc7').innerHTML = _RED2(_INC7, 5);


//


    document.getElementById('inc8').innerHTML = _RED2(_IC_Combinada, 5);

    if (document.getElementById('alicuota').value < 1) {
        document.getElementById('inc9').innerHTML = '0.006';
    } else if (document.getElementById('alicuota').value == 1) {
        document.getElementById('inc9').innerHTML = '0.005';
    } else if ((document.getElementById('alicuota').value >= 2) && (document.getElementById('alicuota').value <= 5)) {
        document.getElementById('inc9').innerHTML = '0.01';
    } else if ((document.getElementById('alicuota').value >= 6) && (document.getElementById('alicuota').value <= 10)) {
        document.getElementById('inc9').innerHTML = '0.02';
    } else if ((document.getElementById('alicuota').value >= 11) && (document.getElementById('alicuota').value <= 25)) {
        document.getElementById('inc9').innerHTML = '0.03';
    } else if ((document.getElementById('alicuota').value >= 26) && (document.getElementById('alicuota').value <= 50)) {
        document.getElementById('inc9').innerHTML = '0.05';
    } else {
        document.getElementById('inc9').innerHTML = '0.08';
    }

    // document.getElementById('inc9').innerHTML = document.getElementById('alicuota').value < 1 ? '0.006': document.getElementById('alicuota').value = 1 ? '0.005':'fuera rango';
    //  document.getElementById('inc10').innerHTML = document.getElementById('balon').value;

    if (document.getElementById('balon').value == 5) {
        document.getElementById('inc10').innerHTML = '0.025';
    } else if (document.getElementById('balon').value == 10) {
        document.getElementById('inc10').innerHTML = '0.025';
    } else if (document.getElementById('balon').value == 25) {
        document.getElementById('inc10').innerHTML = '0.03';
    } else if (document.getElementById('balon').value == 50) {
        document.getElementById('inc10').innerHTML = '0.05';
    } else if (document.getElementById('balon').value == 100) {
        document.getElementById('inc10').innerHTML = '0.08';
    } else if (document.getElementById('balon').value == 200) {
        document.getElementById('inc10').innerHTML = '0.1';
    } else if (document.getElementById('balon').value == 250) {
        document.getElementById('inc10').innerHTML = '0.11';
    } else if (document.getElementById('balon').value == 300) {
        document.getElementById('inc10').innerHTML = '0.12';
    } else if (document.getElementById('balon').value == 500) {
        document.getElementById('inc10').innerHTML = '0.15';
    } else if (document.getElementById('balon').value == 1000) {
        document.getElementById('inc10').innerHTML = '0.3';
    } else if (document.getElementById('balon').value == 2000) {
        document.getElementById('inc10').innerHTML = '0.5';
    } else {
        'error';
    }

    document.getElementById('mmA1').innerHTML = _RED2(_MMA1, 2);
    document.getElementById('mmA2').innerHTML = _RED2(_MMA2, 2);
    document.getElementById('mmB1').innerHTML = _RED2(_MMB1, 2);
    document.getElementById('mmB2').innerHTML = _RED2(_MMB2, 2);
    document.getElementById('promA').innerHTML = _RED2(_PROM1, 2);
    document.getElementById('promB').innerHTML = _RED2(_PROM2, 2);
    document.getElementById('desvA').innerHTML = _RED2(_DESVA, 2);
    document.getElementById('desvB').innerHTML = _RED2(_DESVB, 2);

    document.getElementById('val1').innerHTML = _RED2((parseFloat(document.getElementById('masaB1').value) + parseFloat(document.getElementById('masaB2').value)) / 2, 2);
    document.getElementById('val2').innerHTML = _RED2(_CON2, 4);
    document.getElementById('val3').innerHTML = _RED2((parseFloat(document.getElementById('masaA1').value) + parseFloat(document.getElementById('masaA2').value)) / 2, 2);
    if (document.getElementById('alicuota').value < 1) {
        document.getElementById('val4').innerHTML = document.getElementById('alicuota').value;
    } else {
        document.getElementById('val4').innerHTML = document.getElementById('alicuota').value + '.00';
    }

    document.getElementById('val5').innerHTML = document.getElementById('balon').value + '.00';
    document.getElementById('val7').innerHTML = document.getElementById('unidad').value == 0 ? document.getElementById('desvA').innerHTML : document.getElementById('desvB').innerHTML;
    document.getElementById('val8').innerHTML = document.getElementById('densidad').value;

    document.getElementById('des2').innerHTML = _RED2(_INC7, 5);
    document.getElementById('des3').innerHTML = document.getElementById('inc1').innerHTML;
    document.getElementById('des4').innerHTML = document.getElementById('inc9').innerHTML;
    document.getElementById('des5').innerHTML = document.getElementById('inc10').innerHTML;
    document.getElementById('des7').innerHTML = _RED2(document.getElementById('val7').innerHTML / Math.sqrt(2), 2);
    document.getElementById('des8').innerHTML = _RED2(_IC_Combinada, 5);

    document.getElementById('est1').innerHTML = _RED2(document.getElementById('des1').value / Math.sqrt(6), 4);
    document.getElementById('est2').innerHTML = document.getElementById('des2').innerHTML;
    document.getElementById('est3').innerHTML = document.getElementById('des3').innerHTML;
    document.getElementById('est4').innerHTML = _RED2(document.getElementById('des4').innerHTML / Math.sqrt(6), 4);
    document.getElementById('est5').innerHTML = _RED2(document.getElementById('des5').innerHTML / Math.sqrt(6), 4);
    document.getElementById('est6').innerHTML = document.getElementById('des6').value;
    document.getElementById('est7').innerHTML = document.getElementById('des7').innerHTML;
    document.getElementById('est8').innerHTML = document.getElementById('des8').innerHTML;

    lolo1 = Math.pow((document.getElementById('est1').innerHTML) / (document.getElementById('val1').innerHTML), 2);
    jk = '' + lolo1;
    for (z = 0; z < jk.length; z++) {
        if (jk[z] != '0' && jk[z] != '.') {
            jk = z;
            break;
        }
    }
    if (jk == 0) jk = 10;
    document.getElementById('cua1').innerHTML = _RED2(lolo1, jk);
    lolo2 = Math.pow((document.getElementById('est2').innerHTML) / (document.getElementById('val2').innerHTML), 2);
    jk = '' + lolo2;
    for (z = 0; z < jk.length; z++) {
        if (jk[z] != '0' && jk[z] != '.') {
            jk = z;
            break;
        }
    }
    if (jk == 0) jk = 10;
    document.getElementById('cua2').innerHTML = _RED2(lolo2, jk);
    lolo3 = Math.pow((document.getElementById('est3').innerHTML) / (document.getElementById('val3').innerHTML), 2);
    jk = '' + lolo3;
    for (z = 0; z < jk.length; z++) {
        if (jk[z] != '0' && jk[z] != '.') {
            jk = z;
            break;
        }
    }
    if (jk == 0) jk = 10;
    document.getElementById('cua3').innerHTML = _RED2(lolo3, jk);
    lolo4 = Math.pow((document.getElementById('est4').innerHTML) / (document.getElementById('val4').innerHTML), 2);
    jk = '' + lolo4;
    for (z = 0; z < jk.length; z++) {
        if (jk[z] != '0' && jk[z] != '.') {
            jk = z;
            break;
        }
    }
    if (jk == 0) jk = 10;
    document.getElementById('cua4').innerHTML = _RED2(lolo4, jk);
    lolo5 = Math.pow((document.getElementById('est5').innerHTML) / (document.getElementById('val5').innerHTML), 2);
    jk = '' + lolo5;
    for (z = 0; z < jk.length; z++) {
        if (jk[z] != '0' && jk[z] != '.') {
            jk = z;
            break;
        }
    }
    if (jk == 0) jk = 10;
    document.getElementById('cua5').innerHTML = _RED2(lolo5, jk);
    lolo6 = Math.pow((document.getElementById('est6').innerHTML) / (document.getElementById('val6').value), 2);
    jk = '' + lolo6;
    for (z = 0; z < jk.length; z++) {
        if (jk[z] != '0' && jk[z] != '.') {
            jk = z;
            break;
        }
    }
    if (jk == 0) jk = 13;
    document.getElementById('cua6').innerHTML = _RED2(lolo6, jk);
    lolo7 = Math.pow((document.getElementById('est7').innerHTML), 2);
    jk = '' + lolo7;
    for (z = 0; z < jk.length; z++) {
        if (jk[z] != '0' && jk[z] != '.') {
            jk = z;
            break;
        }
    }
    if (jk == 0) jk = 10;
    document.getElementById('cua7').innerHTML = _RED2(lolo7, jk);
    lolo8 = Math.pow((document.getElementById('est8').innerHTML) / (document.getElementById('val8').innerHTML), 2);
    jk = '' + lolo8;
    for (z = 0; z < jk.length; z++) {
        if (jk[z] != '0' && jk[z] != '.') {
            jk = z;
            break;
        }
    }
    if (jk == 0) jk = 10;
    document.getElementById('cua8').innerHTML = _RED2(lolo8, jk);


    var suma1 = parseFloat(lolo1) + parseFloat(lolo2) + parseFloat(lolo3) + parseFloat(lolo4) + parseFloat(lolo5) + parseFloat(lolo6);
    var suma2 = suma1 + parseFloat(lolo8);
    document.getElementById('suma1').innerHTML = _RED2(document.getElementById('promA').innerHTML * Math.sqrt(suma1), 4);
    document.getElementById('suma2').innerHTML = _RED2(document.getElementById('promB').innerHTML * Math.sqrt(suma2), 4);

    var temprom = Math.sqrt(parseFloat(Math.pow(document.getElementById('suma1').innerHTML / Math.sqrt(2), 2)) + parseFloat(Math.pow(document.getElementById('desvA').innerHTML / Math.sqrt(2), 2)));
    document.getElementById('suma3').innerHTML = _RED2(temprom, 2);
    var temprom = Math.sqrt(parseFloat(Math.pow(document.getElementById('suma2').innerHTML / Math.sqrt(2), 2)) + parseFloat(Math.pow(document.getElementById('desvB').innerHTML / Math.sqrt(2), 2)));
    document.getElementById('suma4').innerHTML = _RED2(temprom, 2);

    document.getElementById('suma5').innerHTML = _RED2(document.getElementById('suma3').innerHTML * 2, 4);
    document.getElementById('suma6').innerHTML = _RED2(document.getElementById('suma4').innerHTML * 2, 4);


}

function __balanza() {
    N4 = document.getElementById('repetibilidad2').value / Math.sqrt(10);
    document.getElementById('rep_comp2').innerHTML = _RED2(N4, 5);
    //
    N5 = document.getElementById('resolucion2').value / Math.sqrt(3);
    document.getElementById('res_comp2').innerHTML = _RED2(N5, 5);
    //
    N6 = document.getElementById('inc_cer').value / 2;
    document.getElementById('cer_comp').innerHTML = _RED2(N6, 6);
    //
    N7 = document.getElementById('emp').value / Math.sqrt(3);
    document.getElementById('emp_comp').innerHTML = _RED2(N7, 4);
    //
    _IC = Math.sqrt(parseFloat(Math.pow(N4, 2)) + parseFloat(Math.pow(N5, 2)) + parseFloat(Math.pow(N6, 2)) + parseFloat(Math.pow(N7, 2)));
    document.getElementById('inc_balanza').innerHTML = _RED2(_IC, 5);
    document.getElementById('inc1').innerHTML = _RED2(document.getElementById('na').value * _IC, 5);
    document.getElementById('des3').innerHTML = _RED2(document.getElementById('na').value * _IC, 5);
}

function __combos() {
    //OBTIENE INC DE LA ALICUOTA
    control = document.getElementById('alicuota');
    var inc = control.options[control.selectedIndex].getAttribute('inc');
    document.getElementById('inc6').innerHTML = inc;
    document.getElementById('val4').innerHTML = document.getElementById('alicuota').value;
    document.getElementById('des4').innerHTML = inc;
    //OBTIENE INC DEL BALON
    control = document.getElementById('balon');
    var inc = control.options[control.selectedIndex].getAttribute('inc');
    document.getElementById('inc7').innerHTML = inc;
    document.getElementById('val5').innerHTML = document.getElementById('balon').value;
    document.getElementById('des5').innerHTML = inc;
    //
    document.getElementById('val8').innerHTML = document.getElementById('densidad').value;
    document.getElementById('des8').innerHTML = document.getElementById('inc5').value;
    document.getElementById('est8').innerHTML = _RED2(document.getElementById('inc5').value / 2, 3);
}