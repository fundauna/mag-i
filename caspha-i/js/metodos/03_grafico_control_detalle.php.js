function datos(id){
		
	if(confirm('Guardar observacion?')){
		var parametros = {
			'_AJAX' : 1,
			'id' : id,
			'observacion' : $('#observacion').val()
		};
		
		$.ajax({
			data:  parametros,
			url:   __SHELL__,
			type:  'post',
			beforeSend: function () {
				ALFA('Por favor espere....');
			},
			success: function (_response) {
				switch(_response){
					case '-0':alert('Sesi�n expirada [Err:0]');break;
					case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
					case '0':
						OMEGA('Error transaccional');
						break;
					case '1':
						GAMA('Transaccion finalizada');
						break;
					default:alert('Tiempo de espera agotado');break;
				}
			}
		});
	}
}

function GeneraGrafico(_ING, _LA, _LM, _LB){
	document.getElementById('tr_grafico').style.display = '';

	$(function () {
    $('#container').highcharts({
		data: {
            table: 'datatable'
        },
		plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
				}
			}
        },
        title: {
            text: 'Grafico de control en el analisis de ingrediente activo en formulaciones de plaguicidas',
        },
        subtitle: {
            text: 'Ingrediente Activo: '+_ING,
        },
        xAxis: {
			title: {
                text: 'CONTROL NUMERO'
            }
        },
        yAxis: {			
			max: (_LA+1),
            title: {
                text: 'DESV. EST'
            },
			plotLines: [/*{
				color: 'red',
				value: _LA, 
				width: 2
			},*/{
				color: 'blue', 
				value: _LM, 
				width: 2    
			}/*,{
				color: 'green', 
				value: _LB, 
				width: 2     
			}*/]
        }
    });
});
}

function Docimas(){
	window.open("03_grafico_control_docimas.php","","width=850,height=700,scrollbars=yes,status=no");
}