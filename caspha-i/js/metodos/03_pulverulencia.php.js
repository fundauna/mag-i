var _decimales = 4;

function __lolo(){
	document.getElementById('masa').value = '51';
	document.getElementById('volumen').value = '99.5';
	document.getElementById('tamiz1').value = '1';
	document.getElementById('tamiz2').value = '2';
	document.getElementById('tamiz3').value = '3';
	document.getElementById('tamiz4').value = '4';
	document.getElementById('tamiz5').value = '5';
	__calcula();
}

function __calcula(){	
	var tam2 = 100*document.getElementById('tamiz2').value/document.getElementById('tamiz1').value;
	var tam3 = 100*document.getElementById('tamiz3').value/document.getElementById('tamiz1').value;
	var tam4 = 100*document.getElementById('tamiz4').value/document.getElementById('tamiz1').value;
	var tam5 = 100*document.getElementById('tamiz5').value/document.getElementById('tamiz1').value;
	var perdida1 = document.getElementById('tamiz1').value - (parseFloat(document.getElementById('tamiz2').value)+parseFloat(document.getElementById('tamiz3').value)+parseFloat(document.getElementById('tamiz4').value)+parseFloat(document.getElementById('tamiz5').value));
	var perdida2 = 100*perdida1/document.getElementById('tamiz1').value;
	var total = tam2 + tam3 + tam4 + tam5 + perdida2;
	var acumulativo = tam4 + tam5 + perdida2;
	var fraccion = tam5 + perdida2;
	
	document.getElementById('porc2').innerHTML = tam2.toFixed(2);
	document.getElementById('porc3').innerHTML = tam3.toFixed(2);
	document.getElementById('porc4').innerHTML = tam4.toFixed(2);
	document.getElementById('porc5').innerHTML = tam5.toFixed(2);
	document.getElementById('perdida1').innerHTML = perdida1.toFixed(2);
	document.getElementById('perdida2').innerHTML = perdida2.toFixed(2);
	document.getElementById('total').innerHTML = total.toFixed(2);
	document.getElementById('acumulativo').innerHTML = acumulativo.toFixed(2);
	document.getElementById('fraccion').innerHTML = fraccion.toFixed(2);
}

function datos(){	
	/*if($('#ingrediente').val()==''){
		OMEGA('Debe indicar el ingrediente activo');
		return;
	}*/
	
	if($('#fechaA').val()==''){
		OMEGA('Debe indicar la fecha del an�lisis');
		return;
	}
		
	if(!confirm('Modificar datos?')) return;

	var parametros = {
		'_AJAX' : 1,
		'xanalizar' : $('#xanalizar').val(),
		'tipo' : $('#tipo').val(),
		'ingrediente' : $('#ingrediente').val(),
		'fechaA' : $('#fechaA').val(),
		'tipo_form' : $('#tipo_form').val(),
		'masa' : $('#masa').val(),
		'volumen' : $('#volumen').val(),
		'tamiz1' : $('#tamiz1').val(),
		'tamiz2' : $('#tamiz2').val(),
		'tamiz3' : $('#tamiz3').val(),
		'tamiz4' : $('#tamiz4').val(),
		'tamiz5' : $('#tamiz5').val(),
		'acumulativo' : document.getElementById('acumulativo').innerHTML,
		'fraccion' : document.getElementById('fraccion').innerHTML,
		'obs' : $('#obs').val()	
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					opener.location.reload();
					alert("Transaccion finalizada");
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Redondear(txt){
	_RED(txt, _decimales);
	__calcula();
}

function densidadAparente(){
	document.getElementById('aparente').innerHTML = _RED2(document.getElementById('masa').value / document.getElementById('volumen').value,4);
}