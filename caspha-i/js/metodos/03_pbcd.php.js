var _IC = masa_normal1 = masa_normal2 = volu_normal1 = volu_normal2 = kilo_normal1 = kilo_normal2 = lito_normal1 = lito_normal2 = 0;
var masa_delta1 = masa_delta2 = volu_delta1 = volu_delta2 = kilo_delta1 = kilo_delta2 = 0;

function __lolo(){
	document.getElementById('balon1').selectedIndex=3;
	document.getElementById('balon2').selectedIndex=3;
	document.getElementById('repetibilidad').value = '0.00012';
	document.getElementById('resolucion').value = '0.0001';
	document.getElementById('inc_cer').value = '0.00017';
	document.getElementById('emp').value = '0.000093';
	document.getElementById('masa1').value = '0.5012';
	document.getElementById('cn1').value = '0.00145';
	document.getElementById('dumbre1').value = '0.00001';
	document.getElementById('delta1').value = '1';
	document.getElementById('certi1').value = '0.01';
	document.getElementById('masa2').value = '0.5112';
	document.getElementById('cn2').value = '0.00112';
	document.getElementById('dumbre2').value = '0.00001';
	document.getElementById('delta2').value = '1';
	document.getElementById('certi2').value = '0.1';
	__calcula();
}

function datos(){	
	if($('#fechaA').val()==''){
		OMEGA('Debe indicar la fecha del an�lisis');
		return;
	}
	
	if(!confirm('Ingresar ensayo?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'xanalizar' : $('#xanalizar').val(),
		'tipo' : $('#tipo').val(),
		'fechaA' : $('#fechaA').val(),
		'balon1' : $('#balon1').val(),
		'balon2' : $('#balon2').val(),	
		'pipetaD1' : $('#pipetaD1').val(),
		'pipetaD2' : $('#pipetaD2').val(),
		'pipetaD3' : $('#pipetaD3').val(),
		'balonD1' : $('#balonD1').val(),
		'balonD2' : $('#balonD2').val(),
		'balonD3' : $('#balonD3').val(),
		'repetibilidad' : $('#repetibilidad').val(),
		'resolucion' : $('#resolucion').val(),
		'inc_cer' : $('#inc_cer').val(),
		'emp' : $('#emp').val(),
		'masa1' : $('#masa1').val(),
		'masa2' : $('#masa2').val(),
		'cn1' : $('#cn1').val(),
		'cn2' : $('#cn2').val(),
		'dumbre1' : $('#dumbre1').val(),
		'dumbre2' : $('#dumbre2').val(),
		'delta1' : $('#delta1').val(),
		'delta2' : $('#delta2').val(),
		'certi1' : $('#certi1').val(),
		'certi2' : $('#certi2').val(),
		'prom_kilo' : document.getElementById('prom_kilo').innerHTML,
		'prom_inc' : document.getElementById('prom_inc').innerHTML,
		'obs' : $('#obs').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
                    console.log(_response);
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					opener.location.reload();
					alert("Transaccion finalizada");
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Redondear(txt){
	_RED(txt, 6);
	__calcula();
}

function __calcula(){
	var joker = 0;
	//if(document.getElementById('unidad').value == '') return;
	
	__combos();
	//SACA LOS DATOS DE LA BALANZA
	N4 = document.getElementById('repetibilidad').value / Math.sqrt(10);
	document.getElementById('rep_comp').innerHTML = _RED2(N4, 5);
	//
	N5 = document.getElementById('resolucion').value / Math.sqrt(12);
	document.getElementById('res_comp').innerHTML = _RED2(N5, 5);
	//
	N6 = document.getElementById('inc_cer').value / 2;
	document.getElementById('cer_comp').innerHTML = _RED2(N6, 6);
	//
	//N7 = document.getElementById('emp').value / Math.sqrt(3);
	N7 = document.getElementById('emp').value / 2;
	document.getElementById('emp_comp').innerHTML = _RED2(N7, 6);
	//
	_IC = Math.sqrt(parseFloat(Math.pow(N4,2)) + parseFloat(Math.pow(N5,2)) + parseFloat(Math.pow(N6,2)) + parseFloat(Math.pow(N7,2)) );
	document.getElementById('inc_balanza').innerHTML = _RED2(_IC, 6);
	//RESULTADOS PARCIALES
	masa_normal1 = (document.getElementById('cn1').value*document.getElementById('balon1').value*(document.getElementById('balonD1').value/document.getElementById('pipetaD1').value)*(document.getElementById('balonD2').value/document.getElementById('pipetaD2').value)*(document.getElementById('balonD3').value/document.getElementById('pipetaD3').value))/(10000*document.getElementById('masa1').value);
	masa_normal2 = (document.getElementById('cn2').value*document.getElementById('balon2').value*(document.getElementById('balonD1').value/document.getElementById('pipetaD1').value)*(document.getElementById('balonD2').value/document.getElementById('pipetaD2').value)*(document.getElementById('balonD3').value/document.getElementById('pipetaD3').value))/(10000*document.getElementById('masa2').value);
	volu_normal1 = masa_normal1*document.getElementById('delta1').value;
	volu_normal2 = masa_normal2*document.getElementById('delta2').value;
	kilo_normal1 = masa_normal1*10000;
	kilo_normal2 = masa_normal2*10000;
	lito_normal1 = kilo_normal1*document.getElementById('delta1').value;
	lito_normal2 = kilo_normal2*document.getElementById('delta2').value;
	//
	__BORO_CALCIO(6);
	//
	prom_masa = (masa_normal1 + masa_normal2) / 2;
	prom_volu = (volu_normal1 + volu_normal2) / 2;
	prom_kilo = (kilo_normal1 + kilo_normal2) / 2;
	prom_lito = (lito_normal1 + lito_normal2) / 2; 
	prom_delta = ((kilo_delta1 + kilo_delta2)/10000) / 2;
	//
	var fVarianceA = 0;
	fVarianceA += parseFloat(Math.pow(kilo_normal1 - prom_kilo, 2));
	fVarianceA += parseFloat(Math.pow(kilo_normal2 - prom_kilo, 2));
	var prom_desv = Math.sqrt(fVarianceA)/Math.sqrt(1);
	var prom_DE = prom_desv / Math.sqrt(2);
	var promedio = Math.max(kilo_delta1, kilo_delta2);

	var prom_inc = Math.sqrt((Math.pow(promedio/Math.sqrt(2),2)+ Math.pow(prom_DE,2)));
	var prom_exp = prom_inc*2;




	//
	document.getElementById('masa_normal1').innerHTML = _RED2(masa_normal1, 5);
	document.getElementById('masa_normal2').innerHTML = _RED2(masa_normal2, 5);
	document.getElementById('volu_normal1').innerHTML = _RED2(volu_normal1, 5);
	document.getElementById('volu_normal2').innerHTML = _RED2(volu_normal2, 5);
	document.getElementById('kilo_normal1').innerHTML = _RED2(kilo_normal1, 1);
	document.getElementById('kilo_normal2').innerHTML = _RED2(kilo_normal2, 1);
	document.getElementById('lito_normal1').innerHTML = _RED2(lito_normal1, 1);
	document.getElementById('lito_normal2').innerHTML = _RED2(lito_normal2, 1);
	document.getElementById('masa_delta1').innerHTML = _RED2(masa_delta1, 4);
	document.getElementById('masa_delta2').innerHTML = _RED2(masa_delta2, 4);
	document.getElementById('volu_delta1').innerHTML = _RED2(volu_delta1, 4);
	document.getElementById('volu_delta2').innerHTML = _RED2(volu_delta2, 4);
	document.getElementById('kilo_delta1').innerHTML = _RED2(kilo_delta1, 4);
	document.getElementById('kilo_delta2').innerHTML = _RED2(kilo_delta2, 4);
	document.getElementById('lito_delta1').innerHTML = _RED2(lito_delta1, 4);
	document.getElementById('lito_delta2').innerHTML = _RED2(lito_delta2, 4);
	
	/*if(prom_inc >= 1 && prom_inc < 10){
		 prom_inc = _RED2(prom_inc, 1);
		 prom_masa = _RED2(prom_masa, 1);
		 prom_volu = _RED2(prom_volu, 1);
		 prom_kilo = _RED2(prom_kilo, 1);
		 prom_lito = _RED2(prom_lito, 1);
		 prom_delta = _RED2(prom_delta, 1);
		 prom_desv = _RED2(prom_desv, 1);
		 prom_DE = _RED2(prom_DE, 1);
		 prom_inc = _RED2(prom_inc, 1);
	}else{
		if(prom_inc > 10){
			prom_inc = Math.round(prom_inc);
			prom_masa = Math.round(prom_masa);
			prom_volu = Math.round(prom_volu);
			prom_kilo = Math.round(prom_kilo);
			prom_lito = Math.round(prom_lito);
			prom_delta = Math.round(prom_delta);
			prom_desv = Math.round(prom_desv);
			prom_DE = Math.round(prom_DE);
			prom_inc = Math.round(prom_inc);
		}else{
			jk = ''+prom_inc;
			for(z=0;z<jk.length;z++){
				if(jk[z] != '0' && jk[z] != '.'){
					jk = z;
					break;
				}
			}
			jk--;
			prom_inc = _RED2(prom_inc, jk);
			prom_masa = _RED2(prom_masa, jk);
			prom_volu = _RED2(prom_volu, jk);
			prom_kilo = _RED2(prom_kilo, jk);
		 	prom_lito = _RED2(prom_lito, jk);
			prom_delta = _RED2(prom_delta, jk);
			prom_desv = _RED2(prom_desv, jk);
			prom_DE = _RED2(prom_DE, jk);
			prom_inc = _RED2(prom_inc, jk);
			decimals = Math.pow(10,2);
  			var intPart = Math.floor(prom_inc);
			var fracPart = (prom_inc % 1)*decimals;
			if(fracPart == '10' || fracPart == '20' || fracPart == '30' || fracPart == '40' || fracPart == '50' || fracPart == '60' || fracPart == '70' || fracPart == '80' || fracPart == '90')
				joker = '1';
		}
	}
	
	if(joker == '1'){
		document.getElementById('prom_masa').innerHTML = ''+prom_masa+'0';
		document.getElementById('prom_volu').innerHTML = ''+prom_volu+'0';
		document.getElementById('prom_kilo').innerHTML = ''+prom_kilo+'0';
		document.getElementById('prom_lito').innerHTML = ''+prom_lito+'0';
		document.getElementById('prom_delta').innerHTML = ''+prom_delta+'0';
		document.getElementById('prom_desv').innerHTML = ''+prom_desv+'0';
		document.getElementById('prom_DE').innerHTML = ''+prom_DE+'0';
		document.getElementById('prom_inc').innerHTML = ''+prom_inc+'0';
	}else{*/
		document.getElementById('prom_masa').innerHTML =_RED2(prom_masa,5);
		document.getElementById('prom_volu').innerHTML =_RED2(prom_volu,5);
		document.getElementById('prom_kilo').innerHTML =_RED2(prom_kilo,1);
		document.getElementById('prom_lito').innerHTML =_RED2(prom_lito,1);
		//document.getElementById('prom_delta').innerHTML =_RED2(prom_delta,4);
		document.getElementById('prom_desv').innerHTML =_RED2(prom_desv, 5);
		document.getElementById('prom_DE').innerHTML =_RED2(prom_DE,5);
		document.getElementById('prom_inc').innerHTML =_RED2(prom_inc, 4);
		document.getElementById('prom_exp').innerHTML =_RED2(prom_exp, 4);
	//}
}

function __combos(){
	var control;
	//OBTIENE INC DEL PRIMER BALON
	control = document.getElementById('balon1');
	var inc = control.options[control.selectedIndex].getAttribute('inc');
	document.getElementById('inc_balon').innerHTML = inc;
	//OBTIENE INC DE LA PIPETA D1
	control = document.getElementById('pipetaD1');
	var inc = control.options[control.selectedIndex].getAttribute('inc');
	document.getElementById('inc_pipD1').innerHTML = inc;
	//OBTIENE INC DE LA PIPETA D2
	control = document.getElementById('pipetaD2');
	var inc = control.options[control.selectedIndex].getAttribute('inc');
	document.getElementById('inc_pipD2').innerHTML = inc;
	//OBTIENE INC DE LA PIPETA D3
	control = document.getElementById('pipetaD3');
	var inc = control.options[control.selectedIndex].getAttribute('inc');
	document.getElementById('inc_pipD3').innerHTML = inc;
	//OBTIENE INC DEL BALON D1
	control = document.getElementById('balonD1');
	var inc = control.options[control.selectedIndex].getAttribute('inc');
	document.getElementById('inc_balD1').innerHTML = inc;
	//OBTIENE INC DEL BALON D2
	control = document.getElementById('balonD2');
	var inc = control.options[control.selectedIndex].getAttribute('inc');
	document.getElementById('inc_balD2').innerHTML = inc;
	//OBTIENE INC DEL BALON D3
	control = document.getElementById('balonD3');
	var inc = control.options[control.selectedIndex].getAttribute('inc');
	document.getElementById('inc_balD3').innerHTML = inc;
}

function __BORO_CALCIO(_temp){	
	var tmpA1 = Math.pow(document.getElementById('dumbre1').value/document.getElementById('cn1').value, 2);
	var tmpA2 = Math.pow(document.getElementById('dumbre2').value/document.getElementById('cn2').value, 2);
	//
	var tmp2A = Math.pow((document.getElementById('inc_balon').innerHTML/Math.sqrt(_temp))/document.getElementById('balon1').value, 2);
	var tmp2B = Math.pow((document.getElementById('inc_balon').innerHTML/Math.sqrt(_temp))/document.getElementById('balon2').value, 2);
	var tmp3 = Math.pow((document.getElementById('inc_balD1').innerHTML/Math.sqrt(_temp))/document.getElementById('balonD1').value, 2);
	var tmp4 = Math.pow((document.getElementById('inc_pipD1').innerHTML/Math.sqrt(_temp))/document.getElementById('pipetaD1').value, 2);
	var tmp5 = Math.pow((document.getElementById('inc_balD2').innerHTML/Math.sqrt(_temp))/document.getElementById('balonD2').value, 2);
	var tmp6 = Math.pow((document.getElementById('inc_pipD2').innerHTML/Math.sqrt(_temp))/document.getElementById('pipetaD2').value, 2);
	var tmp7 = Math.pow((document.getElementById('inc_balD3').innerHTML/Math.sqrt(_temp))/document.getElementById('balonD3').value, 2);
	var tmp8 = Math.pow((document.getElementById('inc_pipD3').innerHTML/Math.sqrt(_temp))/document.getElementById('pipetaD3').value, 2);
	//
	var tmpB1 = Math.pow(_IC/document.getElementById('masa1').value, 2);
	var tmpB2 = Math.pow(_IC/document.getElementById('masa2').value, 2);
	var tmpY1 = Math.pow((_IC/2)/document.getElementById('masa1').value, 2);
	var tmpY2 = Math.pow((_IC/2)/document.getElementById('masa2').value, 2);
	//
	var tmpZ1 = Math.pow((document.getElementById('certi1').value/2)/document.getElementById('delta1').value, 2);
	var tmpZ2 = Math.pow((document.getElementById('certi2').value/2)/document.getElementById('delta2').value, 2);
	//
	masa_delta1 = Math.sqrt( (parseFloat(tmpA1)+parseFloat(tmp2A)+parseFloat(tmp3)+parseFloat(tmp4)+parseFloat(tmp5)+parseFloat(tmp6)+parseFloat(tmp7)+parseFloat(tmp8)+parseFloat(tmpB1)) );
	masa_delta1 = masa_delta1 * masa_normal1;
	masa_delta2 = Math.sqrt( (parseFloat(tmpA2)+parseFloat(tmp2B)+parseFloat(tmp3)+parseFloat(tmp4)+parseFloat(tmp5)+parseFloat(tmp6)+parseFloat(tmp7)+parseFloat(tmp8)+parseFloat(tmpB2)) );
	masa_delta2 = masa_delta2 * masa_normal2;
	volu_delta1 = Math.sqrt( (parseFloat(tmpA1)+parseFloat(tmp2A)+parseFloat(tmp3)+parseFloat(tmp4)+parseFloat(tmp5)+parseFloat(tmp6)+parseFloat(tmp7)+parseFloat(tmp8)+parseFloat(tmpB1)) )+parseFloat(tmpZ1);
	volu_delta1 = volu_delta1 * volu_normal1;
	volu_delta2 = Math.sqrt( (parseFloat(tmpA2)+parseFloat(tmp2B)+parseFloat(tmp3)+parseFloat(tmp4)+parseFloat(tmp5)+parseFloat(tmp6)+parseFloat(tmp7)+parseFloat(tmp8)+parseFloat(tmpB2)) )+parseFloat(tmpZ2);
	volu_delta2 = volu_delta2 * volu_normal2;
	kilo_delta1 = Math.sqrt( (parseFloat(tmpA1)+parseFloat(tmp2A)+parseFloat(tmp3)+parseFloat(tmp4)+parseFloat(tmp5)+parseFloat(tmp6)+parseFloat(tmp7)+parseFloat(tmp8)+parseFloat(tmpY1)) );
	kilo_delta1 = kilo_delta1 * kilo_normal1;
	kilo_delta2 = Math.sqrt( (parseFloat(tmpA2)+parseFloat(tmp2B)+parseFloat(tmp3)+parseFloat(tmp4)+parseFloat(tmp5)+parseFloat(tmp6)+parseFloat(tmp7)+parseFloat(tmp8)+parseFloat(tmpY2)) );
	kilo_delta2 = kilo_delta2 * kilo_normal2;
	lito_delta1 = volu_delta1 * 10000;
	lito_delta2 = volu_delta2 * 10000;
}