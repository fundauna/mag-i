var _decimales = 6;
var contenido1 = contenido1 = 0;
var _conc = 0;
var J = 0;
var K = 0;

function __lolo(){
	document.getElementById('masa1').value = '101.2';
	document.getElementById('masa2').value = '102.3';
	document.getElementById('error1').value = '0.00112';
	document.getElementById('error2').value = '0.00112';
	document.getElementById('masaA1').value = '5.63';
	document.getElementById('masaA2').value = '5.54';
	document.getElementById('densidad').value = '1';
	document.getElementById('incden').value = '0.074';
	document.getElementById('rep1').value = '0.15';
	document.getElementById('rep2').value = '0.000125';
	document.getElementById('resol1').value = '0.001';
	document.getElementById('resol2').value = '0.0001';
	document.getElementById('pres').value = '0';
	document.getElementById('inclin').value = '0.000077';
	document.getElementById('ccur').value = '0.0538';
	document.getElementById('incex').value = '0.000093';
	__calcula();
	__calcula2();
	__calcula3();
}

function datos(){	
		
	if($('#fechaA').val()==''){
		OMEGA('Debe indicar la fecha del an�lisis');
		return;
	}
	
	if(!confirm('Ingresar ensayo?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'xanalizar' : $('#xanalizar').val(),
		'tipo' : $('#tipo').val(),
		'ingrediente' : $('#ingrediente').val(),
		'fechaA' : $('#fechaA').val(),
		'rango' : $('#rango').val(),
		'masa1' : $('#masa1').val(),
		'masa2' : $('#masa2').val(),
		'masaA1' : $('#masaA1').val(),
		'masaA2' : $('#masaA2').val(),
		'error1' : $('#error1').val(),
		'densidad' : $('#densidad').val(),
		'incden' : $('#incden').val(),
		'rep1' : $('#rep1').val(),
		'rep2' : $('#rep2').val(),
		'resol1' : $('#resol1').val(),
		'resol2' : $('#resol2').val(),
		'pres' : $('#pres').val(),
		'inclin' : $('#inclin').val(),
		'ccur' : $('#ccur').val(),
		'incex' : $('#incex').val(),
		'resultado1' : $('#resultado1').val(),
		'resultado2' : $('#resultado2').val(),
		'resultado3' : $('#resultado3').val(),
		'resultado4' : $('#resultado4').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					opener.location.reload();
					alert("Transaccion finalizada");
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Redondear(txt,tk){
	_RED(txt, _decimales);
	if(tk == 1)
		__calcula();
	if(tk == 2)
		__calcula2();
	else
		__calcula3();
}

function __calcula(){
	document.getElementById('corregida1').innerHTML = (parseFloat(document.getElementById('masa1').value) * (1 - (parseFloat(document.getElementById('error1').value)/100))).toFixed(4);
	document.getElementById('mmuestra1').innerHTML = (parseFloat(document.getElementById('masa1').value) * (1 - (parseFloat(document.getElementById('error1').value)/100))).toFixed(4);
	document.getElementById('corregida2').innerHTML = (parseFloat(document.getElementById('masa2').value) * (1 - (parseFloat(document.getElementById('error2').value)/100))).toFixed(4);
	document.getElementById('mmuestra2').innerHTML = (parseFloat(document.getElementById('masa2').value) * (1 - (parseFloat(document.getElementById('error2').value)/100))).toFixed(4);
}

function __calcula2(){	
	document.getElementById('mm1').innerHTML = (parseFloat(document.getElementById('masaA1').value) / parseFloat(document.getElementById('mmuestra1').innerHTML) * 100).toFixed(2);
	document.getElementById('mm2').innerHTML = (parseFloat(document.getElementById('masaA2').value) / parseFloat(document.getElementById('mmuestra2').innerHTML) * 100).toFixed(2);
	document.getElementById('mv1').innerHTML = (parseFloat(document.getElementById('masaA1').value) / parseFloat(document.getElementById('mmuestra1').innerHTML) * 100 * parseFloat(document.getElementById('densidad').value)).toFixed(2);
	document.getElementById('mv2').innerHTML = (parseFloat(document.getElementById('masaA2').value) / parseFloat(document.getElementById('mmuestra2').innerHTML) * 100 * parseFloat(document.getElementById('densidad').value)).toFixed(2);
	
	var media = (parseFloat(document.getElementById('mm1').innerHTML) + parseFloat(document.getElementById('mm2').innerHTML)) / 2;
	var media2 = (parseFloat(document.getElementById('mv1').innerHTML) + parseFloat(document.getElementById('mv2').innerHTML)) / 2;
	var diff1 = Math.pow(parseFloat(document.getElementById('mm1').innerHTML) - media,2);
	var diff2 = Math.pow(parseFloat(document.getElementById('mm2').innerHTML) - media,2);
	var diff3 = Math.pow(parseFloat(document.getElementById('mv1').innerHTML) - media2,2);
	var diff4 = Math.pow(parseFloat(document.getElementById('mv2').innerHTML) - media2,2);
	var varianza = Math.sqrt(diff1 + diff2 / 1);
	var varianza2 = Math.sqrt(diff3 + diff4 / 1);
	document.getElementById('de11').innerHTML = varianza.toFixed(3);
	document.getElementById('de12').innerHTML = varianza.toFixed(3);	
	document.getElementById('de21').innerHTML = varianza2.toFixed(3);
	document.getElementById('de22').innerHTML = varianza2.toFixed(3);	
	document.getElementById('incde11').innerHTML = (varianza/Math.sqrt(2)).toFixed(3);
	document.getElementById('incde21').innerHTML = (varianza2/Math.sqrt(2)).toFixed(3);	
	document.getElementById('incde12').innerHTML = (varianza/Math.sqrt(2)).toFixed(3);
	document.getElementById('incde22').innerHTML = (varianza2/Math.sqrt(2)).toFixed(3);
		
	document.getElementById('mmazufre').innerHTML = media.toFixed(3);
	document.getElementById('mvazufre').innerHTML = media2.toFixed(3);
	J = media;
	K = media2;
}

function __calcula3(){
	if(document.getElementById('rep1').value != '') 
		document.getElementById('rep11').innerHTML = (parseFloat(document.getElementById('rep1').value) / Math.sqrt(10)).toFixed(5);
	if(document.getElementById('rep2').value != '') 
		document.getElementById('rep21').innerHTML = (parseFloat(document.getElementById('rep2').value) / Math.sqrt(10)).toFixed(5);
	if(document.getElementById('resol1').value != '') 
		document.getElementById('resol11').innerHTML = (parseFloat(document.getElementById('resol1').value) / Math.sqrt(3)).toFixed(5);
	if(document.getElementById('resol2').value != '') 
		document.getElementById('resol21').innerHTML = (parseFloat(document.getElementById('resol2').value) / Math.sqrt(3)).toFixed(6);
	if(document.getElementById('pres').value != '') 
		document.getElementById('pres1').innerHTML = (parseFloat(document.getElementById('pres').value) / Math.sqrt(3)).toFixed(5);
	if(document.getElementById('inclin').value != '') 
		document.getElementById('inclin1').innerHTML = (parseFloat(document.getElementById('inclin').value) / 2).toFixed(7);
	if(document.getElementById('ccur').value != '') 
		document.getElementById('ccur1').innerHTML = document.getElementById('ccur').value;
	if(document.getElementById('incex').value != '') 
		document.getElementById('incex1').innerHTML = (parseFloat(document.getElementById('incex').value) / 2).toFixed(6);
	document.getElementById('icequipo1').innerHTML = (Math.sqrt(Math.pow(document.getElementById('rep11').innerHTML,2)+Math.pow(document.getElementById('resol11').innerHTML,2)+Math.pow(document.getElementById('ccur1').innerHTML,2))).toFixed(5);
	
	var k = Math.pow(parseFloat(document.getElementById('rep2').value) / Math.sqrt(10),2);
	var j = Math.pow(parseFloat(document.getElementById('resol2').value) / Math.sqrt(3),2);
	var h = Math.pow(parseFloat(document.getElementById('inclin').value) / 2,2);
	var g = Math.pow(parseFloat(document.getElementById('incex').value) / 2,2);
	
	document.getElementById('icbalanza1').innerHTML = Math.sqrt(k+j+h+g).toFixed(5);
	
	var media = (parseFloat(document.getElementById('mmuestra1').innerHTML) + parseFloat(document.getElementById('mmuestra2').innerHTML)) / 2;
	var media2 = (parseFloat(document.getElementById('masaA1').value) + parseFloat(document.getElementById('masaA2').value)) / 2;

	var p1 = Math.pow(parseFloat(document.getElementById('icbalanza1').innerHTML) * media / media,2);
	var p2 = Math.pow(parseFloat(document.getElementById('icequipo1').innerHTML) / media2,2);
	var p3 = parseFloat(document.getElementById('mmazufre').innerHTML) * Math.sqrt(p2+p1);
	var p4 = Math.sqrt(Math.pow(p3,2) + Math.pow(parseFloat(document.getElementById('incde11').innerHTML),2));
	
	var inccomb = Math.sqrt(Math.pow(0.00027/2,2) + Math.pow(parseFloat(document.getElementById('resol21').innerHTML),2));
	var incest = inccomb / 2;
	var p5 =  Math.pow(incest / document.getElementById('densidad').value,2);
	var p6 = parseFloat(document.getElementById('mvazufre').innerHTML) * Math.sqrt(p2+p1+p5);
	var p7 = Math.sqrt(Math.pow(p6,2) + Math.pow(parseFloat(document.getElementById('incde21').innerHTML),2));
	
	document.getElementById('incc1').innerHTML = p4.toFixed(3);
	document.getElementById('incc2').innerHTML = p7.toFixed(3);
	document.getElementById('ince1').innerHTML = (p4 * 2).toFixed(3);
	document.getElementById('ince2').innerHTML = (p7 * 2).toFixed(3);
	
	joker = '';
	if(p4 >= 1 && p4 < 10){
		 pro1 = _RED2(p4, 1);
		 pro2 = _RED2(J, 1);
	}else{
		if(p4 > 10){
			pro1 = Math.round(p4);
			pro2 = Math.round(J);
		}else{
			jk = ''+p4;
			for(z=0;z<jk.length;z++){
				if(jk[z] != '0' && jk[z] != '.'){
					jk = z;
					break;
				}
			}
			pro1 = p4.toFixed(jk);
			pro2 = J.toFixed(jk);
		}
	}
	document.getElementById('incc1').innerHTML = pro1;
	document.getElementById('mmazufre').innerHTML = pro2;
	document.getElementById('resultado1').value = pro2;
	document.getElementById('resultado2').value = pro1;
	
	joker = '';
	if(p7 >= 1 && p7 < 10){
		 pro1 = p7.toFixed(1);
		 pro2 = K.toFixed(1);
	}else{
		if(p7 > 10){
			pro1 = Math.round(p7);
			pro2 = Math.round(K);
		}else{
			jk = ''+p7;
			for(z=0;z<jk.length;z++){
				if(jk[z] != '0' && jk[z] != '.'){
					jk = z;
					break;
				}
			}
			pro1 = p7.toFixed(jk);
			pro2 = K.toFixed(jk);
		}
	}
	document.getElementById('incc2').innerHTML = pro1;
	document.getElementById('mvazufre').innerHTML = pro2;
	document.getElementById('resultado3').value = pro2;
	document.getElementById('resultado4').value = pro1;
}