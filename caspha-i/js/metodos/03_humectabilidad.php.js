var _decimales = 4;

function datos(){	
	/*if($('#ingrediente').val()==''){
		OMEGA('Debe indicar el ingrediente activo');
		return;
	}*/
	
	if($('#fechaA').val()==''){
		OMEGA('Debe indicar la fecha del an�lisis');
		return;
	}
	
	if($('#tipo_form').val()==''){
		OMEGA('Debe seleccionar el tipo de formulaci�n');
		return;
	}
	
	if($('#fechaP').val()==''){
		OMEGA('Debe indicar la fecha de preparaci�n');
		return;
	}
	
	if($('#fechaD').val()==''){
		OMEGA('Debe indicar la fecha de comprobaci�n');
		return;
	}
	
	if(!confirm('Ingresar ensayo?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'xanalizar' : $('#xanalizar').val(),
		'tipo' : $('#tipo').val(),
		'ingrediente' : $('#ingrediente').val(),
		'fechaA' : $('#fechaA').val(),
		'tipo_form' : $('#tipo_form').val(),
		'dosis' : $('#dosis').val(),
		//'metodo' : $('#metodo').val(),
		'fechaP' : $('#fechaP').val(),
		'maria' : $('#maria').val(),
		'numreactivo' : $('#numreactivo').val(),
		'fechaD' : $('#fechaD').val(),
		'resultado' : $('#resultado').val(),
		'cantidad' : $('#cantidad').val(),
		'volumen' : $('#volumen').val(),
		'obs' : $('#obs').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					opener.location.reload();
					alert("Transaccion finalizada");
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Redondear(txt){
	_RED(txt, _decimales);
	//__calcula();
}