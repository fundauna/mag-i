var _decimales = 4;

function __lolo(){
	//document.getElementById('ingrediente').value = 'IA';
	document.getElementById('numreactivo').value = 'R111';
	document.getElementById('dosis').value = '123';
	document.getElementById('maria').value = '124';
	document.getElementById('resultado').value = '125';
	document.getElementById('cantidad').value = '66';
	document.getElementById('cremado1').value = '1';
	document.getElementById('cremado2').value = '2';
	document.getElementById('cremado3').value = '3';
	document.getElementById('cremado4').value = '4';
	document.getElementById('aceite1').value = '5';
	document.getElementById('aceite2').value = '6';
	document.getElementById('aceite3').value = '7';
	document.getElementById('aceite4').value = '8';
	document.getElementById('cremado5').value = '11';
	document.getElementById('cremado6').value = '12';
	document.getElementById('cremado7').value = '13';
	document.getElementById('cremado8').value = '14';
	document.getElementById('aceite5').value = '15';
	document.getElementById('aceite6').value = '16';
	document.getElementById('aceite7').value = '17';
	document.getElementById('aceite8').value = '18';
	document.getElementById('clari1').selectedIndex=2;
	document.getElementById('espon1').selectedIndex=1;
	__calcula();
}

function duplicar(){
	if(!confirm('Copiar de muestra 1 a muestra 2?')) return;
	
	document.getElementById('cremado5').value = document.getElementById('cremado1').value;
	document.getElementById('cremado6').value = document.getElementById('cremado2').value;
	document.getElementById('cremado7').value = document.getElementById('cremado3').value;
	document.getElementById('cremado8').value = document.getElementById('cremado4').value;
	document.getElementById('aceite5').value = document.getElementById('aceite1').value;
	document.getElementById('aceite6').value = document.getElementById('aceite2').value;
	document.getElementById('aceite7').value = document.getElementById('aceite3').value;
	document.getElementById('aceite8').value = document.getElementById('aceite4').value;
	document.getElementById('clari2').selectedIndex=document.getElementById('clari1').selectedIndex;
	document.getElementById('espon2').selectedIndex=document.getElementById('espon1').selectedIndex;
	
	__calcula();
}

function __calcula(){
	var promA1 = parseFloat( $('#cremado1').val() ) + parseFloat( $('#cremado5').val() );
	var promA2 = parseFloat( $('#cremado2').val() ) + parseFloat( $('#cremado6').val() );
	var promA3 = parseFloat( $('#cremado3').val() ) + parseFloat( $('#cremado7').val() );
	var promA4 = parseFloat( $('#cremado4').val() ) + parseFloat( $('#cremado8').val() );
	var promB1 = parseFloat( $('#aceite1').val() ) + parseFloat( $('#aceite5').val() );
	var promB2 = parseFloat( $('#aceite2').val() ) + parseFloat( $('#aceite6').val() );
	var promB3 = parseFloat( $('#aceite3').val() ) + parseFloat( $('#aceite7').val() );
	var promB4 = parseFloat( $('#aceite4').val() ) + parseFloat( $('#aceite8').val() );
	
	document.getElementById('promA1').innerHTML = _RED2(promA1/2, 4);
	document.getElementById('promA2').innerHTML = _RED2(promA2/2, 4);
	document.getElementById('promA3').innerHTML = _RED2(promA3/2, 4);
	document.getElementById('promA4').innerHTML = _RED2(promA4/2, 4);
	document.getElementById('promB1').innerHTML = _RED2(promB1/2, 4);
	document.getElementById('promB2').innerHTML = _RED2(promB2/2, 4);
	document.getElementById('promB3').innerHTML = _RED2(promB3/2, 4);
	document.getElementById('promB4').innerHTML = _RED2(promB4/2, 4);
}

function datos(){	
	if($('#fechaA').val()==''){
		OMEGA('Debe indicar la fecha del an�lisis');
		return;
	}
	
	if($('#tipo_form').val()==''){
		OMEGA('Debe seleccionar el tipo de formulaci�n');
		return;
	}
	
	/*if($('#ingrediente').val()==''){
		OMEGA('Debe indicar el ingrediente activo');
		return;
	}*/
	
	if($('#fechaP').val()==''){
		OMEGA('Debe indicar la fecha de preparaci�n');
		return;
	}
	
	if($('#fechaD').val()==''){
		OMEGA('Debe indicar la fecha de comprobaci�n');
		return;
	}
	
	if($('#numreactivo').val()==''){
		OMEGA('Debe indicar el n�mero de reactivo');
		return;
	}
	
	if( $('#clari1').val()=='' || $('#clari2').val()=='' ){
		OMEGA('Debe selecionar si existe clarificaci�n de agua');
		return;
	}
	
	if( $('#espon1').val()=='' || $('#espon2').val()=='' ){
		OMEGA('Debe selecionar si existe espontaneidad');
		return;
	}
	
	if(!confirm('Ingresar ensayo?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'xanalizar' : $('#xanalizar').val(),
		'tipo' : $('#tipo').val(),
		'ingrediente' : $('#ingrediente').val(),
		'fechaA' : $('#fechaA').val(),
		'tipo_form' : $('#tipo_form').val(),
		'dosis' : $('#dosis').val(),
		'fechaP' : $('#fechaP').val(),
		'maria' : $('#maria').val(),
		'numreactivo' : $('#numreactivo').val(),
		'fechaD' : $('#fechaD').val(),
		'resultado' : $('#resultado').val(),
		'cantidad' : $('#cantidad').val(),
		'cremado1' : $('#cremado1').val(),
		'cremado2' : $('#cremado2').val(),
		'cremado3' : $('#cremado3').val(),
		'cremado4' : $('#cremado4').val(),
		'cremado5' : $('#cremado5').val(),
		'cremado6' : $('#cremado6').val(),
		'cremado7' : $('#cremado7').val(),
		'cremado8' : $('#cremado8').val(),
		'clari1' : $('#clari1').val(),
		'espon1' : $('#espon1').val(),
		'clari2' : $('#clari2').val(),
		'espon2' : $('#espon2').val(),
		'aceite1' : $('#aceite1').val(),
		'aceite2' : $('#aceite2').val(),
		'aceite3' : $('#aceite3').val(),
		'aceite4' : $('#aceite4').val(),
		'aceite5' : $('#aceite5').val(),
		'aceite6' : $('#aceite6').val(),
		'aceite7' : $('#aceite7').val(),
		'aceite8' : $('#aceite8').val(),
		'promA2' : document.getElementById('promA2').innerHTML,
		'promA3' : document.getElementById('promA3').innerHTML,
		'promA4' : document.getElementById('promA4').innerHTML,
		'promB2' : document.getElementById('promB2').innerHTML,
		'promB3' : document.getElementById('promB3').innerHTML,
		'promB4' : document.getElementById('promB4').innerHTML,
		'obs' : $('#obs').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					opener.location.reload();
					alert("Transaccion finalizada");
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Redondear(txt){
	_RED(txt, _decimales);
	__calcula();
}