$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		/*"sScrollX": "100%",*/
		"sScrollXInner": "110%",
		"bScrollCollapse": true,
		"bFilter": true,
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function GrafControl(_ING, _DEC, _FOR){
	window.open("03_grafico_control_detalle.php?I="+_ING+"&D="+_DEC+"&F="+_FOR,"","width=900,height=600,scrollbars=yes,status=no");
}

function EquiposLista(){
	window.open(__SHELL__ + "?list=1","","width=500,height=200,scrollbars=yes,status=no");
}

function EquipoEscoge(cs, codigo, nombre){
	opener.document.getElementById('equipo').value=cs;
	opener.document.getElementById('nomequipo').value='('+codigo+') '+nombre;
	window.close();
}

function modificar(accion, id){
		
	if(confirm('Modificar Datos?')){
		var parametros = {
			'_AJAX' : 1,
			'id' : id,
			'cuarto' : $('#cuarto').val(),
			'temp1' : $('#temp1').val(),
			'temp2' : $('#temp2').val(),
			'hum1' : $('#hum1').val(),
			'hum2' : $('#hum2').val(),
			'accion' : accion
		};
		
		$.ajax({
			data:  parametros,
			url:   __SHELL__,
			type:  'post',
			beforeSend: function () {
				ALFA('Por favor espere....');
			},
			success: function (_response) {
				switch(_response){
					case '-0':alert('Sesi�n expirada [Err:0]');break;
					case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
					case '0':
						OMEGA('Error transaccional');
						break;
					case '1':
						GAMA('Transaccion finalizada');
						break;
					default:alert('Tiempo de espera agotado');break;
				}
			}
		});
	}
}