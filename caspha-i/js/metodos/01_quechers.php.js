/*function datos(accion){	
	if($('#archivo').val()=='' ){
		OMEGA('Debe adjuntar un documento');
		return;
	}	
	
	if(!confirm('Ingresar datos?')) return;
	btn.disabled = true;
	ALFA('Por favor espere....');
	document.form.submit();
}*/

function Redondear(txt){
	_RED(txt, 10);
}


function vector11(ctrl){
	var str = "1=1";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		str += "&" + control[i].value.replace(/&/g, '');
	}
	return str;
}

function vector(ctrl){
	var str = "1=1";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		str += "&" + control[i].value.replace(/&/g, '');

	}
	return str;
}

function vector2(ctrl){
	var str = "1=1";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		if(control[i].checked)
			str += "&1";
		else
			str += "&0";
	}
	return str;
}

function datos(){
	//
	if(!confirm('Modificar datos?')) return;
	var parametros = {
		'_AJAX' : 1,
		'xanalizar' : $('#xanalizar').val(),
		'tipo' : $('#tipo').val(),
		'matriz' : $('#matriz').val(),
		'id' : vector("id"),
		'analito' : vector("analito"),
		'tecnica' :  vector11("tecnica"),
		'resultado' : vector("resultado"),
		'reportar' : vector2("reportar")
	};

	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					opener.location.reload();
					DELTA("Transaccion finalizada");
					//window.close();				
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}