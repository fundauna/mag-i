var _decimales = 4;
var contenido1 = contenido1 = 0;

function __lolo(){	
	document.getElementById('aceite').selectedIndex=1;
	document.getElementById('numreactivo').value = 'Reactivo 1';
	document.getElementById('fechaA').value = '02/05/2017';
	document.getElementById('fechaP').value = '02/05/2017';
	document.getElementById('ampolla1').value = '0.05';
	document.getElementById('ampolla2').value = '0.0001';
	document.getElementById('BA').selectedIndex=8;
	document.getElementById('muestra1').value = '0.2010';
	document.getElementById('muestra2').value = '0.2101';
	document.getElementById('consumido1').value = '11.99';
	document.getElementById('consumido2').value = '12.32';
	document.getElementById('densidad').value = '1.2';
	document.getElementById('origen').selectedIndex=1;
	document.getElementById('bureta').selectedIndex=2;
	__calcula();
}

function datos(){
	
	if($('#fechaA').val()==''){
		OMEGA('Debe indicar la fecha del an�lisis');
		return;
	}
	
	if($('#rango').val()==''){
		OMEGA('Debe indicar la concentraci�n declarada');
		return;
	}
	
	if($('#unidad').val()==''){
		OMEGA('Debe seleccionar la unidad');
		return;
	}
	
	if($('#fechaP').val()==''){
		OMEGA('Debe indicar la fecha de preparaci�n');
		return;
	}
	
	if($('#incertidumbre').val()==''){
		OMEGA('Debe indicar la incertidumbre expandida');
		return;
	}
	
	if(!confirm('Ingresar ensayo?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'xanalizar' : $('#xanalizar').val(),
		'tipo' : $('#tipo').val(),
		'ingrediente' : $('#ingrediente').val(),
		'fechaA' : $('#fechaA').val(),
		'rango' : $('#rango').val(),
		'unidad' : $('#unidad').val(),
		'tipo_form' : $('#tipo_form').val(),
		'aceite' : $('#aceite').val(),
		'numreactivo' : $('#numreactivo').val(),
		'fechaP' : $('#fechaP').val(),
		'IECB' : $('#IECB').val(),
		'linealidad1' : $('#linealidad1').val(),
		'linealidad2' : $('#linealidad2').val(),
		'ampolla1' : $('#ampolla1').val(),
		'ampolla2' : $('#ampolla2').val(),
		'repeti1' : $('#repeti1').val(),
		'repeti2' : $('#repeti2').val(),
		'muestra1' : $('#muestra1').val(),
		'muestra2' : $('#muestra2').val(),
		'BA' : $('#BA').val(),
		'consumido1' : $('#consumido1').val(),
		'consumido2' : $('#consumido2').val(),
		'contenido1' : $('#contenido1').val(),
		'contenido2' : $('#contenido2').val(),
		'contenido3' : $('#contenido3').val(),
		'contenido4' : $('#contenido4').val(),
		'aforado1' : $('#origen').val(),
		'aforado2' : $('#bureta').val(),
		'masa' : $('#masa').val(),
		'densidad' : $('#densidad').val(),
		'promedio' : $('#promedio').val(),
		'incertidumbre' : $('#incertidumbre').val(),
		'obs' : $('#obs').val()	
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					opener.location.reload();
					alert("Transaccion finalizada");
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Redondear(txt, dec = ''){
    if(dec = ''){
        dec = _decimales;
    }else{
        dec = 8;
    }
        
	_RED(txt, dec);
	__calcula();
}

function __calcula(){
	if(document.getElementById('unidad').value == '') return;
		
	if(document.getElementById('unidad').value == '0') 
		var densidad = 1;
	else
		var densidad = document.getElementById('densidad').value;
		
		
	if(document.getElementById('ingrediente').value == 'Ferbam') var ing = 138.82;	
	if(document.getElementById('ingrediente').value == 'Mancozeb') var ing = 135.5;	
	if(document.getElementById('ingrediente').value == 'Maneb') var ing = 132.65;	
	if(document.getElementById('ingrediente').value == 'Metiram') var ing = 136.08;	
	if(document.getElementById('ingrediente').value == 'Nabam') var ing = 128.18;	
	if(document.getElementById('ingrediente').value == 'Propineb') var ing = 144.90;	
	if(document.getElementById('ingrediente').value == 'Zineb') var ing = 137.87;	
	if(document.getElementById('ingrediente').value == 'Ziram') var ing = 152.91;	
	if(document.getElementById('ingrediente').value == 'Patr�n') var ing = 225.31;		
	
	contenido1 = (((document.getElementById('consumido1').value / 1000) * 2 * document.getElementById('ampolla1').value) * ing * densidad * 100) / document.getElementById('muestra1').value;
	contenido2 = (((document.getElementById('consumido2').value / 1000) * 2 * document.getElementById('ampolla1').value) * ing * densidad * 100) / document.getElementById('muestra2').value;
	
	var promedio = (contenido1 + contenido2) / 2;
	if(document.getElementById('unidad').value == '0'){
		document.getElementById('contenido1').value = _RED2(contenido1, 2);
		document.getElementById('contenido2').value = _RED2(contenido2, 2);
		document.getElementById('contenido3').value = '';
		document.getElementById('contenido4').value = '';
	}else{
		document.getElementById('contenido1').value = '';
		document.getElementById('contenido2').value = '';
		document.getElementById('contenido3').value = _RED2(contenido1, 2);
		document.getElementById('contenido4').value = _RED2(contenido2, 2);	
	}
	document.getElementById('promedio').value = _RED2(promedio, 2);
	
	__calcula2(contenido1, contenido2);
}

function __calcula2(contenido1, contenido2){
	var inccomr1 = __inccomr1(); 
        //alert(inccomr1);
	var inccomr2 = __inccomr2();
        //alert(inccomr2);
	var desvest = __desvest(contenido1,contenido2); 
	
	var incexp = 2 * Math.sqrt(Math.pow(((inccomr1 + inccomr2)/2)/Math.sqrt(2),2) + Math.pow(desvest * (1.189 / Math.sqrt(2)),2));
	
	document.getElementById('incertidumbre').value = _RED2(incexp, 2);
}

function __inccomr1(){
	if(document.getElementById('unidad').value == '0'){
		var contenidoS = document.getElementById('contenido1').value;
		var extra = 0;
	}else{ 
		var contenidoS = document.getElementById('contenido3').value;
		var extra = Math.pow((0.0001*Math.sqrt(3))/document.getElementById('densidad').value,2);
	}
	if(document.getElementById('origen').value == '1'){
		var r = Math.pow((((0.02/100)*3*document.getElementById('ampolla1').value) / Math.sqrt(3)),2);
		var t = Math.pow((document.getElementById('ampolla2').value / 2),2);
		var w = Math.pow(Math.sqrt(r+t) / document.getElementById('ampolla1').value,2);
		////////////////////////////////////////////////////////////////////////////////////////////S
		r = Math.pow(document.getElementById('BA').options[document.getElementById('BA').selectedIndex].getAttribute('TOL') / Math.sqrt(6),2);
		t = Math.pow(document.getElementById('BA').options[document.getElementById('BA').selectedIndex].getAttribute('TEMP') / Math.sqrt(3),2);
		y = Math.pow(document.getElementById('BA').options[document.getElementById('BA').selectedIndex].getAttribute('REP'),2);
		var q = Math.pow(Math.sqrt(r+t+y) / document.getElementById('BA').value,2);
		var pot1 = document.getElementById('ampolla1').value * Math.sqrt(Math.pow(Math.sqrt(w+q),2));
		pot1 = Math.pow(pot1/document.getElementById('ampolla1').value,2);
	}else{
		var pot1 = document.getElementById('ampolla2').value / 2;
	}
        //alert("Pot 1:"+pot1);
	if(document.getElementById('bureta').value == '20'){
		var pot2 = Math.sqrt(2*Math.pow((0.030/Math.sqrt(6)),2));
	}else{
		r = Math.pow(document.getElementById('bureta').options[document.getElementById('bureta').selectedIndex].getAttribute('TOL')/Math.sqrt(6),2);
		t = Math.pow(document.getElementById('bureta').options[document.getElementById('bureta').selectedIndex].getAttribute('TEMP')/Math.sqrt(3),2);
		y = Math.pow(document.getElementById('bureta').options[document.getElementById('bureta').selectedIndex].getAttribute('REP'),2);
		q = r+t+y+(Math.pow(0.1/(2*Math.sqrt(3)),2));
		var pot2 = Math.sqrt(2*(q));
	}
	pot2 = Math.pow(pot2/document.getElementById('consumido1').value,2);
//alert("Pot 2:"+pot2);	
    var pot3 = Math.sqrt(Math.pow(0.1/Math.sqrt(3),2)+Math.pow(0.067/2,2)+Math.pow((2.92*0.032)/Math.sqrt(3),2)+Math.pow(0.081/2,2)) / 1000;
	pot3 = Math.pow(pot3/document.getElementById('muestra1').value,2);	
	inc = contenidoS * Math.sqrt(pot1+pot2+pot3+extra);
	return inc;
}

function __inccomr2(){
	if(document.getElementById('unidad').value == '0'){
		var contenidoS = document.getElementById('contenido1').value;
		var extra = 0;
	}else{ 
		var contenidoS = document.getElementById('contenido3').value;
		var extra = Math.pow((0.0001*Math.sqrt(3))/document.getElementById('densidad').value,2);
	}
	if(document.getElementById('origen').value == '1'){
		var r = Math.pow((((0.02/100)*3*document.getElementById('ampolla1').value) / Math.sqrt(3)),2);
		var t = Math.pow((document.getElementById('ampolla2').value / 2),2);
		var w = Math.pow(Math.sqrt(r+t) / document.getElementById('ampolla1').value,2);
		////////////////////////////////////////////////////////////////////////////////////////////S
		r = Math.pow(document.getElementById('BA').options[document.getElementById('BA').selectedIndex].getAttribute('TOL') / Math.sqrt(6),2);
		t = Math.pow(document.getElementById('BA').options[document.getElementById('BA').selectedIndex].getAttribute('TEMP') / Math.sqrt(3),2);
		y = Math.pow(document.getElementById('BA').options[document.getElementById('BA').selectedIndex].getAttribute('REP'),2);
		var q = Math.pow(Math.sqrt(r+t+y) / document.getElementById('BA').value,2);
		var pot1 = document.getElementById('ampolla1').value * Math.sqrt(Math.pow(Math.sqrt(w+q),2));
		pot1 = Math.pow(pot1/document.getElementById('ampolla1').value,2);
	}else{
		var pot1 = document.getElementById('ampolla2').value / 2;
	}
	if(document.getElementById('bureta').value == '20')
		var pot2 = Math.sqrt(2*Math.pow((0.030/Math.sqrt(6)),2));
	else{
		r = Math.pow(document.getElementById('bureta').options[document.getElementById('bureta').selectedIndex].getAttribute('TOL')/Math.sqrt(6),2);
		t = Math.pow(document.getElementById('bureta').options[document.getElementById('bureta').selectedIndex].getAttribute('TEMP')/Math.sqrt(3),2);
		y = Math.pow(document.getElementById('bureta').options[document.getElementById('bureta').selectedIndex].getAttribute('REP'),2);
		q = r+t+y+(Math.pow(0.1/(2*Math.sqrt(3)),2));
		var pot2 = Math.sqrt(2*(q));
	}
	pot2 = Math.pow(pot2/document.getElementById('consumido1').value,2);
	var pot3 = Math.sqrt(Math.pow(0.1/Math.sqrt(3),2)+Math.pow(0.067/2,2)+Math.pow((2.92*0.032)/Math.sqrt(3),2)+Math.pow(0.081/2,2)) / 1000;
	pot3 = Math.pow(pot3/document.getElementById('muestra1').value,2);
	inc = contenidoS * Math.sqrt(pot1+pot2+pot3+extra);
	return inc;
}

function __desvest(contenido1,contenido2){
	var media = (contenido1 + contenido2) / 2;
	var diff1 = Math.pow(contenido1 - media,2);
	var diff2 = Math.pow(contenido2 - media,2);
	var varianza = Math.sqrt(diff1 + diff2 / 1);
	return varianza;
}


function RAIZ(_num){
	return Math.sqrt(_num);
}