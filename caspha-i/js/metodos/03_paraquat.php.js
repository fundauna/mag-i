var _decimales = 4;
var fin1 = fin2 = _pendiente = _intercepto = _r2 = _sm = _sb = _promcalA = _promcalB = dilucion1 = dilucion2 = 0;

function __lolo(){
	document.getElementById('rango').value = '20';
	document.getElementById('unidad').selectedIndex=1;
	document.getElementById('rango2').value = '20';
	document.getElementById('unidad2').selectedIndex=1;
	document.getElementById('factor').selectedIndex=1;
	document.getElementById('pureza').value = '99.50';
	document.getElementById('densidad').value = '1';
	document.getElementById('masa').value = '110.10';
	document.getElementById('bal1').selectedIndex = 6;
	document.getElementById('bal2').selectedIndex = 6;
	document.getElementById('bal3').selectedIndex = 4;
	document.getElementById('bal4').selectedIndex = 4;
	document.getElementById('bal5').selectedIndex = 4;
	document.getElementById('bal6').selectedIndex = 4;
	document.getElementById('vol2').selectedIndex = 9;
	document.getElementById('vol3').selectedIndex = 6;
	document.getElementById('vol4').selectedIndex = 9;
	document.getElementById('vol5').selectedIndex = 10;
	document.getElementById('vol6').selectedIndex = 11;
	document.getElementById('abs3').value = '0.22139';
	document.getElementById('abs4').value = '0.44325';
	document.getElementById('abs5').value = '0.66030';
	document.getElementById('abs6').value = '0.86805';
	document.getElementById('mm1').value = '2.3576';
	document.getElementById('mm6').value = '2.4817';
	document.getElementById('aa2').selectedIndex = 9;
	document.getElementById('aa3').selectedIndex = 9;
	document.getElementById('aa7').selectedIndex = 9;
	document.getElementById('aa8').selectedIndex = 9;
	document.getElementById('bb1').selectedIndex = 7;
	document.getElementById('bb2').selectedIndex = 4;
	document.getElementById('bb3').selectedIndex = 4;
	document.getElementById('bb6').selectedIndex = 7;
	document.getElementById('bb7').selectedIndex = 4;
	document.getElementById('bb8').selectedIndex = 4;
	document.getElementById('cur3').value = '9.10060';
	document.getElementById('cur4').value = '9.08700';
	document.getElementById('cur5').value = '9.07740';
	document.getElementById('cur8').value = '9.54720';
	document.getElementById('cur9').value = '9.53260';
	document.getElementById('curA').value = '9.52020';
	document.getElementById('sor3').value = '0.62734';
	document.getElementById('sor4').value = '0.62641';
	document.getElementById('sor5').value = '0.62576';
	document.getElementById('sor8').value = '0.65769';
	document.getElementById('sor9').value = '0.65670';
	document.getElementById('sorA').value = '0.65586';
	__calcula();
}

function datos(){	
	if($('#ingrediente').val()==''){
		OMEGA('Debe indicar el ingrediente activo');
		return;
	}
	
	if($('#fechaA').val()==''){
		OMEGA('Debe indicar la fecha del an�lisis');
		return;
	}
	
	if($('#rango').val()==''){
		OMEGA('Debe indicar la concentraci�n declarada');
		return;
	}
	
	if($('#unidad').val()==''){
		OMEGA('Debe seleccionar la unidad');
		return;
	}
	
	if($('#origen').val()==''){
		OMEGA('Debe indicar el origen del estandar');
		return;
	}
	
	if($('#tipo_form').val()==''){
		OMEGA('Debe seleccionar el tipo de formulaci�n');
		return;
	}
	
	if($('#fechaP').val()==''){
		OMEGA('Debe indicar la fecha de preparaci�n');
		return;
	}
	
	if(!confirm('Ingresar ensayo?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 1,
		'xanalizar' : $('#xanalizar').val(),
		'tipo' : $('#tipo').val(),
		'ingrediente' : $('#ingrediente').val(),
		'fechaA' : $('#fechaA').val(),
		'rango' : $('#rango').val(),
		'unidad' : $('#unidad').val(),
		'tipo_form' : $('#tipo_form').val(),
		'rango2' : $('#rango2').val(),
		'unidad2' : $('#unidad2').val(),
		'origen' : $('#origen').val(),
		'fechaP' : $('#fechaP').val(),
		'pureza' : $('#pureza').val(),
		'factor' : $('#factor').val(),
		'existe' : $('#existe').val(),
		'densidad' : $('#densidad').val(),
		'linealidad1' : $('#linealidad1').val(),
		'linealidad2' : $('#linealidad2').val(),
		'repeti1' : $('#repeti1').val(),
		'repeti2' : $('#repeti2').val(),
		'masa' : $('#masa').val(),
		'bal1' : $('#bal1').val(),
		'vol2' : $('#vol2').val(),
		'bal2' : $('#bal2').val(),
		'vol3' : $('#vol3').val(),
		'bal3' : $('#bal3').val(),
		'abs3' : $('#abs3').val(),
		'vol4' : $('#vol4').val(),
		'bal4' : $('#bal4').val(),
		'abs4' : $('#abs4').val(),
		'vol5' : $('#vol5').val(),
		'bal5' : $('#bal5').val(),
		'abs5' : $('#abs5').val(),
		'vol6' : $('#vol6').val(),
		'bal6' : $('#bal6').val(),
		'abs6' : $('#abs6').val(),
		'mm1' : $('#mm1').val(),
		'bb1' : $('#bb1').val(),
		'aa2' : $('#aa2').val(),
		'bb2' : $('#bb2').val(),
		'aa3' : $('#aa3').val(),
		'bb3' : $('#bb3').val(),
		'cur3' : $('#cur3').val(),
		'sor3' : $('#sor3').val(),
		'cur4' : $('#cur4').val(),
		'sor4' : $('#sor4').val(),
		'cur5' : $('#cur5').val(),
		'sor5' : $('#sor5').val(),
		'mm6' : $('#mm6').val(),
		'bb6' : $('#bb6').val(),
		'aa7' : $('#aa7').val(),
		'bb7' : $('#bb7').val(),
		'aa8' : $('#aa8').val(),
		'bb8' : $('#bb8').val(),
		'cur8' : $('#cur8').val(),
		'sor8' : $('#sor8').val(),
		'cur9' : $('#cur9').val(),
		'sor9' : $('#sor9').val(),
		'curA' : $('#curA').val(),
		'sorA' : $('#sorA').val(),
		'IC' : document.getElementById('promedio').innerHTML,
		'EX' : document.getElementById('EX').innerHTML,
		'contenido1' : document.getElementById('fin1').innerHTML,
		'contenido2' : document.getElementById('fin2').innerHTML,
		'obs' : $('#obs').val()	
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					opener.location.reload();
					alert("Transaccion finalizada");
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Redondear(txt){
	_RED(txt, _decimales);
	__calcula();
}

function __calcula(){
	if(document.getElementById('unidad').value == '') return;
	
	var con1 = ((document.getElementById('masa').value*(document.getElementById('pureza').value/100))/document.getElementById('bal1').value)/document.getElementById('factor').value;
	var con2 = con1*(document.getElementById('vol2').value/document.getElementById('bal2').value);
	var con3 = _RED2(con2, 6)*document.getElementById('vol3').value/document.getElementById('bal3').value*1000;
	var con4 = _RED2(con2, 6)*document.getElementById('vol4').value/document.getElementById('bal4').value*1000;
	var con5 = _RED2(con2, 6)*document.getElementById('vol5').value/document.getElementById('bal5').value*1000;
	var con6 = _RED2(con2, 6)*document.getElementById('vol6').value/document.getElementById('bal6').value*1000;
	var cal1 = document.getElementById('mm1').value/document.getElementById('bb1').value;
	var cal6 = document.getElementById('mm6').value/document.getElementById('bb6').value;
	var cal2 = _RED2(cal1, 6)*document.getElementById('aa2').value/document.getElementById('bb2').value;
	var cal7 = _RED2(cal6, 6)*document.getElementById('aa7').value/document.getElementById('bb7').value;
	//
	document.getElementById('con1').innerHTML = _RED2(con1, 5);
	document.getElementById('con2').innerHTML = _RED2(con2, 5);
	document.getElementById('con3').innerHTML = _RED2(con3, 5);
	document.getElementById('con4').innerHTML = _RED2(con4, 5);
	document.getElementById('con5').innerHTML = _RED2(con5, 5);
	document.getElementById('con6').innerHTML = _RED2(con6, 5);
	document.getElementById('cal1').innerHTML = _RED2(cal1, 5);
	document.getElementById('cal2').innerHTML = _RED2(cal2, 5);
	document.getElementById('cal6').innerHTML = _RED2(cal6, 5);
	document.getElementById('cal7').innerHTML = _RED2(cal7, 5);
	//
	dilucion1 = document.getElementById('bb1').value*document.getElementById('bb2').value/document.getElementById('aa2').value*document.getElementById('bb3').value/document.getElementById('aa3').value;
	dilucion2 = document.getElementById('bb6').value*document.getElementById('bb7').value/document.getElementById('aa7').value*document.getElementById('bb8').value/document.getElementById('aa8').value;	
	//
	Curvas();
	//
	document.getElementById('dilucion1').innerHTML = _RED2(dilucion1, 5);
	document.getElementById('dilucion2').innerHTML = _RED2(dilucion2, 5);
	var cal3 = (document.getElementById('sor3').value-_intercepto)/_pendiente;
	var cal4 = (document.getElementById('sor4').value-_intercepto)/_pendiente;
	var cal5 = (document.getElementById('sor5').value-_intercepto)/_pendiente;
	var cal8 = (document.getElementById('sor8').value-_intercepto)/_pendiente;
	var cal9 = (document.getElementById('sor9').value-_intercepto)/_pendiente;
	var calA = (document.getElementById('sorA').value-_intercepto)/_pendiente;
	_promcalA = (cal3+cal4+cal5)/3;
	_promcalB = (cal8+cal9+calA)/3;
	
	document.getElementById('cal3').innerHTML = _RED2(cal3, 4);
	document.getElementById('cal4').innerHTML = _RED2(cal4, 4);
	document.getElementById('cal5').innerHTML = _RED2(cal5, 4);
	document.getElementById('cal8').innerHTML = _RED2(cal8, 4);
	document.getElementById('cal9').innerHTML = _RED2(cal9, 4);
	document.getElementById('calA').innerHTML = _RED2(calA, 4);
	//
	//var prom1 = (parseFloat(document.getElementById('cur3').value) + parseFloat(document.getElementById('cur4').value) + parseFloat(document.getElementById('cur5').value))/3; 
	var prom1 = (cal3 + cal4 + cal5)/3; 
	var prom2 = (cal8 + cal9 + calA)/3; 
	//var prom2 = (parseFloat(document.getElementById('cur8').value) + parseFloat(document.getElementById('cur9').value) + parseFloat(document.getElementById('curA').value))/3;  

	if(document.getElementById('unidad').value == '0'){ //m/m
		fin1 = ((prom1/Math.pow(10,6)) * (document.getElementById('bb1').value*(document.getElementById('bb2').value/document.getElementById('aa2').value)*(document.getElementById('bb3').value/document.getElementById('aa3').value))*100) / document.getElementById('mm1').value;
		fin2 = ((prom2/Math.pow(10,6)) * (document.getElementById('bb6').value*(document.getElementById('bb7').value/document.getElementById('aa7').value)*(document.getElementById('bb8').value/document.getElementById('aa8').value))*100) / document.getElementById('mm6').value;
		//fin1 = (((prom1/Math.pow(10,6))*dilucion1)*100)/(document.getElementById('mm1').value*document.getElementById('factor').value);
		//fin2 = (((prom2/Math.pow(10,6))*dilucion2)*100)/(document.getElementById('mm6').value*document.getElementById('factor').value);
	}else{
		fin1 = ((prom1/Math.pow(10,6)) * (document.getElementById('bb1').value*(document.getElementById('bb2').value/document.getElementById('aa2').value)*(document.getElementById('bb3').value/document.getElementById('aa3').value))*document.getElementById('densidad').value*100) / document.getElementById('mm1').value;
		fin2 = ((prom2/Math.pow(10,6)) * (document.getElementById('bb6').value*(document.getElementById('bb7').value/document.getElementById('aa7').value)*(document.getElementById('bb8').value/document.getElementById('aa8').value))*document.getElementById('densidad').value*100) / document.getElementById('mm6').value;
		//fin1 = (((prom1/Math.pow(10,6))*dilucion1)*document.getElementById('densidad').value*100)/(document.getElementById('mm1').value*document.getElementById('factor').value);
		//fin2 = (((prom2/Math.pow(10,6))*dilucion2)*document.getElementById('densidad').value*100)/(document.getElementById('mm6').value*document.getElementById('factor').value);
	}
	
	var promedio = (fin1+fin2)/2;
	document.getElementById('fin1').innerHTML = _RED2(fin1, 4);
	document.getElementById('fin2').innerHTML = _RED2(fin2, 4);
	document.getElementById('promedio').innerHTML = _RED2(promedio, 2);
	
	__calcula2();
}

function __calcula2(){
	var fVarianceA = 0;
	var tmp1 = $('#linealidad1').val()/Math.sqrt(3);
	var tmp2 = $('#linealidad2').val()/Math.sqrt(3);
	var IEC1 = Math.sqrt(Math.pow(2*$('#repeti1').val(), 2) + Math.pow(2*tmp1, 2));
	var IEC2 = Math.sqrt(Math.pow(2*$('#repeti2').val(), 2) + Math.pow(2*tmp2, 2));
	var IR1 = IEC1/$('#mm1').val();
	var IR2 = IEC2/$('#mm6').val();
	//
	var prom = (parseFloat($('#sor3').val()) + parseFloat($('#sor4').val()) + parseFloat($('#sor5').val())) / 3;
	//
	fVarianceA += parseFloat(Math.pow($('#sor3').val() - prom, 2));
	fVarianceA += parseFloat(Math.pow($('#sor4').val() - prom, 2));
	fVarianceA += parseFloat(Math.pow($('#sor5').val() - prom, 2));
	var desv = Math.sqrt(fVarianceA/2);///Math.sqrt(1);
	var absprom_b2 = RAIZ(Math.pow(desv, 2) + Math.pow(_sb, 2));
	var absprom_b1 = prom - _intercepto;
	var absprom_b3 = absprom_b2 / absprom_b1;
	var cn2 = RAIZ( Math.pow(absprom_b3, 2) + Math.pow(_sm/_pendiente, 2) );
	var cn3A = cn2 / _promcalA;
	//
	prom = (parseFloat($('#sor8').val()) + parseFloat($('#sor9').val()) + parseFloat($('#sorA').val())) / 3;
	fVarianceA = 0;
	//
	fVarianceA += parseFloat(Math.pow($('#sor8').val() - prom, 2));
	fVarianceA += parseFloat(Math.pow($('#sor9').val() - prom, 2));
	fVarianceA += parseFloat(Math.pow($('#sorA').val() - prom, 2));
	desv = Math.sqrt(fVarianceA/2);
	absprom_b2 = RAIZ(Math.pow(desv, 2) + Math.pow(_sb, 2));
	absprom_b1 = prom - _intercepto;
	absprom_b3 = absprom_b2 / absprom_b1;
	cn2 = RAIZ( Math.pow(absprom_b3, 2) + Math.pow(_sm/_pendiente, 2) );
	var cn3B = cn2 / _promcalB;
	//
	var BB1 = document.getElementById('bb1').options[document.getElementById('bb1').selectedIndex].getAttribute('IR');
	var BB2 = document.getElementById('bb2').options[document.getElementById('bb2').selectedIndex].getAttribute('IR');
	var BB3 = document.getElementById('bb2').options[document.getElementById('bb2').selectedIndex].getAttribute('IR');
	var AA2 = document.getElementById('aa2').options[document.getElementById('aa2').selectedIndex].getAttribute('inc');
	var AA3 = document.getElementById('aa3').options[document.getElementById('aa3').selectedIndex].getAttribute('inc');	
	var FD2 = RAIZ( Math.pow(BB1, 2) + Math.pow(BB2, 2) + Math.pow(BB3, 2) + Math.pow(AA2, 2) + Math.pow(AA3, 2) );
	var FD3 = FD2/dilucion1;
	//
	if(document.getElementById('unidad').value == '0'){ //m/m
		var IRDEN = 0;
	}else{
		var tol = (0.0001/RAIZ(3))/$('#densidad').val();
		var IRDEN = Math.pow(tol, 2);
	}

	var YIR1 = fin1 * RAIZ( Math.pow(IR1, 2) + Math.pow(cn3A, 2) + Math.pow(FD3, 2) + IRDEN);
	var YIR2 = fin2 * RAIZ( Math.pow(IR2, 2) + Math.pow(cn3B, 2) + Math.pow(FD3, 2) + IRDEN);
	var FIN = (YIR1+YIR2)/2;
	
	document.getElementById('EX').innerHTML = _RED2(FIN, 2);
	return;
}

function Curvas(){
	var sumCN = sumAR = sumA = sumB = sumC = sumD = sumE = sumF = sumG = sumH = sumI = 0;
	
	Pendiente();
	
	for(i=0,j=3;i<4;i++,j++){
		document.getElementById('_CN'+i).innerHTML = document.getElementById('con'+j).innerHTML;
		document.getElementById('_AR'+i).innerHTML = document.getElementById('abs'+j).value;
		document.getElementById('_A'+i).innerHTML = Math.pow(document.getElementById('_CN'+i).innerHTML, 2); 
		document.getElementById('_G'+i).innerHTML = _pendiente * document.getElementById('_CN'+i).innerHTML + parseFloat(_intercepto);
		document.getElementById('_H'+i).innerHTML = Math.abs(document.getElementById('_AR'+i).innerHTML-document.getElementById('_G'+i).innerHTML);
		document.getElementById('_I'+i).innerHTML = Math.pow(document.getElementById('_H'+i).innerHTML, 2); 
		sumA += parseFloat(document.getElementById('_A'+i).innerHTML);
		sumG += parseFloat(document.getElementById('_G'+i).innerHTML);
		sumH += parseFloat(document.getElementById('_H'+i).innerHTML);
		sumI += parseFloat(document.getElementById('_I'+i).innerHTML);
		sumCN += parseFloat(document.getElementById('_CN'+i).innerHTML);
		sumAR += parseFloat(document.getElementById('_AR'+i).innerHTML);
	}
	document.getElementById('_CN'+i).innerHTML = sumCN/4;
	document.getElementById('_AR'+i).innerHTML = sumAR/4;
	document.getElementById('_A'+i).innerHTML = sumA/4;
	document.getElementById('_G'+i).innerHTML = sumG/4;
	document.getElementById('_H'+i).innerHTML = sumH/4;
	document.getElementById('_I'+i).innerHTML = sumI/4;
	document.getElementById('_CN5').innerHTML = sumCN;
	document.getElementById('_AR5').innerHTML = sumAR;
	document.getElementById('_A5').innerHTML = sumA;
	document.getElementById('_G5').innerHTML = sumG;
	document.getElementById('_H5').innerHTML = sumH;
	document.getElementById('_I5').innerHTML = sumI;
	//
	for(i=0,j=3;i<4;i++,j++){
		document.getElementById('_B'+i).innerHTML = document.getElementById('_CN'+i).innerHTML - document.getElementById('_CN4').innerHTML;
		document.getElementById('_C'+i).innerHTML = Math.pow(document.getElementById('_B'+i).innerHTML, 2); 
		document.getElementById('_D'+i).innerHTML = document.getElementById('_AR'+i).innerHTML - document.getElementById('_AR4').innerHTML;
		document.getElementById('_E'+i).innerHTML = Math.pow(document.getElementById('_D'+i).innerHTML, 2); 
		document.getElementById('_F'+i).innerHTML = document.getElementById('_B'+i).innerHTML * document.getElementById('_D'+i).innerHTML;
		sumB += parseFloat(document.getElementById('_B'+i).innerHTML);
		sumC += parseFloat(document.getElementById('_C'+i).innerHTML);
		sumD += parseFloat(document.getElementById('_D'+i).innerHTML);
		sumE += parseFloat(document.getElementById('_E'+i).innerHTML);
		sumF += parseFloat(document.getElementById('_F'+i).innerHTML);
	}
	document.getElementById('_B'+i).innerHTML = sumB/4;
	document.getElementById('_C'+i).innerHTML = sumC/4;
	document.getElementById('_D'+i).innerHTML = sumD/4;
	document.getElementById('_E'+i).innerHTML = sumE/4;
	document.getElementById('_F'+i).innerHTML = sumF/4;
	document.getElementById('_B5').innerHTML = sumB;
	document.getElementById('_C5').innerHTML = sumC;
	document.getElementById('_D5').innerHTML = sumD;
	document.getElementById('_E5').innerHTML = sumE;
	document.getElementById('_F5').innerHTML = sumF;
	//
	var _r = document.getElementById('_F5').innerHTML / Math.sqrt(document.getElementById('_C5').innerHTML*document.getElementById('_E5').innerHTML);
	document.getElementById('r').innerHTML = _RED2(_r, 6);
	_r2 = _r*_r;
	var SYX = Math.sqrt(document.getElementById('_I5').innerHTML / 2);
	document.getElementById('sxy').innerHTML = _RED2(SYX, 3);
	_sm = SYX/Math.sqrt(document.getElementById('_C5').innerHTML);
	document.getElementById('sm').innerHTML = _RED2(_sm, 5);
	_sb = SYX*(Math.sqrt(document.getElementById('_A5').innerHTML/(4*document.getElementById('_C5').innerHTML)));
	document.getElementById('sb').innerHTML = _RED2(_sb, 5);
}

function Pendiente(){
	var x2=0;
	var y=0;
	var x=0;
	var xy=0;
	var cantidad = 4;
	var promP = promC = 0;
	
	for(i=0,j=3;i<cantidad;i++,j++){
		  x2 += parseFloat(document.getElementById('con'+j).innerHTML*document.getElementById('con'+j).innerHTML);
		  y += parseFloat(document.getElementById('abs'+j).value);
		  x += parseFloat(document.getElementById('con'+j).innerHTML);
		  xy += parseFloat(document.getElementById('con'+j).innerHTML*document.getElementById('abs'+j).value);
	}
	
	_pendiente = (cantidad*xy-x*y)/(cantidad*x2-x*x);
	document.getElementById('pendiente').innerHTML = _RED2(_pendiente, 4);
	//INTERCEPTO
	for(i=0,j=3;i<cantidad;i++,j++){
		promC += parseFloat(document.getElementById('con'+j).innerHTML);
		promP += parseFloat(document.getElementById('abs'+j).value);
	}
	promC /= cantidad;
	promP /= cantidad;
	_intercepto = promP-(_pendiente*promC);
	document.getElementById('intercepto').innerHTML = _RED2(_intercepto, 4);
}

function Generar(){	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 2,
		'x0' : document.getElementById('con3').innerHTML,
		'x1' : document.getElementById('con4').innerHTML,
		'x2' : document.getElementById('con5').innerHTML,
		'x3' : document.getElementById('con6').innerHTML,
		'y0' : $('#abs3').val(),
		'y1' : $('#abs4').val(),
		'y2' : $('#abs5').val(),
		'y3' : $('#abs6').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response){
			if(_response == ''){
				OMEGA("No hay datos que mostrar");
				return;
			}
			BETA();
			document.getElementById('td_tabla').innerHTML = _response;
			GeneraGrafico();
			return;
		}
	});
}

function GeneraGrafico(){
	document.getElementById('tr_grafico').style.display = '';

	$(function () {
		$('#container').highcharts({
			legend: {
            	enabled: false
       		},
			data: {
				table: document.getElementById('tabla')
			},
			title: {
                text: 'Curva de calibracion',
                x: -20 //center
            },
			subtitle: {
				text: 'R2=' + _RED2(_r2, 4),
				floating: true,
				align: 'right',
				x: -20,
				verticalAlign: 'bottom',
				y: -75
			},
            yAxis: {
                title: {
                    text: 'Absorbancia'
                }
            },
			xAxis: {
                title: {
                    text: 'Concentraci�n (mg/mL)'
                },
				gridLineWidth: 1
            },
          	tooltip: {
				enabled: false
			}
		});
	});
}

function RAIZ(_num){
	return Math.sqrt(_num);
}