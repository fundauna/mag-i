////ARREGLAR FACTORES
var _FACTOR2 = _MMA1 = _MMB1 = _MMA2 = _MMB2 = _PROM1 = _PROM2 = 0;

function __lolo() {
    document.getElementById('reactivos').value = '10.90';
    document.getElementById('volumen2').value = '20';
    document.getElementById('masaA1').value = '2.5';
    document.getElementById('masaA2').value = '2.5';
    document.getElementById('masaB1').value = '8.50';
    document.getElementById('masaB2').value = '8.40';
    __calcula();
}

function datos() {
    if ($('#fechaA').val() == '') {
        OMEGA('Debe indicar la fecha del an�lisis');
        return;
    }

    if ($('#rango').val() == '') {
        OMEGA('Debe indicar la concentraci�n declarada');
        return;
    }

    if ($('#unidad').val() == '') {
        OMEGA('Debe seleccionar la unidad');
        return;
    }

    if (!confirm('Ingresar ensayo?')) return;

    if (document.getElementById('unidad').value == '0') {
        var lbl = 'm/m';
        var CD = document.getElementById('promA').innerHTML;
        var IC = document.getElementById('suma3').innerHTML;
    } else {
        var lbl = 'm/v';
        var CD = document.getElementById('promB').innerHTML;
        var IC = document.getElementById('suma4').innerHTML;
    }

    //agregado con los campos de las incertidumbres
    //no se ocupan mandar estos datos 
    //var ic_balanza = document.getElementById('incer').innerHTML;
    //var ic_combinada = document.getElementById('incRes').innerHTML;

    var parametros = {
        '_AJAX': 1,
        'xanalizar': $('#xanalizar').val(),
        'tipo': $('#tipo').val(),
        'fechaA': $('#fechaA').val(),
        'rango': $('#rango').val(),
        'unidad': $('#unidad').val(),
        'reactivos': $('#reactivos').val(),
        'factor1': $('#factor1').val(),
        'volumen1': $('#volumen1').val(),
        'densidad': $('#densidad').val(),
        'volumen2': $('#volumen2').val(),
        'masaA1': $('#masaA1').val(),
        'masaA2': $('#masaA2').val(),
        'masaB1': $('#masaB1').val(),
        'masaB2': $('#masaB2').val(),
        'des1': $('#des1').val(),
        'des3': $('#des3').val(),
        'des4': $('#des4').val(),
        'CD': CD,
        'IC': IC,
        'obs': $('#obs').val(),
        'cer': $('#cert').val(),
        'res': $('#res').val()
    }

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    break;
                case '1':
                    opener.location.reload();
                    alert("Transaccion finalizada");
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function Redondear(txt) {
    _RED(txt, 6);
    __calcula();
}

function Redondear2(txt) {
    _RED(txt, 9);
    __calcula();
}

function __calcula() {
    if (document.getElementById('unidad').value == '') return;
    //
    _FACTOR2 = document.getElementById('factor1').value / (document.getElementById('volumen1').value - document.getElementById('reactivos').value);
    _MMA1 = (document.getElementById('volumen2').value - document.getElementById('masaB1').value) * _FACTOR2;
    _MMB1 = _MMA1 * document.getElementById('densidad').value;
    _MMA2 = (document.getElementById('volumen2').value - document.getElementById('masaB2').value) * _FACTOR2;
    _MMB2 = _MMA2 * document.getElementById('densidad').value;
    _PROM1 = (_MMA1 + _MMA2) / 2;
    _PROM2 = (_MMB1 + _MMB2) / 2;

    var promCTAB = (parseFloat(document.getElementById('masaB1').value) + parseFloat(document.getElementById('masaB2').value)) / 2;

    var tkn1 = Math.pow(0.05 / Math.sqrt(6), 2);
    var tkn2 = parseFloat(tkn1) + parseFloat(tkn1);
    var tkn3 = Math.sqrt(tkn2);
    var tkn4 = (document.getElementById('volumen1').value - document.getElementById('reactivos').value);
    var tkn5 = Math.pow(tkn3 / tkn4, 2);
    var tkmax = parseFloat(Math.pow(0.01 / document.getElementById('factor1').value, 2)) + parseFloat(tkn5);
    var des2 = _FACTOR2 * Math.sqrt(tkmax);
    //var des2 = math.std(a, b, c, �)
    ;
    //
    var fVarianceA = 0;
    fVarianceA += parseFloat(Math.pow(_MMB1 - _PROM2, 2));
    fVarianceA += parseFloat(Math.pow(_MMB2 - _PROM2, 2));
    var des5 = Math.sqrt(fVarianceA) / Math.sqrt(1);
    ///////

    ///////////////
    var est1 = document.getElementById('des1').value / Math.sqrt(6);
    var est2 = des2 / Math.sqrt(3);
    var est3 = document.getElementById('des3').value / Math.sqrt(6);
    var est4 = document.getElementById('des4').value / 2;
    var est5 = des5 / Math.sqrt(2);
    var lolo1 = Math.pow(est1, 2);
    var lolo2 = 0;
    var lolo3 = Math.pow(est3, 2);
    var lolo4 = 0;
    var lolo5 = Math.pow(est5, 2);
    var lolo6 = Math.pow(est51, 2);


    //inc certificado
    var inCer= document.getElementById('cert').value / 2;
    document.getElementById('inCer').innerHTML = inCer.toFixed(5);
    //inc resolucion
    var incRes = document.getElementById('res').value/ Math.sqrt(12);
    document.getElementById('incRes').innerHTML = incRes.toFixed(5);
    // inc den
    var incDen = Math.sqrt(Math.pow(inCer,2) + Math.pow(incRes,2));
    document.getElementById('des4').innerHTML = incDen.toFixed(5);



    //desv estandard
    var val1 =  (document.getElementById('volumen2').value - document.getElementById('masaB1').value) * _FACTOR2;
    var val2 =  (document.getElementById('volumen2').value - document.getElementById('masaB2').value) * _FACTOR2;
    var promedio = (val1 + val2)/2;

    var val3 =  _MMA1 * document.getElementById('densidad').value;
    var val4 = _MMA2 * document.getElementById('densidad').value;
    var promedio1 = (val3 + val4)/2;


    var desvmm = Math.sqrt((Math.pow((val1-promedio),2) + Math.pow(( val2 - promedio),2)/1));
    var desvmv = Math.sqrt((Math.pow((val3-promedio1),2) + Math.pow(( val4 - promedio1),2)/1));

    document.getElementById('desvA').innerHTML = desvmm.toFixed(4);
    document.getElementById('desvB').innerHTML = desvmv.toFixed(4);
//
var vol1 =  parseFloat($('#masaB1').val());
var vol2 = parseFloat($('#masaB2').val());

var promeVol = (vol1 + vol2) / 2;

var desvStaMm = desvmm / Math.sqrt(2);
var desvStaVol = desvmv / Math.sqrt(2);





    document.getElementById('factor2').innerHTML = _RED2(_FACTOR2, 4);
    document.getElementById('mmA1').innerHTML = _RED2(_MMA1, 3);
    document.getElementById('mmB1').innerHTML = _RED2(_MMB1, 3);
    document.getElementById('mmA2').innerHTML = _RED2(_MMA2, 3);
    document.getElementById('mmB2').innerHTML = _RED2(_MMB2, 3);
    document.getElementById('promA').innerHTML = _RED2(_PROM1, 3);
    document.getElementById('promB').innerHTML = _RED2(_PROM2, 3);
   // document.getElementById('val1').innerHTML = _RED2(promeVol, 3);
    document.getElementById('val1').innerHTML = _RED2(promCTAB, 3);
    document.getElementById('val2').innerHTML = _RED2(_FACTOR2, 4);
    document.getElementById('val3').innerHTML = document.getElementById('volumen2').value;
    document.getElementById('val4').innerHTML = document.getElementById('densidad').value;
    document.getElementById('des2').innerHTML = _RED2(des2, 4);
    document.getElementById('des51').innerHTML = desvmm.toFixed(4);
    document.getElementById('des5').innerHTML = des5.toFixed(4);
    document.getElementById('est1').innerHTML = _RED2(est1, 3);
    document.getElementById('est2').innerHTML = document.getElementById('des2').innerHTML;
    document.getElementById('est3').innerHTML = _RED2(est3, 3);
    document.getElementById('est4').innerHTML = incDen.toFixed(5);
    document.getElementById('est5').innerHTML = _RED2(est5, 3);
    document.getElementById('est51').innerHTML = _RED2(desvStaMm, 3);
    document.getElementById('cua1').innerHTML = _RED2(lolo1, 5);
    document.getElementById('cua2').innerHTML = _RED2((Math.pow(document.getElementById('est2').innerHTML / document.getElementById('val2').innerHTML, 2)), 11);
    document.getElementById('cua3').innerHTML = _RED2(lolo3, 5);
    document.getElementById('cua4').innerHTML = _RED2((Math.pow(document.getElementById('est4').innerHTML / document.getElementById('val4').innerHTML, 2)), 10);
    document.getElementById('cua5').innerHTML = _RED2(lolo5, 4);



  //  var incMue1mm = document.getElementById('masaA1').innerHTML * Math.sqrt(parseFloat(Math.pow(Math.sqrt(parseFloat(document.getElementById('cua1').innerHTML) + parseFloat(document.getElementById('cua3').innerHTML)) / (document.getElementById('val3').innerHTML - document.getElementById('val1').innerHTML), 2)) + parseFloat(document.getElementById('cua2').innerHTML));

    var mas1 = document.getElementById('masaA1').value;
    var mas2 = document.getElementById('masaA2').value;

    var vol1 = document.getElementById('masaB1').value;
    var vol2 = document.getElementById('masaB2').value;



  // var suma1 = document.getElementById('promA').innerHTML * Math.sqrt(parseFloat(Math.pow(Math.sqrt(parseFloat(document.getElementById('cua1').innerHTML) + parseFloat(document.getElementById('cua3').innerHTML)) / (document.getElementById('val3').innerHTML - document.getElementById('val1').innerHTML), 2)) + parseFloat(document.getElementById('cua2').innerHTML));



    var suma1 = _MMA1 * Math.sqrt(parseFloat(Math.pow(Math.sqrt(parseFloat(document.getElementById('cua1').innerHTML) + parseFloat(document.getElementById('cua3').innerHTML)) / (document.getElementById('val3').innerHTML - vol1 ), 2)) + parseFloat(document.getElementById('cua2').innerHTML));
    var suma2 = _MMA2 * Math.sqrt(parseFloat(Math.pow(Math.sqrt(parseFloat(document.getElementById('cua1').innerHTML) + parseFloat(document.getElementById('cua3').innerHTML)) / (document.getElementById('val3').innerHTML - vol2), 2)) + parseFloat(document.getElementById('cua2').innerHTML));

    var suma7 = _MMB1 * Math.sqrt(parseFloat(Math.pow(Math.sqrt(parseFloat(document.getElementById('cua1').innerHTML) + parseFloat(document.getElementById('cua3').innerHTML)) / (document.getElementById('val3').innerHTML - vol1 ), 2)) + parseFloat(document.getElementById('cua2').innerHTML) + Math.pow((document.getElementById('des4').innerHTML /  document.getElementById('val4').innerHTML),2));
    var suma8 = _MMB2 * Math.sqrt(parseFloat(Math.pow(Math.sqrt(parseFloat(document.getElementById('cua1').innerHTML) + parseFloat(document.getElementById('cua3').innerHTML)) / (document.getElementById('val3').innerHTML - vol2), 2)) + parseFloat(document.getElementById('cua2').innerHTML)  + Math.pow((document.getElementById('des4').innerHTML /  document.getElementById('val4').innerHTML),2));

    var maxmm=(Math.max(suma1,suma2));
    var maxmv=(Math.max(suma7,suma8));

    //var suma1 = document.getElementById('promA').innerHTML * Math.sqrt(parseFloat(Math.pow(Math.sqrt(parseFloat(document.getElementById('cua1').innerHTML) + parseFloat(document.getElementById('cua3').innerHTML)) / (document.getElementById('val3').innerHTML - document.getElementById('val1').innerHTML), 2)) + parseFloat(document.getElementById('cua2').innerHTML));
   // var suma2 = document.getElementById('promB').innerHTML * Math.sqrt(parseFloat(Math.pow(Math.sqrt(parseFloat(document.getElementById('cua1').innerHTML) + parseFloat(document.getElementById('cua3').innerHTML)) / (document.getElementById('val3').innerHTML - document.getElementById('val1').innerHTML), 2)) + parseFloat(document.getElementById('cua2').innerHTML) + parseFloat(document.getElementById('cua4').innerHTML));
    var suma3 = Math.sqrt(parseFloat(Math.pow(suma1, 2) + Math.pow(desvStaMm ,2),2));
    var suma4 = Math.sqrt(parseFloat(Math.pow(suma2, 2) + Math.pow(desvStaVol ,2),2));
   // var suma4 = Math.sqrt(parseFloat(Math.pow(document.getElementById('suma2').innerHTML / Math.sqrt(2), 2)) + parseFloat(document.getElementById('desvB').innerHTML / Math.sqrt(2), 2));




    document.getElementById('incMue1mm').innerHTML = _RED2(suma1, 4);
    document.getElementById('incMue1mv').innerHTML = _RED2(suma7, 4);
    document.getElementById('incMue2mm').innerHTML = _RED2(suma2, 4);
    document.getElementById('incMue2mv').innerHTML = _RED2(suma8, 4);





    document.getElementById('suma1').innerHTML = _RED2(maxmm, 4);
    document.getElementById('suma2').innerHTML = _RED2(maxmv, 4);
    document.getElementById('suma3').innerHTML = _RED2(suma3, 3);
    document.getElementById('suma4').innerHTML = _RED2(suma4, 3);
    document.getElementById('suma5').innerHTML = _RED2(suma3 * 2, 3);
    document.getElementById('suma6').innerHTML = _RED2(suma4 * 2, 3);


}