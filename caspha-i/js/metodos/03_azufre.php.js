var _decimales = 4;
var contenido1 = contenido1 = 0;
var _conc = 0;

function __lolo(){
	//document.getElementById('unidad').selectedIndex=2;
	document.getElementById('numreactivo').value = 'Reactivo 1';
	document.getElementById('fechaA').value = '01/01/2001';
	document.getElementById('fechaP').value = '02/01/2001';
	document.getElementById('ampolla1').value = '0.1';
	document.getElementById('ampolla2').value = '0.00070';
	document.getElementById('BA').selectedIndex=7;
	document.getElementById('densidad').value = '1.36';
	document.getElementById('muestra1').value = '1.4265';
	document.getElementById('muestra2').value = '1.6265';
	document.getElementById('consumido1').value = '15';
	document.getElementById('consumido2').value = '19';
	document.getElementById('origen').selectedIndex=1;
	__calcula();
}

function datos(){	
	if($('#ingrediente').val()==''){
		OMEGA('Debe indicar el ingrediente activo');
		return;
	}
	
	if($('#fechaA').val()==''){
		OMEGA('Debe indicar la fecha del an�lisis');
		return;
	}
	
	if($('#rango').val()==''){
		OMEGA('Debe indicar la concentraci�n declarada');
		return;
	}
	
	if($('#unidad').val()==''){
		OMEGA('Debe seleccionar la unidad');
		return;
	}
	
	if($('#tipo_form').val()==''){
		OMEGA('Debe seleccionar el tipo de formulaci�n');
		return;
	}
	
	if($('#fechaP').val()==''){
		OMEGA('Debe indicar la fecha de preparaci�n');
		return;
	}
	
	if(!confirm('Ingresar ensayo?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'xanalizar' : $('#xanalizar').val(),
		'tipo' : $('#tipo').val(),
		'ingrediente' : $('#ingrediente').val(),
		'fechaA' : $('#fechaA').val(),
		'rango' : $('#rango').val(),
		'unidad' : $('#unidad').val(),
		'tipo_form' : $('#tipo_form').val(),
		'numreactivo' : $('#numreactivo').val(),
		'fechaP' : $('#fechaP').val(),
		'IECB' : $('#IECB').val(),
		'linealidad1' : $('#linealidad1').val(),
		'linealidad2' : $('#linealidad2').val(),
		'repeti1' : $('#repeti1').val(),
		'repeti2' : $('#repeti2').val(),
		'ampolla1' : $('#ampolla1').val(),
		'ampolla2' : $('#ampolla2').val(),
		'muestra1' : $('#muestra1').val(),
		'muestra2' : $('#muestra2').val(),
		'BA' : $('#BA').val(),
		'consumido1' : $('#consumido1').val(),
		'consumido2' : $('#consumido2').val(),
		'origen' : $('#origen').val(),
		'contenido1' : $('#contenido1').val(),
		'contenido2' : $('#contenido2').val(),
		'contenido3' : $('#contenido3').val(),
		'contenido4' : $('#contenido4').val(),
		'densidad' : $('#densidad').val(),
		'promedio1' : $('#promedio1').val(),
		'incertidumbre1' : $('#incertidumbre1').val(),
		'promedio2' : $('#promedio2').val(),
		'incertidumbre2' : $('#incertidumbre2').val(),
		'obs' : $('#obs').val()	
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					opener.location.reload();
					alert("Transaccion finalizada");
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Redondear(txt){
	_RED(txt, _decimales);
	__calcula();
}

function __calcula(){
	if(document.getElementById('unidad').value == '') return;
	
	if(document.getElementById('unidad').value == '0'){ //m/m
		contenido1 = ((((document.getElementById('consumido1').value/1000)*document.getElementById('ampolla1').value)*32.066*2*100*2.5)/document.getElementById('muestra1').value);
		contenido2 = ((((document.getElementById('consumido2').value/1000)*document.getElementById('ampolla1').value)*32.066*2*100*2.5)/document.getElementById('muestra2').value);
		var promedio = (contenido1 + contenido2) / 2;
		document.getElementById('contenido1').value = _RED2(contenido1,4);
		document.getElementById('contenido2').value = _RED2(contenido2,4);
		document.getElementById('promedio1').value = _RED2(promedio,2);
		document.getElementById('promedio2').value = '';
		document.getElementById('contenido3').value = '';
		document.getElementById('contenido4').value = '';
	}else{
		contenido1 = ((((document.getElementById('consumido1').value/1000)*document.getElementById('ampolla1').value)*32.066*2*100*2.5*document.getElementById('densidad').value)/document.getElementById('muestra1').value);
		contenido2 = ((((document.getElementById('consumido2').value/1000)*document.getElementById('ampolla1').value)*32.066*2*100*2.5*document.getElementById('densidad').value)/document.getElementById('muestra2').value);
		var promedio = (contenido1 + contenido2) / 2;
		document.getElementById('contenido3').value = _RED2(contenido1,4);
		document.getElementById('contenido4').value = _RED2(contenido2,4);
		document.getElementById('promedio2').value = _RED2(promedio, 2);
		document.getElementById('promedio1').value = '';
		document.getElementById('contenido1').value = '';
		document.getElementById('contenido2').value = '';
	}	
	__calcula2(contenido1,contenido2);
}

function __calcula2(contenido1,contenido2){
	var inccomr1 = __inccomr1(); 
	var inccomr2 = __inccomr2();
	var desvest = __desvest(contenido1,contenido2); 
	
	var incexp = 2 * Math.sqrt(Math.pow(((inccomr1 + inccomr2)/2)/Math.sqrt(2),2) + Math.pow(desvest * (1.189 / Math.sqrt(2)),2));
	
	if(document.getElementById('unidad').value == '0'){ //m/m
		document.getElementById('incertidumbre1').value = _RED2(incexp, 2);
		document.getElementById('incertidumbre2').value = '';
	}else{
		document.getElementById('incertidumbre2').value = _RED2(incexp, 2);
		document.getElementById('incertidumbre1').value = '';
	}
}

function __inccomr1(){
	if(document.getElementById('unidad').value == '0'){
		var contenidoS = document.getElementById('contenido1').value;
		var extra = 0;
	}else{ 
		var contenidoS = document.getElementById('contenido3').value;
		var extra = Math.pow((0.0001*Math.sqrt(3))/document.getElementById('densidad').value,2);
	}
	if(document.getElementById('origen').value == '1'){
		var r = Math.pow((((0.02/100)*3*document.getElementById('ampolla1').value) / Math.sqrt(3)),2);
		var t = Math.pow((document.getElementById('ampolla2').value / 2),2);
		var w = Math.pow(Math.sqrt(r+t) / document.getElementById('ampolla1').value,2);
		////////////////////////////////////////////////////////////////////////////////////////////S
		r = Math.pow(document.getElementById('BA').options[document.getElementById('BA').selectedIndex].getAttribute('TOL') / Math.sqrt(6),2);
		t = Math.pow(document.getElementById('BA').options[document.getElementById('BA').selectedIndex].getAttribute('TEMP') / Math.sqrt(3),2);
		y = Math.pow(document.getElementById('BA').options[document.getElementById('BA').selectedIndex].getAttribute('REP'),2);
		var q = Math.pow(Math.sqrt(r+t+y) / document.getElementById('BA').value,2);
		var pot1 = document.getElementById('ampolla1').value * Math.sqrt(Math.pow(Math.sqrt(w+q),2));
		pot1 = Math.pow(pot1/document.getElementById('ampolla1').value,2);
	}else{
		var pot1 = document.getElementById('ampolla2').value / 2;
	}
	var pot2 = Math.sqrt(2*(Math.pow(0.05/Math.sqrt(6),2)+Math.pow(0.032/Math.sqrt(3),2)+Math.pow(0.02,2)+Math.pow(0.1/(2*Math.sqrt(3)),2)));
	pot2 = Math.pow(pot2/document.getElementById('consumido1').value,2);
	var pot3 = Math.pow((0.006/Math.sqrt(3))/32.066,2);
	var pot4 = Math.sqrt(Math.pow(0.1/Math.sqrt(3),2)+Math.pow(0.067/2,2)+Math.pow((2.92*0.032)/Math.sqrt(3),2)+Math.pow(0.081/2,2)) / 1000;
	pot4 = Math.pow(pot4/document.getElementById('muestra1').value,2);
	var pot5 = Math.sqrt(Math.pow(0.12/Math.sqrt(6),2)+Math.pow(0.158/Math.sqrt(3),2)+Math.pow(0.03,2));
	pot5 = Math.pow(pot5/250,2);
	var pot6 = Math.sqrt(Math.pow(0.08/Math.sqrt(6),2)+Math.pow(0.063/Math.sqrt(3),2));
	pot6 = Math.pow(pot5/100,2);
	inc = contenidoS * Math.sqrt(pot1+pot2+pot3+pot4+pot5+pot6+extra);	
	return inc;
}

function __inccomr2(){
	if(document.getElementById('unidad').value == '0'){
		var contenidoS = document.getElementById('contenido1').value;
		var extra = 0;
	}else{ 
		var contenidoS = document.getElementById('contenido3').value;
		var extra = Math.pow((0.0001*Math.sqrt(3))/document.getElementById('densidad').value,2);
	}
	if(document.getElementById('origen').value == '1'){
		var r = Math.pow((((0.02/100)*3*document.getElementById('ampolla1').value) / Math.sqrt(3)),2);
		var t = Math.pow((document.getElementById('ampolla2').value / 2),2);
		var w = Math.pow(Math.sqrt(r+t) / document.getElementById('ampolla1').value,2);
		////////////////////////////////////////////////////////////////////////////////////////////S
		r = Math.pow(document.getElementById('BA').options[document.getElementById('BA').selectedIndex].getAttribute('TOL') / Math.sqrt(6),2);
		t = Math.pow(document.getElementById('BA').options[document.getElementById('BA').selectedIndex].getAttribute('TEMP') / Math.sqrt(3),2);
		y = Math.pow(document.getElementById('BA').options[document.getElementById('BA').selectedIndex].getAttribute('REP'),2);
		var q = Math.pow(Math.sqrt(r+t+y) / document.getElementById('BA').value,2);
		var pot1 = document.getElementById('ampolla1').value * Math.sqrt(Math.pow(Math.sqrt(w+q),2));
		pot1 = Math.pow(pot1/document.getElementById('ampolla1').value,2);
	}else{
		var pot1 = document.getElementById('ampolla2').value / 2;
	}
	var pot2 = Math.sqrt(2*(Math.pow(0.05/Math.sqrt(6),2)+Math.pow(0.032/Math.sqrt(3),2)+Math.pow(0.02,2)+Math.pow(0.1/(2*Math.sqrt(3)),2)));
	pot2 = Math.pow(pot2/document.getElementById('consumido2').value,2);
	var pot3 = Math.pow((0.006/Math.sqrt(3))/32.066,2);
	var pot4 = Math.sqrt(Math.pow(0.1/Math.sqrt(3),2)+Math.pow(0.067/2,2)+Math.pow((2.92*0.032)/Math.sqrt(3),2)+Math.pow(0.081/2,2)) / 1000;
	pot4 = Math.pow(pot4/document.getElementById('muestra2').value,2);
	var pot5 = Math.sqrt(Math.pow(0.12/Math.sqrt(6),2)+Math.pow(0.158/Math.sqrt(3),2)+Math.pow(0.03,2));
	pot5 = Math.pow(pot5/250,2);
	var pot6 = Math.sqrt(Math.pow(0.08/Math.sqrt(6),2)+Math.pow(0.063/Math.sqrt(3),2));
	pot6 = Math.pow(pot5/100,2);
	inc = contenidoS * Math.sqrt(pot1+pot2+pot3+pot4+pot5+pot6+extra);	
	return inc;
}

function __desvest(contenido1,contenido2){
	var media = (contenido1 + contenido2) / 2;
	var diff1 = Math.pow(contenido1 - media,2);
	var diff2 = Math.pow(contenido2 - media,2);
	var varianza = Math.sqrt(diff1 + diff2 / 1);
	return varianza;
}

function RAIZ(_num){
	return Math.sqrt(_num);
}

function PipetasIEC(_tam){
	if(_tam==0.5) return 0.002;
	else if(_tam==1) return 0.006;
	else if(_tam==2) return 0.004;
	else if(_tam==3) return 0.004;
	else if(_tam==4) return 0.004;
	else if(_tam==5) return 0.006;
	else if(_tam==6) return 0.005;
	else if(_tam==7) return 0.005;
	else if(_tam==10) return 0.010;
	else if(_tam==15) return 0.013;
	else if(_tam==20) return 0.014;
	else if(_tam==25) return 0.015;
	else if(_tam==50) return 0.026;
	else if(_tam==100) return 0.046;
}