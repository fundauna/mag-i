var _decimales = 4;
var res1 = 0;
var cuA = cuB = cuC = cuD = cuE = cuF = 0;
var coA = coB = coC = coD = coE = coF = 0;

function __lolo(){
	document.getElementById('unidad').selectedIndex=2;
	document.getElementById('ampolla1').value = '0.1';
	document.getElementById('ampolla2').value = '0.1';
	document.getElementById('balon').selectedIndex = 7;
	document.getElementById('bureta').selectedIndex = 3;
	document.getElementById('densidad').value = '1.0733';
	document.getElementById('factor').selectedIndex = 2;
	document.getElementById('masaA').value = '3.1000';
	document.getElementById('masaB').value = '2.9978';
	document.getElementById('masaC').value = '2.7890';
	document.getElementById('masaD').value = '2.8123';
	document.getElementById('masaE').value = '2.9505';
	document.getElementById('masaF').value = '3.0000';
	document.getElementById('consuA').value = '17.30';
	document.getElementById('consuB').value = '16.95';
	document.getElementById('consuC').value = '14.70';
	document.getElementById('consuD').value = '15.00';
	document.getElementById('consuE').value = '18.10';
	document.getElementById('consuF').value = '18.50';
	document.getElementById('alicuotaA').selectedIndex = 10;
	document.getElementById('alicuotaB').selectedIndex = 10;
	document.getElementById('alicuotaE').selectedIndex = 8;
	document.getElementById('alicuotaF').selectedIndex = 8;
	document.getElementById('balA').selectedIndex = 5;
	document.getElementById('balB').selectedIndex = 5;
	document.getElementById('balE').selectedIndex = 5;
	document.getElementById('balF').selectedIndex = 5;
	
	__calcula();
}

function datos(){	
	if($('#ingrediente').val()==''){
		OMEGA('Debe indicar el ingrediente activo');
		return;
	}
	
	if($('#fechaA').val()==''){
		OMEGA('Debe indicar la fecha del an�lisis');
		return;
	}
	
	if($('#rango').val()==''){
		OMEGA('Debe indicar la concentraci�n declarada');
		return;
	}
	
	if($('#unidad').val()==''){
		OMEGA('Debe seleccionar la unidad');
		return;
	}
	
	if($('#tipo_form').val()==''){
		OMEGA('Debe seleccionar el tipo de formulaci�n');
		return;
	}
	
	if($('#numreactivo').val()==''){
		OMEGA('Debe indicar el n�mero de reactivo');
		return;
	}
	
	if($('#fechaP').val()==''){
		OMEGA('Debe indicar la fecha de preparaci�n');
		return;
	}
	
	if(!confirm('Ingresar ensayo?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'xanalizar' : $('#xanalizar').val(),
		'tipo' : $('#tipo').val(),
		'ingrediente' : $('#ingrediente').val(),
		'fechaA' : $('#fechaA').val(),
		'rango' : $('#rango').val(),
		'unidad' : $('#unidad').val(),
		'tipo_form' : $('#tipo_form').val(),
		'numreactivo' : $('#numreactivo').val(),
		'fechaP' : $('#fechaP').val(),
		'IECB' : $('#IECB').val(),
		'linealidad1' : $('#linealidad1').val(),
		'linealidad2' : $('#linealidad2').val(),
		'repeti1' : $('#repeti1').val(),
		'repeti2' : $('#repeti2').val(),
		'metodo' : $('#metodo').val(),
		'bureta' : $('#bureta').val(),
		'ampolla1' : $('#ampolla1').val(),
		'ampolla2' : $('#ampolla2').val(),
		'densidad' : $('#densidad').val(),
		'factor' : $('#factor').val(),
		'balon' : $('#balon').val(),
		'masaA' : $('#masaA').val(),
		'masaB' : $('#masaB').val(),
		'masaC' : $('#masaC').val(),
		'masaD' : $('#masaD').val(),
		'masaE' : $('#masaE').val(),
		'masaF' : $('#masaF').val(),
		'consuA' : $('#consuA').val(),
		'consuB' : $('#consuB').val(),
		'consuC' : $('#consuC').val(),
		'consuD' : $('#consuD').val(),
		'consuE' : $('#consuE').val(),
		'consuF' : $('#consuF').val(),
		'alicuotaA' : $('#alicuotaA').val(),
		'alicuotaB' : $('#alicuotaB').val(),
		'alicuotaE' : $('#alicuotaE').val(),
		'alicuotaF' : $('#alicuotaF').val(),
		'balA' : $('#balA').val(),
		'balB' : $('#balB').val(),
		'balE' : $('#balE').val(),
		'balF' : $('#balF').val(),
		'finA' : document.getElementById('finA').innerHTML,
		'finB' : document.getElementById('finB').innerHTML,
		'finC' : document.getElementById('finC').innerHTML,
		'finD' : document.getElementById('finD').innerHTML,
		'contenido1' : document.getElementById('cuA').innerHTML,
		'contenido2' : document.getElementById('cuB').innerHTML,
		'obs' : $('#obs').val()	
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
                    console.log(_response);
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					opener.location.reload();
					alert("Transaccion finalizada");
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function __calcula(){
	__limpia();
	
	if(document.getElementById('unidad').value == '') return;
	
	var finA = finB = finC = finD = 0
	var densidad = 1;
	res1 = $('#ampolla1').val()/($('#balon').val()/1000);
	var res2 = '';
	
	if(document.getElementById('unidad').value == '1') densidad = $('#densidad').val(); //m/v
	
	if( $('#metodo').val()=='A' || $('#metodo').val()=='C' ){
		res2 = $('#balA').val()/$('#alicuotaA').val();
		
		cuA = (($('#consuA').val()/1000)*res1*($('#balA').val()/$('#alicuotaA').val())*63.546*densidad*100)/$('#masaA').val();
		cuB = (($('#consuB').val()/1000)*res1*($('#balB').val()/$('#alicuotaB').val())*63.546*densidad*100)/$('#masaB').val();
		coA = (($('#consuA').val()/1000)*res1*($('#balA').val()/$('#alicuotaA').val())*$('#factor').val()*densidad*63.546*100)/$('#masaA').val();
		coB = (($('#consuB').val()/1000)*res1*($('#balB').val()/$('#alicuotaB').val())*$('#factor').val()*densidad*63.546*100)/$('#masaB').val();
		finA = (cuA+cuB)/2;
		finC = (coA+coB)/2;
		
		document.getElementById('cuA').innerHTML = _RED2(cuA, 2);
		document.getElementById('cuB').innerHTML = _RED2(cuB, 2);
		document.getElementById('coA').innerHTML = _RED2(coA, 2);
		document.getElementById('coB').innerHTML = _RED2(coB, 2);
	}else if( $('#metodo').val()=='B' ){
		cuC = (($('#consuC').val()/1000)*res1*63.546*densidad*100)/$('#masaC').val();
		cuD = (($('#consuD').val()/1000)*res1*63.546*densidad*100)/$('#masaD').val();
		coC = (($('#consuC').val()/1000)*res1*$('#factor').val()*densidad*63.546*100)/$('#masaC').val();
		coD = (($('#consuD').val()/1000)*res1*$('#factor').val()*densidad*63.546*100)/$('#masaD').val();
		finA = (cuC+cuD)/2;
		finC = (coC+coD)/2;
		
		document.getElementById('cuC').innerHTML = _RED2(cuC, 2);
		document.getElementById('cuD').innerHTML = _RED2(cuD, 2);
		document.getElementById('coC').innerHTML = _RED2(coC, 2);
		document.getElementById('coD').innerHTML = _RED2(coD, 2);
	}else if( $('#metodo').val()=='D' ){
		res2 = $('#balE').val()/$('#alicuotaF').val();
		
		cuE = (($('#consuE').val()/1000)*res1*($('#balE').val()/$('#alicuotaE').val())*63.546*densidad*100)/$('#masaE').val();
		cuF = (($('#consuF').val()/1000)*res1*($('#balF').val()/$('#alicuotaF').val())*63.546*densidad*100)/$('#masaF').val();
		coE = (($('#consuE').val()/1000)*res1*($('#balE').val()/$('#alicuotaE').val())*$('#factor').val()*densidad*63.546*100)/$('#masaE').val();
		coF = (($('#consuF').val()/1000)*res1*($('#balF').val()/$('#alicuotaF').val())*$('#factor').val()*densidad*63.546*100)/$('#masaF').val();
		finA = (cuE+cuF)/2;
		finC = (coE+coF)/2;
		
		document.getElementById('cuE').innerHTML = _RED2(cuE, 2);
		document.getElementById('cuF').innerHTML = _RED2(cuF, 2);
		document.getElementById('coE').innerHTML = _RED2(coE, 2);
		document.getElementById('coF').innerHTML = _RED2(coF, 2);
	}
	
	document.getElementById('res1').innerHTML = _RED2(res1, 4);
	document.getElementById('res2').innerHTML = _RED2(res2, 2);
	document.getElementById('finA').innerHTML = _RED2(finA, 2);
	document.getElementById('finC').innerHTML = _RED2(finC, 2);
	
	__calcula2();
}

function __calcula2(){
	var tmp1 = $('#linealidad1').val()/Math.sqrt(3);
	var tmp2 = $('#linealidad2').val()/Math.sqrt(3);
	var IEC1 = Math.sqrt(Math.pow(2*$('#repeti1').val(), 2) + Math.pow(2*tmp1, 2));
	var IEC2 = Math.sqrt(Math.pow(2*$('#repeti2').val(), 2) + Math.pow(2*tmp2, 2));


	if( $('#metodo').val()=='A' || $('#metodo').val()=='C' ){
		var IR1 = IEC1/$('#masaA').val();
		var IR2 = IEC2/$('#masaB').val();
		var NA1 = $('#IECB').val()/$('#consuA').val();
		var NA2 = $('#IECB').val()/$('#consuB').val();
		var pipeta1 = document.getElementById('alicuotaA');
		var balon1 = document.getElementById('balA');
		var BAIR1 = pipeta1.options[pipeta1.selectedIndex].getAttribute('inc');
		var BAIR2 = balon1.options[balon1.selectedIndex].getAttribute('IR');
		var tmp1 = Math.sqrt(((Math.pow(BAIR2, 2)) + (Math.pow(BAIR1, 2))),2 );
		var tmp2 = balon1.value/pipeta1.value;
		var RES3 = tmp1/tmp2
	}else if( $('#metodo').val()=='B' ){
		var IR1 = IEC1/$('#masaC').val();
		var IR2 = IEC2/$('#masaD').val();
		var NA1 = $('#IECB').val()/$('#consuC').val();
		var NA2 = $('#IECB').val()/$('#consuD').val();
	}else if( $('#metodo').val()=='D' ){
		var IR1 = IEC1/$('#masaE').val();
		var IR2 = IEC2/$('#masaF').val();
		var NA1 = $('#IECB').val()/$('#consuD').val();
		var NA2 = $('#IECB').val()/$('#consuE').val();
		var pipeta1 = document.getElementById('alicuotaE');
		var balon1 = document.getElementById('balE');
		var BAIR1 = pipeta1.options[pipeta1.selectedIndex].getAttribute('inc');
		var BAIR2 = balon1.options[balon1.selectedIndex].getAttribute('IR');
		var tmp1 = RAIZ( Math.pow(BAIR2, 2) + Math.pow(BAIR1, 2) );
		var tmp2 = balon1.value/pipeta1.value;
		var RES3 = tmp1/tmp2
	}
	
	var control = document.getElementById('balon');
	var BAIR = control.options[control.selectedIndex].getAttribute('IR');
	var tmp = (RAIZ( Math.pow(0.0007/RAIZ(3), 2) )) / $('#ampolla1').val();
	var _tol1 = RAIZ( Math.pow(tmp, 2) + Math.pow(BAIR, 2) );
	var RES2 = _tol1 / res1;


	if( $('#metodo').val()=='A' || $('#metodo').val()=='C' ){
		var MM1 = RAIZ( Math.pow(IR1, 2) + Math.pow(RES2, 2) + Math.pow(NA1, 2) + Math.pow(RES3, 2) );
		var MM2 = RAIZ( Math.pow(IR2, 2) + Math.pow(RES2, 2) + Math.pow(NA2, 2) + Math.pow(RES3, 2) );
		var ME1 = MM1 * cuA;
		var ME2 = MM2 * cuB;
		var ME3 = MM1 * coA;
		var ME4 = MM2 * coB;
		var fin1 = (ME1 + ME2);
		var fin2 = ME3 + ME4;

//aqui va nueva
		var nue= ($('#ampolla1').val() * 0 );




	}else if( $('#metodo').val()=='B' ){
		var MM1 = RAIZ( Math.pow(IR1, 2) + Math.pow(RES2, 2) + Math.pow(NA1, 2) );
		var MM2 = RAIZ( Math.pow(IR2, 2) + Math.pow(RES2, 2) + Math.pow(NA2, 2) );
		var ME1 = MM1 * cuC;
		var ME2 = MM2 * cuD;
		var ME3 = MM1 * coC;
		var ME4 = MM2 * coD;
		var fin1 = ME1 + ME2;
		var fin2 = ME3 + ME4;
	}else if( $('#metodo').val()=='D' ){
		var MM1 = RAIZ( Math.pow(IR1, 2) + Math.pow(RES2, 2) + Math.pow(NA1, 2) + Math.pow(RES3, 2) );
		var MM2 = RAIZ( Math.pow(IR2, 2) + Math.pow(RES2, 2) + Math.pow(NA2, 2) + Math.pow(RES3, 2) );
		var ME1 = MM1 * cuE;
		var ME2 = MM2 * cuF;
		var ME3 = MM1 * coE;
		var ME4 = MM2 * coF;
		var fin1 = ME1 + ME2;
		var fin2 = ME3 + ME4;
	}
	
	document.getElementById('finB').innerHTML = _RED2(fin1, 2);
	document.getElementById('finD').innerHTML = _RED2(fin2, 2);
	return;
}

function Redondear(txt){
	_RED(txt, _decimales);
	__calcula();
}

function __limpia(){
	document.getElementById('cuA').innerHTML = '';
	document.getElementById('cuB').innerHTML = '';
	document.getElementById('cuC').innerHTML = '';
	document.getElementById('cuD').innerHTML = '';
	document.getElementById('cuE').innerHTML = '';
	document.getElementById('cuF').innerHTML = '';
	document.getElementById('coA').innerHTML = '';
	document.getElementById('coB').innerHTML = '';
	document.getElementById('coC').innerHTML = '';
	document.getElementById('coD').innerHTML = '';
	document.getElementById('coE').innerHTML = '';
	document.getElementById('coF').innerHTML = '';
}

function RAIZ(_num){
	return Math.sqrt(_num);
}