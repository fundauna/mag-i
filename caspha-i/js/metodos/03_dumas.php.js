////ARREGLAR FACTORES
var _IE = _IC = _MM1 = _MM2 = _MV1 = _MV2 = 0;

function __lolo() {
    document.getElementById('nitro1').value = '14.90';
    document.getElementById('nitro2').value = '14.80';
    document.getElementById('masa1').value = '51.1';
    document.getElementById('masa2').value = '50.2';
    document.getElementById('delta1').value = '1.0997';
    document.getElementById('delta2').value = '0.00015';
    document.getElementById('delta2').value = '0.00015';
    document.getElementById('repetibilidad1').value = '0.6';
    document.getElementById('resolucion1').value = '0.01';
    document.getElementById('curva').value = '0.56';
    document.getElementById('repetibilidad2').value = '0.000125';
    document.getElementById('resolucion2').value = '0.0001';
    document.getElementById('inc_cer').value = '0.00017';
    document.getElementById('emp').value = '0.000093';
    __calcula();
}

function datos() {
    if ($('#fechaA').val() == '') {
        OMEGA('Debe indicar la fecha del an�lisis');
        return;
    }

    if ($('#rango').val() == '') {
        OMEGA('Debe indicar la concentraci�n declarada');
        return;
    }

    if ($('#unidad').val() == '') {
        OMEGA('Debe seleccionar la unidad');
        return;
    }

    if (!confirm('Ingresar ensayo?')) return;

    if (document.getElementById('unidad').value == '0') {
        var lbl = 'm/m';
        var CD = document.getElementById('prom1').innerHTML;
        var IC = document.getElementById('prom2').innerHTML;
    } else {
        var lbl = 'm/v';
        var CD = document.getElementById('prom4').innerHTML;
        var IC = document.getElementById('prom5').innerHTML;
    }
//agregado con los campos de las incertidumbres
    var ic_balanza = document.getElementById('inc_balanza').innerHTML;
    var ic_combinada = document.getElementById('res_incCertificado').innerHTML;

    var parametros = {
        '_AJAX': 1,
        'xanalizar': $('#xanalizar').val(),
        'tipo': $('#tipo').val(),
        'fechaA': $('#fechaA').val(),
        'rango': $('#rango').val(),
        'unidad': $('#unidad').val(),
        'nitro1': $('#nitro1').val(),
        'nitro2': $('#nitro2').val(),
        'nitro3': $('#nitro3').val(),
        'masa1': $('#masa1').val(),
        'masa2': $('#masa2').val(),
        'masa3': $('#masa3').val(),
        'delta1': $('#delta1').val(),
      //  'delta2': $('#delta2').val(),
        'repetibilidad1': $('#repetibilidad1').val(),
        'resolucion1': $('#resolucion1').val(),
        'curva': $('#curva').val(),
        'repetibilidad2': $('#repetibilidad2').val(),
        'resolucion2': $('#resolucion2').val(),
        'inc_cer': $('#inc_cer').val(),
        'emp': $('#emp').val(),
        'CD': CD,
        'IC': IC,
        'obs': $('#obs').val(),
        'ic_balanza': ic_balanza,
        'ic_combinada': ic_combinada,
        'incCertificado': $('#incCertificado ').val(),
        'incResolucion': $('#incResolucion').val()
    }

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    break;
                case '1':
                    opener.location.reload();
                    alert("Transaccion finalizada");
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function Redondear(txt) {
    _RED(txt, 6);
    __calcula();
}

function __calcula() {
    if (document.getElementById('unidad').value == '') return;

    __equipos();
    __balanza();
    _Densidad();

    var mas1 = document.getElementById('nitro1').value;
    var mas2 = document.getElementById('nitro2').value;
    var mas3 = document.getElementById('nitro3').value;
    var mas4 = document.getElementById('masa1').value;
    var mas5 = document.getElementById('masa2').value;
    var mas6 = document.getElementById('masa3').value;


    _MM1 = document.getElementById('nitro1').value / document.getElementById('masa1').value * 100;
    _MM2 = document.getElementById('nitro2').value / document.getElementById('masa2').value * 100;
    _MM3 = document.getElementById('nitro3').value / document.getElementById('masa3').value * 100;
    _MV1 = _MM1 * document.getElementById('delta1').value;
    _MV2 = _MM2 * document.getElementById('delta1').value;
    _MV3 = _MM3 * document.getElementById('delta1').value;
    del = document.getElementById('delta1').value;

    //incertidumbre de calculo promedio mm * sqrt 2 (division de icequipo/ mm �2) + (division de icmasa/mm�2)
    var inCalcu1 = _MM1 * Math.sqrt((Math.pow(_IE / mas1, 2)) + Math.pow(_IC / mas4, 2), 2);
    var inCalcu2 = _MM2 * Math.sqrt((Math.pow(_IE / mas2, 2)) + Math.pow(_IC / mas5, 2), 2);
    var inCalcu3 = _MM3 * Math.sqrt((Math.pow(_IE / mas3, 2)) + Math.pow(_IC / mas6, 2), 2);
    document.getElementById('incCal1').innerHTML = _RED2(inCalcu1, 3);
    document.getElementById('incCal2').innerHTML = _RED2(inCalcu2, 3);
    document.getElementById('incCal3').innerHTML = _RED2(inCalcu3, 3);

    var inCalcu4 = _MV1 * Math.sqrt((Math.pow(_IE / mas1, 2)) + Math.pow(_IC / mas4, 2) + Math.pow(_ID / del,2 ),2);
    var inCalcu5 = _MV2 * Math.sqrt((Math.pow(_IE / mas2, 2)) + Math.pow(_IC / mas5, 2) + Math.pow(_ID / del,2 ),2);
    var inCalcu6 = _MV3 * Math.sqrt((Math.pow(_IE / mas3, 2)) + Math.pow(_IC / mas6, 2) + Math.pow(_ID / del,2 ), 2);
    document.getElementById('incCal4').innerHTML = _RED2(inCalcu4, 3);
    document.getElementById('incCal5').innerHTML = _RED2(inCalcu5, 3);
    document.getElementById('incCal6').innerHTML = _RED2(inCalcu6, 3);



    var mayor = Math.max(inCalcu1, inCalcu2, inCalcu3);
    var mayor2 = Math.max(inCalcu4, inCalcu5, inCalcu6);

    //DESV. ESTANDAR
    var fVarianceA = 0;
    var promedio1 = (_MM1 + _MM2 + _MM3) / 3;
    fVarianceA += parseFloat(Math.pow(_MM1 - promedio1, 2));
    fVarianceA += parseFloat(Math.pow(_MM2 - promedio1, 2));
    fVarianceA += parseFloat(Math.pow(_MM3 - promedio1, 2));

    var desv3 = Math.sqrt(fVarianceA) / Math.sqrt(2);//faltan decimales
    var desv4 = desv3 / Math.sqrt(3);

    fVarianceA = 0;
    var promedio2 = (_MV1 + _MV2 + _MV3) / 3;
    fVarianceA += parseFloat(Math.pow(_MV1 - promedio2, 2));
    fVarianceA += parseFloat(Math.pow(_MV2 - promedio2, 2));
    fVarianceA += parseFloat(Math.pow(_MV3 - promedio2, 2));

    var desv5 = Math.sqrt(fVarianceA) / Math.sqrt(2);
    var desv6 = desv5 / Math.sqrt(3);
    //
   // var delta3 = document.getElementById('delta2').value / 2;
  // var delta3 = document.getElementById('delta2').value;
    var promN = (parseFloat(document.getElementById('nitro1').value) + parseFloat(document.getElementById('nitro2').value) + parseFloat(document.getElementById('nitro3').value)) / 3;
    var promM = (parseFloat(document.getElementById('masa1').value) + parseFloat(document.getElementById('masa2').value) + parseFloat(document.getElementById('masa3').value)) / 3;
    var prom2 = Math.sqrt(Math.pow(promedio1 * Math.sqrt(Math.pow(parseFloat(document.getElementById('desv1').innerHTML) / promN, 2) + Math.pow(parseFloat(document.getElementById('desv2').innerHTML) / promM, 2)) / Math.sqrt(2), 2) + Math.pow(desv4, 2));
    var l1 = parseFloat(document.getElementById('desv1').innerHTML);
    var l2 = parseFloat(document.getElementById('desv2').innerHTML);
    var l3 = promedio1 * document.getElementById('delta1').value;
    //var prom3 = Math.sqrt(Math.pow(_IE/promedio1, 2)+Math.pow(_IC/promedio2, 2)+Math.pow(delta3/document.getElementById('delta1').value, 2)+Math.pow(desv6, 2));
    //var prom3 = Math.sqrt(Math.pow(l3*Math.sqrt(Math.pow(l1/promedio1, 2)+Math.pow(l2/promedio2, 2)+Math.pow(delta3/document.getElementById('delta1').value, 2)),2)+Math.pow(desv6, 2));
    var v = (parseFloat(document.getElementById('nitro1').value) + parseFloat(document.getElementById('nitro2').value)) / 2;
    var b = (parseFloat(document.getElementById('masa1').value) + parseFloat(document.getElementById('masa2').value)) / 2;
   // var n = delta3 / document.getElementById('delta1').value;
    var mix = document.getElementById('mm1').innerHTML / document.getElementById('delta1').value;
    var prom3 = Math.sqrt(Math.pow((promedio1 * document.getElementById('delta1').value) * Math.sqrt((Math.pow(parseFloat(document.getElementById('desv1').innerHTML) / promN, 2) + Math.pow(parseFloat(document.getElementById('desv2').innerHTML) / promM, 2)) + Math.pow(mix, 2)) / Math.sqrt(2), 2) + Math.pow(desv6, 2));
    //
    var pp = Math.sqrt(Math.pow(mayor / Math.sqrt(3), 2) + (Math.pow(desv4, 2)));

    var qq = Math.sqrt(Math.pow(mayor2 / Math.sqrt(3),2) + (Math.pow(desv6, 2)));

    document.getElementById('mm1').innerHTML = _RED2(_MM1, 3);
    document.getElementById('mm2').innerHTML = _RED2(_MM2, 3);
    document.getElementById('mm3').innerHTML = _RED2(_MM3, 3);
    document.getElementById('desv3').innerHTML = _RED2(desv3, 2);
    document.getElementById('desv4').innerHTML = _RED2(desv4, 5);
   // document.getElementById('delta3').innerHTML = delta3;


    document.getElementById('mv1').innerHTML = _RED2(_MV1, 3);
    document.getElementById('mv2').innerHTML = _RED2(_MV2, 3);
    document.getElementById('mv3').innerHTML = _RED2(_MV3, 3);
    document.getElementById('desv5').innerHTML = _RED2(desv5, 3);
    document.getElementById('desv6').innerHTML = _RED2(desv6, 3);
    document.getElementById('prom1').innerHTML = _RED2(promedio1, 3);
    document.getElementById('prom2').innerHTML = _RED2(desv4, 3);
    document.getElementById('prom3').innerHTML = _RED2(pp, 3);
    document.getElementById('prom4').innerHTML = _RED2(promedio1 * document.getElementById('delta1').value, 3);
    document.getElementById('prom5').innerHTML = _RED2(desv6, 3);
    document.getElementById('prom6').innerHTML = _RED2(qq, 3);

    /*joker = '';
    if(prom2 >= 1 && prom2 < 10){
         pro1 = _RED2(promedio1, 1);
         pro2 = _RED2(prom2, 1);
         pro3 = _RED2(prom2*2, 1);
         pro4 = _RED2(promedio1*document.getElementById('delta1').value, 1);
         pro5 = _RED2(prom3, 1);
         pro6 = _RED2(prom3*2, 1);
    }else{
        if(prom2 > 10){
            pro1 = Math.round(promedio1);
            pro2 = Math.round(prom2);
            pro3 = Math.round(prom2*2);
            pro4 = Math.round(promedio1*document.getElementById('delta1').value);
            pro5 = Math.round(prom3);
            pro6 = Math.round(prom3*2);
        }else{
            jk = ''+prom2;
            for(z=0;z<jk.length;z++){
                if(jk[z] != '0' && jk[z] != '.'){
                    jk = z;
                    break;
                }
            }
            pro1 = promedio1.toFixed(jk);
            pro2 = _RED2(prom2, jk);
            pro3 = _RED2(prom2*2, jk);
            pro4 = (promedio1*document.getElementById('delta1').value).toFixed(jk);
            pro5 = _RED2(prom3, jk);
            pro6 = _RED2(prom3*2, jk);
            decimals = Math.pow(10,2);
              var intPart = Math.floor(prom2);
            var fracPart = (prom2 % 1)*decimals;
            if(fracPart == '10' || fracPart == '20' || fracPart == '30' || fracPart == '40' || fracPart == '50' || fracPart == '60' || fracPart == '70' || fracPart == '80' || fracPart == '90')
                joker = '1';
        }
    }
    if(joker == '1'){
        document.getElementById('prom1').innerHTML = ''+pro1+'0';
        document.getElementById('prom2').innerHTML = ''+pro2+'0';
        document.getElementById('prom3').innerHTML = ''+pro3+'0';
        document.getElementById('prom4').innerHTML = ''+pro4+'0';
        document.getElementById('prom5').innerHTML = ''+pro5+'0';
        document.getElementById('prom6').innerHTML = ''+pro6+'0';
    }else{
        document.getElementById('prom1').innerHTML = pro1;
        document.getElementById('prom2').innerHTML = pro2;
        document.getElementById('prom3').innerHTML = pro3;
        document.getElementById('prom4').innerHTML = pro4;
        document.getElementById('prom5').innerHTML = pro5;
        document.getElementById('prom6').innerHTML = pro6;
    }*/
}

function __equipos() {
    var repetibilidad = (document.getElementById('repetibilidad1').value / Math.sqrt(10)).toFixed(5);
    var resolucion = (document.getElementById('resolucion1').value / Math.sqrt(12)).toFixed(6);
    var _promedio = (parseFloat(document.getElementById('nitro1').value) + parseFloat(document.getElementById('nitro2').value) + parseFloat(document.getElementById('nitro3').value)) / 3;
    var _temp = 0.5 * _promedio / 100
    var precision = _temp / Math.sqrt(3);

    //_IE = Math.sqrt(parseFloat(Math.pow(repetibilidad,2)) + parseFloat(Math.pow(resolucion,2)) + parseFloat(Math.pow(precision,2)) + parseFloat(Math.pow(document.getElementById('curva').value,2)) );
    var potrepetibilidad = Math.pow(repetibilidad, 2);
    var potresolucion = Math.pow(resolucion, 2);
    var potcurva = Math.pow(document.getElementById('curva').value, 2);

    _IE = Math.sqrt(parseFloat(potrepetibilidad) + parseFloat(potresolucion) + parseFloat(potcurva));

    document.getElementById('rep_comp1').innerHTML = _RED2(repetibilidad, 5);
    document.getElementById('res_comp1').innerHTML = _RED2(resolucion, 6);
    document.getElementById('precision').innerHTML = _RED2(_temp, 6);
    document.getElementById('pre_comp').innerHTML = _RED2(precision, 6);
    document.getElementById('cur_comp').innerHTML = _RED2(document.getElementById('curva').value, 5);
    document.getElementById('inc_equipo').innerHTML = _RED2(_IE, 5);
    document.getElementById('desv1').innerHTML = _RED2(_IE, 5);
}

function __balanza() {
    N4 = document.getElementById('repetibilidad2').value / Math.sqrt(10);

    document.getElementById('rep_comp2').innerHTML = _RED2(N4, 7);
    //
    N5 = document.getElementById('resolucion2').value / Math.sqrt(12);
    document.getElementById('res_comp2').innerHTML = _RED2(N5, 6);
    //
    N6 = document.getElementById('inc_cer').value / 2;
    document.getElementById('cer_comp').innerHTML = _RED2(N6, 6);

    N7 = document.getElementById('emp').value / 2;
    document.getElementById('emp_comp').innerHTML = _RED2(N7, 6);

    _IC = Math.sqrt(parseFloat(Math.pow(N4, 2)) + parseFloat(Math.pow(N5, 2)) + parseFloat(Math.pow(N6, 2)) + parseFloat(Math.pow(N7, 2)), 2);
    document.getElementById('inc_balanza').innerHTML = _RED2(_IC, 6);
    /*masas = (parseFloat(document.getElementById('masa1').value) + parseFloat(document.getElementById('masa2').value)) / 2
    masas = masas * document.getElementById('inc_balanza').innerHTML;
    document.getElementById('desv2').innerHTML = _RED2(masas,5);*/
    document.getElementById('desv2').innerHTML = _RED2(_IC * 1000, 6);
}

function _Densidad() {
    D1 = document.getElementById('incCertificado').value / 2;
    document.getElementById('res_incCertificado').innerHTML = _RED2(D1, 4);

    D2 = document.getElementById('incResolucion').value / Math.sqrt(12);
    document.getElementById('res_incResolucion').innerHTML = _RED2(D2, 7);

    _ID = Math.sqrt(Math.pow(D1,2)+ Math.pow(D2,2));
    document.getElementById('res_incCombinada').innerHTML = _RED2(_ID, 5);

    document.getElementById('d_inc').innerHTML = _RED2(_ID, 5);
}