var _decimales = 4;
var _promedio = 0;
var contenido1 = contenido1 = 0;

function __lolo(){
	document.getElementById('unidad').selectedIndex=2;
	document.getElementById('yodo').value = '0.1089';
	document.getElementById('yodo2').value = '0.0001';
	document.getElementById('densidad').value = '1.2188';
	document.getElementById('muestra1').value = '0.6615';
	document.getElementById('consumido1').value = '16.80';
	document.getElementById('muestra2').value = '0.6640';
	document.getElementById('consumido2').value = '17.00';
	__calcula();
}

function datos(){
	if($('#ingrediente').val()==''){
		OMEGA('Debe indicar el ingrediente activo');
		return;
	}
	
	if($('#fechaA').val()==''){
		OMEGA('Debe indicar la fecha del an�lisis');
		return;
	}
	
	if($('#rango').val()==''){
		OMEGA('Debe indicar la concentraci�n declarada');
		return;
	}
	
	if($('#unidad').val()==''){
		OMEGA('Debe seleccionar la unidad');
		return;
	}
	
	if($('#tipo_form').val()==''){
		OMEGA('Debe seleccionar el tipo de formulaci�n');
		return;
	}
	
	if($('#fechaP').val()==''){
		OMEGA('Debe indicar la fecha de preparaci�n');
		return;
	}
	
	if(!confirm('Ingresar ensayo?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'xanalizar' : $('#xanalizar').val(),
		'tipo' : $('#tipo').val(),
		'accion' : $('#accion').val(),
		'ingrediente' : $('#ingrediente').val(),
		'fechaA' : $('#fechaA').val(),
		'rango' : $('#rango').val(),
		'unidad' : $('#unidad').val(),
		'tipo_form' : $('#tipo_form').val(),
		'masa' : $('#masa').val(),
		'muestra1' : $('#muestra1').val(),
		'muestra2' : $('#muestra2').val(),
		'numreactivo' : $('#numreactivo').val(),
		'consumido1' : $('#consumido1').val(),
		'consumido2' : $('#consumido2').val(),
		'fechaP' : $('#fechaP').val(),
		'IECB' : $('#IECB').val(),
		'linealidad1' : $('#linealidad1').val(),
		'linealidad2' : $('#linealidad2').val(),
		'repeti1' : $('#repeti1').val(),
		'repeti2' : $('#repeti2').val(),
		'yodo' : $('#yodo').val(),
		'yodo2' : $('#yodo2').val(),
		'densidad' : $('#densidad').val(),
		'promedio' : $('#promedio').val(),
		'incertidumbre' : $('#incertidumbre').val(),
		'contenido1' : $('#contenido1').val(),
		'contenido2' : $('#contenido2').val(),
		'obs' : $('#obs').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					opener.location.reload();
					alert("Transaccion finalizada");
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Redondear(txt){
	_RED(txt, _decimales);
	__calcula();
}

function __calcula(){
	if(document.getElementById('unidad').value == '') return;
	
	if(document.getElementById('unidad').value == '0'){ //m/m
		document.getElementById('lbl1').innerHTML = '<strong>Contenido de '+document.getElementById('ingrediente').value+' (% m/m)</strong>';
		document.getElementById('lbl2').innerHTML = '<strong>(% m/m)</strong>';
		contenido1 = document.getElementById('consumido1').value/1000*document.getElementById('yodo').value*document.getElementById('masa').value*100/document.getElementById('muestra1').value;
		contenido2 = document.getElementById('consumido2').value/1000*document.getElementById('yodo').value*document.getElementById('masa').value*100/document.getElementById('muestra2').value;
	}else{
		document.getElementById('lbl1').innerHTML = '<strong>Contenido de '+document.getElementById('ingrediente').value+' (% m/v)</strong>';
		document.getElementById('lbl2').innerHTML = '<strong>(% m/v)</strong>';
		contenido1 = document.getElementById('consumido1').value/1000*document.getElementById('yodo').value*document.getElementById('masa').value*document.getElementById('densidad').value*100/document.getElementById('muestra1').value;
		contenido2 = document.getElementById('consumido2').value/1000*document.getElementById('yodo').value*document.getElementById('masa').value*document.getElementById('densidad').value*100/document.getElementById('muestra2').value;
	}
	//
	_promedio = (contenido1 + contenido2) / 2;
	document.getElementById('contenido1').value = _RED2(contenido1, 2);
	document.getElementById('contenido2').value = _RED2(contenido2, 2);
	document.getElementById('promedio').value = _RED2(_promedio, 2);
	
	__calcula2();
}

function __calcula2(){
	var tmp1 = $('#linealidad1').val()/Math.sqrt(3);
	var tmp2 = $('#linealidad2').val()/Math.sqrt(3);
	var IEC1 = Math.sqrt(Math.pow(2*$('#repeti1').val(), 2) + Math.pow(2*tmp1, 2));
	var IEC2 = Math.sqrt(Math.pow(2*$('#repeti2').val(), 2) + Math.pow(2*tmp2, 2));
	var IR1 = IEC1/$('#muestra1').val();
	var IR2 = IEC2/$('#muestra2').val();
	//
	var CNAO = $('#yodo2').val()/$('#yodo').val();
	//
	var VNAO1 = $('#IECB').val()/$('#consumido1').val();
	var VNAO2 = $('#IECB').val()/$('#consumido2').val();
	//
	if(document.getElementById('unidad').value == '0'){ //m/m
		var IRDEN = 0;
	}else{
		var IRDEN = Math.pow(0.000058/$('#densidad').val(), 2);
	}
	var PRE1 = RAIZ(Math.pow(IR1, 2) + Math.pow(CNAO, 2) + Math.pow(VNAO1, 2) + IRDEN);
	var PRE2 = RAIZ(Math.pow(IR2, 2) + Math.pow(CNAO, 2) + Math.pow(VNAO2, 2) + IRDEN);
	var MED1 = PRE1 * contenido1;
	var MED2 = PRE2 * contenido2;
	var FIN = (MED1+MED2)/2;
	
	document.getElementById('incertidumbre').value = _RED2(FIN*2, 2);
}

function RAIZ(_num){
	return Math.sqrt(_num);
}

function PipetasIEC(_tam){
	if(_tam==0.5) return 0.002;
	else if(_tam==1) return 0.006;
	else if(_tam==2) return 0.004;
	else if(_tam==3) return 0.004;
	else if(_tam==4) return 0.004;
	else if(_tam==5) return 0.006;
	else if(_tam==6) return 0.005;
	else if(_tam==7) return 0.005;
	else if(_tam==10) return 0.010;
	else if(_tam==15) return 0.013;
	else if(_tam==20) return 0.014;
	else if(_tam==25) return 0.015;
	else if(_tam==50) return 0.026;
	else if(_tam==100) return 0.046;
}