var _decimales = 4;
var promA = promB = res1 = res2 = res3 = res4 = res5 = res6 = 0;

function __lolo(){
	document.getElementById('masa').value = '50.80';
	document.getElementById('pureza').value = '99.50';
	document.getElementById('muestra').value = '5.0669';
	document.getElementById('encontrado').value = '78.48';
	document.getElementById('1A').value = '4760.88';
	document.getElementById('1B').value = '5277.75';
	document.getElementById('2A').value = '4759.14';
	document.getElementById('2B').value = '5279.53';
	document.getElementById('valA1').value = '50.00';
	document.getElementById('valA2').value = '5.00';
	document.getElementById('valA3').value = '50.00';
	document.getElementById('valA4').value = '1.00';
	document.getElementById('valA5').value = '1.00';
	document.getElementById('valB1').value = '100.00';
	document.getElementById('valB2').value = '5.00';
	document.getElementById('valB3').value = '100.00';
	document.getElementById('valB4').value = '7.00';
	document.getElementById('valB5').value = '50.00';
	__calcula();
}

function __calcula(){
	promA = ( parseFloat(document.getElementById('1A').value) + parseFloat(document.getElementById('2A').value) )/2;
	promB = ( parseFloat(document.getElementById('1B').value) + parseFloat(document.getElementById('2B').value) )/2;
	res1 = (document.getElementById('muestra').value*document.getElementById('encontrado').value)/100;
	res3 = promA/promB;
	res4 = document.getElementById('masa').value*document.getElementById('pureza').value/100/document.getElementById('valA1').value;
	a = document.getElementById('valA2').value / document.getElementById('valA3').value;
	b = document.getElementById('valA4').value / document.getElementById('valA5').value;
	c = document.getElementById('valB1').value;
	d = document.getElementById('valB3').value / document.getElementById('valB2').value;
	e = document.getElementById('valB5').value / document.getElementById('valB4').value;
	res5 = (a * b) * c * ( d * e);
	res2 = (res3*(res4/1000))*res5;
	res6 = 111*(res1-res2)/res1;
	
	document.getElementById('promA').innerHTML = _RED2(promA, 2);
	document.getElementById('promB').innerHTML = _RED2(promB, 2);
	document.getElementById('res1').innerHTML = _RED2(res1, 4);
	document.getElementById('res2').innerHTML = _RED2(res2, 4);
	document.getElementById('res3').innerHTML = _RED2(res3, 3);
	document.getElementById('res4').innerHTML = _RED2(res4, 3);
	document.getElementById('res5').innerHTML = _RED2(res5, 2);
	document.getElementById('res6').innerHTML = _RED2(res6, 2);
}

function datos(){	
	if($('#fechaA').val()==''){
		OMEGA('Debe indicar la fecha del an�lisis');
		return;
	}
	
	if($('#tipo_form').val()==''){
		OMEGA('Debe seleccionar el tipo de formulaci�n');
		return;
	}
	
	if($('#ingrediente').val()==''){
		OMEGA('Debe indicar el ingrediente activo');
		return;
	}
	
	if($('#fechaP').val()==''){
		OMEGA('Debe indicar la fecha de preparaci�n');
		return;
	}
	
	if($('#fechaD').val()==''){
		OMEGA('Debe indicar la fecha de comprobaci�n');
		return;
	}
	
	if($('#numreactivo').val()==''){
		OMEGA('Debe indicar el n�mero de reactivo');
		return;
	}
	
	if($('#origen').val()==''){
		OMEGA('Debe indicar el origen del estandar');
		return;
	}
	
	if($('#existe').val()==''){
		OMEGA('Debe seleccionar si existe certificado');
		return;
	}
	
	if($('#disol').val()==''){
		OMEGA('Debe indicar el disolvente utilizado');
		return;
	}
	
	if(!confirm('Ingresar ensayo?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'xanalizar' : $('#xanalizar').val(),
		'tipo' : $('#tipo').val(),
		'ingrediente' : $('#ingrediente').val(),
		'fechaA' : $('#fechaA').val(),
		'tipo_form' : $('#tipo_form').val(),
		'dosis' : $('#dosis').val(),
		'fechaP' : $('#fechaP').val(),
		'maria' : $('#maria').val(),
		'numreactivo' : $('#numreactivo').val(),
		'fechaD' : $('#fechaD').val(),
		'resultado' : $('#resultado').val(),
		'origen' : $('#origen').val(),
		'1A' : $('#1A').val(),
		'1B' : $('#1B').val(),
		'2A' : $('#2A').val(),
		'2B' : $('#2B').val(),
		'masa' : $('#masa').val(),
		'pureza' : $('#pureza').val(),
		'existe' : $('#existe').val(),
		'disol' : $('#disol').val(),
		'muestra' : $('#muestra').val(),
		'encontrado' : $('#encontrado').val(),
		'valA1' : $('#valA1').val(),
		'valA2' : $('#valA2').val(),
		'valA3' : $('#valA3').val(),
		'valA4' : $('#valA4').val(),
		'valA5' : $('#valA5').val(),
		'valB1' : $('#valB1').val(),
		'valB2' : $('#valB2').val(),
		'valB3' : $('#valB3').val(),
		'valB4' : $('#valB4').val(),
		'valB5' : $('#valB5').val(),
		'IC' : document.getElementById('res6').innerHTML,
		'obs' : $('#obs').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					opener.location.reload();
					alert("Transaccion finalizada");
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Redondear(txt){
	_RED(txt, _decimales);
	__calcula();
}