function datos(tipo) {
    if (Mal(1, $('#descripcion').val())) {
        OMEGA('Debe indicar la descripci�n');
        return;
    }

    descripcion_v = $('.descripcion_v');
    descripcion_va = new Array();
    for (i = 0; i < descripcion_v.length; i++) {
        if (Mal(1, descripcion_v[i].value)) {
            OMEGA('Debe indicar la descripci�n de bandeja #' + (i + 1));
            return;
        }
        descripcion_va.push(descripcion_v[i].value);
    }

    dimensionx = $('.dimensionx');
    dimensionxa = new Array();
    for (i = 0; i < dimensionx.length; i++) {
        if (Mal(1, dimensionx[i].value)) {
            OMEGA('Debe indicar la dimensi�n X de bandeja #' + (i + 1));
            return;
        } else if (isNaN(dimensionx[i].value)) {
            OMEGA('Debe indicar un n�mero en dimensi�n X de bandeja #' + (i + 1));
            return;
        }
        dimensionxa.push(dimensionx[i].value);
    }

    dimensiony = $('.dimensiony');
    dimensionya = new Array();
    for (i = 0; i < dimensiony.length; i++) {
        if (Mal(1, dimensiony[i].value)) {
            OMEGA('Debe indicar la dimensi�n Y de bandeja #' + (i + 1));
            return;
        } else if (isNaN(dimensiony[i].value)) {
            OMEGA('Debe indicar un n�mero en dimensi�n Y de bandeja #' + (i + 1));
            return;
        }
        dimensionya.push(dimensiony[i].value);
    }

    rotulox = $('.rotulox');
    rotuloxa = new Array();
    for (i = 0; i < rotulox.length; i++) {
        rotuloxa.push(rotulox[i].value);
    }

    rotuloy = $('.rotuloy');
    rotuloya = new Array();
    for (i = 0; i < rotuloy.length; i++) {
        rotuloya.push(rotuloy[i].value);
    }

    if (!confirm('Modificar datos?'))
        return;
    
    bandejalinea=$('.bandejalinea');
    bandejalineav = new Array();
    for(i=0;i<bandejalinea.length;i++){
        bandejalineav.push(bandejalinea[i].value);
    }
    
    var parametros = {
        '_AJAX': 1,
        'acc': $('#accion').val(),
        'id': $('#accion').val() == 'I' ? '0' : $('#id').val(),
        'descripcion': $('#descripcion').val(),
        'estado': $('#estado').val(),
        'descripcion_v': descripcion_va,
        'dimensionx': dimensionxa,
        'dimensiony': dimensionya,
        'rotulox': rotuloxa,
        'rotuloy': rotuloya,
        'bandejalinea':bandejalineav
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    alert("Transaccion finalizada");
                    if (tipo == 'e' ||$('#accion').val() == 'M') {
                        location.reload();
                    } else {
                        opener.location.reload();
                        window.close();
                    }
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function agregarBandeja() {
    str = '<tr> <td><input class="descripcion_v" value="" type="text" /></td> <td><input class="dimensionx" value="" type="number" step="0.01" /></td> <td><input class="dimensiony" value="" type="number" step="0.01" /></td> <td> <select class="rotulox" > <option value="l" >Letras</option> <option value="n" >N&uacute;meros</option> </select> </td> <td> <select class="rotuloy" > <option value="l" >Letras</option> <option value="n" >N&uacute;meros</option> </select> </td> <td><img onclick="eliminaBandejaNueva($(this))" src="../../caspha-i/imagenes/del.png" title="Eliminar" class="tab2"></td> <input type="hidden" class="bandejalinea" value="-1" /> </tr>';
    $('#lolo').append(str);
}

function eliminarBandeja(linea) {
    if (confirm("Desea eliminar esta Bandeja y sus Cajas Asociadas!")) {
        if($('#accion').val()=='M'){datosEliminaBandeja(linea);}
    }
}

function eliminaBandejaNueva(elemento){
    elemento.parent('td').parent('tr').remove();
}

function datosEliminaBandeja(linea){
     var parametros = {
        '_AJAX': 2,
        'acc': $('#accion').val(),
        'id': $('#accion').val() == 'I' ? '0' : $('#id').val(),
        'linea':linea
    };
     $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    alert("Transaccion finalizada");
                        location.reload();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function cajas(id,linea){
       window.open("caja_detalle.php?ID="+id+"&L="+linea,"","width=350,height=500,scrollbars=yes,status=no");
}