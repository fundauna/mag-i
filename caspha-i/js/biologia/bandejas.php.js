function desmarca(){
	document.getElementById('add').disabled = false;
	document.getElementById('mod').disabled = true;
	document.getElementById('del').disabled = true;
	document.getElementById('new').disabled = true;
	document.getElementById('id').value = '';
	document.getElementById('dimx').value = '';
	document.getElementById('dimy').value = '';
}

function marca(control){
	if(control.selectedIndex!= -1){
		
		document.getElementById('id').value = control.options[control.selectedIndex].getAttribute('id');
		document.getElementById('dimx').value = control.options[control.selectedIndex].getAttribute('dimx');
		document.getElementById('dimy').value = control.options[control.selectedIndex].getAttribute('dimy');
		//
		var tmp = control.options[control.selectedIndex].getAttribute('labelx');
		if(tmp == 'L') document.getElementById('labelx').selectedIndex = 0;
		else document.getElementById('labelx').selectedIndex = 1;
		//
		var tmp = control.options[control.selectedIndex].getAttribute('labely');
		if(tmp == 'L') document.getElementById('labely').selectedIndex = 0;
		else document.getElementById('labely').selectedIndex = 1;
		
		document.getElementById('mod').disabled = false;
		document.getElementById('del').disabled = false;
		document.getElementById('add').disabled = true;
		document.getElementById('new').disabled = false;
	}
}

function modificar(accion){
	if( Mal(1, document.getElementById('id').value) ){
		OMEGA('Debe digitar el id');
		return;
	}
	
	if( Mal(1, document.getElementById('dimx').value) || _FVAL(document.getElementById('dimx').value) < 1 ){
		OMEGA('Debe digitar la dimensi�n X');
		return;
	}
	
	if( Mal(1, document.getElementById('dimy').value) || _FVAL(document.getElementById('dimy').value) < 1 ){
		OMEGA('Debe digitar la dimensi�n Y');
		return;
	}
	
	if( _FVAL(document.getElementById('dimx').value) > 20 ){
		OMEGA('Debe dimensi�n es 20');
		return;
	}
	
	if( _FVAL(document.getElementById('dimy').value) > 20 ){
		OMEGA('Debe dimensi�n es 20');
		return;
	}
	
	if(confirm('Modificar Datos?')){
		var parametros = {
			'_AJAX' : 1,
			'id' : $('#id').val(),
			'dimx' : $('#dimx').val(),
			'dimy' : $('#dimy').val(),
			'labelx' : $('#labelx').val(),
			'labely' : $('#labely').val(),
			'accion' : accion
		};
		
		$.ajax({
			data:  parametros,
			url:   __SHELL__,
			type:  'post',
			beforeSend: function () {
				document.getElementById('add').disabled = true;
				document.getElementById('mod').disabled = true;
				document.getElementById('del').disabled = true;
				document.getElementById('new').disabled = true;	
				ALFA('Por favor espere....');
			},
			success: function (_response) {
				switch(_response){
					case '-0':alert('Sesi�n expirada [Err:0]');break;
					case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
					case '0':
						OMEGA('Error transaccional');
						document.getElementById('add').disabled = false;
						break;
					case '1':
						GAMA('Transaccion finalizada');
						document.getElementById('add').disabled = false;
						break;
					default:alert('Tiempo de espera agotado');break;
				}
			}
		});
	}
}