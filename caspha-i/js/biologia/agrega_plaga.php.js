function agregar(plaga){
    var parametros = {
        '_AJAX': 1,
        'id_caja':$('#ID').val(),
        'id_plaga':plaga
    };
    
     $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    alert("Transaccion finalizada");
                        opener.location.reload();
                        window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}