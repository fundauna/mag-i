function agrega_plaga() {
    if($('#caja').val()==''){
        alert('Seleccione un caja');
        return;
    }
    window.open("agrega_plaga.php?ID=" + $('#caja').val()+"&L=" + $('#L').val(), "", "width=250,height=500,scrollbars=yes,status=no");
}

function agregar_caja() {
    window.open("agrega_caja.php?ID=" + $('#ID').val() + "&L=" + $('#L').val(), "", "width=350,height=500,scrollbars=yes,status=no");
}

function datos() {
    var parametros = {
        '_AJAX': 1,
        'id': $('#caja').val(),
        'nombre': $('#nombre').val(),
        'ubicacion': $('#ubicacion').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    alert("Transaccion finalizada");
                    location.reload();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function detalle(id) {
    var parametros = {
        '_AJAX': 3,
        'id': id
    };
    
    $('#nombre').val('');
    $('#ubicacion').val('');
    $('#lolo').html('');
    if(id==''){
        return;
    }
    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            // ALFA('Por favor espere....');
        },
        success: function (_response) {
            datosJ = JSON.parse(_response);
            if (datosJ.length > 0) {
                $('#nombre').val(datosJ[0].nombre);
                $('#ubicacion').val(datosJ[0].ubicacion);
            }
        }
    });
    
    var parametros = {
        '_AJAX': 4,
        'id': id
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            // ALFA('Por favor espere....');
        },
        success: function (_response) {
            datosJ = JSON.parse(_response);
            if (datosJ.length > 0) {
                for (i = 0; i < datosJ.length; i++) {
                    $('#lolo').append('<tr><td>' + datosJ[i].nombre + '</td><td><img onclick="elimina_plaga(' + datosJ[i].id_plaga + ')" src="../../caspha-i/imagenes/del.png" title="Eliminar" class="tab2"/></td></tr>');
                }
            }
        }
    });
}

function elimina_plaga(id_plaga) {
    var parametros = {
        '_AJAX': 2,
        'id_caja': $('#caja').val(),
        'id_plaga': id_plaga
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    alert("Transaccion finalizada");
                    location.reload();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function agrega_caja() {
    nombre = prompt("Ingrese en nombre de la caja:");
    if (nombre == null) {
        return;
    }
    var parametros = {
        '_AJAX': 3,
        'id': $('#ID').val(),
        'nombre': nombre
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    alert("Transaccion finalizada");
                    location.reload();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}
