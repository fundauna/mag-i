function Imprimir(){
	location.href = 'trabajo_imprimir.php?ID=' + $('#cs').val();
}

function Aplica(){
	if(!confirm('Aplicar volumen deseado como\ndefault para todas las muestras?')) return;
	
	var control1 = document.getElementsByName('vol1');
	for(i=0;i<control1.length;i++){
		control1[i].value = $('#todos').val();
		_FLOAT(control1[i]);
	}
	CalculaReacciones();
}

function AdjuntaPasoD(){
	if( $('#cuantificacion').val()=='' ) txt = 'Eliminar archivo';
	else txt = 'Adjuntar archivo';
	
	if(!confirm(txt)) return;
	document.formulario.submit();
}

function DocumentosZip(_ARCHIVO, _MODULO){
	var ruta = "../seguridad/documentos.php?ARCHIVO=" + _ARCHIVO + "&MODULO=" + _MODULO + "&ACCESO=A7";
	window.open(ruta,"","width=100,height=100,scrollbars=yes,status=no");
	//window.showModalDialog(ruta, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}

function ProcsCarga(_paso){
	if( Mal(5, $('#procedimiento').val()) ){
		OMEGA('Debe seleccionar el procedimiento');
		return;
	}
	
	if(!confirm('Cargar secci�n del procedimiento: '+ $('#procedimiento').val() + '?')) return;
	
	if(_paso=='A') CargaPasoA(0);
	if(_paso=='C') CargaPasoC(0);
	if(_paso=='D') CargaPasoD(0);
	if(_paso=='E') CargaPasoE(0);
	if(_paso=='X') CargaPasoX(0);
	if(_paso=='F') CargaPasoF(0);
	if(_paso=='G') CargaPasoG(0);
}

function ProcsConsulta(){
	if( Mal(5, $('#procedimiento').val()) ){
		OMEGA('Debe seleccionar el procedimiento');
		return;
	}
	window.open("proc_detalle.php?acc=V&ID="+$('#procedimiento').val(),"","width=600,height=500,scrollbars=yes,status=no");
	//window.showModalDialog("proc_detalle.php?acc=V&ID="+$('#procedimiento').val(), this.window, "dialogWidth:600px;dialogHeight:500px;status:no;");
}

function ProcsLista(){
	window.open(__SHELL__ + "?list0=1","","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list0=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function ProcsEscoge(id){
	opener.document.getElementById('procedimiento').value = id;
	window.close();
}

function MacroAnalisisLista(_categoria, _tipo, _linea){
	$('#tipo').val(_tipo);
	window.open(__SHELL__ + "?list1="+_linea+"&tipo="+_categoria,"","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list1="+_linea+"&tipo="+_categoria, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function AlicuotasLista(_tipo, _linea){
	$('#tipo').val(_tipo);
	window.open(__SHELL__ + "?list4="+_linea+"&tipo="+_tipo,"","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list4="+_linea+"&tipo="+_tipo, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function DependeLista(_tipo, _linea){
	var categoria = $('#'+_tipo+'tipo'+_linea).val();
	if(categoria==''){
		alert('Debe indicar el tipo');
		return;
	}else if(categoria=='0'){
		InsumosLista(_tipo, _linea, document.getElementById('tipo'));
	}else if(categoria=='1'){
		AlicuotasLista(_tipo, _linea);
	}
}

function AgregarAnalisis(_categoria, _tipo){
	var linea = document.getElementsByName(_tipo+"cod_analisis").length;
	var fila = document.createElement("tr");
	var colum = new Array(2);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[0].innerHTML = (linea+1);
	colum[1].innerHTML = '<input type="text" id="'+_tipo+'analisis'+linea+'" class="lista2" readonly onclick="MacroAnalisisLista(\''+_categoria+'\', \''+_tipo+'\', '+linea+')" /><input type="hidden" id="'+_tipo+'cod_analisis'+linea+'" name="'+_tipo+'cod_analisis" />';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	document.getElementById('lolo'+_tipo).appendChild(fila);
}

function ValidaExiste(_vector, _id){
	if(_id=='') return false;
	
	var control = document.getElementsByName(_vector);
	for(i=0;i<control.length;i++){
		if(document.getElementById(_vector + i).value == _id) return true
	}
	return false;
}

function AlicuotaEscoge(id, linea){
	var tipo = opener.document.getElementById('tipo').value;
	
	if( opener.ValidaExiste(tipo+'insumos', id) ){
		alert('La opci�n seleccionada ya se encuentra en la lista');
		return;
	}
	
	opener.document.getElementById(tipo+'cod_insumos'+linea).value = id;
	opener.document.getElementById(tipo+'insumos'+linea).value = id;
	window.close();
}

function AnalisisEscoge(linea, id, nombre, costo){
	var tipo = opener.document.getElementById('tipo').value;
	
	if( opener.ValidaExiste(tipo+'cod_analisis', id) ){
		alert('La opci�n seleccionada ya se encuentra en la lista');
		return;
	}
	
	opener.document.getElementById(tipo+'cod_analisis'+linea).value = id;
	opener.document.getElementById(tipo+'analisis'+linea).value = nombre;
	window.close();
}

function AgregarMuestras(){
	var linea = document.getElementsByName("codigo").length;
	var fila = document.createElement("tr");
	var colum = new Array(2);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	
	colum[0].innerHTML = (linea+1);
	colum[1].innerHTML = '<input type="text" id="codigo'+linea+'" name="codigo" size="10" maxlength="10"/>';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	
	document.getElementById('lolo').appendChild(fila);
}

function Limpia1(_linea){
	$('#E1insumos'+_linea).val('');
	$('#E1cod_insumos'+_linea).val('');
}

function AgregarPlantilla(_tipo){
	var linea = document.getElementsByName(_tipo+"tipo").length;
	var fila = document.createElement("tr");
	
	if(_tipo=='E1'){
		var colum = new Array(8);
		
		colum[0] = document.createElement("td");
		colum[1] = document.createElement("td");
		colum[2] = document.createElement("td");
		colum[3] = document.createElement("td");
		colum[4] = document.createElement("td");
		colum[5] = document.createElement("td");
		colum[6] = document.createElement("td");
		colum[7] = document.createElement("td");
		
		colum[0].innerHTML = (linea+1);
		colum[1].innerHTML = '<select id="'+_tipo+'tipo'+linea+'" name="'+_tipo+'tipo" onchange="Limpia1('+linea+')"><option value="">...</option><option value="0">Reactivo</option><option value="1">Alicuota</option></select>';
		colum[2].innerHTML = '<input type="text" name="E1descr" size="20" maxlength="20" />';
		colum[3].innerHTML = '<input type="text" id="'+_tipo+'insumos'+linea+'" name="'+_tipo+'insumos" class="lista" readonly onclick="DependeLista(\''+_tipo+'\', '+linea+')" /><input type="hidden" id="'+_tipo+'cod_insumos'+linea+'" name="'+_tipo+'cod_insumos" />';
		colum[4].innerHTML = '<input type="text" name="E1concS" size="10" maxlength="20" />';
		colum[5].innerHTML = '<input type="text" name="E1concF" size="10" maxlength="20" />';
		colum[6].innerHTML = '<input type="text" name="'+_tipo+'volumen" class="monto2" value="0" onblur="_FLOAT(this);CalculaReacciones();"/>';
		colum[7].innerHTML = '<input type="text" name="'+_tipo+'reacciones" class="monto2" value="0" readonly/>';
	}else{
		var colum = new Array(6);
		
		colum[0] = document.createElement("td");
		colum[1] = document.createElement("td");
		colum[2] = document.createElement("td");
		colum[3] = document.createElement("td");
		colum[4] = document.createElement("td");
		colum[5] = document.createElement("td");
		
		colum[0].innerHTML = (linea+1);
		colum[1].innerHTML = '<select id="'+_tipo+'tipo'+linea+'" name="'+_tipo+'tipo" onchange="Limpia1('+linea+')"><option value="">...</option><option value="0">Reactivo</option><option value="1">Alicuota</option></select>';
		colum[2].innerHTML = '<input type="text" name="F1descr" size="10" maxlength="20" />';
		colum[3].innerHTML = '<input type="text" id="'+_tipo+'insumos'+linea+'" name="'+_tipo+'insumos" class="lista2" readonly onclick="DependeLista(\''+_tipo+'\', '+linea+')" /><input type="hidden" id="'+_tipo+'cod_insumos'+linea+'" name="'+_tipo+'cod_insumos" />';
		colum[4].innerHTML = '<input type="text" name="'+_tipo+'volumen" class="monto2" value="0" onblur="_FLOAT(this);CalculaReacciones();"/>';
		colum[5].innerHTML = '<input type="text" name="'+_tipo+'reacciones" class="monto2" value="0" readonly/>';
	}
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	document.getElementById('lolo'+_tipo).appendChild(fila);
}

function AgregarAdicionales(){
	var linea = document.getElementsByName("E0muestras").length;
	var fila = document.createElement("tr");
	var colum = new Array(2);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");	
	colum[0].innerHTML = (linea+1);
	colum[1].innerHTML = '<input type="text" name="E0muestras" size="10" maxlength="10"/>';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	
	document.getElementById('loloE0').appendChild(fila);
}

function AgregarControles(){
	var linea = document.getElementsByName("E4insumos").length;
	var fila = document.createElement("tr");
	var colum = new Array(2);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");	
	colum[0].innerHTML = (linea+1);
	colum[1].innerHTML = '<input type="text" id="E4insumos'+linea+'" name="E4insumos" class="lista" readonly onclick="AlicuotasLista(\'E4\', '+linea+')"/><input type="hidden" id="E4cod_insumos'+linea+'" name="E4cod_insumos"/>';

	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	
	document.getElementById('loloE4').appendChild(fila);
}

function AgregarControles1(){
	var linea = document.getElementsByName("C3insumos").length;
	var fila = document.createElement("tr");
	var colum = new Array(2);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");	
	colum[0].innerHTML = (linea+1);
	colum[1].innerHTML = '<input type="text" id="C3insumos'+linea+'" name="C3insumos" class="lista" readonly onclick="AlicuotasLista(\'C3\', '+linea+')"/><input type="hidden" id="C3cod_insumos'+linea+'" name="C3cod_insumos"/>';

	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	
	document.getElementById('loloC3').appendChild(fila);
}

function CambiaUnidad(){
	var _valor = $('#unidad').val();
	if(_valor=='0') document.getElementById('lb_unidad').innerHTML = ' (&mu;g/&mu;L)';
	else if(_valor=='1') document.getElementById('lb_unidad').innerHTML = ' (ng/&mu;L)';
	else document.getElementById('lb_unidad').innerHTML = '';
}

function CalculaReacciones(){
	var reac = $('#reaccionesE').val();
	var tot1 = tot2 =  tot3 = tot4 =0;
	
	var control1 = document.getElementsByName('E1volumen');
	var control2 = document.getElementsByName('E1reacciones');
	for(i=0;i<control1.length;i++){
		control2[i].value = control1[i].value * reac;
		tot1 += parseFloat(control1[i].value);
		tot2 += parseFloat(control2[i].value);
		_FLOAT(control2[i]);
	}
	//
	var reac = $('#reaccionesF').val();
	var control1 = document.getElementsByName('F1volumen');
	var control2 = document.getElementsByName('F1reacciones');
	for(i=0;i<control1.length;i++){
		control2[i].value = control1[i].value * reac;
		tot3 += parseFloat(control1[i].value);
		tot4 += parseFloat(control2[i].value);
		_FLOAT(control2[i]);
	}
	//SUMA DEL AMPLICON
	$('#Famplicon2').val( $('#Famplicon1').val() * reac );
	tot3 += parseFloat( $('#Famplicon1').val() );
	tot4 += parseFloat( $('#Famplicon2').val() );
	_FLOAT(document.getElementById('Famplicon2'));
	
	document.getElementById('tot1').innerHTML = tot1;
	document.getElementById('tot2').innerHTML = tot2;
	document.getElementById('tot3').innerHTML = tot3;
	document.getElementById('tot4').innerHTML = tot4;
	//CALCULO DE FORMULAS DILUCION
	var control1 = document.getElementsByName('CAN');
	for(i=0;i<control1.length;i++){
		if( $('#normalizada').val() < $('#CAN'+i).val() ){
			var tmp1 = $('#vol1'+i).val() * $('#normalizada').val() / $('#CAN'+i).val();
			var tmp2 = $('#vol1'+i).val() - tmp1;
			
			document.getElementById('vol2'+i).value = tmp1;
			document.getElementById('vol3'+i).value = tmp2;
			_FLOAT(document.getElementById('vol2'+i));
			_FLOAT(document.getElementById('vol3'+i));
		}else{
			document.getElementById('vol2'+i).value = 'N/A';
			document.getElementById('vol3'+i).value = 'N/A';
		}
	}
}

function Guarda1(){
	var hay=0;
	var control1 = document.getElementsByName('codigo');
	var control2 = document.getElementsByName('estado');
	for(i=0;i<control1.length;i++){
		if( !Mal(1, control1[i].value) ) hay++;
	}
	
	if(hay < 1){
		OMEGA('No ha indicado ninguna muestra');
		return;
	}
	
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 'A',
		'accion' : $('#accion').val(),
		'cs' : $('#cs').val(),
		'procedimiento' : $('#procedimiento').val(),
		'obsA' : $('#obsA').val(),
		'A1cod_analisis' : vector('A1cod_analisis'),
		'A2cod_analisis' : vector('A2cod_analisis'),
		'A3cod_analisis' : vector('A3cod_analisis'),
		'muestras' : vector('codigo')
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					
					break;
				case '1':
					alert("Transaccion finalizada");
					opener.location.reload();
					window.close();				
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Guarda2(){
	if( $('#fechaProc').val()=='' ){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 'B',
		'cs' : $('#cs').val(),
		'fechaB' : $('#fechaProc').val(),
		'tempB' : $('#tempB').val(),
		'humedadB' : $('#humedadB').val(),
		'obsB' : $('#obsB').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					OMEGA("Transaccion finalizada");
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function GuardaW(){
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 'W',
		'cs' : $('#cs').val(),
		'tiempoW' : $('#tiempoW').val(),
		'tempW' : $('#tempW').val(),
		'W2cod_equipos' : vector('W2cod_equipos'),
		'obsW' : $('#obsW').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					OMEGA("Transaccion finalizada");
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function GuardaX(){
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 'X',
		'cs' : $('#cs').val(),
		'tempX' : $('#tempX').val(),
		'humedadX' : $('#humedadX').val(),
		'tiempoX' : $('#tiempoX').val(),
		'voltajeX' : $('#voltajeX').val(),
		'X1cod_insumos' : vector('X1cod_insumos'),
		'X1total' : vector('X1total'),
		'X2cod_equipos' : vector('X2cod_equipos'),
		'obsX' : $('#obsX').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					OMEGA("Transaccion finalizada");
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function CargaPasoA(_num){
	var tabla = '';
	if(_num==0){
		tabla = 'loloA1';
		_num = 1;
	}else if(_num==1){
		BETA();
		tabla = 'loloA2';
		_num = 2;
	}else if(_num==2){
		BETA();
		tabla = 'loloA3';
		_num = 3;
	}else{
		BETA();
		return;
	}
	
	var parametros = {
		'_IMPORT' : 1,
		'paso' : _num,
		'procedimiento' : $('#procedimiento').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '': OMEGA('Error de carga'); break;
				default: 
					document.getElementById(tabla).innerHTML = _response;
					CargaPasoA(_num);
					break;
			}
		}
	});
}

function CargaPasoC(_num){
	var tabla = '';
	var letra = 'C';
	
	if(_num==0){
		tabla = 'loloC1';	
		_num = 4;
	}else if(_num==4){
		BETA();
		tabla = 'loloC2';
		_num = 5;
	}else{
		BETA();
		return;
	}

	var parametros = {
		'_IMPORT' : 1,
		'paso' : _num,
		'letra' : letra,
		'procedimiento' : $('#procedimiento').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '': OMEGA('Error de carga'); break;
				default: 
					document.getElementById(tabla).innerHTML = _response;
					CargaPasoC(_num);
					break;
			}
		}
	});
}

function CargaPasoD(_num){
	var tabla = '';
	var letra = 'D';
	
	if(_num==0){
		tabla = 'loloD1';	
		_num = 4;
	}else if(_num==4){
		BETA();
		tabla = 'loloD2';
		_num = 5;
	}else if(_num==5){
		BETA();
		tabla = 'loloDA';
		_num = 6;
	}else{
		BETA();
		return;
	}
	
	var parametros = {
		'_IMPORT' : 1,
		'paso' : _num,
		'letra' : letra,
		'procedimiento' : $('#procedimiento').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '': OMEGA('Error de carga'); break;
				default: 
					document.getElementById(tabla).innerHTML = _response;
					CargaPasoD(_num);
					break;
			}
		}
	});
}

function CargaPasoE(_num){
	var tabla = '';
	var letra = 'E';
	
	if(_num==0){
		tabla = 'loloE2';
		_num = 5;
	}else if(_num==5){
		BETA();
		tabla = 'termo';
		_num = 8;
	}else if(_num==8){
		BETA();
		tabla = 'loloE1';
		_num = 11;
	}else{
		CalculaReacciones();
		BETA();
		return;
	}
	
	var parametros = {
		'_IMPORT' : 1,
		'paso' : _num,
		'letra' : letra,
		'procedimiento' : $('#procedimiento').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '': OMEGA('Error de carga'); break;
				default: 
					document.getElementById(tabla).innerHTML = _response;
					CargaPasoE(_num);
					break;
			}
		}
	});
}

function CargaPasoF(_num){
	var tabla = '';
	var letra = 'F';
	
	if(_num==0){
		tabla = 'loloF1';	
		_num = 11;
	}else if(_num==11){
		BETA();
		tabla = 'loloF2';
		_num = 5;
	}else if(_num==5){
		BETA();
		tabla = 'loloF';
		_num = 9;
	}else if(_num==9){
		BETA();
		tabla = 'amplicon';
		_num = 13;
	}else{
		CalculaReacciones();
		BETA();
		return;
	}
	
	var parametros = {
		'_IMPORT' : 1,
		'paso' : _num,
		'letra' : letra,
		'procedimiento' : $('#procedimiento').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '': OMEGA('Error de carga'); break;
				default: 
					document.getElementById(tabla).innerHTML = _response;
					CargaPasoF(_num);
					break;
			}
		}
	});
}

function CargaPasoX(_num){
	var tabla = '';
	var letra = 'X';
	
	if(_num==0){
		tabla = 'loloX1';	
		_num = 4;
	}else if(_num==4){
		BETA();
		tabla = 'loloX2';
		_num = 5;
	}else if(_num==5){
		BETA();
		tabla = 'loloX';
		_num = 12;
	}else{
		BETA();
		return;
	}
	
	var parametros = {
		'_IMPORT' : 1,
		'paso' : _num,
		'letra' : letra,
		'procedimiento' : $('#procedimiento').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '': OMEGA('Error de carga'); break;
				default: 
					document.getElementById(tabla).innerHTML = _response;
					CargaPasoX(_num);
					break;
			}
		}
	});
}

function CargaPasoG(_num){
	var tabla = '';
	var letra = 'G';
	
	if(_num==0){
		tabla = 'loloG1';	
		_num = 4;
	}else if(_num==4){
		BETA();
		tabla = 'loloG2';
		_num = 5;
	}else if(_num==5){
		BETA();
		tabla = 'loloG';
		_num = 10;
	}else{
		BETA();
		return;
	}
	
	var parametros = {
		'_IMPORT' : 1,
		'paso' : _num,
		'letra' : letra,
		'procedimiento' : $('#procedimiento').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '': OMEGA('Error de carga'); break;
				default: 
					document.getElementById(tabla).innerHTML = _response;
					CargaPasoG(_num);
					break;
			}
		}
	});
}

function Guarda3(){
	var hay=0;
	var control1 = document.getElementsByName('C1cod_insumos');
	var control2 = document.getElementsByName('C1total');
	for(i=0;i<control1.length;i++){
		if( !Mal(1, control1[i].value) ){			
			if( control2[i].value == '0' || control2[i].value == '0.00' ){
				OMEGA('Debe indicar la cantidad de insumo a utilizar');
				return;
			}
			hay++;
		}
	}
	
	if(hay < 1){
		OMEGA('No ha indicado ning�n insumo');
		return;
	}
	
	hay=0;
	control1 = document.getElementsByName('C2cod_equipos');
	for(i=0;i<control1.length;i++){
		if( !Mal(1, control1[i].value) ){
			hay++;
			break;
		}
	}
	
	if(hay < 1){
		OMEGA('No ha indicado ning�n equipo');
		return;
	}
	
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 'C',
		'cs' : $('#cs').val(),
		'tempC' : $('#tempC').val(),
		'humedadC' : $('#humedadC').val(),
		'obsC' : $('#obsC').val(),
		'C1cod_insumos' : vector('C1cod_insumos'),
		'C1total' : vector('C1total'),
		'C2cod_equipos' : vector('C2cod_equipos'),
		'C3cod_insumos' : vector('C3cod_insumos')
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					OMEGA("Transaccion finalizada");
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Guarda4(){
	if( Mal(1, $('#analito').val()) ){
		OMEGA('Debe indicar el analito');
		return;
	}
	
	if( Mal(1, $('#unidad').val()) ){
		OMEGA('Debe indicar la unidad');
		return;
	}
	
	var hay=0;
	var control1 = document.getElementsByName('D1cod_insumos');
	var control2 = document.getElementsByName('D1total');
	for(i=0;i<control1.length;i++){
		if( !Mal(1, control1[i].value) ){			
			if( control2[i].value == '0' || control2[i].value == '0.00' ){
				OMEGA('Debe indicar la cantidad de insumo a utilizar');
				return;
			}
			hay++;
		}
	}
	
	if(hay < 1){
		OMEGA('No ha indicado ning�n insumo');
		return;
	}
	
	hay=0;
	control1 = document.getElementsByName('D2cod_equipos');
	for(i=0;i<control1.length;i++){
		if( !Mal(1, control1[i].value) ){
			hay++;
			break;
		}
	}
	
	if(hay < 1){
		OMEGA('No ha indicado ning�n equipo');
		return;
	}
	
	var control2 = document.getElementsByName('conc');

	for(i=0;i<control2.length;i++){
		if( control2[i].value == '0' || control2[i].value == '0.00' ){
			OMEGA('Debe indicar la concentraci�n');
			return;
		}
	}
	
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 'D',
		'cs' : $('#cs').val(),
		'tempD' : $('#tempD').val(),
		'humedadD' : $('#humedadD').val(),
		'normalizada' : $('#normalizada').val(),
		'analito' : $('#analito').val(),
		'unidad' : $('#unidad').val(),
		'obsD' : $('#obsD').val(),
		'Dmuestra' : vector('Dmuestra'),
		'CAN' : vector('CAN'),
		'coef1' : vector('coef1'),
		'coef2' : vector('coef2'),
		'vol1' : vector('vol1'),
		'D1cod_insumos' : vector('D1cod_insumos'),
		'D1total' : vector('D1total'),
		'D2cod_equipos' : vector('D2cod_equipos')
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					OMEGA("Transaccion finalizada");
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Guarda5(){
	if( $('#fechaPcr').val()=='' ){
		OMEGA('Debe seleccionar la fecha');
		return;
	}
	
	var hay=0;
	var control1 = document.getElementsByName('E1tipo');
	var control2 = document.getElementsByName('E1cod_insumos');
	for(i=0;i<control1.length;i++){
		if( control1[i].value != '' && Mal(1, control2[i].value) ){			
			OMEGA('Debe indicar el c�digo en la mezcla de reacci�n');
			return;
		}
	}
	
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 'E',
		'cs' : $('#cs').val(),
		'fechaE' : $('#fechaPcr').val(),
		'tempE' : $('#tempE').val(),
		'humedadE' : $('#humedadE').val(),
		'reacciones' : $('#reaccionesE').val(),
		'programaE' : $('#programaE').val(),
		'nombreE' : $('#nombreE').val(),
		'obsE' : $('#obsE').val(),
		'E1tipo' : vector('E1tipo'),
		'E1descr' : vector('E1descr'),
		'E1insumos' : vector('E1insumos'),
		'E1concS' : vector('E1concS'),
		'E1concF' : vector('E1concF'),
		'E1volumen' : vector('E1volumen'),
		'E1reacciones' : vector('E1reacciones'),
		'E2muestras' : vector('E0muestras'),
		'E2cod_equipos' : vector('E2cod_equipos'),
		'control' : vector('E4insumos')
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					OMEGA("Transaccion finalizada");
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Guarda6(){
	var hay=0;
	var control1 = document.getElementsByName('F1tipo');
	var control2 = document.getElementsByName('F1cod_insumos');
	for(i=0;i<control1.length;i++){
		if( control1[i].value != '' && Mal(1, control2[i].value) ){			
			OMEGA('Debe indicar el c�digo en la mezcla de reacci�n');
			return;
		}
	}
	
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 'F',
		'cs' : $('#cs').val(),
		'reaccionesF' : $('#reaccionesF').val(),
		'F1tipo' : vector('F1tipo'),
		'F1descr' : vector('F1descr'),
		'F1insumos' : vector('F1insumos'),
		'F1volumen' : vector('F1volumen'),
		'F1reacciones' : vector('F1reacciones'),
		'F2cod_equipos' : vector('F2cod_equipos'),
		'Famplicon1' : $('#Famplicon1').val(),
		'Famplicon2' : $('#Famplicon2').val(),
		'programaF' : $('#programaF').val(),
		'obsF' : $('#obsF').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					OMEGA("Transaccion finalizada");
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Guarda7(){
	var hay=0;
	var control1 = document.getElementsByName('G1cod_insumos');
	var control2 = document.getElementsByName('G1total');
	for(i=0;i<control1.length;i++){
		if( !Mal(1, control1[i].value) ){			
			if( control2[i].value == '0' || control2[i].value == '0.00' ){
				OMEGA('Debe indicar la cantidad de insumo a utilizar');
				return;
			}
			hay++;
		}
	}
	
	if(hay < 1){
		OMEGA('No ha indicado ning�n insumo');
		return;
	}
	
	hay=0;
	control1 = document.getElementsByName('G2cod_equipos');
	for(i=0;i<control1.length;i++){
		if( !Mal(1, control1[i].value) ){
			hay++;
			break;
		}
	}
	
	if(hay < 1){
		OMEGA('No ha indicado ning�n equipo');
		return;
	}
	
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 'G',
		'cs' : $('#cs').val(),
		'tempG' : $('#tempG').val(),
		'humedadG' : $('#humedadG').val(),
		'tiempoG' : $('#tiempoG').val(),
		'voltajeG' : $('#voltajeG').val(),
		'obsG' : $('#obsG').val(),
		'G1cod_insumos' : vector('G1cod_insumos'),
		'G1total' : vector('G1total'),
		'G2cod_equipos' : vector('G2cod_equipos')
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					OMEGA("Transaccion finalizada");
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Guarda8(){
	var control1 = document.getElementsByName('cumple[]');
	for(i=0;i<control1.length;i++){
		if( Mal(1, control1[i].value) ){
			OMEGA('Falta indicar alg�n resultado');
			return;
		}
	}
	
	var control1 = document.getElementsByName('result[]');
	for(i=0;i<control1.length;i++){
		if( Mal(1, control1[i].value) ){
			OMEGA('Falta indicar alg�n resultado');
			return;
		}
	}
	
	if(!confirm('Modificar datos?')) return;
	ALFA('Por favor espere....');
	document.formulario2.submit();
}

function vector(ctrl){
	var str = "";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		if(control[i].value != ""){
			str += "&" + control[i].value.replace(/&/g, ''); 
		}
	}
	return str;
}

function Germinacion(){
	var valor = $('#requiere').val();
	
	if(valor=='0')
		document.getElementById('tr_germinacion').style.display = 'none';
	else
		document.getElementById('tr_germinacion').style.display = '';
}

function EquiposLista(_tipo, _linea){
	$('#tipo').val(_tipo);
	window.open(__SHELL__ + "?list2="+_linea,"","width=500,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list2="+_linea, this.window, "dialogWidth:500px;dialogHeight:200px;status:no;");
}

function EquipoEscoge(cs, codigo, linea){
	var tipo = opener.document.getElementById('tipo').value;
	
	if( opener.ValidaExiste(tipo+'cod_equipos', cs) ){
		alert('La opci�n seleccionada ya se encuentra en la lista');
		return;
	}
	
	opener.document.getElementById(tipo+'cod_equipos'+linea).value = cs;
	opener.document.getElementById(tipo+'equipos'+linea).value = codigo;
	
	window.close();
}

function InsumosLista(_tipo, _linea, control){
	$('#tipo').val(_tipo);
	var padre = control.getAttribute('padre');
	window.open(__SHELL__ + "?list3="+_linea+"&padre="+padre,"","width=500,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list3="+_linea+"&padre="+padre, this.window, "dialogWidth:500px;dialogHeight:200px;status:no;");
}

function CatalogoEscoge(cs, codigo, linea){
	var tipo = opener.document.getElementById('tipo').value;

	if( opener.ValidaExiste(tipo+'cod_insumos', cs) ){
		alert('La opci�n seleccionada ya se encuentra en la lista');
		return;
	}
	
	if(tipo=='E1' || tipo=='F1'){
		if( opener.ValidaExiste(tipo+'cod_insumos', codigo) ){
			alert('La opci�n seleccionada ya se encuentra en la lista');
			return;
		}
		
		opener.document.getElementById(tipo+'cod_insumos'+linea).value = codigo;
	}else{
		if( opener.ValidaExiste(tipo+'cod_insumos', cs) ){
			alert('La opci�n seleccionada ya se encuentra en la lista');
			return;
		}
		
		opener.document.getElementById(tipo+'cod_insumos'+linea).value = cs;
	}
	opener.document.getElementById(tipo+'insumos'+linea).value = codigo;
	
	window.close();
}

function AgregarInsumos(_tipo){
	var linea = document.getElementsByName(_tipo+"cod_insumos").length;
	var fila = document.createElement("tr");
	var colum = new Array(3);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[0].innerHTML = (linea+1) + '&nbsp;&nbsp;<input type="text" id="'+_tipo+'insumos'+linea+'" class="lista" readonly onclick="InsumosLista(\''+_tipo+'\', '+linea+', this)" padre="" /><input type="hidden" id="'+_tipo+'cod_insumos'+linea+'" name="'+_tipo+'cod_insumos" />';
	colum[1].innerHTML = '<font size="-2" id="'+_tipo+'nomb'+linea+'">...</font>';
	colum[2].innerHTML = '<input type="text" id="'+_tipo+'total'+linea+'" name="'+_tipo+'total" class="monto2" value="0" onblur="_FLOAT(this)" />';

	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	document.getElementById('lolo'+_tipo).appendChild(fila);
}

function Sugerido(ctr){
	ctr.style.color = '#000000';
}

function AgregarEquipos(_tipo){
	var linea = document.getElementsByName(_tipo+"cod_equipos").length;
	var fila = document.createElement("tr");
	var colum = new Array(2);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[0].innerHTML = (linea+1);	
	colum[1].innerHTML = '<input type="text" id="'+_tipo+'equipos'+linea+'" class="lista2" readonly onclick="EquiposLista(\''+_tipo+'\', '+linea+')" /><input type="hidden" id="'+_tipo+'cod_equipos'+linea+'" name="'+_tipo+'cod_equipos" />';

	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	document.getElementById('lolo'+_tipo).appendChild(fila);
}

function Oculta(_paso){
	if(document.getElementById('tabla'+_paso).style.display == 'none')
		document.getElementById('tabla'+_paso).style.display = '';
	else
		document.getElementById('tabla'+_paso).style.display = 'none';
}