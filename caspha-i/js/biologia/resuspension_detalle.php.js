function _RED(_CANTIDAD, _DECIMALES){
	var _CANTIDAD = parseFloat(_CANTIDAD);
	var _DECIMALES = parseFloat(_DECIMALES);
	_DECIMALES = (!_DECIMALES ? 2 : _DECIMALES);
	_DECIMALES = Math.round(_CANTIDAD * Math.pow(10, _DECIMALES)) / Math.pow(10, _DECIMALES);
	if(isNaN(_DECIMALES) || _DECIMALES == Infinity || _DECIMALES == -Infinity) return "0.00";
	else return _DECIMALES;
}

function Calcula(){
	if(document.getElementById('concentracionI').value != '0.00'){
		var formula = (parseFloat(document.getElementById('concentracionM').value) * parseFloat(document.getElementById('vol_disolvente').value)) / parseFloat(document.getElementById('concentracionI').value);		
		document.getElementById('formula').value = _RED(formula, 4);
	}else{
		document.getElementById('formula').value = 0;
	}
}

function Sort(){
	var linea = document.getElementsByName("observaciones").length;
	var madre = $('#codigo').val().replace(/ /g, '');
	for(i=0;i<linea;i++){
		document.getElementById('linea'+i).innerHTML = madre + '-' + (i+1);
	}
}

function ResAgregarLinea(){
	var linea = document.getElementsByName("observaciones").length;
	var fila = document.createElement("tr");
	fila.style.textAlign="center"; 
	var colum = new Array(3);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	
	colum[0].innerHTML = '';
	colum[0].id = 'linea' + linea;
	colum[1].innerHTML = '<input type="text" name="observaciones" />';
	colum[2].innerHTML = '<select name="estado"><option value="1">Disponible</option><option value="0">Agotado</option></select>';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	
	document.getElementById('lolo').appendChild(fila);
	Sort();
}

function vector(ctrl){
	var str = "";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++) str += "&" + control[i].value.replace(/&/g, ''); 
	return str;
}

function datos(){
	if( Mal(1, $('#codigo').val()) ){
		OMEGA('Debe indicar el c�digo �nico');
		return;
	}
	
	if( Mal(1, $('#fecha_r').val()) ){
		OMEGA('Debe indicar la fecha de resuspensi�n');
		return;
	}
	
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'accion' : $('#accion').val(),
		'codigo' : $('#codigo').val(),
		'reactivo' : $('#reactivo').val(),
		'fecha_r' : $('#fecha_r').val(),
		'disolvente' : $('#disolvente').val(),
		'concentracionI' : $('#concentracionI').val(),
		'vol_disolvente' : $('#vol_disolvente').val(),
		'concentracionM' : $('#concentracionM').val(),
		'medida1' : $('#medida1').val(),
		'medida2' : $('#medida2').val(),
		'medida3' : $('#medida3').val(),
		'observaciones' : vector("observaciones"),
		'estado' : vector("estado")		
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					alert("Transaccion finalizada");
					window.close();				
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}