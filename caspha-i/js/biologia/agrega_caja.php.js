function datos(){
    var parametros = {
        '_AJAX': 1,
        'id_camara':$('#ID').val(),
        'bandeja_linea':$('#L').val(),
        'nombre':$('#nombre').val(),
        'ubicacion':$('#ubicacion').val(),
    };
    
     $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
            }
            
            if(_response>1){
                alert("Transaccion finalizada");
                opener.location.href="caja_detalle.php?ID="+$('#ID').val()+"&L="+$('#L').val()+"&C="+_response;
                window.close();
            }
        }
    });
}