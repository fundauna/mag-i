var combo = '<option value="0">M</option><option value="1">&mu;M</option><option value="2">nM</option><option value="3">mM</option><option value="4">pM</option><option value="5">&mu;L</option><option value="6">mL</option><option value="7">L</option><option value="8">g</option><option value="9">&mu;g</option><option value="A">ng</option>';

function ResuspencionEscoge(codigo){
	opener.document.getElementById('codigo').value = codigo;
	window.close();
}

function ResuspencionesLista(){
	window.open(__SHELL__ + "?resus=1","","width=200,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?resus=1", this.window, "dialogWidth:200px;dialogHeight:200px;status:no;");
}

function _RED(_CANTIDAD, _DECIMALES){
//REDONDEO
	var _CANTIDAD = parseFloat(_CANTIDAD);
	var _DECIMALES = parseFloat(_DECIMALES);
	_DECIMALES = (!_DECIMALES ? 2 : _DECIMALES);
	_DECIMALES = Math.round(_CANTIDAD * Math.pow(10, _DECIMALES)) / Math.pow(10, _DECIMALES);
	if(isNaN(_DECIMALES) || _DECIMALES == Infinity || _DECIMALES == -Infinity) return "0.00";
	else return _DECIMALES;
}

function Calcula(){
	var control = document.getElementsByName("concentracionI");
	var formula = 0;
	for(i=0;i<control.length;i++){
		if(document.getElementById('concentracionF'+i).value != '0.00'){
			/*formula = (document.getElementById('concentracionF'+i).value * document.getElementById('disolvente'+i).value) / document.getElementById('concentracionI'+i).value;*/
			formula = (parseFloat(document.getElementById('concentracionF'+i).value) * parseFloat(document.getElementById('disolvente'+i).value)) / parseFloat(document.getElementById('concentracionI'+i).value);
			document.getElementById('formula'+i).value = _RED(formula, 4);
		}else{
			document.getElementById('formula'+i).value = 0;
		}
	}
}

function __agregaHR(){
	var colum = new Array(1);
	var fila = document.createElement("tr");
	colum[0] = document.createElement("td");
	colum[0].setAttribute('colspan', 5);
	colum[0].innerHTML = '<hr>';
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	document.getElementById('lolo').appendChild(fila);
}

function __agregaFila1(){
	var linea = document.getElementsByName("concentracionI").length;
	var colum = new Array(5);
	var fila = document.createElement("tr");
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	colum[4] = document.createElement("td");
	colum[0].setAttribute('rowspan', 2);
	colum[0].setAttribute('align', 'center');
	
	colum[0].innerHTML = (linea+1);
	colum[1].innerHTML = 'Analista:';
	colum[2].innerHTML = '';
	colum[3].innerHTML = 'Fecha:';
	colum[4].innerHTML = '';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	document.getElementById('lolo').appendChild(fila);
}

function __agregaFila2(){
	var colum = new Array(2);
	var fila = document.createElement("tr");
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[1].setAttribute('colspan', 4);
	
	colum[0].innerHTML = 'Observaciones:';
	colum[1].innerHTML = '<input type="text" name="observaciones" maxlength="100" style="width:90%" />';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	document.getElementById('lolo').appendChild(fila);
}

function __agregaFila3(){
	var colum = new Array(5);
	var fila = document.createElement("tr");
	fila.setAttribute('align', 'center');
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	colum[4] = document.createElement("td");
	
	colum[0].innerHTML = '<strong>C&oacute;digo disolvente</strong>';
	colum[1].innerHTML = '<strong>Concentraci&oacute;n inicial</strong>';
	colum[2].innerHTML = '<strong>Volumen disolvente</strong>';
	colum[3].innerHTML = '<strong>Concentraci&oacute;n final</strong>';
	colum[4].innerHTML = '<strong>Volumen a tomar</strong>';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	document.getElementById('lolo').appendChild(fila);
}

function __agregaFila4(){
	var linea = document.getElementsByName("concentracionI").length;
	var colum = new Array(5);
	var fila = document.createElement("tr");
	fila.setAttribute('align', 'center');
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	colum[4] = document.createElement("td");
	
	colum[0].innerHTML = '<input type="text" name="cod_disolvente" size="15" maxlength="20"/>';
	colum[1].innerHTML = '<input type="text" name="concentracionI" id="concentracionI'+linea+'" value="0.00" onblur="_FLOAT(this);Calcula();" class="monto"/><select name="medida1" id="medida1'+linea+'">'+combo+'</select>';
	colum[2].innerHTML = '<input type="text" name="disolvente" id="disolvente'+linea+'" value="0.00" onblur="_FLOAT(this);Calcula();" class="monto"/><select name="medida2" id="medida2'+linea+'">'+combo+'</select>';
	colum[3].innerHTML = '<input type="text" name="concentracionF" id="concentracionF'+linea+'" value="1.00" onblur="_FLOAT(this);Calcula();" class="monto"/><select name="medida3" id="medida3'+linea+'">'+combo+'</select>';
	colum[4].innerHTML = '<input type="text" readonly id="formula'+linea+'" class="monto" value="0.00"/>';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	document.getElementById('lolo').appendChild(fila);
}

function AliAgregarLinea(){
	__agregaHR();
	__agregaFila1();
	__agregaFila2();
	__agregaFila3();
	__agregaFila4();
}

function vector(ctrl){
	var str = "";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		//if(control[i].value != ""){
			str += "&" + control[i].value.replace(/&/g, ''); 
	}
	return str;
}

function datos(){
	if( Mal(1, $('#codigo').val()) ){
		OMEGA('Debe seleccionar el c�digo de resuspenci�n');
		return;
	}
	
	var control3 = document.getElementsByName('cod_disolvente');
	var hay = 0;
	
	for(i=0;i<control3.length;i++){
		if( !Mal(1, control3[i].value) ){
			hay++;
			break;
		}
	}
	
	if(hay < 1){
		OMEGA('No ha indicado ning�n detalle');
		return;
	}
	
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'accion' : $('#accion').val(),
		'codigo' : $('#codigo').val(),
		'reactivo' : $('#reactivo').val(),
		'observaciones' : vector("observaciones"),
		'cod_disolvente' : vector("cod_disolvente"),
		'concentracionI' : vector("concentracionI"),
		'disolvente' : vector("disolvente"),
		'concentracionF' : vector("concentracionF"),
		'medida1' : vector("medida1"),
		'medida2' : vector("medida2"),
		'medida3' : vector("medida3")
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			//$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					//$('#btn').disabled = false;
					break;
				case '1':
					alert("Transaccion finalizada");
					window.close();				
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}