function ResAgregarLinea(){
	var linea = document.getElementsByName("observaciones").length;
	var fila = document.createElement("tr");
	fila.style.textAlign="center"; 
	var colum = new Array(7);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	colum[4] = document.createElement("td");
	colum[5] = document.createElement("td");
	colum[6] = document.createElement("td");
	
	colum[0].innerHTML = '';
	colum[0].id = 'linea' + linea;
	colum[1].innerHTML = '<select id="tipo'+linea+'" name="tipo"><option value="">...</option><option value="0">Agua</option><option value="1">Controles</option><option value="2">dNTP</option><option value="3">Primer</option><option value="4">Sonda</option><option value="5">Otro</option></select>';
	colum[2].innerHTML = '<input type="text" id="nombre'+linea+'" name="nombre" size="20" maxlength="20"/>';	
	colum[3].innerHTML = '<select id="estado'+linea+'" name="estado"><option value="">...</option><option value="0">Agotado</option><option value="1">Disponible</option></select>';
	colum[4].innerHTML = '<textarea name="observaciones"></textarea>';
	colum[5].innerHTML = '';
	colum[6].innerHTML = '';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	
	document.getElementById('lolo').appendChild(fila);
	
	Sort();
}

function Sort(){
	var linea = document.getElementsByName("observaciones").length;
	var madre = $('#cs').val().replace(/ /g, '');
	for(i=0;i<linea;i++){
		document.getElementById('linea'+i).innerHTML = madre + '-' + (i+1);
	}
}

function vector(ctrl){
	var str = "";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		str += "&" + control[i].value.replace(/&/g, '');
	}
	return str;
}

function datos(){
	if( Mal(1, $('#cs').val()) ){
		OMEGA('Debe indicar el c�digo �nico');
		return;
	}
	
	var hay=0;
	var control1 = document.getElementsByName('tipo');
	var control2 = document.getElementsByName('estado');
	
	for(i=0;i<control1.length;i++){
		if( !Mal(1, control1[i].value) ){			
			
			if( Mal(1, control2[i].value) ){
				OMEGA('Debe indicar el estado');
				return;
			}
			
			hay++;
		}
	}
	
	if(hay < 1){
		OMEGA('No ha indicado ning�n detalle');
		return;
	}
	
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'accion' : $('#accion').val(),
		'cs' : $('#cs').val(),
		'tipo' : vector("tipo"),
		'nombre' : vector("nombre"),		
		'estado' : vector("estado"),
		'observaciones' : vector("observaciones")
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					alert("Transaccion finalizada");
					window.close();				
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}