$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		//"sScrollX": "100%",
		"sScrollXInner": "110%",
		"bScrollCollapse": true,
		"bFilter": true,
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function ResAgrega(){
	window.open("resuspension_detalle.php?acc=I","","width=535,height=500,scrollbars=yes,status=no");
	//window.showModalDialog("resuspension_detalle.php?acc=I", this.window, "dialogWidth:535px;dialogHeight:500px;status:no;");
}

function ResBuscar(btn){
	if( Mal(1, $('#codigo').val()) ){
		if( Mal(1, $('#desde').val()) || Mal(1, $('#hasta').val()) ){
			OMEGA('Debe indicar alg�n criterio de b�squeda');
			return;
		}
	}
	
	btn.disabled = true;
	btn.form.submit();
}

function ResModifica(_CS){
	window.open("resuspension_detalle.php?acc=M&CS="+_CS,"","width=535,height=500,scrollbars=yes,status=no");
	//window.showModalDialog("resuspension_detalle.php?acc=M&CS="+_CS, this.window, "dialogWidth:535px;dialogHeight:500px;status:no;");
}

function ResElimina(_CS){	
	if(!confirm('Eliminar registro?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'cs' : _CS,
		'accion' : 'D',
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					GAMA('Transacci�n finalizada');
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function marca(valor){
	if(valor==1){
		document.getElementById('id').readOnly = false;
		document.getElementById('nombre').readOnly = true;
		document.getElementById('nombre').value = '';
	}else{
		document.getElementById('nombre').readOnly = false;
		document.getElementById('id').readOnly = true;
		document.getElementById('id').value = '';
	}
}