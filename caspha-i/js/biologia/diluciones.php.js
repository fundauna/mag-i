$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		//"sScrollX": "100%",
		"sScrollXInner": "110%",
		"bScrollCollapse": true,
		"bFilter": true,
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function AliAgrega(){
	window.open("diluciones_detalle.php?acc=I","","width=670,height=500,scrollbars=yes,status=no");
	//window.showModalDialog("diluciones_detalle.php?acc=I", this.window, "dialogWidth:670px;dialogHeight:500px;status:no;");
}

function AliBuscar(btn){
	if( Mal(1, $('#codigo').val()) ){
		if( Mal(1, $('#desde').val()) || Mal(1, $('#hasta').val()) ){
			OMEGA('Debe indicar alg�n criterio de b�squeda');
			return;
		}
	}
	
	btn.disabled = true;
	btn.form.submit();
}

function AliModifica(_CS){
	window.open("diluciones_detalle.php?acc=M&CS="+_CS,"","width=670,height=500,scrollbars=yes,status=no");
	//window.showModalDialog("diluciones_detalle.php?acc=M&CS="+_CS, this.window, "dialogWidth:670px;dialogHeight:500px;status:no;");
}

function AliElimina(_CS){	
	if(!confirm('Eliminar registro?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'cs' : _CS,
		'accion' : 'D',
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					GAMA('Transacci�n finalizada');
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}