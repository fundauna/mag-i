$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		//"sScrollX": "100%",
		"sScrollXInner": "110%",
		"bScrollCollapse": true,
		"bFilter": true,
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function ResAgrega(){
	window.open("adenoteca_detalle.php?acc=I","","width=690,height=500,scrollbars=yes,status=no");
	//window.showModalDialog("adenoteca_detalle.php?acc=I", this.window, "dialogWidth:690px;dialogHeight:500px;status:no;");
}

function ResVer(_LINEA){
    var caja = document.getElementById("caja"+_LINEA);
    if(caja.value == ''){
        OMEGA('Debe seleccionar una caja');
	return;
    }
    iP = caja.getAttribute("dato1");
    nP = caja.getAttribute("dato2");
    window.open("escoge_bandejas.php?caja="+caja.value+"&iP="+iP+"&nP="+nP,"","width=690,height=500,scrollbars=yes,status=no");
}