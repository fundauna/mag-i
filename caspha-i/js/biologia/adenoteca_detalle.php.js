function BandejasLista(_actual){
	window.open(__SHELL__ + "?bandeja=1","","width=200,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?bandeja=1", this.window, "dialogWidth:200px;dialogHeight:200px;status:no;");
}

function CeldasLista(bandeja){
	window.open("../../../baltha-i/biologia/bandejas_detalle.php?bandeja="+bandeja,"","width=400,height=400,scrollbars=yes,status=no");
	//window.showModalDialog("../../../baltha-i/biologia/bandejas_detalle.php?bandeja="+bandeja, this.window, "dialogWidth:400px;dialogHeight:400px;status:no;");
}

function padre(rotulo, y, x, bandeja){
	var actual = 0;
	//
	opener.document.getElementById('rotulo'+actual).value=bandeja + ', ' + rotulo;
	opener.document.getElementById('bandeja'+actual).value=bandeja;
	opener.document.getElementById('x'+actual).value=x;
	opener.document.getElementById('y'+actual).value=y;
	window.close();
}

function CeldaLimpia(){
	var actual = opener.document.getElementById('actual').value;
	//
	opener.document.getElementById('rotulo'+actual).value='';
	opener.document.getElementById('bandeja'+actual).value='';
	opener.document.getElementById('x'+actual).value='';
	opener.document.getElementById('y'+actual).value='';
	window.close();
}

function ValidaExiste(_vector, _id){
	if(_id=='') return false;
	
	var control = document.getElementsByName(_vector);
	for(i=0;i<control.length;i++){
		if(document.getElementById(_vector + i).value == _id) return true
	}
	return false;
}

function AnalisisEscoge(linea, id, nombre, costo){
	var tipo = opener.document.getElementById('tipo').value;
	
	if( opener.ValidaExiste(tipo+'cod_analisis', id) ){
		alert('La opci�n seleccionada ya se encuentra en la lista');
		return;
	}
	
	opener.document.getElementById(tipo+'cod_analisis'+linea).value = id;
	opener.document.getElementById(tipo+'analisis'+linea).value = nombre;
	window.close();
}

function MacroAnalisisLista(_categoria, _tipo, _linea){
	$('#tipo').val(_tipo);
	window.open(__SHELL__ + "?list1="+_linea+"&tipo="+_categoria,"","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list1="+_linea+"&tipo="+_categoria, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function AgregarAnalisis(_categoria, _tipo){
	var linea = document.getElementsByName(_tipo+"cod_analisis").length;
	var fila = document.createElement("tr");
	var colum = new Array(2);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[0].innerHTML = (linea+1);
	colum[1].innerHTML = '<input type="text" id="'+_tipo+'analisis'+linea+'" class="lista2" readonly onclick="MacroAnalisisLista(\''+_categoria+'\', \''+_tipo+'\', '+linea+')" /><input type="hidden" id="'+_tipo+'cod_analisis'+linea+'" name="'+_tipo+'cod_analisis" />';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	document.getElementById('lolo'+_tipo).appendChild(fila);
}

function AnalisisMas(){
	var linea = document.getElementsByName("resultado").length;
	var fila = document.createElement("tr");
	fila.align="center";
	var colum = new Array(3);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	
	colum[0].innerHTML = '<input type="text" id="A3analisis'+linea+'" class="lista2" readonly onclick="MacroAnalisisLista(6, \'A3\', '+linea+')" /><input type="hidden" id="A3cod_analisis'+linea+'" name="A3cod_analisis" />';
	colum[1].innerHTML = '<select id="resultado'+linea+'" name="resultado"><option value="">...</option><option value="0">-</option><option value="1">+</option><option value="2">NC</option></select>';
	colum[2].innerHTML = '<textarea id="obs'+linea+'" name="obs" style="height:38px"></textarea>';
		
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	document.getElementById('lolo').appendChild(fila);
}

function vector(ctrl){
	var str = "";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		//if(control[i].value != ""){
		str += "&" + control[i].value.replace(/&/g, ''); 
		//}
	}
	return str;
}

function datos(){
	if( Mal(1, $('#cs').val()) ){
		OMEGA('Debe indicar el c�digo de muestra');
		return;
	}
	
	if( Mal(1, $('#analito').val()) ){
		OMEGA('Debe indicar el �cido nucleico');
		return;
	}
	
	if( Mal(1, $('#rotulo0').val()) ){
		OMEGA('Debe seleccionar la bandeja');
		return;
	}
	
	var control1 = document.getElementsByName('A3cod_analisis');
	var hay = 0;
	
	for(i=0;i<control1.length;i++){
		if( !Mal(1, control1[i].value) ){
			hay++;
			break;
		}
	}
	
	if(hay < 1){
		OMEGA('No ha indicado ninguna l�nea en el detalle');
		return;
	}
	
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'accion' : $('#accion').val(),
		'cs' : $('#cs').val(),
		'analito' : $('#analito').val(),
		'rotulo' : $('#rotulo0').val(),
		'bandeja' :  $('#bandeja0').val(),
		'x' : $('#x0').val(),
		'y' : $('#y0').val(),
		'cultivo' : $('#cultivo').val(),
		'cuanti' : $('#cuanti').val(),
		'hoja' : $('#hoja').val(),
		'fechaA' : $('#fechaA').val(),
		'observaciones' : $('#observaciones').val(),
		'A1cod_analisis' : vector('A1cod_analisis'),
		'A2cod_analisis' : vector('A2cod_analisis'),
		'A3cod_analisis' : vector('A3cod_analisis'),
		'resultado' : vector('resultado'),
		'obs' : vector('obs')
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					alert("Transaccion finalizada");
					opener.location.reload();
					window.close();				
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function eliminar(){

	if(!confirm('Desea eliminar el registro?')) return;
	
	var parametros = {
		'_AJAX' : 2,
		'cs' : $('#cs').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					alert("Transaccion finalizada");
					opener.location.reload();
					window.close();				
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}