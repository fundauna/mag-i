function DocumentosZip(_ARCHIVO, _MODULO){
	var ruta = "../seguridad/documentos.php?ARCHIVO=" + _ARCHIVO + "&MODULO=" + _MODULO + "&ACCESO=A7";
	window.open(ruta,"","width=100,height=100,scrollbars=yes,status=no");
	//window.showModalDialog(ruta, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}

function datos(){
	if( Mal(5, $('#cs').val()) ){
		OMEGA('Debe indicar el c�digo del procedimiento');
		return;
	}
	
	if( Mal(5, $('#nombre').val()) ){
		OMEGA('Debe indicar el nombre del procedimiento');
		return;
	}
	
	if( Mal(1, $('#Danalito').val()) ){
		OMEGA('Debe indicar el analito');
		return;
	}
	
	if( Mal(1, $('#Dunidad').val()) ){
		OMEGA('Debe indicar la unidad');
		return;
	}
	
	if(!confirm('Modificar datos?')) return;
	
	ALFA('Por favor espere....');
	document.formulario.submit();
}

function ValidaExiste(_vector, _id){
	if(_id=='') return false;
	
	var control = document.getElementsByName(_vector+'[]');
	for(i=0;i<control.length;i++){
		if(document.getElementById(_vector + i).value == _id) return true
	}
	return false;
}

function EquipoEscoge(cs, codigo, linea){
	var tipo = opener.document.getElementById('tipo').value;
	
	if( opener.ValidaExiste(tipo+'cod_equipos', cs) ){
		alert('La opci�n seleccionada ya se encuentra en la lista');
		return;
	}
	
	opener.document.getElementById(tipo+'cod_equipos'+linea).value = cs;
	opener.document.getElementById(tipo+'equipos'+linea).value = codigo;
	
	window.close();
}

function CatalogoEscoge(cs, codigo, linea){
	var tipo = opener.document.getElementById('tipo').value;
	
	if( opener.ValidaExiste(tipo+'cod_insumos', cs) ){
		alert('La opci�n seleccionada ya se encuentra en la lista');
		return;
	}
		
	opener.document.getElementById(tipo+'cod_insumos'+linea).value = cs;
	opener.document.getElementById(tipo+'insumos'+linea).value = codigo;
	window.close();
}

function AnalisisEscoge(linea, id, nombre, costo){
	var tipo = opener.document.getElementById('tipo').value;
	
	if( opener.ValidaExiste(tipo+'cod_analisis', id) ){
		alert('La opci�n seleccionada ya se encuentra en la lista');
		return;
	}
	
	opener.document.getElementById(tipo+'cod_analisis'+linea).value = id;
	opener.document.getElementById(tipo+'analisis'+linea).value = nombre;
	window.close();
}

function EquiposLista(_tipo, _linea){
	$('#tipo').val(_tipo);
	window.open(__SHELL__ + "?list2="+_linea,"","width=500,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list2="+_linea, this.window, "dialogWidth:500px;dialogHeight:200px;status:no;");
}

function InsumosLista(_tipo, _linea){
	$('#tipo').val(_tipo);
	window.open(__SHELL__ + "?list3="+_linea,"","width=500,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list3="+_linea, this.window, "dialogWidth:500px;dialogHeight:200px;status:no;");
}

function MacroAnalisisLista(_categoria, _tipo, _linea){
	$('#tipo').val(_tipo);
	window.open(__SHELL__ + "?list1="+_linea+"&tipo="+_categoria,"","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list1="+_linea+"&tipo="+_categoria, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function AgregarAnalisis(_categoria, _tipo){
	var linea = document.getElementsByName(_tipo+"cod_analisis[]").length;
	var fila = document.createElement("tr");
	var colum = new Array(2);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[0].innerHTML = (linea+1);
	colum[1].innerHTML = '<input type="text" id="'+_tipo+'analisis'+linea+'" class="lista2" readonly onclick="MacroAnalisisLista(\''+_categoria+'\', \''+_tipo+'\', '+linea+')" /><input type="hidden" id="'+_tipo+'cod_analisis'+linea+'" name="'+_tipo+'cod_analisis[]" />';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	document.getElementById('lolo'+_tipo).appendChild(fila);
}

function AgregarPlantilla1(){
	var linea = document.getElementsByName("E1tipo[]").length;
	var fila = document.createElement("tr");
	var colum = new Array(6);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	colum[4] = document.createElement("td");
	colum[5] = document.createElement("td");
	
	colum[0].innerHTML = (linea+1);
	colum[1].innerHTML = '<select name="E1tipo[]"><option value="">...</option><option value="0">Reactivo</option><option value="1">Alicuota</option></select>';
	colum[2].innerHTML = '<input type="text" name="E1descr[]" size="20" maxlength="20" />';
	colum[3].innerHTML = '<input type="text" name="E1concS[]" size="10" maxlength="20" />';
	colum[4].innerHTML = '<input type="text" name="E1concF[]" size="10" maxlength="20" />';
	colum[5].innerHTML = '<input type="text" name="E1volumen[]" class="monto2" value="0" onblur="_FLOAT(this)"/>';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	document.getElementById('loloE1').appendChild(fila);
}

function AgregarPlantilla2(){
	var linea = document.getElementsByName("G1tipo[]").length;
	var fila = document.createElement("tr");
	var colum = new Array(4);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	
	colum[0].innerHTML = (linea+1);
	colum[1].innerHTML = '<select name="G1tipo[]"><option value="">...</option><option value="0">Reactivo</option><option value="1">Alicuota</option></select>';
	colum[2].innerHTML = '<input type="text" name="G1descr[]" size="20" maxlength="20" />';
	colum[3].innerHTML = '<input type="text" name="G1volumen[]" class="monto2" value="0" onblur="_FLOAT(this)"/>';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	document.getElementById('loloG1').appendChild(fila);
}

function AgregarInsumos(_tipo){
	var linea = document.getElementsByName(_tipo+"cod_insumos[]").length;
	var fila = document.createElement("tr");
	var colum = new Array(2);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[0].innerHTML = (linea+1) + '&nbsp;&nbsp;<input type="text" id="'+_tipo+'insumos'+linea+'" class="lista2" readonly onclick="InsumosLista(\''+_tipo+'\', '+linea+')" /><input type="hidden" id="'+_tipo+'cod_insumos'+linea+'" name="'+_tipo+'cod_insumos[]" />';
	colum[1].innerHTML = '<input type="text" id="'+_tipo+'total'+linea+'" name="'+_tipo+'total[]" class="monto2" value="0" onblur="_FLOAT(this)" />';

	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	document.getElementById('lolo'+_tipo).appendChild(fila);
}

function AgregarEquipos(_tipo){
	var linea = document.getElementsByName(_tipo+"cod_equipos").length;
	var fila = document.createElement("tr");
	var colum = new Array(2);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[0].innerHTML = (linea+1);	
	colum[1].innerHTML = '<input type="text" id="'+_tipo+'equipos'+linea+'" class="lista2" readonly onclick="EquiposLista(\''+_tipo+'\', '+linea+')" /><input type="hidden" id="'+_tipo+'cod_equipos'+linea+'" name="'+_tipo+'cod_equipos[]" />';

	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	document.getElementById('lolo'+_tipo).appendChild(fila);
}

function vector(ctrl){
	var str = "";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		if(control[i].value != ""){
			str += "&" + control[i].value.replace(/&/g, ''); 
		}
	}
	return str;
}