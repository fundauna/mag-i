function HojaTrabajo(_url, _tk = 0) {
    window.open(_url, "", "width=800,height=600,scrollbars=yes,status=no");
    if (_tk == "1") {
        window.open(_url.replace("hoja2", "hoja7"), "", "width=800,height=600,scrollbars=yes,status=no");
    }
    if (_tk == "2") {
        window.open(_url.replace("hoja6", "hoja7"), "", "width=800,height=600,scrollbars=yes,status=no");
    }
    //window.showModalDialog(_url, this.window, "dialogWidth:800px;dialogHeight:600px;status:no;");
}

function Anular() {
    document.getElementById('obs').disabled = false;
    //$('#obs').val('');
    //FALTA JUSTIFICACION
    if ($('#obs').val() == '') {
        OMEGA('Debe indicar la justificacion en observaciones');
        return;
    }

    if (!confirm('Anular documento?'))
        return;
    _Modificar(5);
}

function _Modificar(_estado) {
    var parametros = {
        'modificar': 1,
        'solicitud': $('#ID').val(),
        'obs': $('#obs').val(),
        'codigo': vector("codigo"),
        'estado': _estado,
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesion expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el envio de parametros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    alert("Transaccion finalizada");
                    opener.location.reload();
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function Aprobar() {
    if (!confirm('Aprobar documento?'))
        return;
    _Modificar(2);
}
function Reversar() {
    if (!confirm('Reversar documento?'))
        return;
    _Modificar(0);
}

function Procesar() {
    if (!confirm('Enviar documento a revisi�n?'))
        return;
    _Modificar(1);
}

function datos() {
    if ($('#nomcliente').val() == '') {
        OMEGA('Debe indicar un cliente');
        return;
    }

    if ($('#nomsolicitante').val() == '') {
        OMEGA('Debe indicar un solicitante');
        return;
    }

    if (Mal(2, $('#entregado').val())) {
        OMEGA('Debe indicar el nombre de la persona que entrega la muestra');
        return;
    }

    if ($('#proposito').val() == '') {
        OMEGA('Debe indicar un prop�sito del analisis');
        return;
    }

    if (Mal(2, $('#muestreo').val())) {
        OMEGA('Debe indicar el muestro');
        return;
    }

    if (Mal(1, $('#entregacliente').val())) {
        OMEGA('Debe indicar el plazo de entrega al cliente');
        return;
    }

    if (Mal(1, $('#entregaanalista').val())) {
        OMEGA('Debe indicar el plazo de entrega al analista');
        return;
    }

    if (Mal(1, $('#fmuestra').val())) {
        OMEGA('Debe indicar la fecha de muestreo');
        return;
    }

    if (Mal(1, $('#hmuestra').val())) {
        OMEGA('Debe indicar la hora del muestreo');
        return;
    }

    if (Mal(1, $('#flab').val())) {
        OMEGA('Debe indicar la fecha de recepcion del laboratorio');
        return;
    }

    if (Mal(1, $('#hlab').val())) {
        OMEGA('Debe indicar la hora de recepcion del laboratorio');
        return;
    }

    var control2 = document.getElementsByName('codigo');
    var control3 = document.getElementsByName('matriz');
    var control21 = document.getElementsByName('tipo');
    var control4 = document.getElementsByName('presentacion');
    var control5 = document.getElementsByName('empaque');
    var control6 = document.getElementsByName('forma');
    var contro20 = document.getElementsByName('masa');
    var control7 = document.getElementsByName('observaciones');
    var control8 = document.getElementsByName('nom_analisis');
    var control9 = document.getElementsByName('metodo');
    var controlA = document.getElementsByName('parte');
    var control22 = document.getElementsByName('hmuestra');
    var control23 = document.getElementsByName('fmuestra');
    var control24 = document.getElementsByName('nia');
    var hay = 0;

    for (i = 0; i < control2.length; i++) {
        if (!Mal(1, control2[i].value)) {

            if (Mal(1, control3[i].value)) {
                OMEGA('Debe indicar el elemento matriz');
                return;
            }

            if (Mal(1, control4[i].value)) {
                OMEGA('Debe indicar la presentaci�n de la muestra');
                return;
            }
            if (Mal(1, control5[i].value)) {
                OMEGA('Debe indicar el estado del empaque');
                return;
            }
            if (Mal(1, control6[i].value)) {
                OMEGA('Debe indicar la forma en que se recibe');
                return;
            }

            if (Mal(1, control8[i].value)) {
                OMEGA('Debe indicar el analisis solicitado');
                return;
            }

            if (Mal(1, control9[i].value)) {
                OMEGA('Debe indicar el método');
                return;
            }

            if (Mal(1, controlA[i].value)) {
                OMEGA('Debe indicar la parte a analizar');
                return;
            }
            if (Mal(1, contro20[i].value)) {
                OMEGA('Debe indicar la masa');
                return;
            }
            if (Mal(1, control21[i].value)) {
                OMEGA('Debe especificar un tipo');
                return;
            }

            if (Mal(1, control22[i].value)) {
                OMEGA('Debe especificar una hora');
                return;
            }
            if (Mal(1, control23[i].value)) {
                OMEGA('Debe especificar una fecha');
                return;
            }

            hay++;
        }
    }

    if (hay < 1) {
        OMEGA('No ha indicado ninguna l�nea');
        return;
    }

    if (!confirm('Modificar datos?'))
        return;

    var file_data = $("#adjunto").prop("files")[0];
    var form_data = new FormData();

    form_data.append("_AJAX", 1);
    form_data.append("cs", $('#cs').val());
    form_data.append("accion", $('#accion').val());
    form_data.append("cliente", $('#cliente').val());
    form_data.append("nomcliente", $('#nomcliente').val());
    form_data.append("nomsolicitante", $('#nomsolicitante').val());
    form_data.append("entregado", $('#entregado').val());
    form_data.append("proposito", $('#proposito').val());
    form_data.append("muestreo", $('#muestreo').val());
    form_data.append("entregacliente", $('#entregacliente').val());
    form_data.append("entregaanalista", $('#entregaanalista').val());
    form_data.append("obs", $('#obs').val());
    form_data.append("muestra", vector("muestra"));
    form_data.append("codigo", vector("codigo"));
    form_data.append("matriz", vector("matriz"));
    form_data.append("tipo", vector("tipo"));
    form_data.append("presentacion", vector("presentacion"));
    form_data.append("empaque", vector("empaque"));
    form_data.append("forma", vector("forma"));
    form_data.append("masa", vector("masa"));
    form_data.append("observaciones", vector("observaciones"));
    form_data.append("obsfrf", vector("obsfrf"));
    form_data.append("analisis", vector("cod_analisis"));
    form_data.append("parte", vector("parte"));
    form_data.append("metodo", vector("metodo"));
    form_data.append("flab", $('#flab').val());
    form_data.append("hlab", $('#hlab').val());
    form_data.append("fmuestra",vector("fmuestra"));
    form_data.append("hmuestra", vector("hmuestra"));
    form_data.append("file", file_data);
    form_data.append("nia", vector("nia"));


    $.ajax({
        data: form_data,
        url: __SHELL__,
        type: 'post',
        dataType: 'script',
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {
            $('#btn').disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    alert("Transaccion finalizada");
                    opener.location.reload();
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}



function asigna(){
    if (!confirm('Asignar número de informe?'))
        return;
    var form_data = new FormData();
    form_data.append("_AJAX", 101);
    form_data.append("cs", $('#cs').val());
    form_data.append("muestra", vector("muestra"));

    $.ajax({
        data: form_data,
        url: __SHELL__,
        type: 'post',
        dataType: 'script',
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {
            $('#btn').disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    alert("Transaccion finalizada");
                    opener.location.reload();
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}





function getSolicitante() {
    var parametros = {
        '_AJAX': 2,
        'cliente': $('#cliente').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            //$('#btn').disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            BETA();
            datosJ = JSON.parse(_response);
            if (datosJ.length > 0) {
                $('#nomsolicitante').val(datosJ[0].contacto);
            } else {
                OMEGA('No se encontro solicitante');
            }
        }
    });
}

function vector(ctrl) {
    var str = "1=1";
    control = document.getElementsByName(ctrl);
    for (i = 0; i < control.length; i++) {
        str += "&" + control[i].value.replace(/&/g, '');
    }
    return str;
}

function SolicitudAgregar() {
    var linea = document.getElementsByName("codigo").length;
    var fila = document.createElement("tr");
    var colum = new Array(15);

    colum[0] = document.createElement("td");
    colum[1] = document.createElement("td");
    colum[2] = document.createElement("td");
    colum[3] = document.createElement("td");
    colum[4] = document.createElement("td");
    colum[5] = document.createElement("td");
    colum[6] = document.createElement("td");
    colum[7] = document.createElement("td");
    colum[8] = document.createElement("td");
    colum[9] = document.createElement("td");
    colum[10] = document.createElement("td");
    colum[11] = document.createElement("td");
    colum[12] = document.createElement("td");
    colum[13] = document.createElement("td");
    colum[14] = document.createElement("td");

    colum[0].innerHTML = '<input type="text" size="8" readonly/>';
    colum[1].innerHTML = '<input type="text" size="29" name="codigo"/>';
    colum[2].innerHTML = '<input type="text" size="10" name="nommatriz" id="nommatriz' + linea + '" class="lista" readonly onclick="MatrizLista(' + linea + ')"><input type="hidden" name="matriz" id="matriz' + linea + '">';
    colum[3].innerHTML = '<input type="text" size="10" name="tipo"/>';
    colum[4].innerHTML = '<select name="presentacion"><option value="">...</option><option value="0">Congelada</option><option value="1">Descongelada</option><option value="2">Fresca, no refrigerada</option><option value="3">Fresca, refrigerada</option></select>';
    colum[5].innerHTML = '<select name="empaque"><option value="">...</option><option value="0">Bolsa Intacta</option><option value="1">Bolsa Da&ntilde;ada</option><option value="2">Botella Vidrio Ambar</option><option value="3">Otro</option></select>';
    colum[6].innerHTML = '<select name="forma"><option value="">...</option><option value="0">Entero</option><option value="1">En Cuartos</option><option value="2">Rebanadas</option><option value="3">Podrida</option><option value="4">Golpeada</option><option value="5">Otro</option></select>';
    colum[7].innerHTML = '<input type="text" id="fmuestra' + linea + '"name="fmuestra" size="10" class="fecha" onClick="show_calendar(this.id)" value="" /> <input type="text" size="3" id="fmuestra' + linea + '" name="hmuestra" />';
    colum[8].innerHTML = '<input type="text" size="9" name="masa"/>';
    colum[9].innerHTML = '<textarea name="observaciones" title="Alfanum�rico (5/200)"></textarea>';
    colum[10].innerHTML = '<input size="8" type="text" name="obsfrf" id="obsfrf" value="" />';
    colum[11].innerHTML = '<input type="text" name="nom_analisis" id="nom_analisis' + linea + '" class="lista" readonly onclick="SubAnalisisLista(' + linea + ')"><input type="hidden" name="cod_analisis" id="cod_analisis' + linea + '">';
    colum[12].innerHTML = '<select name="parte"><option value="">...</option><option value="0">&Iacute;ntegra</option><option value="1">C&aacute;scara</option><option value="2">Pulpa</option></select>';
    colum[13].innerHTML = '<input type="text" name="metodo" size="7" maxlength="20" value="No"/>';
    colum[14].innerHTML = '<input type="text" id="nia" size="8"  value="" readonly />';



    for (i = 0; i < colum.length; i++)
        fila.appendChild(colum[i]);

    document.getElementById('lolo').appendChild(fila);
}

function ClienteEscoge(id, nombre) {
    opener.document.getElementById('cliente').value = id;
    opener.document.getElementById('nomcliente').value = nombre;

    strBusqueda = nombre.toLowerCase();
    if (strBusqueda.search('estacion') == 0 || strBusqueda.search('estaci�n') == 0) {
        opener.document.getElementById('entregacliente').value = 3;
    } else {
        opener.document.getElementById('entregacliente').value = 10;
    }
    window.opener.getSolicitante();
    //opener.document.getElementById('nomsolicitante').value=contacto;
    window.close();
}

function ClientesLista() {
    window.open(__SHELL__ + "?list=1", "", "width=400,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function CambiaDiasEntrega(nombre) {
    alert(nombre);
}

function SolicitanteLista() {
    cliente = document.getElementById('cliente').value;
    window.open(__SHELL__ + "?list=4&cliente=" + cliente, "", "width=400,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function SolicitanteEscoge(nombre) {
    opener.document.getElementById('nomsolicitante').value = nombre;
    //opener.document.getElementById('nomsolicitante').value=contacto;
    window.close();
}

function MatrizLista(linea) {
    window.open(__SHELL__ + "?list=2&linea=" + linea, "", "width=400,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list=2&linea="+linea, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function SubAnalisisLista(linea) {
    window.open(__SHELL__ + "?list=3&linea=" + linea, "", "width=400,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list=3&linea="+linea, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function MatrizEscoge(id, nombre, linea) {
    if (ValidaMatrizEscoge(id)) {
        opener.document.getElementById('matriz' + linea).value = id;
        opener.document.getElementById('nommatriz' + linea).value = nombre;
        window.close();
    } else {
        alert("No puede combinar matrices, debe contener solo Vegetales o Agua o Suelo");
    }
}

function ValidaMatrizEscoge(id) {
    var bandera = true;
    var agua = false;
    var vegetal = false;
    var suelo = false;
    var contador = 0;
    control = opener.document.getElementsByName("matriz");
    for (i = 0; i < control.length; i++) {
        if (control[i].value == '3') {
            agua = true;
            //alert("agua");
        }
        else
        {
            if (control[i].value == '149') {
                suelo = true;
                //alert("suelo");
            }
            else{
                if (control[i].value != '') {
                vegetal =true;
                //alert("vegetal");
                }
            }
        }
    
        contador++;
    }
    var contadorTipos = 0;
    if (id == '3' ) {
        agua = true;
    }
    else{
        if (id == '149'  ) {
            suelo = true;
        }
        else
        {vegetal=true;}
    }
    
    if (agua){ contadorTipos++;}
    if (suelo){ contadorTipos++;}
    if (vegetal){ contadorTipos++;}
    //si tiene mas de un tipo es un error

    /*if (id == '3' && contador > 1) {
        agua = true;
    }
    if (id == '149' && contador > 1) {
        suelo = true;
    }
    if (agua || suelo) {
        bandera = false;
    }*/
    //alert(contadorTipos);
    if (contadorTipos>1)
    {
        bandera = false;
    }
    return bandera;
}

function ElementosEscoge(linea, id, nombre) {
    opener.document.getElementById('cod_analisis' + linea).value = id;
    opener.document.getElementById('nom_analisis' + linea).value = nombre;
    window.close();
}

function UsuarioEscoge(id, nombre) {
    opener.document.getElementById('solicitante').value = id;
    opener.document.getElementById('nomsolicitante').value = nombre;
    window.close();
}

function UsuariosLista() {
    window.open(__SHELL__ + "?list2=1", "", "width=400,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list2=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function eliminaDoc() {
    if (!confirm("Desea eliminar el documento adjunto?")) {
        return;
    }

    var parametros = {
        '_AJAX': 3,
        'cs': $('#ID').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error al eliminar el documento');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    alert("Documento eliminado");
                    location.reload();
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });


}

function SolicitudesImprime1(){
    _ID = document.getElementById('ID').value;
   /* var tipo = $('#LAB').val(); //document.getElementById('LAB').value;*/
   var tipo = "1";
    window.open("solicitud0" + tipo + "_imprimir.php?ID="+_ID,"","");
    //window.showModalDialog("solicitud0" + tipo + "_imprimir.php?ID="+_ID, this.window, "dialogWidth:900px;dialogHeight:750px;status:no;");
}

function imprimirlo() {
    var cs = $('#ID').val();
    cs = cs.trim(cs);
    window.open("https://app.sfe.go.cr/ws_ReporteSILAB/frmreportesolicitud?solicitud=" + cs + "&formulario=3", "", "width='100%',height='100%',scrollbars=yes,status=no");
}


function homogenizacion() {
    var cs = $('#ID').val();
    cs = cs.trim(cs);
    window.open("https://app.sfe.go.cr/ws_ReporteSILAB/frmreportesolicitud?solicitud=" + cs + "&formulario=4", "", "width='100%',height='100%',scrollbars=yes,status=no");
}


function inyeccion() {
    var cs = $('#ID').val();
    cs = cs.trim(cs);
    window.open("https://app.sfe.go.cr/ws_ReporteSILAB/frmreportesolicitud?solicitud=" + cs + "&formulario=5", "", "width='100%',height='100%',scrollbars=yes,status=no");
}

function extraccion() {
    var cs = $('#ID').val();
    cs = cs.trim(cs);
    window.open("https://app.sfe.go.cr/ws_ReporteSILAB/frmreportesolicitud?solicitud=" + cs + "&formulario=8", "", "width='100%',height='100%',scrollbars=yes,status=no");
}

function extraccionAgua() {
    var cs = $('#ID').val();
    cs = cs.trim(cs);
    window.open("https://app.sfe.go.cr/ws_ReporteSILAB/frmreportesolicitud?solicitud=" + cs + "&formulario=13", "", "width='100%',height='100%',scrollbars=yes,status=no");
}

function humedad() {
    //falta crear el formulario
    var cs = $('#ID').val();
    cs = cs.trim(cs);
    window.open("https://app.sfe.go.cr/ws_ReporteSILAB/frmreportesolicitud?solicitud=" + cs + "&formulario=1", "", "width='100%',height='100%',scrollbars=yes,status=no");
}

function ImprimirTodasAgua() {
     
    inyeccion() ;
    extraccionAgua();

}
function ImprimirTodasSuelo() {
    homogenizacion();
    inyeccion() ;
    extraccion();
    humedad();
}
function ImprimirTodasVegetal() {
    homogenizacion();
    inyeccion() ;
    extraccionAgua();

}
