var valor = label = '', indice;

function CambiaTipo(){
	$('#elementos0').val('');
	$('#codigoB0').val('');
}

function ElementosLista(linea){
	var tipo = $('#tipo').val();
	window.open(__SHELL__ + "?list="+linea+"&tipo="+tipo,"","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list="+linea+"&tipo="+tipo, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function ElementosEscoge(linea, id, nombre){
	opener.document.getElementById('codigoB'+linea).value = id;
	opener.document.getElementById('elementos'+linea).value = nombre;
	window.close();
}

function ImpurezasEscoge(linea, id, nombre){
	opener.document.getElementById('codigoB'+linea).value = id;
	opener.document.getElementById('elementos'+linea).value = nombre;
	window.close();
}

function habilita(bool){
	document.getElementById('btn_buscar').disabled = !bool;
	document.getElementById('btn_agregar').disabled = !bool;
	document.getElementById('btn_eliminar').disabled = !bool;
}

function AutoBuscar(){
	if( Mal(1, $('#codigoB0').val()) ){
		OMEGA('Debe seleccionar el tipo');
		return;
	}
	
	var parametros = {
		'_AJAX' : 1,
		'PermisosBuscar' : 1,
		'analisis' : $('#codigoB0').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			habilita(false);
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				default:
					BETA();
					habilita(true);
					document.getElementById('td_asignados').innerHTML = _response;
					break;
			}
		}
	});
}

function PermisosAgrega(){
	if(document.getElementById('btn_agregar').disabled) return;
	
	var control = document.getElementById('permisos');
	if(control.selectedIndex == -1){
		OMEGA('Seleccione una formulaci�n');
		return;
	}
	habilita(false);
	valor = control.options[control.selectedIndex].value;
	label = control.options[control.selectedIndex].text;
	
	var parametros = {
		'_AJAX' : 1,
		'PermisosAgrega' : 1,
		'analisis' : $('#codigoB0').val(),
		'permiso' : control.options[control.selectedIndex].value,
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			habilita(false);
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					BETA();
					habilita(true);
					break;
				case '1':
					BETA();
					habilita(true);
					/**/
					var oOption = document.createElement('OPTION');
					oOption.value = valor;
					oOption.text = label;
					var combo = document.getElementById('asignados');
					combo.options[combo.options.length] = new Option(oOption.text, oOption.value);
					/**/
					break;
				default:alert(_response);break;
			}
		}
	});
}

function PermisosElimina(){
	var control = document.getElementById('asignados');
	if(control.selectedIndex == -1){
		OMEGA('Seleccione una formulaci�n asignada');
		return;
	}
	
	habilita(false);
	indice = control.selectedIndex;
	valor = control.options[control.selectedIndex].value;
	
	var parametros = {
		'_AJAX' : 1,
		'PermisosElimina' : 1,
		'analisis' : $('#codigoB0').val(),
		'permiso' : control.options[control.selectedIndex].value
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			habilita(false);
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '1':
					BETA();
					habilita(true);
					/**/
					document.getElementById('asignados').remove(indice);
					habilita(true);
					/**/
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}