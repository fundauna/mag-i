var _aviso = false;

function Documentos(_ID, _estado) {
    window.open("solicitud_docs.php?ID=" + _ID + "&estado=" + _estado, "", "width=600,height=450,scrollbars=yes,status=no");
    //window.showModalDialog("solicitud_docs.php?ID="+_ID + "&estado=" + _estado, this.window, "dialogWidth:600px;dialogHeight:450px;status:no;");
}

function Aprobar() {
    if (!confirm('Aprobar documento?')) return;
    _Modificar(2);
}

function AnularMedio() {
    document.getElementById('obs').disabled = false;
    if (!_aviso) {
        $('#obs').val('');
        _aviso = true;
    }

    if ($('#obs').val() == '') {
        OMEGA('Debe indicar la justificaci�n en observaciones');
        return;
    }

    if (!confirm('Enviar solicitud de anulaci�n a superior?')) return;
    _Modificar(6);
}

function Rechazar() {
    if (!confirm('Rechazar documento?')) return;
    _Modificar(4);
}

function Procesar() {
    if (!confirm('Enviar documento a revisi�n?')) return;
    _Modificar(1);
}

function Anular() {
    document.getElementById('obs').disabled = false;
    if (!_aviso) {
        $('#obs').val('');
        _aviso = true;
    }

    if ($('#obs').val() == '') {
        OMEGA('Debe indicar la justificaci�n en observaciones');
        return;
    }

    if (!confirm('Anular documento?')) return;
    _Modificar(5);
}

function _Modificar(_estado) {
    var parametros = {
        'modificar': 1,
        'solicitud': $('#ID').val(),
        'obs': $('#obs').val(),
        'estado': _estado,
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    alert("Transaccion finalizada");
                    opener.location.reload();
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function ValidaDensidad() {
    var vector = document.getElementsByName("unidad");

    if (document.getElementById('densidad').selectedIndex == 2) { //SI SE SOLICITO DENSIDAD
        //DEBE HABER POR LO MENOS UN M/V
        var MV = false
        for (i = 0; i < vector.length; i++) {
            if (vector[i].value == '1') {
                return true;
            }
        }

        OMEGA('No puede seleccionar densidad si no hay unidades %m/v');
        document.getElementById('densidad').selectedIndex = 1;
        return;

    }
}

function CambiaTipoAnalisis() {
    var vector = document.getElementsByName("unidad");

    for (i = 0; i < vector.length; i++) {
        if (vector[i].value == '1') {
            document.getElementById('densidad').selectedIndex = 2;
            return;
        }
    }
}

function SolicitudCarga(_valor) {
    if (_valor == '') {
        location.href = 'solicitud03_detalle.php?acc=I';
        return;
    } else {
        //
        var parametros = {
            'buscar': 1,
            'solicitud': _valor,
            'lab': 3,
        };

        $.ajax({
            data: parametros,
            url: __SHELL__,
            type: 'post',
            beforeSend: function () {
                ALFA('Por favor espere....');
            },
            success: function (_response) {
                switch (_response) {
                    case '0':
                        OMEGA('La solicitud digitada no existe o no se encuentra aprobada');
                        $('#solicitud').val('');
                        break;
                    case '1':
                        BETA();
                        if (!confirm("Recargar usando solicitud " + _valor)) return;
                        location.href = 'solicitud03_detalle.php?acc=R&ID=' + _valor;
                        return;
                    default:
                    case '-1':
                        alert('Error en el env�o de par�metros [Err:-1]');
                        break;
                        break;
                }
            }
        });
        //
    }
}

function datos() {
    CambiaTipoAnalisis();

    if ($('#proposito').val() == '') {
        OMEGA('Debe indicar un prop�sito del an�lisis');
        return;
    }

    if ($('#dependencia').val() == '') {
        OMEGA('Debe indicar la dependencia');
        return;
    }

    if ($('#cliente').val() == '') {
        OMEGA('Debe seleccionar el cliente');
        return;
    }

    if ($('#direccion').val() == '') {
        OMEGA('Debe indicar la direcci�n');
        return;
    }

    if (Mal(2, $('#entregadas').val())) {
        OMEGA('Debe indicar el nombre de la persona que entrega la muestra');
        return;
    }

    var hay = 0;
    //INGREDIENTES ACTIVOS/ELEMENTOS
    var control1 = document.getElementsByName('codigoB');
    var control2 = document.getElementsByName('rango');
    var control3 = document.getElementsByName('unidad');
    var control4 = document.getElementsByName('elementos');
    var control5 = document.getElementsByName('quela');

    for (i = 0; i < control1.length; i++) {
        if (!Mal(1, control1[i].value)) {
            if (Mal(1, control2[i].value)) {
                OMEGA('Debe indicar la conc. declarada');
                return;
            }

            //SI ES FERTILIZANTES
            if ($('#tipo').val() == '1') {
                //SI NO ES NITROGENO DEBE PONER EL TIPO
                var str = control4[i].value;
                if (str.indexOf("Nitr�geno") == -1) {
                    if (Mal(1, control5[i].value)) {
                        OMEGA('Debe indicar el tipo en la l�nea: ' + (i + 1));
                        return;
                    }
                }
            }

            if (Mal(1, control3[i].value)) {
                OMEGA('Debe indicar la unidad');
                return;
            }

            hay++;
        }
    }

    //IMPUREZAS
    var control1 = document.getElementsByName('impurezas');
    for (i = 0; i < control1.length; i++) {
        if (!Mal(1, control1[i].value)) hay++;
    }

    if (hay < 1) {
        OMEGA('No ha indicado ning�n an�lisis');
        return;
    }
    //
    if (Mal(2, $('#producto').val())) {
        OMEGA('Debe indicar el producto');
        return;
    }

    if (Mal(1, $('#tipo_form').val())) {
        OMEGA('Debe indicar el tipo de formulaci�n');
        return;
    }

    if (Mal(1, $('#metodo').val())) {
        OMEGA('Debe indicar si el m�todo es suministrado');
        return;
    }

    if ($('#tipo').val() == '1') {
        if (Mal(1, $('#densidad').val())) {
            OMEGA('Debe indicar si requiere densidad');
            return;
        }

        if (Mal(1, $('#mezcla').val())) {
            OMEGA('Debe indicar el tipo de mezcla');
            return;
        }
    }
    //MUESTRAS
    var control1 = document.getElementsByName('cod_ext');
    var control2 = document.getElementsByName('rechazada');
    var control3 = document.getElementsByName('obse');
    var hay = 0;

    for (i = 0; i < control1.length; i++) {
        if (!Mal(1, control1[i].value)) {
            if (control2[i].value == '1' && Mal(1, control3[i].value)) {
                OMEGA('Debe indicar por qu� rechaza la muestra en observaciones');
                return;
            }
            hay++;
        }
    }

    if (hay < 1) {
        OMEGA('No ha indicado ninguna muestra');
        return;
    }
    //
    if (!confirm('Modificar datos?')) return;

    var parametros = {
        '_AJAX': 1,
        'cs': $('#ID').val(),
        'accion': $('#accion').val(),
        'solicitud': $('#solicitud').val(),
        'tipo': $('#tipo').val(),
        'proposito': $('#proposito').val(),
        'dependencia': $('#dependencia').val(),
        'obs': $('#obs').val(),
        'cliente': $('#cliente').val(),
        'direccion': $('#direccion').val(),
        'entregadas': $('#entregadas').val(),
        'producto': $('#producto').val(),
        'tipo_form': $('#tipo_form').val(),
        'metodo': $('#metodo').val(),
        'registro': $('#registro').val(),
        //'dosis' : $('#dosis').val(),
        'mezcla': $('#mezcla').val(),
        'densidad': $('#densidad').val(),
        //ELEMENTOS
        'elementos': vector("codigoB"),
        'rango': vector("rango"),
        'quela': vector("quela"),
        'unidad': vector("unidad"),
        'fuente': vector("fuente"),
        //IMPUREZAS
        'impurezas': vector("codigoC"),
        //MUESTRAS
        'cod_ext': vector("cod_ext"),
        'recipiente': vector("recipiente"),
        'sellado': vector("sellado"),
        'lote': vector("lote"),
        'rechazada': vector("rechazada"),
        'obse': vector("obse")
    };
    //alert(vector("codigoC")); 
    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            $('#btn').disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    alert("Transaccion finalizada");
                    opener.location.reload();
                    window.close();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function ClienteEscoge(id, nombre, direccion) {
    opener.document.getElementById('cliente').value = id;
    opener.document.getElementById('tmp').value = nombre;
    opener.document.getElementById('direccion').value = direccion;
    window.close();
}

function ClientesLista() {
    //window.showModalDialog(__SHELL__ + "?list4=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
    window.open(__SHELL__ + "?list4=1", "", "width=400,height=200,scrollbars=yes,status=no");
}

function vector(ctrl) {
    var str = "1=1";
    control = document.getElementsByName(ctrl);
    for (i = 0; i < control.length; i++) {
        str += "&" + control[i].value.replace(/&/g, '');
    }
    //alert("vector: " & ctrl &" datos:" & str);
    return str;
}

function CambiaTipo() {
    if ($('#tipo').val() == '1') {
        location.href = 'solicitud03_detalle.php?acc=I';
    } else {
        location.href = 'solicitud03P_detalle.php?acc=I';
    }
}

function ElementosLista(linea) {
    if ($('#tipo').val() == '1') var tipo = 1;
    else var tipo = 3;
    window.open(__SHELL__ + "?list2=" + linea + "&tipo=" + tipo, "", "width=400,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list2="+linea+"&tipo="+tipo, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function ImpurezasLista(linea) {
    if ($('#tipo').val() == '1') var tipo = 4;
    else var tipo = 0;
    window.open(__SHELL__ + "?list3=" + linea + "&tipo=" + tipo, "", "width=400,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list3="+linea+"&tipo="+tipo, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function ValidaExiste3(_id) {
    if (_id == '') return false;

    var control = document.getElementsByName('impurezas');
    for (i = 0; i < control.length; i++) {
        if (document.getElementById('codigoC' + i).value == _id) return true
    }
    return false;
}

function ValidaExiste2(_id) {
    if (_id == '') return false;

    var control = document.getElementsByName('elementos');
    for (i = 0; i < control.length; i++) {
        if (document.getElementById('codigoB' + i).value == _id) return true
    }
    return false;
}

function ElementosEscoge(linea, id, nombre) {
    if (opener.ValidaExiste2(id)) {
        alert('El an�lisis ya se encuentra en la solicitud');
        return;
    }
    if (nombre == 'F�sforo (P<sub>2</sub>O<sub>5</sub>)') nombre = 'F�sforo (P2O5)';
    if (nombre == 'Potasio (K<sub>2</sub>O)') nombre = 'Potasio (K2O)';
    opener.document.getElementById('codigoB' + linea).value = id;
    opener.document.getElementById('elementos' + linea).value = nombre;
    window.close();
}

function ImpurezasEscoge(linea, id, nombre) {
    if (opener.ValidaExiste3(id)) {
        alert('La impureza ya se encuentra en la solicitud');
        return;
    }
    opener.document.getElementById('codigoC' + linea).value = id;
    opener.document.getElementById('impurezas' + linea).value = nombre;
    window.close();
}

function IngredientesEliminar(linea) {
    var element = document.getElementById("trElementos" + linea);
    element.parentNode.removeChild(element);

    //Recalcula
    var trElementos = document.getElementsByName("trElementos");
    for (k = 0; k < trElementos.length; k++) {
        trElementos[k].setAttribute('id', 'trElementos' + k);
    }
    var elementos = document.getElementsByName("elementos");
    for (i = 0; i < elementos.length; i++) {
        elementos[i].setAttribute('id', 'elementos' + i);
    }
    var numLineas = document.getElementsByName("numLinea");
    for (j = 0; j < numLineas.length; j++) {
        numLineas[j].innerHTML = j + 1 + '.';
    }
    var ingredientes = document.getElementsByName("ingredienteElimina");
    for (m = 0; m < ingredientes.length; m++) {
        ingredientes[m].setAttribute('onClick', 'IngredientesEliminar(' + m + ')');
    }
}

function ElementosMas() {
    var linea = document.getElementsByName("elementos").length;
    var fila = document.createElement("tr");
    fila.setAttribute('id', 'trElementos' + linea);
    fila.setAttribute('name', 'trElementos');
    var colum = new Array(7);

    colum[0] = document.createElement("td");
    colum[1] = document.createElement("td");
    colum[2] = document.createElement("td");
    colum[3] = document.createElement("td");
    colum[4] = document.createElement("td");
    colum[5] = document.createElement("td");
    colum[6] = document.createElement("td");
    //colum[3].id = 'td_tipo'+linea;

    colum[0].innerHTML = (linea + 1) + '.';
    colum[0].setAttribute('name', 'numLinea');
    colum[1].innerHTML = '<input type="text" id="elementos' + linea + '" name="elementos" class="lista2" readonly onclick="ElementosLista(' + linea + ')" /><input type="hidden" id="codigoB' + linea + '" name="codigoB" />';
    colum[2].innerHTML = '<input type="text" name="rango" size="10" maxlength="20">';
    colum[3].innerHTML = '<select name="quela" style="width:100px"><option value="">N/A</option><option value="0">Disponible</option><option value="1">Total</option><option value="2">Quelatado</option><option value="3">No-Quelatado</option></select>';
    colum[4].innerHTML = '<select name="unidad" onchange="CambiaTipoAnalisis()"><option value="">...</option><option value="0">%m/m</option><option value="1">%m/v</option></select>';
    colum[5].innerHTML = '<input type="text" name="fuente" size="20" maxlength="30">';
    colum[6].innerHTML = '<img onclick="IngredientesEliminar(' + linea + ')" src="../../caspha-i/imagenes/del.png" title="Eliminar" class="tab2" name="ingredienteElimina"/>';

    for (i = 0; i < colum.length; i++)
        fila.appendChild(colum[i]);

    document.getElementById('lolo2').appendChild(fila);
}

function ImpurezasEliminar(linea) {
    var element = document.getElementById("trImpurezas" + linea);
    element.parentNode.removeChild(element);

    //Recalcula
    var trImpurezas = document.getElementsByName("trImpurezas");
    for (k = 0; k < trImpurezas.length; k++) {
        trImpurezas[k].setAttribute('id', 'trImpurezas' + k);
    }
    var impurezas = document.getElementsByName("impurezas");
    for (i = 0; i < impurezas.length; i++) {
        impurezas[i].setAttribute('id', 'impurezas' + i);
    }
    var impLineas = document.getElementsByName("impLinea");
    for (j = 0; j < impLineas.length; j++) {
        impLineas[j].innerHTML = j + 1 + '.';
    }
    var impurezasEliminas = document.getElementsByName("impurezasElimina");
    for (m = 0; m < impurezasEliminas.length; m++) {
        impurezasEliminas[m].setAttribute('onClick', 'ImpurezasEliminar(' + m + ')');
    }
}

function ImpurezasMas() {
    var linea = document.getElementsByName("impurezas").length;
    var fila = document.createElement("tr");
    fila.setAttribute('id', 'trImpurezas' + linea);
    fila.setAttribute('name', 'trImpurezas');
    var colum = new Array(3);

    colum[0] = document.createElement("td");
    colum[1] = document.createElement("td");
    colum[2] = document.createElement("td");

    colum[0].innerHTML = (linea + 1) + '.';
    colum[0].setAttribute('name', 'impLinea');
    colum[1].innerHTML = '<input type="text" id="impurezas' + linea + '" name="impurezas" class="lista2" readonly onclick="ImpurezasLista(' + linea + ')" /><input type="hidden" id="codigoC' + linea + '" name="codigoC" />';
    colum[2].innerHTML = '<img onclick="ImpurezasEliminar(' + linea + ')" src="../../caspha-i/imagenes/del.png" title="Eliminar" class="tab2" name="impurezasElimina"/>';

    for (i = 0; i < colum.length; i++)
        fila.appendChild(colum[i]);

    document.getElementById('lolo3').appendChild(fila);
}

function MuestrasEliminar(linea) {
    var element1 = document.getElementById("tr1Muestras" + linea);
    var element2 = document.getElementById("tr2Muestras" + linea);
    var element3 = document.getElementById("tr3Muestras" + linea);
    var element4 = document.getElementById("tr4Muestras" + linea);
    element1.parentNode.removeChild(element1);
    element2.parentNode.removeChild(element2);
    element3.parentNode.removeChild(element3);
    element4.parentNode.removeChild(element4);

    //Recalcula
    var tr1Muestras = document.getElementsByName("tr1Muestras");
    for (k = 0; k < tr1Muestras.length; k++) {
        tr1Muestras[k].setAttribute('id', 'tr1Muestras' + k);
    }
    var tr2Muestras = document.getElementsByName("tr2Muestras");
    for (k = 0; k < tr2Muestras.length; k++) {
        tr2Muestras[k].setAttribute('id', 'tr2Muestras' + k);
    }
    var tr3Muestras = document.getElementsByName("tr3Muestras");
    for (k = 0; k < tr3Muestras.length; k++) {
        tr3Muestras[k].setAttribute('id', 'tr3Muestras' + k);
    }
    var tr4Muestras = document.getElementsByName("tr4Muestras");
    for (k = 0; k < tr4Muestras.length; k++) {
        tr4Muestras[k].setAttribute('id', 'tr4Muestras' + k);
    }
    var muestrasEliminas = document.getElementsByName("muestrasElimina");
    for (m = 0; m < muestrasEliminas.length; m++) {
        muestrasEliminas[m].setAttribute('onClick', 'MuestrasEliminar(' + m + ')');
    }
}

function MuestrasMas() {
    var colum = new Array(4);
    var linea = document.getElementsByName("tr1Muestras").length;
    colum[0] = document.createElement("td");
    colum[1] = document.createElement("td");
    colum[2] = document.createElement("td");
    colum[3] = document.createElement("td");

    //FILA 1
    var fila = document.createElement("tr");
    colum[0].innerHTML = '<strong>C&oacute;digo externo:</strong>';
    colum[1].innerHTML = '<input type="text" name="cod_ext" size="15" maxlength="50" />';
    colum[2].innerHTML = '<strong>Interno:</strong>';
    colum[3].innerHTML = '--';
    for (i = 0; i < colum.length; i++) fila.appendChild(colum[i]);
    document.getElementById('lolo4').appendChild(fila);
    fila.style.background = "#CCCCCC";
    fila.setAttribute('id', 'tr1Muestras' + linea);
    fila.setAttribute('name', 'tr1Muestras');
    _fila2();
}

function _fila2() {
    var fila = document.createElement("tr");
    var colum = new Array(4);
    var linea = document.getElementsByName("tr2Muestras").length;
    colum[0] = document.createElement("td");
    colum[1] = document.createElement("td");
    colum[2] = document.createElement("td");
    colum[3] = document.createElement("td");

    colum[0].innerHTML = 'Recipiente:';
    colum[1].innerHTML = '<select id="recipiente" name="recipiente"><option value="">...</option><option value="0">Bolsa metalizada</option><option value="1">Bolsa pl&aacute;stica</option><option value="2">Frasco pl&aacute;stico</option><option value="3">Frasco de vidrio</option></select>';
    colum[2].innerHTML = 'Sellado:';
    colum[3].innerHTML = '<select id="sellado" name="sellado"><option value="0">No</option><option value="1">S&iacute;</option></select>';

    for (i = 0; i < colum.length; i++) fila.appendChild(colum[i]);
    document.getElementById('lolo4').appendChild(fila);
    fila.setAttribute('id', 'tr2Muestras' + linea);
    fila.setAttribute('name', 'tr2Muestras');
    _fila3();
}

function _fila3() {
    var fila = document.createElement("tr");
    var colum = new Array(4);
    var linea = document.getElementsByName("tr3Muestras").length;
    colum[0] = document.createElement("td");
    colum[1] = document.createElement("td");
    colum[2] = document.createElement("td");
    colum[3] = document.createElement("td");

    colum[0].innerHTML = 'Lote fabricaci&oacute;n:';
    colum[1].innerHTML = '<input type="text" name="lote" size="15" maxlength="20" />';
    colum[2].innerHTML = 'Rechazada?:';
    colum[3].innerHTML = '<select name="rechazada"><option value="0">No</option><option value="1">S&iacute;</option></select>';
    for (i = 0; i < colum.length; i++) fila.appendChild(colum[i]);
    document.getElementById('lolo4').appendChild(fila);
    fila.setAttribute('id', 'tr3Muestras' + linea);
    fila.setAttribute('name', 'tr3Muestras');
    _fila4();
}

function _fila4() {
    var fila = document.createElement("tr");
    var colum = new Array(3);
    var linea = document.getElementsByName("tr4Muestras").length;
    colum[0] = document.createElement("td");
    colum[1] = document.createElement("td");
    colum[1].setAttribute("colspan", 2, 0);
    colum[2] = document.createElement("td");

    colum[0].innerHTML = 'Observaci&oacute;n:';
    colum[1].innerHTML = '<input type="text" id="obse" name="obse" size="30" maxlength="50" />';
    colum[2].innerHTML = '<img onclick="MuestrasEliminar(' + linea + ')" src="../../caspha-i/imagenes/del.png" title="Eliminar" class="tab2" name="muestrasElimina"/>';
    for (i = 0; i < colum.length; i++) fila.appendChild(colum[i]);
    document.getElementById('lolo4').appendChild(fila);
    fila.setAttribute('id', 'tr4Muestras' + linea);
    fila.setAttribute('name', 'tr4Muestras');
}