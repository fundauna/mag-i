var _FINALIZAR = false; //PARA SABER SI POR LO MENOS HAY UN ENSAYO BUENO

function CambiarEstado(_id, _analista, _estado){
	if( ('0' != $('#UID').val()) && (_analista != $('#UID').val()) ){
		OMEGA('S�lo el analista que realiz� el<br>ensayo puede modificar su estado');
		return;
	}
	
	if(_estado==0) txt = 'Inv�lidar ensayo?';
	else txt = 'Establecer ensayo como v�lido?';
	
	if(!confirm(txt)) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 2,
		'xanalizar' : '',
		'ensayo' : _id,
		'estado' : _estado
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					location.reload();	
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Habilita(){
	_FINALIZAR = true;
	document.getElementById('btn1').disabled = false;
}

function EnsayoGenerar(_xanalizar, _id, _pagina){
	window.open("../metodos/" + _pagina + "?acc=I&xanalizar=" + _xanalizar + "&ID=&tipo="+_id,"","width=800,height=600,scrollbars=yes,status=no");
	//window.showModalDialog("../metodos/" + _pagina + "?acc=I&xanalizar=" + _xanalizar + "&ID=&tipo="+_id, this.window, "dialogWidth:800px;dialogHeight:750px;status:no;");
}

function EnsayoVer(_xanalizar, _id, _pagina, _ensayo){
	window.open("../metodos/" + _pagina + "?acc=V&xanalizar=" + _xanalizar + "&ID="+_id+"&tipo="+_ensayo,"","width=800,height=600,scrollbars=yes,status=no");
	//window.showModalDialog("../metodos/" + _pagina + "?acc=V&xanalizar=" + _xanalizar + "&ID="+_id+"&tipo=", this.window, "dialogWidth:800px;dialogHeight:750px;status:no;");
}

function ArqueoVer(_id, _nombre){
	window.open('arqueo_detalle.php?ID=' + _id + '&nombre=' + _nombre,"","width=650,height=400,scrollbars=yes,status=no");
	//window.showModalDialog('arqueo_detalle.php?ID=' + _id + '&nombre=' + _nombre, this.window, "dialogWidth:650px;dialogHeight:400px;status:no;");
}

function Regresar(){
	location.href = 'analisis_pendientes.php';
}

function Aprobar(_xanalizar){
	if(!_FINALIZAR){
		OMEGA('No hay ning&uacute;n ensayo v&aacute;lido');
		return;
	}
	
	if( parseInt($('#ok').val()) > 1 ){
		if(!confirm('Existe m�s de un ensayo v�lido.\nDesea finalizar los ensayos y\nreportar 2 tipos de m�todo?')) return;
	}else{
		if(!confirm('Finalizar ensayos a la muestra?')) return;
	}
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 1,
		'xanalizar' : _xanalizar
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					alert("Transaccion finalizada");
					Regresar();		
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}