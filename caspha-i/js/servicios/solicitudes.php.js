function Activa(_tipo){
	$('#opcion').val(_tipo);
	if(_tipo == '1'){
		$('#solicitud').val('');
		document.getElementById('solicitud').readOnly = true;
	}else{
		$('#desde').val('');
		$('#hasta').val('');
		document.getElementById('solicitud').readOnly = false;
	}
}

$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bFilter": true,
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );



function SolicitudesAgrega(){
	var tipo = $('#LAB').val(); //document.getElementById('LAB').value;
	window.open("solicitud0" + tipo + "_detalle.php?acc=I","","width=900,height=600,scrollbars=yes,status=no");
	//window.showModalDialog("solicitud0" + tipo + "_detalle.php?acc=I", this.window, "dialogWidth:900px;dialogHeight:750px;status:no;");
}

function SolicitudesModifica(_ID){
	var tipo = $('#LAB').val(); //document.getElementById('LAB').value;
	window.open("solicitud0" + tipo + "_detalle.php?acc=M&ID="+_ID,"","width=900,height=600,scrollbars=yes,status=no");
	//window.showModalDialog("solicitud0" + tipo + "_detalle.php?acc=M&ID="+_ID, this.window, "dialogWidth:900px;dialogHeight:750px;status:no;");
}

function SolicitudesImprime(_ID){
	var tipo = $('#LAB').val(); //document.getElementById('LAB').value;
	if (tipo==1)
	{
		window.open("https://app.sfe.go.cr/ws_ReporteSILAB/frmreportesolicitud?solicitud=" + _ID + "&formulario=3", "", "width='100%',height='100%',scrollbars=yes,status=no");
	}
	else{
	window.open("solicitud0" + tipo + "_imprimir.php?ID="+_ID,"","width=900,height=600,scrollbars=yes,status=no");
	}
	//window.showModalDialog("solicitud0" + tipo + "_imprimir.php?ID="+_ID, this.window, "dialogWidth:900px;dialogHeight:750px;status:no;");
}

function SolicitudesBuscar(btn){
	if($('#opcion').val()=='1'){
		if( Mal(1, $('#desde').val()) ){
			OMEGA('Debe indicar la fecha');
			return;
		}
		
		if( Mal(1, $('#hasta').val()) ){
			OMEGA('Debe indicar la fecha');
			return;
		}
	}
	
	btn.disabled = true;
	btn.form.submit();
}