var valor = label = '', indice;

function habilita(bool) {
    document.getElementById('btn_buscar').disabled = !bool;
    document.getElementById('btn_agregar').disabled = !bool;
    document.getElementById('btn_eliminar').disabled = !bool;
}

function RelacionesBuscar() {
    if ($('#matriz').val() == '') {
        OMEGA('Debe seleccionar un grupo');
        return;
    }

    var parametros = {
        '_AJAX': 1,
        'AnalitosBuscar': 1,
        'matriz': $('#matriz').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            habilita(false);
            ALFA('Buscando analitos....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                default:
                    BETA();
                    habilita(true);
                    document.getElementById('td_asignados').innerHTML = _response;
                    break;
                //default:alert(_response);break;
            }
        }
    });
}


function AnalitosAgrega() {
    if (document.getElementById('btn_agregar').disabled) return;


    //obtiene val
    var valoresCheck = [];

    $("input[type=checkbox]:checked").each(function () {
        valoresCheck.push(this.value);
    });
    var cant = valoresCheck.length;

    // obtiene nombre analito
    var valoresCheckName = [];
    $("input[type=checkbox]:checked").each(function () {
        valoresCheckName.push(this.name);
    });

    var control = document.getElementById('analitos');


    habilita(false);

    var cuantificadores = prompt("Digite el limite de Cuantificaci�n", "");

    if (isNaN(cuantificadores)) {
        alert("Debe digitar valores numericos");
    }
    var control2 = document.getElementById('matriz');

    var i = 0;
    while (i < cant) {

        var parametros = {
            '_AJAX': 1,
            'AnalitosAgrega': 1,
            'idm': $('#matriz').val(),
            'matriz': control2.options[control2.selectedIndex].getAttribute("label"),
            'ida': valoresCheck[i],
            'analito': valoresCheckName[i],
            'cuantificadores': cuantificadores

        };

        $.ajax({
            data: parametros,
            url: __SHELL__,
            type: 'post',
            beforeSend: function () {
                habilita(false);
                ALFA('Por favor espere....');
            },
            success: function (_response) {
                switch (_response) {
                    case '-0':
                        alert('Sesi�n expirada [Err:0]');
                        break;
                    case '-1':
                        alert('Error en el env�o de par�metros [Err:-1]');
                        break;
                    case '0':
                        BETA();
                        alert('El campo indicado, ya se encuentra agregado!');
                        habilita(true);
                        break;
                    case '1':
                        BETA();
                        habilita(true);
                        //alert("agregado correctamente");
                        window.location.reload();
                        /**/
                        /* var oOption = document.createElement('OPTION');
                         oOption.value = valor;
                         oOption.text = "" + "------->   " + cuantificadores;
             alert("agregado correctamente");


                         var combo = document.getElementById('asignados');
                         combo.options[combo.options.length] = new Option(oOption.text, oOption.value,);

                         /**/
                        break;
                    default:
                        alert(_response);
                        break;


                }
            }
        });
        i++;
    }
}

function AnalitosElimina() {
    var control = document.getElementById('asignados');
    if (control.selectedIndex == -1) {
        OMEGA('Seleccione un adicional asignado');
        return;
    }

    habilita(false);
    indice = control.selectedIndex;
    valor = control.options[control.selectedIndex].value;


    var parametros = {
        '_AJAX': 1,
        'AnalitosElimina': 1,
        'matriz': $('#matriz').val(),
        'ida': control.options[control.selectedIndex].value
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            habilita(false);
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '1':
                    BETA();
                    habilita(true);
                    /**/
                    document.getElementById('asignados').remove(indice);
                    habilita(true);
                    /**/
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });


    /*  function AnalitosAgregar() {
          if (document.getElementById('btn_agregar').disabled) return;

          let valoresCheck = [];
          var nombre;
          $("input[type=checkbox]:checked").each(function () {
              valoresCheck.push(this.value);
          });
          var cant = valoresCheck.length;


          var control = document.getElementById('analitos');

          if (control.selectedIndex == -1) {
              OMEGA('Seleccione un analito');
              return;
          }
          habilita(false);

          var cuantificadores = prompt("Digite el limite de Cuantificadores", "");

          if (isNaN(cuantificadores)) {
              alert("Debe digitar valores numericos");
          }

          valor = control.options[control.selectedIndex].value;
          label = control.options[control.selectedIndex].text;

          var control2 = document.getElementById('matriz');


          var parametros = {
              '_AJAX': 1,
              'AnalitosAgrega': 1,
              'idm': $('#matriz').val(),
              'matriz': control2.options[control2.selectedIndex].getAttribute("label"),
              'ida': valor,
              'analito': label,
              'cuantificadores': cuantificadores
          };

          $.ajax({
              data: parametros,
              url: __SHELL__,
              type: 'post',
              beforeSend: function () {
                  habilita(false);
                  ALFA('Por favor espere....');
              },
              success: function (_response) {
                  switch (_response) {
                      case '-0':
                          alert('Sesi�n expirada [Err:0]');
                          break;
                      case '-1':
                          alert('Error en el env�o de par�metros [Err:-1]');
                          break;
                      case '0':
                          BETA();
                          alert('El campo indicado, ya se encuentra agregado!');
                          habilita(true);
                          break;
                      case '1':
                          BETA();
                          habilita(true);
                          /**/
    /*  var oOption = document.createElement('OPTION');
      oOption.value = valor;
      oOption.text = label + "------->   " + cuantificadores;


      var combo = document.getElementById('asignados');
      combo.options[combo.options.length] = new Option(oOption.text, oOption.value,);

      /**/
    /*    break;
    default:
        alert(_response);
        break;
}
}
});
}*/

}