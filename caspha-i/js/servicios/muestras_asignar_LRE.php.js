$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bFilter": true,
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function MuestraReasigna(id, linea){
	if( Mal(1, $('#Rusuario'+linea).val()) ){
		OMEGA('Debe seleccionar un analista');
		return;
	}
        var cambios = false;
        if($('#Rusuario'+linea).val() == $('#actual'+linea).val())
            cambios = true;
        if(!($('#Rusuario2'+linea).val() == $('#actual2'+linea).val()))
            cambios = false;
        if(cambios){
            alert('Debe escoger un analista diferente');
            return;
        }
	
	if(!confirm('Reasignar muestra?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 2,
		'muestra' : id,
		'analista' : $('#Rusuario'+linea).val(),
                'analista2' : $('#Rusuario2'+linea).val() || 'NULL'
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					BETA();
					document.getElementById('Rtmp'+linea).disabled = true;
                                        document.getElementById('Rtmp2'+linea).disabled = true;
					document.getElementById('RimgA'+linea).style.display = 'none';
					document.getElementById('RimgB'+linea).style.display = '';		
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function MuestraAsigna(id, linea){
	if( Mal(1, $('#usuario'+linea).val()) ){
		OMEGA('Debe seleccionar un analista');
		return;
	}
        if( $('#usuario'+linea).val() == $('#usuario2'+linea).val()){
		OMEGA('Debe seleccionar un analista 2 distinto al analista principal');
		return;
	}
	
	if(!confirm('Asignar analista?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 1,
		'muestra' : id,
		'analista' : $('#usuario'+linea).val(),
                'analista2' : $('#usuario2'+linea).val() || 'NULL'
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					BETA();
					document.getElementById('tmp'+linea).disabled = true;
                                        document.getElementById('tmp2'+linea).disabled = true;
					document.getElementById('imgA'+linea).style.display = 'none';
					document.getElementById('imgB'+linea).style.display = '';		
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function SolicitudesImprime(_ID, _tipo, _ref){
	window.open("solicitud0" + _tipo + "_imprimir.php?acc=M&ID=" + _ID + "&ref=" + _ref,"","width=900,height=600,scrollbars=yes,status=no");
	//window.showModalDialog("solicitud0" + _tipo + "_imprimir.php?acc=M&ID=" + _ID + "&ref=" + _ref, this.window, "dialogWidth:900px;dialogHeight:750px;status:no;");
}

function UsuariosLista(muestra, linea, tipo, campo, campo2){
	$('#_TIPO').val(tipo);
	window.open(__SHELL__ + "?list=" + linea + "&muestra=" + muestra + "&campo=" + campo + "&campo2=" + campo2,"","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=" + linea + "&muestra=" + muestra, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function UsuarioEscoge(id, nombre, linea, campo, campo2){
	opener.document.getElementById(campo2+linea).value=id;
	opener.document.getElementById(campo+linea).value=nombre;
	window.close();
}