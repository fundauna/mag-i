function TiposBuscar(btn){
	btn.form.submit();
}

function desmarca(){
	document.getElementById('add').disabled = false;
	document.getElementById('mod').disabled = true;
	document.getElementById('del').disabled = true;
	document.getElementById('new').disabled = true;
	document.getElementById('id').value = '';
	document.getElementById('nombre').value = '';
	//document.getElementById('tipo').value = '';
	document.getElementById('tipoCroma').value = 0;
}

function marca(control){
	if(control.selectedIndex!= -1){
		document.getElementById('id').value = control.options[control.selectedIndex].getAttribute('id');
		document.getElementById('nombre').value = control.options[control.selectedIndex].getAttribute('nombre');
		var selectedName = control.options[control.selectedIndex].getAttribute('nombre');
		//VALIDA QUE NO PUEDA MODIFICAR NOMBRES RESERVADOR COMO Ingrediente Activo, Impurezas
		if( selectedName == 'Ingrediente Activo' || selectedName == 'Impurezas' ){
			document.getElementById('mod').disabled = true;
			document.getElementById('del').disabled = true;
		}else{
			document.getElementById('mod').disabled = false;
			document.getElementById('del').disabled = false;
		}
		document.getElementById('add').disabled = true;
		document.getElementById('new').disabled = false;
	}
}

function marca1(control){
	if(control.selectedIndex!= -1){
		document.getElementById('id').value = control.options[control.selectedIndex].getAttribute('id');
		document.getElementById('nombre').value = control.options[control.selectedIndex].getAttribute('nombre');
		document.getElementById('tipoCroma').value = control.options[control.selectedIndex].getAttribute('tipoCroma');
		var selectedName = control.options[control.selectedIndex].getAttribute('nombre');
		//VALIDA QUE NO PUEDA MODIFICAR NOMBRES RESERVADOR COMO Ingrediente Activo, Impurezas
		if( selectedName == 'Ingrediente Activo' || selectedName == 'Impurezas' ){
			document.getElementById('mod').disabled = true;
			document.getElementById('del').disabled = true;
		}else{
			document.getElementById('mod').disabled = false;
			document.getElementById('del').disabled = false;
		}
		document.getElementById('add').disabled = true;
		document.getElementById('new').disabled = false;
	}
}






function modificar(accion){
	var tipo1 = $('#tipo').val();

	if( Mal(1, $('#nombre').val()) ){
		OMEGA('Debe digitar el nombre');
		return;
	}

	if((tipo1 == 8) && $('#tipoCroma').val() == null){
		OMEGA('Debe elegir la t�cnica');
		return;
	}

	//VALIDA QUE NO PUEDA AGREGAR NOMBRES RESERVADOR COMO Ingrediente Activo, Impurezas
	if(accion=='I' || accion=='M'){
		var temp =  $('#nombre').val();
		temp = temp.replace(/ /g, ''); 
		if(temp == 'IngredienteActivo' || temp == 'Impurezas'){
			OMEGA('No puede ingresar nombres como: Ingrediente Activo, Impurezas');
			return;
		}
	}
	
	if(confirm('Modificar Datos?')){

	if(tipo1=8){
		var parametros = {
			'_AJAX' : 1,
			'id' : $('#id').val(),
			'nombre' : $('#nombre').val(),
			'tipo' : $('#tipo').val(),
			'tipoCroma' : $('#tipoCroma').val(),
			'accion' : accion
		};
	} else {

		var parametros = {
			'_AJAX': 1,
			'id': $('#id').val(),
			'nombre': $('#nombre').val(),
			'tipo': $('#tipo').val(),
			'accion': accion
		};
	}
		$.ajax({
			data:  parametros,
			url:   __SHELL__,
			type:  'post',
			beforeSend: function () {
				document.getElementById('add').disabled = true;
				document.getElementById('mod').disabled = true;
				document.getElementById('del').disabled = true;
				document.getElementById('new').disabled = true;	
				ALFA('Por favor espere....');
			},
			success: function (_response) {
				switch(_response){
					case '-0':alert('Sesi�n expirada [Err:0]');break;
					case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
					case '0':
						OMEGA('Error transaccional');
						document.getElementById('add').disabled = false;
						break;
					case '1':
						GAMA('Transaccion finalizada');
						document.getElementById('add').disabled = false;
						document.getElementById('nombre').value = '';
						document.getElementById('tipoCroma').value = 0;
						break;
					default:alert('Tiempo de espera agotado');break;
				}
			}
		});
	}
}