$(document).ready(function () {
    oTable = $('#example').dataTable({
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "sScrollX": "100%",
        "bScrollCollapse": true,
        "bFilter": true,
        "oLanguage": {
            "sEmptyTable": "No hay datos que mostrar",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sLoadingRecords": "Cargando...",
            "sProcessing": "Procesando...",
            "sSearch": "Buscar:",
            "sZeroRecords": "No hay datos que mostrar",
            "sInfoFiltered": "(filtro de _MAX_ registros)",
            "sInfoEmpty": "Mostrando 0 registros",
            "oPaginate": {
                "sFirst": "Primer",
                "sLast": "Fin",
                "sNext": "Sig.",
                "sPrevious": "Prev."
            }
        }
    });
});

function MuestraReasigna(id, linea) {
//	if( Mal(1, $('#Rusuario'+linea).val()) ){
//		OMEGA('Debe seleccionar un analista');
//		return;
//	}
    //var cambios = false;
//        if($('#Rusuario'+linea).val() == $('#actual'+linea).val())
//            cambios = true;
//        if(!($('#Rusuario2'+linea).val() == $('#actual2'+linea).val()))
//            cambios = false;
//        if(cambios){
//            alert('Debe escoger un analista diferente');
//            return;
//        }
    if (!confirm('Reasignar muestra?')) {
        return;
    }

    var parametros = {
        '_AJAX': 1,
        'paso': 2,
        'muestra': id,
        'analista': $('#Rusuario1' + linea).val() != '' ? $('#Rusuario1' + linea).val() : null,
        'analista2': $('#Rusuario2' + linea).val() != '' ? $('#Rusuario2' + linea).val() : null,
        'analista3': $('#Rusuario3' + linea).val() != '' ? $('#Rusuario3' + linea).val() : null,
        'analista4': $('#Rusuario4' + linea).val() != '' ? $('#Rusuario4' + linea).val() : null
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            BETA();
            switch (_response) {
                case '-0':
                    alert('Sesion expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el envio de parametros [Err:-1]');
                    break;
                case '-2':
                    alert('Error no lleno todos los espacios');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    break;
                case '1':
                    BETA();
                    document.getElementById('Rtmp' + linea).disabled = true;
                    document.getElementById('Rtmp2' + linea).disabled = true;
                    document.getElementById('Rtmp3' + linea).disabled = true;
                    document.getElementById('Rtmp4' + linea).disabled = true;
                    document.getElementById('RimgA' + linea).style.display = 'none';
                    document.getElementById('RimgB' + linea).style.display = '';
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function MuestraAsigna(id, linea) {

    if (!confirm('Asignar analistas?')) {
        return;
    }

    var parametros = {
        '_AJAX': 1,
        'paso': 1,
        'muestra': id,
        'analista': $('#usuario' + linea).val() != '' ? $('#usuario' + linea).val() : null,
        'analista2': $('#usuario2' + linea).val() != '' ? $('#usuario2' + linea).val() : null,
        'analista3': $('#usuario3' + linea).val() != '' ? $('#usuario3' + linea).val() : null,
        'analista4': $('#usuario4' + linea).val() != '' ? $('#usuario4' + linea).val() : null
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            BETA();
            switch (_response) {
                case '-0':
                    alert('Sesion expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el envio de parametros [Err:-1]');
                    break;
                case '-2':
                    alert('Error no lleno todos los espacios');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    break;
                case '1':
                    BETA();
                    document.getElementById('tmp' + linea).disabled = true;
                    document.getElementById('tmp2' + linea).disabled = true;
                    document.getElementById('tmp3' + linea).disabled = true;
                    document.getElementById('tmp4' + linea).disabled = true;
                    document.getElementById('imgA' + linea).style.display = 'none';
                    document.getElementById('imgB' + linea).style.display = '';
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function SolicitudesImprime(_ID, _tipo, _ref) {
    window.open("https://app.sfe.go.cr/ws_ReporteSILAB/frmreportesolicitud?solicitud=" + _ID + "&formulario=3", "", "width='100%',height='100%',scrollbars=yes,status=no");
    //window.open("solicitud0" + _tipo + "_imprimir.php?acc=M&ID=" + _ID + "&ref=" + _ref, "", "width=900,height=600,scrollbars=yes,status=no");
    //window.showModalDialog("solicitud0" + _tipo + "_imprimir.php?acc=M&ID=" + _ID + "&ref=" + _ref, this.window, "dialogWidth:900px;dialogHeight:750px;status:no;");
}

function UsuariosLista(muestra, linea, tipo, campo, campo2) {
    $('#_TIPO').val(tipo);
    window.open(__SHELL__ + "?list=" + linea + "&muestra=" + muestra + "&campo=" + campo + "&campo2=" + campo2, "", "width=400,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list=" + linea + "&muestra=" + muestra, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function UsuarioEscoge(id, nombre, linea, campo, campo2) {
    opener.document.getElementById(campo2 + linea).value = id;
    opener.document.getElementById(campo + linea).value = nombre;
    window.close();
}