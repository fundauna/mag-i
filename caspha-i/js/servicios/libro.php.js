$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"sScrollX": "100%",
		/*"sScrollXInner": "110%",*/
		"bScrollCollapse": true,
		"bFilter": true,
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function SolicitudesBuscar(btn){
	if( Mal(1, $('#dato').val()) ){
		if( Mal(1, $('#desde').val()) || Mal(1, $('#hasta').val()) ){
			OMEGA('Debe indicar la fecha');
			return;
		}
	}
	
	btn.disabled = true;
	btn.form.submit();
}

function MuestraHistorial(_id, _solicitud){
	window.open("libro_historial.php?ID=" + _id + "&solicitud=" + _solicitud,"","width=610,height=400,scrollbars=yes,status=no");
	//window.showModalDialog("libro_historial.php?ID=" + _id + "&solicitud=" + _solicitud, this.window, "dialogWidth:610px;dialogHeight:400px;status:no;");
}

function BarCode(_id){
	window.open("barcode.php?ID=" + _id,"","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog("barcode.php?ID=" + _id, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}