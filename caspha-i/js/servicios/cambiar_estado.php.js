$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"sScrollX": "100%",
		/*"sScrollXInner": "110%",*/
		"bScrollCollapse": true,
		"bFilter": true,
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function Marca(_ctrl){
	var lolo = document.getElementsByName('seleccionados');
	for(i=0;i<lolo.length;i++){
		lolo[i].checked = _ctrl.checked;
	}
}

function Cambiar(btn){
	var lolo = document.getElementsByName('seleccionados');
	var ok = false;
	
	for(i=0;i<lolo.length;i++){
		if(lolo[i].checked){
			ok = true;
			break;
		}
	}
	
	if(!ok){
		OMEGA('No ha seleccionado muestras');
		return;
	}else if(!confirm("Cambiar estado de las muestras seleccionadas?")) return;
	
	var parametros = {
		'_AJAX' : 1,
		'seleccionados' : vector("seleccionados")
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					alert("Transaccion finalizada");
					location.reload();			
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function vector(ctrl){
	var str = "1=1";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		if(control[i].checked)
		str += "&" + control[i].value.replace(/&/g, ''); 
	}
	return str;
}

function MuestraHistorial(_id, _solicitud){
	window.open("libro_historial.php?ID=" + _id + "&solicitud=" + _solicitud,"","width=610,height=400,scrollbars=yes,status=no");
	//window.showModalDialog("libro_historial.php?ID=" + _id + "&solicitud=" + _solicitud, this.window, "dialogWidth:610px;dialogHeight:400px;status:no;");
}