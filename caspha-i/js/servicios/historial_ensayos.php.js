$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"sScrollX": "100%",
		/*"sScrollXInner": "110%",*/
		"bScrollCollapse": true,
		"bFilter": true,
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function SolicitudesBuscar(btn){
	if( Mal(1, $('#dato').val()) ){
		OMEGA('Debe indicar la solicitud');
		return;
	}
	
	btn.disabled = true;
	btn.form.submit();
}

function EnsayoVer(_xanalizar, _id, _tipo, _pagina){
	window.open("../metodos/" + _pagina + "?acc=V&xanalizar=" + _xanalizar + "&ID="+_id+"&tipo="+_tipo,"","width=800,height=600,scrollbars=yes,status=no");
	//window.showModalDialog("../metodos/" + _pagina + "?acc=V&xanalizar=" + _xanalizar + "&ID="+_id+"&tipo="+_tipo, this.window, "dialogWidth:800px;dialogHeight:750px;status:no;");
}