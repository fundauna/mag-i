/*
document.getElementById('pto1_ind_asc').value = '0.5000';
document.getElementById('pto2_ind_asc').value = '49.9991';
document.getElementById('pto3_ind_asc').value = '99.9975';
document.getElementById('pto4_ind_asc').value = '199.9969';
//
document.getElementById('pto1_ind_dsc').value = '0.5000';
document.getElementById('pto2_ind_dsc').value = '49.9990';
document.getElementById('pto3_ind_dsc').value = '99.9973';
document.getElementById('pto4_ind_dsc').value = '199.9969';
//
document.getElementById('valA0').value = '0.4999';
document.getElementById('valA1').value = '0.4999';
document.getElementById('valA2').value = '0.4998';
document.getElementById('valA3').value = '0.5000';
document.getElementById('valA4').value = '0.5000';
document.getElementById('valA5').value = '0.5000';
document.getElementById('valA6').value = '0.4999';
document.getElementById('valA7').value = '0.5000';
document.getElementById('valA8').value = '0.5000';
document.getElementById('valA9').value = '0.5000';
//
document.getElementById('valB0').value = '49.9991';
document.getElementById('valB1').value = '49.9990';
document.getElementById('valB2').value = '49.9990';
document.getElementById('valB3').value = '49.9990';
document.getElementById('valB4').value = '49.9990';
document.getElementById('valB5').value = '49.9990';
document.getElementById('valB6').value = '49.9990';
document.getElementById('valB7').value = '49.9990';
document.getElementById('valB8').value = '49.9991';
document.getElementById('valB9').value = '49.9990';
//
document.getElementById('valC0').value = '99.9974';
document.getElementById('valC1').value = '99.9974';
document.getElementById('valC2').value = '99.9974';
document.getElementById('valC3').value = '99.9974';
document.getElementById('valC4').value = '99.9974';
document.getElementById('valC5').value = '99.9974';
document.getElementById('valC6').value = '99.9974';
document.getElementById('valC7').value = '99.9974';
document.getElementById('valC8').value = '99.9974';
document.getElementById('valC9').value = '99.9974';
//
document.getElementById('valD0').value = '199.9971';
document.getElementById('valD1').value = '199.9970';
document.getElementById('valD2').value = '199.9970';
document.getElementById('valD3').value = '199.9969';
document.getElementById('valD4').value = '199.9970';
document.getElementById('valD5').value = '199.9969';
document.getElementById('valD6').value = '199.9969';
document.getElementById('valD7').value = '199.9971';
document.getElementById('valD8').value = '199.9969';
document.getElementById('valD9').value = '199.9969';
//
document.getElementById('sensibilidad').value = '0.0003';
document.getElementById('pos0').value = '99.9970';
document.getElementById('pos1').value = '99.9975';
document.getElementById('pos2').value = '99.9974';
document.getElementById('pos3').value = '99.9973';
document.getElementById('pos4').value = '99.9975';
document.getElementById('pos5').value = '99.9982';
*/

function Redondear(txt, tipo){
	var decimales;

	if(tipo==0)
		decimales = 4;
	else
		decimales = parseInt(document.getElementById('decimales' + tipo).value);
	
	_RED(txt, decimales);
	CalculaTotal();
}

function CalculaTotal(){
	document.getElementById('pto1').innerHTML=document.getElementById('serie1').innerHTML=document.getElementById('nominal1').value;
	document.getElementById('pto2').innerHTML=document.getElementById('serie2').innerHTML=document.getElementById('nominal2').value;
	document.getElementById('pto3').innerHTML=document.getElementById('serie3').innerHTML=document.getElementById('nominal3').value;
	document.getElementById('pto4').innerHTML=document.getElementById('serie4').innerHTML=document.getElementById('nominal4').value;
	//
	document.getElementById('td_linealidad').innerHTML = document.getElementById('linealidad').value;
	document.getElementById('rep1').innerHTML = document.getElementById('repetibilidad').value;
	document.getElementById('rep2').innerHTML = document.getElementById('repetibilidad').value;
	document.getElementById('rep3').innerHTML = document.getElementById('repetibilidad').value;
	document.getElementById('rep4').innerHTML = document.getElementById('repetibilidad').value;
	//error ascendente
	document.getElementById('pto1_err_asc').value = _RED2(document.getElementById('real1').value - document.getElementById('pto1_ind_asc').value, 7);
	document.getElementById('pto2_err_asc').value = _RED2(document.getElementById('real2').value - document.getElementById('pto2_ind_asc').value, 7);
	document.getElementById('pto3_err_asc').value = _RED2(document.getElementById('real3').value - document.getElementById('pto3_ind_asc').value, 7);
	document.getElementById('pto4_err_asc').value = _RED2(document.getElementById('real4').value - document.getElementById('pto4_ind_asc').value, 7);
	//error descendente
	document.getElementById('pto1_err_dsc').value = _RED2(document.getElementById('real1').value - document.getElementById('pto1_ind_dsc').value, 7);
	document.getElementById('pto2_err_dsc').value = _RED2(document.getElementById('real2').value - document.getElementById('pto2_ind_dsc').value, 7);
	document.getElementById('pto3_err_dsc').value = _RED2(document.getElementById('real3').value - document.getElementById('pto3_ind_dsc').value, 7);
	document.getElementById('pto4_err_dsc').value = _RED2(document.getElementById('real4').value - document.getElementById('pto4_ind_dsc').value, 7);
	//
	var txt='';
	if(Math.abs(document.getElementById('pto1_err_asc').value) <= document.getElementById('linealidad').value) txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S�'; else txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No'; document.getElementById('pto1_si_asc').innerHTML = txt;
	if(Math.abs(document.getElementById('pto2_err_asc').value) <= document.getElementById('linealidad').value) txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S�'; else txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No'; document.getElementById('pto2_si_asc').innerHTML = txt;
	if(Math.abs(document.getElementById('pto3_err_asc').value) <= document.getElementById('linealidad').value) txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S�'; else txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No'; document.getElementById('pto3_si_asc').innerHTML = txt;
	if(Math.abs(document.getElementById('pto4_err_asc').value) <= document.getElementById('linealidad').value) txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S�'; else txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No'; document.getElementById('pto4_si_asc').innerHTML = txt;
	//
	var txt='';
	if(Math.abs(document.getElementById('pto1_err_dsc').value) <= document.getElementById('linealidad').value) txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S�'; else txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No'; document.getElementById('pto1_si_dsc').innerHTML = txt;
	if(Math.abs(document.getElementById('pto2_err_dsc').value) <= document.getElementById('linealidad').value) txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S�'; else txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No'; document.getElementById('pto2_si_dsc').innerHTML = txt;
	if(Math.abs(document.getElementById('pto3_err_dsc').value) <= document.getElementById('linealidad').value) txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S�'; else txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No'; document.getElementById('pto3_si_dsc').innerHTML = txt;
	if(Math.abs(document.getElementById('pto4_err_dsc').value) <= document.getElementById('linealidad').value) txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S�'; else txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No'; document.getElementById('pto4_si_dsc').innerHTML = txt;
	//DESVIACIONES ESTANDAR
	var vectorA = document.getElementsByName("valA");
	var vectorB = document.getElementsByName("valB");
	var vectorC = document.getElementsByName("valC");
	var vectorD = document.getElementsByName("valD");
	var sumA = sumB = sumC = sumD = 0;
	var fVarianceA = fVarianceB = fVarianceC = fVarianceD = 0;
	
	for(i=0;i<vectorA.length;i++){
		sumA += parseFloat(vectorA[i].value);
		sumB += parseFloat(vectorB[i].value);
		sumC += parseFloat(vectorC[i].value);
		sumD += parseFloat(vectorD[i].value);
	}
	
	var fMeanA = sumA / vectorA.length
	var fMeanB = sumB / vectorB.length 
	var fMeanC = sumC / vectorC.length
	var fMeanD = sumD / vectorD.length
	
	for(i=0;i<vectorA.length;i++){
		 fVarianceA += parseFloat(Math.pow(vectorA[i].value - fMeanA, 2));
		 fVarianceB += parseFloat(Math.pow(vectorB[i].value - fMeanB, 2));
		 fVarianceC += parseFloat(Math.pow(vectorC[i].value - fMeanC, 2));
		 fVarianceD += parseFloat(Math.pow(vectorD[i].value - fMeanD, 2));
	}
	
	document.getElementById('desv1').innerHTML = _RED2(Math.sqrt(fVarianceA)/Math.sqrt(vectorA.length-1), 7);    
	document.getElementById('desv2').innerHTML = _RED2(Math.sqrt(fVarianceB)/Math.sqrt(vectorB.length-1), 7);    
	document.getElementById('desv3').innerHTML = _RED2(Math.sqrt(fVarianceC)/Math.sqrt(vectorC.length-1), 7);    
	document.getElementById('desv4').innerHTML = _RED2(Math.sqrt(fVarianceD)/Math.sqrt(vectorD.length-1), 7);    
	//
	if(Math.abs(document.getElementById('desv1').innerHTML) <= document.getElementById('repetibilidad').value) txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S�'; else txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No'; document.getElementById('esta1').innerHTML = txt;
	if(Math.abs(document.getElementById('desv2').innerHTML) <= document.getElementById('repetibilidad').value) txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S�'; else txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No'; document.getElementById('esta2').innerHTML = txt;
	if(Math.abs(document.getElementById('desv3').innerHTML) <= document.getElementById('repetibilidad').value) txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S�'; else txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No'; document.getElementById('esta3').innerHTML = txt;
	if(Math.abs(document.getElementById('desv4').innerHTML) <= document.getElementById('repetibilidad').value) txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S�'; else txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No'; document.getElementById('esta4').innerHTML = txt;
	/////////PESAJE
	// X_
	document.getElementById('pos6').innerHTML = _RED2((parseFloat(document.getElementById('pos0').value) + parseFloat(document.getElementById('pos5').value)) / 2, 7);
	//
	document.getElementById('lectura1').innerHTML = _RED2(document.getElementById('pos1').value - document.getElementById('pos6').innerHTML, 7);
	document.getElementById('lectura2').innerHTML = _RED2(document.getElementById('pos2').value - document.getElementById('pos6').innerHTML, 7);
	document.getElementById('lectura3').innerHTML = _RED2(document.getElementById('pos3').value - document.getElementById('pos6').innerHTML, 7);
	document.getElementById('lectura4').innerHTML = _RED2(document.getElementById('pos4').value - document.getElementById('pos6').innerHTML, 7);
	//MAYOR
	var mayor = Math.abs(parseFloat(document.getElementById('lectura1').innerHTML));
	if( mayor < Math.abs(parseFloat(document.getElementById('lectura2').innerHTML)) ) mayor = Math.abs(parseFloat(document.getElementById('lectura2').innerHTML));
	if( mayor < Math.abs(parseFloat(document.getElementById('lectura3').innerHTML)) ) mayor = Math.abs(parseFloat(document.getElementById('lectura3').innerHTML));
	if( mayor < Math.abs(parseFloat(document.getElementById('lectura4').innerHTML)) ) mayor = Math.abs(parseFloat(document.getElementById('lectura4').innerHTML));
	document.getElementById('mayor').innerHTML = mayor;
	//
	if(mayor <= document.getElementById('sensibilidad').value) txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S�'; else txt = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No'; document.getElementById('esta').innerHTML = txt;

}

function datos(){
	if($('#cod_bal').val()==''){
		OMEGA('Debe seleccionar la balanza');
		return;
	}
        
	if($('#linealidad').val()=='0'){
		OMEGA('Debe indicar la linealidad');
		return;
	}
	
	if($('#repetibilidad').val()=='0'){
		OMEGA('Debe indicar la repetibilidad');
		return;
	}
	
	if( Mal(2, $('#rango').val()) ){
		OMEGA('Debe indicar rango de la pesada');
		return;
	}
	
	if($('#decimalesA').val()==''){OMEGA('Debe indicar los decimales de la pesa 1');return;}
	if($('#decimalesB').val()==''){OMEGA('Debe indicar los decimales de la pesa 2');return;}
	if($('#decimalesC').val()==''){OMEGA('Debe indicar los decimales de la pesa 3');return;}
	if($('#decimalesD').val()==''){OMEGA('Debe indicar los decimales de la pesa 4');return;}
	//
	if($('#pesa1').val()==''){OMEGA('Debe seleccionar la masa 1');return;}
	if($('#pesa2').val()==''){OMEGA('Debe seleccionar la masa 2');return;}
	if($('#pesa3').val()==''){OMEGA('Debe seleccionar la masa 3');return;}
	if($('#pesa4').val()==''){OMEGA('Debe seleccionar la masa 4');return;}
	//
	if($('#pesa5').val()==''){OMEGA('Debe seleccionar la pesa para excentricidad');return;}
	
	if($('#sensibilidad').val()=='0'){
		OMEGA('Debe indicar el EMP en excentricidad');
		return;
	}
	//
	if($('#pos0').val()==''){OMEGA('Debe indicar la carga para la posici�n 0');return;}
	if($('#pos1').val()==''){OMEGA('Debe indicar la carga para la posici�n 1');return;}
	if($('#pos2').val()==''){OMEGA('Debe indicar la carga para la posici�n 2');return;}
	if($('#pos3').val()==''){OMEGA('Debe indicar la carga para la posici�n 3');return;}
	if($('#pos4').val()==''){OMEGA('Debe indicar la carga para la posici�n 4');return;}
	if($('#pos5').val()==''){OMEGA('Debe indicar la carga para la posici�n 0');return;}
	//
	if(!confirm('Modificar datos?')) return;

	var parametros = {
		'_AJAX' : 1,
		'paso' : 1,
		'cs' : $('#cs').val(),
		'accion' : $('#accion').val(),
		'tipo_bal' : $('#tipo_bal').val(),
		'cod_bal' : $('#cod_bal').val(),
		'linealidad' : $('#linealidad').val(),
		'repetibilidad' : $('#repetibilidad').val(),
		'rango' : $('#rango').val(),
		'decimalesA' : $("#decimalesA").val(),
		'decimalesB' : $("#decimalesB").val(),
		'decimalesC' : $("#decimalesC").val(),
		'decimalesD' : $("#decimalesD").val(),
		'cert1' : $('#cert1').val(),
		'cert2' : $('#cert2').val(),
		'cert3' : $('#cert3').val(),
		'cert4' : $('#cert4').val(),
		'pesa1' : $('#pesa1').val(),
		'pesa2' : $('#pesa2').val(),
		'pesa3' : $('#pesa3').val(),
		'pesa4' : $('#pesa4').val(),
		'nominal1' : $('#nominal1').val(),		
		'nominal2' : $('#nominal2').val(),
		'nominal3' : $('#nominal3').val(),
		'nominal4' : $('#nominal4').val(),
		'real1' : $('#real1').val(),
		'real2' : $('#real2').val(),
		'real3' : $('#real3').val(),
		'real4' : $('#real4').val(),
		'inc1' : $('#inc1').val(),
		'inc2' : $('#inc2').val(),
		'inc3' : $('#inc3').val(),
		'inc4' : $('#inc4').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '':
					alert('Tiempo de espera agotado');break;
					break;
				default:
					__Paso2(_response);
					break;
			}
		}
	});
}

/*FUNCIONES PRIVADAS*/
function __Paso2(_cs){
	var parametros = {
		'_AJAX' : 1,
		'paso' : 2,
		'cs' : _cs,
		'obs' : $("#obs").val(),
		//
		'pto1_ind_asc' : $("#pto1_ind_asc").val(),
		'pto2_ind_asc' : $("#pto2_ind_asc").val(),
		'pto3_ind_asc' : $("#pto3_ind_asc").val(),
		'pto4_ind_asc' : $("#pto4_ind_asc").val(),
		//
		'pto1_ind_dsc' : $('#pto1_ind_dsc').val(),
		'pto2_ind_dsc' : $('#pto2_ind_dsc').val(),
		'pto3_ind_dsc' : $('#pto3_ind_dsc').val(),
		'pto4_ind_dsc' : $('#pto4_ind_dsc').val(),
		//
		'platillo' : $('#platillo').val(),
		'pesa5' : $('#pesa5').val(),
		'nominal5' : $('#nominal5').val(),
		'real5' : $('#real5').val(),
		'sensibilidad' : $('#sensibilidad').val(),
		//
		'pos0' : $('#pos0').val(),
		'pos1' : $('#pos1').val(),
		'pos2' : $('#pos2').val(),
		'pos3' : $('#pos3').val(),
		'pos4' : $('#pos4').val(),
		'pos5' : $('#pos5').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '':
					alert('Tiempo de espera agotado');break;
					break;
				default:
					__Paso3(_response);
					break;
			}
		}
	});
}

function __Paso3(_cs){
	var parametros = {
		'_AJAX' : 1,
		'paso' : 3,
		'cs' : _cs,
		//
		'valA' : vector("valA"),
		'valB' : vector("valB"),
		'valC' : vector("valC"),
		'valD' : vector("valD"),
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					location.href = 'carta1_detalle.php?acc=M&ID=' + _cs;
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}
/*FUNCIONES PRIVADAS*/

function SolicitudAgregar(){
	var linea = document.getElementsByName("valA").length;
	var fila = document.createElement("tr");
	var colum = new Array(5);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	colum[4] = document.createElement("td");
	
	colum[0].innerHTML = '&nbsp;&nbsp;&nbsp;' + (linea + 1);
	colum[1].innerHTML = '<input type="text" id="valA'+linea+'" name="valA" value="0" class="monto" onblur="Redondear(this, 0);">';
	colum[2].innerHTML = '<input type="text" id="valB'+linea+'" name="valB" value="0" class="monto" onblur="Redondear(this, 0);">';
	colum[3].innerHTML = '<input type="text" id="valC'+linea+'" name="valC" value="0" class="monto" onblur="Redondear(this, 0);">';
	colum[4].innerHTML = '<input type="text" id="valD'+linea+'" name="valD" value="0" class="monto" onblur="Redondear(this, 0);">';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	
	document.getElementById('lolo').appendChild(fila);
}

function vector(ctrl){
	var str = "1=1";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		//if(control[i].value != ""){
			str += "&" + control[i].value.replace(/&/g, ''); 
		//}
	}
	return str;
}

function DisponibleEscoge(id, codigo, nombre, linea){
	opener.document.getElementById('cod_bal').value=id;
	opener.document.getElementById('nom_bal').value=codigo;
	opener.document.getElementById('marca').innerHTML=nombre;
	opener.CalculaTotal();
	window.close();
}

function EquipoEscoge(cs, codigo, marca, modelo){
	opener.document.getElementById('cod_bal').value=cs;
	opener.document.getElementById('nom_bal').value=codigo;
	opener.document.getElementById('marca').innerHTML = ''+marca+' / '+modelo;
	window.close();
}

function EquiposLista(){
	window.open(__SHELL__ + "?list=1","","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function PesaEscoge(codigo, nominal, vreal, inc, certificado, linea){
	opener.document.getElementById('pesa' + linea).value=codigo;
	opener.document.getElementById('nominal' + linea).value=nominal;
	opener.document.getElementById('real' + linea).value=vreal;
	
	if(linea != 5){
		opener.document.getElementById('cert' + linea).value=certificado;	
		opener.document.getElementById('inc' + linea).value=inc;
	}
	opener.CalculaTotal();
	window.close();
}

function PesasLista(_linea){
	window.open(__SHELL__ + "?list2=" + _linea,"","width=600,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list2=" + _linea, this.window, "dialogWidth:600px;dialogHeight:200px;status:no;");
}

function Procesar(tipo){
	if(!confirm('Modificar estado?')) return;

	var parametros = {
		'_AJAX' : 1,
		'paso' : 9,
		'cs' : $('#cs').val(),
		'accion' : tipo
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					location.reload();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}