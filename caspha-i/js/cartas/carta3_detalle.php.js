/*
document.getElementById('ph41').value = '3.999';
document.getElementById('ph42').value = '4,005';
document.getElementById('ph43').value = '4,003';
document.getElementById('ph44').value = '4,003';
document.getElementById('ph45').value = '4,002';
document.getElementById('ph46').value = '4,007';
document.getElementById('ph47').value = '3.992';
document.getElementById('ph48').value = '3.992';
document.getElementById('ph49').value = '3.998';
document.getElementById('ph410').value = '3.999';
//
document.getElementById('ph71').value = '7.000';
document.getElementById('ph72').value = '6.996';
document.getElementById('ph73').value = '6.996';
document.getElementById('ph74').value = '6.996';
document.getElementById('ph75').value = '6.996';
document.getElementById('ph76').value = '6.996';
document.getElementById('ph77').value = '6.995';
document.getElementById('ph78').value = '6.999';
document.getElementById('ph79').value = '7.007';
document.getElementById('ph710').value = '7.004';
//
document.getElementById('ph101').value = '10.009';
document.getElementById('ph102').value = '10.011';
document.getElementById('ph103').value = '10.012';
document.getElementById('ph104').value = '10.011';
document.getElementById('ph105').value = '10.018';
document.getElementById('ph106').value = '10.012';
document.getElementById('ph107').value = '10.012';
document.getElementById('ph108').value = '10.015';
document.getElementById('ph109').value = '10.006';
document.getElementById('ph1010').value = '10.019';
//
document.getElementById('phcond1').value = '13.11';
document.getElementById('phcond2').value = '13.11';
document.getElementById('phcond3').value = '13.11';
document.getElementById('phcond4').value = '12.93';
document.getElementById('phcond5').value = '13.08';
document.getElementById('phcond6').value = '13.09';
document.getElementById('phcond7').value = '13.10';
document.getElementById('phcond8').value = '13.10';
document.getElementById('phcond9').value = '13.11';
document.getElementById('phcond10').value = '13.11';
//
CalculaTotal();
*/
function CalculaTotal(){
	var sumA = sumB = sumC = sumD = 0;
	var fVarianceA = fVarianceB = fVarianceC = fVarianceD = 0;
	var desA = desB = desC = desD = 0;
	
	for(i=1;i<=10;i++){
		_RED(document.getElementById('ph4'+i), 3);
		_RED(document.getElementById('ph7'+i), 3);
		_RED(document.getElementById('ph10'+i), 3);
		_RED(document.getElementById('phcond'+i), 2);
		
		sumA += parseFloat(document.getElementById('ph4'+i).value);
		sumB += parseFloat(document.getElementById('ph7'+i).value);
		sumC += parseFloat(document.getElementById('ph10'+i).value);
		sumD += parseFloat(document.getElementById('phcond'+i).value);
	}
	
	var fMeanA = sumA / 10;
	var fMeanB = sumB / 10;
	var fMeanC = sumC / 10;
	var fMeanD = sumD / 10;
	
	document.getElementById('promph4').value = _RED2(fMeanA,3);
	document.getElementById('promph7').value = _RED2(fMeanB,3);
	document.getElementById('promph10').value = _RED2(fMeanC,3);
	document.getElementById('promphcond').value = _RED2(fMeanD,3);
	//
	for(i=1;i<=10;i++){
		 fVarianceA += parseFloat(Math.pow(document.getElementById('ph4'+i).value - fMeanA, 2));
		 fVarianceB += parseFloat(Math.pow(document.getElementById('ph7'+i).value - fMeanB, 2));
		 fVarianceC += parseFloat(Math.pow(document.getElementById('ph10'+i).value - fMeanC, 2));
		 fVarianceD += parseFloat(Math.pow(document.getElementById('phcond'+i).value - fMeanD, 2));
	}
	//
	desA = Math.sqrt(fVarianceA)/Math.sqrt(9);
	desB = Math.sqrt(fVarianceB)/Math.sqrt(9);
	desC = Math.sqrt(fVarianceC)/Math.sqrt(9);
	desD = Math.sqrt(fVarianceD)/Math.sqrt(9);
	document.getElementById('desvph4').value = _RED2(desA, 3);    
	document.getElementById('desvph7').value = _RED2(desB, 3);    
	document.getElementById('desvph10').value = _RED2(desC, 3); 
	document.getElementById('desvphcond').value = _RED2(desD, 3); 
	//
	document.getElementById('coevarph4').value = _RED2( (desA/fMeanA), 4);    
	document.getElementById('coevarph7').value = _RED2( (desB/fMeanB), 4);    
	document.getElementById('coevarph10').value = _RED2( (desC/fMeanC), 4);   
	document.getElementById('coevarphcond').value = _RED2( (desD/fMeanD), 4);   
	Limites();
}

function Limites(){
	_RED(document.getElementById('limiteph'), 2);
	_RED(document.getElementById('limitedesv'), 2);
	_RED(document.getElementById('limitecoef'), 2);
	
	if(Math.abs(4-document.getElementById('promph4').value) <= document.getElementById('limiteph').value) document.getElementById('lectph4').value = 'Si';
	else document.getElementById('lectph4').value = 'No';
	if(Math.abs(7-document.getElementById('promph7').value) <= document.getElementById('limiteph').value) document.getElementById('lectph7').value = 'Si';
	else document.getElementById('lectph7').value = 'No';
	if(Math.abs(10-document.getElementById('promph10').value) <= document.getElementById('limiteph').value) document.getElementById('lectph10').value = 'Si';
	else document.getElementById('lectph10').value = 'No';
	//
	if(parseFloat(document.getElementById('desvph4').value) <= parseFloat(document.getElementById('limitedesv').value) ) document.getElementById('desvest4').value = 'Si';
	else document.getElementById('desvest4').value = 'No';
	if(parseFloat(document.getElementById('desvph7').value) <= parseFloat(document.getElementById('limitedesv').value) ) document.getElementById('desvest7').value = 'Si';
	else document.getElementById('desvest7').value = 'No';
	if(parseFloat(document.getElementById('desvph10').value) <= parseFloat(document.getElementById('limitedesv').value) ) document.getElementById('desvest10').value = 'Si';
	else document.getElementById('desvest10').value = 'No';
	//
	if(parseFloat(document.getElementById('coevarph4').value) <= parseFloat(document.getElementById('limitecoef').value)) document.getElementById('coeest4').value = 'Si';
	else document.getElementById('coeest4').value = 'No';
	if(parseFloat(document.getElementById('coevarph7').value) <= parseFloat(document.getElementById('limitecoef').value)) document.getElementById('coeest7').value = 'Si';
	else document.getElementById('coeest7').value = 'No';
	if(parseFloat(document.getElementById('coevarph10').value) <= parseFloat(document.getElementById('limitecoef').value)) document.getElementById('coeest10').value = 'Si';
	else document.getElementById('coeest10').value = 'No';
	
	if(document.getElementById('promph4').value == '0')
		document.getElementById('lectph4').value = '';
	if(document.getElementById('promph7').value == '0')
		document.getElementById('lectph7').value = '';
	if(document.getElementById('promph10').value == '0')
		document.getElementById('lectph10').value = '';
	if(document.getElementById('desvph4').value == '0')
		document.getElementById('desvest4').value = '';
	if(document.getElementById('desvph7').value == '0')
		document.getElementById('desvest7').value = '';
	if(document.getElementById('desvph10').value == '0')
		document.getElementById('desvest10').value = '';
	if(document.getElementById('coevarph4').value == '0')
		document.getElementById('coeest4').value = '';
	if(document.getElementById('coevarph7').value == '0')
		document.getElementById('coeest7').value = '';
	if(document.getElementById('coevarph10').value == '0')
		document.getElementById('coeest10').value = '';
	//
	_RED(document.getElementById('pendiente'), 4);
	_RED(document.getElementById('desviacion'), 4);
}

function datos(){	
	if($('#fechavence0').val()==''){
		OMEGA('Debe indicar la fecha de �ltima calibraci�n');
		return;
	}
        
        if( Mal(1, $('#fecha').val()) ){
		OMEGA('Debe seleccionar la fecha');
		return;
	}
		
	if($('#equipo').val()==''){
		OMEGA('Debe seleccionar el equipo');
		return;
	}
	
	if($('#fechavence1').val()=='' || $('#fechavence2').val()=='' || $('#fechavence3').val()=='' || $('#fechavence4').val()==''){
		OMEGA('Debe indicar las fechas');
		return;
	}
	
	if($('#lote1').val()=='' || $('#lote2').val()=='' || $('#lote3').val()=='' || $('#lote4').val()==''){
		OMEGA('Debe indicar los lotes');
		return;
	}
	
	if($('#certificado1').val()=='' || $('#certificado2').val()=='' || $('#certificado3').val()=='' || $('#certificado4').val()==''){
		OMEGA('Debe indicar los certificados');
		return;
	}
		
	if($('#lecturaC').val()==''){
		OMEGA('Debe indicar si cumple la conductividad');
		return;
	}
	
	if(!confirm('Modificar datos?')) return;

	var parametros = {
		'_AJAX' : 1,
		'paso' : 1,
		'cs' : $('#cs').val(),
		'accion' : $('#accion').val(),
                'fecha' : $('#fecha').val(),
		'observaciones' : $('#observaciones').val(),
		'equipo' : $('#equipo').val(),
		'fechavence0' : $('#fechavence0').val(),
		'fechavence1' : $('#fechavence1').val(),
		'fechavence2' : $('#fechavence2').val(),
		'fechavence3' : $('#fechavence3').val(),
		'fechavence4' : $('#fechavence4').val(),
		'lote1' : $('#lote1').val(),
		'lote2' : $('#lote2').val(),
		'lote3' : $('#lote3').val(),
		'lote4' : $('#lote4').val(),
		'certificado1' : $('#certificado1').val(),
		'certificado2' : $('#certificado2').val(),
		'certificado3' : $("#certificado3").val(),
		'certificado4' : $("#certificado4").val(),
		//
		'limiteph' : $("#limiteph").val(),
		'limitedesv' : $("#limitedesv").val(),
		'limitecoef' : $("#limitecoef").val(),
		'limitecond' : $("#limitecond").val(),
		//
		'lecturaC' : $("#lecturaC").val(),
		'pendiente' : $("#pendiente").val(),
		'desviacion' : $("#desviacion").val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '':
					alert('Tiempo de espera agotado');break;
					break;
				default:
					__Paso2(_response);
					break;
			}
		}
	});
}

/*FUNCIONES PRIVADAS*/
function __Paso2(_cs){
	var parametros = {
		'_AJAX' : 1,
		'paso' : 2,
		'cs' : _cs,
		'ph4' : vector("ph4"),
		'ph7' : vector("ph7"),
		'ph10' : vector("ph10"),
		'phcond' : vector("phcond")
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					location.href = 'carta3_detalle.php?acc=M&ID=' + _cs;
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}
/*FUNCIONES PRIVADAS*/

function vector(ctrl){
	var str = "1=1";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		//if(control[i].value != ""){
			str += "&" + control[i].value.replace(/&/g, ''); 
		//}
	}
	return str;
}

function DisponibleEscoge(id, codigo, nombre, linea){
	opener.document.getElementById('equipo').value=id;
	opener.document.getElementById('nomequipo').value=codigo;
	opener.document.getElementById('marca').innerHTML=nombre;
	window.close();
}

/*function EquiposLista(){
	window.open(__SHELL__ + "?list=1","","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}*/

function EquiposLista(_linea){
	window.open(__SHELL__ + "?list="+_linea,"","width=500,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list="+_linea, this.window, "dialogWidth:500px;dialogHeight:200px;status:no;");
}

function EquipoEscoge(cs, codigo, marca, modelo){
	opener.document.getElementById('equipo').value=cs;
	opener.document.getElementById('nomequipo').value=codigo;
	opener.document.getElementById('marca').innerHTML = ''+marca+' / '+modelo;
	window.close();
}

function Procesar(tipo){
	if(!confirm('Modificar estado?')) return;

	var parametros = {
		'_AJAX' : 1,
		'paso' : 9,
		'cs' : $('#cs').val(),
		'accion' : tipo
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					location.reload();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}