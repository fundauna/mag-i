/*
document.getElementById('aforado').selectedIndex = 2;

document.getElementById('area0').value = '1092.7';
document.getElementById('area1').value = '5647.7';
document.getElementById('area2').value = '11270.9';
document.getElementById('area3').value = '27233.2';
//
document.getElementById('vol_ali1').selectedIndex = 5;
document.getElementById('vol_ali2').selectedIndex = 5;

document.getElementById('vol_afo0').selectedIndex = 2;
document.getElementById('vol_afo1').selectedIndex = 3;
document.getElementById('vol_afo2').selectedIndex = 2;
//
document.getElementById('are0').value = '2794.40';
document.getElementById('are1').value = '2793.90';
document.getElementById('are2').value = '2798.65';
document.getElementById('are3').value = '2789.34';
document.getElementById('are4').value = '2794.78';
document.getElementById('are5').value = '2797.54';
document.getElementById('are6').value = '2792.37';
document.getElementById('are7').value = '2794.94';
document.getElementById('are8').value = '2788.68';
document.getElementById('are9').value = '2786.57';
document.getElementById('are10').value = '2785.75';
document.getElementById('are11').value = '2791.31';
document.getElementById('are12').value = '2786.68';
document.getElementById('are13').value = '2789.61';
document.getElementById('are14').value = '2791.90';
//
document.getElementById('tr0').value = '4.745';
document.getElementById('tr1').value = '4.745';
document.getElementById('tr2').value = '4.749';
document.getElementById('tr3').value = '4.748';
document.getElementById('tr4').value = '4.746';
document.getElementById('tr5').value = '4.741';
document.getElementById('tr6').value = '4.745';
document.getElementById('tr7').value = '4.740';
document.getElementById('tr8').value = '4.745';
document.getElementById('tr9').value = '4.745';
document.getElementById('tr10').value = '4.741';
document.getElementById('tr11').value = '4.740';
document.getElementById('tr12').value = '4.737';
document.getElementById('tr13').value = '4.736';
document.getElementById('tr14').value = '4.736';
//
document.getElementById('alt0').value = '217.09';
document.getElementById('alt1').value = '216.91';
document.getElementById('alt2').value = '217.63';
document.getElementById('alt3').value = '217.43';
document.getElementById('alt4').value = '216.73';
document.getElementById('alt5').value = '217.59';
document.getElementById('alt6').value = '216.59';
document.getElementById('alt7').value = '216.86';
document.getElementById('alt8').value = '216.90';
document.getElementById('alt9').value = '216.54';
document.getElementById('alt10').value = '217.33';
document.getElementById('alt11').value = '216.81';
document.getElementById('alt12').value = '215.51';
document.getElementById('alt13').value = '215.96';
document.getElementById('alt14').value = '215.96';
//
CalculaTotal();
*/

function Pendiente(){
	var x2=0;
	var y=0;
	var x=0;
	var xy=0;
	var cantidad = 4;
	for(i=0;i<cantidad;i++){
		  x2 += parseFloat(document.getElementById('cn'+i).innerHTML*document.getElementById('cn'+i).innerHTML);
		  y += parseFloat(document.getElementById('area'+i).value);
		  x += parseFloat(document.getElementById('cn'+i).innerHTML);
		  xy += parseFloat(document.getElementById('cn'+i).innerHTML*document.getElementById('area'+i).value);
	}
	var b = (cantidad*xy-x*y)/(cantidad*x2-x*x);
	document.getElementById('pendiente').innerHTML = _RED2(b, 4);
}

function Curvas(){
	var promP = promC = sumCN = sumAR = sumA = sumB = sumC = sumD = sumE = sumF = sumG = sumH = sumI = 0;

	Pendiente();
	
	//INTERCEPTO
	for(i=0;i<4;i++){
		promC += parseFloat(document.getElementById('cn'+i).innerHTML);
		promP += parseFloat(document.getElementById('area'+i).value);
	}
	promC /= 4;
	promP /= 4;
	var B = promP-(document.getElementById('pendiente').innerHTML*promC);
	document.getElementById('intercepto').innerHTML = _RED2(B, 4);
	//
	for(i=0;i<4;i++){
		document.getElementById('_CN'+i).innerHTML = document.getElementById('cn'+i).innerHTML;
		document.getElementById('_AR'+i).innerHTML = document.getElementById('area'+i).value;
		
		document.getElementById('_A'+i).innerHTML = Math.pow(document.getElementById('cn'+i).innerHTML, 2); 
		document.getElementById('_G'+i).innerHTML = document.getElementById('pendiente').innerHTML * document.getElementById('_CN'+i).innerHTML + parseFloat(document.getElementById('intercepto').innerHTML);
		document.getElementById('_H'+i).innerHTML = Math.abs(document.getElementById('_AR'+i).innerHTML-document.getElementById('_G'+i).innerHTML);
		document.getElementById('_I'+i).innerHTML = Math.pow(document.getElementById('_H'+i).innerHTML, 2); 
		sumA += parseFloat(document.getElementById('_A'+i).innerHTML);
		sumG += parseFloat(document.getElementById('_G'+i).innerHTML);
		sumH += parseFloat(document.getElementById('_H'+i).innerHTML);
		sumI += parseFloat(document.getElementById('_I'+i).innerHTML);
		sumCN += parseFloat(document.getElementById('cn'+i).innerHTML);
		sumAR += parseFloat(document.getElementById('_AR'+i).innerHTML);
	}
	document.getElementById('_CN'+i).innerHTML = sumCN/4;
	document.getElementById('_AR'+i).innerHTML = sumAR/4;
	document.getElementById('_A'+i).innerHTML = sumA/4;
	document.getElementById('_G'+i).innerHTML = sumG/4;
	document.getElementById('_H'+i).innerHTML = sumH/4;
	document.getElementById('_I'+i).innerHTML = sumI/4;
	document.getElementById('_CN5').innerHTML = sumCN;
	document.getElementById('_AR5').innerHTML = sumAR;
	document.getElementById('_A5').innerHTML = sumA;
	document.getElementById('_G5').innerHTML = sumG;
	document.getElementById('_H5').innerHTML = sumH;
	document.getElementById('_I5').innerHTML = sumI;
	//
	for(i=0;i<4;i++){
		document.getElementById('_B'+i).innerHTML = document.getElementById('_CN'+i).innerHTML - document.getElementById('_CN4').innerHTML;
		document.getElementById('_C'+i).innerHTML = Math.pow(document.getElementById('_B'+i).innerHTML, 2); 
		document.getElementById('_D'+i).innerHTML = _RED2(document.getElementById('_AR'+i).innerHTML - document.getElementById('_AR4').innerHTML, 5);
		document.getElementById('_E'+i).innerHTML = Math.pow(document.getElementById('_D'+i).innerHTML, 2); 
		document.getElementById('_F'+i).innerHTML = document.getElementById('_B'+i).innerHTML * document.getElementById('_D'+i).innerHTML;
		sumB += parseFloat(document.getElementById('_B'+i).innerHTML);
		sumC += parseFloat(document.getElementById('_C'+i).innerHTML);
		sumD += parseFloat(document.getElementById('_D'+i).innerHTML);
		sumE += parseFloat(document.getElementById('_E'+i).innerHTML);
		sumF += parseFloat(document.getElementById('_F'+i).innerHTML);
	}
	document.getElementById('_B'+i).innerHTML = sumB/4;
	document.getElementById('_C'+i).innerHTML = sumC/4;
	document.getElementById('_D'+i).innerHTML = sumD/4;
	document.getElementById('_E'+i).innerHTML = sumE/4;
	document.getElementById('_F'+i).innerHTML = sumF/4;
	document.getElementById('_B5').innerHTML = sumB;
	document.getElementById('_C5').innerHTML = sumC;
	document.getElementById('_D5').innerHTML = sumD;
	document.getElementById('_E5').innerHTML = sumE;
	document.getElementById('_F5').innerHTML = sumF;
	//
	document.getElementById('r').innerHTML = _RED2(document.getElementById('_F5').innerHTML / Math.sqrt(document.getElementById('_C5').innerHTML*document.getElementById('_E5').innerHTML), 6);
	var SYX = Math.sqrt(document.getElementById('_I5').innerHTML / (4-2));
	document.getElementById('sm').innerHTML = _RED2(SYX/(Math.sqrt(document.getElementById('_C5').innerHTML)), 2);
	document.getElementById('sb').innerHTML = _RED2(SYX*(Math.sqrt(document.getElementById('_A5').innerHTML/(4*document.getElementById('_C5').innerHTML))), 2);
}

function datos(){		
	if($('#cod_bal').val()==''){
		OMEGA('Debe seleccionar el equipo');
		return;
	}
	
	if($('#fecha').val()==''){
		OMEGA('Debe indicar la fecha');
		return;
	}
		
	if(!confirm('Modificar datos?')) return;

	var parametros = {
		'_AJAX' : 1,
		'paso' : 1,
		'cs' : $('#cs').val(),
		'fecha' : $('#fecha').val(),
		'accion' : $('#accion').val(),
		'nombre' : $('#nombre').val(),
		'masa' : $('#masa').val(),
		'origen' : $('#origen').val(),
		'aforado' : $('#aforado').val(),
		'tiene' : $('#tiene').val(),
		'pureza' : $('#pureza').val(),
		'cod_bal' : $('#cod_bal').val(),
		'tipo_inyector' : $('#tipo_inyector').val(),
		'inyeccion' : $('#inyeccion').val(),
		'temp1' : $('#temp').val(),
		'modo' : $('#modo').val(),
		'tipo_col' : $('#tipo_col').val(),
		'gas1' : $('#gas1').val(),
		'flujo1' : $('#flujo1').val(),
		'presion1' : $('#presion1').val(),
		'gas2' : $('#gas2').val(),
		'flujo2' : $('#flujo2').val(),
		'presion2' : $('#presion2').val(),
		'gas3' : $('#gas3').val(),
		'flujo3' : $('#flujo3').val(),
		'presion3' : $('#presion3').val(),
		'gas4' : $('#gas4').val(),
		'flujo4' : $('#flujo4').val(),
		'presion4' : $('#presion4').val(),
		'cod_col' : $('#cod_col').val(),
		'marcafase' : $('#marcafase').val(),
		'fecha_em' : $('#fecha_em').val(),
		'temp2' : $('#temp2').val(),
		'tipo_detector' : $('#tipo_detector').val(),
		'dimensiones' : $('#dimensiones').val(),
		'temp3' : $('#temp3').val(),
		'otros' : $('#otros').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesion expirada [Err:0]');break;
				case '-1':alert('Error en el envio de parametros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '':
					alert('Tiempo de espera agotado');break;
					break;
				default:
					__Paso3(_response);
					break;
			}
		}
	});
}

/*FUNCIONES PRIVADAS*/
function __Paso3(_cs){
	var parametros = {
		'_AJAX' : 1,
		'paso' : 3,
		'cs' : _cs,
		//
		'vol_ali' : vector2("vol_ali"),
		'vol_afo' : vector2("vol_afo"),
		'area' : vector2("area")
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesion expirada [Err:0]');break;
				case '-1':alert('Error en el envio de parametros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '':
					alert('Tiempo de espera agotado');break;
					break;
				default:
					__Paso4(_response);
					break;
			}
		}
	});
}

function __Paso4(_cs){
	var parametros = {
		'_AJAX' : 1,
		'paso' : 4,
		'cs' : _cs,
		//
		'are' : vector2("are"),
		'tr' : vector2("tr"),
		'alt' : vector2("alt")
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesion expirada [Err:0]');break;
				case '-1':alert('Error en el envio de parametros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					location.href = 'carta8_detalle.php?acc=M&ID=' + _cs;
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}
/*FUNCIONES PRIVADAS*/

function CalculaTotal2(){
	var linea = document.getElementsByName("are").length;
	var sumA = sumB = sumC = 0;
	
	for(i=0;i<linea;i++){
		sumA += parseFloat(document.getElementById('are'+i).value);
		sumB += parseFloat(document.getElementById('tr'+i).value);
		sumC += parseFloat(document.getElementById('alt'+i).value);
	}
	var fMeanA = sumA / linea;
	var fMeanB = sumB / linea;
	var fMeanC = sumC / linea;
	var fVarianceA = fVarianceB = fVarianceC = 0;
	//
	for(i=0;i<linea;i++){
		 fVarianceA += parseFloat(Math.pow(document.getElementById('are'+i).value - fMeanA, 2));
		 fVarianceB += parseFloat(Math.pow(document.getElementById('tr'+i).value - fMeanB, 2));
		 fVarianceC += parseFloat(Math.pow(document.getElementById('alt'+i).value - fMeanC, 2));
	}
	//
	document.getElementById('prom_are').innerHTML = _RED2(fMeanA, 3);
	document.getElementById('prom_tr').innerHTML = _RED2(fMeanB, 3);
	document.getElementById('prom_alt').innerHTML = _RED2(fMeanC, 3);
	document.getElementById('desv_are').innerHTML = _RED2(Math.sqrt(fVarianceA)/Math.sqrt(linea-1), 3);
	document.getElementById('desv_tr').innerHTML = _RED2(Math.sqrt(fVarianceB)/Math.sqrt(linea-1), 3);    
	document.getElementById('desv_alt').innerHTML = _RED2(Math.sqrt(fVarianceC)/Math.sqrt(linea-1), 3); 
	document.getElementById('cv_are').innerHTML = _RED2((document.getElementById('desv_are').innerHTML / document.getElementById('prom_are').innerHTML)*100, 3); 
	document.getElementById('cv_tr').innerHTML = _RED2((document.getElementById('desv_tr').innerHTML / document.getElementById('prom_tr').innerHTML)*100, 3); 
	document.getElementById('cv_alt').innerHTML = _RED2((document.getElementById('desv_alt').innerHTML / document.getElementById('prom_alt').innerHTML)*100, 3); 
	//LIMITES
	document.getElementById('3s_are').innerHTML = _RED2(fMeanA + parseFloat((3*document.getElementById('desv_are').innerHTML)), 1);
	document.getElementById('2s_are').innerHTML = _RED2(fMeanA + parseFloat((2*document.getElementById('desv_are').innerHTML)), 1);
	document.getElementById('1s_are').innerHTML = _RED2(fMeanA + parseFloat((1*document.getElementById('desv_are').innerHTML)), 1);
	document.getElementById('3i_are').innerHTML = _RED2(fMeanA - parseFloat((3*document.getElementById('desv_are').innerHTML)), 1);
	document.getElementById('2i_are').innerHTML = _RED2(fMeanA - parseFloat((2*document.getElementById('desv_are').innerHTML)), 1);
	document.getElementById('1i_are').innerHTML = _RED2(fMeanA - parseFloat((1*document.getElementById('desv_are').innerHTML)), 1);
	//
	document.getElementById('3s_tr').innerHTML = _RED2(fMeanB + parseFloat((3*document.getElementById('desv_tr').innerHTML)), 3);
	document.getElementById('2s_tr').innerHTML = _RED2(fMeanB + parseFloat((2*document.getElementById('desv_tr').innerHTML)), 3);
	document.getElementById('1s_tr').innerHTML = _RED2(fMeanB + parseFloat((1*document.getElementById('desv_tr').innerHTML)), 3);
	document.getElementById('3i_tr').innerHTML = _RED2(fMeanB - parseFloat((3*document.getElementById('desv_tr').innerHTML)), 3);
	document.getElementById('2i_tr').innerHTML = _RED2(fMeanB - parseFloat((2*document.getElementById('desv_tr').innerHTML)), 3);
	document.getElementById('1i_tr').innerHTML = _RED2(fMeanB - parseFloat((1*document.getElementById('desv_tr').innerHTML)), 3);
	//
	document.getElementById('3s_alt').innerHTML = _RED2(fMeanC + parseFloat((3*document.getElementById('desv_alt').innerHTML)), 2);
	document.getElementById('2s_alt').innerHTML = _RED2(fMeanC + parseFloat((2*document.getElementById('desv_alt').innerHTML)), 2);
	document.getElementById('1s_alt').innerHTML = _RED2(fMeanC + parseFloat((1*document.getElementById('desv_alt').innerHTML)), 2);
	document.getElementById('3i_alt').innerHTML = _RED2(fMeanC - parseFloat((3*document.getElementById('desv_alt').innerHTML)), 2);
	document.getElementById('2i_alt').innerHTML = _RED2(fMeanC - parseFloat((2*document.getElementById('desv_alt').innerHTML)), 2);
	document.getElementById('1i_alt').innerHTML = _RED2(fMeanC - parseFloat((1*document.getElementById('desv_alt').innerHTML)), 2);
	//
	Curvas();
}

function inicio(){
	SolicitudAgregar2();SolicitudAgregar2();SolicitudAgregar2();SolicitudAgregar2();SolicitudAgregar2();
	SolicitudAgregar2();SolicitudAgregar2();SolicitudAgregar2();SolicitudAgregar2();SolicitudAgregar2();
	SolicitudAgregar2();SolicitudAgregar2();SolicitudAgregar2();SolicitudAgregar2();
}

function CalculaTotal(){
	var conce = document.getElementById('masa').value * (document.getElementById('pureza').value/100)/document.getElementById('aforado').value;
	document.getElementById('conce').innerHTML = _RED2(conce, 4);
	//LINEALIDAD
	linea = document.getElementsByName("vol_ali").length;
	var cn;
	for(i=0;i<linea-1;i++){
		cn = conce * (document.getElementById('vol_ali'+i).value/document.getElementById('vol_afo'+i).value);
		document.getElementById('cn'+i).innerHTML = _RED2(cn, 5);
	}
	document.getElementById('cn'+i).innerHTML = document.getElementById('conce').innerHTML;

	CalculaTotal2();
}

function Redondear(txt, decimales){
	_RED(txt, decimales);
	CalculaTotal();
}

function Vector(tipo){
	var linea = document.getElementsByName("are").length;
	var str = '';
	
	for(i=0;i<linea;i++){
		str += ';' + document.getElementById(tipo+i).value;
	}
	return str;
}

function vector2(ctrl){
	var str = "1=1";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		str += "&" + control[i].value.replace(/&/g, ''); 
	}
	return str;
}

function Generar(){	
	GenerarA();
	GenerarB();
	GenerarC();
	GenerarD();
	GenerarA();
}

function GenerarA(){	
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 5,
		'tipo' : $('#graf_pesa').val(),
		'x0' : document.getElementById('cn0').innerHTML,
		'x1' : document.getElementById('cn1').innerHTML,
		'x2' : document.getElementById('cn2').innerHTML,
		'x3' : document.getElementById('cn3').innerHTML,
		'y0' : $('#area0').val(),
		'y1' : $('#area1').val(),
		'y2' : $('#area2').val(),
		'y3' : $('#area3').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			//ALFA('Por favor espere....');
		},
		success: function (_response){
			if(_response == ''){
				OMEGA("No hay datos que mostrar");
				return;
			}
			BETA();
			document.getElementById('td_tablaA').innerHTML = _response;
			GeneraGraficoA();
			return;
		}
	});
}

function GenerarB(){	
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 5,
		'tipo' : $('#graf_pesa1').val(),
		'x' : Vector('are')
	}	
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			//ALFA('Por favor espere....');
		},
		success: function (_response){
			if(_response == ''){
				OMEGA("No hay datos que mostrar");
				return;
			}
			BETA();
			document.getElementById('td_tablaB').innerHTML = _response;
			GeneraGraficoB();
			return;
		}
	});
}

function GenerarC(){	
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 5,
		'tipo' : $('#graf_pesa2').val(),
		'x' : Vector('tr')
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			//ALFA('Por favor espere....');
		},
		success: function (_response){
			if(_response == ''){
				OMEGA("No hay datos que mostrar");
				return;
			}
			BETA();
			document.getElementById('td_tablaC').innerHTML = _response;
			GeneraGraficoC();
			return;
		}
	});
}

function GenerarD(){	
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 5,
		'tipo' : $('#graf_pesa3').val(),
		'x' : Vector('alt')
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			//ALFA('Por favor espere....');
		},
		success: function (_response){
			if(_response == ''){
				OMEGA("No hay datos que mostrar");
				return;
			}
			BETA();
			document.getElementById('td_tablaD').innerHTML = _response;
			GeneraGraficoD();
			return;
		}
	});
}

function GeneraGraficoA(){
	document.getElementById('tr_graficoA').style.display = '';	
	
	titulo = 'Curva de calibraci�n';
	labely = 'Area';
	labelx = 'Concentraci�n (mg/mL)';
	ticks = null;
	var media = 0;
	var limite1 = 0;
	var limite2 = 0;
	var limite3 = 0;
	var limite4 = 0;
	var limite5 = 0;
	var limite6 = 0;

	$(function () {
		$('#containerA').highcharts({
			legend: {
            	enabled: false
       		},
			data: {
				table: document.getElementById('tablaA')
			},
			title: {
                text: titulo,
                x: -20 //center
            },
            yAxis: {
                title: {
                    text: labely
                }				
            },
			xAxis: {
                title: {
                    text: labelx
                },
				gridLineWidth: 1,
				tickInterval: ticks
            },
          	tooltip: {
				enabled: true
			}
		});
	});
}

function GeneraGraficoB(){
	document.getElementById('tr_graficoB').style.display = '';
	
	titulo = 'Gr�fico de control del �rea';
	labely = 'Area';
	labelx = '# de corrida';
	ticks = 1;
	//
	var media = _RED2(document.getElementById('prom_are').innerHTML, 2);
	var limite1 = _RED2(document.getElementById('3s_are').innerHTML, 2);
	var limite2 = _RED2(document.getElementById('2s_are').innerHTML, 2);
	var limite3 = _RED2(document.getElementById('1s_are').innerHTML, 2);
	var limite4 = _RED2(document.getElementById('1i_are').innerHTML, 2);
	var limite5 = _RED2(document.getElementById('2i_are').innerHTML, 2);
	var limite6 = _RED2(document.getElementById('3i_are').innerHTML, 2);

	$(function () {
		$('#containerB').highcharts({
			legend: {
            	enabled: false
       		},
			data: {
				table: document.getElementById('tablaB')
			},
			title: {
                text: titulo,
                x: -20 //center
            },
            yAxis: {
				min: limite6,     
   				max: limite1,
                title: {
                    text: labely
                },
				plotLines: [
					{color: '#0066FF',width: 2,value: media},
					{color: '#FF0000',width: 2,value: limite1},
					{color: '#FFFF00',width: 2,value: limite2},
					{color: '#00FF00',width: 2,value: limite3},
					{color: '#00FF00',width: 2,value: limite4},
					{color: '#FFFF00',width: 2,value: limite5},
					{color: '#FF0000',width: 2,value: limite6}
				]
            },
			xAxis: {
                title: {
                    text: labelx
                },
				gridLineWidth: 1,
				tickInterval: ticks
            },
          	tooltip: {
				enabled: true
			}
		});
	});
}

function GeneraGraficoC(){
	document.getElementById('tr_graficoC').style.display = '';	
	
	titulo = 'Gr�fico de control del tiempo de retenci�n';
	labely = 'TR';
	labelx = '# de corrida';
	ticks = 1;
	//
	var media = _RED2(document.getElementById('prom_tr').innerHTML, 3);
	var limite1 = _RED2(document.getElementById('3s_tr').innerHTML, 3);
	var limite2 = _RED2(document.getElementById('2s_tr').innerHTML, 3);
	var limite3 = _RED2(document.getElementById('1s_tr').innerHTML, 3);
	var limite4 = _RED2(document.getElementById('1i_tr').innerHTML, 3);
	var limite5 = _RED2(document.getElementById('2i_tr').innerHTML, 3);
	var limite6 = _RED2(document.getElementById('3i_tr').innerHTML, 3);
	$(function () {
		$('#containerC').highcharts({
			legend: {
            	enabled: false
       		},
			data: {
				table: document.getElementById('tablaC')
			},
			title: {
                text: titulo,
                x: -20 //center
            },
            yAxis: {
				min: limite6,     
   				max: limite1,
                title: {
                    text: labely
                },
				plotLines: [
					{color: '#0066FF',width: 2,value: media},
					{color: '#FF0000',width: 2,value: limite1},
					{color: '#FFFF00',width: 2,value: limite2},
					{color: '#00FF00',width: 2,value: limite3},
					{color: '#00FF00',width: 2,value: limite4},
					{color: '#FFFF00',width: 2,value: limite5},
					{color: '#FF0000',width: 2,value: limite6}
				]
            },
			xAxis: {
                title: {
                    text: labelx
                },
				gridLineWidth: 1,
				tickInterval: ticks
            },
          	tooltip: {
				enabled: true
			}
		});
	});
}

function GeneraGraficoD(){
	document.getElementById('tr_graficoD').style.display = '';	
	
	titulo = 'Gr�fico de control de la altura';
	labely = 'Altura';
	labelx = '# de corrida';
	ticks = 1;
	//
	var media = _RED2(document.getElementById('prom_alt').innerHTML, 2);
	var limite1 = _RED2(document.getElementById('3s_alt').innerHTML, 2);
	var limite2 = _RED2(document.getElementById('2s_alt').innerHTML, 2);
	var limite3 = _RED2(document.getElementById('1s_alt').innerHTML, 2);
	var limite4 = _RED2(document.getElementById('1i_alt').innerHTML, 2);
	var limite5 = _RED2(document.getElementById('2i_alt').innerHTML, 2);
	var limite6 = _RED2(document.getElementById('3i_alt').innerHTML, 2);

	$(function () {
		$('#containerD').highcharts({
			legend: {
            	enabled: false
       		},
			data: {
				table: document.getElementById('tablaD')
			},
			title: {
                text: titulo,
                x: -20 //center
            },
            yAxis: {
				min: limite6,     
   				max: limite1,
                title: {
                    text: labely
                },
				plotLines: [
					{color: '#0066FF',width: 2,value: media},
					{color: '#FF0000',width: 2,value: limite1},
					{color: '#FFFF00',width: 2,value: limite2},
					{color: '#00FF00',width: 2,value: limite3},
					{color: '#00FF00',width: 2,value: limite4},
					{color: '#FFFF00',width: 2,value: limite5},
					{color: '#FF0000',width: 2,value: limite6}
				]
            },
			xAxis: {
                title: {
                    text: labelx
                },
				gridLineWidth: 1,
				tickInterval: ticks
            },
          	tooltip: {
				enabled: true
			}
		});
	});
}

function SolicitudAgregar2(){
	var linea = document.getElementsByName("are").length;
	var fila = document.createElement("tr");
	var colum = new Array(4);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	
	colum[0].innerHTML = '&nbsp;&nbsp;&nbsp;' + (linea + 1);
	colum[1].innerHTML = '<input type="text" id="are'+linea+'" name="are" class="monto" value="0" onblur="Redondear(this, 2);" />';
	colum[2].innerHTML = '<input type="text" id="tr'+linea+'" name="tr" class="monto" value="0" onblur="Redondear(this, 3);" />';
	colum[3].innerHTML = '<input type="text" id="alt'+linea+'" name="alt" class="monto" value="0" onblur="Redondear(this, 2);" />';
	
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]);
	
	document.getElementById('lolo2').appendChild(fila);
}

function DisponibleEscoge(id, codigo, nombre, linea, marca, modelo){
	opener.document.getElementById('cod_bal').value=id;
	opener.document.getElementById('nom_bal').value=codigo;
	opener.document.getElementById('marca').innerHTML=marca+' / '+modelo;
	window.close();
}

function EquiposLista(){
	window.open(__SHELL__ + "?list=1","","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function ColumnasEscoge(id, codigo, nombre, marca, dimensiones){
	opener.document.getElementById('cod_col').value = codigo;
	opener.document.getElementById('marcafase').value = marca;
	opener.document.getElementById('dimensiones').innerHTML = dimensiones;
	window.close();
}

function ColumnaLista(){
	window.open(__SHELL__ + "?list=2","","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function Procesar(tipo){
	if(!confirm('Modificar estado?')) return;

	var parametros = {
		'_AJAX' : 1,
		'paso' : 9,
		'cs' : $('#cs').val(),
		'accion' : tipo
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesion expirada [Err:0]');break;
				case '-1':alert('Error en el envio de parametros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					location.reload();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}