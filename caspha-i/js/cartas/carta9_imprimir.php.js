function Buscar(num){
	var parametros = {
		'_VER' : num,
		'id' : $('#id').val(),
		'acc' : $('#acc').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		success: function (_response){
			document.getElementById('t'+num).innerHTML = _response;
			//
			if(num==2){
				Cumple1();
				Cumple2();
				Cumple3();
				Cumple4();
				Cumple5();
				Cumple6();
				Cumple7();
			}else if(num==3){
				Cumple8();
			}else if(num==4){
				Redondear(1);
				Redondear(2);
				Redondear(3);
				Cumple9();
				GeneraDatosTipoA(1, 'tr', 1);
				GeneraDatosTipoA(2, 'area', 1);
				GeneraDatosTipoA(3, 'tr', 2);
				GeneraDatosTipoA(4, 'area', 2);
				GeneraDatosTipoA(5, 'tr', 3);
				GeneraDatosTipoA(6, 'area', 3);
			}else if(num==5){
				GeneraDatosTipoB(7);
			}
			return;
		}
	});
}

function Vector(ctrl){
	var str = "1=1";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		str += "&" + control[i].value.replace(/&/g, ''); 
	}
	return str;
}

function Cumple1(){
	var a,b,c;
	a = document.getElementById('v1').value;
	b = document.getElementById('v2').value;
	c = document.getElementById('v3').value;
	if( a == '69' && b == '219' && c == '502') document.getElementById('cumple1').innerHTML = 'S�';
	else document.getElementById('cumple1').innerHTML = 'No';
}

function Cumple2(){
	var a,b,c;
	a = parseFloat(document.getElementById('v4').value);
	b = parseFloat(document.getElementById('v5').value);
	c = parseFloat(document.getElementById('v6').value);
	
	if( a >= 0.5 && a <= 0.7 && b >= 0.5 && b <= 0.7 && c >= 0.5 && c <= 0.7) document.getElementById('cumple2').innerHTML = 'S�';
	else document.getElementById('cumple2').innerHTML = 'No';
}

function Cumple3(){
	var a = parseFloat(document.getElementById('v7').value);
	if(a >= 0 && a <= 40) document.getElementById('cumple3').innerHTML = 'S�';
	else document.getElementById('cumple3').innerHTML = 'No';
}

function Cumple4(){
	var a = parseFloat(document.getElementById('v8').value);
	if(a >= 1000 && a <= 3000) document.getElementById('cumple4').innerHTML = 'S�';
	else document.getElementById('cumple4').innerHTML = 'No';
}

function Cumple5(){
	var a = document.getElementById('v9').value.replace(/,/g, '.');
	if(a == '69' || a == '219') document.getElementById('cumple5').innerHTML = 'S�';
	else document.getElementById('cumple5').innerHTML = 'No';
}

function Cumple6(){
	var a = parseFloat(document.getElementById('v10').value);
	if(a >= 400000 && a <= 600000) document.getElementById('cumple6').innerHTML = 'S�';
	else document.getElementById('cumple6').innerHTML = 'No';
}

function Cumple7(){
	var a = parseFloat(document.getElementById('v11').value);
	var b = parseFloat(document.getElementById('v12').value);
	var c = parseFloat(document.getElementById('v13').value);
	var d = parseFloat(document.getElementById('v14').value);
	var e = parseFloat(document.getElementById('v15').value);
	
	if(a >= 0.5 && a <= 1.6) document.getElementById('cumple7').innerHTML = 'S�';
	else document.getElementById('cumple7').innerHTML = 'No';
	
	if(b >= 3.2 && b <= 5.4) document.getElementById('cumple8').innerHTML = 'S�';
	else document.getElementById('cumple8').innerHTML = 'No';
	
	if(c >= 7.9 && c <= 12.3) document.getElementById('cumple9').innerHTML = 'S�';
	else document.getElementById('cumple9').innerHTML = 'No';
	
	if(d < 20) document.getElementById('cumpleA').innerHTML = 'S�';
	else document.getElementById('cumpleA').innerHTML = 'No';
	
	if(e < 10) document.getElementById('cumpleB').innerHTML = 'S�';
	else document.getElementById('cumpleB').innerHTML = 'No';
}

function Cumple8(){
	var platos_decano = parseFloat(document.getElementById('platos2').value);
	var platos_undeca = parseFloat(document.getElementById('platos4').value);
	//
	var resol_decano = parseFloat(document.getElementById('resol2').value);
	var resol_undeca = parseFloat(document.getElementById('resol4').value);
	var asime_decano = parseFloat(document.getElementById('asim2').value);
	var asime_undeca = parseFloat(document.getElementById('asim4').value);
	//
	var resol_octa = parseFloat(document.getElementById('resol3').value);
	var asime_octa = parseFloat(document.getElementById('asim3').value);
	//
	var resol_diclo = parseFloat(document.getElementById('resol11').value);
	var asime_diclo = parseFloat(document.getElementById('asim11').value);
	
	if(platos_decano > 20000 && platos_undeca > 20000) document.getElementById('cumpleC').innerHTML = 'S�';
	else document.getElementById('cumpleC').innerHTML = 'No';
	
	if( (resol_decano > 1.5 && resol_undeca > 1.5) && (asime_decano < 1.5 && asime_undeca < 1.5) ) document.getElementById('cumpleD').innerHTML = 'S�';
	else document.getElementById('cumpleD').innerHTML = 'No';
	
	if(resol_octa > 1.5 && asime_octa < 1.5) document.getElementById('cumpleE').innerHTML = 'S�';
	else document.getElementById('cumpleE').innerHTML = 'No';
	
	if(resol_diclo > 1.5 && asime_diclo < 1.5) document.getElementById('cumpleF').innerHTML = 'S�';
	else document.getElementById('cumpleF').innerHTML = 'No';
	
	if(document.getElementById('tr1').value == '0.00' && document.getElementById('resol1').value == '0.00' && document.getElementById('qual1').value == '0' && document.getElementById('asim1').value == '0.00' && document.getElementById('platos1').value == '0.00') document.getElementById('cumpleG').innerHTML = 'No';
	else document.getElementById('cumpleG').innerHTML = 'S�';	
}

function Cumple9(){
	_FLOAT(document.getElementById('vresol'));
	_FLOAT(document.getElementById('vresol2'));
	_FLOAT(document.getElementById('vasimetria'));
	_FLOAT(document.getElementById('vasimetria2'));
	document.getElementById('cumple32').style.display = 'none';
	document.getElementById('cumple33').style.display = '';
	document.getElementById('cumple34').style.display = 'none';
	document.getElementById('cumple35').style.display = '';
	
	if(document.getElementById('vresol').value.replace(/,/g, '.') > 1.5 && document.getElementById('vasimetria').value.replace(/,/g, '.') < 1.5 ){
		document.getElementById('cumple33').style.display = 'none';
		document.getElementById('cumple32').style.display = '';
	}
	if(document.getElementById('vresol2').value.replace(/,/g, '.') > 1.5 && document.getElementById('vasimetria2').value.replace(/,/g, '.') < 1.5 ){
		document.getElementById('cumple35').style.display = 'none';
		document.getElementById('cumple34').style.display = '';
	}
}

function Redondear(tipo){
	var sumA = 0, sumB = 0;
	for(i=1;i<=7;i++){
		_RED(document.getElementById('simtr'+tipo+i), 3);
		_RED(document.getElementById('simarea'+tipo+i),0);
		//
		sumA += parseFloat(document.getElementById('simtr'+tipo+i).value);
		sumB += parseFloat(document.getElementById('simarea'+tipo+i).value);
	}
	var fMeanA = sumA / 7;
	var fMeanB = sumB / 7;
	var fVarianceA = fVarianceB = 0;
	
	for(i=1;i<=7;i++){
		 fVarianceA += parseFloat(Math.pow(document.getElementById('simtr'+tipo+i).value - fMeanA, 2));
		 fVarianceB += parseFloat(Math.pow(document.getElementById('simarea'+tipo+i).value - fMeanB, 2));
	}
	
	document.getElementById('simpromtr'+tipo).value = _RED2(fMeanA, 3);
	document.getElementById('simpromarea'+tipo).value = _RED2(fMeanB, 3);  
	
	document.getElementById('simdstr'+tipo).value = _RED2(Math.sqrt(fVarianceA)/Math.sqrt(7-1), 3);
	document.getElementById('simdsarea'+tipo).value = _RED2(Math.sqrt(fVarianceB)/Math.sqrt(7-1), 3);    
	document.getElementById('simcvtr'+tipo).value = _RED2((document.getElementById('simdstr'+tipo).value / fMeanA)*100, 3); 
	document.getElementById('simcvarea'+tipo).value = _RED2((document.getElementById('simdsarea'+tipo).value / fMeanB)*100, 3); 
	//OBTIENE PUNTOS MENORES Y MAYORES
	var mayorTR = document.getElementById('simtr'+tipo+1).value;
	var menorTR = document.getElementById('simtr'+tipo+1).value;
	var mayorAR = document.getElementById('simarea'+tipo+1).value;
	var menorAR = document.getElementById('simarea'+tipo+1).value;
	for(i=1;i<=7;i++){
		if(mayorTR < document.getElementById('simtr'+tipo+i).value) mayorTR = document.getElementById('simtr'+tipo+i).value;
		if(menorTR > document.getElementById('simtr'+tipo+i).value) menorTR = document.getElementById('simtr'+tipo+i).value;
		if(mayorAR < document.getElementById('simarea'+tipo+i).value) mayorAR = document.getElementById('simarea'+tipo+i).value;
		if(menorAR > document.getElementById('simarea'+tipo+i).value) menorAR = document.getElementById('simarea'+tipo+i).value;
	}
	//
	document.getElementById('limitetrSup'+tipo).value = _RED2(parseFloat(document.getElementById('simpromtr'+tipo).value) + 2*document.getElementById('simdstr'+tipo).value,2);
	document.getElementById('limitearSup'+tipo).value = _RED2(parseFloat(document.getElementById('simpromarea'+tipo).value) + 2*document.getElementById('simdsarea'+tipo).value,2);
	document.getElementById('limitetrInf'+tipo).value = _RED2(parseFloat(document.getElementById('simpromtr'+tipo).value) - 2*document.getElementById('simdstr'+tipo).value,2);
	document.getElementById('limitearInf'+tipo).value = _RED2(parseFloat(document.getElementById('simpromarea'+tipo).value) - 2*document.getElementById('simdsarea'+tipo).value,2);
	
	var cumple = 1;
	if(document.getElementById('limitetrSup'+tipo).value < mayorTR) cumple = 0;
	if(document.getElementById('limitearSup'+tipo).value < mayorAR) cumple = 0;
	if(document.getElementById('limitetrInf'+tipo).value > menorTR) cumple = 0;
	if(document.getElementById('limitearInf'+tipo).value > menorAR) cumple = 0;
	document.getElementById('result'+tipo).value = cumple;
}

function Oculta(obj){
	document.getElementById('t'+obj).style.display = '';
	document.getElementById('f'+obj).style.display = '';
	Buscar(obj);
}

function GeneraDatosTipoA(_tipo, _elemento, _nivel){
	var parametros = {
		'_AJAX' : 1,
		'paso' : 6,
		'clase' : 'A',
		'tipo' : _tipo,
		'y0' : $('#sim'+_elemento+_nivel+'1').val(),
		'y1' : $('#sim'+_elemento+_nivel+'2').val(),
		'y2' : $('#sim'+_elemento+_nivel+'3').val(),
		'y3' : $('#sim'+_elemento+_nivel+'4').val(),
		'y4' : $('#sim'+_elemento+_nivel+'5').val(),
		'y5' : $('#sim'+_elemento+_nivel+'6').val(),
		'y6' : $('#sim'+_elemento+_nivel+'7').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		success: function (_response){
			document.getElementById('td_tabla'+_tipo).innerHTML = _response;
			GeneraGraficoTipo1(_tipo);
			return;
		}
	});
}

function GeneraGraficoTipo1(_tipo){
	document.getElementById('tr_grafico'+_tipo).style.display = '';
	labelx = '# Corrida';
	
	if(_tipo==1){
		titulo = 'Variaci�n de TR n-Undecano';
		labely = 'TR';
		var media = document.getElementById('simpromtr1').value;
		var des = document.getElementById('simdstr1').value
		var result = document.getElementById('result1').value;
		var sobrepasa = 1;
	}else if(_tipo==2){
		titulo = 'Variaci�n de �rea n-Undecano';
		labely = '�rea';
		var media = document.getElementById('simpromarea1').value;
		var des = document.getElementById('simdsarea1').value;
		var result = document.getElementById('result1').value;
		var sobrepasa = 1;
	}else if(_tipo==3){
		titulo = 'Variaci�n de TR 2,6-Dimetilfenol';
		labely = 'TR';
		var media = document.getElementById('simpromtr2').value;
		var des = document.getElementById('simdstr2').value;
		var result = document.getElementById('result2').value;
		var sobrepasa = 2;
	}else if(_tipo==4){
		titulo = 'Variaci�n de �rea 2,6-Dimetilfenol';
		labely = '�rea';
		var media = document.getElementById('simpromarea2').value;
		var des = document.getElementById('simdsarea2').value;
		var result = document.getElementById('result2').value;
		var sobrepasa = 2;
	}else if(_tipo==5){
		titulo = 'Variaci�n de TR 2,6-Dimetilanilina';
		labely = 'TR';
		var media = document.getElementById('simpromtr3').value;
		var des = document.getElementById('simdstr3').value;
		var result = document.getElementById('result3').value;
		var sobrepasa = 3;
	}else if(_tipo==6){
		titulo = 'Variaci�n de �rea 2,6-Dimetilanilina';
		labely = '�rea';
		var media = document.getElementById('simpromarea3').value;
		var des = document.getElementById('simdsarea3').value;
		var result = document.getElementById('result3').value;
		var sobrepasa = 3;
	}
	
	var limite1 = _RED2(parseFloat(media) + 3*des, 2);
	var limite2 = _RED2(parseFloat(media) + 2*des, 2);
	var limite3 = _RED2(parseFloat(media) - 2*des, 2);
	var limite4 = _RED2(parseFloat(media) - 3*des, 2);

	if(result == '1') subtitulo = '<strong>S� cumple</strong>';
	else subtitulo = '<strong>No cumple</strong>';

	document.getElementById('sobrepasa'+sobrepasa).innerHTML = 'Tendencia de puntos no sobrepasan los l�mites de advertencia:  ' + subtitulo;

	$(function () {
		$('#container'+_tipo).highcharts({
			legend: {
            	enabled: false
       		},
			data: {
				table: document.getElementById('tabla'+_tipo)
			},
			title: {
                text: titulo,
                x: -20
            },
            yAxis: {
                title: {
                    text: labely
                },
				plotLines: [
					{color: '#000000',width: 2,value: media},
					{color: '#FF0000',width: 2,value: limite1},
					{color: '#FFFF00',width: 2,value: limite2},
					{color: '#FFFF00',width: 2,value: limite3},
					{color: '#FF0000',width: 2,value: limite4}
				]
            },
			xAxis: {
                title: {
                    text: labelx
                }
            },
          	tooltip: {
				enabled: false
			}
		});
	});
}

function GeneraDatosTipoB(_tipo){
	var parametros = {
		'_AJAX' : 1,
		'paso' : 6,
		'clase' : 'B',
		'tipo' : _tipo,
		'x' : Vector('5conc'),
		'y' : Vector('5area')
	}

	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		success: function (_response){
			document.getElementById('td_tabla'+_tipo).innerHTML = _response;
			//POBLA TABLA DE DATOS
			for(i=0;i<4;i++){
				document.getElementById(_tipo+'xx'+i).innerHTML = _RED2(document.getElementById(_tipo+'_CN'+i).innerHTML, 2);
				document.getElementById(_tipo+'yy'+i).innerHTML = _RED2(document.getElementById(_tipo+'_AR'+i).innerHTML, 2);
			}
			Curvas(_tipo);
			GeneraGraficoTipo2(_tipo);
			return;
		}
	});
}

function GeneraGraficoTipo2(_tipo){
	document.getElementById('tr_grafico'+_tipo).style.display = '';
	labelx = 'Concentraci�n (ug/ml)';
	labely = '�rea';
	ticks = 0.5;
	grid = 0;
	
	if(_tipo==7) titulo = 'Curva n-Undecano';
	else if(_tipo==8) titulo = 'Curva 2,6-Dimetilfenol';
	else if(_tipo==9) titulo = 'Curva 2,6-Dimetilanilina';

	$(function () {
		$('#container'+_tipo).highcharts({
			legend: {
            	enabled: false
       		},
			data: {
				table: document.getElementById('curva_'+_tipo)
			},
			title: {
                text: titulo,
                x: -20 //center
            },
			subtitle: {
				text: 'R2 = ' + document.getElementById(_tipo+'r').innerHTML,
				floating: true,
				align: 'right',
				x: -20,
				verticalAlign: 'bottom',
				y: -75
			},
            yAxis: {
                title: {
                    text: labely
                }
            },
			xAxis: {
                title: {
                    text: labelx
                },
				tickInterval: ticks
            },
          	tooltip: {
				enabled: false
			}
		});
	});
	//SI YA TERMINO INVOCAMOS A OTRO
	if(_tipo==7) GeneraDatosTipoB(8);
	else if(_tipo==8) GeneraDatosTipoB(9);
	else if(_tipo==9){
		if(parseFloat(document.getElementById('7r').innerHTML) > 0.99 && parseFloat(document.getElementById('8r').innerHTML) > 0.99 && parseFloat(document.getElementById('9r').innerHTML) > 0.99)
			subtitulo = '<strong>S� cumple</strong>';
		else
			subtitulo = '<strong>No cumple</strong>';
			
		document.getElementById('sobrepasa4').innerHTML = 'Coeficiente de correlaci�n en curva de cada compuesto mayor 0,99:  ' + subtitulo;
	}
}

function Pendiente(_tipo){
	var x2=0;
	var y=0;
	var x=0;
	var xy=0;
	var cantidad = 4;
	for(i=0;i<cantidad;i++){
		x2 += parseFloat(document.getElementById(_tipo+'_CN'+i).innerHTML*document.getElementById(_tipo+'_CN'+i).innerHTML);
		y += parseFloat(document.getElementById(_tipo+'_AR'+i).innerHTML);
		x += parseFloat(document.getElementById(_tipo+'_CN'+i).innerHTML);
		xy += parseFloat(document.getElementById(_tipo+'_CN'+i).innerHTML*document.getElementById(_tipo+'_AR'+i).innerHTML);
	}
	var M = (cantidad*xy-x*y)/(cantidad*x2-x*x);
	document.getElementById(_tipo+'pendiente').innerHTML = _RED2(M, 4);
}

function Curvas(_tipo){
	var sumCN = sumAR = sumA = sumB = sumC = sumD = sumE = sumF = 0;

	Pendiente(_tipo);
	
	//INTERCEPTO
	var promC = document.getElementById(_tipo+'_CN4').innerHTML;
	var promP = document.getElementById(_tipo+'_AR4').innerHTML;
	var B = promP-(document.getElementById(_tipo+'pendiente').innerHTML*promC);
	document.getElementById(_tipo+'intercepto').innerHTML = _RED2(B, 4);
	//
	for(i=0;i<4;i++){		
		document.getElementById(_tipo+'_A'+i).innerHTML = Math.pow(document.getElementById(_tipo+'_CN'+i).innerHTML, 2); 
		sumA += parseFloat(document.getElementById(_tipo+'_A'+i).innerHTML);
		sumCN += parseFloat(document.getElementById(_tipo+'_CN'+i).innerHTML);
		sumAR += parseFloat(document.getElementById(_tipo+'_AR'+i).innerHTML);
	}
	document.getElementById(_tipo+'_CN'+i).innerHTML = sumCN/4;
	document.getElementById(_tipo+'_AR'+i).innerHTML = sumAR/4;
	document.getElementById(_tipo+'_A'+i).innerHTML = sumA/4;
	document.getElementById(_tipo+'_CN5').innerHTML = sumCN;
	document.getElementById(_tipo+'_AR5').innerHTML = sumAR;
	document.getElementById(_tipo+'_A5').innerHTML = sumA;
	//
	for(i=0;i<4;i++){
		document.getElementById(_tipo+'_B'+i).innerHTML = document.getElementById(_tipo+'_CN'+i).innerHTML - document.getElementById(_tipo+'_CN4').innerHTML;
		document.getElementById(_tipo+'_C'+i).innerHTML = Math.pow(document.getElementById(_tipo+'_B'+i).innerHTML, 2); 
		document.getElementById(_tipo+'_D'+i).innerHTML = document.getElementById(_tipo+'_AR'+i).innerHTML - document.getElementById(_tipo+'_AR4').innerHTML;
		document.getElementById(_tipo+'_E'+i).innerHTML = Math.pow(document.getElementById(_tipo+'_D'+i).innerHTML, 2); 
		document.getElementById(_tipo+'_F'+i).innerHTML = document.getElementById(_tipo+'_B'+i).innerHTML * document.getElementById(_tipo+'_D'+i).innerHTML;
		sumB += parseFloat(document.getElementById(_tipo+'_B'+i).innerHTML);
		sumC += parseFloat(document.getElementById(_tipo+'_C'+i).innerHTML);
		sumD += parseFloat(document.getElementById(_tipo+'_D'+i).innerHTML);
		sumE += parseFloat(document.getElementById(_tipo+'_E'+i).innerHTML);
		sumF += parseFloat(document.getElementById(_tipo+'_F'+i).innerHTML);
	}
	document.getElementById(_tipo+'_B'+i).innerHTML = sumB/4;
	document.getElementById(_tipo+'_C'+i).innerHTML = sumC/4;
	document.getElementById(_tipo+'_D'+i).innerHTML = sumD/4;
	document.getElementById(_tipo+'_E'+i).innerHTML = sumE/4;
	document.getElementById(_tipo+'_F'+i).innerHTML = sumF/4;
	document.getElementById(_tipo+'_B5').innerHTML = sumB;
	document.getElementById(_tipo+'_C5').innerHTML = sumC;
	document.getElementById(_tipo+'_D5').innerHTML = sumD;
	document.getElementById(_tipo+'_E5').innerHTML = sumE;
	document.getElementById(_tipo+'_F5').innerHTML = sumF;
	//
	document.getElementById(_tipo+'r').innerHTML = document.getElementById(_tipo+'_F5').innerHTML / Math.sqrt(document.getElementById(_tipo+'_C5').innerHTML*document.getElementById(_tipo+'_E5').innerHTML);
	document.getElementById(_tipo+'r').innerHTML = _RED2(document.getElementById(_tipo+'r').innerHTML*document.getElementById(_tipo+'r').innerHTML, 4);
	//
	if(_tipo==7 && parseFloat(document.getElementById('7r').innerHTML) > 0.99) cumple7 = true;
	if(_tipo==8 && parseFloat(document.getElementById('8r').innerHTML) > 0.99) cumple8 = true;
	if(_tipo==9 && parseFloat(document.getElementById('9r').innerHTML) > 0.99) cumple9 = true;
}