$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"sScrollX": "100%",
		/*"sScrollXInner": "110%",*/
		"bScrollCollapse": true,
		"bFilter": true,
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function CartasAgrega(){
	var control = document.getElementById('tipo2');
	var id = control.options[control.selectedIndex].getAttribute('value');

	window.open("carta" + id + "_detalle.php?acc=I","","width=900,height=600,scrollbars=yes,status=no");
	//window.showModalDialog("carta" + id + "_detalle.php?acc=I", this.window, "dialogWidth:" + size + "px;dialogHeight:750px;status:no;");
}

function CartaDetalle(_ID, _TIPO){
	window.open("carta" + _TIPO + "_detalle.php?acc=M&ID="+_ID,"","width=900,height=600,scrollbars=yes,status=no");
	//window.showModalDialog("carta" + _TIPO + "_detalle.php?acc=M&ID="+_ID, this.window, "dialogWidth:" + _TAM + "px;dialogHeight:750px;status:no;");
}

function CartasBuscar(btn){
	if( Mal(1, $('#desde').val()) ){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	if( Mal(1, $('#hasta').val()) ){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	btn.disabled = true;
	btn.form.submit();
}