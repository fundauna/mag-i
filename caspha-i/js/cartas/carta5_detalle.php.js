function CalculaTemp(){
	if(document.getElementById('ind1').innerHTML != '...' && document.getElementById('temp1').value != ''){
		var par1 = (parseFloat(document.getElementById('ind1').innerHTML) + parseFloat(document.getElementById('ind2').innerHTML)) / 2;
		var par2 = (parseFloat(document.getElementById('ind2').innerHTML) + parseFloat(document.getElementById('ind3').innerHTML)) / 2;
		
		if(parseFloat(document.getElementById('temp1').value) < par1)
			document.getElementById('temp2').value = document.getElementById('temp1').value - document.getElementById('factor1').innerHTML;
		else if(parseFloat(document.getElementById('temp1').value) <= par2)
			document.getElementById('temp2').value = document.getElementById('temp1').value - document.getElementById('factor2').innerHTML;
		else
			document.getElementById('temp2').value = document.getElementById('temp1').value - document.getElementById('factor3').innerHTML;
	}
}

function Redondear(txt, tipo){
	var decimales = parseInt(document.getElementById('decimales' + tipo).value);
	_RED(txt, decimales);
}

function GeneraGrafico(){	
	document.getElementById('tr_grafico').style.display = '';
	var media = _RED2(document.getElementById('graf_media').value, 2);
	var limite1 = _RED2(document.getElementById('graf_limite1').value, 2);
	var limite2 = _RED2(document.getElementById('graf_limite2').value, 2);
	var limite3 = _RED2(document.getElementById('graf_limite3').value, 2);
	var limite4 = _RED2(document.getElementById('graf_limite4').value, 2);
	
	titulo = 'Control de temperatura';
	ejey = 'Temperatura (C)';

	$(function () {
		$('#container').highcharts({
			legend: {
            	enabled: false
       		},
			data: {
				table: document.getElementById('tabla')
			},
			title: {
                text: titulo,
                x: -20 //center
            },
			subtitle: {
            	text: $('#cod_cam').val() + ' ' + document.getElementById('ubicacion').innerHTML
        	},
            yAxis: {
                title: {
                    text: ejey
                },
				plotLines: [
					{color: '#0066FF',width: 2,value: media},
					{color: '#FF0000',width: 2,value: limite1},
					{color: '#00FF00',width: 2,value: limite2},
					{color: '#00FF00',width: 2,value: limite3},
					{color: '#FF0000',width: 2,value: limite4}
				]
            },
          	tooltip: {
				enabled: false
			}
		});
	});
}

function EliminaLinea(_cs, _fecha, _hora){	
	if(!confirm('Eliminar l�nea?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 3,
		'cs' : _cs,
		'fecha' : _fecha,
		'hora' : _hora
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					alert("Transaccion finalizada");
					location.reload();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Generar(){	
	if($('#graf_desde').val()=='' || $('#graf_hasta').val()==''){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 2,
		'cs' : $('#cs').val(),
		'tipo' : $('#graf_pesa').val(),
		'desde' : $('#graf_desde').val(),
		'hasta' : $('#graf_hasta').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response){
			if(_response == ''){
				OMEGA("No hay datos que mostrar");
				return;
			}
			BETA();
			document.getElementById('td_tabla').innerHTML = _response;
			GeneraGrafico();
			return;
		}
	});
}

function datos(){
	if($('#cod_cam').val()==''){
		OMEGA('Debe indicar el equipo');
		return;
	}
	
	if($('#cod_ter').val()==''){
		OMEGA('Debe indicar el term�metro');
		return;
	}
	
	if($('#fecha').val()==''){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	if($('#hora').val()==''){
		OMEGA('Debe indicar la hora');
		return;
	}
	
	if($('#temp1').val()==''){
		OMEGA('Debe indicar la temperatura');
		return;
	}
	
	if($('#temp2').val()==''){
		OMEGA('Debe indicar la temperatura corregida');
		return;
	}
		
	if(!confirm('Modificar datos?')) return;

	var parametros = {
		'_AJAX' : 1,
		'paso' : 1,
		'cs' : $('#cs').val(),
		'accion' : $('#accion').val(),
		//
		'cod_cam' : $('#cod_cam').val(),
		'cod_ter' : $('#cod_ter').val(),
		'limites' : document.getElementById('limites').innerHTML,
		'tipo' : document.getElementById('tipo').innerHTML,
		'ubicacion' : document.getElementById('ubicacion').innerHTML,
		'inc1' : document.getElementById('inc1').innerHTML,
		'ind1' : document.getElementById('ind1').innerHTML,
		'factor1' : document.getElementById('factor1').innerHTML,
		'inc2' : document.getElementById('inc2').innerHTML,
		'ind2' : document.getElementById('ind2').innerHTML,
		'factor2' : document.getElementById('factor2').innerHTML,
		'inc3' : document.getElementById('inc3').innerHTML,
		'ind3' : document.getElementById('ind3').innerHTML,
		'factor3' : document.getElementById('factor3').innerHTML,
		'inc4' : document.getElementById('inc4').innerHTML,
		'ind4' : document.getElementById('ind4').innerHTML,
		'factor4' : document.getElementById('factor4').innerHTML,
		'inc5' : document.getElementById('inc5').innerHTML,
		'ind5' : document.getElementById('ind5').innerHTML,
		'factor5' : document.getElementById('factor5').innerHTML,
		//
		'fecha' : $('#fecha').val(),
		'hora' : $('#hora').val(),
		'temp1' : $('#temp1').val(),
		'temp2' : $('#temp2').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').prop("disabled",false);
					break;
				case '':
					alert('Tiempo de espera agotado');break;
					break;
				default:
					location.href = 'carta5_detalle.php?acc=M&ID=' + _response;
					break;
			}
		}
	});
}

function CamaraEscoge(codigo, tipo, limite1, ubicacion){
	opener.document.getElementById('cod_cam').value=codigo;
	opener.document.getElementById('tipo').innerHTML=tipo;
	opener.document.getElementById('limites').innerHTML=limite1;
	opener.document.getElementById('ubicacion').innerHTML=ubicacion;
	window.close();
}

function CamarasLista(){
	window.open(__SHELL__ + "?list2=1","","width=500,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list2=1", this.window, "dialogWidth:500px;dialogHeight:200px;status:no;");
}

function TermoEscoge(codigo, cuarto, limites, inc1, indicacion1, error1, inc2, indicacion2, error2, inc3, indicacion3, error3, inc4, indicacion4, error4, inc5, indicacion5, error5, humedad, hr1, err_hr1, hr2, err_hr2, hr3, err_hr3){
	opener.document.getElementById('cod_ter').value=codigo;
	opener.document.getElementById('inc1').innerHTML=inc1;
	opener.document.getElementById('ind1').innerHTML=indicacion1;
	opener.document.getElementById('factor1').innerHTML=error1;
	opener.document.getElementById('inc2').innerHTML=inc2;
	opener.document.getElementById('ind2').innerHTML=indicacion2;
	opener.document.getElementById('factor2').innerHTML=error2;
	opener.document.getElementById('inc3').innerHTML=inc3;
	opener.document.getElementById('ind3').innerHTML=indicacion3;
	opener.document.getElementById('factor3').innerHTML=error3;
	opener.document.getElementById('inc4').innerHTML=inc4;
	opener.document.getElementById('ind4').innerHTML=indicacion4;
	opener.document.getElementById('factor4').innerHTML=error4;
	opener.document.getElementById('inc5').innerHTML=inc5;
	opener.document.getElementById('ind5').innerHTML=indicacion5;
	opener.document.getElementById('factor5').innerHTML=error5;
	opener.CalculaTemp();
	window.close();
}

function TermosLista(_linea){
	window.open(__SHELL__ + "?list1=1" + _linea,"","width=500,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list1=1" + _linea, this.window, "dialogWidth:500px;dialogHeight:200px;status:no;");
}

function Procesar(tipo){
	if(!confirm('Modificar estado?')) return;

	var parametros = {
		'_AJAX' : 1,
		'paso' : 9,
		'cs' : $('#cs').val(),
		'accion' : tipo
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					location.reload();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}