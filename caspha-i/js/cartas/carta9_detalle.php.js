/*******************************************************************
 MISC
 *******************************************************************/
var img1 = ''; //ruta de las imagenes de mostrar/ocultar
var img2 = '';

//PARA SABER QUE YA LA INFO SE CARG�
var _paso2 = false;
var _paso3 = false;
var _paso4 = false;
var _paso5 = false;

function VersionImprimible() {
    this.location.href = 'carta9_imprimir.php?ID=' + $('#id').val();
}

function DisponibleEscoge(id, codigo, nombre, linea, marca, modelo) {
    opener.document.getElementById('equipo').value = id;
    opener.document.getElementById('nomequipo').value = codigo;
    opener.document.getElementById('marca').innerHTML = marca + ' / ' + modelo;
    window.close();
}

function InventarioEscoge(tipo, id, codigo, nombre, linea) {
    opener.document.getElementById('id' + tipo).value = id;
    opener.document.getElementById('cod' + tipo).innerHTML = codigo;
    opener.document.getElementById('nom' + tipo).value = nombre;
    window.close();
}

function ColumnasEscoge(id, codigo, nombre, marca, dimensiones) {
    opener.document.getElementById('idColumna').value = id;
    opener.document.getElementById('codColumna').innerHTML = codigo;
    opener.document.getElementById('nomColumna').value = codigo;
    window.close();
}

function EstandarEscoge(id, codigo, nombre) {
    opener.document.getElementById('idEstandar').value = id;
    opener.document.getElementById('codEstandar').innerHTML = codigo;
    opener.document.getElementById('nomEstandar').value = nombre;
    window.close();
}

function EquiposLista() {
    window.open(__SHELL__ + "?list=1", "", "width=400,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function ColumnaLista() {
    window.open(__SHELL__ + "?list2=1", "", "width=400,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function EstandarLista() {
    window.open(__SHELL__ + "?list2=2", "", "width=400,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function InventarioLista(tipo) {
    window.open(__SHELL__ + "?list2=" + tipo, "", "width=400,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list2="+tipo, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function Buscar(num) {
    var parametros = {
        '_VER': num,
        'id': $('#id').val(),
        'acc': $('#acc').val()
    }

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            BETA();
            document.getElementById('t' + num).innerHTML = _response;
            //
            if (num == 2) {
                Cumple1();
                Cumple2();
                Cumple3();
                Cumple4();
                Cumple5();
                Cumple6();
                Cumple7();
            } else if (num == 3) {
                Cumple8();
            } else if (num == 4) {
                Redondear(1);
                Redondear(2);
                Redondear(3);
                Cumple9();
            }
            return;
        }
    });
}

function Cumple1() {
    var a, b, c;
    a = document.getElementById('v1').value;
    b = document.getElementById('v2').value;
    c = document.getElementById('v3').value;
    if (a == '69' && b == '219' && c == '502')
        document.getElementById('cumple1').innerHTML = 'S&iacute;';
    else
        document.getElementById('cumple1').innerHTML = 'No';
}

function Cumple2() {
    var a, b, c;
    a = parseFloat(document.getElementById('v4').value.replace(/,/g, ''));
    b = parseFloat(document.getElementById('v5').value.replace(/,/g, ''));
    c = parseFloat(document.getElementById('v6').value.replace(/,/g, ''));

    if (a >= 0.5 && a <= 0.7 && b >= 0.5 && b <= 0.7 && c >= 0.5 && c <= 0.7)
        document.getElementById('cumple2').innerHTML = 'S&iacute;';
    else
        document.getElementById('cumple2').innerHTML = 'No';
}

function Cumple3() {
    var a = parseFloat(document.getElementById('v7').value.replace(/,/g, ''));
    if (a >= 0 && a <= 40)
        document.getElementById('cumple3').innerHTML = 'S&iacute;';
    else
        document.getElementById('cumple3').innerHTML = 'No';
}

function Cumple4() {
    var a = parseFloat(document.getElementById('v8').value.replace(/,/g, ''));
    if (a >= 1000 && a <= 3000)
        document.getElementById('cumple4').innerHTML = 'S&iacute;';
    else
        document.getElementById('cumple4').innerHTML = 'No';
}

function Cumple5() {
    var a = document.getElementById('v9').value.replace(/,/g, '.');
    if (a == '69' || a == '219')
        document.getElementById('cumple5').innerHTML = 'S&iacute;';
    else
        document.getElementById('cumple5').innerHTML = 'No';
}

function Cumple6() {
    var a = parseFloat(document.getElementById('v10').value.replace(/,/g, ''));
    if (a >= 400000 && a <= 600000)
        document.getElementById('cumple6').innerHTML = 'S&iacute;';
    else
        document.getElementById('cumple6').innerHTML = 'No';
}

function Cumple7() {
    var a = parseFloat(document.getElementById('v11').value.replace(/,/g, ''));
    var b = parseFloat(document.getElementById('v12').value.replace(/,/g, ''));
    var c = parseFloat(document.getElementById('v13').value.replace(/,/g, ''));
    var d = parseFloat(document.getElementById('v14').value.replace(/,/g, ''));
    var e = parseFloat(document.getElementById('v15').value.replace(/,/g, ''));

    if (a >= 0.5 && a <= 1.6)
        document.getElementById('cumple7').innerHTML = 'S&iacute;';
    else
        document.getElementById('cumple7').innerHTML = 'No';

    if (b >= 3.2 && b <= 5.4)
        document.getElementById('cumple8').innerHTML = 'S&iacute;';
    else
        document.getElementById('cumple8').innerHTML = 'No';

    if (c >= 7.9 && c <= 12.3)
        document.getElementById('cumple9').innerHTML = 'S&iacute;';
    else
        document.getElementById('cumple9').innerHTML = 'No';

    if (d < 20)
        document.getElementById('cumpleA').innerHTML = 'S&iacute;';
    else
        document.getElementById('cumpleA').innerHTML = 'No';

    if (e < 10)
        document.getElementById('cumpleB').innerHTML = 'S&iacute;';
    else
        document.getElementById('cumpleB').innerHTML = 'No';
}

function Cumple8() {
    var platos_decano = parseFloat(document.getElementById('platos2').value.replace(/,/g, ''));
    var platos_undeca = parseFloat(document.getElementById('platos4').value.replace(/,/g, ''));
    //
    var resol_decano = parseFloat(document.getElementById('resol2').value.replace(/,/g, ''));
    var resol_undeca = parseFloat(document.getElementById('resol4').value.replace(/,/g, ''));
    var asime_decano = parseFloat(document.getElementById('asim2').value.replace(/,/g, ''));
    var asime_undeca = parseFloat(document.getElementById('asim4').value.replace(/,/g, ''));
    //
    var resol_octa = parseFloat(document.getElementById('resol3').value.replace(/,/g, ''));
    var asime_octa = parseFloat(document.getElementById('asim3').value.replace(/,/g, ''));
    //
    var resol_diclo = parseFloat(document.getElementById('resol11').value.replace(/,/g, ''));
    var asime_diclo = parseFloat(document.getElementById('asim11').value.replace(/,/g, ''));

    if (platos_decano > 20000 && platos_undeca > 20000)
        document.getElementById('cumple0').innerHTML = 'S&iacute;';
    else
        document.getElementById('cumple0').innerHTML = 'No';

    if ((document.getElementById('resol2').value > 1.5 && document.getElementById('resol4').value > 1.5) && (document.getElementById('asim2').value < 1.5 && document.getElementById('asim4').value < 1.5))
        document.getElementById('cumple1_1').innerHTML = 'S&iacute;';
    else
        document.getElementById('cumple1_1').innerHTML = 'No';

    if (resol_octa > 1.5 && asime_octa < 1.5)
        document.getElementById('cumple2_1').innerHTML = 'S&iacute;';
    else
        document.getElementById('cumple2_1').innerHTML = 'No';

    if (resol_diclo > 1.5 && asime_diclo < 1.5)
        document.getElementById('cumple3_1').innerHTML = 'S&iacute;';
    else
        document.getElementById('cumple3_1').innerHTML = 'No';

    /*if(document.getElementById('tr1').value == '0.00' && document.getElementById('resol1').value == '0.00' && document.getElementById('qual1').value == '0' && document.getElementById('asim1').value == '0.00' && document.getElementById('platos1').value == '0.00') document.getElementById('cumple4').innerHTML = 'No';
     else document.getElementById('cumple4').innerHTML = 'S&iacute;';	*/
}

function Cumple9() {
    _FLOAT(document.getElementById('vresol'));
    _FLOAT(document.getElementById('vresol2'));
    _FLOAT(document.getElementById('vasimetria'));
    _FLOAT(document.getElementById('vasimetria2'));
    document.getElementById('cumple32').style.display = 'none';
    document.getElementById('cumple33').style.display = '';
    document.getElementById('cumple34').style.display = 'none';
    document.getElementById('cumple35').style.display = '';

    if (document.getElementById('vresol').value.replace(/,/g, '.') > 1.5 && document.getElementById('vasimetria').value.replace(/,/g, '.') < 1.5) {
        document.getElementById('cumple33').style.display = 'none';
        document.getElementById('cumple32').style.display = '';
    }
    if (document.getElementById('vresol2').value.replace(/,/g, '.') > 1.5 && document.getElementById('vasimetria2').value.replace(/,/g, '.') < 1.5) {
        document.getElementById('cumple35').style.display = 'none';
        document.getElementById('cumple34').style.display = '';
    }
}

function Redondear(tipo) {
    var sumA = 0, sumB = 0;
    for (i = 1; i <= 7; i++) {
        _RED(document.getElementById('simtr' + tipo + i), 3);
        _RED(document.getElementById('simarea' + tipo + i), 0);
        //
        sumA += parseFloat(document.getElementById('simtr' + tipo + i).value);
        sumB += parseFloat(document.getElementById('simarea' + tipo + i).value);
    }

    var fMeanA = sumA / 7;
    var fMeanB = sumB / 7;
    var fVarianceA = fVarianceB = 0;

    for (i = 1; i <= 7; i++) {
        fVarianceA += parseFloat(Math.pow(document.getElementById('simtr' + tipo + i).value - fMeanA, 2));
        fVarianceB += parseFloat(Math.pow(document.getElementById('simarea' + tipo + i).value - fMeanB, 2));
    }

    document.getElementById('simpromtr' + tipo).value = _RED2(fMeanA, 3);
    document.getElementById('simpromarea' + tipo).value = _RED2(fMeanB, 3);

    document.getElementById('simdstr' + tipo).value = Math.sqrt(fVarianceA) / Math.sqrt(7 - 1);
    document.getElementById('simdsarea' + tipo).value = _RED2(Math.sqrt(fVarianceB) / Math.sqrt(7 - 1), 3);
    document.getElementById('simcvtr' + tipo).value = _RED2((document.getElementById('simdstr' + tipo).value / fMeanA) * 100, 3);
    document.getElementById('simcvarea' + tipo).value = _RED2((document.getElementById('simdsarea' + tipo).value / fMeanB) * 100, 3);
    //OBTIENE PUNTOS MENORES Y MAYORES
    var mayorTR = document.getElementById('simtr' + tipo + 1).value;
    var menorTR = document.getElementById('simtr' + tipo + 1).value;
    var mayorAR = document.getElementById('simarea' + tipo + 1).value;
    var menorAR = document.getElementById('simarea' + tipo + 1).value;
    for (i = 1; i <= 7; i++) {
        if (mayorTR < document.getElementById('simtr' + tipo + i).value)
            mayorTR = document.getElementById('simtr' + tipo + i).value;
        if (menorTR > document.getElementById('simtr' + tipo + i).value)
            menorTR = document.getElementById('simtr' + tipo + i).value;
        if (mayorAR < document.getElementById('simarea' + tipo + i).value)
            mayorAR = document.getElementById('simarea' + tipo + i).value;
        if (menorAR > document.getElementById('simarea' + tipo + i).value)
            menorAR = document.getElementById('simarea' + tipo + i).value;
    }
    //
    document.getElementById('limitetrSup' + tipo).value = _RED2(parseFloat(document.getElementById('simpromtr' + tipo).value) + 2 * document.getElementById('simdstr' + tipo).value, 2);
    document.getElementById('limitearSup' + tipo).value = _RED2(parseFloat(document.getElementById('simpromarea' + tipo).value) + 2 * document.getElementById('simdsarea' + tipo).value, 2);
    document.getElementById('limitetrInf' + tipo).value = _RED2(parseFloat(document.getElementById('simpromtr' + tipo).value) - 2 * document.getElementById('simdstr' + tipo).value, 2);
    document.getElementById('limitearInf' + tipo).value = _RED2(parseFloat(document.getElementById('simpromarea' + tipo).value) - 2 * document.getElementById('simdsarea' + tipo).value, 2);

    var cumple = 1;
    if (document.getElementById('limitetrSup' + tipo).value < mayorTR)
        cumple = 0;
    if (document.getElementById('limitearSup' + tipo).value < mayorAR)
        cumple = 0;
    if (document.getElementById('limitetrInf' + tipo).value > menorTR)
        cumple = 0;
    if (document.getElementById('limitearInf' + tipo).value > menorAR)
        cumple = 0;
    document.getElementById('result' + tipo).value = cumple;
}

function Oculta(obj) {
    if (document.getElementById('t' + obj).style.display == 'none') {
        document.getElementById('t' + obj).style.display = '';
        document.getElementById('f' + obj).style.display = '';
        document.getElementById('i_' + obj).src = img2;
    } else {
        document.getElementById('t' + obj).style.display = 'none';
        document.getElementById('f' + obj).style.display = 'none';
        document.getElementById('i_' + obj).src = img1;
    }

    if (obj == 2 && !_paso2) {
        Buscar(obj);
        _paso2 = true;
    }
    if (obj == 3 && !_paso3) {
        Buscar(obj);
        _paso3 = true;
    }
    if (obj == 4 && !_paso4) {
        Buscar(obj);
        _paso4 = true;
    }
    if (obj == 5 && !_paso5) {
        Buscar(obj);
        _paso5 = true;
    }
}

function Vector(ctrl) {
    var str = "1=1";
    control = document.getElementsByName(ctrl);
    for (i = 0; i < control.length; i++) {
        str += "&" + control[i].value.replace(/&/g, '');
    }
    return str;
}

/*******************************************************************
 VALIDACIONES
 *******************************************************************/
function Paso1() {
    if (Mal(1, $('#nomequipo').val())) {
        OMEGA('Debe seleccionar el equipo');
        return;
    }

    if (Mal(1, $('#fecha').val())) {
        OMEGA('Debe seleccionar la fecha');
        return;
    }

    if (Mal(1, $('#nomColumna').val())) {
        OMEGA('Debe seleccionar la columna');
        return;
    }

    if (Mal(1, $('#nomEstandar').val())) {
        OMEGA('Debe seleccionar el estandar');
        return;
    }

    if (!confirm('Modificar datos?'))
        return;

    var parametros = {
        '_AJAX': 1,
        'paso': 1,
        'id': $('#cs').val(),
        'fecha': $('#fecha').val(),
        'acc': $('#acc').val(),
        'equipo': $('#equipo').val(),
        'idColumna': $('#idColumna').val(),
        'idEstandar': $('#idEstandar').val(),
        'connominal': $('#connominal').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            $('#btn1').prop("disabled", true);
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn1').prop("disabled", false);
                    break;
                case '':
                    alert('Tiempo de espera agotado');
                    break;
                    break;
                default:
                    location.href = 'carta9_detalle.php?acc=M&ID=' + _response;
                    break;
            }
        }
    });
}

function Paso2() {
    if (Mal(1, $('#v1').val())) {
        OMEGA('Debe indicar todos los valores');
        return;
    }
    if (Mal(1, $('#v2').val())) {
        OMEGA('Debe indicar todos los valores');
        return;
    }
    if (Mal(1, $('#v3').val())) {
        OMEGA('Debe indicar todos los valores');
        return;
    }
    if (Mal(1, $('#v4').val())) {
        OMEGA('Debe indicar todos los valores');
        return;
    }
    if (Mal(1, $('#v5').val())) {
        OMEGA('Debe indicar todos los valores');
        return;
    }
    if (Mal(1, $('#v6').val())) {
        OMEGA('Debe indicar todos los valores');
        return;
    }
    if (Mal(1, $('#v7').val())) {
        OMEGA('Debe indicar todos los valores');
        return;
    }
    if (Mal(1, $('#v8').val())) {
        OMEGA('Debe indicar todos los valores');
        return;
    }
    if (Mal(1, $('#v9').val())) {
        OMEGA('Debe indicar todos los valores');
        return;
    }
    if (Mal(1, $('#v10').val())) {
        OMEGA('Debe indicar todos los valores');
        return;
    }
    if (Mal(1, $('#v11').val())) {
        OMEGA('Debe indicar todos los valores');
        return;
    }
    if (Mal(1, $('#v12').val())) {
        OMEGA('Debe indicar todos los valores');
        return;
    }
    if (Mal(1, $('#v13').val())) {
        OMEGA('Debe indicar todos los valores');
        return;
    }
    if (Mal(1, $('#v14').val())) {
        OMEGA('Debe indicar todos los valores');
        return;
    }
    if (Mal(1, $('#v15').val())) {
        OMEGA('Debe indicar todos los valores');
        return;
    }

    if (!confirm('Modificar datos?'))
        return;

    var parametros = {
        '_AJAX': 1,
        'paso': 2,
        'id': $('#id').val(),
        'v1': $('#v1').val(),
        'v2': $('#v2').val(),
        'v3': $('#v3').val(),
        'v4': $('#v4').val(),
        'v5': $('#v5').val(),
        'v6': $('#v6').val(),
        'v7': $('#v7').val(),
        'v8': $('#v8').val(),
        'v9': $('#v9').val(),
        'v10': $('#v10').val(),
        'v11': $('#v11').val(),
        'v12': $('#v12').val(),
        'v13': $('#v13').val(),
        'v14': $('#v14').val(),
        'v15': $('#v15').val()
    }

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            $('#btn1').prop("disabled", true);
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn2').prop("disabled", false);
                    break;
                case '1':
                    OMEGA('Transaccion finalizada');
                    $('#btn2').prop("disabled", false);
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function Paso3() {
    if (Mal(1, $('#qual1').val())) {
        OMEGA('Debe indicar todos los Qual%');
        return;
    }
    if (Mal(1, $('#qual2').val())) {
        OMEGA('Debe indicar todos los Qual%');
        return;
    }
    if (Mal(1, $('#qual3').val())) {
        OMEGA('Debe indicar todos los Qual%');
        return;
    }
    if (Mal(1, $('#qual4').val())) {
        OMEGA('Debe indicar todos los Qual%');
        return;
    }
    if (Mal(1, $('#qual5').val())) {
        OMEGA('Debe indicar todos los Qual%');
        return;
    }
    if (Mal(1, $('#qual6').val())) {
        OMEGA('Debe indicar todos los Qual%');
        return;
    }
    if (Mal(1, $('#qual7').val())) {
        OMEGA('Debe indicar todos los Qual%');
        return;
    }
    if (Mal(1, $('#qual8').val())) {
        OMEGA('Debe indicar todos los Qual%');
        return;
    }
    if (Mal(1, $('#qual9').val())) {
        OMEGA('Debe indicar todos los Qual%');
        return;
    }
    if (Mal(1, $('#qual10').val())) {
        OMEGA('Debe indicar todos los Qual%');
        return;
    }
    if (Mal(1, $('#qual11').val())) {
        OMEGA('Debe indicar todos los Qual%');
        return;
    }
    if (Mal(1, $('#qual12').val())) {
        OMEGA('Debe indicar todos los Qual%');
        return;
    }

    if (!confirm('Modificar datos?'))
        return;

    var parametros = {
        '_AJAX': 1,
        'paso': 3,
        'id': $('#id').val(),
        'dimensiones': $('#dimensiones').val(),
        //
        'tr': Vector('tr'),
        'resol': Vector('resol'),
        'qual': Vector('qual'),
        'asim': Vector('asim'),
        'platos': Vector('platos'),
        'cumple4': $('#cumple4_selecccion').val()
    }

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            $('#btn3').prop("disabled", true);
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn3').prop("disabled", false);
                    break;
                case '1':
                    OMEGA('Transaccion finalizada');
                    $('#btn3').prop("disabled", false);
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function Paso4() {
    /*if( Mal(1, $('#vresol').val()) ){ OMEGA('Debe indicar el valor de Resol');return;}
     if( Mal(1, $('#vasimetria').val()) ){ OMEGA('Debe indicar el valor de Asimetr�a');return;}
     if( Mal(1, $('#vresol2').val()) ){ OMEGA('Debe indicar el valor de Resol');return;}
     if( Mal(1, $('#vasimetria2').val()) ){ OMEGA('Debe indicar el valor de Asimetr�a');return;}*/

    if (!confirm('Modificar datos?'))
        return;

    var parametros = {
        '_AJAX': 1,
        'paso': 4,
        'id': $('#id').val(),
        'vresol': $('#vresol').val(),
        'vasimetria': $('#vasimetria').val(),
        'vresol2': $('#vresol2').val(),
        'vasimetria2': $('#vasimetria2').val(),
        'tr': Vector('tr'),
        'area': Vector('area')
    }

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            $('#btn4').prop("disabled", true);
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn4').prop("disabled", false);
                    break;
                case '1':
                    OMEGA('Transaccion finalizada');
                    $('#btn4').prop("disabled", false);
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function Paso5() {
    control = document.getElementsByName('area');
    for (i = 0; i < control.length; i++) {
        if (Mal(1, control[i].value)) {
            OMEGA('Debe indicar todas las �reas');
            return;
        }
    }

    if (!confirm('Modificar datos?'))
        return;

    var parametros = {
        '_AJAX': 1,
        'paso': 5,
        'id': $('#id').val(),
        /*'coef' : $('#coef').val(),*/
        'obs': $('#obs').val(),
        'tr': Vector('tr'),
        'area': Vector('area'),
        'conc': Vector('conc')
    }

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            $('#btn5').prop("disabled", true);
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn5').prop("disabled", false);
                    break;
                case '1':
                    OMEGA('Transaccion finalizada');
                    $('#btn5').prop("disabled", false);
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function Generar() {
    if ($('#graf_pesa').val() == 'A') {
        var parametros = {
            '_AJAX': 1,
            'paso': 6,
            'tipo': $('#graf_pesa').val(),
            'y0': $('#simtr11').val(),
            'y1': $('#simtr12').val(),
            'y2': $('#simtr13').val(),
            'y3': $('#simtr14').val(),
            'y4': $('#simtr15').val(),
            'y5': $('#simtr16').val(),
            'y6': $('#simtr17').val()
        }
    } else if ($('#graf_pesa').val() == 'B') {
        var parametros = {
            '_AJAX': 1,
            'paso': 6,
            'tipo': $('#graf_pesa').val(),
            'y0': $('#simarea11').val(),
            'y1': $('#simarea12').val(),
            'y2': $('#simarea13').val(),
            'y3': $('#simarea14').val(),
            'y4': $('#simarea15').val(),
            'y5': $('#simarea16').val(),
            'y6': $('#simarea17').val()
        }
    } else if ($('#graf_pesa').val() == 'C') {
        var parametros = {
            '_AJAX': 1,
            'paso': 6,
            'tipo': $('#graf_pesa').val(),
            'y0': $('#simtr21').val(),
            'y1': $('#simtr22').val(),
            'y2': $('#simtr23').val(),
            'y3': $('#simtr24').val(),
            'y4': $('#simtr25').val(),
            'y5': $('#simtr26').val(),
            'y6': $('#simtr27').val()
        }
    } else if ($('#graf_pesa').val() == 'D') {
        var parametros = {
            '_AJAX': 1,
            'paso': 6,
            'tipo': $('#graf_pesa').val(),
            'y0': $('#simarea21').val(),
            'y1': $('#simarea22').val(),
            'y2': $('#simarea23').val(),
            'y3': $('#simarea24').val(),
            'y4': $('#simarea25').val(),
            'y5': $('#simarea26').val(),
            'y6': $('#simarea27').val()
        }
    } else if ($('#graf_pesa').val() == 'E') {
        var parametros = {
            '_AJAX': 1,
            'paso': 6,
            'tipo': $('#graf_pesa').val(),
            'y0': $('#simtr31').val(),
            'y1': $('#simtr32').val(),
            'y2': $('#simtr33').val(),
            'y3': $('#simtr34').val(),
            'y4': $('#simtr35').val(),
            'y5': $('#simtr36').val(),
            'y6': $('#simtr37').val()
        }
    } else if ($('#graf_pesa').val() == 'F') {
        var parametros = {
            '_AJAX': 1,
            'paso': 6,
            'tipo': $('#graf_pesa').val(),
            'y0': $('#simarea31').val(),
            'y1': $('#simarea32').val(),
            'y2': $('#simarea33').val(),
            'y3': $('#simarea34').val(),
            'y4': $('#simarea35').val(),
            'y5': $('#simarea36').val(),
            'y6': $('#simarea37').val()
        }
    } else if ($('#graf_pesa').val() == 'G') {
        var parametros = {
            '_AJAX': 1,
            'paso': 6,
            'tipo': $('#graf_pesa').val(),
            'x': Vector('conc'),
            'y': Vector('area')
        }
    }

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            if (_response == '') {
                OMEGA("No hay datos que mostrar");
                return;
            }
            BETA();
            document.getElementById('td_tabla').innerHTML = _response;
            GeneraGrafico();
            return;
        }
    });
}

function Generar2() {
    var parametros = {
        '_AJAX': 1,
        'paso': 6,
        'tipo': $('#graf_pesa2').val(),
        'x': Vector('conc'),
        'y': Vector('area')
    }

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            if (_response == '') {
                OMEGA("No hay datos que mostrar");
                return;
            }
            BETA();
            document.getElementById('td_tabla2').innerHTML = _response;
            GeneraGrafico2();
            return;
        }
    });
}

function GeneraGrafico() {
    document.getElementById('tr_grafico').style.display = '';
    labelx = '# Corrida';
    ticks = null;
    grid = 1;

    if ($('#graf_pesa').val() == 'A') {
        titulo = 'Variacion de TR n-Undecano';
        labely = 'TR';
        var media = document.getElementById('simpromtr1').value;
        var des = document.getElementById('simdstr1').value
        var result = document.getElementById('result1').value;
    } else if ($('#graf_pesa').val() == 'B') {
        titulo = 'Variacion de area n-Undecano';
        labely = '�rea';
        var media = document.getElementById('simpromarea1').value;
        var des = document.getElementById('simdsarea1').value;
        var result = document.getElementById('result1').value;
    } else if ($('#graf_pesa').val() == 'C') {
        titulo = 'Variacion de TR 2,6-Dimetilfenol';
        labely = 'TR';
        var media = document.getElementById('simpromtr2').value;
        var des = document.getElementById('simdstr2').value;
        var result = document.getElementById('result2').value;
    } else if ($('#graf_pesa').val() == 'D') {
        titulo = 'Variacion de area 2,6-Dimetilfenol';
        labely = '�rea';
        var media = document.getElementById('simpromarea2').value;
        var des = document.getElementById('simdsarea2').value;
        var result = document.getElementById('result2').value;
    } else if ($('#graf_pesa').val() == 'E') {
        titulo = 'Variacion de TR 2,6-Dimetilanilina';
        labely = 'TR';
        var media = document.getElementById('simpromtr3').value;
        var des = document.getElementById('simdstr3').value;
        var result = document.getElementById('result3').value;
    } else if ($('#graf_pesa').val() == 'F') {
        titulo = 'Variacion de area 2,6-Dimetilanilina';
        labely = '�rea';
        var media = document.getElementById('simpromarea3').value;
        var des = document.getElementById('simdsarea3').value;
        var result = document.getElementById('result3').value;
    }

    var limite1 = _RED2(parseFloat(media) + 3 * des, 2);
    var limite2 = _RED2(parseFloat(media) + 2 * des, 2);
    var limite3 = _RED2(parseFloat(media) - 2 * des, 2);
    var limite4 = _RED2(parseFloat(media) - 3 * des, 2);

    if (result == '1')
        subtitulo = 'Tendencia de puntos NO sobrepasan los limites de advertencia';
    else
        subtitulo = 'Tendencia de puntos SI sobrepasan los limites de advertencia';

    $(function () {
        $('#container').highcharts({
            legend: {
                enabled: false
            },
            data: {
                table: document.getElementById('tabla')
            },
            title: {
                text: titulo,
                x: -20
            },
            subtitle: {
                text: subtitulo,
                floating: true,
                align: 'right',
                x: -20,
                verticalAlign: 'bottom',
                y: -75
            },
            yAxis: {
                title: {
                    text: labely
                },
                min: limite4,
                max: limite1,
                plotLines: [
                    {color: '#000000', width: 2, value: media},
                    {color: '#FF0000', width: 2, value: limite1},
                    {color: '#FFFF00', width: 2, value: limite2},
                    {color: '#FFFF00', width: 2, value: limite3},
                    {color: '#FF0000', width: 2, value: limite4}
                ]
            },
            xAxis: {
                title: {
                    text: labelx
                },
                gridLineWidth: grid,
                tickInterval: ticks
            },
            tooltip: {
                enabled: true
            }
        });
    });
}

function GeneraGrafico2() {
    document.getElementById('tr_grafico2').style.display = '';
    labelx = 'Concentraci�n (ug/ml)';
    labely = '�rea';
    ticks = 0.5;
    grid = 0;

    if ($('#graf_pesa2').val() == 'G') {
        titulo = 'Curva n-Undecano';
    } else if ($('#graf_pesa2').val() == 'H') {
        titulo = 'Curva 2,6-Dimetilfenol';
    } else if ($('#graf_pesa2').val() == 'I') {
        titulo = 'Curva 2,6-Dimetilanilina';
    }

    $(function () {
        $('#container2').highcharts({
            legend: {
                enabled: false
            },
            data: {
                table: document.getElementById('tabla')
            },
            title: {
                text: titulo,
                x: -20 //center
            },
            yAxis: {
                title: {
                    text: labely
                }
            },
            xAxis: {
                title: {
                    text: labelx
                },
                gridLineWidth: grid,
                tickInterval: ticks
            },
            tooltip: {
                enabled: false
            }
        });
    });
}

function Procesar(tipo) {
    if (!confirm('Modificar estado?'))
        return;

    var parametros = {
        '_AJAX': 1,
        'paso': 9,
        'cs': $('#id').val(),
        'accion': tipo
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    break;
                case '1':
                    location.reload();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function AgregaInvolucrado() {
    if (Mal(1, $('#usuario').val())) {
        OMEGA('Debe seleccionar el usuario');
        return;
    }

    if (!confirm('Agregar involucrado?'))
        return;

    var parametros = {
        '_AJAX': 1,
        'paso': 7,
        'cs': $('#id').val(),
        'usuario': $('#usuario').val(),
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    break;
                case '1':
                    location.reload();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}

function UsuariosLista() {
    window.open(__SHELL__ + "?list3=1", "", "width=400,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list3=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function UsuarioEscoge(id, nombre) {
    opener.document.getElementById('usuario').value = id;
    opener.document.getElementById('tmp').value = nombre;
    window.close();
}