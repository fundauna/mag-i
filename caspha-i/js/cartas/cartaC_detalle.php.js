function DocumentosZip(_ARCHIVO, _MODULO){
	var ruta = "../seguridad/documentos.php?ARCHIVO=" + _ARCHIVO + "&MODULO=" + _MODULO + "&ACCESO=00";
	window.open(ruta,"","width=100,height=100,scrollbars=yes,status=no");
	//window.showModalDialog(ruta, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}

function datos(accion){	
	if($('#archivo').val()=='' ){
		OMEGA('Debe adjuntar un documento');
		return;
	}	
	
	if(!confirm('Ingresar datos?')) return;
	btn.disabled = true;
	ALFA('Por favor espere....');
	document.form.submit();
}

function GeneraGrafico(){	
	document.getElementById('tr_grafico').style.display = '';

	$(function () {
		$('#container').highcharts({
			legend: {
            	enabled: false
       		},
			data: {
				table: document.getElementById('tabla')
			},
			title: {
                text: '',
                x: -20 //center
            },
            yAxis: {
                title: {
                    text: ''
                },
				plotLines: [
					{color: '#FF0000',width: 2,value: 140},
					{color: '#00FF00',width: 2,value: $('#graf_prom').val()},
					{color: '#FF0000',width: 2,value: 60}
				]
            }/*,
          	tooltip: {
				enabled: false
			}*/
		});
	});
}

function Generar(){	
	if($('#graf_desde').val()=='' || $('#graf_hasta').val()==''){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	if($('#graf_prom').val()==''){
		OMEGA('Debe indicar el promedio de validación');
		return;
	}
	
	var parametros = {
		'_AJAX' : 1,
		'tipo' : $('#graf_tipo').val(),
		'desde' : $('#graf_desde').val(),
		'hasta' : $('#graf_hasta').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response){
			if(_response == ''){
				OMEGA("No hay datos que mostrar");
				return;
			}
			BETA();
			document.getElementById('td_tabla').innerHTML = _response;
			GeneraGrafico();
			return;
		}
	});
}