function __lolo() {
    document.getElementById('conc').value = '1000';
    //
    document.getElementById('blanco0').value = '0.0003';
    document.getElementById('blanco1').value = '0.0000';
    document.getElementById('blanco2').value = '0.0000';
    document.getElementById('blanco3').value = '0.0001';
    /*document.getElementById('blanco4').value = '-0.0001';
     document.getElementById('blanco5').value = '0.0003';
     document.getElementById('blanco6').value = '0.0004';
     document.getElementById('blanco7').value = '0.0000';
     document.getElementById('blanco8').value = '0.0000';
     document.getElementById('blanco9').value = '0.0000';*/
    //
    document.getElementById('abs10').value = '0.0952';
    document.getElementById('abs11').value = '0.0959';
    document.getElementById('abs12').value = '0.0954';
    document.getElementById('abs13').value = '0.0966';
    /*document.getElementById('abs14').value = '0.0976';
     document.getElementById('abs15').value = '0.0955';
     document.getElementById('abs16').value = '0.0964';
     document.getElementById('abs17').value = '0.0971';
     document.getElementById('abs18').value = '0.0974';
     document.getElementById('abs19').value = '0.0951';*/
    //
    document.getElementById('abs20').value = '0.1859';
    document.getElementById('abs21').value = '0.1817';
    document.getElementById('abs22').value = '0.1860';
    document.getElementById('abs23').value = '0.1819';
    /*document.getElementById('abs24').value = '0.1852';
     document.getElementById('abs25').value = '0.1863';
     document.getElementById('abs26').value = '0.1865';
     document.getElementById('abs27').value = '0.1828';
     document.getElementById('abs28').value = '0.1837';
     document.getElementById('abs29').value = '0.1853';*/
    //
    document.getElementById('abs30').value = '0.2746';
    document.getElementById('abs31').value = '0.2675';
    document.getElementById('abs32').value = '0.2663';
    document.getElementById('abs33').value = '0.2673';
    /*document.getElementById('abs34').value = '0.2721';
     document.getElementById('abs35').value = '0.2695';
     document.getElementById('abs36').value = '0.2759';
     document.getElementById('abs37').value = '0.2707';
     document.getElementById('abs38').value = '0.2668';
     document.getElementById('abs39').value = '0.2721';*/
    //
    document.getElementById('abs40').value = '0.3608';
    document.getElementById('abs41').value = '0.3581';
    document.getElementById('abs42').value = '0.3552';
    document.getElementById('abs43').value = '0.3666';
    /*document.getElementById('abs44').value = '0.3584';
     document.getElementById('abs45').value = '0.3571';
     document.getElementById('abs46').value = '0.3615';
     document.getElementById('abs47').value = '0.3663';
     document.getElementById('abs48').value = '0.3465';
     document.getElementById('abs49').value = '0.3507';*/
    //
    document.getElementById('abs50').value = '0.2281';
    document.getElementById('abs51').value = '0.2231';
    document.getElementById('abs52').value = '0.2258';
    document.getElementById('abs53').value = '0.2252';
    document.getElementById('abs54').value = '0.2271';
    document.getElementById('abs55').value = '0.2276';
    document.getElementById('abs56').value = '0.2280';
    document.getElementById('abs57').value = '0.2238';
    document.getElementById('abs58').value = '0.2309';
    document.getElementById('abs59').value = '0.2273';
    //
    document.getElementById('volA0').value = '0.0';
    document.getElementById('volA1').value = '200.0';
    document.getElementById('volA2').value = '400.0';
    document.getElementById('volA3').value = '600.0';
    document.getElementById('volA4').value = '800.0';
    document.getElementById('volA5').value = '500.0';
    //
    document.getElementById('volB0').value = '100.00';
    document.getElementById('volB1').value = '100.00';
    document.getElementById('volB2').value = '100.00';
    document.getElementById('volB3').value = '100.00';
    document.getElementById('volB4').value = '100.00';
    document.getElementById('volB5').value = '100.00';
    CalculaTotal();
}

function Pendiente() {
    var x2 = 0;
    var y = 0;
    var x = 0;
    var xy = 0;
    var cantidad = 5;
    var promP = promC = 0;

    for (i = 0; i < cantidad; i++) {
        x2 += parseFloat(document.getElementById('volC' + i).value * document.getElementById('volC' + i).value);
        y += parseFloat(document.getElementById('volP' + i).value);
        x += parseFloat(document.getElementById('volC' + i).value);
        xy += parseFloat(document.getElementById('volC' + i).value * document.getElementById('volP' + i).value);
    }
    var b = (cantidad * xy - x * y) / (cantidad * x2 - x * x);
    document.getElementById('pendiente').innerHTML = _RED2(b, 4);
    //INTERCEPTO
    for (i = 0; i < cantidad; i++) {
        promC += parseFloat(document.getElementById('volC' + i).value);
        promP += parseFloat(document.getElementById('volP' + i).value);
    }
    promC /= cantidad;
    promP /= cantidad;
    var B = promP - (b * promC);
    document.getElementById('intercepto').innerHTML = _RED2(B, 4);
}

function Curvas() {
    var sumCN = sumAR = sumA = sumB = sumC = sumD = sumE = sumF = sumG = sumH = sumI = 0;
    var promCN = promAR = promA = promB = promC = promD = promE = promF = promG = promH = promI = 0;
    
    Pendiente();

    var CNprom = CNsum = ARprom = ARsum = 0;

    for (i = 0; i < 5; i++) {
        CNsum += parseFloat(document.getElementById('volC' + i).value);
        ARsum += parseFloat(document.getElementById('volP' + i).value);
    }

    CNprom = CNsum / 5;
    ARprom = ARsum / 5;

    for (i = 0; i < 5; i++) {
        document.getElementById('_CN' + i).innerHTML = document.getElementById('volC' + i).value;
        document.getElementById('_AR' + i).innerHTML = document.getElementById('volP' + i).value;
        document.getElementById('_A' + i).innerHTML = Math.pow(document.getElementById('volC' + i).value, 2);
        document.getElementById('_B' + i).innerHTML = (document.getElementById('volC' + i).value - CNprom);
        document.getElementById('_C' + i).innerHTML = Math.pow(document.getElementById('_B' + i).innerHTML, 2);
        document.getElementById('_D' + i).innerHTML = (document.getElementById('volP' + i).value - ARprom);
        document.getElementById('_E' + i).innerHTML = Math.pow(document.getElementById('_D' + i).innerHTML, 2);
        document.getElementById('_F' + i).innerHTML = document.getElementById('_B' + i).innerHTML * document.getElementById('_D' + i).innerHTML;
        document.getElementById('_G' + i).innerHTML = (0.0442 * document.getElementById('volC' + i).value) + 0.005;
        document.getElementById('_H' + i).innerHTML = Math.abs(document.getElementById('volP' + i).value - document.getElementById('_G0').innerHTML);
        document.getElementById('_I' + i).innerHTML = Math.pow(document.getElementById('_H' + i).innerHTML, 2);
    }

    for (i = 0; i < 5; i++) {
        sumA += parseFloat(document.getElementById('_A' + i).innerHTML);
        sumB += parseFloat(document.getElementById('_B' + i).innerHTML);
        sumC += parseFloat(document.getElementById('_C' + i).innerHTML);
        sumD += parseFloat(document.getElementById('_D' + i).innerHTML);
        sumE += parseFloat(document.getElementById('_E' + i).innerHTML);
        sumF += parseFloat(document.getElementById('_F' + i).innerHTML);
        sumG += parseFloat(document.getElementById('_G' + i).innerHTML);
        sumH += parseFloat(document.getElementById('_H' + i).innerHTML);
        sumI += parseFloat(document.getElementById('_I' + i).innerHTML);
    }
    
    promA = sumA / 5;
    promB = sumB / 5;
    promC = sumC / 5;
    promD = sumD / 5;
    promE = sumE / 5;
    promF = sumF / 5;
    promG = sumG / 5;
    promH = sumH / 5;
    promI = sumI / 5;
    
    document.getElementById('r').innerHTML = _RED2(sumF / Math.sqrt(sumC * sumE), 6);
    var SYX = Math.sqrt(sumI / (5 - 2));
    console.log(SYX);
    console.log(sumI);
    document.getElementById('sm').innerHTML = _RED2(SYX / (Math.sqrt(sumC)), 6);
    document.getElementById('sb').innerHTML = _RED2(SYX * (Math.sqrt(sumA / (5 * sumC))), 6);
     
    /*for (i = 0; i < 5; i++) {
     document.getElementById('_CN' + i).innerHTML = document.getElementById('volC' + i).value;
     document.getElementById('_A' + i).innerHTML = Math.pow(document.getElementById('volC' + i).value, 2);
     document.getElementById('_G' + i).innerHTML = document.getElementById('pendiente').innerHTML * document.getElementById('_CN' + i).innerHTML + parseFloat(document.getElementById('intercepto').innerHTML);
     document.getElementById('_H' + i).innerHTML = Math.abs(document.getElementById('_AR' + i).innerHTML - document.getElementById('_G0').innerHTML);
     document.getElementById('_I' + i).innerHTML = Math.pow(document.getElementById('_H' + i).innerHTML, 2);
     sumA += parseFloat(document.getElementById('_A' + i).innerHTML);
     sumG += parseFloat(document.getElementById('_G' + i).innerHTML);
     sumH += parseFloat(document.getElementById('_H' + i).innerHTML);
     sumI += parseFloat(document.getElementById('_I' + i).innerHTML);
     sumCN += parseFloat(document.getElementById('volC' + i).value);
     sumAR += parseFloat(document.getElementById('volP' + i).value);
     }
     document.getElementById('_CN' + i).innerHTML = sumCN / 5;
     document.getElementById('_AR' + i).innerHTML = sumAR / 5;
     document.getElementById('_A' + i).innerHTML = sumA / 5;
     document.getElementById('_G' + i).innerHTML = sumG / 5;
     document.getElementById('_H' + i).innerHTML = sumH / 5;
     document.getElementById('_I' + i).innerHTML = sumI / 5;
     document.getElementById('_CN6').innerHTML = sumCN;
     document.getElementById('_AR6').innerHTML = sumAR;
     document.getElementById('_A6').innerHTML = sumA;
     document.getElementById('_G6').innerHTML = sumG;
     document.getElementById('_H6').innerHTML = sumH;
     document.getElementById('_I6').innerHTML = sumI;
     //
     for (i = 0; i < 5; i++) {
     document.getElementById('_B' + i).innerHTML = document.getElementById('_CN' + i).innerHTML - document.getElementById('_CN5').innerHTML;
     document.getElementById('_C' + i).innerHTML = Math.pow(document.getElementById('_B' + i).innerHTML, 2);
     document.getElementById('_D' + i).innerHTML = _RED2(document.getElementById('_AR' + i).innerHTML - document.getElementById('_AR5').innerHTML, 5);
     document.getElementById('_E' + i).innerHTML = Math.pow(document.getElementById('_D' + i).innerHTML, 2);
     document.getElementById('_F' + i).innerHTML = document.getElementById('_B' + i).innerHTML * document.getElementById('_D' + i).innerHTML;
     sumB += parseFloat(document.getElementById('_B' + i).innerHTML);
     sumC += parseFloat(document.getElementById('_C' + i).innerHTML);
     sumD += parseFloat(document.getElementById('_D' + i).innerHTML);
     sumE += parseFloat(document.getElementById('_E' + i).innerHTML);
     sumF += parseFloat(document.getElementById('_F' + i).innerHTML);
     }
     document.getElementById('_B' + i).innerHTML = sumB / 5;
     document.getElementById('_C' + i).innerHTML = sumC / 5;
     document.getElementById('_D' + i).innerHTML = sumD / 5;
     document.getElementById('_E' + i).innerHTML = sumE / 5;
     document.getElementById('_F' + i).innerHTML = sumF / 5;
     document.getElementById('_B6').innerHTML = sumB;
     document.getElementById('_C6').innerHTML = sumC;
     document.getElementById('_D6').innerHTML = sumD;
     document.getElementById('_E6').innerHTML = sumE;
     document.getElementById('_F6').innerHTML = sumF;
     //
     document.getElementById('r').innerHTML = _RED2(document.getElementById('_F6').innerHTML / Math.sqrt(document.getElementById('_C6').innerHTML * document.getElementById('_E6').innerHTML), 6);
     var SYX = Math.sqrt(document.getElementById('_I6').innerHTML / (5 - 2));
     document.getElementById('sm').innerHTML = _RED2(SYX / (Math.sqrt(document.getElementById('_C6').innerHTML)), 5);
     document.getElementById('sb').innerHTML = _RED2(SYX * (Math.sqrt(document.getElementById('_A6').innerHTML / (5 * document.getElementById('_C6').innerHTML))), 5);
     */
}

function CalculaTotal() {
    var linea = document.getElementsByName("blanco").length;
    var prom1 = prom2 = prom3 = prom4 = prom5 = prom6 = 0;

    for (i = 0; i < linea; i++) {
        prom1 += parseFloat(document.getElementById('blanco' + i).value);
        prom2 += parseFloat(document.getElementById('abs1' + i).value);
        prom3 += parseFloat(document.getElementById('abs2' + i).value);
        prom4 += parseFloat(document.getElementById('abs3' + i).value);
        prom5 += parseFloat(document.getElementById('abs4' + i).value);
    }
    
    for (i = 0; i < 10; i++) {
        prom6 += parseFloat(document.getElementById('abs5' + i).value);
    }
    
    var can = 4;

    document.getElementById('volP0').value = _RED2(prom1 / can, 4);
    document.getElementById('volP1').value = _RED2(prom2 / can, 4);
    document.getElementById('volP2').value = _RED2(prom3 / can, 4);
    document.getElementById('volP3').value = _RED2(prom4 / can, 4);
    document.getElementById('volP4').value = _RED2(prom5 / can, 4);
    document.getElementById('volP5').value = _RED2(prom6 / 10, 4);
    //CURVAS
    document.getElementById('_AR0').innerHTML = _RED2(prom1 / linea, 5);
    document.getElementById('_AR1').innerHTML = _RED2(prom2 / linea, 5);
    document.getElementById('_AR2').innerHTML = _RED2(prom3 / linea, 5);
    document.getElementById('_AR3').innerHTML = _RED2(prom4 / linea, 5);
    document.getElementById('_AR4').innerHTML = _RED2(prom5 / linea, 5);
    //
    var modalidad = 0;

    if (document.getElementById('volP5').value == '1') {
        modalidad = 0.2;
    } else {
        modalidad = 0.1;
    }

    if (parseFloat(document.getElementById('volP5').value) >= modalidad)
        document.getElementById('dentro').innerHTML = 'S�';
    else
        document.getElementById('dentro').innerHTML = 'No';
    //
    var estandar = _FVAL(document.getElementById('conc').value);

    document.getElementById('volC0').value = estandar * ((document.getElementById('volA0').value / 1000) / document.getElementById('volB0').value);
    document.getElementById('volC1').value = estandar * ((document.getElementById('volA1').value / 1000) / document.getElementById('volB1').value);
    document.getElementById('volC2').value = estandar * ((document.getElementById('volA2').value / 1000) / document.getElementById('volB2').value);
    document.getElementById('volC3').value = estandar * ((document.getElementById('volA3').value / 1000) / document.getElementById('volB3').value);
    document.getElementById('volC4').value = estandar * ((document.getElementById('volA4').value / 1000) / document.getElementById('volB4').value);
    document.getElementById('volC5').value = estandar * ((document.getElementById('volA5').value / 1000) / document.getElementById('volB5').value);
    _FLOAT(document.getElementById('volC0'));
    _FLOAT(document.getElementById('volC1'));
    _FLOAT(document.getElementById('volC2'));
    _FLOAT(document.getElementById('volC3'));
    _FLOAT(document.getElementById('volC4'));
    _FLOAT(document.getElementById('volC5'));

    Curvas();
}

function Redondear(txt, decimales) {
    _RED(txt, decimales);
    CalculaTotal();
}

function CambiaEtiquetas(opc) {
    switch (opc) {
        case '1':
            //document.getElementById('label1').innerHTML = 'llama';
            document.getElementById('label2').innerHTML = '2 mg/ml';
            document.getElementById('label3').innerHTML = '4 mg/ml';
            document.getElementById('label4').innerHTML = '6 mg/ml';
            document.getElementById('label5').innerHTML = '8 mg/ml';
            document.getElementById('label6').innerHTML = '5 mg/ml';
            document.getElementById('label7').innerHTML = 'mg/ml';
            document.getElementById('label8').innerHTML = '0.2';
            break;
        case '2':
            //document.getElementById('label1').innerHTML = 'horno';
            document.getElementById('label2').innerHTML = '10 &micro;g/L';
            document.getElementById('label3').innerHTML = '20 &micro;g/L';
            document.getElementById('label4').innerHTML = '30 &micro;g/L';
            document.getElementById('label5').innerHTML = '40 &micro;g/L';
            document.getElementById('label6').innerHTML = '30 &micro;g/L';
            document.getElementById('label7').innerHTML = '&micro;g/L';
            document.getElementById('label8').innerHTML = '0.1';
            break;
        default:
            //document.getElementById('label1').innerHTML = 'no definido';
            document.getElementById('label2').innerHTML = '';
            document.getElementById('label3').innerHTML = '';
            document.getElementById('label4').innerHTML = '';
            document.getElementById('label5').innerHTML = '';
            document.getElementById('label6').innerHTML = '';
            document.getElementById('label7').innerHTML = '';
            document.getElementById('label8').innerHTML = '';
            break;
    }
}

function Generar() {
    var parametros = {
        '_AJAX': 1,
        'paso': 3,
        'x0': $('#volC0').val(),
        'x1': $('#volC1').val(),
        'x2': $('#volC2').val(),
        'x3': $('#volC3').val(),
        'x4': $('#volC4').val(),
        'y0': $('#volP0').val(),
        'y1': $('#volP1').val(),
        'y2': $('#volP2').val(),
        'y3': $('#volP3').val(),
        'y4': $('#volP4').val()
    }

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            if (_response == '') {
                OMEGA("No hay datos que mostrar");
                return;
            }
            BETA();
            document.getElementById('td_tabla').innerHTML = _response;
            GeneraGrafico();
            return;
        }
    });
}

function GeneraGrafico() {
    document.getElementById('tr_grafico').style.display = '';

    $(function () {
        $('#container').highcharts({
            legend: {
                enabled: false
            },
            data: {
                table: document.getElementById('tabla')
            },
            title: {
                text: 'Curva de calibracion',
                x: -20 //center
            },
            yAxis: {
                title: {
                    text: 'Absorbancia'
                }
            },
            xAxis: {
                title: {
                    text: 'Concentraci�n (mg/mL)'
                },
                gridLineWidth: 1
            },
            tooltip: {
                enabled: false
            }
        });
    });
}

function SolicitudAgregar(tipo) {
    var linea = document.getElementsByName("abs5").length;
    var fila = document.createElement("tr");
    var colum = new Array(7);

    colum[0] = document.createElement("td");
    colum[1] = document.createElement("td");
    colum[2] = document.createElement("td");
    colum[3] = document.createElement("td");
    colum[4] = document.createElement("td");
    colum[5] = document.createElement("td");
    colum[6] = document.createElement("td");

    if (tipo == 1) {
        colum[0].innerHTML = '&nbsp;&nbsp;&nbsp;' + (linea + 1);
        colum[1].innerHTML = '<input type="text" name="blanco" id="blanco' + linea + '" class="monto" value="0.00" onblur="Redondear(this, 4);" />';
        colum[2].innerHTML = '<input type="text" name="abs1" id="abs1' + linea + '" class="monto" value="0.00" onblur="Redondear(this, 4);" />';
        colum[3].innerHTML = '<input type="text" name="abs2" id="abs2' + linea + '" class="monto" value="0.00" onblur="Redondear(this, 4);" />';
        colum[4].innerHTML = '<input type="text" name="abs3" id="abs3' + linea + '" class="monto" value="0.00" onblur="Redondear(this, 4);" />';
        colum[5].innerHTML = '<input type="text" name="abs4" id="abs4' + linea + '" class="monto" value="0.00" onblur="Redondear(this, 4);" />';
        colum[6].innerHTML = '<input type="text" name="abs5" id="abs5' + linea + '" class="monto" value="0.00" onblur="Redondear(this, 4);" />';
    } else {
        colum[0].innerHTML = '&nbsp;&nbsp;&nbsp;' + (linea + 1);
        colum[1].innerHTML = '';
        colum[2].innerHTML = '';
        colum[3].innerHTML = '';
        colum[4].innerHTML = '';
        colum[5].innerHTML = '';
        colum[6].innerHTML = '<input type="text" name="abs5" id="abs5' + linea + '" class="monto" value="0.00" onblur="Redondear(this, 4);" />';
    }

    for (i = 0; i < colum.length; i++)
        fila.appendChild(colum[i]);

    fila.style.textAlign = 'center';

    document.getElementById('lolo').appendChild(fila);
}

function datos() {
    if ($('#cod_bal').val() == '') {
        OMEGA('Debe seleccionar el equipo');
        return;
    }

    if (Mal(1, $('#fecha').val())) {
        OMEGA('Debe seleccionar la fecha');
        return;
    }

    if ($('#conc').val() == '') {
        OMEGA('Debe indicar la concentracion estandar');
        return;
    }

    if (!confirm('Modificar datos?'))
        return;

    var parametros = {
        '_AJAX': 1,
        'paso': 1,
        'cs': $('#cs').val(),
        'fecha': $('#fecha').val(),
        'accion': $('#accion').val(),
        'modalidad': $('#modalidad').val(),
        'cod_bal': $('#cod_bal').val(),
        'nombre': $('#nombre').val(),
        'corriente': $('#corriente').val(),
        'origen': $('#origen').val(),
        'longitud': $('#longitud').val(),
        'tiene': $('#tiene').val(),
        'ancho': $('#ancho').val(),
        'conc': $('#conc').val(),
        //
        'volA': vector("volA"),
        'volB': vector("volB")
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            $('#btn').disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesion expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el envio de parametros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '':
                    alert('Tiempo de espera agotado');
                    break;
                    break;
                default:
                    __Paso2(_response);
                    break;
            }
        }
    });
}

/*FUNCIONES PRIVADAS*/
function __Paso2(_cs) {
    var parametros = {
        '_AJAX': 1,
        'paso': 2,
        'cs': _cs,
        //
        'blanco': vector("blanco"),
        'abs1': vector("abs1"),
        'abs2': vector("abs2"),
        'abs3': vector("abs3"),
        'abs4': vector("abs4"),
        'abs5': vector("abs5")
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            $('#btn').disabled = true;
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesion expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el envio de parametros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    BETA();
                    if ($('#accion').val() == 'I') {
                        alert("Transaccion finalizada");
                        window.close();
                    }
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}
/*FUNCIONES PRIVADAS*/

function vector(ctrl) {
    var str = "1=1";
    control = document.getElementsByName(ctrl);
    for (i = 0; i < control.length; i++) {
        str += "&" + control[i].value.replace(/&/g, '');
    }
    return str;
}

function DisponibleEscoge(id, codigo, nombre, linea) {
    opener.document.getElementById('cod_bal').value = id;
    opener.document.getElementById('nom_bal').value = codigo;
    opener.document.getElementById('marca').innerHTML = nombre;
    window.close();
}

function EquiposLista() {
    window.open(__SHELL__ + "?list=1", "", "width=400,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function Procesar(tipo) {
    if (!confirm('Modificar estado?'))
        return;

    var parametros = {
        '_AJAX': 1,
        'paso': 9,
        'cs': $('#cs').val(),
        'accion': tipo
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesion expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el envio de parametros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    break;
                case '1':
                    location.reload();
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}