function Redondear(txt, tipo){
	var decimales = parseInt(document.getElementById('decimales' + tipo).value);
	_RED(txt, decimales);
}

function GeneraGrafico(){	
	document.getElementById('tr_grafico').style.display = '';
	var estandar3 = _RED2(document.getElementById('estandar').value, 2);
	var estandar1 = _RED2(3 * estandar3, 2);
	var estandar2 = _RED2(2 * estandar3, 2);
	var estandar4 = _RED2(estandar3 * -1, 2);
	var estandar5 = _RED2(2 * estandar3 * -1, 2);
	var estandar6 = _RED2(3 * estandar3 * -1, 2);
	
	if( $('#graf_pesa').val()=='A' ) titulo = 'Pesa 1';
	else titulo = 'Pesa 2';

	$(function () {
		$('#container').highcharts({
			legend: {
            	enabled: false
       		},
			data: {
				table: document.getElementById('tabla')
			},
			title: {
                text: titulo + 'g' + ' (' + document.getElementById('marca').innerHTML + ')',
                x: -20 //center
            },
            yAxis: {
                title: {
                    text: 'Masa (g)'
                },
				plotLines: [
					{color: '#FF0000',width: 2,value: estandar1},
					{color: '#FFFF00',width: 2,value: estandar2},
					{color: '#00FF00',width: 2,value: estandar3},
					{color: '#00FF00',width: 2,value: estandar4},
					{color: '#FFFF00',width: 2,value: estandar5},
					{color: '#FF0000',width: 2,value: estandar6}
				]
            },
          	tooltip: {
				enabled: false
			}
		});
	});
}

function EliminaLinea(_cs, _fecha){	
	if(!confirm('Eliminar l�nea?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 3,
		'cs' : _cs,
		'fecha' : _fecha
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesion expirada [Err:0]');break;
				case '-1':alert('Error en el envio de parametros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					alert("Transaccion finalizada");
					location.reload();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Generar(){	
	if($('#graf_desde').val()=='' || $('#graf_hasta').val()==''){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 2,
		'cs' : $('#cs').val(),
		'tipo' : $('#graf_pesa').val(),
		'desde' : $('#graf_desde').val(),
		'hasta' : $('#graf_hasta').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response){
			if(_response == ''){
				OMEGA("No hay datos que mostrar");
				return;
			}
			BETA();
			document.getElementById('td_tabla').innerHTML = _response;
			GeneraGrafico();
			return;
		}
	});
}

function datos(){	
	if($('#tipo_bal').val()==''){
		OMEGA('Debe indicar el tipo de balanza');
		return;
	}
	
	if($('#consecutivo').val()==''){
		OMEGA('Debe indicar el consecutivo');
		return;
	}
	
	if($('#cod_bal').val()==''){
		OMEGA('Debe seleccionar la balanza');
		return;
	}
	
	if( Mal(2, $('#rango').val()) ){
		OMEGA('Debe indicar rango de la pesada');
		return;
	}
	
	if($('#pesa1').val()==''){
		OMEGA('Debe indicar la masa 1');
		return;
	}
	
	if($('#pesa2').val()==''){
		OMEGA('Debe indicar la masa 2');
		return;
	}
	//
	if($('#fecha').val()==''){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	if($('#decimalesA').val()==''){
		OMEGA('Debe indicar los decimales de la pesa 1');
		return;
	}
	
	if($('#masaA').val()==''){
		OMEGA('Debe indicar la masa de la pesa 1');
		return;
	}
	
	if($('#decimalesB').val()==''){
		OMEGA('Debe indicar los decimales de la pesa 2');
		return;
	}
	
	if($('#masaB').val()==''){
		OMEGA('Debe indicar la masa de la pesa 2');
		return;
	}
		
	if(!confirm('Modificar datos?')) return;

	var parametros = {
		'_AJAX' : 1,
		'paso' : 1,
		'cs' : $('#cs').val(),
		'accion' : $('#accion').val(),
		'tipo_bal' : $('#tipo_bal').val(),
		'cod_bal' : $('#cod_bal').val(),
		'rango' : $('#rango').val(),
		'pesa1' : $('#pesa1').val(),
		'nominal1' : $('#nominal1').val(),
		'certificado1' : $('#certificado1').val(),
		'pesa2' : $('#pesa2').val(),
		'nominal2' : $('#nominal2').val(),
		'certificado2' : $('#certificado2').val(),
		'fecha' : $("#fecha").val(),
		'masaA' : $("#masaA").val(),
		'masaB' : $("#masaB").val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').prop("disabled",true);
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesion expirada [Err:0]');break;
				case '-1':alert('Error en el envio de parametros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').prop("disabled",false);
					break;
				case '':
					alert('Tiempo de espera agotado');break;
					break;
				default:
					location.href = 'carta0_detalle.php?acc=M&ID=' + _response;
					break;
			}
		}
	});
}

function DisponibleEscoge(id, codigo, nombre, linea){
	opener.document.getElementById('cod_bal').value=id;
	opener.document.getElementById('nom_bal').value=codigo;
	opener.document.getElementById('marca').innerHTML=nombre;
	window.close();
}

function EquipoEscoge(cs, codigo, marca, modelo){
	opener.document.getElementById('cod_bal').value=cs;
	opener.document.getElementById('nom_bal').value=codigo;
	opener.document.getElementById('marca').innerHTML = ''+marca+' / '+modelo;
	window.close();
}

function EquiposLista(){
	window.open(__SHELL__ + "?list=1","","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function PesaEscoge(codigo, nominal, vreal, inc, certificado, linea){
	opener.document.getElementById('pesa' + linea).value=codigo;
	opener.document.getElementById('nominal' + linea).value=nominal;
	opener.document.getElementById('certificado' + linea).value=certificado;
	window.close();
}

function PesasLista(_linea){
	window.open(__SHELL__ + "?list2=" + _linea,"","width=600,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list2=" + _linea, this.window, "dialogWidth:600px;dialogHeight:200px;status:no;");
}

function Procesar(tipo){
	if(!confirm('Modificar estado?')) return;

	var parametros = {
		'_AJAX' : 1,
		'paso' : 9,
		'cs' : $('#cs').val(),
		'accion' : tipo
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesion expirada [Err:0]');break;
				case '-1':alert('Error en el envio de parametros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					location.reload();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}