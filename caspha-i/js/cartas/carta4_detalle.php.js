function CalculaTemp(){
	if(document.getElementById('indicacion1').innerHTML != '...' && document.getElementById('temp1').value != '' && document.getElementById('hume1').value != ''){
		var par1 = (parseFloat(document.getElementById('indicacion1').innerHTML) + parseFloat(document.getElementById('indicacion2').innerHTML)) / 2;
		var par2 = (parseFloat(document.getElementById('indicacion2').innerHTML) + parseFloat(document.getElementById('indicacion3').innerHTML)) / 2;
		var par3 = (parseFloat(document.getElementById('hr1').innerHTML) + parseFloat(document.getElementById('hr2').innerHTML)) / 2;
		var par4 = (parseFloat(document.getElementById('hr2').innerHTML) + parseFloat(document.getElementById('hr3').innerHTML)) / 2;

		if(parseFloat(document.getElementById('temp1').value) < par1)
			document.getElementById('temp2').value = document.getElementById('temp1').value - document.getElementById('error1').innerHTML;
		else if(parseFloat(document.getElementById('temp1').value) <= par2)
			document.getElementById('temp2').value = document.getElementById('temp1').value - document.getElementById('error2').innerHTML;
		else
			document.getElementById('temp2').value = document.getElementById('temp1').value - document.getElementById('error3').innerHTML;
		//
		if(parseFloat(document.getElementById('hume1').value) < par3)
			document.getElementById('hume2').value = document.getElementById('hume1').value - document.getElementById('err_hr1').innerHTML;
		else if(parseFloat(document.getElementById('hume1').value) <= par4)
			document.getElementById('hume2').value = document.getElementById('hume1').value - document.getElementById('err_hr2').innerHTML;
		else
			document.getElementById('hume2').value = document.getElementById('hume1').value - document.getElementById('err_hr3').innerHTML;
	}
}

function Redondear(txt, tipo){
	var decimales = parseInt(document.getElementById('decimales' + tipo).value);
	_RED(txt, decimales);
}

function GeneraGrafico(){	
	document.getElementById('tr_grafico').style.display = '';
	var media = _RED2(document.getElementById('graf_media').value, 2);
	var limite1 = _RED2(document.getElementById('graf_limite1').value, 2);
	var limite2 = _RED2(document.getElementById('graf_limite2').value, 2);
	var limite3 = _RED2(document.getElementById('graf_limite3').value, 2);
	var limite4 = _RED2(document.getElementById('graf_limite4').value, 2);
	
	if( $('#graf_pesa').val()=='temp2' ){
		titulo = 'Control de temperatura';
		ejey = 'Temperatura (C)';
	}else{
		titulo = 'Control de humedad';
		ejey = '% Humedad relativa';
	}

	$(function () {
		$('#container').highcharts({
			legend: {
            	enabled: false
       		},
			data: {
				table: document.getElementById('tabla')
			},
			title: {
                text: titulo,
                x: -20 //center
            },
			subtitle: {
            	text: $('#cod_ter').val() + ' ' + document.getElementById('cuarto').innerHTML
        	},
            yAxis: {
                title: {
                    text: ejey
                },
				plotLines: [
					{color: '#0066FF',width: 2,value: media},
					{color: '#FF0000',width: 2,value: limite1},
					{color: '#00FF00',width: 2,value: limite2},
					{color: '#00FF00',width: 2,value: limite3},
					{color: '#FF0000',width: 2,value: limite4}
				]
            },
          	tooltip: {
				enabled: false
			}
		});
	});
}

function EliminaLinea(_cs, _fecha, _hora){	
	if(!confirm('Eliminar l�nea?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 3,
		'cs' : _cs,
		'fecha' : _fecha,
		'hora' : _hora
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					alert("Transaccion finalizada");
					location.reload();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Generar(){	
	if($('#graf_desde').val()=='' || $('#graf_hasta').val()==''){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 2,
		'cs' : $('#cs').val(),
		'tipo' : $('#graf_pesa').val(),
		'desde' : $('#graf_desde').val(),
		'hasta' : $('#graf_hasta').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response){
			if(_response == ''){
				OMEGA("No hay datos que mostrar");
				return;
			}
			BETA();
			document.getElementById('td_tabla').innerHTML = _response;
			GeneraGrafico();
			return;
		}
	});
}

function datos(){	
	if($('#cod_ter').val()==''){
		OMEGA('Debe indicar el termohig�metro');
		return;
	}
	
	if($('#fecha').val()==''){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	if($('#hora').val()==''){
		OMEGA('Debe indicar la hora');
		return;
	}
	
	if($('#temp1').val()==''){
		OMEGA('Debe indicar la temperatura');
		return;
	}
	
	if($('#temp2').val()==''){
		OMEGA('Debe indicar la temperatura corregida');
		return;
	}
	
	if($('#hume1').val()==''){
		OMEGA('Debe indicar la humedad');
		return;
	}
	
	if($('#hume2').val()==''){
		OMEGA('Debe indicar la humedad relativa');
		return;
	}
		
	if(!confirm('Modificar datos?')) return;

	var parametros = {
		'_AJAX' : 1,
		'paso' : 1,
		'cs' : $('#cs').val(),
		'accion' : $('#accion').val(),
		//
		'cod_ter' : $('#cod_ter').val(),
		'obs' : $('#obs').val(),
		'cuarto' : document.getElementById('cuarto').innerHTML,
		'limites' : document.getElementById('limites').innerHTML,
		'humedad' : document.getElementById('humedad').innerHTML,
		'indicacion1' : document.getElementById('indicacion1').innerHTML,
		'error1' : document.getElementById('error1').innerHTML,
		'indicacion2' : document.getElementById('indicacion2').innerHTML,
		'error2' : document.getElementById('error2').innerHTML,
		'indicacion3' : document.getElementById('indicacion3').innerHTML,
		'error3' : document.getElementById('error3').innerHTML,
		'hr1' : document.getElementById('hr1').innerHTML,
		'err_hr1' : document.getElementById('err_hr1').innerHTML,
		'hr2' : document.getElementById('hr2').innerHTML,
		'err_hr2' : document.getElementById('err_hr2').innerHTML,
		'hr3' : document.getElementById('hr3').innerHTML,
		'err_hr3' : document.getElementById('err_hr3').innerHTML,
		//
		'fecha' : $('#fecha').val(),
		'hora' : $('#hora').val(),
		'temp1' : $('#temp1').val(),
		'temp2' : $('#temp2').val(),
		'hume1' : $('#hume1').val(),
		'hume2' : $("#hume2").val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').prop("disabled",false);
					break;
				case '':
					alert('Tiempo de espera agotado');break;
					break;
				default:
					location.href = 'carta4_detalle.php?acc=M&ID=' + _response;
					break;
			}
		}
	});
}

function TermoEscoge(codigo, cuarto, limites, indicacion1, error1, indicacion2, error2, indicacion3, error3, humedad, hr1, err_hr1, hr2, err_hr2, hr3, err_hr3){
	opener.document.getElementById('cod_ter').value=codigo;
	opener.document.getElementById('cuarto').innerHTML=cuarto;
	opener.document.getElementById('limites').innerHTML=limites;
	opener.document.getElementById('indicacion1').innerHTML=indicacion1;
	opener.document.getElementById('error1').innerHTML=error1;
	opener.document.getElementById('indicacion2').innerHTML=indicacion2;
	opener.document.getElementById('error2').innerHTML=error2;
	opener.document.getElementById('indicacion3').innerHTML=indicacion3;
	opener.document.getElementById('error3').innerHTML=error3;
	opener.document.getElementById('humedad').innerHTML=humedad;
	opener.document.getElementById('hr1').innerHTML=hr1;
	opener.document.getElementById('err_hr1').innerHTML=err_hr1;
	opener.document.getElementById('hr2').innerHTML=hr2;
	opener.document.getElementById('err_hr2').innerHTML=err_hr2;
	opener.document.getElementById('hr3').innerHTML=hr3;
	opener.document.getElementById('err_hr3').innerHTML=err_hr3;
	opener.CalculaTemp();
	window.close();
}

function TermosLista(_linea){
	window.open(__SHELL__ + "?list=" + _linea,"","width=500,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=" + _linea, this.window, "dialogWidth:500px;dialogHeight:200px;status:no;");
}

function hora(num){
	if(num.length < 3)
		return ''+num+':00';
}

function Procesar(tipo){
	if(!confirm('Modificar estado?')) return;

	var parametros = {
		'_AJAX' : 1,
		'paso' : 9,
		'cs' : $('#cs').val(),
		'accion' : tipo
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					location.reload();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}