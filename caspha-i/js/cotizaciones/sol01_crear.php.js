function CambiaTipoMetodo(_valor){
	if(_valor == '2') document.getElementById('tr_metodo').style.display = '';
	else document.getElementById('tr_metodo').style.display = 'none';
}

function CambiaTipoDiscre(_valor){
	if(_valor == '1') document.getElementById('tr_discre').style.display = '';
	else document.getElementById('tr_discre').style.display = 'none';
}

function CambiaTipoAnalisis(){
	var linea = document.getElementsByName("analisis[]").length;
	document.getElementById('tipoE').checked = false;
	
	for(i=0;i<linea;i++){
		if(document.getElementById('tipo'+i).value == '1'){
			document.getElementById('tipoE').checked = true;
			return;
		}
	}
}

function ValidaExiste2(_id){
	if(_id=='') return false;
	
	var control = document.getElementsByName('impurezas[]');
	for(i=0;i<control.length;i++){
		if(document.getElementById('codigoB'+i).value == _id) return true
	}
	return false;
}

function ValidaExiste(_id){
	if(_id=='') return false;
	
	var control = document.getElementsByName('analisis[]');
	for(i=0;i<control.length;i++){
		if(document.getElementById('codigo'+i).value == _id) return true
	}
	return false;
}

function MacroAnalisisLista(linea){
	window.open(__SHELL__ + "?list="+linea,"","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list="+linea, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function ImpurezasLista(linea){
	window.open(__SHELL__ + "?list2="+linea,"","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list2="+linea, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function AnalisisEscoge(linea, id, nombre, costo){
	if( opener.ValidaExiste(id) ){
		alert('El an�lisis solicitado ya se encuentra en la solicitud');
		return;
	}
	opener.document.getElementById('codigo'+linea).value = id;
	opener.document.getElementById('analisis'+linea).value = nombre;
	
	window.close();
}

function ImpurezasEscoge(linea, id, nombre){
	if( opener.ValidaExiste2(id) ){
		alert('La impureza ya se encuentra en la solicitud');
		return;
	}
	opener.document.getElementById('codigoB'+linea).value = id;
	opener.document.getElementById('impurezas'+linea).value = nombre;
	window.close();
}

function AnalisisMas(){
	var linea = document.getElementsByName("analisis[]").length;
	var fila = document.createElement("tr");
	var colum = new Array(6);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	colum[4] = document.createElement("td");
	colum[5] = document.createElement("td");
	
	colum[0].innerHTML = (linea+1) + '.';
	colum[1].innerHTML = '<input type="text" id="analisis' + linea + '" name="analisis[]" class="lista" readonly onclick="MacroAnalisisLista(' + linea + ')" /><input type="hidden" id="codigo' + linea + '" name="codigo[]" />';
	colum[2].innerHTML = '<input type="text" id="rango' + linea + '" name="rango[]" size="20" maxlength="20">';
	colum[3].innerHTML = '<select id="quela' + linea + '" name="quela[]"><option value="">...</option><option value="0">Disponible</option><option value="1">Total</option><option value="2">Quelatado</option><option value="3">No-Quelatado</option></select>';
	colum[4].innerHTML = '<select id="tipo' + linea + '" name="tipo[]" onchange="CambiaTipoAnalisis()"><option value="">...</option><option value="0">%m/m</option><option value="1">%m/v</option><option value="2">ppm</option></select>';
	colum[5].innerHTML = '<input type="text" name="fuente[]" size="20" maxlength="30">';

	for(i=0;i<colum.length;i++)
		fila.appendChild(colum[i]);
	
	document.getElementById('lolo').appendChild(fila);
}

function ImpurezasMas(){
	var linea = document.getElementsByName("impurezas[]").length;
	var fila = document.createElement("tr");
	var colum = new Array(2);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	
	colum[0].innerHTML = (linea+1) + '.';
	colum[1].innerHTML = '<input type="text" id="impurezas' + linea + '" name="impurezas[]" class="lista2" readonly onclick="ImpurezasLista(' + linea + ')" /><input type="hidden" id="codigoB' + linea + '" name="codigoB[]" />';
	
	for(i=0;i<colum.length;i++)
		fila.appendChild(colum[i]);
	
	document.getElementById('lolo2').appendChild(fila);
}

function SolicitudGenerar(btn){
	 CambiaTipoAnalisis()
	 
	if( Mal(1, $('#formula').val()) ){
		OMEGA('Debe indicar la f�rmula del fertilizante');
		return;
	}
	
	if( Mal(1, $('#comercial').val()) ){
		OMEGA('Debe indicar el nombre comercial');
		return;
	}
	
	if( Mal(1, $('#tipo_form').val()) ){
		OMEGA('Debe indicar el tipo de formulaci�n');
		return;
	}
	
	if( Mal(1, $('#metodo').val()) ){
		OMEGA('Debe indicar el m�todo de an�lisis');
		return;
	}
	
	if($('#metodo').val() == '2' && Mal(1, $('#aporta').val()) ){
		OMEGA('Debe indicar si el cliente aporta documentaci�n');
		return;
	}
	
	if( Mal(1, $('#mezcla').val()) ){
		OMEGA('Debe indicar el tipo de mezcla');
		return;
	}
	
	if( Mal(1, $('#suministro').val()) ){
		OMEGA('Debe indicar quien suministra el est�ndar');
		return;
	}
	
	if($('#discre').val() == '1' && Mal(1, $('#num_sol').val()) ){
		OMEGA('Debe indicar el n�mero de solicitud');
		return;
	}
	
	if($('#discre').val() == '1' && Mal(1, $('#num_mue').val()) ){
		OMEGA('Debe indicar el c�digo externo');
		return;
	}
	
	var control = document.getElementsByName('analisis[]');
	var cant = hay = 0;
	
	for(i=0;i<control.length;i++){
		if(document.getElementById('analisis'+i).value != ''){
			hay++;
			
			if( Mal(1, $('#rango'+i).val()) ){
				OMEGA('Debe indicar la concentraci�n declarada en la l�nea: '+ (i+1));
				return;
			}
			
			//SI NO ES NITROGENO DEBE PONER EL TIPO
			var str = $('#analisis'+i).val();
			if(str.indexOf("Nitr�geno") == -1){
				if( Mal(1, $('#quela'+i).val()) ){
					OMEGA('Debe indicar el tipo en la l�nea: '+ (i+1));
					return;
				}
			}
			
			if( Mal(1, $('#tipo'+i).val()) ){
				OMEGA('Debe indicar la unidad en la l�nea: '+ (i+1));
				return;
			}
		}
	}
	
	var control = document.getElementsByName('codigoB[]');
	for(i=0;i<control.length;i++){
		if(document.getElementById('codigoB'+i).value != ''){
			hay++;
			break;
		}
	}
	
	if(hay < 1){
		OMEGA('No ha indicado ning�n an�lisis');
		return;
	}
	
	if( $('#total').val() == '' || $('#total').val() == '0' ){
		OMEGA('Debe indicar el n�mero de muestras');
		return;
	}
	
	if( Mal(1, $('#nombre_sol').val()) ){
		OMEGA('Debe indicar el nombre del solicitante');
		return;
	}
	
	if( Mal(1, $('#encargado').val()) ){
		OMEGA('Debe indicar el nombre del encargado de las muestras');
		return;
	}
	
	if(!confirm('Ingresar solicitud?')) return;
	ALFA('Por favor espere....');
	btn.disabled = true;
	btn.form.submit();
}