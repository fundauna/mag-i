function BorraIngredientes(){
	var linea = document.getElementsByName("elementos[]").length;
	
	for(i=0;i<linea;i++){
		$('#elementos'+i).val('');
		$('#codigoE'+i).val('');
	}
	//
	var linea = document.getElementsByName("impurezas[]").length;
	
	for(i=0;i<linea;i++){
		$('#impurezas'+i).val('');
		$('#codigoB'+i).val('');
	}
}

function CambiaTipoAnalisis(){
	document.getElementById('densidad').checked = false;
	var linea = document.getElementsByName("elementos[]").length;
	
	for(i=0;i<linea;i++){
		if(document.getElementById('tipoE'+i).value == '1'){
			document.getElementById('densidad').checked = true;
			return;
		}
	}
	
	var linea = document.getElementsByName("analisis[]").length;
	
	for(i=0;i<linea;i++){
		if(document.getElementById('tipo'+i).value == '1'){
			document.getElementById('densidad').checked = true;
			return;
		}
	}
}

function ElementosMas(){
	var linea = document.getElementsByName("elementos[]").length;
	var fila = document.createElement("tr");
	var colum = new Array(5);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	colum[4] = document.createElement("td");
	
	colum[0].innerHTML = (linea+1) + '.';
	colum[1].innerHTML = '<input type="text" id="elementos' + linea + '" name="elementos[]" class="lista" readonly onclick="ElementosLista(' + linea + ')" /><input type="hidden" id="codigoE' + linea + '" name="codigoE[]" />';
	colum[2].innerHTML = '<input type="text" id="rangoE' + linea + '" name="rangoE[]" size="20" maxlength="20">';
	colum[3].innerHTML = '<select id="tipoE' + linea + '" name="tipoE[]" onchange="CambiaTipoAnalisis()"><option value="">...</option><option value="0">%m/m</option><option value="1">%m/v</option><option value="2">ppm</option></select>';
	colum[4].innerHTML = '<input type="text" name="fuenteE[]" size="20" maxlength="30">';

	for(i=0;i<colum.length;i++)
		fila.appendChild(colum[i]);
	
	document.getElementById('lolo').appendChild(fila);
}

function ElementosEscoge(linea, id, nombre){
	if( opener.ValidaExiste3(id) ){
		alert('El ingrediente ya se encuentra en la solicitud');
		return;
	}
	
	opener.document.getElementById('codigoE'+linea).value = id;
	opener.document.getElementById('elementos'+linea).value = nombre;
	window.close();
}

function ElementosLista(linea){
	if( Mal(1, $('#tipo_form').val()) ){
		OMEGA('Debe indicar el tipo de formulaci�n');
		return;
	}
	
	var formulacion = $('#tipo_form').val();
	window.open(__SHELL__ + "?list3="+linea+"&formulacion="+formulacion,"","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list3="+linea+"&formulacion="+formulacion, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function ImpurezasLista(linea){
	if( Mal(1, $('#tipo_form').val()) ){
		OMEGA('Debe indicar el tipo de formulaci�n');
		return;
	}
	
	var formulacion = $('#tipo_form').val();
	window.open(__SHELL__ + "?list2="+linea+"&formulacion="+formulacion,"","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list2="+linea, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function CambiaTipoMetodo(_valor){
	if(_valor == '2') document.getElementById('tr_metodo').style.display = '';
	else document.getElementById('tr_metodo').style.display = 'none';
}

function CambiaTipoDiscre(_valor){
	if(_valor == '1') document.getElementById('tr_discre').style.display = '';
	else document.getElementById('tr_discre').style.display = 'none';
}

function CambiaTipoForm(_valor){
	if(_valor == 'K') document.getElementById('otro').style.display = '';
	else document.getElementById('otro').style.display = 'none';
}

function ValidaExiste3(_id){
	if(_id=='') return false;
	
	var control = document.getElementsByName('elementos[]');
	for(i=0;i<control.length;i++){
		if(document.getElementById('codigoE'+i).value == _id) return true
	}
	return false;
}

function ValidaExiste2(_id){
	if(_id=='') return false;
	
	var control = document.getElementsByName('impurezas[]');
	for(i=0;i<control.length;i++){
		if(document.getElementById('codigoB'+i).value == _id) return true
	}
	return false;
}

function ValidaExiste(_id){
	if(_id=='') return false;
	
	var control = document.getElementsByName('analisis[]');
	for(i=0;i<control.length;i++){
		if(document.getElementById('codigo'+i).value == _id) return true
	}
	return false;
}

function MacroAnalisisLista(linea){
	window.open(__SHELL__ + "?list="+linea,"","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list="+linea, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function AnalisisEscoge(linea, id, nombre, costo){
	if( opener.ValidaExiste(id) ){
		alert('El an�lisis solicitado ya se encuentra en la solicitud');
		return;
	}
	opener.document.getElementById('codigo'+linea).value = id;
	opener.document.getElementById('analisis'+linea).value = nombre;
	//if(id=='') opener.document.getElementById('cant'+linea).value = 0;
	//opener.Totales();
	window.close();
}

function ImpurezasEscoge(linea, id, nombre){
	if( opener.ValidaExiste2(id) ){
		alert('La impureza ya se encuentra en la solicitud');
		return;
	}
	opener.document.getElementById('codigoB'+linea).value = id;
	opener.document.getElementById('impurezas'+linea).value = nombre;
	window.close();
}

function AnalisisMas(){
	var linea = document.getElementsByName("analisis[]").length;
	var fila = document.createElement("tr");
	var colum = new Array(5);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	colum[4] = document.createElement("td");
	
	colum[0].innerHTML = (linea+1) + '.';
	colum[1].innerHTML = '<input type="text" id="analisis' + linea + '" name="analisis[]" class="lista" readonly onclick="MacroAnalisisLista(' + linea + ')" /><input type="hidden" id="codigo' + linea + '" name="codigo[]" />';
	colum[2].innerHTML = '<input type="hidden" id="rango' + linea + '" name="rango[]" size="20" maxlength="20">';
	colum[3].innerHTML = '<select id="tipo' + linea + '" name="tipo[]" onchange="CambiaTipoAnalisis()" style="visibility:hidden"><option value="">...</option><option value="0">%m/m</option><option value="1">%m/v</option><option value="2">ppm</option></select>';
	colum[4].innerHTML = '<input type="hidden" name="fuente[]" size="20" maxlength="30">';

	for(i=0;i<colum.length;i++)
		fila.appendChild(colum[i]);
	
	document.getElementById('lolo2').appendChild(fila);
}

function ImpurezasMas(){
	var linea = document.getElementsByName("impurezas[]").length;
	var fila = document.createElement("tr");
	var colum = new Array(2);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	
	colum[0].innerHTML = (linea+1) + '.';
	colum[1].innerHTML = '<input type="text" id="impurezas' + linea + '" name="impurezas[]" class="lista" readonly onclick="ImpurezasLista(' + linea + ')" /><input type="hidden" id="codigoB' + linea + '" name="codigoB[]" />';

	for(i=0;i<colum.length;i++)
		fila.appendChild(colum[i]);
	
	document.getElementById('lolo3').appendChild(fila);
}

function SolicitudGenerar(btn){
	CambiaTipoAnalisis();
	
	if( Mal(1, $('#cliente').val()) ){
		OMEGA('Debe indicar el cliente');
		return;
	} 
	
	if( Mal(1, $('#contiene').val()) ){
		OMEGA('Debe indicar lo que contiene el producto');
		return;
	}
	
	if( Mal(1, $('#tipo_form').val()) ){
		OMEGA('Debe indicar el tipo de formulaci�n');
		return;
	}else if( $('#tipo_form').val()=='3' && Mal(1, $('#otro').val()) ){
		OMEGA('Debe indicar el tipo de formulaci�n');
		return;
	}
	
	if( Mal(1, $('#metodo').val()) ){
		OMEGA('Debe indicar el m�todo de an�lisis');
		return;
	}
	
	if($('#metodo').val() == '2' && Mal(1, $('#aporta').val()) ){
		OMEGA('Debe indicar si el cliente aporta documentaci�n');
		return;
	}
	
	if( Mal(1, $('#suministro').val()) ){
		OMEGA('Debe indicar quien suministra el est�ndar');
		return;
	}
	
	if($('#discre').val() == '1' && Mal(1, $('#num_sol').val()) ){
		OMEGA('Debe indicar el n�mero de solicitud');
		return;
	}
	
	if($('#discre').val() == '1' && Mal(1, $('#num_mue').val()) ){
		OMEGA('Debe indicar el c�digo externo');
		return;
	}
	
	var hay = 0;
	var control = document.getElementsByName('elementos[]');
	
	for(i=0;i<control.length;i++){
		if(document.getElementById('elementos'+i).value != ''){
			hay++;
			
			if( Mal(1, $('#rangoE'+i).val()) ){
				OMEGA('Debe indicar la concentraci�n declarada en la l�nea: '+ (i+1));
				return;
			}
			
			if( Mal(1, $('#tipoE'+i).val()) ){
				OMEGA('Debe indicar la unidad en la l�nea: '+ (i+1));
				return;
			}
		}
	}
	
	if(hay < 1){
		OMEGA('No ha indicado ning�n ingrediente activo');
		return;
	}
	
	var hay = 0;
	var control = document.getElementsByName('analisis[]');
	
	for(i=0;i<control.length;i++){
		if(document.getElementById('analisis'+i).value != ''){
			hay++;
			
			/*if( Mal(1, $('#rango'+i).val()) ){
				OMEGA('Debe indicar la concentraci�n declarada en la l�nea: '+ (i+1));
				return;
			}
			
			if( Mal(1, $('#tipo'+i).val()) ){
				OMEGA('Debe indicar la unidad en la l�nea: '+ (i+1));
				return;
			}*/
		}
	}
	
	if(hay < 1){
		OMEGA('No ha indicado ning�n an�lisis');
		return;
	}
	
	if( $('#total').val() == '' || $('#total').val() == '0' ){
		OMEGA('Debe indicar el n�mero de muestras');
		return;
	}
	
	if( Mal(1, $('#nombre_sol').val()) ){
		OMEGA('Debe indicar el nombre del solicitante');
		return;
	}
	
	if( Mal(1, $('#encargado').val()) ){
		OMEGA('Debe indicar el nombre del encargado de las muestras');
		return;
	}
	
	if(!confirm('Ingresar solicitud?')) return;
	ALFA('Por favor espere....');
	btn.disabled = true;
	btn.form.submit();
}

function ClienteLista(){
	window.open(__SHELL__ + "?list4","","width=400,height=200,scrollbars=yes,status=no");
}

function ClienteEscoge(id, nombre){
	opener.document.getElementById('cliente').value = id;
	opener.document.getElementById('nomcliente').value = nombre;
	window.close();
}