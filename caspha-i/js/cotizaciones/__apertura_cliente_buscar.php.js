$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"sScrollX": "100%",
		/*"sScrollXInner": "110%",*/
		"bScrollCollapse": true,
		"bFilter": true,
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function SolicitudesDetalle(_ID, _TIPO){	
	window.showModalDialog('apertura_detalle.php?ver=1&ID=' + _ID, this.window, "dialogWidth:650px;dialogHeight:550px;status:no;");
}

/*function AperturaCrear(_ID, _TIPO){	
	window.showModalDialog('apertura_crear.php', this.window, "dialogWidth:650px;dialogHeight:550px;status:no;");
}*/

function SolicitudesBuscar(btn){
	if( Mal(1, $('#desde').val()) ){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	if( Mal(1, $('#hasta').val()) ){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	btn.disabled = true;
	btn.form.submit();
}