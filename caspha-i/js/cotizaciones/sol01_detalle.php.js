var advertencia = false;//PARA SABER QUE YA APARECIO LA JUSTIFICACION DE ANULACION 

function LabAprobar(_solicitud){ 
	if( confirm("Aprobar solicitud?") ) 
	Modificar(_solicitud, '2', '1', ''); 
}

function ClienteAnular(_solicitud){ 
	if( confirm("Anular solicitud?") ) 
	Modificar(_solicitud, 'a', '', 'Anulada por el cliente'); 
}

function LabAnular(_solicitud){
	if(!advertencia){
		advertencia = true;
		OMEGA('Debe indicar la justificaci�n de la anulaci�n en observaciones');
		document.getElementById('td_obs').innerHTML = '<input type="text" id="obs" name="obs" size="30" maxlength="50" />';
	}else{
		if( Mal(1, $('#obs').val()) ){
			OMEGA('Debe indicar la justificaci�n de la anulaci�n en observaciones');
			return;
		}
	
		if( confirm("Anular solicitud?") ) 
		Modificar( _solicitud, 'a', '1', $('#obs').val() ); 
	}
}

function Modificar(_solicitud, _estado, _admin, _obs){
	var parametros = {
		'_AJAX' : 1,
		'solicitud' : _solicitud,
		'estado' : _estado,
		'admin' : _admin,
		'obs' : _obs
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response){
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					//window.dialogArguments.location.reload();
					opener.location.reload();
					alert('Transacci�n finalizada');
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}