var _marcado = 0; // PARA SABER SI HAY CAMPO CHECKEADO
var _apertura = false; //PARA SABER SI HAY ALGUN ANALISIS DE TIPO 20
var _otros = false; //PARA SABER SI HAY ALGUN ANALISIS DE TIPO 21

function CambiaEstacion(_valor){
	if(_valor == '9') document.getElementById('otro2').style.display = '';
	else document.getElementById('otro2').style.display = 'none';
}

function Marca(_valor){
	if(_valor) _marcado++;
	else _marcado--;
}

function CambiaTipo(_valor){
	if(_valor == 'Q') document.getElementById('otro').style.display = '';
	else document.getElementById('otro').style.display = 'none';
}

function CambiaTipo2(_valor){
	if(_valor == '1') document.getElementById('tr_plaguicidas').style.display = '';
	else{
		document.getElementById('tr_plaguicidas').style.display = 'none';
		document.getElementById('lolo2').innerHTML='';
	}
}

/*function ValidaExiste(_id){
	var control = document.getElementsByName('analisis[]');
	for(i=0;i<control.length;i++){
		if(document.getElementById('codigo'+i).value == _id) return true
	}
	return false;
}*/

function Totales(){
	var control = document.getElementsByName('analisis[]');
	var tmp = 0;
	_apertura = _otros = false;
	
	for(i=0;i<control.length;i++){
		tmp += parseInt(document.getElementById('cant'+i).value);
		if(document.getElementById('tarifa'+i).value == '1') _apertura = true;
		else if(document.getElementById('tarifa'+i).value == '2') _otros = true;
	}
	
	document.getElementById('total').value = tmp;
	//
	if(_apertura)
		document.getElementById('td_estacion').style.display = '';
	else
		document.getElementById('td_estacion').style.display = 'none';
}

function AnalisisLista(linea){
	tipo = document.getElementById('cliente').value;
	window.open(__SHELL__ + "?list="+linea+"&tipo="+tipo,"","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list="+linea+"&tipo="+tipo, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function AnalisisEscoge(linea, cod_tarifa, id, nombre, tarifa, costo){
//function AnalisisEscoge(linea, id, nombre, costo){
	/*if( opener.ValidaExiste(id) ){
		alert('El an�lisis solicitado ya se encuentra en la solicitud');
		return;
	}*/
	opener.document.getElementById('cod'+linea).value = cod_tarifa;
	opener.document.getElementById('codigo'+linea).value = id;
	opener.document.getElementById('analisis'+linea).value = nombre;
	opener.document.getElementById('tarifa'+linea).value = tarifa;
	if(id=='') opener.document.getElementById('cant'+linea).value = 0;
	opener.Totales();
	window.close();
}

function AnalisisMas(){
	var linea = document.getElementsByName("analisis[]").length;
	var fila = document.createElement("tr");
	var colum = new Array(5);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	colum[4] = document.createElement("td");
	
	colum[0].innerHTML = (linea+1) + '.';
	colum[1].innerHTML = '<input type="text" size="3" id="cod' + linea + '" readonly/><input type="hidden" id="tarifa' + linea + '"/>';
	colum[2].innerHTML = '<input type="text" id="analisis' + linea + '" name="analisis[]" value="" codigo="" class="lista" readonly onclick="AnalisisLista(' + linea + ')" /><input type="hidden" id="codigo' + linea + '" name="codigo[]" /><input type="hidden" id="tarifa' + linea + '" />';
	colum[3].innerHTML = '<input type="text" id="cant' + linea + '" name="cant[]" maxlength="2" value="0" onblur="_INT(this);Totales();" class="cantidad">';
	colum[4].innerHTML = '<select id="dpto' + linea + '" name="dpto[]"><option value="">...</option><option value="0">Entomolog&iacute;a</option><option value="1">Fitopatolog&iacute;a</option><option value="2">Nematolog&iacute;a</option><option value="3">Biolog&iacute;a molecular</option></select>';
	
	for(i=0;i<colum.length;i++)
		fila.appendChild(colum[i]);
	
	document.getElementById('lolo').appendChild(fila);
}

function PlaguicidasMas(){
	var linea = document.getElementsByName("plag[]").length;
	var fila = document.createElement("tr");
	var colum = new Array(5);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	colum[4] = document.createElement("td");
	
	colum[0].innerHTML = '<input type="text" id="plag' + linea + '" name="plag[]" size="20" maxlength="30" />';
	colum[1].innerHTML = '<input type="text" id="tip' + linea + '" name="tip[]" size="20" maxlength="20" />';
	colum[2].innerHTML = '<input type="text" id="frecuencia' + linea + '" name="frecuencia[]" size="10" maxlength="20" />';
	colum[3].innerHTML = '<input type="text" id="dosis' + linea + '" name="dosis[]" size="10" maxlength="20" />';
	colum[4].innerHTML = '<input type="text" id="fecha' + linea + '" name="fecha[]" class="fecha" readonly onClick="show_calendar(this.id);" />';

	for(i=0;i<colum.length;i++)
		fila.appendChild(colum[i]);
	
	document.getElementById('lolo2').appendChild(fila);
}

function SolicitudGenerar(btn){
	var control = document.getElementsByName('analisis[]');
	var cant = hay = 0;
	
	for(i=0;i<control.length;i++){
		if(document.getElementById('analisis'+i).value != ''){
			hay++;
			cant = _FVAL(document.getElementById('cant'+i).value);
			if(cant < 1){
				OMEGA('Debe indicar la cantidad en la l�nea: '+ (i+1));
				return;
			}
			
			if( Mal(1, $('#dpto'+i).val()) ){
				OMEGA('Debe indicar el departamento en la l�nea: '+ (i+1));
				return;
			}
		}
	}
	
	if(hay < 1){
		OMEGA('No ha indicado ning�n an�lisis');
		return;
	}
	//
	if( _apertura && Mal(1, $('#solicitante').val()) ){
		OMEGA('Debe indicar el nombre del solicitante');
		return;
	}
	
	if(_apertura){
		if( Mal(1, $('#estacion').val()) ){
			OMEGA('Debe indicar la estacion');
			return;
		}else if( $('#estacion').val()=='9' && Mal(1, $('#otro2').val()) ){
			OMEGA('Debe indicar la estacion');
			return;
		}
	}
	
	if( _apertura && Mal(1, $('#fecha').val()) ){
		OMEGA('Debe indicar la fecha de apertura');
		return;
	}
	
	if( _apertura && Mal(1, $('#archivo').val()) ){
		OMEGA('Debe adjuntar la carta de solicitud de apertura');
		return;
	}
	
	if( _otros && Mal(1, $('#obs').val()) ){
		OMEGA('Ha solcitado un c�digo 21. Debe indicar la descripci�n del servicio requerido en observaciones');
		return;
	}
	
	if( Mal(1, $('#cultivo').val()) ){
		OMEGA('Debe indicar el nombre del cultivo');
		return;
	}
	
	if( Mal(1, $('#sintomas').val()) ){
		OMEGA('Debe indicar la sintomatolog�a');
		return;
	}
	
	if( Mal(1, $('#plaga').val()) ){
		OMEGA('Debe indicar la plaga a detectar');
		return;
	}
	
	if( Mal(1, $('#tipo').val()) ){
		OMEGA('Debe indicar el tipo de muestra');
		return;
	}else{
		if( $('#tipo').val() =='Q' ){
			if( Mal(1, $('#otro').val()) ){
				OMEGA('Debe indicar el tipo de muestra');
				return;
			}
		}
	}
	
	if(_marcado < 1){
		OMEGA('Debe indicar las partes afectadas');
		return;
	}
	
	if( $('#aplicado').val() == '1' ){
		var control = document.getElementsByName('plag[]');
	
		for(i=0;i<control.length;i++){
			if(document.getElementById('plag'+i).value != ''){		
				if( Mal(1, $('#tip'+i).val()) ){
					OMEGA('Debe indicar el tipo de plaguicida en la l�nea: '+ (i+1));
					return;
				}
				if( Mal(1, $('#frecuencia'+i).val()) ){
					OMEGA('Debe indicar la frecuencia en la l�nea: '+ (i+1));
					return;
				}
				if( Mal(1, $('#dosis'+i).val()) ){
					OMEGA('Debe indicar la dosis en la l�nea: '+ (i+1));
					return;
				}
				if( Mal(1, $('#fecha'+i).val()) ){
					OMEGA('Debe indicar la fecha en la l�nea: '+ (i+1));
					return;
				}
			}
		}
	}
	
	if(!confirm('Ingresar solicitud?')) return;
	ALFA('Por favor espere....');
	btn.disabled = true;
	btn.form.submit();
}