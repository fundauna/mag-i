function ValidaExiste(_id){
	var control = document.getElementsByName('analisis[]');
	for(i=0;i<control.length;i++){
		if(document.getElementById('codigo'+i).value == _id) return true
	}
	return false;
}

function Totales(){
	var control = document.getElementsByName('analisis[]');
	var tmp = 0;
	
	for(i=0;i<control.length;i++){
		tmp += parseInt(document.getElementById('cant'+i).value);
	}
	
	document.getElementById('total').value = tmp;
}

function AnalisisLista(linea){
	window.open(__SHELL__ + "?list="+linea,"","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list="+linea, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function AnalisisEscoge(linea, id, nombre, costo){
	if( opener.ValidaExiste(id) ){
		alert('El an�lisis solicitado ya se encuentra en la solicitud');
		return;
	}
	opener.document.getElementById('codigo'+linea).value = id;
	opener.document.getElementById('analisis'+linea).value = nombre;
	if(id=='') opener.document.getElementById('cant'+linea).value = 0;
	opener.Totales();
	window.close();
}

function AnalisisMas(){
	var linea = document.getElementsByName("analisis[]").length;
	var fila = document.createElement("tr");
	var colum = new Array(3);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	
	colum[0].innerHTML = (linea+1) + '.';
	colum[1].innerHTML = '<input type="text" id="analisis' + linea + '" name="analisis[]" value="" codigo="" class="lista" readonly onclick="AnalisisLista(' + linea + ')" /><input type="hidden" id="codigo' + linea + '" name="codigo[]" />';
	colum[2].innerHTML = '<input type="text" id="cant' + linea + '" name="cant[]" maxlength="2" value="0" onblur="_INT(this);Totales();" class="cantidad">';
	
	for(i=0;i<colum.length;i++)
		fila.appendChild(colum[i]);
	
	document.getElementById('lolo').appendChild(fila);
}

function SolicitudGenerar(btn){	
	if( Mal(1, $('#comercial').val()) ){
		OMEGA('Debe indicar el nombre del producto');
		return;
	}
	
	var control = document.getElementsByName('analisis[]');
	var cant = hay = 0;
	
	for(i=0;i<control.length;i++){
		if(document.getElementById('analisis'+i).value != ''){
			hay++;
			cant = _FVAL(document.getElementById('cant'+i).value);
			if(cant < 1){
				OMEGA('Debe indicar la cantidad en la l�nea: '+ (i+1));
				return;
			}
		}
	}
	
	if(hay < 1){
		OMEGA('No ha indicado ning�n an�lisis');
		return;
	}
	
	if(!confirm('Ingresar solicitud?')) return;
	ALFA('Por favor espere....');
	btn.disabled = true;
	btn.form.submit();
}