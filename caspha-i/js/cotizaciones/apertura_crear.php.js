function CambiaEstacion(_valor){
	if(_valor == '9') document.getElementById('otro').style.display = '';
	else document.getElementById('otro').style.display = 'none';
}

/*function ValidaExiste(_id){
	var control = document.getElementsByName('analisis[]');
	for(i=0;i<control.length;i++){
		if(document.getElementById('codigo'+i).value == _id) return true
	}
	return false;
}*/

function Totales(){
	var control = document.getElementsByName('analisis[]');
	var tmp = 0;
	
	for(i=0;i<control.length;i++){
		tmp += parseInt(document.getElementById('cant'+i).value);
	}
	
	document.getElementById('total').value = tmp;
}

function AnalisisLista(linea){
	tipo = document.getElementById('cliente').value;
	window.open(__SHELL__ + "?list="+linea+"&tipo="+tipo,"","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list="+linea+"&tipo="+tipo, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function AnalisisEscoge(linea, cod_tarifa, id, nombre, tarifa, costo){
	opener.document.getElementById('cod'+linea).value = cod_tarifa;
	opener.document.getElementById('codigo'+linea).value = id;
	opener.document.getElementById('analisis'+linea).value = nombre;
	opener.document.getElementById('tarifa'+linea).value = tarifa;
	if(id=='') opener.document.getElementById('cant'+linea).value = 0;
	opener.Totales();
	window.close();
}

function AnalisisMas(){
	var linea = document.getElementsByName("analisis[]").length;
	var fila = document.createElement("tr");
	var colum = new Array(5);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	colum[4] = document.createElement("td");
	
	colum[0].innerHTML = (linea+1) + '.';
	colum[1].innerHTML = '<input type="text" size="3" id="cod' + linea + '" readonly/><input type="hidden" id="tarifa' + linea + '"/>';
	colum[2].innerHTML = '<input type="text" id="analisis' + linea + '" name="analisis[]" value="" codigo="" class="lista" readonly onclick="AnalisisLista(' + linea + ')" /><input type="hidden" id="codigo' + linea + '" name="codigo[]" /><input type="hidden" id="tarifa' + linea + '" />';
	colum[3].innerHTML = '<input type="text" id="cant' + linea + '" name="cant[]" maxlength="2" value="0" onblur="_INT(this);Totales();" class="cantidad">';
	colum[4].innerHTML = '<select id="dpto' + linea + '" name="dpto[]"><option value="">...</option><option value="0">Entomolog&iacute;a</option><option value="1">Fitopatolog&iacute;a</option><option value="2">Nematolog&iacute;a</option><option value="3">Biolog&iacute;a molecular</option></select>';
	
	for(i=0;i<colum.length;i++)
		fila.appendChild(colum[i]);
	
	document.getElementById('lolo').appendChild(fila);
}

function SolicitudGenerar(btn){
	if( Mal(1, $('#estacion').val()) ){
		OMEGA('Debe indicar la estacion');
		return;
	}else if( $('#estacion').val()=='9' && Mal(1, $('#otro').val()) ){
		OMEGA('Debe indicar la estacion');
		return;
	}
	
	if( Mal(1, $('#fecha').val()) ){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	if( Mal(1, $('#cultivo').val()) ){
		OMEGA('Debe indicar el cultivo');
		return;
	}
	
	if( Mal(1, $('#archivo').val()) ){
		OMEGA('Debe adjuntar la carta de solicitud de apertura');
		return;
	}
	
	var control = document.getElementsByName('analisis[]');
	var cant = hay = 0;
	
	for(i=0;i<control.length;i++){
		if(document.getElementById('analisis'+i).value != ''){
			hay++;
			cant = _FVAL(document.getElementById('cant'+i).value);
			if(cant < 1){
				OMEGA('Debe indicar la cantidad en la l�nea: '+ (i+1));
				return;
			}
			
			if( Mal(1, $('#dpto'+i).val()) ){
				OMEGA('Debe indicar el departamento en la l�nea: '+ (i+1));
				return;
			}
		}
	}
	
	if(hay < 1){
		OMEGA('No ha indicado ning�n an�lisis');
		return;
	}
		
	if(!confirm('Ingresar solicitud?')) return;
	ALFA('Por favor espere....');
	btn.disabled = true;
	btn.form.submit();
}