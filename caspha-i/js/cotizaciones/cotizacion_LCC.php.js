var img1 = ''; //ruta de las imagenes de mostrar/ocultar
var img2 = '';
var advertencia = false;//PARA SABER QUE YA APARECIO LA JUSTIFICACION DE ANULACION 

function SolicitudAnular(btn) {
    if (!advertencia) {
        advertencia = true;
        OMEGA('Debe indicar la justificaci�n de la anulaci�n en observaciones');
        document.getElementById('td_obs').innerHTML = '<strong>Observaciones:</strong><input type="text" id="obs" name="obs" size="40" maxlength="50" />';
    } else {
        if (Mal(1, $('#obs').val())) {
            OMEGA('Debe indicar la justificaci�n de la anulaci�n en observaciones');
            return;
        }

        SolicitudModificar(btn, $('#obs').val(), '5');
    }
}

function Oculta() {
    if (document.getElementById('t_historial').style.display == 'none') {
        document.getElementById('t_historial').style.display = '';
        document.getElementById('i_historial').src = img2;
    } else {
        document.getElementById('t_historial').style.display = 'none';
        document.getElementById('i_historial').src = img1;
    }
}

function Deshabilita() {
    document.getElementById('solicitud').disabled = true;
    document.getElementById('solicitante').disabled = true;
    document.getElementById('tipo').disabled = true;
    document.getElementById('tipo2').disabled = true;
    document.getElementById('tmp').disabled = true;
    document.getElementById('nombre').disabled = true;
    document.getElementById('insumos').disabled = true;
    document.getElementById('moneda').disabled = true;
    //
    var control = document.getElementsByName('analisis[]');

    for (i = 0; i < control.length; i++) {
        document.getElementById('item' + i).disabled = true;
        document.getElementById('analisis' + i).disabled = true;
        document.getElementById('cant' + i).disabled = true;
    }
}

function SolicitudAprobar(btn) {
    if (!confirm('Aprobar cotizaci�n?'))
        return;
    ALFA('Por favor espere....');
    btn.disabled = true;
    btn.form.submit();
}

function SolicitudCancela(btn) {
    if (Mal(1, $('#deposito').val())) {
        OMEGA('Debe indicar el n�mero de recibo');
        return;
    }

    if (Mal(1, $('#archivo').val())) {
        OMEGA('Debe adjuntar el comprobante de recibo');
        return;
    }

    if (!confirm('Finalizar cotizaci�n?'))
        return;
    btn.disabled = true;
    btn.form.submit();
}

function SolicitudModificar(btn, _obs, _estado) {
    if (!confirm("Modificar estado?"))
        return;

    var parametros = {
        'modificar': 1,
        'cotizacion': $('#ID').val(),
        'obs': _obs,
        'aprobacion': $('#aprobacion').val(),
        'estado': _estado
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            btn.disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '1':
                    location.reload();
                    alert("Transacci�n finalizada");
                    return;
                default:
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                    break;
            }
        }
    });
}

function CambiaAprueba() {
    if (!confirm("Modificar persona?"))
        return;

    var parametros = {
        'ModAprueba': 1,
        'aprobacion': $('#aprobacion').val(),
        'id': $('#ID').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '1':
                    location.reload();
                    alert("Transacci�n finalizada");
                    return;
                default:
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                    break;
            }
        }
    });
}

function SolicitudCarga(_valor) {
    if (_valor == '') {
        location.href = 'cotizacion_LCC.php?acc=I';
        return;
    } else {
        //
        var parametros = {
            'buscar': 1,
            'solicitud': _valor,
            'lab': 1,
        };

        $.ajax({
            data: parametros,
            url: __SHELL__,
            type: 'post',
            beforeSend: function () {
                ALFA('Por favor espere....');
            },
            success: function (_response) {
                switch (_response) {
                    case '0':
                        OMEGA('La solicitud digitada no existe o no se encuentra aprobada');
                        $('#solicitud').val('');
                        break;
                    case '1':
                        BETA();
                        if (!confirm("Recargar usando solicitud " + _valor))
                            return;
                        location.href = 'cotizacion_LCC.php?acc=R&ID=' + _valor;
                        return;
                    default:
                    case '-1':
                        alert('Error en el env�o de par�metros [Err:-1]');
                        break;
                        break;
                }
            }
        });
        //
    }
}

function CambiaTipo(_valor) {
    var txt = '';

    if (_valor == '1')//FERTILIZANTES
        txt = 'F&oacute;rmula de fertilizante:';
    else if (_valor == '2')//PLAGUICIDAS
        txt = 'Nombre gen&eacute;rico I.A.:';

    document.getElementById('td_tipo').innerHTML = txt;
}

function ClienteEscoge(id, nombre) {
    opener.document.getElementById('cliente').value = id;
    opener.document.getElementById('tmp').value = nombre;
    window.close();
}

function ClientesLista() {
    window.open(__SHELL__ + "?list2=1", "", "width=400,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list2=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function ValidaExiste(_id) {
    var control = document.getElementsByName('analisis[]');
    for (i = 0; i < control.length; i++) {
        if (document.getElementById('codigo' + i).value == _id)
            return true
    }
    return false;
}

function Totales() {
    var control = document.getElementsByName('analisis[]');
    //VARIABLES GENERALES
    var muestras = subtotal = descuento = 0;
    //
    var cant = tmp = porcent = 0;

    for (i = 0; i < control.length; i++) {
        cant = document.getElementById('muestras').value;
        if (cant == "")
            cant = 0;
        muestras += parseInt(cant);
        //DESCUENTOS
        if (document.getElementById('descuento' + i).value == '1') {
            if (cant > 3 && cant < 11)
                porcent = 0.05;
            else if (cant > 10)
                porcent = 0.1;
            else
                porcent = 0;
        } else
            porcent = 0;
        //
        tmp = parseInt(document.getElementById('cant' + i).value) * _FVAL(document.getElementById('punit' + i).value);
        subtotal += tmp;
        descuento += (tmp * porcent);
        //SI HAY DESCUENTO HABILITA LA LINEA
        document.getElementById('tr_descuento' + i).style.display = 'none';
        if (porcent > 0) {
            document.getElementById('tr_descuento' + i).style.display = '';
            document.getElementById('td_descuento' + i).innerHTML = _RED2(tmp * porcent, 2);
        }
        //
        document.getElementById('ptotal' + i).value = tmp;
        _FLOAT(document.getElementById('ptotal' + i));
    }

    //$('#muestras').val(muestras);
    $('#subtotal').val(subtotal);
    $('#descu').val(descuento);
    $('#monto').val(subtotal - descuento);

    _FLOAT(document.getElementById('subtotal'));
    _FLOAT(document.getElementById('descu'));
    _FLOAT(document.getElementById('monto'));
}

function AnalisisLista(linea) {
    window.open(__SHELL__ + "?list=" + linea, "", "width=400,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list="+linea, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function AnalisisEscogeTarifa(linea, id, tarifa, nombre, costo, descuento) {
    if (opener.ValidaExiste(id)) {
        alert('El an�lisis solicitado ya se encuentra en la solicitud');
        return;
    }
    opener.document.getElementById('codigo' + linea).value = id;
    opener.document.getElementById('item' + linea).value = tarifa;
    opener.document.getElementById('analisis' + linea).value = nombre;
    opener.document.getElementById('punit' + linea).value = costo;
    opener.document.getElementById('descuento' + linea).value = descuento;
    if (id == '')
        opener.document.getElementById('cant' + linea).value = 0;
    opener.Totales();
    window.close();
}

function AnalisisMas() {
    var linea = document.getElementsByName("analisis[]").length;
    var fila = document.createElement("tr");
    var colum = new Array(5);

    colum[0] = document.createElement("td");
    colum[1] = document.createElement("td");
    colum[2] = document.createElement("td");
    colum[3] = document.createElement("td");
    colum[4] = document.createElement("td");
    colum[5] = document.createElement("td");

    colum[0].innerHTML = '<input type="text" id="item' + linea + '" name="item[]" size="3" readonly/><input type="hidden" id="descuento' + linea + '" />';
    colum[1].innerHTML = '<input type="text" id="analisis' + linea + '" name="analisis[]" value="" codigo="" class="lista2" readonly onclick="AnalisisLista(' + "this.id.split('analisis')[1]" + ')" /><input type="hidden" id="codigo' + linea + '" name="codigo[]" />';
    colum[2].innerHTML = '<input type="text" id="cant' + linea + '" name="cant[]" maxlength="2" value="0" onblur="_INT(this);Totales();" class="cantidad">';
    colum[3].innerHTML = '<input type="text" id="punit' + linea + '" name="punit[]" value="0" class="monto" readonly>';
    colum[4].innerHTML = '<input type="text" id="ptotal' + linea + '" name="ptotal[]" value="0" class="monto" readonly>';
    colum[5].innerHTML = ' <img onclick="EliminaLinea($( this ))" src="../../caspha-i/imagenes/del.png" title="Eliminar" class="tab2"/>';
    for (i = 0; i < colum.length; i++)
        fila.appendChild(colum[i]);

    document.getElementById('lolo').appendChild(fila);

    _LineaDescuento(linea);
}

function _LineaDescuento(_linea) {
    var fila = document.createElement("tr");
    var colum = new Array(2);

    colum[0] = document.createElement("td");
    colum[1] = document.createElement("td");
    //
    colum[0].innerHTML = 'Descuento:';
    colum[1].innerHTML = '';
    //
    fila.setAttribute('id', 'tr_descuento' + _linea);
    fila.style.display = 'none';
    colum[0].setAttribute('colspan', 4);
    colum[0].setAttribute('align', 'right');
    colum[1].setAttribute('id', 'td_descuento' + _linea);

    for (i = 0; i < colum.length; i++)
        fila.appendChild(colum[i]);

    document.getElementById('lolo').appendChild(fila);
}

function SolicitudGenerar(btn) {
    if (Mal(1, $('#cliente').val())) {
        OMEGA('Debe seleccionar el cliente');
        return;
    }

    if ($('#tipo').val() == '1') {
        if (Mal(1, $('#nombre').val())) {
            OMEGA('Debe indicar la f�rmula de fertilizante');
            return;
        }
    } else {
        if (Mal(1, $('#nombre').val())) {
            OMEGA('Debe indicar el nombre gen�rico IA');
            return;
        }
    }

    var control = document.getElementsByName('analisis[]');
    var cant = hay = 0;

    for (i = 0; i < control.length; i++) {
        if (document.getElementById('analisis' + i).value != '') {
            hay++;
            cant = _FVAL(document.getElementById('cant' + i).value);
            if (cant < 1) {
                OMEGA('Debe indicar la cantidad en la l�nea: ' + (i + 1));
                return;
            }
        }
    }

    if (hay < 1) {
        OMEGA('No ha indicado ning�n an�lisis');
        return;
    }

    /*if( Mal(1, $('#nombre_sol').val()) ){
     OMEGA('Debe indicar el nombre del solicitante');
     return;
     }
     
     if( Mal(1, $('#moneda').val()) ){
     OMEGA('Debe indicar la moneda');
     return;
     }
     */
    if (!confirm('Ingresar cotizaci�n?'))
        return;
    btn.disabled = true;
    btn.form.submit();
}

function DocumentosZip(_ARCHIVO, _MODULO) {
    var ruta = "../seguridad/documentos.php?ARCHIVO=" + _ARCHIVO + "&MODULO=" + _MODULO + "&ACCESO=94";
    window.open(ruta, "", "width=100,height=100,scrollbars=yes,status=no");
    //window.showModalDialog(ruta, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}

function EliminaLinea(linea) {

    linea.parent('td').parent('tr').get(0).remove();
    indexReal=0;
    $("#lolo tr").each(function (index) {
        if (this.id == '') {
            inputs = $(this).find('input');
            for (i = 0; i < inputs.length; i++) {
                switch (i) {
                    case 0:
                        inputs[i].id = 'item' + indexReal;
                        break;
                    case 1:
                        inputs[i].id = 'descuento' + indexReal;
                        break;
                    case 2:
                        inputs[i].id = 'analisis' + indexReal;
                        break;
                    case 3:
                        inputs[i].id = 'codigo' + indexReal;
                        break;
                    case 4:
                        inputs[i].id = 'cant' + indexReal;
                        break;
                    case 5:
                        inputs[i].id = 'punit' + indexReal;
                        break;
                    case 6:
                        inputs[i].id = 'ptotal' + indexReal;
                        break;
                }
            }
            indexReal++;
        }
    });
    Totales();
}
