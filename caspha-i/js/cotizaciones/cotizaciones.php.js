$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		/*"sScrollX": "100%",*/
		"sScrollXInner": "110%",
		"bScrollCollapse": true,
		"bFilter": true,
                "aaSorting": [[0, 'asc'], [1, 'asc']],
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function ClienteEscoge(id, nombre){
	opener.document.getElementById('cliente').value=id;
	opener.document.getElementById('tmp').value=nombre;
	window.close();
}

function ClientesLista(){
	window.open(__SHELL__ + "?list=1","","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function CotizacionDetalle(_LAB, _ID){
	var url = '';
	if(_LAB == 1) url = 'cotizacion_LRE.php?acc=M&ID='+_ID;
	else if(_LAB == 2) url = 'cotizacion_LDP.php?acc=M&ID='+_ID;
	else if(_LAB == 3) url = 'cotizacion_LCC.php?acc=M&ID='+_ID;
	
	window.open(url,"","width=650,height=550,scrollbars=yes,status=no");
	//window.showModalDialog(url, this.window, "dialogWidth:650px;dialogHeight:550px;status:no;");
}

function CotizacionCrear(_LAB){
	var url = '';
	if(_LAB == 1) url = 'cotizacion_LRE.php?acc=I';
	else if(_LAB == 2) url = 'cotizacion_LDP.php?acc=I';
	else if(_LAB == 3) url = 'cotizacion_LCC.php?acc=I';
	
	window.open(url,"","width=650,height=550,scrollbars=yes,status=no");
	//window.showModalDialog(url, this.window, "dialogWidth:650px;dialogHeight:550px;status:no;");
}

function CotizacionBuscar(btn){
	if( Mal(1, $('#cotizacion').val()) ){
		if( Mal(1, $('#desde').val()) ){
			OMEGA('Debe indicar la fecha');
			return;
		}
		
		if( Mal(1, $('#hasta').val()) ){
			OMEGA('Debe indicar la fecha');
			return;
		}
	}
	
	btn.disabled = true;
	btn.form.submit();
}