var advertencia = false;//PARA SABER QUE YA APARECIO LA JUSTIFICACION DE ANULACION 

function MuestraAnulacion(_reactivos, _material, _personal, _tiempo, _equipos, _viabilidad){
	document.getElementById('justi').style.display = '';
	document.getElementById('Jreactivos').selectedIndex = parseInt(_reactivos)+1;
	document.getElementById('Jmaterial').selectedIndex = parseInt(_material)+1;
	document.getElementById('Jpersonal').selectedIndex = parseInt(_personal)+1;
	document.getElementById('Jtiempo').selectedIndex = parseInt(_tiempo)+1;
	document.getElementById('Jequipos').selectedIndex = parseInt(_equipos)+1;
	document.getElementById('Jviabilidad').selectedIndex = parseInt(_viabilidad)+1;
	
	document.getElementById('Jreactivos').disabled = true;
	document.getElementById('Jmaterial').disabled = true;
	document.getElementById('Jpersonal').disabled = true;
	document.getElementById('Jtiempo').disabled = true;
	document.getElementById('Jequipos').disabled = true;
	document.getElementById('Jviabilidad').disabled = true;
	document.getElementById('obs').readOnly = true;
}

function LabAprobar(_solicitud){ 
	if( confirm("Aprobar solicitud?") ) 
	Modificar(_solicitud, '2', '1', '', '', ''); 
}

function ClienteAnular(_solicitud){ 
	if( confirm("Anular solicitud?") ) 
	Modificar(_solicitud, 'a', '', 'Anulada por el cliente', '', ''); 
}

function LabAnular(_solicitud){
	if(!advertencia){
		advertencia = true;
		document.getElementById('justi').style.display = '';
		OMEGA('Debe indicar la justificaci�n de la anulaci�n');
	}else{
		if( Mal(1, $('#Jreactivos').val()) || Mal(1, $('#Jmaterial').val()) || Mal(1, $('#Jpersonal').val()) || Mal(1, $('#Jtiempo').val()) || Mal(1, $('#Jequipos').val()) || Mal(1, $('#Jviabilidad').val()) ){
			OMEGA('Falta completar la justificaci�n');
			return;
		}
	
		var otros = $('#Jreactivos').val() +':'+ $('#Jmaterial').val() +':'+ $('#Jpersonal').val() +':'+ $('#Jtiempo').val() +':'+ $('#Jequipos').val() +':'+ $('#Jviabilidad').val();
		
		if( confirm("Anular solicitud?") ) 
		Modificar( _solicitud, 'a', '1', '1', otros); 
	}
}

function Modificar(_solicitud, _estado, _admin, _obs, _otros){
	if(_obs != '') _obs = $('#obs').val();
	
	var parametros = {
		'_AJAX' : 1,
		'solicitud' : _solicitud,
		'estado' : _estado,
		'admin' : _admin,
		'obs' : _obs,
		'otros' : _otros
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					//window.dialogArguments.location.reload();
					opener.location.reload();
					alert('Transacci�n finalizada');
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}