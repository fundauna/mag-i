var img1 = ''; //ruta de las imagenes de mostrar/ocultar
var img2 = '';
var advertencia = false;//PARA SABER QUE YA APARECIO LA JUSTIFICACION DE ANULACION 

function MuestraAnulacion(_reactivos, _material, _personal, _tiempo, _equipos, _viabilidad) {
    document.getElementById('justi').style.display = '';
    document.getElementById('Jreactivos').selectedIndex = parseInt(_reactivos) + 1;
    document.getElementById('Jmaterial').selectedIndex = parseInt(_material) + 1;
    document.getElementById('Jpersonal').selectedIndex = parseInt(_personal) + 1;
    document.getElementById('Jtiempo').selectedIndex = parseInt(_tiempo) + 1;
    document.getElementById('Jequipos').selectedIndex = parseInt(_equipos) + 1;
    document.getElementById('Jviabilidad').selectedIndex = parseInt(_viabilidad) + 1;

    document.getElementById('Jreactivos').disabled = true;
    document.getElementById('Jmaterial').disabled = true;
    document.getElementById('Jpersonal').disabled = true;
    document.getElementById('Jtiempo').disabled = true;
    document.getElementById('Jequipos').disabled = true;
    document.getElementById('Jviabilidad').disabled = true;
    document.getElementById('obs').readOnly = true;
}

function SolicitudCarga(_valor) {
    if (_valor == '') {
        location.href = 'cotizacion_LRE.php?acc=I';
        return;
    } else {
        //
        var parametros = {
            'buscar': 1,
            'solicitud': _valor
        };

        $.ajax({
            data: parametros,
            url: __SHELL__,
            type: 'post',
            beforeSend: function () {
                ALFA('Por favor espere....');
            },
            success: function (_response) {
                switch (_response) {
                    case '0':
                        OMEGA('La solicitud digitada no existe o no se encuentra aprobada');
                        $('#solicitud').val('');
                        break;
                    case '1':
                        BETA();
                        if (!confirm("Recargar usando solicitud " + _valor))
                            return;
                        location.href = 'cotizacion_LRE.php?acc=R&ID=' + _valor;
                        return;
                    default:
                    case '-1':
                        alert('Error en el env�o de par�metros [Err:-1]');
                        break;
                        break;
                }
            }
        });
        //
    }
}

function SolicitudAnular(btn) {
    if (!advertencia) {
        advertencia = true;
        document.getElementById('justi').style.display = '';
        OMEGA('Debe indicar la justificaci�n de la anulaci�n');
    } else {
        if (Mal(1, $('#Jreactivos').val()) || Mal(1, $('#Jmaterial').val()) || Mal(1, $('#Jpersonal').val()) || Mal(1, $('#Jtiempo').val()) || Mal(1, $('#Jequipos').val()) || Mal(1, $('#Jviabilidad').val())) {
            OMEGA('Falta completar la justificaci�n');
            return;
        }

        var otros = $('#Jreactivos').val() + ':' + $('#Jmaterial').val() + ':' + $('#Jpersonal').val() + ':' + $('#Jtiempo').val() + ':' + $('#Jequipos').val() + ':' + $('#Jviabilidad').val();


        SolicitudModificar(btn, $('#obs').val(), '5', otros);
    }
}

function Oculta() {
    if (document.getElementById('t_historial').style.display == 'none') {
        document.getElementById('t_historial').style.display = '';
        document.getElementById('i_historial').src = img2;
    } else {
        document.getElementById('t_historial').style.display = 'none';
        document.getElementById('i_historial').src = img1;
    }
}

function Deshabilita() {
    document.getElementById('tmp').disabled = true;
    document.getElementById('nombre').disabled = true;
    document.getElementById('moneda').disabled = true;
    document.getElementById('solicitud').disabled = true;
    //
    var control = document.getElementsByName('analisis[]');

    for (i = 0; i < control.length; i++) {
        document.getElementById('item' + i).disabled = true;
        document.getElementById('analisis' + i).disabled = true;
        document.getElementById('cant' + i).disabled = true;
    }
}

function SolicitudCancela(btn) {
    if (Mal(1, $('#deposito').val())) {
        OMEGA('Debe indicar el n�mero de recibo');
        return;
    }

    if (Mal(1, $('#archivo').val())) {
        OMEGA('Debe adjuntar el comprobante de recibo');
        return;
    }

    if (!confirm('Finalizar cotizaci�n?'))
        return;
    btn.disabled = true;
    btn.form.submit();
}

function SolicitudModificar(btn, _obs, _estado, _otros) {
    if (!confirm("Modificar estado?"))
        return;

    if (_obs != '' && _estado == 5)
        _obs = $('#obs').val();

    var parametros = {
        'modificar': 1,
        'cotizacion': $('#ID').val(),
        'obs': _obs,
        'estado': _estado,
        'otros': _otros
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            btn.disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '1':
                    location.reload();
                    alert("Transacci�n finalizada");
                    return;
                default:
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                    break;
            }
        }
    });
}

function SolicitudModificar2(btn, _obs, _estado, _otros) {
    if (!confirm("Modificar estado?"))
        return;

    if (_obs != '' && _estado == 5)
        _obs = $('#obs').val();

    var parametros = {
        'modificar': 2,
        'cotizacion': $('#ID').val(),
        'obs': _obs,
        'estado': _estado,
        'otros': _otros,
        'Jreactivos': $('#Jreactivos').val(),
        'Jmaterial': $('#Jmaterial').val(),
        'Jpersonal': $('#Jpersonal').val(),
        'Jtiempo': $('#Jtiempo').val(),
        'Jequipos': $('#Jequipos').val(),
        'Jviabilidad': $('#Jviabilidad').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            btn.disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '1':
                    location.reload();
                    alert("Transacci�n finalizada");
                    return;
                default:
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                    break;
            }
        }
    });
}

function ClienteEscoge(id, nombre) {
    opener.document.getElementById('cliente').value = id;
    opener.document.getElementById('tmp').value = nombre;
    window.close();
}

function ClientesLista() {
    window.open(__SHELL__ + "?list2=1", "", "width=400,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list2=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function ValidaExiste(_id) {
    var control = document.getElementsByName('analisis[]');
    for (i = 0; i < control.length; i++) {
        if (document.getElementById('codigo' + i).value == _id)
            return true
    }
    return false;
}

function Totales() {
    var control = document.getElementsByName('analisis[]');
    var cant = tmp = tmp2 = descuento = total = 0;

    for (i = 0; i < control.length; i++) {
        //cant = document.getElementById('cant' + i).value;
        cant = document.getElementById('total').value;
        if (cant == "")
            cant = 0;
        tmp += parseInt(cant);
        //DESCUENTOS
        if (document.getElementById('descuento' + i).value == '1') {
            if (cant > 5 && cant < 11)
                descuento = 0.05;
            else if (cant > 10)
                descuento = 0.1;
        }
        //
        total = parseInt(document.getElementById('cant' + i).value) * _FVAL(document.getElementById('punit' + i).value);
        document.getElementById('sub' + i).value = total;
        document.getElementById('desc' + i).value = (total * descuento);
        total -= (total * descuento);
        document.getElementById('ptotal' + i).value = total;
        tmp2 += total;
        _FLOAT(document.getElementById('sub' + i));
        _FLOAT(document.getElementById('desc' + i));
        _FLOAT(document.getElementById('ptotal' + i));
    }

    //document.getElementById('total').value = tmp;
    document.getElementById('monto').value = tmp2;
    _FLOAT(document.getElementById('monto'));
    // DESCUENTO TOTAL
    descuento = tmp = 0;
    for (i = 0; i < control.length; i++) {
        tmp = _FVAL(document.getElementById('desc' + i).value);
        descuento += tmp;
    }
    document.getElementById('descT').value = descuento;
    _FLOAT(document.getElementById('descT'));
}

function AnalisisLista(linea) {
    window.open(__SHELL__ + "?list=" + linea, "", "width=550,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list="+linea, this.window, "dialogWidth:550px;dialogHeight:200px;status:no;");
}

function AnalisisEscogeTarifa(linea, id, tarifa, nombre, costo, descuento) {
    if (opener.ValidaExiste(id)) {
        alert('El an�lisis solicitado ya se encuentra en la solicitud');
        return;
    }
    opener.document.getElementById('codigo' + linea).value = id;
    opener.document.getElementById('item' + linea).value = tarifa;
    opener.document.getElementById('analisis' + linea).value = nombre;
    opener.document.getElementById('punit' + linea).value = costo;
    opener.document.getElementById('descuento' + linea).value = descuento;
    if (id == '')
        opener.document.getElementById('cant' + linea).value = 0;
    opener.Totales();
    window.close();
}

function AnalisisMas() {
    var linea = document.getElementsByName("analisis[]").length;
    var fila = document.createElement("tr");
    var colum = new Array(7);

    colum[0] = document.createElement("td");
    colum[1] = document.createElement("td");
    colum[2] = document.createElement("td");
    colum[3] = document.createElement("td");
    colum[4] = document.createElement("td");
    colum[5] = document.createElement("td");
    colum[6] = document.createElement("td");
    colum[7] = document.createElement("td");

    colum[0].innerHTML = '<input type="text" id="item' + linea + '" name="item[]" size="3" readonly/><input type="hidden" id="descuento' + linea + '" />';
    colum[1].innerHTML = '<input type="text" id="cant' + linea + '" name="cant[]" maxlength="2" value="0" onblur="_INT(this);Totales();" class="cantidad">';
    colum[2].innerHTML = '<input type="text" id="analisis' + linea + '" name="analisis[]" value="" codigo="" class="lista2" readonly onclick="AnalisisLista(this.id.split(' + "'analisis'" + ')[1])" /><input type="hidden" id="codigo' + linea + '" name="codigo[]" />';
    colum[3].innerHTML = '<input type="text" id="punit' + linea + '" name="punit[]" value="0" class="monto" readonly>';
    colum[4].innerHTML = '<input type="text" id="sub' + linea + '" name="sub[]" value="0" class="monto" readonly>';
    colum[5].innerHTML = '<input type="text" id="desc' + linea + '" name="desc[]" value="0" class="monto" readonly>';
    colum[6].innerHTML = '<input type="text" id="ptotal' + linea + '" name="ptotal[]" value="0" class="monto" readonly>';
    colum[7].innerHTML = ' <img onclick="EliminaLinea($( this ))" src="../../caspha-i/imagenes/del.png" title="Eliminar" class="tab2"/>';

    for (i = 0; i < colum.length; i++)
        fila.appendChild(colum[i]);

    document.getElementById('lolo').appendChild(fila);
}

function SolicitudGenerar(btn) {
    if (Mal(1, $('#cliente').val())) {
        OMEGA('Debe seleccionar el cliente');
        return;
    }

    if (Mal(1, $('#nombre').val())) {
        OMEGA('Debe indicar el producto');
        return;
    }

    var control = document.getElementsByName('analisis[]');
    var cant = hay = 0;

    for (i = 0; i < control.length; i++) {
        if (document.getElementById('analisis' + i).value != '') {
            hay++;
            cant = _FVAL(document.getElementById('cant' + i).value);
            if (cant < 1) {
                OMEGA('Debe indicar la cantidad en la l�nea: ' + (i + 1));
                return;
            }
        }
    }

    if (hay < 1) {
        OMEGA('No ha indicado ning�n an�lisis');
        return;
    }

    if (Mal(1, $('#moneda').val())) {
        OMEGA('Debe indicar la moneda');
        return;
    }

    if (!confirm('Ingresar cotizaci�n?'))
        return;
    btn.disabled = true;
    btn.form.submit();
}

function DocumentosZip(_ARCHIVO, _MODULO) {
    var ruta = "../seguridad/documentos.php?ARCHIVO=" + _ARCHIVO + "&MODULO=" + _MODULO + "&ACCESO=94";

    window.open(ruta, "", "width=100,height=100,scrollbars=yes,status=no");
    //window.showModalDialog(ruta, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}

function EliminaLinea(linea) {
     linea.parent('td').parent('tr').get( 0 ).remove();
    $("#lolo tr").each(function (index) {
        inputs = $(this).find('input');
        for (i = 0; i < inputs.length; i++) {
            switch (i) {
                case 0:
                    inputs[i].id = 'item' + index;
                    break;
                case 1:
                    inputs[i].id = 'descuento' + index;
                    break;
                case 2:
                    inputs[i].id = 'cant' + index;
                    break;
                case 3:
                    inputs[i].id = 'analisis' + index;
                    break;
                case 4:
                    inputs[i].id = 'codigo' + index;
                    break;
                case 5:
                    inputs[i].id = 'punit' + index;
                    break;
                case 6:
                    inputs[i].id = 'sub' + index;
                    break;
                case 7:
                    inputs[i].id = 'desc' + index;
                    break;
                case 8:
                    inputs[i].id = 'ptotal' + index;
                    break;
            }
        }
    });
    Totales();
}