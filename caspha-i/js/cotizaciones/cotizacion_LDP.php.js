var img1 = ''; //ruta de las imagenes de mostrar/ocultar
var img2 = '';
var advertencia = false;//PARA SABER QUE YA APARECIO LA JUSTIFICACION DE ANULACION 

function SolicitudAnular(btn) {
    if (!advertencia) {
        advertencia = true;
        OMEGA('Debe indicar la justificaci�n de la anulaci�n en observaciones');
        document.getElementById('td_obs').innerHTML = '<strong>Observaciones:</strong><input type="text" id="obs" name="obs" size="40" maxlength="50" />';
    } else {
        if (Mal(1, $('#obs').val())) {
            OMEGA('Debe indicar la justificaci�n de la anulaci�n en observaciones');
            return;
        }

        SolicitudModificar(btn, $('#obs').val(), '5');
    }
}

function Oculta() {
    if (document.getElementById('t_historial').style.display == 'none') {
        document.getElementById('t_historial').style.display = '';
        document.getElementById('i_historial').src = img2;
    } else {
        document.getElementById('t_historial').style.display = 'none';
        document.getElementById('i_historial').src = img1;
    }
}

function Deshabilita() {
    document.getElementById('solicitud').disabled = true;
    document.getElementById('apertura').disabled = true;
    document.getElementById('desc1').disabled = true;
    //
    var control = document.getElementsByName('analisis[]');

    for (i = 0; i < control.length; i++) {
        document.getElementById('item' + i).disabled = true;
        document.getElementById('analisis' + i).disabled = true;
        document.getElementById('cant' + i).disabled = true;
    }
}

function SolicitudCancela(btn) {
    if (Mal(1, $('#deposito').val())) {
        OMEGA('Debe indicar el n�mero de recibo');
        return;
    }

    if (Mal(1, $('#archivo').val())) {
        OMEGA('Debe adjuntar el comprobante de recibo');
        return;
    }

    if (!confirm('Finalizar cotizaci�n?'))
        return;
    ALFA('Por favor espere....');
    btn.disabled = true;
    btn.form.submit();
}

function SolicitudModificar(btn, _obs, _estado) {
    if (!confirm("Modificar estado?"))
        return;

    var parametros = {
        'modificar': 1,
        'cotizacion': $('#ID').val(),
        'obs': _obs,
        'estado': _estado
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            btn.disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '1':
                    location.reload();
                    alert("Transacci�n finalizada");
                    return;
                default:
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                    break;
            }
        }
    });
}

function SolicitudCarga(_valor) {
    if (_valor == '') {
        location.href = 'cotizacion_LDP.php?acc=I';
        return;
    } else {
        //
        var parametros = {
            'buscar': 1,
            'solicitud': _valor,
            'lab': 1,
        };

        $.ajax({
            data: parametros,
            url: __SHELL__,
            type: 'post',
            beforeSend: function () {
                ALFA('Por favor espere....');
            },
            success: function (_response) {
                switch (_response) {
                    case '0':
                        OMEGA('La solicitud digitada no existe o no se encuentra aprobada');
                        $('#solicitud').val('');
                        break;
                    case '1':
                        BETA();
                        if (!confirm("Recargar usando solicitud " + _valor))
                            return;
                        location.href = 'cotizacion_LDP.php?acc=R&ID=' + _valor;
                        return;
                    default:
                    case '-1':
                        alert('Error en el env�o de par�metros [Err:-1]');
                        break;
                        break;
                }
            }
        });
        //
    }
}

function AperturaCarga(_valor) {
    if (_valor == '') {
        location.href = 'cotizacion_LDP.php?acc=I';
        return;
    } else {
        //
        var parametros = {
            'buscar2': 1,
            'apertura': _valor,
            'lab': 2,
        };

        $.ajax({
            data: parametros,
            url: __SHELL__,
            type: 'post',
            beforeSend: function () {
                ALFA('Por favor espere....');
            },
            success: function (_response) {
                switch (_response) {
                    case '0':
                        OMEGA('La apertura digitada no existe o no se encuentra aprobada');
                        $('#apertura').val('');
                        break;
                    case '1':
                        BETA();
                        if (!confirm("Recargar usando apertura " + _valor))
                            return;
                        location.href = 'cotizacion_LDP.php?acc=A&ID=' + _valor;
                        return;
                    default:
                    case '-1':
                        alert('Error en el env�o de par�metros [Err:-1]');
                        break;
                        break;
                }
            }
        });
        //
    }
}

function ValidaExiste(_id) {
    var control = document.getElementsByName('analisis[]');
    for (i = 0; i < control.length; i++) {
        if (document.getElementById('codigo' + i).value == _id)
            return true
    }
    return false;
}

function Totales() {
    var control = document.getElementsByName('analisis[]');
    var cant = tmp = tmp2 = total = 0;

    if (document.getElementById('desc1').checked)
        var descuento = 0.8;
    else
        var descuento = 0;

    for (i = 0; i < control.length; i++) {
        cant = document.getElementById('cant' + i).value;
        if (cant == "")
            cant = 0;
        tmp += parseInt(cant);
        total = parseInt(document.getElementById('cant' + i).value) * _FVAL(document.getElementById('punit' + i).value);
        //DESCUENTO SOLO PARA CODIGOS 8
        var tarifa = document.getElementById('item' + i).value.substring(0, 1);
        if (tarifa == '8') {
            total -= (total * descuento);
        }

        document.getElementById('ptotal' + i).value = total
        tmp2 += total;
        _FLOAT(document.getElementById('ptotal' + i));
    }

    document.getElementById('total').value = tmp;
    document.getElementById('monto').value = tmp2;
    _FLOAT(document.getElementById('monto'));
}

function AnalisisLista(linea) {
    window.open(__SHELL__ + "?list=" + linea, "", "width=400,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list="+linea, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function AnalisisEscogeTarifa(linea, id, tarifa, nombre, costo, descuento) {
    if (opener.ValidaExiste(id)) {
        alert('El an�lisis solicitado ya se encuentra en la solicitud');
        return;
    }
    opener.document.getElementById('codigo' + linea).value = id;
    opener.document.getElementById('item' + linea).value = tarifa;
    opener.document.getElementById('analisis' + linea).value = nombre;
    opener.document.getElementById('punit' + linea).value = costo;
    opener.document.getElementById('descuento' + linea).value = descuento;
    if (id == '')
        opener.document.getElementById('cant' + linea).value = 0;
    opener.Totales();
    window.close();
}

function AnalisisMas() {
    var linea = document.getElementsByName("analisis[]").length;
    var fila = document.createElement("tr");
    var colum = new Array(5);

    colum[0] = document.createElement("td");
    colum[1] = document.createElement("td");
    colum[2] = document.createElement("td");
    colum[3] = document.createElement("td");
    colum[4] = document.createElement("td");
    colum[5] = document.createElement("td");

    colum[0].innerHTML = '<input type="text" id="item' + linea + '" name="item[]" size="3" readonly/><input type="hidden" id="descuento' + linea + '" />';
    colum[1].innerHTML = '<input type="text" id="analisis' + linea + '" name="analisis[]" value="" codigo="" class="lista2" readonly onclick="AnalisisLista(' + linea + ')" /><input type="hidden" id="codigo' + linea + '" name="codigo[]" />';
    colum[2].innerHTML = '<input type="text" id="cant' + linea + '" name="cant[]" maxlength="2" value="0" onblur="_INT(this);Totales();" class="cantidad">';
    colum[3].innerHTML = '<input type="text" id="punit' + linea + '" name="punit[]" value="0" class="monto" onblur="_FLOAT(this);Totales();">';
    colum[4].innerHTML = '<input type="text" id="ptotal' + linea + '" name="ptotal[]" value="0" class="monto" readonly>';
    colum[5].innerHTML = '<img onclick="EliminaLinea($( this ))" src="../../caspha-i/imagenes/del.png" title="Eliminar" class="tab2"/>';

    for (i = 0; i < colum.length; i++)
        fila.appendChild(colum[i]);

    document.getElementById('lolo').appendChild(fila);
}

function SolicitudGenerar(btn) {
    var control = document.getElementsByName('analisis[]');
    var cant = hay = 0;

    for (i = 0; i < control.length; i++) {
        if (document.getElementById('analisis' + i).value != '') {
            hay++;
            cant = _FVAL(document.getElementById('cant' + i).value);
            if (cant < 1) {
                OMEGA('Debe indicar la cantidad en la l�nea: ' + (i + 1));
                return;
            }
        }
    }

    if (hay < 1) {
        OMEGA('No ha indicado ning�n an�lisis');
        return;
    }

    if (!confirm('Ingresar cotizaci�n?'))
        return;
    btn.disabled = true;
    btn.form.submit();
}

function DocumentosZip(_ARCHIVO, _MODULO) {
    var ruta = "../seguridad/documentos.php?ARCHIVO=" + _ARCHIVO + "&MODULO=" + _MODULO + "&ACCESO=94";

    window.open(ruta, "", "width=100,height=100,scrollbars=yes,status=no");
    //window.showModalDialog(ruta, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}

function EliminaLinea(linea) {

    linea.parent('td').parent('tr').get(0).remove();
    indexReal = 0;
    $("#lolo tr").each(function (index) {
        if (this.id == '') {
            inputs = $(this).find('input');
            for (i = 0; i < inputs.length; i++) {
                if (inputs.length > 6) {
                    switch (i) {
                        case 0:
                            inputs[i].id = 'item' + indexReal;
                            break;
                        case 1:
                            inputs[i].id = 'descuento' + indexReal;
                            break;
                        case 2:
                            inputs[i].id = 'analisis' + indexReal;
                            break;
                        case 3:
                            inputs[i].id = 'codigo' + indexReal;
                            break;
                        case 4:
                            inputs[i].id = 'cant' + indexReal;
                            break;
                        case 5:
                            inputs[i].id = 'punit' + indexReal;
                            break;
                        case 6:
                            inputs[i].id = 'ptotal' + indexReal;
                            break;
                    }
                } else {
                    switch (i) {
                        case 0:
                            inputs[i].id = 'item' + indexReal;
                            break;
                        case 1:
                            inputs[i].id = 'analisis' + indexReal;
                            break;
                        case 2:
                            inputs[i].id = 'codigo' + indexReal;
                            break;
                        case 3:
                            inputs[i].id = 'cant' + indexReal;
                            break;
                        case 4:
                            inputs[i].id = 'punit' + indexReal;
                            break;
                        case 5:
                            inputs[i].id = 'ptotal' + indexReal;
                            break;
                    }
                }
            }
            indexReal++;
        }
    });
    Totales();
}
