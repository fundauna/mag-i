$(document).ready(function () {
    oTable = $('#example').dataTable({
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        /*"sScrollX": "100%",*/
        "sScrollXInner": "110%",
        "bScrollCollapse": true,
        "bFilter": true,
        "oLanguage": {
            "sEmptyTable": "No hay datos que mostrar",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sLoadingRecords": "Cargando...",
            "sProcessing": "Procesando...",
            "sSearch": "Buscar:",
            "sZeroRecords": "No hay datos que mostrar",
            "sInfoFiltered": "(filtro de _MAX_ registros)",
            "sInfoEmpty": "Mostrando 0 registros",
            "oPaginate": {
                "sFirst": "Primer",
                "sLast": "Fin",
                "sNext": "Sig.",
                "sPrevious": "Prev."
            }
        }
    });
});


function LaboratorioBuscar() {
    var parametros = {
        '_AJAX': 1,
        'LaboratorioBuscar': 1,
        'laboratorio': $('#laboratorio').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            habilita(false);
            ALFA('Buscando hojas....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                default:
                    BETA();
                    habilita(true);
                    document.getElementById('td_hojas').innerHTML = _response;
                    break;
                //default:alert(_response);break;
            }
        }
    });
}

function LabB(_OPC) {
    if (_OPC == 'M' && $('#enc').val() == '') {
        OMEGA('Debe seleccionar una plantilla de encabezado o pie de pagina');
        return;
    }
    _ID = $('#enc').val();
    _TIPO = document.getElementById('enc').options[document.getElementById('enc').selectedIndex].getAttribute('tipo');
    if (_OPC == 'M') {
        window.open("plantilla_encpie_detalle.php?acc=" + _OPC + "&id=" + _ID + "&tipo=" + _TIPO, "", "width=950,height=650,scrollbars=yes,status=no");
    } else {
        window.open("plantilla_encpie_detalle.php?acc=" + _OPC + "&id=" + _ID, "", "width=950,height=650,scrollbars=yes,status=no");
    }
}

function LabA(_OPC, _SIGLA, _CODIGO) {
    window.open("encabezados_detalle.php?acc=" + _OPC + "&sigla=" + _SIGLA + "&codigo=" + _CODIGO, "", "width=710,height=450,scrollbars=yes,status=no");
}

function Guarda(_HOJA, _LINEA) {
    var parametros = {
        'LOLO': 1,
        'hoja': _HOJA,
        'encabezado': $('#encabezado' + _LINEA).val(),
        'pie': $('#pie' + _LINEA).val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            ALFA('Guardando cambios..');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                default:
                    GAMA('Transacci&oacute;n Finalizada');
                    break;
                //default:alert(_response);break;
            }
        }
    });
}