function desmarca(){
	document.getElementById('add').disabled = false;
	document.getElementById('mod').disabled = true;
	document.getElementById('del').disabled = true;
	document.getElementById('new').disabled = true;
	document.getElementById('id').value = '';
	document.getElementById('nombre').value = '';
	document.getElementById('descripcion').value = '';
}

function marca(control, tipo){
	if(control.selectedIndex!= -1){
		if(tipo==1){
			document.getElementById('id').value = control.options[control.selectedIndex].getAttribute('id');
			document.getElementById('sigla').value = control.options[control.selectedIndex].getAttribute('sigla');
			document.getElementById('nombre').value = control.options[control.selectedIndex].getAttribute('nombre');
		}else if(tipo==3){
			document.getElementById('id').value = control.options[control.selectedIndex].getAttribute('id');
			var tmp = control.options[control.selectedIndex].getAttribute('sigla');
			//NO VISIBILIDAD GENERAL
			if(tmp == '0') document.getElementById('sigla').selectedIndex = 1;
			//ESTADO NORMAL
			else if(tmp == '1') document.getElementById('sigla').selectedIndex = 0;
			//NOTIFICACION
			else document.getElementById('sigla').selectedIndex = 1;
				
			document.getElementById('nombre').value = control.options[control.selectedIndex].getAttribute('nombre');
			document.getElementById('descripcion').value = control.options[control.selectedIndex].getAttribute('descripcion');
		}else{
			document.getElementById('id').value = control.options[control.selectedIndex].getAttribute('id');		
			document.getElementById('nombre').value = control.options[control.selectedIndex].getAttribute('nombre');
			document.getElementById('descripcion').value = control.options[control.selectedIndex].getAttribute('descripcion');
		}
		
		if(document.getElementById('id').value == '0'){
                        document.getElementById('id').disabled = false;
			document.getElementById('mod').disabled = true;
			document.getElementById('del').disabled = true;
		}else{
                        document.getElementById('id').disabled = true;
			document.getElementById('mod').disabled = false;
			document.getElementById('del').disabled = false;
		}
		document.getElementById('add').disabled = true;
		document.getElementById('new').disabled = false;
	}
}

function modificar(accion){
        if( Mal(1, document.getElementById('tipo').value) ){
		OMEGA('Debe seleccionar el tipo');
		return;
	}
	if( Mal(1, document.getElementById('id').value) ){
		OMEGA('Debe digitar el id');
		return;
	}
	
	if( Mal(2, document.getElementById('nombre').value) ){
		OMEGA('Debe digitar el nombre');
		return;
	}
	
	if(confirm('Modificar Datos?')){
		var parametros = {
			'_AJAX' : 1,
			'id' : $('#id').val(),
			'sigla' : $('#sigla').val(),
			'nombre' : $('#nombre').val(),
			'descripcion' : $('#descripcion').val(),
			'tipo' : $('#tipo').val(),
			'accion' : accion
		};
		
		$.ajax({
			data:  parametros,
			url:   __SHELL__,
			type:  'post',
			beforeSend: function () {
				document.getElementById('add').disabled = true;
				document.getElementById('mod').disabled = true;
				document.getElementById('del').disabled = true;
				document.getElementById('new').disabled = true;	
				ALFA('Por favor espere....');
			},
			success: function (_response) {
				switch(_response){
					case '-0':alert('Sesi�n expirada [Err:0]');break;
					case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
					case '0':
						OMEGA('Error transaccional');
						document.getElementById('add').disabled = false;
						break;
					case '1':
						GAMA('Transaccion finalizada');
						document.getElementById('add').disabled = false;
						break;
                                            case '2':
                                                GAMA('No se puede eliminar por estar en uso');
						document.getElementById('add').disabled = false;
                                            break;
					default:alert('Tiempo de espera agotado');break;
				}
			}
		});
	}
}