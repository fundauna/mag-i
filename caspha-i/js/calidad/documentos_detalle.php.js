function datos(accion){	
	if( $('#tipo').val()=='' ){
		OMEGA('Debe indicar un tipo de documento');
		return;
	}
	
	if( Mal(1, $('#codigo').val()) ){
		OMEGA('Debe indicar el c�digo del documento');
		return;
	}
	
	if( Mal(2, $('#descripcion').val()) ){
		OMEGA('Debe indicar el nombre del documento');
		return;
	}
	
	if( Mal(1, $('#version').val()) ){
		OMEGA('Debe indicar el n�mero de versi�n');
		return;
	}
	
	if( Mal(1, $('#revision').val()) ){
		OMEGA('Debe indicar el n�mero de revisi�n');
		return;
	}
	
	if( $('#naturaleza').val()=='' ){
		OMEGA('Debe indicar la naturaleza del documento');
		return;
	}
	
	if( $('#unidad').val()=='' ){
		OMEGA('Debe indicar un laboratorio para el documento');
		return;
	}
	
	if( $('#modulo').val()=='' ){
		OMEGA('Debe indicar un m�dulo del documento');
		return;
	}
	
	if( $('#estado').val()=='' ){
		OMEGA('Debe indicar un estado del documento');
		return;
	}
	
	if(accion=='I' && $('#archivo').val()=='' ){
		OMEGA('Debe adjuntar un documento');
		return;
	}	
	
	//if(accion=='M' && $('#archivo').val()!=''){
		//var valor=parseInt(document.getElementById('version').value);
		//document.getElementById('version').value= valor+1;
	//}
	
	if(accion=='I'){
		if(!confirm('Ingresar datos?')) return;
		btn.disabled = true;
		ALFA('Por favor espere....');
		document.form.submit();
	}else{
		if(!confirm('Modificar datos?')) return;
		btn.disabled = true;
		ALFA('Por favor espere....');
		document.form.submit();
	}		
}

function formateaVersion(n){
    if(!n){
        document.getElementById('version').value = "01";
        return;
    }
    document.getElementById('version').value = n.length >= 2 ? "" + n: "0" + n;
}

function DocumentosZip(_ARCHIVO, _MODULO){
	var ruta = "../seguridad/documentos.php?ARCHIVO=" + _ARCHIVO + "&MODULO=" + _MODULO + "&ACCESO=09";
	window.open(ruta,"","width=100,height=100,scrollbars=yes,status=no");
	//window.showModalDialog(ruta, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}

function DocumentosElimina(_CS,_VER,_ACC){
	if(!confirm('Eliminar documento?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'cs' : _CS,
		'version' : _VER,
		'accion' : _ACC,
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					OMEGA('Transaccion finalizada');
					window.close();
					opener.location.reload();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function DocumentosTrasladar(_ID, _COD, _NOMBRE){
	window.open("firmas.php?ID=" + _ID + "&COD=" + _COD + "&NOMBRE="+_NOMBRE,"","width=450,height=450,scrollbars=yes,status=no");
	//window.showModalDialog("firmas.php?ID=" + _ID + "&COD=" + _COD + "&NOMBRE="+_NOMBRE, this.window, "dialogWidth:450px;dialogHeight:450px;status:no;");
}

function DocumentosVinculados(_ID, _COD, _NOMBRE){
	window.open("vinculados.php?ID=" + _ID + "&COD=" + _COD + "&NOMBRE="+_NOMBRE,"","width=450,height=450,scrollbars=yes,status=no");
	//window.showModalDialog("vinculados.php?ID=" + _ID + "&COD=" + _COD + "&NOMBRE="+_NOMBRE, this.window, "dialogWidth:450px;dialogHeight:450px;status:no;");
}