var valor = label = '', indice;

function habilita(bool) {
    document.getElementById('btn_buscar').disabled = !bool;
    document.getElementById('btn_agregar').disabled = !bool;
    document.getElementById('btn_eliminar').disabled = !bool;
}

function LaboratorioBuscar() {
    var parametros = {
        '_AJAX': 1,
        'LaboratorioBuscar': 1,
        'laboratorio': $('#laboratorio').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            habilita(false);
            ALFA('Buscando hojas....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                default:
                    BETA();
                    habilita(true);
                    document.getElementById('td_hojas').innerHTML = _response;
                    break;
                //default:alert(_response);break;
            }
        }
    });
}

function HojaBuscar() {
    alert('lolo');
}

function PermisosAgrega() {
    if (document.getElementById('btn_agregar').disabled) return;

    var control = document.getElementById('permisos');
    if (control.selectedIndex == -1) {
        OMEGA('Seleccione un permiso');
        return;
    }
    habilita(false);
    valor = control.options[control.selectedIndex].value;
    label = control.options[control.selectedIndex].text;

    var parametros = {
        '_AJAX': 1,
        'PermisosAgrega': 1,
        'perfil': $('#perfiles').val(),
        'permiso': control.options[control.selectedIndex].value,
        'privilegio': $('#privilegio').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            habilita(false);
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '0':
                    BETA();
                    habilita(true);
                    break;
                case '1':
                    BETA();
                    habilita(true);
                    /**/
                    var oOption = document.createElement('OPTION');
                    oOption.value = valor;
                    oOption.text = label + ' (' + document.getElementById('privilegio').value + ')';
                    var combo = document.getElementById('asignados');
                    combo.options[combo.options.length] = new Option(oOption.text, oOption.value);
                    /**/
                    break;
                default:
                    alert(_response);
                    break;
            }
        }
    });
}

function PermisosElimina() {
    var control = document.getElementById('asignados');
    if (control.selectedIndex == -1) {
        OMEGA('Seleccione un permiso asignado');
        return;
    }

    habilita(false);
    indice = control.selectedIndex;
    valor = control.options[control.selectedIndex].value;

    var parametros = {
        '_AJAX': 1,
        'PermisosElimina': 1,
        'perfil': $('#perfiles').val(),
        'permiso': control.options[control.selectedIndex].value
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            habilita(false);
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-0':
                    alert('Sesi�n expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el env�o de par�metros [Err:-1]');
                    break;
                case '1':
                    BETA();
                    habilita(true);
                    /**/
                    document.getElementById('asignados').remove(indice);
                    habilita(true);
                    /**/
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
            }
        }
    });
}