function Recarga(){
	if(typeof window.dialogArguments!='undefined') 
		window.dialogArguments.document.getElementById('btn').disabled = true;
		
	location.reload();
}

function datos(accion, linea){
	if( Mal(1, $('#usuario').val()) ){
		OMEGA('Debe indicar el usuario a enviar');
		return;
	}
	
	if( Mal(1, $('#peticion').val()) ){
		OMEGA('Debe indicar el tipo de petici�n');
		return;
	}
	
	if(!confirm('Modificar datos?')) return;	
	
	var parametros = {
		'_AJAX' : 1,
		'padre' : $('#padre').val(),
		'usuario' : $('#usuario').val(),
		'peticion' : $('#peticion').val(),
		'detalle' : $('#detalle').val(),
		'yo' : $('#yo').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					OMEGA('Transaccion finalizada');
					Recarga();		
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function UsuariosLista(){
	window.open(__SHELL__ + "?list=1","","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function UsuarioEscoge(id, nombre){
	opener.document.getElementById('usuario').value=id;
	opener.document.getElementById('tmp').value=nombre;
	window.close();
}