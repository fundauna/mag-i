function datos(accion, linea){	
	if(accion=='I'){
		if( Mal(2, $('#codigo').val()) ){
			OMEGA('Debe indicar el c�digo del documento');
			return;
		}
	
		if( $('#cod_padre').val() == $('#codigo').val() ){
			OMEGA('No puede ingresar el c�digo<br>del documento padre');
			return;
		}
	}
	
	if(!confirm('Modificar datos?')) return;	
	
	var parametros = {
		'_AJAX' : 1,
		'padre' : $('#padre').val(),
		'vinculo' : $('#codigo').val(),
		'linea' : linea,
		'accion' : accion
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					OMEGA('Transaccion finalizada');
					location.reload();					
					break;
				case '2':
					OMEGA('El documento digitado no existe');
					$('#codigo').val('')
					$('#btn').disabled = false;
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function DocumentosZip(_ARCHIVO, _MODULO){
	var ruta = "../seguridad/documentos.php?ARCHIVO=" + _ARCHIVO + "&MODULO=" + _MODULO + "&ACCESO=09";
	window.open(ruta,"","width=100,height=100,scrollbars=yes,status=no");
	//window.showModalDialog(ruta, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}