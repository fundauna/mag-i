function Revisar(){
	$('#jus').val('');
	document.getElementById('tr1').style.display = 'none';
	document.getElementById('tr2').style.display = 'none';
	
	if(confirm('Establecer documento como revisado?')){
		var parametros = {
			'_AJAX' : 1,
			'tablon' : $('#tablon').val(),
			'cs' : $('#cs').val(),
			'jus' : '',
			'accion' : 'R'
		};
		
		$.ajax({
			data:  parametros,
			url:   __SHELL__,
			type:  'post',
			beforeSend: function () {
				document.getElementById('btn1').disabled = true;
				document.getElementById('btn2').disabled = true;
				ALFA('Por favor espere....');
			},
			success: function (_response) {
				switch(_response){
					case '-0':alert('Sesi�n expirada [Err:0]');break;
					case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
					case '0':
						OMEGA('Error transaccional');
						break;
					case '1':
						//if(typeof window.dialogArguments=='undefined') window.dialogArguments.location.reload();
						opener.location.reload();
						alert('Transaccion finalizada');
						window.close();
						break;
					default:alert('Tiempo de espera agotado');break;
				}
			}
		});
	}
}

function Aprobar(){
	$('#jus').val('');
	document.getElementById('tr1').style.display = 'none';
	document.getElementById('tr2').style.display = 'none';
	
	if(confirm('Aprobar documento?')){
		var parametros = {
			'_AJAX' : 1,
			'tablon' : $('#tablon').val(),
			'cs' : $('#cs').val(),
			'jus' : '',
			'accion' : 'A'
		};
		
		$.ajax({
			data:  parametros,
			url:   __SHELL__,
			type:  'post',
			beforeSend: function () {
				document.getElementById('btn1').disabled = true;
				document.getElementById('btn2').disabled = true;
				ALFA('Por favor espere....');
			},
			success: function (_response) {
				switch(_response){
					case '-0':alert('Sesi�n expirada [Err:0]');break;
					case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
					case '0':
						OMEGA('Error transaccional');
						break;
					case '1':
						opener.location.reload();
						alert('Transaccion finalizada');
						window.close();
						break;
					default:alert('Tiempo de espera agotado');break;
				}
			}
		});
	}
}

function Denegar(){	
	if( Mal(5, document.getElementById('jus').value) ){
		document.getElementById('tr1').style.display = '';
		document.getElementById('tr2').style.display = '';
		OMEGA('Debe indicar la justificaci�n');
		return;
	}
	
	if(confirm('Denegar documento?')){
		var parametros = {
			'_AJAX' : 1,
			'tablon' : $('#tablon').val(),
			'cs' : $('#cs').val(),
			'jus' : $('#jus').val(),
			'accion' : 'D'
		};
		
		$.ajax({
			data:  parametros,
			url:   __SHELL__,
			type:  'post',
			beforeSend: function () {
				document.getElementById('btn1').disabled = true;
				document.getElementById('btn2').disabled = true;
				ALFA('Por favor espere....');
			},
			success: function (_response) {
				switch(_response){
					case '-0':alert('Sesi�n expirada [Err:0]');break;
					case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
					case '0':
						OMEGA('Error transaccional');
						break;
					case '1':
						opener.location.reload();
						alert('Transaccion finalizada');
						window.close();
						break;
					default:alert('Tiempo de espera agotado');break;
				}
			}
		});
	}
}

function DocumentosZip(_ARCHIVO, _MODULO){
	var ruta = "../seguridad/documentos.php?ARCHIVO=" + _ARCHIVO + "&MODULO=" + _MODULO + "&ACCESO=00";
	window.open(ruta,"","width=100,height=100,scrollbars=yes,status=no");
	//window.showModalDialog(ruta, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}