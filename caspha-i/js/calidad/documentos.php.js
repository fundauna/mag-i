$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		/*"sScrollX": "100%",*/
		"sScrollXInner": "110%",
		"bScrollCollapse": true,
		"bFilter": true,
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function DocumentosAgrega(){
	window.open("documentos_detalle.php?acc=I","","width=610,height=550,scrollbars=yes,status=no");
	//window.showModalDialog("documentos_detalle.php?acc=I", this.window, "dialogWidth:610px;dialogHeight:550px;status:no;");
}

function DocumentosBuscar(btn){
	btn.disabled = true;
	btn.form.submit();
}

function DocumentosModifica(_ID){
	window.open("documentos_detalle.php?acc=M&ID="+_ID,"","width=610,height=550,scrollbars=yes,status=no");
	//window.showModalDialog("documentos_detalle.php?acc=M&ID="+_ID, this.window, "dialogWidth:610px;dialogHeight:550px;status:no;");
}

function DocumentosZip(_ARCHIVO, _MODULO){
	var ruta = "../seguridad/documentos.php?ARCHIVO=" + _ARCHIVO + "&MODULO=" + _MODULO + "&ACCESO=09";
	window.open(ruta,"","width=100,height=100,scrollbars=yes,status=no");
	//window.showModalDialog(ruta, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}

function EquiposCertificados(_ID){
	window.open("vinculados.php?ID="+_ID,"","width=600,height=450,scrollbars=yes,status=no");
	//window.showModalDialog("vinculados.php?ID="+_ID, this.window, "dialogWidth:600px;dialogHeight:450px;status:no;");
}

function marca(valor){
	if(valor==1){
		document.getElementById('codigo').readOnly = false;
		document.getElementById('nombre').readOnly = true;
		document.getElementById('nombre').value = '';
	}else{
		document.getElementById('nombre').readOnly = false;
		document.getElementById('codigo').readOnly = true;
		document.getElementById('codigo').value = '';
	}
}

function DocumentosAnula(_CS,_VER,_ACC){
	if(!confirm('Anular documento?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'cs' : _CS,
		'version' : _VER,
		'accion' : _ACC,
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					GAMA('Transaccion finalizada');
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function DocumentosElimina(_CS,_VER,_ACC){
	if(!confirm('Eliminar documento?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'cs' : _CS,
		'version' : _VER,
		'accion' : _ACC,
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					GAMA('Transaccion finalizada');
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}