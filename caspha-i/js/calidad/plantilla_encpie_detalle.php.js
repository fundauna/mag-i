$(document).ready(function() {
	$('#summernote').summernote();
});
function LaboratorioBuscar(){
	var parametros = {
		'_AJAX' : 1,
		'LaboratorioBuscar' : 1,
		'laboratorio' : $('#laboratorio').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			habilita(false);
			ALFA('Buscando hojas....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				default:
					BETA();
					habilita(true);
					document.getElementById('td_hojas').innerHTML = _response;
					break;
				//default:alert(_response);break;
			}
		}
	});
}