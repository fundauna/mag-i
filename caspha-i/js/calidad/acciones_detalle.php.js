function AccionMas(){
	var linea = document.getElementsByName("numero[]").length;
	var fila = document.createElement("tr");
	var colum = new Array(9);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	colum[4] = document.createElement("td");
	colum[5] = document.createElement("td");
	colum[6] = document.createElement("td");
	colum[7] = document.createElement("td");
	colum[8] = document.createElement("td");
	
	colum[0].innerHTML = '<input type="text" id="numero' + linea + '" name="numero[]" size="15" maxlength="15">';
	colum[1].innerHTML = '<input type="text" id="fec_acc' + linea + '" name="fec_acc[]" class="fecha2" readonly onClick="show_calendar(this.id);">';
	colum[2].innerHTML = '<input type="text" id="tmpB' + linea + '" class="lista2" readonly onclick="UsuariosLista(\'B' + linea + '\')" /><input type="hidden" id="usuarioB' + linea + '" name="usuarioB[]"/>';
	colum[3].innerHTML = '<input type="text" id="fec_seg' + linea + '" name="fec_seg[]" class="fecha2" readonly onClick="show_calendar(this.id);">';
	colum[4].innerHTML = '<input type="text" id="tmpC' + linea + '" class="lista2" readonly onclick="UsuariosLista(\'C' + linea + '\')" /><input type="hidden" id="usuarioC' + linea + '" name="usuarioC[]"/>';
	colum[5].innerHTML = '<input type="text" id="fec_rea' + linea + '" name="fec_rea[]" class="fecha2" readonly onClick="show_calendar(this.id);">';
	colum[6].innerHTML = '<select id="estado' + linea + '" name="estado[]"><option value="">...</option><option value="A">Abierta</option><option value="C">Cerrada</option></select>';
	colum[7].innerHTML = '<input type="text" id="fec_cie' + linea + '" name="fec_cie[]" class="fecha2" readonly onClick="show_calendar(this.id);">';
	colum[8].innerHTML = '<input type="text" id="tmpD' + linea + '" class="lista2" readonly onclick="UsuariosLista(\'D' + linea + '\')" /><input type="hidden" id="usuarioD' + linea + '" name="usuarioD[]"/>';
	
	for(i=0;i<colum.length;i++)
		fila.appendChild(colum[i]);
	
	document.getElementById('lolo').appendChild(fila);
}

function AccionAgregar(btn){
	if( Mal(1, document.getElementById('tipo').value) ){
		OMEGA('Debe indicar el tipo de documento');
		return;
	}
	
	if( Mal(5, document.getElementById('codigo').value) ){
		OMEGA('Debe digitar el c�digo');
		return;
	}
	
	if( Mal(5, document.getElementById('fec_pre').value) ){
		OMEGA('Debe indicar la fecha de presentaci�n');
		return;
	}
	
	if( Mal(1, document.getElementById('hallazgo').value) ){
		OMEGA('Debe indicar el tipo de hallazgo');
		return;
	}
	
	if( Mal(1, document.getElementById('correccion').value) ){
		OMEGA('Debe indicar si aplica correcci�n inmediata');
		return;
	}
	
	var control = document.getElementsByName('numero[]');
	var hay = 0;
	
	for(i=0;i<control.length;i++){
		if( !Mal(1, control[i].value) ){	
			if( Mal(1, document.getElementById('estado'+i).value) ){
				OMEGA('Debe indicar el estado<br>L�nea: ' + (i+1) );
				return;
			}
			
			hay++;
		}
	}
	
	if(hay < 1){
		OMEGA('No ha indicado ninguna l�nea');
		return;
	}
	
	if(!confirm('Modificar datos?')) return;
	btn.disabled = true;
	ALFA('Por favor espere....');
	document.form.submit();
}

function UsuariosLista(linea){
	window.open(__SHELL__ + "?list=1&linea=" + linea,"","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1&linea=" + linea, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function UsuarioEscoge(id, nombre, linea){
	opener.document.getElementById('usuario'+linea).value=id;
	opener.document.getElementById('tmp'+linea).value=nombre;
	window.close();
}

function DocumentosZip(_ARCHIVO, _MODULO){
	var ruta = "../seguridad/documentos.php?ARCHIVO=" + _ARCHIVO + "&MODULO=" + _MODULO + "&ACCESO=94";
	window.open(ruta,"","width=100,height=100,scrollbars=yes,status=no");
	//window.showModalDialog(ruta, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}

function CambiaCorreccion(value){
        var campos = document.getElementsByClassName('correccion');
        var visibility = 'visible';
        if(value == '0'){
            visibility = 'hidden'
        }
        for(var i=0;i<campos.length;i++){
            campos[i].style.visibility = visibility;
        }
}