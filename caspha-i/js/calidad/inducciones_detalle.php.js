function GeneraGrafico(tipo, titulo){	
	$(function () {
		$('#container' + tipo).highcharts({
			data: {
				table: document.getElementById('datos' + tipo)
			},
			chart: {
				type: 'column',
				zoomType: 'xy'
			},
			title: {
				text: titulo
			},
			yAxis: {
				allowDecimals: false,
				title: {
					text: 'Cantidad de horas'
				}
			},
			tooltip: {
				formatter: function() {
					return this.y;
				}
			}
		});
	});
}

var img1 = ''; //ruta de las imagenes de mostrar/ocultar
var img2 = '';
var rutaimg = '';

function Muestra(_tipo){
	if(_tipo==1){
		document.getElementById('grafico_1').style.display = '';
		document.getElementById('grafico_2').style.display = 'none';
		document.getElementById('grafico_3').style.display = 'none';
	}else if(_tipo==2){
		document.getElementById('grafico_1').style.display = 'none';
		document.getElementById('grafico_2').style.display = '';
		document.getElementById('grafico_3').style.display = 'none';
		GeneraGrafico(2, 'Total de horas por tipo');
			
	}else if(_tipo==3){
		document.getElementById('grafico_1').style.display = 'none';
		document.getElementById('grafico_2').style.display = 'none';
		document.getElementById('grafico_3').style.display = '';
		GeneraGrafico(3, 'Total de horas');
	}
}

function GeneraTabla(){
	var parametros = {
		'_AJAX' : 1,
		'paso' : 6,
		'id' : $('#id').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response){
			if(_response == ''){
				OMEGA("No hay datos que mostrar");
				return;
			}
			BETA();
			document.getElementById('t4').style.display = '';
			document.getElementById('grafico_1').innerHTML = _response;
			//
			document.getElementById('btnt1').disabled = false;
			document.getElementById('btnt2').disabled = false;
			document.getElementById('btnt3').disabled = false;
			return;
		}
	});
}

function EditarLinea(_persona, _linea){
	var parametros = {
		'_AJAX' : 1,
		'paso' : 4,
		'id' : _persona,
		'linea': _linea,
		'imagenes': rutaimg
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response){
			BETA();
			document.getElementById('t3').innerHTML = _response;
			document.getElementById('t3').style.display = '';
			return;
		}
	});
}

function Buscar(num){
	if( Mal(1, $('#desde').val()) ){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	if( Mal(1, $('#hasta').val()) ){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : num,
		'id' : $('#id').val(),
		'desde' : $('#desde').val(),
		'hasta' : $('#hasta').val(),
		'tipo' : $('#cmb_tipo').val(),
		'imagenes': rutaimg
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response){
			BETA();
			document.getElementById('t'+num).innerHTML = _response;
			document.getElementById('t3').style.display = '';
			document.getElementById('btn3').disabled = false;
			return;
		}
	});
}

function Oculta(obj){
	if(document.getElementById('t'+obj).style.display == 'none'){
		document.getElementById('t'+obj).style.display = '';
		document.getElementById('i_'+obj).src = img2;
	}else{
		document.getElementById('t'+obj).style.display = 'none';
		document.getElementById('i_'+obj).src = img1;
	}
}

function Vector(ctrl){
	var str = "";
	var control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		str += ";" + control[i].value.replace(/&/g, ''); 
	}
	return str.substring(1);
}

function CursosMas(){
	document.getElementById('t3').style.display = '';
	document.getElementById('btn3').disabled = false;
	
	var linea = document.getElementsByName("tipo").length;
	var fila = document.createElement("tr");
	var colum = new Array(5);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	colum[4] = document.createElement("td");
        colum[5] = document.createElement("td");
        colum[6] = document.createElement("td");
	
	colum[0].innerHTML = '';
	colum[1].innerHTML = '<select id="tipo' + linea + '" name="tipo"><option value="">N/A</option><option value="S">SGC</option><option value="T">T&eacute;cnica</option><option value="O">Otras</option><option value="I">Internacionales</option></select>';
        colum[2].innerHTML = '<input type="text" id="nombre' + linea + '" name="nombre" size="70" maxlength="150">';
        colum[3].innerHTML = '<input type="text" id="empresa' + linea + '" name="empresa" size="70" maxlength="150">';
	colum[4].innerHTML = '<input type="text" id="fecha' + linea + '" name="fecha" class="fecha" readonly onClick="show_calendar(this.id);">';
	colum[5].innerHTML = '<input type="text" id="horas' + linea + '" name="horas" size="2" maxlength="3" onblur="_INT(this)">';
        colum[6].innerHTML ='<a href="#" onclick="'+"OMEGA('Debe guardar la capacitacion');"+'"><img  alt="subir" title="Subir Adjunto"  src="../../caspha-i/imagenes/subir.png"/></a>';
        
	for(i=0;i<colum.length;i++) fila.appendChild(colum[i]); 	
	
	document.getElementById('t3').appendChild(fila);
}

function datos(){
	if( Mal(1, $('#siglas').val()) ){
		OMEGA('Debe indicar las siglas');
		return;
	}
	
	if( Mal(1, $('#prox').val()) ){
		OMEGA('Debe indicar la fecha de pr�xima actualizaci�n');
		return;
	}
	
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 1,
		'id' : $('#id').val(),
		'siglas' : $('#siglas').val(),
		'prox' : $('#prox').val(),
		'req_cedula' : $('#req_cedula').val(),
		'req_titulo' : $('#req_titulo').val(),
		'req_colegio' : $('#req_colegio').val(),
		'req_hv' : $('#req_hv').val(),
		'req_compromiso' : $('#req_compromiso').val(),
		'req_competente' : $('#req_competente').val(),
		'req_perfil' : $('#req_perfil').val(),
		'req_clase' : $('#req_clase').val(),
		'req_especialidad' : $('#req_especialidad').val(),
		'req_induccion' : $('#req_induccion').val(),
		'req_autorizacion' : $('#req_autorizacion').val(),
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn1').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '1':
					OMEGA('Transaccion finalizada');
					$('#btn1').disabled = false;
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Paso2(){
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso': 2,
		'id' : $('#id').val(),
		'tipo' : Vector('tipo'),
		'nombre' : Vector('nombre'),
                'empresa' : Vector('empresa'),
		'fecha' : Vector('fecha'),
		'horas' : Vector('horas')
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn3').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn3').disabled = false;
					break;
				case '1':
					OMEGA('Transaccion finalizada');
					//$('#btn3').disabled = false;
                                        location.reload();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Paso3(_linea){
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso': 5,
		'id' : $('#id').val(),
		'linea' : _linea,
		'tipo' : $('#tipo'+_linea).val(),
		'nombre' : $('#nombre'+_linea).val(),
		'fecha' : $('#fecha'+_linea).val(),
		'horas' : $('#horas'+_linea).val(),
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					OMEGA('Transaccion finalizada');
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function SubirAdjunto(linea){
   window.open('../calidad/inducciones_detalle_subir.php?ID='+$('#_ID').val()+'&l='+linea,"","width=360,height=150,scrollbars=yes,status=no");
}