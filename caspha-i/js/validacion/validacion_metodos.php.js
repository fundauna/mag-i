$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"sScrollX": "100%",
		/*"sScrollXInner": "110%",*/
		"bScrollCollapse": true,
		"bFilter": true,
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function SolicitudesAgrega(){
	var tipo = document.getElementById('tipo2').value;
	if(tipo==3 || tipo==1) size = 920;
	else size = 800;
	window.open("validacion" + tipo + "_detalle.php?acc=I&ID=","","width="+size+",height=600,scrollbars=yes,status=no");
	//window.showModalDialog("validacion" + tipo + "_detalle.php?acc=I&ID=", this.window, "dialogWidth:"+size+"px;dialogHeight:750px;status:no;");
}

function SolicitudesModifica(_ID, _tipo){
	if(_tipo==3 || _tipo==1) size = 920;
	else size = 800;
	window.open("validacion" + _tipo + "_detalle.php?acc=M&ID="+_ID,"","width="+size+",height=600,scrollbars=yes,status=no");
	//window.showModalDialog("validacion" + _tipo + "_detalle.php?acc=M&ID="+_ID, this.window, "dialogWidth:"+size+"px;dialogHeight:750px;status:no;");
}

function SolicitudesBuscar(btn){
	if( Mal(1, $('#desde').val()) ){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	if( Mal(1, $('#hasta').val()) ){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	btn.disabled = true;
	btn.form.submit();
}