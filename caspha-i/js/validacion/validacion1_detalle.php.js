var _conc = 0;
var _cantidad = 4;
var _promedioB = _promedioC = 0
var _r = _m = _b = 0;

function __lolo(){
	document.getElementById('metodo').value = 'CIPAC L';
	document.getElementById('estandar').value = '13-165';
	document.getElementById('IA').value = 'NOVALURON';
	document.getElementById('masa').value = '31.7';
	document.getElementById('pureza').value = '99.5';
	document.getElementById('volumen').value = '50';
	document.getElementById('balon').value = '5';
	document.getElementById('A0').value = '0.37850';
	document.getElementById('A1').value = '0.50466';
	document.getElementById('A2').value = '0.56775';
	document.getElementById('A3').value = '0.63083';
	document.getElementById('C0').value = '4434.8';
	document.getElementById('C1').value = '5846.7';
	document.getElementById('C2').value = '6626.2';
	document.getElementById('C3').value = '7296.7';
	__calcula();
}

function Procesar(){
	if(!confirm('Enviar documento a revisi�n?')) return;
	_Modificar(1);
}

function Revisar(){
	if(!confirm('Enviar documento al superior?')) return;
	_Modificar(3);
}

function Aprobar(){
	if(!confirm('Aprobar documento?')) return;
	_Modificar(2);
}

function Anular(){
	if(!confirm('Anular documento?')) return;
	_Modificar(5);
}

function _Modificar(_estado){
	var parametros = {
		'_AJAX' : 1,
		'paso' : 3,
		'cs' :$('#cs').val(),
		'estado' : _estado,
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					alert("Transaccion finalizada");
					opener.location.reload();
					window.close();				
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function __calcula(){
	var _conc = document.getElementById('masa').value*document.getElementById('pureza').value/100/document.getElementById('volumen').value;
	
	for(i=0,sumaB=0,sumaC=0,sumaN=0;i<_cantidad;i++){
		tmp = _conc*document.getElementById('A'+i).value/document.getElementById('balon').value;
		sumaB += parseFloat(tmp);
		sumaC += parseFloat(document.getElementById('C'+i).value);
		sumaN += parseFloat(tmp*tmp);
		document.getElementById('B'+i).value = _RED2(tmp, 5);
		document.getElementById('M'+i).innerHTML = _RED2(tmp, 5);
		document.getElementById('N'+i).innerHTML = _RED2(tmp*tmp, 5);
		document.getElementById('R'+i).innerHTML = document.getElementById('C'+i).value;
		document.getElementById('B'+i).title = tmp;
	}
	
	_promedioB = sumaB/_cantidad;
	_promedioC = sumaC/_cantidad;
	
	for(i=0,sumaD=0,sumaE=0,sumaF=0,sumaG=0,sumaH=0;i<_cantidad;i++){
		tmp = document.getElementById('B'+i).title-_promedioB;
		tmp2 = document.getElementById('C'+i).value-_promedioC;
		sumaD += parseFloat(tmp);
		sumaE += parseFloat(tmp*tmp);
		sumaF += parseFloat(tmp2);
		sumaG += parseFloat(tmp2*tmp2);
		//
		document.getElementById('D'+i).value = _RED2(tmp, 5);
		document.getElementById('D'+i).title = tmp;
		document.getElementById('E'+i).value = _RED2(tmp*tmp, 7);
		document.getElementById('E'+i).title = tmp*tmp;
		document.getElementById('F'+i).value = _RED2(tmp2, 2);
		document.getElementById('F'+i).title = tmp2;
		document.getElementById('G'+i).value = _RED2(tmp2*tmp2, 3);
		document.getElementById('G'+i).title = tmp2*tmp2;
		//
		tmp3 = document.getElementById('D'+i).title*document.getElementById('F'+i).title;
		sumaH += parseFloat(tmp3);
		document.getElementById('H'+i).value = _RED2(tmp3, 2);
		document.getElementById('H'+i).title = tmp3;
	}
	//
	_r = sumaH/Math.pow(sumaE*sumaG, 0.5);
	_m = sumaH/sumaE;
	_b = _promedioC-_m*_promedioB;
	//
	document.getElementById('conc').innerHTML = _RED2(_conc, 5);
	document.getElementById('B'+i).value = _RED2(sumaB, 5);
	document.getElementById('B'+i).title = sumaB;
	document.getElementById('C'+i).value = _RED2(sumaC, 4);
	document.getElementById('C'+i).title = sumaC;
	document.getElementById('D'+i).value = _RED2(sumaD, 4);
	document.getElementById('D'+i).title = sumaD;
	document.getElementById('E'+i).value = _RED2(sumaE, 4);
	document.getElementById('E'+i).title = sumaE;
	document.getElementById('F'+i).value = _RED2(sumaF, 4);
	document.getElementById('F'+i).title = sumaF;
	document.getElementById('G'+i).value = _RED2(sumaG, 4);
	document.getElementById('G'+i).title = sumaG;
	document.getElementById('H'+i).value = _RED2(sumaH, 4);
	document.getElementById('H'+i).title = sumaH;
	document.getElementById('N'+i).innerHTML = _RED2(sumaN, 4);
	//
	document.getElementById('B5').value = _RED2(_promedioB, 5);
	document.getElementById('B5').title = _promedioB;
	document.getElementById('C5').value = _RED2(_promedioC, 4);
	document.getElementById('C5').title = _promedioC;
	//
	document.getElementById('_r').innerHTML = _RED2(_r, 6);
	document.getElementById('_m').innerHTML = _RED2(_m, 2);
	document.getElementById('_b').innerHTML = _RED2(_b, 2);
	//
	for(i=0,sumaQ=0;i<_cantidad;i++){
		tmp = _m*parseFloat(document.getElementById('B'+i).title)+parseFloat(_b);
		document.getElementById('O'+i).innerHTML = _RED2(tmp, 2);
		tmp2 = Math.abs(document.getElementById('R'+i).innerHTML-document.getElementById('O'+i).innerHTML);
		document.getElementById('P'+i).innerHTML = _RED2(tmp2, 2);
		document.getElementById('Q'+i).innerHTML = _RED2(tmp2*tmp2, 2);
		sumaQ += parseFloat(tmp2*tmp2);
	}
	
	document.getElementById('Q'+i).innerHTML = _RED2(sumaQ, 2);
	//
	sxy = Math.pow(sumaQ/2, 0.5);
	sm = sxy/Math.pow(sumaE, 0.5);
	sb = sxy*Math.pow(sumaN/(4*sumaE), 0.5);
	ldd = ((_b+3*sxy)-_b)/_m;
	ldc = ((_b+10*sxy)-_b)/_m;
	//
	document.getElementById('sxy').innerHTML = _RED2(sxy, 1);
	document.getElementById('sm').innerHTML = _RED2(sm, 1);
	document.getElementById('sb').innerHTML = _RED2(sb, 1);
	document.getElementById('ldd').innerHTML = _RED2(ldd, 4);
	document.getElementById('ldc').innerHTML = _RED2(ldc, 4);
	//
	if(_r>0.99) document.getElementById('cumple').innerHTML = 'S�';
	else document.getElementById('cumple').innerHTML = 'No';
}

function datos(){	
	if($('#metodo').val()==''){
		OMEGA('Debe indicar el m�todo');
		return;
	}
	
	if($('#tmp_equipo').val()==''){
		OMEGA('Debe indicar el equipo');
		return;
	}
	
	if($('#estandar').val()==''){
		OMEGA('Debe indicar el estandar');
		return;
	}
	
	if($('#IA').val()==''){
		OMEGA('Debe indicar el ingrediente activo');
		return;
	}
	
	if(!confirm('Ingresar ensayo?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 1,
		'tipo' : 1,
		'metodo' : $('#metodo').val(),
		'masa' : $('#masa').val(),
		'equipo' : $('#equipo').val(),
		'pureza' : $('#pureza').val(),
		'volumen' : $('#volumen').val(),
		'estandar' : $('#estandar').val(),
		'balon' : $('#balon').val(),
		'obs' : $('#obs').val(),
		'IA' : $('#IA').val(),
		'A0' : $('#A0').val(),
		'A1' : $('#A1').val(),
		'A2' : $('#A2').val(),
		'A3' : $('#A3').val(),
		'C0' : $('#C0').val(),
		'C1' : $('#C1').val(),
		'C2' : $('#C2').val(),
		'C3' : $('#C3').val(),
		'r' : _r
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					opener.location.reload();
					alert("Transaccion finalizada");
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Redondear(txt){
	_RED(txt, 5);
	__calcula();
}

function Generar(){	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 2,
		'x0' : $('#B0').val(),
		'x1' : $('#B1').val(),
		'x2' : $('#B2').val(),
		'x3' : $('#B3').val(),
		'y0' : $('#C0').val(),
		'y1' : $('#C1').val(),
		'y2' : $('#C2').val(),
		'y3' : $('#C3').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response){
			if(_response == ''){
				OMEGA("No hay datos que mostrar");
				return;
			}
			BETA();
			document.getElementById('td_tabla').innerHTML = _response;
			GeneraGrafico();
			return;
		}
	});
}

function GeneraGrafico(){
	document.getElementById('tr_grafico').style.display = '';
	var r2 = _RED2(_r*_r, 4);

	$(function () {
		$('#container').highcharts({
			legend: {
            	enabled: false
       		},
			data: {
				table: document.getElementById('tabla')
			},
			title: {
                text: 'Curva de calibraci�n',
                x: -20 //center
            },
			subtitle: {
				text: 'r2=' + r2,
				floating: true,
				align: 'right',
				x: -20,
				verticalAlign: 'bottom',
				y: -75
			},
            yAxis: {
                title: {
                    text: 'Area'
                }
            },
			xAxis: {
                title: {
                    text: 'Concentraci�n (mg/mL)'
                },
				gridLineWidth: 1
            },
          	tooltip: {
				enabled: false
			}
		});
	});
}

function EquiposLista(){
	window.open(__SHELL__ + "?list=1","","width=500,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:500px;dialogHeight:200px;status:no;");
}

function EquipoEscoge(cs, codigo, linea){
	opener.document.getElementById('equipo').value=cs;
	opener.document.getElementById('tmp_equipo').value=codigo;
	window.close();
}