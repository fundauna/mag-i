function Muestra(_valor){
	if(_valor=='1'){
		document.getElementById('pagina1').style.display = '';
		document.getElementById('pagina3').style.display = '';
		document.getElementById('pagina2').style.display = 'none';
	}else{
		document.getElementById('pagina2').style.display = '';
		document.getElementById('pagina1').style.display = 'none';
		document.getElementById('pagina3').style.display = 'none';
	}
}

function Procesar(){
	if(!confirm('Enviar documento al superior?')) return;
	_Modificar(3);
}

function Aprobar(){
	if(!confirm('Aprobar documento?')) return;
	_Modificar(2);
}

function Anular(){
	if(!confirm('Anular documento?')) return;
	_Modificar(5);
}

function _Modificar(_estado){
	var parametros = {
		'_AJAX' : 1,
		'paso' : 2,
		'cs' :$('#cs').val(),
		'estado' : _estado,
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					alert("Transaccion finalizada");
					opener.location.reload();
					window.close();				
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function EquiposLista(){
	window.open(__SHELL__ + "?list=1","","width=500,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:500px;dialogHeight:200px;status:no;");
}

function EquipoEscoge(cs, codigo, linea){
	opener.document.getElementById('equipo').value=cs;
	opener.document.getElementById('tmp_equipo').value=codigo;
	window.close();
}

function ValidaAprobada(){
	if( $('#validacion').val() == '' ) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 0,
		'validacion' : $('#validacion').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '1':
					BETA();
					break;
				case '2':
					OMEGA('El No. de validaci�n no existe o no est� aprobado');
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Valida(_accion){
	if( Mal(4, $('#validacion').val()) ){
		OMEGA('Debe indicar el No. de validaci�n');
		return;
	}
	
	if( Mal(1, $('#equipo').val()) ){
		OMEGA('Debe seleccionar el equipo');
		return;
	}
	
	if(!confirm('Ingresar ensayo?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 1,
		'accion' : _accion,
		'cs' : $('#cs').val(),
		'validacion' : $('#validacion').val(),
		'objetivo' : $('#objetivo').val(),
		'analitos' : $('#analitos').val(),
		'ambito' : $('#ambito').val(),
		'tipo_metodo' : $('#tipo_metodo').val(),
		'cod_metodo' : $('#cod_metodo').val(),
		'precision' : $('#precision').val(),
		'veracidad' : $('#veracidad').val(),
		'limite1' : $('#limite1').val(),
		'limite2' : $('#limite2').val(),
		'linealidad' : $('#linealidad').val(),
		'especificidad' : $('#especificidad').val(),
		'efecto' : $('#efecto').val(),
		'robustez' : $('#robustez').val(),
		'inc' : $('#inc').val(),
		'ref' : $('#ref').val(),
		'modif' : $('#modif').val(),
		'tecnica' : $('#tecnica').val(),
		'interno' : $('#interno').val(),
		'bitacora' : $('#bitacora').val(),
		'pagina' : $('#pagina').val(),
		'equipo' : $('#equipo').val(),
		'otros' : $('#otros').val(),
		'resumen' : $('#resumen').val(),
		'tratamiento' : $('#tratamiento').val(),
		'declaracion' : $('#declaracion').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					alert("Transaccion finalizada");
					window.close();				
					break;
				case '2':
					OMEGA('El No. de validaci�n no existe o no est� aprobado');
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}
