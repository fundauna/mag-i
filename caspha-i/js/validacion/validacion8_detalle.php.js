function __lolo(){
	$('#metodo').val('M�todo 1');
	$('#estandar').val('Estandar 1');
	$('#masa').val('30');
	$('#analito').val('50');
	$('#pureza').val('40');
	$('#vol_ext').val(1);
	$('#coefM').val(4);
	$('#ali_ext').val(2);
	$('#coefB').val(5);
	$('#vol_fin').val(3);
	$('#curva').val(6);
	$('#obs').val('obs lolo');
	$('#fac1').val('fac1');
	$('#fac2').val('fac2');
	$('#mas1').val('mas1');
	$('#mas2').val('mas2');
	$('#men1').val('men1');
	$('#men2').val('men2');
	$('#mass0').val(11);
	$('#mass1').val(12);
	$('#mass2').val(13);
	$('#mass3').val(14);
	$('#area0').val(15);
	$('#area1').val(16);
	$('#area2').val(17);
	$('#area3').val(18);
	__calcula();
}

function EquiposLista(_linea){
	window.open(__SHELL__ + "?list="+_linea,"","width=500,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list="+_linea, this.window, "dialogWidth:500px;dialogHeight:200px;status:no;");
}


function EquipoEscoge(cs, codigo, linea){
	opener.document.getElementById('equipo').value=cs;
	opener.document.getElementById('tmp_equipo1').value=codigo;
	window.close();
}

function Procesar(){
	if(!confirm('Enviar documento a revisi�n?')) return;
	_Modificar(1);
}

function Revisar(){
	if(!confirm('Enviar documento al superior?')) return;
	_Modificar(3);
}

function Aprobar(){
	if(!confirm('Aprobar documento?')) return;
	_Modificar(2);
}

function Anular(){
	if(!confirm('Anular documento?')) return;
	_Modificar(5);
}

function _Modificar(_estado){
	var parametros = {
		'_AJAX' : 1,
		'paso' : 3,
		'cs' :$('#cs').val(),
		'estado' : _estado,
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					alert("Transaccion finalizada");
					opener.location.reload();
					window.close();				
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function __calcula(){
	var linea = document.getElementsByName("mass").length;
	var _promE = 0;
	
	for(i=0;i<linea;i++){
		var tmp = (($('#area'+i).val()-$('#coefB').val())/$('#coefM').val())*($('#vol_fin').val()/$('#ali_ext').val())*($('#vol_ext').val()/($('#mass'+i).val()/1000000));
		document.getElementById('conc'+i).innerHTML = _RED2(tmp, 4);
		document.getElementById('conc'+i).title = tmp;
	}
	//
	var prom1 = (parseFloat(document.getElementById('conc0').title)+parseFloat(document.getElementById('conc2').title))/2;
	var prom2 = (parseFloat(document.getElementById('conc1').title)+parseFloat(document.getElementById('conc3').title))/2;
	var prom3 = (parseFloat(document.getElementById('conc0').title)+parseFloat(document.getElementById('conc1').title))/2;
	var prom4 = (parseFloat(document.getElementById('conc2').title)+parseFloat(document.getElementById('conc3').title))/2;
	//
	var prom = (parseFloat(document.getElementById('conc0').title)+parseFloat(document.getElementById('conc1').title)+parseFloat(document.getElementById('conc2').title)+parseFloat(document.getElementById('conc3').title))/4;
	var varianza = 0;
	varianza += Math.pow(document.getElementById('conc0').title - prom, 2);
	varianza += Math.pow(document.getElementById('conc1').title - prom, 2);
	varianza += Math.pow(document.getElementById('conc2').title - prom, 2);
	varianza += Math.pow(document.getElementById('conc3').title - prom, 2);
	var de1 = (Math.sqrt(varianza)/Math.sqrt(3))*Math.sqrt(2);
	
	document.getElementById('resA').innerHTML = _RED2(prom1-prom2, 3);
	document.getElementById('resB').innerHTML = _RED2(prom3-prom4, 2);
	document.getElementById('resC').innerHTML = _RED2(de1, 4);
	
	//COMPROBACION
	if( parseFloat(document.getElementById('resC').innerHTML) > parseFloat(document.getElementById('resA').innerHTML) && parseFloat(document.getElementById('resC').innerHTML) > parseFloat(document.getElementById('resB').innerHTML) )
		document.getElementById('resD').innerHTML = '<strong>Robusto</strong>';
	else
		document.getElementById('resD').innerHTML = '<strong>No Robusto</strong>';
}

function datos(){		
	if($('#metodo').val()==''){
		OMEGA('Debe indicar el m�todo');
		return;
	}
	
	if($('#estandar').val()==''){
		OMEGA('Debe indicar el estandar');
		return;
	}
	
	if($('#tmp_equipo1').val()==''){
		OMEGA('Debe indicar el equipo');
		return;
	}
	
	if(!confirm('Ingresar ensayo?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 1,
		'tipo' : 8,
		'metodo' : $('#metodo').val(),
		'equipo' : $('#equipo').val(),
		'estandar' : $('#estandar').val(),
		'masa1' : $('#masa1').val(),
		'analito' : $('#analito').val(),
		'pureza' : $('#pureza').val(),
		'vol_ext' : $('#vol_ext').val(),
		'coefM' : $('#coefM').val(),
		'ali_ext' : $('#ali_ext').val(),
		'coefB' : $('#coefB').val(),
		'vol_fin' : $('#vol_fin').val(),
		'curva' : $('#curva').val(),
		'obs' : $('#obs').val(),
		'fac1' : $('#fac1').val(),
		'fac2' : $('#fac2').val(),
		'mas1' : $('#mas1').val(),
		'mas2' : $('#mas2').val(),
		'men1' : $('#men1').val(),
		'men2' : $('#men2').val(),
		'mass0' : $('#mass0').val(),
		'mass1' : $('#mass1').val(),
		'mass2' : $('#mass2').val(),
		'mass3' : $('#mass3').val(),
		'area0' : $('#area0').val(),
		'area1' : $('#area1').val(),
		'area2' : $('#area2').val(),
		'area3' : $('#area3').val()
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					opener.location.reload();
					alert("Transaccion finalizada");
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Redondear(txt){
	_RED(txt, 4);
	__calcula();
}

function vector(ctrl){
	var str = "1=1";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		str += "&" + control[i].value.replace(/&/g, ''); 
	}
	return str;
}