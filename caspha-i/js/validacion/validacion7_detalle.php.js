function __lolo(){
	$('#metodo').val('M�todo 1');
	$('#estandar').val('Estandar 1');
	$('#masa').val('30');
	$('#analito').val('50');
	$('#pureza').val('40');
	$('#vol_ext').val(1);
	$('#coefM').val(4);
	$('#ali_ext').val(2);
	$('#coefB').val(5);
	$('#vol_fin').val(3);
	$('#curva').val(6);
	$('#_masa0').val(7);
	$('#_area0').val(8);
	$('#_res0').val(9);
	$('#_masa1').val(10);
	$('#_area1').val(11);
	$('#_res1').val(12);
	$('#_masa2').val(13);
	$('#_area2').val(14);
	$('#_res2').val(15);
	$('#_mass0').val(20);
	$('#_mass1').val(21);
	$('#_mass2').val(22);
	$('#_mass3').val(23);
	$('#_mass4').val(24);
	$('#_mass5').val(25);
	$('#_mass6').val(26);
	$('#_mass7').val(27);
	$('#_mass8').val(28);
	$('#_are0').val(30);
	$('#_are1').val(31);
	$('#_are2').val(32);
	$('#_are3').val(33);
	$('#_are4').val(34);
	$('#_are5').val(35);
	$('#_are6').val(36);
	$('#_are7').val(37);
	$('#_are8').val(38);
	$('#_reso0').val(40);
	$('#_reso1').val(41);
	$('#_reso2').val(42);
	$('#_reso3').val(43);
	$('#_reso4').val(44);
	$('#_reso5').val(45);
	$('#_reso6').val(46);
	$('#_reso7').val(47);
	$('#_reso8').val(48);
	$('#_enri0').val(50);
	$('#_enri1').val(51);
	$('#_enri2').val(52);
	$('#_enri3').val(53);
	$('#_enri4').val(54);
	$('#_enri5').val(55);
	$('#_enri6').val(56);
	$('#_enri7').val(57);
	$('#_enri8').val(58);
	$('#_fexp0').val(70);
	$('#_fexp1').val(71);
	$('#_fexp2').val(72);
	$('#_fteo0').val(19);
	$('#_fteo1').val(19);
	$('#_fteo2').val(19);
	__calcula();
}

function EquiposLista(_linea){
	window.open(__SHELL__ + "?list="+_linea,"","width=500,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list="+_linea, this.window, "dialogWidth:500px;dialogHeight:200px;status:no;");
}

function EquipoEscoge(cs, codigo, linea){
	opener.document.getElementById('equipo').value=cs;
	opener.document.getElementById('tmp_equipo1').value=codigo;
	window.close();
}

function Procesar(){
	if(!confirm('Enviar documento a revisi�n?')) return;
	_Modificar(1);
}

function Revisar(){
	if(!confirm('Enviar documento al superior?')) return;
	_Modificar(3);
}

function Aprobar(){
	if(!confirm('Aprobar documento?')) return;
	_Modificar(2);
}

function Anular(){
	if(!confirm('Anular documento?')) return;
	_Modificar(5);
}

function _Modificar(_estado){
	var parametros = {
		'_AJAX' : 1,
		'paso' : 3,
		'cs' :$('#cs').val(),
		'estado' : _estado,
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					alert("Transaccion finalizada");
					opener.location.reload();
					window.close();				
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function __calcula(){
	var linea = document.getElementsByName("_masa").length;
	var _promE = 0;
	for(i=0;i<linea;i++){
		var tmp = (($('#_area'+i).val()-$('#coefB').val())/$('#coefM').val())*($('#vol_fin').val()/$('#ali_ext').val())*($('#vol_ext').val()/($('#_masa'+i).val()/1000000));
		_promE += parseFloat(tmp);
		document.getElementById('_conc'+i).innerHTML = _RED2(tmp, 4);
		document.getElementById('_conc'+i).title = tmp;
	}
	_promE /= linea;
	//
	var linea = document.getElementsByName("_mass").length;
	for(i=0;i<linea;i++){
		var tmp1 = ((($('#_are'+i).val()-$('#coefB').val())/$('#coefM').val())*($('#vol_fin').val()/$('#ali_ext').val())*($('#vol_ext').val()/($('#_mass'+i).val()/1000000)))-_promE;
		var tmp2 = (tmp1/$('#_enri'+i).val())*100;
		
		document.getElementById('_enco'+i).innerHTML = _RED2(tmp1, 4);
		document.getElementById('_recu'+i).innerHTML = _RED2(tmp2, 4);
		document.getElementById('_enco'+i).title = tmp1;
		document.getElementById('_recu'+i).title = tmp2;
	}
	//CALCULO VARIANZA MUESTRAL
	var _x1 = (parseFloat(document.getElementById('_enco0').title)+parseFloat(document.getElementById('_enco1').title)+parseFloat(document.getElementById('_enco2').title)) / 3;
	var _x2 = (parseFloat(document.getElementById('_enco3').title)+parseFloat(document.getElementById('_enco4').title)+parseFloat(document.getElementById('_enco5').title)) / 3;
	var _x3 = (parseFloat(document.getElementById('_enco6').title)+parseFloat(document.getElementById('_enco7').title)+parseFloat(document.getElementById('_enco8').title)) / 3;
	var varS1 = Math.pow(document.getElementById('_enco0').title-_x1,2) + Math.pow(document.getElementById('_enco1').title-_x1,2) + Math.pow(document.getElementById('_enco2').title-_x1,2);
	var varS2 = Math.pow(document.getElementById('_enco3').title-_x2,2) + Math.pow(document.getElementById('_enco4').title-_x2,2) + Math.pow(document.getElementById('_enco5').title-_x2,2);
	var varS3 = Math.pow(document.getElementById('_enco6').title-_x3,2) + Math.pow(document.getElementById('_enco7').title-_x3,2) + Math.pow(document.getElementById('_enco8').title-_x3,2);
	//CALCULO PROMEDIO RECUPRACION
	var promR1 = (parseFloat( document.getElementById('_recu0').title )+parseFloat( document.getElementById('_recu1').title )+parseFloat( document.getElementById('_recu2').title ))/3;
	var promR2 = (parseFloat( document.getElementById('_recu3').title )+parseFloat( document.getElementById('_recu4').title )+parseFloat( document.getElementById('_recu5').title ))/3;
	var promR3 = (parseFloat( document.getElementById('_recu6').title )+parseFloat( document.getElementById('_recu7').title )+parseFloat( document.getElementById('_recu8').title ))/3;
	//CALCULO DEV EST.
	var de1 = Math.sqrt(varS1)/Math.sqrt(2);
	var de2 = Math.sqrt(varS2)/Math.sqrt(2);
	var de3 = Math.sqrt(varS3)/Math.sqrt(2);
	//CALCULO CV ENRIQUECIDA
	var cv1 = de1/_x1*100;
	var cv2 = de2/_x2*100;
	var cv3 = de3/_x3*100;
	//
	document.getElementById('va1').innerHTML = _RED2(varS1/2, 2);
	document.getElementById('va2').innerHTML = _RED2(varS2/2, 2);
	document.getElementById('va3').innerHTML = _RED2(varS3/2, 2);
	document.getElementById('de1').innerHTML = _RED2(de1, 4);
	document.getElementById('de2').innerHTML = _RED2(de2, 4);
	document.getElementById('de3').innerHTML = _RED2(de3, 4);
	document.getElementById('re1').innerHTML = _RED2(promR1, 4);
	document.getElementById('re2').innerHTML = _RED2(promR2, 4);
	document.getElementById('re3').innerHTML = _RED2(promR3, 4);
	document.getElementById('cv1').innerHTML = _RED2(cv1, 4);
	document.getElementById('cv2').innerHTML = _RED2(cv2, 4);
	document.getElementById('cv3').innerHTML = _RED2(cv3, 4);
	//COMPROBACION'
	if( parseFloat($('#_fteo0').val()) > parseFloat($('#_fexp0').val()) && parseFloat($('#_fteo1').val()) > parseFloat($('#_fexp1').val()) && parseFloat($('#_fteo2').val()) > parseFloat($('#_fexp2').val()) )
		document.getElementById('regresion').innerHTML = '<strong>Homocedastica</strong>';
	else
		document.getElementById('regresion').innerHTML = '<strong>Heterocedastica</strong>';
}

function datos(){		
	if($('#metodo').val()==''){
		OMEGA('Debe indicar el m�todo');
		return;
	}
	
	if($('#estandar').val()==''){
		OMEGA('Debe indicar el estandar');
		return;
	}
	
	if($('#tmp_equipo1').val()==''){
		OMEGA('Debe indicar el equipo');
		return;
	}
	
	if(!confirm('Ingresar ensayo?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 1,
		'tipo' : 7,
		'metodo' : $('#metodo').val(),
		'equipo' : $('#equipo').val(),
		'estandar' : $('#estandar').val(),
		'masa1' : $('#masa1').val(),
		'analito' : $('#analito').val(),
		'pureza' : $('#pureza').val(),
		'vol_ext' : $('#vol_ext').val(),
		'coefM' : $('#coefM').val(),
		'ali_ext' : $('#ali_ext').val(),
		'coefB' : $('#coefB').val(),
		'vol_fin' : $('#vol_fin').val(),
		'curva' : $('#curva').val(),
		'obs' : $('#obs').val(),
		'_fexp0' : $('#_fexp0').val(),
		'_fexp1' : $('#_fexp1').val(),
		'_fexp2' : $('#_fexp2').val(),
		'_fteo0' : $('#_fteo0').val(),
		'_fteo1' : $('#_fteo1').val(),
		'_fteo2' : $('#_fteo2').val(),
		'masa' : vector('_masa'),
		'area' : vector('_area'),
		'res' : vector('_res'),
		'mass' : vector('_mass'),
		'are' : vector('_are'),
		'reso' : vector('_reso'),
		'enri' : vector('_enri')
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					opener.location.reload();
					alert("Transaccion finalizada");
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Redondear(txt){
	_RED(txt, 4);
	__calcula();
}

function vector(ctrl){
	var str = "1=1";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		str += "&" + control[i].value.replace(/&/g, ''); 
	}
	return str;
}