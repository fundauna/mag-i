var _prom = _desv = 0;

function __lolo(){
	document.getElementById('IA').value = 'I.A.1';
	document.getElementById('estandar').value = 'Estandar 1';
	
	document.getElementById('A0').value = '534.6';
	document.getElementById('A1').value = '535.7';
	document.getElementById('A2').value = '534.1';
	document.getElementById('A3').value = '539.9';
	document.getElementById('A4').value = '541.2';
	document.getElementById('A5').value = '539.0';
	document.getElementById('A6').value = '534.2';
	document.getElementById('B0').value = '5745.7';
	document.getElementById('B1').value = '5764.7';
	document.getElementById('B2').value = '5751.2';
	document.getElementById('B3').value = '5741.1';
	document.getElementById('B4').value = '5737.7';
	document.getElementById('B5').value = '5741.2';
	document.getElementById('B6').value = '5728.5';
	document.getElementById('C0').value = '5743.1';
	document.getElementById('C1').value = '5755.0';
	document.getElementById('C2').value = '5758.3';
	document.getElementById('C3').value = '5737.6';
	document.getElementById('C4').value = '5728.1';
	document.getElementById('C5').value = '5730.3';
	document.getElementById('C6').value = '5725.5';
	document.getElementById('D0').value = '5758.8';
	document.getElementById('D1').value = '5751.5';
	document.getElementById('D2').value = '5759.4';
	document.getElementById('D3').value = '5735.5';
	document.getElementById('D4').value = '5737.0';
	document.getElementById('D5').value = '5733.6';
	document.getElementById('D6').value = '5730.6';
	document.getElementById('vol_bal').value = '100';
	document.getElementById('mv').value = '10';
	document.getElementById('vol_int').value = '100';
	document.getElementById('ali_mue').value = '1';
	document.getElementById('densidad').value = '1.0812';
	document.getElementById('ali_enr').value = '2';
	document.getElementById('ali_dil').value = '5';
	document.getElementById('vol_bal2').value = '3';
	document.getElementById('masa').value = '49.6';
	document.getElementById('vol_dil').value = '6';
	document.getElementById('mm').value = '4';
	document.getElementById('pureza').value = '99.5';
	//
	document.getElementById('G0').value = '5588.2';
	document.getElementById('G1').value = '5598.6';
	document.getElementById('G2').value = '5579.7';
	document.getElementById('G3').value = '5676.7';
	document.getElementById('G4').value = '5654.7';
	document.getElementById('G5').value = '5620.7';
	document.getElementById('G6').value = '5580.7';
	document.getElementById('H0').value = '5589.9';
	document.getElementById('H1').value = '5603.5';
	document.getElementById('H2').value = '5577.2';
	document.getElementById('H3').value = '5673.6';
	document.getElementById('H4').value = '5647.7';
	document.getElementById('H5').value = '5624.5';
	document.getElementById('H6').value = '5582.5';
	document.getElementById('I0').value = '5578.2';
	document.getElementById('I1').value = '5601.1';
	document.getElementById('I2').value = '5578.0';
	document.getElementById('I3').value = '5671.9';
	document.getElementById('I4').value = '5651.0';
	document.getElementById('I5').value = '5632.6';
	document.getElementById('I6').value = '5589.0';
	__calcula();
}

function Procesar(){
	if(!confirm('Enviar documento a revisi�n?')) return;
	_Modificar(1);
}

function Revisar(){
	if(!confirm('Enviar documento al superior?')) return;
	_Modificar(3);
}

function Aprobar(){
	if(!confirm('Aprobar documento?')) return;
	_Modificar(2);
}

function Anular(){
	if(!confirm('Anular documento?')) return;
	_Modificar(5);
}

function _Modificar(_estado){
	var parametros = {
		'_AJAX' : 1,
		'paso' : 3,
		'cs' :$('#cs').val(),
		'estado' : _estado,
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					alert("Transaccion finalizada");
					opener.location.reload();
					window.close();				
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function __calcula(){
	var linea = document.getElementsByName("_A").length;
	
	var conc = $('#masa').val()*$('#pureza').val()/100/$('#vol_int').val();
	document.getElementById('conc').innerHTML = _RED2(conc, 5);
	
	for(i=0;i<linea;i++){
		tmp = (parseFloat($('#B'+i).val()) + parseFloat($('#C'+i).val()) + parseFloat($('#D'+i).val())) / 3;
		tmp2 = (parseFloat($('#G'+i).val()) + parseFloat($('#H'+i).val()) + parseFloat($('#I'+i).val())) / 3;
		document.getElementById('E'+i).value = _RED2(tmp, 2);
		document.getElementById('E'+i).title = tmp;
		document.getElementById('J'+i).value = _RED2(tmp2, 1);
		document.getElementById('J'+i).title = tmp2;
		//
		fVariance = 0;
		fVariance += parseFloat(Math.pow($('#B'+i).val() - tmp, 2));
		fVariance += parseFloat(Math.pow($('#C'+i).val() - tmp, 2));
		fVariance += parseFloat(Math.pow($('#D'+i).val() - tmp, 2));
		desv = Math.sqrt(fVariance)/Math.sqrt(2);
		cv = (desv/tmp)*100;
		document.getElementById('F'+i).value = _RED2(cv, 2);
		document.getElementById('F'+i).title = cv;
		//
		fVariance = 0;
		fVariance += parseFloat(Math.pow($('#G'+i).val() - tmp2, 2));
		fVariance += parseFloat(Math.pow($('#H'+i).val() - tmp2, 2));
		fVariance += parseFloat(Math.pow($('#I'+i).val() - tmp2, 2));
		desv = Math.sqrt(fVariance)/Math.sqrt(2);
		cv = (desv/tmp2)*100;
		document.getElementById('K'+i).value = _RED2(cv, 2);
		document.getElementById('K'+i).title = cv;
		//
		tmp3 = (tmp2/tmp)*($('#masa').val()*$('#pureza').val()/100/$('#vol_int').val())*($('#ali_dil').val()/$('#vol_dil').val())*($('#vol_bal2').val()/$('#ali_mue').val())*($('#vol_bal').val()/$('#A'+i).val())*100;
		document.getElementById('L'+i).value = _RED2(tmp3, 2);
		document.getElementById('L'+i).title = tmp3;
		document.getElementById('M'+i).value = _RED2(tmp3*$('#densidad').val(), 2);
		document.getElementById('M'+i).title = tmp3*$('#densidad').val();
	}
	__calcula2();
}

function __calcula2(){
	var linea = document.getElementsByName("_A").length;
	var ttabla = TablaGrubbs(linea);
	document.getElementById('tr_tabla').innerHTML = 'T tabla n=' + linea + ', 95% de confianza';
	document.getElementById('_tabla').innerHTML = ttabla;
	
	if( $('#tipo').val()=='0' ) var letra = 'L';
	else  var letra = 'M';
	_prom = 0;
	var usados = 0;
	
	for(i=0;i<linea;i++){
		if(document.getElementById('X'+i).checked){
			usados++;
			_prom += parseFloat(document.getElementById(letra+i).title);
		}
	}
	
	_prom = _prom / usados;
	
	varianza = 0;
	for(i=0;i<linea;i++){
		if(document.getElementById('X'+i).checked){
			varianza += Math.pow(document.getElementById(letra+i).title - _prom, 2);
		}
	}
	_desv = Math.sqrt(varianza)/Math.sqrt(usados-1);
	_cv = _desv/_prom*100;
	document.getElementById('_prom').innerHTML = _RED2(_prom, 2);
	document.getElementById('_desv').innerHTML = _RED2(_desv, 3);
	document.getElementById('_cv').innerHTML = _RED2(_cv, 3);
	document.getElementById('_lcc').innerHTML = _RED2(_cv, 3);
	
	//VALIDACION DE T EXPERIMENTALES
	for(i=0;i<linea;i++){
		var texp = Math.abs(document.getElementById(letra+i).title-_prom)/_desv;
		document.getElementById('X'+i).title = 'T. exp=' + _RED2(texp, 2);
		if(texp < ttabla) document.getElementById('tr_usar'+i).style.background = "";
		else document.getElementById('tr_usar'+i).style.background = "#CCCCCC";
	}

	_horwitz1 = Math.pow(2, (1-(0.5*Math.log10(_prom/100))));
	_horwitz2 = _horwitz1 * 0.67;
	document.getElementById('_horwitz1').innerHTML = _RED2(_horwitz1, 3);
	document.getElementById('_horwitz2').innerHTML = _RED2(_horwitz2, 3);
	if(_horwitz2 > _cv){
		document.getElementById('_cumple').innerHTML = 'S�';
		document.getElementById('cumple').value = 1;
	}else{
		document.getElementById('_cumple').innerHTML = 'No';
		document.getElementById('cumple').value = 0;
	}
}

function MuestrasMas(){
	var linea = document.getElementsByName("_A").length;
	if(linea>14){
		alert('Ya no puede agregar m�s r�plicas');
		return;
	}
	var fila = document.createElement("tr");
	var colum = new Array(7);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	colum[4] = document.createElement("td");
	colum[5] = document.createElement("td");
	colum[6] = document.createElement("td");
	
	colum[0].innerHTML = (linea+1) + '.';
	colum[1].innerHTML = '<input type="text" id="A'+ linea +'" name="_A" class="monto" onblur="Redondear(this)" value="0">';
	colum[2].innerHTML = '<input type="text" id="B'+ linea +'" name="_B" class="monto" onblur="Redondear(this)" value="0">';
	colum[3].innerHTML = '<input type="text" id="C'+ linea +'" name="_C" class="monto" onblur="Redondear(this)" value="0">';
	colum[4].innerHTML = '<input type="text" id="D'+ linea +'" name="_D" class="monto" onblur="Redondear(this)" value="0">';
	colum[5].innerHTML = '<input type="text" id="E'+ linea +'" class="monto" readonly>';
	colum[6].innerHTML = '<input type="text" id="F'+ linea +'" class="monto" readonly>';
	
	for(i=0;i<colum.length;i++)
		fila.appendChild(colum[i]);
	
	document.getElementById('lolo').appendChild(fila);
	//
	var fila2 = document.createElement("tr");
	var colum2 = new Array(9);
	fila2.id = 'tr_usar' + linea;
	
	colum2[0] = document.createElement("td");
	colum2[1] = document.createElement("td");
	colum2[2] = document.createElement("td");
	colum2[3] = document.createElement("td");
	colum2[4] = document.createElement("td");
	colum2[5] = document.createElement("td");
	colum2[6] = document.createElement("td");
	colum2[7] = document.createElement("td");
	colum2[8] = document.createElement("td");
	
	colum2[0].innerHTML = (linea+1) + '.';
	colum2[1].innerHTML = '<input type="text" id="G'+ linea +'" name="_G" class="monto" onblur="Redondear(this)" value="0">';
	colum2[2].innerHTML = '<input type="text" id="H'+ linea +'" name="_H" class="monto" onblur="Redondear(this)" value="0">';
	colum2[3].innerHTML = '<input type="text" id="I'+ linea +'" name="_I" class="monto" onblur="Redondear(this)" value="0">';
	colum2[4].innerHTML = '<input type="text" id="J'+ linea +'" class="monto" readonly>';
	colum2[5].innerHTML = '<input type="text" id="K'+ linea +'" class="monto" readonly>';
	colum2[6].innerHTML = '<input type="text" id="L'+ linea +'" name="_L" class="monto" readonly>';
	colum2[7].innerHTML = '<input type="text" id="M'+ linea +'" name="_M" class="monto" readonly>';
	colum2[8].innerHTML = '<input type="checkbox" id="X'+ linea +'" name="X" onclick="__calcula()" checked />';
	
	for(i=0;i<colum2.length;i++)
		fila2.appendChild(colum2[i]);
	
	document.getElementById('lolo2').appendChild(fila2);
}

function datos(){		
	if($('#estandar').val()==''){
		OMEGA('Debe indicar el estandar');
		return;
	}
	
	if($('#IA').val()==''){
		OMEGA('Debe indicar el ingrediente activo');
		return;
	}
	
	if(!confirm('Ingresar ensayo?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'paso' : 1,
		'tipo' : 2,
		'vol_bal' : $('#vol_bal').val(),
		'mv' : $('#mv').val(),
		'vol_int' : $('#vol_int').val(),
		'ali_mue' : $('#ali_mue').val(),
		'densidad' : $('#densidad').val(),
		'ali_enr' : $('#ali_enr').val(),
		'estandar' : $('#estandar').val(),
		'ali_dil' : $('#ali_dil').val(),
		'vol_bal2' : $('#vol_bal2').val(),
		'masa' : $('#masa').val(),
		'vol_dil' : $('#vol_dil').val(),
		'mm' : $('#mm').val(),
		'pureza' : $('#pureza').val(),
		'unidad' : $('#tipo').val(),
		'IA' : $('#IA').val(),
		'cumple' : $('#cumple').val(),
		'obs' : '',
		'A' : vector('_A'),
		'B' : vector('_B'),
		'C' : vector('_C'),
		'D' : vector('_D'),
		'G' : vector('_G'),
		'H' : vector('_H'),
		'I' : vector('_I'),
		'MM' : vector('_L'),
		'MV' : vector('_M'),
		'X' : vector2('X')
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					opener.location.reload();
					alert("Transaccion finalizada");
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function Redondear(txt){
	_RED(txt, 4);
	__calcula();
}

function TablaGrubbs(_n){
	if(_n==3) return 1.15;
	else if(_n==4) return 1.46;
	else if(_n==5) return 1.67;
	else if(_n==6) return 1.82;
	else if(_n==7) return 1.94;
	else if(_n==8) return 2.03;
	else if(_n==9) return 2.11;
	else if(_n==10) return 2.18;
	else if(_n==11) return 2.23;
	else if(_n==12) return 2.29;
	else if(_n==13) return 2.33;
	else if(_n==14) return 2.37;
	else if(_n==15) return 2.41;
	else if(_n==16) return 2.44;
	else if(_n==17) return 2.47;
	else if(_n==18) return 2.5;
	else if(_n==19) return 2.53;
	else if(_n==20) return 2.56;
	else if(_n==21) return 2.58;
	else if(_n==22) return 2.6;
	else if(_n==23) return 2.62;
	else if(_n==24) return 2.64;
	else if(_n==25) return 2.66;
	else if(_n==26) return 2.68;
	else if(_n==27) return 2.7;
	else if(_n==28) return 2.72;
	else if(_n==29) return 2.73;
	else if(_n==30) return 2.75;
}

function LimpiaColores(){
	var linea = document.getElementsByName("_A").length;
	for(i=0;i<linea;i++){
		document.getElementById('tr_usar'+i).style.background = "#FFFFFF";
	}
}

function vector(ctrl){
	var str = "1=1";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		str += "&" + control[i].value.replace(/&/g, ''); 
	}
	return str;
}

function vector2(ctrl){
	var str = "1=1";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		if(control[i].checked)
			str += "&1";
		else
			str += "&0";
	}
	
	return str;
}