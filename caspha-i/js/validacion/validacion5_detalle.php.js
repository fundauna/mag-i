//var _cantidad = 17;
var P1 = P2 = P3 = P4 = P5 = bajo1 = bajo2 = bajo3 = alto1 = alto2 = alto3 = final = final2 = 0;

function DocumentosZip(_ARCHIVO, _MODULO){
	var ruta = "../seguridad/documentos.php?ARCHIVO=" + _ARCHIVO + "&MODULO=" + _MODULO + "&ACCESO=00";
	window.open(ruta,"","width=100,height=100,scrollbars=yes,status=no");
	//window.showModalDialog(ruta, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}

function Cargar(){	
	if($('#archivo').val()=='' ){
		OMEGA('Debe adjuntar un documento');
		return;
	}	
	
	if(!confirm('Cargar archivo de datos?')) return;
	ALFA('Por favor espere....');
	document.form.submit();
}

function Procesar(){
	if(!confirm('Enviar documento a revisi�n?')) return;
	_Modificar(1);
}

function Revisar(){
	if(!confirm('Enviar documento al superior?')) return;
	_Modificar(3);
}

function Aprobar(){
	if(!confirm('Aprobar documento?')) return;
	_Modificar(2);
}

function Anular(){
	if(!confirm('Anular documento?')) return;
	_Modificar(5);
}

function _Modificar(_estado){
	var parametros = {
		'_AJAX' : 1,
		'paso' : 3,
		'cs' :$('#cs').val(),
		'estado' : _estado,
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					alert("Transaccion finalizada");
					opener.location.reload();
					window.close();				
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function deshabilita(){
	document.getElementById('teo6').style.display = 'none';
	document.getElementById('teo7').style.display = 'none';
	document.getElementById('teo8').style.display = 'none';
	document.getElementById('teo9').style.display = 'none';
	document.getElementById('teo10').style.display = 'none';
	document.getElementById('teo11').style.display = 'none';
	document.getElementById('teo12').style.display = 'none';
	document.getElementById('teo13').style.display = 'none';
	document.getElementById('teo14').style.display = 'none';
	document.getElementById('teo15').style.display = 'none';
	document.getElementById('teo16').style.display = 'none';
	document.getElementById('teo17').style.display = 'none';
	document.getElementById('teo18').style.display = 'none';
}

function __calcula(_tabla){
	P1 = ($('#tabla'+_tabla+'_exp1').val()/$('#tabla'+_tabla+'_teo1').val()-1)*100;
	P2 = ($('#tabla'+_tabla+'_exp2').val()/$('#tabla'+_tabla+'_teo2').val()-1)*100;
	P3 = ($('#tabla'+_tabla+'_exp3').val()/$('#tabla'+_tabla+'_teo3').val()-1)*100;
	P4 = ($('#tabla'+_tabla+'_exp4').val()/$('#tabla'+_tabla+'_teo4').val()-1)*100;
	P5 = ($('#tabla'+_tabla+'_exp5').val()/$('#tabla'+_tabla+'_teo5').val()-1)*100;
	
	bajo1 = ( parseFloat($('#tabla'+_tabla+'_exp6').val()) + parseFloat($('#tabla'+_tabla+'_exp7').val()) + parseFloat($('#tabla'+_tabla+'_exp8').val()) + parseFloat($('#tabla'+_tabla+'_exp9').val()) + parseFloat($('#tabla'+_tabla+'_exp10').val()) )/5;
	bajo2 = (bajo1/$('#bajo').val())*100;
	alto1 = ( parseFloat($('#tabla'+_tabla+'_exp11').val()) + parseFloat($('#tabla'+_tabla+'_exp12').val()) + parseFloat($('#tabla'+_tabla+'_exp13').val()) + parseFloat($('#tabla'+_tabla+'_exp14').val()) + parseFloat($('#tabla'+_tabla+'_exp15').val()) )/5;
	alto2 = (alto1/$('#medio').val())*100;
	
	var fVarianceA = 0;
	fVarianceA += parseFloat(Math.pow($('#tabla'+_tabla+'_exp6').val() - bajo1, 2));
	fVarianceA += parseFloat(Math.pow($('#tabla'+_tabla+'_exp7').val() - bajo1, 2));
	fVarianceA += parseFloat(Math.pow($('#tabla'+_tabla+'_exp8').val() - bajo1, 2));
	fVarianceA += parseFloat(Math.pow($('#tabla'+_tabla+'_exp9').val() - bajo1, 2));
	fVarianceA += parseFloat(Math.pow($('#tabla'+_tabla+'_exp10').val() - bajo1, 2));
	bajo3 = ( Math.sqrt(fVarianceA)/Math.sqrt(5) )/bajo1*100;
	
	var fVarianceA = 0;
	fVarianceA += parseFloat(Math.pow($('#tabla'+_tabla+'_exp11').val() - alto1, 2));
	fVarianceA += parseFloat(Math.pow($('#tabla'+_tabla+'_exp12').val() - alto1, 2));
	fVarianceA += parseFloat(Math.pow($('#tabla'+_tabla+'_exp13').val() - alto1, 2));
	fVarianceA += parseFloat(Math.pow($('#tabla'+_tabla+'_exp14').val() - alto1, 2));
	fVarianceA += parseFloat(Math.pow($('#tabla'+_tabla+'_exp15').val() - alto1, 2));
	alto3 = ( Math.sqrt(fVarianceA)/Math.sqrt(5) )/alto1*100;
	
	final = ($('#tabla'+_tabla+'_pic0').val()/$('#tabla'+_tabla+'_pic1').val())*100;
	final2 = ($('#tabla'+_tabla+'_pic17').val()/$('#tabla'+_tabla+'_pic18').val()-1)*100;
	
	document.getElementById('tabla'+_tabla+'_P1').innerHTML = Math.round(P1)+'%';
	document.getElementById('tabla'+_tabla+'_P2').innerHTML = Math.round(P2)+'%';
	document.getElementById('tabla'+_tabla+'_P3').innerHTML = Math.round(P3)+'%';
	document.getElementById('tabla'+_tabla+'_P4').innerHTML = Math.round(P4)+'%';
	document.getElementById('tabla'+_tabla+'_P5').innerHTML = Math.round(P5)+'%';
	document.getElementById('tabla'+_tabla+'_bajo1').innerHTML = _RED2(bajo1, 2);
	document.getElementById('tabla'+_tabla+'_bajo2').innerHTML = Math.round(bajo2)+'%';
	document.getElementById('tabla'+_tabla+'_bajo3').innerHTML = Math.round(bajo3)+'%';
	document.getElementById('tabla'+_tabla+'_alto1').innerHTML = _RED2(alto1, 2);
	document.getElementById('tabla'+_tabla+'_alto2').innerHTML = Math.round(alto2)+'%';
	document.getElementById('tabla'+_tabla+'_alto3').innerHTML = Math.round(alto3)+'%';
	document.getElementById('tabla'+_tabla+'_final').innerHTML = _RED2(final, 1)+'%';
	document.getElementById('tabla'+_tabla+'_final2').innerHTML = _RED2(final2, 1)+'%';
	//
	document.getElementById('res'+_tabla+'_P1').value = Math.round(P1);
	document.getElementById('res'+_tabla+'_P2').value = Math.round(P2);
	document.getElementById('res'+_tabla+'_P3').value = Math.round(P3);
	document.getElementById('res'+_tabla+'_P4').value = Math.round(P4);
	document.getElementById('res'+_tabla+'_P5').value = Math.round(P5);
	document.getElementById('res'+_tabla+'_bajo1').value = _RED2(bajo1, 2);
	document.getElementById('res'+_tabla+'_bajo2').value = Math.round(bajo2);
	document.getElementById('res'+_tabla+'_bajo3').value = Math.round(bajo3);
	document.getElementById('res'+_tabla+'_alto1').value = _RED2(alto1, 2);
	document.getElementById('res'+_tabla+'_alto2').value = Math.round(alto2);
	document.getElementById('res'+_tabla+'_alto3').value = Math.round(alto3);
	document.getElementById('res'+_tabla+'_final').value = _RED2(final, 1);
	document.getElementById('res'+_tabla+'_final2').value = _RED2(final2, 1);
	//CUMPLIMIENTOS
	var cumple = 1;
	if(P1 > 20 || P2 > 20 || P3 > 20 || P4 > 20 || P5 > 20){
		cumple = 0;
		document.getElementById('tabla'+_tabla+'_cumple_1').style.color = "#FF0000";
	}
	
	if(bajo2 < 70 || bajo2 > 120){
		cumple = 0;
		document.getElementById('tabla'+_tabla+'_cumple_2').style.color = "#FF0000";
	}
	
	if(bajo3 > 20 ){
		cumple = 0;
		document.getElementById('tabla'+_tabla+'_cumple_2').style.color = "#FF0000";
	}
	
	if(alto2 < 70 || alto2 > 120){
		cumple = 0;
		document.getElementById('tabla'+_tabla+'_cumple_3').style.color = "#FF0000";
	}
	
	if(alto3 > 20 ){
		cumple = 0;
		document.getElementById('tabla'+_tabla+'_cumple_3').style.color = "#FF0000";
	}
	
	if(final > 30 ){
		cumple = 0;
		document.getElementById('tabla'+_tabla+'_cumple_4').style.color = "#FF0000";
	}
	if(final2 > 20 ){
		cumple = 0;
		document.getElementById('tabla'+_tabla+'_cumple_5').style.color = "#FF0000";
	}
	$('#cumple'+_tabla).val(cumple);
}

function datos(){
	if( Mal(1, $('#elaboracion').val()) ){
		OMEGA('Debe indicar la fecha de elaboraci�n');
		return;
	}
	
	if( Mal(1, $('#asociada').val()) ){
		OMEGA('Debe indicar la validaci�n asociada');
		return;
	}
	
	if($('#matriz0').val()==''){
		OMEGA('Debe seleccionar la matriz');
		return;
	}
	
	if(!confirm('Ingresar ensayo?')) return;

	var parametros = {
		'_AJAX' : 1,
		'paso' : 1,
		'tipo' : 5,
		'elaboracion' : $('#elaboracion').val(),
		'asociada' : $('#asociada').val(),
		'matriz' : $('#matriz0').val(),
		'bajo' : $('#bajo').val(),
		'medio' : $('#medio').val(),
		'obs' : $('#obs').val(),
		'ana' : vector('ana'),
		'pic' : vector('pic'),
		'teo' : vector('teo'),
		'exp' : vector('exp'),
		'analito' : vector('analito'),
		'P1' : vector('P1'),
		'P2' : vector('P2'),
		'P3' : vector('P3'),
		'P4' : vector('P4'),
		'P5' : vector('P5'),
		'bajo1' : vector('bajo1'),
		'bajo2' : vector('bajo2'),
		'bajo3' : vector('bajo3'),
		'alto1' : vector('alto1'),
		'alto2' : vector('alto2'),
		'alto3' : vector('alto3'),
		'final' : vector('final'),
		'final2' : vector('final2'),
		'cumple' : vector('cumple')
	}
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					break;
				case '1':
					opener.location.reload();
					alert("Transaccion finalizada");
					window.close();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function vector(ctrl){
	var str = "1=1";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		if(control[i].value != '')
			str += "&" + control[i].value.replace(/&/g, ''); 
	}
	return str;
}

function Redondear(txt, tabla){
	_RED(txt, 9);
	__calcula(tabla);
}

function MatrizLista(linea){
	window.open(__SHELL__ + "?list=2&linea="+linea,"","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=2&linea="+linea, this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function MatrizEscoge(id, nombre, linea){
	opener.document.getElementById('matriz'+linea).value=id;
	opener.document.getElementById('nommatriz'+linea).value=nombre;
	window.close();
}