function desmarca(){
	document.getElementById('add').disabled = false;
	document.getElementById('mod').disabled = true;
	document.getElementById('del').disabled = true;
	document.getElementById('new').disabled = true;
	document.getElementById('codigo').value = '';
	document.getElementById('codigocer').value = '';
	document.getElementById('fecha').value = '';
	document.getElementById('activo').selectedIndex = 0;
	document.getElementById('cuarto').value = '';
	document.getElementById('limite1').value = '0.00';
	document.getElementById('limite2').value = '0.00';
	document.getElementById('indicacion1').value = '0.00';
	document.getElementById('error1').value = '0.00';
	document.getElementById('inc1').value = '0.00';
	document.getElementById('inc2').value = '0.00';
	document.getElementById('inc3').value = '0.00';
	document.getElementById('inc4').value = '0.00';
	document.getElementById('inc5').value = '0.00';
	document.getElementById('indicacion2').value = '0.00';
	document.getElementById('error2').value = '0.00';
	document.getElementById('indicacion3').value = '0.00';
	document.getElementById('error3').value = '0.00';
	document.getElementById('indicacion4').value = '0.00';
	document.getElementById('error4').value = '0.00';
	document.getElementById('indicacion5').value = '0.00';
	document.getElementById('error5').value = '0.00';
	document.getElementById('humedad1').value = '0.00';
	document.getElementById('humedad2').value = '0.00';
	document.getElementById('hr1').value = '0.00';
	document.getElementById('err_hr1').value = '0.00';
	document.getElementById('hr2').value = '0.00';
	document.getElementById('err_hr2').value = '0.00';
	document.getElementById('hr3').value = '0.00';
	document.getElementById('err_hr3').value = '0.00';
}

function marca(control){
	if(control.selectedIndex!= -1){
		document.getElementById('codigo').value = control.options[control.selectedIndex].getAttribute('codigo');
		document.getElementById('codigocer').value = control.options[control.selectedIndex].getAttribute('codigocer');
		document.getElementById('fecha').value = control.options[control.selectedIndex].getAttribute('fecha');
		document.getElementById('activo').selectedIndex = control.options[control.selectedIndex].getAttribute('activo');
		document.getElementById('cuarto').value = control.options[control.selectedIndex].getAttribute('cuarto');
		document.getElementById('limite1').value = control.options[control.selectedIndex].getAttribute('limite1');
		document.getElementById('limite2').value = control.options[control.selectedIndex].getAttribute('limite2');
		document.getElementById('indicacion1').value = control.options[control.selectedIndex].getAttribute('indicacion1');
		document.getElementById('error1').value = control.options[control.selectedIndex].getAttribute('error1');
		document.getElementById('indicacion2').value = control.options[control.selectedIndex].getAttribute('indicacion2');
		document.getElementById('error2').value = control.options[control.selectedIndex].getAttribute('error2');
		document.getElementById('indicacion3').value = control.options[control.selectedIndex].getAttribute('indicacion3');
		document.getElementById('error3').value = control.options[control.selectedIndex].getAttribute('error3');
		document.getElementById('indicacion4').value = control.options[control.selectedIndex].getAttribute('indicacion4');
		document.getElementById('error4').value = control.options[control.selectedIndex].getAttribute('error4');
		document.getElementById('indicacion5').value = control.options[control.selectedIndex].getAttribute('indicacion5');
		document.getElementById('error5').value = control.options[control.selectedIndex].getAttribute('error5');
		document.getElementById('humedad1').value = control.options[control.selectedIndex].getAttribute('humedad1');
		document.getElementById('humedad2').value = control.options[control.selectedIndex].getAttribute('humedad2');
		document.getElementById('hr1').value = control.options[control.selectedIndex].getAttribute('hr1');
		document.getElementById('err_hr1').value = control.options[control.selectedIndex].getAttribute('err_hr1');
		document.getElementById('hr2').value = control.options[control.selectedIndex].getAttribute('hr2');
		document.getElementById('err_hr2').value = control.options[control.selectedIndex].getAttribute('err_hr2');
		document.getElementById('hr3').value = control.options[control.selectedIndex].getAttribute('hr3');
		document.getElementById('err_hr3').value = control.options[control.selectedIndex].getAttribute('err_hr3');
		document.getElementById('inc1').value = control.options[control.selectedIndex].getAttribute('inc1');
		document.getElementById('inc2').value = control.options[control.selectedIndex].getAttribute('inc2');
		document.getElementById('inc3').value = control.options[control.selectedIndex].getAttribute('inc3');
		document.getElementById('inc4').value = control.options[control.selectedIndex].getAttribute('inc4');
		document.getElementById('inc5').value = control.options[control.selectedIndex].getAttribute('inc5');
		//
		document.getElementById('mod').disabled = false;
		document.getElementById('del').disabled = false;
		document.getElementById('add').disabled = true;
		document.getElementById('new').disabled = false;
	}
}

function modificar(accion){
	if( Mal(2, document.getElementById('codigo').value) ){
		OMEGA('Debe digitar el c�digo');
		return;
	}
	
	if( Mal(1, document.getElementById('fecha').value) ){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	if(confirm('Modificar Datos?')){
		var parametros = {
			'_AJAX' : 1,
			'codigo' : $('#codigo').val(),
			'codigocer' : $('#codigocer').val(),
			'fecha' : $('#fecha').val(),
			'activo' : $('#activo').val(),
			'cuarto' : $('#cuarto').val(),
			'limite1' : $('#limite1').val(),
			'limite2' : $('#limite2').val(),
			'indicacion1' : $('#indicacion1').val(),
			'indicacion2' : $('#indicacion2').val(),
			'indicacion3' : $('#indicacion3').val(),
			'indicacion4' : $('#indicacion3').val(),
			'indicacion5' : $('#indicacion3').val(),
			'error1' : $('#error1').val(),
			'error2' : $('#error2').val(),
			'error3' : $('#error3').val(),
			'error4' : $('#error3').val(),
			'error5' : $('#error3').val(),
			'inc1' : $('#inc1').val(),
			'inc2' : $('#inc2').val(),
			'inc3' : $('#inc3').val(),
			'inc4' : $('#inc3').val(),
			'inc5' : $('#inc3').val(),
			'humedad1' : $('#humedad1').val(),
			'humedad2' : $('#humedad2').val(),
			'hr1' : $('#hr1').val(),
			'hr2' : $('#hr2').val(),
			'hr3' : $('#hr3').val(),
			'err_hr1' : $('#err_hr1').val(),
			'err_hr2' : $('#err_hr2').val(),
			'err_hr3' : $('#err_hr3').val(),
			'inc6' : $('#inc3').val(),
			'inc7' : $('#inc3').val(),
			'inc8' : $('#inc3').val(),
			'tipo' : $('#tipo').val(),
			'accion' : accion
		};
		
		$.ajax({
			data:  parametros,
			url:   __SHELL__,
			type:  'post',
			beforeSend: function () {
				document.getElementById('add').disabled = true;
				document.getElementById('mod').disabled = true;
				document.getElementById('del').disabled = true;
				document.getElementById('new').disabled = true;	
				ALFA('Por favor espere....');
			},
			success: function (_response) {
				switch(_response){
					case '-0':alert('Sesi�n expirada [Err:0]');break;
					case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
					case '0':
						OMEGA('Error transaccional');
						document.getElementById('add').disabled = false;
						break;
					case '1':
						GAMA('Transaccion finalizada');
						document.getElementById('add').disabled = false;
						break;
					default:alert('Tiempo de espera agotado');break;
				}
			}
		});
	}
}