function UsuariosLista(){
	window.open(__SHELL__ + "?list=1","","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function UsuarioEscoge(id, nombre){
	opener.document.getElementById('usuario').value=id;
	opener.document.getElementById('tmp').value=nombre;
	window.close();
}

function datos(){
	if( Mal(1, $('#fecha').val()) ){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	if( Mal(2, $('#descr').val()) ){
		OMEGA('Debe indicar el detalle');
		return;
	}
	
	if( Mal(1, $('#usuario').val()) ){
		OMEGA('Debe indicar el usuario');
		return;
	}
	
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'id' : $('#id').val(),
		'equipo' : $('#equipo').val(),
		'fecha' : $('#fecha').val(),
		'descr' : $('#descr').val(),
		'usuario' : $('#usuario').val(),
		'accion' : 'I'
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '1':
					location.href = 'historial.php?ID=' + $('#equipo').val();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function HistorialElimina(_id){
	if(confirm('Eliminar registro?')){
		var parametros = {
			'_AJAX' : 1,
			'id' : _id,
			'equipo' : '',
			'fecha' : '',
			'descr' : '',
			'usuario' : '',
			'accion' : 'D'
		};
		
		$.ajax({
			data:  parametros,
			url:   __SHELL__,
			type:  'post',
			beforeSend: function () {
				btn.style.display='none';
				ALFA('Por favor espere....');
			},
			success: function (_response) {
				switch(_response){
					case '-0':alert('Sesi�n expirada [Err:0]');break;
					case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
					case '1':
						location.href = 'historial.php?ID=' + $('#equipo').val();
						break;
					default:alert('Tiempo de espera agotado');break;
				}
			}
		});
	}
}