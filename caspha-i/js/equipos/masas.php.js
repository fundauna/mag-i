function desmarca() {
    document.getElementById('add').disabled = false;
    document.getElementById('mod').disabled = true;
    document.getElementById('del').disabled = true;
    document.getElementById('new').disabled = true;
    document.getElementById('ocodigo').value = '';
    document.getElementById('onominal').value = '';
    document.getElementById('codigo').value = '';
    document.getElementById('codigocer').value = '';
    document.getElementById('fecha').value = '';
    document.getElementById('nominal').value = '';
    document.getElementById('real').value = '';
    document.getElementById('inc').value = '';
    document.getElementById('certificado').value = '';
    document.getElementById('activo').selectedIndex = 0;
}

function marca(control) {
    if (control.selectedIndex != -1) {
        document.getElementById('ocodigo').value = control.options[control.selectedIndex].getAttribute('codigo');
        document.getElementById('onominal').value = control.options[control.selectedIndex].getAttribute('nominal');
        document.getElementById('codigo').value = control.options[control.selectedIndex].getAttribute('codigo');
        document.getElementById('codigocer').value = control.options[control.selectedIndex].getAttribute('codigocer');
        document.getElementById('fecha').value = control.options[control.selectedIndex].getAttribute('fecha');
        document.getElementById('nominal').value = control.options[control.selectedIndex].getAttribute('nominal');
        document.getElementById('real').value = control.options[control.selectedIndex].getAttribute('real');
        document.getElementById('inc').value = control.options[control.selectedIndex].getAttribute('inc');
        document.getElementById('certificado').value = control.options[control.selectedIndex].getAttribute('certificado');
        document.getElementById('activo').selectedIndex = control.options[control.selectedIndex].getAttribute('activo');
        //
        document.getElementById('mod').disabled = false;
        document.getElementById('del').disabled = false;
        document.getElementById('add').disabled = true;
        document.getElementById('new').disabled = false;
    }
}

function redondea(_OBJ) {
    _OBJ.value = _OBJ.value.replace(',', '.');
}

function modificar(accion) {
    if (Mal(2, document.getElementById('codigo').value)) {
        OMEGA('Debe digitar el c�digo');
        return;
    }

    if (Mal(1, document.getElementById('fecha').value)) {
        OMEGA('Debe indicar la fecha');
        return;
    }

    if (Mal(1, document.getElementById('nominal').value)) {
        OMEGA('Debe indicar el valor nominal');
        return;
    }

    if (Mal(1, document.getElementById('real').value)) {
        OMEGA('Debe indicar el valor real');
        return;
    }

    if (Mal(1, document.getElementById('inc').value)) {
        OMEGA('Debe indicar la incertidumbre');
        return;
    }

    if (Mal(2, document.getElementById('certificado').value)) {
        OMEGA('Debe digitar el certificado');
        return;
    }


    if (confirm('Modificar Datos?')) {
        $('#accion').val(accion);
        document.getElementById("frmmasas").submit();
    }
}