function desmarca(){
	document.getElementById('add').disabled = false;
	document.getElementById('mod').disabled = true;
	document.getElementById('del').disabled = true;
	document.getElementById('new').disabled = true;
	document.getElementById('codigo').value = '';
	document.getElementById('fecha').value = '';
	document.getElementById('limite1').value = '';
	document.getElementById('limite2').value = '';
	document.getElementById('ubicacion').value = '';
	document.getElementById('activo').selectedIndex = 0;
}

function marca(control){
	if(control.selectedIndex!= -1){
		document.getElementById('codigo').value = control.options[control.selectedIndex].getAttribute('codigo');
		document.getElementById('fecha').value = control.options[control.selectedIndex].getAttribute('fecha');
		document.getElementById('limite1').value = control.options[control.selectedIndex].getAttribute('limite1');
		document.getElementById('limite2').value = control.options[control.selectedIndex].getAttribute('limite2');
		document.getElementById('ubicacion').value = control.options[control.selectedIndex].getAttribute('ubicacion');
		document.getElementById('activo').selectedIndex = control.options[control.selectedIndex].getAttribute('activo');
		
		if(control.options[control.selectedIndex].getAttribute('tipo')=='0')
			document.getElementById('tipo').selectedIndex = 0;
		else if(control.options[control.selectedIndex].getAttribute('tipo')=='1')
			document.getElementById('tipo').selectedIndex = 1;
		else
			document.getElementById('tipo').selectedIndex = 2;
		//
		document.getElementById('mod').disabled = false;
		document.getElementById('del').disabled = false;
		document.getElementById('add').disabled = true;
		document.getElementById('new').disabled = false;
	}
}

function modificar(accion){
	if( Mal(2, document.getElementById('codigo').value) ){
		OMEGA('Debe digitar el c�digo');
		return;
	}
	
	if( Mal(1, document.getElementById('fecha').value) ){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	if( Mal(2, document.getElementById('ubicacion').value) ){
		OMEGA('Debe indicar la ubicaci�n');
		return;
	}
	
	if(confirm('Modificar Datos?')){
		var parametros = {
			'_AJAX' : 1,
			'codigo' : $('#codigo').val(),
			'fecha' : $('#fecha').val(),
			'tipo' : $('#tipo').val(),
			'limite1' : $('#limite1').val(),
			'limite2' : $('#limite2').val(),
			'ubicacion' : $('#ubicacion').val(),
			'activo' : $('#activo').val(),
			'accion' : accion
		};
		
		$.ajax({
			data:  parametros,
			url:   __SHELL__,
			type:  'post',
			beforeSend: function () {
				document.getElementById('add').disabled = true;
				document.getElementById('mod').disabled = true;
				document.getElementById('del').disabled = true;
				document.getElementById('new').disabled = true;	
				ALFA('Por favor espere....');
			},
			success: function (_response) {
				switch(_response){
					case '-0':alert('Sesi�n expirada [Err:0]');break;
					case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
					case '0':
						OMEGA('Error transaccional');
						document.getElementById('add').disabled = false;
						break;
					case '1':
						GAMA('Transaccion finalizada');
						document.getElementById('add').disabled = false;
						break;
					default:alert('Tiempo de espera agotado');break;
				}
			}
		});
	}
}