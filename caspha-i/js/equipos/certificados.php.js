function datos(){
	var control = document.getElementsByName('detalle[]');
	
	for(i=0;i<control.length;i++){
		if( Mal(4, control[i].value) ){
			OMEGA('Debe indicar el detalle<br>en la l�nea: ' + (i+1));
			return;
		}
	}
	
	if(!confirm('Modificar datos?')) return;
	$('#btn').disabled = true;
	ALFA('Por favor espere....');
	document.form.submit();
}

function CertificadoMas(){
	var linea = document.getElementsByName("detalle[]").length;
	var fila = document.createElement("tr");
	var colum = new Array(2);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	
	colum[0].innerHTML = (linea + 1) + ':';
	colum[1].innerHTML = '<input type="text" name="detalle[]" size="30" maxlength="50">&nbsp;<input type="file" name="archivo[]" />';
	
	for(i=0;i<colum.length;i++){
		fila.appendChild(colum[i]); 	
	}
	
	document.getElementById('t3').appendChild(fila);
}

function DocumentosZip(_ARCHIVO, _MODULO){
	var ruta = "../seguridad/documentos.php?ARCHIVO=" + _ARCHIVO + "&MODULO=" + _MODULO + "&ACCESO=06";
	window.open(ruta,"","width=100,height=100,scrollbars=yes,status=no");
	//window.showModalDialog(ruta, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}

function DocElimina(doc, equipo){
	if(confirm('Eliminar documento?')){
		var parametros = {
			'_AJAX' : 1,
			'id' : doc,
			'equipo' : equipo
		};
		
		$.ajax({
			data:  parametros,
			url:   __SHELL__,
			type:  'post',
			beforeSend: function () {
				//btn.style.display='none';
				ALFA('Por favor espere....');
			},
			success: function (_response) {
				switch(_response){
					case '-0':alert('Sesi�n expirada [Err:0]');break;
					case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
					case '1':
						location.href = 'certificados.php?ID=' + equipo;
						break;
					default:alert('Tiempo de espera agotado');break;
				}
			}
		});
	}
}