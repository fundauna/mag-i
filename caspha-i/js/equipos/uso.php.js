$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		/*"sScrollX": "100%",*/
		"sScrollXInner": "110%",
		"bScrollCollapse": true,
		"bFilter": true,
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function UsoBuscar(btn){
	if( Mal(1, $('#desde').val()) ){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	if( Mal(1, $('#hasta').val()) ){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	btn.disabled = true;
	btn.form.submit();
}

function UsoAgregar(){
	window.open("uso_detalle.php?acc=I","","width=530,height=450,scrollbars=yes,status=no");
	//window.showModalDialog("uso_detalle.php?acc=I", this.window, "dialogWidth:530px;dialogHeight:450px;status:no;");
}

function UsoModificar(_CODIGO, _USUARIO, _FECHA, _ACCION){
	window.open("uso_detalle.php?acc=V&CODIGO="+_CODIGO+"&USUARIO="+_USUARIO+"&FECHA="+_FECHA+"&ACCION="+_ACCION,"","width=530,height=450,scrollbars=yes,status=no");
	//window.showModalDialog("uso_detalle.php?acc=V&CODIGO="+_CODIGO+"&USUARIO="+_USUARIO+"&FECHA="+_FECHA+"&ACCION="+_ACCION, this.window, "dialogWidth:530px;dialogHeight:450px;status:no;");
}

function EquiposLista(){
	window.open(__SHELL__ + "?list=1","","width=500,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:500px;dialogHeight:200px;status:no;");
}

function EquipoEscoge(cs, codigo, linea){
	opener.document.getElementById('equipo'+linea).value=cs;
	opener.document.getElementById('tmp_equipo'+linea).value=codigo;
	window.close();
}