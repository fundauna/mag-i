function ayer(hoy, dia) {
    var ano1 = hoy.value.substr(6, 4);
    var mes1 = hoy.value.substr(3, 2) * 1 - 1;
    var dia1 = hoy.value.substr(0, 2);
    //
    var ano2 = dia.value.substr(6, 4);
    var mes2 = dia.value.substr(3, 2) * 1 - 1;
    var dia2 = dia.value.substr(0, 2);
    //
    var Fecha_Hoy = new Date(ano1, mes1, dia1);
    var Fecha_Fin = new Date(ano2, mes2, dia2);

    if (Fecha_Hoy > Fecha_Fin)
        return false;
    return true;
}

function ValidaDia() {
    if (!ayer(document.getElementById('hoy'), document.getElementById('dia'))) {
        DELTA('No se pueden agregar eventos en fechas pasadas');
        return;
    }
}

function desmarca() {
    document.getElementById('add').disabled = false;
    document.getElementById('mod').disabled = true;
    document.getElementById('del').disabled = true;
    document.getElementById('new').disabled = true;
    document.getElementById('cs').value = "";
    document.getElementById('final').value = "";
    document.getElementById('equipo').value = "";
    document.getElementById('tmp_equipo').value = "";
    document.getElementById('responsable').value = "";
    document.getElementById('tmp_resp').value = "";
    document.getElementById('aviso').value = "";
    document.getElementById('tipo').value = "";
    document.getElementById('deshabilita').value = "";
    document.getElementById('observaciones').value = "";
    document.getElementById('tr_fecha').style.display = 'none';
    $('#nueva').val($('#dia').val());
}

function marca(control) {
    if (control.selectedIndex != -1) {
        document.getElementById('cs').value = control.options[control.selectedIndex].getAttribute('cs');
        document.getElementById('final').value = control.options[control.selectedIndex].getAttribute('final');
        document.getElementById('equipo').value = control.options[control.selectedIndex].getAttribute('equipo');
        document.getElementById('responsable').value = control.options[control.selectedIndex].getAttribute('responsable');
        document.getElementById('tmp_resp').value = control.options[control.selectedIndex].getAttribute('tmp_resp');
        document.getElementById('aviso').value = control.options[control.selectedIndex].getAttribute('aviso');
        document.getElementById('tmp_equipo').value = control.options[control.selectedIndex].getAttribute('codigo');
        document.getElementById('tipo').value = control.options[control.selectedIndex].getAttribute('tipo');
        document.getElementById('deshabilita').value = control.options[control.selectedIndex].getAttribute('deshabilita');
        document.getElementById('observaciones').value = control.options[control.selectedIndex].getAttribute('observaciones');
        document.getElementById('tr_fecha').style.display = '';

        document.getElementById('mod').disabled = false;
        document.getElementById('del').disabled = false;
        document.getElementById('add').disabled = true;
        document.getElementById('new').disabled = false;
    } else {
        document.getElementById('tr_fecha').style.display = 'none';
        $('#nueva').val($('#dia').val());
    }
}

function modificar(accion) {
    if ($('#nueva').val() != $('#dia').val()) {
        if (!ayer(document.getElementById('hoy'), document.getElementById('nueva'))) {
            OMEGA('La nueva fecha es incorrecta');
            return;
        }
    }

    if (document.getElementById('equipo').value == "") {
        OMEGA('Debe seleccionar un equipo');
        return;
    }

    if (document.getElementById('responsable').value == "") {
        OMEGA('Debe seleccionar un responsable');
        return;
    }

    if (document.getElementById('tipo').value == "") {
        OMEGA('Debe seleccionar el tipo de mantenimiento');
        return;
    }

    if (document.getElementById('deshabilita').value == "") {
        OMEGA('Debe seleccionar si se deshabilitar&aacute; el equipo');
        return;
    }

    if (confirm('Modificar Datos?')) {
        var parametros = {
            '_AJAX': 1,
            'cs': $('#cs').val(),
            'fecha': $('#fecha').val(),
            'final': $('#final').val(),
            'nueva': $('#nueva').val(),
            'aviso': $('#aviso').val(),
            'equipo': $('#equipo').val(),
            'responsable': $('#responsable').val(),
            'tipo': $('#tipo').val(),
            'deshabilita': $('#deshabilita').val(),
            'observaciones': $('#observaciones').val(),
            'accion': accion
        };

        $.ajax({
            data: parametros,
            url: __SHELL__,
            type: 'post',
            beforeSend: function () {
                document.getElementById('add').disabled = true;
                document.getElementById('mod').disabled = true;
                document.getElementById('del').disabled = true;
                document.getElementById('new').disabled = true;
                ALFA('Modificando datos....');
            },
            success: function (_response) {
                switch (_response) {
                    case '-0':
                        alert('Sesión expirada [Err:0]');
                        break;
                    case '-1':
                        alert('Error en el envío de parámetros [Err:-1]');
                        break;
                    case '0':
                        OMEGA('Error transaccional');
                        document.getElementById('add').disabled = false;
                        break;
                    case '1':
                        GAMA('Datos modificados');
                        //window.dialogArguments.location.reload();
                        //opener.location.reload();
                        document.getElementById('add').disabled = false;
                        break;
                    default:
                        alert('Tiempo de espera agotado');
                        break;
                }
            }
        });
    }
}

function EquiposLista() {
    window.open(__SHELL__ + "?list=1", "", "width=500,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:500px;dialogHeight:200px;status:no;");
}

function EquipoEscoge(cs, codigo, linea, tk = 0) {
    if (cs != '') {
        if (document.getElementById('ck' + tk).checked) {
            opener.document.getElementById('equipo' + linea).value = opener.document.getElementById('equipo' + linea).value + ',' + cs;
            opener.document.getElementById('tmp_equipo' + linea).value = opener.document.getElementById('tmp_equipo' + linea).value + ',' + codigo;
        } else {
            opener.document.getElementById('equipo' + linea).value = opener.document.getElementById('equipo' + linea).value.replace(cs, '');
            opener.document.getElementById('tmp_equipo' + linea).value = opener.document.getElementById('tmp_equipo' + linea).value.replace(codigo, '');
        }
    } else {
        opener.document.getElementById('equipo' + linea).value = '';
        opener.document.getElementById('tmp_equipo' + linea).value = '';
        location.reload();
    }
}

function UsuariosLista() {
    window.open(__SHELL__ + "?list=2", "", "width=500,height=200,scrollbars=yes,status=no");
    //window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:500px;dialogHeight:200px;status:no;");
}

function UsuarioEscoge(id, nombre, linea) {
    opener.document.getElementById('responsable').value = id;
    opener.document.getElementById('tmp_resp').value = nombre;
    window.close();
}