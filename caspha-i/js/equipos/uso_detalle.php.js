function cerrar(){
	window.close();
}

function datos(){
	
	if( Mal(1, $('#equipo').val()) ){
		OMEGA('Debe seleccionar el equipo');
		return;
	}	

	if( Mal(1, $('#horas').val()) ){
		OMEGA('Debe indicar la cantidad de horas');
		return;
	}
	
	if( Mal(1, $('#referencia').val()) ){
		OMEGA('Debe indicar una referencia/descripcion');
		return;
	}
	
	if( Mal(1, $('#accion').val()) ){
		OMEGA('Debe seleccionar una accion');
		return;
	}
	
	if(!confirm('Modificar datos?')) return;

	var parametros = {
		'_AJAX' : 1,
		'equipo' : $('#equipo').val(),
		'horas' : $('#horas').val(),
		'referencia' : $('#referencia').val(),
		'accion' : $('#accion').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').prop("disabled",true);
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					document.getElementById('add').disabled = false;
					break;
				case '1':
					DELTA('Transaccion finalizada');
					document.getElementById('btn').disabled = false;
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function EquiposLista(){
	window.open(__SHELL__ + "?list=1","","width=500,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:500px;dialogHeight:200px;status:no;");
}

function EquipoEscoge(cs, codigo, linea){
	opener.document.getElementById('equipo'+linea).value=cs;
	opener.document.getElementById('tmp_equipo'+linea).value=codigo;
	window.close();
}