$(document).ready(function() {
	oTable = $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		/*"sScrollX": "100%",*/
		"sScrollXInner": "110%",
		"bScrollCollapse": true,
		"bFilter": true,
		"oLanguage": {
			"sEmptyTable": "No hay datos que mostrar",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sLoadingRecords": "Cargando...",
			"sProcessing": "Procesando...",
			"sSearch": "Buscar:",
			"sZeroRecords": "No hay datos que mostrar",
			"sInfoFiltered": "(filtro de _MAX_ registros)",
			"sInfoEmpty": "Mostrando 0 registros",
			"oPaginate": {
				"sFirst": "Primer", 
				"sLast": "Fin", 
				"sNext": "Sig.", 
				"sPrevious": "Prev."
			}
		}
	});
} );

function MantesBuscar(btn){
	if( Mal(1, $('#desde').val()) ){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	if( Mal(1, $('#hasta').val()) ){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	btn.disabled = true;
	btn.form.submit();
}

function MantesAgregar(){
	window.open("mantenimientos_detalle.php?acc=I","","width=530,height=450,scrollbars=yes,status=no");
	//window.showModalDialog("mantenimientos_detalle.php?acc=I", this.window, "dialogWidth:530px;dialogHeight:450px;status:no;");
}

function MantesModificar(_ID){
	window.open("mantenimientos_detalle.php?acc=M&ID="+_ID,"","width=530,height=450,scrollbars=yes,status=no");
	//window.showModalDialog("mantenimientos_detalle.php?acc=M&ID="+_ID, this.window, "dialogWidth:530px;dialogHeight:450px;status:no;");
}

function EquiposLista(){
	window.open(__SHELL__ + "?list=1","","width=500,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:500px;dialogHeight:200px;status:no;");
}

function EquipoEscoge(cs, codigo, linea){
	opener.document.getElementById('equipo'+linea).value=cs;
	opener.document.getElementById('tmp_equipo'+linea).value=codigo;
	window.close();
}

function MantesElimina(_id){
	if(!confirm('Eliminar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'id' : _id
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesión expirada [Err:0]');break;
				case '-1':alert('Error en el envío de parámetros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					alert("Transaccion finalizada");
					location.reload();				
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}