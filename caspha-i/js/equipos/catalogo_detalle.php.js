function HistorialMuestra(){
	var parametros = {
		'_historial' : 1,
		'id' : $('#id').val()
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '':
					OMEGA('Error transaccional');
					break;
				default:
					BETA();
					document.getElementById('tbody').innerHTML = _response;
					break;
			}
		}
	});
}

function EquiposCertificados(_ID){
	window.open("certificados.php?ID="+_ID,"","width=600,height=450,scrollbars=yes,status=no");
	//window.showModalDialog("certificados.php?ID="+_ID, this.window, "dialogWidth:600px;dialogHeight:450px;status:no;");
}

function _RED(_CANTIDAD, _DECIMALES){
//REDONDEO
	var _CANTIDAD = parseFloat(_CANTIDAD);
	var _DECIMALES = parseFloat(_DECIMALES);
	_DECIMALES = (!_DECIMALES ? 2 : _DECIMALES);
	_DECIMALES = Math.round(_CANTIDAD * Math.pow(10, _DECIMALES)) / Math.pow(10, _DECIMALES);
	if(isNaN(_DECIMALES) || _DECIMALES == Infinity || _DECIMALES == -Infinity) return "0.00";
	else return _DECIMALES;
}

function vector(ctrl){
	var str = "";
	control = document.getElementsByName(ctrl);
	for(i=0;i<control.length;i++){
		str += "&" + control[i].value.replace(/&/g, ''); 
	}
	return str;
}

function ErrorAgregar(){
	var linea = document.getElementsByName("valor").length;
	var fila = document.createElement("tr");
	var colum = new Array(2);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	
	colum[0].innerHTML = '<input type="text" name="valor" value="0" onblur="this.value=_RED(this.value,4);" class="monto"/>&nbsp;&plusmn;';
	colum[1].innerHTML = '<input type="text" name="error" value="0" onblur="this.value=_RED(this.value,4);" class="monto"/>';
	
	for(i=0;i<colum.length;i++)
		fila.appendChild(colum[i]);
	
	document.getElementById('lolo2').appendChild(fila);
}

function RangosAgregar(){
	var linea = document.getElementsByName("rango1").length;
	var fila = document.createElement("tr");
	var colum = new Array(5);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	colum[4] = document.createElement("td");
	
	colum[0].innerHTML = 'Rango '+(linea + 1);
	colum[1].innerHTML = '<input type="text" name="rango1" value="0" onblur="_FLOAT(this);" class="monto"/>';
	colum[2].innerHTML = '<input type="text" name="rango2" value="0" onblur="_FLOAT(this);" class="monto"/>';
	colum[3].innerHTML = '<input type="text" name="resolucion" value="0" class="monto"/>';
	colum[4].innerHTML = '<input type="text" name="error" onblur="this.value=_RED(this.value,6);" class="monto"/>';
	
	for(i=0;i<colum.length;i++)
		fila.appendChild(colum[i]);
	
	document.getElementById('lolo3').appendChild(fila);
}

function ComponenteAgregar(){
	var linea = document.getElementsByName("c_codigo").length;
	var fila = document.createElement("tr");
	var colum = new Array(6);
	
	colum[0] = document.createElement("td");
	colum[1] = document.createElement("td");
	colum[2] = document.createElement("td");
	colum[3] = document.createElement("td");
	colum[4] = document.createElement("td");
	colum[5] = document.createElement("td");
	
	colum[0].innerHTML = '<input type="text" name="c_codigo" size="22" maxlength="15"/>';
	colum[1].innerHTML = '<input type="text" name="c_nombre" />';
	colum[2].innerHTML = '<input type="text" name="c_patrimonio" />';
	colum[3].innerHTML = '<input type="text" name="c_marca" size="22" maxlength="20"/>';
	colum[4].innerHTML = '<input type="text" name="c_modelo" size="22" maxlength="20"/>';
	colum[5].innerHTML = '<input type="text" name="c_serie" size="22" maxlength="20"/>';
	
	for(i=0;i<colum.length;i++)
		fila.appendChild(colum[i]);
	
	document.getElementById('lolo').appendChild(fila);
}

function ComponentesEliminar(_ID){
	if(!confirm('Eliminar componente?')) return;
	
	var parametros = {
		'_AJAX' : 3,
		'id' : _ID,
		//'accion' : 'D',
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					GAMA('Transacci�n finalizada');
                                        location.reload();
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function HEquipoAgregar(_id, _linea, _sigla){
	window.open("catalogo_detalle_historial.php?id="+_id+"&l="+_linea+"&acc="+_sigla,"","width=400,height=250,scrollbars=yes,status=no");
}

function HEquipoElimina(_id, _linea){
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 2,
		'id' : _id,
		'linea' : _linea
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					alert("Transaccion finalizada");
					location.reload();				
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}

function ProveedoresLista(){
	window.open(__SHELL__ + "?list=1","","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function ProveedorEscoge(cs, nombre){
	opener.document.getElementById('proveedor').value=cs;
	opener.document.getElementById('tmp_proveedor').value=nombre;
	window.close();
}

function Chequeado(_id){
	if($(_id).prop("checked")==false) return 0;
	else return 1;
}

function datos(){
	if( Mal(4, $('#codigo').val()) ){
		OMEGA('Debe indicar el c�digo');
		return;
	}
	
	if( Mal(2, $('#nombre').val()) ){
		OMEGA('Debe indicar el nombre');
		return;
	}
	
	if( Mal(1, $('#marca').val()) ){
		OMEGA('Debe indicar la marca');
		return;
	}
	
	if( Mal(1, $('#modelo').val()) ){
		OMEGA('Debe indicar el modelo');
		return;
	}
	//
	var control = document.getElementsByName('c_codigo');
	var control2 = document.getElementsByName('c_nombre');
	
	for(i=0;i<control.length;i++){
		if( !Mal(1, control[i].value) ){
			if( Mal(1, control2[i].value) ){
				OMEGA('Debe indicar nombre del componente. L�nea:' + (i+1));
				return;
			}
		}
	}
	//
	if( Mal(1, $('#ubi_equipo').val()) ){
		OMEGA('Debe indicar la ubicaci�n en el laboratorio');
		return;
	}
	//
	if( Mal(1, $('#proveedor').val()) ){
		OMEGA('Debe indicar el proveedor');
		return;
	}
	//
	if( Mal(1, $('#control_metro').val()) ){
		OMEGA('Debe indicar si requiere control');
		return;
	}
	
	if( $('#control_metro').val() == '1'){
		if( $("#tipo_cali").prop("checked")==false && $("#tipo_veri").prop("checked")==false ){
			OMEGA('Debe indicar el tipo de control');
			return;
		}
	}
	//
	if( Mal(1, $('#unidad').val()) ){
		OMEGA('Debe la unidad de medida');
		return;
	}
	//
	if( Mal(1, $('#estado').val()) ){
		OMEGA('Debe indicar el estado');
		return;
	}
	
	if( $('#estado').val()=='0' && Mal(1, $('#justi').val()) ){
		OMEGA('Debe justificar por qu� el equipo est� fuera de uso');
		return;
	}
	
	if(!confirm('Modificar datos?')) return;
	
	var parametros = {
		'_AJAX' : 1,
		'id' : $('#id').val(),
		'accion' : $('#accion').val(),
		'codigo' : $('#codigo').val(),
		'nombre' : $('#nombre').val(),
		'patrimonio' : $('#patrimonio').val(),
		'marca' : $('#marca').val(),
		'modelo' : $('#modelo').val(),
		'serie' : $('#serie').val(),
		'ubi_fabricante' : $('#ubi_fabricante').val(),
		'fecha' : $('#fecha').val(),
		'temp1' : $('#temp1').val(),
		'temp2' : $('#temp2').val(),
		'hume1' : $('#hume1').val(),
		'hume2' : $('#hume2').val(),
		'ubi_equipo' : $('#ubi_equipo').val(),
		'garantia' : $('#garantia').val(),
		'proveedor' : $('#proveedor').val(),
		'control_metro' : $('#control_metro').val(),
		'tipo_cali' : Chequeado('#tipo_cali'),
		'tipo_veri' : Chequeado('#tipo_veri'),
		'tipo_mant' : $('#tipo_mant').val(),
		'otro' : $('#otro').val(),
		'unidad' : $('#unidad').val(),
		//
		'rango1' : vector("rango1"),
		'rango2' : vector("rango2"),
		'resolucion' : vector("resolucion"),
		'error' : vector("error"),
		//
		'estado' : $('#estado').val(),
		'justi' : $('#justi').val(),
		//
		'c_codigo' : vector("c_codigo"),
		'c_nombre' : vector("c_nombre"),
		'c_patrimonio' : vector("c_patrimonio"),
		'c_marca' : vector("c_marca"),
		'c_modelo' : vector("c_modelo"),
		'c_serie' : vector("c_serie")	
	};
	
	$.ajax({
		data:  parametros,
		url:   __SHELL__,
		type:  'post',
		beforeSend: function () {
			$('#btn').disabled = true;
			ALFA('Por favor espere....');
		},
		success: function (_response) {
			switch(_response){
				case '-0':alert('Sesi�n expirada [Err:0]');break;
				case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
				case '0':
					OMEGA('Error transaccional');
					$('#btn').disabled = false;
					break;
				case '1':
					alert("Transaccion finalizada");
					opener.location.reload();
					window.close();				
					break;
				default:alert('Tiempo de espera agotado');break;
			}
		}
	});
}