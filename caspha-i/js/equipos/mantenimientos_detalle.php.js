function CambiaTipo(_valor){
	if(_valor == '0' || _valor == '1')
		document.getElementById('tr_prov').style.display = '';
	else
		document.getElementById('tr_prov').style.display = 'none';
}

function datos(){
	if( Mal(1, $('#reporte').val()) ){
		OMEGA('Debe indicar el c�digo de reporte');
		return;
	}
	
	if( Mal(1, $('#equipo').val()) ){
		OMEGA('Debe seleccionar el equipo');
		return;
	}
	
	if( Mal(4, $('#fecha').val()) ){
		OMEGA('Debe indicar la fecha');
		return;
	}
	
	if( Mal(1, $('#tipo').val()) ){
		OMEGA('Debe indicar el tipo');
		return;
	}
	
	if( Mal(1, $('#usuario').val()) ){
		OMEGA('Debe seleccionar el encargado');
		return;
	}
	
	/*if( Mal(3, $('#maestro').val()) ){
		OMEGA('Debe indicar el c�digo de registro');
		return;
	}*/
	
	if( $('#tipo').val() == '0' || $('#tipo').val() == '1' ){
		if( Mal(1, $('#prov').val()) ){
			OMEGA('Debe seleccionar el proveedor');
			return;
		}
	}
	
	if(!confirm('Modificar datos?')) return;
	$('#btn').disabled = true;
	ALFA('Por favor espere....');
	document.form.submit();
}

/*function cambia(valor){
	if(valor == '0' || valor == '1'){
		document.getElementById('prox').style.visibility = 'visible';
	}else{
		document.getElementById('prox').style.visibility = 'hidden';
		document.getElementById('proximo').value = '';
	}
}*/

function UsuariosLista(){
	window.open(__SHELL__ + "?list2=1","","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list2=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function UsuarioEscoge(id, nombre){
	opener.document.getElementById('usuario').value=id;
	opener.document.getElementById('tmp').value=nombre;
	window.close();
}

function EquiposLista(){
	window.open(__SHELL__ + "?list=1","","width=500,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list=1", this.window, "dialogWidth:500px;dialogHeight:200px;status:no;");
}

function EquipoEscoge(cs, codigo, linea){
	opener.document.getElementById('equipo'+linea).value=cs;
	opener.document.getElementById('tmp_equipo'+linea).value=codigo;
	window.close();
}

function ProveedoresLista(){
	window.open(__SHELL__ + "?list3=1","","width=400,height=200,scrollbars=yes,status=no");
	//window.showModalDialog(__SHELL__ + "?list3=1", this.window, "dialogWidth:400px;dialogHeight:200px;status:no;");
}

function ProveedorEscoge(cs, nombre){
	opener.document.getElementById('prov').value=cs;
	opener.document.getElementById('proveedor').value=nombre;
	window.close();
}

function DocumentosZip(_ARCHIVO, _MODULO){
	var ruta = "../seguridad/documentos.php?ARCHIVO=" + _ARCHIVO + "&MODULO=" + _MODULO + "&ACCESO=06";
	window.open(ruta,"","width=100,height=100,scrollbars=yes,status=no");
	//window.showModalDialog(ruta, this.window, "dialogWidth:100px;dialogHeight:100px;status:no;");
}

function DocElimina(id, modulo){	
	if(confirm('Eliminar documento adjunto?')){
		var parametros = {
			'_AJAX' : 1,
			'id' : id,
			'modulo' : modulo,
			'accion' : 'X'
		};
		
		$.ajax({
			data:  parametros,
			url:   __SHELL__,
			type:  'post',
			beforeSend: function () {
				document.getElementById('btn2').style.display='none';
				ALFA('Por favor espere....');
			},
			success: function (_response) {
				switch(_response){
					case '-0':alert('Sesi�n expirada [Err:0]');break;
					case '-1':alert('Error en el env�o de par�metros [Err:-1]');break;
					case '1':
						OMEGA('Transacci�n finalizada');
						document.getElementById('adjunto').innerHTML='<input type="file" name="archivo" id="archivo">';
						break;
					default:alert('Tiempo de espera agotado');break;
				}
			}
		});
	}
}