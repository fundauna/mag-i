<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['xanalizar']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot = new Metodos();

	echo $Robot->EmulsionEncabezadoSet($_POST['xanalizar'], 
		$_POST['tipo'], 
		$_POST['ingrediente'], 
		$_POST['fechaA'], 
		$_POST['tipo_form'], 
		$_POST['dosis'], 
		$_POST['fechaP'], 
		$_POST['maria'], 
		$_POST['numreactivo'], 
		$_POST['fechaD'], 
		$_POST['resultado'], 
		$_POST['cantidad'], 		
		$_POST['cremado1'],
		$_POST['cremado2'],
		$_POST['cremado3'],
		$_POST['cremado4'],
		$_POST['cremado5'],
		$_POST['cremado6'],
		$_POST['cremado7'],
		$_POST['cremado8'],
		$_POST['clari1'],
		$_POST['espon1'],
		$_POST['aceite1'],
		$_POST['aceite2'],
		$_POST['aceite3'],
		$_POST['aceite4'],
		$_POST['aceite5'],
		$_POST['aceite6'],
		$_POST['aceite7'],
		$_POST['aceite8'],
		$_POST['clari2'],
		$_POST['espon2'],
		$_POST['promA2'],
		$_POST['promA3'],
		$_POST['promA4'],
		$_POST['promB2'],
		$_POST['promB3'],
		$_POST['promB4'],
		$_POST['obs'],
		$_ROW['UID']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _03_estabilidadE extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if(!isset($_GET['xanalizar']) or !isset($_GET['ID']) or !isset($_GET['tipo'])) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS GENERALES*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('28') );
			/*PERMISOS ENSAYO*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioMetodo($this->_ROW['UID'], $_GET['tipo']) );
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Robot = new Metodos();
			if( $_GET['ID']=='')
				return $Robot->EmulsionEncabezadoVacio($_GET['xanalizar']);
			else	
				return $Robot->EmulsionEncabezadoGet($_GET['ID']);
		}		
	}
//
}
?>