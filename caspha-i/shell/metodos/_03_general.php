<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['xanalizar'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$tipo = $_FILES['archivo']['type'];
	//if($tipo != 'application/vnd.ms-excel' and $tipo != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') die('El archivo debe ser formato Excel(.xls, xlsx).');
	
	$Robot = new Metodos();
	
	if( $Robot->GeneralEncabezadoSet($_POST['xanalizar'], $_POST['tipo'], $_POST['ingrediente'], $_POST['fechaA'], $_POST['rango'], $_POST['unidad'], $_POST['tipo_form'], $_POST['ce'], $_POST['ie'], $_POST['densidad'], $_POST['incden'], $_ROW['UID'], $_POST['tA']) ){	
		//SUBIR ARCHIVO			
		if($_FILES['archivo']['size'] > 0){
			$nombre = $_FILES['archivo']['name'];
			$file = $_FILES['archivo']['tmp_name'];
			$ruta = "../../docs/metodos/{$_POST['xanalizar']}.zip";
			Bibliotecario::ZipCrear($nombre, $file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");
		}
		//SUBIR ARCHIVO			
		echo'<script>alert("Ensayo ingresado");opener.location.reload();window.close();</script>';
		exit();
	}else{
		echo 'Error al guardar los datos';
		exit();
	}

}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _03_general extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if(!isset($_GET['xanalizar']) or !isset($_GET['ID']) or !isset($_GET['tipo'])) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS GENERALES*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('28') );
			/*PERMISOS ENSAYO*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioMetodo($this->_ROW['UID'], $_GET['tipo']) );
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Robot = new Metodos();
			if( $_GET['ID']=='')
				return $Robot->GeneralEncabezadoVacio($_GET['xanalizar']);
			else	
				return $Robot->GeneralEncabezadoGet($_GET['ID']);
		}		
	}
//
}
?>