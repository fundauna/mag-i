<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['id']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Metodor = new Metodos();
	echo $Metodor->GrafControllObs($_POST['id'], $_POST['observacion']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _03_grafico_control_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('66') );
			/*PERMISOS*/
		}
		
		function GrafControllMuestraEnc(){
			$Metodor = new Metodos();
			return $Metodor->GrafControllMuestraEnc($_GET['I'], $_GET['D'], $_GET['F']);
		}
		
		function GrafControllMuestraDet($_id){
			$Metodor = new Metodos();
			return $Metodor->GrafControllMuestraDet($_id);
		}
		
		function GrafControllMuestraObs($_id){
			$Metodor = new Metodos();
			return $Metodor->GrafControllMuestraObs($_id);
		}
		
		function Formulacion($_id){
			$Variator = new Varios();
			return $Variator->FormulacionesGets($_id);
		}
		
		function Metodo($_id){
			$Metodor = new Metodos();
			return $Metodor->Metodo($_id);
		}
		
		function Formato($_NUMBER){
			return number_format($_NUMBER, 2, '.', ',');
		}
	}
}
?>