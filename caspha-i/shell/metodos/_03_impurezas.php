<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	/*PARA TRAER LA SESION DEL LAB*/
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	$Listator = new Listador();
	if($_GET['list'] == '1')
		$Listator->EquiposCalibrajeLista2('metodos', $_GET['list'], $ROW['LID'], basename(__FILE__));
	if($_GET['list'] == '2')
		$Listator->ColumnasLista('metodos', $ROW['LID'], basename(__FILE__),1);
	exit;
}elseif( isset($_GET['grafico'])){ 
	$str = '<table id="tabla"><tr><td>X</td><td>Y</td></tr>';
	$str .= "<tr><td>{$_POST['x0']}</td><td>{$_POST['y0']}</td></tr>";
	$str .= "<tr><td>{$_POST['x1']}</td><td>{$_POST['y1']}</td></tr>";
	$str .= "<tr><td>{$_POST['x2']}</td><td>{$_POST['y2']}</td></tr>";
	$str .= "<tr><td>{$_POST['x3']}</td><td>{$_POST['y3']}</td></tr>";
	$str .= "</table>";
	echo $str;
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
}elseif( isset($_POST['_AJAX'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['xanalizar']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot = new Metodos();

	echo $Robot->ImpurezasEncabezadoSet($_POST['xanalizar'], 
		$_POST['tipo'], 
		$_POST['ingrediente'], 
		$_POST['fechaA'], 
		$_POST['impureza'], 
		$_POST['rango'], 
		$_POST['unidad'],  
		$_POST['densidad'],
		$_POST['metodo'],
		$_POST['disolvente1'],
		$_POST['validado'],
		$_POST['disolvente2'],
		$_POST['croma'],
		$_POST['disolvente3'],
		$_POST['estandar'],
		$_POST['tiempo1'],
		$_POST['origen1'],
		$_POST['tiempo2'],
		$_POST['certificado1'],
		$_POST['tiempo3'],
		$_POST['pureza1'],
		$_POST['certificado2'],
		$_POST['interno2'],
		$_POST['inyeccion'],
		$_POST['origen2'],
		$_POST['loop'],
		$_POST['certificado3'],
		$_POST['pureza2'],
		$_POST['equipo'],
		$_POST['tipo_c'],
		$_POST['codigo_c'],
		$_POST['marca_c'],
		$_POST['temp_c'],
		$_POST['dim_c'],
		$_POST['tipo_i'],
		$_POST['temp_i'],
		$_POST['modo_i'],
		$_POST['condicion_i'],
		$_POST['gas1'],
		$_POST['flujo1'],
		$_POST['presion1'],
		$_POST['bomba_d'],
		$_POST['elu_d'],
		$_POST['canal_crom1'],
		$_POST['canal_crom2'],
		$_POST['canal_crom3'],
		$_POST['canal_crom4'],
		$_POST['flujo_crom1'],
		$_POST['flujo_crom2'],
		$_POST['flujo_crom3'],
		$_POST['flujo_crom4'],
		$_POST['presion_crom1'],
		$_POST['presion_crom2'],
		$_POST['presion_crom3'],
		$_POST['presion_crom4'],
		$_POST['tipo_d'],
		$_POST['temp_d'],
		$_POST['long_d'],
		$_POST['flujofid1'],
		$_POST['flujofid2'],
		$_POST['flujofid3'],
		$_POST['modo'],
		$_POST['temp1_g'],
		$_POST['temp2_g'],
		$_POST['temp3_g'],
		$_POST['masa1'],
		$_POST['aforado11'],
		$_POST['alicuota11'],
		$_POST['aforado12'],
		$_POST['alicuota12'],
		$_POST['aforado13'],
		$_POST['masa2'],
		$_POST['aforado21'],
		$_POST['alicuota21'],
		$_POST['aforado22'],
		$_POST['alicuota22'],
		$_POST['aforado23'],
		$_POST['alicuota31'],
		$_POST['aforado31'],
		$_POST['alicuota32'],
		$_POST['aforado32'],
		$_POST['concentracion3'],
		$_POST['areaimp3'],
		$_POST['areaint3'],
		$_POST['alicuota41'],
		$_POST['aforado41'],
		$_POST['alicuota42'],
		$_POST['aforado42'],
		$_POST['concentracion4'],
		$_POST['areaimp4'],
		$_POST['areaint4'],
		$_POST['alicuota51'],
		$_POST['aforado51'],
		$_POST['alicuota52'],
		$_POST['aforado52'],
		$_POST['concentracion5'],
		$_POST['areaimp5'],
		$_POST['areaint5'],
		$_POST['alicuota61'],
		$_POST['aforado61'],
		$_POST['alicuota62'],
		$_POST['aforado62'],
		$_POST['concentracion6'],
		$_POST['areaimp6'],
		$_POST['areaint6'],
		$_POST['muestramasa1'],
		$_POST['muestraaforado11'],
		$_POST['muestraalicuota11'],
		$_POST['muestraaforado12'],
		$_POST['muestraalicuota12'], 
		$_POST['muestraaforado13'], 
		$_POST['muestramasa2'], 
		$_POST['muestraaforado21'], 
		$_POST['muestraalicuota21'],  
		$_POST['muestraaforado22'],
		$_POST['muestraalicuota22'],
		$_POST['muestraaforado23'],
		$_POST['areaA1'],
		$_POST['areaB1'],
		$_POST['areaA2'],
		$_POST['areaB2'],
		$_POST['muestramasa3'],
		$_POST['muestraaforado31'],
		$_POST['muestraalicuota31'],
		$_POST['muestraaforado32'],
		$_POST['muestraalicuota32'],
		$_POST['muestraaforado33'],
		$_POST['muestramasa4'],
		$_POST['muestraaforado41'],
		$_POST['muestraalicuota41'],
		$_POST['muestraaforado42'],
		$_POST['muestraalicuota42'],
		$_POST['muestraaforado43'],
		$_POST['areaA3'],
		$_POST['areaB3'],
		$_POST['areaA4'],
		$_POST['areaB4'],
		$_POST['CCRmuestramasa'],
		$_POST['CCRmuestradil1'],
		$_POST['CCRmuestraali1'],
		$_POST['CCRmuestradil2'],
		$_POST['CCRmuestraali2'],
		$_POST['CCRmuestradil3'],
		$_POST['CCRinternomasa'],
		$_POST['CCRinternodil1'],
		$_POST['CCRinternoali1'],
		$_POST['CCRinternodil2'],
		$_POST['CCRinternoali2'],
		$_POST['CCRinternodil3'],
		$_POST['CCRenrimasa'],
		$_POST['CCRenridil1'],
		$_POST['CCRenriali1'],
		$_POST['CCRenridil2'],
		$_POST['CCRenriali2'],
		$_POST['CCRenridil3'],
		$_POST['CCRAreaA1'],
		$_POST['CCRAreaB1'],
		$_POST['CCRAreaA2'],
		$_POST['CCRAreaB2'],
		$_POST['final_P'],
		$_POST['final_I'],   
		$_POST['obs'], 
		$_ROW['UID']);	
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _03_impurezas extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if(!isset($_GET['xanalizar']) or !isset($_GET['ID']) or !isset($_GET['tipo'])) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS GENERALES*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('28') );
			/*PERMISOS ENSAYO*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioMetodo($this->_ROW['UID'], $_GET['tipo']) );
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Robot = new Metodos();
			if( $_GET['ID']=='')
				return $Robot->ImpurezasEncabezadoVacio($_GET['xanalizar']);
			else	
				return $Robot->ImpurezasEncabezadoGet($_GET['ID']);
		}
		
		function SubAnalisisGet(){
			$Robot = new Varios();
			return $Robot->SubAnalisisGet('0', '3');
		}		
	}
//
}
?>