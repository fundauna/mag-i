<?php

if (isset($_POST['xanalizar'])) {
    function __autoload($_BaseClass)
    {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    $ROW = $Securitor->SesionGet();

    $Robot = new Metodos();

    $cs = $Robot->ConsecutivoEnsayoGet();
    if ($Robot->QuechersEncabezadoSet($cs, $_POST['xanalizar'], $_POST['tipo'], $ROW['UID'])) {
        //

        $Robot->QuechersDetalleSet($cs,
            $_POST['analito'],
            $_POST['matriz'],
            $_POST['id'],
            $_POST['resultado'],
            $_POST['reportar'],
            $_POST['tecnica']
        );
        $Robot->ConsecutivoEnsayoSet();
        //
    } else die('0');
    echo 1;
} else {
    /*******************************************************************
     * DECLARACION CLASE GESTORA
     *******************************************************************/
    function __autoload($_BaseClass)
    {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _01_quechers extends Mensajero
    {
        private $_ROW = array();
        private $Securitor = '';

        function __construct()
        {
            //SOLO SE PUEDE ABRIR COMO POP-UP MODAL
            $this->ValidaModal();

            if (!isset($_GET['xanalizar']) or !isset($_GET['ID']) or !isset($_GET['tipo'])) die('Error de parámetros');

            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth()) $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /*PERMISOS GENERALES*/
            if ($this->_ROW['ROL'] != '0') $this->Portero($this->Securitor->UsuarioPermiso('28'));
            /*PERMISOS ENSAYO*/
            if ($this->_ROW['ROL'] != '0') $this->Portero($this->Securitor->UsuarioMetodo($this->_ROW['UID'], $_GET['tipo']));
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function ObtieneDatos()
        {
            $Robot = new Metodos();
            return $Robot->QuechersEncabezadoGet($_GET['xanalizar']);
        }

        function Detalle()
        {
            $Robot = new Metodos();
            return $Robot->QuechersDetalleGet($_GET['ID']);
        }

        function Analista()
        {
            $Robot = new Metodos();
            return $Robot->EnsayoAnalistaGet($_GET['ID']);
        }

        function Analitos()
        {
            $Variator = new Varios();
            return $Variator->SubAnalisisGet1('8', '1');
        }

        function MAAsignados($_matriz)
        {
            $Variator = new Inventario();
            $idgrp = $Variator->obtieneIdGrupo($_matriz);

            return $Variator->MAAsignados2($idgrp);
        }

        function Formato($_NUMBER)
        {
            if ($_NUMBER == '-1') return 'ND';
            elseif ($_NUMBER == '-2') return 'NC';
//cambia punto o coma para los decimales
            return number_format($_NUMBER, 2, ',', '');
        }
    }
}
?>