<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['xanalizar']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot = new Metodos();

	echo $Robot->DitioEncabezadoSet($_POST['xanalizar'], 
		$_POST['tipo'], 
		$_POST['ingrediente'], 
		$_POST['fechaA'], 
		$_POST['rango'], 
		$_POST['unidad'], 
		$_POST['tipo_form'], 
		$_POST['aceite'], 
		$_POST['numreactivo'], 
		$_POST['fechaP'], 
		$_POST['IECB'], 
		$_POST['linealidad1'], 
		$_POST['linealidad2'], 
		$_POST['ampolla1'], 
		$_POST['ampolla2'], 
		$_POST['repeti1'], 
		$_POST['repeti2'], 
		$_POST['muestra1'], 
		$_POST['muestra2'], 
		$_POST['BA'], 
		$_POST['consumido1'], 
		$_POST['consumido2'], 
		$_POST['contenido1'], 
		$_POST['contenido2'], 
		$_POST['contenido3'], 
		$_POST['contenido4'], 
		$_POST['aforado1'], 
		$_POST['aforado2'], 
		$_POST['masa'], 
		$_POST['densidad'], 
		$_POST['promedio'], 
		$_POST['incertidumbre'], 
		$_POST['obs'], 
		$_ROW['UID'],
		$_ROW['UNAME']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _03_ditio extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if(!isset($_GET['xanalizar']) or !isset($_GET['ID']) or !isset($_GET['tipo'])) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS GENERALES*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('28') );
			/*PERMISOS ENSAYO*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioMetodo($this->_ROW['UID'], $_GET['tipo']) );
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Robot = new Metodos();
			if( $_GET['ID']=='')
				return $Robot->DitioEncabezadoVacio($_GET['xanalizar']);
			else	
				return $Robot->DitioEncabezadoGet($_GET['ID']);
		}		
	}
//
}
?>