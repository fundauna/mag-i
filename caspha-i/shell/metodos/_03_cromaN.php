<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	/*PARA TRAER LA SESION DEL LAB*/
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	$Listator = new Listador();
	if($_GET['list'] == '1')
		$Listator->EquiposCalibrajeLista2('metodos', $_GET['list'], $ROW['LID'], basename(__FILE__));
	if($_GET['list'] == '2')
		$Listator->ColumnasLista('metodos', $ROW['LID'], basename(__FILE__),1);
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
}elseif( isset($_POST['_AJAX'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['xanalizar']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot = new Metodos();

	echo $Robot->CromaNEncabezadoSet($_POST['xanalizar'], 
		$_POST['tipo'], 
		$_POST['ingrediente'],
		$_POST['fechaA'], 
		$_POST['rango'], 
		$_POST['unidad'], 
		$_POST['tipo_form'], 
		$_POST['densidad'],
		$_POST['metodo'],
		$_POST['disolvente1'],
		$_POST['validado'],
		$_POST['disolvente2'],
		$_POST['croma'],
		$_POST['disolvente3'],
		$_POST['estandar'],
		$_POST['tiempo1'],
		$_POST['origen1'],
		$_POST['tiempo2'],
		$_POST['certificado1'],
		$_POST['tiempo3'],
		$_POST['pureza1'],
		$_POST['purezas1'],
		$_POST['interno1'],
		$_POST['inyeccion'],
		$_POST['interno2'],
		$_POST['loop'],
		$_POST['origen2'],
		$_POST['metcalibra'],
		$_POST['certificado2'],
		$_POST['pureza2'],
		$_POST['equipo'],
		$_POST['tipo_i'],
		$_POST['temp_i'],
		$_POST['modo_i'],
		$_POST['condicion'],
		$_POST['tipo_c'],
		$_POST['codigo_c'],
		$_POST['marca_c'],
		$_POST['temp_c'],
		$_POST['dim_c'],
		$_POST['gas1'],
		$_POST['flujo1'],
		$_POST['presion1'],
		$_POST['gas2'],
		$_POST['flujo2'],
		$_POST['presion2'],
		$_POST['gas3'],
		$_POST['flujo3'],
		$_POST['presion3'],
		$_POST['tipo_d'],
		$_POST['temp_d'],
		$_POST['long_d'],
		$_POST['fid'],
		$_POST['flujofid1'],
		$_POST['fid1'],
		$_POST['flujofid2'],
		$_POST['fid2'],
		$_POST['flujofid3'],
		$_POST['modo'],
		$_POST['temp1_g'],
		$_POST['temp2_g'],
		$_POST['temp3_g'],
		$_POST['bomba_d'],
		$_POST['elu_d'],
		$_POST['gas4'],
		$_POST['flujo4'],
		$_POST['presion4'],
		$_POST['gas5'],
		$_POST['flujo5'],
		$_POST['presion5'],
		$_POST['gas6'],
		$_POST['flujo6'],
		$_POST['presion6'],
		$_POST['gas7'],
		$_POST['flujo7'],
		$_POST['presion7'],
		$_POST['masa1'],
		$_POST['aforadoA1'],
		$_POST['muestraalicuota1'],
		$_POST['muestraaforadoA2'],
		$_POST['muestraalicuota2'],
		$_POST['muestraaforadoA3'],
		$_POST['masa2'],
		$_POST['aforadoA2'],
		$_POST['plaguicidaalicuota1'],
		$_POST['plaguicidaaforadoA2'],
		$_POST['plaguicidaalicuota2'],
		$_POST['plaguicidaaforadoA3'],
		$_POST['masa3'],
		$_POST['aforadoA3'],
		$_POST['alicuota'],
		$_POST['aforadoB3'],
		$_POST['internoalicuota'],
		$_POST['internoaforadoB3'],
		$_POST['areaA1'],
		$_POST['areaB1'],
		$_POST['areaC1'],
		$_POST['areaA2'],
		$_POST['areaB2'],
		$_POST['areaC2'],
		$_POST['areaA3'],
		$_POST['areaB3'],
		$_POST['areaC3'],
		$_POST['areaA4'],
		$_POST['areaB4'],
		$_POST['areaC4'],
		$_POST['masa4'],
		$_POST['aforadoA4'],
		$_POST['muestraalicuota3'],
		$_POST['muestraaforadoA4'],
		$_POST['muestraalicuota4'],
		$_POST['muestraaforadoA5'],
		$_POST['masa5'],
		$_POST['aforadoA5'],
		$_POST['plaguicidaalicuota3'],
		$_POST['plaguicidaaforadoA4'],
		$_POST['plaguicidaalicuota4'],
		$_POST['plaguicidaaforadoA5'],
		$_POST['masa6'],
		$_POST['aforadoA6'],
		$_POST['alicuota1'],
		$_POST['aforadoB4'],
		$_POST['internoalicuota1'],
		$_POST['internoaforadoB4'],
		$_POST['areaA5'],
		$_POST['areaB5'],
		$_POST['areaC5'],
		$_POST['areaA6'],
		$_POST['areaB6'],
		$_POST['areaC6'],
		$_POST['areaA7'],
		$_POST['areaB7'],
		$_POST['areaC7'],
		$_POST['areaA8'],
		$_POST['areaB8'],
		$_POST['areaC8'],
		$_POST['linealidad1'], 
		$_POST['linealidad2'], 
		$_POST['repeti1'], 
		$_POST['repeti2'], 
		$_POST['IECA'],  
		$_POST['IECB'],
		$_POST['final_P'],
		$_POST['final_I'],   
		$_POST['obs'], 
		$_POST['contenido1'],
		$_POST['contenido2'],
		$_POST['contenido3'],
		$_POST['contenido4'],
		$_ROW['UID'],
		$_ROW['UNAME']);	
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _03_cromaN extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if(!isset($_GET['xanalizar']) or !isset($_GET['ID']) or !isset($_GET['tipo'])) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS GENERALES*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('28') );
			/*PERMISOS ENSAYO*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioMetodo($this->_ROW['UID'], $_GET['tipo']) );
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Robot = new Metodos();
			if( $_GET['ID']=='')
				return $Robot->CromaNEncabezadoVacio($_GET['xanalizar']);
			else	
				return $Robot->CromaNEncabezadoGet($_GET['ID']);
		}		
	}
//
}
?>