<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot = new Metodos();

	if($_POST['paso'] == '1'){
		if( !isset($_POST['xanalizar']) ) die('-1');
	
		echo $Robot->ParaquatEncabezadoSet($_POST['xanalizar'], 
			$_POST['tipo'], 
			$_POST['ingrediente'], 
			$_POST['fechaA'], 
			$_POST['rango'], 
			$_POST['unidad'], 
			$_POST['tipo_form'], 
			$_POST['rango2'],
			$_POST['unidad2'],
			$_POST['origen'],
			$_POST['fechaP'],
			$_POST['pureza'],
			$_POST['factor'],
			$_POST['existe'],
			$_POST['densidad'],
			$_POST['linealidad1'],
			$_POST['linealidad2'],
			$_POST['repeti1'],
			$_POST['repeti2'],
			$_POST['masa'],
			$_POST['bal1'],
			$_POST['vol2'],
			$_POST['bal2'],
			$_POST['vol3'],
			$_POST['bal3'],
			$_POST['abs3'],
			$_POST['vol4'],
			$_POST['bal4'],
			$_POST['abs4'],
			$_POST['vol5'],
			$_POST['bal5'],
			$_POST['abs5'],
			$_POST['vol6'],
			$_POST['bal6'],
			$_POST['abs6'],
			$_POST['mm1'],
			$_POST['bb1'],
			$_POST['aa2'],
			$_POST['bb2'],
			$_POST['aa3'],
			$_POST['bb3'],
			$_POST['cur3'],
			$_POST['sor3'],
			$_POST['cur4'],
			$_POST['sor4'],
			$_POST['cur5'],
			$_POST['sor5'],
			$_POST['mm6'],
			$_POST['bb6'],
			$_POST['aa7'],
			$_POST['bb7'],
			$_POST['aa8'],
			$_POST['bb8'],
			$_POST['cur8'],
			$_POST['sor8'],
			$_POST['cur9'],
			$_POST['sor9'],
			$_POST['curA'],
			$_POST['sorA'],
			$_POST['IC'],
			$_POST['EX'],
			$_POST['obs'],
			$_POST['contenido1'],
			$_POST['contenido2'], 
			$_ROW['UID'],
			$_ROW['UNAME']);	
		exit;
	}elseif($_POST['paso'] == '2'){
		$str = '<table id="tabla"><tr><td>X</td><td>Y</td></tr>';
		$str .= "<tr><td>{$_POST['x0']}</td><td>{$_POST['y0']}</td></tr>";
		$str .= "<tr><td>{$_POST['x1']}</td><td>{$_POST['y1']}</td></tr>";
		$str .= "<tr><td>{$_POST['x2']}</td><td>{$_POST['y2']}</td></tr>";
		$str .= "<tr><td>{$_POST['x3']}</td><td>{$_POST['y3']}</td></tr>";
		$str .= "</table>";
		echo $str;
		exit;
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _03_paraquat extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if(!isset($_GET['xanalizar']) or !isset($_GET['ID']) or !isset($_GET['tipo'])) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS GENERALES*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('28') );
			/*PERMISOS ENSAYO*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioMetodo($this->_ROW['UID'], $_GET['tipo']) );
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Robot = new Metodos();
			if( $_GET['ID']=='')
				return $Robot->ParaquatEncabezadoVacio($_GET['xanalizar']);
			else	
				return $Robot->ParaquatEncabezadoGet($_GET['ID']);
		}		
	}
//
}
?>