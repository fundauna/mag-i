<?php
if( isset($_POST['xanalizar'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Robot = new Metodos();
	
	if($_FILES['archivo']['size'] > 0){
		$ruta = $_FILES['archivo']['name'];
		$file = $_FILES['archivo']['tmp_name'];
		if($_FILES['archivo']['type'] != 'application/x-msdownload' and $_FILES['archivo']['type'] != 'application/vnd.ms-excel') die('Error: El archivo debe ser tipo .xls (Excel 2003)');
		
		Bibliotecario::ZipSubir($file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");
		
		$cs = $Robot->ConsecutivoEnsayoGet();
		if( $Robot->QuechersEncabezadoSet($cs, $_POST['xanalizar'], $_POST['tipo'], $ROW['UID']) ){
		//
			require_once 'Excel/reader.php';
			$data = new Spreadsheet_Excel_Reader();
			//$data->setOutputEncoding('CP1251');
			//$data->setUTFEncoder('mb');
			$data->read($ruta);
			for($i=2;$i<=$data->sheets[0]['numRows'];$i++){
				if( isset($data->sheets[0]['cells'][$i][1]) && str_replace('','',$data->sheets[0]['cells'][$i][1]) != '')
					$Robot->QuechersDetalleSet($cs, 
						$data->sheets[0]['cells'][$i][1], 
						$_POST['matriz'], 
						$data->sheets[0]['cells'][$i][2]
					);
			}
			Bibliotecario::ZipEliminar($ruta);
			
			$Robot->ConsecutivoEnsayoset();
		//
		}else die('Error al ingreasar los datos');
	}else die('Archivo vac�o');
	?>
	<script>
		opener.location.reload();
		alert('Datos ingresados');
		window.close();
	</script>
	<?php
}else{
	/*******************************************************************
	DECLARACION CLASE GESTORA
	*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _01_quechers extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if(!isset($_GET['xanalizar']) or !isset($_GET['ID']) or !isset($_GET['tipo'])) die('Error de par�metros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS GENERALES*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('28') );
			/*PERMISOS ENSAYO*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioMetodo($this->_ROW['UID'], $_GET['tipo']) );
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Robot = new Metodos();
			return $Robot->QuechersEncabezadoGet($_GET['xanalizar']);
		}
		
		function Detalle(){
			$Robot = new Metodos();
			return $Robot->QuechersDetalleGet($_GET['ID']);
		}
		
		function Analista(){
			$Robot = new Metodos();
			return $Robot->EnsayoAnalistaGet($_GET['ID']);
		}
		
		function Analitos(){
			$Variator = new Varios();
			return $Variator->SubAnalisisGet('8', '1');
		}	
	}
}
?>