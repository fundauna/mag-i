<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['xanalizar']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot = new Metodos();


	 //esta funcion invoca al inserta, que es la funcion donde se agregan los siguientes campos a la tabla met_inc.
    //Los nombres que se invocan en el post, debe ser el mismo con el que se envio en el js, en caso de no venir algun parametro, se debe enviar vacio ('').
    //Ya en Metodos.php, se le ponene los nombres oficales en relacion a la tabla
	echo $Robot->inserta(
		$_POST['cer'],
		$_POST['res'],
		'','','','','',''
	);



	echo $Robot->PotasioEncabezadoSet($_POST['xanalizar'], 
		$_POST['tipo'], 
		$_POST['fechaA'], 
		$_POST['rango'], 
		$_POST['unidad'], 
		$_POST['reactivos'], 
		$_POST['factor1'], 
		$_POST['volumen1'], 
		$_POST['densidad'], 
		$_POST['volumen2'], 
		$_POST['masaA1'], 
		$_POST['masaA2'], 
		$_POST['masaB1'], 
		$_POST['masaB2'], 
		$_POST['des1'], 
		$_POST['des3'], 
		$_POST['des4'], 
		$_POST['CD'], 
		$_POST['IC'], 
		$_POST['obs'],
		$_ROW['UID']);	
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _03_potasio extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if(!isset($_GET['xanalizar']) or !isset($_GET['ID']) or !isset($_GET['tipo'])) die('Error de par�metros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS GENERALES*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('28') );
			/*PERMISOS ENSAYO*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioMetodo($this->_ROW['UID'], $_GET['tipo']) );
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Robot = new Metodos();
			if( $_GET['ID']=='')
				return $Robot->PotasioEncabezadoVacio($_GET['xanalizar']);
			else	
				return $Robot->PotasioEncabezadoGet($_GET['ID']);
		}	
		
		function Analista(){
			$Robot = new Metodos();
			return $Robot->EnsayoAnalistaGet($_GET['ID']);
		}	
	}
//
}
?>