<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['xanalizar']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot = new Metodos();

	echo $Robot->AzufreComEncabezadoSet($_POST['xanalizar'], 
		$_POST['tipo'], 
		$_POST['ingrediente'], 
		$_POST['fechaA'],
		$_POST['rango'], 
		$_POST['masa1'], 
		$_POST['masa2'], 
		$_POST['masa3'],
		$_POST['masaA1'],
		$_POST['masaA2'],
		$_POST['masaA3'],
		$_POST['error1'],
		$_POST['error2'],
		$_POST['error3'],
		$_POST['densidad'],
		//$_POST['incden'],
		$_POST['rep1'], 
		$_POST['rep2'], 
		$_POST['resol1'],
		$_POST['resol2'],  
		/*$_POST['pres'],
		$_POST['inclin'],*/
		$_POST['ccur'], 
		$_POST['incex'], 
		$_POST['resultado1'], 
		$_POST['resultado2'], 
		$_ROW['UID']);	
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _03_azufrecombustion extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if(!isset($_GET['xanalizar']) or !isset($_GET['ID']) or !isset($_GET['tipo'])) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS GENERALES*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('28') );
			/*PERMISOS ENSAYO*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioMetodo($this->_ROW['UID'], $_GET['tipo']) );
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Robot = new Metodos();
			if( $_GET['ID']=='')
				return $Robot->AzufreComEncabezadoVacio($_GET['xanalizar']);
			else	
				return $Robot->AzufreComEncabezadoGet($_GET['ID']);
		}
		function ObtieneDatos2(){
			$Robot = new Metodos();
			if( $_GET['ID']=='')
				return $Robot->AzufreMasaVacio();
			else	
				return $Robot->AzufreMasaGet($_GET['ID']);
		}		
	}
//
}
?>