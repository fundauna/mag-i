<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['xanalizar']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot = new Metodos();

    echo $Robot->inserta(
        $_POST['certificadoD'],
        $_POST['resolucionD'],
        $_POST['ic_combinada'],
        $_POST['repetibilidadM'],
        $_POST['resolucionM'],
        $_POST['linealidad'],
        $_POST['excentricidad'],
        $_POST['ic_balanza']
    );


	echo $Robot->KjeldahlEncabezadoSet($_POST['xanalizar'], 
		$_POST['tipo'], 
		$_POST['fechaA'], 
		$_POST['rango'], 
		$_POST['unidad'], 
		$_POST['na'], 
		$_POST['molar'], 
		$_POST['valorado'], 
		$_POST['densidad'], 
		$_POST['alicuota'], 
		$_POST['balon'], 
		$_POST['inc2'], 
		$_POST['inc3'], 
		$_POST['inc5'], 
		$_POST['repetibilidad2'], 
		$_POST['resolucion2'], 
		$_POST['inc_cer'], 
		$_POST['emp'], 
		$_POST['masaA1'], 
		$_POST['masaA2'], 
		$_POST['masaB1'], 
		$_POST['masaB2'], 
		$_POST['des1'], 
		$_POST['val6'], 
		$_POST['des6'], 
		$_POST['CD'], 
		$_POST['IC'],  
		$_POST['obs'],
		$_ROW['UID']);	
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _03_kjeldahl extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if(!isset($_GET['xanalizar']) or !isset($_GET['ID']) or !isset($_GET['tipo'])) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS GENERALES*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('28') );
			/*PERMISOS ENSAYO*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioMetodo($this->_ROW['UID'], $_GET['tipo']) );
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Robot = new Metodos();
			if( $_GET['ID']=='')
				return $Robot->KjeldahlEncabezadoVacio($_GET['xanalizar']);
			else	
				return $Robot->KjeldahlEncabezadoGet($_GET['ID']);
		}
		
		function Analista(){
			$Robot = new Metodos();
			return $Robot->EnsayoAnalistaGet($_GET['ID']);
		}			
	}
//
}
?>