<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['xanalizar']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot = new Metodos();

	/*echo $Robot->inserta(
		'0',// $_POST['certificadoD'],
       '0',// $_POST['resolucionD'],
        '0',//$_POST['ic_combinada'],
        $_POST['repetibilidad'],
        $_POST['resolucion'],
        $_POST['inc_cer'],
        $_POST['emp'],
        '0',//$_POST['ic_balanza']
    );*/


	echo $Robot->MacroEncabezadoSet($_POST['xanalizar'], 
		$_POST['tipo'], 
		$_POST['fechaA'], 
		$_POST['rango'], 
		$_POST['unidad'], 
		$_POST['balon1'], 
		$_POST['balon2'], 	
		$_POST['pipetaD1'], 
		$_POST['pipetaD2'], 
		$_POST['pipetaD3'], 
		$_POST['balonD1'], 
		$_POST['balonD2'], 
		$_POST['balonD3'], 
		$_POST['repetibilidad'], 
		$_POST['resolucion'], 
		$_POST['inc_cer'], 
		$_POST['emp'], 
		$_POST['masa1'], 
		$_POST['masa2'], 
		$_POST['cn1'], 
		$_POST['cn2'], 
		$_POST['dumbre1'], 
		$_POST['dumbre2'], 
		$_POST['delta1'], 
		$_POST['delta2'], 
		$_POST['certi1'], 
		$_POST['certi2'], 
		$_POST['prom_masa'], 
		$_POST['prom_volu'], 
		$_POST['final'], 
		$_POST['obs'],
		$_ROW['UID']);	
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _03_macro extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if(!isset($_GET['xanalizar']) or !isset($_GET['ID']) or !isset($_GET['tipo'])) die('Error de par�metros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS GENERALES*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('28') );
			/*PERMISOS ENSAYO*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioMetodo($this->_ROW['UID'], $_GET['tipo']) );
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Robot = new Metodos();
			if( $_GET['ID']=='')
				return $Robot->MacroEncabezadoVacio($_GET['xanalizar']);
			else	
				return $Robot->MacroEncabezadoGet($_GET['ID']);
		}
		
		function Analista(){
			$Robot = new Metodos();
			return $Robot->EnsayoAnalistaGet($_GET['ID']);
		}	
	}
//
}
?>