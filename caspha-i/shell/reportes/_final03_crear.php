<?php
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	$Listator = new Listador();
	$Listator->EquiposCalibrajeLista('reportes', $_GET['list'], $ROW['LID'], basename(__FILE__));
	exit;
}elseif( isset($_GET['list2'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	$Listator = new Listador();
	$Listator->UsuariosLista('reportes', basename(__FILE__), $_GET['list2'], $ROW['LID']);
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
}elseif( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['estado']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot = new Reportes();
	//error_log("hola 00");
	if($_POST['estado'] == '4'){
		echo $Robot->InformeTemporalElimina($_POST['id']);
		exit;
	}elseif($_POST['estado'] == '0'){
                echo $Robot->InformeTemporalConvierte($_POST['id'], $_POST['obs'], $_POST['obser'],$_POST['muestra'], $_POST['metodos'], $_POST['metodos'],$_POST['metodosotros'], $_POST['equipos'], $_POST['analistas'], $_POST['horas'], $_ROW['UID'], $_ROW['LID']);
		exit;
	}elseif($_POST['estado'] == '2' or $_POST['estado'] == '5'){
		echo $Robot->Informe01ModificaEstado($_POST['id'], $_POST['obs'], $_ROW['UID'], $_POST['estado'], $_POST['cliente']);
		exit;
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _final03_crear extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $Reportes = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['ID']) ) die('Error de par�metros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('85') );
			/*PERMISOS*/
			
			$this->Reportes = new Reportes();
		}

        function MEncabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function GetEncabezado(){
			return $this->Reportes->Informe03EncabezadoGet($_GET['ID'], $this->_ROW['LID']);
		}
		
		function Estado($_var){
			return $this->Reportes->InformeEstado($_var);
		}
	
		function Resultados($_muestra, $_clasificacion){
			return $this->Reportes->Informe03DetalleGet($_GET['ID'], $_muestra, $_clasificacion);
		}
	
		function Aprobar(){
			if($this->_ROW['ROL'] == '0') return true;
			elseif($this->Securitor->UsuarioPermiso('68')=='E') return true;
			else return false;
		}
		
		function Unidad($_var){
			if($_var=='0') return '%m/m';
			elseif($_var=='1'){
				$_POST['densidad']=1;
				return '%m/v';
			}elseif($_var=='2') return 'ppm';
			elseif($_var=='3') return 'mg/kg';
		}
		
		function Dependencia($_var){
			if($_var=='0'){
				$_POST['tipo'] = 2;
				return 'Venta de servicios';
			}elseif($_var=='1'){
				$_POST['tipo'] = 1;
				return 'Fiscalizaci�n';
			}elseif($_var=='2'){
				$_POST['tipo'] = 1;
				return 'Registro';
			}
		}
		
		function Recipiente($_var){
			if($_var=='0') return 'Bolsa metalizada';
			elseif($_var=='1') return 'Bolsa pl�stica';
			elseif($_var=='2') return 'Frasco pl�stico';
			elseif($_var=='3') return 'Frasco de vidrio';
		}
		
		function SolicitudMuestras($_solicitud){
			$Robot = new Servicios();
			return $Robot->Solicitud03MuestrasGet($_solicitud);
		}
		
		function Combo($_clasificacion){
			if(!isset($_POST[$_clasificacion])){
				$_POST[$_clasificacion] = $this->Reportes->GeneraCombos($_clasificacion, $this->_ROW['LID']);
			}
			return $_POST[$_clasificacion];
		}
	}
//
}
?>