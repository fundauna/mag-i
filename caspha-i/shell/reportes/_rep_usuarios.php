<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['lab'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=bitacora.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$Robot = new Reportes();
	$Robot->TableHeader('Reporte de usuarios');
	$ROW = $Robot->UsuariosLab($_POST['lab'], $_POST['usuario'], $_POST['estado']);
	?>
	<table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
	<tr>
		<td><strong>Laboratorio</strong></td>
		<td><strong>Usuario</strong></td>
		<td><strong>Estado</strong></td>
		<td><strong>Perfil</strong></td>
		<td><strong>Permiso</strong></td>
		<td><strong>Privilegio</strong></td>
	</tr>
	<?php
	for($x=0;$x<count($ROW);$x++){
		$cambio = false;
		
		if($x>0){
			if($ROW[$x]['lab']==$ROW[$x-1]['lab']){
				if($ROW[$x]['usuario']==$ROW[$x-1]['usuario']){
					if($ROW[$x]['perfil']==$ROW[$x-1]['perfil']){
						$lab = '';
						$usuario = '';
						$perfil = '';
						$estado = '';
					}
				}else $cambio=true;
			}else $cambio=true;
		}else $cambio=true;
		
		if($cambio){
			echo '<tr><td colspan="6"><hr /></td></tr>';
			$lab = $Robot->LabTipo($ROW[$x]['lab']);
			$usuario = $ROW[$x]['usuario'];
			$perfil = $ROW[$x]['perfil'];
			$estado = $Robot->UsuarioEstado($ROW[$x]['estado']);
		}
	?>
	<tr>
		<td><?=$lab?></td>
		<td><?=$usuario?></td>
		<td><?=$estado?></td>
		<td><?=$perfil?></td>
		<td><?=$ROW[$x]['permiso']?></td>
		<td><?=$ROW[$x]['privilegio']=='L' ? 'Lectura' : 'Escritura'?></td>
	</tr>
	<?php
	}
	echo '</table>';
	
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	class _rep_usuarios extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('86') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function UsuariosMuestra(){
			$Robot = new Seguridad();
			$lid = $this->_ROW['LID'];
			if($this->_ROW['UID'] == 28) $lid = '';
			return $Robot->UsuariosMuestra(0, $lid);
		}
		
		function getUsuario(){
			if($this->_ROW['UID'] == 28)
				echo '<option value="1">LRE</option><option value="2">LDP</option><option value="3">LCC</option><option value="">Todos</option>';
			else{
				if($this->_ROW['LID'] == '1') echo '<option value="'.$this->_ROW['LID'].'">LRE</option>';
				if($this->_ROW['LID'] == '2') echo '<option value="'.$this->_ROW['LID'].'">LDP</option>';
				if($this->_ROW['LID'] == '3') echo '<option value="'.$this->_ROW['LID'].'">LCC</option>';
			}
		}
	}
}
?>