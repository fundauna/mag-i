<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	$Listator->ClientesLista('reportes', $ROW['LID'], basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
}elseif( isset($_POST['formato'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=reportes.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$Robot = new Reportes();
	$Robot->TableHeader('Base de datos de muestras recibidas');	
	?>
	<table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
	<tr align="center">
		<td><strong>Fecha de recepci&oacute;n<br />de muestra</strong></td>
		<td><strong>Cliente</strong></td>
		<td><strong>Solicitud</strong></td>
		<td><strong>C&oacute;digo Externo</strong></td>
		<td><strong>Lote</strong></td>
		<td><strong>C&oacute;digo Interno</strong></td>
		<td><strong>No. de Informe</strong></td>
		<td><strong>Fecha de emisi&oacute;n<br />del informe</strong></td>
		<td><strong>Fecha de entrega<br />del informe</strong></td>
		<td><strong>Duraci&oacute;n de la muestra<br />(D&iacute;as H&aacute;biles)</strong></td>
		<td><strong>Matriz</strong></td>
	</tr>
	<tr><td colspan="11"><hr /></td></tr>
	<?php
	$ROW = $Robot->BDMuestras($_POST['desde'], $_POST['hasta']);
	for($x=0;$x<count($ROW);$x++){
	?>
	<tr align="center">
		<td><?=$ROW[$x]['recepcion']?></td>
		<td><?=$ROW[$x]['cliente']?></td>
		<td><?=$ROW[$x]['solicitud']?></td>
		<td><?=$ROW[$x]['externo']?></td>
		<td><?=$ROW[$x]['lote']?></td>
		<td><?=$ROW[$x]['interno']?></td>
		<td><?=$ROW[$x]['informe']?></td>
		<td><?=$ROW[$x]['emision']?></td>
		<td><?=$ROW[$x]['entrega']?></td>
		<td><?=$ROW[$x]['dias']?></td>
		<td><?=$ROW[$x]['matriz']?></td>
	</tr>
	<?php } ?>
	</table>
	<?php
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _01_BDmuestras extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			
			/*PERMISOS*/
			//if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('21') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
	}
//
}
?>