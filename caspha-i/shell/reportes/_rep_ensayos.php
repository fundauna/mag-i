<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['desde'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=bitacora.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$Robot = new Reportes();
	$Robot->TableHeader("Reporte de ensayos<br>{$_POST['desde']} a {$_POST['hasta']}");
	$ROW = $Robot->EnsayosHistorial($_POST['lab'], $_POST['desde'], $_POST['hasta'], $_POST['ensayo'], $_POST['tipo'], $_POST['cumple']);
	?>
	<table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
	<tr>
		<td><strong>Ensayo</strong></td>
		<td><strong>Fecha</strong></td>
		<td><strong>Solicitud</strong></td>
                <td><strong>Tipo</strong></td>
		<td><strong>Resultado</strong></td>
		<td><strong>Validez</strong></td>
		<td><strong>Analista</strong></td>
	</tr>
	<tr><td colspan="7"><hr /></td></tr>
	<?php
	for($x=0;$x<count($ROW);$x++){
            if ($_POST['lab'] == 3) {
                $Q = $Robot->obtieneTipo($ROW[$x]['solicitud']);
                $ROW[$x]['tipo'] = $Q[0]['tipo'] == '2' ? 'Plaguicidas' : 'Fertilizantes';
            } else {
                $ROW[$x]['tipo'] = '';
            }
	?>
	<tr>
		<td><?=$ROW[$x]['nombre']?></td>
		<td><?=$ROW[$x]['fecha1']?></td>
		<td><?=$ROW[$x]['solicitud']?></td>
                <td><?=$ROW[$x]['tipo']?></td>
		<td><?=$ROW[$x]['resultado']?></td>
		<td><?=$ROW[$x]['cumple']=='1' ? 'V�lido' : 'Inv�lido'?></td>
		<td><?=$ROW[$x]['analista']?></td>
	</tr>
	<?php
	}
	echo "<tr><td colspan='7'><hr></td></tr><tr>
		<td colspan='5' align='right'><strong>Total:</strong></td>
		<td align='center'><strong>{$x}</strong></td>
	</tr></table>";
	
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	class _rep_ensayos extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			
			$_POST['LID'] = $this->_ROW['LID'];
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function Ensayos(){
			$Robot = new Reportes();
			return $Robot->EnsayosMuestra($_POST['LID']);
		}
	}
}
?>