<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['desde'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=bitacora.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$Robot = new Reportes();
	$Robot->TableHeader('Reporte de Validaciones');
	$ROW = $Robot->ValidacionesLab($_POST['lab'], $_POST['tipo'], $_POST['desde'], $_POST['hasta']);
	?>
	<table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
	<tr align="center">
		<td><strong>Tipo</strong></td>
		<td><strong>Consecutivo</strong></td>
		<td><strong>Fecha</strong></td>
		<td><strong>Cumple</strong></td>
		<td><strong>Estado</strong></td>
		<td><strong>Observaciones</strong></td>
		<td><strong>Analista</strong></td>
	</tr>
	<tr><td colspan="7"><hr /></td></tr>
	<?php
	for($x=0;$x<count($ROW);$x++){
	?>
	<tr align="center">
		<td><?=$Robot->ValidacionesTipo($ROW[$x]['tipo'])?></td>
		<td><?=$ROW[$x]['cs']?></td>
		<td><?=$ROW[$x]['fecha1']?></td>
		<td><?=$ROW[$x]['cumple']=='1' ? 'S�' : 'No'?></td>
		<td><?=$Robot->ValidacionesEstado($ROW[$x]['estado'])?></td>
		<td><?=$ROW[$x]['obs']?></td>
		<td><?=$ROW[$x]['analista']?></td>
	</tr>
	<?php
	}
	echo "<tr>
		<td colspan='6' align='right'><strong>Total:</strong></td>
		<td align='center'><strong>{$x}</strong></td>
	</tr></table>";
	
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	class _rep_validaciones extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			
			$_POST['LID'] = $this->_ROW['LID'];
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
	}
}
?>