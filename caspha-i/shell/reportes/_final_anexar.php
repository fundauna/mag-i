<?php
if( isset($_POST['id'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
			
	//SUBE ARCHIVOS
	if($_FILES['archivo']['size'] > 0){	
		$ruta = "../../docs/reportes/{$_POST['id']}.zip";
		$nombre = $_FILES['archivo']['name'];
		$file = $_FILES['archivo']['tmp_name'];
		
		//VERIFICA QUE NO SEA ZIP
		if($_FILES['archivo']['type'] != 'application/zip'){
			Bibliotecario::ZipCrear($nombre, $file, $ruta, 1) or die("Error: No es posible crear el archivo {$nombre}");
		}else{
			Bibliotecario::ZipSubir($file, $ruta);
		}
	}
	
	echo'<script>alert("Transacción Finalizada");window.close();</script>';
	exit();	
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _final_anexar extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			if(!isset($_GET['ID'])) die('Error de parámetros');
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('88') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
	}
}
?>