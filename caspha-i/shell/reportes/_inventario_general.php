<?php
/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
 * ***************************************************************** */
if (isset($_POST['formato'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    if ($_POST['formato'] == '1') {
        header('Content-type: application/x-msdownload');
        header('Content-Disposition: attachment; filename=reportes.xls');
        header('Pragma: no-cache');
        header('Expires: 0');
    }

    $Robot = new Reportes();
    $Inventor = new Inventario();
    $titulo = "<p>Inventario: Listado General {$_POST['nombre']}</p> <p>Moneda: CRC</p>";
    $Robot->TableHeader($titulo,1);
    $ROW = $Robot->InventarioGeneral($_POST['es'], $_POST['tipo']);
    $D = $Inventor->PresentacionesMuestra();

    if ($_POST['es'] == '0' || $_POST['tipo'] == '-99') { //sin E/S
        ?>
        <table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
            <tr align="center">
                <td><strong>C&oacute;digo</strong></td>
                <td><strong>Nombre</strong></td>
                <td><strong>Marca</strong></td>
                <td><strong>Stock Bod. Lab.</strong></td>
                <td><strong>Stock Bod. Aux.</strong></td>
                <td><strong>Presentaci&oacute;n</strong></td>
                <td><strong>Ubicaci&oacute;n</strong></td>
            </tr>
            <tr><td colspan="7"><hr /></td></tr>
        <?php
        for ($x = 0; $x < count($ROW); $x++) {
            ?>
                <tr align="center">
                    <td><?= $ROW[$x]['codigo'] ?></td>
                    <td><?= $ROW[$x]['nombre'] ?></td>
                    <td><?= $ROW[$x]['marca'] ?></td>
                    <td><?= $ROW[$x]['stock1'] ?></td>
                    <td><?= $ROW[$x]['stock2'] ?></td>
                    <td>
                        <?php
                            for($y=0;$y<count($D);$y++){ 
                                if($ROW[$x]['unidad'] == $D[$y]['cs']){ 
                                    echo $D[$y]['nombre'];
                                }else{ 
                                    echo '';                                    
                                } 
                            }
                        ?>
                    </td>
                    <td><?= $ROW[$x]['ubicacion'] ?></td>
                </tr>
        <?php }//FOR  ?>
            <tr align="center">
                <td colspan="5" align="right"><strong>Registros:</strong></td>
                <td><?= $x-- ?></td>
            </tr>
        </table>
    <?php } else { ?>
        <table class='radius' align='center' width='100%' border="1">
            <tr align="center">
                <td><strong>C&oacute;digo<br />interno</strong></td>
                <td><strong>Lote</strong></td>
                <td><strong>Nombre</strong></td>
                <td><strong>Marca</strong></td>
                <td><strong>Stock Bod. Lab.</strong></td>
                <td><strong>Stock Bod. Aux.</strong></td>
                <td><strong>Presentaci&oacute;n</strong></td>
                <td><strong>Ubicaci&oacute;n<br />Bod. Lab.</strong></td>
                <td><strong>Ubicaci&oacute;n<br />Bod. Aux.</strong></td>
                <td><strong>Costo</strong></td>
            </tr>
            <?php
            for ($x = 0, $total = 0; $x < count($ROW); $x++) {
                $total += $ROW[$x]['costo'];
                ?>
                <tr align="center">
                    <td><?= $ROW[$x]['codigo'] ?></td>
                    <td><?= $ROW[$x]['lote'] ?></td>
                    <td><?= $ROW[$x]['nombre'] . ' ' . $ROW[$x]['presentacion'] ?></td>
                    <td><?= $ROW[$x]['marca'] ?></td>
                    <td><?= $ROW[$x]['stock1'] ?></td>
                    <td><?= $ROW[$x]['stock2'] ?></td>
                    <td>
                        <?php
                            for($y=0;$y<count($D);$y++){ 
                                if($ROW[$x]['unidad'] == $D[$y]['cs']){ 
                                    echo $D[$y]['nombre'];
                                }else{ 
                                    echo '';                                    
                                } 
                            }
                        ?>
                    </td>
                    <td><?= $ROW[$x]['ubicacion1'] ?></td>
                    <td><?= $ROW[$x]['ubicacion2'] ?></td>
                    <td align="right"><?= $Robot->Formato($ROW[$x]['costo']) ?> CRC</td>
                </tr>
        <?php }//FOR  ?>
            <tr>
                <td align="right" colspan="6" ><strong>Registros:</strong></td>
                <td align="center"><?= $x-- ?></td>
                <td align="center"><strong>Total:</strong></td>
                <td align="right"><?= $Robot->Formato($total) ?> CRC</td>
            </tr>
        </table>
    <?php }//IF ?>

    <?php
    $Robot->TableFooter();
} else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    class _inventario_general extends Mensajero {

        private $_ROW = array();
        private $Securitor = '';

        function __construct() {
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth())
                $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0')
                $this->Portero($this->Securitor->UsuarioPermiso('82'));
            /* PERMISOS */
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function TiposMuestra() {
            $Inventor = new Inventario();
            return $Inventor->TiposMuestra($this->_ROW['LID'], $this->_ROW['ROL']);
        }

    }

}
?>