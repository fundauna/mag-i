<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['formato'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=reportes.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$Robot = new Reportes();
	$titulo = "Inventario: Registro General de Inventarios";
	$Robot->TableHeader($titulo);	
	?>
	<table class='radius' align='center' width='100%' border="1">
	<tr align="center">
        <td><strong>C&oacute;digo</strong></td>
        <td><strong>Descripci&oacute;n</strong></td>
        <td><strong>Ubicaci&oacute;n (n&uacute;mero de estante)</strong></td>
        <td><strong>Cantidad en LCC</strong></td>
        <td><strong>Cantidad en Pavas</strong></td>
        <td><strong>Cantidad total</strong></td>
        <td><strong>Cantidad en tr&aacute;nsito</strong></td>
        <td><strong>Nivel de inventario</strong></td>
        <td><strong>Costo unitario (USD)</strong></td>
        <td><strong>Costo unitario (CRC)</strong></td>
        <td><strong>Costo total (CRC)</strong></td>
        <td><strong>Presentaci&oacute;n</strong></td>
        <td><strong>N&uacute;mero de parte o cat&aacute;logo</strong></td>
        <td><strong>Equipo  para el que se usa</strong></td>
        <td><strong>A&ntilde;o de ingreso de equipo</strong></td>
        <td><strong>An&aacute;lisis en el que se emplea</strong></td>
        <td><strong>Consumo por an&aacute;lisis (unidades)</strong></td>
        <td><strong>Cantidad de an&aacute;lisis por a&ntilde;o</strong></td>
        <td><strong>Consumo por a&ntilde;o (unidades)</strong></td>
        <td><strong>Unidad de medida</strong></td>
        <td><strong>Inventario de seguridad m&aacute;x (a&ntilde;os)</strong></td>
        <td><strong>Punto de reorden (Unidades)</strong></td>
        <td><strong>Fecha de vencimiento</strong></td>
        <td><strong>Estado de inventario</strong></td>
        <td><strong>Cantidad a pedir para 1 a&ntilde;o de abastecimiento</strong></td>
        <td><strong>Clasificaci&oacute;n ABC para control peri&oacute;dico</strong></td>
        <td><strong>Observaciones</strong></td>
        <td><strong>Encargado</strong></td>
	</tr>
	<?php
    $Robo = new Inventario();
	$ROW = $Robo->General_inventario();
	for($x=0;$x<count($ROW);$x++){
	?>
	<tr align="center">
        <td><?=$ROW[$x]['codigo']?></td>
        <td><?=$ROW[$x]['descripcion']?></td>
        <td><?=$ROW[$x]['ubicacion']?></td>
        <td><?=$ROW[$x]['cantidadlcc']?></td>
        <td><?=$ROW[$x]['cantidadpavas']?></td>
        <td><?=$ROW[$x]['cantidadtotal']?></td>
        <td><?=$ROW[$x]['cantidadtransito']?></td>
        <td><?=$ROW[$x]['nivel']?></td>
        <td><?=$ROW[$x]['costounitariod']?></td>
        <td><?=$ROW[$x]['costounitarioc']?></td>
        <td><?=$ROW[$x]['costototal']?></td>
        <td><?=$ROW[$x]['presentacion']?></td>
        <td><?=$ROW[$x]['parte']?></td>
        <td><?=$ROW[$x]['equipo']?></td>
        <td><?=$ROW[$x]['ingreso']?></td>
        <td><?=$ROW[$x]['analisis']?></td>
        <td><?=$ROW[$x]['consumoanalisis']?></td>
        <td><?=$ROW[$x]['consumoanual']?></td>
        <td><?=$ROW[$x]['medida']?></td>
        <td><?=$ROW[$x]['seguridadmax']?></td>
        <td><?=$ROW[$x]['reorden']?></td>
        <td><?=$ROW[$x]['vencimiento']?></td>
        <td><?=$ROW[$x]['estado']?></td>
        <td><?=$ROW[$x]['pedir']?></td>
        <td><?=$ROW[$x]['abc']?></td>
        <td><?=$ROW[$x]['obs']?></td>
        <td><?=$ROW[$x]['encargado']?></td>
	</tr>
	<?php } ?>
	<tr align="center">
		<td align="right" colspan="19" ><strong>Registros:</strong></td>
		<td align="center"><?=$x--?></td>
	</tr>
	</table>
	<?php
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	class _inventario_vencimientos extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('82') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function Lab(){
			return $this->_ROW['LID'];
		}
	}
}
?>