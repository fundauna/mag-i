<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['desde'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Robot = new Reportes();
	$Robot->TableHeader("Cronograma de Equipos {$_POST['desde']} - {$_POST['hasta']}");
	$ROW = $Robot->EquiposCronograma($_POST['desde'], $_POST['hasta']);
	?>
	<table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
	<tr align="center">
		<td><strong>Fecha</strong></td>
		<td><strong>Tipo</strong></td>
		<td><strong>Cod. Equipo</strong></td>
		<td><strong>Nom. Equipo</strong></td>
		<td><strong>Deshabilita?</strong></td>
		<td><strong>Obs</strong></td>
	</tr>
	<tr><td colspan="6"><hr /></td></tr>
	<?php
	for($x=0;$x<count($ROW);$x++){
	?>
	<tr align="center">
		<td><?=$ROW[$x]['fecha1']?></td>
		<td><?=$ROW[$x]['tipo']?></td>
		<td><?=$ROW[$x]['codigo']?></td>
		<td><?=$ROW[$x]['nombre']?></td>
		<td><?=$ROW[$x]['deshabilita']=='1' ? 'X' : ''?></td>
		<td><?=$ROW[$x]['observaciones']?></td>
	</tr>
	<?php
	}
	echo '</table>';
	
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	class _equipos_cronograma extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('83') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
	}
}
?>