<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	$Listator->ClientesLista('reportes', $ROW['LID'], basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
}elseif( isset($_POST['formato'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=reportes.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	if(!isset($_POST['tipo'])) $_POST['tipo'] = '';

	$Robot = new Reportes();
	$Robot->TableHeader("Reporte de Cotizaciones<br>De: {$_POST['desde']} a: {$_POST['hasta']}");	
	?>
	<table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
	<tr align="center">
		<td><strong>Laboratorio</strong></td>
		<td><strong>Cotizaci&oacute;n</strong></td>
		<td><strong>Fecha</strong></td>
		<td><strong>Cliente</strong></td>
		<td><strong>Solicitante</strong></td>
		<td><strong>Monto</strong></td>
		<td><strong>Estado</strong></td>
	</tr>
	<tr><td colspan="7"><hr /></td></tr>
	<?php
	$ROW = $Robot->CotizacionesXcliente($_POST['LAB'], $_POST['cliente'], $_POST['desde'], $_POST['hasta'], $_POST['estado']);
	for($x=0, $total=0;$x<count($ROW);$x++){
		$total+=$ROW[$x]['monto'];
	?>
	<tr align="center">
		<td><?=$Robot->LabTipo($_POST['LAB'])?></td>
		<td><?=$ROW[$x]['numero']?></td>
		<td><?=$ROW[$x]['fecha1']?></td>
		<td><?=$ROW[$x]['nombre']?></td>
		<td><?=$ROW[$x]['solicitante']?></td>
		<td><?=$Robot->CotizacionEstado($ROW[$x]['estado'])?></td>
		<td>&cent; <?=$ROW[$x]['monto']?></td>
	</tr>
	<?php } 
	echo "<tr>
		<td colspan='5' align='right'><strong>Totales:</strong></td>
		<td align='center'>{$x}</td>
		<td align='center'>&cent; {$total}</td>
	</tr></table>";
	
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _cotizacionesXcliente extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
						
			$_POST['LAB'] = $this->_ROW['LID'];
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
	}
//
}
?>