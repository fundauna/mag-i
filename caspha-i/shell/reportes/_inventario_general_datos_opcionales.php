<?php
/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
 * ***************************************************************** */
if (isset($_POST['formato'])) {

    function __autoload($_BaseClass)
    {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    if ($_POST['formato'] == '1') {
        header('Content-type: application/x-msdownload');
        header('Content-Disposition: attachment; filename=reportes.xls');
        header('Pragma: no-cache');
        header('Expires: 0');
    }

    $Robot = new Reportes();
    $Inventor = new Inventario();
    $titulo = "<p>Inventario: Listado General {$_POST['nombre']}</p> <p>Moneda: CRC</p>";
    $Robot->TableHeader($titulo, 1);
    $ROW = $Inventor->InventarioDetalleGet('', '3');
    $ROW3 = $Inventor->inventarioAdicionalGet($ROW[0]['id'], $ROW[0]['familia']);
    $D = $Inventor->PresentacionesMuestra();

    ?>
    <table class='radius' align='center' width='100%' border="1">
        <tr align="center">
            <td><strong>N&uacute;mero de cat&aacute;logo</strong></td>
            <td><strong>Orden de Compra</strong></td>
            <td><strong>Factura</strong></td>
            <td><strong>Proveedor</strong></td>
            <td><strong>N&uacute;mero de Acta</strong></td>
            <td><strong>Nombre</strong></td>
            <td><strong>Marca</strong></td>
            <td><strong>Marca Comercial</strong></td>
            <td><strong>Presentaci&oacute;n</strong></td>
            <td><strong>Stock Bod. Lab.</strong></td>
            <td><strong>Stock Bod. Aux.</strong></td>
            <td><strong>Ubicaci&oacute;n<br/>Bod. Lab.</strong></td>
            <td><strong>Ubicaci&oacute;n<br/>Bod. Aux.</strong></td>
            <?php foreach ($ROW3 as $dato): ?>
                <td><strong><?= $dato['nombre'] ?></strong></td>
            <?php endforeach; ?>
        </tr>
        <?php
        for ($x = 0; $x < count($ROW); $x++) {
            ?>
            <tr align="center">
                <td><?= $ROW[$x]['codigo'] ?></td>
                <td><?= $ROW[$x]['orden'] ?></td>
                <td><?= $ROW[$x]['factura'] ?></td>
                <td><?= $ROW[$x]['proveedor'] ?></td>
                <td><?= $ROW[$x]['acta'] ?></td>
                <td><?= $ROW[$x]['nombre'] ?></td>
                <td><?= $ROW[$x]['marca'] ?></td>
                <td><?= $ROW[$x]['marcacomercial'] ?></td>
                <td>
                    <?php
                    for ($y = 0; $y < count($D); $y++) {
                        if ($ROW[$x]['presentacion'] == $D[$y]['cs']) {
                            echo $D[$y]['nombre'];
                        } else {
                            echo '';
                        }
                    }
                    ?>
                </td>
                <td><?= $ROW[$x]['stock1'] ?></td>
                <td><?= $ROW[$x]['stock2'] ?></td>
                <td><?= $ROW[$x]['ubicacion1'] ?></td>
                <td><?= $ROW[$x]['ubicacion2'] ?></td>
                <?php foreach ($ROW3 as $dato): ?>
                    <td><strong><?= $dato['valor'] ?></strong></td>
                <?php endforeach; ?>
            </tr>
        <?php }//FOR  ?>
        <tr>
            <td align="right" colspan="6"><strong>Registros:</strong></td>
            <td align="center"><?= $x-- ?></td>
        </tr>
    </table>

    <?php
    $Robot->TableFooter();
} else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass)
    {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    class _inventario_general extends Mensajero
    {

        private $_ROW = array();
        private $Securitor = '';

        function __construct()
        {
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth())
                $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0')
                $this->Portero($this->Securitor->UsuarioPermiso('82'));
            /* PERMISOS */
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function TiposMuestra()
        {
            $Inventor = new Inventario();
            return $Inventor->TiposMuestra($this->_ROW['LID'], $this->_ROW['ROL']);
        }

    }

}
?>