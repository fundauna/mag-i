<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['tipo'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=perfiles.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$Robot = new Reportes();
	$Robot->TableHeader('Reporte de proveedores de an�lisis externos');
	$ROW = $Robot->AnalisisExtLab($_POST['lab'], $_POST['tipo'], $_POST['proveedor']);
	?>
	<table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
	<tr>
		<td><strong>Tipo</strong></td>
		<td><strong>An&aacute;lisis</strong></td>	
		<td><strong>Proveedor</strong></td>
		<td><strong>Descripci&oacute;n</strong></td>
	</tr>
	<tr><td colspan="4"><hr /></td></tr>
	<?php
	for($x=0;$x<count($ROW);$x++){
	?>
	<tr>
		<td><?=$Robot->AnalisisExtTipo($ROW[$x]['tipo'])?></td>
		<td><?=$ROW[$x]['nombre']?></td>
		<td><?=$ROW[$x]['proveedor']?></td>
		<td><?=$ROW[$x]['descripcion']?></td>
	</tr>
	<?php
	}
	echo '</table>';
	
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _rep_analisisE extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			
			$_POST['LID'] = $this->_ROW['LID'];
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
	}
}
?>