<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['lab'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=perfiles.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$Robot = new Reportes();
	$Robot->TableHeader('Reporte de perfiles');
	$ROW = $Robot->PerfilesLab($_POST['lab']);
	?>
	<table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
	<tr>
		<td><strong>Laboratorio</strong></td>
		<td><strong>M&oacute;dulo</strong></td>
		<td><strong>Perfil</strong></td>
		<td><strong>Permiso</strong></td>
		<td><strong>Privilegio</strong></td>
	</tr>
	<tr><td colspan="5"><hr /></td></tr>
	<?php
	for($x=0;$x<count($ROW);$x++){
	?>
	<tr>
		<td><?=$Robot->LabTipo($ROW[$x]['lab'])?></td>
		<td><?=$ROW[$x]['modulo']?></td>
		<td><?=$ROW[$x]['perfil']?></td>
		<td><?=$ROW[$x]['permiso']?></td>
		<td><?=$ROW[$x]['privilegio']=='L' ? 'Lectura' : 'Escritura'?></td>
	</tr>
	<?php
	}
	echo '</table>';
	
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	class _rep_perfiles extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('86') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function getUsuario(){
			if($this->_ROW['UID'] == 28)
				echo '<option value="1">LRE</option><option value="2">LDP</option><option value="3">LCC</option><option value="">Todos</option>';
			else{
				if($this->_ROW['LID'] == '1') echo '<option value="'.$this->_ROW['LID'].'">LRE</option>';
				if($this->_ROW['LID'] == '2') echo '<option value="'.$this->_ROW['LID'].'">LDP</option>';
				if($this->_ROW['LID'] == '3') echo '<option value="'.$this->_ROW['LID'].'">LCC</option>';
			}
		}
	}
}
?>