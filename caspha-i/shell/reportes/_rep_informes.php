<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['desde'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=bitacora.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$Robot = new Reportes();
	$Robot->TableHeader("Reporte de Informes de Resultados<br>{$_POST['desde']} a {$_POST['hasta']}");
	$ROW = $Robot->InformesReporteGet($_POST['tipo'], $_POST['valor'], $_POST['desde'], $_POST['hasta'], $_POST['estado'], isset($_POST['tipo']) ? $_POST['tipo'] : '');
	?>
	<table style="font-size:10px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
	<tr>
		<td><strong>Informe</strong></td>
		<td><strong>Solicitud</strong></td>
        <td><strong>Muestra</strong></td>
        <td><strong>Tipo</strong></td>
		<td><strong>Producto</strong></td>
		<td><strong>Registro</strong></td>
		<td><strong>Lote</strong></td>
        <td><strong>Formulaci&oacute;n</strong></td>
        <td><strong>Fecha Aprobaci&oacute;n</strong></td>
        <td><strong>An&aacute;lisis</strong></td>
        <td><strong>Resultado</strong></td>
	</tr>
	<tr><td colspan="11"><hr /></td></tr>
	<?php
	for($x=0;$x<count($ROW);$x++){
            if ($ROW[$x]['lab'] == 3) {
                $ROW[$x]['tipo'] = $ROW[$x]['tipo'] == '2' ? 'Plaguicidas' : 'Fertilizantes';
            } else {
                $ROW[$x]['tipo'] = '';
            }
	?>
	<tr>
		<td><?=$ROW[$x]['informe']?></td>
        <td><?=$ROW[$x]['solicitud']?></td>
        <td><?=$ROW[$x]['muestra']?></td>
        <td><?=$ROW[$x]['tipo']?></td>
        <td><?=$ROW[$x]['producto']?></td>
        <td><?=$ROW[$x]['registro']?></td>
        <td><?=$ROW[$x]['lote']?></td>
        <td><?=$ROW[$x]['formulacion']?></td>
        <td><?=$ROW[$x]['fecha1']?></td>
        <td><?=$ROW[$x]['analisis']?></td>
        <td><?=$ROW[$x]['resultado']?></td>
	</tr>
	<?php
	}
	echo "<tr><td colspan='11'><hr></td></tr><tr>
		<td colspan='9' align='right'><strong>Total:</strong></td>
		<td><strong>{$x}</strong></td>
	</tr></table>";
	
	$Robot->TableFooter();
}else{
	/*******************************************************************
	DECLARACION CLASE GESTORA
	*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _rep_informes extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('85') );
			/*PERMISOS*/
                        $_POST['LAB'] = $this->_ROW['LID'];
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
	}
}
?>