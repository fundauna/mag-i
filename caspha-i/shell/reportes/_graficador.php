<?php
function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
class _graficador extends Mensajero{
//
	private $_ROW = array();

	function __construct(){
		if(!isset($_POST['_TIPO'])) die('Error de par�metros');
			
		$_POST['desde'] = substr($_POST['desde'], 6, 4);
		
		$Robot = new Reportes();
		
		if($_POST['_TIPO']==0){
			$_POST['titulo'] = 'Comparaci�n de las horas de capacitaci�n total recibidas por cada funcionario por a�o';
			$Robot->TablaCapacitacionesSet($_POST['unidad'], $_POST['desde'], 4);
		}elseif($_POST['_TIPO']==1){
			if($_POST['tipo'] == 'S') $label = 'SGC';
			elseif($_POST['tipo'] == 'T') $label = 'T�cnica';
			elseif($_POST['tipo'] == 'O') $label = 'Otras';
			elseif($_POST['tipo'] == 'I') $label = 'Internacional';
			$_POST['titulo'] = "Comparaci�n de las horas de capacitaci�n {$label} recibida por cada funcionario por a�o";
			$Robot->TablaCapacitacionesSet($_POST['unidad'], $_POST['desde'], 4, $_POST['tipo']);
		}
		
		$this->_ROW = $Robot->TablaCapacitacionesGet();
	}
	
	function TablaCapacitaciones(){
		return $this->_ROW;
	}
	
	function LimpiaTabla(){
		//$Robot = new Reportes();
		//$Robot->TablaCapacitacionesDel();
	}
//
}
?>