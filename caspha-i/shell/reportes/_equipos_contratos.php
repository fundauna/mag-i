<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['formato'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=reportes.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$Robot = new Reportes();
	$titulo = 'Equipos: Contratos';
	$Robot->TableHeader($titulo);	
	?>
	<table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
	<tr align="center" bgcolor="#CCCCCC">
    	<td colspan="4">Informaci&oacute;n de equipo</td>
        <td colspan="5">Informaci&oacute;n del contrato</td>
    </tr>
    <tr align="center">
		<td><strong>C&oacute;digo</strong></td>
		<td><strong>Nombre</strong></td>
		<td><strong>Marca</strong></td>
		<td><strong>Serie</strong></td>
		<td><strong>Contrato</strong></td>
		<td><strong>Tipo</strong></td>
		<td><strong>Proveedor</strong></td>
        <td><strong>Tel&eacute;fono</strong></td>
	</tr>
	<tr><td colspan="8"><hr /></td></tr>
	<?php
	$ROW = $Robot->EquiposContratosGet($_POST['codigo'], $_POST['nombre']);
	for($x=0;$x<count($ROW);$x++){
	?>
	<tr align="center">
		<td><?=$ROW[$x]['codigo']?></td>
		<td><?=$ROW[$x]['nombre']?></td>
		<td><?=$ROW[$x]['marca']?></td>
		<td><?=$ROW[$x]['serie']?></td>
		<td><?=$ROW[$x]['contrato']?></td>
		<td><?=$ROW[$x]['tipo']?></td>
        <td><?=$ROW[$x]['proveedor']?></td>
		<td><?=$ROW[$x]['telefono']?></td>
	</tr>
	<?php } ?>
	</table>
	<?php
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	class _equipos_contratos extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('83') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
	}
}
?>