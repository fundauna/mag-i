<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['formato'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=reportes.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$Robot = new Reportes();
	$titulo = "Inventario: Control Material Vencido";
	$Robot->TableHeader($titulo);	
	?>
	<table class='radius' align='center' width='100%' border="1">
	<tr align="center">
		<td><strong>C&oacute;digo<br />n&uacute;mero de parte</strong></td>
        <td><strong>Material</strong></td>
        <td><strong>Costo Unitario</strong></td>
        <td><strong>Fecha Vencimiento</strong></td>
        <td><strong>Cantidad Vencida</strong></td>
        <td><strong>Unidad</strong></td>
        <td><strong>Costo Total</strong></td>
        <td><strong>Observaciones</strong></td>
	</tr>
	<?php
    $Robo = new Inventario();
	$ROW = $Robo->MaterialVencido();
	for($x=0;$x<count($ROW);$x++){
	?>
	<tr align="center">
		<td><?=$ROW[$x]['codigo']?></td>
        <td><?=$ROW[$x]['material']?></td>
        <td><?=$ROW[$x]['costo']?></td>
        <td><?=$ROW[$x]['vencimiento']?></td>
        <td><?=$ROW[$x]['cantidad']?></td>
        <td><?=$ROW[$x]['unidad']?></td>
        <td><?=$ROW[$x]['total']?></td>
        <td><?=$ROW[$x]['obs']?></td>
	</tr>
	<?php } ?>
	<tr align="center">
		<td align="right" colspan="7" ><strong>Registros:</strong></td>
		<td align="center"><?=$x--?></td>
	</tr>
	</table>
	<?php
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	class _inventario_vencimientos extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('82') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function Lab(){
			return $this->_ROW['LID'];
		}
	}
}
?>