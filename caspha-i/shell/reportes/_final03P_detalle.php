<?php

/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 * ***************************************************************** */
if (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth())
        die('-0');
    $_ROW = $Securitor->SesionGet();

    $Robot = new Reportes();

    if ($_POST['_AJAX'] == '1') {

        if ($_POST['estado'] == '2' or $_POST['estado'] == '5') {
            echo $Robot->InformeModificaEstado($_POST['id'], $_POST['obs'], $_ROW['UID'], $_POST['estado'], $_POST['cliente']);
        } elseif ($_POST['estado'] == '4') {
            echo $Robot->InformeFinalElimina($_POST['id'], $_ROW['UID']);
        }
    } else {
        echo $Robot->Informe03MetodosPlaguicidasIME($_POST['id'], $_POST['metodo'], isset($_POST['metodos']) ? $_POST['metodos'] : [], $_ROW['UID']);
    }
    exit;
} else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _final03P_detalle extends Mensajero {

        private $_ROW = array();
        private $Securitor = '';

        function __construct() {
            //SOLO SE PUEDE ABRIR COMO POP-UP MODAL
            $this->ValidaModal();

            if (!isset($_GET['ID']))
                die('Error de parámetros');

            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth())
                $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0')
                $this->Portero($this->Securitor->UsuarioPermiso('85'));
            /* PERMISOS */
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function GetEncabezado() {
            $Robot = new Reportes();
            return $Robot->Informe03EncabezadoGet($_GET['ID'], $this->_ROW['LID']);
        }

        function Metodos() {
            $Robot = new Reportes();
            return $Robot->Informe03MetodosPlaguicidasGet($_GET['ID']);
        }

        function Metodo() {
            $Robot = new Reportes();
            return $Robot->Informe03DetalleGetM($_GET['ID']);
        }

        function Observaciones() {
            $Robot = new Reportes();
            return $Robot->Informe03ObservacionesGet($_GET['ID']);
        }

        function Equipos() {
            $Robot = new Reportes();
            return $Robot->Informe03EquiposGet($_GET['ID']);
        }

        function Estado($_var) {
            $Robot = new Reportes();
            return $Robot->InformeEstado($_var);
        }

        function Resultados($_muestra, $_clasificacion) {
            $Robot = new Reportes();
            return $Robot->Informe03PlagasDetalleGet($_GET['ID'], $_muestra, $_clasificacion);
        }

        function Accion($_var) {
            $Robot = new Reportes();
            return $Robot->InformeAccion($_var);
        }

        function Historial() {
            $Robot = new Reportes();
            return $Robot->InformeHistorial($_GET['ID']);
        }

        function Aprobar() {
            if ($this->_ROW['ROL'] == '0')
                return true;
            elseif ($this->Securitor->UsuarioPermiso('87') == 'E')
                return true;
            else
                return false;
        }

        function Unidad($_var) {
            if ($_var == '0')
                return '%m/m';
            elseif ($_var == '1')
                return '%m/v';
            elseif ($_var == '2')
                return 'ppm';
            elseif ($_var == '3')
                return 'mg/kg';
        }

        function Dependencia($_var) {
            if ($_var == '0') {
                $_POST['tipo'] = 2;
                return 'Venta de servicios';
            } elseif ($_var == '1') {
                $_POST['tipo'] = 1;
                return 'Fiscalización';
            } elseif ($_var == '2') {
                $_POST['tipo'] = 1;
                return 'Registro';
            }
        }

        function Recipiente($_var) {
            if ($_var == '0')
                return 'Bolsa metalizada';
            elseif ($_var == '1')
                return 'Bolsa plástica';
            elseif ($_var == '2')
                return 'Frasco plástico';
            elseif ($_var == '3')
                return 'Frasco de vidrio';
        }

        function SolicitudMuestras($_solicitud) {
            $Robot = new Servicios();
            return $Robot->Solicitud03MuestrasGet($_solicitud);
        }

    }

//
}
?>