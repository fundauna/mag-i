<?php

if (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth()) {
        die('-0');
    }
    $_ROW = $Securitor->SesionGet();

    $Inventor = new Reportes();

    if ($_POST['_AJAX'] == '1') {
        echo $Inventor->Mant_JefeIME($_POST['id'], $_POST['accion'], $_POST['nombre'], $_POST['cargo'], $_POST['estado'], $_POST['obs']);
    }
    exit;
} else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _mant_jefe_detalle extends Mensajero {

        private $_ROW = array();
        private $Securitor = '';
        private $Inventor;

        function __construct() {
            //SOLO SE PUEDE ABRIR COMO POP-UP MODAL
            $this->ValidaModal();

            if (!isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M')) {
                die('Error de parámetros');
            }

            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth()) {
                $this->Err();
            }
            $this->_ROW = $this->Securitor->SesionGet();
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0') {
                $this->Portero($this->Securitor->UsuarioPermiso('A4'));
            }
            /* PERMISOS */

            $this->Inventor = new Reportes();
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function ObtieneDatos() {


            if (!isset($_GET['ID']) or $_GET['ID'] == '') {
                return $this->Inventor->Mant_JefeVacio();
            } else {
                return $this->Inventor->Mant_Jefe($_GET['ID']);
            }
        }

    }

}
?>