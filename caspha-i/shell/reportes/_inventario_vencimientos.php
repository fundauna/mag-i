<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['formato'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=reportes.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$Robot = new Reportes();
	$titulo = "Inventario: Fechas de vencimiento";
	$Robot->TableHeader($titulo);	
	?>
	<table class='radius' align='center' width='100%' border="1">
	<tr align="center">
		<td><strong>C&oacute;digo<br />interno</strong></td>
        <td><strong>Lote</strong></td>
        <td><strong>Nombre</strong></td>
        <td><strong>Marca</strong></td>
        <td><strong>Stock Bod. Lab.</strong></td>
        <td><strong>Stock Bod. Aux.</strong></td>
        <td><strong>Ubicaci&oacute;n<br />Bod. Lab.</strong></td>
        <td><strong>Ubicaci&oacute;n<br />Bod. Aux.</strong></td>
		<td><strong>Vencimiento</strong></td>
	</tr>
	<?php
	$ROW = $Robot->InventarioVencimientos();
	for($x=0;$x<count($ROW);$x++){
	?>
	<tr align="center">
		<td><?=$ROW[$x]['codigo']?></td>
        <td><?=$ROW[$x]['lote']?></td>
        <td><?=$ROW[$x]['nombre'].' '.$ROW[$x]['presentacion']?></td>
        <td><?=$ROW[$x]['marca']?></td>
        <td><?=$ROW[$x]['stock1']?></td>
        <td><?=$ROW[$x]['stock2']?></td>
        <td><?=$ROW[$x]['ubicacion1']?></td>
        <td><?=$ROW[$x]['ubicacion2']?></td>
		<td><?=$ROW[$x]['vencimiento']?></td>
	</tr>
	<?php } ?>
	<tr align="center">
		<td align="right" colspan="8" ><strong>Registros:</strong></td>
		<td align="center"><?=$x--?></td>
	</tr>
	</table>
	<?php
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	class _inventario_vencimientos extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('82') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function Lab(){
			return $this->_ROW['LID'];
		}
	}
}
?>