<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	$Listator->ClientesLista('reportes', $ROW['LID'], basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
}elseif( isset($_POST['formato'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=reportes.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	if(!isset($_POST['tipo'])) $_POST['tipo'] = '';

	$Robot = new Reportes();
	$titulo = "Muestras por cliente";
	$Robot->TableHeader($titulo);	
	?>
	<table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
	<tr align="center"><td colspan="4"></td><td colspan="2">Muestra</td></tr>
	<tr align="center">
		<td><strong>Solicitud</strong></td>
		<td><strong>Fecha</strong></td>
                <td><strong>Tipo</strong></td>
		<td><strong>Cliente</strong></td>
		<td><strong>Estado</strong></td>
		<td><strong>Cod. Int.</strong></td>
		<td><strong>Cod. Ext.</strong></td>
	</tr>
	<tr><td colspan="7"><hr /></td></tr>
	<?php
	$actual = '';
	$contador = 1;
	$siglas = array('LRE-', 'LCC-');
	$cambiar = array('', '');
	//
	$ROW = $Robot->MuestrasXcliente($_POST['LAB'], $_POST['cliente'], $_POST['desde'], $_POST['hasta'], isset($_POST['tipo']) ? $_POST['tipo'] : '');
	for($x=0, $total=0;$x<count($ROW);$x++){
		if($actual != $ROW[$x]['cs']){
			$contador = 1;
			$actual = $ROW[$x]['cs'];
		}else{
			$contador++;
		}
                if($_POST['LAB'] == 3){
                    $ROW[$x]['tipo'] = $ROW[$x]['tipo'] == '-2' ? 'Plaguicidas' : 'Fertilizantes';
                }else{
                    $ROW[$x]['tipo'] = '';
                }
		$cod_int = str_replace($siglas, $cambiar, $ROW[$x]['cs']).'-'.$contador.$ROW[$x]['tipo'];
	?>
	<tr align="center">
		<td><?=$ROW[$x]['cs']?></td>		
                <td><?=$ROW[$x]['fecha']?></td>
                <td><?=$ROW[$x]['tipo']?></td>
		<td><?=$ROW[$x]['cliente']?></td>
		<td><?=$Robot->SolicitudEstado($ROW[$x]['estado'])?></td>
		<td><?=$cod_int?></td>
		<td><?=$ROW[$x]['cod_ext']?></td>
	</tr>
	<?php } ?>
        <tr><td colspan="7"><hr></td></tr>
        <tr align='center'>
            <td colspan='5'>
            <td><strong>Total:</strong></td>
            <td><strong><?=$x?></strong></td>
	</tr>
	</table>
	<?php
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _muestrasXcliente extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			
			/*PERMISOS*/
			//if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('21') );
			/*PERMISOS*/
			$_POST['LAB'] = $this->_ROW['LID'];
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
	}
//
}
?>