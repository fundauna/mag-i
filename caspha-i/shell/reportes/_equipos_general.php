<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['formato'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=reportes.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$Robot = new Reportes();
	$titulo = "Equipos: Listado General";
	$Robot->TableHeader($titulo);	
	?>
	<table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
	<tr align="center">
		<td><strong>C&oacute;digo</strong></td>
		<td><strong>Nombre</strong></td>
		<td><strong>Patrimonio</strong></td>
		<td><strong>Marca</strong></td>
		<td><strong>Modelo</strong></td>
		<td><strong>Serie</strong></td>
		<td><strong>Ubicaci&oacute;n</strong></td>
		<td><strong>Ctrl. metro?</strong></td>
        <td><strong>Proveedor</strong></td>
		<td><strong>Estado</strong></td>
	</tr>
	<tr><td colspan="10"><hr /></td></tr>
	<?php
	$ROW = $Robot->EquiposGeneralGet($_POST['nombre'], $_POST['control_metro'], $_POST['estado']);
	for($x=0;$x<count($ROW);$x++){
	?>
	<tr align="center">
		<td><?=$ROW[$x]['codigo']?></td>
		<td><?=$ROW[$x]['nombre']?></td>
		<td><?=$ROW[$x]['patrimonio']?></td>
		<td><?=$ROW[$x]['marca']?></td>
		<td><?=$ROW[$x]['modelo']?></td>
		<td><?=$ROW[$x]['serie']?></td>
		<td><?=$ROW[$x]['ubi_equipo']?></td>
		<td><?=$ROW[$x]['control_metro']=='1' ? 'S�' : 'No'?></td>
        <td><?=$ROW[$x]['proveedor']?></td>
		<td><?=$ROW[$x]['estado']?></td>
	</tr>
	<?php } ?>
	</table>
	<?php
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	class _equipos_general extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('83') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
	}
}
?>