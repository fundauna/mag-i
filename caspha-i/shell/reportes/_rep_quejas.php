<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['desde'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=bitacora.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$Robot = new Reportes();
	$Robot->TableHeader('Reporte de Quejas presentadas contra el Departamento de Laboratorios');
	$ROW = $Robot->QuejasLab($_POST['instancia'], $_POST['estado'], $_POST['desde'], $_POST['hasta']);
	?>
	<table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
	<tr align="center">
		<td><strong>C&oacute;digo</strong></td>
		<td><strong>Fecha presentaci&oacute;n</strong></td>
		<td><strong>Fecha l&iacute;mite</strong></td>
		<td><strong>Fecha recibido G.C.</strong></td>
		<td><strong>Instancia</strong></td>
		<td><strong>ACAP. asociada</strong></td>
		<td><strong>Responsable</strong></td>
		<td><strong>Estado</strong></td>
	</tr>
	<tr><td colspan="8"><hr /></td></tr>
	<?php
	for($x=0;$x<count($ROW);$x++){
	?>
	<tr align="center">
		<td><?=$ROW[$x]['codigo']?></td>
		<td><?=$ROW[$x]['fec_pre']?></td>
		<td><?=$ROW[$x]['fec_lim']?></td>
		<td><?=$ROW[$x]['fec_rec']?></td>
		<td><?=$Robot->QuejasTipo($ROW[$x]['instancia'])?></td>
		<td><?=$ROW[$x]['acap']?></td>
		<td><?=$ROW[$x]['responsable']?></td>
        <td><?=$ROW[$x]['estado']=='1' ? 'Abierta' : 'Cerrada'?></td>
	</tr>
	<?php
	}
	echo "<tr>
		<td colspan='7' align='right'><strong>Total:</strong></td>
		<td align='center'><strong>{$x}</strong></td>
	</tr></table>";
	
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	class _rep_quejas extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('51') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
	}
}
?>