<?php
/*******************************************************************
 * PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 *******************************************************************/
if (isset($_POST['_AJAX'])) {
    function __autoload($_BaseClass)
    {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    if (!isset($_POST['estado'])) die('-1');

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth()) die('-0');
    $_ROW = $Securitor->SesionGet();


    $Robot = new Reportes();

    if ($_POST['estado'] == '4') {
        echo $Robot->InformeTemporalElimina($_POST['id']);
        exit;
    } elseif ($_POST['estado'] == '0') {
        //echo $Robot->InformeTemporalConvierte($_POST['id'], $_POST['obs'], '', '', $_POST['procedimiento'], $_POST['procedimiento'], '', '', '', '', $_ROW['UID'], $_ROW['LID']);
        echo $Robot->InformeTemporalConvierte($_POST['id'], '', '', '', '', '', '', '', '', '', $_ROW['UID'], $_ROW['LID']);
        exit;
    } elseif ($_POST['estado'] == '2' or $_POST['estado'] == '5') {
        echo $Robot->InformeModificaEstado($_POST['id'], '', $_ROW['UID'], $_POST['estado'], $_POST['cliente']);
        exit;
    } elseif ($_POST['estado'] == '9') {
        echo $Robot->FirmarInforme($_POST['id'], $_POST['firmante'], $_ROW['UID'], $_POST['estado']);
        exit;
    } elseif ($_POST['estado'] == '8') {
        echo $Robot->EliminaFirma($_POST['id'], $_ROW['UID'], $_POST['estado']);
        exit;
    }
} else {
    /*******************************************************************
     * DECLARACION CLASE GESTORA
     *******************************************************************/
    function __autoload($_BaseClass)
    {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _final01_detalle extends Mensajero
    {
        private $_ROW = array();
        private $Securitor = '';

        function __construct()
        {
            //SOLO SE PUEDE ABRIR COMO POP-UP MODAL
            $this->ValidaModal();

            if (!isset($_GET['ID'])) die('Error de par�metros');

            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth()) $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /*PERMISOS*/
            if ($this->_ROW['ROL'] != '0') $this->Portero($this->Securitor->UsuarioPermiso('85'));
            /*PERMISOS*/
        }

        function MEncabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function Mes()
        {
            $meses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
            return $meses[date('n') - 1] . ', ' . date('Y');
        }

        function Asterisco($_analito)
        {
            if (strpos($_analito, '*') === false) return false;
            else return true;
        }

        function Etiqueta($_analito, $_acreditado1)
        {
            if ($this->Asterisco($_analito) && $_acreditado1) {
                $_analito = str_replace('*', '', $_analito);
                return '<font style="font-size:14px; font-weight:bold">*</font>&nbsp;' . $_analito;
            } else {
                $_analito = str_replace('*', '', $_analito);
            }
            return strtoupper($_analito);
        }

        function Encabezado()
        {

            $Robot = new Reportes();
            return $Robot->Informe01EncabezadoGet($_GET['ID'], $this->_ROW['LID']);
        }

        function Estado($_var)
        {
            $Robot = new Reportes();
            return $Robot->InformeEstado($_var);
        }

        function Resultados($_idm)
        {
            $Robot = new Reportes();
            return $Robot->Informe01DetalleGet($_POST['ensayo'], $_idm);
        }

        function Formato($_NUMBER, $_inc = false)
        {
            if (!is_numeric($_NUMBER)) {
                if ($_NUMBER == 'NC') return '<strong>NC</strong>';
                else return $_NUMBER;
            } else {
                if ($_inc) return '<strong>&plusmn; ' . number_format($_NUMBER, 2, ',', '') . '</strong>';
                else return '<strong>' . number_format($_NUMBER, 2, ',', '') . '</strong>';
            }
        }

        function Aprobar()
        {
            if ($this->_ROW['ROL'] == '0') return true;
            elseif ($this->Securitor->UsuarioPermiso('87') == 'E') return true;
            else return false;
        }

        function obtieneFirma()
        {
            $Robot = new Reportes();
            $Q = $Robot->Mant_Jefe('', 1);
            if ($Q) {
                return $Q;
            } else {
                return array(0 => array(
                    'id' => '',
                    'nombre' => 'Sin Registro',
                    'cargo' => '',
                    'estado' => '',
                    'obs' => ''
                ));
            }
        }

        function obtieneFechas($_id)
        {
            $Robot = new Reportes();
            return $Robot->obtieneFechas($_id);
        }

        function obtieneFechasMuestreo($_id)
        {
            $Robot = new Reportes();
            return $Robot->obtieneFechaMuestreo($_id);
        }

        function obtieneObservacion($_cs)
        {
            $Robot = new Reportes();
            return $Robot->obtieneobservaciones($_cs);
        }

        function obtieneFRF($_id)
        {
            $Robot = new Reportes();
            return $Robot->obtieneFRF($_id);
        }


        function obtieneTipo($_id)
        {
            $Robot = new Reportes();
            return $Robot->tip($_id);
        }


        function obtieneGrupo($_matriz)
        {
            $Robot = new Inventario();
            return $Robot->obtieneIdGrupo('', $_matriz);
        }

        function MAAsignados($_grupo)
        {
            $Robot = new Inventario();
            $Q = $Robot->MAAsignados($_grupo);
            $VECTOR = array();
            foreach ($Q as $dato) {
                array_push($VECTOR, str_replace(' ', '', strtolower(utf8_decode($dato['analito']))));
            }
            return $VECTOR;
        }

        function MAAsignados1($_grupo)
        {
            $Robot = new Inventario();///1///
            $Q = $Robot->MAAsignados2($_grupo);
            $VECTOR = array();
            foreach ($Q as $dato) {

                $analito = ($dato['analito']);
                $cuantificador = $dato['cuantificador'];
                $tipoCroma = $dato['tipoCroma'];
                array_push($VECTOR, $analito, $cuantificador, $tipoCroma);
            }
            return $VECTOR;
        }

//firmantes
        function Firmantes()
        {
            $Variator = new Varios();
            return $Variator->FirmantesGet();
        }

        function matriz()
        {
            $Robot = new Inventario();
        }

        function Firmado($solicitud)
        {
            $Variator = new Varios();
            return $Variator->Firmado($solicitud);
        }

        function ResultadosDeInforme($numInforme)
        {
            $Variator = new Varios();

            return $Variator->ResultadosDeInforme($numInforme);
        }

    }
//
}
?>