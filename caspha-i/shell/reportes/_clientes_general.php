<?php
if (!isset($_GET['lab'])) {
    die('error de parametros');
    exit;
}

function __autoload($_BaseClass)
{
    require_once "../../../melcha-i/{$_BaseClass}.php";
}

if ($_GET['formato'] == '1') {
    header('Content-type: application/x-msdownload');
    header('Content-Disposition: attachment; filename=reportes.xls');
    header('Pragma: no-cache');
    header('Expires: 0');
}

if ($_GET['lab'] == '1') {
    $lab = 'LRE';
} elseif ($_GET['lab'] == '2') {
    $lab = 'LDP';
} else {
    $lab = 'LCC';
}
$Sc = new Sc();
$Robot = new Reportes();
$titulo = "<p>Servicio al cliente: Listado General de {$lab}</p>";
$Robot->TableHeader($titulo, 1);
$ROW = $Sc->clienteGet('', $_GET['lab']);

?>
    <table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
        <tr align="center">
            <td><strong>Identificaci&oacute;n</strong></td>
            <td><strong>Nombre</strong></td>
            <td><strong>Unidad</strong></td>
            <td><strong>Representante</strong></td>
            <td><strong>Tel&eacute;fono.</strong></td>
            <td><strong>Correo</strong></td>
            <td><strong>Fax</strong></td>
            <td><strong>Direcci&oacute;n</strong></td>
            <td><strong>Actividad</strong></td>
            <td><strong>Tipo</strong></td>
            <td><strong>Estado</strong></td>
        </tr>
        <tr>
            <td colspan="7">
                <hr/>
            </td>
        </tr>
        <?php
        for ($x = 0; $x < count($ROW); $x++) {
            ?>
            <tr align="center">
                <td><?= $ROW[$x]['id'] ?></td>
                <td><?= $ROW[$x]['nombre'] ?></td>
                <td><?= $ROW[$x]['unidad'] ?></td>
                <td><?= $ROW[$x]['representante'] ?></td>
                <td><?= $ROW[$x]['tel'] ?></td>
                <td><?= $ROW[$x]['email'] ?></td>
                <td><?= $ROW[$x]['fax'] ?></td>
                <td><?= $ROW[$x]['direccion'] ?></td>
                <td><?= $ROW[$x]['actividad'] ?></td>
                <td><?= $ROW[$x]['tipo'] == '0' ? 'interno' : 'externo' ?></td>
                <td><?= $ROW[$x]['estado'] == '0' ? 'inactivo' : 'activo' ?></td>
            </tr>
        <?php }//FOR  ?>
        <tr align="center">
            <td colspan="5" align="right"><strong>Registros:</strong></td>
            <td><?= $x-- ?></td>
        </tr>
    </table>

<?php
$Robot->TableFooter();

?>