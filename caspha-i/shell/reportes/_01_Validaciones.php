<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['formato'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=reportes.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$Robot = new Reportes();
	$Robot->TableHeader('Reporte de Informes de Validación para Análisis de Multiresiduos de Plaguicidas');	
	?>
	<table class="radius" border="1" width="100%">
	<tr align="center">
		<td rowspan="2"><strong>No. Validaci&oacute;n</strong></td>
		<td rowspan="2"><strong>Analito</strong></td>
		<td colspan="5"><strong>Linealidad</strong><br />Criterio: Residual &le; 20%</td>
		<td rowspan="2"><strong>Selectividad/<br />Especificidad</strong><br>Criterio:<br>&le; 30% del L.C.</td>
		<td colspan="2"><strong>Nivel Bajo</strong></td>
		<td colspan="2"><strong>Nivel Medio</strong></td>
		<td rowspan="2"><strong>Declaraci&oacute;n<br>de conformidad</strong></td>
	</tr>
	<tr align="center">
		<td><strong>P1</strong></td>
		<td><strong>P2</strong></td>
		<td><strong>P3</strong></td>
		<td><strong>P4</strong></td>
		<td><strong>P5</strong></td>
		<td><strong>Exactitud</strong><br>Criterio:<br>En el intervalo<br>70-120%</td>
		<td><strong>Precisi&oacute;n</strong><br>Criterio:<br>RSDr% &le;20%</td>
		<td><strong>Exactitud</strong><br>Criterio:<br>En el intervalo<br>70-120%</td>
		<td><strong>Precisi&oacute;n</strong><br>Criterio:<br>RSDr% &le;20%</td>
	</tr>
	<?php 
	$ROW = $Robot->ValidacionesLRE($_POST['desde'], $_POST['hasta']);
	for($x=0;$x<count($ROW);$x++){
		if($ROW[$x]['cumple']=='1') $ROW[$x]['cumple'] = 'Conforme';
		else $ROW[$x]['cumple'] = 'No conforme';
	?>
		<tr align='center'>
			<td><?=$ROW[$x]['validacion']?></td>
			<td><?=$ROW[$x]['analito']?></td>
			<td><?=$ROW[$x]['P1']?></td>
			<td><?=$ROW[$x]['P2']?></td>
			<td><?=$ROW[$x]['P3']?></td>
			<td><?=$ROW[$x]['P4']?></td>
			<td><?=$ROW[$x]['P5']?></td>
			<td><?=$ROW[$x]['final']?></td>
			
			<td><?=$ROW[$x]['bajo2']?></td>
			<td><?=$ROW[$x]['bajo3']?></td>
			
			<td><?=$ROW[$x]['alto2']?></td>
			<td><?=$ROW[$x]['alto3']?></td>
			<td><?=$ROW[$x]['cumple']?></td>
		</tr>
	<?php
	} echo '</table>';
	
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _01_Validaciones extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			
			/*PERMISOS*/
			//if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('21') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
	}
//
}
?>