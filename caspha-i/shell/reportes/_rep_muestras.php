<?php
/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UNA LISTA
 * ***************************************************************** */
if (isset($_POST['formato'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    if ($_POST['formato'] == '1') {
        header('Content-type: application/x-msdownload');
        header('Content-Disposition: attachment; filename=reportes.xls');
        header('Pragma: no-cache');
        header('Expires: 0');
    }

    $Robot = new Reportes();
    $Robot->TableHeader("Muestras Reportadas<br>{$_POST['desde']}-{$_POST['hasta']}");
    ?>
    <table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
        <tr align="center">
            <td><strong>Cod. Int.</strong></td>
            <td><strong>Cod. Ext.</strong></td>
            <td><strong>Solicitud</strong></td>
            <td><strong># de Informe</strong></td>
            <td><strong>Tipo</strong></td>
            <td><strong>Fecha</strong></td>
            <td><strong>Cliente</strong></td>
            <td><strong>Estado</strong></td>
        </tr>
        <tr><td colspan="8"><hr /></td></tr>
        <?php
        $ROW = $Robot->MuestrasLab($_POST['LAB'], $_POST['cliente'], $_POST['desde'], $_POST['hasta'], isset($_POST['tipo']) ? $_POST['tipo'] : '');        
        for ($x = 0, $total = 0; $x < count($ROW); $x++) {            
            if ($_POST['LAB'] == 3) {
                $ROW[$x]['tipo'] = $ROW[$x]['tipo'] == '2' ? 'Plaguicidas' : 'Fertilizantes';
            } else {
                $ROW[$x]['tipo'] = '';
            }
            ?>
            <tr align="center">
                <td><?= $ROW[$x]['ref'] ?></td>
                <td><?= $ROW[$x]['externo'] ?></td>
                <td><?= $ROW[$x]['solicitud'] ?></td>
                <td><?= $ROW[$x]['informe'] ?></td>
                <td><?= $ROW[$x]['tipo'] ?></td>
                <td><?= $ROW[$x]['fecha1'] ?></td>
                <td><?= $ROW[$x]['nombre'] ?></td>
                <td><?= $Robot->MuestrasEstado($ROW[$x]['estado']) ?></td>

            </tr>
            <?php
        }
        echo "<tr><td colspan='8'><hr></td></tr><tr align='center'>
		<td colspan='5'>
		<td><strong>Total:</strong></td>
		<td><strong>{$x}</strong></td>
	</tr></table>";

        $Robot->TableFooter();
    } else {
        /*         * *****************************************************************
          DECLARACION CLASE GESTORA
         * ***************************************************************** */

        function __autoload($_BaseClass) {
            require_once "../../melcha-i/{$_BaseClass}.php";
        }

        final class _rep_muestras extends Mensajero {

            private $_ROW = array();
            private $Securitor = '';

            function __construct() {
                $this->Securitor = new Seguridad();
                if (!$this->Securitor->SesionAuth())
                    $this->Err();
                $this->_ROW = $this->Securitor->SesionGet();

                /* PERMISOS */
                //if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('21') );
                /* PERMISOS */
                $_POST['LAB'] = $this->_ROW['LID'];
            }

            function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        }

//
    }
    ?>