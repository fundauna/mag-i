<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['id']) ) die('-1');
	
	$ruta = "../../docs/reportes/{$_POST['id']}.zip";
	Bibliotecario::ZipEliminar($ruta);
	die('1');
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _final_historial extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('85') );
			/*PERMISOS*/
			
			$_POST['LID'] = $this->_ROW['LID'];
			//
			if(!isset($_POST['tipo'])){
				$_POST['tipo'] = $_POST['valor'] = $_POST['desde'] = $_POST['hasta'] = $_POST['estado'] = '';
				$this->inicio = true;
			}
			//SI VENIMOS DESDE TABLON, ELIMINO LA NOTIFICACION
			if( isset($_GET['ver']) && isset($_GET['tablon']) )$this->Securitor->TablonDel($_GET['tablon']);
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function Historial(){
			$Robot = new Reportes();
			if($this->inicio) return $Robot->InformesPendientesGet($this->_ROW['LID']);
			return $Robot->InformesHistorialGet($this->_ROW['LID'], $_POST['tipo'], $_POST['valor'], $_POST['desde'], $_POST['hasta'], $_POST['estado']);
		}
		
		function Estado($_var){
			$Robot = new Reportes();
			return $Robot->InformeEstado($_var);
		}
	}
//
}
?>