<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['tipo'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=perfiles.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$Robot = new Reportes();
	$Robot->TableHeader('Cat�logo de proveedores');
	$ROW = $Robot->ProveedoresLab($_POST['lab'], $_POST['tipo'], $_POST['clasificacion']);
	?>
	<table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
	<tr>
		<td><strong>Tipo</strong></td>
		<td><strong>Clasificaci&oacute;n</strong></td>
		<td><strong>Identificaci&oacute;n</strong></td>
		<td><strong>Nombre</strong></td>
		<td><strong>Tel.</strong></td>
		<td><strong>Fax</strong></td>
		<td><strong>Email</strong></td>
		<td><strong>Direcci&oacute;n</strong></td>
	</tr>
	<tr><td colspan="8"><hr /></td></tr>
	<?php
	for($x=0;$x<count($ROW);$x++){
	?>
	<tr>
		<td><?=$ROW[$x]['tipo']=='0' ? 'F�sico' : 'Jur�dico'?></td>
		<td><?=$ROW[$x]['clasificacion']?></td>
		<td><?=$ROW[$x]['id']?></td>
		<td><?=$ROW[$x]['nombre']?></td>
		<td><?=$ROW[$x]['telefono']?></td>
		<td><?=$ROW[$x]['fax']?></td>
		<td><?=$ROW[$x]['correo']?></td>
		<td><?=$ROW[$x]['direccion']?></td>
	</tr>
	<?php
	}
	echo '</table>';
	
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _rep_proveedores extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			
			$_POST['LID'] = $this->_ROW['LID'];
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ProveedoresFiltro(){
			$Provitor = new Proveedores();
			return $Provitor->ClasificacionesMuestra($this->_ROW['LID']);
		}
	}
}
?>