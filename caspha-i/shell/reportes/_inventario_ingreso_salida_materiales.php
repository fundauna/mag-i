<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['formato'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=reportes.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$Robot = new Reportes();
	$titulo = "Inventario: Control Ingreso y Salida de Materiales";
	$Robot->TableHeader($titulo);	
	?>
	<table class='radius' align='center' width='100%' border="1">
	<tr align="center">
		<td><strong>Tipo</strong></td>
        <td><strong>C&oacute;digo material</strong></td>
        <td><strong>Descripci&oacute;n</strong></td>
        <td><strong>Orden de compra</strong></td>
        <td><strong>Factura</strong></td>
        <td><strong>Proveedor</strong></td>
        <td><strong>Costo</strong></td>
        <td><strong>Fecha de ingreso BI</strong></td>
        <td><strong>Fecha de ingreso LCC</strong></td>
        <td><strong>Fecha de salida</strong></td>
        <td><strong>Cantidad</strong></td>
        <td><strong>Unidades</strong></td>
        <td><strong>Observaciones</strong></td>
	</tr>
	<?php
    $Robo = new Inventario();
	$ROW = $Robo->IngresoSalida();
	for($x=0;$x<count($ROW);$x++){
	?>
	<tr align="center">
		<td><?=$ROW[0]['tipo'] == '1' ? 'Entrada' : 'Salida'?></td>
        <td><?=$ROW[$x]['codigo']?></td>
        <td><?=$ROW[$x]['descripcion']?></td>
        <td><?=$ROW[$x]['orden']?></td>
        <td><?=$ROW[$x]['factura']?></td>
        <td><?=$ROW[$x]['proveedor']?></td>
        <td><?=$ROW[$x]['costo']?></td>
        <td><?=$ROW[$x]['ingresobi']?></td>
        <td><?=$ROW[$x]['ingresolcc']?></td>
        <td><?=$ROW[$x]['salida']?></td>
        <td><?=$ROW[$x]['cantidad']?></td>
        <td><?=$ROW[$x]['unidad']?></td>
        <td><?=$ROW[$x]['obs']?></td>
	</tr>
	<?php } ?>
	<tr align="center">
		<td align="right" colspan="12" ><strong>Registros:</strong></td>
		<td align="center"><?=$x--?></td>
	</tr>
	</table>
	<?php
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	class _inventario_vencimientos extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('82') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function Lab(){
			return $this->_ROW['LID'];
		}
	}
}
?>