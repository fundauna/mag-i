<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	$Listator->ClientesLista('reportes', $ROW['LID'], basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
}elseif( isset($_POST['formato'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=reportes.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	if(!isset($_POST['tipo'])) $_POST['tipo'] = '';

	$Robot = new Reportes();
	$titulo = 'Solicitudes por cliente';
	$Robot->TableHeader($titulo);	
	?>
	<table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
	<tr align="center">
		<td><strong>Solicitud</strong></td>
		<td><strong>Fecha</strong></td>
		<td><strong>Cliente</strong></td>
		<td><strong>Tipo</strong></td>
		<td><strong>Estado</strong></td>
	</tr>
	<tr><td colspan="5"><hr /></td></tr>
	<?php
	$ROW = $Robot->SolicitudesXcliente($_POST['LAB'], $_POST['cliente'], $_POST['desde'], $_POST['hasta'], $_POST['tipo'], $_POST['estado']);
        $con = 0;
	for($x=0, $total=0;$x<count($ROW);$x++){ $con++;
	?>
	<tr align="center">
		<td><?=$ROW[$x]['cs']?></td>
		<td><?=$ROW[$x]['fecha']?></td>
		<td><?=$ROW[$x]['cliente']?></td>
		<td><?=$Robot->SolicitudTipo($ROW[$x]['tipo'])?></td>
		<td><?=$Robot->SolicitudEstado($ROW[$x]['estado'])?></td>
	</tr>
	<?php } ?>
        <tr align="right"><td colspan="5"><hr><b>Total:</b> <?=$con?></td></tr>
	</table>
	<?php
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _solicitudesXcliente extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			
			/*PERMISOS*/
			//if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('21') );
			/*PERMISOS*/			
			$_POST['LAB'] = $this->_ROW['LID'];
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
	}
//
}
?>