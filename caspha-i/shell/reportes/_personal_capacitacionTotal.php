<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['_TIPO'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=reportes.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$_POST['desde'] = substr($_POST['desde'], 6, 4);

	$Robot = new Reportes();
	if($_POST['_TIPO']==0){
		$titulo = "Comparaci�n de las horas de capacitaci�n total recibidas por cada funcionario por a�o";
		$Robot->TableHeader($titulo);
		$Robot->TablaCapacitacionesSet($_POST['unidad'], $_POST['desde'], $_POST['anos']);
	}else{
		if($_POST['tipo'] == 'S') $label = 'SGC';
		elseif($_POST['tipo'] == 'T') $label = 'T�cnica';
		elseif($_POST['tipo'] == 'O') $label = 'Otras';
		elseif($_POST['tipo'] == 'I') $label = 'Internacional';
		$titulo = "Comparaci�n de las horas de capacitaci�n {$label} recibida por cada funcionario por a�o";
		$Robot->TableHeader($titulo);
		$Robot->TablaCapacitacionesSet($_POST['unidad'], $_POST['desde'], $_POST['anos'], $_POST['tipo']);
	}
	
	$ROW = $Robot->TablaCapacitacionesGet();
	?>
	<table class='radius' align='center' width='100%' border="1">
	<tr align="center">
		<td rowspan="2" width="25%"><strong>Funcionario</strong></td>
		<td colspan="<?=$_POST['anos']+1?>"><strong><?=$titulo?></strong></td>
		<td rowspan="2" width="25%"><strong>A&ntilde;o de ingreso al SFE</strong></td>
	</tr>
	<tr align="center">
		<td><strong><?=$_POST['desde']?></strong></td>
		<?php if($_POST['anos']>0){ ?><td><strong><?=(date('Y')-1)?></strong></td><?php } ?>
		<?php if($_POST['anos']>1){ ?><td><strong><?=(date('Y')-2)?></strong></td><?php } ?>
		<?php if($_POST['anos']>2){ ?><td><strong><?=(date('Y')-3)?></strong></td><?php } ?>
		<?php if($_POST['anos']>3){ ?><td><strong><?=(date('Y')-4)?></strong></td><?php } ?>
	</tr>
	<?php
	$totA = $tot1 = $tot2 = $tot3 = $tot4 = 0;
	for($x=0;$x<count($ROW);$x++){
		$totA += $ROW[$x]['actual'];
		$tot1 += $ROW[$x]['menos1'];
		$tot2 += $ROW[$x]['menos2'];
		$tot3 += $ROW[$x]['menos3'];
		$tot4 += $ROW[$x]['menos4'];
	?>
	<tr align="center">
		<td><?=$ROW[$x]['nombre']?></td>
		<td><?=$ROW[$x]['actual']?></td>
		<?php 
		if($_POST['anos']>0){ echo "<td>{$ROW[$x]['menos1']}</td>"; }
		if($_POST['anos']>1){ echo "<td>{$ROW[$x]['menos2']}</td>"; }
		if($_POST['anos']>2){ echo "<td>{$ROW[$x]['menos3']}</td>"; }
		if($_POST['anos']>3){ echo "<td>{$ROW[$x]['menos4']}</td>"; }
		?>
		<td><?=$ROW[$x]['ingreso']?></td>
	</tr>
	<?php } ?>
	<tr align="center">
		<td><strong>Total en horas</strong></td>
		<td><?=$totA?></td>
		<?php 
		if($_POST['anos']>0){ echo "<td>{$tot1}</td>"; }
		if($_POST['anos']>1){ echo "<td>{$tot2}</td>"; }
		if($_POST['anos']>2){ echo "<td>{$tot3}</td>"; }
		if($_POST['anos']>3){ echo "<td>{$tot4}</td>"; }
		?>
		<td></td>
	</tr>
	</table>
	<br />
	<table>
	<tr>
		<td></td>
		<td><strong><?=$_POST['desde']?></strong></td>
		<td><strong><?=(date('Y')-1)?></strong></td>
		<td><strong><?=(date('Y')-2)?></strong></td>
		<td><strong><?=(date('Y')-3)?></strong></td>
		<td><strong><?=(date('Y')-4)?></strong></td>
	</tr>
	<?php
	for($x=0;$x<count($ROW);$x++){
	?>
	<tr align="center">
		<td><?=$ROW[$x]['nombre']?></td>
		<td><?=$ROW[$x]['actual']?></td>
		<td><?=$ROW[$x]['menos1']?></td>
		<td><?=$ROW[$x]['menos2']?></td>
		<td><?=$ROW[$x]['menos3']?></td>
		<td><?=$ROW[$x]['menos4']?></td>
	</tr>
	<?php
	}
	echo '</table>';
	
	$Robot->TableFooter();
	$Robot->TablaCapacitacionesDel();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	class _personal_capacitacionTotal extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('81') );
			/*PERMISOS*/
			
			if(!isset($_GET['tipo'])) die('Error de par�metros');
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
	}
}
?>