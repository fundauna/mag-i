<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['formato'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=reportes.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$Robot = new Reportes();
	if($_POST['tipo']=='0'){
		$txt = 'Analitos por matriz';
		$col1 = 'Matriz';
		$col2 = 'Analito';
	}else{
		$txt = 'Matrices por analito';
		$col1 = 'Analito';
		$col2 = 'Matriz';
	}
	$Robot->TableHeader($txt);	
	?>
	<script>
	function lolo(btn, _linea){
		if(btn.title=='1'){
			btn.title = '0';
			document.getElementById('body'+_linea).style.display = 'none';
		}else{
			btn.title = '1';
			document.getElementById('body'+_linea).style.display = '';
		}
	}
	</script>
	<table class='radius' align='center' width='100%' border="1">
	<tbody id="body">
	<?php
	$ROW = $Robot->MatrizXanalito($_POST['tipo'], $_POST['desde'], $_POST['hasta']);
	for($x=0,$actual='',$body=0;$x<count($ROW);$x++){
		if($actual != $ROW[$x]['campoA']){
			echo "</tbody>
			<tr>
				<td colspan='3'><input type='button' style='height:17px; width:15px' value='&plusmn;' title='0' onclick='lolo(this, {$body})' />&nbsp;<strong>{$col1}: {$ROW[$x]['campoA']}</strong></td>
			</tr>
			<tbody id='body{$body}' style='display:none'>
			<tr align='center'>
				<td><strong>{$col2}</strong></td>
				<td><strong>Muestra</strong></td>
				<td><strong>Concentración</strong></td>
			</tr>";
			$body++;
			$actual = $ROW[$x]['campoA'];
		}
		
		if($ROW[$x]['conc']=='-1') $ROW[$x]['conc'] = 'ND';
		elseif($ROW[$x]['conc']=='-2') $ROW[$x]['conc'] = 'NC';
	?>
	<tr align="center">
		<td><?=$ROW[$x]['campoB']?></td>
		<td><?=$ROW[$x]['ref']?></td>
		<td><?=$ROW[$x]['conc']?></td>
	</tr>
	<?php } ?>
	</tbody>
	</table>
	<?php
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _01_MatrizAnalito extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			
			/*PERMISOS*/
			//if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('21') );
			/*PERMISOS*/
		}
	}
//
}
?>