<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot = new Reportes();
        if ($_POST['_AJAX'] == '1') {
	if($_POST['estado'] == '2' or $_POST['estado'] == '5'){
		echo $Robot->InformeModificaEstado($_POST['id'], $_POST['obs'], $_ROW['UID'], $_POST['estado'], $_POST['cliente']);
	}elseif($_POST['estado'] == '4'){
		echo $Robot->InformeFinalElimina($_POST['id'],$_ROW['UID']);
	}
        }else{
             echo  $Robot->Informe03MetodosFertilizantesIME($_POST['id'], $_POST['metodos'], $_POST['metodosotros'],$_ROW['UID']);
        }
        exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _final03_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['ID']) ) die('Error de par�metros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('85') );
			/*PERMISOS*/
		}

        function MEncabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function GetEncabezado(){
			$Robot = new Reportes();
			return $Robot->Informe03EncabezadoGet($_GET['ID'], $this->_ROW['LID']);
		}
		
		function Metodos(){
			$Robot = new Reportes();
			return $Robot->Informe03MetodosFertilizantesGet($_GET['ID']);
		}
                
                function MetodosTodos(){
			$Robot = new Reportes();
			return $Robot->Informe03MetodosFertilizantesGetTodos();
		}
                
                function MetodoOtro($_informe,$_linea){
                    $Robot = new Reportes();
                    return $Robot->Informe03MetodosOtrosGet($_informe, $_linea);
                }
		
		function Observaciones(){
			$Robot = new Reportes();
			return $Robot->Informe03ObservacionesGet($_GET['ID']);
		}
		
		function Equipos(){
			$Robot = new Reportes();
			return $Robot->Informe03EquiposGet($_GET['ID']);
		}
		
		function Estado($_var){
			$Robot = new Reportes();
			return $Robot->InformeEstado($_var);
		}
	
		function Resultados($_muestra, $_clasificacion){
			$Robot = new Reportes();
			return $Robot->Informe03DetalleGet($_GET['ID'], $_muestra, $_clasificacion);
		}
		
		function Metodo(){
			$Robot = new Reportes();
			return $Robot->Informe03DetalleGetM($_GET['ID']);
		}
	
		function Accion($_var){
			$Robot = new Reportes();
			return $Robot->InformeAccion($_var);
		}
	
		function Historial(){
			$Robot = new Reportes();
			return $Robot->InformeHistorial($_GET['ID']);
		}
		
		function Aprobar(){
			if($this->_ROW['ROL'] == '0') return true;
			elseif($this->Securitor->UsuarioPermiso('87')=='E') return true;
			else return false;
		}
		
		function Unidad($_var){
			if($_var=='0') return '%m/m';
			elseif($_var=='1'){
				$_POST['densidad']=1;
				return '%m/v';
			}elseif($_var=='2') return 'ppm';
			elseif($_var=='3') return 'mg/kg';
		}
		
		function Dependencia($_var){
			if($_var=='0'){
				$_POST['tipo'] = 2;
				return 'Venta de servicios';
			}elseif($_var=='1'){
				$_POST['tipo'] = 1;
				return 'Fiscalizaci�n';
			}elseif($_var=='2'){
				$_POST['tipo'] = 1;
				return 'Registro';
			}
		}
		
		function Recipiente($_var){
			if($_var=='0') return 'Bolsa metalizada';
			elseif($_var=='1') return 'Bolsa pl�stica';
			elseif($_var=='2') return 'Frasco pl�stico';
			elseif($_var=='3') return 'Frasco de vidrio';
		}
		
		function Formulacion($_var){
			if($_var=='A') return 'Ingrediente activo grado t�cnico';
			elseif($_var=='B') return 'Concentrado dispersable';
			elseif($_var=='C') return 'Granulado';
			elseif($_var=='D') return 'Polvo soluble';
			elseif($_var=='E') return 'Concentrado emulsificable';
			elseif($_var=='F') return 'Gr�nulo dispersable';
			elseif($_var=='G') return 'Polvo dispersable';
			elseif($_var=='H') return 'Concentrado soluble';
			elseif($_var=='I') return 'Suspensi�n concentrada';
			elseif($_var=='J') return 'Polvo mojable';
			elseif($_var=='L') return 'Suspo-emulsi�n acuosa';
			elseif($_var=='M') return 'Gr�nulos solubles en agua';
			elseif($_var=='N') return 'Emulsiones de aceite en agua';
			elseif($_var=='O') return 'Polvos solubles para tratamiento de semillas';
			elseif($_var=='P') return 'L�quidos miscibles en aceite';
			elseif($_var=='K' or $_var=='3') return 'Otro';
			elseif($_var=='0') return 'L�quida';
			elseif($_var=='1') return 'Granulado';
			elseif($_var=='2') return 'Polvo';
			elseif($_var=='4') return 'Suspensi�n';
		}
		
		function SolicitudMuestras($_solicitud){
			$Robot = new Servicios();
			return $Robot->Solicitud03MuestrasGet($_solicitud);
		}
	}
//
}
?>