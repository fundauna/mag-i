<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['formato'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=reportes.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$Robot = new Reportes();
	$titulo = 'Equipos: Proveedores';
	$Robot->TableHeader($titulo);	
	?>
	<table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
	<tr align="center" bgcolor="#CCCCCC">
    	<td colspan="4">Informaci&oacute;n de equipo</td>
        <td colspan="5">Informaci&oacute;n del proveedor</td>
    </tr>
    <tr align="center">
		<td><strong>C&oacute;digo</strong></td>
		<td><strong>Nombre</strong></td>
		<td><strong>Marca</strong></td>
		<td><strong>Serie</strong></td>
		<td><strong>Id.</strong></td>
		<td><strong>Nombre</strong></td>
		<td><strong>Tel&eacute;fono</strong></td>
        <td><strong>Fax</strong></td>
        <td><strong>Email</strong></td>
	</tr>
	<tr><td colspan="9"><hr /></td></tr>
	<?php
	$ROW = $Robot->EquiposProveedoresGet($_POST['codigo'], $_POST['nombre'], $_POST['proveedor']);
	for($x=0;$x<count($ROW);$x++){
	?>
	<tr align="center">
		<td><?=$ROW[$x]['codigo']?></td>
		<td><?=$ROW[$x]['nombre']?></td>
		<td><?=$ROW[$x]['marca']?></td>
		<td><?=$ROW[$x]['serie']?></td>
		<td><?=$ROW[$x]['id']?></td>
		<td><?=$ROW[$x]['proveedor']?></td>
		<td><?=$ROW[$x]['telefono']?></td>
        <td><?=$ROW[$x]['fax']?></td>
		<td><?=$ROW[$x]['correo']?></td>
	</tr>
	<?php } ?>
	</table>
	<?php
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	class _equipos_proveedores extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('83') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
	}
}
?>