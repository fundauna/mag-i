<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['cs']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot = new Biologia();
	echo $Robot->AliIME('D', $_POST['cs'], $_tipo, $_nombre, $_estado, $_observaciones, $_ROW['UID']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _alicuotas extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('A7') );
			/*PERMISOS*/
			if($this->_ROW['LID'] != '2') die('Secci�n bloqueada para este laboratorio');
			
			if(!isset($_POST['codigo'])){
				$_POST['desde'] = $_POST['hasta'] = date('d-m-Y');				
				$_POST['codigo'] = '';
				$this->inicio = true;
			}
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ResMuestra(){
			if($this->inicio) return array();
			else{
				$Robot = new Biologia();
				return $Robot->AliResumenGet($_POST['desde'], $_POST['hasta'], $_POST['codigo']);		
			}
		}
		
		function Tipo($_var){
			$Robot = new Biologia();
			return $Robot->AliTipo($_var);
		}
	}
}
?>