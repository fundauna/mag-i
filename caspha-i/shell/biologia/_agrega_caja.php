<?php

/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 * ***************************************************************** */
if (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth())
        die('-0');
    $ROW = $Securitor->SesionGet();

    $Robot = new Biologia();
    $_POST['id'] = $Robot->consecutivo('cajas');

    if( $Robot->caja_ime("I", $_POST['id'], $_POST['id_camara'], $_POST['bandeja_linea'], $_POST['nombre'], $_POST['ubicacion'])){
        echo  $_POST['id'];
    }


    exit;
} else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _agrega_caja extends Mensajero {

        private $_ROW = array();
        private $Securitor = '';
        private $inicio = false;
        private $biologia;

        function __construct() {
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth())
                $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0')
                $this->Portero($this->Securitor->UsuarioPermiso('A7'));
            /* PERMISOS */
            if ($this->_ROW['LID'] != '2')
                die('Secci�n bloqueada para este laboratorio');

            if (!isset($_POST['codigo'])) {
                $_POST['desde'] = $_POST['hasta'] = date('d/m/Y');
                $_POST['codigo'] = '';
                $this->inicio = true;
            }
            $this->biologia = new Biologia();
        }

    }

}
?>