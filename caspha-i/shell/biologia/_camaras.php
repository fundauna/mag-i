<?php
/*******************************************************************
 * PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 *******************************************************************/
if (isset($_POST['_AJAX'])) {
    function __autoload($_BaseClass)
    {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth()) die('-0');
    if (!isset($_POST['perfil'])) die('-1');

    $Robot = new Biologia();

    if (isset($_POST['PermisosBuscar'])) {
        $str = '<select id="asignados" style="width:340px;" size="10">';
        $ROW = $Robot->CamarasResumenGet($_POST['perfil']);
        for ($x = 0; $x < count($ROW); $x++) {
            $str .= "<option value='{$ROW[$x]['bandeja']}'>{$ROW[$x]['bandeja']}</option>";
        }
        $str .= '</select>';
        die($str);
    } elseif (isset($_POST['PermisosAgrega'])) {
        echo $Robot->CamarasIME($_POST['perfil'], $_POST['permiso'], 'I');
        exit;
    } elseif (isset($_POST['PermisosElimina'])) {
        echo $Robot->CamarasIME($_POST['perfil'], $_POST['permiso'], 'D');
        exit;
    } elseif (isset($_POST['AgregarCajas'])) {
        echo $Robot->CajasIME($_POST['id']);
        exit;
    }
} else {
    /*******************************************************************
     * DECLARACION CLASE GESTORA
     *******************************************************************/
    function __autoload($_BaseClass)
    {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _camaras extends Mensajero
    {
        private $_ROW = array();
        private $Securitor = '';

        function __construct()
        {
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth()) $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /*PERMISOS*/
            if ($this->_ROW['ROL'] != '0') $this->Portero($this->Securitor->UsuarioPermiso('A7'));
            /*PERMISOS*/
            if ($this->_ROW['LID'] != '2') die('Secci�n bloqueada para este laboratorio');
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function Tipos()
        {
            $Robot = new Biologia();
            return $Robot->BandejasResumenGet($this->_ROW['LID']);
        }

        function Campos()
        {
            $Robot = new Biologia();
            return $Robot->BandejasResumenGet($this->_ROW['LID']);
        }

        function Cajas()
        {
            $Robot = new Biologia();
            return $Robot->CajasResumenGet($this->_ROW['LID']);
        }
    }
}
?>