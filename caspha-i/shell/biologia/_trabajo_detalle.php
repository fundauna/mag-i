<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list0'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Listator = new Listador();
	$Listator->ProcedimientosLista('biologia', basename(__FILE__));
}elseif( isset($_GET['list1'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Listator = new Listador();
	$Listator->MacroAnalisisLista('biologia', 2, $_GET['tipo'], $_GET['list1'], basename(__FILE__));
	exit;
}elseif( isset($_GET['list2'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Listator = new Listador();
	$Listator->EquiposLista('biologia', $_GET['list2'], 2, basename(__FILE__));
	exit;
}elseif( isset($_GET['list3'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Listator = new Listador();
	$Listator->InventarioEntradas('biologia', $_GET['list3'], $_GET['padre'], 2, basename(__FILE__));
	exit;
}elseif( isset($_GET['list4'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Listator = new Listador();
	$Listator->AlicuotasLista('biologia', $_GET['list4'], 2, basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
}elseif( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['cs']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot = new Biologia();

	if($_POST['paso'] == 'A'){
		echo $Robot->TrabajoPasoASet($_POST['accion'],
			$_POST['cs'],
			$_POST['procedimiento'],
			$_POST['obsA'],
			$_POST['A1cod_analisis'],
			$_POST['A2cod_analisis'],
			$_POST['A3cod_analisis'],
			$_POST['muestras'],
			$_ROW['UID']);
		exit;
	}elseif($_POST['paso'] == 'B'){
		echo $Robot->TrabajoPasoBSet($_POST['cs'],
			$_POST['fechaB'],
			$_POST['tempB'],
			$_POST['humedadB'],
			$_POST['obsB'],
			$_ROW['UID']);
		exit;
	}elseif($_POST['paso'] == 'W'){
		echo $Robot->TrabajoPasoWSet($_POST['cs'],
			$_POST['tiempoW'],
			$_POST['tempW'],
			$_POST['W2cod_equipos'],
			$_POST['obsW'],
			$_ROW['UID']);
		exit;
	}elseif($_POST['paso'] == 'C'){
		echo $Robot->TrabajoPasoCSet($_POST['cs'],
			$_POST['tempC'],
			$_POST['humedadC'],
			$_POST['obsC'],
			$_POST['C1cod_insumos'],
			$_POST['C1total'],
			$_POST['C2cod_equipos'],
			$_POST['C3cod_insumos'],
			$_ROW['UID']);
		exit;
		
	}elseif($_POST['paso'] == 'D'){
		echo $Robot->TrabajoPasoDSet($_POST['cs'],
			$_POST['tempD'],
			$_POST['humedadD'],
			$_POST['normalizada'],
			$_POST['analito'],
			$_POST['unidad'],
			$_POST['obsD'],
			$_POST['Dmuestra'],
			$_POST['CAN'],
			$_POST['coef1'],
			$_POST['coef2'],
			$_POST['vol1'],
			$_POST['D1cod_insumos'],
			$_POST['D1total'],
			$_POST['D2cod_equipos'],
			$_ROW['UID']);
	}elseif($_POST['paso'] == 'E'){
		echo $Robot->TrabajoPasoESet($_POST['cs'],
			$_POST['fechaE'],
			$_POST['tempE'],
			$_POST['humedadE'],
			$_POST['reacciones'],
			$_POST['programaE'],
			$_POST['nombreE'],
			$_POST['obsE'],
			$_POST['E1tipo'],
			$_POST['E1descr'],
			$_POST['E1insumos'],
			$_POST['E1concS'],
			$_POST['E1concF'],
			$_POST['E1volumen'],
			$_POST['E1reacciones'],
			$_POST['E2muestras'],
			$_POST['E2cod_equipos'],
			$_POST['control'],
			$_ROW['UID']);
		exit;
	}elseif($_POST['paso'] == 'X'){
		echo $Robot->TrabajoPasoXSet($_POST['cs'],
			$_POST['tempX'],
			$_POST['humedadX'],
			$_POST['tiempoX'],
			$_POST['voltajeX'],
			$_POST['X1cod_insumos'],
			$_POST['X1total'],
			$_POST['X2cod_equipos'],
			$_POST['obsX'],
			$_ROW['UID']);
		exit;
	}elseif($_POST['paso'] == 'F'){
		echo $Robot->TrabajoPasoFSet($_POST['cs'],
			$_POST['reaccionesF'],
			$_POST['Famplicon1'],
			$_POST['Famplicon2'],
			$_POST['programaF'],
			$_POST['obsF'],
			$_POST['F1tipo'],
			$_POST['F1descr'],
			$_POST['F1insumos'],
			$_POST['F1volumen'],
			$_POST['F1reacciones'],
			$_POST['F2cod_equipos'],
			$_ROW['UID']);
		exit;
	}elseif($_POST['paso'] == 'G'){
		echo $Robot->TrabajoPasoGSet($_POST['cs'],
			$_POST['tempG'],
			$_POST['humedadG'],
			$_POST['tiempoG'],
			$_POST['voltajeG'],
			$_POST['obsG'],
			$_POST['G1cod_insumos'],
			$_POST['G1total'],
			$_POST['G2cod_equipos'],
			$_ROW['UID']);
		exit;
	}elseif($_POST['paso'] == 'R'){
		if( !isset($_POST['incluir']) ) $_POST['incluir'] = '';
		if( !isset($_POST['control']) ) $_POST['control'] = '';
		if( !isset($_POST['cumple']) ) $_POST['cumple'] = '';
		if($Robot->TrabajoPasoRSet($_POST['cs'], $_POST['obsR'], $_POST['control'], $_POST['cumple'], $_POST['muestra'], $_POST['analisis'], $_POST['result'], $_POST['incluir'], $_ROW['UID']) ){
			
			$ruta = "../../docs/biologia/resultados/{$_POST['cs']}.zip";
			if($_FILES['archivo']['size'] > 0){
				$nombre = $_FILES['archivo']['name'];
				$file = $_FILES['archivo']['tmp_name'];	
				Bibliotecario::ZipCrear($nombre, $file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");	
			}else{
				Bibliotecario::ZipEliminar($ruta);
			}
		
			die("<script>alert('Transacción finalizada');window.close();</script>");
		}else{
			die('Error al ingresar los resultados');
		}
	}
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UNA IMPORTACION
*******************************************************************/
}elseif( isset($_POST['_IMPORT'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Robot = new Biologia();
			
	if($_POST['paso'] == '0'){ //ARCHIVO CUANTIFICACION
		$ruta = "../../docs/biologia/cuantificacion/{$_POST['cs']}.zip";
		
		if($_FILES['cuantificacion']['size'] > 0){	
			$nombre = str_replace(' ', '', $_FILES['cuantificacion']['name']);
			$file = $_FILES['cuantificacion']['tmp_name'];
			if($_FILES['cuantificacion']['type'] != 'application/x-msdownload') die('Error: El archivo debe ser tipo .xls (Excel 2003)');
			
			Bibliotecario::ZipCrear($nombre, $file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");

			require_once '../metodos/Excel/reader.php';
			$data = new Spreadsheet_Excel_Reader();
			$data->read($file);
			
			$Robot->TrabajoTemporalDel($_POST['cs']);
			
			for($i=2;$i<=$data->sheets[0]['numRows'];$i++){
				if( isset($data->sheets[0]['cells'][$i][1]) && str_replace('', '', $data->sheets[0]['cells'][$i][1]) != ''){
					$Robot->TrabajoTemporalSet($_POST['cs'], 
						$data->sheets[0]['cells'][$i][2], 
						$data->sheets[0]['cells'][$i][5], 
						$data->sheets[0]['cells'][$i][9],
						$data->sheets[0]['cells'][$i][10]
					);
				}
			}
			Bibliotecario::ZipEliminar($file);	//elimina el temporal
			
		}else Bibliotecario::ZipEliminar($ruta);
		
		die("<script>alert('Transacción finalizada');window.close();</script>");
	}elseif($_POST['paso'] == '1' or $_POST['paso'] == '2' or $_POST['paso'] == '3'){ //ANALISIS
		if($_POST['paso'] == '1') $tipo = 5;
		elseif($_POST['paso'] == '2') $tipo = 7;
		elseif($_POST['paso'] == '3') $tipo = 6;
		
		$ROW = $Robot->ProcAnalisisGet($_POST['procedimiento'], $_POST['paso']);
		for($i=0,$str='';$i<count($ROW);$i++){
			$ROW[$i]['nombre'] = utf8_encode($ROW[$i]['nombre']);
			$str.= "<tr>
				<td>".($i+1)."</td>
				<td><input type='text' id='A{$_POST['paso']}analisis{$i}' class='lista2' readonly onclick='MacroAnalisisLista({$tipo}, \"A{$_POST['paso']}\" , {$i})' value='{$ROW[$i]['nombre']}' /><input type='hidden' id='A{$_POST['paso']}cod_analisis{$i}' name='A{$_POST['paso']}cod_analisis' value='{$ROW[$i]['id']}' /></td>
			</tr>";
		}
		echo $str;
		exit;
	}elseif($_POST['paso'] == '4'){ //INSUMOS
	
		$ROW = $Robot->ProcInsumosImporta($_POST['procedimiento'], $_POST['letra']);
		
		for($i=0,$str='';$i<count($ROW);$i++){
			$ROW[$i]['nombre'] = utf8_encode($ROW[$i]['nombre']);
			$str.= "<tr align='center'>
				<td>".($i+1)."&nbsp;&nbsp;<input type='text' id='{$_POST['letra']}1insumos{$i}' class='lista' readonly onclick='InsumosLista(\"{$_POST['letra']}1\", {$i}, this)' padre='{$ROW[$i]['padre']}' /><input type='hidden' id='{$_POST['letra']}1cod_insumos{$i}' name='{$_POST['letra']}1cod_insumos' value='{$ROW[$i]['id']}' /></td>
				<td>...{$ROW[$i]['nombre']}</td>
				<td><input type='text' id='{$_POST['letra']}1total{$i}' name='{$_POST['letra']}1total' class='monto2' value='{$ROW[$i]['cantidad']}' onblur='_FLOAT(this)' /></td>	
			</tr>";
		}
		echo $str;
		exit;
	}elseif($_POST['paso'] == '5'){ //EQUIPOS
		$ROW = $Robot->ProcEquiposGet($_POST['procedimiento'], $_POST['letra']);
		
		for($i=0,$str='';$i<count($ROW);$i++){
			$ROW[$i]['nombre'] = utf8_encode($ROW[$i]['nombre']);
			$str.= "<tr>
				<td>".($i+1)."</td>	
				<td>({$ROW[$i]['codigo']}) <input type='text' id='{$_POST['letra']}2equipos{$i}' class='lista2' readonly onclick='EquiposLista(\"{$_POST['letra']}2\", {$i})' value='{$ROW[$i]['nombre']}' /><input type='hidden' id='{$_POST['letra']}2cod_equipos{$i}' name='{$_POST['letra']}2cod_equipos' value='{$ROW[$i]['id']}' /></td>
			</tr>";
		}
		echo $str;
		exit;
	}elseif($_POST['paso'] == '6'){ //PASO D. NORMALIZADA, ANALITO, UNIDAD
		$ROW = $Robot->ProcOtrosGet($_POST['procedimiento']);
		$str = "<td>Concentraci&oacute;n normalizada (ng/&mu;L)::</td>
			<td><input type='text' id='normalizada' class='monto' onblur='_FLOAT(this)' value='{$ROW[0]['Dnormalizada']}'></td>
			<td><strong>Analito:</strong></td>
			<td><select id='analito' name='analito'>
				<option value=''>...</option>
				<option value='0'".($ROW[0]['Danalito']=='0' ? 'selected' : '').">ADN gen&oacute;mico</option>
				<option value='1'".($ROW[0]['Danalito']=='1' ? 'selected' : '').">ARN total</option>
				<option value='2'".($ROW[0]['Danalito']=='2' ? 'selected' : '').">ADN doble banda</option>
				<option value='3'".($ROW[0]['Danalito']=='3' ? 'selected' : '').">ADN simple banda</option>
				<option value='4'".($ROW[0]['Danalito']=='4' ? 'selected' : '').">ARN</option>
			</select></td>
			<td><strong>Unidad:</strong></td>
			<td><select id='unidad' name='unidad'>
				<option value=''>...</option>
				<option value='0'".($ROW[0]['Dunidad']=='0' ? 'selected' : '').">&mu;g/&mu;L</option>
				<option value='1'".($ROW[0]['Dunidad']=='1' ? 'selected' : '').">ng/&mu;L</option>
			</select></td>";
		echo $str;
		exit;
	}elseif($_POST['paso'] == '8'){ //PASO E. PROGRAMA
		$ROW = $Robot->ProcOtrosGet($_POST['procedimiento']);
		$str = "<input type='text' id='programaE' size='10' maxlength='20' value='{$ROW[0]['Eprograma']}' />";
		echo $str;
		exit;
	}elseif($_POST['paso'] == '9'){ //PASO F. PROGRAMA
		$ROW = $Robot->ProcOtrosGet($_POST['procedimiento']);
		$str = "<input type='text' id='programaF' size='10' maxlength='20' value='{$ROW[0]['Gprograma']}' />";
		echo $str;
		exit;
	}elseif($_POST['paso'] == '10'){ //PASO G. 
		$ROW = $Robot->ProcOtrosGet($_POST['procedimiento']);
		$str = "<td>Tiempo:</td>
		<td><input type='text' id='tiempoG' class='monto' onblur='_FLOAT(this)' value='{$ROW[0]['Htiempo']}'></td>
		<td>Voltaje:</td>
		<td><input type='text' id='voltajeG' class='monto' onblur='_FLOAT(this)' value='{$ROW[0]['Hvoltaje']}'></td>";
		echo $str;
		exit;
	}elseif($_POST['paso'] == '11'){ //Plantilla 
		if($_POST['letra']=='E'){
			$tipo = '1';
			$ROW = $Robot->ProcPlantillaGet($_POST['procedimiento'], $tipo);
			for($i=0,$str='';$i<count($ROW);$i++){	
				$str .= "<tr>
				<td>".($i+1)."</td>
				<td><select name='{$_POST['letra']}1tipo' id='{$_POST['letra']}1tipo{$i}'>
					<option value=''>...</option>
					<option value='0'".($ROW[$i]['tipo']=='0' ? 'selected' : '').">Reactivo</option>
					<option value='1'".($ROW[$i]['tipo']=='1' ? 'selected' : '').">Alicuota</option>
				</select></td>
				<td><input type='text' name='E1descr' size='10' maxlength='20' value='{$ROW[$i]['descr']}' /></td>
				<td><input type='text' id='{$_POST['letra']}1insumos{$i}' name='{$_POST['letra']}1insumos' class='lista' readonly onclick='DependeLista(\"{$_POST['letra']}1\", {$i})' value='...{$ROW[$i]['descr']}' /><input type='hidden' id='{$_POST['letra']}1cod_insumos{$i}' name='{$_POST['letra']}1cod_insumos' /></td>
				<td><input type='text' name='E1concS' size='10' maxlength='20' value='{$ROW[$i]['concS']}' /></td>
				<td><input type='text' name='E1concF' size='10' maxlength='20' value='{$ROW[$i]['concF']}' /></td>
				<td><input type='text' name='{$_POST['letra']}1volumen' class='monto2' value='{$ROW[$i]['volumen']}' onblur='_FLOAT(this);CalculaReacciones();'/></td>
				<td><input type='text' name='{$_POST['letra']}1reacciones' class='monto2' value='0' readonly/></td>
				</tr>";
			}
		}else{
			$tipo = '';
			$ROW = $Robot->ProcPlantillaGet($_POST['procedimiento'], $tipo);
			for($i=0,$str='';$i<count($ROW);$i++){	
				$str .= "<tr>
				<td>".($i+1)."</td>
				<td><select name='{$_POST['letra']}1tipo' id='{$_POST['letra']}1tipo{$i}'>
					<option value=''>...</option>
					<option value='0'".($ROW[$i]['tipo']=='0' ? 'selected' : '').">Reactivo</option>
					<option value='1'".($ROW[$i]['tipo']=='1' ? 'selected' : '').">Alicuota</option>
				</select></td>
				<td><input type='text' name='F1descr' size='10' maxlength='20' value='{$ROW[$i]['descr']}' /></td>
				<td><input type='text' id='{$_POST['letra']}1insumos{$i}' name='{$_POST['letra']}1insumos' class='lista2' readonly onclick='DependeLista(\"{$_POST['letra']}1\", {$i})' /><input type='hidden' id='{$_POST['letra']}1cod_insumos{$i}' name='{$_POST['letra']}1cod_insumos' /></td>
				<td><input type='text' name='{$_POST['letra']}1volumen' class='monto2' value='{$ROW[$i]['volumen']}' onblur='_FLOAT(this);CalculaReacciones();'/></td>
				<td><input type='text' name='{$_POST['letra']}1reacciones' class='monto2' value='0' readonly/></td>
				</tr>";
			}
		}//IF
		echo $str;
		exit;
	}elseif($_POST['paso'] == '12'){ //PASO X. 
		$ROW = $Robot->ProcOtrosGet($_POST['procedimiento']);
		$str = "<td>Tiempo:</td>
		<td><input type='text' id='tiempoX' class='monto' onblur='_FLOAT(this)' value='{$ROW[0]['Ftiempo']}'></td>
		<td>Voltaje:</td>
		<td><input type='text' id='voltajeX' class='monto' onblur='_FLOAT(this)' value='{$ROW[0]['Fvoltaje']}'></td>";
		echo $str;
		exit;
	}elseif($_POST['paso'] == '13'){ //PRODUCTO PCR (amplicon)
		$ROW = $Robot->ProcOtrosGet($_POST['procedimiento']);
		$str = "<input type='text' id='Famplicon1' class='monto2' value='{$ROW[0]['Gamplicon']}' onblur='_FLOAT(this);CalculaReacciones()'/>";
		echo $str;
		exit;
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _trabajo_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M' && $_GET['acc'] != 'R') ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('A7') );
			/*PERMISOS*/
			
			$_POST['usuario'] = $this->_ROW['UID'];
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Robot = new Biologia();
			if( !isset($_GET['ID']) or $_GET['ID']=='' ){
				$_POST['disabled'] = 'disabled';
				return $Robot->TrabajoEncabezadoVacio('');
			}else{
				$_POST['disabled'] = '';
				return $Robot->TrabajoEncabezadoGet($_GET['ID']);
			}
		}
		
		function DetalleAnalisis($_paso){
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return array();
			else{
				$Robot = new Biologia();
				return $Robot->TrabajoAnalisisGet($_GET['ID'], $_paso);
			}
		}
		
		function DetalleMuestras($_tipo){
			$Robot = new Biologia();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Robot->TrabajoMuestrasVacio();
			else
				return $Robot->TrabajoMuestrasGet($_GET['ID'], $_tipo);
		}
		
		function DetalleInsumos($_paso){
			$Robot = new Biologia();
			return $Robot->TrabajoInsumosGet($_GET['ID'], $_paso);
		}
		
		function DetalleEquipos($_paso){
			$Robot = new Biologia();
			return $Robot->TrabajoEquiposGet($_GET['ID'], $_paso);
		}
		
		function DetalleControles($_tipo=''){
			$Robot = new Biologia();
			return $Robot->TrabajoControlesGet($_GET['ID'], $_tipo);
		}
		
		function DetalleResultados($_tipo){
			$Robot = new Biologia();
			if($_tipo=='')
				return $Robot->TrabajoResultadosVacio($_GET['ID']);
			else
				return $Robot->TrabajoResultadosGet($_GET['ID']);
		}
		
		function DetallePlantilla($_paso){
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return array();
			else{
				$Robot = new Biologia();
				return $Robot->TrabajoPlantillaGet($_GET['ID'], $_paso);
			}
		}
	}
}
?>