<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['formato'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=reportes.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$Robot = new Reportes();
	$titulo = "Historial Muestras Desechadas";
	$Robot->TableHeader($titulo);	
	?>
	<table class='radius' align='center' width='100%' border="1">
	<tr align="center">
		<td><strong>C&oacute;digo</strong></td>
        <td><strong>&Aacute;cido nucleico</strong></td>
        <td><strong>Bandeja</strong></td>        
        <td><strong>Cultivo</strong></td>
        <td><strong>Cuantificaci&oacute;n</strong></td>
        <td><strong>Hoja de trabajo</strong></td>
        <td><strong>Fecha ingreso</strong></td>
        <td><strong>Fecha salida</strong></td>
        <td><strong>Observaciones</strong></td>
	</tr>
	<?php
	$ROW = $Robot->ObtieneVencimientos();
	for($x=0;$x<count($ROW);$x++){
            if($ROW[$x]['analito'] == '0') $ROW[$x]['analito'] = 'ADN gen&oacute;mico';
            if($ROW[$x]['analito'] == '1') $ROW[$x]['analito'] = 'ARN total';
            if($ROW[$x]['analito'] == '2') $ROW[$x]['analito'] = 'ADN doble banda';
            if($ROW[$x]['analito'] == '3') $ROW[$x]['analito'] = 'ADN simple banda';
            if($ROW[$x]['analito'] == '5') $ROW[$x]['analito'] = 'ARN';
	?>
	<tr align="center">
		<td><?=$ROW[$x]['id']?></td>
            <td><?=$ROW[$x]['analito']?></td>
            <td><?=$ROW[$x]['bandeja']?></td>            
            <td><?=$ROW[$x]['cultivo']?></td>
            <td><?=$ROW[$x]['cuanti']?></td>
            <td><?=$ROW[$x]['hoja']?></td>
            <td><?=$ROW[$x]['fechaC']?></td>
            <td><?=$ROW[$x]['fechaA']?></td>
            <td><?=$ROW[$x]['obs']?></td>
	</tr>
        <tr>
            <td>
                <table>
                    <tr><td><strong>An&aacute;lisis</strong></td><td><strong>Tipo Material</strong></td></tr>
                    <?php 
                        $M = $Robot->ObtieneVencimientos1($ROW[$x]['id']);
                        for($m=0;$m<count($M);$m++){
                    ?>
                    <tr><td><?=$M[$m]['analisis']?></td><td><?=$M[$m]['paso']?></td></tr>
                    <?php } ?>
                </table>
            <td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr><td><strong>Pat&oacute;genos Analizados</strong></td><td><strong>Resultados</strong></td><td><strong>Observaciones</strong></td></tr>
                    <?php 
                        $M = $Robot->ObtieneVencimientos2($ROW[$x]['id']);
                        for($m=0;$m<count($M);$m++){
                    ?>
                    <tr><td><?=$M[$m]['analisis']?></td><td><?=$M[$m]['resultado']?></td><td><?=$M[$m]['obs']?></td></tr>
                    <?php } ?>
                </table>
            <td>
        </tr>
        <tr><td colspan="9"><hr></td></tr>
	<?php } ?>
	<tr align="center">
		<td align="right" colspan="8" ><strong>Registros:</strong></td>
		<td align="center"><?=$x--?></td>
	</tr>
	</table>
	<?php
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	class _inventario_vencimientos extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('82') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function Lab(){
			return $this->_ROW['LID'];
		}
	}
}
?>