<?php

require_once "GOD.php";

function _FREE($_VAR) {
    $_VAR = str_replace('á', 'a', $_VAR);
    $_VAR = str_replace('é', 'e', $_VAR);
    $_VAR = str_replace('í', 'i', $_VAR);
    $_VAR = str_replace('ó', 'o', $_VAR);
    $_VAR = str_replace('ú', 'u', $_VAR);
    $_VAR = str_replace('ñ', 'n', $_VAR);
    $_VAR = str_replace("'", '', $_VAR);
    $_VAR = str_replace("(", '', $_VAR);
    $_VAR = str_replace(")", '', $_VAR);
    return $_VAR;
}

include 'Classes/PHPExcel/IOFactory.php';

if ($_FILES['archivo']['size'] > 0) {
    $ruta = $_FILES['archivo']['name'];
    $file = $_FILES['archivo']['tmp_name'];
    $_ruta = str_replace(' ', '', $ruta);
    @move_uploaded_file($file, $_ruta);

    $XLFileType = PHPExcel_IOFactory::identify($_ruta);
    $objReader = PHPExcel_IOFactory::createReader($XLFileType);
    $objPHPExcel = $objReader->load($_ruta);
    $objWorksheet = $objPHPExcel->setActiveSheetIndexByName('Hoja1');

    for ($x = 3; $x != -1; $x++) {
        /*********************************************************************************/
        $id = $objPHPExcel->getActiveSheet()->getCell('A' . $x)->getFormattedValue();
        $analito = $objPHPExcel->getActiveSheet()->getCell('B' . $x)->getFormattedValue();
        $cultivo = $objPHPExcel->getActiveSheet()->getCell('C' . $x)->getFormattedValue();
        $cuantificacion = $objPHPExcel->getActiveSheet()->getCell('D' . $x)->getFormattedValue();
        $hoja = $objPHPExcel->getActiveSheet()->getCell('E' . $x)->getFormattedValue();
        $fecha = $objPHPExcel->getActiveSheet()->getCell('F' . $x)->getFormattedValue();
        $observaciones = _FREE($objPHPExcel->getActiveSheet()->getCell('G' . $x)->getFormattedValue());
        $bandeja = $objPHPExcel->getActiveSheet()->getCell('H' . $x)->getFormattedValue();
        $posicion = $objPHPExcel->getActiveSheet()->getCell('I' . $x)->getFormattedValue();
        /*********************************************************************************/
        $bandeja = $bandeja.', '.$posicion;

        if (!_QUERY("SELECT id FROM INV045 WHERE id = '{$id}';")) {
            _TRANS("INSERT INTO INV045 VALUES('{$id}', '{$analito}', '{$bandeja}', '{$hoja}', '{$cultivo}', '{$cuantificacion}', GETDATE(),'{$fecha}', '28', '{$observaciones}', 'c');");
        }else {
            _TRANS("UPDATE INV045 SET analito = '{$analito}', bandeja = '{$bandeja}', hoja = '{$hoja}', cultivo = '{$cultivo}', cuanti = '{$cuantificacion}', fechaA = '{$fecha}', obs = '{$observaciones}' WHERE id = '{$id}';");
        }
        
        $y = $x+1;
        if($objPHPExcel->getActiveSheet()->getCell('A'.$y)->getFormattedValue() == '')
             break;	
    }
    
    $objWorksheet = $objPHPExcel->setActiveSheetIndexByName('Hoja2');

    for ($x = 3; $x != -1; $x++) {
        /*********************************************************************************/
        $analisis = $objPHPExcel->getActiveSheet()->getCell('A' . $x)->getFormattedValue();
        $paso = $objPHPExcel->getActiveSheet()->getCell('B' . $x)->getFormattedValue();
        /*********************************************************************************/
        if($objPHPExcel->getActiveSheet()->getCell('A'.$x)->getFormattedValue() != '')
            _TRANS("INSERT INTO INV046 VALUES('{$id}', '{$paso}', '{$analisis}');");
        
       $y = $x+1;
        if($objPHPExcel->getActiveSheet()->getCell('A'.$y)->getFormattedValue() == '')
             break;
    }
    
    $objWorksheet = $objPHPExcel->setActiveSheetIndexByName('Hoja3');

    for ($x = 3; $x != -1; $x++) {
        /*********************************************************************************/
        $patogeno = $objPHPExcel->getActiveSheet()->getCell('A' . $x)->getFormattedValue();
        $resultado = $objPHPExcel->getActiveSheet()->getCell('B' . $x)->getFormattedValue();
        $obs = $objPHPExcel->getActiveSheet()->getCell('B' . $x)->getFormattedValue();
        /*********************************************************************************/
        if($objPHPExcel->getActiveSheet()->getCell('A'.$x)->getFormattedValue() != '')
            _TRANS("INSERT INTO INV047 VALUES('{$id}', '{$patogeno}', '{$resultado}', '{$obs}');");
        
       $y = $x+1;
        if($objPHPExcel->getActiveSheet()->getCell('A'.$y)->getFormattedValue() == '')
             break;
    }
    echo "Inventario importado correctamente";
    @unlink($_ruta);
    exit();
} else
    die('Archivo vacio');
exit;
?>