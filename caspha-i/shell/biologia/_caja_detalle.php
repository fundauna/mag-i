<?php

/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 * ***************************************************************** */
if (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth())
        die('-0');
    $ROW = $Securitor->SesionGet();

    $Robot = new Biologia();
    
    if($_POST['_AJAX']==1){
        echo $Robot->caja_ime("M",$_POST['id'],'','',$_POST['nombre'],$_POST['ubicacion']);
    }elseif($_POST['_AJAX']==2){
        echo $Robot->caja_linea_IME("E", $_POST['id_caja'], $_POST['id_plaga']);
    }elseif($_POST['_AJAX']==3){
        echo json_encode($Robot->caja_get($_POST['id']));
    }elseif($_POST['_AJAX']==4){
        echo json_encode($Robot->caja_linea_get($_POST['id']));
    }

    exit;
} else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _caja_detalle extends Mensajero {

        private $_ROW = array();
        private $Securitor = '';
        private $inicio = false;
        private $biologia;

        function __construct() {
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth())
                $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0')
                $this->Portero($this->Securitor->UsuarioPermiso('A7'));
            /* PERMISOS */
            if ($this->_ROW['LID'] != '2')
                die('Secci�n bloqueada para este laboratorio');

            if (!isset($_POST['codigo'])) {
                $_POST['desde'] = $_POST['hasta'] = date('d/m/Y');
                $_POST['codigo'] = '';
                $this->inicio = true;
            }
            $this->biologia = new Biologia();
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function obtieneDatos() {
            return isset($_GET['ID']) ? $this->biologia->caja_get('',$_GET['ID'],$_GET['L']) : array();
        }

    }

}
?>