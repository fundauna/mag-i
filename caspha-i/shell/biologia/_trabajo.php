<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['cs']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot = new Biologia();
	echo $Robot->TrabajoAnula($_POST['cs'], $_ROW['UID']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _trabajo extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('A7') );
			/*PERMISOS*/
			if($this->_ROW['LID'] != '2') die('Secci�n bloqueada para este laboratorio');
			
			if(!isset($_POST['codigo'])){
				$_POST['estado'] = 'r';
				$_POST['codigo'] = '';
				$this->inicio = true;
			}
			
			$_POST['usuario'] = $this->_ROW['UID'];
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function Estado($_var){
			$Robot = new Biologia();
			return $Robot->TrabajoEstado($_var);
		}
	
		function ResMuestra(){
			$Robot = new Biologia();
			return $Robot->TrabajoResumenGet($_POST['estado'], $_POST['codigo']);
		}
	}
}
?>