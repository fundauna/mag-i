<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

final class _trabajo_imprimir extends Mensajero{
	private $_ROW = array();
	private $Securitor = '';
	
	function __construct(){
		//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
		$this->ValidaModal();
		
		if( !isset($_GET['ID']) ) die('Error de parámetros');
		
		$this->Securitor = new Seguridad();
		if(!$this->Securitor->SesionAuth()) $this->Err();
		$this->_ROW = $this->Securitor->SesionGet();
		/*PERMISOS*/
		if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('A7') );
		/*PERMISOS*/
	}

    function Encabezado($_hoja, $_tipo, $_titulo)
    {
        $Qualitor = new Calidad();
        echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
    }
	
	function ObtieneDatos(){
		$Robot = new Biologia();
		return $Robot->TrabajoEncabezadoGet($_GET['ID']);
	}
	
	function DetalleAnalisis($_paso){
		$Robot = new Biologia();
		return $Robot->TrabajoAnalisisGet($_GET['ID'], $_paso);
	}
	
	function DetalleMuestras($_tipo){
		$Robot = new Biologia();
		return $Robot->TrabajoMuestrasGet($_GET['ID'], $_tipo);
	}
	
	function DetalleInsumos($_paso){
		$Robot = new Biologia();
		return $Robot->TrabajoInsumosGet($_GET['ID'], $_paso);
	}
	
	function DetalleEquipos($_paso){
		$Robot = new Biologia();
		return $Robot->TrabajoEquiposGet($_GET['ID'], $_paso);
	}
	
	function DetalleControles($_tipo=''){
		$Robot = new Biologia();
		return $Robot->TrabajoControlesGet($_GET['ID'], $_tipo);
	}
	
	function DetalleResultados($_tipo){
		$Robot = new Biologia();
		if($_tipo=='')
			return $Robot->TrabajoResultadosVacio($_GET['ID']);
		else
			return $Robot->TrabajoResultadosGet($_GET['ID']);
	}
	
	function DetallePlantilla($_paso){
		$Robot = new Biologia();
		return $Robot->TrabajoPlantillaGet($_GET['ID'], $_paso);
	}
}
?>