<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list1'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Listator = new Listador();
	$Listator->MacroAnalisisLista('biologia', 2, $_GET['tipo'], $_GET['list1'], basename(__FILE__));
	exit;
}elseif( isset($_GET['list2'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Listator = new Listador();
	$Listator->EquiposLista('biologia', $_GET['list2'], 2, basename(__FILE__));
	exit;
}elseif( isset($_GET['list3'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Listator = new Listador();
	$Listator->InventarioCatalogo('biologia', $_GET['list3'], 2, basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
}elseif( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['cs']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	
	$Robot = new Biologia();

	if(!isset($_POST['A1cod_analisis'])) $_POST['A1cod_analisis'] = '';
	if(!isset($_POST['A2cod_analisis'])) $_POST['A2cod_analisis'] = '';
	if(!isset($_POST['A3cod_analisis'])) $_POST['A3cod_analisis'] = '';
	if(!isset($_POST['C1cod_insumos'])) $_POST['C1cod_insumos'] = '';
	if(!isset($_POST['C1total'])) $_POST['C1total'] = '';
	if(!isset($_POST['C2cod_equipos'])) $_POST['C2cod_equipos'] = '';
	if(!isset($_POST['D1cod_insumos'])) $_POST['D1cod_insumos'] = '';
	if(!isset($_POST['D1total'])) $_POST['D1total'] = '';
	if(!isset($_POST['D2cod_equipos'])) $_POST['D2cod_equipos'] = '';
	if(!isset($_POST['E1tipo'])) $_POST['E1tipo'] = '';
	if(!isset($_POST['E1descr'])) $_POST['E1descr'] = '';
	if(!isset($_POST['E1concS'])) $_POST['E1concS'] = '';
	if(!isset($_POST['E1concF'])) $_POST['E1concF'] = '';
	if(!isset($_POST['E1volumen'])) $_POST['E1volumen'] = '';
	if(!isset($_POST['E2cod_equipos'])) $_POST['E2cod_equipos'] = '';
	if(!isset($_POST['F1cod_insumos'])) $_POST['F1cod_insumos'] = '';
	if(!isset($_POST['F1total'])) $_POST['F1total'] = '';
	if(!isset($_POST['F2cod_equipos'])) $_POST['F2cod_equipos'] = '';
	if(!isset($_POST['G1tipo'])) $_POST['G1tipo'] = '';
	if(!isset($_POST['G1descr'])) $_POST['G1descr'] = '';
	if(!isset($_POST['G1volumen'])) $_POST['G1volumen'] = '';
	if(!isset($_POST['G2cod_equipos'])) $_POST['G2cod_equipos'] = '';
	if(!isset($_POST['H1cod_insumos'])) $_POST['H1cod_insumos'] = '';
	if(!isset($_POST['H1total'])) $_POST['H1total'] = '';
	if(!isset($_POST['H2cod_equipos'])) $_POST['H2cod_equipos'] = '';

	if(! $Robot->ProcIME($_POST['accion'],
		$_POST['cs'],
		$_POST['nombre'],
		$_POST['A1cod_analisis'],
		$_POST['A2cod_analisis'],
		$_POST['A3cod_analisis'],
		$_POST['C1cod_insumos'],
		$_POST['C1total'],
		$_POST['C2cod_equipos'],
		$_POST['Dnormalizada'],
		$_POST['Danalito'],
		$_POST['Dunidad'],
		$_POST['D1cod_insumos'],
		$_POST['D1total'],
		$_POST['D2cod_equipos'],
		$_POST['E1tipo'],
		$_POST['E1descr'],
		$_POST['E1concS'],
		$_POST['E1concF'],
		$_POST['E1volumen'],
		$_POST['E2cod_equipos'],
		$_POST['Eprograma'],
		$_POST['F1cod_insumos'],
		$_POST['F1total'],
		$_POST['F2cod_equipos'],
		$_POST['Ftiempo'],
		$_POST['Fvoltaje'],
		$_POST['G1tipo'],
		$_POST['G1descr'],
		$_POST['G1volumen'],
		$_POST['Gamplicon'],
		$_POST['G2cod_equipos'],
		$_POST['Gprograma'],
		$_POST['H1cod_insumos'],
		$_POST['H1total'],
		$_POST['H2cod_equipos'],
		$_POST['Htiempo'],
		$_POST['Hvoltaje']
		) ) die('Error transaccional');
	
	$ruta = "../../docs/biologia/procs/{$_POST['cs']}.zip";
	if($_FILES['archivo']['size'] > 0){
		$nombre = $_FILES['archivo']['name'];
		$file = $_FILES['archivo']['tmp_name'];	
		Bibliotecario::ZipCrear($nombre, $file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");	
	}elseif( isset($_POST['eliminar']) && $_POST['eliminar']=='on' ){
		Bibliotecario::ZipEliminar($ruta);
	}
			
	die("<script>alert('Transacción finalizada');window.close();</script>");
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _proc_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M' && $_GET['acc'] != 'V') ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('A7') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Robot = new Biologia();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Robot->ProcEncabezadoVacio();
			else
				return $Robot->ProcEncabezadoGet($_GET['ID']);
		}
		
		function DetallePlantilla($_tipo){
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return array();
			else{
				$Robot = new Biologia();
				return $Robot->ProcPlantillaGet($_GET['ID'], $_tipo);
			}
		}
		
		function DetalleEquipos($_paso){
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return array();
			else{
				$Robot = new Biologia();
				return $Robot->ProcEquiposGet($_GET['ID'], $_paso);
			}
		}
		
		function DetalleAnalisis($_paso){
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return array();
			else{
				$Robot = new Biologia();
				return $Robot->ProcAnalisisGet($_GET['ID'], $_paso);
			}
		}
		
		function DetalleInsumos($_paso){
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return array();
			else{
				$Robot = new Biologia();
				return $Robot->ProcInsumosGet($_GET['ID'], $_paso);
			}
		}
	}
}
?>