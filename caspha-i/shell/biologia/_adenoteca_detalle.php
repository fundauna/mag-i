<?php
/*******************************************************************
 * PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
 *******************************************************************/
if (isset($_GET['bandeja'])) {
    function __autoload($_BaseClass)
    {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    $ROW = $Securitor->SesionGet();
    $Listator = new Listador();
    $Listator->BandejasLista('biologia', $ROW['LID'], basename(__FILE__));
} elseif (isset($_GET['list1'])) {
    function __autoload($_BaseClass)
    {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Listator = new Listador();
    $Listator->MacroAnalisisLista('biologia', 2, $_GET['tipo'], $_GET['list1'], basename(__FILE__));
    exit;
    /*******************************************************************
     * PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
     *******************************************************************/
} elseif (isset($_POST['_AJAX'])) {
    function __autoload($_BaseClass)
    {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    if (!isset($_POST['cs'])) die('-1');

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth()) die('-0');
    $_ROW = $Securitor->SesionGet();

    $Robot = new Biologia();

    if ($_POST['_AJAX'] == '1') {
        echo $Robot->ADNoIME($_POST['accion'],
            $_POST['cs'],
            $_POST['analito'],
            $_POST['rotulo'],
            $_POST['bandeja'],
            $_POST['x'],
            $_POST['y'],
            $_POST['cultivo'],
            $_POST['cuanti'],
            $_POST['hoja'],
            $_POST['fechaA'],
            $_POST['observaciones'],
            $_POST['A1cod_analisis'],
            $_POST['A2cod_analisis'],
            $_POST['A3cod_analisis'],
            $_POST['resultado'],
            $_POST['obs'],
            'c',
            $_ROW['UID']);
    } else {
        echo $Robot->ADNoE($_POST['cs'], $_ROW['UID']);
    }
    exit;
} else {
    /*******************************************************************
     * DECLARACION CLASE GESTORA
     *******************************************************************/
    function __autoload($_BaseClass)
    {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _adenoteca_detalle extends Mensajero
    {
        private $_ROW = array();
        private $Securitor = '';

        function __construct()
        {
            //SOLO SE PUEDE ABRIR COMO POP-UP MODAL
            $this->ValidaModal();

            if (!isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M')) die('Error de parámetros');

            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth()) $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /*PERMISOS*/
            if ($this->_ROW['ROL'] != '0') $this->Portero($this->Securitor->UsuarioPermiso('A7'));
            /*PERMISOS*/
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function ObtieneDatos()
        {
            $Robot = new Biologia();
            if (!isset($_GET['id']) or $_GET['id'] == '')
                return $Robot->ADNoEncabezadoVacio();
            else
                return $Robot->ADNoEncabezadoGet($_GET['id']);
        }

        function DetalleAnalisis($_paso)
        {
            if (!isset($_GET['id']) or $_GET['id'] == '')
                return array();
            else {
                $Robot = new Biologia();
                return $Robot->ADNoAnalisisGet($_GET['id'], $_paso);
            }
        }

        function Detalle()
        {
            $Robot = new Biologia();
            if (!isset($_GET['id']) or $_GET['id'] == '')
                return $Robot->ADNoDetalleVacio();
            else
                return $Robot->ADNoDetalleGet($_GET['id']);
        }
    }
}
?>