<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['codigo']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot = new Biologia();

	echo $Robot->ResIME($_POST['accion'],
		$_POST['codigo'],
		$_POST['reactivo'],
		$_POST['fecha_r'], 
		$_POST['disolvente'],
		$_POST['concentracionI'], 
		$_POST['vol_disolvente'], 
		$_POST['concentracionM'], 
		$_POST['medida1'],
		$_POST['medida2'],
		$_POST['medida3'],
		$_POST['observaciones'],
		$_POST['estado'],
		$_ROW['UID']
	);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _resuspension_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M') ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('A7') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Robot = new Biologia();
			if( !isset($_GET['CS']) or $_GET['CS']=='')
				return $Robot->ResEncabezadoVacio();
			else
				return $Robot->ResEncabezadoGet($_GET['CS']);
		}
		
		function ResLineasMuestra(){
			$Robot = new Biologia();
			if( !isset($_GET['CS']) or $_GET['CS']=='')
				return $Robot->ResDetalleVacio();
			else
				return $Robot->ResDetalleGet($_GET['CS']);
		}
		
		function Formato($_NUMBER){
			return number_format($_NUMBER, 2, '.', ',');
		}
	}
}
?>