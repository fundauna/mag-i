<?php

/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 * ***************************************************************** */
if (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth())
        die('-0');
    $ROW = $Securitor->SesionGet();

    $Robot = new Biologia();
    if ($_POST['_AJAX'] == 1) {
        $_POST['id'] = $_POST['acc'] == 'I' ? $Robot->consecutivo('camaras') : $_POST['id'];
        if ($Robot->camaras_IME($_POST['acc'], $_POST['id'], $_POST['descripcion'], $_POST['estado'])) {
            $bandejamax = $Robot->bandeja_max($_POST['id']);
            $lineamax = $bandejamax ? $bandejamax[0]['linea']==null?0:$bandejamax[0]['linea']: 0;
            if (isset($_POST['descripcion_v'])) {
                for ($i = 0; $i < count($_POST['descripcion_v']); $i++) {
                    if ($_POST['bandejalinea'][$i] > -1) {
                        $Robot->bandeja_ime('M', $_POST['id'], $_POST['bandejalinea'][$i], $_POST['descripcion_v'][$i], $_POST['dimensionx'][$i], $_POST['dimensiony'][$i], $_POST['rotulox'][$i], $_POST['rotuloy'][$i]);
                    } else {
                        $Robot->bandeja_ime('I', $_POST['id'], $lineamax++, $_POST['descripcion_v'][$i], $_POST['dimensionx'][$i], $_POST['dimensiony'][$i], $_POST['rotulox'][$i], $_POST['rotuloy'][$i]);
                    }
                }
            }
            echo 1;
        }
    } else {
        echo $Robot->bandeja_ime("E", $_POST['id'], $_POST['linea']);
    }
    exit;
} else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _camaras_detalle extends Mensajero {

        private $_ROW = array();
        private $Securitor = '';
        private $inicio = false;
        private $biologia;

        function __construct() {
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth())
                $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0')
                $this->Portero($this->Securitor->UsuarioPermiso('A7'));
            /* PERMISOS */
            if ($this->_ROW['LID'] != '2')
                die('Secci�n bloqueada para este laboratorio');

            if (!isset($_POST['codigo'])) {
                $_POST['desde'] = $_POST['hasta'] = date('d/m/Y');
                $_POST['codigo'] = '';
                $this->inicio = true;
            }
            $this->biologia = new Biologia();
        }

        function obtieneDatos() {
            return isset($_GET['ID']) && is_numeric($_GET['ID']) ? $this->biologia->camaras_get($_GET['ID']) : $this->biologia->camaras_get_vacio();
        }

        function obtieneLineas() {
            return isset($_GET['ID']) && is_numeric($_GET['ID']) ? $this->biologia->bandeja_get($_GET['ID']) : array();
        }

    }

}
?>