<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _escoge_bandejas extends Mensajero{
		
		function __construct(){
			if(!isset($_GET['caja'])) die('Error de parámetros');
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function BandejasMuestra(){
			$Robot= new Biologia();
			return $Robot->CamarasResumenGet($_GET['caja'], $_GET['iP']);
		}
	}
?>