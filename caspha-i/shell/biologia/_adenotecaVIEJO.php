<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['cs']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$ROW = $Securitor->SesionGet();
	
	$Robot = new Biologia();
	echo $Robot->ADNoIME('D', $_POST['cs'], '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', $ROW['UID']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _adenoteca extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('A7') );
			/*PERMISOS*/
			if($this->_ROW['LID'] != '2') die('Secci�n bloqueada para este laboratorio');
			
			if(!isset($_POST['codigo'])){
				$_POST['desde'] = $_POST['hasta'] = date('d/m/Y');				
				$_POST['codigo'] = '';
				$this->inicio = true;
			}
		}
		
                function MuestraPlagas(){
                    $Robot = new Varios();
                    return $Robot->SubAnalisisGet('6', '2');
		}
                
		function ResMuestra($_plaga){
                    $Robot = new Biologia();
                    return $Robot->CajasResumenGetPlaga($_plaga);
		}
                
                function VaciaTemporal(){
                    $Robot = new Biologia();
                    return $Robot->VaciaTemporal();
		}
	}
}
?>