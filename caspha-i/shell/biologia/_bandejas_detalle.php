<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _bandejas_detalle extends Mensajero{

		private $_vector = array('N'=>array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20), 
			 'L'=>array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T')
		 );
		
		function __construct(){
			if(!isset($_GET['bandeja'])) die('Error de parámetros');
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function BandejasMuestra(){
			$Robot= new Biologia();
			return $Robot->BandejasMuestra($_GET['bandeja']);
		}
		
		function BandejasMuestraCajas(){
			$Robot= new Biologia();
			return $Robot->BandejasMuestraCajas($_GET['bandeja']);
		}
		
		function Label($_tipo, $_pos){
			return $this->_vector[$_tipo][$_pos];
		}
	}
?>