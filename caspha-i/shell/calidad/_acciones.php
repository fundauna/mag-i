<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['cs']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Qualitor = new Calidad();
	echo $Qualitor->AccionesElimina($_ROW['UID'], $_POST['cs']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _acciones extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('94') );
			/*PERMISOS*/
			if(!isset($_POST['desde'])){
				$_POST['desde'] = $_POST['hasta'] = date('d-m-Y');
				$_POST['tipo'] = 'C';
				$_POST['estado'] = 'A';
				$this->inicio = true;
			}
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function AccionesMuestra(){
			if($this->inicio) return array();
			$Qualitor= new Calidad();
			return $Qualitor->AccionesResumenGet($_POST['tipo'], $_POST['desde'], $_POST['hasta'], $_POST['estado']);
		}
		
	}
}
?>