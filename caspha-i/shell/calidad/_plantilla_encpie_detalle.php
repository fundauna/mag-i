<?php
/*******************************************************************
 * PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 *******************************************************************/
if (isset($_POST['id'])) {
    function __autoload($_BaseClass)
    {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    $Qualitor = new Calidad();

    if ($_POST['id'] == '') {
        $CS = $Qualitor->Encabezado();
        $_POST['id'] = $CS[0]['encabezado'];
    }
    if ($Qualitor->PlantillaEncpieIME($_POST['acc'], $_POST['id'], $_POST['tipo'], $_POST['nombre'], $_POST['detalle'], $_POST['lab'])) {
        echo '<script>alert("Realizado con exito");opener.location.reload();window.close();</script>';
        exit();
    } else
        echo 'Error al guardar los datos';

} else {
    /*******************************************************************
     * DECLARACION CLASE GESTORA
     *******************************************************************/
    function __autoload($_BaseClass)
    {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _plantilla_encpie_detalle extends Mensajero
    {
        private $_ROW = array();
        private $Securitor = '';

        function __construct()
        {
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth()) $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /*PERMISOS*/
            if ($this->_ROW['ROL'] != '0') $this->Portero($this->Securitor->UsuarioPermiso('C0'));
            /*PERMISOS*/
            if (!isset($_GET['tipo'])) $_GET['tipo'] = '';
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function ObtieneDatos()
        {
            $Qualitor = new Calidad();
            if ($_GET['acc'] == 'I')
                return $Qualitor->PlantillaEncpieMuestraVacio();
            else
                return $Qualitor->PlantillaEncabezadosMuestra($_GET['id']);
        }
    }
}
?>