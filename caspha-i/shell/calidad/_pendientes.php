<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['cs']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');	
	
	$Qualitor = new Calidad();
	echo $Qualitor->DocumentosIME($_POST['cs'],$_POST['version'],$_POST['accion']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _pendientes extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			if( !isset($_GET['ver']) || !isset($_GET['tablon']) ) die('Error de parámetros');
		
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function DocumentosMuestra(){
			$Qualitor = new Calidad();
			return $Qualitor->DocumentosPendientes($this->_ROW['UID']);
		}

	}
}
?>