<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

final class _vinculados_ver extends Mensajero{
	private $_ROW = array();
	private $Securitor = '';
	
	function __construct(){
		$this->Securitor = new Seguridad();
		if(!$this->Securitor->SesionAuth()) $this->Err();
		
		if(!isset($_GET['ID'])) die('Error de parámetros');
	}

    function Encabezado($_hoja, $_tipo, $_titulo)
    {
        $Qualitor = new Calidad();
        echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
    }
	
	function ObtieneDatos(){
		$Qualitor = new Calidad();
		return $Qualitor->DocumentosDetalleGet($_GET['ID']);
	}	
		
	function DocumentosVinculados(){
		$Qualitor = new Calidad();
		return $Qualitor->DocumentosVinculadosGet($_GET['ID'], 1);
	}
}
?>