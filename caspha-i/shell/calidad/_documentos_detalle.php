<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['cs']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');	
	
	$Qualitor = new Calidad();
	echo $Qualitor->DocumentosIME($_POST['cs'],$_POST['version'],$_POST['accion']);
	exit;
}elseif( isset($_POST['accion'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');	
	
	$token=0;
	if($_POST['versionO'] == $_POST['version']) $token=1;
	
	$Qualitor= new Calidad();
	if($_POST['accion']=='I'){										
		$ROW = $Qualitor->DocumentosQuery();							
		if($Qualitor->DocumentosTrans($ROW[0]['cs_doc'],$_POST['unidad'],$_POST['codigo'],$_POST['descripcion'],$_POST['tipo'],$_POST['modulo'],$_POST['naturaleza'],$_POST['estado'],$_POST['version'],$_POST['revision'],$_POST['accion'],'')==1){		
			//SUBIR ARCHIVO			
			if($_FILES['archivo']['size'] > 0){
				$nombre = $_FILES['archivo']['name'];
				$file = $_FILES['archivo']['tmp_name'];
				$ruta = "../../docs/calidad/{$ROW[0]['cs_doc']}-{$_POST['version']}.zip";
				Bibliotecario::ZipCrear($nombre, $file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");
			}
			//SUBIR ARCHIVO			
			echo'<script>alert("Documento ingresado");opener.location.reload();window.close();</script>';
			exit();
		}else
			echo 'Error al guardar los datos';
	}else{
		if($Qualitor->DocumentosTrans($_POST['cs'],$_POST['unidad'],$_POST['codigo'],$_POST['descripcion'],$_POST['tipo'],$_POST['modulo'],$_POST['naturaleza'],$_POST['estado'],$_POST['version'],$_POST['revision'],$_POST['accion'],$token)==1){		
			//if($token==0){
				//SUBIR ARCHIVO			
				if($_FILES['archivo']['size'] > 0){
					$nombre = $_FILES['archivo']['name'];
					$file = $_FILES['archivo']['tmp_name'];
					$ruta = "../../docs/calidad/{$_POST['cs']}-{$_POST['version']}.zip";
					Bibliotecario::ZipCrear($nombre, $file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");	
				}/*else
					echo 'Error al guardar los datos';*/
			//}	
			echo'<script>alert("Documento modificado");opener.location.reload();window.close();</script>';
			exit();	
		}
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _documentos_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M') ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('92') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function DocumentosEstado(){
			$Qualitor= new Calidad();
			return $Qualitor->DocumentosEstado();
		}
		
		function DocumentosFiltro(){
			$Qualitor= new Calidad();
			return $Qualitor->DocumentosFiltro();
		}
		
		function DocumentosModulo(){
			$Qualitor= new Calidad();
			return $Qualitor->DocumentosModulo();
		}
		
		function DocumentosNaturaleza(){
			$Qualitor= new Calidad();
			return $Qualitor->DocumentosNaturaleza();
		}
		
		function DocumentosVersiones(){
			$Qualitor= new Calidad();
			return $Qualitor->DocumentosVersiones($_GET['ID']);
		}
		
		function ObtieneDatos(){
			$Qualitor = new Calidad();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Qualitor->DocumentosVacio();
			else	
				return $Qualitor->DocumentosDetalleGet($_GET['ID']);
		}		
	
	}
}
?>