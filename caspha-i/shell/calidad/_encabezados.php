<?php
/*******************************************************************
 * PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 *******************************************************************/
if (isset($_POST['_AJAX'])) {
    function __autoload($_BaseClass)
    {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    $Cartor = new Calidad();
    if (!$Securitor->SesionAuth()) die('-0');
    if (!isset($_POST['laboratorio'])) die('-1');

    if (isset($_POST['LaboratorioBuscar'])) {
        $str = '<select id="hojas">';
        $ROW = $Cartor->CartasMuestra($_POST['laboratorio']);
        for ($x = 0; $x < count($ROW); $x++) {
            $str .= "<option value='{$ROW[$x]['id']}'>{$ROW[$x]['desc']}</option>";
        }
        $str .= '</select>&nbsp;<input type="button" value="Buscar" class="boton2" onClick="HojaBuscar();">';
        die($str);
    } elseif (isset($_POST['PermisosAgrega'])) {
        echo $Securitor->PermisosIME($_POST['perfil'], $_POST['permiso'], $_POST['privilegio'], 'I');
        exit;
    } elseif (isset($_POST['PermisosElimina'])) {
        echo $Securitor->PermisosIME($_POST['perfil'], $_POST['permiso'], '', 'D');
        exit;
    }
} elseif (isset($_POST['LOLO'])) {
    function __autoload($_BaseClass)
    {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    $Cartor = new Calidad();
    if (!$Securitor->SesionAuth()) die('-0');
    if (!isset($_POST['hoja'])) die('-1');

    echo $Cartor->EncabezadoModifica($_POST['hoja'], $_POST['encabezado'], $_POST['pie']);
    exit;
} else {
    /*******************************************************************
     * DECLARACION CLASE GESTORA
     *******************************************************************/
    function __autoload($_BaseClass)
    {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _encabezados extends Mensajero
    {
        private $_ROW = array();
        private $Securitor = '';

        function __construct()
        {
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth()) $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /*PERMISOS*/
            if ($this->_ROW['ROL'] != '0') $this->Portero($this->Securitor->UsuarioPermiso('C0'));
            /*PERMISOS*/
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function PlantillaEncabezadosMuestra()
        {
            $Qualitor = new Calidad();
            $_lab = $this->_ROW['LID'];
            if ($this->_ROW['UID'] == '28') {
                $_lab = '-1';
            }
            return $Qualitor->PlantillaEncabezadosMuestra('', $_lab);
        }

        function EncabezadoMuestra()
        {
            $Qualitor = new Calidad();
            return $Qualitor->EncabezadoMuestra();
        }

        function obtieneNombreEncPie($_id)
        {
            $Qualitor = new Calidad();
            $Q = $Qualitor->PlantillaEncabezadosMuestra($_id);
            if(!$Q){
                return '-';
            }else{
                return $Q[0]['nombre'];
            }
        }
    }
}
?>