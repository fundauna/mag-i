<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$_ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	$Listator->UsuariosLista('calidad', basename(__FILE__), $_GET['linea'], $_ROW['LID']);
	exit;
}elseif( isset($_POST['accion'])){ 
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$_ROW = $Securitor->SesionGet();
			
	$Qualitor = new Calidad();
	if($_POST['accion']=='I') $_POST['cs'] = $Qualitor->AccionesConsecutivo();
	
	if( $Qualitor->AccionesResumenSet($_POST['cs'], $_POST['tipo'], $_POST['codigo'], $_POST['fec_pre'], $_POST['hallazgo'], $_POST['correccion'], $_POST['fec_corr'], $_POST['usuarioA'], $_POST['accion']) ){
		//BORRA DE TABLON CUALKIER NOTIFICACION RELACIONADA
		$Qualitor->TablonDel2(94, $_ROW['UID'], $_POST['cs']);
		//INGRESA DETALLE
		$notificar = false; //PARA SABER SI HAY QUE INGRESAR EN EL TABLON
		$Qualitor->AccionesLineasDel($_POST['cs']);
		$estado = 'C';
		for($i=0;$i<count($_POST['numero']);$i++){
			if( str_replace('', '', $_POST['numero'][$i]) != ''){
				
				if($_POST['fec_cie'][$i] == '' && !$notificar) $notificar = true;
				
				if($_POST['estado'][$i]=='A') $estado = 'A';
				
				$Qualitor->AccionesLineasSet($_POST['numero'][$i], 
					$_POST['cs'], 
					$_POST['fec_acc'][$i], 
					$_POST['fec_seg'][$i], 
					$_POST['fec_rea'][$i], 
					$_POST['fec_cie'][$i],
					$_POST['estado'][$i],
					$_POST['usuarioB'][$i],
					$_POST['usuarioC'][$i],
					$_POST['usuarioD'][$i]
				);
			}
		}
		//ACTUALIZA ESTADO GENERAL
		$Qualitor->AccionesEstadoSet($_POST['cs'], $estado);
		
		//PARA SABER SI HAY QUE BORRAR DEL TABLON
		if($notificar) $Qualitor->TablonSet(94, $_ROW['UID'], $_POST['cs'], false);
		
		//SUBIR ARCHIVO			
		/*if( isset($_FILES['archivo']) ){
			if($_FILES['archivo']['size'] > 0){
				$nombre = $_FILES['archivo']['name'];
				$file = $_FILES['archivo']['tmp_name'];
				$ruta = "../../docs/calidad/acap/{$_POST['cs']}.zip";
				Bibliotecario::ZipCrear($nombre, $file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");	
			}
		}*/
			
		echo'<script>alert("Transaccion Finalizada");opener.location.reload();window.close();</script>';
		exit();
	}	
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _acciones_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M') ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('94') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Qualitor = new Calidad();
			if( !isset($_GET['ID']) or $_GET['ID']==''){
				$_GET['ID']='';
				return $Qualitor->AccionesEncabezadoVacio();
			}else
				return $Qualitor->AccionesDetalleGet($_GET['ID']);
		}		
	
		function ObtieneLineas(){
			$Qualitor = new Calidad();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Qualitor->AccionesLineasVacio();
			else	
				return $Qualitor->AccionesLineasGet($_GET['ID']);
		}
		
		function ObtieneResponsables($_numero){
			$Qualitor = new Calidad();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Qualitor->AccionesResponsablesVacio();
			else	
				return $Qualitor->AccionesResponsablesGet($_numero);
		}
	}
}
?>