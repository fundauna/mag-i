<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['cs']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$ROW = $Securitor->SesionGet();
	
	$Qualitor = new Calidad();
	echo $Qualitor->FirmasIME($_POST['tablon'], $_POST['cs'], $ROW['UID'], $_POST['jus'], $_POST['accion']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _pendientes_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['ID']) || !isset($_GET['tablon']) ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Qualitor = new Calidad();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Qualitor->DocumentosVacio();
			else	
				return $Qualitor->DocumentosDetalleGet($_GET['ID']);
		}		
	
		function DocumentosEstado(){
			$Qualitor= new Calidad();
			return $Qualitor->DocumentosEstado();
		}
		
		function DocumentosVersiones(){
			$Qualitor= new Calidad();
			return $Qualitor->DocumentosVersiones($_GET['ID']);
		}
		
		function PuedeRevisar(){
			if($this->_ROW['ROL'] == '0') return true;
			elseif($this->Securitor->UsuarioPermiso('99')=='E') return true;
			else return false;
		}
		
		function PuedeAprobar(){
			if($this->_ROW['ROL'] == '0') return true;
			elseif($this->Securitor->UsuarioPermiso('93')=='E') return true;
			else return false;
		}
	}
}
?>