<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['padre']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');	
	
	$Qualitor = new Calidad();
	echo $Qualitor->DocumentosTraslada($_POST['padre'], $_POST['usuario'], $_POST['peticion'], $_POST['detalle'], $_POST['yo']);
	exit;
}elseif( isset($_GET['list'])){ 
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');	
	$ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	$Listator->UsuariosLista('calidad', basename(__FILE__), '', $ROW['LID']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _firmas extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $Qualitor;
		
		function __construct(){
			if(!isset($_GET['ID'])) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			$_GET['YO'] = $this->_ROW['UID'];
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('92') );
			/*PERMISOS*/
			
			$this->Qualitor = new Calidad();
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function DocumentosFirmas(){
			return $this->Qualitor->DocumentosFirmas($_GET['ID']);
		}
		
		function Tipo($_tipo){
			return $this->Qualitor->FirmasTipo($_tipo);
		}
	}
}
?>