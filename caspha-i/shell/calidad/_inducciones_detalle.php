<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');	
	
	$Personitor = new Calidad();
	
	if($_POST['paso'] == '1'){
		//INGRESAR ENCABEZADO
		echo $Personitor->InduccionesEncabezadoSet($_POST['id'],
			$_POST['siglas'],
			$_POST['prox'],
			$_POST['req_cedula'],
			$_POST['req_titulo'],
			$_POST['req_colegio'],
			$_POST['req_hv'],
			$_POST['req_compromiso'],
			$_POST['req_competente'],
			$_POST['req_perfil'],
			$_POST['req_clase'],
			$_POST['req_especialidad'],
			$_POST['req_induccion'],
			$_POST['req_autorizacion']
		);
	}elseif($_POST['paso'] == '2'){
		//INGRESAR DETALLE
		$_POST['tipo'] = explode(';',$_POST['tipo']);
		$_POST['nombre'] = explode(';',$_POST['nombre']);
                $_POST['empresa'] = explode(';',$_POST['empresa']);
		$_POST['fecha'] = explode(';',$_POST['fecha']);
		$_POST['horas'] = explode(';',$_POST['horas']);
		for($i=0;$i<count($_POST['tipo']);$i++){
			$Personitor->InduccionesDetalleSet($_POST['id'],
				$_POST['tipo'][$i],
				$_POST['nombre'][$i],
                                $_POST['empresa'][$i],
				$_POST['fecha'][$i],
				$_POST['horas'][$i]
			);
		}
		//$Personitor->InduccionesIndexa($_POST['id']);
		echo 1;
	}elseif($_POST['paso'] == '3'){ 
		//VER DETALLE LECTURA
		$ROW = $Personitor->InduccionesDetalleGet($_POST['id'], $_POST['tipo'], $_POST['desde'], $_POST['hasta']);
		$cont = count($ROW);
		for($x=0;$x<$cont;$x++){
		?>
			<tr>
				<td align="center"><img src="<?=$_POST['imagenes']?>" class="tab2" onclick="EditarLinea('<?=$_POST['id']?>', '<?=$ROW[$x]['linea']?>')" title="Editar l&iacute;nea" /> </td>
				<td><?=utf8_encode($Personitor->InduccionesTipo($ROW[$x]['tipo']))?></td>
				<td><?=utf8_encode($ROW[$x]['nombre'])?></td>
                                <td><?= $ROW[$x]['empresa'] ?></td>
				<td><?=$Personitor->InduccionesFechaFormato($ROW[$x]['fecha1'])?></td>
				<td><?=$ROW[$x]['horas']?></td>
                                <td>
                                    <a href="#" onclick="SubirAdjunto(<?=$ROW[$x]['linea']?>)"><img  alt="subir" title="Subir Adjunto"  src="../../caspha-i/imagenes/subir.png"/></a>
                                    <?php if(file_exists("../../docs/calidad/inducciones/{$_POST['id']}-{$ROW[$x]['linea']}.zip")): ?>
                                    <a href="../../caspha-i/docs/calidad/inducciones/<?=$_POST['id'] ?>-<?=$ROW[$x]['linea'] ?>.zip" ><img  alt="bajar" title="Descargar Adjunto"  src="../../caspha-i/imagenes/bajar.png"/></a>
                                    <?php endif; ?>
                                </td>
			</tr>
		<?php
		}
	}elseif($_POST['paso'] == '4'){ 
		//VER DETALLE
		$ruta = str_replace('mod', 'guardar', $_POST['imagenes']);
		$ROW = $Personitor->InduccionesDetalleLineaGet($_POST['id'], $_POST['linea']);
		$x = $_POST['linea'];
		?>
		<tr>
			<td align="center"><img src="<?=$ruta?>" id="btn4" class="tab2" onclick="Paso3('<?=$x?>')" title="Guardar l&iacute;nea" /></td>
			<td><select id="tipo<?=$x?>">
				<option value="">N/A</option>
				<option value="S" <?php if($ROW[0]['tipo'] == 'S') echo 'selected';?>>SGC</option>
				<option value="T" <?php if($ROW[0]['tipo'] == 'T') echo 'selected';?>>T&eacute;cnica</option>
				<option value="O" <?php if($ROW[0]['tipo'] == 'O') echo 'selected';?>>Otras</option>
				<option value="I" <?php if($ROW[0]['tipo'] == 'I') echo 'selected';?>>Internacionales</option>
				</select>
			</td>
			<td><input type="text" id="nombre<?=$x?>" value="<?=utf8_encode($ROW[0]['nombre'])?>" size="70" maxlength="150"></td>
			<td><input type="text" id="fecha<?=$x?>" class="fecha" value="<?=$ROW[0]['fecha1']?>" readonly onClick="show_calendar(this.id);"></td>
			<td><input type="text" id="horas<?=$x?>" value="<?=$ROW[0]['horas']?>" size="2" maxlength="3" onblur="_INT(this)"></td>
		</tr>
		<?php
	}elseif($_POST['paso'] == '5'){ 
		$Personitor->InduccionesDetalleLineaSet($_POST['id'],
			$_POST['linea'],
			$_POST['tipo'],
			$_POST['nombre'],
			$_POST['fecha'],
			$_POST['horas']
		);
		$Personitor->InduccionesIndexa($_POST['id']);
		echo 1;
	}elseif($_POST['paso'] == '6'){
		$Robot = new Reportes();
		$Robot->PersonaTablaCapacitacionesSet($_POST['id'], 4);
		$ROW = $Robot->PersonaTablaCapacitacionesGet($_POST['id']);
		if(!$ROW) exit;
		?>
		<table id="datos2" border="1" class="radius" width="400px">
		<tr align="center">
			<td><strong>Tipo de capacitaci&oacute;n</strong></td>
			<td><strong><?=date('Y')?></strong></td>
			<td><strong><?=(date('Y')-1)?></strong></td>
			<td><strong><?=(date('Y')-2)?></strong></td>
			<td><strong><?=(date('Y')-3)?></strong></td>
			<td><strong><?=(date('Y')-4)?></strong></td>
		</tr>
		<?php
		for($x=0;$x<count($ROW)-1;$x++){
		?>
		<tr align="center">
			<td><?=utf8_encode($Personitor->InduccionesTipo($ROW[$x]['tipo']))?></td>
			<td><?=$ROW[$x]['actual']?></td>
			<td><?=$ROW[$x]['menos1']?></td>
			<td><?=$ROW[$x]['menos2']?></td>
			<td><?=$ROW[$x]['menos3']?></td>
			<td><?=$ROW[$x]['menos4']?></td>
		</tr>
		<?php 
		}
		?>
		</table>
		<br />
		<table id="datos3" border="1" class="radius" width="400px">
		<tr align="center">
			<td><strong><?=date('Y')?></strong></td>
			<td><strong><?=(date('Y')-1)?></strong></td>
			<td><strong><?=(date('Y')-2)?></strong></td>
			<td><strong><?=(date('Y')-3)?></strong></td>
			<td><strong><?=(date('Y')-4)?></strong></td>
		</tr>
		<tr align="center">
			<td><?=$ROW[$x]['actual']?></td>
			<td><?=$ROW[$x]['menos1']?></td>
			<td><?=$ROW[$x]['menos2']?></td>
			<td><?=$ROW[$x]['menos3']?></td>
			<td><?=$ROW[$x]['menos4']?></td>
		</tr>
		</table>
		<?php
		$Robot->PersonaTablaCapacitacionesDel($_POST['id']);
	}
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _inducciones_detalle extends Mensajero{	
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['ID']) ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('95') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Personitor = new Calidad();
			return $Personitor->InduccionesEncabezadoGet($_GET['ID']);
		}
	}
	
}
?>