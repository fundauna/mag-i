<?php
/*******************************************************************
 * PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 *******************************************************************/
if (isset($_POST['id'])) {
    function __autoload($_BaseClass)
    {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    $Qualitor = new Calidad();

    if ($Qualitor->EncabezadosIME($_POST['id'], $_POST['codigo'], $_POST['version'], $_POST['nombre'], $_POST['rige'], $_POST['encabezado'], $_POST['pie'], $_POST['enombre'], $_POST['epuesto'], $_POST['efecha'], $_POST['rnombre'], $_POST['rpuesto'], $_POST['rfecha'], $_POST['anombre'], $_POST['apuesto'], $_POST['afecha'])) {
        echo '<script>alert("Realizado con exito");opener.location.reload();location.href="../../../baltha-i/calidad/encabezados.php"; window.close();</script>';
        exit();
    } else
        echo 'Error al guardar los datos';

} else {
    /*******************************************************************
     * DECLARACION CLASE GESTORA
     *******************************************************************/
    function __autoload($_BaseClass)
    {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _encabezados_detalle extends Mensajero
    {
        private $_ROW = array();
        private $Securitor = '';

        function __construct()
        {
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth()) $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /*PERMISOS*/
            if ($this->_ROW['ROL'] != '0') $this->Portero($this->Securitor->UsuarioPermiso('C0'));
            /*PERMISOS*/
            $ceros = '00';
            if ($_GET['codigo'] < 10) {
                $ceros .= '0';
            }
            $_GET['id'] = $_GET['sigla'] . $ceros . $_GET['codigo'];
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function ObtieneDatos()
        {
            $Qualitor = new Calidad();
            $Q = $Qualitor->EncabezadosMuestra($_GET['id']);
            if ($Q) {
                return $Q;
            } else {
                return $Qualitor->EncabezadosMuestraVacio();
            }
        }

        function PlantillaEncabezadosMuestra($_id, $_lab, $_tipo)
        {
            $Qualitor = new Calidad();
            return $Qualitor->PlantillaEncabezadosMuestra($_id, $_lab, $_tipo);
        }
    }
}
?>