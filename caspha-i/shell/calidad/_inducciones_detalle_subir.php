<?php

/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 * ***************************************************************** */
if (isset($_POST['ID'], $_POST['l'], $_FILES['archivo']) && $_FILES['archivo']['size'] > 0) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $accion = false;
    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth())
        die('-0');

    $nombre = $_FILES['archivo']['name'];
    $file = $_FILES['archivo']['tmp_name'];
    $ruta = "../../docs/calidad/inducciones/{$_POST['ID']}-{$_POST['l']}.zip";
    if (file_exists($ruta)) {
        unlink($ruta);
    }
    $zip = new ZipArchive();
    $zip->open($ruta, ZipArchive::CREATE);
    $accion = $zip->addFile($file, $nombre);
    $zip->close();
    ?>
    <?php if ($accion): ?>
        <script type="text/javascript">
            opener.location.reload();
            alert('Archivo Guardado');
            window.close();
        </script>
    <?php else: ?>
        <script type="text/javascript">
            alert('Error al guardar el archivo');
        </script>
    <?php endif; ?>
    <?php

    exit;
} else {
    
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _inducciones_detalle_subir extends Mensajero {

        private $_ROW = array();
        private $Securitor = '';

        function __construct() {
            //SOLO SE PUEDE ABRIR COMO POP-UP MODAL
            $this->ValidaModal();

            if (!isset($_GET['ID']))
                die('Error de parámetros');

            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth())
                $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0')
                $this->Portero($this->Securitor->UsuarioPermiso('95'));
            /* PERMISOS */
        }

    }

}