<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['cs']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');	
	
	$Qualitor = new Calidad();
	echo $Qualitor->DocumentosIME($_POST['cs'],$_POST['version'],$_POST['accion']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _documentos extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('92') );
			/*PERMISOS*/
			if(!isset($_POST['nombre'])){
				$_POST['lab']=$_POST['tipo'] = $_POST['codigo'] = $_POST['nombre'] = '';
				$_POST['lolo'] = '1';
				$this->inicio = true;
			}		
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function DocumentosMuestra(){
			if($this->inicio) return array();
			$Qualitor = new Calidad();
			return $Qualitor->DocumentosMuestra($_POST['tipo'], $_POST['codigo'], $_POST['nombre'], $_POST['lolo'], $this->_ROW['ROL'], $_POST['lab']);
		}
		
		function DocumentosFiltro(){
			$Qualitor = new Calidad();
			return $Qualitor->DocumentosFiltro();
		}

	}
}
?>