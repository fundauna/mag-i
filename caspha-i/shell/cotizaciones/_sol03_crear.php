<?php
$LID = 2; //LABORATORIO LDP
$TIPO = 3; //TIPO
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Listator = new Listador();
	$Listator->AnalisisListaLDP('cotizaciones', $LID, $_GET['tipo'], $_GET['list'], basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
}elseif( isset($_POST['cultivo'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$ROW = $Securitor->SesionGet();
	//
	$Cotizator = new Cotizaciones();
	$cs = $Cotizator->SolicitudNumeroGet($LID, $TIPO);
	
	//INGRESA ENCABEZADO
	$ok = $Cotizator->SolicitudEncabezadoSet($cs, $ROW['UCED'], $TIPO, $_POST['obs'], $ROW['UNAME'], '', $LID);
	
	if($ok){
		$afectada = ''; //ALMACENA SI ES DENSIDAD O GRANULACION
		if( isset($_POST['afectadaA']) ) $afectada .= 'A';
		if( isset($_POST['afectadaB']) ) $afectada .= 'B';
		if( isset($_POST['afectadaC']) ) $afectada .= 'C';
		if( isset($_POST['afectadaD']) ) $afectada .= 'D';
		if( isset($_POST['afectadaE']) ) $afectada .= 'E';
		if( isset($_POST['afectadaF']) ) $afectada .= 'F';
		if( isset($_POST['afectadaG']) ) $afectada .= 'G';
		if( isset($_POST['afectadaH']) ) $afectada .= 'H';
		if( isset($_POST['afectadaI']) ) $afectada .= 'I';
		if( isset($_POST['afectadaJ']) ) $afectada .= 'J';
		if( isset($_POST['afectadaK']) ) $afectada .= 'K';
		if( isset($_POST['afectadaL']) ) $afectada .= 'L';
		
		$Cotizator->SolLDPEncabezadoSet($cs,
			$_POST['cultivo'],
			$_POST['cientifico'],
			$_POST['sintomas'],
			$_POST['plaga'],
			$_POST['tipo'],
			$afectada,
			$_POST['otro'],
			$_POST['provincia'],
			$_POST['canton'],
			$_POST['direccion'],
			$_POST['gravedad'],
			$_POST['etapa'],
			$_POST['area_cultivo'],
			$_POST['area_afectada'],
			$_POST['aplicado'],
			$_POST['riego'],
			$_POST['drenaje'],
			$_POST['suelo'],
			$_POST['programa'],
			$_POST['densidad']
		);
		
		//INGRESA DETALLE ANALISIS
		for($i=0;$i<count($_POST['codigo']);$i++){
			if($_POST['codigo'][$i] != '')
				$Cotizator->SolLDPDetalleAnalisisSet($cs, $_POST['codigo'][$i], $_POST['dpto'][$i], $_POST['cant'][$i]);
		}
		
		//INGRESA DETALLE PLAGUICIDAS
		if(isset($_POST['plag'])){
			for($i=0;$i<count($_POST['plag']);$i++){
				if($_POST['plag'][$i] != '')
					$Cotizator->SolLDPDetallePlaguiSet($cs, $i, $_POST['plag'][$i], $_POST['tip'][$i], $_POST['frecuencia'][$i], $_POST['dosis'][$i], $_POST['fecha'][$i]);
			}
		}
		
		//SI REQUIERE APERTURA, CREA APERTURA INTERNAMENTE
		if( isset($_POST['estacion']) && $_POST['estacion'] != ''){
			$ap = $Cotizator->AperturaNumeroGet();
			$ok = $Cotizator->AperturaEncabezadoSet($ap,
				$_POST['solicitante'],
				$_POST['cultivo'], 
				$_POST['estacion'], 
				$_POST['otro'], 
				$_POST['obs'], 
				0,
				$LID);
					
			if($ok){
				$Cotizator->AperturaAnexa($ap, $cs);
				
				//INGRESA DETALLE ANALISIS
				for($i=0;$i<count($_POST['codigo']);$i++){
					if($_POST['codigo'][$i] != '')
						$Cotizator->AperturaDetalleSet($ap, $_POST['codigo'][$i], $_POST['dpto'][$i], $_POST['cant'][$i]);
				}
				
				//SUBIR ARCHIVO			
				if( isset($_FILES['archivo']) ){
					if($_FILES['archivo']['size'] > 0){
						$nombre = $_FILES['archivo']['name'];
						$file = $_FILES['archivo']['tmp_name'];
						$ruta = "../../docs/aperturas/{$ap}.zip";
						Bibliotecario::ZipCrear($nombre, $file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");	
					}
				}
				
				$Cotizator->AperturaNumeroSet();
				$Cotizator->NotificaJefes($LID, '-5', '-1');
			}
		}else
			$Cotizator->NotificaJefes($LID, '-1');
		
		$Cotizator->SolicitudNumeroSet($LID, $TIPO);
		
		$msj = "Solicitud {$cs} ingresada";
	}else{
		$msj = 'Error al ingresar la solicitud';
	}
	
	header("location: ../../../baltha-i/seguridad/msj.php?msj={$msj}");
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _sol03_crear extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function Tipo(){
			$Servitor = new Sc();
			return $Servitor->ClientesTipoGet($this->_ROW['UID']);
		}
	}
}
?>