<?php
$LID = 3; //LCC
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	$Listator = new Listador();
	$Listator->AnalisisListaTarifa('cotizaciones', $ROW['LID'], $_GET['list'], basename(__FILE__));
	exit;
}elseif( isset($_GET['list2'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	$Listator = new Listador();
	$Listator->ClientesLista('cotizaciones', $ROW['LID'], basename(__FILE__));
	exit;
}elseif( isset($_POST['ModAprueba'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Cotizator = new Cotizaciones();
	echo $Cotizator->ModAprueba($_POST['id'], $_POST['aprobacion']);
	exit;
}elseif( isset($_POST['buscar'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Cotizator = new Cotizaciones();
	echo $Cotizator->SolicitudExiste($_POST['solicitud'], $LID);
	exit;
}elseif( isset($_POST['modificar'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Cotizator = new Cotizaciones();
	$Cotizator->CotizacionModificarEstado($_POST['cotizacion'], $_POST['obs'], $_POST['estado'], $ROW['UID']);
	if($_POST['estado']=='3'){
		//PENDIENTE
		$Cotizator->NotificaJefes($LID, '-3');
	//}elseif($_POST['estado']=='1'){
		//APROBAR
		//$Cotizator->CotizacionNotificaCliente($_POST['cotizacion'], 'Aprobada');
	}elseif($_POST['estado']=='5'){
		//ANULAR
		$Cotizator->CotizacionNotificaCliente($_POST['cotizacion'], $_POST['obs']);
		Bibliotecario::ZipEliminar("../../docs/cotizaciones/{$_POST['cotizacion']}.zip");
	}
	echo 1;
	exit;
}elseif( isset($_POST['documento'])){ //APROBACION
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Cotizator = new Cotizaciones();
	$Cotizator->CotizacionModificarEstado($_POST['ID'], '', '1', $ROW['UID']);
	$Cotizator->CotizacionNotificaCliente($_POST['ID'], 'Aprobada');
	
	header("location: ../../../baltha-i/seguridad/msj.php?msj=Cotización aprobada");
	exit;
}elseif( isset($_POST['deposito'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Cotizator = new Cotizaciones();
	$Cotizator->CotizacionModificarEstado($_POST['ID'], "Deposito: {$_POST['deposito']}", '2', $ROW['UID']);
	$Cotizator->CotizacionNotificaCliente($_POST['ID'], "Depositada,. Ref: {$_POST['deposito']}");
	if($_FILES['archivo']['size'] > 0){
		$nombre = $_FILES['archivo']['name'];
		$file = $_FILES['archivo']['tmp_name'];
		$ruta = "../../docs/cotizaciones/{$_POST['ID']}.zip";
		Bibliotecario::ZipCrear($nombre, $file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");
	}
	
	header("location: ../../../baltha-i/seguridad/msj.php?msj=Cotización finalizada");
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
}elseif( isset($_POST['moneda'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$ROW = $Securitor->SesionGet();
	//
	$Cotizator = new Cotizaciones();
	if($_POST['accion']=='R') $_POST['accion']='I';
	if($_POST['accion']=='I')
		$cs = $Cotizator->CotizacionNumeroGet($LID, $_POST['tipo']);
	else
		$cs = $_POST['ID'];
	
	$_insumos = '';
	if( isset($_POST['insumos']) ) $_insumos .= '1';
	
	//INGRESA ENCABEZADO
	$ok = $Cotizator->CotizacionEncabezadoSetLCC($cs, $_POST['accion'], 
		$_POST['cliente'], 
		$_POST['solicitante'], 
		$_POST['moneda'], 
		$_POST['monto'],
		$_POST['tipo'],
		$LID,
		$_POST['solicitud'],
		$_POST['tipo2'],
		$_insumos,
		$_POST['nombre'],
		$_POST['muestras'],
		$ROW['UID']
	);
	
	if($ok){
		$Cotizator->CotizacionDetalleDel($cs);
		//INGRESA DETALLE
		for($i=0;$i<count($_POST['codigo']);$i++){
			if($_POST['codigo'][$i] != '')
				$Cotizator->CotizacionDetalleSet($cs, $_POST['codigo'][$i], $_POST['cant'][$i], $_POST['item'][$i], $_POST['punit'][$i], $_POST['ptotal'][$i]);
		}
		
		$Cotizator->CotizacionNumeroSet($LID, $_POST['tipo']);
		
		$msj = "Cotización {$cs} ingresada";
	}else{
		$msj = 'Error al ingresar la cotización';
	}
	
	header("location: ../../../baltha-i/seguridad/msj.php?msj={$msj}");
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _cotizacion_LCC extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'R' && $_GET['acc'] != 'M') ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('-2') );
			/*PERMISOS*/
			
			if(!isset($_GET['ID'])) $_GET['ID'] = '';
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Cotizator = new Cotizaciones();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Cotizator->CotizacionEncabezadoVacioLCC();
			elseif($_GET['acc'] == 'R') //IMPORTACION DE UNA SOLICITUD
				return $Cotizator->SolicitudImportaEncabezado($_GET['ID']);
			else
				return $Cotizator->CotizacionEncabezadoGetLCC($_GET['ID']);
		}	
		
		function ObtieneLineas(){
			$Cotizator = new Cotizaciones();
			if( !isset($_GET['ID']) or $_GET['ID']=='' or $_GET['acc']=='R')
				return $Cotizator->CotizacionDetalleVacio();
			//elseif($_GET['acc'] == 'R') //IMPORTACION DE UNA SOLICITUD
				//return $Cotizator->SolicitudImportaDetalle($_GET['ID']);
			else	
				return $Cotizator->CotizacionDetalleGet($_GET['ID']);
		}
		
		function Existe($_tipo, $_cadena){
			if (strpos($_cadena, $_tipo) === false) return false;
			return true;
		}
		
		function Formato($_NUMBER){
			return number_format($_NUMBER, 2, '.', ',');
		}
		
		function Aprobar(){
			if($this->_ROW['ROL'] == '0') return true;
			elseif($this->Securitor->UsuarioPermiso('-3')=='E') return true;
			else return false;
		}
		
		function Estado($_var){
			$Cotizator = new Cotizaciones();
			return $Cotizator->CotizacionEstado($_var);
		}
		
		function ModAprueba(){
			$Cotizator = new Cotizaciones();
			return $Cotizator->ModApruebaMuestra($_GET['ID']);
		}
		
		function Accion($_var){
			$Cotizator = new Cotizaciones();
			return $Cotizator->CotizacionAccion($_var);
		}
		
		function Historial(){
			$Cotizator = new Cotizaciones();
			return $Cotizator->CotizacionHistorialGet($_GET['ID']);
		}
	}
}
?>