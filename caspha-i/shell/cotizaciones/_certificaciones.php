<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['id']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');	
	
	$Cotizator = new Cotizaciones();
	echo $Cotizator->CertificacionAccion($_POST['id'],$_POST['accion'] );
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _certificaciones extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('-6') );
			/*PERMISOS*/
			if(!isset($_POST['estado'])){
				$_POST['desde'] = $_POST['hasta'] = '';
				$_POST['estado'] = 'r';
				$this->inicio = true;
			}
			
			//SI VENIMOS DESDE TABLON, ELIMINO LA NOTIFICACION
			if( isset($_GET['ver']) && isset($_GET['tablon']) )$this->Securitor->TablonDel($_GET['tablon']);
		
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function CertificacionesMuestra(){
			$Cotizator = new Cotizaciones();
			if($this->inicio)
				return array();//$Cotizator->SolicitudPendientesCliente($this->_ROW['UCED']);
			else
				return $Cotizator->CertificacionesGet($this->_ROW['LID'], $_POST['desde'], $_POST['hasta'], $_POST['estado']);		
		}
		
		function Estado($_var){
			$Cotizator = new Cotizaciones();
			return $Cotizator->CertificacionesEstado($_var);
		}
	}
}
?>