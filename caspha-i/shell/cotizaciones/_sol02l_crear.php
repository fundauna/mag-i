<?php
$LID = 3; //LABORATORIO LCC
$TIPO = 2; //TIPO PLAGUICIDAS
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Listator = new Listador();
	$Listator->MacroAnalisisLista('cotizaciones', $LID, $TIPO, $_GET['list'], basename(__FILE__));
	exit;
}elseif( isset($_GET['list2'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Listator = new Listador();
	//$Listator->ImpurezasLista('cotizaciones', $LID, '0', $_GET['list2'], basename(__FILE__));
	$Listator->IMPLista('cotizaciones', $LID, $_GET['formulacion'], $_GET['list2'], basename(__FILE__));
	exit;
}elseif( isset($_GET['list3'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Listator = new Listador();
	$Listator->IALista('cotizaciones', $LID, $_GET['formulacion'], $_GET['list3'], basename(__FILE__));
	exit;
}elseif( isset($_GET['list4'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Listator = new Listador();
	$Listator->ClientesLista('cotizaciones', $LID, basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
}elseif( isset($_POST['nombre_sol'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$ROW = $Securitor->SesionGet();
	//
	$Cotizator = new Cotizaciones();
	$cs = $Cotizator->SolicitudNumeroGet($LID, $TIPO);
	
	//INGRESA ENCABEZADO
	$ok = $Cotizator->SolicitudEncabezadoSet($cs, $_POST['cliente'], $TIPO, $_POST['obs'], $_POST['nombre_sol'], $_POST['encargado'], $LID);
	
	if($ok){
		if(isset($_POST['densidad'])) $_POST['densidad'] = 1;
		else $_POST['densidad'] = 0;
		//INGRESA INFO ESPECIFICA
		$Cotizator->SolPlaguidicasEncabezadoSet($cs, 
			$_POST['contiene'],
			$_POST['tipo_form'],
			$_POST['metodo'],
			$_POST['aporta'],
			$_POST['suministro'],
			$_POST['discre'],
			$_POST['num_sol'],
			$_POST['num_mue'],
			$_POST['total'],
			$_POST['densidad']
		);
		//INGRESA INGREDIENTES ACTIVOS
		for($i=0;$i<count($_POST['codigoE']);$i++){
			if($_POST['codigoE'][$i] != '')
				$Cotizator->SolPlaguicidasDetalleSet($cs, $_POST['codigoE'][$i], $_POST['rangoE'][$i], $_POST['tipoE'][$i], $_POST['fuenteE'][$i], 0);
		}
		//INGRESA ANALISIS
		for($i=0;$i<count($_POST['codigo']);$i++){
			if($_POST['codigo'][$i] != '')
				$Cotizator->SolPlaguicidasDetalleSet($cs, $_POST['codigo'][$i], $_POST['rango'][$i], $_POST['tipo'][$i], $_POST['fuente'][$i], 1);
		}
		//INGRESA IMPUREZAS
		for($i=0;$i<count($_POST['codigoB']);$i++){
			if($_POST['codigoB'][$i] != '')
				$Cotizator->SolPlaguicidasDetalleSet($cs, $_POST['codigoB'][$i], '', '', '', 2);
		}
		$Cotizator->SolicitudNumeroSet($LID, $TIPO);
		$Cotizator->NotificaNuevaCotizacion($cs);
		
		$msj = "Solicitud {$cs} ingresada";
	}else{
		$msj = 'Error al ingresar la solicitud';
	}
	
	header("location: ../../../baltha-i/seguridad/msj.php?msj={$msj}");
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _sol02l_crear extends Mensajero{
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function Formulaciones(){
			$Robot = new Varios();
			return $Robot->FormulacionesGet('P');
		}
	}
}
?>