<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	$Listator = new Listador();
	$Listator->ClientesLista('cotizaciones', $ROW['LID'], basename(__FILE__));
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _cotizaciones extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('-2') );
			/*PERMISOS*/
			if(!isset($_POST['cotizacion'])){
				$_POST['cotizacion'] = $_POST['tmp'] = $_POST['cliente'] = $_POST['desde'] = $_POST['hasta'] = '';
				$_POST['estado'] = 'r';
				$this->inicio = true;
			}
			
			if(!isset($_POST['tipo'])) $_POST['tipo'] = '';
		
			$_GET['LID'] = $this->_ROW['LID'];
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function CotizacionesMuestra(){
			$Cotizator = new Cotizaciones();
			if($this->inicio)
				return $Cotizator->CotizacionPendientesLab($this->_ROW['LID']);
			else
				return $Cotizator->CotizacionResumenGet($_POST['cotizacion'], $_POST['cliente'], $_POST['desde'], $_POST['hasta'], $_POST['tipo'], $_POST['estado'], $this->_ROW['LID']);		
		}
		
		function Estado($_var){
			$Cotizator = new Cotizaciones();
			return $Cotizator->CotizacionEstado($_var);
		}
		
		function Moneda($_var){
			if($_var=='0') return '&cent;';
			elseif($_var=='1') return '$';
		}
		
		function Formato($_NUMBER){
			return number_format($_NUMBER, 2, '.', ',');
		}
	}
}
?>