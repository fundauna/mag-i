<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	$Listator = new Listador();
	$Listator->ClientesLista('cotizaciones', $ROW['LID'], basename(__FILE__));
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _solicitudes extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			$_POST['LID'] = $this->_ROW['LID']; //PARA QUE A APAREZCA EL FILTRO TIPO
			
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('-1') );
			/*PERMISOS*/
			if(!isset($_POST['solicitud'])){
				$_POST['solicitud'] = $_POST['tmp'] = $_POST['cliente'] = $_POST['desde'] = $_POST['hasta'] = '';
				$_POST['estado'] = 'r';
				$this->inicio = true;
			}
			
			if(!isset($_POST['tipo'])) $_POST['tipo'] = '';
			
			//SI VENIMOS DESDE TABLON, ELIMINO LA NOTIFICACION
			if( isset($_GET['ver']) && isset($_GET['tablon']) )$this->Securitor->TablonDel($_GET['tablon']);
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function SolicitudesMuestra(){
			$Cotizator = new Cotizaciones();
			if($this->inicio)
				return $Cotizator->SolicitudPendientesLab($this->_ROW['LID']);
			else
				return $Cotizator->SolicitudLabResumenGet($_POST['solicitud'], $_POST['cliente'], $_POST['desde'], $_POST['hasta'], $_POST['tipo'], $_POST['estado'], $this->_ROW['LID']);		
		}
		
		function LCC(){
			if($this->_ROW['LID'] == '3') return true;
			else return false;
		}
		
		function Estado($_var){
			$Cotizator = new Cotizaciones();
			return $Cotizator->SolicitudEstado($_var);
		}
		
		function Tipo($_var){
			$Cotizator = new Cotizaciones();
			return $Cotizator->SolicitudTipo($_var);
		}
	}
}
?>