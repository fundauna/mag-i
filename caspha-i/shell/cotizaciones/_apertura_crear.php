<?php
$LID = 2; //LABORATORIO LDP

/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Listator = new Listador();
	$Listator->AnalisisListaLDP('cotizaciones', $LID, $_GET['tipo'], $_GET['list'], basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
}elseif( isset($_POST['cultivo'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$ROW = $Securitor->SesionGet();
	//
	$Cotizator = new Cotizaciones();
	$cs = $Cotizator->AperturaNumeroGet();
	
	//INGRESA ENCABEZADO
	$ok = $Cotizator->AperturaEncabezadoSet($cs,
		$ROW['UNAME'], 
		$_POST['cultivo'], 
		$_POST['estacion'], 
		$_POST['otro'], 
		$_POST['obs'], 
		1,
		$LID);
	
	if($ok){
		//INGRESA DETALLE
		for($i=0;$i<count($_POST['codigo']);$i++){
			if($_POST['codigo'][$i] != '')
				$Cotizator->AperturaDetalleSet($cs, $_POST['codigo'][$i], $_POST['cant'][$i], $_POST['dpto'][$i]);
		}
		
		//SUBIR ARCHIVO			
		if( isset($_FILES['archivo']) ){
			if($_FILES['archivo']['size'] > 0){
				$nombre = $_FILES['archivo']['name'];
				$file = $_FILES['archivo']['tmp_name'];
				$ruta = "../../docs/aperturas/{$cs}.zip";
				Bibliotecario::ZipCrear($nombre, $file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");	
			}
		}
		
		$Cotizator->AperturaNumeroSet();
		$Cotizator->NotificaJefes($LID, '-5');
		
		$msj = "Apertura {$cs} ingresada";
	}else{
		$msj = 'Error al ingresar la apertura';
	}
	
	header("location: ../../../baltha-i/seguridad/msj.php?msj={$msj}");
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _apertura_crear extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('-4') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function Tipo(){
			$Servitor = new Sc();
			return $Servitor->ClientesTipoGet($this->_ROW['UID']);
		}
	}
}
?>