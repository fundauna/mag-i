<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['token'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	$Securitor = new Seguridad();
	$_ROW = $Securitor->SesionGet();
	if(!$Securitor ->SesionAuth()) die('-0');	
	
	$Cotizator= new Cotizaciones();
	$ROW = $Cotizator->CertificacionesConsecutivoGet();
	if($Cotizator->CertificacionesIME($ROW[0]['certificaciones'], $_ROW['UCED'])){	
		//SUBIR ARCHIVO			
		if($_FILES['archivo']['size'] > 0){
			$nombre = $_FILES['archivo']['name'];
			$file = $_FILES['archivo']['tmp_name'];
			$year = date('Y');
			$ruta = "../../docs/certificaciones/{$ROW[0]['certificaciones']}-{$year}.zip";
			Bibliotecario::ZipCrear($nombre, $file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");
		}
		//SUBIR ARCHIVO			
		echo'<script>alert("Documento ingresado");opener.location.reload();window.close();</script>';
		exit();
	}else{
		echo 'Error al guardar los datos';
		exit();
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _certificaciones_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M') ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
	}
}
?>