<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['solicitud']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor->SesionAuth()) die('-0');
	
	$Cotizator = new Cotizaciones();
	if($_POST['estado'] == 'a')
		echo $Cotizator->SolicitudAnula($_POST['solicitud'], $_POST['admin'], $_POST['obs']);
	else
		echo $Cotizator->SolicitudAprueba($_POST['solicitud']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _sol03_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			
			if( !isset($_GET['ID']) ) die('Error de parámetros');
			if( !isset($_GET['admin']) ) $_GET['admin'] = ''; //PARA SABER SI SE ABRE DESDE EL LABORATORIO
			elseif($_GET['admin']==1){
				/*PERMISOS*/
				if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('-1') );
				/*PERMISOS*/
			}
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function Aprobar(){
			if($this->_ROW['ROL'] == '0') return true;
			elseif($this->Securitor->UsuarioPermiso('-1')=='E') return true;
			else return false;
		}
		
		function ObtieneDatos(){
			$Cotizator = new Cotizaciones();
			return $Cotizator->SolLDPEncabezadoGet($_GET['ID']);	
		}
		
		function ObtieneAnalisis(){
			$Cotizator = new Cotizaciones();
			return $Cotizator->SolLDPAnalisisGet($_GET['ID']);	
		}
		
		function ObtienePlaguicidas(){
			$Cotizator = new Cotizaciones();
			return $Cotizator->SolLDPPlaguicidasGet($_GET['ID']);	
		}
		
		function Apertura(){
			$Cotizator = new Cotizaciones();
			return $Cotizator->SolAperturaGet($_GET['ID']);
		}
		
		function Estado($_var){
			$Cotizator = new Cotizaciones();
			return $Cotizator->SolicitudEstado($_var);
		}
		
		function AperturaEstado($_var){
			$Cotizator = new Cotizaciones();
			return $Cotizator->CotizacionEstado($_var);
		}
		
		function Historial(){
			$Cotizator = new Cotizaciones();
			return $Cotizator->SolicitudHistorialGet($_GET['ID']);
		}
		
		function Existe($_tipo, $_cadena){
			if (strpos($_cadena, $_tipo) === false) return false;
			return true;
		}
	}
}
?>