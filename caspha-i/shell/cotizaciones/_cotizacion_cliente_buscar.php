<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

final class _cotizacion_cliente_buscar extends Mensajero{
	private $_ROW = array();
	private $Securitor = '';
	private $inicio = false;
	
	function __construct(){
		$this->Securitor = new Seguridad();
		if(!$this->Securitor->SesionAuth()) $this->Err();
		$this->_ROW = $this->Securitor->SesionGet();
		
		if(!isset($_POST['desde'])){
			$_POST['desde'] = $_POST['hasta'] = '';
			$this->inicio = true;
		}
	}

    function Encabezado($_hoja, $_tipo, $_titulo)
    {
        $Qualitor = new Calidad();
        echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
    }
	
	function CotizacionesMuestra(){
		$Cotizator = new Cotizaciones();
		if($this->inicio)
			return array();//$Cotizator->SolicitudPendientesCliente($this->_ROW['UCED']);
		else
			return $Cotizator->CotizacionClienteResumenGet($this->_ROW['UCED'], $_POST['desde'], $_POST['hasta']);		
	}
	
	function Estado($_var){
		$Cotizator = new Cotizaciones();
		return $Cotizator->CotizacionEstado($_var);
	}
	
	function Tipo($_var){
		$Cotizator = new Cotizaciones();
		return $Cotizator->SolicitudTipo($_var);
	}
}
?>