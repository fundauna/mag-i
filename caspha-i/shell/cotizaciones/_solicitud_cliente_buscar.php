<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

final class _solicitud_cliente_buscar extends Mensajero{
	private $_ROW = array();
	private $Securitor = '';
	private $inicio = false;
	
	function __construct(){
		$this->Securitor = new Seguridad();
		if(!$this->Securitor->SesionAuth()) $this->Err();
		$this->_ROW = $this->Securitor->SesionGet();
		
		if(!isset($_POST['estado'])){
			$_POST['desde'] = $_POST['hasta'] = '';
			$_POST['estado'] = 'r';
			$this->inicio = true;
		}
	}

    function Encabezado($_hoja, $_tipo, $_titulo)
    {
        $Qualitor = new Calidad();
        echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
    }
	
	function SolicitudesMuestra(){
		$Cotizator = new Cotizaciones();
		if($this->inicio)
			return array();//$Cotizator->SolicitudPendientesCliente($this->_ROW['UCED']);
		else
			return $Cotizator->SolicitudClienteResumenGet($this->_ROW['UCED'], $_POST['desde'], $_POST['hasta'], $_POST['estado']);		
	}
	
	function Estado($_var){
		$Cotizator = new Cotizaciones();
		return $Cotizator->SolicitudEstado($_var);
	}
	
	function Tipo($_var){
		$Cotizator = new Cotizaciones();
		return $Cotizator->SolicitudTipo($_var);
	}
}
?>