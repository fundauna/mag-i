<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

final class _aperturas extends Mensajero{
	private $_ROW = array();
	private $Securitor = '';
	private $inicio = false;
	
	function __construct(){
		$this->Securitor = new Seguridad();
		if(!$this->Securitor->SesionAuth()) $this->Err();
		$this->_ROW = $this->Securitor->SesionGet();
		
		//SI VENIMOS DESDE TABLON, ELIMINO LA NOTIFICACION
		if( isset($_GET['ver']) && isset($_GET['tablon']) )$this->Securitor->TablonDel($_GET['tablon']);
		
		if(!isset($_POST['estado'])){
			$_POST['desde'] = $_POST['hasta'] = '';
			$_POST['estado'] = '0';
			$this->inicio = true;
		}
	}

    function Encabezado($_hoja, $_tipo, $_titulo)
    {
        $Qualitor = new Calidad();
        echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
    }
	
	function SolicitudesMuestra(){
		$Cotizator = new Cotizaciones();
		if($this->inicio)
			return $Cotizator->AperturaLabPendientes($this->_ROW['LID']);
		else
			return $Cotizator->AperturaLabResumenGet($this->_ROW['LID'], $_POST['desde'], $_POST['hasta'], $_POST['estado']);		
	}
	
	function Estado($_var){
		$Cotizator = new Cotizaciones();
		return $Cotizator->CotizacionEstado($_var);
	}
}
?>