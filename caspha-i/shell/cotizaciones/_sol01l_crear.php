<?php
$LID = 3; //LABORATORIO LCC
$TIPO = 1; //TIPO FERTILIZANTES
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Listator = new Listador();
	$Listator->MacroAnalisisLista('cotizaciones', $LID, $TIPO, $_GET['list'], basename(__FILE__));
	exit;
}elseif( isset($_GET['list2'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Listator = new Listador();
	$Listator->ImpurezasLista('cotizaciones', $LID, 4, $_GET['list2'], basename(__FILE__));
	exit;
}elseif( isset($_GET['list3'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Listator = new Listador();
	$Listator->ClientesLista('cotizaciones', $LID, basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
}elseif( isset($_POST['nombre_sol'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$ROW = $Securitor->SesionGet();
	//
	$Cotizator = new Cotizaciones();
	$cs = $Cotizator->SolicitudNumeroGet($LID, $TIPO);
	
	//INGRESA ENCABEZADO
	$ok = $Cotizator->SolicitudEncabezadoSet($cs, $_POST['cliente'], $TIPO, $_POST['obs'], $_POST['nombre_sol'], $_POST['encargado'], $LID);
	
	if($ok){
		$tipo = ''; //ALMACENA SI ES DENSIDAD O GRANULACION
		if( isset($_POST['tipoE']) ) $tipo .= 'E';
		if( isset($_POST['tipoF']) ) $tipo .= 'F';
		//INGRESA INFO ESPECIFICA
		$Cotizator->SolFertilizantesEncabezadoSet($cs, 
			$_POST['formula'],
			$_POST['comercial'],
			$_POST['tipo_form'],
			$_POST['metodo'],
			$_POST['mezcla'],
			$_POST['aporta'],
			$tipo,
			$_POST['suministro'],
			$_POST['discre'],
			$_POST['num_sol'],
			$_POST['num_mue'],
			$_POST['total']
		);
		//INGRESA DETALLE
		for($i=0;$i<count($_POST['codigo']);$i++){
			if($_POST['codigo'][$i] != '')
				$Cotizator->SolFertilizantesDetalleSet($cs, $_POST['codigo'][$i], $_POST['rango'][$i], $_POST['quela'][$i], $_POST['tipo'][$i], $_POST['fuente'][$i], 1);
		}
		//INGRESA IMPUREZAS
		for($i=0;$i<count($_POST['codigoB']);$i++){
			if($_POST['codigoB'][$i] != '')
				$Cotizator->SolFertilizantesDetalleSet($cs, $_POST['codigoB'][$i], '', '', '', '', 2);
		}
		$Cotizator->SolicitudNumeroSet($LID, $TIPO);
		$Cotizator->NotificaNuevaCotizacion($cs);
		
		$msj = "Solicitud {$cs} ingresada";
	}else{
		$msj = 'Error al ingresar la solicitud';
	}
	
	header("location: ../../../baltha-i/seguridad/msj.php?msj={$msj}");
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _sol01l_crear extends Mensajero{
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function Formulaciones(){
			$Robot = new Varios();
			return $Robot->FormulacionesGet('F');
		}
	}
}
?>