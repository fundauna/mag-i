<?php
$LID = 1; //LABORATORIO LRE
$TIPO = 0; //TIPO
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Listator = new Listador();
	$Listator->AnalisisLista('cotizaciones', $LID, $_GET['list'], basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
}elseif( isset($_POST['comercial'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$ROW = $Securitor->SesionGet();
	//
	$Cotizator = new Cotizaciones();
	$cs = $Cotizator->SolicitudNumeroGet($LID, $TIPO);
	
	//INGRESA ENCABEZADO
	$ok = $Cotizator->SolicitudEncabezadoSet($cs, $ROW['UCED'], $TIPO, $_POST['obs'], $_POST['comercial'], '', $LID);
	
	if($ok){
		//INGRESA DETALLE
		for($i=0;$i<count($_POST['codigo']);$i++){
			if($_POST['codigo'][$i] != '')
				$Cotizator->SolLREDetalleSet($cs, $_POST['codigo'][$i], $_POST['cant'][$i]);
		}
		
		$Cotizator->SolicitudNumeroSet($LID, $TIPO);
		$Cotizator->NotificaJefes($LID, '-1');
		
		$msj = "Solicitud {$cs} ingresada";
	}else{
		$msj = 'Error al ingresar la solicitud';
	}
	
	header("location: ../../../baltha-i/seguridad/msj.php?msj={$msj}");
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _sol00_crear extends Mensajero{
		//private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
	}
}
?>