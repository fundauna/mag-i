<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

final class _detalle_LRE extends Mensajero{
	private $Securitor = '';
	private $inicio = false;
	
	function __construct(){
		//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
		$this->ValidaModal();
		
		if( !isset($_GET['ID']) ) die('Error de parámetros');
		
		$this->Securitor = new Seguridad();
		if(!$this->Securitor->SesionAuth()) $this->Err();
	}

    function Encabezado($_hoja, $_tipo, $_titulo)
    {
        $Qualitor = new Calidad();
        echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
    }
	
	function ObtieneDatos(){
		$Cotizator = new Cotizaciones();
		return $Cotizator->CotizacionEncabezadoGetLCC($_GET['ID']);
	}	
	
	function ObtieneLineas(){
		$Cotizator = new Cotizaciones();
		return $Cotizator->CotizacionDetalleGet($_GET['ID']);
	}
	
	function ObtieneAnulacion(){
		$Cotizator = new Cotizaciones();
		return $Cotizator->RegistroGet($_GET['ID']);	
	}
		
	function Formato($_NUMBER){
		return number_format($_NUMBER, 2, '.', ',');
	}
	
	function Estado($_var){
		$Cotizator = new Cotizaciones();
		return $Cotizator->CotizacionEstado($_var);
	}
}
?>