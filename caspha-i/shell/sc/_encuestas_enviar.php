<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['id']) ) die('-1');
	
	$Servitor = new Sc();
	$Securitor = new Seguridad();
	
	if(!$Securitor ->SesionAuth()) die('-0');	
	echo $Servitor->EncuestaEnvia($_POST['id'], $_POST['clientes']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _encuestas_enviar extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['ID']) ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('52') );
			/*PERMISOS*/
                        if(!isset($_GET['T'])){
                            $_GET['T'] = 'N';
                        }
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Servitor = new Sc();
			return $Servitor->EncuestaDetalle($_GET['ID']);
		}
		
		function ClientesMuestra(){
			$Servitor = new Sc();
			return $Servitor->ClientesResumenGet('', '', $this->_ROW['LID'], 1, true);
		}
	}
}
?>