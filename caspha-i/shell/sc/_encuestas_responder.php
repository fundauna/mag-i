<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['tipos']) ) die('-1');
	
	$Servitor = new Sc();
	$Securitor = new Seguridad();
	
	if(!$Securitor ->SesionAuth()) die('-0');	
	$_ROW = $Securitor->SesionGet();
	
	echo $Servitor->EncuestaResponde($_ROW['UID'], $_POST['tipos'], $_POST['preguntas1'], $_POST['respuestas1'], $_POST['respuestas2']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _encuestas_responder extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			//$this->ValidaModal();
			
			if( !isset($_GET['ID'])) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Servitor = new Sc();
			return $Servitor->EncuestaDetalle($_GET['ID']);
		}
		
		function GetOpciones($id){
			$Servitor = new Sc();
			return $Servitor->GetOpciones($_GET['ID'],$id);
		}
		
		function VistaPrevia(){
			$Servitor = new Sc();
			return $Servitor->VistaPrevia($_GET['ID']);
		}
		
	}
}
?>