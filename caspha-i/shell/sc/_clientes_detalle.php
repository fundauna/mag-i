<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	//if( !isset($_POST['id']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	
	$Servitor = new Sc();
        if($_POST['_AJAX']=='1'){
	echo $Servitor->ClientesIME($_POST['id'], 
		$_POST['nombre'], 
		$_POST['unidad'],  
		$_POST['representante'],
		$_POST['tel'],
		$_POST['email'],
		/**/
		$_POST['contacto'],
		$_POST['telefonos'], 
		$_POST['correo'],  
		/**/
		$_POST['solicitante'],
		$_POST['soltel'], 
		$_POST['solemail'],  
		/**/
		$_POST['fax'],   
		$_POST['direccion'], 
		$_POST['actividad'], 
		$_POST['tipo'], 
		$_POST['LRE'], 
		$_POST['LDP'], 
		$_POST['LCC'], 
		$_POST['estado'], 
        $_POST['accion']);}else{
            echo $Servitor->Clientes_CambiaID($_POST['idActual'], $_POST['idNuevo']);
        }
	exit;
}elseif( isset($_POST['_REENVIAR'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['id']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Servitor = new Sc();
	echo $Servitor->ClientesReenvio($_POST['id'], $_POST['email']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _clientes_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $clave = false;
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M') ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('53') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function Clave(){
			if($this->_ROW['ROL'] == '0') return true;
			elseif($this->Securitor->UsuarioPermiso('54')=='E') return true;
			else return false;
		}
		
		function ObtieneDatos(){
			$Servitor = new Sc();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Servitor->ClientesVacio();
			else	
				return $Servitor->ClientesDetalleGet($_GET['ID']);
		}
		
		function ClientesContactos(){
			$Servitor = new Sc();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Servitor->ClientesContactosVacio();
			else	
				return $Servitor->ClientesContactosGet($_GET['ID']);
		}
		
		function ClientesSolicitantes(){
			$Servitor = new Sc();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Servitor->ClientesSolicitantesVacio();
			else	
				return $Servitor->ClientesSolicitantesGet($_GET['ID']);
		}
	}
}
?>