<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['id']) ) die('-1');
	
	$Servitor = new Sc();
	$Securitor = new Seguridad();
	
	if(!$Securitor ->SesionAuth()) die('-0');
	echo $Servitor->EncuestaIME($_POST['id'], '', '', $_POST['accion']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _encuestas extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('52') );
			/*PERMISOS*/
			if(!isset($_GET['estado'])){
				$_GET['estado'] = '';
				$this->inicio = true;
			}
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function EncuestaMuestra(){
			if($this->inicio) return array();
			$Servitor = new Sc();
			return $Servitor->EncuestaMuestra($_GET['estado']);
		}
		
	}
}
?>