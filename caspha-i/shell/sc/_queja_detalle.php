<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$_ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	$Listator->UsuariosLista('sc', basename(__FILE__), $_GET['linea'], $_ROW['LID']);
	exit;
}elseif( isset($_POST['accion'])){ 
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$_ROW = $Securitor->SesionGet();
	
	$Servitor = new Sc();
	if($_POST['accion']=='I') $_POST['cs'] = $Servitor->QuejasConsecutivo();
	
	if( $Servitor->QuejasIME($_POST['cs'], $_POST['codigo'], $_POST['fec_pre'], $_POST['fec_lim'], $_POST['fec_rec'], $_POST['instancia'], $_POST['usuarioA'], $_POST['fec_res'], $_POST['acap'], $_POST['usuarioB'], $_POST['fec_cal'], $_POST['fec_ser'], $_POST['fec_cli'], $_POST['obs'], $_POST['estado'], $_POST['accion']) ){

		//SUBIR ARCHIVO
		if($_FILES['archivo']['size'] > 0){
			$nombre = $_FILES['archivo']['name'];
			$file = $_FILES['archivo']['tmp_name'];
			$ruta = "../../docs/sc/{$_POST['cs']}.zip";
			Bibliotecario::ZipCrear($nombre, $file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");	
		}
			
		echo'<script>alert("Transaccion Finalizada");opener.location.reload();window.close();</script>';
		exit();	
	}else
		echo 'Error al guardar los datos';
		
	exit();	
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _quejas_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M') ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Servitor = new Sc();
			if( !isset($_GET['ID']) or $_GET['ID']==''){
				$_GET['ID'] = '';
				return $Servitor->QuejasDetalleVacio();
			}else	
				return $Servitor->QuejasDetalleGet($_GET['ID']);
		}
		
		function ObtieneResponsables(){
			$Servitor = new Sc();
			if( $_GET['ID']=='')
				return $Servitor->QuejasResponsablesVacio();
			else	
				return $Servitor->QuejasResponsablesGet($_GET['ID']);
		}
	}
}
?>