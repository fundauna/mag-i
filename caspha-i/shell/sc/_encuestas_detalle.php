<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['nombre']) ) die('-1');
	
	$Servitor = new Sc();
	$Securitor = new Seguridad();
	
	if(!$Securitor ->SesionAuth()) die('-0');	
	echo $Servitor->EncuestaIME($_POST['id'], $_POST['nombre'], $_POST['estado'], $_POST['accion']);
	exit;
	
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN FORM
*******************************************************************/
}elseif( isset($_POST['accion'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	$Servitor = new Sc();
	if($Servitor->EncuestaIME($_POST['id'], $_POST['nombre'], $_POST['estado'], $_POST['accion']) == 1){		
		//SUBIR ARCHIVO			
		if($_FILES['archivo']['size'] > 0){
			$nombre = $_FILES['archivo']['name'];
			$file = $_FILES['archivo']['tmp_name'];
			$ruta = "../../docs/sc/encuestas/{$_POST['id']}.zip";
			Bibliotecario::ZipCrear($nombre, $file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");
		}
		//SUBIR ARCHIVO			
			echo'<script>alert("Documento ingresado");opener.location.reload();window.close();</script>';
			exit();	
	}else
		echo 'Error al guardar los datos';
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _encuestas_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M') ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('52') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Servitor = new Sc();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Servitor->EncuestaVacio();
			else	
				return $Servitor->EncuestaDetalle($_GET['ID']);
		}
		
		function PerfilesMuestra(){
			return $this->Securitor->PerfilesMuestra();
		}

	}
}
?>