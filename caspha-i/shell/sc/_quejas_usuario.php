<?php
/*******************************************************************
 * PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 *******************************************************************/
if (isset($_POST['_AJAX'])) {
    function __autoload($_BaseClass)
    {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    if (!isset($_POST['cs'])) die('-1');

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth()) die('-0');
    $_ROW = $Securitor->SesionGet();

    $Servitor = new Sc();
    echo $Servitor->QuejasIME($_POST['cs'], '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'D');
    exit;
} else {
    /*******************************************************************
     * DECLARACION CLASE GESTORA
     *******************************************************************/
    function __autoload($_BaseClass)
    {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _quejas extends Mensajero
    {
        private $_ROW = array();
        private $Securitor = '';
        private $inicio = false;

        function __construct()
        {
            $this->Securitor = new Seguridad();
            if(!$this->Securitor->SesionAuth()) $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            if (!isset($_POST['desde'])) {
                $_POST['desde'] = $_POST['hasta'] = date('d-m-Y');
                $this->inicio = true;
            }
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function QuejasMuestra()
        {
            if ($this->inicio) return array();
            $Servitor = new Sc();
            return $Servitor->QuejasResumenGet(1, 3, $_POST['desde'], $_POST['hasta'], $_POST['instancia']);
        }

        function Tipo($_tipo)
        {
            $Servitor = new Sc();
            return $Servitor->QuejasInstancia($_tipo);
        }
    }
}
?>