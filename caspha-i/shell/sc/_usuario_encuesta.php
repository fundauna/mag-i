<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['id']) ) die('-1');
	
	$Servitor = new Sc();
	$Securitor = new Seguridad();
	
	if(!$Securitor ->SesionAuth()) die('-0');	
	echo $Servitor->OpcionesIME($_POST['id'], $_POST['linea'], $_POST['descr'], $_POST['accion']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _usuario_encuesta extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
		}
		
		function OpcionesMuestra(){
			$Servitor = new Sc();
			return $Servitor->OpcionesMuestra($_GET['ID']);
		}
		
		function GetEncuesta(){
			$Servitor = new Sc();
			return $Servitor->GetEncuesta($_GET['ID']);
		}
		
		function GetOpciones($id){
			$Servitor = new Sc();
			return $Servitor->GetOpciones($_GET['ID'],$id);
		}
		
		function VistaPrevia(){
			$Servitor = new Sc();
			return $Servitor->VistaPrevia($_GET['ID']);
		}
		
	}
}
?>