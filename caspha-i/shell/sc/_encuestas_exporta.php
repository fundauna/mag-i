<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _encuestas_exporta extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			//$this->ValidaModal();
			
			if( !isset($_GET['ID'])) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('52') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function GetEncuesta(){
			$Servitor = new Sc();
			return $Servitor->GetEncuesta($_GET['ID']);
		}
		
		function Paso0(){
			$Servitor = new Sc();
			return $Servitor->ExportaPaso0($_GET['ID']);
		}
		
		function Paso1($_tipo){
			$Servitor = new Sc();
			return $Servitor->ExportaPaso1($_GET['ID'], $_tipo);
		}
		
		function Paso2(){
			$Servitor = new Sc();
			return $Servitor->ExportaPaso2($_GET['ID']);
		}
	}
?>