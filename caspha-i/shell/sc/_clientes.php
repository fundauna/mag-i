<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['id']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$ROW = $Securitor->SesionGet();
	
	$Servitor = new Sc();
	echo $Servitor->ClientesIME($_POST['id'], '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', $_POST['estado'], $_POST['accion']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _clientes extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('53') );
			/*PERMISOS*/
			if(!isset($_POST['nombre'])){
				$_POST['id'] = $_POST['nombre'] = '';
				$_POST['lolo'] = $_POST['filtro'] = '1';
				$this->inicio = true;
			}
		}

		function Encabezado($_hoja, $_tipo, $_titulo)
		{
			$Qualitor = new Calidad();
			echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
		}
		
		function ClientesMuestra(){
			if($this->inicio) return array();
			else{
				$Servitor = new Sc();
				return $Servitor->ClientesResumenGet($_POST['id'], $_POST['nombre'], $_POST['filtro'], $this->_ROW['LID'], $_POST['lolo']);		
			}
		}

		function obtieneLAB()
		{
			return $this->_ROW['LID'];
		}
	}
}
?>
