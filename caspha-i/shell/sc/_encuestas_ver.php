<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	$Listator = new Listador();
	$Listator->ClientesLista('sc', $ROW['LID'], basename(__FILE__));
	exit;
}
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['id']) ) die('-1');
	
	$Servitor = new Sc();
	$Securitor = new Seguridad();
	
	if(!$Securitor ->SesionAuth()) die('-0');	
	echo $Servitor->OpcionesIME($_POST['id'], $_POST['linea'], $_POST['descr'], $_POST['accion']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _encuestas_ver extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			//$this->ValidaModal();
			
			if( !isset($_GET['ID'])) die('Error de parámetros');
			if( !isset($_POST['client'])){
				$_POST['client'] = $_POST['cliente'] = '';				
			}
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('52') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function OpcionesMuestra(){
			$Servitor = new Sc();
			return $Servitor->OpcionesMuestra($_GET['ID']);
		}
		
		function GetEncuesta(){
			$Servitor = new Sc();
			return $Servitor->GetEncuesta($_GET['ID']);
		}
		
		function GetOpciones($id){
			$Servitor = new Sc();
			return $Servitor->GetOpciones($_GET['ID'], $id);
		}
		
		function GetCheckbox($id){
			$Servitor = new Sc();
			return $Servitor->GetCheckbox($_GET['ID'],$id,$_POST['client']);
		}
		
		function Respuestas(){
			if($_POST['cliente'] == '') return array();
			
			$Servitor = new Sc();
			return $Servitor->GetRespuestas($_GET['ID'], $_POST['client']);
		}
		
	}
}
?>