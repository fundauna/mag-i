<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['id']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Provitor = new Proveedores();
	echo $Provitor->EvaIME($_POST['id'],'D', '', '', '', '', $_ROW['UID'],'', '', '', '', '', '', '', '', '', '', '');
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _evaluaciones extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('B3') );
			/*PERMISOS*/
			if(!isset($_POST['nombre'])){
				$_POST['desde'] = $_POST['hasta'] = date('d-m-Y');				
				$_POST['nombre'] = '';
				$this->inicio = true;
			}
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function EvasMuestra(){
			$Provitor = new Proveedores();
			if($this->inicio)
				return array();
			else
				return $Provitor->EvasResumenGet($_POST['desde'], $_POST['hasta'], $_POST['nombre']);		
		}
	}
}
?>