<?php

/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 * ***************************************************************** */
if (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }


    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth())
        die('-0');
    $_ROW = $Securitor->SesionGet();

    $Provitor = new Proveedores();
    if($_POST['_AJAX']=='1'){
    echo $Provitor->ProveedoresIME($_POST['cs'], $_POST['accion'], $_POST['tipo'], $_POST['naturaleza'], $_POST['id'], $_POST['nombre'], $_POST['direccion'], $_POST['contacto'], $_POST['area'], $_POST['telefonos'], $_POST['extension'], $_POST['directo'], $_POST['Otelefono'], $_POST['correo'], $_POST['Ocorreo'], $_POST['telefono'], $_POST['fax'], $_POST['otro'], $_POST['email'], $_POST['notas'], $_POST['clasificacion'], $_POST['LRE'], $_POST['LDP'], $_POST['LCC'], $_POST['critico']);
    }elseif($_POST['_AJAX']=='2'){
        echo $Provitor->ProveedoresContactos_IME($_POST['cs'], $_POST['accion'], $_POST['contacto'], $_POST['area'], $_POST['telefono'], $_POST['extension'], $_POST['directo'], $_POST['Otelefono'], $_POST['correo'], $_POST['Ocorreo'],$_POST['borrar']);
    }
    exit;
}else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _proveedores_detalle extends Mensajero {

        private $_ROW = array();
        private $Securitor = '';

        function __construct() {
            //SOLO SE PUEDE ABRIR COMO POP-UP MODAL
            //$this->ValidaModal();

            if (!isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M'))
                die('Error de parámetros');
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth())
                $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0')
                $this->Portero($this->Securitor->UsuarioPermiso('B2'));
            /* PERMISOS */
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function ObtieneDatos() {
            $Provitor = new Proveedores();
            if (!isset($_GET['ID']) or $_GET['ID'] == '')
                return $Provitor->ProveedoresVacio();
            else
                return $Provitor->ProveedoresDetalleGet($_GET['ID']);
        }

        function ProveedoresContactos() {
            $Provitor = new Proveedores();
            if (!isset($_GET['ID']) or $_GET['ID'] == '')
                return $Provitor->ProveedoresContactosVacio();
            else
                return $Provitor->ProveedoresContactosGet($_GET['ID']);
        }

        function ProveedoresFiltro() {
            $Provitor = new Proveedores();
            return $Provitor->ClasificacionesMuestra($_GET['acc'] == 'I'?$this->_ROW['LID']:'');
        }
        
        function get_ROW() {
            return $this->_ROW['LID'];
        }
    }

}
?>