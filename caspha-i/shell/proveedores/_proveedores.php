<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['id']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	
	$Provitor = new Proveedores();
	echo $Provitor->ProveedoresIME($_POST['id'], $_POST['accion'], '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');	
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _proveedores extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('B2') );
			/*PERMISOS*/
			if(!isset($_POST['nombre'])){
				$_POST['tipo'] = $_POST['id'] = $_POST['nombre'] = '';
				$_POST['lolo'] = $_POST['filtro'] = '1';
				$this->inicio = true;
			}
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ProveedoresMuestra(){
			if($this->inicio) return array();
			else{
				$Provitor = new Proveedores();
				return $Provitor->ProveedoresResumenGet($_POST['id'], $_POST['nombre'], $_POST['filtro'], $_POST['tipo'], $this->_ROW['LID'], $_POST['lolo']);		
			}
		}
		
		function ProveedoresFiltro(){
			$Provitor = new Proveedores();
			return $Provitor->ClasificacionesMuestra($this->_ROW['LID']);
		}
	}
}
?>