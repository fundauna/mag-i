<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	$Listator = new Listador();
	$Listator->ProveedoresLista('proveedores', $ROW['LID'], basename(__FILE__));
	exit;
}
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['id']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Provitor = new Proveedores();
	echo $Provitor->EvaIME($_POST['id'],
		$_POST['accion'], 
		$_POST['prov'], 
		$_POST['orden'], 
		$_POST['lineas'], 
		$_POST['total'], 
		$_ROW['UID'],
		$_POST['precio1'], 
		$_POST['precio2'], 
		$_POST['porc'], 
		$_POST['calif1'], 
		$_POST['calif2'], 
		$_POST['calif3'], 
		$_POST['calif4'], 
		$_POST['calif5'], 
		$_POST['calif6'], 
		$_POST['calif7'], 
		$_POST['calif8']);	
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _evaluaciones_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'V') ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('B3') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function Lab(){
			if($this->_ROW['LID'] == '1') return 'Laboratorio de análisis de residuos de agroquímicos';
			elseif($this->_ROW['LID'] == '2') return 'Laboratorio central de diagnóstico de plagas';
			elseif($this->_ROW['LID'] == '3') return 'Laboratorio de control de calidad de agroquímicos';
		}
		
		function ObtieneDatos(){
			$Provitor = new Proveedores();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Provitor->EvaVacio();
			else
				return $Provitor->EvasDetalleGet($_GET['ID']);
		}
		
		function Formato($_NUMBER){
			return number_format($_NUMBER, 2, '.', ',');
		}
	}
}
?>