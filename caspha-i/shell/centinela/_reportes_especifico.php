<?php
/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
 * ***************************************************************** */

function __autoload($_BaseClass) {
    require_once "../../../melcha-i/{$_BaseClass}.php";
}

$Robot = new Reportes();

$titulo = "<p><b>Resultados de b&uacute;squeda espec&iacute;fica de an&aacute;lisis</b></p> <p>Consulta realizada el " . date('d/m/Y H:i') . "</p>";
$Robot->TableHeader($titulo, 1);
$ROW = $Robot->obtieneEspecificosCentinela($_POST['solicitud'], $_POST['informe']);
?>
<table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='40%'>
    <tr align="center">
        <td><strong>Informe</strong></td>
        <td><strong>Tipo</strong></td>
        <td><strong>Solicitud</strong></td>
    </tr>
    <tr><td colspan="14"><hr /></td></tr>
    <?php
    for ($x = 0; $x < count($ROW); $x++) {
        ?>
        <tr align="center">
            <td><a href="_reporte_especifico.php?INF=<?= $ROW[$x]['informe'] ?>&TIP=<?= $ROW[$x]['tipo'] ?>" target="_self"><?= $ROW[$x]['informe'] ?></a></td>
            <td><a href="_reporte_especifico.php?INF=<?= $ROW[$x]['informe'] ?>&TIP=<?= $ROW[$x]['tipo'] ?>" target="_self"><?= $ROW[$x]['tipo'] == '2' ? 'Fertilizantes' : 'Plaguicidas' ?></a></td>
            <td><a href="_reporte_especifico.php?INF=<?= $ROW[$x]['informe'] ?>&TIP=<?= $ROW[$x]['tipo'] ?>" target="_self"><?= $ROW[$x]['solicitud'] ?></a></td>               
        </tr>
    <?php }//FOR   ?>
    <tr align="center">
        <td colspan="14" align="right"><strong>Total de resultados:</strong></td>
        <td><?= $x-- ?></td>
    </tr>
</table>