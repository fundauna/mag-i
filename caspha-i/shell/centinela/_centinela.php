<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['desde'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if($_POST['formato'] == '1'){
		header('Content-type: application/x-msdownload'); 
		header('Content-Disposition: attachment; filename=bitacora.xls'); 
		header('Pragma: no-cache'); 
		header('Expires: 0');
	}

	$Robot = new Reportes();
	$Robot->TableHeader("Reporte de an�lisis<br>{$_POST['desde']} a {$_POST['hasta']}");
	$ROW = $Robot->AnalisisHistorial($_POST['lab'], $_POST['desde'], $_POST['hasta'], $_POST['analisis'], $_POST['estado']);
	?>
	<table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
	<tr>
		<td><strong>Categor&iacute;a</strong></td>
		<td><strong>An&aacute;lisis</strong></td>
		<td><strong>Solicitud</strong></td>
                <td><strong>Muestra</strong></td>
		<td><strong>Tipo</strong></td>
		<td><strong>Fecha</strong></td>
		<td><strong>Estado</strong></td>
	</tr>
	<tr><td colspan="7"><hr /></td></tr>
	<?php
	for($x=0;$x<count($ROW);$x++){
	?>
	<tr>
		<td><?=$Robot->AnalisisCategorias($ROW[$x]['categoria'])?></td>
		<td><?=$ROW[$x]['analisis']?></td>
		<td><?=$ROW[$x]['solicitud']?></td>
		<td><?=$_POST['analisis']=='24'||$_POST['analisis']=='25'? $ROW[$x]['muestra'] : $Robot->SolicitudMuestra($ROW[$x]['solicitud'], $ROW[$x]['examen'])?></td>
                <td><?=$Robot->SolicitudTipo($ROW[$x]['tipo'])?></td>
		<td><?=$ROW[$x]['fecha1']?></td>
		<td><?=$Robot->SolicitudEstado($ROW[$x]['estado'])?></td>
	</tr>
	<?php
	}
	echo "<tr>
		<td colspan='5' align='right'><strong>Total:</strong></td>
		<td><strong>{$x}</strong></td>
	</tr></table>";
	
	$Robot->TableFooter();
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	class _centinela extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			
			$_POST['LID'] = $this->_ROW['LID'];
		}
//agrergada
        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function obtieneIngredientesCentinela(){
			$Robot = new Reportes();
			return $Robot->obtieneIngredientesCentinela();
		}
                
                function obtieneProductosCentinela(){
			$Robot = new Reportes();
			return $Robot->obtieneProductosCentinela();
		}
		
		function Tipo($_tipo){
			$Robot = new Reportes();
			return $Robot->AnalisisCategorias($_tipo);
		}
		
	}
}
?>