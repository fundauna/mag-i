<?php
/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
 * ***************************************************************** */
if (isset($_GET['INF'])) {

    if ($_GET['TIP'] == '3') {
        header('location: _reporte_especifico_1.php?INF=' . $_GET['INF'] . '&TIP=' . $_GET['TIP']);
        exit;
    }

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Robot = new Reportes();
    $A = $Robot->obtieneInformeCentinela($_GET['INF'], $_GET['TIP']);
    $B1 = $Robot->obtieneMuestrasFertTotalCentinela($A[0]['id']);    
    $E = $Robot->obtieneEstadoCentinela($A[0]['id']);
    $F = $Robot->obtieneFechaIngresoCentinela($A[0]['id_solicitud']);
    ?>
    <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%'>
        <tr>
            <td><strong style="font-size: 14px;">Ministerio de Agricultura y Ganader&iacute;a</strong></td>
        </tr>
        <tr>
            <td>Servicio Fitosanitario del Estado</td>
            <td align="right">Tel. 2549-3606</td>
        </tr>
        <tr>
            <td>Departamento de Laboratorios</td>
            <td align="right">Fax. 2549-3599</td>
        </tr>
        <tr>
            <td>Laboratorio de Control de Calidad de Agroqu&iacute;micos</td>
        </tr> 
        <tr>
            <td>Fertilizantes</td>
        </tr> 
    </table>
    <center><h2>Informe de An&aacute;lisis <?= $_GET['INF'] ?></h2></center>
    <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%'>
        <tr><td colspan="5"><hr></td></tr>
        <tr>
            <td>Solicitud:</td>
            <td><?= $A[0]['id_solicitud'] ?></td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>Ingreso de la muestra:</td>
            <td><?= $F[0]['d_Estado'] ?></td>
        </tr>
        <tr>
            <td>Dependencia:</td>
            <td><?= $A[0]['dependencia'] ?></td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>Conclusi&oacute;n del an&aacute;lisis:</td>
            <td><?= $A[0]['f_conclusion'] ?></td>
        </tr>
        <tr>
            <td>Solicitante:</td>
            <td><?= $A[0]['solicitante'] ?></td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>Reporte:</td>
            <td><?= $E[0]['d_Estado'] ?></td>
        </tr>
        <tr><td><br></td></tr>        
    </table>
    <?php 
        for ($p = 0; $p < count($B1); $p++) { 
            $B = $Robot->obtieneMuestrasFertCentinela($A[0]['id'], $B1[$p]['id_muestra']);
    ?>
        <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%'>
            <tr style="background-color: #000; color: #FFF; font-size: 14px;">
                <td colspan="7"><b>Nombre comercial: <?= $B[0]['producto'] ?></b></td>
            </tr>
            <tr>
                <td>No. Muestra:</td>
                <td><?= $B[0]['codigo_interno'] ?></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>            
                <td>No. registro</td>
                <td><?= $B[0]['registro'] ?></td>
                <td>Formulaci&oacute;n:</td>
                <td></td>
            </tr>
            <tr>
                <td>Cinta:</td>
                <td><?= $B[0]['codigo_externo'] ?></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>Lote fab.:</td>
                <td><?= $B[0]['lote'] ?></td>
                <td><?= $B[0]['formulacion'] ?></td>
                <td></td>
            </tr>
            <tr>
                <td>Sellada:</td>
                <td><?= $B[0]['sello'] ? 'Si' : 'No' ?></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>Recipiente:</td>
                <td><?= $B[0]['recipiente'] ?></td>
                <td></td>
                <td></td>
            </tr>
            <tr><td><br></td></tr>
            <tr style="font-size: 14px;">
                <td colspan="5"><b>An&aacute;lisis Qu&iacute;mico RTCR 228</b></td>
            </tr>
        </table>
        <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%'>
            <tr>
                <td><u>Elemento</u></td>
                <td><u>U.C.</u></td>
                <td><u>C.D.</u></td>
                <td><u>C.E.</u></td>
                <td><u>I.C.</u></td>
                <td><u>L.I.</u></td>
                <td><u>L.S.</u></td>
                <td><u>CPL</u></td>
                <td><u>M&eacute;todo</u></td>
            </tr>
            <?php for ($i = 0; $i < count($B); $i++) { ?>
                <tr>
                    <td><?= $B[$i]['ingrediente'] ?></td>
                    <td><?= $B[$i]['metrica'] ?></td>
                    <td><?= $B[$i]['declarada'] ?></td>
                    <td><?= $B[$i]['encontrada'] ?></td>
                    <td><?= $B[$i]['incertidumbre'] ?></td>
                    <td><?= $B[$i]['limite_inferior'] ?></td>
                    <td><?= $B[$i]['limite_superior'] ?></td>
                    <td><?= $B[$i]['cumple'] ? 'Si' : 'No' ?></td>
                    <td><?= $B[$i]['metodo'] ?></td>
                </tr>
            <?php } ?>    
            <tr><td><br></td></tr>
        </table>
        <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%'>
            <?php 
                $C = $Robot->obtieneDensidadCentinela($A[0]['id'], $B1[$p]['id_muestra']);                 
                if ($C && ($C[0]['densidad'] != 0 || $C[0]['metodo'] != '')) { ?>
                <tr>
                    <td><u>Densidad</u></td>
                    <td><u>M&eacute;todo</u></td>
                    <td align='center'><u>Observaciones</u></td>
                </tr>
                <tr>
                    <td><?= $C[0]['densidad'] ?> g/mL</td>
                    <td><?= $C[0]['metodo'] ?></td>
                    <td align='center'><?= $C[0]['observacion'] ?></td>
                </tr>
            <?php } ?>
        </table>
        <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%'>
            <?php 
                $D = $Robot->obtieneImpurezasCentinela($A[0]['id'], $B1[$p]['id_muestra']);
                if ($D) { ?>
                <tr><td><br></td></tr>
                <tr style="font-size: 14px;">
                    <td colspan="5"><b>Impurezas</b></td>
                </tr>
                <tr><td><br></td></tr>
                <tr>
                    <td><u>Impureza</u></td>
                    <td><u>U.C.</u></td>
                    <td><u>C.E.</u></td>
                    <td><u>C.P.</u></td>
                    <td><u>I.C.</u></td>
                    <td><u>M&eacute;todo</u></td>
                </tr>
                <?php for ($x = 0; $x < count($D); $x++) { ?>
                    <tr>
                        <td><?= $D[$x]['c_NmbIngrediente'] ?></td>
                        <td><?= $D[$x]['c_NmbMetrica'] ?></td>
                        <td><?= $D[$x]['dc_CtcEncontrada'] ?></td>
                        <td><?= $D[$x]['ti_CumpleNorma'] ?></td>
                        <td><?= $D[$x]['dc_Incertidumbre'] ?></td>
                        <td><?= $D[$x]['vc_Metodo'] ?></td>
                    </tr>
                    <tr><td colspan="6"><?= $D[$x]['vc_Observacion'] ?></td></tr>
                <?php } ?>
            <?php } ?>
        </table>
    <?php } ?>
    <table style="font-size:12px; background-color:#FFFFFF;" align='right' width='30%'>   
        <tr><td align="center"><br><br><br><br>___________________________________________________________<br>Licda. Sonia Mes&eacute;n Ju&aacute;rez<br>Jefe del Laboratorio de Control de Calidad</td></tr>
    </table>
    <table style="font-size:12px; background-color:#FFFFFF;" align='right' width='100%'>   
        <tr><td align="center">----------------------------------------------------FIN DE DOCUMENTO----------------------------------------------------</td></tr>
        <tr><td>Cc. Archivo</td></tr>
    </table>
    <br><br><br>

    <br>

    <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%' cellpadding="0" cellspacing="0">
        <tr><td style="font-size: 10px;" colspan="5">Los resultados de an&aacute;lisis se refieren &uacute;nicamente a la(s) muestra(s) indicada(s) en el presente informe. Impreso en: <?= date('d/m/Y H:i') ?><br></td></tr>
        <tr><td style="font-size: 9px;" colspan="5">U.C.: unidades de concentraci&oacute;n.  C.D.: concentraci&oacute;n declarada.  C.E.: concentraci&oacute;n encontrada.  I.C.: incertidumbre combinada.  L.I.: l&iacute;mite inferior.  L.S.: l&iacute;mite superior.  N.D.: no detectable.  N.C.: no cuantificable.  N.A.: no aplica.  CPL: Cumple</td></tr>
        <tr style="background-color: #aaa" align="center"><td><strong>Ministerio de Agricultura y Ganader&iacute;a</strong></td><td><strong>Servicio Fitosanitario del Estado</strong></td><td><strong>San Jos&eacute;, Costa Rica.</strong></td><td><strong>Apdo 1521-1200</strong></td></tr>
    </table>
    <?php
} else {
    echo "<center>Error en par&aacute;metros.<br><br><a href='#' onclick='window.close()'>[Cerrar]</a></center>";
}
?>