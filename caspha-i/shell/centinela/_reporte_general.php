<?php
/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
 * ***************************************************************** */
if (isset($_POST['registro'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    /* if ($_POST['formato'] == '1') {
      header('Content-type: application/x-msdownload');
      header('Content-Disposition: attachment; filename=reportes.xls');
      header('Pragma: no-cache');
      header('Expires: 0');
      } */

    $Robot = new Reportes();
    $txt = 'plaguicidas y fertilizantes';
    if ($_POST['tipo'] == 'FERT') {
        $txt = 'fertilizantes';
    }
    if ($_POST['tipo'] == 'PLAG') {
        $txt = 'plaguicidas';
    }
    $filtro = '';
    if ($_POST['desde'] != '' && $_POST['hasta'] != '') {
        $filtro .= 'fecha,';
    }
    if ($_POST['registro'] != '') {
        $filtro .= ' registro,';
    }
    if ($_POST['elemento'] != '-1') {
        $filtro .= ' producto/elemento,';
    }
    if ($_POST['solicitud'] != '') {
        $filtro .= ' solicitud,';
    }
    if ($_POST['informe'] != '') {
        $filtro .= ' informe,';
    }
    if ($_POST['ingrediente'] != '-1') {
        $filtro .= ' ingrediente,';
    }
    if ($_POST['tipo'] != '-1') {
        $filtro .= ' tipo,';
    }
    if ($filtro == '') {
        $filtro = ' ning&uacute;n filtro aplicado.';
    }
    $titulo = "<p><b>Reporte general de an&aacute;lisis de {$txt}</b></p> <p>Filtrado por: " . $filtro . "</p> <p>Informe generado el " . date('d/m/Y H:i') . "</p>";
    $Robot->TableHeader($titulo, 1);
    $ROW = $Robot->obtieneGeneralCentinela($_POST['desde'], $_POST['hasta'], $_POST['registro'], $_POST['elemento'], $_POST['solicitud'], $_POST['informe'], $_POST['ingrediente'], $_POST['tipo']);
    ?>
    <table style="font-size:12px; background-color:#FFFFFF; border:solid 1px #759C28" align='center' width='100%'>
        <tr align="center">
            <td><strong>Muestra</strong></td>
            <td><strong>Informe</strong></td>
            <td><strong>Solicitud</strong></td>
            <td><strong>Tipo</strong></td>
            <td><strong>Registro</strong></td>
            <td><strong>Lote</strong></td>
            <td><strong>Formulaci&oacute;n</strong></td>
            <td><strong>Nombre de Producto</strong></td>
            <td><strong>Ingrediente Activo</strong></td>
            <td><strong>Fecha Aprobaci&oacute;n</strong></td>
            <td><strong>Concentraci&oacute;n declarada</strong></td>
            <td><strong>Concentraci&oacute;n encontrada</strong></td>
            <td><strong>Incertidumbre</strong></td>
            <td><strong>L&iacute;mite inferior</strong></td>
            <td><strong>L&iacute;mite superior</strong></td>
            <td><strong>Impurezas</strong></td>
        </tr>
        <tr><td colspan="16"><hr /></td></tr>
        <?php
        for ($x = 0; $x < count($ROW); $x++) {
            $impurezas = '';
            $D = $Robot->obtieneImpurezasCentinela($ROW[$x]['id'], $ROW[$x]['id_muestra']);
            for ($b = 0; $b < count($D); $b++) {
                $impurezas .= $D[$b]['c_NmbIngrediente'] . ': ' . $D[$b]['dc_CtcEncontrada'] . ', ';
            }
            ?>
            <tr align="center">
                <td><?= $ROW[$x]['codigo_interno'] ?></td>
                <td><?= $ROW[$x]['cod_informe'] ?></td>
                <td><?= $ROW[$x]['id_solicitud'] ?></td>
                <td><?= $ROW[$x]['id_lab'] == '2' ? 'Fertilizantes' : 'Plaguicidas' ?></td>
                <td><?= $ROW[$x]['registro'] ?></td>
                <td><?= $ROW[$x]['lote'] ?></td>
                <td><?= $ROW[$x]['formulacion'] ?></td>
                <td><?= $ROW[$x]['producto'] ?></td>
                <td><?= $ROW[$x]['ingrediente'] ?></td>
                <td><?= $ROW[$x]['f_conclusion'] ?></td>
                <td><?= $ROW[$x]['declarada'] ?></td>
                <td><?= $ROW[$x]['encontrada'] ?></td>
                <td><?= $ROW[$x]['incertidumbre'] ?></td>
                <td><?= $ROW[$x]['limite_inferior'] ?></td>
                <td><?= $ROW[$x]['limite_superior'] ?></td>  
                <td><?= $impurezas ?></td>  
            </tr>
        <?php }//FOR    ?>
        <tr align="center">
            <td colspan="16" align="center"><strong>----------------------------------------------------&Uacute;ltima L&iacute;nea>----------------------------------------------------</strong><br></td>
        </tr>
        <tr align="center">
            <td colspan="16" align="right"><strong>Total de muestras:</strong></td>
            <td><?= $x-- ?></td>
        </tr>
    </table>

    <?php
    $Robot->TableFooter();
} else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    class _inventario_general extends Mensajero {

        private $_ROW = array();
        private $Securitor = '';

        function __construct() {
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth())
                $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0')
                $this->Portero($this->Securitor->UsuarioPermiso('82'));
            /* PERMISOS */
        }

        function TiposMuestra() {
            $Inventor = new Inventario();
            return $Inventor->TiposMuestra($this->_ROW['LID'], $this->_ROW['ROL']);
        }

    }

}
?>