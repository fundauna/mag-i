<?php
/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
 * ***************************************************************** */
if (isset($_GET['INF'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Robot = new Reportes();
    $A = $Robot->obtieneInformeCentinela($_GET['INF'], $_GET['TIP']);
    $B1 = $Robot->obtieneMuestrasPlagTotalCentinela($A[0]['id']);
    $E = $Robot->obtieneEstadoCentinela($A[0]['id']);
    $F = $Robot->obtieneFechaIngresoCentinela($A[0]['id_solicitud']);
    ?>
    <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%'>
        <tr>
            <td><strong style="font-size: 14px;">Ministerio de Agricultura y Ganader&iacute;a</strong></td>
        </tr>
        <tr>
            <td>Servicio Fitosanitario del Estado</td>
            <td align="right">Tel. 2549-3606</td>
        </tr>
        <tr>
            <td>Departamento de Laboratorios</td>
            <td align="right">Fax. 2549-3599</td>
        </tr>
        <tr>
            <td>Laboratorio de Control de Calidad de Agroqu&iacute;micos</td>
        </tr> 
        <tr>
            <td>Plaguicidas</td>
        </tr> 
    </table>
    <center><h2>Informe de An&aacute;lisis <?= $_GET['INF'] ?></h2></center>    
    <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%'>
        <tr><td colspan="5"><hr></td></tr>
        <tr>
            <td>Solicitud:</td>
            <td><?= $A[0]['id_solicitud'] ?></td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>Ingreso de la muestra:</td>
            <td><?= $F[0]['d_Estado'] ?></td>
        </tr>
        <tr>
            <td>Dependencia:</td>
            <td><?= $A[0]['dependencia'] ?></td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>Conclusi&oacute;n del an&aacute;lisis:</td>
            <td><?= $A[0]['f_conclusion'] ?></td>
        </tr>
        <tr>
            <td>Solicitante:</td>
            <td><?= $A[0]['solicitante'] ?></td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>Reporte:</td>
            <td><?= $E[0]['d_Estado'] ?></td>
        </tr>
        <tr><td><br></td></tr>        
    </table>
    <?php
    for ($p = 0; $p < count($B1); $p++) {
        $B = $Robot->obtieneMuestrasPlagCentinela($A[0]['id'], $B1[$p]['id_muestra']);
        ?>
        <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%'>
            <tr style="background-color: #000; color: #FFF; font-size: 14px;">
                <td colspan="7"><b>Nombre comercial: <?= $B[0]['producto'] ?></b></td>
            </tr>        
            <tr>
                <td>Muestra/C&oacute;d Interno:</td>
                <td><?= $B[0]['codigo_interno'] ?></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>            
                <td>No. registro</td>
                <td><?= $B[0]['registro'] ?></td>
                <td>Sellada?: <?= $B[0]['sello'] ? 'Si' : 'No' ?></td>            
                <td></td>
            </tr>
            <tr>
                <td>Cinta/C&oacute; Externo:</td>
                <td><?= $B[0]['codigo_externo'] ?></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>Lote fab.:</td>
                <td><?= $B[0]['lote'] ?></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Recipiente:</td>
                <td><?= $B[0]['recipiente'] ?></td>            
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>Formulaci&oacute;n:</td>
                <td><?= $B[0]['descripcion'] . ' - ' . $B[0]['formulacion'] ?></td>
                <td></td>
            </tr>
        </table>
        <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%'>
            <tr><td><br></td></tr>
            <tr style="font-size: 14px;">
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td align="center" style="border:solid 1px #000"><b>An&aacute;lisis Qu&iacute;mico</b></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            </tr>
        </table>
        <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%'>
            <tr><td colspan="9"><br></td></tr>
            <tr><td colspan="9" style="font-size:14px;"><b>Tolerancia para la concentraci&oacute;n del ingrediente activo (RTCR 171)</b></td></tr>
            <tr><td colspan="9"><br></td></tr>
            <tr>
                <td><u>Ingrediente Activo</u></td>
                <td><u>U.C.</u></td>
                <td><u>C.D.</u></td>
                <td><u>C.E.</u></td>
                <td><u>I.C.</u></td>
                <td><u>L.I.</u></td>
                <td><u>L.S.</u></td>
                <td><u>Cumple</u></td>
                <td style="width:400px;"><u>M&eacute;todo</u></td>
            </tr>
            <?php for ($i = 0; $i < count($B); $i++) { ?>
                <tr>
                    <td><?= $B[$i]['ingrediente'] ?></td>
                    <td><?= $B[$i]['metrica'] ?></td>
                    <td><?= $B[$i]['declarada'] ?></td>
                    <td><?= $B[$i]['encontrada'] ?></td>
                    <td><?= $B[$i]['incertidumbre'] ?></td>
                    <td><?= $B[$i]['limite_inferior'] ?></td>
                    <td><?= $B[$i]['limite_superior'] ?></td>
                    <td><?= $B[$i]['cumple_norma'] ? 'Si' : 'No' ?></td>
                    <td><?= $B[$i]['metodo'] ?></td>
                </tr>
            <?php } ?>
        </table>
        <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%'>
            <?php
            $D = $Robot->obtieneImpurezasCentinela($A[0]['id'], $B1[$p]['id_muestra']);
            if ($D) {
                ?>
                <tr><td><br></td></tr>
                <tr style="font-size: 14px;">
                    <td colspan="5"><b>Impurezas</b></td>
                </tr>
                <tr><td><br></td></tr>
                <tr>
                    <td><u>Impureza</u></td>
                    <td><u>U.C.</u></td>
                    <td><u>C.E.</u></td>                    
                    <td><u>I.C.</u></td>
                    <td><u>C.P.</u></td>
                    <td><u>M&eacute;todo</u></td>
                </tr>
                <?php for ($x = 0; $x < count($D); $x++) { ?>
                    <tr>
                        <td><?= $D[$x]['c_NmbIngrediente'] ?></td>
                        <td><?= $D[$x]['c_NmbMetrica'] ?></td>
                        <td><?= $D[$x]['dc_CtcEncontrada'] ?></td>
                        <td><?= $D[$x]['dc_Incertidumbre'] ?></td>
                        <td><?= $D[$x]['dc_LimiteMaxPermitido'] ?></td>                        
                        <td><?= $D[$x]['vc_Metodo'] ?></td>
                    </tr>
                    <tr><td colspan="6"><?= $D[$x]['vc_Observacion'] ?></td></tr>
                <?php } ?>
            <?php } ?>
        </table>
        <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%'>
            <tr><td><br></td></tr>
            <tr style="font-size: 14px;">
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td align="center" style="border:solid 1px #000"><b>An&aacute;lisis F&iacute;sico</b></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            </tr>
        </table>
        <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%'>
            <?php
            $C = $Robot->obtieneDensidadCentinela($A[0]['id'], $B1[$p]['id_muestra']);
            if ($C && ($C[0]['densidad'] != 0 || $C[0]['metodo'] != '')) {
                ?>
                <tr>
                    <td><u>Densidad</u></td>
                    <td><u>M&eacute;todo</u></td>
                    <td align='center'><u>Observaciones</u></td>
                </tr>
                <tr>
                    <td><?= $C[0]['densidad'] ?> g/mL</td>
                    <td><?= $C[0]['metodo'] ?></td>
                    <td align='center'><?= $C[0]['observacion'] ?></td>
                </tr>
            <?php } ?>
        </table>
        <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%'>
            <?php
            $G = $Robot->obtieneDisolucionCentinela($A[0]['id'], $B1[$p]['id_muestra']);
            if ($G) {
                ?>
                <tr><td><br></td></tr>
                <tr><td colspan="5" style="font-size:14px;"><b>Estabilidad de la Disoluci&oacute;n Acuosa (RTCR 175)</b></td></tr>
                <tr><td><br></td></tr>
                <tr>
                    <td>Lectura a 18 horas</td>
                    <td></td>             
                </tr>
                <tr>
                    <td>Separaci&oacute;n de aceite: <?= $G[0]['dc_SeparacionAceite'] ?> ml</td>
                    <td>Cumple?: <?= $G[0]['b_CumpleNorma'] ? 'S&iacute;' : 'No' ?></td>      
                </tr>  
                <tr>
                    <td>Separaci&oacute;n de sedimento: <?= $G[0]['dc_SeparacionSedimento'] ?> ml</td>
                    <td>Material suspendido?: <?= $G[0]['b_HayParticulasVisibles'] ? 'S&iacute;' : 'No' ?></td>
                </tr>  
                <tr>
                    <td colspan="2">M&eacute;todo: <?= $G[0]['vc_Metodo'] ?></td>
                </tr>
                <tr>
                    <td colspan="6" style="font-size: 10px"><i><?= $G[0]['vc_Observacion'] ?></i></td>
                </tr> 
            <?php } ?>
        </table>
        <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%'>
            <?php
            $H = $Robot->obtieneSuspensibilidadCentinela($A[0]['id'], $B1[$p]['id_muestra']);
            if ($H) {
                ?>
                <tr><td><br></td></tr>
                <tr><td colspan="5" style="font-size:14px;"><b>Suspensibilidad del Ingrediente Activo RTCR 174 y RTCR 233</b></td></tr>
                <tr><td><br></td></tr>
                <tr>
                    <td><u>Ingrediente activo</u></td>
                    <td><u>U.C.</u></td>
                    <td><u>Susp.</u></td>
                    <td><u>V.M.P.</u></td>
                    <td><u>Cumple</u></td> 
                    <td><u>M&eacute;todo</u></td> 
                </tr>
                <?php for ($m = 0; $m < count($H); $m++) { ?>
                    <tr>
                        <td><?= $H[$m]['ingrediente'] ?></td>
                        <td><?= $H[$m]['metrica'] ?></td>
                        <td><?= $H[$m]['dc_Suspensibilidad'] ?></td>
                        <td><?= $H[$m]['dc_MinimoPermitido'] ?></td>
                        <td><?= $H[$m]['b_CumpleNorma'] ? 'S&iacute;' : 'No' ?></td>  
                        <td><?= $H[$m]['vc_Metodo'] ?></td>
                    </tr>
                    <tr>
                        <td colspan="6" style="font-size: 10px"><i><?= $H[$m]['vc_Observacion'] ?></i></td>
                    </tr> 
                <?php } ?>

            <?php } ?>
        </table>
        <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%'>
            <?php
            $I = $Robot->obtieneHumectabilidadCentinela($A[0]['id'], $B1[$p]['id_muestra']);
            if ($I) {
                ?>
                <tr><td><br></td></tr>
                <tr><td colspan="4" style="font-size:14px;"><b>Humectabilidad (RTCR 173)</b></td></tr>
                <tr><td><br></td></tr>
                <tr>
                    <td>Humectabilidad: <?= $I[0]['si_Humectabilidad'] ?> seg</td>
                    <td>Cumple?: <?= $I[0]['b_CumpleNorma'] ? 'S&iacute;' : 'No' ?></td> 
                </tr>
                <tr>
                    <td colspan="2">M&eacute;todo: <?= $I[0]['vc_Metodo'] ?></td> 
                </tr>
                <tr>
                    <td colspan="6" style="font-size: 10px"><i><?= $I[0]['vc_Observacion'] ?></i></td>
                </tr> 
            <?php } ?>
        </table>
        <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%'>
            <?php
            $J = $Robot->obtieneEmulsionCentinela($A[0]['id'], $B1[$p]['id_muestra']);
            if ($J) {
                ?>
                <tr><td><br></td></tr>
                <tr><td colspan="4" style="font-size:14px;"><b>Estabilidad de la Emulsi&oacute;n (RTCR 172)</b></td></tr>
                <tr><td><br></td></tr>
                <tr>
                    <td></td>
                    <td><u>Lectura a 30 min</u></td> 
                    <td><u>Lectura a 120 min</u></td> 
                    <td><u>Lectura a 24.5 h</u></td> 
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Cremado</td> 
                    <td><?= $J[0]['dc_30minCremado'] ?> ml</td>
                    <td><?= $J[0]['dc_120minCremado'] ?> ml</td>
                    <td><?= $J[0]['dc_24hrsCremado'] ?> ml</td>
                    <td>Espontaneidad?: <?= $J[0]['b_HayEspontaneidad'] ? 'S&iacute;' : 'No' ?></td>
                </tr>
                <tr>
                    <td>Aceite</td> 
                    <td><?= $J[0]['dc_30minAceite'] ?> ml</td>
                    <td><?= $J[0]['dc_120minAceite'] ?> ml</td>
                    <td><?= $J[0]['dc_24hrsAceite'] ?> ml</td>
                    <td>Cumple?: <?= $J[0]['b_CumpleNorma'] ? 'S&iacute;' : 'No' ?></td>
                </tr>
                <tr>
                    <td colspan="5">M&eacute;todo: <?= $J[0]['vc_Metodo'] ?></td> 
                </tr>
                <tr>
                    <td colspan="6" style="font-size: 10px"><i><?= $J[0]['vc_Observacion'] ?></i></td>
                </tr>
            <?php } ?>
        </table>
        <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%'>
            <?php
            $K = $Robot->obtieneTamizCentinela($A[0]['id'], $B1[$p]['id_muestra']);
            if ($K) {
                ?>
                <tr><td><br></td></tr>
                <tr><td colspan="4" style="font-size:14px;"><b>Pulverulencia (RTCR 210)</b></td></tr>
                <tr><td><br></td></tr>
                <tr>
                    <td>% Acumulativo: <?= $K[0]['dc_Acumulativo'] ?></td>
                    <td>% Receptor: <?= $K[0]['dc_Receptor'] ?></td>
                    <td>Cumple?: <?= $K[0]['b_CumpleNorma'] ? 'S&iacute;' : 'No' ?></td>
                </tr>
                <tr>
                    <td colspan="5">M&eacute;todo: <?= $K[0]['vc_Metodo'] ?></td> 
                </tr>
                <tr>
                    <td colspan="6" style="font-size: 10px"><i><?= $K[0]['vc_Observacion'] ?></i></td>
                </tr>
            <?php } ?>
        </table>
    <?php } ?>
    <table style="font-size:12px; background-color:#FFFFFF;" align='right' width='30%'>   
        <tr><td align="center"><br><br><br><br>___________________________________________________________<br>Licda. Sonia Mes&eacute;n Ju&aacute;rez<br>Jefe del Laboratorio de Control de Calidad</td></tr>
    </table>
    <table style="font-size:12px; background-color:#FFFFFF;" align='right' width='100%'>   
        <tr><td align="center">----------------------------------------------------FIN DE DOCUMENTO----------------------------------------------------</td></tr>
        <tr><td>Cc. Archivo</td></tr>
    </table>
    <br><br><br>

    <br>

    <table style="font-size:12px; background-color:#FFFFFF;" align='center' width='100%' cellpadding="0" cellspacing="0">
        <tr><td style="font-size: 10px;" colspan="5">Los resultados de an&aacute;lisis se refieren &uacute;nicamente a la(s) muestra(s) indicada(s) en el presente informe. Impreso en: <?= date('d/m/Y H:i') ?><br></td></tr>
        <tr><td style="font-size: 9px;" colspan="5">U.C.: unidades de concentraci&oacute;n.  C.D.: concentraci&oacute;n declarada.  C.E.: concentraci&oacute;n encontrada.  I.C.: incertidumbre combinada.  L.I.: l&iacute;mite inferior.  L.S.: l&iacute;mite superior.  N.D.: no detectable.  N.C.: no cuantificable.  N.A.: no aplica. V.M.P.: valor m&iacut&iacute;nimo permitido. C.P.: cantidad permitida.  CPL: cumple</td></tr>
        <tr style="background-color: #aaa" align="center"><td><strong>Ministerio de Agricultura y Ganader&iacute;a</strong></td><td><strong>Servicio Fitosanitario del Estado</strong></td><td><strong>San Jos&eacute;, Costa Rica.</strong></td><td><strong>Apdo 1521-1200</strong></td></tr>
    </table>
    <?php
} else {
    echo "<center>Error en par&aacute;metros.<br><br><a href='#' onclick='window.close()'>[Cerrar]</a></center>";
}
?>