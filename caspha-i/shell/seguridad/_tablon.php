<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['id']) ) die('-1');
	
	$Securitor = new Seguridad();
	$Securitor->TablonDel($_POST['id']);
	echo 1;
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _tablon extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			if( !isset($_GET['ver']) ) die('Error de parámetros');
		
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			//
			$this->Securitor->NotificacionesGet($this->_ROW['LID'], $this->_ROW['UID']);
		}
		
		function TablonMuestra(){
			return $this->Securitor->TablonGet($this->_ROW['UID']);
		}
	}
}
?>