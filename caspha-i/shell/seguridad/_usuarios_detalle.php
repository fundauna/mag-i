<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['nombre']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	echo $Securitor->UsuariosIME($_POST['id'],
		$_POST['cedula'], 
		$_POST['nombre'],
		$_POST['ap1'], 
		$_POST['ap2'], 
		$_POST['email'],
		$_POST['perfil'], 
		$_POST['estado'], 
		$_POST['login'], 
		$_POST['clave'],
		$_POST['accion'],
		$_ROW['LID']
	);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _usuarios_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M') ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('13') );
			/*PERMISOS*/
		}
		
		function ObtieneDatos(){
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $this->Securitor->UsuariosVacio();
			else	
				return $this->Securitor->UsuariosDetalle($_GET['ID'], $this->_ROW['LID']);
		}
		
		function Perfiles(){
			return $this->Securitor->PerfilesMuestra($this->_ROW['LID']);
		}
		
		function EsAdmin(){
			if( !isset($_GET['ID']) ) $_GET['ID'] = '';
			
			if($this->_ROW['ROL']==0 && $this->_ROW['UID']==$_GET['ID']) return true;
			return false;
		}
	}
}
?>