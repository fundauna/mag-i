<?php
function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

/*******************************************************************
DECLARACION CLASE GESTORA NO HEREDABLE
*******************************************************************/
final class _menu2 extends Mensajero{
	private $_ROW = array();
	private $Securitor = '';
	
	function __construct(){
		$this->Securitor = new Seguridad();
		if(!$this->Securitor->SesionAuth()) $this->Err();
		$this->_ROW = $this->Securitor->SesionGet();
	}
	
	function CargaBanner(){
		return $this->Incluir('banner1', 'bkg');
	}
	
	function Get($_campo){
		return $this->_ROW[$_campo];
	}
}
?>