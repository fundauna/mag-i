<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	$Listator->UsuariosLista('seguridad', basename(__FILE__), '', $_ROW['LID']);
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
}elseif( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}

	if( !isset($_POST['usuario']) ) die('-1');
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	
	if( isset($_POST['PermisosBuscar']) ){
		$str = '<select id="asignados" style="width:400px;" size="10">';
		$ROW = $Securitor->AutorizacionesCartasMuestra($_POST['usuario']);
		for($x=0;$x<count($ROW);$x++){
			//$ROW[$x]['nombre'] = utf8_encode($ROW[$x]['nombre']);
			$str .= "<option value='{$ROW[$x]['id']}'>{$ROW[$x]['desc']}</option>";	
		}
		$str .= '</select>';
		die($str);
	}elseif( isset($_POST['PermisosAgrega']) ){
		echo $Securitor->AutorizacionesCartasIME($_POST['usuario'], $_POST['permiso'], 'I');
		exit;
	}elseif( isset($_POST['PermisosElimina']) ){
		echo $Securitor->AutorizacionesCartasIME($_POST['usuario'], $_POST['permiso'], 'D');
		exit;
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _aut_cartas extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('14') );
			/*PERMISOS*/
		}
		
		function CartasMuestra(){
			$Robot = new Cartas();
			return $Robot->CartasCatalogo();
		}
	}
}
?>