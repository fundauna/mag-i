<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['nombre']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$Securitor->UsuarioDatosSet($_POST['nombre'], $_POST['ap1'], $_POST['ap2'],	$_POST['email']);
	if($_POST['clave']=='1'){
		if( !$Securitor->UsuarioClave($_POST['actual'], $_POST['nueva']) ) die('-2');
	}
	die('1');
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _datos extends Mensajero{	
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
		}
		
		function ObtieneDatos(){
			return $this->Securitor->UsuarioDatosGet();
		}
	}
}
?>