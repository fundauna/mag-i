<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _paginas extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
		}
		
		function Paginas(){
			$this->Securitor = new Seguridad();
			return $this->Securitor->PaginasMuestra();
		}
		
		function Seccion($_var){
			if($_var=='A') return 'Inventario';
			elseif($_var=='B' or $_var=='Q') return 'Gesti&oacute;n de Calidad';
			elseif($_var=='C') return 'Equipos';
			elseif($_var=='D') return 'Cotizaciones';
			elseif($_var=='E') return 'Equipos';
			elseif($_var=='F') return 'Inventario';
			elseif($_var=='G') return 'Tra. y Serv. Externos';
			elseif($_var=='I') return 'Personal';
			elseif($_var=='J') return 'Proveedores';
			elseif($_var=='K') return 'Reportes';
			elseif($_var=='L') return 'Serv. Cliente';
			elseif($_var=='M') return 'Seguridad';
			elseif($_var=='N' or $_var=='H') return 'Servicios';
			elseif($_var=='P') return 'Clientes externos';
		}
		
		function Codigo($_sigla, $_codigo){
			$_codigo = str_pad($_codigo, 4, '0', STR_PAD_LEFT);
			return $_sigla.$_codigo;
		}
	}
?>