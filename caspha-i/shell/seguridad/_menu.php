<?php
function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

/*******************************************************************
DECLARACION CLASE GESTORA NO HEREDABLE
*******************************************************************/
final class _menu extends Mensajero{
	private $_ROW = array();
	private $Securitor = '';
	private $admin = false;
	
	function __construct(){
		$this->Securitor = new Seguridad();
		if(!$this->Securitor->SesionAuth()) $this->Err();
		$this->_ROW = $this->Securitor->SesionGet();
		parent::SetLab($this->_ROW['LID']);
	}
	
	function EsAdmin(){
		if($this->_ROW['ROL']==0) return true;
		return false;
	}
	
	function EsLCC(){
		if($this->_ROW['LID']=='3') return true;
		return false;
	}
	
	function EsLDP(){
		if($this->_ROW['LID']=='2') return true;
		return false;
	}
	
	function EsLRE(){
		if($this->_ROW['LID']=='1') return true;
		return false;
	}
	
	function CargaBanner(){
		return $this->Incluir('banner'.$this->_ROW['LID'], 'bkg');
	}
	
	function CargaPermisos(){
		if($this->_ROW['ROL'] == 0) $this->admin = true;
		else{
			$lid = $this->_ROW['LID'];
			$this->_ROW = $this->Securitor->UsuarioMenu($this->_ROW['LID'], $this->_ROW['ROL']);
			$this->_ROW['LID'] = $lid; //PARA QUE NO SE PIERDA LA VARIABLE $this->_ROW['LID']
		}
		return;
	}
	
	function Get($_campo){
		return $this->_ROW[$_campo];
	}
	
	function NombreRol(){
		return $this->Securitor->GetNombreRol($this->_ROW['ROL']);
	}
	
	function TienePermiso($_modulo){
		if($this->admin) return true;
		for($x=0;$x<count($this->_ROW);$x++){
			if(isset($this->_ROW[$x]['permiso']) and $this->_ROW[$x]['permiso']==$_modulo) return true;
		}
		return false;
	}
	
	function Tablon(){
		if($this->admin) return '';
		else return 'tablon.php?ver=1';
	}
}
?>