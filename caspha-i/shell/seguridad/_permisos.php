<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	if( !isset($_POST['perfil']) ) die('-1');
	
	if( isset($_POST['PermisosBuscar']) ){
		$str = '<select id="asignados" style="width:340px;" size="10">';
		$ROW = $Securitor->PermisosMuestra($_POST['perfil']);
		for($x=0;$x<count($ROW);$x++){
			//$ROW[$x]['nombre'] = utf8_encode($ROW[$x]['nombre']);
			$str .= "<option value='{$ROW[$x]['id']}'>{$ROW[$x]['nombre']} ({$ROW[$x]['privilegio']})</option>";	
		}
		$str .= '</select>';
		die($str);
	}elseif( isset($_POST['PermisosAgrega']) ){
		echo $Securitor->PermisosIME($_POST['perfil'], $_POST['permiso'], $_POST['privilegio'], 'I');
		exit;
	}elseif( isset($_POST['PermisosElimina']) ){
		echo $Securitor->PermisosIME($_POST['perfil'], $_POST['permiso'], '', 'D');
		exit;
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _permisos extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('12') );
			/*PERMISOS*/
		}
		
		function Perfiles(){
			return $this->Securitor->PerfilesMuestra($this->_ROW['LID']);
		}
		
		function Permisos(){
			return $this->Securitor->PermisosMuestra();
		}
	}
}
?>