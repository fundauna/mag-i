<?php

/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 * ***************************************************************** */
if (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    if($_POST['LID']=='1'){
        $rol='48';
    }elseif ($_POST['LID']=='2') {
         $rol='49';
    }elseif ($_POST['LID']=='3') {
         $rol='50';
    }
    echo $Securitor->UsuariosIME('', $_POST['cedula'], $_POST['nombre'], $_POST['ap1'], $_POST['ap2'], $_POST['email'], $rol, 1, $_POST['login'], $_POST['clave'], 'I', $_POST['LID'])?1:-1;
    exit;
}else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _usuarios_detalle2 extends Mensajero {

        private $_ROW = array();
        private $Securitor = '';

        function __construct() {
            //SOLO SE PUEDE ABRIR COMO POP-UP MODAL
            $this->ValidaModal();

            if (!isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M'))
                die('Error de parámetros');
            $this->Securitor = new Seguridad();
        }

        function ObtieneDatos() {
   
                return $this->Securitor->UsuariosVacio();

        }

    }

}
?>