<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

final class _documentos extends Mensajero{
	private $_ROW = array();
	private $Securitor = '';
	
	function __construct(){
		//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
		$this->ValidaModal();
		
		if( !isset($_GET['ARCHIVO'])) die('Error de parámetros');
		if( !isset($_GET['MODULO'])) die('Error de parámetros');
		if( !isset($_GET['ACCESO'])) die('Error de parámetros');
		
		$this->Securitor = new Seguridad();
		if(!$this->Securitor->SesionAuth()) $this->Err();
		$this->_ROW = $this->Securitor->SesionGet();
		/*PERMISOS*/
		if($_GET['ACCESO'] != '00') //usado en hoja de vida
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso($_GET['ACCESO']) );
		/*PERMISOS*/
		
		$ruta = $this->Incluir($_GET['ARCHIVO'], 'doc', $_GET['MODULO']);
		Bibliotecario::ZipDescargar($ruta);
	}

}
?>