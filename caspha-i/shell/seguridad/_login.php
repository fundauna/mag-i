<?php

/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 * ***************************************************************** */
if (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass)
    {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    if ($_POST['_AJAX'] == 1) {
        //if (Autenticacion::login($_POST['usuario'], $_POST['clave']) == 1) {
        if ($_POST['_AJAX'] == 1) {
            $login = $Securitor->UsuarioLogin($_POST['usuario'], $_POST['clave'], $_POST['LID']);
            if ($login == -2) {
                echo '2';
            } else {
                echo $login;
            }
        } else {
            echo '-2';
        }
    }

    exit;
} else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA NO HEREDABLE
     * ***************************************************************** */

    function __autoload($_BaseClass)
    {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _login extends Mensajero
    {

        function __construct()
        {

            if (isset($_GET['LID'])) {
                if ($_GET['LID'] == 'c4ca4238a0b923820dcc509a6f75849b') {
                    $this->Bienvenida(1);
                } elseif ($_GET['LID'] == 'c81e728d9d4c2f636f067f89cc14862c') {
                    $this->Bienvenida(2);
                } elseif ($_GET['LID'] == 'eccbc87e4b5ce2fe28308fd9f2a7baf3') {
                    $this->Bienvenida(3);
                } elseif ($_GET['LID'] == 'FIN') {
                    //LOG OUT
                    $Securitor = new Seguridad();
                    $Securitor->UsuarioFin();
                    $this->Index();
                } else {
                    $this->Index();
                }
            } else {
                $this->Index();
            }
        }

        private function Bienvenida($_ID)
        {
            $Securitor = new Seguridad();
            $Securitor->SesionDestruye();
            parent::SetLab($_ID);
        }

    }

}
?>