<?php

/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 * ***************************************************************** */
if (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    if (!isset($_POST['clave']))
        die('-1');

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth())
        die('-0');
    
    //Validar que la clave no exista entra las ultimas 3
    $claves=$Securitor->ClienteHistorialGet(1);
    
    if($claves){
    foreach ($claves as $clave){
        if($clave['clave']== base64_encode($_POST['nueva'])){
            die('-3');
        }
    }
    }

    $Securitor->ClientesDatosSet($_POST['email']);
    if ($_POST['clave'] == '1') {
        if (!$Securitor->ClientesClave($_POST['actual'], $_POST['nueva'])) {
            die('-2');
        }else{
            $Securitor->ClientesHistorialIME("I", $_POST['nueva']);
        }
    }
    die('1');
} else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _datos2 extends Mensajero {

        private $_ROW = array();
        private $Securitor = '';

        function __construct() {
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth())
                $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
        }

        function ObtieneDatos() {
            return $this->Securitor->ClientesDatosGet();
        }

    }

}
?>