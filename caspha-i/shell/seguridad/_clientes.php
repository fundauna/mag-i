<?php
/*******************************************************************
 * PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 *******************************************************************/
if (isset($_POST['_AJAX'])) {
    function __autoload($_BaseClass)
    {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    if (!isset($_POST['usuario'])) die('-1');

    $Securitor = new Seguridad();
    $login = $Securitor->ClientesLogin($_POST['usuario'], $_POST['clave']);
    if ($login == 1) {
        $dias = $Securitor->ClienteHistorialGet(2);
        if (empty($dias) || $dias[0]['dias'] > 60) {
            echo '-4';
            exit;
        }
    }
    echo $login;
    exit;
} else {
    /*******************************************************************
     * DECLARACION CLASE GESTORA NO HEREDABLE
     *******************************************************************/
    function __autoload($_BaseClass)
    {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _clientes extends Mensajero
    {

    }

}

?>