<?php

/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 * ***************************************************************** */
if (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    if (!isset($_POST['id']))
        die('-1');

    $Servitor = new Sc();
    if ($_POST['_AJAX'] == '1') {
        if($Servitor->ClientesIME($_POST['id'], $_POST['nombre'], $_POST['unidad'], $_POST['representante'], $_POST['tel'], $_POST['email'],
                /**/ $_POST['contacto'], $_POST['telefonos'], $_POST['correo'],
                /**/ $_POST['solicitante'], $_POST['soltel'], $_POST['solemail'],
                /**/ $_POST['fax'], $_POST['direccion'], $_POST['actividad'], $_POST['tipo'], $_POST['LRE'], $_POST['LDP'], $_POST['LCC'], $_POST['estado'], $_POST['accion'])){
                     echo json_encode([base64_decode($Servitor->clienteGet($_POST['id'])[0]['clave'])]); 
                }
    } elseif ($_POST['_AJAX'] == '2') {
        $lolo = $Servitor->clienteGet($_POST['id']);
        if($lolo){
            echo '1';
        }else{
            echo '0';
        }
    }
    exit;
} elseif (isset($_POST['_REENVIAR'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    if (!isset($_POST['id']))
        die('-1');

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth())
        die('-0');
    $_ROW = $Securitor->SesionGet();

    $Servitor = new Sc();
    echo $Servitor->ClientesReenvio($_POST['id'], $_POST['email']);
    exit;
}else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _clientes_registra extends Mensajero {

        function __construct() {
            
        }

    }

}
?>