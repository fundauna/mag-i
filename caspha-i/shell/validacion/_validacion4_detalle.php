<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['token'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot= new Validacion();
	
	if($_POST['acc'] == 'I'){
	
		$ROW = $Robot->ConsecutivoGet(2);
		
		if( $Robot->Validacion4EncabezadoSet($ROW, $_POST['proce'], $_POST['nombre'], $_ROW['UID'], $_POST['acc']) ){	
			//SUBIR ARCHIVO			
			if($_FILES['archivo']['size'] > 0){
				$nombre = $_FILES['archivo']['name'];
				$file = $_FILES['archivo']['tmp_name'];
				$year = date('Y');
				$ruta = "../../docs/validacion/{$ROW}.zip";
				Bibliotecario::ZipCrear($nombre, $file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");
			}
			//SUBIR ARCHIVO			
			echo'<script>opener.location.reload();window.close();</script>';
			exit();
		}else{
			echo 'Error al guardar los datos';
			exit();
		}
	}elseif($_POST['acc'] == 'M'){
		if( $Robot->Validacion4EncabezadoSet($_POST['id'], $_POST['proce'], $_POST['nombre'], $_ROW['UID'], $_POST['acc']) ){	
			//SUBIR ARCHIVO			
			if($_FILES['archivo']['size'] > 0){
				$nombre = $_FILES['archivo']['name'];
				$file = $_FILES['archivo']['tmp_name'];
				$year = date('Y');
				$ruta = "../../docs/validacion/{$_POST['id']}.zip";
				Bibliotecario::ZipCrear($nombre, $file, $ruta, 1) or die("Error: No es posible crear el archivo {$nombre}");
			}
			//SUBIR ARCHIVO			
			echo'<script>opener.location.reload();window.close();</script>';
			exit();
		}else{
			echo 'Error al guardar los datos';
			exit();
		}
	}else{
		if( $Robot->Validacion4EncabezadoSet($_POST['id'], '', '', $_ROW['UID'], $_POST['acc']) ){
			unlink("../../docs/validacion/{$_POST['id']}.zip");
			echo'<script>opener.location.reload();window.close();</script>';
			exit();
		}
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _validacion4_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['ID']) ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS GENERALES*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('96') );
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Robot = new Validacion();
			if( $_GET['ID']=='')
				return $Robot->Validacion4EncabezadoVacio();
			else	
				return $Robot->Validacion4EncabezadoGet($_GET['ID']);
		}
	}
}
?>