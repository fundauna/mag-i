<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	/*PARA TRAER LA SESION DEL LAB*/
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	$Listator = new Listador();
	$Listator->MatrizLista('validacion', $ROW['LID'], $_GET['linea'] , basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
}elseif( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['paso']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot = new Validacion();
	
	if($_POST['paso'] == '1'){
		echo $Robot->Validacion5EncabezadoSet($_POST['tipo'], 
			$_POST['elaboracion'],
			$_POST['asociada'],
			$_POST['matriz'],
			$_POST['bajo'],
			$_POST['medio'],
			$_POST['obs'],
			$_POST['ana'],
			$_POST['pic'],
			$_POST['teo'],
			$_POST['exp'],
			$_POST['analito'],
			$_POST['P1'],
			$_POST['P2'],
			$_POST['P3'],
			$_POST['P4'],
			$_POST['P5'],
			$_POST['bajo1'],
			$_POST['bajo2'],
			$_POST['bajo3'],
			$_POST['alto1'],
			$_POST['alto2'],
			$_POST['alto3'],
			$_POST['final'],
			$_POST['final2'],
			$_POST['cumple'],
			$_ROW['LID'],
			$_ROW['UID']);	
		exit;
	}elseif($_POST['paso'] == '2'){
	//
		if($_FILES['archivo']['size'] > 0){
			$ruta = $_FILES['archivo']['name'];
			$file = $_FILES['archivo']['tmp_name'];		
			if($_FILES['archivo']['type'] != 'application/x-msdownload' and $_FILES['archivo']['type'] != 'application/vnd.ms-excel') die('Error: El archivo debe ser tipo .xls (Excel 2003)');
			
			Bibliotecario::ZipSubir($file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");
			
			require_once '../metodos/Excel/reader.php';
			$data = new Spreadsheet_Excel_Reader();
			$data->read($ruta);
			
			$Robot->Validacion5TemporalDel();
			
			for($i=1;$i<=$data->sheets[0]['numRows'];$i++){
				if( isset($data->sheets[0]['cells'][$i][1]) && str_replace('','',$data->sheets[0]['cells'][$i][1]) != ''){
					$Robot->Validacion5TemporalSet($i, 
						$data->sheets[0]['cells'][$i][2], 
						$data->sheets[0]['cells'][$i][3], 
						$data->sheets[0]['cells'][$i][4], 
						$data->sheets[0]['cells'][$i][5]
					);
				}
			}
			Bibliotecario::ZipEliminar($ruta);
			header("location: ../../../baltha-i/validacion/validacion5_detalle.php?acc=R&ID=");
			exit;
		}else die('Archivo vacío');
	//
	}elseif($_POST['paso'] == '3'){
		if($_POST['estado']=='3') $Robot->NotificaJefes($_ROW['LID'], '97');
		echo $Robot->ValidacionModificaEstado($_POST['cs'], $_POST['estado'], $_ROW['UID']);
		exit;
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _validacion5_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['ID']) or ($_GET['acc']!='I' and $_GET['acc']!='R' and $_GET['acc']!='M') ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS GENERALES*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('96') );
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Robot = new Validacion();
			if( $_GET['ID']=='')
				return $Robot->Validacion5EncabezadoVacio();
			else	
				return $Robot->Validacion5EncabezadoGet($_GET['ID']);
		}	
		
		function Lineas(){
			$Robot = new Validacion();
			return $Robot->Validacion5DetalleGet($_GET['ID']);
		}
		
		function Historial(){
			$Robot = new Validacion();
			return $Robot->ValidacionHistorial($_GET['ID']);
		}
		
		function Importacion(){
			$Robot = new Validacion();
			return $Robot->Validacion5TemporalGet();
		}
		
		function LimpiaTabla(){
			$Robot = new Validacion();
			return $Robot->Validacion5TemporalDel();
		}
		
		function Accion($_var){
			$Robot = new Validacion();
			return $Robot->ValidacionAccion($_var);
		}
		
		function Aprobar(){
			if($this->_ROW['ROL'] == '0') return true;
			elseif($this->Securitor->UsuarioPermiso('97')=='E') return true;
			else return false;
		}
		
		function Revisar(){
			if($this->_ROW['ROL'] == '0') return true;
			elseif($this->Securitor->UsuarioPermiso('98')=='E') return true;
			else return false;
		}
		
		function Labels($_var){
			if($_var==0) return 'Blanco matriz';
			elseif($_var==1) return 'Punto 1 curva de calibración (CC 1)';
			elseif($_var==2) return 'Punto 2 curva de calibración (CC 2)';
			elseif($_var==3) return 'Punto 3 curva de calibración (CC 3)';
			elseif($_var==4) return 'Punto 4 curva de calibración (CC 4)';
			elseif($_var==5) return 'Punto 5 curva de calibración (CC 5)';
			elseif($_var==6) return 'Recuperación 1 a nivel bajo';
			elseif($_var==7) return 'Recuperación 2 a nivel bajo';
			elseif($_var==8) return 'Recuperación 3 a nivel bajo';
			elseif($_var==9) return 'Recuperación 4 a nivel bajo';
			elseif($_var==10) return 'Recuperación 5 a nivel bajo';
			elseif($_var==11) return 'Recuperación 1 a nivel medio';
			elseif($_var==12) return 'Recuperación 2 a nivel medio';
			elseif($_var==13) return 'Recuperación 3 a nivel medio';
			elseif($_var==14) return 'Recuperación 4 a nivel medio';
			elseif($_var==15) return 'Recuperación 5 a nivel medio';
			elseif($_var==16) return 'Blanco reactivo';
			elseif($_var==17) return 'Pendiente curva de calibración en matriz';
			elseif($_var==18) return 'Pendiente curva de calibración en disolvente';
		}
		
		function Formato($_NUMBER){
			if( is_numeric($_NUMBER) )
				if($_NUMBER=='-1') return 'N/A';
				else return number_format($_NUMBER, 2, ',', '');
			else
				return $_NUMBER;
		}
	}
//
}
?>