<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

final class _validacion_metodos extends Mensajero{
	private $_ROW = array();
	private $Securitor = '';
	private $inicio = false;
	
	function __construct(){
		$this->Securitor = new Seguridad();
		if(!$this->Securitor->SesionAuth()) $this->Err();
		$this->_ROW = $this->Securitor->SesionGet();
		
		/*PERMISOS*/
		if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('96') );
		/*PERMISOS*/
		
		if(!isset($_POST['estado'])){
			$_POST['desde'] = $_POST['hasta'] = '';
			$_POST['estado'] = 'r';
			$this->inicio = true;
		}
		
		if(!isset($_POST['tipo'])) $_POST['tipo'] = '';
			
		$_POST['LAB'] = $this->_ROW['LID'];
		
		//SI VENIMOS DESDE TABLON, ELIMINO LA NOTIFICACION
		if( isset($_GET['ver']) && isset($_GET['tablon']) )$this->Securitor->TablonDel($_GET['tablon']);
	}

    function Encabezado($_hoja, $_tipo, $_titulo)
    {
        $Qualitor = new Calidad();
        echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
    }
	
	function SolicitudesMuestra(){
		if($this->inicio)
			return array();
		else{
			$Robot = new Validacion();
			return $Robot->ValidacionesGet($this->_ROW['LID'], $_POST['desde'], $_POST['hasta'], $_POST['tipo'], $_POST['estado']);
		}
	}
	
	function Estado($_var){
		$Robot = new Validacion();
		return $Robot->ValidacionesEstado($_var);
	}
	
	function Tipo($_var){
		$Robot = new Validacion();
		return $Robot->ValidacionesTipo($_var);
	}
}
?>