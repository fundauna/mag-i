<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	/*PARA TRAER LA SESION DEL LAB*/
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	$Listator = new Listador();
	$Listator->EquiposCalibrajeLista('validacion', $_GET['list'], $ROW['LID'], basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
}elseif( isset($_POST['_AJAX'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['paso']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot = new Validacion();
	
	if($_POST['paso'] == '1'){
		echo $Robot->Validacion8EncabezadoSet($_POST['tipo'], 
			$_POST['metodo'],
			$_POST['equipo'],
			$_POST['estandar'],
			$_POST['masa1'],
			$_POST['analito'],
			$_POST['pureza'],
			$_POST['vol_ext'],
			$_POST['coefM'],
			$_POST['ali_ext'],
			$_POST['coefB'],
			$_POST['vol_fin'],
			$_POST['curva'],
			$_POST['obs'],
			$_POST['fac1'],
			$_POST['fac2'],
			$_POST['mas1'],
			$_POST['mas2'],
			$_POST['men1'],
			$_POST['men2'],
			$_POST['mass0'],
			$_POST['mass1'],
			$_POST['mass2'],
			$_POST['mass3'],
			$_POST['area0'],
			$_POST['area1'],
			$_POST['area2'],
			$_POST['area3'],
			$_ROW['LID'],
			$_ROW['UID']);	
		exit;
	}elseif($_POST['paso'] == '3'){
		if($_POST['estado']=='3') $Robot->NotificaJefes($_ROW['LID'], '97');
		echo $Robot->ValidacionModificaEstado($_POST['cs'], $_POST['estado'], $_ROW['UID']);
		exit;
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _validacion8_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['ID']) ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS GENERALES*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('96') );
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Robot = new Validacion();
			if( $_GET['ID']=='')
				return $Robot->Validacion8EncabezadoVacio();
			else	
				return $Robot->Validacion8EncabezadoGet($_GET['ID']);
		}
		
		function Historial(){
			$Robot = new Validacion();
			return $Robot->ValidacionHistorial($_GET['ID']);
		}
		
		function Accion($_var){
			$Robot = new Validacion();
			return $Robot->ValidacionAccion($_var);
		}
		
		function Aprobar(){
			if($this->_ROW['ROL'] == '0') return true;
			elseif($this->Securitor->UsuarioPermiso('97')=='E') return true;
			else return false;
		}
		
		function Revisar(){
			if($this->_ROW['ROL'] == '0') return true;
			elseif($this->Securitor->UsuarioPermiso('98')=='E') return true;
			else return false;
		}
	}
//
}
?>