<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	/*PARA TRAER LA SESION DEL LAB*/
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	$Listator = new Listador();
	$Listator->EquiposCalibrajeLista('validacion', $_GET['list'], $ROW['LID'], basename(__FILE__));
	exit;
}elseif( isset($_GET['lista'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	/*PARA TRAER LA SESION DEL LAB*/
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	$Listator = new Listador();
	$Listator->EquiposColumnasLista('validacion', $_GET['lista'], $ROW['LID'], basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
}elseif( isset($_POST['_AJAX'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['paso']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot = new Validacion();
	
	if($_POST['paso'] == '1'){
		echo $Robot->Validacion3EncabezadoSet($_POST['tipo'], 
			$_POST['metodo'], 
			$_POST['masa'], 
			$_POST['masa1'], 
			$_POST['masa2'], 
			$_POST['analitico'], 
			$_POST['pureza'],
			$_POST['pureza1'],
			$_POST['pureza2'], 
			$_POST['equipo1'], 
			$_POST['equipo2'], 
			$_POST['equipo3'],
			$_POST['columna1'], 
			$_POST['columna2'], 
			$_POST['columna3'], 
			$_POST['vol1'], 
			$_POST['vol11'], 
			$_POST['vol12'], 
			$_POST['ali2'], 
			$_POST['ali21'], 
			$_POST['ali22'], 
			$_POST['vol2'],
			$_POST['vol21'],
			$_POST['vol22'], 
			$_POST['IA'], 
			$_POST['mue_vol1'], 
			$_POST['mue_ali2'], 
			$_POST['formulado'], 
			$_POST['mue_vol2'], 
			$_POST['densidad'], 
			$_POST['declarada'], 
			$_POST['unidad'], 
			$_POST['cumple'], 
			$_POST['obs'], 
			$_POST['A'],
			$_POST['B'],
			$_POST['C'],
			$_POST['D'],
			$_POST['G'],
			$_POST['H'],
			$_POST['I'],
			$_POST['X'],
			$_POST['MM'],
			$_POST['MV'],
			$_POST['XX'],
			$_ROW['LID'],
			$_ROW['UID']);	
		exit;
	}if($_POST['paso'] == '2'){
		if( $Robot->ExisteRepetibilidad($_POST['cs']) ){
			$ROW = $Robot->ObtieneLineasRepetibilidad($_POST['cs']);
			for($x=0,$str='';$x<count($ROW);$x++){
				$str .= "<tr>
					<td colspan='12' align='center'>-- Registros importados --</td>
					<td><input type='text' id='MM{$x}' name='MM' class='monto2' onblur='Redondear(this)' value='{$ROW[$x]['MM']}'></td>
					<td><input type='text' id='MV{$x}' name='MV' class='monto2' onblur='Redondear(this)' value='{$ROW[$x]['MV']}'></td>
					<td><input type='checkbox' id='XX{$x}' name='XX' onclick='__calcula()' checked/></td>
				</tr>";
			}
			echo $str;
			exit;
		}
		echo '0';
		exit;
	}elseif($_POST['paso'] == '3'){
		if($_POST['estado']=='3') $Robot->NotificaJefes($_ROW['LID'], '97');
		echo $Robot->ValidacionModificaEstado($_POST['cs'], $_POST['estado'], $_ROW['UID']);
		exit;
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _validacion3_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['ID']) ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS GENERALES*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('96') );
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Robot = new Validacion();
			if( $_GET['ID']=='')
				return $Robot->Validacion3EncabezadoVacio();
			else	
				return $Robot->Validacion3EncabezadoGet($_GET['ID']);
		}
		
		function ObtieneSet($_tipo){
			$Robot = new Validacion();
			if( $_GET['ID']=='') return $Robot->Validacion3DetalleVacio();
			return $Robot->Validacion3Detalle1Get($_GET['ID'], $_tipo);
		}
		
		function ObtieneImportados(){
			if( $_GET['ID']=='') return array();
			$Robot = new Validacion();
			return $Robot->Validacion3Detalle2Get($_GET['ID']);
		}
		
		function Historial(){
			$Robot = new Validacion();
			return $Robot->ValidacionHistorial($_GET['ID']);
		}
		
		function Accion($_var){
			$Robot = new Validacion();
			return $Robot->ValidacionAccion($_var);
		}
		
		function Aprobar(){
			if($this->_ROW['ROL'] == '0') return true;
			elseif($this->Securitor->UsuarioPermiso('97')=='E') return true;
			else return false;
		}
		
		function Revisar(){
			if($this->_ROW['ROL'] == '0') return true;
			elseif($this->Securitor->UsuarioPermiso('98')=='E') return true;
			else return false;
		}
	}
//
}
?>