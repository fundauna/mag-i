<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UNA LISTA
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['paso']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot = new Validacion();
	
	if($_POST['paso'] == '1'){
		echo $Robot->Validacion2EncabezadoSet($_POST['tipo'], 
			$_POST['vol_bal'], 
			$_POST['mv'],
			$_POST['vol_int'],
			$_POST['ali_mue'],
			$_POST['densidad'],
			$_POST['ali_enr'],
			$_POST['estandar'],
			$_POST['ali_dil'], 
			$_POST['vol_bal2'],
			$_POST['masa'],
			$_POST['vol_dil'],
			$_POST['mm'],
			$_POST['pureza'],
			$_POST['unidad'],
			$_POST['IA'], 
			$_POST['cumple'], 
			$_POST['obs'], 
			$_POST['A'],
			$_POST['B'],
			$_POST['C'],
			$_POST['D'],
			$_POST['G'],
			$_POST['H'],
			$_POST['I'],
			$_POST['MM'],
			$_POST['MV'],
			$_POST['X'],
			$_ROW['LID'],
			$_ROW['UID']);	
		exit;
	}elseif($_POST['paso'] == '3'){
		if($_POST['estado']=='3') $Robot->NotificaJefes($_ROW['LID'], '97');
		echo $Robot->ValidacionModificaEstado($_POST['cs'], $_POST['estado'], $_ROW['UID']);
		exit;
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _validacion2_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['ID']) ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS GENERALES*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('96') );
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Robot = new Validacion();
			if( $_GET['ID']=='')
				return $Robot->Validacion2EncabezadoVacio();
			else	
				return $Robot->Validacion2EncabezadoGet($_GET['ID']);
		}
		
		function ObtieneGrupo1(){
			if( $_GET['ID']=='') return array();
			$Robot = new Validacion();
			return $Robot->Validacion2DetalleGet($_GET['ID'], 0);
		}
		
		function ObtieneGrupo2(){
			if( $_GET['ID']=='') return array();
			$Robot = new Validacion();
			return $Robot->Validacion2DetalleGet($_GET['ID'], 1);
		}
		
		function Historial(){
			$Robot = new Validacion();
			return $Robot->ValidacionHistorial($_GET['ID']);
		}
		
		function Accion($_var){
			$Robot = new Validacion();
			return $Robot->ValidacionAccion($_var);
		}
		
		function Aprobar(){
			if($this->_ROW['ROL'] == '0') return true;
			elseif($this->Securitor->UsuarioPermiso('97')=='E') return true;
			else return false;
		}
		
		function Revisar(){
			if($this->_ROW['ROL'] == '0') return true;
			elseif($this->Securitor->UsuarioPermiso('98')=='E') return true;
			else return false;
		}
	}
//
}
?>