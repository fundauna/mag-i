<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	/*PARA TRAER LA SESION DEL LAB*/
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	$Listator->EquiposCalibrajeLista('validacion', '', $ROW['LID'], basename(__FILE__));
	exit; 
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
}elseif( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['paso']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Robot = new Validacion();
	
	if($_POST['paso'] == '0'){
		if( !$Robot->Validacion5EstaAprobada($_POST['validacion']) ){
			echo '2';
			exit;
		}	
		
		die('1');
	}elseif($_POST['paso'] == '1'){
		if( !$Robot->Validacion5EstaAprobada($_POST['validacion']) ){
			echo '2';
			exit;
		}
		
		echo $Robot->Validacion6EncabezadoSet($_POST['accion'], $_POST['cs'],
			$_POST['validacion'], 
			$_POST['objetivo'],
			$_POST['analitos'],
			$_POST['ambito'],
			$_POST['tipo_metodo'],
			$_POST['cod_metodo'],
			$_POST['precision'],
			$_POST['veracidad'],
			$_POST['limite1'],
			$_POST['limite2'],
			$_POST['linealidad'],
			$_POST['especificidad'],
			$_POST['efecto'],
			$_POST['robustez'],
			$_POST['inc'],
			$_POST['ref'],
			$_POST['modif'],
			$_POST['tecnica'],
			$_POST['interno'],
			$_POST['bitacora'],
			$_POST['pagina'],
			$_POST['equipo'],
			$_POST['otros'],
			$_POST['resumen'],
			$_POST['tratamiento'],
			$_POST['declaracion'],
			$_ROW['LID'],
			$_ROW['UID']);	
		exit;
	}elseif($_POST['paso'] == '2'){
		if($_POST['estado']=='3') $Robot->NotificaJefes($_ROW['LID'], '97');
		echo $Robot->ValidacionModificaEstado($_POST['cs'], $_POST['estado'], $_ROW['UID']);	
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _validacion6_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('96') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Robot = new Validacion();
			if( $_GET['ID']=='')
				return $Robot->Validacion6EncabezadoVacio();
			else	
				return $Robot->Validacion6EncabezadoGet($_GET['ID']);
		}
		
		function Accion($_var){
			$Robot = new Validacion();
			return $Robot->ValidacionAccion($_var);
		}
		
		function Historial(){
			$Robot = new Validacion();
			return $Robot->ValidacionHistorial($_GET['ID']);
		}
		
		function Detalle($_validacion){
			$Robot = new Validacion();
			return $Robot->Validacion6DetalleGet($_validacion);
		}
	}
//
}
?>