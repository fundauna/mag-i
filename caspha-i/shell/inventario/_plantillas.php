<?php

/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 * ***************************************************************** */
if (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth()) {
        die('-0');
    }
    if (!isset($_POST['perfil'])) {
        die('-1');
    }

    $Inventario = new Inventario();

    if (isset($_POST['PermisosBuscar'])) {
        $str = '<select id="asignados" style="width:340px;" size="10">';
        $ROW = $Inventario->OpcionalesAsignados($_POST['perfil']);
        for ($x = 0; $x < count($ROW); $x++) {
            $str .= "<option value='{$ROW[$x]['id']}'>{$ROW[$x]['nombre']}</option>";
        }
        $str .= '</select>';
        die($str);
    } elseif (isset($_POST['PermisosAgrega'])) {
        echo $Inventario->OpcionalesIME($_POST['perfil'], $_POST['permiso'], 'I');
        exit;
    } elseif (isset($_POST['PermisosElimina'])) {
        echo $Inventario->OpcionalesIME($_POST['perfil'], $_POST['permiso'], 'D');
        exit;
    }
} else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _plantillas extends Mensajero {

        private $_ROW = array();
        private $Securitor = '';
        private $Inventor = '';

        function __construct() {
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth())
                $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0')
                $this->Portero($this->Securitor->UsuarioPermiso('A1'));
            /* PERMISOS */

            $this->Inventor = new Inventario();
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function Tipos() {
            return $this->Inventor->TiposMuestra($this->_ROW['LID'], $this->_ROW['ROL']);
        }

        function Campos() {
            return $this->Inventor->OpcionalesMuestra();
        }

    }

}
?>