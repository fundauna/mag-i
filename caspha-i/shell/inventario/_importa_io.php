<?php

require_once "GOD.php";

function _FREE($_VAR)
{
    $_VAR = str_replace('á', 'a', $_VAR);
    $_VAR = str_replace('é', 'e', $_VAR);
    $_VAR = str_replace('í', 'i', $_VAR);
    $_VAR = str_replace('ó', 'o', $_VAR);
    $_VAR = str_replace('ú', 'u', $_VAR);
    $_VAR = str_replace('ñ', 'n', $_VAR);
    $_VAR = str_replace("'", '', $_VAR);
    $_VAR = str_replace("(", '', $_VAR);
    $_VAR = str_replace(")", '', $_VAR);
    return $_VAR;
}

include 'Classes/PHPExcel/IOFactory.php';

if ($_FILES['archivo']['size'] > 0) {
    $ruta = $_FILES['archivo']['name'];
    $file = $_FILES['archivo']['tmp_name'];
    $_ruta = str_replace(' ', '', $ruta);
    @move_uploaded_file($file, $_ruta);

    $XLFileType = PHPExcel_IOFactory::identify($_ruta);
    $objReader = PHPExcel_IOFactory::createReader($XLFileType);
    $objPHPExcel = $objReader->load($_ruta);
    try {
        $objWorksheet = $objPHPExcel->setActiveSheetIndexByName('R-80 Ing y sal materiales');
    } catch (Exception $e) {
        die('Error nombre de hoja erronea');
    }
    $highestRow = $objPHPExcel->getActiveSheet()->getHighestRow();

    for ($row = 9; $row <= $highestRow; $row++) {
        $descripcion = $objPHPExcel->getActiveSheet()->getCell('B' . $row)->getFormattedValue();
        if (strcmp($descripcion, '') != 0) {
            $descripcion = _FREE($descripcion);

            $codigo = _FREE($objPHPExcel->getActiveSheet()->getCell('A' . $row)->getFormattedValue());
            $orden = _FREE($objPHPExcel->getActiveSheet()->getCell('C' . $row)->getFormattedValue());
            $factura = _FREE($objPHPExcel->getActiveSheet()->getCell('D' . $row)->getFormattedValue());
            $proveedor = _FREE($objPHPExcel->getActiveSheet()->getCell('E' . $row)->getFormattedValue());
            $costo = _FREE($objPHPExcel->getActiveSheet()->getCell('F' . $row)->getFormattedValue());
            $ingresobi = $objPHPExcel->getActiveSheet()->getCell('G' . $row)->getFormattedValue();
            $ingresolcc = $objPHPExcel->getActiveSheet()->getCell('H' . $row)->getFormattedValue();
            if (strcmp($ingresobi, '') == 0 || $ingresobi == null || $ingresobi == '' || $ingresobi == 'na' || $ingresobi == 'NA' || $ingresobi == 'N/A' || $ingresobi == 'N.A.' || $ingresobi == 'n.a' || $ingresobi == 'N.A') {
                $ingresobi = '1900-01-01';
            }
            if (strcmp($ingresolcc, '') == 0 || $ingresolcc == null || $ingresolcc == '' || $ingresolcc == 'na' || $ingresolcc == 'NA' || $ingresolcc == 'N/A' || $ingresolcc == 'N.A.' || $ingresolcc == 'n.a' || $ingresobi == 'N.A') {
                $ingresolcc = '1900-01-01';
            }
            $salida = _FREE($objPHPExcel->getActiveSheet()->getCell('I' . $row)->getFormattedValue());
            $cantidad = _FREE($objPHPExcel->getActiveSheet()->getCell('J' . $row)->getFormattedValue());
            $unidad = _FREE($objPHPExcel->getActiveSheet()->getCell('K' . $row)->getFormattedValue());
            $obs = '';

            $sal = str_replace('.', '', $salida);
            $sal = str_replace('/', '', $salida);
            $sal = str_replace(' ', '', $salida);
            $sal = strtoupper($sal);

            // 1 ingreso : 2 salida

            if (($sal == "N.A.") || ($sal == "N/A") || ($sal == "NA") || ($sal == "N A") ) {
                $tipo = 1;
            } else {
                $tipo = 2;
            }


            $ROW = _QUERY("SELECT MAX(id) as id FROM INV063;");
            $id = $ROW[0]['id'];

            if ($id != null || $id == 0) {
                $id++;
                _TRANS("INSERT INTO INV063 VALUES ($id , $tipo, '$codigo', '$descripcion', '$orden', '$factura', '$proveedor', '$costo', '$ingresobi', '$ingresolcc', '$salida', '$cantidad', '$unidad', '$obs')");
            } else {
                _TRANS("INSERT INTO INV063 VALUES (0 , $tipo, '$codigo', '$descripcion', '$orden', '$factura', '$proveedor', '$costo', '$ingresobi', '$ingresolcc', '$salida', '$cantidad', '$unidad', '$obs')");
            }
        }
    }

    echo "Inventario importado correctamente!";
    @unlink($_ruta);
    exit();
} else {
    die('Archivo vacio');
}
exit;
?>