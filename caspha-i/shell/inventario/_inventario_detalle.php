<?php
/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UNA LISTA
 * ***************************************************************** */
if (isset($_GET['list'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    /* PARA TRAER LA SESION DEL LAB */
    $Securitor = new Seguridad();
    $ROW = $Securitor->SesionGet();
    $Listator = new Listador();
    $Listator->ProveedoresLista('inventario', $ROW['LID'], basename(__FILE__));
    exit;
    /*     * *****************************************************************
      PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
     * ***************************************************************** */
} elseif (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth())
        die('-0');
    $_ROW = $Securitor->SesionGet();

    $Inventor = new Inventario();


    if ($_POST['_AJAX'] == '1') {

        if ($_POST['accion'] == 'I') {
            //SI ES INCLUSION, DEBO OBTENER EL ULTIMO ID
            $_POST['id'] = $Inventor->InventarioConsecutivo();
        }

        $x = $Inventor->InventarioIME($_POST['id'], $_POST['tipo'], $_POST['codigo'], $_POST['orden'], $_POST['factura'], $_POST['proveedor'], $_POST['acta'], $_POST['nombre'], $_POST['marca'], $_POST['marcacomercial'], $_POST['presentacion'], $_POST['es'], $_POST['minimo1'], $_POST['minimo2'], $_POST['stock1'], $_POST['stock2'], $_POST['ubicacion1'], $_POST['ubicacion2'], $_POST['adicional'], $_POST['accion'], isset($_POST['valorinv']) ? $_POST['valorinv'] : [], isset($_POST['idcat']) ? $_POST['idcat'] : []);
    } else if ($_POST['_AJAX'] == 2) {
        $id = $Inventor->InventarioEncuentraxCatalogo($_POST['id'], $_POST['familia']);
        if (count($id) > 0) {
            $Inventor->InventarioActualizaEstado($id[0]['id']);
            $x = $id[0]['id'];
        } else {
            $x = 0;
        }
    } elseif ($_POST['_AJAX'] == 3) {
        $campos = $Inventor->inventarioAdicionalCanpos($_POST['familia']);
        ?>
        <table width="100%">
        <?php foreach ($campos as $dato): ?>
                <tr><td><?= $dato['nombre'] ?></td><td> <input type="hidden" name="idcat[]" value="<?= $dato['id'] ?>" /> <input type="text" name="valorinv[]" value="" /> </td></tr>
            <?php endforeach; ?>
        </table>
            <?php
            exit;
        }
        echo $x;
        exit;
    } else {
        /*         * *****************************************************************
          DECLARACION CLASE GESTORA
         * ***************************************************************** */

        function __autoload($_BaseClass) {
            require_once "../../melcha-i/{$_BaseClass}.php";
        }

        final class _inventario_detalle extends Mensajero {

            private $_ROW = array();
            private $Securitor = '';
            private $Inventor;

            function __construct() {
                //SOLO SE PUEDE ABRIR COMO POP-UP MODAL
                $this->ValidaModal();

                if (!isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M'))
                    die('Error de parámetros');

                $this->Securitor = new Seguridad();
                if (!$this->Securitor->SesionAuth())
                    $this->Err();
                $this->_ROW = $this->Securitor->SesionGet();
                /* PERMISOS */
                if ($this->_ROW['ROL'] != '0')
                    $this->Portero($this->Securitor->UsuarioPermiso('A4'));
                /* PERMISOS */

                $this->Inventor = new Inventario();
            }

            function Encabezado($_hoja, $_tipo, $_titulo)
            {
                $Qualitor = new Calidad();
                echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
            }

            function ObtieneDatos() {


                if (!isset($_GET['ID']) or $_GET['ID'] == '')
                    return $this->Inventor->InventarioVacio();
                else
                    return $this->Inventor->InventarioDetalleGet($_GET['ID']);
            }

            function OpcionalesMuestra() {

                $this->Inventor->InventarioOpcionalesGet($_GET['ID']);
            }

            function PresentacionesMuestra() {

                return $this->Inventor->PresentacionesMuestra();
            }

            function TiposMuestra() {

                return $this->Inventor->TiposMuestra($this->_ROW['LID'], $this->_ROW['ROL']);
            }

            function inventarioAdicionales($_familia) {

                return $_GET['acc'] == 'I' ? [] : $this->Inventor->inventarioAdicionalGet($_GET['ID'],$_familia);
            }

        }

    }
    ?>