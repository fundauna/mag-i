<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['id']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Inventor = new Inventario();
		
	echo $Inventor->InventarioSalidaSet($_POST['id'], $_POST['tipo'], $_POST['tipo_tras'], $_POST['cant_tras'], $_POST['cant_bod1'], $_POST['cant_bod2'], $_ROW['UID'], $_POST['producto']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _inventario_salida extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if(!isset($_POST['desde'])){
				$_POST['desde'] = $_POST['hasta'] = $_POST['tipo'] = '';
			}else $_GET['ID'] = $_POST['articulo'];
			
			if( !isset($_GET['ID']) ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('A5') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Inventor = new Inventario();	
			return $Inventor->InventarioEncabezado($_GET['ID']);
		}
			
		function Disponibles(){
			$Inventor = new Inventario();
			return $Inventor->InventarioDisponiblesGet($_GET['ID']);
		}
	}
//
}
?>