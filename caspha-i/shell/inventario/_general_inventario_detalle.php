<?php

if (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth()) {
        die('-0');
    }
    $_ROW = $Securitor->SesionGet();

    $Inventor = new Inventario();

    if ($_POST['_AJAX'] == '1') {
        echo $Inventor->General_inventarioIME($_POST['id'], $_POST['accion'], $_POST['codigo'], $_POST['descripcion'], $_POST['ubicacion'], $_POST['cantidadlcc'], $_POST['cantidadpavas'], $_POST['cantidadtotal'], $_POST['cantidadtransito'], $_POST['nivel'], $_POST['costounitariod'], $_POST['costounitarioc'], $_POST['costototal'], $_POST['presentacion'], $_POST['parte'], $_POST['equipo'], $_POST['ingreso'], $_POST['analisis'], $_POST['consumoanalisis'], $_POST['cantidadanalisis'], $_POST['consumoanual'], $_POST['medida'], $_POST['seguridadmax'], $_POST['reorden'], $_POST['vencimiento'], $_POST['estado'], $_POST['pedir'], $_POST['abc'], $_POST['obs'], $_POST['encargado']);
    }
    exit;
} else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _general_inventario_detalle extends Mensajero {

        private $_ROW = array();
        private $Securitor = '';
        private $Inventor;

        function __construct() {
            //SOLO SE PUEDE ABRIR COMO POP-UP MODAL
            $this->ValidaModal();

            if (!isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M')) {
                die('Error de parámetros');
            }

            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth()) {
                $this->Err();
            }
            $this->_ROW = $this->Securitor->SesionGet();
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0') {
                $this->Portero($this->Securitor->UsuarioPermiso('A4'));
            }
            /* PERMISOS */

            $this->Inventor = new Inventario();
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function ObtieneDatos() {


            if (!isset($_GET['ID']) or $_GET['ID'] == '') {
                return $this->Inventor->General_inventarioVacio();
            } else {
                return $this->Inventor->General_inventario($_GET['ID']);
            }
        }

    }

}
?>