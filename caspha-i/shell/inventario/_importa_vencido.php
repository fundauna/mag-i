<?php

require_once "GOD.php";

function _FREE($_VAR)
{
    $_VAR = str_replace('á', 'a', $_VAR);
    $_VAR = str_replace('é', 'e', $_VAR);
    $_VAR = str_replace('í', 'i', $_VAR);
    $_VAR = str_replace('ó', 'o', $_VAR);
    $_VAR = str_replace('ú', 'u', $_VAR);
    $_VAR = str_replace('ñ', 'n', $_VAR);
    $_VAR = str_replace("'", '', $_VAR);
    $_VAR = str_replace("(", '', $_VAR);
    $_VAR = str_replace(")", '', $_VAR);
    return $_VAR;
}

include 'Classes/PHPExcel/IOFactory.php';

if ($_FILES['archivo']['size'] > 0) {
    $ruta = $_FILES['archivo']['name'];
    $file = $_FILES['archivo']['tmp_name'];
    $_ruta = str_replace(' ', '', $ruta);
    @move_uploaded_file($file, $_ruta);

    $XLFileType = PHPExcel_IOFactory::identify($_ruta);
    $objReader = PHPExcel_IOFactory::createReader($XLFileType);
    $objPHPExcel = $objReader->load($_ruta);
    try {
        $objWorksheet = $objPHPExcel->setActiveSheetIndexByName('R-82 Material vencido');
    } catch (Exception $e) {
        die('Error nombre de hoja erronea');
    }
    $highestRow = $objPHPExcel->getActiveSheet()->getHighestRow();

    for ($row = 5; $row <= $highestRow; $row++) {
        $codigo = $objPHPExcel->getActiveSheet()->getCell('A' . $row)->getFormattedValue();
        if (strcmp($codigo, '') != 0) {
            $codigo = _FREE($codigo);
            $material = _FREE($objPHPExcel->getActiveSheet()->getCell('B' . $row)->getFormattedValue());
            $costo = _FREE($objPHPExcel->getActiveSheet()->getCell('C' . $row)->getFormattedValue());
            $vencimiento = _FREE($objPHPExcel->getActiveSheet()->getCell('D' . $row)->getFormattedValue());
            $cantidad = _FREE($objPHPExcel->getActiveSheet()->getCell('E' . $row)->getFormattedValue());
            $unidad = _FREE($objPHPExcel->getActiveSheet()->getCell('F' . $row)->getFormattedValue());
            $total = _FREE($objPHPExcel->getActiveSheet()->getCell('G' . $row)->getFormattedValue());
            $obs = '';

            $ROW = _QUERY("SELECT MAX(id) as id FROM INV064;");
            $id = $ROW[0]['id'];

            if ($id != null || $id == 0) {
                $id++;
                _TRANS("INSERT INTO INV064 VALUES ($id , '$codigo', '$material', '$costo', '$vencimiento', '$cantidad', '$unidad', '$total', '$obs')");
            } else {
                _TRANS("INSERT INTO INV064 VALUES (0 , '$codigo', '$material', '$costo', '$vencimiento', '$cantidad', '$unidad', '$total', '$obs')");
            }
        }else{
            break;
        }
    }

    echo "Inventario importado correctamente!";
    @unlink($_ruta);
    exit();
} else {
    die('Archivo vacio');
}
exit;
?>