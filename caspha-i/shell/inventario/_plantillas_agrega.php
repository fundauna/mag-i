<?php

/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 * ***************************************************************** */
if (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    function getColumnLetter($number) {
        $prefix = '';
        $suffix = '';
        $prefNum = intval($number / 26);
        if ($number > 25) {
            $prefix = getColumnLetter($prefNum - 1);
        }
        $suffix = chr(fmod($number, 26) + 65);
        return $prefix . $suffix;
    }

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth()) {
        die('-0');
    }
    if (!isset($_POST['nombre'])) {
        die('-1');
    }

    $Inventario = new Inventario();
    $_POST['nombre'] = $Securitor->FreeAcento($_POST['nombre']);
    $cs = $Inventario->inventarioAdicionalcatalogoConsecutivo($_POST['nombre']);
    if($cs == -2){
        echo '-2';
        exit();
    }
    if ($Inventario->inventarioAdicionalcatalogoIME($cs, $_POST['nombre'], 1)) {
        include 'Classes/PHPExcel/IOFactory.php';

        $_ruta = "../../docs/formato.xlsx";
        $XLFileType = PHPExcel_IOFactory::identify($_ruta);
        $objReader = PHPExcel_IOFactory::createReader($XLFileType);
        $objPHPExcel = $objReader->load($_ruta);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $XLFileType);
        $objWorksheet = $objPHPExcel->setActiveSheetIndexByName('Inventario');

        $encabezado = $objPHPExcel->getActiveSheet()->getCell(getColumnLetter(1) . '2')->getFormattedValue();
        for ($k = 1; !empty($encabezado); $k++) {
            $encabezado = trim($objPHPExcel->getActiveSheet()->getCell(getColumnLetter($k) . '2')->getFormattedValue());
        }

        $siguiente = getColumnLetter($k - 1);
        $siguiente .= '2'; //Fija en fila 2 del documento, preparando para escribir

        $objPHPExcel->getActiveSheet()->setCellValue($siguiente, $_POST['nombre']);
        $objWriter->save($_ruta);
        echo '1';
    }
} else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _plantillas_agrega extends Mensajero {

        private $_ROW = array();
        private $Securitor = '';
        private $Inventor = '';

        function __construct() {
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth())
                $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0')
                $this->Portero($this->Securitor->UsuarioPermiso('A1'));
            /* PERMISOS */

            $this->Inventor = new Inventario();
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function Tipos() {
            return $this->Inventor->TiposMuestra($this->_ROW['LID'], $this->_ROW['ROL']);
        }

        function Campos() {
            return $this->Inventor->OpcionalesMuestra();
        }

    }

}
?>