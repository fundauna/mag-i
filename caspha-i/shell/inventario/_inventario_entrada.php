<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	$Listator->ProveedoresLista('inventario', $ROW['LID'], basename(__FILE__));
	exit;
}elseif( isset($_POST['_AJAX'])){ 
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['codigo']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
		
	$Inventor = new Inventario();
	
	if( $Inventor->InventarioESValidaCodigo($_ROW['LID'], $_POST['codigo']) ){
		echo '-4';
		exit;
	}
	
	echo '1';
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
}elseif( isset($_POST['prov'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Inventor = new Inventario();
	//
	/*if( $Inventor->InventarioESValidaCodigo($_ROW['LID'], $_POST['codigo']) ){
		echo'<script>alert("El c�digo interno ya existe");window.close();</script>';
		exit();
	}*/
	//
	$cs = $Inventor->InventarioMovimientoCs();
	
	if($Inventor->InventarioEntradaSet($cs, $_POST['producto'], $_POST['codigo'], $_POST['lote'], $_POST['oc_inicio'], $_POST['tramite'], $_POST['oc'], $_POST['req'], $_POST['factura'], $_POST['prov'], $_POST['stock1'], $_POST['stock2'], $_POST['costo'], $_POST['vence'], $_POST['vencimiento'], $_POST['aviso'], $_POST['certificado'], $_POST['pureza'], $_ROW['UID']) ){		
		//SUBIR ARCHIVO			
		if($_FILES['archivo']['size'] > 0){
			$nombre = $_FILES['archivo']['name'];
			$file = $_FILES['archivo']['tmp_name'];
			$ruta = "../../docs/inventario/{$cs}.zip";
			Bibliotecario::ZipCrear($nombre, $file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");
		}
		//SUBIR ARCHIVO			
		echo'<script>alert("Transaccion finalizada");window.close();</script>';
		exit();
	}else
		echo 'Error al guardar los datos';
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _inventario_entrada extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['ID']) ) die('Error de par�metros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('A5') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Inventor = new Inventario();	
			return $Inventor->InventarioEncabezado($_GET['ID']);
		}
	}
}
?>