<?php

/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 * ***************************************************************** */
if (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    if (!isset($_POST['id'])) {
        die('-1');
    }

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth()) {
        die('-0');
    }

    $Inventor = new Inventario();
    echo $Inventor->IngresoSalidaIME($_POST['id'], $_POST['accion'], '', '', '', '', '', '', '', '', '', '', '', '', '');
    exit;
} else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _ingreso_salida extends Mensajero {

        private $_ROW = array();
        private $Securitor = '';
        private $inicio = false;

        function __construct() {
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth()) {
                $this->Err();
            }
            $this->_ROW = $this->Securitor->SesionGet();
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0') {
                $this->Portero($this->Securitor->UsuarioPermiso('A4'));
            }
            /* PERMISOS */
            if (!isset($_POST['codigo'])) {
                $_POST['codigo'] = $_POST['tipo'] = $_POST['descripcion'] = $_POST['orden'] = $_POST['factura'] = $_POST['proveedor'] = $_POST['desde'] = $_POST['hasta'] = '';
            }
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function TiposMuestra() {
            $Inventor = new Inventario();
            return $Inventor->TiposMuestra($this->_ROW['LID'], $this->_ROW['ROL']);
        }

        function InventarioMuestra() {
            $Inventor = new Inventario();
            return $Inventor->IngresoSalida('', $_POST['tipo'], $_POST['codigo'], $_POST['descripcion'], $_POST['orden'], $_POST['factura'], $_POST['proveedor'], $_POST['desde'], $_POST['hasta']);
        }

    }

}
?>