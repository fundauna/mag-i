<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Inventor = new Inventario();
	
	if( isset($_POST['ProductoBuscarTipo']) ){
	//BUSQUEDA DE TIPOS
		if( !isset($_POST['tipo']) ) die('-1');
	
		$str = '<select id="productos" style="width:340px;" size="10">';
		$ROW = $Inventor->InventarioResumenGet('', '', $_POST['tipo'], $_ROW['LID'], true);
		for($x=0;$x<count($ROW);$x++){
			$ROW[$x]['nombre'] = utf8_encode($ROW[$x]['nombre']);
			$str .= "<option value='{$ROW[$x]['id']}'>({$ROW[$x]['codigo']}) {$ROW[$x]['nombre']} {$ROW[$x]['unidades']}</option>";	
		}
		$str .= '</select>';
		die($str);
	}elseif( isset($_POST['ProductoBuscarAsignados']) ){
	//BUSQUEDA DE ASIGNADOS (ARQUEO)
		if( !isset($_POST['analisis']) ) die('-1');
	
		$str = '<select id="asignados" style="width:340px;" size="10">';
		$ROW = $Inventor->ArqueoRequerido($_POST['analisis']);
		for($x=0;$x<count($ROW);$x++){
			$ROW[$x]['nombre'] = utf8_encode($ROW[$x]['nombre']);
			$str .= "<option value='{$ROW[$x]['id']}'>{$ROW[$x]['cantidad']}x({$ROW[$x]['codigo']}) {$ROW[$x]['nombre']} {$ROW[$x]['unidades']}</option>";	
		}
		$str .= '</select>';
		die($str);
	}elseif( isset($_POST['ProductoAgrega']) ){
		echo $Inventor->ArqueoIME($_POST['analisis'], $_POST['producto'], $_POST['cantidad'], $_POST['obs'], 'I');
		exit;
	}elseif( isset($_POST['ProductoElimina']) ){
		echo $Inventor->ArqueoIME($_POST['analisis'], $_POST['producto'], '', '', 'D');
		exit;
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _arqueo extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('A4') );
			/*PERMISOS*/
			if(!isset($_POST['nombre'])){
				$_POST['codigo'] = $_POST['nombre'] = $_POST['tipo'] = $_POST['estado'] = '';
				$_POST['lolo'] = '2';
				$this->inicio = true;
			}
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function MetodosMuestra(){
			$Robot = new Varios();
			return $Robot->IngredientesMuestra($this->_ROW['LID']);
		}
		
		function TiposMuestra(){
			$Inventor = new Inventario();
			return $Inventor->TiposMuestra($this->_ROW['LID'], $this->_ROW['ROL']);		
		}
	}
}
?>