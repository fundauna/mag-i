<?php

require_once "GOD.php";

function _FREE($_VAR) {
    $_VAR = str_replace('á', 'a', $_VAR);
    $_VAR = str_replace('é', 'e', $_VAR);
    $_VAR = str_replace('í', 'i', $_VAR);
    $_VAR = str_replace('ó', 'o', $_VAR);
    $_VAR = str_replace('ú', 'u', $_VAR);
    $_VAR = str_replace('ñ', 'n', $_VAR);
    $_VAR = str_replace("'", '', $_VAR);
    $_VAR = str_replace("(", '', $_VAR);
    $_VAR = str_replace(")", '', $_VAR);
    return $_VAR;
}

function _FREE2($_VAR) {
    $_VAR = str_replace(" ", '', $_VAR);
    $_VAR = str_replace(",", '', $_VAR);
    $_VAR = str_replace(">", '', $_VAR);
    $_VAR = str_replace(".", '', $_VAR);
    $_VAR = str_replace("-", '', $_VAR);
    return $_VAR;
}

function getColumnLetter($number) {
    $prefix = '';
    $suffix = '';
    $prefNum = intval($number / 26);
    if ($number > 25) {
        $prefix = getColumnLetter($prefNum - 1);
    }
    $suffix = chr(fmod($number, 26) + 65);
    return $prefix . $suffix;
}

function tipoInventarioId($tipo) {
    return _QUERY("SELECT id FROM INV000 WHERE nombre = '{$tipo}' AND lab = '{$_POST['lab']}';");
}

function presentacionInventarioId($presentacion) {
    return _QUERY("SELECT id FROM INV001 WHERE nombre like '%{$presentacion}%';");
}

function inventarioAdicionalcatalogoId($nombre) {
    return _QUERY("SELECT id FROM INVACAT WHERE nombre like '%{$nombre}%';");
}

function inventarioAdicionalIME($_idcat, $_idinv, $_valor, $_sigla = 'I') {
    if ($_sigla == 'I') {
        return _TRANS("INSERT INTO INVA VALUES('{$_idcat}','{$_idinv}','{$_valor}');");
    } else {
        return _TRANS("UPDATE INVA SET valor = '{$_valor}' WHERE idcat = '{$_idcat}' AND idinv = '{$_idinv}';");
    }
}

function inventarioAdicionalcatalogoIME($_id, $_nombre, $_tipo) {
    return _TRANS("INSERT INTO INVACAT VALUES('{$_id}','$_nombre','{$_tipo}')");
}

function inventarioConsecutivo($_codigo) {
    $CS = _QUERY("SELECT id AS inventario FROM INV002 WHERE codigo = '{$_codigo}';");
    if (!$CS) {
        _TRANS("UPDATE MAG000 SET inventario = inventario + 1;");
        $CS = _QUERY("SELECT inventario FROM MAG000;");
    }
    return $CS[0]['inventario'];
}

function inventarioTiposConsecutivo() {
    _TRANS("UPDATE MAG000 SET inventario_tipos = inventario_tipos + 1;");
    $b = _QUERY("SELECT inventario_tipos FROM MAG000;");
    return $b[0]['inventario_tipos'];
}

function inventarioAdicionalcatalogoConsecutivo() {
    _TRANS("UPDATE MAG000 SET INVACAT = INVACAT + 1;");
    $b = _QUERY("SELECT INVACAT FROM MAG000;");
    return $b[0]['INVACAT'];
}

function inventarioPresentacionConsecutivo() {
    _TRANS("UPDATE MAG000 SET inventario_unidades = inventario_unidades + 1;");
    $b = _QUERY("SELECT inventario_unidades FROM MAG000;");
    return $b[0]['inventario_unidades'];
}

function esHomologa($es) {
    //$es == 'SI' || $es == 'Si' || $es == 'Sí' || $es == 'si' || $es == '1'
    switch ($es) {
        case "SI":
        case "Si":
        case "Sí":
        case "si":
        case "1":
        case "S�":
            return '1';
        default :
            return '0';
    }
}

include 'Classes/PHPExcel/IOFactory.php';

if ($_FILES['archivo']['size'] > 0) {
    $ruta = $_FILES['archivo']['name'];
    $file = $_FILES['archivo']['tmp_name'];
    $_ruta = str_replace(' ', '', $ruta);
    @move_uploaded_file($file, $_ruta);

    $XLFileType = PHPExcel_IOFactory::identify($_ruta);
    $objReader = PHPExcel_IOFactory::createReader($XLFileType);
    $objPHPExcel = $objReader->load($_ruta);
    try {
        $objWorksheet = $objPHPExcel->setActiveSheetIndexByName('Inventario');
    } catch (Exception $e) {
        die('Error nombre de hoja erronea');
    }
    $encabezado = _FREE($objPHPExcel->getActiveSheet()->getCell(getColumnLetter(0) . '2')->getFormattedValue());
    $tipo = _FREE($objPHPExcel->getActiveSheet()->getCell('A3')->getFormattedValue());
    for ($i = 3, $j = 0; !empty($encabezado) && !empty($tipo);) {

        $consecutivo = inventarioConsecutivo($objPHPExcel->getActiveSheet()->getCell('B' . $i)->getFormattedValue());
        $tipoCatalogo = tipoInventarioId($tipo);
        if (empty($tipoCatalogo)) { //No existe en catalogo
            $consectipoCatalogo = inventarioTiposConsecutivo();
            _TRANS("INSERT INTO INV000 VALUES('{$consectipoCatalogo}', '{$tipo}', '{$_POST['lab']}');");
            $tipoCatalogo = tipoInventarioId($tipo);
        }
        $codigo = $objPHPExcel->getActiveSheet()->getCell('B' . $i)->getFormattedValue();
        $orden = $objPHPExcel->getActiveSheet()->getCell('C' . $i)->getFormattedValue();
        $factura = $objPHPExcel->getActiveSheet()->getCell('D' . $i)->getFormattedValue();
        $proveedor = $objPHPExcel->getActiveSheet()->getCell('E' . $i)->getFormattedValue();
        $acta = $objPHPExcel->getActiveSheet()->getCell('F' . $i)->getFormattedValue();
        $nombre = _FREE($objPHPExcel->getActiveSheet()->getCell('G' . $i)->getFormattedValue());
        $marca = _FREE($objPHPExcel->getActiveSheet()->getCell('H' . $i)->getFormattedValue());
        $presentacion = $objPHPExcel->getActiveSheet()->getCell('I' . $i)->getFormattedValue();
        $presentacionCatalogo = presentacionInventarioId($presentacion);
        if (empty($presentacionCatalogo)) {
            $consectipoCatalogo = inventarioPresentacionConsecutivo();
            _TRANS("INSERT INTO INV001 VALUES('{$consectipoCatalogo}', '{$presentacion}');");
            $presentacionCatalogo = presentacionInventarioId($presentacion);
        }
        $es = esHomologa($objPHPExcel->getActiveSheet()->getCell('J' . $i)->getFormattedValue());
        $bodegaPmin = $objPHPExcel->getActiveSheet()->getCell('K' . $i)->getFormattedValue();
        $bodegaPstock = $objPHPExcel->getActiveSheet()->getCell('L' . $i)->getFormattedValue();
        $bodegaPubicacion = $objPHPExcel->getActiveSheet()->getCell('M' . $i)->getFormattedValue();
        $bodegaAmin = $objPHPExcel->getActiveSheet()->getCell('N' . $i)->getFormattedValue();
        $bodegaAstock = $objPHPExcel->getActiveSheet()->getCell('O' . $i)->getFormattedValue();
        $bodegaAubicacion = $objPHPExcel->getActiveSheet()->getCell('P' . $i)->getFormattedValue();


        $W = _QUERY("SELECT id FROM INV002 WHERE codigo = '{$codigo}';");
        $sigla = 'I';
        $str = "INSERT INTO INV002 VALUES('{$consecutivo}', '{$tipoCatalogo[0]['id']}', '{$codigo}', '{$orden}', '{$factura}', '{$proveedor}', '{$acta}', '{$nombre}', '{$marca}', '{$presentacionCatalogo[0]['id']}', '{$es}', '{$bodegaPmin}', '{$bodegaAmin}', '{$bodegaPstock}', '{$bodegaAstock}', '{$bodegaPubicacion}', '{$bodegaAubicacion}', '1',NULL);";
        echo $str;
        echo '<br>';
        if (!empty($W)) {
            $sigla = 'M';
            $str = "UPDATE INV002 SET familia = '{$tipoCatalogo[0]['id']}', orden = '{$orden}', factura = '{$factura}', proveedor = '{$proveedor}', acta = '{$acta}', nombre = '{$nombre}', marca = '{$marca}', presentacion = '{$presentacionCatalogo[0]['id']}', es = '{$es}', minimo1 = '{$bodegaPmin}', minimo2 = '{$bodegaAmin}', stock1 = '{$bodegaPstock}', stock2 = '{$bodegaAstock}', ubicacion1 = '{$bodegaPubicacion}', ubicacion2 = '{$bodegaAubicacion}' WHERE id = '{$consecutivo}';";
        }
        if (_TRANS($str)) {
            $encabezadoAdicional = _FREE($objPHPExcel->getActiveSheet()->getCell(getColumnLetter(16) . '2')->getFormattedValue());
            $datoAdicional = _FREE($objPHPExcel->getActiveSheet()->getCell(getColumnLetter(16) . $i)->getFormattedValue());
            $actual = 1;
            for ($k = 0; !empty($datoAdicional) && !empty($encabezadoAdicional);) {


                $invCat = inventarioAdicionalcatalogoId($encabezadoAdicional);
                if (empty($invCat)) {
                    $consectipoCatalogo = inventarioAdicionalcatalogoConsecutivo();
                    inventarioAdicionalcatalogoIME($consectipoCatalogo, $encabezadoAdicional, '1');
                    $invCat = inventarioAdicionalcatalogoId($encabezadoAdicional);
                }
                inventarioAdicionalIME($invCat[0]['id'], $consecutivo, $datoAdicional, $sigla);
                $datoAdicional = _FREE($objPHPExcel->getActiveSheet()->getCell(getColumnLetter(( $k++) + 16) . $i)->getFormattedValue());
                $str = "INSERT INTO INVA VALUES({$actual}, '{$consecutivo}', '{$datoAdicional}');";
                $actual++;
                $encabezadoAdicional = _FREE($objPHPExcel->getActiveSheet()->getCell(getColumnLetter($k + 16) . '2')->getFormattedValue());
            }
        }
        $encabezado = _FREE($objPHPExcel->getActiveSheet()->getCell(getColumnLetter(( ++$j)) . '2')->getFormattedValue());
        $tipo = _FREE($objPHPExcel->getActiveSheet()->getCell('A' . ( ++$i))->getFormattedValue());
    }

    echo "Inventario importado correctamente!";
    @unlink($_ruta);
    exit();
} else {
    die('Archivo vacio');
}
exit;
?>