<?php

require_once "GOD.php";

function _FREE($_VAR)
{
    $_VAR = str_replace('á', 'a', $_VAR);
    $_VAR = str_replace('é', 'e', $_VAR);
    $_VAR = str_replace('í', 'i', $_VAR);
    $_VAR = str_replace('ó', 'o', $_VAR);
    $_VAR = str_replace('ú', 'u', $_VAR);
    $_VAR = str_replace('ñ', 'n', $_VAR);
    $_VAR = str_replace("'", '', $_VAR);
    $_VAR = str_replace("(", '', $_VAR);
    $_VAR = str_replace(")", '', $_VAR);
    return $_VAR;
}

include 'Classes/PHPExcel/IOFactory.php';

if ($_FILES['archivo']['size'] > 0) {
    $ruta = $_FILES['archivo']['name'];
    $file = $_FILES['archivo']['tmp_name'];
    $_ruta = str_replace(' ', '', $ruta);
    @move_uploaded_file($file, $_ruta);

    $XLFileType = PHPExcel_IOFactory::identify($_ruta);
    $objReader = PHPExcel_IOFactory::createReader($XLFileType);
    $objPHPExcel = $objReader->load($_ruta);
    try {
        $objWorksheet = $objPHPExcel->setActiveSheetIndexByName('R-75 Registro Gral Inventarios');
    } catch (Exception $e) {
        die('Error nombre de hoja erronea');
    }
    $highestRow = $objPHPExcel->getActiveSheet()->getHighestRow();

    for ($row = 8; $row <= $highestRow; $row++) {
        $descripcion = $objPHPExcel->getActiveSheet()->getCell('B' . $row)->getFormattedValue();
        if (strcmp($descripcion, '') != 0) {
            $descripcion = _FREE($descripcion);
            $codigo = _FREE($objPHPExcel->getActiveSheet()->getCell('A' . $row)->getFormattedValue());
            $ubicacion = _FREE($objPHPExcel->getActiveSheet()->getCell('C' . $row)->getFormattedValue());
            $cantidad_llc = _FREE($objPHPExcel->getActiveSheet()->getCell('D' . $row)->getFormattedValue());
            $cantidad_pavas = _FREE($objPHPExcel->getActiveSheet()->getCell('E' . $row)->getFormattedValue());
            $cantidad_total = _FREE($objPHPExcel->getActiveSheet()->getCell('F' . $row)->getFormattedValue());
            $cantidad_transito = _FREE($objPHPExcel->getActiveSheet()->getCell('G' . $row)->getFormattedValue());
            $nivel = _FREE($objPHPExcel->getActiveSheet()->getCell('H' . $row)->getFormattedValue());
            $costo_unitario_d = _FREE($objPHPExcel->getActiveSheet()->getCell('I' . $row)->getFormattedValue());
            $costo_unitario_c = _FREE($objPHPExcel->getActiveSheet()->getCell('J' . $row)->getFormattedValue());
            $costo_total = _FREE($objPHPExcel->getActiveSheet()->getCell('K' . $row)->getFormattedValue());
            $presentacion = _FREE($objPHPExcel->getActiveSheet()->getCell('L' . $row)->getFormattedValue());
            $parte = _FREE($objPHPExcel->getActiveSheet()->getCell('M' . $row)->getFormattedValue());
            $equipo = _FREE($objPHPExcel->getActiveSheet()->getCell('N' . $row)->getFormattedValue());
            $ingreso = _FREE($objPHPExcel->getActiveSheet()->getCell('O' . $row)->getFormattedValue());
            $analisis = _FREE($objPHPExcel->getActiveSheet()->getCell('P' . $row)->getFormattedValue());
            $consumo_analisis = _FREE($objPHPExcel->getActiveSheet()->getCell('Q' . $row)->getFormattedValue());
            $cantidad_analisis = _FREE($objPHPExcel->getActiveSheet()->getCell('R' . $row)->getFormattedValue());
            $consumo_anual = _FREE($objPHPExcel->getActiveSheet()->getCell('S' . $row)->getFormattedValue());
            $medida = _FREE($objPHPExcel->getActiveSheet()->getCell('T' . $row)->getFormattedValue());
            $seguridadmax = _FREE($objPHPExcel->getActiveSheet()->getCell('U' . $row)->getFormattedValue());
            $reorden = _FREE($objPHPExcel->getActiveSheet()->getCell('V' . $row)->getFormattedValue());
            $vencimiento = _FREE($objPHPExcel->getActiveSheet()->getCell('W' . $row)->getFormattedValue());
            $estado = _FREE($objPHPExcel->getActiveSheet()->getCell('X' . $row)->getFormattedValue());
            $pedir = _FREE($objPHPExcel->getActiveSheet()->getCell('Y' . $row)->getFormattedValue());
            $abc = _FREE($objPHPExcel->getActiveSheet()->getCell('Z' . $row)->getFormattedValue());
            $obs = _FREE($objPHPExcel->getActiveSheet()->getCell('AA' . $row)->getFormattedValue());
            $encargado = _FREE($objPHPExcel->getActiveSheet()->getCell('AB' . $row)->getFormattedValue());

            $ROW = _QUERY("SELECT MAX(id) as id FROM INV062;");
            $id = $ROW[0]['id'];

            if ($id != null || $id == 0) {
                $id++;
                _TRANS("INSERT INTO INV062 VALUES ($id , '$codigo', '$descripcion', '$ubicacion', '$cantidad_llc', '$cantidad_pavas', '$cantidad_total', '$cantidad_transito', '$nivel', '$costo_unitario_d', '$costo_unitario_c', '$costo_total', '$presentacion', '$parte', '$equipo', '$ingreso', '$analisis', '$consumo_analisis', '$cantidad_analisis', '$consumo_anual', '$medida', '$seguridadmax', '$reorden', '$vencimiento', '$estado', '$pedir', '$abc', '$obs', '$encargado')");
            } else {
                _TRANS("INSERT INTO INV062 VALUES ($id , '$codigo', '$descripcion', '$ubicacion', '$cantidad_llc', '$cantidad_pavas', '$cantidad_total', '$cantidad_transito', '$nivel', '$costo_unitario_d', '$costo_unitario_c', '$costo_total', '$presentacion', '$parte', '$equipo', '$ingreso', '$analisis', '$consumo_analisis', '$cantidad_analisis', '$consumo_anual', '$medida', '$seguridadmax', '$reorden', '$vencimiento', '$estado', '$pedir', '$abc', '$obs', '$encargado')");
            }

        }
    }

    echo "Inventario importado correctamente!";
    @unlink($_ruta);
    exit();
} else {
    die('Archivo vacio');
}
exit;
?>