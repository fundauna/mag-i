<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

final class _inventario_historial extends Mensajero{
	private $_ROW = array();
	private $Securitor = '';
	
	function __construct(){
		//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
		$this->ValidaModal();
		
		if(!isset($_POST['desde'])){
			$_POST['desde'] = $_POST['hasta'] = '';
			$_POST['tipo'] = '-1';
		}else $_GET['ID'] = $_POST['articulo'];
		
		if( !isset($_GET['ID']) ) die('Error de parámetros');
		
		$this->Securitor = new Seguridad();
		if(!$this->Securitor->SesionAuth()) $this->Err();
		$this->_ROW = $this->Securitor->SesionGet();
		/*PERMISOS*/
		if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('A5') );
		/*PERMISOS*/
	}

    function Encabezado($_hoja, $_tipo, $_titulo)
    {
        $Qualitor = new Calidad();
        echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
    }
	
	function ObtieneDatos(){
		$Inventor = new Inventario();	
		return $Inventor->InventarioEncabezado($_GET['ID']);
	}
		
	function Movimientos(){
		$Inventor = new Inventario();
		return $Inventor->InventarioMovimientoGet($_GET['ID'], $_POST['tipo'], $_POST['desde'], $_POST['hasta']);
	}
	
	function Tipo($_tipo){
		$Inventor = new Inventario();
		return $Inventor->InventarioMovimientoTipo($_tipo);
	}
}
?>