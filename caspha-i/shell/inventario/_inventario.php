<?php

/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 * ***************************************************************** */
if (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    if (!isset($_POST['id'])) {
        die('-1');
    }

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth()) {
        die('-0');
    }

    $Inventor = new Inventario();
    echo $Inventor->InventarioIME($_POST['id'], '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', $_POST['accion'], '', '');
    exit;
}else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _inventario extends Mensajero {

        private $_ROW = array();
        private $Securitor = '';
        private $inicio = false;

        function __construct() {
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth())
                $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0')
                $this->Portero($this->Securitor->UsuarioPermiso('A4'));
            /* PERMISOS */
            if (!isset($_POST['nombre'])) {
                $_POST['codigo'] = $_POST['nombre'] = $_POST['tipo'] = $_POST['parte'] = $_POST['estado'] = '';
                $_POST['lolo'] = '2';
                $this->inicio = true;
            }
            //SI VENIMOS DESDE TABLON, ELIMINO LA NOTIFICACION
            if (isset($_GET['ver']) && isset($_GET['tablon']))
                $this->Securitor->TablonDel($_GET['tablon']);
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function TiposMuestra() {
            $Inventor = new Inventario();
            return $Inventor->TiposMuestra($this->_ROW['LID'], $this->_ROW['ROL']);
        }

        function InventarioMuestra() {
            $Inventor = new Inventario();
            if ($this->inicio)
                return $Inventor->InventarioAlertaGet($this->_ROW['LID']);
            else
                return $Inventor->InventarioResumenGet($_POST['codigo'], $_POST['nombre'], $_POST['tipo'], $this->_ROW['LID'], '', $_POST['parte'], isset($_POST['estado']) ? $_POST['estado'] : '1');
        }

        function obtienePatrimonio($_codigo) {
            $Inventor = new Inventario();
            return $Inventor->ObtienePatrimonio($_codigo);
        }

    }

}
?>