<?php

/* * *****************************************************************
  DECLARACION CLASE GESTORA
 * ***************************************************************** */

function __autoload($_BaseClass) {
    require_once "../../melcha-i/{$_BaseClass}.php";
}

final class _libro extends Mensajero {

    private $_ROW = array();
    private $Securitor = '';
    private $inicio = false;

    function __construct() {
        $this->Securitor = new Seguridad();
        if (!$this->Securitor->SesionAuth())
            $this->Err();
        $this->_ROW = $this->Securitor->SesionGet();
        /* PERMISOS */
        if ($this->_ROW['ROL'] != '0')
            $this->Portero($this->Securitor->UsuarioPermiso('25'));
        /* PERMISOS */

        if (!isset($_POST['tipo'])) {
            $_POST['tipo'] = $_POST['dato'] = $_POST['desde'] = $_POST['hasta'] = $_POST['estado'] = '';
            $this->inicio = true;
        }
    }

    function Encabezado($_hoja, $_tipo, $_titulo)
    {
        $Qualitor = new Calidad();
        echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
    }

    function Muestras() {
        if ($this->inicio) {
            return array();
        }
        $Servitor = new Servicios();
        $Q = $Servitor->MuestrasGet($this->_ROW['LID'], $_POST['tipo'], $_POST['dato'], $_POST['desde'], $_POST['hasta'], $_POST['estado']);
        if(!$Q){
            return array();
        }else{
            return $Q;
        }
    }

    function Estado($_var) {
        $Servitor = new Servicios();
        return $Servitor->MuestraEstado($_var);
    }

}

?>