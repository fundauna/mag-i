<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

final class _historial_ensayos extends Mensajero{
	private $_ROW = array();
	private $Securitor = '';
	private $inicio = false;
	
	function __construct(){	
		$this->Securitor = new Seguridad();
		if(!$this->Securitor->SesionAuth()) $this->Err();
		$this->_ROW = $this->Securitor->SesionGet();
		/*PERMISOS*/
		if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('28') );
		/*PERMISOS*/
		
		if(!isset($_POST['dato'])){
			$_POST['dato'] = '';
			$this->inicio = true;
		}
	}

    function Encabezado($_hoja, $_tipo, $_titulo)
    {
        $Qualitor = new Calidad();
        echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
    }
	
	function Historial(){
		if($this->inicio) return array();
		$Servitor = new Servicios();
		return $Servitor->EnsayosHistorialGeneralGet($_POST['dato']);
	}
}
?>