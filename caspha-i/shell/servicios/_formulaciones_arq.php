<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Listator = new Listador();
	if($_GET['tipo']=='3')
		$Listator->ElementosLista('servicios', 3, $_GET['tipo'], $_GET['list'], basename(__FILE__));
	else
		$Listator->ImpurezasLista('servicios', 3, 0, $_GET['list'], basename(__FILE__));
		
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
}elseif( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}

	if( !isset($_POST['analisis']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	
	$Robot = new Varios();
	
	if( isset($_POST['PermisosBuscar']) ){
		$str = '<select id="asignados" style="width:400px;" size="10">';
		$ROW = $Robot->FormulacionesIngredienteMuestra($_POST['analisis']);
		for($x=0;$x<count($ROW);$x++){
			//$ROW[$x]['nombre'] = utf8_encode($ROW[$x]['nombre']);
			$str .= "<option value='{$ROW[$x]['id']}'>{$ROW[$x]['nombre']}</option>";	
		}
		$str .= '</select>';
		die($str);
	}elseif( isset($_POST['PermisosAgrega']) ){
		echo $Robot->FormulacionesIngredienteIME($_POST['analisis'], $_POST['permiso'], 'I');
		exit;
	}elseif( isset($_POST['PermisosElimina']) ){
		echo $Robot->FormulacionesIngredienteIME($_POST['analisis'], $_POST['permiso'], 'D');
		exit;
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _formulaciones_arq extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('27') );
			/*PERMISOS*/
			
			$_POST['LID'] = $this->_ROW['LID'];
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function FormulacionesMuestra(){
			$Robot = new Varios();
			return $Robot->FormulacionesGet('P');
		}
	}
}
?>