<?php

$LID = 3; //LCC

/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
 * ***************************************************************** */
if (isset($_GET['list'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Listator = new Listador();
    $Listator->AnalisisForLista('servicios', $LID, $_GET['tipo'], $_GET['list'], basename(__FILE__));
    exit;
} elseif (isset($_GET['list2'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Listator = new Listador();
    $Listator->IALista2('servicios', $LID, $_GET['formulacion'], $_GET['list2'], basename(__FILE__));
    exit;
} elseif (isset($_GET['list3'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Listator = new Listador();
    $Listator->IMPLista('servicios', $LID, $_GET['formulacion'], $_GET['list3'], basename(__FILE__));
    exit;
} elseif (isset($_GET['list4'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Listator = new Listador();
    $Listator->ClientesLista('servicios', $LID, basename(__FILE__));
    exit;
} elseif (isset($_POST['buscar'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Robot = new Cotizaciones();
    echo $Robot->SolicitudExiste($_POST['solicitud'], $LID);
    exit;
} elseif (isset($_POST['modificar'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth())
        die('-0');
    $_ROW = $Securitor->SesionGet();

    $Robot = new Servicios();
    echo $Robot->Solicitud03Modifica($_POST['solicitud'], $_POST['obs'], $_POST['estado'], $_ROW['UID']);
    exit;
    /*     * *****************************************************************
      PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
     * ***************************************************************** */
}elseif (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    if (!isset($_POST['accion']))
        die('-1');

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth())
        die('-0');
    $_ROW = $Securitor->SesionGet();
    //
    $Robot = new Servicios();

    if ($_POST['accion'] == 'R') {
        $_POST['accion'] = 'I';
    }
    if ($_POST['accion'] == 'I') {
        $cs = 'LCC-' . date('Y') . '-' . $Robot->SolicitudesConsecutivo($LID);
    } else {
        $cs = $_POST['cs'];
    }

    //INGRESA ENCABEZADO
    $ok = $Robot->Solicitud03IME($cs, $_POST['accion'], $_POST['solicitud'], $_POST['tipo'], $_POST['proposito'], $_POST['dependencia'], $_POST['obs'], $_POST['cliente'], $_POST['direccion'], $_POST['entregadas'], $_POST['producto'], $_POST['tipo_form'], $_POST['metodo'], $_POST['registro'], $_POST['dosis'], $_POST['mezcla'], $_POST['densidad'], $_ROW['UID']
    );

    if ($ok) {
        $Robot->Solicitud03ElementosLimpia($cs);
        //INGRESA ANALISIS
        $Robot->Solicitud03ElementosSet($cs, $_POST['analisis'], $_POST['declarada'], '', $_POST['uc'], $_POST['lugar'], '0');
        //INGRESA ELEMENTOS
        $Robot->Solicitud03ElementosSet($cs, $_POST['elementos'], $_POST['rango'], '', $_POST['unidad'], $_POST['fuente'], '1', $_POST['tipo_form']);
        //INGRESA IMPUREZAS
        $Robot->Solicitud03AnalisisSet($cs, $_POST['impurezas'], 0);
        //INGRESA MUESTRAS
        $Robot->Solicitud03MuestrasSet($cs, $_POST['cod_ext'], $_POST['recipiente'], $_POST['sellado'], $_POST['lote'], $_POST['rechazada'], $_POST['obse']);

        echo 1;
        exit;
    } else {
        echo 0;
        exit;
    }

    header("location: ../../../baltha-i/seguridad/msj.php?msj={$msj}");
    exit;
} else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _solicitud03P_detalle extends Mensajero {

        private $_ROW = array();
        private $Securitor = '';
        private $inicio = false;

        function __construct() {
            //SOLO SE PUEDE ABRIR COMO POP-UP MODAL
            $this->ValidaModal();

            if (!isset($_GET['acc']))
                die('Error de parámetros');

            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth())
                $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            if (!isset($_GET['ID']))
                $_GET['ID'] = '';
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0')
                $this->Portero($this->Securitor->UsuarioPermiso('21'));
            /* PERMISOS */
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function ObtieneDatos() {
            $Robot = new Servicios();
            if (!isset($_GET['ID']) or $_GET['ID'] == '') {
                $_POST['tipo'] = '';
                return $Robot->Solicitud03EncabezadoVacio();
            } elseif ($_GET['acc'] == 'R') { //IMPORTACION DE UNA SOLICITUD
                $ROW = $Robot->SolicitudImportaEncabezado($_GET['ID']);
                $_POST['tipo'] = $ROW[0]['tipo'];
                return $ROW;
            } else
                return $Robot->Solicitud03EncabezadoGet($_GET['ID']);
        }

        function SolicitudAnalisis() {
            $Robot = new Servicios();
            if (!isset($_GET['ID']) or $_GET['ID'] == '')
                return $Robot->Solicitud03ElementosVacio();
            elseif ($_GET['acc'] == 'R') { //IMPORTACION DE UNA SOLICITUD
                if ($_POST['tipo'] == '1')
                    return array(); //si es fertilizantes no cargo analisis sino elementos
                return $Robot->SolicitudImportaDetalle($_GET['ID'], 1);
            } else
                return $Robot->Solicitud03ElementosGet($_GET['ID'], 0);
        }

        function SolicitudElementos() {
            $Robot = new Servicios();
            if (!isset($_GET['ID']) or $_GET['ID'] == '')
                return $Robot->Solicitud03ElementosVacio();
            elseif ($_GET['acc'] == 'R') { //IMPORTACION DE UNA SOLICITUD
                if ($_POST['tipo'] == '1')
                    return $Robot->SolicitudImportaElementos($_GET['ID']);
                else
                    return $Robot->SolicitudImportaDetalle($_GET['ID'], 0);
            } else
                return $Robot->Solicitud03ElementosGet($_GET['ID'], 1);
        }

        function SolicitudImpurezas() {
            $Robot = new Servicios();
            if (!isset($_GET['ID']) or $_GET['ID'] == '')
                return $Robot->Solicitud03AnalisisVacio();
            elseif ($_GET['acc'] == 'R') //IMPORTACION DE UNA SOLICITUD
                return $Robot->SolicitudImportaDetalle($_GET['ID'], 2);
            else
                return $Robot->Solicitud03AnalisisGet($_GET['ID'], 0);
        }

        function SolicitudMuestras() {
            $Robot = new Servicios();
            if (!isset($_GET['ID']) or $_GET['ID'] == '' or $_GET['acc'] == 'R')
                return $Robot->Solicitud03MuestrasVacio($_POST['muestras']);
            else
                return $Robot->Solicitud03MuestrasGet($_GET['ID']);
        }

        function Aprobar() {
            if ($this->_ROW['ROL'] == '0')
                return true;
            elseif ($this->Securitor->UsuarioPermiso('22') == 'E')
                return true;
            else
                return false;
        }

        function SuperAnular() {
            if ($this->_ROW['ROL'] == '0')
                return true;
            elseif ($this->Securitor->UsuarioPermiso('24') == 'E')
                return true;
            else
                return false;
        }

        function Estado($_var) {
            $Robot = new Servicios();
            return $Robot->ServiciosEstado($_var);
        }

        function Formulaciones() {
            $Robot = new Varios();
            return $Robot->FormulacionesGet('P');
        }

    }

}
?>