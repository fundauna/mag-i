<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

final class _solicitud01_imprimir extends Mensajero{
	private $_ROW = array();
	private $Securitor = '';
	
	function __construct(){
		//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
		$this->ValidaModal();
		
		if(!isset($_GET['ID'])) die('Error de parametros');
		if(!isset($_GET['ref'])) $_GET['ref'] = ''; //SE USA CUANDO SE ESTAN ASIGNANDO MUESTRAS PARA MARCAR LA MUESTRA ESPECIFICA EN ROJO
		else $_GET['ref'] = str_replace(' ', '', $_GET['ref']);
		
		$this->Securitor = new Seguridad();
		if(!$this->Securitor->SesionAuth()) $this->Err();
		$this->_ROW = $this->Securitor->SesionGet();
		/*PERMISOS*/
		if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('21') );
		/*PERMISOS*/
	}

    function Encabezado($_hoja, $_tipo, $_titulo)
    {
        $Qualitor = new Calidad();
        echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
    }
	
	function Estado($_var){
		$Cotizator = new Servicios();
		return $Cotizator->ServiciosEstado($_var);
	}
	
	function ObtieneDatos(){
		$Servitor = new Servicios();
		return $Servitor->Solicitud01EncabezadoGet($_GET['ID']);
	}
	
	/*function ObtienePersonas(){
		$Servitor = new Servicios();
		return $Servitor->Solicitud01PersonasGet($_GET['ID']);
	}*/
	
	function SolicitudLineas(){
		$Servitor = new Servicios();
		return $Servitor->Solicitud01DetalleGet($_GET['ID']);
	}
	
	function Accion($_var){
		$Robot = new Servicios();
		return $Robot->ServiciosAccion($_var);
	}
	
	function Historial(){
		$Robot = new Servicios();
		return $Robot->Solicitud01HistorialGet($_GET['ID']);
	}
        function obtieneHora(){
		$Robot = new Servicios();
		return $Robot->obtieneHora($_GET['ID']);
	}
        
        
}
?>