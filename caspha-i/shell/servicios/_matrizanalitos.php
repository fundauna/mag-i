<?php

/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 * ***************************************************************** */
if (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass)
    {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth()) {
        die('-0');
    }
    if (!isset($_POST['matriz'])) {
        die('-1');
    }

    $Inventario = new Inventario();

    if (isset($_POST['AnalitosBuscar'])) {
        $str = '<select id="asignados" style="width:400px;" size="12">';
        $ROW = $Inventario->MAAsignados2($_POST['matriz']);
        for ($x = 0; $x < count($ROW); $x++) {
            $ROW[$x]['id'] = $ROW[$x]['ida'];
            $ROW[$x]['nombre'] = $ROW[$x]['analito'];
            $ROW[$x]['cuant'] = $ROW[$x]['cuantificador'];
            $ROW[$x]['tip'] = $ROW[$x]['tipoCroma'];

            //asigna tipo
            if ($ROW[$x]['tip'] == 1) {
                $ROW[$x]['tip'] = 'CL EM/EM';
            } else if ($ROW[$x]['tip'] == 2) {
                $ROW[$x]['tip'] = 'CG EM/EM';
            } else {
                $ROW[$x]['tip'] = 'N/A';
            }

            $str .= "<option value='{$ROW[$x]['id']}'>{$ROW[$x]['nombre']} ---> {$ROW[$x]['tip']} ----> {$ROW[$x]['cuant']} </option>";
        }
        $str .= '</select>';
        die($str);

    } elseif (isset($_POST['AnalitosAgrega'])) {
        echo $Inventario->MAIME($_POST['idm'], $_POST['matriz'], $_POST['ida'], $_POST['analito'], $_POST['cuantificadores'], 'I');
        exit;
    } elseif (isset($_POST['AnalitosElimina'])) {
        echo $Inventario->MAIMEDEL($_POST['ida'], $_POST['matriz']);
        exit;
    }
} else {
    /*******************************************************************
     * DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass)
    {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _matrizanalitos extends Mensajero
    {

        private $_ROW = array();
        private $Securitor = '';
        private $Inventor = '';

        function __construct()
        {
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth())
                $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0')
                $this->Portero($this->Securitor->UsuarioPermiso('A1'));
            /* PERMISOS */

            $this->Inventor = new Inventario();
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function Registros($_tipo = '8')
        {
            $Variator = new Varios();
            return $Variator->SubAnalisisGet1($_tipo, $this->_ROW['LID']);
        }

        function Grupos()
        {
            $Variator = new Varios();
            return $Variator->GrupoGet();
        }

    }

}
?>