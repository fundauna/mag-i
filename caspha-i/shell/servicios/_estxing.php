<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	$Listator->MacroAnalisisLista('servicios', $_ROW['LID'], $_GET['tipo'], '', basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
}elseif( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}

	if( !isset($_POST['formulacion']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	
	$Robot = new Varios();
	
	if( isset($_POST['FormulacionBuscar']) ){
		$str = '<select id="asignados" style="width:400px;" size="10">';
		$ROW = $Robot->EstIngActMuestra($_POST['formulacion']);
		for($x=0;$x<count($ROW);$x++){
			//$ROW[$x]['nombre'] = utf8_encode($ROW[$x]['nombre']);
			$str .= "<option value='{$ROW[$x]['estandar']}'>{$ROW[$x]['codigo']} | {$ROW[$x]['nombre']}</option>";	
		}
		$str .= '</select>';
		die($str);
	}elseif( isset($_POST['PermisosAgrega']) ){
		echo $Robot->EstIngActIME($_POST['formulacion'], $_POST['analisis'], 'I');
		exit;
	}elseif( isset($_POST['PermisosElimina']) ){
		echo $Robot->EstIngActIME($_POST['formulacion'], $_POST['analisis'], 'D');
		exit;
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _estxing extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('27') );
			/*PERMISOS*/
			
			$_POST['LID'] = $this->_ROW['LID'];
		}
		
		function IngredientesMuestra(){
			$Robot = new Varios();
			return $Robot->SubAnalisisGet('3', '3');
		}
		
		function EstandarMuestra(){
			$Robot = new Inventario();
			return $Robot->InventarioResumenGet('', '', '4', '3');
		}
	}
}
?>