<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

final class _analisis_pendientes extends Mensajero{
	private $_ROW = array();
	private $Securitor = '';
	
	function __construct(){
		$this->Securitor = new Seguridad();
		if(!$this->Securitor->SesionAuth()) $this->Err();
		$this->_ROW = $this->Securitor->SesionGet();
		/*PERMISOS*/
		if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('25') );
		/*PERMISOS*/
		
		//SI VENIMOS DESDE TABLON, ELIMINO LA NOTIFICACION
		if( isset($_GET['ver']) && isset($_GET['tablon']) )$this->Securitor->TablonDel($_GET['tablon']);
	}

    function Encabezado($_hoja, $_tipo, $_titulo)
    {
        $Qualitor = new Calidad();
        echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
    }
	
	function Analisis(){
		$Servitor = new Servicios();
		return $Servitor->AnalisisPendientes($this->_ROW['LID'], $this->_ROW['ROL'], $this->_ROW['UID']);
	}
        
        function getROL(){
		return $this->_ROW['ROL'];
	}
}
?>