<?php

/* * *****************************************************************
  DECLARACION CLASE GESTORA
 * ***************************************************************** */

function __autoload($_BaseClass) {
    require_once "../../melcha-i/{$_BaseClass}.php";
}

final class _solicitudes extends Mensajero {

    private $_ROW = array();
    private $Securitor = '';
    private $inicio = false;

    function __construct() {
        $this->Securitor = new Seguridad();
        if (!$this->Securitor->SesionAuth())
            $this->Err();
        $this->_ROW = $this->Securitor->SesionGet();

        /* PERMISOS */
        if ($this->_ROW['ROL'] != '0')
            $this->Portero($this->Securitor->UsuarioPermiso('21'));
        /* PERMISOS */

        if (!isset($_POST['estado'])) {
            $_POST['desde'] = $_POST['hasta'] = $_POST['solicitud'] = '';
            $_POST['estado'] = 'r';
            $_POST['opcion'] = '1';
            $this->inicio = true;
        }

        if (!isset($_POST['tipo']))
            $_POST['tipo'] = '';

        //SI VENIMOS DESDE TABLON, ELIMINO LA NOTIFICACION
        if (isset($_GET['ver']) && isset($_GET['tablon']))
            $this->Securitor->TablonDel($_GET['tablon']);

        $_POST['LAB'] = $this->_ROW['LID'];
        if ($_POST['LAB'] == '2')
            die('Secci�n bloqueada para este laboratorio');
    }

    function Encabezado($_hoja, $_tipo, $_titulo)
    {
        $Qualitor = new Calidad();
        echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
    }

    function SolicitudesMuestra() {
        $Servitor = new Servicios();
        if ($this->inicio)
            return $Servitor->SolicitudesPendientes($this->_ROW['LID']);
        else
            return $Servitor->SolicitudesGet($this->_ROW['LID'], $_POST['opcion'], $_POST['solicitud'], $_POST['desde'], $_POST['hasta'], $_POST['tipo'], $_POST['estado']);
    }

    function Estado($_var) {
        $Servitor = new Servicios();
        return $Servitor->ServiciosEstado($_var);
    }

    function obtieneLAB() {
        return $this->_ROW['LID'];
    }

}

?>