<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['id']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();

   $type= $_POST['tipo'];
    if($type != 8) {
        $Variator = new Varios();
        echo $Variator->SubAnalisisIME($_POST['id'], $_POST['nombre'], $_POST['tipo'], $_ROW['LID'], $_POST['accion']);
        exit;
    } else {
        $Variator = new Varios();
        echo $Variator->SubAnalisisIME1($_POST['id'], $_POST['nombre'], $_POST['tipo'], $_ROW['LID'], $_POST['accion'], $_POST['tipoCroma']);
        exit;
    }
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _subanalisis extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('23') );
			/*PERMISOS*/
			
			if( !isset($_POST['tipo']) ) $_POST['tipo'] = '';
			
			$_POST['LID'] = $this->_ROW['LID'];
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function Registros(){
			$Variator = new Varios();
			return $Variator->SubAnalisisGet($_POST['tipo'], $this->_ROW['LID']);
		}

        function Registros1(){
            $Variator = new Varios();
            return $Variator->SubAnalisisGet1($_POST['tipo'], $this->_ROW['LID']);
        }
    }
}
?>