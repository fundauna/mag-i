<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

final class _libro_historial extends Mensajero{
	private $_ROW = array();
	private $Securitor = '';
	
	function __construct(){
		//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
		$this->ValidaModal();
		
		if(!isset($_GET['ID'])) die('Error de parametros');
		
		$this->Securitor = new Seguridad();
		if(!$this->Securitor->SesionAuth()) $this->Err();
		$this->_ROW = $this->Securitor->SesionGet();
		/*PERMISOS*/
		if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('25') );
		/*PERMISOS*/
	}

    function Encabezado($_hoja, $_tipo, $_titulo)
    {
        $Qualitor = new Calidad();
        echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
    }

	function Accion($_var){
		$Robot = new Servicios();
		return $Robot->MuestraAccion($_var);
	}
	
	function Historial(){
		$Robot = new Servicios();
		return $Robot->MuestrasHistorialGet($_GET['ID']);
	}
}
?>