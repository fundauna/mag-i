<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

final class _muestras_cliente_buscar extends Mensajero{
	private $_ROW = array();
	private $Securitor = '';
	
	function __construct(){
		$this->Securitor = new Seguridad();
		if(!$this->Securitor->SesionAuth()) $this->Err();
		$this->_ROW = $this->Securitor->SesionGet();
	}

    function Encabezado($_hoja, $_tipo, $_titulo)
    {
        $Qualitor = new Calidad();
        echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
    }
	
	function Muestras(){
		$Servitor = new Servicios();
		return $Servitor->MuestrasClienteGet($this->_ROW['UCED']);
	}
	
	function Estado($_var){
		$Servitor = new Servicios();
		return $Servitor->MuestraEstado($_var);
	}
}
?>