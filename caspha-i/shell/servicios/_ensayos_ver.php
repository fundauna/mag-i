<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['xanalizar']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Servitor = new Servicios();
	if($_POST['paso']==1)
		echo $Servitor->ApruebaXanalizar($_POST['xanalizar'], $_ROW['UID']);	
	if($_POST['paso']==2)
		echo $Servitor->ValidaInvalida($_POST['ensayo'], $_POST['estado'], $_ROW['UID']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _ensayos_ver extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			if(!isset($_GET['ID'])) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('28') );
			/*PERMISOS*/
			$_POST['UID'] = $this->_ROW['UID'];
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Servitor = new Servicios();
			return $Servitor->EnsayosEncabezadoGet($_GET['ID']);
		}
		
		function MetodosDisponibles(){
			$Robot = new Varios();
			if($this->_ROW['ROL'] == '0') { //ADMIN MUESTRA TODOS
                return $Robot->AnalisisMetodosMuestra($_POST['analisis']);
            } else {
                return $Robot->UsuarioMetodoXAnalisis($this->_ROW['UID'], $_POST['analisis']);
            }
		}
		
		function Ensayos(){
			$Servitor = new Servicios();
			return $Servitor->EnsayosHistorialMuestraGet($_GET['ID']);
		}
	}
//
}
?>