<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

final class _solicitud03_imprimir extends Mensajero{
	private $_ROW = array();
	private $Securitor = '';
	
	function __construct(){
		//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
		$this->ValidaModal();
		
		if(!isset($_GET['ID'])) die('Error de parametros');
		if(!isset($_GET['ref'])) $_GET['ref'] = ''; //SE USA CUANDO SE ESTAN ASIGNANDO MUESTRAS PARA MARCAR LA MUESTRA ESPECIFICA EN ROJO
		else $_GET['ref'] = str_replace(' ', '', $_GET['ref']);
		
		$this->Securitor = new Seguridad();
		if(!$this->Securitor->SesionAuth()) $this->Err();
		$this->_ROW = $this->Securitor->SesionGet();
		/*PERMISOS*/
		//if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('25') );
		/*PERMISOS*/
	}

    function Encabezado($_hoja, $_tipo, $_titulo)
    {
        $Qualitor = new Calidad();
        echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
    }
	
	function ObtieneDatos(){
		$Robot = new Servicios();
		return $Robot->Solicitud03EncabezadoGet($_GET['ID']);
	}
	
	function SolicitudElementos(){
		$Robot = new Servicios();
		return $Robot->Solicitud03ElementosGet($_GET['ID'], 1);
	}
	
	function SolicitudImpurezas(){
		$Robot = new Servicios();
		return $Robot->Solicitud03AnalisisGet($_GET['ID'], 0);
	}
	
	function SolicitudMuestras(){
		$Robot = new Servicios();
		return $Robot->Solicitud03MuestrasGet($_GET['ID']);
	}
	
	function Estado($_var){
		$Robot = new Servicios();
		return $Robot->ServiciosEstado($_var);
	}
	
	function Accion($_var){
		$Robot = new Servicios();
		return $Robot->ServiciosAccion($_var);
	}
	
	function Historial(){
		$Robot = new Servicios();
		return $Robot->Solicitud03HistorialGet($_GET['ID']);
	}
	
	function Formulaciones(){
		$Robot = new Varios();
		return $Robot->FormulacionesGet('F');
	}
}
?>