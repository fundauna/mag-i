<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	$Listator->AnalistasDisponibles('servicios', basename(__FILE__), $_GET['list'], $ROW['LID'], $_GET['muestra'], $_GET['campo'], $_GET['campo2']);
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
}elseif( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['muestra']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Servitor = new Servicios();
	if($_POST['paso']=='1')
		echo $Servitor->MuestrasAsignarSet($_POST['muestra'], $_POST['analista'], $_POST['analista2'], $_ROW['UID']);
	else
		echo $Servitor->MuestrasReasignar($_POST['muestra'], $_POST['analista'], $_POST['analista2'], $_ROW['UID']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _muestras_asignar_LRE extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		private $Servicios = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('26') );
			/*PERMISOS*/
			
			//SI VENIMOS DESDE TABLON, ELIMINO LA NOTIFICACION
			if( isset($_GET['ver']) && isset($_GET['tablon']) )$this->Securitor->TablonDel($_GET['tablon']);

			$_POST['LID'] = $this->_ROW['LID'];
			
			$this->Servicios = new Servicios();
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function Muestras($_reasignar){
			return $this->Servicios->MuestrasAsignarGet($this->_ROW['LID'], $_reasignar);
		}
	}
//
}
?>