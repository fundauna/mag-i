<?php

/* * *****************************************************************
  DECLARACION CLASE GESTORA
 * ***************************************************************** */

function __autoload($_BaseClass) {
    require_once "../../melcha-i/{$_BaseClass}.php";
}

final class _LRE_hoja6 extends Mensajero {

    private $_ROW = array();
    private $Securitor = '';

    function __construct() {
        //SOLO SE PUEDE ABRIR COMO POP-UP MODAL
        $this->ValidaModal();

        $this->Securitor = new Seguridad();
        if (!$this->Securitor->SesionAuth())
            $this->Err();
        $this->_ROW = $this->Securitor->SesionGet();
        /* PERMISOS */
        if ($this->_ROW['ROL'] != '0')
            $this->Portero($this->Securitor->UsuarioPermiso('21'));
        /* PERMISOS */
    }

    function ObtieneDatos() {
        $Servitor = new Servicios();
        return $Servitor->Solicitud01EncabezadoGet($_GET['ID']);
    }

    function SolicitudLineas() {
        $Servitor = new Servicios();
        return $Servitor->Solicitud01DetalleGet($_GET['ID']);
    }

    function formatoLote($_lote) {
        $datos = explode('-', $_lote);
        $num = substr($datos[0], 2);
        $consecutivo = $datos[1];
        while (strlen($consecutivo) < 4) {
            $consecutivo = '0' . $consecutivo;
        }
        return $num . '-' . $consecutivo;
    }

}

?>