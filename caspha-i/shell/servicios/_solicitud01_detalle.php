<?php

/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UNA LISTA
 * ***************************************************************** */
if (isset($_GET['list'])) {

    function __autoload($_BaseClass)
    {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    $ROW = $Securitor->SesionGet();

    $Listator = new Listador();
    if ($_GET['list'] == '1') {
        $Listator->ClientesLista('servicios', $ROW['LID'], basename(__FILE__));
    } elseif ($_GET['list'] == '2') {
        $Listator->MatrizLista('servicios', $ROW['LID'], $_GET['linea'], basename(__FILE__));
    } elseif ($_GET['list'] == '3') {
        $Listator->ElementosLista('servicios', $ROW['LID'], 9, $_GET['linea'], basename(__FILE__));
    } elseif ($_GET['list'] == '4') {
        $Listator->SolicitanteLista('servicios', $ROW['LID'], $_GET['cliente'], basename(__FILE__));
    }


    exit;
} elseif (isset($_POST['modificar'])) {

    function __autoload($_BaseClass)
    {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth()) {
        die('-0');
    }
    $_ROW = $Securitor->SesionGet();

    $Robot = new Servicios();
    echo $Robot->Solicitud01Modifica($_POST['solicitud'], $_POST['obs'], $_POST['estado'], $_POST['codigo'], $_ROW['UID']);



    exit;
    /*     * *****************************************************************
      PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
     * ***************************************************************** */
} elseif (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass)
    {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    //if( !isset($_POST['accion']) ) die('-1');

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth()) {
        die('-0');
    }
    $_ROW = $Securitor->SesionGet();

    if ($_POST['_AJAX'] == 1) {
        $Servitor = new Servicios();
        $Servitor1 = new Servicios();
        if ($_POST['accion'] == 'I') {
            $hj = $Servitor->SolicitudesConsecutivo(1);
            $ceros = '0';

            if ($hj < 10000) {
                if ($hj >= 1000) {
                    $ceros = '0';
                } elseif ($hj >= 100) {
                    $ceros = '00';
                } elseif ($hj >= 10) {
                    $ceros = '000';
                } else
                    $ceros = '0000';
            }

            $_POST['cs'] = date('y') . '-' . $ceros . $Servitor->SolicitudesConsecutivo(1);
        }
        $_POST['cs'] = str_replace(' ', '', $_POST['cs']);
        if ($Servitor->Solicitud01IME($_POST['accion'], $_POST['cliente'], $_POST['nomsolicitante'], $_POST['cs'], $_POST['entregado'], $_ROW['UID'], $_POST['proposito'], $_POST['muestreo'], $_POST['entregacliente'], $_POST['entregaanalista'], $_POST['obs'], $_POST['codigo'], $_POST['matriz'], $_POST['tipo'], $_POST['presentacion'], $_POST['empaque'], $_POST['forma'], $_POST['masa'], $_POST['observaciones'], $_POST['obsfrf'], $_POST['analisis'], $_POST['parte'], $_POST['metodo'], $_POST['flab'], $_POST['hlab'], $_POST['fmuestra'], $_POST['hmuestra'], $_POST['nia'])) {
            if (isset($_FILES['file'])) {
                if ($_FILES['file']['size'] > 0) {
                    $nombre = $_FILES['file']['name'];
                    $file = $_FILES['file']['tmp_name'];
                    $ruta = "../../docs/servicios/{$_POST['cs']}.zip";
                    Bibliotecario::ZipCrear($nombre, $file, $ruta, 1) or die("Error: No es posible crear el archivo {$nombre}");
                }
            }
            //SUBIR ARCHIVO			
            echo 1;
        } else {
            echo 0;
        }

    } elseif ($_POST['_AJAX'] == 2) {
        $sc = new Sc();
        $solicitante = $sc->SolicitanteGet($_POST['cliente']);
        echo json_encode($solicitante);
    } elseif ($_POST['_AJAX'] == 3) {
        $_POST['cs'] = str_replace(' ', '', $_POST['cs']);
        unlink("../../docs/servicios/{$_POST['cs']}.zip");
        echo 1;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    } elseif ($_POST['_AJAX'] == 101) {
        $Servitor101 = new Servicios();
        echo $Servitor101->asignaInforme($_POST['cs'], $_POST['codigo']);
        echo 1;
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    } else




    exit;
} else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass)
    {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _solicitud01_detalle extends Mensajero
    {

        private $_ROW = array();
        private $Securitor = '';

        function __construct()
        {
            //SOLO SE PUEDE ABRIR COMO POP-UP MODAL
            $this->ValidaModal();

            if (!isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M')) {
                die('Error de parámetros');
            }

            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth()) {
                $this->Err();
            }
            $this->_ROW = $this->Securitor->SesionGet();
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0') {
                $this->Portero($this->Securitor->UsuarioPermiso('21'));
            }
            /* PERMISOS */
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function ObtieneDatos()
        {
            $Servitor = new Servicios();
            if (!isset($_GET['ID']) or $_GET['ID'] == '') {
                return $Servitor->Solicitud01EncabezadoVacio();
            } else {
                return $Servitor->Solicitud01EncabezadoGet($_GET['ID']);
            }
        }

        function SolicitudLineas()
        {
            $Servitor = new Servicios();
            if (!isset($_GET['ID']) or $_GET['ID'] == '') {
                return $Servitor->Solicitud01DetalleVacio();
            } else {
                return $Servitor->Solicitud01DetalleGet($_GET['ID']);
            }
        }


        function obtenerSiguiente()
        {
            $Servitor = new Servicios();
            return $Servitor->informesConsecutivo();
        }


        function existeMuestraSuelo($_datos)
        {
            foreach ($_datos as $dato) {
                if (strpos(strtoupper($dato['nombre']), 'SUELO') === 0) {
                    return true;
                }
            }
            return false;
        }

        function formatoLote($_lote)
        {
            if ($_lote != '') {
                $datos = explode('-', $_lote);
                $num = substr($datos[0], 2);
                $consecutivo = $datos[1];
                while (strlen($consecutivo) < 4) {
                    $consecutivo = '0' . $consecutivo;
                }
                return $num . '-' . $consecutivo;
            } else {
                return '';
            }
        }

    }

//
}
?>