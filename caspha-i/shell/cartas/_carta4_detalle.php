<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	$Listator->TermosLista('cartas', 0, $ROW['LID'], basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
}elseif( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['paso']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Cartor = new Cartas();
	
	if($_POST['paso'] == '1'){	
		echo $Cartor->Carta4IME($_POST['cs'], 
			$_POST['accion'], 
			$_POST['cod_ter'], 
			$_POST['obs'], 
			//
			$_POST['cuarto'], 
			$_POST['limites'], 
			$_POST['humedad'], 
			$_POST['indicacion1'], $_POST['error1'], 
			$_POST['indicacion2'], $_POST['error2'], 
			$_POST['indicacion3'], $_POST['error3'], 
			$_POST['hr1'], $_POST['err_hr1'], 
			$_POST['hr2'], $_POST['err_hr2'],
			$_POST['hr3'], $_POST['err_hr3'],
			//
			$_POST['fecha'], 
			$_POST['hora'], 
			$_POST['temp1'], 
			$_POST['temp2'], 
			$_POST['hume1'],
			$_POST['hume2'],  
			$_ROW['LID'],
			$_ROW['UID']);	
		exit;
	}elseif($_POST['paso'] == '2'){
		echo $Cartor->Carta4DetalleColumnaGet($_POST['cs'], $_POST['tipo'], $_POST['desde'], $_POST['hasta']);
		exit;
	}elseif($_POST['paso'] == '3'){
		echo $Cartor->Carta4DetalleElimina($_POST['cs'], $_POST['fecha'], $_POST['hora']);
		exit;
	}elseif($_POST['paso'] == '9'){
		echo $Cartor->CartaActualiza($_POST['cs'], $_ROW['UID'], $_POST['accion']);
		exit;
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _carta4_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('67') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Cartor = new Cartas();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Cartor->Carta4EncabezadoVacio();
			else	
				return $Cartor->Carta4EncabezadoGet($_GET['ID']);
		}
		
		function ObtieneLineas(){
			$Cartor = new Cartas();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return array();
			else	
				return $Cartor->Carta4DetalleGet($_GET['ID']);
		}
		//
		function Estado($_var){
			$Cartor = new Cartas();
			return $Cartor->CartasEstado($_var);
		}
		
		function Accion($_var){
			$Cartor = new Cartas();
			return $Cartor->CartasAccion($_var);
		}
	
		function Historial(){
			$Cartor = new Cartas();
			return $Cartor->CartaHistorial($_GET['ID']);
		}
		
		function Aprobar(){
			if($this->_ROW['ROL'] == '0') return true;
			elseif($this->Securitor->UsuarioPermiso('68')=='E') return true;
			else return false;
		}
		//
	}
//
}
?>