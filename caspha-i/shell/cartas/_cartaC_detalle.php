<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	
	$Cartor = new Cartas();
	
	echo $Cartor->CartaADetalleColumnaGet($_POST['tipo'], $_POST['desde'], $_POST['hasta']);
	exit;
}elseif( isset($_POST['subir'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Robot = new Cartas();
			
	if($_FILES['archivo']['size'] > 0){
		$ruta = $_FILES['archivo']['name'];
		$file = $_FILES['archivo']['tmp_name'];		
		if($_FILES['archivo']['type'] != 'application/x-msdownload' and $_FILES['archivo']['type'] != 'application/vnd.ms-excel') die('Error: El archivo debe ser tipo .xls (Excel 2003)');
		
		Bibliotecario::ZipSubir($file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");
		
		require_once '../metodos/Excel/reader.php';
		$data = new Spreadsheet_Excel_Reader();
		$data->read($ruta);
		for($i=2;$i<=$data->sheets[0]['numRows'];$i++){
			if( isset($data->sheets[0]['cells'][$i][1]) && str_replace('','',$data->sheets[0]['cells'][$i][1]) != '')
				$Robot->QuechersDetalleSet(
					$data->sheets[0]['cells'][$i][1], //%recu
					$data->sheets[0]['cells'][$i][2], //matriz
					$data->sheets[0]['cells'][$i][3], //analito
					$data->sheets[0]['cells'][$i][4], //analista
					$data->sheets[0]['cells'][$i][5] //obs
				);
		}
		Bibliotecario::ZipEliminar($ruta);
		
	}else die('Archivo vac�o');
	?>
	<script>
		opener.location.reload();
		alert('Datos ingresados');
		window.close();
	</script>
	<?php
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _cartaA_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) ) die('Error de par�metros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('67') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function Analitos(){
			$Variator = new Varios();
			return $Variator->SubAnalisisGet('8', '1');
		}	
	}
//
}
?>