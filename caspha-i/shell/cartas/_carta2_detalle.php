<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	//$Listator->EquiposDisponibles('cartas', 0, $ROW['UID'], $ROW['LID'], basename(__FILE__));
	$Listator->EquiposCalibrajeLista2('cartas', $_GET['list'], $ROW['LID'], basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
}elseif( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['paso']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Cartor = new Cartas();
	
	if($_POST['paso'] == '1'){	
		echo $Cartor->Carta2Paso1($_POST['cs'], 
			$_POST['accion'], 
			$_POST['fecha'], 
                        $_POST['equipo'], 
			$_POST['fechavence1'], 
			$_POST['fechavence2'], 
			$_POST['fechavence3'], 
			$_POST['lote1'], 
			$_POST['lote2'], 
			$_POST['lote3'], 
			$_POST['certificado1'], 
			$_POST['certificado2'], 
			$_POST['certificado3'], 
			$_POST['electrodo'], 
			$_POST['limiteph'],
			$_POST['limitedesv'], 
			$_POST['limitecoef'], 
			$_POST['limitemv'],
			$_ROW['UID'], 
			$_ROW['LID']);	
		exit;
	}elseif($_POST['paso'] == '2'){
		echo $Cartor->Carta2Paso2($_POST['cs'], 
			$_POST['ph4'], 
			$_POST['ph7'],
			$_POST['ph10']);
		exit;
	}elseif($_POST['paso'] == '9'){
		echo $Cartor->CartaActualiza($_POST['cs'], $_ROW['UID'], $_POST['accion']);
		exit;
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _carta2_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('67') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Cartor = new Cartas();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Cartor->Carta2EncabezadoVacio();
			else	
				return $Cartor->Carta2EncabezadoGet($_GET['ID']);
		}
		
		function ObtieneLineas(){
			$Cartor = new Cartas();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Cartor->Carta2DetalleVacio();
			else	
				return $Cartor->Carta2DetalleGet($_GET['ID']);
		}
		//
		function Estado($_var){
			$Cartor = new Cartas();
			return $Cartor->CartasEstado($_var);
		}
		
		function Accion($_var){
			$Cartor = new Cartas();
			return $Cartor->CartasAccion($_var);
		}
	
		function Historial(){
			$Cartor = new Cartas();
			return $Cartor->CartaHistorial($_GET['ID']);
		}
		
		function Aprobar(){
			if($this->_ROW['ROL'] == '0') return true;
			elseif($this->Securitor->UsuarioPermiso('68')=='E') return true;
			else return false;
		}
		
		function Modificar($_creador){
			if($this->_ROW['UID'] == $_creador) return true;
			else return false;
		}
		//
	}
//
}
?>