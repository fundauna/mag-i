<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

final class _historial extends Mensajero{
	private $_ROW = array();
	private $Securitor = '';
	private $inicio = false;
	
	function __construct(){
		$this->Securitor = new Seguridad();
		if(!$this->Securitor->SesionAuth()) $this->Err();
		$this->_ROW = $this->Securitor->SesionGet();
		/*PERMISOS*/
		if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('67') );
		/*PERMISOS*/
		
		if(!isset($_POST['tipo'])){
			$_POST['desde'] = $_POST['hasta'] = '';
			$_POST['tipo'] = $_POST['estado'] = '0';
			$this->inicio = true;
		}
	}

    function Encabezado($_hoja, $_tipo, $_titulo)
    {
        $Qualitor = new Calidad();
        echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
    }
	
	function CartasMuestra(){
		$Cartor = new Cartas();
		if($this->inicio)
			return array();
		else
			return $Cartor->CartasResumenGet($_POST['desde'], $_POST['hasta'], $_POST['tipo'], $_POST['estado'], $this->_ROW['LID']);
	}
	
	function Catalogo(){
		//if($this->_ROW['ROL'] == '0'){ //ADMIN
			$Robot = new Cartas();
			return $Robot->CartasCatalogo($this->_ROW['LID']);
		//}else{
		//	return $this->Securitor->AutorizacionesCartasMuestra($this->_ROW['UID']);
		//}
	}
	
	function Estado($_var){
		$Cartor = new Cartas();
		return $Cartor->CartasEstado($_var);
	}
}
?>