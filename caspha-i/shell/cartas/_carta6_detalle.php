<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	$Listator->EquiposDisponibles('cartas', 0, $ROW['UID'], $ROW['LID'], basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
}elseif( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['paso']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Cartor = new Cartas();
	
	if($_POST['paso'] == '1'){	
		echo $Cartor->Carta6Paso1($_POST['cs'], 
                        $_POST['fecha'],
			$_POST['accion'],
			$_POST['modalidad'], 
			$_POST['cod_bal'], 
			$_POST['nombre'], 
			$_POST['corriente'],
			$_POST['origen'], 
			$_POST['longitud'], 
			$_POST['tiene'], 
			$_POST['ancho'], 
			$_POST['conc'], 
			$_POST['volA'], 
			$_POST['volB'],
			$_ROW['LID'],
			$_ROW['UID']);	
		exit;
	}elseif($_POST['paso'] == '2'){
		echo $Cartor->Carta6Paso2($_POST['cs'], 
			$_POST['blanco'], 
			$_POST['abs1'],
			$_POST['abs2'],
			$_POST['abs3'],
			$_POST['abs4'],
			$_POST['abs5']);	
		exit;
	}elseif($_POST['paso'] == '3'){
		$str = '<table id="tabla"><tr><td>X</td><td>Y</td></tr>';
		$str .= "<tr><td>{$_POST['x0']}</td><td>{$_POST['y0']}</td></tr>";
		$str .= "<tr><td>{$_POST['x1']}</td><td>{$_POST['y1']}</td></tr>";
		$str .= "<tr><td>{$_POST['x2']}</td><td>{$_POST['y2']}</td></tr>";
		$str .= "<tr><td>{$_POST['x3']}</td><td>{$_POST['y3']}</td></tr>";
		$str .= "<tr><td>{$_POST['x4']}</td><td>{$_POST['y4']}</td></tr>";
		$str .= "</table>";
		echo $str;
		exit;
	}elseif($_POST['paso'] == '9'){
		echo $Cartor->CartaActualiza($_POST['cs'], $_ROW['UID'], $_POST['accion']);
		exit;
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _carta6_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('67') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Cartor = new Cartas();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Cartor->Carta6EncabezadoVacio();
			else	
				return $Cartor->Carta6EncabezadoGet($_GET['ID']);
		}
		
		function ObtieneLineas(){
			$Cartor = new Cartas();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Cartor->Carta6DetalleVacio();
			else	
				return $Cartor->Carta6DetalleGet($_GET['ID']);
		}
		
		function ObtieneVolumenes(){
			$Cartor = new Cartas();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Cartor->Carta6VolumenesVacio();
			else	
				return $Cartor->Carta6VolumenesGet($_GET['ID']);
		}
		//
		function Estado($_var){
			$Cartor = new Cartas();
			return $Cartor->CartasEstado($_var);
		}
		
		function Accion($_var){
			$Cartor = new Cartas();
			return $Cartor->CartasAccion($_var);
		}
	
		function Historial(){
			$Cartor = new Cartas();
			return $Cartor->CartaHistorial($_GET['ID']);
		}
		
		function Aprobar(){
			if($this->_ROW['ROL'] == '0') return true;
			elseif($this->Securitor->UsuarioPermiso('68')=='E') return true;
			else return false;
		}
		
		function Modificar($_creador){
			if($this->_ROW['UID'] == $_creador) return true;
			else return false;
		}
		//
	}
//
}
?>