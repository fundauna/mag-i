<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	//$Listator->EquiposDisponibles('cartas', 0, $ROW['UID'], $ROW['LID'], basename(__FILE__));
	$Listator->EquiposCalibrajeLista2('cartas', $_GET['list'], $ROW['LID'], basename(__FILE__));
	exit;
}elseif( isset($_GET['list2'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	$Listator->PesasLista('cartas', $_GET['list2'], $ROW['LID'], basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
}elseif( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['paso']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Cartor = new Cartas();
	
	if($_POST['paso'] == '1'){	
		echo $Cartor->Carta1Paso1($_POST['cs'], 
			$_POST['accion'], 
			$_POST['tipo_bal'], 
			$_POST['cod_bal'], 
			$_POST['linealidad'], 
			$_POST['repetibilidad'],
			$_POST['rango'],  
			$_POST['decimalesA'], 
			$_POST['decimalesB'], 
			$_POST['decimalesC'], 
			$_POST['decimalesD'], 
			$_POST['cert1'], 
			$_POST['cert2'], 
			$_POST['cert3'], 
			$_POST['cert4'], 
			$_POST['pesa1'], 
			$_POST['pesa2'], 
			$_POST['pesa3'], 
			$_POST['pesa4'], 
			$_POST['nominal1'], 
			$_POST['nominal2'], 
			$_POST['nominal3'], 
			$_POST['nominal4'], 
			$_POST['real1'], 
			$_POST['real2'], 
			$_POST['real3'], 
			$_POST['real4'], 
			$_POST['inc1'], 
			$_POST['inc2'], 
			$_POST['inc3'], 
			$_POST['inc4'],
			$_ROW['LID'],
			$_ROW['UID']);	
		exit;
	}elseif($_POST['paso'] == '2'){
		echo $Cartor->Carta1Paso2($_POST['cs'], 
			$_POST['obs'], 
			$_POST['pto1_ind_asc'],
			$_POST['pto2_ind_asc'],
			$_POST['pto3_ind_asc'],
			$_POST['pto4_ind_asc'],
			$_POST['pto1_ind_dsc'],
			$_POST['pto2_ind_dsc'],
			$_POST['pto3_ind_dsc'],
			$_POST['pto4_ind_dsc'],
			$_POST['platillo'],
			$_POST['pesa5'],
			$_POST['nominal5'],
			$_POST['real5'],
			$_POST['sensibilidad'],
			$_POST['pos0'],
			$_POST['pos1'],
			$_POST['pos2'],
			$_POST['pos3'],
			$_POST['pos4'],
			$_POST['pos5']);	
		exit;
	}elseif($_POST['paso'] == '3'){
		echo $Cartor->Carta1Paso3($_POST['cs'], 
			$_POST['valA'],
			$_POST['valB'],
			$_POST['valC'],
			$_POST['valD']);	
		exit;
	}elseif($_POST['paso'] == '9'){
		echo $Cartor->CartaActualiza($_POST['cs'], $_ROW['UID'], $_POST['accion']);
		exit;
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _carta1_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('67') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Cartor = new Cartas();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Cartor->Carta1EncabezadoVacio();
			else	
				return $Cartor->Carta1EncabezadoGet($_GET['ID']);
		}
		
		function ObtieneLineas(){
			$Cartor = new Cartas();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Cartor->Carta1DetalleVacio();
			else	
				return $Cartor->Carta1DetalleGet($_GET['ID']);
		}
		//
		function Estado($_var){
			$Cartor = new Cartas();
			return $Cartor->CartasEstado($_var);
		}
		
		function Accion($_var){
			$Cartor = new Cartas();
			return $Cartor->CartasAccion($_var);
		}
	
		function Historial(){
			$Cartor = new Cartas();
			return $Cartor->CartaHistorial($_GET['ID']);
		}
		
		function Aprobar(){
			if($this->_ROW['ROL'] == '0') return true;
			elseif($this->Securitor->UsuarioPermiso('68')=='E') return true;
			else return false;
		}
		
		function Modificar($_creador){
			if($this->_ROW['UID'] == $_creador) return true;
			else return false;
		}
		//
	}
//
}
?>