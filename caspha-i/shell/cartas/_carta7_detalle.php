<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	//$Listator->EquiposDisponibles('cartas', 0, $ROW['UID'], $ROW['LID'], basename(__FILE__));
	if($_GET['list'] == '1')
		$Listator->EquiposDisponibles('cartas', 0, $ROW['UID'], $ROW['LID'], basename(__FILE__));
	else
		$Listator->ColumnasLista('cartas', $ROW['LID'], basename(__FILE__), 1);
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
}elseif( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['paso']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Cartor = new Cartas();
	
	if($_POST['paso'] == '1'){	
		echo $Cartor->Carta7Paso1($_POST['cs'], 
			$_POST['fecha'], 
			$_POST['accion'], 
			$_POST['nombre'], 
			$_POST['masa'], 
			$_POST['origen'], 
			$_POST['aforado'], 
			$_POST['tiene'], 
			$_POST['pureza'], 
			$_POST['cod_bal'], 
			$_POST['inyeccion'], 
			$_POST['tipo_bomba'], 
			$_POST['tipo_inyector'], 
			$_POST['eluente'], 
			$_POST['temp1'], 
			$_POST['modo'], 
			$_POST['canalA'], 
			$_POST['flujoA'], 
			$_POST['presiA'], 
			$_POST['canalB'], 
			$_POST['flujoB'], 
			$_POST['presiB'], 
			$_POST['canalC'], 
			$_POST['flujoC'], 
			$_POST['presiC'], 
			$_POST['canalD'], 
			$_POST['flujoD'], 
			$_POST['presiD'], 
			$_POST['tipo_col'], 
			$_POST['cod_col'], 
			$_POST['marcafase'], 
			$_POST['fecha_em'], 
			$_POST['tipo_detector'], 
			$_POST['temp2'], 
			$_POST['temp3'], 
			$_POST['dimensiones'], 
			$_POST['longitud'], 
			$_POST['otros'], 
			$_POST['bar'], 
			$_POST['pat_ali'], 
			$_POST['pat_afo'], 
			$_ROW['LID'],
			$_ROW['UID']);	
		exit;
	}elseif($_POST['paso'] == '2'){
		echo $Cartor->Carta7Paso2($_POST['cs'], $_POST['t'], $_POST['mass']);	
		exit;
	}elseif($_POST['paso'] == '3'){
		echo $Cartor->Carta7Paso3($_POST['cs'], $_POST['vol_ali'], $_POST['vol_afo'], $_POST['area']);	
		exit;
	}elseif($_POST['paso'] == '4'){
		echo $Cartor->Carta7Paso4($_POST['cs'], $_POST['are'], $_POST['tr'], $_POST['alt']);	
		exit;
	}elseif($_POST['paso'] == '5'){
		$str = '<table id="tabla"><tr><td>X</td><td>Y</td></tr>';
		if($_POST['tipo'] == 'A'){
			$str .= "<tr><td>{$_POST['x0']}</td><td>{$_POST['y0']}</td></tr>";
			$str .= "<tr><td>{$_POST['x1']}</td><td>{$_POST['y1']}</td></tr>";
			$str .= "<tr><td>{$_POST['x2']}</td><td>{$_POST['y2']}</td></tr>";
			$str .= "<tr><td>{$_POST['x3']}</td><td>{$_POST['y3']}</td></tr>";
			
		}else{
			$ROW = explode(';', $_POST['x']);
			for($x=1;$x<count($ROW);$x++){
				$str .= "<tr><td>{$x}</td><td>{$ROW[$x]}</td></tr>";
			}
		}
		$str .= "</table>";
		echo $str;
		exit;
	}elseif($_POST['paso'] == '9'){
		echo $Cartor->CartaActualiza($_POST['cs'], $_ROW['UID'], $_POST['accion']);
		exit;
	}
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _carta7_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('67') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Cartor = new Cartas();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Cartor->Carta7EncabezadoVacio();
			else	
				return $Cartor->Carta7EncabezadoGet($_GET['ID']);
		}
		
		function ObtieneLineas(){
			$Cartor = new Cartas();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Cartor->Carta7DetalleVacio();
			else	
				return $Cartor->Carta7DetalleGet($_GET['ID']);
		}
		
		function ObtieneDetecciones(){
			$Cartor = new Cartas();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Cartor->Carta7LinealidadVacio();
			else	
				return $Cartor->Carta7LinealidadGet($_GET['ID']);
		}
		
		function ObtieneRepro(){
			$Cartor = new Cartas();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Cartor->Carta7ReproVacio();
			else	
				return $Cartor->Carta7ReproGet($_GET['ID']);
		}
		//
		function Estado($_var){
			$Cartor = new Cartas();
			return $Cartor->CartasEstado($_var);
		}
		
		function Accion($_var){
			$Cartor = new Cartas();
			return $Cartor->CartasAccion($_var);
		}
	
		function Historial(){
			$Cartor = new Cartas();
			return $Cartor->CartaHistorial($_GET['ID']);
		}
		
		function Aprobar(){
			if($this->_ROW['ROL'] == '0') return true;
			elseif($this->Securitor->UsuarioPermiso('68')=='E') return true;
			else return false;
		}
		
		function Modificar($_creador){
			if($this->_ROW['UID'] == $_creador) return true;
			else return false;
		}
		//
	}
//
}
?>