<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}

	if( !isset($_POST['paso']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Cartor = new Cartas();
			
	if($_POST['paso'] == '1'){ 	
		echo $Cartor->Carta9Paso1($_POST['id'],
			$_POST['acc'], 
			$_POST['equipo'], 
			$_POST['idColumna'], 
			$_POST['idEstandar'], 
			$_POST['connominal'],
			$_ROW['LID'],
			$_ROW['UID']
		);
	}elseif($_POST['paso'] == '2'){ 
		echo $Cartor->Carta9Paso2($_POST['id'],
			$_POST['v1'], 
			$_POST['v2'], 
			$_POST['v3'], 
			$_POST['v4'], 
			$_POST['v5'], 
			$_POST['v6'], 
			$_POST['v7'], 
			$_POST['v8'], 
			$_POST['v9'], 
			$_POST['v10'], 
			$_POST['v11'], 
			$_POST['v12'], 
			$_POST['v13'], 
			$_POST['v14'], 
			$_POST['v15'], 
			$_ROW['UID']
		);
	}elseif($_POST['paso'] == '3'){ 
		echo $Cartor->Carta9Paso3($_POST['id'],
			$_POST['dimensiones'], 
			$_POST['tr'], 
			$_POST['resol'], 
			$_POST['qual'], 
			$_POST['asim'], 
			$_POST['platos'], 
			$_ROW['UID']
		);
	}elseif($_POST['paso'] == '4'){ 
		echo $Cartor->Carta9Paso4($_POST['id'],
			$_POST['vresol'], 
			$_POST['vasimetria'], 
			$_POST['vresol2'], 
			$_POST['vasimetria2'], 
			$_POST['tr'], 
			$_POST['area'], 
			$_ROW['UID']
		);
	}elseif($_POST['paso'] == '5'){ 
		echo $Cartor->Carta9Paso5($_POST['id'],
			$_POST['coef'], 
			$_POST['obs'], 
			$_POST['tr'], 
			$_POST['area'], 
			$_POST['conc'], 
			$_ROW['UID']
		);
	}elseif($_POST['paso'] == '6'){ 
		if($_POST['clase'] == 'A'){
			$str = "<table id='tabla{$_POST['tipo']}'><tr><td>X</td><td>Y</td></tr>";
			$str .= "<tr><td>1</td><td>{$_POST['y0']}</td></tr>";
			$str .= "<tr><td>2</td><td>{$_POST['y1']}</td></tr>";
			$str .= "<tr><td>3</td><td>{$_POST['y2']}</td></tr>";
			$str .= "<tr><td>4</td><td>{$_POST['y3']}</td></tr>";
			$str .= "<tr><td>5</td><td>{$_POST['y4']}</td></tr>";
			$str .= "<tr><td>6</td><td>{$_POST['y5']}</td></tr>";
			$str .= "<tr><td>7</td><td>{$_POST['y6']}</td></tr>";
		}else{
			$str = "<table id='tabla{$_POST['tipo']}'><tr><td>X</td><td>Y</td><td>A</td><td>B</td><td>C</td><td>D</td><td>E</td><td>F</td></tr>";
			//
			$ROW1 = explode('&', $_POST['x']);
			$ROW2 = explode('&', $_POST['y']);
			//
			if($_POST['tipo'] == 7) $empieza = 1;
			else if($_POST['tipo'] == 8) $empieza = 13;
			else if($_POST['tipo'] == 9) $empieza = 25;
			//
			$sumX = $sumY = 0;
			
			for($x=0,$i=$empieza;$x<4;$x++,$i+=3){
				$promx = ($ROW1[$i] + $ROW1[$i+1] + $ROW1[$i+2]) / 3;
				$promy = ($ROW2[$i] + $ROW2[$i+1] + $ROW2[$i+2]) / 3;
				$sumX += $promx;
				$sumY += $promy;
				$str .= "<tr>
					<td id='{$_POST['tipo']}_CN{$x}'>{$promx}</td>
					<td id='{$_POST['tipo']}_AR{$x}'>{$promy}</td>
					<td id='{$_POST['tipo']}_A{$x}'></td>
					<td id='{$_POST['tipo']}_B{$x}'></td>
					<td id='{$_POST['tipo']}_C{$x}'></td>
					<td id='{$_POST['tipo']}_D{$x}'></td>
					<td id='{$_POST['tipo']}_E{$x}'></td>
					<td id='{$_POST['tipo']}_F{$x}'></td>
				</tr>";
			}//FOR
			$promx = $sumX/4;
			$promy = $sumY/4;
			$x2 = $x+1;
			$str .= "<tr>
					<td id='{$_POST['tipo']}_CN{$x}'>{$promx}</td>
					<td id='{$_POST['tipo']}_AR{$x}'>{$promy}</td>
					<td id='{$_POST['tipo']}_A{$x}'></td>
					<td id='{$_POST['tipo']}_B{$x}'></td>
					<td id='{$_POST['tipo']}_C{$x}'></td>
					<td id='{$_POST['tipo']}_D{$x}'></td>
					<td id='{$_POST['tipo']}_E{$x}'></td>
					<td id='{$_POST['tipo']}_F{$x}'></td>
				</tr>
				<tr>
					<td id='{$_POST['tipo']}_CN{$x2}'>{$sumX}</td>
					<td id='{$_POST['tipo']}_AR{$x2}'>{$sumY}</td>
					<td id='{$_POST['tipo']}_A{$x2}'></td>
					<td id='{$_POST['tipo']}_B{$x2}'></td>
					<td id='{$_POST['tipo']}_C{$x2}'></td>
					<td id='{$_POST['tipo']}_D{$x2}'></td>
					<td id='{$_POST['tipo']}_E{$x2}'></td>
					<td id='{$_POST['tipo']}_F{$x2}'></td>
				</tr>";
		}
		$str .= "</table>";
		echo $str;
	}
	
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO LECTURA
*******************************************************************/
}elseif( isset($_POST['_VER'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}

	$Robot = new Cartas();
	
	if($_POST['_VER']=='2'){
		$ROW = $Robot->Carta9ChequeoGet($_POST['id']);
	?>
		<tr align="center">
			<td><strong>Rubro</strong></td>
			<td><strong>Variable</strong></td>
			<td><strong>Cumple</strong></td>
		</tr>
		<tr>
			<td align="center">Asignaci&oacute;n de masas (69,219,502)</td>
			<td align="center">
				<input type="text" id="v1" value="<?=$ROW[0]['v1']?>" class="ano" readonly>
				<input type="text" id="v2" value="<?=$ROW[0]['v2']?>" class="ano" readonly>
				<input type="text" id="v3" value="<?=$ROW[0]['v3']?>" class="ano" readonly>
			</td>
			<td align="center" id="cumple1">No</td>
		</tr>
		<tr>
			<td align="center">Ancho de se&ntilde;ales (Pw50, 0.6 &plusmn; 0.1)</td>
			<td align="center">
				<input type="text" id="v4" value="<?=$ROW[0]['v4']?>" class="ano" readonly>
				<input type="text" id="v5" value="<?=$ROW[0]['v5']?>" class="ano" readonly>
				<input type="text" id="v6" value="<?=$ROW[0]['v6']?>" class="ano" readonly>
			</td>
			<td align="center" id="cumple2">No</td>
		</tr>
		<tr>
			<td align="center">Voltaje Repeller (0-40v)</td>
			<td align="center"><input type="text" id="v7" value="<?=$ROW[0]['v7']?>" class="monto" readonly></td>
			<td align="center" id="cumple3">No</td>
		</tr>
		<tr>
			<td align="center">Voltaje electromultiplicador (EMVolts,1000-3000V)</td>
			<td align="center"><input type="text" id="v8" value="<?=$ROW[0]['v8']?>" class="monto" readonly></td>
			<td align="center" id="cumple4">No</td>
		</tr>
		<tr>
			<td align="center">Se&ntilde;al m&aacute;s abundante (Base, 69 o 219)</td>
			<td align="center"><input type="text" id="v9" value="<?=$ROW[0]['v9']?>" class="ano" readonly></td>
			<td align="center" id="cumple5">No</td>
		</tr>
		<tr>
			<td align="center">Abundancia del m&aacute;s abundante (Abundance, 400000-600000):</td>
			<td align="center"><input type="text" id="v10" value="<?=$ROW[0]['v10']?>" class="monto" readonly></td>
			<td align="center" id="cumple6">No</td>
		</tr>
		<tr>
			<td align="center"><strong>Relaci&oacute;n de isotopos (Iso Ratio)</strong></td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td align="center">70/69 (&ge; 0.5% &le; 1.6%):</td>
			<td align="center"><input type="text" id="v11" value="<?=$ROW[0]['v11']?>" class="monto" readonly></td>
			<td align="center" id="cumple7">No</td>
		</tr>
		<tr>
			<td align="center">220/219 (&ge; 3.2% &le; 5.4%):</td>
			<td align="center"><input type="text" id="v12" value="<?=$ROW[0]['v12']?>" class="monto" readonly></td>
			<td align="center" id="cumple8">No</td>
		</tr>
		<tr>
			<td align="center">503/502 (&ge; 7.9% &le; 12.3%):</td>
			<td align="center"><input type="text" id="v13" value="<?=$ROW[0]['v13']?>" class="monto" readonly></td>
			<td align="center" id="cumple9">No</td>
		</tr>
		<tr>
			<td align="center">Chequeo de agua (&lt; 20%):</td>
			<td align="center"><input type="text" id="v14" value="<?=$ROW[0]['v14']?>" class="monto" readonly></td>
			<td align="center" id="cumpleA">No</td>
		</tr>
		<tr>
			<td align="center">Chequeo de nitr&oacute;geno (&lt; 10%):</td>
			<td align="center"><input type="text" id="v15" value="<?=$ROW[0]['v15']?>" class="monto" readonly></td>
			<td align="center" id="cumpleB">No</td>
		</tr>
		<?php
	}elseif($_POST['_VER'] == '3'){
		$ROW = $Robot->Carta9OtrosGet($_POST['id']);
		?>
		<tr>
			<td colspan="3" align="center">Dimensiones de la Columna(seg&uacute;n calibraci&oacute;n del equipo)</td>
			<td colspan="4"><?=$ROW[0]['dimensiones']?></td>
		</tr>
		<tr align="center">
			<td><strong>Se&ntilde;al</strong></td>
			<td><strong>TR(min)</strong></td>
			<td><strong>Resol</strong></td>
			<td><strong>Nombre del Compuesto<br /> seg&uacute;n NIST</strong></td>
			<td><strong>% Qual</strong></td>
			<td><strong>Asimetr&iacute;a</strong></td>
			<td><strong>Platos<br>Te&oacute;ricos</strong></td>
		</tr>	
		<?php
		$ROW = $Robot->Carta9BarridoScanGet($_POST['id']);
		$ROW7 = array('<strong>2,3-Butanodiol</strong>','<strong>Decano</strong>','1-Octanol','<strong>Undecano</strong>','Nonanal','2,6-Dimetilfenol','Acido 2-etil hexanoico','2,6-Dimetilfenol','Metil decanoato','Metil undecanoato','<strong>Diciclohexilamina</strong>','Metil dodecanoato');
		for($x=0;$x<count($ROW);$x++){
		?>
			<tr>
				<td align="center"><strong><?=$x+1?></strong></td>
				<td align="center"><input id="tr<?=$x+1?>" name="tr" value="<?=$ROW[$x]['tr']?>" class="monto" readonly/></td>
				<td align="center"><input id="resol<?=$x+1?>" name="resol" value="<?=$ROW[$x]['resol']?>" class="monto" readonly/></td>
				<td align="center"><?=$ROW7[$x]?></td>
				<td align="center"><input id="qual<?=$x+1?>" name="qual" value="<?=$ROW[$x]['qual']?>" class="cantidad" readonly/></td>
				<td align="center"><input id="asim<?=$x+1?>" name="asim" value="<?=$ROW[$x]['asim']?>" class="monto" readonly/></td>
				<td align="center"><input id="platos<?=$x+1?>" name="platos" value="<?=$ROW[$x]['platos']?>" class="monto" readonly/></td>
			</tr>
		<?php 
		}
		?>
		<tr><td colspan="7">&nbsp;</td></tr>
		<tr>
			<td colspan="3"><strong>Rubro</strong></td>
			<td colspan="4"><strong>Cumple</strong></td>
		</tr>
		<tr>
			<td colspan="3">Se&ntilde;ales de decano y undecano # platos te&oacute;ricos mayor a 20000:</td>
			<td colspan="4" id="cumpleC">No</td>
		</tr>
		<tr>
			<td colspan="3">Se&ntilde;ales de decano y undecano Resol > 1.5 Asimetr&iacute;a < 1.5:</td>
			<td colspan="4" id="cumpleD">No</td>
		</tr>
		<tr>
			<td colspan="3">Se&ntilde;al de 1-Octanol Resol > 1.5 Asimetr&iacute;a < 1.5:</td>
			<td colspan="4" id="cumpleE">No</td>
		</tr>
		<tr>
			<td colspan="3">Se&ntilde;al de diciclohexilamina Resol > 1.5 Asimetr&iacute;a < 1.5:</td>
			<td colspan="4" id="cumpleF">No</td>
		</tr>
		<tr>
			<td colspan="3">Se&ntilde;al de 2,3-Butanodiol Identificada por NIST:</td>
			<td colspan="4" id="cumpleG">No</td>
		</tr>
		<?php
	}elseif($_POST['_VER'] == '4'){
		$ROW = $Robot->Carta9BarridoSimGet($_POST['id']);
		$x=$y=0;
		?>
		<tr align="center">
			<td><strong>Compuesto</strong></td>
			<td><strong># Corrida</strong></td>
			<td><strong>TR(min)</strong></td>
			<td><strong>&Aacute;rea</strong></td>
		</tr>			
		<tr>
			<td rowspan="10" align="center">n-Undecano</td>
			<td align="center">1</td>
			<td align="center"><input type="text" id="simtr11" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea11" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">2</td>
			<td align="center"><input type="text" id="simtr12" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea12" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">3</td>
			<td align="center"><input type="text" id="simtr13" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea13" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">4</td>
			<td align="center"><input type="text" id="simtr14" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea14" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">5</td>
			<td align="center"><input type="text" id="simtr15" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea15" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">6</td>
			<td align="center"><input type="text" id="simtr16" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea16" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">7</td>
			<td align="center"><input type="text" id="simtr17" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea17" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">Promedio</td>
			<td align="center"><input type="text" id="simpromtr1" value="0.00" class="monto" readonly></td>
			<td align="center"><input type="text" id="simpromarea1" value="0.00" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">Desviaci&oacute;n Estandar</td>
			<td align="center"><input type="text" id="simdstr1" value="0.00" class="monto" readonly></td>
			<td align="center"><input type="text" id="simdsarea1" value="0.00" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">Coeficiente Variaci&oacute;n</td>
			<td align="center"><input type="text" id="simcvtr1" value="0.00" class="monto" readonly></td>
			<td align="center"><input type="text" id="simcvarea1" value="0.00" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center" colspan="2">Limite superior</td>
			<td align="center"><input type="text" id="limitetrSup1"></td>
			<td align="center"><input type="text" id="limitearSup1"></td>
		</tr>
		<tr>
			<td align="center" colspan="2">Limite inferior</td>
			<td align="center"><input type="text" id="limitetrInf1"></td>
			<td align="center"><input type="text" id="limitearInf1"></td>
		</tr>
		<input type="hidden" id="result1">
		<tr><td colspan="4"><hr /></td></tr>
		<tr>
			<td rowspan="10" align="center">2,6-Dimetilfenol</td>
			<td align="center">1</td>
			<td align="center"><input type="text" id="simtr21" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea21" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">2</td>
			<td align="center"><input type="text" id="simtr22" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea22" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">3</td>
			<td align="center"><input type="text" id="simtr23" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea23" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">4</td>
			<td align="center"><input type="text" id="simtr24" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea24" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">5</td>
			<td align="center"><input type="text" id="simtr25" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea25" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">6</td>
			<td align="center"><input type="text" id="simtr26" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea26" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">7</td>
			<td align="center"><input type="text" id="simtr27" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea27" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">Promedio</td>
			<td align="center"><input type="text" id="simpromtr2" value="0.00" class="monto" readonly></td>
			<td align="center"><input type="text" id="simpromarea2" value="0.00" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">Desviaci&oacute;n Estandar</td>
			<td align="center"><input type="text" id="simdstr2" value="0.00" class="monto" readonly></td>
			<td align="center"><input type="text" id="simdsarea2" value="0.00" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">Coeficiente Variaci&oacute;n</td>
			<td align="center"><input type="text" id="simcvtr2" value="0.00" class="monto" readonly></td>
			<td align="center"><input type="text" id="simcvarea2" value="0.00" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center" colspan="2">Limite superior</td>
			<td align="center"><input type="text" id="limitetrSup2"></td>
			<td align="center"><input type="text" id="limitearSup2"></td>
		</tr>
		<tr>
			<td align="center" colspan="2">Limite inferior</td>
			<td align="center"><input type="text" id="limitetrInf2"></td>
			<td align="center"><input type="text" id="limitearInf2"></td>
		</tr>
		<input type="hidden" id="result2">
		<tr><td colspan="4"><hr /></td></tr>
		<tr>
			<td rowspan="10" align="center">2,6-Dimetilanilina</td>
			<td align="center">1</td>
			<td align="center"><input type="text" id="simtr31" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea31" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">2</td>
			<td align="center"><input type="text" id="simtr32" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea32" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">3</td>
			<td align="center"><input type="text" id="simtr33" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea33" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">4</td>
			<td align="center"><input type="text" id="simtr34" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea34" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">5</td>
			<td align="center"><input type="text" id="simtr35" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea35" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">6</td>
			<td align="center"><input type="text" id="simtr36" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea36" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">7</td>
			<td align="center"><input type="text" id="simtr37" name="tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly></td>
			<td align="center"><input type="text" id="simarea37" name="area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">Promedio</td>
			<td align="center"><input type="text" id="simpromtr3" value="0.00" class="monto" readonly></td>
			<td align="center"><input type="text" id="simpromarea3" value="0.00" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">Desviaci&oacute;n Estandar</td>
			<td align="center"><input type="text" id="simdstr3" value="0.00" class="monto" readonly></td>
			<td align="center"><input type="text" id="simdsarea3" value="0.00" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center">Coeficiente Variaci&oacute;n</td>
			<td align="center"><input type="text" id="simcvtr3" value="0.00" class="monto" readonly></td>
			<td align="center"><input type="text" id="simcvarea3" value="0.00" class="monto" readonly></td>
		</tr>
		<tr>
			<td align="center" colspan="2">Limite superior</td>
			<td align="center"><input type="text" id="limitetrSup3"></td>
			<td align="center"><input type="text" id="limitearSup3"></td>
		</tr>
		<tr>
			<td align="center" colspan="2">Limite inferior</td>
			<td align="center"><input type="text" id="limitetrInf3"></td>
			<td align="center"><input type="text" id="limitearInf3"></td>
		</tr>
		<input type="hidden" id="result3">
		<tr><td colspan="4"><hr /></td></tr>
		<tr align="center">
			<td colspan="2"><strong>Rubro</strong></td>
			<td><strong>Variable</strong></td>
			<td><strong>Cumple</strong></td>
		</tr>
		<?php
		$ROW = $Robot->Carta9OtrosGet($_POST['id']);
		?>
		<tr>
			<td align="center" colspan="2">Se&ntilde;al de 2,6-Dimetilfenol Resol > 1,5 Asimetr&iacute;a < 1,5</td>
			<td align="center">
				<input type="text" id="vresol" value="<?=$ROW[0]['vresol']?>" class="monto" readonly>
				<input type="text" id="vasimetria" value="<?=$ROW[0]['vasimetria']?>" class="monto" readonly>
			</td>
			<td align="center"><label id="cumple32" style="display:none">S&iacute;</label><label id="cumple33">No</label></td>
		</tr>
		<tr>
			<td align="center" colspan="2">Se&ntilde;al de 2,6-Dimetilanilina Resol > 1,5 Asimetr&iacute;a < 1,5</td>
			<td align="center">
				<input type="text" id="vresol2" value="<?=$ROW[0]['vresol2']?>" class="monto" readonly>
				<input type="text" id="vasimetria2" value="<?=$ROW[0]['vasimetria2']?>" class="monto" readonly>
			</td>
			<td align="center"><label id="cumple34" style="display:none">S&iacute;</label><label id="cumple35">No</label></td>
		</tr>
		<?php
	}elseif($_POST['_VER'] == '5'){
		$ROW = $Robot->Carta9LinealidadGet($_POST['id']);
		$x=$y=$z=0;
		?>
		<tr>
			<td align="center"><strong>Compuesto</strong></td>
			<td align="center"><strong>Nivel</strong></td>
			<td align="center"><strong>TR(min)</strong></td>
			<td align="center"><strong>&Aacute;rea</strong></td>
			<td align="center"><strong>Conc(ug/ml)</strong></td>
		</tr>
		<tr>
			<td rowspan="7" align="center"><strong>n-Undecano</strong></td>
			<td align="center">1</td>	
			<td align="center"><input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly>
			</td>						
		</tr>
		<tr><td colspan="5"><hr /></td></tr>
		<tr>					
			<td align="center">2</td>	
			<td align="center"><input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly>
			</td>					
		</tr>
		<tr><td colspan="5"><hr /></td></tr>
		<tr>					
			<td align="center">3</td>	
			<td align="center"><input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly>
			</td>					
		</tr>
		<tr><td colspan="5"><hr /></td></tr>
		<tr>					
			<td align="center">4</td>	
			<td align="center"><input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly>
			</td>					
		</tr>
		<tr><td colspan="5"><hr /></td></tr>
		<tr>
			<td rowspan="7" align="center"><strong>2,6-Dimetilfenol</strong></td>
			<td align="center">1</td>
			<td align="center"><input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly>
			</td>					
		</tr>
		<tr><td colspan="5"><hr /></td></tr>
		<tr>					
			<td align="center">2</td>	
			<td align="center"><input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly>
			</td>				
		</tr>
		<tr><td colspan="5"><hr /></td></tr>
		<tr>					
			<td align="center">3</td>	
			<td align="center"><input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly>
			</td>					
		</tr>
		<tr><td colspan="5"><hr /></td></tr>
		<tr>					
			<td align="center">4</td>	
			<td align="center"><input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly>
			</td>					
		</tr>				
		<tr><td colspan="5"><hr /></td></tr>
		<tr>
			<td rowspan="7" align="center"><strong>2,6-Dimetilanilina</strong></td>
			<td align="center">1</td>	
			<td align="center"><input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly>
			</td>					
		</tr>
		<tr><td colspan="5"><hr /></td></tr>
		<tr>					
			<td align="center">2</td>	
			<td align="center"><input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly>
			</td>					
		</tr>
		<tr><td colspan="5"><hr /></td></tr>
		<tr>					
			<td align="center">3</td>	
			<td align="center"><input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly>
			</td>					
		</tr>
		<tr><td colspan="5"><hr /></td></tr>
		<tr>					
			<td align="center">4</td>	
			<td align="center"><input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x++]['tr']?>" class="monto" readonly><br />
				<input type="text" name="5tr" value="<?=$ROW[$x]['tr']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y++]['area']?>" class="monto" readonly><br />
				<input type="text" name="5area" value="<?=$ROW[$y]['area']?>" class="monto" readonly>
			</td>	
			<td align="center"><input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z++]['conc']?>" class="monto" readonly><br />
				<input type="text" name="5conc" value="<?=$ROW[$z]['conc']?>" class="monto" readonly>
			</td>					
		</tr>
		<tr><td colspan="5"><hr /></td></tr>
		<?php
	}elseif($_POST['_VER'] == '6'){
		$ROW = $Robot->Carta9OtrosGet($_POST['id']);
		?>
		<tr>
			<td><strong>Observaciones finales</strong></td>
			<td colspan="4"><textarea id="obs" cols="40" readonly><?=$ROW[0]['obs']?></textarea></td>
		</tr>
		<?php
	}
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _carta9_imprimir extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
	
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['ID']) ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('67') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function GetEncabezado(){
			$Cartor = new Cartas();
			return $Cartor->Carta9EncabezadoGet($_GET['ID']);
		}
		//
		function Estado($_var){
			$Cartor = new Cartas();
			return $Cartor->CartasEstado($_var);
		}
		
		function Accion($_var){
			$Cartor = new Cartas();
			return $Cartor->CartasAccion($_var);
		}
	
		function Historial(){
			$Cartor = new Cartas();
			return $Cartor->CartaHistorial($_GET['ID'], 1);
		}
		//
	}	
}
?>