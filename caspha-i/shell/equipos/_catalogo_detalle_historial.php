<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$ROW = $Securitor->SesionGet();
	
	$Equipor = new Equipos();
	echo $Equipor->CatalogoHistorialIME($_POST['id'],
		$_POST['accion'],
		$_POST['linea'],
		$_POST['fecha'],
		$_POST['descripcion'], 
		$_POST['usuario']		
	);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _catalogo_detalle_historial extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M') ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('61') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Equipor = new Equipos();
			if( !isset($_GET['l']) or $_GET['l']=='')
				return $Equipor->CatalogoHistorialMuestraVacio();
			else	
				return $Equipor->CatalogoHistorialMuestra($_GET['id'], $_GET['l']);
		}
		
		function ObtieneDatos2(){
			$Robot = new Seguridad();
			$lid = $this->_ROW['LID'];
			if($this->_ROW['UID'] == 28) $lid = '';
			return $Robot->UsuariosMuestra(0, $lid);
		}		
	}
}
?>