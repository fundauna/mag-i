<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['id']) ) die('-1');
	
	$ruta = "../../docs/equipos/cer/{$_POST['id']}.zip";
	$Equipor = new Equipos();
	$Equipor->EquiposCertificadosDel($_POST['equipo'], $_POST['id']);
	Bibliotecario::ZipEliminar($ruta);
	die('1');
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
}elseif( isset($_POST['id'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}

	$Equipor = new Equipos();
	//$Equipor->EquiposCertificadosDel($_POST['id']);
	
	if(isset($_POST['detalle'])){
		for($x=0;$x<count($_POST['detalle']);$x++){
			$_POST['detalle'][$x] = str_replace(' ', '_', $_POST['detalle'][$x]);
			$ruta = "../../docs/equipos/cer/{$_POST['id']}-{$_POST['detalle'][$x]}.zip";
			
			//SUBE ARCHIVOS
			if($_FILES['archivo']['size'][$x] > 0){	
				$nombre = $_FILES['archivo']['name'][$x];
				$file = $_FILES['archivo']['tmp_name'][$x];
				
				//VERIFICA QUE NO SEA ZIP
				if($_FILES['archivo']['type'][$x] != 'application/zip'){
					Bibliotecario::ZipCrear($nombre, $file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");
				}else{
					Bibliotecario::ZipSubir($file, $ruta);
				}
			}
			
			//SOLO INGRESA SI EXISTE UN CERTIFICADO
			//if(file_exists($ruta))
				$Equipor->EquiposCertificadosSet($_POST['id'], $_POST['detalle'][$x]);
		}
	}
	
	echo'<script>alert("Transacción Finalizada");window.close();</script>';
	exit();	
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _certificados extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			if(!isset($_GET['ID'])) die('Error de parámetros');
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('61') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function CertificadosMuestra(){
			$Equipor = new Equipos();
			return $Equipor->EquiposCertificadosGet($_GET['ID']);
		}
	}
}
?>