<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['equipo']) ) die('-1');
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	if(!$Securitor ->SesionAuth()) die('-0');	
	
	$Equipor = new Equipos();
	echo $Equipor->UsoIME($_POST['equipo'], $_POST['horas'], $_POST['referencia'], $_POST['accion'], $ROW['UID']);
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
}elseif( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	/*PARA TRAER LA SESION DEL LAB*/
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	$Listator = new Listador();
	$Listator->EquiposCalibrajeLista('equipos', '', $ROW['LID'], basename(__FILE__));
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _uso_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'V') ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('69') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Equipor = new Equipos();
			if( $_GET['acc']== 'I')
				return $Equipor->UsoVacio();
			else	
				return $Equipor->UsoDetalleGet($_GET['CODIGO'], $_GET['USUARIO'], $_GET['FECHA'], $_GET['ACCION']);
		}
	}
}
?>