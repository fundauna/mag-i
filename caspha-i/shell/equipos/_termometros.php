<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['codigo']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Variator = new Varios();
	echo $Variator->TermosIME($_POST['codigo'], 
		$_POST['codigocer'],
		$_POST['fecha'],
		$_POST['activo'],
		$_POST['cuarto'], 
		$_POST['limite1'], 
		$_POST['limite2'], 
		$_POST['indicacion1'], 
		$_POST['error1'], 
		$_POST['inc1'], 
		$_POST['indicacion2'], 
		$_POST['error2'], 
		$_POST['inc2'], 
		$_POST['indicacion3'], 
		$_POST['error3'], 
		$_POST['inc3'], 
		$_POST['indicacion4'], 
		$_POST['error4'], 
		$_POST['inc4'], 
		$_POST['indicacion5'], 
		$_POST['error5'], 
		$_POST['inc5'], 
		$_POST['humedad1'], 
		$_POST['humedad2'], 
		$_POST['hr1'], 
		$_POST['err_hr1'], 
		$_POST['hr2'], 
		$_POST['err_hr2'], 
		$_POST['hr3'], 
		$_POST['err_hr3'],
		$_POST['inc6'], 
		$_POST['inc7'], 
		$_POST['inc8'],  
		$_POST['tipo'], 
		$_ROW['LID'], 
		$_POST['accion']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _termometros extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			
			if(!isset($_GET['tipo'])) die('Error de parámetros');
			
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('66') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function Termos(){
			$Variator = new Varios();
			return $Variator->TermosResumenGet($this->_ROW['LID'], $_GET['tipo']);
		}
		
		function Formato($_NUMBER){
			return number_format($_NUMBER, 2, '.', ',');
		}
	}
}
?>