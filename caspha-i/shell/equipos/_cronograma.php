<?php

/* * *****************************************************************
  DECLARACION CLASE GESTORA
 * ***************************************************************** */

function __autoload($_BaseClass) {
    require_once "../../melcha-i/{$_BaseClass}.php";
}

final class _cronograma extends Mensajero {

    private $_ROW = array();
    private $Securitor = '';
    private $inicio = false;

    function __construct() {
        $this->Securitor = new Seguridad();
        if (!$this->Securitor->SesionAuth())
            $this->Err();
        $this->_ROW = $this->Securitor->SesionGet();

        /* PERMISOS */
        if ($this->_ROW['ROL'] != '0')
            $this->Portero($this->Securitor->UsuarioPermiso('64'));
        /* PERMISOS */

        if (!isset($_GET['m'])) {
            $_GET['m'] = date('n');
            $_GET['y'] = date('Y');
        }

        //SI VENIMOS DESDE TABLON, ELIMINO LA NOTIFICACION
        if (isset($_GET['ver']) && isset($_GET['tablon']))
            $this->Securitor->TablonDel($_GET['tablon']);
    }

    function Encabezado($_hoja, $_tipo, $_titulo)
    {
        $Qualitor = new Calidad();
        echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
    }

    function ObtieneDatos($mes, $a�o) {
        $Equipor = new Equipos();
        return $Equipor->CronoGet($mes, $a�o, $this->_ROW['LID']);
    }

    function ObtieneTipos($fecha) {
        $Equipor = new Equipos();
        return $Equipor->CronoGetTipos($fecha);
    }

    function Controles($mes, $a�o) {
        //Crea los controles para manejar el calendario
        $meses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre');
        $mesatras = $mes;
        $messig = $mes;
        $a�oatras = $a�o;
        $a�osig = $a�o;
        $mactual = date('n');
        $aactual = date('Y');
        echo "<form method='post'><table align='center'>";
        if ($mes > 1) {
            $mesatras -= 1;
        } else {
            $mesatras = 12;
            $a�oatras -= 1;
        }
        echo '<tr>';
        echo '<td><a href="cronograma.php?m=' . $mesatras . '&y=' . $a�oatras . '" class="control"><img src="../../caspha-i/imagenes/calendario/del_y.gif"></a></td>';
        echo '<td><h2 align="center">' . $meses[$mes - 1] . ' ' . $a�o . '</h2></td>';
        if ($mes < 12) {
            $messig += 1;
        } else {
            $messig = 1;
            $a�osig += 1;
        }
        echo '<td><a href="cronograma.php?m=' . $messig . '&y=' . $a�osig . '" class="control"><img src="../../caspha-i/imagenes/calendario/add_y.gif"></a></td>';
        echo '</tr></table></form>';
    }

    // Dibuja un calendario 
    function calendario($mes, $a�o) {
        // Dibuja la tabla
        $calendario = '<table cellpadding="0" cellspacing="0" class="calendar" align="center">';

        // Define los dias que seran encabezados 
        $dias = array('Domingo', 'Lunes', 'Martes', 'Mi�rcoles', 'Jueves', 'Viernes', 'S�bado');
        //Se dibujan los encabezados
        $calendario .= '<tr class="calendar-row"><td class="calendar-day-head">' . implode('</td><td class="calendar-day-head">', $dias) . '</td></tr>';
        // Variables
        $running_day = date('w', mktime(0, 0, 0, $mes, 1, $a�o));
        $days_in_month = date('t', mktime(0, 0, 0, $mes, 1, $a�o));
        $days_in_this_week = 1;
        $day_counter = 0;
        $dactual = date("j");
        $mactual = date("n");
        $aactual = date("Y");
        // Columnas para las semanas 	
        $calendario .= '<tr class="calendar-row">';
        // Desactiva los dias de la primera semana no incluidos en el mes
        for ($x = 0; $x < $running_day; $x++):
            $calendario .= '<td class="calendar-day-np"></td>';
            $days_in_this_week++;
        endfor;

        if ($mes < 10)
            $mes = '0' . $mes;

        // Mostrar dias  
        for ($list_day = 1; $list_day <= $days_in_month; $list_day++):
            if ($list_day < 10)
                $list_day = '0' . $list_day;

            //Valida que los dias anteriores a la fecha actual no esten disponibles para modificacion
            if ($mes >= $mactual && $a�o >= $aactual) {
                //Distingue el de hoy en otro color
                if ($list_day == $dactual && $mes == $mactual && $a�o == $aactual) {
                    $calendario .= '<td class="calendar-day" style="background-color:#B40404" onclick="ventana(' . $list_day . ',' . $mes . ',' . $a�o . ',1)">';
                } else {
                    if ($list_day < $dactual && $mes == $mactual && $a�o == $aactual) {
                        $calendario .= '<td class="calendar-day" id="' . $list_day . '-' . $mes . '-' . $a�o . '" onclick="ventana(' . $list_day . ',' . $mes . ',' . $a�o . ',0)">';
                    } else {
                        //Si no es el dia de hoy, entonces habilita el dia y lo deja en color blanco
                        $calendario .= '<td class="calendar-day" id="' . $list_day . '-' . $mes . '-' . $a�o . '" onclick="ventana(' . $list_day . ',' . $mes . ',' . $a�o . ',1)">';
                    }
                }
            } else {
                $calendario .= '<td class="calendar-day" id="' . $list_day . '-' . $mes . '-' . $a�o . '" onclick="ventana(' . $list_day . ',' . $mes . ',' . $a�o . ',0)">';
            }

            // Agregar numero a los dias      
            $calendario .= '<div class="day-number">' . $list_day . '</div>';
            // Se asigna un espacio dentro de los cuadros de dias para engrosar las casillas
            $calendario .= '<p id="color' . $list_day . '-' . $mes . '-' . $a�o . '">&nbsp;</p>';
            // Pregunta a la base de datos si el dia esta disponible  
            $calendario .= '</td>';

            if ($running_day == 6):
                $calendario .= '</tr>';
                if (($day_counter + 1) != $days_in_month):
                    $calendario .= '<tr class="calendar-row">';
                endif;

                $running_day = -1;
                $days_in_this_week = 0;
            endif;

            $days_in_this_week++;
            $running_day++;
            $day_counter++;
        endfor;

        // Desactiva los dias de la �ltima semana no incluidos en el mes 
        if ($days_in_this_week < 8):
            for ($x = 1; $x <= (8 - $days_in_this_week); $x++):
                $calendario .= '<td class="calendar-day-np"></td>';
            endfor;
        endif;

        // Final de las Columnas 
        $calendario .= '</tr>';

        // Fin de la tabla 
        $calendario .= '</table>';

        // Listo, retorna el resultado 
        echo $calendario;

        //Se�ala los d�as que est�n ocupados ... Excepto el d�a actual que se se�ala diferente
        $Gestor = new _cronograma();
        $ROW = $Gestor->ObtieneDatos($mes, $a�o);
        if ($ROW) {
            for ($x = 0; $x < count($ROW); $x++) {
                $TOKEN = $Gestor->ObtieneTipos($ROW[$x]['fecha']);
                if (count($TOKEN) == 1)
                    echo "<script>colorea('{$ROW[$x]['fecha']}', '{$TOKEN[0]['tipo']}','','');</script>";
                if (count($TOKEN) == 2)
                    echo "<script>colorea('{$ROW[$x]['fecha']}', '{$TOKEN[0]['tipo']}', '{$TOKEN[1]['tipo']}','');</script>";
                if (count($TOKEN) == 3)
                    echo "<script>colorea('{$ROW[$x]['fecha']}', '{$TOKEN[0]['tipo']}', '{$TOKEN[1]['tipo']}', '{$TOKEN[2]['tipo']}');</script>";
            }
        }
    }

}

?>