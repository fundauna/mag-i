<?php

/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 * ***************************************************************** */
if (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    if (!isset($_POST['cs'])) {
        die('-1');
    }

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth()) {
        die('-0');
    }
    $_ROW = $Securitor->SesionGet();

    $Equipor = new Equipos();
    echo $Equipor->CronoIME($_POST['cs'], $_POST['fecha'], $_POST['nueva'], $_POST['final'], $_POST['aviso'], $_POST['equipo'], $_POST['responsable'], $_POST['tipo'], $_POST['deshabilita'], $_POST['observaciones'], $_POST['accion'], $_ROW['LID']);
    exit;
    /*     * *****************************************************************
      PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
     * ***************************************************************** */
} elseif (isset($_GET['list'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    $Securitor = new Seguridad();
    $ROW = $Securitor->SesionGet();
    $Listator = new Listador();
    if ($_GET['list'] == '1') {
        $Listator->EquiposLista('equipos', '', $ROW['LID'], basename(__FILE__), 1);
    }
    if ($_GET['list'] == '2') {
        $Listator->UsuariosLista('equipos', basename(__FILE__), '', $ROW['LID']);
    }
    exit;
} else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _cronograma_detalle extends Mensajero {

        private $_ROW = array();
        private $Securitor = '';

        function __construct() {
            //SOLO SE PUEDE ABRIR COMO POP-UP MODAL
            $this->ValidaModal();

            if (!isset($_GET['d']))
                die('Error de parámetros');
            //FORMATEA FECHA REAL DEL DIA
            if ($_GET['d'] < 10) {
                $dia = '0' . $_GET['d'];
            } else {
                $dia = $_GET['d'];
            }
            if ($_GET['m'] < 10) {
                $mes = '0' . $_GET['m'];
            } else {
                $mes = $_GET['m'];
            }
            $_POST['dia'] = $dia . '-' . $mes . '-' . $_GET['a'];
            //
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth()) {
                $this->Err();
            }
            $this->_ROW = $this->Securitor->SesionGet();
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0') {
                $this->Portero($this->Securitor->UsuarioPermiso('64'));
            }
            /* PERMISOS */
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function ObtieneDatos() {
            $Equipor = new Equipos();
            return $Equipor->CronoDetalleGet($_GET['d'] < 10 ? '0' . $_GET['d'] : '', $_GET['m'] < 10 ? '0' . $_GET['m'] : $_GET['m'], $_GET['a']);
        }

        function ObtieneEquipos($_cs) {
            $Equipor = new Equipos();
            return $Equipor->CronoDetalleGetEquipos($_cs);
        }

        function Obtiene($_id) {
            $Equipor = new Seguridad();
            $v = $Equipor->UsuarioDatosGet2($_id);
            return $v[0]['nombre'] . ' ' . $v[0]['ap1'] . ' ' . $v[0]['ap2'];
        }

        function Color($_tipo) {
            if ($_tipo == '0') {
                return '0099FF';
            } elseif ($_tipo == '1') {
                return '999999';
            } elseif ($_tipo == '2') {
                return 'FF9900';
            }
        }

    }

}
?>