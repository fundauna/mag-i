<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['id']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	
	$Equipor = new Equipos();
	echo $Equipor->EquiposIME($_POST['id'], $_POST['accion'], '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', $_POST['estado'], '', '', '', '', '', '', '', '');
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _catalogo extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			if(!isset($_POST['nombre'])){
				$_POST['codigo'] = $_POST['nombre'] = '';
				$_POST['lolo'] = '1';
				$this->inicio = true;
			}
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('61') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function EquiposMuestra(){
			$Equipor = new Equipos();
			if($this->inicio)
				return $Equipor->EquiposAlertaGet($this->_ROW['LID']);
			else
				return $Equipor->EquiposResumenGet($_POST['codigo'], $_POST['nombre'], $this->_ROW['LID'], $_POST['lolo']);		
		}
		
		function Estado($_ESTADO){
			if($_ESTADO == '0') return 'Deshabilitado';
			elseif($_ESTADO == '1') return 'Habilitado';
			elseif($_ESTADO == '2') return 'Almacenado';
		}
	}
}
?>