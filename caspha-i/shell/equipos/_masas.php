<?php

/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 * ***************************************************************** */
if (isset($_POST['accion'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    if (!isset($_POST['codigo']))
        die('-1');

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth())
        die('-0');
    $_ROW = $Securitor->SesionGet();

    $Variator = new Varios();
    if ($Variator->MasasIME($_POST['ocodigo'], $_POST['onominal'], $_POST['codigo'], $_POST['codigocer'], $_POST['fecha'], $_POST['nominal'], $_POST['real'], $_POST['inc'], $_POST['certificado'], $_POST['activo'], $_ROW['LID'], $_POST['accion'])) {
        ?>
        <script>
            window.location = '../../../baltha-i/equipos/masas.php';
        </script>
        <?php
    }
    exit;
} else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _masas extends Mensajero {

        private $_ROW = array();
        private $Securitor = '';

        function __construct() {
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth())
                $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0')
                $this->Portero($this->Securitor->UsuarioPermiso('66'));
            /* PERMISOS */
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function Masas() {
            $Variator = new Varios();
            return $Variator->MasasResumenGet($this->_ROW['LID']);
        }

        function Formato($_NUMBER) {
            return number_format($_NUMBER, 2, '.', ',');
        }

    }

}
?>