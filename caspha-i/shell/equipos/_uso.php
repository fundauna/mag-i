<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	/*PARA TRAER LA SESION DEL LAB*/
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	$Listator = new Listador();
	$Listator->EquiposCalibrajeLista('equipos', '', $ROW['LID'], basename(__FILE__),1);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _uso extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			if(!isset($_POST['equipo'])){
				$_POST['tmp_equipo'] = $_POST['equipo'] = $_POST['desde'] = $_POST['hasta'] = '';
				$this->inicio = true;
			}
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('69') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function UsoMuestra(){
			if($this->inicio) return array();
			else{
				$Equipor = new Equipos();
				return $Equipor->UsoResumenGet($_POST['equipo'], $this->_ROW['LID'], $_POST['desde'], $_POST['hasta']);
			}
		}
	}
}
?>