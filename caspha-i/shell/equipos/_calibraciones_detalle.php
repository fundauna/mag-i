<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['id']) ) die('-1');

	Bibliotecario::ZipEliminar("../../docs/{$_POST['modulo']}/{$_POST['id']}.zip");
	die('1');
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
}elseif( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	/*PARA TRAER LA SESION DEL LAB*/
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	$Listator->EquiposCalibrajeLista('equipos', '', $ROW['LID'], basename(__FILE__));
	exit;
}elseif( isset($_GET['list2'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	$Listator->UsuariosLista('equipos', basename(__FILE__), '', $ROW['LID']);
	exit;
}elseif( isset($_GET['list3'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	$Listator = new Listador();
	$Listator->ProveedoresLista('equipos', $ROW['LID'], basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
}elseif( isset($_POST['id'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	
	$Equipor = new Equipos();
	
	if($_POST['accion'] == 'I'){
		//CREAMOS EL CONSECUTIVO
		$_POST['id'] = $Equipor->CalisConsecutivoGet();
	}
	
	$x = $Equipor->CalisIME($_POST['id'],
		$_POST['maestro'], 
		$_POST['codigo'], 
		$_POST['equipo'],
		$_POST['fecha'],
		$_POST['prov'],
		$_POST['tolerancia'],
		$_POST['tolerancia2'],
		$_POST['conforme'],
		$_POST['justificacion'],
		$_POST['usuario'],
		$_POST['obs'],
		$_POST['accion']
	);
	
	if($x){
		if( isset($_FILES['archivo']) ){
			if($_FILES['archivo']['size'] > 0){
				$nombre = $_FILES['archivo']['name'];
				$file = $_FILES['archivo']['tmp_name'];
				$ruta = "../../docs/equipos/cal/{$_POST['id']}.zip";
				Bibliotecario::ZipCrear($nombre, $file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");	
			}
		}
		echo '<script>alert("Transaccion finalizada");window.close();</script>';
	}else
		echo '<script>alert("Error al ingresar los datos");window.close();</script>';
	
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _calibraciones_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M') ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('62') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Equipor = new Equipos();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Equipor->CalisVacio();
			else	
				return $Equipor->CalisDetalleGet($_GET['ID']);
		}
		
		function Formato($_NUMBER){
			return number_format($_NUMBER, 4, '.', ',');
		}
	}
}
?>