<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	$Listator = new Listador();
	$Listator->ProveedoresLista('equipos', $ROW['LID'], basename(__FILE__));
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
}elseif( isset($_POST['_historial'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	
	$Equipor = new Equipos();
	echo $Equipor->EquiposProveedoresHistorial($_POST['id']);
	exit;
}elseif( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$ROW = $Securitor->SesionGet();
	
	$Equipor = new Equipos();
	
	if($_POST['_AJAX'] == '2'){
		echo $Equipor->CatalogoHistorialIME($_POST['id'], 'E', $_POST['linea'], '', '', '');
	}elseif($_POST['_AJAX'] == '3'){
		echo $Equipor->EquiposComponenteElimina($_POST['id']);
	}else{
	
		echo $Equipor->EquiposIME($_POST['id'],
			$_POST['accion'],
			$_POST['codigo'],
			$_POST['nombre'], 
			$_POST['patrimonio'],
			$_POST['marca'],
			$_POST['modelo'], 
			$_POST['serie'], 
			$_POST['ubi_fabricante'],
			$_POST['fecha'],
			$_POST['temp1'],
			$_POST['temp2'],
			$_POST['hume1'],
			$_POST['hume2'],
			$_POST['ubi_equipo'],
			$_POST['garantia'],
			$_POST['proveedor'],
			$_POST['control_metro'],
			$_POST['tipo_cali'],
			$_POST['tipo_veri'],
			$_POST['tipo_mant'],
			$_POST['otro'],
			$_POST['unidad'],
			$_POST['rango1'],
			$_POST['rango2'],
			$_POST['resolucion'],
			$_POST['error'],
			$_POST['estado'],
			$_POST['justi'],
			$ROW['LID'],
			//
			$_POST['c_codigo'],
			$_POST['c_nombre'],
			$_POST['c_patrimonio'],
			$_POST['c_marca'],
			$_POST['c_modelo'],
			$_POST['c_serie']		
		);
	}
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _catalogo_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M') ) die('Error de parámetros');
			if ( !isset($_GET['ID'])) $_GET['ID']='';
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('61') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Equipor = new Equipos();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Equipor->EquiposVacio();
			else	
				return $Equipor->EquiposDetalleGet($_GET['ID']);
		}
		
		function ComponentesMuestra(){
			$Equipor = new Equipos();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Equipor->EquiposComponentesVacio();
			else	
				return $Equipor->EquiposComponentesGet($_GET['ID']);
		}
		
		function CatalogoHistorialMuestra(){
			$Equipor = new Equipos();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Equipor->CatalogoHistorialMuestraVacio();
			else	
				return $Equipor->CatalogoHistorialMuestra($_GET['ID']);
		}
		
		function ErroresMuestra(){
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return array();
			else{
				$Equipor = new Equipos();
				return $Equipor->EquiposErroresGet($_GET['ID']);
			}
		}
		
		function RangosMuestra(){
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return array();
			else{
				$Equipor = new Equipos();
				return $Equipor->EquiposRangosGet($_GET['ID']);
			}
		}
		
		function Formato($_NUMBER){
			return number_format($_NUMBER, 2, '.', ',');
		}
	}
}
?>