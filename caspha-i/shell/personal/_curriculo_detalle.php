<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Personitor = new Personal();

	$Personitor->InfoUpdate($_POST['id']);
			
	if($_POST['paso'] == '1'){ 
		if( !isset($_POST['nombre']) ) die('-1');
			
		echo $Personitor->InfoLaboralSet($_POST['id'],
			$_POST['cedula'], 
			$_POST['nombre'], 
			$_POST['ap1'], 
			$_POST['ap2'], 
			$_POST['email'], 
			$_POST['fnacimiento'], 
			$_POST['genero'], 
			$_POST['nacionalidad'], 
			$_POST['domicilio'], 
			$_POST['tel_hab'], 
			$_POST['tel_cel'], 
			$_POST['fingreso'], 
			$_POST['profesion'], 
			$_POST['cargo'], 
			$_POST['clase_puesto'], 
			$_POST['num_puesto'], 
			$_POST['colegio'], 
			$_POST['num_colegiado']
		);
	}elseif($_POST['paso'] == '2'){ //ok
		$Personitor->InfoFormacionDel($_POST['id']);
		$_POST['titulo'] = explode(';',$_POST['titulo']);
		$_POST['institucion'] = explode(';',$_POST['institucion']);
		$_POST['ano'] = explode(';',$_POST['ano']);
		
		for($i=0;$i<count($_POST['titulo']);$i++){
			$Personitor->InfoFormacionSet($_POST['id'], $i,
				$_POST['titulo'][$i],
				$_POST['institucion'][$i],
				$_POST['ano'][$i]
			);
		}
		echo 1;
	}elseif($_POST['paso'] == '3'){
		$Personitor->InfoIdiomasDel($_POST['id']);
		$_POST['idioma'] = explode(';',$_POST['idioma']);
		$_POST['lee'] = explode(';',$_POST['lee']);
		$_POST['habla'] = explode(';',$_POST['habla']);
		$_POST['escribe'] = explode(';',$_POST['escribe']);
		for($i=0;$i<count($_POST['idioma']);$i++){
			$Personitor->InfoIdiomasSet($_POST['id'], $i,
				$_POST['idioma'][$i],
				$_POST['lee'][$i],
				$_POST['habla'][$i],
				$_POST['escribe'][$i]
			);
		}
		echo 1;
	}elseif($_POST['paso'] == '4'){
		$Personitor->InfoPublicacionesDel($_POST['id']);
		$_POST['publicacion'] = explode(';',$_POST['publicacion']);
		$_POST['entidad'] = explode(';',$_POST['entidad']);
		$_POST['ano'] = explode(';',$_POST['ano']);
		for($i=0;$i<count($_POST['publicacion']);$i++){
			$Personitor->InfoPublicacionesSet($_POST['id'], $i,
				$_POST['publicacion'][$i],
				$_POST['entidad'][$i],
				$_POST['ano'][$i]
			);
		}
		echo 1;
	}elseif($_POST['paso'] == '51'){
		$Personitor->InfoExperienciaDel($_POST['id']);
		$_POST['empresa'] = explode(';',$_POST['empresa']);
		$_POST['periodo'] = explode(';',$_POST['periodo']);
		for($i=0;$i<count($_POST['empresa']);$i++){
			$Personitor->InfoExperienciaSet($_POST['id'], $i,
				$_POST['empresa'][$i],
				$_POST['periodo'][$i]
			);
		}
		echo 1;
	
	}elseif($_POST['paso'] == '52'){
		$Personitor->InfoAnalisisDel($_POST['id']);
		$_POST['tecnica'] = explode(';',$_POST['tecnica']);
		$_POST['empresa'] = explode(';',$_POST['empresa']);
		$_POST['periodo'] = explode(';',$_POST['periodo']);
		for($i=0;$i<count($_POST['tecnica']);$i++){
			$Personitor->InfoAnalisisSet($_POST['id'], $i,
				$_POST['tecnica'][$i],
				$_POST['empresa'][$i],
				$_POST['periodo'][$i]
			);
		}
		echo 1;
	}elseif($_POST['paso'] == '53'){
		$Personitor->InfoEquiposDel($_POST['id']);
		$_POST['equipos'] = explode(';',$_POST['equipos']);
		$_POST['empresa'] = explode(';',$_POST['empresa']);
		$_POST['periodo'] = explode(';',$_POST['periodo']);
		for($i=0;$i<count($_POST['equipos']);$i++){
			$Personitor->InfoEquiposSet($_POST['id'], $i,
				$_POST['equipos'][$i],
				$_POST['empresa'][$i],
				$_POST['periodo'][$i]
			);
		}
		echo 1;
	}elseif($_POST['paso'] == '54'){
		$Personitor->InfoCalidadDel($_POST['id']);
		$_POST['actividad'] = explode(';',$_POST['actividad']);
		$_POST['empresa'] = explode(';',$_POST['empresa']);
		$_POST['periodo'] = explode(';',$_POST['periodo']);
		for($i=0;$i<count($_POST['actividad']);$i++){
			$Personitor->InfoCalidadSet($_POST['id'], $i,
				$_POST['actividad'][$i],
				$_POST['empresa'][$i],
				$_POST['periodo'][$i]
			);
		}
		echo 1;
	}elseif($_POST['paso'] == '55'){
		$Personitor->InfoOtrosDel($_POST['id']);
		$_POST['actividad'] = explode(';',$_POST['actividad']);
		$_POST['empresa'] = explode(';',$_POST['empresa']);
		$_POST['periodo'] = explode(';',$_POST['periodo']);
		for($i=0;$i<count($_POST['actividad']);$i++){
			$Personitor->InfoOtrosSet($_POST['id'], $i,
				$_POST['actividad'][$i],
				$_POST['empresa'][$i],
				$_POST['periodo'][$i]
			);
		}
		echo 1;
	}elseif($_POST['paso'] == '6'){
		$Personitor->InfoCursosDel($_POST['id']);
		$_POST['tipo'] = explode(';',$_POST['tipo']);
		$_POST['nombre'] = explode(';',$_POST['nombre']);
		$_POST['empresa'] = explode(';',$_POST['empresa']);
		$_POST['duracion'] = explode(';',$_POST['duracion']);
		$_POST['fecha'] = explode(';',$_POST['fecha']);
		for($i=0;$i<count($_POST['tipo']);$i++){
			$Personitor->InfoCursosSet($_POST['id'], $i,
				$_POST['tipo'][$i],
				$_POST['nombre'][$i],
				$_POST['empresa'][$i],
				$_POST['duracion'][$i],
				$_POST['fecha'][$i]
			);
		}
		echo 1;
	}
	exit;
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO LECTURA
*******************************************************************/
}elseif( isset($_POST['_VER'])){
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}

	$Robot = new Personal();
	
	if($_POST['_VER'] == '2'){
		$ROW = $Robot->InfoFormacionGet($_POST['id']);
		$cont = count($ROW);
		for($x=0;$x<$cont;$x++){
		?>
			<tr>
				<td><input type="text" id="2titulo<?=$x?>" name="2titulo" value="<?=$ROW[$x]['titulo']?>" size="50" maxlength="100"></td>
				<td><input type="text" id="2institucion<?=$x?>" name="2institucion" value="<?=$ROW[$x]['institucion']?>" size="40" maxlength="100"></td>
				<td><input type="text" id="2ano<?=$x?>" name="2ano" value="<?=$ROW[$x]['ano']?>" maxlength="4" class="ano"></td>
			</tr>
		<?php
		}
	}elseif($_POST['_VER'] == '3'){
		$ROW = $Robot->InfoIdiomasGet($_POST['id']);
		$cont = count($ROW);
		for($x=0;$x<$cont;$x++){
		?>
			<tr>
				<td><select id="3idioma<?=$x?>" name="3idioma">
					<option value="">N/A</option>
					<option value="I" <?php if($ROW[$x]['idioma'] == 'I') echo 'selected';?>>Ingl&eacute;s</option>
					<option value="F" <?php if($ROW[$x]['idioma'] == 'F') echo 'selected';?>>Franc&eacute;s</option>
					<option value="A" <?php if($ROW[$x]['idioma'] == 'A') echo 'selected';?>>Alem&aacute;n</option>
					<option value="P" <?php if($ROW[$x]['idioma'] == 'P') echo 'selected';?>>Portugu&eacute;s</option>
					<option value="J" <?php if($ROW[$x]['idioma'] == 'J') echo 'selected';?>>Japon&eacute;s</option>
					<option value="S" <?php if($ROW[$x]['idioma'] == 'S') echo 'selected';?>>Sueco</option>
					<option value="C" <?php if($ROW[$x]['idioma'] == 'C') echo 'selected';?>>Canton&eacute;s</option>
					<option value="M" <?php if($ROW[$x]['idioma'] == 'M') echo 'selected';?>>Mandar&iacute;n</option>
					<option value="H" <?php if($ROW[$x]['idioma'] == 'H') echo 'selected';?>>Holand&eacute;s</option>
					<option value="R" <?php if($ROW[$x]['idioma'] == 'R') echo 'selected';?>>Ruso</option>
					</select>
				</td>
				<td><input id="3lee<?=$x?>" name="3lee" size="3" maxlength="3" onblur="_INT(this)" value="<?=$ROW[$x]['lee']?>" /></td>
				<td><input id="3habla<?=$x?>" name="3habla" size="3" maxlength="3" onblur="_INT(this)" value="<?=$ROW[$x]['habla']?>" /></td>
				<td><input id="3escribe<?=$x?>" name="3escribe" size="3" maxlength="3" onblur="_INT(this)" value="<?=$ROW[$x]['escribe']?>" /></td>
			</tr>
		<?php
		}
	}elseif($_POST['_VER'] == '4'){
		$ROW = $Robot->InfoPublicacionesGet($_POST['id']);
		$cont = count($ROW);
		for($x=0;$x<$cont;$x++){
		?>
			<tr>
				<td><input type="text" id="4publicacion<?=$x?>" name="4publicacion" value="<?=$ROW[$x]['publicacion']?>" size="50" maxlength="100"></td>
				<td><input type="text" id="4entidad<?=$x?>" name="4entidad" value="<?=$ROW[$x]['entidad']?>" size="40" maxlength="50"></td>
				<td><input type="text" id="4ano<?=$x?>" name="4ano" value="<?=$ROW[$x]['ano']?>" size="4" maxlength="4"></td>
			</tr>
		<?php
		}
	}elseif($_POST['_VER'] == '51'){
		$ROW = $Robot->InfoExperienciaGet($_POST['id']);
		$cont = count($ROW);
		for($x=0;$x<$cont;$x++){
		?>
			<tr>
				<td><input type="text" id="51empresa<?=$x?>" name="51empresa" value="<?=$ROW[$x]['empresa']?>" size="50" maxlength="50"></td>
				<td><input type="text" id="51periodo<?=$x?>" name="51periodo" value="<?=$ROW[$x]['periodo']?>" size="9" maxlength="9" title="eje: 1990-2005"></td>
			</tr>
		<?php
		}
	}elseif($_POST['_VER'] == '52'){
		$ROW = $Robot->InfoAnalisisGet($_POST['id']);
		$cont = count($ROW);
		for($x=0;$x<$cont;$x++){
		?>
			<tr>
				<td><input type="text" id="52tecnica<?=$x?>" name="52tecnica" value="<?=$ROW[$x]['tecnica']?>" size="50" maxlength="100"></td>
				<td><input type="text" id="52empresa<?=$x?>" name="52empresa" value="<?=$ROW[$x]['empresa']?>" size="40" maxlength="50"></td>
				<td><input type="text" id="52periodo<?=$x?>" name="52periodo" value="<?=$ROW[$x]['periodo']?>" size="7" maxlength="9" title="eje: 1990-2005"></td>
			</tr>
		<?php
		}
	}elseif($_POST['_VER'] == '53'){
		$ROW = $Robot->InfoEquiposGet($_POST['id']);
		$cont = count($ROW);
		for($x=0;$x<$cont;$x++){
		?>
			<tr>
				<td><input type="text" id="53equipos<?=$x?>" name="53equipos" value="<?=$ROW[$x]['equipos']?>" size="50" maxlength="100"></td>
				<td><input type="text" id="53empresa<?=$x?>" name="53empresa" value="<?=$ROW[$x]['empresa']?>" size="40" maxlength="50"></td>
				<td><input type="text" id="53periodo<?=$x?>" name="53periodo" value="<?=$ROW[$x]['periodo']?>" size="7" maxlength="9" title="eje: 1990-2005"></td>
			</tr>
		<?php
		}
	}elseif($_POST['_VER'] == '54'){
		$ROW = $Robot->InfoCalidadGet($_POST['id']);
		$cont = count($ROW);
		for($x=0;$x<$cont;$x++){
		?>
			<tr>
				<td><input type="text" id="54actividad<?=$x?>" name="54actividad" value="<?=$ROW[$x]['actividad']?>" size="50" maxlength="200"></td>
				<td><input type="text" id="54empresa<?=$x?>" name="54empresa" value="<?=$ROW[$x]['empresa']?>" size="40" maxlength="50"></td>
				<td><input type="text" id="54periodo<?=$x?>" name="54periodo" value="<?=$ROW[$x]['periodo']?>" size="7" maxlength="9" title="eje: 1990-2005"></td>
			</tr>
		<?php
		}
	}elseif($_POST['_VER'] == '55'){
		$ROW = $Robot->InfoOtrosGet($_POST['id']);
		$cont = count($ROW);
		for($x=0;$x<$cont;$x++){
		?>
			<tr>
				<td><input type="text" id="55actividad<?=$x?>" name="55actividad" value="<?=$ROW[$x]['actividad']?>" size="50" maxlength="200"></td>
				<td><input type="text" id="55empresa<?=$x?>" name="55empresa" value="<?=$ROW[$x]['empresa']?>" size="40" maxlength="50"></td>
				<td><input type="text" id="55periodo<?=$x?>" name="55periodo" value="<?=$ROW[$x]['periodo']?>" size="7" maxlength="9" title="eje: 1990-2005"></td>
			</tr>
		<?php
		}
	}elseif($_POST['_VER'] == '6'){
		$ROW = $Robot->InfoCursosGet($_POST['id']);
		$cont = count($ROW);
		for($x=0;$x<$cont;$x++){
		?>
			<tr>
				<td>
				<?php
				$ruta = "../../../caspha-i/docs/personal/cur_{$_POST['id']}_{$x}.zip";
				if(file_exists($ruta)){
				?>
					<img src="../../caspha-i/imagenes/bajar.png" title="Descargar" onclick="DocumentosZip('cur_<?=$_POST['id']?>_<?=$x?>', 'personal')" class="tab2"/>
				<?php
				}else{
				?>
					<img src="../../caspha-i/imagenes/bajar2.png" title="No disponible" class="tab2"/>
				<?php
				}
				?>
				</td>
				<td><img src="../../caspha-i/imagenes/subir.png" title="Subir" onclick="DocumentosSubir('<?=$_POST['id']?>','<?=$x?>')" class="tab2"/></td>
				<td><input type="text" id="6tipo<?=$x?>" name="6tipo" value="<?=$ROW[$x]['tipo']?>" size="20" maxlength="20"></td>
				<td><input type="text" id="6nombre<?=$x?>" name="6nombre" value="<?=$ROW[$x]['nombre']?>" size="50" maxlength="200"></td>
				<td><input type="text" id="6empresa<?=$x?>" name="6empresa" value="<?=$ROW[$x]['empresa']?>" size="30" maxlength="50"></td>
				<td><input type="text" id="6duracion<?=$x?>" name="6duracion" value="<?=$ROW[$x]['duracion']?>"></td>
                                <td><input type="text" id="6fecha<?=$x?>" name="6fecha" placeholder="MM/YYYY" value="<?=$ROW[$x]['fecha']?>" ></td>
			</tr>
		<?php
		}
	}
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _curriculo_detalle extends Mensajero{
		private $yo = false; //PARA SABER SI SOY YO EL DE LA HOJA DE VIDA
	
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
		
			if( !isset($_GET['ID']) ) die('Error de parámetros');
			
			$Securitor = new Seguridad();
			if(!$Securitor->SesionAuth()) $this->Err();
			$_ROW = $Securitor->SesionGet();
			/*PERMISOS*/
			if($_GET['ID'] == $_ROW['UID']) $this->yo = true;
			elseif($_ROW['ROL'] != '0') $this->Portero( $Securitor->UsuarioPermiso('42') );
			//SI NO SOY YO, VALIDO QUE POR LO MENOS PUEDA ACTUALIZAR LA SECCION DE CURSOS DE LOS DEMAS
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function InfoLaboral(){
			$Personitor = new Personal();
			return $Personitor->InfoLaboralGet($_GET['ID']);
		}
		
		function InfoPersonal(){
			$Personitor = new Personal();
			return $Personitor->InfoPersonalGet($_GET['ID']);
		}
		
		function SoyYo(){
			if(!$this->yo) echo "<script>SecccionesBloquea();</script>";
		}
	}
	
}
?>