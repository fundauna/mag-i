<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['tmp'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	
	$Personitor = new Personal();
	
	echo $Personitor->CitasDetalleSet($_POST['id'],
		$_POST['usuario'], 
		$_POST['fecha'],
		$_POST['lugar'],
		$_POST['comprobante'], 
		$_POST['obs'], 
		$_POST['accion']
	);
	
	if($_FILES['archivo']['size'] > 0){
		$file = $_FILES['archivo']['tmp_name'];
		$zip= new ZipArchive();
                $_POST['accion']=='I'?$_POST['id']=$Personitor->CitasDetalleUltimoId()[0]['id']:'';
		$_POST['id'] = str_replace(' ','',$_POST['id']);
		if(($zip->open("../../docs/citas/{$_POST['id']}.zip", ZipArchive::CREATE))!==true){ die('Error: No es posible crear el archivo zip');}
		$zip->addFile($file, $_FILES['archivo']['name']);
		$zip->close();		
	}
	echo '<script>window.close();</script>';
	exit;
}elseif( isset($_GET['list'])){ 
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	$Listator->UsuariosLista('personal', basename(__FILE__), '', $ROW['LID']);
	exit;
}elseif(isset ($_POST['_AJAX'])){
   echo unlink("../../docs/citas/{$_POST['id']}.zip");
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _citas_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M') ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('42') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

		function ObtieneDatos(){
			$Personitor = new Personal();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Personitor->CitasVacio();
			else	
				return $Personitor->CitasDetalleGet($_GET['ID']);
		}
	}
}
?>