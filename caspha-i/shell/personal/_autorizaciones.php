<?php
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

final class _autorizaciones extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			if(!isset($_GET['tipo'])) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('43') );
			/*PERMISOS*/
		}

    function Encabezado($_hoja, $_tipo, $_titulo)
    {
        $Qualitor = new Calidad();
        echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
    }
		
		function Estado($_estado){
			return $this->Securitor->UsuariosEstado($_estado);
		}
		
		function UsuariosMuestra(){
			$privilegio = $this->Securitor->UsuarioPermiso('43');
			//TIENE PERMISOS DE ESCRITURA PARA VER TODOS LOS USUARIOS
			if($privilegio == 'E' or $this->_ROW['ROL'] == '0')
				return $this->Securitor->UsuariosMuestra(0, $this->_ROW['LID']);
			else
				return $this->Securitor->UsuariosYo($this->_ROW['UID']);
		}
	}
?>