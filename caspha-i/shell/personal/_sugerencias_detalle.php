<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	
	$Personitor = new Personal();
	
	$USR = $Securitor->SesionGet();
	
	echo $Personitor->SugerenciasDetalleSet($_POST['id'],
		$USR['UID'],
		$_POST['obs'],
		$_POST['personal'],
		$_POST['materiales'],
		$_POST['equipos'],
		$_POST['presupuesto'],
		$_POST['otros'],
		$_POST['costo'],
		$_POST['beneficio'],
		$_POST['propuesta'],
		$_POST['accion']
	);
	exit;
}elseif( isset($_GET['list'])){ 
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	
	$Listator = new Listador();
	$Listator->UsuariosLista('personal', basename(__FILE__), '', $ROW['LID']);
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _sugerencias_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M') ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('42') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Personitor = new Personal();
			if( !isset($_GET['ID']) or $_GET['ID']=='')
				return $Personitor->SugerenciasVacio();
			else	
				return $Personitor->SugerenciasDetalleGet($_GET['ID']);
		}
	}
}
?>