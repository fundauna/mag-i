<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['cs']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');	
	
	$Personitor = new Personal();
	echo $Personitor->AutorizacionesDel($_POST['cs']);
	exit;
}elseif( isset($_POST['persona'])){
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['persona']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');	
	
	$Personitor = new Personal();
	
	$cs = $Personitor->AutorizacionesCs();
	
	if($Personitor->AutorizacionesSet($cs, $_POST['persona'], $_POST['codigo'], $_POST['fecha'], $_POST['tipo'])==1){		
		//SUBIR ARCHIVO			
		if($_FILES['archivo']['size'] > 0){
			$nombre = $_FILES['archivo']['name'];
			$file = $_FILES['archivo']['tmp_name'];
			$ruta = "../../docs/personal/ind_{$cs}.zip";
			Bibliotecario::ZipCrear($nombre, $file, $ruta) or die("Error: No es posible crear el archivo {$nombre}");
		}
		//SUBIR ARCHIVO			
		echo'<script>alert("Transaccion finalizada");window.close();</script>';
		exit();
	}else
		echo 'Error al guardar los datos';
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _autorizaciones_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['ID']) ) die('Error de par�metros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('43') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function Escritura(){
			$privilegio = $this->Securitor->UsuarioPermiso('43');
			//TIENE PERMISOS DE ESCRITURA PARA VER TODOS LOS USUARIOS
			if($privilegio == 'E' or $this->_ROW['ROL'] == '0') return true;
			else false;
		}
		
		function ObtieneDatos(){
			$Personitor = new Personal();
			return $Personitor->AutorizacionesGet($_GET['ID']);
		}
		
		function Tipo($_valor){
			if($_valor == '0') return  'Inducci�n general';
			elseif($_valor == '1') return  'Inducci�n t�cnica';
			elseif($_valor == '2') return  'Autorizaci�n';
		}
	}
}
?>