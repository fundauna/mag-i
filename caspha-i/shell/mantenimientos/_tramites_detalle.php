<?php

/* * *****************************************************************
  PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
 * ***************************************************************** */
if (isset($_POST['_AJAX'])) {

    function __autoload($_BaseClass) {
        require_once "../../../melcha-i/{$_BaseClass}.php";
    }

    if (!isset($_POST['cs'])) {
        die('-1');
    }

    $Securitor = new Seguridad();
    if (!$Securitor->SesionAuth()) {
        die('-0');
    }
    $_ROW = $Securitor->SesionGet();

    $Mantenitor = new Mantenimientos();
    if ($_POST['accion'] == 'I') {
        $_POST['cs'] = $Mantenitor->obtieneConsecutivo('tramites', 'MAG000');
    }
    if ($Mantenitor->TramitesIME($_POST['cs'], $_POST['accion'], $_POST['tipo'], $_POST['descripcion'], $_POST['requisitos'], $_POST['fecha'], $_POST['aviso'], $_POST['vigencia'], $_ROW['LID'])) {
        if ($_FILES['archivo']['size'] > 0) {
            $nombre = $_FILES['archivo']['name'];
            $file = $_FILES['archivo']['tmp_name'];
            $ruta = "../../docs/mantenimientos/tramites/{$_POST['cs']}.zip";
            Bibliotecario::ZipCrear($nombre, $file, $ruta, 1) or die("Error: No es posible crear el archivo {$nombre}");
        }
        echo 1;
    }
    exit;
} else {
    /*     * *****************************************************************
      DECLARACION CLASE GESTORA
     * ***************************************************************** */

    function __autoload($_BaseClass) {
        require_once "../../melcha-i/{$_BaseClass}.php";
    }

    final class _tramites_detalle extends Mensajero {

        private $_ROW = array();
        private $Securitor = '';

        function __construct() {
            //SOLO SE PUEDE ABRIR COMO POP-UP MODAL
            $this->ValidaModal();

            if (!isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M'))
                die('Error de parámetros');
            if (!isset($_GET['tipo']))
                $_GET['tipo'] = '';
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth())
                $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
            /* PERMISOS */
            if ($this->_ROW['ROL'] != '0')
                $this->Portero($this->Securitor->UsuarioPermiso('73'));
            /* PERMISOS */
        }

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

        function ObtieneDatos() {
            $Mantenitor = new Mantenimientos();
            if (!isset($_GET['ID']) or $_GET['ID'] == '')
                return $Mantenitor->TramitesVacio();
            else
                return $Mantenitor->TramitesDetalleGet($_GET['ID']);
        }

    }

}
?>