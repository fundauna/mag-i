<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	$Listator = new Listador();
	if(!isset($_GET['tipo']))
		$Listator->ProveedoresLista('mantenimientos', $ROW['LID'], basename(__FILE__));
	else
		$Listator->EquiposLista('mantenimientos',$_GET['linea'], $ROW['LID'], basename(__FILE__));
	exit;
}
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN POST
*******************************************************************/
if( isset($_POST['accion'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['cs']) ) die('-1');

	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Mantenitor = new Mantenimientos();
	
	if($_POST['accion']=='I') $_POST['cs'] = $Mantenitor->ContratosConsecutivo();
	
	if(!isset($_POST['equipo'])) $_POST['equipo'] = ''; //SI NO VIENEN MONTOS
	
	if(!isset($_POST['ano'])){ //SI NO VIENEN MONTOS
		$_POST['ano'] = $_POST['moneda'] = $_POST['obs'] = '';
		$_POST['monto'] = '0.00';
	}
	
	if( $Mantenitor->ContratosIME($_POST['cs'], $_POST['accion'], $_POST['contrato'], $_POST['tipo'], $_POST['prov'],$_POST['fecha'], $_POST['aviso'], $_POST['equipo'], $_POST['ano'], $_POST['monto'], $_POST['moneda'], $_POST['obs'], $_ROW['LID']) ){
		//SUBIR ARCHIVO			
		if($_FILES['archivo']['size'] > 0){
			$nombre = $_FILES['archivo']['name'];
			$file = $_FILES['archivo']['tmp_name'];
			$ruta = "../../docs/mantenimientos/contratos/{$_POST['cs']}.zip";
			Bibliotecario::ZipCrear($nombre, $file, $ruta,1) or die("Error: No es posible crear el archivo {$nombre}");
		}
		
		echo'<script>alert("Transacción Finalizada");window.close();</script>';
		exit;
	}else
		echo 'Error al guardar los datos';
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _contratos_detalle extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			//SOLO SE PUEDE ABRIR COMO POP-UP MODAL
			$this->ValidaModal();
			
			if( !isset($_GET['acc']) || ($_GET['acc'] != 'I' && $_GET['acc'] != 'M') ) die('Error de parámetros');
			
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('72') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function ObtieneDatos(){
			$Mantenitor = new Mantenimientos();
			if( !isset($_GET['ID']) or $_GET['ID']==''){
				$_GET['ID'] = '0';
				return $Mantenitor->ContratosVacio();
			}else
				return $Mantenitor->ContratosDetalleGet($_GET['ID']);
		}
		
		function TiposMuestra(){
			$Mantenitor = new Mantenimientos();
			return $Mantenitor->ContratosTiposMuestra($this->_ROW['LID']);
		}
		
		function EquiposMuestra(){
			$Mantenitor = new Mantenimientos();
			return $Mantenitor->ContratosEquiposMuestra($_GET['ID']);
		}
		
		function MontosMuestra(){
			$Mantenitor = new Mantenimientos();
			return $Mantenitor->ContratosMontosMuestra($_GET['ID']);
		}
		
		function Formato($_NUMBER){
			return number_format($_NUMBER, 2, '.', ',');
		}
	}
}
?>