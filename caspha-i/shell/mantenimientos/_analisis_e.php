<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UNA LISTA
*******************************************************************/
if( isset($_GET['list'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	$ROW = $Securitor->SesionGet();
	$Listator = new Listador();
	$Listator->ProveedoresLista('mantenimientos', $ROW['LID'], basename(__FILE__));
	exit;
}
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA COMO UN FORMULARIO
*******************************************************************/
elseif( isset($_POST['accion'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	$_ROW = $Securitor->SesionGet();
	
	$Mantenitor = new Mantenimientos();
	
	if($_POST['accion']=='I') $_POST['id'] = $Mantenitor->AnalisisExtCs();
	
	if($Mantenitor->AnalisisExtIME($_POST['id'], $_POST['nombre'], $_POST['tipo'], $_POST['fecha'], $_POST['prov'], $_POST['descripcion'], $_ROW['LID'], $_POST['accion'])==1){		
		//SUBIR ARCHIVO			
		if($_FILES['archivo']['size'] > 0){
			$nombre = $_FILES['archivo']['name'];
			$file = $_FILES['archivo']['tmp_name'];
			$ruta = "../../docs/mantenimientos/{$_POST['id']}.zip";
			Bibliotecario::ZipCrear($nombre, $file, $ruta,1) or die("Error: No es posible crear el archivo {$nombre}");
		}
		//SUBIR ARCHIVO	
		if($_POST['accion']=='D'){				
			$ruta = "../../docs/mantenimientos/{$_POST['id']}.zip";
			Bibliotecario::ZipEliminar($ruta);
		}
		
		echo'<script>alert("Transacción Finalizada");location.href="../../../baltha-i/mantenimientos/analisis_e.php"</script>';
		exit;
	}else
		echo 'Error al guardar los datos';
	
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}
	
	final class _analisis_e extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('71') );
			/*PERMISOS*/
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }
		
		function AnalisisExtMuestra(){
			$Mantenitor = new Mantenimientos();
			return $Mantenitor->AnalisisExtMuestra($this->_ROW['LID']);
		}
	}
}
?>