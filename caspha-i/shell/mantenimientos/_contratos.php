<?php
/*******************************************************************
PARA SABER SI LA PAGINA ESTA SIENDO LLAMADA DESDE UN AJAX
*******************************************************************/
if( isset($_POST['_AJAX'])){ 
	function __autoload($_BaseClass){require_once "../../../melcha-i/{$_BaseClass}.php";}
	
	if( !isset($_POST['id']) ) die('-1');
	
	$Securitor = new Seguridad();
	if(!$Securitor ->SesionAuth()) die('-0');
	
	$Mantenitor = new Mantenimientos();
	//
	Bibliotecario::ZipEliminar("../../docs/mantenimientos/contratos/{$_POST['id']}.zip");
	//
	echo $Mantenitor->ContratosIME($_POST['id'], $_POST['accion'], '', '', '', '', '', '', '', '','','','');
	exit;
}else{
/*******************************************************************
DECLARACION CLASE GESTORA
*******************************************************************/
	function __autoload($_BaseClass){require_once "../../melcha-i/{$_BaseClass}.php";}

	final class _contratos extends Mensajero{
		private $_ROW = array();
		private $Securitor = '';
		private $inicio = false;
		
		function __construct(){
			$this->Securitor = new Seguridad();
			if(!$this->Securitor->SesionAuth()) $this->Err();
			$this->_ROW = $this->Securitor->SesionGet();
			/*PERMISOS*/
			if($this->_ROW['ROL'] != '0') $this->Portero( $this->Securitor->UsuarioPermiso('72') );
			/*PERMISOS*/
			if(!isset($_POST['nombre'])){
				$_POST['id'] = $_POST['nombre'] = '';
				$_POST['lolo'] = '1';
				$this->inicio = true;
			}
		}

        function Encabezado($_hoja, $_tipo, $_titulo)
        {
            $Qualitor = new Calidad();
            echo $Qualitor->obtieneEncabezadoPie($_hoja, $_tipo, $_titulo);
        }

		function ContratosMuestra(){
			$Mantenitor = new Mantenimientos();
			if($this->inicio)
				return $Mantenitor->ContratosPendientes($this->_ROW['LID']);
			else
				return $Mantenitor->ContratosResumenGet($_POST['id'], $_POST['nombre'], $this->_ROW['LID'], $_POST['lolo']);		
		}
	}
}
?>