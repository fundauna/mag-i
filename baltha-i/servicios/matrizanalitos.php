<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _matrizanalitos();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('f10', 'hr', 'Servicios :: Rel. Matriz-Analitos') ?>
<?= $Gestor->Encabezado('F0010', 'e', 'Rel. Matriz-Analitos') ?>
<br/>
<table class="radius" align="center">
    <tr>
        <td class="titulo">Grupos Registrados</td>
    </tr>
    <tr>
        <td align="center">
            <select id="matriz">
                <option value="">...</option>
                <?php
                $ROW = $Gestor->Grupos();
                for ($x = 0; $x < count($ROW); $x++) {
                    echo "<option value='{$ROW[$x]['id']}' label='{$ROW[$x]['nombre']}' label='{$ROW[$x]['nombre']}'>{$ROW[$x]['nombre']}</option>";
                }

                unset($ROW);
                ?>
            </select>&nbsp;<input type="button" id="btn_buscar" value="Buscar" class="boton2"
                                  onClick="RelacionesBuscar();">
        </td>
    </tr>
</table>
<br/>
<br>
<table class="radius" align="center" border="all">
    <tr>
        <td class="titulo">Analitos Disponibles</td>
        <td class="titulo">Analitos Asignados ---> T&eacute;cnica ---> Limite de Cuantificaci&oacute;n</td>
    </tr>
    <tr>
        <td>
            <div class="divScroll">
                <?php
                $ROW = $Gestor->Registros();

                for ($x = 0; $x < count($ROW); $x++) {
                    if ($ROW[$x]['tipoCroma'] == 1) {
                        $crom = "CL EM/EM";
                    } else if ($ROW[$x]['tipoCroma'] == 2) {
                        $crom = "CG EM/EM";
                    } else {
                        $crom = "NR";
                    }
                    echo "<input id='analitos'  type='checkbox' value='{$ROW[$x]['id']}' label='{$ROW[$x]['nombre']}' name='{$ROW[$x]['nombre']}' title='{$ROW[$x]['tipoCroma']}'>{$ROW[$x]['nombre']} ------ > $crom</br></input>";
                }
                ?>
            </div>
        </td>
        <td id="td_asignados">
            <select id="asignados" style="width:400px;" size="10"></select>
        </td>

    </tr>


    <tr>
        <td align="center">
            <input id="btn_agregar" type="button" value="Agregar" class="boton" onClick="AnalitosAgrega();" disabled>
        </td>
        <td align="center">
            <input id="btn_eliminar" type="button" value="Eliminar" class="boton" onClick="AnalitosElimina();" disabled>
        </td>
    </tr>
</table>
<?= $Gestor->Encabezado('F0010', 'p', '') ?>
</body>
</html>