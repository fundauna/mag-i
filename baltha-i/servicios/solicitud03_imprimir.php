<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _solicitud03_imprimir();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

//SI ES DE PLAGUICIDAS REDIRIGE
if ($ROW[0]['tipo'] == '2') {
    header("location: solicitud03P_imprimir.php?ID={$ROW[0]['cs']}");
    exit;
}

$_TIPO = 1;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<center>
    <?php $Gestor->Incluir('n15', 'hr', 'Servicios :: Solicitud de An&aacute;lisis de Fertilizantes') ?>
    <br/><?= $Gestor->Encabezado('N0015', 'e', 'Solicitud de An&aacute;lisis de Fertilizantes') ?>
    <br>
    <table class="radius" style="font-size:12px" width="630px">
        <tr>
            <td class="titulo" colspan="4">Informaci&oacute;n General</td>
        </tr>
        <tr>
            <td><strong>N&uacute;mero de solicitud:</strong></td>
            <td><?= $_GET['ID'] ?></td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td><strong>Solicitud de cotizaci&oacute;n origen:</strong></td>
            <td><?= $ROW[0]['solicitud'] ?></td>
            <td><strong>Fecha:</strong></td>
            <td><?= $ROW[0]['fecha'] ?></td>
        </tr>
        <tr>
            <td><strong>Estado:</strong></td>
            <td><?= $Gestor->Estado($ROW[0]['estado']) ?></td>
            <td><strong>Prop&oacute;sito:</strong></td>
            <td><input type="text" size="30" value="<?= $ROW[0]['proposito'] ?>" disabled/></td>
        </tr>
        <tr>
            <td><strong>Dependencia:</strong></td>
            <td><select disabled>
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['dependencia'] == '0') {
                        echo 'selected';
                        $_TIPO = 2;
                    } ?>>Venta de servicios
                    </option>
                    <option value="1" <?php if ($ROW[0]['dependencia'] == '1') echo 'selected'; ?>>
                        Fiscalizaci&oacute;n
                    </option>
                    <option value="2" <?php if ($ROW[0]['dependencia'] == '2') echo 'selected'; ?>>Registro</option>
                </select></td>
            <td><strong>Observaciones:</strong></td>
            <td><textarea style="height:32px" disabled><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
        <tr>
            <td><strong>Solicitante:</strong></td>
            <td><?= $ROW[0]['tmp'] ?></td>
            <td><strong>Entregadas por:</strong></td>
            <td><?= $ROW[0]['entregadas'] ?></td>
        </tr>
        <tr>
            <td><strong>Muestreo:</strong></td>
            <td>Realizado por el cliente</td>
            <td><strong>Firma:</strong></td>
            <td>______________________________</td>
        </tr>
        <?php
        if (isset($ROW[0]['suministro'])) {
            ?>
            <tr>
                <td><strong>Suministro del est&aacute;ndar:</strong></td>
                <td><select disabled>
                        <option value="0">Laboratorio</option>
                        <option <?php if ($ROW[0]['suministro'] == '1') echo 'selected' ?>>Cliente</option>
                    </select>
                <td><strong>Muestra de discrepancia:</strong></td>
                <td><select disabled>
                        <option value="0">No</option>
                        <option <?php if ($ROW[0]['discre'] == '1') echo 'selected' ?>>S&iacute;</option>
                    </select>
                </td>
            </tr>
            <?php
            if ($ROW[0]['discre'] == '1') {
                ?>
                <tr>
                    <td><strong># Solicitud:</strong></td>
                    <td><?= $ROW[0]['num_sol'] ?></td>
                    <td><strong>C&oacute;digo(s) externo:</strong></td>
                    <td><?= $ROW[0]['num_mue'] ?></td>
                </tr>
                <?php
            }
        }
        ?>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="630px">
        <thead>
        <tr>
            <td class="titulo" colspan="6">Elementos solicitados</td>
        </tr>
        <tr>
            <td><strong>#</strong></td>
            <td><strong>Nombre</strong></td>
            <td><strong>Concentraci&oacute;n declarada</strong></td>
            <td><strong>Tipo</strong></td>
            <td><strong>Unidad</strong></td>
            <td><strong>Fuente</strong></td>
        </tr>
        </thead>
        <?php
        $ROW2 = $Gestor->SolicitudElementos();
        for ($x = 0; $x < count($ROW2); $x++) {
            ?>
            <tr>
                <td><?= $x + 1 ?>.</td>
                <td><?= $ROW2[$x]['analisis'] ?></td>
                <td><?= $ROW2[$x]['rango'] ?></td>
                <td><select disabled>
                        <option value="">...</option>
                        <option value="0" <?php if ($ROW2[$x]['quela'] == '0') echo 'selected'; ?>>Disponible</option>
                        <option value="1" <?php if ($ROW2[$x]['quela'] == '1') echo 'selected'; ?>>Total</option>
                        <option value="2" <?php if ($ROW2[$x]['quela'] == '2') echo 'selected'; ?>>Quelatado</option>
                        <option value="3" <?php if ($ROW2[$x]['quela'] == '3') echo 'selected'; ?>>No-Quelatado</option>
                    </select></td>
                <td><select disabled>
                        <option value="">...</option>
                        <option value="0" <?php if ($ROW2[$x]['unidad'] == '0') echo 'selected'; ?>>%m/m</option>
                        <option value="1" <?php if ($ROW2[$x]['unidad'] == '1') echo 'selected'; ?>>%m/v</option>
                        <option value="2" <?php if ($ROW2[$x]['unidad'] == '2') echo 'selected'; ?>>ppm</option>
                    </select></td>
                <td><?= $ROW2[$x]['fuente'] ?></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <?php
    $ROW2 = $Gestor->SolicitudImpurezas();
    $total = count($ROW2);
    if ($total > 0) {
        ?>
        <br/>
        <table class="radius" style="font-size:12px" width="630px">
            <thead>
            <tr>
                <td class="titulo" colspan="2">Impurezas</td>
            </tr>
            <tr>
                <td><strong>#</strong></td>
                <td><strong>Nombre</strong></td>
            </tr>
            </thead>
            <?php
            for ($x = 0; $x < $total; $x++) {
                ?>
                <tr>
                    <td><?= $x + 1 ?>.</td>
                    <td><?= $ROW2[$x]['analisis'] ?></td>
                </tr>
                <?php
            }
            ?>
        </table>
        <?php
    }
    ?>
    <br/>
    <table class="radius" style="font-size:12px" width="630px" cellspacing="0">
        <tr>
            <td class="titulo" colspan="4">Muestras</td>
        </tr>
        <tr>
            <td>Producto:</td>
            <td><input type="text" size="20" value="<?= $ROW[0]['producto'] ?>" disabled/></td>
            <td>Densidad:</td>
            <td><select disabled>
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['densidad'] == '0') echo 'selected'; ?>>No</option>
                    <option value="1" <?php if ($ROW[0]['densidad'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td>M&eacute;todo suministrado por el cliente?:</td>
            <td>
                <select disabled>
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['metodo'] == '0') echo 'selected'; ?>>No</option>
                    <option value="1" <?php if ($ROW[0]['metodo'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select></td>
            <td># Registro:</td>
            <td><input type="text" size="15" value="<?= $ROW[0]['registro'] ?>" disabled/></td>
        </tr>
        <tr>
            <td>Formulaci&oacute;n:</td>
            <td>
                <select id="tipo_form" disabled>
                    <option value="">...</option>
                    <?php
                    $ROW2 = $Gestor->Formulaciones();
                    for ($x = 0; $x < count($ROW2); $x++) {
                        ?>
                        <option value="<?= $ROW2[$x]['id'] ?>" <?php if ($ROW[0]['tipo_form'] === $ROW2[$x]['id']) echo 'selected'; ?>><?= $ROW2[$x]['nombre'] ?></option>
                        <?php
                    }
                    ?>
                </select>
            </td>
            <td>Tipo de mezcla:</td>
            <td><select disabled style="width:150px">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['mezcla'] == '0') echo 'selected'; ?>>Mezcla f&iacute;sica
                    </option>
                    <option value="1" <?php if ($ROW[0]['mezcla'] == '1') echo 'selected'; ?>>Fertilizante qu&iacute;mico</option>
                    <option value="2" <?php if ($ROW[0]['mezcla'] == '2') echo 'selected'; ?>>Fertilizante l&iacute;quido</option>
                    <option value="3" <?php if ($ROW[0]['mezcla'] == '3') echo 'selected'; ?>>Fertilizante
                        hidrosoluble
                    </option>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <hr/>
            </td>
        </tr>
        <tbody id="lolo4">
        <?php
        $ROW2 = $Gestor->SolicitudMuestras();
        for ($x = 0; $x < count($ROW2); $x++) {
            if ($ROW[0]['estado'] != '' and $ROW[0]['estado'] != '5') {
                $cod_int = str_replace('LCC-', '', $ROW[0]['cs']) . '-' . ($x + 1) . '-' . $_TIPO;
            } else $cod_int = '';
            ?>
            <tr bgcolor="#CCCCCC">
                <td><strong>C&oacute;digo externo:</strong></td>
                <td><input type="text" name="cod_ext" size="15" maxlength="20" value="<?= $ROW2[$x]['cod_ext'] ?>"
                           disabled/></td>
                <td><strong>Interno:</strong></td>
                <td><?= $cod_int ?></td>
            </tr>
            <tr>
                <td>Recipiente:</td>
                <td><select name="recipiente" disabled>
                        <option value="0" <?php if ($ROW2[$x]['recipiente'] == '0') echo 'selected'; ?>>Bolsa
                            metalizada
                        </option>
                        <option value="1" <?php if ($ROW2[$x]['recipiente'] == '1') echo 'selected'; ?>>Bolsa pl&aacute;stica</option>
                        <option value="2" <?php if ($ROW2[$x]['recipiente'] == '2') echo 'selected'; ?>>Frasco pl&aacute;stico</option>
                        <option value="3" <?php if ($ROW2[$x]['recipiente'] == '3') echo 'selected'; ?>>Frasco de
                            vidrio
                        </option>
                    </select></td>
                <td>Sellado:</td>
                <td><select name="sellado" disabled>
                        <option value="0" <?php if ($ROW2[$x]['sellado'] == '0') echo 'selected'; ?>>No</option>
                        <option value="1" <?php if ($ROW2[$x]['sellado'] == '1') echo 'selected'; ?>>S&iacute;</option>
                    </select></td>
            </tr>
            <tr>
                <td>Lote fabricaci&oacute;n:</td>
                <td><input type="text" name="lote" size="15" maxlength="20" value="<?= $ROW2[$x]['lote'] ?>" disabled/>
                </td>
                <td>Rechazada?:</td>
                <td><select name="rechazada" disabled>
                        <option value="0" <?php if ($ROW2[$x]['rechazada'] == '0') echo 'selected'; ?>>No</option>
                        <option value="1" <?php if ($ROW2[$x]['rechazada'] == '1') echo 'selected'; ?>>S&iacute;
                        </option>
                    </select></td>
            </tr>
            <tr>
                <td>Observaci&oacute;n:</td>
                <td colspan="3"><input type="text" name="obse" size="30" maxlength="50" value="<?= $ROW2[$x]['obse'] ?>"
                                       disabled/></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="630px">
        <thead>
        <tr>
            <td class="titulo">Recibidos</td>
        </tr>
        </thead>
        <?php
        $ROW2 = $Gestor->Historial();
        for ($x = 0; $x < count($ROW2); $x++) {
            ?>
            <tr>
                <td>
                    <strong><?= $Gestor->Accion($ROW2[$x]['accion']) ?></strong> <?= "{$ROW2[$x]['nombre']}, {$ROW2[$x]['fecha1']}" ?>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
    <br/><?= $Gestor->Encabezado('N0015', 'p', '') ?>
    <br/>
</center>
<?= $Gestor->Footer(2) ?>
<center><br/><input type="button" value="Imprimir" class="boton" onClick="window.print()"></center>
</body>
</html>