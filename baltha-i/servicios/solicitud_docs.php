<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _solicitud_docs();

if ($_GET['estado'] != '2' && $_GET['estado'] != '5') $ok = true;
else $ok = false;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('estilo', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('n16', 'hr', 'Servicios :: Anexos') ?>
<?= $Gestor->Encabezado('N0016', 'e', 'Anexos') ?>
<center>
    <form name="form" method="post" enctype="multipart/form-data"
          action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
        <input type="hidden" name="id" value="<?= $_GET['ID'] ?>"/>
        <table class="radius">
            <tr>
                <td class="titulo" colspan="2">Detalle<?php if ($ok) { ?>&nbsp;<img onclick="CertificadoMas()"
                                                                                    src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                                                                                    title="Agregar l�nea"
                                                                                    class="tab" /><?php } ?></td>
            </tr>
            <tr align="center">
                <td><strong>C&oacute;digo</strong></td>
                <td><strong>Adjunto</strong></td>
            </tr>
            <tbody id="t3">
            <?php
            $ROW = $Gestor->CertificadosMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                $ROW[$x]['detalle'] = str_replace(' ', '', $ROW[$x]['detalle']);
                $zip = "{$_GET['ID']}-{$ROW[$x]['detalle']}";
                $ruta = "../../caspha-i/docs/servicios/{$zip}.zip";
                ?>
                <tr onmouseout="mOut(this)" onmouseover="mOver(this)">
                    <td><?= $x + 1 ?>:</td>
                    <td><input type="text" name="detalle[]" value="<?= $ROW[$x]['detalle'] ?>" size="30" maxlength="50">
                        <?php
                        if (file_exists($ruta)) {
                            ?>
                            <img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar"
                                 onclick="DocumentosZip('<?= $zip ?>', '<?= __MODULO__ ?>')" class="tab2"/>
                            <input type="file" name="archivo[]" style="display:none"/><!-- dummy -->
                        <?php } else { ?>
                            &nbsp;<input type="file" name="archivo[]"/>
                        <?php } ?>
                        &nbsp;<img onclick="DocElimina('<?= $zip ?>', '<?= $_GET['ID'] ?>')"
                                   src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar documento"
                                   class="tab2"/>
                    </td>
                </tr>
                <?php
            }
            if ($x < 1) {
                ?>
                <tr onmouseout="mOut(this)" onmouseover="mOver(this)">
                    <td>1:</td>
                    <td><input type="text" name="detalle[]" size="30" maxlength="50">&nbsp;<input type="file"
                                                                                                  name="archivo[]"/>
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </form>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
</center>
<?= $Gestor->Encabezado('N0016', 'p', '') ?>
</body>
</html>