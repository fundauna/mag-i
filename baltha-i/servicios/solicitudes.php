<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _solicitudes();

if ($_POST['LAB'] == '3') $display = '';
else $display = 'style="display:none"';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<center>
    <br>
    <br>
<?php $Gestor->Incluir('n17', 'hr', 'Servicios :: Solicitudes') ?>
<?= $Gestor->Encabezado('N0017', 'e', 'Solicitudes') ?>
    <br>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <input type="hidden" name="LAB" id="LAB" value="<?= $_POST['LAB'] ?>"/>
        <input type="hidden" name="opcion" id="opcion" value="<?= $_POST['opcion'] ?>"/>
        <table class="radius" align="center" width="630px">
            <tr>
                <td class="titulo" colspan="8">Filtro</td>
            </tr>
            <tr>
                <td><input type="radio" name="opc"
                           onclick="Activa(0)" <?php if ($_POST['opcion'] == '0') echo 'checked'; ?> /></td>
                <td># de Solicitud:</td>
                <td colspan="6"><input type="text" id="solicitud" name="solicitud" size="13" maxlength="13"
                                       value="<?= $_POST['solicitud'] ?>"/></td>
            </tr>
            <tr>
                <td><input type="radio" name="opc"
                           onclick="Activa(1)" <?php if ($_POST['opcion'] == '1') echo 'checked'; ?> /></td>
                <td>Desde:</td>
                <td>
                    <input type="text" id="desde" name="desde" class="fecha" value="<?= $_POST['desde'] ?>" readonly
                           onClick="show_calendar(this.id);">
                    &nbsp;Hasta: <input type="text" id="hasta" name="hasta" class="fecha" value="<?= $_POST['hasta'] ?>"
                                        readonly onClick="show_calendar(this.id);">
                </td>

                <td <?= $display ?>>Tipo:</td>
                <td <?= $display ?>><select id="tipo" name="tipo" style="width:90px">
                        <option value="">Todas</option>
                        <option value="1" <?php if ($_POST['tipo'] == '1') echo 'selected'; ?>>Fertilizantes</option>
                        <option value="2" <?php if ($_POST['tipo'] == '2') echo 'selected'; ?>>Plaguicidas</option>
                    </select></td>

                <td>Estado:</td>
                <td><select id="estado" name="estado">
                        <option value="0">Generada</option>
                        <option value="1" <?php if ($_POST['estado'] == '1') echo 'selected'; ?>>Pendiente</option>
                        <option value="2" <?php if ($_POST['estado'] == '2') echo 'selected'; ?>>Aprobada</option>
                        <option value="5" <?php if ($_POST['estado'] == '5') echo 'selected'; ?>>Anulada</option>
                        <option value="7" <?php if ($_POST['estado'] == '7') echo 'selected'; ?>>Finalizada</option>
                        <option value="" <?php if ($_POST['estado'] == '') echo 'selected'; ?>>Todos</option>
                    </select></td>
                <td><input type="button" value="Buscar" class="boton" onclick="SolicitudesBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <script>Activa(<?=$_POST['opcion']?>)</script>
    <br/>
    <div id="container" style="width:65%">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>No. Solicitud</th>
                <th>Fecha</th>
                <th>Cliente</th>
                <?php if ($Gestor->obtieneLAB() == '1') { ?>
                    <th>Recepcion Lab</th>
                <?php } ?>
                <th>Estado</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->SolicitudesMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#" onclick="SolicitudesModifica('<?= $ROW[$x]['cs'] ?>')"><?= $ROW[$x]['cs'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['fecha'] ?></td>
                    <td><?= $ROW[$x]['nombre'] ?></td>
                    <?php if ($Gestor->obtieneLAB() == '1') { ?>
                        <td><?= $ROW[$x]['flab'] ?></td>
                    <?php } ?>
                    <td><?= $Gestor->Estado($ROW[$x]['estado']) ?></td>
                    <td><img onclick="SolicitudesImprime('<?= $ROW[$x]['cs'] ?>')"
                             src="<?php $Gestor->Incluir('previa', 'bkg') ?>" title="Vista previa" class="tab3"/></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/>
    <input type="button" value="Agregar" class="boton" onClick="SolicitudesAgrega();">
</center>
<?= $Gestor->Encabezado('N0017', 'p', '') ?>
</body>
</html>