<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _LRE_hoja3();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) {
    die('Registro inexistente');
}

$ROW2 = $Gestor->SolicitudLineas();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body style="background-image: none; background-color: #FFF">
<center>
    <?php $Gestor->Incluir('n8', 'hr', 'Servicios :: Hoja de trazabilidad para la inyecci&oacute;n en equipos') ?>
    <?= $Gestor->Encabezado('N0008', 'e', 'Hoja de trazabilidad para la inyecci&oacute;n en equipos') ?>
    <br>
    <table class="radius" width="98%">
        <tr>
            <td><strong>Fecha de ingreso:</strong></td>
            <td><?= $ROW[0]['fecha'] ?> <?= $ROW[0]['hora'] ?></td>
            <td><strong>Tiempo de entrega de resultados:</strong></td>
            <td align="right"><?= $ROW[0]['entregacliente'] ?> d&iacute;as</td>
            <td></td>
            <td align="right"><b style="font-size: 14px"><?= $_GET['ID'] ?></b></td>
        </tr>
        <tr>
            <td><strong>Lote:</strong></td>
            <td><?= $_GET['ID'] ?></td>
            <td><strong>N&uacute;mero de muestras:</strong> <?= count($ROW2) ?></td>
            <td></td>
            <td><strong>Acondicionamiento equipo:</strong></td>
            <td>__________________________________</td>
        </tr>
        <tr>
            <td><strong>Blanco matriz para curvas</strong></td>
            <td></td>
            <td></td>
            <td></td>
            <td><strong>Fecha de inicio de revisi&oacute;n:</strong></td>
            <td>__________________________________</td>
        </tr>
        <tr>
            <td>G1: ________________________</td>
            <td>G3: ________________________</td>
            <td>G5: ________________________</td>
            <td></td>
            <td><strong>Fecha de conclusi&oacute;n de revisi&oacute;n:</strong></td>
            <td>__________________________________</td>
        </tr>
        <tr>
            <td>G2: ________________________</td>
            <td>G4: ________________________</td>
            <td></td>
        </tr>
    </table>
    <br/>
    <table class="tableImpresa" width="98%" border="1" bordercolor="#000000" cellpadding="0" cellspacing="0">
        <tr align="center">
            <td style="width: 150px;"><strong>N&uacute;mero de muestra</td>
            <td style="width: 100px" rowspan="2"><strong>Matriz</strong></td>
            <td style="width: 60px" rowspan="2"><strong>Grupo</strong></td>
            <td style="width: 60px" rowspan="2">
                <strong><?= 'Equipo ' . $_GET['tipo'] ?></strong>
            </td>
            <td rowspan="2" width="900px"><strong>Resultado y Confirmaci&oacute;n</strong></td>
        </tr>
        <tr align="center">
            <td><strong>C&oacute;digo externo</strong></td>
        </tr>
        <?php
        /*for ($x = 0; $x < count($ROW2); $x++) {
            $cod_int = $_GET['ID'] . '-' . ($x + 1);
            ?>*/
        for ($x = 0; $x < count($ROW2); $x++) {
            $cod_int = $_GET['ID'] ;
            $cod_int = str_replace('-', '', $cod_int);
            $cod_int = $cod_int. '-' . ($x + 1);
            ?>
            <tr>
                <td align="center"><?= $cod_int ?>
                    <hr><?= $ROW2[$x]['codigo'] ?></td>
                <td align="center"><?= $ROW2[$x]['nombre'] ?></td>
                <td align="center">&nbsp;G-<?= $ROW2[$x]['grupo'] ?></td>
                <td>&nbsp; &nbsp;EQ-</td>
                <td><br><br><br><br><br><br><br></td>
            </tr>
            <?php
        }

        if ($x < 12) {
            for (; $x < 12; $x++) {
                ?>
                <tr>
                    <td><br/>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><br><br><br><br><br><br></td>
                </tr>
                <?php
            }
        }
        ?>
    </table>
</center>
<div style="width: 99%; margin: auto">
    G1A: Alto contenido en agua  y clorofila, G1B: Alto contenido de agua y contenido escaso o ausencia de clorofila, G2A: Alto contenido de aceite o grasa y muy bajo contenido de agua,
    G2B:  Alto contenido de aceite o grasa e intermedio contenido de agua, G3:  Alto contenido de &aacute;cido y agua,
    G4: Alto contenido de almid&oacute;n y/o prote&iacute;na y bajo contenido de agua y grasa, G5: Productos especiales o dif&iacute;ciles,G6: Alto contenido de az&uacute;car y bajo contenido de agua
</div>
</br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<div align="center" style="width:98%;margin: auto">
    Observaciones:________________________________________________________________________________________________________________________________________________<br/>
    <br/>_____________________________________________________________________________________________________________________________________________________________<br/>
    <br/>_____________________________________________________________________________________________________________________________________________________________
    <br/>
    <br/>
    <br/>
    <div>&Uacute;ltima L&iacute;nea</div>
</div>
<center>
    <br/>
    <br/>
    <input type="button" value="Imprimir" class="boton2" onClick="window.print()">
</center>
<?= $Gestor->Encabezado('N0008', 'p', '') ?>
</body>
</html>