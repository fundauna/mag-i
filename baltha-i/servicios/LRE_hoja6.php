<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _LRE_hoja6();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) {
    die('Registro inexistente');
}

$ROW2 = $Gestor->SolicitudLineas();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <script>
        var _oculta = false;

        function Oculta() {
            total = document.getElementsByName('dummy').length;
            alert= total;
            for (x = 0; x < total; x++) {
                document.getElementById('tdA' + x).innerHTML = '<br>&nbsp;';
                document.getElementById('tdB' + x).innerHTML = '';
                document.getElementById('tdC' + x).innerHTML = '';
            }
        }
    </script>
</head>
<body style="background-image: none; background-color: #FFF">
<center>
   <!-- <table class="radius" style="font-size:12px" width="98%">
        <tr align="center">
            <td>
                <table align="center" width="100%" border="1" bordercolor="#000000" cellpadding="0" cellspacing="0"
                       style="font-size:12px">
                    <tr align="center">
                        <td colspan="2"><?php $Gestor->Incluir('mag', 'png'); ?></td>
                        <td><strong>Ministerio de Agricultura y Ganader&iacute;a<br/>Servicio Fitosanitario del
                                Estado<br/>Laboratorio de An&aacute;lisis de Residuos de Agroqu&iacute;micos</strong>
                        </td>
                        <td colspan="2"><?php $Gestor->Incluir('agro', 'png'); ?></td>
                    </tr>
                    <tr align="center">
                        <td>R-13-LAB-LRE-PO-01</td>
                        <td>Rige a partir de:<br>16-05-2016</td>
                        <td><strong>Hoja de trazabilidad para la extracci&oacute;n en muestras de agua</strong></td>
                        <td><font size='-2'>P&aacute;gina 1 de 1</font></td>
                        <td><font size='-2'>Version: 01</font></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>-->
    <br>
    <table class="radius" width="98%">
        <tr>
            <td width="20%"><strong>Fecha de ingreso:</strong></td>
            <td><?= $ROW[0]['fecha'] ?></td>

            <td width="30%"><strong>Tiempo de entrega de resultados:</strong></td>
            <td width="20%"><?= $ROW[0]['entregacliente'] ?> d&iacute;as</td>
            <td align="right"><b style="font-size: 14px"><?= $_GET['ID'] ?></b></td>
        </tr>
        <tr>
            <td><strong>Lote:</strong></td>
            <td><?= $_GET['ID'] ?></td>

            <td><strong>N&uacute;mero de muestras:</strong></td>
            <td><?= count($ROW2) ?></td>
            <td></td>
        </tr>
        <tr>
            <td><strong>An&aacute;lisis Solicitado:</strong></td>
            <td width="25%"><?= $ROW2[0]['nom_analisis'] ?></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="5"></td>
        </tr>
    </table>
    <br/>
    <table width="98%" border="1" bordercolor="#000000" cellpadding="1" cellspacing="0">
        <tr align="center">
            <td colspan="4"><strong></td>
            <td><strong>Fecha</td>
            <td colspan="2"><strong>Micropipeta</td>
            <td><strong></strong></td>
        </tr>

        <tr align="center">
            <td width="10%" rowspan="2"><strong>Muestra</td>
            <td width="10%" rowspan="2"><strong>Matriz</strong></td>
            <td width="8%" rowspan="2"><strong>T&eacute;cnica</strong></td>
            <td width="5%" rowspan="2"><strong>Volumen<br> (mL)</strong></td>
            <td width="10%" rowspan="2"><strong>Extracci&oacute;n y eluci&oacute;n<hr>Filtrado</strong></td>
            <td width="10%" rowspan="2"><strong>Eluci&oacute;n<hr>Alicuota y acidificaci&oacute;n</strong></td>
            <td width="10%"  rowspan="2"><strong>Reconstituci&oacute;n</strong></td>
            <td width="30%" rowspan="2"><strong>Tratamiento Especial</strong></td>
        </tr>
        <tr align="center">

        </tr>
        <?php
        for ($x = 0; $x < count($ROW2); $x++) {
            $cod_int = $_GET['ID'] . '-' . ($x + 1);
            ?>
            <tr style="font-size: 11px;">
                <td rowspan="2" align="center" id="tdA<?= $x ?>"><?= $cod_int ?><!-- <hr><?= $ROW2[$x]['codigo'] ?>--></td>
                <td rowspan="2" align="center" id="tdB<?= $x ?>"><?= $ROW2[$x]['nombre'] ?></td>
               <!-- <td align="center" id="tdC<?= $x ?>" width="12px"><?= $ROW2[$x]['grupo'] ?></td>-->
                <td  align="center"  width="12px">CG</td>

                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;M-</td>
                <td align="left">M-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ;M-</td>
                <td rowspan="2" align="left"><strong>C&Oacute;DIGO EXTERNO:&nbsp;&nbsp;</strong><?= $ROW2[$x]['codigo'] ?> <br/>Contenido botella centrifugado:&nbsp;&nbsp;  SI__ &nbsp;&nbsp;NO __</td>


                <input type="hidden" name="dummy"/>
            </tr>
            <tr style="font-size: 11px;">
                <td  align="center"  width="12px">CL</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>M-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ;M-</td>
                <td>M-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ;M-</td>
            </tr>
            <?php
        }               //12
        for ($x = 0; $x < 12; $x++) { ?>
            <tr>
                <td rowspan="2"><br><br><br></td>
                <td rowspan="2"><br><br><br></td>
                <td align="center" width="10px">CG</td>
                <td ><br><br><br></td>
                <td ><br><br><br></td>
                <td>M-</td>
                <td>M-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ;M-</td>
                <td rowspan="2" width="60px"><br><br><br></td>
            </tr>
            <tr>
                <td align="center" width="10px">CL</td>
                <td>&nbsp;</td>
                <td><br><br><br></td>
                <td>M-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ;M-</td>
                <td>M-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ;M-</td>
            </tr>
        <?php } ?>
    </table>
</center>
<div style="width: 99%; margin: auto">
    G1A: Alto contenido en agua  y clorofila, G1B: Alto contenido de agua y contenido escaso o ausencia de clorofila, G2A: Alto contenido de aceite o grasa y muy bajo contenido de agua,
    G2B:  Alto contenido de aceite o grasa e intermedio contenido de agua, G3:  Alto contenido de &aacute;cido y agua,
    G4: Alto contenido de almid&oacute;n y/o prote&iacute;na y bajo contenido de agua y grasa, G5: Productos especiales o dif&iacute;ciles,G6: Alto contenido de az&uacute;car y bajo contenido de agua
</div>
</br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<div align="center" style="width:98%;margin: auto">
    Observaciones:________________________________________________________________________________________________________________________________________________<br/>
    <br/>_____________________________________________________________________________________________________________________________________________________________<br/>
    <br/>_____________________________________________________________________________________________________________________________________________________________
    <br/>
    <br/>
    <br/>
    <div>&Uacute;ltima L&iacute;nea</div>
</div>
    <br/>
    <br/>

    <input type="button" value="Imprimir" class="boton2" onClick="window.print()">
</center>
</body>
</html>