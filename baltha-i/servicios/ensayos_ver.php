<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _ensayos_ver();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

$_POST['analisis'] = $ROW[0]['analisis'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="UID" value="<?= $_POST['UID'] ?>"/>
<?php $Gestor->Incluir('n3', 'hr', 'Servicios :: Ensayos') ?>
<?= $Gestor->Encabezado('N0003', 'e', 'Ensayos') ?>
<table width="70%" align="center">
    <tr>
        <td colspan="2" align="center">
            <!-- -->
            <table class="radius" width="35%">
                <tr>
                    <td class="titulo" colspan="2">Informaci&oacute;n General</td>
                </tr>
                <tr>
                    <td><strong>Solicitud:</strong></td>
                    <td><?= $ROW[0]['solicitud'] ?></td>
                </tr>
                <tr>
                    <td><strong># de muestra:</strong></td>
                    <td><?= //$mu= str_replace('--0', '', $ROW[0]['ref']);

                        $mu = str_replace('--0', '', str_replace('-1 ', '-01', str_replace('-2 ', '-02', str_replace('-3 ', '-03', str_replace('-4 ', '-04', str_replace('-5 ', '-05', str_replace('-6 ', '-06', str_replace('-7 ', '-07',
                            str_replace('-8 ', '-08', str_replace('-9 ', '-09', $ROW[0]['ref']))))))))));
                        ?></td>
                </tr>
                <tr>
                    <td><strong>An&aacute;lisis solicitado:</strong></td>
                    <td><?= $ROW[0]['nombre'] ?></td>
                </tr>
                <tr>
                    <td><strong>Opciones:</strong></td>
                    <td>
                        <input type="button" class="boton2" value="Aprobar" onclick="Aprobar('<?= $_GET['ID'] ?>')"
                               title="Finalizar los ensayos a la muestra" id="btn1" disabled/>
                        <input type="button" class="boton2" value="Regresar" onclick="Regresar()"
                               title="Regresar a an&aacute;lisis pendientes"/>
                    </td>
                </tr>
            </table>
            <!-- -->
        </td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr align="center" valign="top">
        <td width="35%">
            <!-- ENSAYOS DISPONIBLES -->
            <table class="radius" style="font-size:12px" width="70%">
                <thead>
                <tr>
                    <td class="titulo" colspan="2">Hojas de ingreso de resultados</td>
                </tr>
                </thead>
                <tr>
                    <td><strong>Nombre</strong></td>
                    <td align="center"><strong>Arqueo</strong></td>
                </tr>
                <?php
                $ROW2 = $Gestor->MetodosDisponibles();
                for ($x = 0; $x < count($ROW2); $x++) {

                    ?>
                    <tr>
                         <td>- <a href="#"
                                 onclick="EnsayoGenerar('<?= $_GET['ID'] ?>', '<?= $ROW2[$x]['id'] ?>', '<?= $ROW2[$x]['pagina'] ?>')"><?= $ROW2[$x]['nombre'] ?></a>
                        </td>


                      <!-- se comenta para que pinte el nombre de la hoja de trabajo
                       <td>- <a href="#"
                                 onclick="EnsayoGenerar('<?= $_GET['ID'] ?>', '<?= $ROW2[$x]['id'] ?>', '<?= $ROW2[$x]['pagina'] ?>')">Ingreso
                                de Resultados </a>
                        </td>-->
                        <td align="center"><img
                                    onclick="ArqueoVer('<?= $ROW2[$x]['id'] ?>', '<?= $ROW2[$x]['nombre'] ?>')"
                                    src="<?php $Gestor->Incluir('arqueo', 'bkg') ?>" border="0"
                                    title="Mostrar inventario requerido" class="tab2"/></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <!-- ENSAYOS DISPONIBLES -->
        </td>
        <td width="40%">
            <!-- ENSAYOS HISTORIAL -->
            <table class="radius" style="font-size:12px" width="100%">
                <thead>
                <tr>
                    <td class="titulo" colspan="3">Historial de ensayos</td>
                </tr>
                </thead>
                <tr align="center">
                    <td><strong>Tipo</strong></td>
                    <td><strong>Resultado</strong></td>
                    <td><strong>V&aacute;lido</strong></td>
                </tr>
                <?php
                $ok = 0;
                $ROW2 = $Gestor->Ensayos();
                for ($x = 0; $x < count($ROW2); $x++) {
                    ?>
                    <tr>
                        <td><a href="#"
                               onclick="EnsayoVer('<?= $_GET['ID'] ?>', '<?= $ROW2[$x]['id'] ?>', '<?= $ROW2[$x]['pagina'] ?>', '<?= $ROW2[$x]['ensayo'] ?>')">Datos Ingresados</a>
                        </td>
                        <td align="center"><?= $ROW2[$x]['resultado'] ?></td>
                        <td align="center">
                            <?php if ($ROW2[$x]['cumple'] == '1') {
                                $ok++; ?>
                                <img src="<?php $Gestor->Incluir('aprobar', 'bkg') ?>" title="S�" class="tab3"
                                     onclick="CambiarEstado('<?= $ROW2[$x]['id'] ?>', '<?= $ROW2[$x]['analista'] ?>', 0)"/>
                            <?php } else { ?>
                                <img src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="No" class="tab3"
                                     onclick="CambiarEstado('<?= $ROW2[$x]['id'] ?>', '<?= $ROW2[$x]['analista'] ?>', 1)"/>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <!-- ENSAYOS HISTORIAL -->
        </td>
    </tr>
</table>
<input type="hidden" id="ok" value="<?= $ok ?>"/>
<?= $Gestor->Encabezado('N0003', 'p', '') ?>
<?php if ($ok > 0) echo '<script>Habilita();</script>'; ?>
</body>
</html>