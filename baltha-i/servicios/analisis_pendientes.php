<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _analisis_pendientes();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('n0', 'hr', 'Servicios :: An�lisis pendientes') ?>
<?= $Gestor->Encabezado('N0000', 'e', 'An�lisis pendientes') ?>
<?php $ROL = $Gestor->getROL(); ?>
<br/>
<center>
    <div id="container" style="width:600px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>No. Muestra</th>
                <th>Solicitud</th>
                <th>An&aacute;lisis</th>
                <?php if ($ROL != '0'): ?>
                    <th>Tipo de Analista</th>
                <?php endif; ?>
                <th>Ensayos</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->Analisis();
            for ($x = 0; $x < count($ROW); $x++) {
                if ($ROW[$x]['nombre'] != 'Densidad') {
                    ?>
                    <tr class="gradeA" align="center">
                        <td><?=
                            $mu = str_replace('--0', '', str_replace('-1 ', '-01', str_replace('-2 ', '-02', str_replace('-3 ', '-03', str_replace('-4 ', '-04', str_replace('-5 ', '-05', str_replace('-6 ', '-06', str_replace('-7 ', '-07',
                                str_replace('-8 ', '-08', str_replace('-9 ', '-09', $ROW[$x]['ref']))))))))));
                           ?></td>
                        <td><?= $ROW[$x]['solicitud'] ?></td>
                        <td><?= $ROW[$x]['nombre'] ?></td>
                        <?php if ($ROL != '0'): ?>
                            <td><?= $ROW[$x]['posicion'] ?></td>
                        <?php endif; ?>
                        <td><img onclick="EnsayosVer('<?= $ROW[$x]['xanalizar'] ?>')"
                                 src="<?php $Gestor->Incluir('previa', 'bkg') ?>" title="Ver ensayos" class="tab3"/>
                        </td>
                    </tr>
                <?php }
            } ?>
            </tbody>
        </table>
    </div>
</center>
<?= $Gestor->Encabezado('N0000', 'p', '') ?>
</body>
</html>