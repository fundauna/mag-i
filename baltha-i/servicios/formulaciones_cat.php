<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _formulaciones();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('n20', 'hr', 'Servicios :: Cat�logo de Formulaciones') ?>
<?= $Gestor->Encabezado('N0020', 'e', 'Cat�logo de Formulaciones') ?>
<form action="<?= basename(__FILE__) ?>" method="post">
    <table class="radius" align="center" style="width:300px">
        <tr>
            <td class="titulo" colspan="2">Filtro</td>
        </tr>
        <tr>
            <td><select id="tipo" name="tipo">
                    <option value="F" <?php if ($_POST['tipo'] == 'F') echo 'selected'; ?>>Fertilizantes</option>
                    <option value="P" <?php if ($_POST['tipo'] == 'P') echo 'selected'; ?>>Plaguicidas</option>
                </select></td>
            <td><input type="button" value="Buscar" class="boton2" onclick="TiposBuscar(this)"/></td>
        </tr>
    </table>
</form>
<br/>
<table class="radius" align="center" width="300px">
    <tr>
        <td class="titulo" colspan="2">Registros</td>
    </tr>
    <tr>
        <td colspan="2">
            <select size="10" style="width:100%" onclick="marca(this);">
                <?php
                $ROW = $Gestor->Registros();
                for ($x = 0; $x < count($ROW); $x++) {
                    echo "<option id='{$ROW[$x]['id']}' nombre='{$ROW[$x]['nombre']}'>{$ROW[$x]['nombre']}</option>";
                }
                unset($ROW);
                ?>
            </select>
        </td>
    </tr>
</table>
<br/>
<table class="radius" align="center" width="300px">
    <tr>
        <td class="titulo" colspan="2">Detalle</td>
    </tr>
    <input type="hidden" id="id">
    <tr>
        <td><b>Nombre:</b></td>
        <td><input type="text" id="nombre" size="25" maxlength="50"></td>
    </tr>
</table>
<br/>
<center>
    <input id="new" type="button" class="boton" value="Limpiar" onclick="desmarca();" disabled>&nbsp;
    <input id="add" type="button" class="boton" value="Agregar" onclick="modificar('I');">&nbsp;
    <input id="mod" type="button" class="boton" value="Modificar" onclick="modificar('M');" disabled>&nbsp;
    <input id="del" type="button" class="boton" value="Eliminar" onclick="modificar('D');" disabled>
</center>
<?= $Gestor->Encabezado('N0020', 'p', '') ?>
</body>
</html>