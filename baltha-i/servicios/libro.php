<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _libro();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('n5', 'hr', 'Servicios :: Libro de custodia') ?>
<?= $Gestor->Encabezado('N0005', 'e', 'Libro de custodia') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center">
            <tr>
                <td class="titulo" colspan="2">Filtro</td>
            </tr>
            <tr>
                <td>Tipo de b&uacute;squeda:</td>
                <td><select name="tipo">
                        <option value="0"># de solicitud</option>
                        <option value="1" <?php if ($_POST['tipo'] == '1') echo 'selected'; ?>># de muestra</option>
                        <option value="2" <?php if ($_POST['tipo'] == '2') echo 'selected'; ?>>Cod. externo</option>
                    </select></td>
            </tr>
            <tr>
                <td>Valor a buscar:</td>
                <td><input type="text" id="dato" name="dato" value="<?= $_POST['dato'] ?>" size="14" maxlength="16"/>
                </td>
            </tr>
            <tr>
                <td>Fecha ingreso:</td>
                <td><input type="text" id="desde" name="desde" class="fecha" value="<?= $_POST['desde'] ?>" readonly
                           onClick="show_calendar(this.id);">&nbsp;
                    <input type="text" id="hasta" name="hasta" class="fecha" value="<?= $_POST['hasta'] ?>" readonly
                           onClick="show_calendar(this.id);"></td>
            </tr>
            <tr>
                <td>Estado:</td>
                <td><select name="estado">
                        <option value="">Todos</option>
                        <option value="0" <?php if ($_POST['estado'] == '0') echo 'selected'; ?>>Recepci&oacute;n
                        </option>
                        <option value="1" <?php if ($_POST['estado'] == '1') echo 'selected'; ?>>An&aacute;lisis
                        </option>
                        <option value="2" <?php if ($_POST['estado'] == '2') echo 'selected'; ?>>Reporte</option>
                        <option value="3" <?php if ($_POST['estado'] == '3') echo 'selected'; ?>>Por retirar</option>
                        <option value="4" <?php if ($_POST['estado'] == '4') echo 'selected'; ?>>Retirada</option>
                    </select></td>
            </tr>
            <tr align="center">
                <td colspan="2"><input type="button" value="Buscar" class="boton2" onclick="SolicitudesBuscar(this)"/>
                </td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:600px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>No.</th>
                <th>Cod. Ext.</th>
                <th>Solicitud</th>
                <th>Ingreso</th>
                <th>Estado</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->Muestras();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#"
                           onclick="MuestraHistorial('<?= $ROW[$x]['id'] ?>', '<?= $ROW[$x]['solicitud'] ?>')"><?= $ROW[$x]['ref'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['externo'] ?></td>
                    <td><?= $ROW[$x]['solicitud'] ?></td>
                    <td><?= $ROW[$x]['fecha1'] ?></td>
                    <td><?= $Gestor->Estado($ROW[$x]['estado']) ?></td>
                    <td><img onclick="BarCode('<?= $ROW[$x]['ref'] ?>')"
                             src="<?php $Gestor->Incluir('barcode', 'bkg') ?>" title="Imprimir Codigo Barras"
                             class="tab2"/></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</center>
<?= $Gestor->Encabezado('N0005', 'p', '') ?>
</body>
</html>