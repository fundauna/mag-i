<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _barcode();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<center>
    <table class="radius" style="font-size:12px" width="90%">
        <tr align="center">
            <td>
                <table align="center" width="100%" cellpadding="0" cellspacing="0" style="font-size:12px">
                    <tr align="center">
                        <td colspan="2"><?php $Gestor->Incluir('mag', 'png'); ?></td>
                        <td><strong>Ministerio de Agricultura y Ganader&iacute;a<br/>Servicio Fitosanitario del
                                Estado<br/>Laboratorio de Control de Calidad de Agroqu&iacute;micos</strong></td>
                        <td colspan="2"><?php $Gestor->Incluir('agro', 'png'); ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br/>
    <img src="../../melcha-i/barcode/html/image.php?text=<?= $_GET['ID'] ?>"/>
</center>
<script>window.print();</script>
</body>
</html>