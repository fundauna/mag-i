<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _LRE_hoja4();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW)
    die('Registro inexistente');

$ROW2 = $Gestor->SolicitudLineas();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <style>
        .Rotate-90 {
            -webkit-transform: rotate(-90deg);
            -moz-transform: rotate(-90deg);
            -ms-transform: rotate(-90deg);
            -o-transform: rotate(-90deg);
            transform: rotate(-90deg);

            -webkit-transform-origin: 50% 50%;
            -moz-transform-origin: 50% 50%;
            -ms-transform-origin: 50% 50%;
            -o-transform-origin: 50% 50%;
            transform-origin: 50% 50%;

            font-size: 10px;
            position: relative;
        }

        @page {
            size: auto;   /* auto is the initial value */
            margin: 6px;  /* this affects the margin in the printer settings */
        }
    </style>
</head>
<body style="font-family:Calibri; font-size:10px">
<?php
for ($x = 0; $x < count($ROW2); $x++) {
    $cod_int = $ROW[0]['lote'] . '-' . ($x + 1);
    $ROW[0]['fecha'] = substr($ROW[0]['fecha'], 0, 10);
    ?>
    <table>
        <tr>
            <td><strong>INGRESO:</strong></td>
            <td><?= $ROW[0]['fecha'] ?></td>
            <td rowspan="5" class="Rotate-90"><img src="../../melcha-i/barcode/html/image.php?text=<?= $cod_int ?>"/>
            </td>
            <td>&nbsp;&nbsp;<strong>INGRESO:</strong></td>
            <td><?= $ROW[0]['fecha'] ?></td>
            <td rowspan="5" class="Rotate-90"><img src="../../melcha-i/barcode/html/image.php?text=<?= $cod_int ?>"/>
            </td>
        </tr>
        <tr>
            <td><strong>MUESTRA:</strong></td>
            <td><?= $cod_int ?></td>
            <td>&nbsp;&nbsp;<strong>MUESTRA:</strong></td>
            <td><?= $cod_int ?></td>
        </tr>
        <tr>
            <td><strong>MATRIZ:</strong></td>
            <td><?= $ROW2[$x]['nombre'] ?></td>
            <td>&nbsp;&nbsp;<strong>MATRIZ:</strong></td>
            <td><?= $ROW2[$x]['nombre'] ?></td>
        </tr>
        <tr>
            <td><strong>MARCHAMO:</strong></td>
            <td><?= $ROW2[$x]['codigo'] ?></td>
            <td>&nbsp;&nbsp;<strong>MARCHAMO:</strong></td>
            <td><?= $ROW2[$x]['codigo'] ?></td>
        </tr>

        <tr>
            <td><strong>CUSTODIA:</strong></td>
            <td>A</td>
            <td>&nbsp;&nbsp;<strong>CUSTODIA:</strong></td>
            <td>B</td>
        </tr>
        <tr>
            <td colspan="3" style="font-size:6px">Almacenar a temperatura menor a 5 �C</td>
            <td colspan="3" style="font-size:6px">Almacenar a temperatura menor a -20 �C</td>
        </tr>
        <tr>
            <td colspan="3" style="font-size:16px" align="center"><br><br><?= $cod_int ?> &nbsp; CG</td>
            <td colspan="3" style="font-size:16px" align="center"><br><br><?= $cod_int ?> &nbsp; CL</td>
        </tr>
    </table>
    <br/><br/><br/><br/><br/>
    <?php
}
?>
<script>window.print()</script>
</body>
</html>