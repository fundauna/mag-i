<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _formulaciones_arq();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <script>
        function Limpia() {
            document.getElementById('tmp').value = '';
            document.getElementById('usuario').value = '';
        }
    </script>
</head>
<body>
<?php $Gestor->Incluir('n21', 'hr', 'Servicios :: Arqueo de Formulaciones vs. I.A.') ?>
<?= $Gestor->Encabezado('N0021', 'e', 'Arqueo de Formulaciones vs. I.A.') ?>
<br/>
<table class="radius" align="center">
    <tr>
        <td class="titulo" colspan="3">Filtro</td>
    </tr>
    <tr>
        <td><select id="tipo" onchange="CambiaTipo()">
                <option value="3">Ingredientes Activos</option>
                <option value="0">Impurezas</option>
            </select>
        </td>
        <td><input type="text" id="elementos0" name="elementos" class="lista2" readonly onclick="ElementosLista(0)"/>
            <input type="hidden" id="codigoB0" name="codigoB"/>
        </td>
        <td>
            <input type="button" id="btn_buscar" value="Buscar" class="boton2" onClick="AutoBuscar();">
        </td>
    </tr>
</table>
<br/>
<br>
<table class="radius" align="center">
    <tr>
        <td class="titulo">Formulaciones Disponibles</td>
        <td class="titulo">Formulaciones Asignadas</td>
    </tr>
    <tr>
        <td>
            <select id="permisos" style="width:400px;" size="10">
                <?php
                $ROW = $Gestor->FormulacionesMuestra();
                for ($x = 0; $x < count($ROW); $x++) {
                    echo "<option value='{$ROW[$x]['id']}'>{$ROW[$x]['nombre']}</option>";
                }
                ?>
            </select>
        </td>
        <td id="td_asignados">
            <select id="asignados" style="width:400px;" size="10"></select>
        </td>
    </tr>
    <tr>
        <td><input id="btn_agregar" type="button" value="Agregar" class="boton" onClick="PermisosAgrega();" disabled>
        </td>
        <td><input id="btn_eliminar" type="button" value="Eliminar" class="boton" onClick="PermisosElimina();" disabled>
        </td>
    </tr>
</table>
<?= $Gestor->Encabezado('N0021', 'p', '') ?>
</body>
</html>