<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _solicitud01_imprimir();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) {
    die('Registro inexistente');
}

$ROW2 = $Gestor->SolicitudLineas();
if ($ROW[0]['proposito'] == '2') {
    $ROW[0]['proposito'] = "Detecci&oacute;n de Residuos";
}
if ($ROW[0]['proposito'] == '3') {
    $ROW[0]['proposito'] = "Importaci&oacute;n";
}
if ($ROW[0]['proposito'] == '4') {
    $ROW[0]['proposito'] = "Ronda intercomparativa";
}
if ($ROW[0]['proposito'] == '5') {
    $ROW[0]['proposito'] = "Otro";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body style="background-image: none; background-color: #FFF;">
<center>
    <?php $Gestor->Incluir('n13', 'hr', 'Servicios :: Solicitud de An&aacute;lisis') ?>
    <?= $Gestor->Encabezado('N0013', 'e', 'Solicitud de An&aacute;lisis') ?>
    <br>
    <table class="radius" style="font-size:18px" width="98%" border="2" cellpadding="0" cellpadding="10" rules="none">
        <tr align="center">
            <td><strong>SOLICITUD DE AN&Aacute;LISIS:</strong>&nbsp;&nbsp;&nbsp;&nbsp;<?= $ROW[0]['cs'] ?></td>
            <td><strong>FECHA:</strong>&nbsp;&nbsp;&nbsp;&nbsp;<?= $ROW[0]['fecha'] . ' ' . $ROW[0]['hora'] ?></td>
        </tr>
    </table>
    <br>

    <table class="radius" width="98%" border="2" cellpadding="10" rules="groups">
        <td>
            <table class="radius" width="98%" border="0" cellpadding="9" rules="none">
                <tr align="right">
                    <td colspan="2"><strong>CLIENTE:</strong>
                    <td align="left"><?= $ROW[0]['nomcliente'] ?></td>
                    <td align="right"><strong>SOLICITANTE:</strong></td>
                    <td colspan="2" align="center"><?= $ROW[0]['nomsolicitante'] ?></td>
                </tr>
                <tr align="right">
                    <td colspan="2"><strong>PROP&Oacute;SITO AN&Aacute;LISIS:</strong></td>
                    <td align="left"><?= $ROW[0]['proposito'] ?></td>
                    <td><strong>DIRECCI&Oacute;N:</strong></td>
                    <td colspan="2" align="center"><?= $ROW[0]['direccion'] ?></td>
                </tr>
                <tr align="right">
                    <td colspan="2"><strong>MUESTREO:</strong></td>
                    <td align="left"><?= $ROW[0]['muestreo'] ?></td>
                    <td><strong>TIEMPO DE ENTREGA(d&iacute;as):</strong></td>
                    <td colspan="2" align="center">  <?= $ROW[0]['entregacliente'] ?> d&iacute;as</td>
                </tr>
            </table>

            <table class="radius" width="98%" border=0px cellpadding="10" rules="none">
                <tr>
                    <td align="right" colspan="2"><strong>N&Uacute;MERO DE MUESTRAS:</strong></td>
                    <td align="center"><?= count($ROW2) ?></td>

                    <td align="right"><strong>N&Uacute;MERO DE LOTE:</strong></td>
                    <td align="center"> <?= substr_replace($ROW[0]['cs'], '', 3, 1) ?></td>

                    <td align="center"><strong>AN&Aacute;LISIS SOLICITADO:</strong></td>
                    <?php if ($ROW2[0]['analisis'] == '130') {
                        $ROW2[0]['analisis'] = "Multi-Residual";
                    } else if ($ROW2[0]['analisis'] == '132') {
                        $ROW2[0]['analisis'] = "Amonio Cuaternarios";
                    } else if ($ROW2[0]['analisis'] == '133') {
                        $ROW2[0]['analisis'] = "Otro";
                    } else if ($ROW2[0]['analisis'] == '131') {
                        $ROW2[0]['analisis'] = "Un Solo Analito";
                    } else {
                        $ROW2[0]['analisis'] = "Desconocido";
                    }
                    ?>
                    <td align="left"><?= $ROW2[0]['analisis'] ?></td>
                </tr>
            </table>
        </td>
    </table>
    <br/>
    <table width="98%" border="3px" cellpadding="8" cellspacing="0">
        <tr style="background:#aaa">
            <td align="center"><strong>MUESTRA</strong></td>
            <td align="center"><strong>C&Oacute;DIGO <br/> EXTERNO</strong></td>
            <td align="center"><strong>PRODUCTO</strong></td>
            <td align="center"><strong>FECHA DE<br/> MUESTREO</strong></td>
            <td align="center"><strong>HORA DE </br>MUESTREO</strong></td>
            <!-- <td align="center"><strong>AN&Aacute;LISIS<br/>SOLICITADO</strong></td>-->
            <td align="center"><strong>PARTE A<br/>ANALIZAR</strong></td>
            <td align="center"><strong>FORMA QUE SE<br/>RECIBI&Oacute;</strong></td>
            <td align="center"><strong>PRESENTACI&Oacute;N DE LA<br/>MUESTRA</strong></td>
            <td align="center"><strong>ESTADO DEL<br/>EMPAQUE DE LA<br>MUESTRA </strong></td>
            <td align="center"><strong>N&Uacute;MERO DE <br>INFORME<br>ASIGNADO</strong></td>
        </tr>
        <?php

        for ($x = 0; $x < count($ROW2); $x++) {
            $color = '';
            if ($ROW2[$x]['parte'] == '0') {
                $ROW2[$x]['parte'] = "&Iacute;ntegra";
            }
            if ($ROW2[$x]['parte'] == '1') {
                $ROW2[$x]['parte'] = "C&aacute;scara";
            }
            if ($ROW2[$x]['parte'] == '2') {
                $ROW2[$x]['parte'] = "Pulpa";
            }
            if ($ROW2[$x]['forma'] == '0') {
                $ROW2[$x]['forma'] = "Entero";
            }
            if ($ROW2[$x]['forma'] == '1') {
                $ROW2[$x]['forma'] = "En cuartos";
            }
            if ($ROW2[$x]['forma'] == '2') {
                $ROW2[$x]['forma'] = "Rebanadas";
            }
            if ($ROW2[$x]['forma'] == '3') {
                $ROW2[$x]['forma'] = "Podrida";
            }
            if ($ROW2[$x]['forma'] == '4') {
                $ROW2[$x]['forma'] = "Golpeada";
            }
            if ($ROW2[$x]['forma'] == '5') {
                $ROW2[$x]['forma'] = "Otro";
            }
            if ($ROW2[$x]['presentacion'] == '0') {
                $ROW2[$x]['presentacion'] = "Congelada";
            }
            if ($ROW2[$x]['presentacion'] == '1') {
                $ROW2[$x]['presentacion'] = "Descongelada";
            }
            if ($ROW2[$x]['presentacion'] == '2') {
                $ROW2[$x]['presentacion'] = "Fresca, no refrigerada";
            }
            if ($ROW2[$x]['presentacion'] == '3') {
                $ROW2[$x]['presentacion'] = "Fresca refrigerada";
            }
            if ($ROW2[$x]['empaque'] == '0') {
                $ROW2[$x]['empaque'] = "Bolsa Intacta";
            }
            if ($ROW2[$x]['empaque'] == '1') {
                $ROW2[$x]['empaque'] = "Bolsa da&ntilde;ada";
            }
            if ($ROW2[$x]['empaque'] == '2') {
                $ROW2[$x]['empaque'] = "Botella vidrio ambar";
            }
            if ($ROW2[$x]['empaque'] == '3') {
                $ROW2[$x]['empaque'] = "Otro";
            }
            if ($ROW[0]['estado'] != '' and $ROW[0]['estado'] != '5') {
                $cod_int = str_replace('LRE-', '', $ROW[0]['cs']) . '-' . ($x + 1);
                $cod_int = str_replace(' ', '', $cod_int);
                $cod_int = substr_replace($cod_int, '', 2, 2);


                if ($cod_int == $_GET['ref']) {
                    $color = 'style="background-color:#FF9933"';
                }
                //$cod_int = $ROW[0]['lote'].'-'.($x+1);
            } else {
                $cod_int = '';
            }
            ?>
            <tr <?= $color ?>>
                <td align="center" style="border-right:#FFF"><?= $ROW2[$x]['conSolicitud'] ?></td>
                <td align="center" style="border-right:#FFF;border-left:#FFF"><?= $ROW2[$x]['codigo'] ?></td>
                <td align="center"
                    style="border-right:#FFF;border-left:#FFF"><?= $ROW2[$x]['nombre'] . ", " . $ROW2[$x]['tipo'] ?></td>
                <td align="center" style="border-right:#FFF;border-left:#FFF"><?= $ROW2[$x]['fecha'] ?></td>
                <td align="center" style="border-right:#FFF;border-left:#FFF"><?= $ROW2[$x]['hora'] ?></td>
                <!--  <td align="center" style="border-right:#FFF;border-left:#FFF"><?= $ROW2[$x]['nom_analisis'] ?></td>-->
                <td align="center" style="border-right:#FFF;border-left:#FFF"><?= $ROW2[$x]['parte'] ?></td>
                <td align="center" style="border-right:#FFF;border-left:#FFF"><?= $ROW2[$x]['forma'] ?></td>
                <td align="center" style="border-right:#FFF;border-left:#FFF"><?= $ROW2[$x]['presentacion'] ?></td>
                <td align="center" style="border-right:#FFF;border-left:#FFF"><?= $ROW2[$x]['empaque'] ?></td>
                <td align="center" style="border-left:#FFF"><?= $ROW2[$x]['nia'] ?></td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td colspan="12" align="center"><strong>---- &Uacute;ltima L&iacute;nea ----</strong></td>
        </tr>
        <?php for ($x = 0; $x < (1 - count($ROW2)); $x++) { ?>
            <tr>
                <td colspan="10" align="center"><br></td>
            </tr>
        <?php } ?>
    </table>
</center>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OBSERVACIONES:
<?php
for ($x = 0; $x < count($ROW2); $x++) {


    $cod_int = str_replace('LRE-', '', $ROW[0]['cs']) . '-' . ($x + 1);
    $cod_int = str_replace(' ', '', $cod_int);
    $cod_int = substr_replace($cod_int, '', 2, 2);


    if ($ROW2[$x]['presentacion'] == '0') {
        $ROW2[$x]['presentacion'] = "Congelada";
    }
    if ($ROW2[$x]['presentacion'] == '1') {
        $ROW2[$x]['presentacion'] = "Descongelada";
    }
    if ($ROW2[$x]['presentacion'] == '2') {
        $ROW2[$x]['presentacion'] = "Fresca, no refrigerada";
    }
    if ($ROW2[$x]['presentacion'] == '3') {
        $ROW2[$x]['presentacion'] = "Fresca refrigerada";
    }
    $obs = '';
    if ($ROW2[$x]['observaciones'] != '') {
        $obs = ", (" . $ROW2[$x]['observaciones'] . ")";
    }
    echo "La muestra " . $cod_int . " viene " . $ROW2[$x]['presentacion'] . ". FRF:" . $ROW2[$x]['obsfrf'] . "" . $obs . ". ";
}
?>
<center>
    <br/>
    <table style="font-size:12px" width="98%">
        <?php
        $ROW2 = $Gestor->Historial();
        $recibido = $aprobado = '';
        for ($x = 0; $x < count($ROW2); $x++) {
            if ($ROW2[$x]['accion'] == 0) {
                $recibido = $ROW2[$x]['nombre'];
            }
            if ($ROW2[$x]['accion'] == 2) {
                $aprobado = $ROW2[$x]['nombre'];
            }
        }
        ?>
        <tr>
            <td align="center"><br><br><br>Firma:_________________________</td>
            <td align="center"><br><br><br>Firma:_________________________</td>
            <td align="center"><br><br><br>Firma:_________________________</td>
        </tr>
        <tr>
            <td align="center">Entregadas por: <?= $ROW[0]['entregado'] ?></td>
            <td align="center">Recibido por: <?= $recibido ?></td>
            <td align="center">Aprobado por: <?= $aprobado ?></td>
        </tr>
    </table>
    <br>
    <br/>
    <input type="button" value="Imprimir" class="boton2" onClick="window.print()">
</center>
<?= $Gestor->Encabezado('N0013', 'p', '') ?>
</body>
</html>