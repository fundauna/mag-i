<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _LRE_hoja1();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

$ROW2 = $Gestor->SolicitudLineas();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body style="background-image: none; background-color: #FFF">
<center>
    <?php $Gestor->Incluir('n6', 'hr', 'Servicios :: Hoja de trazabilidad para la homogenizaci&oacute;n de muestras') ?>
    <?= $Gestor->Encabezado('N0006', 'e', 'Hoja de trazabilidad para la homogenizaci&oacute;n de muestras') ?>

    <br>
    <table class="tableImpresa" width="98%" rules="none" border="0">
        <tr>
            <td width="20%"><strong>Fecha de ingreso:</strong></td>
            <td><?= $ROW[0]['fecha'] ?></td>

            <td width="26%"><strong>Tiempo de entrega de resultados:</strong></td>
            <td width="20%"><?= $ROW[0]['entregaanalista'] ?> d&iacute;a</td>
            <td align="right"><b style="font-size: 20px"><?= $_GET['ID'] ?></b></td>
        </tr>
        <tr>
            <td><strong>Lote:</strong></td>
            <td style="font-size: 15px"><?=
                $cod_int = str_replace('-0', '-', $_GET['ID']);
                ?></td>

            <td><strong>N&uacute;mero de muestras:</strong></td>
            <td><?= count($ROW2) ?></td>
            <td></td>
        </tr>
        <tr>
            <td><strong>An&aacute;lisis Solicitado:</strong></td>
            <td width="25%"><?= $ROW2[0]['nom_analisis'] ?></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="5"></td>
        </tr>

    </table>
    <br/>

    <table class="tableImpresa" border="1" bordercolor="#000000" cellpadding="0" cellspacing="0" width="98%" rules="all">
        <tr align="center">
            <td rowspan="3" width="100px"><strong>Muestra</strong></td>
            <td rowspan="3" width="100px"><strong>Matriz</strong></td>
            <td rowspan="3" width="35px"><strong>Grupo</strong></td>
            <td colspan="7" ><strong>Homogenizaci&oacute;n</strong></td>
        </tr>
        <tr align="center">

            <td rowspan="2" width="90px"><strong>Fecha</strong></td>
            <td rowspan="2" width="90px"><strong>Hora</strong></td>
            <td rowspan="2" width="90px"><strong>Equipo</strong></td>
            <td rowspan="2" width="90px"><strong>Cuarteo</strong></td>
            <td rowspan="2" width="100px"><strong>Responsable</strong></td>
            <td width="100px"><strong>Masa muestra, g</strong></td>
            <td rowspan="2" width="295px"><strong>Observaciones</strong></td>
        </tr>
        <td  width="100px" align="center"><strong>Masa semilla,g</strong></td>

        <tr>

        </tr>
        <?php
        for ($x = 0; $x < count($ROW2); $x++) {
           $cod_int = $_GET['ID'] ;

            $cod_int = str_replace('-0', '', $cod_int);
            if($x < 9 ){
                $cod_int = $cod_int. '-' .'0'.($x + 1);
            } else {
                $cod_int = $cod_int . '-' . ($x + 1);
            }
            ?>
            <tr style="font-size: 10px;">
                <td align="center" rowspan="2"><?= $cod_int ?></td>
                <td align="center" rowspan="2"><?= $ROW2[$x]['nombre'].", ".$ROW2[$x]['tipo'] ?></td>
                <td align="center" rowspan="2"><?= str_replace('-','', substr($ROW2[$x]['nombreGrupo'],0,3)) ?></td>
                <td rowspan="2">&nbsp;</td>
                <td rowspan="2">&nbsp;</td>
                <td rowspan="2"> &nbsp;&nbsp;&nbsp;EQ-</td>
                <td rowspan="2"><input type="checkbox"/>S&iacute;<br/><input type="checkbox"/>No</td>
                <td rowspan="2">&nbsp;</td>
                <td>&nbsp;</td>
                <td rowspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>


            <?php
        }


        ?>
    </table>
    <table border="0" bordercolor="#000000" cellpadding="3" cellspacing="0" width="98%">
        <tr>
            <td colspan="9">
                G1A: Alto contenido en agua  y clorofila, G1B: Alto contenido de agua y contenido escaso o ausencia de clorofila, G2A: Alto contenido de aceite o grasa y muy bajo contenido de agua,
                G2B:  Alto contenido de aceite o grasa e intermedio contenido de agua, G3:  Alto contenido de &aacute;cido y agua,
                G4: Alto contenido de almid&oacute;n y/o prote&iacute;na y bajo contenido de agua y grasa, G5: Productos especiales o dif&iacute;ciles,G6: Alto contenido de az&uacute;car y bajo contenido de agua

            </td>
        </tr>
        <tr>
            <td colspan="9" align="left">
                <br/>
                Observaciones:<hr>
                <br/>
                <hr>
                <br/>
                <hr>
                <br/>
            </td>
        </tr>
    </table>

<center>
    <br/>
    <br/>
    <div align="center">&Uacute;ltima L&iacute;nea</div>
    <br/>
    <br/>
    <br/>
    <input type="button" value="Imprimir" class="boton2" onClick="window.print()">

</center>
<?= $Gestor->Encabezado('N0006', 'p', '') ?>
</body>
</html>