<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _cambiar_estado();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('n2', 'hr', 'Servicios :: Retirar muestras') ?>
<?= $Gestor->Encabezado('N0002', 'e', 'Retirar muestras') ?>
<center>
    <br/>
    <div id="container" style="width:550px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>No.</th>
                <th>Solicitud</th>
                <th>Estado</th>
                <th>Pasar a-></th>
                <th><input type="checkbox" onclick="Marca(this)"/></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->Muestras();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#"
                           onclick="MuestraHistorial('<?= $ROW[$x]['id'] ?>', '<?= $ROW[$x]['solicitud'] ?>')"><?= $ROW[$x]['ref'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['solicitud'] ?></td>
                    <td><?= $Gestor->Estado($ROW[$x]['estado']) ?></td>
                    <?php
                    if ($ROW[$x]['estado'] == '1') {
                        $txt = 'Reporte';/*$valor = '2';*/
                    }
                    if ($ROW[$x]['estado'] == '2') {
                        $txt = 'Por retirar';
                        $valor = '3';
                    }
                    if ($ROW[$x]['estado'] == '3') {
                        $txt = 'Retirada';
                        $valor = '4';
                    }
                    ?>
                    <td><?= $txt ?></td>
                    <td>
                        <?php if ($ROW[$x]['estado'] == '3') { ?>
                            <input type="checkbox" name="seleccionados" value="<?= $ROW[$x]['id'], ';', $valor ?>"/>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/>
    <input type="button" value="Retirar" class="boton" onclick="Cambiar()"/>
</center>
<?= $Gestor->Encabezado('N0002', 'p', '') ?>
</body>
</html>