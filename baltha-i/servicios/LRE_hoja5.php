<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _LRE_hoja5();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) {
    die('Registro inexistente');
}

$ROW2 = $Gestor->SolicitudLineas();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <script>
        var _oculta = false;

        function Oculta() {
            total = document.getElementsByName('dummy').length;
            for (x = 0; x < total; x++) {
                document.getElementById('tdA' + x).innerHTML = '<br>&nbsp;';
                document.getElementById('tdB' + x).innerHTML = '';
                document.getElementById('tdC' + x).innerHTML = '';
            }
        }
    </script>
</head>
<body style="background-image: none; background-color: #FFF">
<center>
    <table class="radius" style="font-size:12px" width="98%">
        <tr align="center">
            <td>
                <table align="center" width="100%" border="1" bordercolor="#000000" cellpadding="0" cellspacing="0"
                       style="font-size:12px">
                    <tr align="center">
                        <td colspan="2"><?php $Gestor->Incluir('sfe_logo', 'jpg'); ?></td>
                        <td><strong>Ministerio de Agricultura y Ganader&iacute;a<br/>Servicio Fitosanitario del
                                Estado<br/>Laboratorio de An&aacute;lisis de Residuos de Agroqu&iacute;micos</strong>
                        </td>
                        <td colspan="2"><?php $Gestor->Incluir('mag', 'png'); ?>
                            &nbsp;&nbsp;<?php $Gestor->Incluir('agro', 'png'); ?></td>
                    </tr>
                    <tr align="center">
                        <td>R-01-LAB-LRE-PT-04</td>
                        <td>Rige a partir de:<br>16-02-2017</td>
                        <td><strong>Hoja de trazabilidad para la determinaci&oacute;n del porcentaje en muestra del
                                suelo</strong></td>
                        <td><font size='-2'>P&aacute;gina 1 de 1</font></td>
                        <td><font size='-2'>Versi&oacute;n: 01</font></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br>
    <table class="radius" width="98%">
        <tr>
            <td><strong>Fecha de ingreso:</strong></td>
            <td><?= $ROW[0]['fecha'] ?></td>
            <td><strong>Tiempo de entrega de resultados:</strong></td>
            <td><?= $ROW[0]['entregacliente'] ?> d&iacute;as</td>
            <td align="right"><b style="font-size: 14px"><?= $_GET['ID'] ?></b></td>
        </tr>
        <tr>
            <td><strong>Lote:</strong></td>
            <td><?= $_GET['ID'] ?></td>
            <td><strong>N&uacute;mero de muestras:</strong></td>
            <td><?= count($ROW2) ?></td>
            <td></td>
        </tr>
        <tr>
            <td><strong>An&aacute;lisis solciitados:</strong></td>
            <td><?= $ROW2[0]['nom_analisis'] ?></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <br/>
    <table width="98%" border="1" bordercolor="#000000" cellpadding="0" cellspacing="0">
        <tr align="center">
            <td rowspan="2"><strong>Muestra</td>
            <td rowspan="2"><strong>Matriz</strong></td>
            <td><strong>Fecha</strong></td>
            <td rowspan="2"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Equipo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
            <td rowspan="2"><strong>Porcentaje de <br> Humedad <br>(% MC)</strong></td>
            <td rowspan="2"><strong>Porcentaje de <br> suelo seco <br> (%)</strong></td>
            <td rowspan="2"><strong>Masa de suelo <br> seco <br> (g)</strong></td>
            <td rowspan="2"><strong>Observaciones</strong></td>
        </tr>
        <tr align="center">
            <td><strong>Masa de suelo h&uacute;medo (g)</strong></td>
        </tr>
        <?php
        for ($x = 0; $x < count($ROW2); $x++) {
            $cod_int = $_GET['ID'] . '-' . ($x + 1);
            if (strpos(strtoupper($ROW2[$x]['nombre']), 'SUELO') === 0) {
                ?>
                <tr>
                    <td align="center"><?= $cod_int ?></td>
                    <td align="center"><?= $ROW2[$x]['nombre'] ?></td>
                    <td><br>
                        <hr>
                        <br></td>
                    <td>EQ-</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <input type="hidden" name="dummy"/>
                </tr>
                <?php
            }
        }
        if ($x < 12) {
            for (; $x < 12; $x++) {
                ?>
                <tr>
                    <td width="120px"></td>
                    <td width="120px"></td>
                    <td width="256px"><br>
                        <hr>
                        <br></td>
                    <td width="80px"></td>
                    <td width="80px">&nbsp;</td>
                    <td width="80px">&nbsp;</td>
                    <td width="80px">&nbsp;</td>
                    <td>&nbsp;</td>
                    <input type="hidden" name="dummy"/>
                </tr>
                <?php
            }
        }
        ?>
    </table>
</center>
</br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Observaciones:_____________________________________________________________________________________________________________________________________________________________________________________________________________________________________
<br>
<table border="1" cellpadding="0" cellspacing="0" width="98%">
    <tr>
        <td>
            <b>Elaborado por:</b> Analista Qu&iacute;mico<br>
            <b>Nombre:</b> Juan Carlos Murillo Gonzalez<br>
            <b>Fecha:</b> 21-12-2017
        </td>
        <td>
            <b>Revisado por:</b> Gestora de Calidad de Laboratorios<br>
            <b>Nombre:</b> Kattia Murillo Alfaro<br>
            <b>Fecha:</b> 22-12-2017
        </td>
        <td>
            <b>Aprobado por:</b> Jefe LRE<br>
            <b>Nombre:</b> Roger Ruiz Zapata<br>
            <b>Fecha:</b> 22-12-2017
        </td>
    </tr>
</table>
<center>
    <style>
        .Rotate-90 {
            -webkit-transform: rotate(-90deg);
            -moz-transform: rotate(-90deg);
            -ms-transform: rotate(-90deg);
            -o-transform: rotate(-90deg);
            transform: rotate(-90deg);

            -webkit-transform-origin: 50% 50%;
            -moz-transform-origin: 50% 50%;
            -ms-transform-origin: 50% 50%;
            -o-transform-origin: 50% 50%;
            transform-origin: 50% 50%;

            font-size: 12px;
            position: relative;
        }
    </style>

    Plaguicidas que se deben eliminar del informe<br><br>
    <br/>
    <table border="1" bordercolor="#000000" cellpadding="0" cellspacing="0" width="98%">
        <tr align="center">
            <td><strong></td>
            <td><strong>RECUPERACI&Oacute;N*</strong></td>
            <td><strong>CONTROL**</strong></td>
        </tr>
        <tr>
            <td align="center" width="50%"><b>CL</b></td>
            <td><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></td>
            <td></td>
        </tr>
        <tr>
            <td align="center" width="50%"><b>CG</b></td>
            <td><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></td>
            <td></td>
        </tr>
    </table>
</center>
<font size="-2">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Los plaguicidas indicados en la columna de RECUPERACI&oacute;N se deben sacar a
    todas las mustras del lote.<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Las indicadas en la columna CONTROL aplica:
    <ul>
        <li>En CG para todas las muestras del lote.</li>
        <li>En CL solamente para el n&uacute;mero de muestras indicado.</li>

    </ul>
</font>
<br/>
    <br/>

    <input type="button" value="Imprimir" class="boton2" onClick="window.print()">
</center>
</body>
</html>