<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _muestras_asignar();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="_TIPO"/> <!-- PARA SABER SI ESTOY ASIGNANDO O REASIGNANDO -->
<?php $Gestor->Incluir('n10', 'hr', 'Servicios :: Asignar analistas') ?>
<?= $Gestor->Encabezado('N0010', 'e', 'Asignar analistas') ?>
<center>
    Muestras por asignar
    <div id="container" style="width:80%">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>No. Muestra</th>
                <th>Solicitud</th>
                <th>Fecha ingreso</th>
                <th><?= $Gestor->obtieneLabel(1) ?></th>
                <th><?= $Gestor->obtieneLabel(2) ?></th>
                <th><?= $Gestor->obtieneLabel(3) ?></th>
                <th><?= $Gestor->obtieneLabel(4) ?></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->Muestras('');
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><?=    $mu = str_replace('--0', '', str_replace('-1 ', '-01', str_replace('-2 ', '-02', str_replace('-3 ', '-03', str_replace('-4 ', '-04', str_replace('-5 ', '-05', str_replace('-6 ', '-06', str_replace('-7 ', '-07',
                            str_replace('-8 ', '-08', str_replace('-9 ', '-09', $ROW[$x]['ref']))))))))));

                        ?></td>

                    <td><a href="#"
                           onclick="SolicitudesImprime('<?= $ROW[$x]['solicitud'] ?>', '<?= $_POST['LID'] ?>', '<?= $ROW[$x]['ref'] ?>')"><?= $ROW[$x]['solicitud'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['fecha1'] ?></td>
                    <td><input type="text" id="tmp<?= $x ?>" class="lista" readonly
                               onclick="UsuariosLista('<?= $ROW[$x]['id'] ?>', '<?= $x ?>', 0, 'tmp', 'usuario')"/><input
                                type="hidden" id="usuario<?= $x ?>"/></td>
                    <td><input type="text" id="tmp2<?= $x ?>" class="lista" readonly
                               onclick="UsuariosLista('<?= $ROW[$x]['id'] ?>', '<?= $x ?>', 0, 'tmp2', 'usuario2')"/><input
                                type="hidden" id="usuario2<?= $x ?>"/></td>
                    <td><input type="text" id="tmp3<?= $x ?>" class="lista" readonly
                               onclick="UsuariosLista('<?= $ROW[$x]['id'] ?>', '<?= $x ?>', 0, 'tmp3', 'usuario3')"/><input
                                type="hidden" id="usuario3<?= $x ?>"/></td>
                    <td><input type="text" id="tmp4<?= $x ?>" class="lista" readonly
                               onclick="UsuariosLista('<?= $ROW[$x]['id'] ?>', '<?= $x ?>', 0, 'tmp4', 'usuario4')"/><input
                                type="hidden" id="usuario4<?= $x ?>"/></td>
                    <td><img id="imgA<?= $x ?>" onclick="MuestraAsigna('<?= $ROW[$x]['id'] ?>', '<?= $x ?>')"
                             src="<?php $Gestor->Incluir('mod', 'bkg') ?>" title="Asignar" class="tab3"/>
                        <img id="imgB<?= $x ?>" src="<?php $Gestor->Incluir('aprobar', 'bkg') ?>" title="Asignada"
                             class="tab3" style="display:none"/>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <?php
    $ROW = $Gestor->Muestras(1);
    if ($ROW) {
        ?>
        <br/><br/>
        <table class="radius" width="80%" align="center">
            <tr>
                <td class="titulo" colspan="11">Muestras asignadas</td>
            </tr>
            <tr>
                <th rowspan="2">No. Muestra</th>
                <th rowspan="2">Solicitud</th>
                <th rowspan="2"><?= $Gestor->obtieneLabel(1) ?></th>
                <th rowspan="2"><?= $Gestor->obtieneLabel(2) ?></th>
                <th rowspan="2"><?= $Gestor->obtieneLabel(3) ?></th>
                <th rowspan="2"><?= $Gestor->obtieneLabel(4) ?></th>
                <th colspan="4">Reasignar a:</th>
                <th rowspan="2"></th>
            </tr>
            <tr>
                <th><?= $Gestor->obtieneLabel(1) ?></th>
                <th><?= $Gestor->obtieneLabel(2) ?></th>
                <th><?= $Gestor->obtieneLabel(3) ?></th>
                <th><?= $Gestor->obtieneLabel(4) ?></th>
            </tr>
            <tbody>
            <?php
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr align="center">
                    <td>20<?= $ROW[$x]['ref'] ?></td>
                    <td><a href="#"
                           onclick="SolicitudesImprime('<?= $ROW[$x]['solicitud'] ?>', '<?= $_POST['LID'] ?>', '<?= $ROW[$x]['ref'] ?>')"><?= $ROW[$x]['solicitud'] ?></a>
                    </td>
                    <td><?= $Gestor->obtieneNombre($ROW[$x]['usuario']) ?></td>
                    <td><?= $Gestor->obtieneNombre($ROW[$x]['usuario2']) ?></td>
                    <td><?= $Gestor->obtieneNombre($ROW[$x]['usuario3']) ?></td>
                    <td><?= $Gestor->obtieneNombre($ROW[$x]['usuario4']) ?></td>
                    <td>
                        <input type="text" id="Rtmp1<?= $x ?>" class="lista" readonly
                               onclick="UsuariosLista('<?= $ROW[$x]['id'] ?>', '<?= $x ?>', 1, 'Rtmp1', 'Rusuario1')"/>
                        <input type="hidden" id="Rusuario1<?= $x ?>"/>
                    </td>
                    <td>
                        <input type="text" id="Rtmp2<?= $x ?>" class="lista" readonly
                               onclick="UsuariosLista('<?= $ROW[$x]['id'] ?>', '<?= $x ?>', 1, 'Rtmp2', 'Rusuario2')"/>
                        <input type="hidden" id="Rusuario2<?= $x ?>"/>
                    </td>
                    <td>
                        <input type="text" id="Rtmp3<?= $x ?>" class="lista" readonly
                               onclick="UsuariosLista('<?= $ROW[$x]['id'] ?>', '<?= $x ?>', 1, 'Rtmp3', 'Rusuario3')"/>
                        <input type="hidden" id="Rusuario3<?= $x ?>"/>
                    </td>
                    <td>
                        <input type="text" id="Rtmp4<?= $x ?>" class="lista" readonly
                               onclick="UsuariosLista('<?= $ROW[$x]['id'] ?>', '<?= $x ?>', 1, 'Rtmp4', 'Rusuario4')"/>
                        <input type="hidden" id="Rusuario4<?= $x ?>"/>
                    </td>
                    <td><img id="RimgA<?= $x ?>" onclick="MuestraReasigna('<?= $ROW[$x]['id'] ?>', '<?= $x ?>')"
                             src="<?php $Gestor->Incluir('mod', 'bkg') ?>" title="Asignar" class="tab3"/>
                        <img id="RimgB<?= $x ?>" src="<?php $Gestor->Incluir('aprobar', 'bkg') ?>" title="Asignada"
                             class="tab3" style="display:none"/>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <?php
    }//IF
    ?>
</center>
<?= $Gestor->Encabezado('N0010', 'p', '') ?>
</body>
</html>