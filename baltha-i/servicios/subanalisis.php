<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _subanalisis();
?>
    <!DOCTYPE html PUBLIC
            "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <title><?= $Gestor->Title() ?></title>
        <?php $Gestor->Incluir('estilo', 'css'); ?>
        <?php $Gestor->Incluir('window', 'js'); ?>
        <?php $Gestor->Incluir('validaciones', 'js'); ?>
        <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
        <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    </head>
<body>
<?php $Gestor->Incluir('n18', 'hr', 'Servicios :: An&aacute;lisis Internos') ?>
<?= $Gestor->Encabezado('N0018', 'e', 'An&aacute;lisis Internos') ?>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" style="width:255px">
            <tr>
                <td class="titulo" colspan="2">Filtro</td>
            </tr>
            <tr>
                <td><select id="tipo" name="tipo">
                        <?php if ($_POST['LID'] == '1') { ?>
                            <option value="9" <?php if ($_POST['tipo'] == '9') echo 'selected'; ?>>Sub-an&aacute;lisis
                            </option>
                            <option value="8" <?php if ($_POST['tipo'] == '8') echo 'selected'; ?>>Analitos</option>
                        <?php } elseif ($_POST['LID'] == '2') { ?>
                            <option value="4" <?php if ($_POST['tipo'] == '4') echo 'selected'; ?>>M&eacute;todos de
                                laboratorio
                            </option>
                            <option value="5" <?php if ($_POST['tipo'] == '5') echo 'selected'; ?>>T&eacute;cnicas de an&aacute;lisis</option>
                            <option value="6" <?php if ($_POST['tipo'] == '6') echo 'selected'; ?>>Plagas</option>
                            <option value="7" <?php if ($_POST['tipo'] == '7') echo 'selected'; ?>>Tipos de material
                            </option>
                        <?php } elseif ($_POST['LID'] == '3') { ?>
                            <option value="4">Impurezas fertilizantes</option>
                            <option value="0" <?php if ($_POST['tipo'] == '0') echo 'selected'; ?>>Impurezas plaguicidas
                            </option>
                            <option value="1" <?php if ($_POST['tipo'] == '1') echo 'selected'; ?>>Elementos
                                fertilizantes
                            </option>
                            <option value="2" <?php if ($_POST['tipo'] == '2') echo 'selected'; ?>>Sub-an&aacute;lisis
                                plaguicidas
                            </option>
                            <option value="3" <?php if ($_POST['tipo'] == '3') echo 'selected'; ?>>Ingredientes activos
                            </option>
                        <?php } ?>
                    </select></td>
                <td><input type="button" value="Buscar" class="boton2" onclick="TiposBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
<table class="radius" align="center" width="255px">
    <tr>
        <td class="titulo" colspan="2">Registros</td>
    </tr>
    <tr>
    <td colspan="2">
    <select size="10" style="width:250px" onclick="marca1(this);">
<?php
if ($_POST['tipo'] == '8') {
    $ROW = $Gestor->Registros1();
    for ($x = 0; $x < count($ROW); $x++) {
        echo "<option id='{$ROW[$x]['id']}' nombre='{$ROW[$x]['nombre']}' tipoCroma='{$ROW[$x]['tipoCroma']}'>{$ROW[$x]['nombre']}</option>";
    }
    ?>

    </select>
    </td>
    </tr>
    </table>
    <br/>
    <table class="radius" align="center" width="255px">
        <tr>
            <td class="titulo" colspan="2">Detalle</td>
        </tr>
        <input type="hidden" id="id">
        <tr>
            <?php
            if ($_POST['tipo'] != '') { ?>
                <td><b>Nombre:</b></td>
                <td><input type="text" id="nombre" size="25" maxlength="50"></td>
            <?php } else { ?>
                <td><b>Nombre:</b></td>
                <td><input type="text" id="nombre" size="25" maxlength="50" disabled></td>
            <?php } ?>
        </tr>
        <!-- muestra o oculta el combo de tecnicas en caso de ser analitos -->
        <?php
        if ($_POST['tipo'] == '8') { ?>
        <tr>
            <td><b>T&eacute;cnica de An&aacute;lisis:</b></td>

            <td><select name="tipoCroma" id="tipoCroma">
                    <option value="0" disabled> Elija Uno</option>
                    <option value="1" <?php ($ROW[0]['tipoCroma'] == "1") ? 'selected' : '' ?>>CL EM/EM</option>
                    <option value="2" <?php ($ROW[0]['tipoCroma'] == "2") ? 'selected' : '' ?>> CG EM/EM</option>
                    <?php } ?>
                </select></td>
        </tr>
    </table>
    <br/>
    <center>
        <input id="new" type="button" class="boton" value="Limpiar" onclick="desmarca();" disabled>&nbsp;
        <input id="add" type="button" class="boton" value="Agregar" onclick="modificar('I');">&nbsp;
        <input id="mod" type="button" class="boton" value="Modificar" onclick="modificar('M');" disabled>&nbsp;
        <input id="del" type="button" class="boton" value="Eliminar" onclick="modificar('D');" disabled>
    </center>
    <?= $Gestor->Encabezado('N0018', 'p', '') ?>
    </body>
    </html>

    <?php unset($ROW);
} else {
    $ROW = $Gestor->Registros();
    for ($x = 0; $x < count($ROW); $x++) {
        echo "
    <option id='{$ROW[$x]['id']}' nombre='{$ROW[$x]['nombre']}'>{$ROW[$x]['nombre']}</option>";
    }
    unset($ROW);
    ?>
    </table>
    <br/>
    <table class="radius" align="center" width="255px">
        <tr>
            <td class="titulo" colspan="2">Detalle</td>
        </tr>
        <input type="hidden" id="id">
        <tr>
            <td><b>Nombre:</b></td>
            <td><input type="text" id="nombre" size="25" maxlength="50" disabled></td>
        </tr>
    </table>
    <br/>
    <center>
        <input id="new" type="button" class="boton" value="Limpiar" onclick="desmarca();" disabled>&nbsp;
        <input id="add" type="button" class="boton" value="Agregar" onclick="modificar('I');">&nbsp;
        <input id="mod" type="button" class="boton" value="Modificar" onclick="modificar('M');" disabled>&nbsp;
        <input id="del" type="button" class="boton" value="Eliminar" onclick="modificar('D');" disabled>
    </center>
    <?= $Gestor->Encabezado('N0018', 'p', '') ?>
    </body>
    </html>

<?php } ?>