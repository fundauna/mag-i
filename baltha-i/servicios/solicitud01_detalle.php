<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);


$Gestor = new _solicitud01_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) {
    die('Registro inexistente');
}

if ($ROW[0]['estado'] != '' && $ROW[0]['estado'] != '0') {
    $disabled = 'disabled';
} else {
    $disabled = '';
}

$ROW2 = $Gestor->SolicitudLineas();
$_con = $Gestor->obtenerSiguiente();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>

    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <link  type='text/javascript'  href='../../caspha-i/js/servicios/solicitud01_detalle.php.js'>

    <style>
        .radius tr {
            border-width: 1px;
        }

    </style>
</head>
<body>
<br>
<br>
<center>
    <div class="divEncabezado">
        <?php $Gestor->Incluir('n12', 'hr', 'Servicios :: ' . ($_GET['acc'] == 'I' ? 'Ingresar' : 'Modificar') . ' Solicitudes') ?>
        <?= $Gestor->Encabezado('N0012', 'e', ' ' . ($_GET['acc'] == 'I' ? 'Ingresar' : 'Modificar') . ' Solicitudes') ?>
    </div>
    <br>
    <tr>
        <input type="hidden" name="ID" id="ID" value="<?= $ROW[0]['cs'] ?>"/>
        <input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
        <input type="hidden" id="cliente" value="<?= $ROW[0]['cliente'] ?>"/>
        <table class="radius">
            <tr>
                <td class="titulo" colspan="2">Datos</td>
            </tr>
            <tr <?php if ($_GET['acc'] == 'I') echo 'style="display:none"'; ?>>
                <td>N&uacute;mero de solicitud:</td>
                <td><input type="text" name="cs" id="cs" value="<?=  $ROW[0]['cs'] ?>" size="9" readonly></td>
            </tr>
            <tr <?php if ($_GET['acc'] == 'I') echo 'style="display:none"'; ?>>
                <td>Fecha de creaci&oacute;n:</td>
                <td><?= $ROW[0]['fecha'] ?></td>
            </tr>
            <tr>
                <td>Cliente:</td>
                <td><input type="text" name="nomcliente" id="nomcliente" value="<?= $ROW[0]['nomcliente'] ?>"
                           class="lista"
                           readonly onclick="ClientesLista()" <?= $disabled ?>></td>
            </tr>
            <tr>
                <td>Solicitante:</td>
                <td>
                    <input type="text" name="nomsolicitante" id="nomsolicitante"
                           value="<?= $ROW[0]['nomsolicitante'] ?>" <?= $disabled ?>>
                </td>
            </tr>
            <tr>
                <td>Entregado por:</td>
                <td><input type="text" name="entregado" id="entregado" value="<?= $ROW[0]['entregado'] ?>"
                           title="Alfanum�rico (2/60)" maxlength="60" <?= $disabled ?>></td>
            </tr>
            <tr <?php if ($_GET['acc'] == 'I') echo 'style="display:none"'; ?>>
                <td>N&uacute;mero de lote:</td>
                <td> <?= substr_replace( $ROW[0]['cs'], '',3,1)?></td>
            </tr>
            <tr>
                <td>Prop&oacute;sito del an&aacute;lisis:</td>
                <td>

                    <select name="proposito" id="proposito" <?= $disabled ?>>
                        <!--  <option value="">...</option>
                            <option disabled value="1" <?= ($ROW[0]['proposito'] == 1) ? 'selected' : '' ?>>Exportaci&oacute;n</option>	-->
                        <option value="2" <?= ($ROW[0]['proposito'] == 2) ? 'selected' : '' ?>>Detecci&oacute;n de
                            Residuos
                        </option>
                        <!-- <option disabled value="3" <?= ($ROW[0]['proposito'] == 3) ? 'selected' : '' ?>>Importaci&oacute;n</option>
                            <option disabled value="4" <?= ($ROW[0]['proposito'] == 4) ? 'selected' : '' ?>>Ronda intercomparativa</option>
                            <option disabled value="5" <?= ($ROW[0]['proposito'] == 5) ? 'selected' : '' ?>>Otro</option>	-->
                    </select>

                </td>
            </tr>
            <tr>
                <td>Muestreo:</td>
                <td><input type="text" name="muestreo" id="muestreo" value="<?= $ROW[0]['muestreo'] ?>"
                           title="Alfanum�rico (2/20)" maxlength="20 <?= $disabled ?>"></td>
            </tr>
          <!--  <tr>
                <td>Fecha/Hora de Muestreo:</td>
                <td>
                    <input type="text" name="fmuestreo" id="fmuestreo" value="<?= $ROW[0]['fmuestreo'] ?>"
                           title="Alfanum�rico (2/20)" class="fecha" <?= $disabled ?> readonly
                           onClick="show_calendar(this.id);">
                    <input type="text" name="hmuestreo" id="hmuestreo" value="<?= $ROW[0]['hmuestreo'] ?>"
                           size="3" <?= $disabled ?>>
                </td>
            </tr>-->
            <tr>
                <td>Fecha/Hora de Recepci&oacute;n de Solicitud:</td>
                <td>
                    <input type="text" name="flab" id="flab" value="<?= $ROW[0]['flab']?>" title="Alfanum�rico (2/20)"
                           class="fecha" <?= $disabled ?> readonly onClick="show_calendar(this.id);">
                    <input type="text" name="hlab" id="hlab" value="<?= $ROW[0]['hlab'] ?>" size="3" <?= $disabled ?>>
                </td>
            </tr>
            <tr>
                <td>Tiempo de entrega al cliente (d&iacute;as):</td>
                <td><input type="text" name="entregacliente" id="entregacliente"
                           value="<?= $ROW[0]['entregacliente'] ?>"
                           size="3" onblur="_INT(this)" <?= $disabled ?>></td>
            </tr>
            <tr>
                <td>Tiempo de entrega del analista (d&iacute;as):</td>
                <td><input type="text" name="entregaanalista" id="entregaanalista"
                           value="<?= $ROW[0]['entregaanalista'] ?>"
                           size="3" onblur="_INT(this)" <?= $disabled ?>></td>
            </tr>
            <tr>
                <td>Adjuntar archivo:</td>
                <td><input type="file" name="adjunto" id="adjunto"></td>
            </tr>
            <?php if ($_GET['acc'] == 'M' && file_exists("../../caspha-i/docs/servicios/" . $_GET['ID'] . ".zip")) { ?>
                <tr>
                    <td>Descargar archivo:</td>
                    <td><a href="../../caspha-i/docs/servicios/<?= $_GET['ID'] ?>.zip"><img
                                    src="<?php $Gestor->Incluir('bajar', 'bkg') ?>"></a>&nbsp;&nbsp;&nbsp;<img
                                src="<?php $Gestor->Incluir('del', 'bkg') ?>" onclick="eliminaDoc();"></td>
                </tr>
            <?php } ?>
            <tr>
                <td><!--Formulario Requisito Fitosanitario (FRF):--></td>
                <td><input type="hidden" name="obs" id="obs" value="<?= $ROW[0]['obs'] ?>"
                           maxlength="50" <?= $disabled ?>>
                </td>
            </tr>
            <?php if ($ROW[0]['estado'] == '2' or $ROW[0]['estado'] == '7') { ?>
                <!--agua -->
                <?php if ($ROW2[0]['matriz'] == '3') { ?>
                    <tr>
                        <td colspan="2"><strong>Hojas de trabajo:</strong>&nbsp;&nbsp; 
                            [<a href="#" onclick="inyeccion()">Inyecci&oacute;n</a>]&nbsp;
                            [<a href="#" onclick="extraccionAgua()">Extracci&oacute;n</a>]&nbsp;
                            [<a href="#" onclick="ImprimirTodasAgua( )">Imprime todas</a>]&nbsp;
                            [<a href="#" onclick="HojaTrabajo('<?= "LRE_hoja4.php?ID={$ROW[0]['cs']}" ?>')">Etiquetas</a>]
                        </td>
                    </tr>
                    <!-- suelo -->
                <?php } elseif ($ROW2[0]['matriz'] == '149') { ?>
                    <tr>
                        <td colspan="2"><strong>Hojas de trabajo:</strong>&nbsp;&nbsp;
                        [<a href="#" onclick="homogenizacion()">Homogeneizaci&oacute;n</a>]&nbsp;
                            [<a href="#" onclick="inyeccion()">Inyecci&oacute;n</a>]&nbsp;
                            [<a href="#" onclick="extraccion()">Extracci&oacute;n</a>]&nbsp;
                            [<a href="#" onclick="humedad()">Humedad</a>]&nbsp; 
                            [<a href="#" onclick="ImprimirTodasSuelo( )">Imprime todas</a>]&nbsp;
                             
                            [<a href="#"
                                onclick="HojaTrabajo('<?= "LRE_hoja4.php?ID={$ROW[0]['cs']}" ?>')">Etiquetas</a>]
                        </td>
                    </tr>
                <?php } else { ?>
                    <tr>
                        <td colspan="2"><strong>Hojas de trabajo:</strong>&nbsp;&nbsp;
                        [<a href="#" onclick="homogenizacion()">Homogeneizaci&oacute;n</a>]&nbsp;
                            [<a href="#" onclick="inyeccion()">Inyecci&oacute;n</a>]&nbsp;
                            [<a href="#" onclick="extraccion()">Extracci&oacute;n</a>]&nbsp;
                             
                            [<a href="#" onclick="ImprimirTodasVegetal( )">Imprime todas</a>]&nbsp;
                            [<a href="#" onclick="HojaTrabajo('<?= "LRE_hoja4.php?ID={$ROW[0]['cs']}" ?>')">Etiquetas</a>]
                        </td>
                    </tr>

 <!-- codigo viejo
   if ($ROW2[0]['matriz'] == '3') { ?>
                    <tr>
                        <td colspan="2"><strong>Hojas de trabajo:</strong>&nbsp;&nbsp;
                            [<a href="#" onclick="homogenizacion()">Homogenizacion</a>]&nbsp;
                            [<a href="#" onclick="HojaTrabajo('<?= "LRE_hoja3.php?ID={$ROW[0]['cs']}" ?>&tipo=CG')">Inyecci&oacute;n
                                CG</a>]&nbsp;
                            [<a href="#" onclick="HojaTrabajo('<?= "LRE_hoja3.php?ID={$ROW[0]['cs']}" ?>&tipo=CL')">Inyecci&oacute;n
                                CL</a>]&nbsp;
                            [<a href="#" onclick="HojaTrabajo('<?= "LRE_multiple.php?ID={$ROW[0]['cs']}" ?>')">Imprime
                                todas</a>]&nbsp;
                            [<a href="#"
                                onclick="HojaTrabajo('<?= "LRE_hoja4.php?ID={$ROW[0]['cs']}" ?>')">Etiquetas</a>]
                        </td>
                    </tr>
                     -- suelo -- >
                <   } elseif ($ROW2[0]['matriz'] == '149') { ?>
                    <tr>
                        <td colspan="2"><strong>Hojas de trabajo:</strong>&nbsp;&nbsp;
                            [<a href="#" onclick="homogenizacion()">Homogenizacion&oacute;n</a>]&nbsp;
                            [<a href="#" onclick="HojaTrabajo('<?= "LRE_hoja2.php?ID={$ROW[0]['cs']}" ?>', '1')">Extracci&oacute;n</a>]&nbsp;
                            [<a href="#" onclick="HojaTrabajo('<?= "LRE_hoja3.php?ID={$ROW[0]['cs']}" ?>&tipo=CG')">Inyecci&oacute;n
                                CG</a>]&nbsp;
                            [<a href="#" onclick="HojaTrabajo('<?= "LRE_hoja3.php?ID={$ROW[0]['cs']}" ?>&tipo=CL')">Inyecci&oacute;n
                                CL</a>]&nbsp;
                            [<a href="#" onclick="HojaTrabajo('<?= "LRE_hoja5.php?ID={$ROW[0]['cs']}" ?>')">Humedad</a>]&nbsp;
                            [<a href="#" onclick="HojaTrabajo('<?= "LRE_multiple.php?ID={$ROW[0]['cs']}" ?>&tk=1')">Imprime
                                todas</a>]&nbsp;
                            [<a href="#"
                                onclick="HojaTrabajo('<?= "LRE_hoja4.php?ID={$ROW[0]['cs']}" ?>')">Etiquetas</a>]
                        </td>
                    </tr>
                
                    <tr>
                        <td colspan="2"><strong>Hojas de trabajo:</strong>&nbsp;&nbsp;
                            [<a href="#" onclick="HojaTrabajo('<?= "LRE_hoja1.php?ID={$ROW[0]['cs']}" ?>')">Homogenizaci&oacute;n</a>]&nbsp;
                            [<a href="#" onclick="HojaTrabajo('<?= "LRE_hoja2.php?ID={$ROW[0]['cs']}" ?>', '1')">Extracci&oacute;n</a>]&nbsp;
                            [<a href="#" onclick="HojaTrabajo('<?= "LRE_hoja3.php?ID={$ROW[0]['cs']}" ?>&tipo=CG')">Inyecci&oacute;n
                                CG</a>]&nbsp;
                            [<a href="#" onclick="HojaTrabajo('<?= "LRE_hoja3.php?ID={$ROW[0]['cs']}" ?>&tipo=CL')">Inyecci&oacute;n
                                CL</a>]&nbsp;
                            [<a href="#" onclick="HojaTrabajo('<?= "LRE_multiple.php?ID={$ROW[0]['cs']}" ?>')">Imprime
                                todas</a>]&nbsp;
                            [<a href="#"
                                onclick="HojaTrabajo('<?= "LRE_hoja4.php?ID={$ROW[0]['cs']}" ?>')">Etiquetas</a>]
                        </td>
                    </tr>
                -->

                <?php } ?>
            <?php } ?>
        </table>
        <br/>
        <table class="radius" rows="5">
            <tr>
                <td class="titulo" colspan="16">Detalle&nbsp;<img onclick="SolicitudAgregar()"
                                                                  src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                                                                  title="Agregar l&iacute;nea" class="tab" <?= $disabled ?>/>
                </td>
            </tr>
            <tr>
                <td align="center">N&uacute;mero<br/>de muestra</td>

                <td align="center" width="119px">C&oacute;digo<br/>externo</td>
                <td align="center">Matriz</td>
                <td align="center">Tipo</td>
                <td align="center">Presentaci&oacute;n<br/>de la muestra</td>
                <td align="center">Estado del empaque<br/>de la Muestra</td>
                <td align="center">Forma en que se<br/> recibe la Muestra</td>
                <td align="center">Fecha y Hora de <br/> Muestreo:</td>
                <td align="center">Masa <br/>Muestra (g)</td>
                <td align="center">Observaciones</td>
                <td align="center">Formulario <br/>F.R.F</td>
                <td align="center">An&aacute;lisis<br/>solicitado</td>
                <td align="center">Parte<br/>a analizar</td>
                <td align="center">Usar M&eacute;todo </br>del Cliente</td>
               <td align="center">N&uacute;mero Informe </br> Asignado</td>
            </tr>
            <tbody id="lolo">
            <?php

            for ($x = 0; $x < count($ROW2); $x++) {
                if ($ROW[0]['estado'] != '' and $ROW[0]['estado'] != '5') {
                   //agrega y controla los 0
                    if($x <= 8 ){
                        $a = 0;
                    } else {
                        $a = '';
                    }
                    $cod_int = str_replace('LRE-', '', $ROW[0]['cs']) . '-'.$a. ($x + 1);
                    $cod_int = str_replace(' ', '', $cod_int);
                    $cod_int =  substr_replace( $cod_int, '',2,2);
                } else
                    $cod_int = '';
                ?>
                <tr>
                  <!--  <td><input type="text" size="8" value="<?= isset($ROW2[$x]['conSolicitud'])== true ? $ROW2[$x]['conSolicitud'] :'' ?>" readonly/></td>-->
                    <td><input type="text" size="8" value="<?= isset($cod_int)== true ? $cod_int :'' ?>" readonly/></td>
                    <td><input type="text" size="29" name="codigo" id="codigo" value="<?= $ROW2[$x]['codigo'] ?>" <?= $disabled ?>/></td>
                    <td>
                        <input type="text" name="nommatriz" id="nommatriz<?= $x ?>" value="<?= $ROW2[$x]['nombre'] ?>"
                               class="lista"  readonly onclick="MatrizLista(<?= $x ?>)" <?= $disabled ?>>
                        <input type="hidden"  name="matriz" id="matriz<?= $x ?>" value="<?= $ROW2[$x]['matriz'] ?>">
                    </td>
                    <td><input type="text" size="10" name="tipo" id="tipo" value="<?= $ROW2[$x]['tipo'] ?>" <?= $disabled ?>/></td>
                    <td>
                        <select name="presentacion" <?= $disabled ?>>
                            <option value="">...</option>
                            <option value="0" <?= ($ROW2[$x]['presentacion'] == '0') ? 'selected' : '' ?>>Congelada
                            </option>
                            <option value="1" <?= ($ROW2[$x]['presentacion'] == '1') ? 'selected' : '' ?>>Descongelada
                            </option>
                            <option value="2" <?= ($ROW2[$x]['presentacion'] == '2') ? 'selected' : '' ?>>Fresca, no
                                refrigerada
                            </option>
                            <option value="3" <?= ($ROW2[$x]['presentacion'] == '3') ? 'selected' : '' ?>>Fresca
                                refrigerada
                            </option>
                        </select>
                    </td>
                    <td>
                        <select name="empaque" <?= $disabled ?>>
                            <option value="">...</option>
                            <option value="0" <?= ($ROW2[$x]['empaque'] == '0') ? 'selected' : '' ?>>Bolsa Intacta
                            </option>
                            <option value="1" <?= ($ROW2[$x]['empaque'] == '1') ? 'selected' : '' ?>>Bolsa Da&ntilde;ada
                            </option>
                            <option value="2" <?= ($ROW2[$x]['empaque'] == '2') ? 'selected' : '' ?>>Botella Vidrio
                                Ambar
                            </option>
                            <option value="3" <?= ($ROW2[$x]['empaque'] == '3') ? 'selected' : '' ?>>Otro</option>
                        </select>
                    </td>
                    <td>
                        <select name="forma" <?= $disabled ?>>
                            <option value="">...</option>
                            <option value="0" <?= ($ROW2[$x]['forma'] == '0') ? 'selected' : '' ?>>Entero</option>
                            <option value="1" <?= ($ROW2[$x]['forma'] == '1') ? 'selected' : '' ?>>En Cuartos</option>
                            <option value="2" <?= ($ROW2[$x]['forma'] == '2') ? 'selected' : '' ?>>Rebanadas</option>
                            <option value="3" <?= ($ROW2[$x]['forma'] == '3') ? 'selected' : '' ?>>Podrida</option>
                            <option value="4" <?= ($ROW2[$x]['forma'] == '4') ? 'selected' : '' ?>>Golpeada</option>
                            <option value="5" <?= ($ROW2[$x]['forma'] == '5') ? 'selected' : '' ?>>Otro</option>
                        </select>
                    </td>

                    <td>
                        <input type="text"  name="fmuestra" id="fmuestra" value="<?= (isset($ROW2[$x]['fecha'])) == true ? $ROW2[$x]['fecha'] : "" ?>"
                               title="Alfanum�rico (2/20)" class="fecha" <?= $disabled ?> readonly
                               onClick="show_calendar(this.id);">
                        <input type="text" name="hmuestra" id="hmuestra" value="<?= (isset($ROW2[$x]['hora'])) == true ? $ROW2[$x]['hora'] : ""  ?>"
                               size="3" <?= $disabled ?>>
                    </td>

                    <td><input type="text" name="masa" size="9" value="<?= $ROW2[$x]['masa'] ?>" <?= $disabled ?>/></td>
                    <td><textarea name="observaciones"
                                  title="Alfanum�rico (5/100)" <?= $disabled ?>><?= $ROW2[$x]['observaciones'] ?></textarea>
                    </td>
                    <td>
                        <input size="8" type="text" name="obsfrf" id="obsfrf"
                               value="<?= $ROW2[$x]['obsfrf'] ?>"/>
                    </td>
                    <td>
                        <input type="text" name="nom_analisis" id="nom_analisis<?= $x ?>"
                               value="<?= $ROW2[$x]['nom_analisis'] ?>" class="lista" readonly
                               onclick="SubAnalisisLista(<?= $x ?>)" <?= $disabled ?>>
                        <input type="hidden" name="cod_analisis" id="cod_analisis<?= $x ?>"
                               value="<?= $ROW2[$x]['cod_analisis'] ?>">
                    </td>
                    <td>
                        <select name="parte" <?= $disabled ?>>
                            <option value="">...</option>
                            <option value="0" <?= ($ROW2[$x]['parte'] == '0') ? 'selected' : '' ?>>&Iacute;ntegra
                            </option>
                            <option value="1" <?= ($ROW2[$x]['parte'] == '1') ? 'selected' : '' ?>>C&aacute;scara
                            </option>
                            <option value="2" <?= ($ROW2[$x]['parte'] == '2') ? 'selected' : '' ?>>Pulpa</option>
                        </select>
                    </td>
                  <!--  <td><input type="text" name="metodo" size="7" maxlength="20"
                               value="<?= $ROW2[$x]['metodo'] ?>" <?= $disabled ?>/></td>-->
                    <td><input type="text" name="metodo" size="7" maxlength="20"
                               value="No"/></td>


                  <td><input type="text" id="nia" name="nia" size="8" value="<?= isset($ROW2[$x]['nia']) == true ? $ROW2[$x]['nia'] :''?>"readonly/></td>
                    
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <br/>
        <?php if ($_GET['acc'] == 'I' or $ROW[0]['estado'] == '0') { ?>
            <input type="button"  id="btn" value="Aceptar" class="boton" onClick="datos()">
        <?php } ?>
        <?php if ($ROW[0]['estado'] == '0') { ?>
            <input type="button" value="Procesar" class="boton" onClick="Procesar()">&nbsp;
        <?php } ?>
        <?php if ($ROW[0]['estado'] == '1') { ?>
            <input type="button" value="Aprobar" class="boton" onClick="Aprobar()">&nbsp;
            <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
            <input type="button" value="Reversar" class="boton" onClick="Reversar()">
        <?php } ?>
        <input type="button" value="Imprimir" class="boton2" onclick="imprimirlo()">



</center>
<?= $Gestor->Encabezado('N0012', 'p', '') ?>

</body>

</html>