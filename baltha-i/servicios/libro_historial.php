<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _libro_historial();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<center>
    <?php $Gestor->Incluir('n5', 'hr', 'Servicios :: Registro de custodia') ?>
    <?= $Gestor->Encabezado('N0005', 'e', 'Registro de custodia') ?>
    <br>
    <table class="radius" style="font-size:12px" width="90%">
        <thead>
        <tr>
            <td class="titulo" colspan="2">Historial</td>
        </tr>
        </thead>
        <?php
        $ROW2 = $Gestor->Historial();
        for ($x = 0; $x < count($ROW2); $x++) {
            ?>
            <tr>
                <td>
                    <strong><?= $Gestor->Accion($ROW2[$x]['accion']) ?></strong> <?= "{$ROW2[$x]['nombre']}, {$ROW2[$x]['fecha1']}" ?>
                </td>
                <td>____________________</td>
            </tr>
            <?php
        }
        ?>
    </table>
    <br/><?= $Gestor->Encabezado('N0005', 'p', '') ?>
    <br/>
    <input type="button" value="Imprimir" class="boton" onClick="window.print()">
</center>
</body>
</html>