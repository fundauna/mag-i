<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _arqueo_detalle();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('n19', 'hr', 'Servicios :: Inventario requerido') ?>
<?= $Gestor->Encabezado('N0019', 'e', 'Inventario requerido') ?>
<center>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td><strong>Hoja de c&aacute;lculo:</strong>&nbsp;<?= $_GET['nombre'] ?></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td><strong>C&oacute;digo</strong></td>
            <td><strong>Nombre</strong></td>
            <td align="center"><strong>Requerido</strong></td>
            <td align="center"><strong>Stock Bodega Central</strong></td>
            <td align="center"><strong>Stock Bodega Auxiliar</strong></td>
            <td><strong>Observaciones</strong></td>
        </tr>
        <?php
        $ROW = $Gestor->Requerido();
        for ($x = 0; $x < count($ROW); $x++) {
            if ($ROW[$x]['stock1'] < $ROW[$x]['cantidad']) $color = 'F00';
            else $color = '000';
            ?>
            <tr>
                <td><?= $ROW[$x]['codigo'] ?></td>
                <td><?= $ROW[$x]['nombre'] ?></td>
                <td align="center"><?= $ROW[$x]['cantidad'] ?></td>
                <td align="center" style="color:#<?= $color ?>"><?= $ROW[$x]['stock1'] ?></td>
                <td align="center"><?= $ROW[$x]['stock2'] ?></td>
                <td><?= $ROW[$x]['obs'] ?></td>
            </tr>
        <?php } ?>
    </table>
    <br/>
    <input type="button" class="boton" value="Imprimir" onclick="window.print()"/>
</center>
<?= $Gestor->Encabezado('N0019', 'p', '') ?>
</body>
</html>