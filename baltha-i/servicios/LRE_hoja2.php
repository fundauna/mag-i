<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _LRE_hoja2();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

$ROW2 = $Gestor->SolicitudLineas();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <script>
        var _oculta = false;

        function Oculta() {
            total = document.getElementsByName('dummy').length;
            for (x = 0; x < total; x++) {
                document.getElementById('tdA' + x).innerHTML = '';
                document.getElementById('tdB' + x).innerHTML = '';
                document.getElementById('tdC' + x).innerHTML = '';
            }
        }
    </script>
</head>
<body style="background-image: none; background-color: #FFF">
<center>


    <?php $Gestor->Incluir('n7', 'hr', 'Servicios :: Hoja de trazabilidad para la extracci&oacute;n de muestras') ?>
    <header><?= $Gestor->Encabezado('N0007', 'e', 'Hoja de trazabilidad para la extracci&oacute;n de muestras') ?></header>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

    <table class=paging>
        <thead>
        <tr>
            <td>&nbsp;</td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <table class="tableImpresa" width="98%" rules="none" border="0">
                    <tr>
                        <td width="20%"><strong>Fecha de ingreso:</strong></td>
                        <td><?= $ROW[0]['fecha'] ?></td>

                        <td width="26%"><strong>Tiempo de entrega de resultados:</strong></td>
                        <td width="20%"><?= $ROW[0]['entregaanalista'] ?> d&iacute;a</td>
                        <td align="right"><b style="font-size: 15px"><?= $_GET['ID'] ?></b></td>
                    </tr>
                    <tr>
                        <td><strong>Lote:</strong></td>
                        <td style="font-size: 15px"><?= $cod_int = str_replace('-0', '-', $_GET['ID']);
                            ?></td>

                        <td><strong>N&uacute;mero de muestras:</strong></td>
                        <td><?= count($ROW2) ?></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><strong>An&aacute;lisis Solicitado:</strong></td>
                        <td width="25%"><?= $ROW2[0]['nom_analisis'] ?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="5"></td>
                    </tr>

                </table>
                <br/>
                <table border="1" bordercolor="#000000" cellpadding="12" cellspacing="0" width="98%">
                    <tr align="center">
                        <td width="100px"><strong>Muestra</td>
                        <td width="100px"><strong>Matriz</strong></td>
                        <td width="45px"><strong>Grupo</strong></td>
                        <td width="100px"><strong>Masa, g</strong></td>
                        <td width="60px"><strong>N&uacute;mero de<br/>Balanza</strong></td>
                        <td width="100px"><strong>Fecha<br/>Extracci&oacute;n</strong></td>
                        <td width="100px"><strong>N&uacute;mero de<br/>Dispensador</strong></td>
                        <td width="100px"><strong>N&uacute;mero de<br/>Micropipeta</strong></td>
                        <td width="500px"><strong>Tratamiento Especial</strong></td>
                    </tr>


                    <tr style="font-size: 10px;">
                        <?php
                        for ($x = 0;
                        $x < count($ROW2);
                        $x++) {
                        $cod_int = $_GET['ID'];
                        $cod_int = str_replace('-0', '', $cod_int);
                        if ($x < 9) {
                            $cod_int = $cod_int . '-' . '0' . ($x + 1);
                        } else {
                            $cod_int = $cod_int . '-' . ($x + 1);
                        }
                        ?>
                        <td align="center" rowspan="2"><?= $cod_int ?></td>
                        <td align="center" rowspan="2"><?= $ROW2[$x]['nombre'] . ", " . $ROW2[$x]['tipo'] ?></td>
                        <td align="center"
                            rowspan="2"><?= (($ROW2[$x]['acreditado'] == 0) ? str_replace('-', '', substr($ROW2[$x]['nombreGrupo'], 0, 3))
                                : str_replace('-', '', substr($ROW2[$x]['nombreGrupo'], 0, 3) . ' *')) ?></td>
                        <td rowspan="2">&nbsp;</td>
                        <td align="left" rowspan="2">&nbsp;EQ-</td>
                        <td rowspan="2">&nbsp;</td>
                        <td rowspan="2" align="left">&nbsp;D-</td>
                        <td align="left">&nbsp;M-</td>
                        <td rowspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left">&nbsp;M-</td>
                    </tr>
                    <?php
                    } ?>
                </table>
                <table border="0" bordercolor="#000000" cellpadding="3" cellspacing="0" width="98%">
                    <tr>
                        <td colspan="9">
                            G1A: Alto contenido en agua y clorofila, G1B: Alto contenido de agua y contenido escaso
                            o ausencia de clorofila, G2A: Alto contenido de aceite o grasa y muy bajo contenido de
                            agua,
                            G2B: Alto contenido de aceite o grasa e intermedio contenido de agua, G3: Alto contenido
                            de &aacute;cido y agua,
                            G4: Alto contenido de almid&oacute;n y/o prote&iacute;na y bajo contenido de agua y
                            grasa, G5: Productos especiales o dif&iacute;ciles,G6: Alto contenido de az&uacute;car y
                            bajo contenido de agua

                        </td>
                    </tr>
                    <tr>
                        <td colspan="9" align="left">
                            <br/>
                            Observaciones:
                            <hr>
                            <br/>
                            <hr>
                            <br/>
                            <hr>
                            <br/>
                        </td>
                    </tr>
                </table>
</center>

</br></br>

<div align="center">&Uacute;ltima L&iacute;nea</div>


<div class="page-break"></div>
</br></br>
</br></br>
</br></br>
</br></br>
</br></br>
</br></br>
</br></br>
</br></br>


Plaguicidas que se deben eliminar del informe<br><br>
<br/>
<table border="0" bordercolor="#000000" cellpadding="0" cellspacing="0" width="98%" style="padding-left: 20px">
    <tr align="center">
        <td width="20%" style="padding-top: 5px; padding-bottom: 5px padding-left:45px; padding-right: 75px"><strong>&nbsp;&nbsp;
        </td>
        <td width="40%" style="padding-left: 150px; padding-right: 150px"><strong>RECUPERACI&Oacute;N*</strong></td>
        <td width="40%" style="padding-left: 150px; padding-right: 150px"><strong>CONTROL**</strong></td>
    </tr>
</table>
<table border="1" bordercolor="#000000" cellpadding="0" cellspacing="0" width="98%">
    <tr>
        <td align="center" width="20%" style=" padding-left: 75px; padding-right: 75px"><b> CL</td>
        <td width="40%"
            style="padding-top: 200px; padding-bottom: 150px; padding-left: 150px; padding-right: 150px"></td>
        <td width="40%"
            style="padding-top: 150px; padding-bottom: 150px; padding-left: 150px; padding-right: 150px"></td>
    </tr>
    <tr>
        <td align="center" width="20%" style=" padding-left: 75px; padding-right: 75px"><b>CG</td>
        <td width="40%"
            style="padding-top: 200px; padding-bottom: 150px; padding-left: 150px; padding-right: 150px"></td>
        <td width="40%"
            style="padding-top: 150px; padding-bottom: 150px; padding-left: 150px; padding-right: 150px"></td>
    </tr>
</table>


<center>
    <br/>
    <br/>
    <input type="button" value="Plantilla" title="Imprimir como hoja en blanco" class="boton2" onclick="Oculta()"/>
    <input type="button" value="Imprimir" class="boton2" onClick="window.print()">
</center>
<?= $Gestor->Encabezado('N0007', 'p', '') ?>
</table>
</body>
</html>