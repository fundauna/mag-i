<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _muestras_asignar_LRE();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="_TIPO"/> <!-- PARA SABER SI ESTOY ASIGNANDO O REASIGNANDO -->
<?php $Gestor->Incluir('n10', 'hr', 'Servicios :: Asignar analistas LRE') ?>
<?= $Gestor->Encabezado('N0010', 'e', 'Asignar analistas LRE') ?>
<center>
    Muestras por asignar
    <div id="container" style="width:600px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>No. Muestra</th>
                <th>Solicitud</th>
                <th>Fecha ingreso</th>
                <th>Analista</th>
                <th>Analista 2</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->Muestras('');
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><?= $ROW[$x]['ref'] ?></td>
                    <td><a href="#"
                           onclick="SolicitudesImprime('<?= $ROW[$x]['solicitud'] ?>', '<?= $_POST['LID'] ?>', '<?= $ROW[$x]['ref'] ?>')"><?= $ROW[$x]['solicitud'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['fecha1'] ?></td>
                    <td><input type="text" id="tmp<?= $x ?>" class="lista" readonly
                               onclick="UsuariosLista('<?= $ROW[$x]['id'] ?>', '<?= $x ?>', 0, 'tmp', 'usuario')"/><input
                                type="hidden" id="usuario<?= $x ?>"/></td>
                    <td><input type="text" id="tmp2<?= $x ?>" class="lista" readonly
                               onclick="UsuariosLista('<?= $ROW[$x]['id'] ?>', '<?= $x ?>', 0, 'tmp2', 'usuario2')"/><input
                                type="hidden" id="usuario2<?= $x ?>"/></td>
                    <td><img id="imgA<?= $x ?>" onclick="MuestraAsigna('<?= $ROW[$x]['id'] ?>', '<?= $x ?>')"
                             src="<?php $Gestor->Incluir('mod', 'bkg') ?>" title="Asignar" class="tab3"/>
                        <img id="imgB<?= $x ?>" src="<?php $Gestor->Incluir('aprobar', 'bkg') ?>" title="Asignada"
                             class="tab3" style="display:none"/>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <?php
    $ROW = $Gestor->Muestras(1);
    if ($ROW) {
        ?>
        <br/><br/>
        <table class="radius" width="600px" align="center">
            <tr>
                <td class="titulo" colspan="7">Muestras asignadas</td>
            </tr>
            <tr>
                <th>No. Muestra</th>
                <th>Solicitud</th>
                <th>Analista</th>
                <th>Analista 2</th>
                <th>Asignar a:</th>
                <th>Asignar segundo a:</th>
                <th></th>
            </tr>
            <tbody>
            <?php
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr align="center">
                    <td><?= $ROW[$x]['ref'] ?></td>
                    <td><a href="#"
                           onclick="SolicitudesImprime('<?= $ROW[$x]['solicitud'] ?>', '<?= $_POST['LID'] ?>', '<?= $ROW[$x]['ref'] ?>')"><?= $ROW[$x]['solicitud'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['analista'] ?></td>
                    <td><?= $ROW[$x]['analista2'] ?></td>
                    <td>
                        <input type="text" id="Rtmp<?= $x ?>" class="lista" readonly
                               onclick="UsuariosLista('<?= $ROW[$x]['id'] ?>', '<?= $x ?>', 1, 'Rtmp', 'Rusuario')"/>
                        <input type="hidden" id="Rusuario<?= $x ?>"/>
                        <input type="hidden" id="actual<?= $x ?>" value="<?= $ROW[$x]['actual'] ?>"/>
                    </td>
                    <td>
                        <input type="text" id="Rtmp2<?= $x ?>" class="lista" readonly
                               onclick="UsuariosLista('<?= $ROW[$x]['id'] ?>', '<?= $x ?>', 1, 'Rtmp2', 'Rusuario2')"/>
                        <input type="hidden" id="Rusuario2<?= $x ?>"/>
                        <input type="hidden" id="actual2<?= $x ?>" value="<?= $ROW[$x]['actual2'] ?>"/>
                    </td>
                    <td><img id="RimgA<?= $x ?>" onclick="MuestraReasigna('<?= $ROW[$x]['id'] ?>', '<?= $x ?>')"
                             src="<?php $Gestor->Incluir('mod', 'bkg') ?>" title="Asignar" class="tab3"/>
                        <img id="RimgB<?= $x ?>" src="<?php $Gestor->Incluir('aprobar', 'bkg') ?>" title="Asignada"
                             class="tab3" style="display:none"/>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <?php
    }//IF
    ?>
</center>
<?= $Gestor->Encabezado('N0010', 'p', '') ?>
</body>
</html>