<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _LRE_hoja2();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW)
    die('Registro inexistente');

$ROW2 = $Gestor->SolicitudLineas();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <script>
        var _oculta = false;

        function Oculta() {
            total = document.getElementsByName('dummy').length;
            for (x = 0; x < total; x++) {
                document.getElementById('tdA' + x).innerHTML = '<br>&nbsp;';
                document.getElementById('tdB' + x).innerHTML = '';
                document.getElementById('tdC' + x).innerHTML = '';
            }
        }
    </script>
    <style>
        .Rotate-90 {
            -webkit-transform: rotate(-90deg);
            -moz-transform: rotate(-90deg);
            -ms-transform: rotate(-90deg);
            -o-transform: rotate(-90deg);
            transform: rotate(-90deg);

            -webkit-transform-origin: 50% 50%;
            -moz-transform-origin: 50% 50%;
            -ms-transform-origin: 50% 50%;
            -o-transform-origin: 50% 50%;
            transform-origin: 50% 50%;

            font-size: 12px;
            position: relative;
        }
    </style>
</head>
<body style="background-image: none; background-color: #FFF">
<center>

    <br>
    Plaguicidas que se deben eliminar del informe<br><br>
    <br/>
    <table border="1" bordercolor="#000000" cellpadding="0" cellspacing="0" width="98%">
        <tr align="center">
            <td><strong></td>
            <td><strong>RECUPERACI&Oacute;N*</strong></td>
            <td><strong>CONTROL**</strong></td>
        </tr>
        <tr>
            <td align="center"><b>CL</b></td>
            <td><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></td>
            <td></td>
        </tr>
        <tr>
            <td align="center"><b>CG</b></td>
            <td><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></td>
            <td></td>
        </tr>
    </table>
</center>
<font size="-2">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Los plaguicidas indicados en la columna de RECUPERACI&oacute;N se deben sacar a
    todas las mustras del lote.<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Las indicadas en la columna CONTROL aplica:
    <ul>
        <li>En CG para todas las muestras del lote.</li>
        <li>En CL solamente para el n&uacute;mero de muestras indicado.</li>

    </ul>
</font>

<center>
    <br/>
    <br/>
    <input type="button" value="Imprimir" class="boton2" onClick="window.print()">
</center>
</body>
</html>