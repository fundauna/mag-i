<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _historial_ensayos();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('n4', 'hr', 'Servicios :: Historial de ensayos') ?>
<?= $Gestor->Encabezado('N0004', 'e', 'Historial de ensayos') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center">
            <tr>
                <td class="titulo" colspan="3">Filtro</td>
            </tr>
            <tr>
                <td># de solicitud:</td>
                <td><input type="text" id="dato" name="dato" value="<?= $_POST['dato'] ?>" size="13" maxlength="13"/>
                </td>
                <td><input type="button" value="Buscar" class="boton" onclick="SolicitudesBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:600px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>Muestra</th>
                <th>M&eacute;todo</th>
                <th>Resultado</th>
                <th>Cumple</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->Historial();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><?=$ROW[$x]['ref'] ?></td>
                    <td><a href="#"
                           onclick="EnsayoVer('<?= $ROW[$x]['xanalizar'] ?>', '<?= $ROW[$x]['id'] ?>', '<?= $ROW[$x]['tipo'] ?>', '<?= $ROW[$x]['pagina'] ?>')"><?= $ROW[$x]['nombre'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['resultado'] ?></td>
                    <td>
                        <?php if ($ROW[$x]['cumple'] == '1') { ?>
                            <img src="<?php $Gestor->Incluir('aprobar', 'bkg') ?>" title="S�" class="tab3"/>
                        <?php } else { ?>
                            <img src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="No" class="tab3"/>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</center>
<?= $Gestor->Encabezado('N0004', 'p', '') ?>
</body>
</html>