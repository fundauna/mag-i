<?php
define('__MODULO__', 'servicios');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _solicitud03P_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW)
    die('Registro inexistente');
$_POST['muestras'] = $ROW[0]['muestras'];
$_TIPO = 1;

if ($ROW[0]['estado'] != '' && $ROW[0]['estado'] != '0')
    $disabled = 'disabled';
else
    $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" name="ID" id="ID" value="<?= $ROW[0]['cs'] ?>"/>
<input type="hidden" name="accion" id="accion" value="<?= $_GET['acc'] ?>"/>
<center>
    <?php $Gestor->Incluir('n14', 'hr', 'Servicios :: Solicitud de An&aacute;lisis de Plaguicidas') ?>
    <?= $Gestor->Encabezado('N0014', 'e', 'Solicitud de An&aacute;lisis de Plaguicidas') ?>
    <br>
    <table class="radius" style="font-size:12px" width="630px">
        <tr>
            <td class="titulo" colspan="4">1. Informaci&oacute;n General</td>
        </tr>
        <tr>
            <td><strong>N&uacute;mero de solicitud:</strong></td>
            <td><?= $_GET['ID'] ?></td>
            <td><strong>Estado:</strong></td>
            <td><?= $Gestor->Estado($ROW[0]['estado']) ?></td>
        </tr>
        <tr>
            <td>Solicitud de cotizaci&oacute;n origen:</td>
            <td><input type="text" id="solicitud" name="solicitud" value="<?= $ROW[0]['solicitud'] ?>" size="12"
                       maxlength="12" onblur="SolicitudCarga(this.value)" <?= $disabled ?>></td>
            <td>Fecha:</td>
            <td><?= $ROW[0]['fecha'] ?></td>
        </tr>
        <tr>
            <td>Tipo de muestra:</td>
            <td><select id="tipo" name="tipo" onchange="CambiaTipo()" <?= $disabled ?>>
                    <option value="1">Fertilizantes</option>
                    <option value="2" selected>Plaguicidas</option>
                </select></td>
            <td>Prop&oacute;sito:</td>
            <td><input type="text" id="proposito" name="proposito" size="30" maxlength="50" <?= $disabled ?>
                       value="<?= $ROW[0]['proposito'] ?>"/></td>
        </tr>
        <tr>
            <td>Dependencia:</td>
            <td><select id="dependencia" name="dependencia" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['dependencia'] == '0') {
                        echo 'selected';
                        $_TIPO = 2;
                    } ?>>Venta de servicios
                    </option>
                    <option value="1" <?php if ($ROW[0]['dependencia'] == '1') echo 'selected'; ?>>
                        Fiscalizaci&oacute;n
                    </option>
                    <option value="2" <?php if ($ROW[0]['dependencia'] == '2') echo 'selected'; ?>>Registro</option>
                </select></td>
            <td>Observaciones:</td>
            <td><textarea rows="5" cols="27" id="obs" name="obs" maxlength="200"
                          <?= $disabled ?>><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
        <tr>
            <td>Solicitante:</td>
            <td><input type="text" id="tmp" name="tmp" class="lista" readonly onclick="ClientesLista()"
                       value="<?= $ROW[0]['tmp'] ?>" <?= $disabled ?>/>
                <input type="hidden" id="cliente" name="cliente" value="<?= $ROW[0]['cliente'] ?>"/></td>
            <td>Entregadas por:</td>
            <td><input type="text" id="entregadas" name="entregadas" size="30" maxlength="50"
                       value="<?= $ROW[0]['entregadas'] ?>" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td>Direcci&oacute;n solicitante:</td>
            <td><textarea style="height:32px" id="direccion" name="direccion" maxlength="500"
                          <?= $disabled ?>><?= $ROW[0]['direccion'] ?></textarea></td>
        </tr>
        <tr>
            <td>Muestreo:</td>
            <td>Realizado por el cliente</td>
            <td>Firma:</td>
            <td>______________________________</td>
        </tr>
        <?php
        if (isset($ROW[0]['suministro'])) {
            ?>
            <tr>
                <td>Suministro del est&aacute;ndar:</td>
                <td><select disabled>
                        <option value="0">Laboratorio</option>
                        <option <?php if ($ROW[0]['suministro'] == '1') echo 'selected' ?>>Cliente</option>
                    </select>
                <td>Muestra de discrepancia:</td>
                <td><select disabled>
                        <option value="0">No</option>
                        <option <?php if ($ROW[0]['discre'] == '1') echo 'selected' ?>>S&iacute;</option>
                    </select>
                </td>
            </tr>
            <?php
            if ($ROW[0]['discre'] == '1') {
                ?>
                <tr>
                    <td># Solicitud:</td>
                    <td><?= $ROW[0]['num_sol'] ?></td>
                    <td>C&oacute;digo(s) externo:</td>
                    <td><?= $ROW[0]['num_mue'] ?></td>
                </tr>
                <?php
            }
        }
        ?>
        <?php if ($_GET['acc'] == 'M' && $ROW[0]['estado'] != '5') { ?>
            <tr>
                <td>Anexos:</td>
                <td colspan="3"><img onclick="Documentos('<?= $ROW[0]['cs'] ?>', '<?= $ROW[0]['estado'] ?>')"
                                     src="<?php $Gestor->Incluir('certificados', 'bkg') ?>"
                                     title="Documentos relacionados" class="tab2"/></td>
            </tr>
        <?php } ?>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="630px">
        <thead>
        <tr>
            <td class="titulo" colspan="5">2. Ingredientes
                activos <?php if ($ROW[0]['estado'] == '' or $ROW[0]['estado'] == '0') { ?>&nbsp;<img
                    onclick="ElementosMas()" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                    class="tab" /><?php } ?></td>
        </tr>
        <tr>
            <td colspan="5">Formulaci&oacute;n:&nbsp;
                <select id="tipo_form" <?= $disabled ?> onchange="BorraIngredientes()">
                    <option value="">...</option>
                    <?php
                    $ROW2 = $Gestor->Formulaciones();
                    for ($x = 0; $x < count($ROW2); $x++) {
                        ?>
                        <option value="<?= $ROW2[$x]['id'] ?>" <?php if ($ROW[0]['tipo_form'] === $ROW2[$x]['id']) echo 'selected'; ?>><?= $ROW2[$x]['nombre'] ?></option>
                        <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <hr/>
            </td>
        </tr>
        <tr align="center">
            <td><strong>#</strong></td>
            <td><strong>Nombre</strong></td>
            <td><strong>Conc. declarada</strong></td>
            <td><strong>Unidad</strong></td>
            <!--<td><strong>Fuente</strong></td>-->
        </tr>
        </thead>
        <tbody id="lolo2">
        <?php
        $ROW2 = $Gestor->SolicitudElementos();
        for ($x = 0; $x < count($ROW2); $x++) {
            ?>
            <tr align="center" id="trElementos<?= $x ?>" name="trElementos">
                <td name="numLinea"><?= $x + 1 ?>.</td>
                <td><input type="text" id="elementos<?= $x ?>" name="elementos" class="lista2" readonly
                           onclick="ElementosLista(<?= $x ?>)" value="<?= $ROW2[$x]['analisis'] ?>" <?= $disabled ?>/>
                    <input type="hidden" id="codigoB<?= $x ?>" name="codigoB" value="<?= $ROW2[$x]['codigo'] ?>"/>
                </td>
                <td><input type="text" step="0.01" onblur="validaNumero(this)" name="rango" size="10" maxlength="20"
                           value="<?= $ROW2[$x]['rango'] ?>" <?= $disabled ?>></td>
                <td><select name="unidad" onchange="CambiaTipoAnalisis()" <?= $disabled ?>>
                        <option value="">...</option>
                        <option value="0" <?php if ($ROW2[$x]['unidad'] == '0') echo 'selected'; ?>>%m/m</option>
                        <option value="1" <?php if ($ROW2[$x]['unidad'] == '1') echo 'selected'; ?>>%m/v</option>
                    </select></td>
                <!--<td><input type="text" name="fuente" size="20" maxlength="30" value="<?= $ROW2[$x]['fuente'] ?>" <?= $disabled ?>></td>-->
                <?php if ($disabled == '') { ?>
                    <td><img onclick="IngredientesEliminar(<?= $x ?>)" src="<?php $Gestor->Incluir('del', 'bkg') ?>"
                             name="ingredienteElimina" title="Eliminar" class="tab2"/></td>
                <?php } ?>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <br/>
    <table class="radius" style="font-size:12px;" width="630px">
        <thead>
        <tr>
            <td class="titulo" colspan="5">3. An&aacute;lisis
                solicitados <?php if ($ROW[0]['estado'] == '' or $ROW[0]['estado'] == '0') { ?>&nbsp;<img
                    onclick="AnalisisMas()" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                    class="tab" /><?php } ?></td>
        </tr>
        <tr>
            <td><strong>#</strong></td>
            <td><strong>Nombre</strong></td>
        </tr>
        </thead>
        <tbody id="lolo">
        <?php
        $ROW2 = $Gestor->SolicitudAnalisis();
        for ($x = 0; $x < count($ROW2); $x++) {
            if ($ROW2[$x]['analisis'] == 'Ingrediente Activo')
                $disabled2 = 'disabled';
            else
                $disabled2 = '';
            ?>
            <tr id="trAnalisis<?= $x ?>" name="trAnalisis">
                <td name="anaLinea"><?= $x + 1 ?>.</td>
                <td><input type="text" id="analisis<?= $x ?>" name="analisis" class="lista2" readonly
                           onclick="AnalisisForLista(<?= $x ?>)" value="<?= $ROW2[$x]['analisis'] ?>" <?= $disabled ?>/>
                    <input type="hidden" id="codigo<?= $x ?>" name="codigo" value="<?= $ROW2[$x]['codigo'] ?>"/>
                </td>
                <!--<td><input type="text" id="declarada<?= $x ?>" name="declarada" size="10" maxlength="20" value="<?= $ROW2[$x]['rango'] ?>" <?= $disabled ?> <?= $disabled2 ?>></td>
                                <td><select id="uc<?= $x ?>" name="uc" onchange="CambiaTipoAnalisis()" <?= $disabled ?> <?= $disabled2 ?>>
                                        <option value="">...</option>
                                        <option value="0" <?php if ($ROW2[$x]['unidad'] == '0') echo 'selected'; ?>>%m/m</option>
                                        <option value="1" <?php if ($ROW2[$x]['unidad'] == '1') echo 'selected'; ?>>%m/v</option>
                                </select></td>-->
                <!--<td><input type="text" id="lugar<?= $x ?>" name="lugar" size="20" maxlength="30" value="<?= $ROW2[$x]['fuente'] ?>" <?= $disabled ?> <?= $disabled2 ?>></td>-->
                <?php if ($disabled == '') { ?>
                    <td><img onclick="AnalisisEliminar(<?= $x ?>)" src="<?php $Gestor->Incluir('del', 'bkg') ?>"
                             name="analisisElimina" title="Eliminar" class="tab2"/></td>
                <?php } ?>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="630px">
        <thead>
        <tr>
            <td class="titulo" colspan="3">4.
                Impurezas <?php if ($ROW[0]['estado'] == '' or $ROW[0]['estado'] == '0') { ?>&nbsp;<img
                    onclick="ImpurezasMas()" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                    class="tab" /><?php } ?></td>
        </tr>
        <tr>
            <td><strong>#</strong></td>
            <td><strong>Nombre</strong></td>
        </tr>
        </thead>
        <tbody id="lolo3">
        <?php
        $ROW2 = $Gestor->SolicitudImpurezas();
        for ($x = 0; $x < count($ROW2); $x++) {
            ?>
            <tr id="trImpurezas<?= $x ?>" name="trImpurezas">
                <td name="impLinea"><?= $x + 1 ?>.</td>
                <td><input type="text" id="impurezas<?= $x ?>" name="impurezas" class="lista2" readonly
                           onclick="ImpurezasLista(<?= $x ?>)" value="<?= $ROW2[$x]['analisis'] ?>" <?= $disabled ?>/>
                    <input type="hidden" id="codigoC<?= $x ?>" name="codigoC" value="<?= $ROW2[$x]['codigo'] ?>"/>
                </td>
                <td><img onclick="ImpurezasEliminar(<?= $x ?>)" src="<?php $Gestor->Incluir('del', 'bkg') ?>"
                         name="impurezasElimina" title="Eliminar" class="tab2"/></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="630px" cellspacing="0">
        <tr>
            <td class="titulo" colspan="5">5.
                Muestras <?php if ($ROW[0]['estado'] == '' or $ROW[0]['estado'] == '0') { ?>&nbsp;<img
                    onclick="MuestrasMas()" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                    class="tab" /><?php } ?></td>
        </tr>
        <tr>
            <td>Producto:</td>
            <td><input type="text" id="producto" size="20" maxlength="50" value="<?= $ROW[0]['producto'] ?>"
                       <?= $disabled ?>/></td>
            <td>Densidad:</td>
            <td><select id="densidad" <?= $disabled ?> onchange="ValidaDensidad()">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['densidad'] == '0') echo 'selected'; ?>>No</option>
                    <option value="1" <?php if ($ROW[0]['densidad'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td>M&eacute;todo suministrado por el cliente?:</td>
            <td>
                <select id="metodo" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['metodo'] == '0') echo 'selected'; ?>>No</option>
                    <option value="1" <?php if ($ROW[0]['metodo'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select></td>
            <td># Registro:</td>
            <td><input type="text" id="registro" size="15" maxlength="20" value="<?= $ROW[0]['registro'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td>Dosis de mayor aplicaci&oacute;n:</td>
            <td colspan="3"><textarea id="dosis" style="width:97%"><?= $ROW[0]['dosis'] ?></textarea></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <!--<td>Dosis de mayor aplicaci&oacute;n:</td>
                            <td><input type="text" id="dosis" size="10" maxlength="10" value="<?= $ROW[0]['dosis'] ?>" <?= $disabled ?> /></td>-->
            <td style="display:none">Tipo de mezcla:</td>
            <td style="display:none"><select id="mezcla" <?= $disabled ?> style="width:150px">
                    <option value="0" <?php if ($ROW[0]['mezcla'] == '0') echo 'selected'; ?>>Mezcla f&iacute;sica
                    </option>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <hr/>
            </td>
        </tr>
        <tbody id="lolo4">
        <?php
        $ROW2 = $Gestor->SolicitudMuestras();
        for ($x = 0; $x < count($ROW2); $x++) {
            if ($ROW[0]['estado'] != '' and $ROW[0]['estado'] != '5') {
                $cod_int = str_replace('LCC-', '', $ROW[0]['cs']) . '-' . ($x + 1) . '-' . $_TIPO;
            } else
                $cod_int = '';
            ?>
            <tr id="tr1Muestras<?= $x ?>" name="tr1Muestras" bgcolor="#CCCCCC">
                <td><strong>C&oacute;digo externo:</strong></td>
                <td><input type="text" name="cod_ext" size="15" maxlength="50"
                           value="<?= str_replace(' ', '', $ROW2[$x]['cod_ext']); ?>" <?= $disabled ?>/></td>
                <td><strong>Interno:</strong></td>
                <td><?= $cod_int ?></td>
            </tr>
            <tr id="tr2Muestras<?= $x ?>" name="tr2Muestras">
                <td>Recipiente:</td>
                <td><select name="recipiente" <?= $disabled ?>>
                        <option value="">...</option>
                        <option value="0" <?php if ($ROW2[$x]['recipiente'] == '0') echo 'selected'; ?>>Bolsa
                            metalizada
                        </option>
                        <option value="1" <?php if ($ROW2[$x]['recipiente'] == '1') echo 'selected'; ?>>Bolsa pl&aacute;stica</option>
                        <option value="2" <?php if ($ROW2[$x]['recipiente'] == '2') echo 'selected'; ?>>Frasco pl&aacute;stico</option>
                        <option value="3" <?php if ($ROW2[$x]['recipiente'] == '3') echo 'selected'; ?>>Frasco de
                            vidrio
                        </option>
                    </select></td>
                <td>Sellado:</td>
                <td><select name="sellado" <?= $disabled ?>>
                        <option value="">...</option>
                        <option value="0" <?php if ($ROW2[$x]['sellado'] == '0') echo 'selected'; ?>>No</option>
                        <option value="1" <?php if ($ROW2[$x]['sellado'] == '1') echo 'selected'; ?>>S&iacute;</option>
                    </select></td>
            </tr>
            <tr id="tr3Muestras<?= $x ?>" name="tr3Muestras">
                <td>Lote fabricaci&oacute;n:</td>
                <td><input type="text" name="lote" size="15" maxlength="20" value="<?= $ROW2[$x]['lote'] ?>"
                           <?= $disabled ?>/></td>
                <td>Rechazada?:</td>
                <td><select name="rechazada" <?= $disabled ?>>
                        <option value="">...</option>
                        <option value="0" <?php if ($ROW2[$x]['rechazada'] == '0') echo 'selected'; ?>>No</option>
                        <option value="1" <?php if ($ROW2[$x]['rechazada'] == '1') echo 'selected'; ?>>S&iacute;
                        </option>
                    </select></td>
            </tr>
            <tr id="tr4Muestras<?= $x ?>" name="tr4Muestras">
                <td>Observaci&oacute;n:</td>
                <td colspan="2"><input type="text" name="obse" size="30" maxlength="50" value="<?= $ROW2[$x]['obse'] ?>"
                                       <?= $disabled ?>/></td>
                <td>
                    <?php if ($disabled == '') { ?>
                        <input onclick="MuestrasEliminar(<?= $x ?>)" name="muestrasElimina" type="button"
                               value="Eliminar Muestra" class="boton2"/>
                    <?php } ?>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <br/><?= $Gestor->Encabezado('N0014', 'p', '') ?>
    <br/>
    <?php if ($ROW[0]['estado'] == '' or $ROW[0]['estado'] == '0') { ?>
        <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">&nbsp;
    <?php } ?>
    <?php if ($ROW[0]['estado'] == '0') { ?>
        <input type="button" value="Procesar" class="boton" onClick="Procesar()">&nbsp;
        <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
    <?php } ?>
    <?php if ($ROW[0]['estado'] == '1'/* and $Gestor->PreAprobar() */) { ?>
        <!--<input type="button" value="Preaprobar" class="boton" onClick="Preaprobar()" title="El documento est� correcto pero debe ser aprobado">&nbsp;-->
        <input type="button" value="Aprobar" class="boton" onClick="Aprobar()">&nbsp;
        <input type="button" value="Rechazar" class="boton" onClick="Rechazar()"
               title="Rechaza el documento para que sea corregido">&nbsp;
    <?php } ?>
    <?php if ($ROW[0]['estado'] == '2'/* and $Gestor->Aprobar() */) { ?>
        <input type="button" value="Imprimir" class="boton" onClick="window.print()">&nbsp;
        <input type="button" value="Pre-Anular" class="boton" onClick="AnularMedio()">&nbsp;
    <?php } ?>
    <?php if ($ROW[0]['estado'] == '6' and $Gestor->SuperAnular()) { ?>
        <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
    <?php } ?>
</center>
</body>
</html>