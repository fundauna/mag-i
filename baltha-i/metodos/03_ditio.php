<?php
//OK
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_ditio();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW)
    die('Registro inexistente');

if ($_GET['acc'] == 'V')
    $disabled = 'disabled';
else
    $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="xanalizar" value="<?= $_GET['xanalizar'] ?>"/>
<input type="hidden" id="tipo" value="<?= $_GET['tipo'] ?>"/>
<center>
    <?php $Gestor->Incluir('h4', 'hr', 'An&aacute;lisis :: Determinaci&oacute;n de Ditiocarbamatos en Formulaciones de Plaguicidas por Volumetr&iacute;a') ?>
    <?= $Gestor->Encabezado('H0004', 'e', 'Determinaci&oacute;n de Ditiocarbamatos en Formulaciones de Plaguicidas por Volumetr&iacute;a') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="3">Datos de la muestra</td>
        </tr>
        <tr>
            <td><strong>N&uacute;mero:</strong></td>
            <td><?= $ROW[0]['ref'] ?></td>
            <td><strong>Ingrediente Activo:</strong></td>
        </tr>
        <tr>
            <td><strong>Fecha de ingreso:</strong></td>
            <td><?= $ROW[0]['fechaI'] ?></td>
            <td><?= $ROW[0]['ingrediente'] ?><input type="hidden" id="ingrediente" maxlength="30"
                                                    value="<?= $ROW[0]['ingrediente'] ?>" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>Fecha de an&aacute;lisis:</strong></td>
            <td><input type="text" id="fechaA" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaA'] ?>" <?= $disabled ?>></td>
            <td><strong>Tipo de formulaci&oacute;n:</strong></td>
        </tr>
        <tr>
            <td><strong>Fecha de conclusi&oacute;n del an&aacute;lisis:</strong></td>
            <td><?= $ROW[0]['fechaC'] ?></td>
            <td><?= $ROW[0]['tipo_form'] ?><input type="hidden" id="tipo_form" value="<?= $ROW[0]['tipo_form'] ?>"/>
            </td>
        </tr>
        <tr>
            <td><strong>Concentraci&oacute;n declarada:</strong></td>
            <td><input type="text" id="rango" maxlength="20" class="monto" value="<?= $ROW[0]['rango'] ?>"
                       <?= $disabled ?>>&nbsp;
                <select id="unidad" <?php if ($ROW[0]['unidad'] != '' or $_GET['acc'] == 'V') echo 'disabled'; ?>
                        onchange="__calcula()">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['unidad'] == '0') echo 'selected'; ?>>%m/m</option>
                    <option value="1" <?php if ($ROW[0]['unidad'] == '1') echo 'selected'; ?>>%m/v</option>
                </select>
            </td>
            <td>
                <strong>Formulaci&oacute;n en aceite:</strong>&nbsp;
                <select id="aceite" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['aceite'] == '0') echo 'selected'; ?>>No</option>
                    <option value="1" <?php if ($ROW[0]['aceite'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select>
            </td>
        </tr>
        <?php if ($_GET['acc'] == 'V') { ?>
            <tr>
                <td><strong>Creado por:</strong></td>
                <td colspan="2"><?= $ROW[0]['analista'] ?></td>
            </tr>
        <?php } ?>
    </table>
    <br/>
    <input type="hidden" id="linealidad1" class="monto" onblur="Redondear(this)" value="0.0001" <?= $disabled ?>>
    <input type="hidden" id="linealidad2" class="monto" onblur="Redondear(this)" value="0.0001" <?= $disabled ?>>
    <input type="hidden" id="IECB" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['IECB'] ?>"
           <?= $disabled ?>>
    <input type="hidden" id="repeti1" class="monto" onblur="Redondear(this)" value="0.00007" <?= $disabled ?>>
    <input type="hidden" id="repeti2" class="monto" onblur="Redondear(this)" value="0.00007" <?= $disabled ?>>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="6">Datos de an&aacute;lisis</td>
        </tr>
        <tr>
            <td>Tiempo de digesti&oacute;n (min)</td>
            <td><input type="text" id="masa" class="monto" onblur="Redondear(this)" value="60"></td>
            <td></td>
            <td><strong>Dato</strong></td>
            <td><strong>Muestra 1</strong></td>
            <td><strong>Muestra 2</strong></td>
        </tr>
        <tr>
            <td>N&uacute;mero de reactivo:</td>
            <td><input type="text" id="numreactivo" value="<?= $ROW[0]['numreactivo'] ?>" maxlength="30"
                       <?= $disabled ?>/></td>
            <td></td>
            <td>Masa muestra (g)</td>
            <td><input type="text" id="muestra1" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['muestra1'] ?>" <?= $disabled ?>></td>
            <td><input type="text" id="muestra2" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['muestra2'] ?>" <?= $disabled ?>></td>
        </tr>
        <tr>
            <td>Fecha de preparaci&oacute;n:</td>
            <td><input type="text" class="fecha" id="fechaP" onclick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaP'] ?>" readonly="readonly" <?= $disabled ?>/></td>
            <td></td>
            <td>Vol. I<sub>2</sub> consumido (mL)</td>
            <td><input type="text" id="consumido1" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['consumido1'] ?>" <?= $disabled ?>></td>
            <td><input type="text" id="consumido2" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['consumido2'] ?>" <?= $disabled ?>></td>
        </tr>
        <tr>
            <td>Origen del yodo:</td>
            <td>
                <select id="origen" name="origen">
                    <option value="">...</option>
                    <option <?= $ROW[0]['aforado1'] == '1' ? 'selected' : '' ?> value="1">Ampolla</option>
                    <option <?= $ROW[0]['aforado1'] == '2' ? 'selected' : '' ?> value="2">Otro</option>
                </select>
            </td>
            <td></td>
            <td>Contenido Ditio (% m/m)</td>
            <td><input type="text" id="contenido1" class="monto" disabled></td>
            <td><input type="text" id="contenido2" class="monto" disabled></td>
        </tr>
        <tr>
            <td>Concentraci&oacute;n I<sub>2</sub> (mol/L)</td>
            <td><input type="text" class="monto" id="ampolla1" onblur="Redondear(this)"
                       value="<?= $ROW[0]['ampolla1'] ?>" <?= $disabled ?>/></td>
            <td></td>
            <td>Contenido Ditio (% m/v)</td>
            <td><input type="text" id="contenido3" class="monto" disabled></td>
            <td><input type="text" id="contenido4" class="monto" disabled></td>
        </tr>
        <tr>
            <td>Inc. expandida (mol/L)</td>
            <td><input type="text" class="monto" id="ampolla2" onblur="Redondear(this)"
                       value="<?= $ROW[0]['ampolla2'] ?>" <?= $disabled ?>/></td>
            <td colspan="4"></td>
        </tr>
        <tr>
            <td>Bal&oacute;n disoluci&oacute;n yodo</td>
            <td><select id="BA" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['BA'] == '10') echo 'selected'; ?> value="10" REP="0.008" TOL="0.02"
                                                                                 TEMP="0.006">10.00
                    </option>
                    <option <?php if ($ROW[0]['BA'] == '25') echo 'selected'; ?> value="25" REP="0.008" TOL="0.03"
                                                                                 TEMP="0.016">25.00
                    </option>
                    <option <?php if ($ROW[0]['BA'] == '50') echo 'selected'; ?> value="50" REP="0.010" TOL="0.05"
                                                                                 TEMP="0.032">50.00
                    </option>
                    <option <?php if ($ROW[0]['BA'] == '100') echo 'selected'; ?> value="100" REP="0.010" TOL="0.08"
                                                                                  TEMP="0.063">100.00
                    </option>
                    <option <?php if ($ROW[0]['BA'] == '200') echo 'selected'; ?> value="200" REP="0.030" TOL="0.1"
                                                                                  TEMP="0.126">200.00
                    </option>
                    <option <?php if ($ROW[0]['BA'] == '250') echo 'selected'; ?> value="250" REP="0.030" TOL="0.120"
                                                                                  TEMP="0.158">250.00
                    </option>
                    <option <?php if ($ROW[0]['BA'] == '500') echo 'selected'; ?> value="500" REP="0.050" TOL="0.2"
                                                                                  TEMP="0.315">500.00
                    </option>
                    <option <?php if ($ROW[0]['BA'] == '1000') echo 'selected'; ?> value="1000" REP="0.050" TOL="0.3"
                                                                                   TEMP="0.630">1000.00
                    </option>
                    <option <?php if ($ROW[0]['BA'] == '2000') echo 'selected'; ?> value="2000" REP="0.1" TOL="0.5"
                                                                                   TEMP="1.260">2000.00
                    </option>
                </select></td>
            <td colspan="4"></td>
        </tr>
        <tr>
            <td>Capacidad de la Bureta (mL)</td>
            <td><select id="bureta" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['aforado2'] == '10') echo 'selected'; ?> value="10" REP="0.010"
                                                                                       TOL="0.025" TEMP="0.006">10.00
                    </option>
                    <option <?php if ($ROW[0]['aforado2'] == '20') echo 'selected'; ?> value="20" REP="" TOL="0.030"
                                                                                       TEMP="">20.00 (Dosino)
                    </option>
                    <option <?php if ($ROW[0]['aforado2'] == '25') echo 'selected'; ?> value="25" REP="0.020"
                                                                                       TOL="0.030" TEMP="0.016">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforado2'] == '50') echo 'selected'; ?> value="50" REP="0.020"
                                                                                       TOL="0.050" TEMP="0.032">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforado2'] == '50') echo 'selected'; ?> value="50" REP="" TOL="0.050"
                                                                                       TEMP="">50.00 (Dosino)
                    </option>
                    <option <?php if ($ROW[0]['aforado2'] == '100') echo 'selected'; ?> value="100" REP="0.030"
                                                                                        TOL="0.100" TEMP="0.063">100.00
                    </option>
                </select></td>
            <td colspan="4"></td>
        </tr>
        <tr>
            <?php if ($ROW[0]['unidad'] == '1') { ?>
                <td>Densidad muestra, r (g/mL)</td>
                <td><input type="text" id="densidad" class="monto" onblur="Redondear(this)"
                           value="<?= $ROW[0]['densidad'] ?>" <?= $disabled ?>></td>
            <?php } else { ?>
                <td></td>
                <td><input type="hidden" id="densidad" class="monto" onblur="Redondear(this)" value="1.2"
                           <?= $disabled ?>></td>
            <?php } ?>
            <td colspan="4"></td>
        </tr>
        <tr>
            <td class="titulo" colspan="6">Reporte de Resultados</td>
        </tr>
        <tr>
            <td></td>
            <td align="center"><strong>Contenido promedio de Ditiocarbamato</strong></td>
            <td align="center"><strong>Incertidumbre expandida</strong></td>
            <td><strong>Unidad</strong></td>
        </tr>
        <tr>
            <td></td>
            <td align="center"><input type="text" id="promedio" class="monto" readonly></td>
            <td align="center"><input type="text" id="incertidumbre" class="monto" readonly></td>
            <td><?= $ROW[0]['unidad'] == '0' ? '%m/m' : '%m/v' ?></td>
        </tr>
        <tr>
            <td class="titulo" colspan="6">Observaciones</td>
        </tr>
        <td colspan="6"><textarea id="obs" style="width:98%" <?= $disabled ?>><?= $ROW[0]['obs'] ?></textarea></td>
        <tr>
            <td class="titulo" colspan="6">Formulas</td>
        </tr>
        <tr>
            <td colspan="6">
                <p style="text-align: center">
                    <img src="imagenes/H0004/f1.PNG" width="681" height="183" alt="f1"/>
                </p>
                <p><b>Donde:</b></p>
                <p>Vol I<sub>2</sub> consumido = Volumen de valorante de disoluci&oacute;n de Yodo consumido.</p>
                <p>Cn I<sub>2</sub> = Concentraci&oacute;n de valorante de disoluci&oacute;n de Yodo.</p>
                <p>M muestra = Masa de muestra pesada.</p>
                <p>&#961; = Densidad de la muestra.</p>
                <p>FG = Factor gravim&eacute;trico correspondiente al ditiocarbamato analizado:</p>

        <tr>
            <td><b>Ditiocarbamato</b></td>
            <td><b>Factor gravim&eacute;trico</b></td>
        </tr>
        <tr>
            <td>Ferbam</td>
            <td>138,82</td>
        </tr>
        <tr>
            <td>Mancozeb</td>
            <td>135,50</td>
        </tr>
        <tr>
            <td>Maneb</td>
            <td>132,65</td>
        </tr>
        <tr>
            <td>Metam Sodio</td>
            <td>129,20</td>
        </tr>
        <tr>
            <td>Metiram</td>
            <td>136,08</td>
        </tr>
        <tr>
            <td>Nabam</td>
            <td>128,18</td>
        </tr>
        <tr>
            <td>Propineb</td>
            <td>144,90</td>
        </tr>
        <tr>
            <td>Zineb</td>
            <td>137,87</td>
        </tr>
        <tr>
            <td>Ziram</td>
            <td>152,91</td>
        </tr>


        </td></tr>
    </table>
    <br/>
    <br/>
    <?php if ($_GET['acc'] == 'V') { ?>
        <script>__calcula();</script>
    <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php } else { ?>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('H0004', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>