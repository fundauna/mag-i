<?php
//OK
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_paraquat();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW)
    die('Registro inexistente');

if ($_GET['acc'] == 'V')
    $disabled = 'disabled';
else
    $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('estilo', 'js') ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('graficador', 'gfx'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="xanalizar" value="<?= $_GET['xanalizar'] ?>"/>
<input type="hidden" id="tipo" value="<?= $_GET['tipo'] ?>"/>
<center>
    <?php $Gestor->Incluir('h12', 'hr', 'An&aacute;lisis :: Determinaci&oacute;n de Paraquat en Formulaciones de Plaguicidas por Espectrofotometr&iacute;a Ultravioleta-Visible') ?>
    <?= $Gestor->Encabezado('H0012', 'e', 'Determinaci&oacute;n de Paraquat en Formulaciones de Plaguicidas por Espectrofotometr&iacute;a Ultravioleta-Visible') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="3">Datos de la muestra</td>
        </tr>
        <tr>
            <td>N&uacute;mero:</td>
            <td><?= $ROW[0]['ref'] ?></td>
            <td>Ingrediente Activo:</td>
        </tr>
        <tr>
            <td>Fecha de ingreso:</td>
            <td><?= $ROW[0]['fechaI'] ?></td>
            <td><?= $ROW[0]['ingrediente'] ?><input type="hidden" id="ingrediente" maxlength="30"
                                                    value="<?= $ROW[0]['ingrediente'] ?>" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td>Fecha de an&aacute;lisis:</td>
            <td><input type="text" id="fechaA" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaA'] ?>" <?= $disabled ?>></td>
            <td>Tipo de formulaci&oacute;n:</td>
        </tr>
        <tr>
            <td>Fecha de conclusi&oacute;n del an&aacute;lisis:</td>
            <td><?= $ROW[0]['fechaC'] ?></td>
            <td><?= $ROW[0]['tipo_form'] ?><input type="hidden" id="tipo_form" value="<?= $ROW[0]['tipo_form'] ?>"/>
            </td>
        </tr>
        <tr>
            <td>Concentraci&oacute;n declarada como i&oacute;n:</td>
            <td colspan="2"><input type="text" id="rango" class="monto" value="<?= $ROW[0]['rango'] ?>" <?= $disabled ?>
                                   onblur="Redondear(this)">&nbsp;
                <select id="unidad" <?php if ($ROW[0]['unidad'] != '' or $_GET['acc'] == 'V') echo 'disabled'; ?>
                        onchange="__calcula()">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['unidad'] == '0') echo 'selected'; ?>>%m/m</option>
                    <option value="1" <?php if ($ROW[0]['unidad'] == '1') echo 'selected'; ?>>%m/v</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Concentraci&oacute;n declarada como sal:</td>
            <td><input type="text" id="rango2" class="monto" value="<?= $ROW[0]['rango2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)">
                <select id="unidad2" <?php if ($ROW[0]['unidad2'] != '' or $_GET['acc'] == 'V') echo 'disabled'; ?>
                        onchange="__calcula()">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['unidad2'] == '0') echo 'selected'; ?>>%m/m</option>
                    <option value="1" <?php if ($ROW[0]['unidad2'] == '1') echo 'selected'; ?>>%m/v</option>
                </select>
            </td>
            <td><?php if ($_GET['acc'] == 'V') { ?><strong>Creado por:</strong> <?= $ROW[0]['analista'] ?><?php } ?>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Datos del an&aacute;lisis</td>
        </tr>
        <tr>
            <td>Origen y n&uacute;mero del est&aacute;ndar:</td>
            <td><input type="text" id="origen" maxlength="50" value="<?= $ROW[0]['origen'] ?>" <?= $disabled ?>/></td>
            <td>Fecha de preparaci&oacute;n de la curva:</td>
            <td><input type="text" id="fechaP" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaP'] ?>" <?= $disabled ?>></td>
        </tr>
        <tr>
            <td>Pureza del est&aacute;ndar (%):</td>
            <td><input type="text" id="pureza" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['pureza'] ?>"
                       <?= $disabled ?>/></td>
            <td>Factor gravim&eacute;trico:</td>
            <td><select id="factor" <?= $disabled ?> onchange="__calcula()">
                    <option value="1" <?php if ($ROW[0]['factor'] == '1') echo 'selected'; ?>>Dicloruro de Paraquat
                        (1,0000)
                    </option>
                    <option value="1.38057" <?php if ($ROW[0]['factor'] == '1.38057') echo 'selected'; ?>>Ion de
                        Paraquat (1,38057)
                    </option>
                </select></td>
        </tr>
        <tr>
            <td>Existe certificado de pureza:</td>
            <td><select id="existe" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['existe'] == '0') echo 'selected'; ?>>No</option>
                    <option value="1" <?php if ($ROW[0]['existe'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select></td>
            <td>Factor de diluci&oacute;n muestra 1:</td>
            <td id="dilucion1"></td>
        </tr>
        <tr>
            <td>Densidad muestra, &rho; (g/mL):</td>
            <td><input type="text" id="densidad" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['densidad'] ?>" <?= $disabled ?>/></td>
            <td>Factor de diluci&oacute;n muestra 2:</td>
            <td id="dilucion2"></td>
        </tr>
        <tr style="visibility:hidden">
            <td class="titulo" colspan="4">Datos del equipo</td>
        </tr>
        <tr style="visibility:hidden">
            <td><strong>Linealidad muestra 1:</strong></td>
            <td><input type="text" id="linealidad1" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['linealidad1'] ?>" <?= $disabled ?>></td>
            <td><strong>Linealidad muestra 2:</strong></td>
            <td><input type="text" id="linealidad2" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['linealidad2'] ?>" <?= $disabled ?>></td>
        </tr>
        <tr style="visibility:hidden">
            <td><strong>Repetibilidad muestra 1:</strong></td>
            <td><input type="text" id="repeti1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['repeti1'] ?>"
                       <?= $disabled ?>></td>
            <td><strong>Repetibilidad muestra 2:</strong></td>
            <td><input type="text" id="repeti2" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['repeti2'] ?>"
                       <?= $disabled ?>></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="7">Datos de la curva</td>
        </tr>
        <tr align="center">
            <td><strong>Disoluci&oacute;n</strong></td>
            <td><strong>Masa del est&aacute;ndar (g)</strong></td>
            <td><strong>Vol. Al&iacute;cuota (mL)</strong></td>
            <td><strong>Vol. Bal&oacute;n Aforado (mL)</strong></td>
            <td><strong>Concentraci&oacute;n(g/mL)</strong></td>
            <td><strong>Concentraci&oacute;n(&micro;g/mL)</strong></td>
            <td><strong>Absorbancia</strong></td>
        </tr>
        <tr align="center" onmouseout="mOut(this)" onmouseover="mOver(this)">
            <td>A</td>
            <td><input type="text" id="masa" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['masa'] ?>"
                       <?= $disabled ?>/></td>
            <td>No Aplica</td>
            <td><select id="bal1" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['bal1'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">10.00
                    </option>
                    <option <?php if ($ROW[0]['bal1'] == '25') echo 'selected'; ?> value="25" IEC="0.015" IR="0.0006">
                        25.00
                    </option>
                    <option <?php if ($ROW[0]['bal1'] == '50') echo 'selected'; ?> value="50" IEC="0.028" IR="0.0006">
                        50.00
                    </option>
                    <option <?php if ($ROW[0]['bal1'] == '100') echo 'selected'; ?> value="100" IEC="0.047" IR="0.0005">
                        100.00
                    </option>
                    <option <?php if ($ROW[0]['bal1'] == '200') echo 'selected'; ?> value="200" IEC="0.082" IR="0.0004">
                        200.00
                    </option>
                    <option <?php if ($ROW[0]['bal1'] == '250') echo 'selected'; ?> value="250" IEC="0.099" IR="0.0004">
                        250.00
                    </option>
                    <option <?php if ($ROW[0]['bal1'] == '500') echo 'selected'; ?> value="500" IEC="0.180" IR="0.0004">
                        500.00
                    </option>
                    <option <?php if ($ROW[0]['bal1'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                     IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['bal1'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                     IR="0.0003">2000.00
                    </option>
                </select></td>
            <td id="con1"></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr align="center" onmouseout="mOut(this)" onmouseover="mOver(this)">
            <td>B</td>
            <td>No Aplica</td>
            <td><select id="vol2" onchange="__calcula()" <?= $disabled ?>>
                    <option value="" inc="">...</option>
                    <option value="0.5" <?php if ($ROW[0]['vol2'] == '0.5') echo 'selected'; ?>>0.5</option>
                    <option value="1" <?php if ($ROW[0]['vol2'] == '1') echo 'selected'; ?>>1.00</option>
                    <option value="2" <?php if ($ROW[0]['vol2'] == '2') echo 'selected'; ?>>2.00</option>
                    <option value="3" <?php if ($ROW[0]['vol2'] == '3') echo 'selected'; ?>>3.00</option>
                    <option value="4" <?php if ($ROW[0]['vol2'] == '4') echo 'selected'; ?>>4.00</option>
                    <option value="5" <?php if ($ROW[0]['vol2'] == '5') echo 'selected'; ?>>5.00</option>
                    <option value="6" <?php if ($ROW[0]['vol2'] == '6') echo 'selected'; ?>>6.00</option>
                    <option value="7" <?php if ($ROW[0]['vol2'] == '7') echo 'selected'; ?>>7.00</option>
                    <option value="10" <?php if ($ROW[0]['vol2'] == '10') echo 'selected'; ?>>10.00</option>
                    <option value="15" <?php if ($ROW[0]['vol2'] == '15') echo 'selected'; ?>>15.00</option>
                    <option value="20" <?php if ($ROW[0]['vol2'] == '20') echo 'selected'; ?>>20.00</option>
                    <option value="25" <?php if ($ROW[0]['vol2'] == '25') echo 'selected'; ?>>25.00</option>
                    <option value="50" <?php if ($ROW[0]['vol2'] == '50') echo 'selected'; ?>>50.00</option>
                    <option value="100" <?php if ($ROW[0]['vol2'] == '100') echo 'selected'; ?>>100.00</option>
                </select></td>
            <td><select id="bal2" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['bal2'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">10.00
                    </option>
                    <option <?php if ($ROW[0]['bal2'] == '25') echo 'selected'; ?> value="25" IEC="0.015" IR="0.0006">
                        25.00
                    </option>
                    <option <?php if ($ROW[0]['bal2'] == '50') echo 'selected'; ?> value="50" IEC="0.028" IR="0.0006">
                        50.00
                    </option>
                    <option <?php if ($ROW[0]['bal2'] == '100') echo 'selected'; ?> value="100" IEC="0.047" IR="0.0005">
                        100.00
                    </option>
                    <option <?php if ($ROW[0]['bal2'] == '200') echo 'selected'; ?> value="200" IEC="0.082" IR="0.0004">
                        200.00
                    </option>
                    <option <?php if ($ROW[0]['bal2'] == '250') echo 'selected'; ?> value="250" IEC="0.099" IR="0.0004">
                        250.00
                    </option>
                    <option <?php if ($ROW[0]['bal2'] == '500') echo 'selected'; ?> value="500" IEC="0.180" IR="0.0004">
                        500.00
                    </option>
                    <option <?php if ($ROW[0]['bal2'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                     IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['bal2'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                     IR="0.0003">2000.00
                    </option>
                </select></td>
            <td id="con2"></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr align="center" onmouseout="mOut(this)" onmouseover="mOver(this)">
            <td>Patr&oacute;n 1</td>
            <td>No Aplica</td>
            <td><select id="vol3" onchange="__calcula()" <?= $disabled ?>>
                    <option value="" inc="">...</option>
                    <option value="0.5" <?php if ($ROW[0]['vol3'] == '0.5') echo 'selected'; ?> inc="0.002">0.5</option>
                    <option value="1" <?php if ($ROW[0]['vol3'] == '1') echo 'selected'; ?> inc="0.005">1.00</option>
                    <option value="2" <?php if ($ROW[0]['vol3'] == '2') echo 'selected'; ?> inc="0.01">2.00</option>
                    <option value="3" <?php if ($ROW[0]['vol3'] == '3') echo 'selected'; ?> inc="0.01">3.00</option>
                    <option value="4" <?php if ($ROW[0]['vol3'] == '4') echo 'selected'; ?> inc="0.01">4.00</option>
                    <option value="5" <?php if ($ROW[0]['vol3'] == '5') echo 'selected'; ?> inc="0.01">5.00</option>
                    <option value="6" <?php if ($ROW[0]['vol3'] == '6') echo 'selected'; ?> inc="0.02">6.00</option>
                    <option value="7" <?php if ($ROW[0]['vol3'] == '7') echo 'selected'; ?> inc="0.02">7.00</option>
                    <option value="10" <?php if ($ROW[0]['vol3'] == '10') echo 'selected'; ?> inc="0.02">10.00</option>
                    <option value="15" <?php if ($ROW[0]['vol3'] == '15') echo 'selected'; ?> inc="0.03">15.00</option>
                    <option value="20" <?php if ($ROW[0]['vol3'] == '20') echo 'selected'; ?> inc="0.03">20.00</option>
                    <option value="25" <?php if ($ROW[0]['vol3'] == '25') echo 'selected'; ?> inc="0.03">25.00</option>
                    <option value="50" <?php if ($ROW[0]['vol3'] == '50') echo 'selected'; ?> inc="0.05">50.00</option>
                    <option value="100" <?php if ($ROW[0]['vol3'] == '100') echo 'selected'; ?> inc="0.08">100.00
                    </option>
                </select></td>
            <td><select id="bal3" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['bal3'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">10.00
                    </option>
                    <option <?php if ($ROW[0]['bal3'] == '25') echo 'selected'; ?> value="25" IEC="0.015" IR="0.0006">
                        25.00
                    </option>
                    <option <?php if ($ROW[0]['bal3'] == '50') echo 'selected'; ?> value="50" IEC="0.028" IR="0.0006">
                        50.00
                    </option>
                    <option <?php if ($ROW[0]['bal3'] == '100') echo 'selected'; ?> value="100" IEC="0.047" IR="0.0005">
                        100.00
                    </option>
                    <option <?php if ($ROW[0]['bal3'] == '200') echo 'selected'; ?> value="200" IEC="0.082" IR="0.0004">
                        200.00
                    </option>
                    <option <?php if ($ROW[0]['bal3'] == '250') echo 'selected'; ?> value="250" IEC="0.099" IR="0.0004">
                        250.00
                    </option>
                    <option <?php if ($ROW[0]['bal3'] == '500') echo 'selected'; ?> value="500" IEC="0.180" IR="0.0004">
                        500.00
                    </option>
                    <option <?php if ($ROW[0]['bal3'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                     IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['bal3'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                     IR="0.0003">2000.00
                    </option>
                </select></td>
            <td></td>
            <td id="con3"></td>
            <td><input type="text" id="abs3" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['abs3'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr align="center" onmouseout="mOut(this)" onmouseover="mOver(this)">
            <td>Patr&oacute;n 2</td>
            <td>No Aplica</td>
            <td><select id="vol4" onchange="__calcula()" <?= $disabled ?>>
                    <option value="" inc="">...</option>
                    <option value="0.5" <?php if ($ROW[0]['vol4'] == '0.5') echo 'selected'; ?> inc="0.002">0.5</option>
                    <option value="1" <?php if ($ROW[0]['vol4'] == '1') echo 'selected'; ?> inc="0.005">1.00</option>
                    <option value="2" <?php if ($ROW[0]['vol4'] == '2') echo 'selected'; ?> inc="0.01">2.00</option>
                    <option value="3" <?php if ($ROW[0]['vol4'] == '3') echo 'selected'; ?> inc="0.01">3.00</option>
                    <option value="4" <?php if ($ROW[0]['vol4'] == '4') echo 'selected'; ?> inc="0.01">4.00</option>
                    <option value="5" <?php if ($ROW[0]['vol4'] == '5') echo 'selected'; ?> inc="0.01">5.00</option>
                    <option value="6" <?php if ($ROW[0]['vol4'] == '6') echo 'selected'; ?> inc="0.02">6.00</option>
                    <option value="7" <?php if ($ROW[0]['vol4'] == '7') echo 'selected'; ?> inc="0.02">7.00</option>
                    <option value="10" <?php if ($ROW[0]['vol4'] == '10') echo 'selected'; ?> inc="0.02">10.00</option>
                    <option value="15" <?php if ($ROW[0]['vol4'] == '15') echo 'selected'; ?> inc="0.03">15.00</option>
                    <option value="20" <?php if ($ROW[0]['vol4'] == '20') echo 'selected'; ?> inc="0.03">20.00</option>
                    <option value="25" <?php if ($ROW[0]['vol4'] == '25') echo 'selected'; ?> inc="0.03">25.00</option>
                    <option value="50" <?php if ($ROW[0]['vol4'] == '50') echo 'selected'; ?> inc="0.05">50.00</option>
                    <option value="100" <?php if ($ROW[0]['vol4'] == '100') echo 'selected'; ?> inc="0.08">100.00
                    </option>
                </select></td>
            <td><select id="bal4" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['bal4'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">10.00
                    </option>
                    <option <?php if ($ROW[0]['bal4'] == '25') echo 'selected'; ?> value="25" IEC="0.015" IR="0.0006">
                        25.00
                    </option>
                    <option <?php if ($ROW[0]['bal4'] == '50') echo 'selected'; ?> value="50" IEC="0.028" IR="0.0006">
                        50.00
                    </option>
                    <option <?php if ($ROW[0]['bal4'] == '100') echo 'selected'; ?> value="100" IEC="0.047" IR="0.0005">
                        100.00
                    </option>
                    <option <?php if ($ROW[0]['bal4'] == '200') echo 'selected'; ?> value="200" IEC="0.082" IR="0.0004">
                        200.00
                    </option>
                    <option <?php if ($ROW[0]['bal4'] == '250') echo 'selected'; ?> value="250" IEC="0.099" IR="0.0004">
                        250.00
                    </option>
                    <option <?php if ($ROW[0]['bal4'] == '500') echo 'selected'; ?> value="500" IEC="0.180" IR="0.0004">
                        500.00
                    </option>
                    <option <?php if ($ROW[0]['bal4'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                     IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['bal4'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                     IR="0.0003">2000.00
                    </option>
                </select></td>
            <td></td>
            <td id="con4"></td>
            <td><input type="text" id="abs4" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['abs4'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr align="center" onmouseout="mOut(this)" onmouseover="mOver(this)">
            <td>Patr&oacute;n 3</td>
            <td>No Aplica</td>
            <td><select id="vol5" onchange="__calcula()" <?= $disabled ?>>
                    <option value="" inc="">...</option>
                    <option value="0.5" <?php if ($ROW[0]['vol5'] == '0.5') echo 'selected'; ?> inc="0.002">0.5</option>
                    <option value="1" <?php if ($ROW[0]['vol5'] == '1') echo 'selected'; ?> inc="0.005">1.00</option>
                    <option value="2" <?php if ($ROW[0]['vol5'] == '2') echo 'selected'; ?> inc="0.01">2.00</option>
                    <option value="3" <?php if ($ROW[0]['vol5'] == '3') echo 'selected'; ?> inc="0.01">3.00</option>
                    <option value="4" <?php if ($ROW[0]['vol5'] == '4') echo 'selected'; ?> inc="0.01">4.00</option>
                    <option value="5" <?php if ($ROW[0]['vol5'] == '5') echo 'selected'; ?> inc="0.01">5.00</option>
                    <option value="6" <?php if ($ROW[0]['vol5'] == '6') echo 'selected'; ?> inc="0.02">6.00</option>
                    <option value="7" <?php if ($ROW[0]['vol5'] == '7') echo 'selected'; ?> inc="0.02">7.00</option>
                    <option value="10" <?php if ($ROW[0]['vol5'] == '10') echo 'selected'; ?> inc="0.02">10.00</option>
                    <option value="15" <?php if ($ROW[0]['vol5'] == '15') echo 'selected'; ?> inc="0.03">15.00</option>
                    <option value="20" <?php if ($ROW[0]['vol5'] == '20') echo 'selected'; ?> inc="0.03">20.00</option>
                    <option value="25" <?php if ($ROW[0]['vol5'] == '25') echo 'selected'; ?> inc="0.03">25.00</option>
                    <option value="50" <?php if ($ROW[0]['vol5'] == '50') echo 'selected'; ?> inc="0.05">50.00</option>
                    <option value="100" <?php if ($ROW[0]['vol5'] == '100') echo 'selected'; ?> inc="0.08">100.00
                    </option>
                </select></td>
            <td><select id="bal5" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['bal5'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">10.00
                    </option>
                    <option <?php if ($ROW[0]['bal5'] == '25') echo 'selected'; ?> value="25" IEC="0.015" IR="0.0006">
                        25.00
                    </option>
                    <option <?php if ($ROW[0]['bal5'] == '50') echo 'selected'; ?> value="50" IEC="0.028" IR="0.0006">
                        50.00
                    </option>
                    <option <?php if ($ROW[0]['bal5'] == '100') echo 'selected'; ?> value="100" IEC="0.047" IR="0.0005">
                        100.00
                    </option>
                    <option <?php if ($ROW[0]['bal5'] == '200') echo 'selected'; ?> value="200" IEC="0.082" IR="0.0004">
                        200.00
                    </option>
                    <option <?php if ($ROW[0]['bal5'] == '250') echo 'selected'; ?> value="250" IEC="0.099" IR="0.0004">
                        250.00
                    </option>
                    <option <?php if ($ROW[0]['bal5'] == '500') echo 'selected'; ?> value="500" IEC="0.180" IR="0.0004">
                        500.00
                    </option>
                    <option <?php if ($ROW[0]['bal5'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                     IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['bal5'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                     IR="0.0003">2000.00
                    </option>
                </select></td>
            <td></td>
            <td id="con5"></td>
            <td><input type="text" id="abs5" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['abs5'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr align="center" onmouseout="mOut(this)" onmouseover="mOver(this)">
            <td>Patr&oacute;n 4</td>
            <td>No Aplica</td>
            <td><select id="vol6" onchange="__calcula()" <?= $disabled ?>>
                    <option value="" inc="">...</option>
                    <option value="0.5" <?php if ($ROW[0]['vol6'] == '0.5') echo 'selected'; ?> inc="0.002">0.5</option>
                    <option value="1" <?php if ($ROW[0]['vol6'] == '1') echo 'selected'; ?> inc="0.005">1.00</option>
                    <option value="2" <?php if ($ROW[0]['vol6'] == '2') echo 'selected'; ?> inc="0.01">2.00</option>
                    <option value="3" <?php if ($ROW[0]['vol6'] == '3') echo 'selected'; ?> inc="0.01">3.00</option>
                    <option value="4" <?php if ($ROW[0]['vol6'] == '4') echo 'selected'; ?> inc="0.01">4.00</option>
                    <option value="5" <?php if ($ROW[0]['vol6'] == '5') echo 'selected'; ?> inc="0.01">5.00</option>
                    <option value="6" <?php if ($ROW[0]['vol6'] == '6') echo 'selected'; ?> inc="0.02">6.00</option>
                    <option value="7" <?php if ($ROW[0]['vol6'] == '7') echo 'selected'; ?> inc="0.02">7.00</option>
                    <option value="10" <?php if ($ROW[0]['vol6'] == '10') echo 'selected'; ?> inc="0.02">10.00</option>
                    <option value="15" <?php if ($ROW[0]['vol6'] == '15') echo 'selected'; ?> inc="0.03">15.00</option>
                    <option value="20" <?php if ($ROW[0]['vol6'] == '20') echo 'selected'; ?> inc="0.03">20.00</option>
                    <option value="25" <?php if ($ROW[0]['vol6'] == '25') echo 'selected'; ?> inc="0.03">25.00</option>
                    <option value="50" <?php if ($ROW[0]['vol6'] == '50') echo 'selected'; ?> inc="0.05">50.00</option>
                    <option value="100" <?php if ($ROW[0]['vol6'] == '100') echo 'selected'; ?> inc="0.08">100.00
                    </option>
                </select></td>
            <td><select id="bal6" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['bal6'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">10.00
                    </option>
                    <option <?php if ($ROW[0]['bal6'] == '25') echo 'selected'; ?> value="25" IEC="0.015" IR="0.0006">
                        25.00
                    </option>
                    <option <?php if ($ROW[0]['bal6'] == '50') echo 'selected'; ?> value="50" IEC="0.028" IR="0.0006">
                        50.00
                    </option>
                    <option <?php if ($ROW[0]['bal6'] == '100') echo 'selected'; ?> value="100" IEC="0.047" IR="0.0005">
                        100.00
                    </option>
                    <option <?php if ($ROW[0]['bal6'] == '200') echo 'selected'; ?> value="200" IEC="0.082" IR="0.0004">
                        200.00
                    </option>
                    <option <?php if ($ROW[0]['bal6'] == '250') echo 'selected'; ?> value="250" IEC="0.099" IR="0.0004">
                        250.00
                    </option>
                    <option <?php if ($ROW[0]['bal6'] == '500') echo 'selected'; ?> value="500" IEC="0.180" IR="0.0004">
                        500.00
                    </option>
                    <option <?php if ($ROW[0]['bal6'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                     IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['bal6'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                     IR="0.0003">2000.00
                    </option>
                </select></td>
            <td></td>
            <td id="con6"></td>
            <td><input type="text" id="abs6" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['abs6'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td colspan="7">
                <hr/>
            </td>
        </tr>
        <tr align="center">
            <td><strong>Pendiente (m)</strong></td>
            <td><strong>Intercepto (b)</strong></td>
            <td><strong>r</strong></td>
            <td><strong>SXY</strong></td>
            <td colspan="2"><strong>Sm</strong></td>
            <td><strong>Sb</strong></td>
        </tr>
        <tr align="center">
            <td id="pendiente"></td>
            <td id="intercepto"></td>
            <td id="r"></td>
            <td id="sxy"></td>
            <td id="sm" colspan="2"></td>
            <td id="sb"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="7">Datos de la muestra</td>
        </tr>
        <tr align="center">
            <td><strong>Disoluci&oacute;n</strong></td>
            <td><strong>Masa de la muestra 1 (g)</strong></td>
            <td><strong>Vol. Al&iacute;cuota (mL)</strong></td>
            <td><strong>Vol. Bal&oacute;n Aforado (mL)</strong></td>
            <td><strong>Concent.<br/>calculada (&micro;g/mL)</strong></td>
            <td><strong>Concent.<br/>encontrada (&micro;g/mL)</strong></td>
            <td><strong>Absorbancia</strong></td>
        </tr>
        <tr align="center" onmouseout="mOut(this)" onmouseover="mOver(this)">
            <td>I</td>
            <td><input type="text" id="mm1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['mm1'] ?>"
                       <?= $disabled ?>/></td>
            <td></td>
            <td><select id="bb1" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['bb1'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">10.00
                    </option>
                    <option <?php if ($ROW[0]['bb1'] == '25') echo 'selected'; ?> value="25" IEC="0.015" IR="0.0006">
                        25.00
                    </option>
                    <option <?php if ($ROW[0]['bb1'] == '50') echo 'selected'; ?> value="50" IEC="0.028" IR="0.0006">
                        50.00
                    </option>
                    <option <?php if ($ROW[0]['bb1'] == '100') echo 'selected'; ?> value="100" IEC="0.047" IR="0.0005">
                        100.00
                    </option>
                    <option <?php if ($ROW[0]['bb1'] == '200') echo 'selected'; ?> value="200" IEC="0.082" IR="0.0004">
                        200.00
                    </option>
                    <option <?php if ($ROW[0]['bb1'] == '250') echo 'selected'; ?> value="250" IEC="0.099" IR="0.0004">
                        250.00
                    </option>
                    <option <?php if ($ROW[0]['bb1'] == '500') echo 'selected'; ?> value="500" IEC="0.180" IR="0.0004">
                        500.00
                    </option>
                    <option <?php if ($ROW[0]['bb1'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                    IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['bb1'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                    IR="0.0003">2000.00
                    </option>
                </select></td>
            <td id="cal1"></td>
            <td colspan="2"></td>
        </tr>
        <tr align="center" onmouseout="mOut(this)" onmouseover="mOver(this)">
            <td>II</td>
            <td></td>
            <td><select id="aa2" onchange="__calcula()" <?= $disabled ?>>
                    <option value="" inc="">...</option>
                    <option value="0.5" <?php if ($ROW[0]['aa2'] == '0.5') echo 'selected'; ?> inc="0.0049">0.5</option>
                    <option value="1" <?php if ($ROW[0]['aa2'] == '1') echo 'selected'; ?> inc="0.0056">1.00</option>
                    <option value="2" <?php if ($ROW[0]['aa2'] == '2') echo 'selected'; ?> inc="0.0020">2.00</option>
                    <option value="3" <?php if ($ROW[0]['aa2'] == '3') echo 'selected'; ?> inc="0.0020">3.00</option>
                    <option value="4" <?php if ($ROW[0]['aa2'] == '4') echo 'selected'; ?> inc="0.0020">4.00</option>
                    <option value="5" <?php if ($ROW[0]['aa2'] == '5') echo 'selected'; ?> inc="0.0020">5.00</option>
                    <option value="6" <?php if ($ROW[0]['aa2'] == '6') echo 'selected'; ?> inc="0.0020">6.00</option>
                    <option value="7" <?php if ($ROW[0]['aa2'] == '7') echo 'selected'; ?> inc="0.0020">7.00</option>
                    <option value="10" <?php if ($ROW[0]['aa2'] == '10') echo 'selected'; ?> inc="0.0020">10.00</option>
                    <option value="15" <?php if ($ROW[0]['aa2'] == '15') echo 'selected'; ?> inc="0.0020">15.00</option>
                    <option value="20" <?php if ($ROW[0]['aa2'] == '20') echo 'selected'; ?> inc="0.0020">20.00</option>
                    <option value="25" <?php if ($ROW[0]['aa2'] == '25') echo 'selected'; ?> inc="0.0020">25.00</option>
                    <option value="50" <?php if ($ROW[0]['aa2'] == '50') echo 'selected'; ?> inc="0.0020">50.00</option>
                    <option value="100" <?php if ($ROW[0]['aa2'] == '100') echo 'selected'; ?> inc="0.0020">100.00
                    </option>
                </select></td>
            <td><select id="bb2" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['bb2'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">10.00
                    </option>
                    <option <?php if ($ROW[0]['bb2'] == '25') echo 'selected'; ?> value="25" IEC="0.015" IR="0.0006">
                        25.00
                    </option>
                    <option <?php if ($ROW[0]['bb2'] == '50') echo 'selected'; ?> value="50" IEC="0.028" IR="0.0006">
                        50.00
                    </option>
                    <option <?php if ($ROW[0]['bb2'] == '100') echo 'selected'; ?> value="100" IEC="0.047" IR="0.0005">
                        100.00
                    </option>
                    <option <?php if ($ROW[0]['bb2'] == '200') echo 'selected'; ?> value="200" IEC="0.082" IR="0.0004">
                        200.00
                    </option>
                    <option <?php if ($ROW[0]['bb2'] == '250') echo 'selected'; ?> value="250" IEC="0.099" IR="0.0004">
                        250.00
                    </option>
                    <option <?php if ($ROW[0]['bb2'] == '500') echo 'selected'; ?> value="500" IEC="0.180" IR="0.0004">
                        500.00
                    </option>
                    <option <?php if ($ROW[0]['bb2'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                    IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['bb2'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                    IR="0.0003">2000.00
                    </option>
                </select></td>
            <td id="cal2"></td>
            <td colspan="2"></td>
        </tr>
        <tr align="center" onmouseout="mOut(this)" onmouseover="mOver(this)">
            <td rowspan="3">III</td>
            <td rowspan="3"></td>
            <td rowspan="3"><select id="aa3" onchange="__calcula()" <?= $disabled ?>>
                    <option value="" inc="">...</option>
                    <option value="0.5" <?php if ($ROW[0]['aa3'] == '0.5') echo 'selected'; ?> inc="0.0049">0.5</option>
                    <option value="1" <?php if ($ROW[0]['aa3'] == '1') echo 'selected'; ?> inc="0.0056">1.00</option>
                    <option value="2" <?php if ($ROW[0]['aa3'] == '2') echo 'selected'; ?> inc="0.0020">2.00</option>
                    <option value="3" <?php if ($ROW[0]['aa3'] == '3') echo 'selected'; ?> inc="0.0020">3.00</option>
                    <option value="4" <?php if ($ROW[0]['aa3'] == '4') echo 'selected'; ?> inc="0.0020">4.00</option>
                    <option value="5" <?php if ($ROW[0]['aa3'] == '5') echo 'selected'; ?> inc="0.0020">5.00</option>
                    <option value="6" <?php if ($ROW[0]['aa3'] == '6') echo 'selected'; ?> inc="0.0020">6.00</option>
                    <option value="7" <?php if ($ROW[0]['aa3'] == '7') echo 'selected'; ?> inc="0.0020">7.00</option>
                    <option value="10" <?php if ($ROW[0]['aa3'] == '10') echo 'selected'; ?> inc="0.0020">10.00</option>
                    <option value="15" <?php if ($ROW[0]['aa3'] == '15') echo 'selected'; ?> inc="0.0020">15.00</option>
                    <option value="20" <?php if ($ROW[0]['aa3'] == '20') echo 'selected'; ?> inc="0.0020">20.00</option>
                    <option value="25" <?php if ($ROW[0]['aa3'] == '25') echo 'selected'; ?> inc="0.0020">25.00</option>
                    <option value="50" <?php if ($ROW[0]['aa3'] == '50') echo 'selected'; ?> inc="0.0020">50.00</option>
                    <option value="100" <?php if ($ROW[0]['aa3'] == '100') echo 'selected'; ?> inc="0.0020">100.00
                    </option>
                </select></td>
            <td rowspan="3"><select id="bb3" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['bb3'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">10.00
                    </option>
                    <option <?php if ($ROW[0]['bb3'] == '25') echo 'selected'; ?> value="25" IEC="0.015" IR="0.0006">
                        25.00
                    </option>
                    <option <?php if ($ROW[0]['bb3'] == '50') echo 'selected'; ?> value="50" IEC="0.028" IR="0.0006">
                        50.00
                    </option>
                    <option <?php if ($ROW[0]['bb3'] == '100') echo 'selected'; ?> value="100" IEC="0.047" IR="0.0005">
                        100.00
                    </option>
                    <option <?php if ($ROW[0]['bb3'] == '200') echo 'selected'; ?> value="200" IEC="0.082" IR="0.0004">
                        200.00
                    </option>
                    <option <?php if ($ROW[0]['bb3'] == '250') echo 'selected'; ?> value="250" IEC="0.099" IR="0.0004">
                        250.00
                    </option>
                    <option <?php if ($ROW[0]['bb3'] == '500') echo 'selected'; ?> value="500" IEC="0.180" IR="0.0004">
                        500.00
                    </option>
                    <option <?php if ($ROW[0]['bb3'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                    IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['bb3'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                    IR="0.0003">2000.00
                    </option>
                </select></td>
            <td id="cal3"></td>
            <td><input type="text" id="cur3" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['cur3'] ?>"
                       <?= $disabled ?>/></td>
            <td><input type="text" id="sor3" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['sor3'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr align="center" onmouseout="mOut(this)" onmouseover="mOver(this)">
            <td id="cal4"></td>
            <td><input type="text" id="cur4" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['cur4'] ?>"
                       <?= $disabled ?>/></td>
            <td><input type="text" id="sor4" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['sor4'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr align="center" onmouseout="mOut(this)" onmouseover="mOver(this)">
            <td id="cal5"></td>
            <td><input type="text" id="cur5" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['cur5'] ?>"
                       <?= $disabled ?>/></td>
            <td><input type="text" id="sor5" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['sor5'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td colspan="7">
                <hr/>
            </td>
        </tr>
        <tr align="center">
            <td><strong>Disoluci&oacute;n</strong></td>
            <td><strong>Masa de la muestra 2 (g)</strong></td>
            <td><strong>Vol. Al&iacute;cuota (mL)</strong></td>
            <td><strong>Vol. Bal&oacute;n Aforado (mL)</strong></td>
            <td><strong>Concent.<br/>calculada (&micro;g/mL)</strong></td>
            <td><strong>Concent.<br/>encontrada (&micro;g/mL)</strong></td>
            <td><strong>Absorbancia</strong></td>
        </tr>
        <tr align="center" onmouseout="mOut(this)" onmouseover="mOver(this)">
            <td>I</td>
            <td><input type="text" id="mm6" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['mm6'] ?>"
                       <?= $disabled ?>/></td>
            <td></td>
            <td><select id="bb6" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['bb6'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">10.00
                    </option>
                    <option <?php if ($ROW[0]['bb6'] == '25') echo 'selected'; ?> value="25" IEC="0.015" IR="0.0006">
                        25.00
                    </option>
                    <option <?php if ($ROW[0]['bb6'] == '50') echo 'selected'; ?> value="50" IEC="0.028" IR="0.0006">
                        50.00
                    </option>
                    <option <?php if ($ROW[0]['bb6'] == '100') echo 'selected'; ?> value="100" IEC="0.047" IR="0.0005">
                        100.00
                    </option>
                    <option <?php if ($ROW[0]['bb6'] == '200') echo 'selected'; ?> value="200" IEC="0.082" IR="0.0004">
                        200.00
                    </option>
                    <option <?php if ($ROW[0]['bb6'] == '250') echo 'selected'; ?> value="250" IEC="0.099" IR="0.0004">
                        250.00
                    </option>
                    <option <?php if ($ROW[0]['bb6'] == '500') echo 'selected'; ?> value="500" IEC="0.180" IR="0.0004">
                        500.00
                    </option>
                    <option <?php if ($ROW[0]['bb6'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                    IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['bb6'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                    IR="0.0003">2000.00
                    </option>
                </select></td>
            <td id="cal6"></td>
            <td colspan="2"></td>
        </tr>
        <tr align="center" onmouseout="mOut(this)" onmouseover="mOver(this)">
            <td>II</td>
            <td></td>
            <td><select id="aa7" onchange="__calcula()" <?= $disabled ?>>
                    <option value="" inc="">...</option>
                    <option value="0.5" <?php if ($ROW[0]['aa7'] == '0.5') echo 'selected'; ?> inc="0.002">0.5</option>
                    <option value="1" <?php if ($ROW[0]['aa7'] == '1') echo 'selected'; ?> inc="0.005">1.00</option>
                    <option value="2" <?php if ($ROW[0]['aa7'] == '2') echo 'selected'; ?> inc="0.01">2.00</option>
                    <option value="3" <?php if ($ROW[0]['aa7'] == '3') echo 'selected'; ?> inc="0.01">3.00</option>
                    <option value="4" <?php if ($ROW[0]['aa7'] == '4') echo 'selected'; ?> inc="0.01">4.00</option>
                    <option value="5" <?php if ($ROW[0]['aa7'] == '5') echo 'selected'; ?> inc="0.01">5.00</option>
                    <option value="6" <?php if ($ROW[0]['aa7'] == '6') echo 'selected'; ?> inc="0.02">6.00</option>
                    <option value="7" <?php if ($ROW[0]['aa7'] == '7') echo 'selected'; ?> inc="0.02">7.00</option>
                    <option value="10" <?php if ($ROW[0]['aa7'] == '10') echo 'selected'; ?> inc="0.02">10.00</option>
                    <option value="15" <?php if ($ROW[0]['aa7'] == '15') echo 'selected'; ?> inc="0.03">15.00</option>
                    <option value="20" <?php if ($ROW[0]['aa7'] == '20') echo 'selected'; ?> inc="0.03">20.00</option>
                    <option value="25" <?php if ($ROW[0]['aa7'] == '25') echo 'selected'; ?> inc="0.03">25.00</option>
                    <option value="50" <?php if ($ROW[0]['aa7'] == '50') echo 'selected'; ?> inc="0.05">50.00</option>
                    <option value="100" <?php if ($ROW[0]['aa7'] == '100') echo 'selected'; ?> inc="0.08">100.00
                    </option>
                </select></td>
            <td><select id="bb7" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['bb7'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">10.00
                    </option>
                    <option <?php if ($ROW[0]['bb7'] == '25') echo 'selected'; ?> value="25" IEC="0.015" IR="0.0006">
                        25.00
                    </option>
                    <option <?php if ($ROW[0]['bb7'] == '50') echo 'selected'; ?> value="50" IEC="0.028" IR="0.0006">
                        50.00
                    </option>
                    <option <?php if ($ROW[0]['bb7'] == '100') echo 'selected'; ?> value="100" IEC="0.047" IR="0.0005">
                        100.00
                    </option>
                    <option <?php if ($ROW[0]['bb7'] == '200') echo 'selected'; ?> value="200" IEC="0.082" IR="0.0004">
                        200.00
                    </option>
                    <option <?php if ($ROW[0]['bb7'] == '250') echo 'selected'; ?> value="250" IEC="0.099" IR="0.0004">
                        250.00
                    </option>
                    <option <?php if ($ROW[0]['bb7'] == '500') echo 'selected'; ?> value="500" IEC="0.180" IR="0.0004">
                        500.00
                    </option>
                    <option <?php if ($ROW[0]['bb7'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                    IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['bb7'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                    IR="0.0003">2000.00
                    </option>
                </select></td>
            <td id="cal7"></td>
            <td colspan="2"></td>
        </tr>
        <tr align="center" onmouseout="mOut(this)" onmouseover="mOver(this)">
            <td rowspan="3">III</td>
            <td rowspan="3"></td>
            <td rowspan="3"><select id="aa8" onchange="__calcula()" <?= $disabled ?>>
                    <option value="" inc="">...</option>
                    <option value="0.5" <?php if ($ROW[0]['aa8'] == '0.5') echo 'selected'; ?> inc="0.002">0.5</option>
                    <option value="1" <?php if ($ROW[0]['aa8'] == '1') echo 'selected'; ?> inc="0.005">1.00</option>
                    <option value="2" <?php if ($ROW[0]['aa8'] == '2') echo 'selected'; ?> inc="0.01">2.00</option>
                    <option value="3" <?php if ($ROW[0]['aa8'] == '3') echo 'selected'; ?> inc="0.01">3.00</option>
                    <option value="4" <?php if ($ROW[0]['aa8'] == '4') echo 'selected'; ?> inc="0.01">4.00</option>
                    <option value="5" <?php if ($ROW[0]['aa8'] == '5') echo 'selected'; ?> inc="0.01">5.00</option>
                    <option value="6" <?php if ($ROW[0]['aa8'] == '6') echo 'selected'; ?> inc="0.02">6.00</option>
                    <option value="7" <?php if ($ROW[0]['aa8'] == '7') echo 'selected'; ?> inc="0.02">7.00</option>
                    <option value="10" <?php if ($ROW[0]['aa8'] == '10') echo 'selected'; ?> inc="0.02">10.00</option>
                    <option value="15" <?php if ($ROW[0]['aa8'] == '15') echo 'selected'; ?> inc="0.03">15.00</option>
                    <option value="20" <?php if ($ROW[0]['aa8'] == '20') echo 'selected'; ?> inc="0.03">20.00</option>
                    <option value="25" <?php if ($ROW[0]['aa8'] == '25') echo 'selected'; ?> inc="0.03">25.00</option>
                    <option value="50" <?php if ($ROW[0]['aa8'] == '50') echo 'selected'; ?> inc="0.05">50.00</option>
                    <option value="100" <?php if ($ROW[0]['aa8'] == '100') echo 'selected'; ?> inc="0.08">100.00
                    </option>
                </select></td>
            <td rowspan="3"><select id="bb8" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['bb8'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">10.00
                    </option>
                    <option <?php if ($ROW[0]['bb8'] == '25') echo 'selected'; ?> value="25" IEC="0.015" IR="0.0006">
                        25.00
                    </option>
                    <option <?php if ($ROW[0]['bb8'] == '50') echo 'selected'; ?> value="50" IEC="0.028" IR="0.0006">
                        50.00
                    </option>
                    <option <?php if ($ROW[0]['bb8'] == '100') echo 'selected'; ?> value="100" IEC="0.047" IR="0.0005">
                        100.00
                    </option>
                    <option <?php if ($ROW[0]['bb8'] == '200') echo 'selected'; ?> value="200" IEC="0.082" IR="0.0004">
                        200.00
                    </option>
                    <option <?php if ($ROW[0]['bb8'] == '250') echo 'selected'; ?> value="250" IEC="0.099" IR="0.0004">
                        250.00
                    </option>
                    <option <?php if ($ROW[0]['bb8'] == '500') echo 'selected'; ?> value="500" IEC="0.180" IR="0.0004">
                        500.00
                    </option>
                    <option <?php if ($ROW[0]['bb8'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                    IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['bb8'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                    IR="0.0003">2000.00
                    </option>
                </select></td>
            <td id="cal8"></td>
            <td><input type="text" id="cur8" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['cur8'] ?>"
                       <?= $disabled ?>/></td>
            <td><input type="text" id="sor8" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['sor8'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr align="center" onmouseout="mOut(this)" onmouseover="mOver(this)">
            <td id="cal9"></td>
            <td><input type="text" id="cur9" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['cur9'] ?>"
                       <?= $disabled ?>/></td>
            <td><input type="text" id="sor9" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['sor9'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr align="center" onmouseout="mOut(this)" onmouseover="mOver(this)">
            <td id="calA"></td>
            <td><input type="text" id="curA" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['curA'] ?>"
                       <?= $disabled ?>/></td>
            <td><input type="text" id="sorA" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['sorA'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Resultados finales</td>
        </tr>
        <tr align="center">
            <td>&nbsp;</td>
            <td><strong>Contenido de Paraquat</strong></td>
            <td><strong>Promedio</strong></td>
            <td><strong>Incertidumbre expandida</strong></td>
        </tr>
        <tr align="center">
            <td><strong>Muestra 1</strong></td>
            <td id="fin1"></td>
            <td rowspan="2" id="promedio"></td>
            <td rowspan="2" id="EX"></td>
        </tr>
        <tr align="center">
            <td><strong>Muestra 2</strong></td>
            <td id="fin2"></td>
        </tr>
    </table>
    <table style="display:none">
        <?php
        for ($x = 0; $x < 6; $x++) {
            ?>
            <tr>
                <td id="_CN<?= $x ?>"></td>
                <td id="_AR<?= $x ?>"></td>
                <td id="_A<?= $x ?>"></td>
                <td id="_B<?= $x ?>"></td>
                <td id="_C<?= $x ?>"></td>
                <td id="_D<?= $x ?>"></td>
                <td id="_E<?= $x ?>"></td>
                <td id="_F<?= $x ?>"></td>
                <td id="_G<?= $x ?>"></td>
                <td id="_H<?= $x ?>"></td>
                <td id="_I<?= $x ?>"></td>
            </tr>
        <?php } ?>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo">Gr&aacute;fico</td>
        </tr>
        <?php if ($_GET['acc'] == 'I') { ?>
            <tr>
                <td>
                    <input type="button" id="btn" value="Generar" class="boton" onClick="Generar()"></a>
                </td>
            </tr>
        <?php } ?>
        <tr style="display:none;">
            <td id="td_tabla"></td>
        </tr>
        <tr align="center" id="tr_grafico" style="display:none;">
            <td>
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="6">Observaciones</td>
        </tr>
        <tr>
            <td colspan="6"><textarea id="obs" style="width:98%" <?= $disabled ?>><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
        <tr>
            <td class="titulo" colspan="6">Formulas</td>
        </tr>
        <tr>
            <td colspan="6">
                <p><b>Preparaci&oacute;n de la curva de calibraci&oacute;n</b></p>
                <p>Disoluci&oacute;n madre:</p>
                <p style="text-align: center">
                    <img src="imagenes/H0012/f1.PNG" width="430" height="80" alt="f1"/>
                </p>
                <p><b>Donde:</b></p>
                <p>Mstd = Masa pesada del est&aacute;ndar</p>
                <p>Pstd = Pureza del est&aacute;ndar</p>
                <p>Vol = Volumen del matraz en el que se diluye el est&aacute;ndar</p>
                <p>FG = Factor gravim&eacute;trico =</p>

                <table border="1">
                    <thead>
                    <tr>
                        <th>Declaraci&oacute;n de est&aacute;ndar de paraquat usado</th>
                        <th>Factor gravim&eacute;trico</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Dicloruro de Paraquat</td>
                        <td>1,00</td>
                    </tr>
                    <tr>
                        <td>Ion de Paraquat</td>
                        <td>1,38057</td>
                    </tr>
                    </tbody>
                </table>


                <p>Patrones de la curva de calibraci&oacute;n:</p>
                <p style="text-align: center">
                    <img src="imagenes/H0012/f2.PNG" width="621" height="73" alt="f2"/>
                </p>

                <p>Donde:</p>
                <p>Cn DM = Concentraci&oacute;n de la disoluci&oacute;n madre.</p>
                <p>FD = Factor de diluci&oacute;n correspondiente a cada patr&oacute;n de la curva =</p>

                <table border="1">
                    <thead>
                    <tr>
                        <th>Patr&oacute;n de curva</th>
                        <th>Factor de diluci&oacute;n</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>0,05 = 5/100</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>0,1 = 10/100</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>0,15 = 15/100</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>0,20 = 20/100</td>
                    </tr>
                    </tbody>
                </table>


                <p>M&iacute;nimos cuadrados, obtenci&oacute;n de la ecuaci&oacute;n de la recta de mejor ajuste, curva
                    de calibraci&oacute;n:</p>
                <p>Obtenci&oacute;n de pendiente de la recta de mejor ajuste de curva de calibraci&oacute;n:</p>
                <p style="text-align: center">
                    <img src="imagenes/H0012/f3.PNG" width="697" height="293" alt="f3"/>
                </p>
                <p>
                    M&iacute;nimos cuadrados, obtenci&oacute;n del intercepto de la recta de mejor ajuste de curva de
                    calibraci&oacute;n:
                </p>
                <p style="text-align: center">
                    <img src="imagenes/H0012/f4.PNG" width="314" height="71" alt="f4"/>
                </p>
                <p>
                    Ecuaci&oacute;n de la recta de mejor ajuste, curva de calibraci&oacute;n:
                </p>
                <p style="text-align: center">
                    <img src="imagenes/H0012/f5.PNG" width="347" height="67" alt="f5"/>
                </p>

                <p>
                    <b>Obtenci&oacute;n de la concentraci&oacute;n de paraquat en la muestra:</b>
                </p>
                <p style="text-align: center">
                    <img src="imagenes/H0012/f6.PNG" width="712" height="454" alt="f6"/>
                </p>
            </td>
        </tr>
    </table>
    <br/>
    <br/>
    <?php if ($_GET['acc'] == 'V') { ?>
        <script>__calcula();
            Generar();</script>
    <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php } else { ?>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('H0012', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>