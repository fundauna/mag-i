<?php
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_potasio();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

if ($_GET['acc'] == 'V') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir('math.min', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="xanalizar" value="<?= $_GET['xanalizar'] ?>"/>
<input type="hidden" id="tipo" value="<?= $_GET['tipo'] ?>"/>
<center>
    <?php $Gestor->Incluir('h14', 'hr', 'An&aacute;lisis :: Determinaci&oacute;n de Potasio en Fertilizantes por M&eacute;todo Volum&eacute;trico') ?>
    <?= $Gestor->Encabezado('H0014', 'e', 'Determinaci&oacute;n de Potasio en Fertilizantes por M&eacute;todo Volum&eacute;trico') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Datos de la muestra</td>
        </tr>
        <tr>
            <td><strong>N&uacute;mero de muestra:</strong></td>
            <td><?= $ROW[0]['ref'] ?></td>
            <td><strong>Solicitud:</strong></td>
            <td><?= $ROW[0]['solicitud'] ?></td>
        </tr>
        <tr>
            <td><strong>Fecha de ingreso:</strong></td>
            <td><?= $ROW[0]['fechaI'] ?></td>
            <td><strong>Fecha de an&aacute;lisis:</strong></td>
            <td><input type="text" id="fechaA" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaA'] ?>" <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Fecha de conclusi&oacute;n del an&aacute;lisis:</strong></td>
            <td><?= $ROW[0]['fechaC'] ?></td>
            <td><strong>% Potasio (K<sub>2</sub>O) declarado:</strong></td>
            <td><input type="text" id="rango" maxlength="20" class="monto" value="<?= $ROW[0]['rango'] ?>"
                       <?= $disabled ?>>&nbsp;
                <select id="unidad" <?php if ($ROW[0]['unidad'] != '' or $_GET['acc'] == 'V') echo 'disabled'; ?>
                        onchange="__calcula()">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['unidad'] == '0') echo 'selected'; ?>>%m/m</option>
                    <option value="1" <?php if ($ROW[0]['unidad'] == '1') echo 'selected'; ?>>%m/v</option>
                </select>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr align="center">
            <td colspan="6"><strong>Datos del an&aacute;lisis</strong></td>
        </tr>
        <tr>
            <td>mL CTAB estandarizaci&oacute;n reactivos (mL)</td>
            <td><input type="text" id="reactivos" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['reactivos'] ?>" <?= $disabled ?>></td>
            <td>Factor c&aacute;lculo F</td>
            <td><input type="text" id="factor1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['factor1'] ?>"
                       <?= $disabled ?>></td>
            <td>Volumen STFB estandarizado (mL)</td>
            <td><input type="text" id="volumen1" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['volumen1'] ?>" <?= $disabled ?>></td>
        </tr>
        <tr>
            <td>Factor F (%K<sub>2</sub>O/mL STFB)</td>
            <td id="factor2"></td>
            <td>Densidad (g/mL)</td>
            <td><input type="text" id="densidad" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['densidad'] ?>" <?= $disabled ?>></td>
            <td>Volumen STFB agregado (mL)</td>
            <td><input type="text" id="volumen2" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['volumen2'] ?>" <?= $disabled ?>></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="5">Titulaci&oacute;n de muestras</td>
        </tr>
        <tr align="center">
            <td>Muestra</td>
            <td>Masa (g)</td>
            <td>Volumen CTAB (mL)</td>
            <td>%m/m K<sub>2</sub>O</td>
            <td>% m/v K<sub>2</sub>O</td>
        </tr>
        <tr align="center">
            <td>1</td>
            <td><input type="text" id="masaA1" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['masaA1'] ?>"
                       <?= $disabled ?>></td>
            <td><input type="text" id="masaB1" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['masaB1'] ?>"
                       <?= $disabled ?>></td>
            <td id="mmA1"></td>
            <td id="mmB1"></td>
        </tr>
        <tr align="center">
            <td>2</td>
            <td><input type="text" id="masaA2" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['masaA2'] ?>"
                       <?= $disabled ?>></td>
            <td><input type="text" id="masaB2" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['masaB2'] ?>"
                       <?= $disabled ?>></td>
            <td id="mmA2"></td>
            <td id="mmB2"></td>
        </tr>
        <tr align="center">
            <td colspan="2"></td>
            <td style="color:#009900"><strong>Promedio</strong></td>
            <td id="promA"></td>
            <td id="promB"></td>
        </tr>
        <tr align="center">
            <td colspan="2"></td>
            <td><strong>Desviaci&oacute;n est&aacute;ndar</strong></td>
            <td id="desvA"></td>
            <td id="desvB"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="75%" >
        <tr>
            <td class="titulo" colspan="3">Incertidumbre de la Densidad</td>
        </tr>
            <tr align="center">
                <td></td>
                <td align="center"><strong>Incertidumbre por densidad</strong></td>
                <td>Componente</td>
            </tr>
            <tr align="center">
                <td>Inc. Certificado</td>
                <td><input type="text" id="cert" class="monto" onblur="Redondear2(this);" value="<?= isset($ROW[0]['icCert']) == true ? $ROW[0]['icCert'] : '0' ?>" <?= $disabled ?> ></td>
                <td id="inCer"</td>
            </tr>
            <tr align="center">

                <td>Inc. Resoluci&oacute;n</td>
                <td><input type="text" id="res" class="monto" onblur="Redondear2(this);"  value="<?= isset($ROW[0]['icRes']) == true ? $ROW[0]['icRes'] : '0' ?>" <?= $disabled ?> ></td>
                <td id="incRes"></td>
            </tr>
    </table>
<br/>



    <table class="radius" style="font-size:12px" width="98%" >
        <tr>
            <td class="titulo" colspan="5">C&aacute;lculo de Incertidumbre</td>
        </tr>
        <tr align="center">
            <td></td>
            <td><strong>Valor</strong></td>
            <td><strong>Inc./Desv. Est.</strong></td>
            <td><strong>Inc. Estandarizada</strong></td>
            <td><strong>Cuadrados</strong></td>
        </tr>
        <tr align="center">
            <td>Vol. CTAB cons.(mL) prom.</td>
            <td id="val1"></td>
            <td><input type="text" id="des1" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['des1'] ?>"
                       <?= $disabled ?>></td>
            <td id="est1"></td>
            <td id="cua1"></td>
        </tr>
        <tr align="center">
            <td>Factor F</td>
            <td id="val2"></td>
            <td id="des2"></td>
            <td id="est2"></td>
            <td id="cua2"></td>
        </tr>
        <tr align="center">
            <td>Vol. STFB cons.(mL)</td>
            <td id="val3"></td>
            <td><input type="text" id="des3" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['des3'] ?>"
                       <?= $disabled ?>></td>
            <td id="est3"></td>
            <td id="cua3"></td>
        </tr>
        <tr align="center">
            <td>Densidad (g/mL)</td>
            <td id="val4"></td>
          <!-- <td><input type="text" id="des4" class="monto" onblur="Redondear2(this);" value="<?= $ROW[0]['des4'] ?>"
                       <?= $disabled ?>-->
             <td id="des4"></td>
            <td id="est4"></td>
            <td id="cua4"></td>
        </tr>
        <tr align="center">
            <td>Desviaci&oacute;n est&aacute;ndar % m/m</td>
            <td id=""></td>
            <td id="des51"></td>
            <td id="est51"></td>
            <td id="cua51"></td>
        </tr>
        <tr align="center">
            <td>Desviaci&oacute;n est&aacute;ndar  % m/v </td>
            <td id=""></td>
            <td id="des5"></td>
            <td id="est5"></td>
            <td id="cua5"></td>
        </tr>



    </table>
<br/>

    <table class="radius" style="font-size:12px" width="50%" >
        <tr>
            <td class="titulo" colspan="5">Resultados de Inc. para muestras</td>
        </tr>
        <tr align="center">
            <td><strong>Descripci&oacute;n</strong></td>
            <td><strong>Inc. % m/m</strong></td>
            <td><strong>Inc. % m/v</strong></td>
        </tr>
        <tr align="center">
            <td>Muestra 1</td>
            <td id="incMue1mm"></td>
            <td id="incMue1mv"></td>
        </tr>

        <tr align="center">
            <td>Muestra 2</td>
            <td id="incMue2mm"></td>
            <td id="incMue2mv"></td>
        </tr>

    </table>
    <br/>




    <table class="radius" style="font-size:12px" width="75%" >
        <tr>
            <td class="titulo" colspan="5">Resultados finales de incertidumbre</td>
        </tr>
        <tr align="center">
            <td><strong>Descripci&oacute;n</strong></td>
            <td><strong>Valores de Inc.</strong></td>
        </tr>
        <tr align="center">
            <td>Inc f&oacute;rmula % m/m Max.</td>
            <td id="suma1"></td>
        </tr>

        <tr align="center">
            <td>Inc f&oacute;rmula % m/v Max.</td>
            <td id="suma2"></td>
        </tr>
        <tr align="center">
            <td>IFC % m/m</td>
            <td id="suma3"></td>
        </tr>
        <tr align="center">
            <td>IFC % m/v</td>
            <td id="suma4"></td>
        </tr>
        <tr align="center">
            <td>IE % m/m</td>
            <td id="suma5"></td>
        </tr>
        <tr align="center">
            <td>IE % m/v</td>
            <td id="suma6"></td>
        </tr>
    </table>


    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo">Observaciones</td>
        </tr>
        <tr>
            <td><textarea id="obs" style="width:98%" <?= $disabled ?>><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>

        <tr>
            <td class="titulo" colspan="6">Formulas</td>
        </tr>
        <tr>
            <td colspan="6">
                <p style="text-align: center">
                    <img src="imagenes/H0014/f1.PNG" alt="f1"/>
                </p>

                <p><b>Donde:</b></p>
                <p>V<sub>STFB</sub>: volumen de soluci&oacute;n de tetrafenilborato de sodio adicionado a la muestra en
                    mL.</p>
                <p>V<sub>CTAB</sub>: volumen de soluci&oacute;n de bromuro de cetiltrimetilamonio consumidos en titulaci&oacute;n
                    en mL</p>
                <p>&#961;: densidad en g/mL</p>

                <p>m = Masa de muestra (g)</p>
                <p>F = Factor de conversi&oacute;n de volumen de STFB consumido a % K<sub>2</sub>O obtenido (% K<sub>2</sub>O / mL STFB)
                 = 34.61 / (43-mL CTAB)</p>
                <p>% m/m = g / 100 g </p>
                <p>% m/v = g / 100 mL </p>
            </td>
        </tr>


    </table>
    <br/>
    <?php
    if ($_GET['acc'] == 'V') {
        $ROW2 = $Gestor->Analista();
        echo "<br /><table class='radius' width='98%'><tr><td><strong>Realizado por:</strong> {$ROW2[0]['analista']}</td></tr></table>";
    }
    ?>
    <br/>
    <script>__calcula();</script>
    <?php if ($_GET['acc'] == 'V') { ?>
        <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php } else { ?>
        <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('H0014', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>