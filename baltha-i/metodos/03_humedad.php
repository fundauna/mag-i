<?php
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_humedad();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

if ($_GET['acc'] == 'V') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="xanalizar" value="<?= $_GET['xanalizar'] ?>"/>
<input type="hidden" id="tipo" value="<?= $_GET['tipo'] ?>"/>
<center>
    <?php $Gestor->Incluir('h5', 'hr', 'An&aacute;lisis :: Hoja de calculo para la  determinacion de humedad en fertilizantes y enmiendas') ?>
    <?= $Gestor->Encabezado('H0005', 'e', 'Hoja de calculo para la  determinacion de humedad en fertilizantes y enmiendas') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Datos de la muestra</td>
        </tr>
        <tr>
            <td><strong>N&uacute;mero de muestra:</strong></td>
            <td><?= $ROW[0]['ref'] ?></td>
            <td><strong>Solicitud:</strong></td>
            <td><?= $ROW[0]['solicitud'] ?></td>
        </tr>
        <tr>
            <td><strong>Fecha de ingreso:</strong></td>
            <td><?= $ROW[0]['fechaI'] ?></td>
            <td><strong>Fecha de an&aacute;lisis:</strong></td>
            <td><input type="text" id="fechaA" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaA'] ?>" <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Fecha de conclusi&oacute;n del an&aacute;lisis:</strong></td>
            <td><?= $ROW[0]['fechaC'] ?></td>
            <td><strong>Concentraci&oacute;n declarada:</strong></td>
            <td><input type="text" id="rango" maxlength="20" class="monto" value="<?= $ROW[0]['rango'] ?>"
                       <?= $disabled ?>>&nbsp;
                <select id="unidad" <?php if ($ROW[0]['unidad'] != '' or $_GET['acc'] == 'V') echo 'disabled'; ?>
                        onchange="__calcula()">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['unidad'] == '0') echo 'selected'; ?>>%m/m</option>
                    <option value="1" <?php if ($ROW[0]['unidad'] == '1') echo 'selected'; ?>>%m/v</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><strong>Cantidad de repeticiones:</strong></td>
            <td><input type="text" id="repeticiones" maxlength="20" class="monto" value="<?= $ROW[0]['repeticiones'] ?>"
                    <?= $disabled ?>>&nbsp;
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="5">C&aacute;lculos</td>
        </tr>
        <tr align="center">
            <td>
                <table width="98%" align="center">
                    <tr>
                        <td></td>
                        <td><strong>Masa (g)</strong></td>
                        <td><strong>Error de medida relativo (%, certificado)</strong></td>
                        <td><strong>Masa de muestra corregida (mg)</strong></td>
                    </tr>
                    <tr>
                        <td rowspan="2">Recipiente</td>
                        <td><input type="text" id="recipientemasa1" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['recipientemasa1'] ?>"
                                <?= $disabled ?>></td>
                        <td><input type="text" id="recipienteerror1" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['recipienteerror1'] ?>"
                                <?= $disabled ?>></td>
                        <td><input type="text" id="recipientemasacorregida1" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['recipientemasacorregida1'] ?>"
                                <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td><input type="text" id="recipientemasa2" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['recipientemasa2'] ?>"
                                <?= $disabled ?>></td>
                        <td><input type="text" id="recipienteerror2" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['recipienteerror2'] ?>"
                                <?= $disabled ?>></td>
                        <td><input type="text" id="recipientemasacorregida2" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['recipientemasacorregida2'] ?>"
                                <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td rowspan="2">Recipiente + Muestra</td>
                        <td><input type="text" id="recipientemuestramasa1" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['recipientemuestramasa1'] ?>"
                                <?= $disabled ?>></td>
                        <td><input type="text" id="recipientemuestraerror1" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['recipientemuestraerror1'] ?>"
                                <?= $disabled ?>></td>
                        <td><input type="text" id="recipientemuestramasacorregida1" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['recipientemuestramasacorregida1'] ?>"
                                <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td><input type="text" id="recipientemuestramasa2" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['recipientemuestramasa2'] ?>"
                                <?= $disabled ?>></td>
                        <td><input type="text" id="recipientemuestraerror2" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['recipientemuestraerror2'] ?>"
                                <?= $disabled ?>></td>
                        <td><input type="text" id="recipientemuestramasacorregida2" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['recipientemuestramasacorregida2'] ?>"
                                <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td rowspan="2">Final recipiente + muestra</td>
                        <td><input type="text" id="recipientefinalmasa1" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['recipientefinalmasa1'] ?>"
                                <?= $disabled ?>></td>
                        <td><input type="text" id="recipientefinalerror1" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['recipientefinalerror1'] ?>"
                                <?= $disabled ?>></td>
                        <td><input type="text" id="recipientefinalmasacorregida1" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['recipientefinalmasacorregida1'] ?>"
                                <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td><input type="text" id="recipientefinalmasa2" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['recipientefinalmasa2'] ?>"
                                <?= $disabled ?>></td>
                        <td><input type="text" id="recipientefinalerror2" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['recipientefinalerror2'] ?>"
                                <?= $disabled ?>></td>
                        <td><input type="text" id="recipientefinalmasacorregida2" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['recipientefinalmasacorregida2'] ?>"
                                <?= $disabled ?>></td>
                    </tr>
                </table>
            </td>

        </tr>
        <tr>
            <td colspan="5">
                <hr/>
            </td>
        </tr>
        <tr align="center">
            <td colspan="2" width="48%">
                <!-- -->
                <table class="radius" width="100%">
                    <tr align="center">
                        <td></td>
                        <td colspan="2"><strong>R&eacute;plica</strong></td>
                        <td></td>
                    </tr>
                    <tr align="center">
                        <td></td>
                        <td><strong>1</strong></td>
                        <td><strong>2</strong></td>
                        <td><strong>Promedio</strong></td>
                    </tr>
                    <tr align="center">
                        <td>Masa recipiente g</td>
                        <td><input type="text" id="masarecipiente1" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['masarecipiente1'] ?>" <?= $disabled ?>></td>
                        <td><input type="text" id="masarecipiente2" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['masarecipiente2'] ?>" <?= $disabled ?>></td>
                        <td><input type="text" id="masarecipientepromedio" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['masarecipientepromedio'] ?>" disabled></td>
                    </tr>
                    <tr align="center">
                        <td>Masa inicial recipiente +  muestra g</td>
                        <td><input type="text" id="masainicialrecipiente1" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['masainicialrecipiente1'] ?>" <?= $disabled ?>></td>
                        <td><input type="text" id="masainicialrecipiente2" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['masainicialrecipiente2'] ?>" <?= $disabled ?>></td>
                        <td><input type="text" id="masainicialrecipientepromedio" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['masainicialrecipientepromedio'] ?>" disabled></td>
                    </tr>
                    <tr align="center">
                        <td>Masa inicial muestra g</td>
                        <td><input type="text" id="masainicialmuestra1" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['masainicialmuestra1'] ?>" <?= $disabled ?>></td>
                        <td><input type="text" id="masainicialmuestra2" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['masainicialmuestra2'] ?>" <?= $disabled ?>></td>
                        <td><input type="text" id="masainicialmuestrapromedio" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['masainicialmuestrapromedio'] ?>" disabled></td>
                    </tr>
                    <tr align="center">
                        <td>Masa final recipiente + muestra g</td>
                        <td><input type="text" id="masafinalrecipiente1" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['masafinalrecipiente1'] ?>" <?= $disabled ?>></td>
                        <td><input type="text" id="masafinalrecipiente2" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['masafinalrecipiente2'] ?>" <?= $disabled ?>></td>
                        <td><input type="text" id="masafinalrecipientepromedio" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['masafinalrecipientepromedio'] ?>" disabled></td>
                    </tr>
                    <tr align="center">
                        <td>Masa final muestra g</td>
                        <td><input type="text" id="masafinalmuestra1" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['masafinalmuestra1'] ?>" <?= $disabled ?>></td>
                        <td><input type="text" id="masafinalmuestra2" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['masafinalmuestra2'] ?>" <?= $disabled ?>></td>
                        <td><input type="text" id="masafinalmuestrapromedio" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['masafinalmuestrapromedio'] ?>" disabled></td>
                    </tr>
                    <tr align="center">
                        <td>Humedad %</td>
                        <td><input type="text" id="humedad1" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['humedad1'] ?>" <?= $disabled ?>></td>
                        <td><input type="text" id="humedad2" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['humedad2'] ?>" <?= $disabled ?>></td>
                        <td><input type="text" id="humedadpromedio" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['humedadpromedio'] ?>" disabled></td>
                    </tr>
                    <tr align="center">
                        <td>Promedio Humedad %</td>
                        <td><input type="text" id="promediohumedad" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['promediohumedad'] ?>" disabled></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr align="center">
                        <td>Desviaci&oacute;n Est&aacute;ndar</td>
                        <td><input type="text" id="humedaddesvest" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['humedaddesvest'] ?>" disabled></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <!-- -->
            </td>
            <td>&nbsp;</td>
            <td colspan="2" width="48%">
                <!-- -->
                <table class="radius" width="100%">
                    <tr align="center">
                        <td colspan="3"><strong>Incertidumbre</strong></td>
                    </tr>
                    <tr align="center">
                        <td><strong>Pesaje de muestra</strong></td>
                        <td><strong>Valor</strong></td>
                        <td><strong>Componente</strong></td>
                    </tr>
                    <tr align="center">
                        <td>Repetibilidad  (n=10) r</td>
                        <td><input type="text" id="repetibilidadval" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['repetibilidadval'] ?>" <?= $disabled ?>></td>
                        <td><input type="text" id="repetibilidadcomp" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['repetibilidadcomp'] ?>" disabled></td>
                    </tr>
                    <tr align="center">
                        <td>EMR</td>
                        <td><input type="text" id="emrval" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['emrval'] ?>" <?= $disabled ?>></td>
                        <td><input type="text" id="emvcomp" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['emvcomp'] ?>" disabled></td>
                    </tr>
                    <tr align="center">
                        <td>Resoluci&oacute;n Res</td>
                        <td><input type="text" id="resval" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['resval'] ?>" <?= $disabled ?>></td>
                        <td><input type="text" id="rescomp" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['rescomp'] ?>" disabled></td>
                    </tr>
                    <tr align="center">
                        <td>Linealidad (k=2) L</td>
                        <td><input type="text" id="linealval" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['linealval'] ?>" <?= $disabled ?>></td>
                        <td><input type="text" id="linealcomp" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['linealcomp'] ?>" disabled></td>
                    </tr>
                    <tr align="center">
                        <td>Excentricidad (k=2) E</td>
                        <td><input type="text" id="excentricidadval" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['excentricidadval'] ?>" <?= $disabled ?>></td>
                        <td><input type="text" id="excentricidadcomp" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['excentricidadcomp'] ?>" disabled></td>
                    </tr>
                    <tr align="center">
                        <td>Inc ind ui</td>
                        <td></td>
                        <td><input type="text" id="incindcomp" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['incindcomp'] ?>" disabled></td>
                    </tr>
                    <tr align="center">
                        <td>Inc numerador uN</td>
                        <td></td>
                        <td><input type="text" id="incnumeradorcomp" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['incnumeradorcomp'] ?>" disabled></td>
                    </tr>
                    <tr align="center">
                        <td>Inc pesaje up</td>
                        <td></td>
                        <td><input type="text" id="incpesajecomp" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['incpesajecomp'] ?>" disabled></td>
                    </tr>
                    <tr align="center">
                        <td>Duplicados</td>
                        <td></td>
                        <td><input type="text" id="duplicadoscomp" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['duplicadoscomp'] ?>" disabled></td>
                    </tr>
                    <tr align="center">
                        <td>Inc DE uDE</td>
                        <td></td>
                        <td><input type="text" id="incdecomp" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['incdecomp'] ?>" disabled></td>
                    </tr>
                    <tr align="center">
                        <td>Inc Final Combinada UFC</td>
                        <td></td>
                        <td><input type="text" id="incfinalcombcomp" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['incfinalcombcomp'] ?>" disabled></td>
                    </tr>
                    <tr align="center">
                        <td>Inc expandida (k=2) UE/td>
                        <td></td>
                        <td><input type="text" id="inxexp" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['inxexp'] ?>" disabled></td>
                    </tr>
                </table>
                <!-- -->
            </td>
        </tr>
        <tr>
            <td>
                <table class="radius" width="100%">
                    <tr align="center">
                        <td colspan="2"><b>Resultado</b></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">Humedad:&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="resulhumedad" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['resulhumedad'] ?>" disabled></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">Inc expandida:&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="resulincexp" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['resulincexp'] ?>" disabled></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo">Observaciones</td>
        </tr>
        <tr>
            <td><textarea id="obs" style="width:98%" <?= $disabled ?>><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
    </table>
    <br/>
    <?php
    if ($_GET['acc'] == 'V') {
        $ROW2 = $Gestor->Analista();
        echo "<br /><table class='radius' width='98%'><tr><td><strong>Realizado por:</strong> {$ROW2[0]['analista']}</td></tr></table>";
    }
    ?>
    <br/>
    <script>__calcula();</script>
    <?php if ($_GET['acc'] == 'V') { ?>
        <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php } else { ?>
        <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('H0005', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>