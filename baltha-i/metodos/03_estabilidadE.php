<?php
//OK
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/'.__MODULO__.'/_'.basename(__FILE__);

$Gestor = new _03_estabilidadE();
$ROW = $Gestor->ObtieneDatos();
if(!$ROW) die('Registro inexistente');

if($_GET['acc']=='V') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Delfos</title>
<?php $Gestor->Incluir('', 'fav'); ?>
<?php $Gestor->Incluir('estilo', 'css')?>
<?php $Gestor->Incluir('window', 'js'); ?>
<?php $Gestor->Incluir('calendario', 'js'); ?>
<?php $Gestor->Incluir('validaciones', 'js')?>
<?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="xanalizar" value="<?=$_GET['xanalizar']?>" />
<input type="hidden" id="tipo" value="<?=$_GET['tipo']?>" />
<center>
    <?php $Gestor->Incluir('h7', 'hr', 'Determinaci&oacute;n de Estabilidad de la Emulsi&oacute;n') ?>
<?=$Gestor->Encabezado('H0007','e','Determinaci&oacute;n de Estabilidad de la Emulsi&oacute;n')?>
<br>
<table class="radius" style="font-size:12px" width="98%">
<tr><td class="titulo" colspan="3">Datos de la muestra</td></tr>
<tr>
	<td>N&uacute;mero:</td>
	<td><?=$ROW[0]['ref']?></td>
	<td>Ingrediente Activo:</td>
</tr>
<tr>
	<td>Fecha de ingreso:</td>
	<td><?=$ROW[0]['fechaI']?></td>
	<td><input type="text" id="ingrediente" value="<?=$ROW[0]['ingrediente']?>" maxlength="30" disabled/></td>
</tr>
<tr>
	<td>Fecha de an&aacute;lisis:</td>
	<td><input type="text" id="fechaA" class="fecha" readonly onClick="show_calendar(this.id);" value="<?=$ROW[0]['fechaA']?>" <?=$disabled?>></td>
	<td><strong>Tipo de formulaci&oacute;n:</strong></td>
</tr>
<tr>
	<td>Fecha de conclusi&oacute;n del an&aacute;lisis:</td>
	<td><?=$ROW[0]['fechaC']?></td>
	<td><?=$ROW[0]['tipo_form']?><input type="hidden" id="tipo_form" value="<?=$ROW[0]['tipo_form']?>" /></td>
</tr>
<tr>
	<td></td>
	<td><input type="hidden" id="dosis" size="10" maxlength="20" value="<?=$ROW[0]['dosis']?>" <?=$disabled?>></td>
	<td><?php if($_GET['acc']=='V'){ ?><strong>Creado por:</strong> <?=$ROW[0]['analista']?> <?php } ?></td>
</tr>
</table>
<br />
<table class="radius" style="font-size:12px" width="98%">
<tr>
  <td class="titulo" colspan="4">Datos del m&eacute;todo</td>
</tr>
<tr>
	<td>Temperatura del agua del ba&ntilde;o mar&iacute;a (�C):</td>
	<td><input type="text" id="maria" class="monto" onblur="Redondear(this)" value="<?=$ROW[0]['maria']?>" <?=$disabled?>/></td>
	<td>N&uacute;mero de reactivo agua dura 342ppm:</td>
	<td><input type="text" id="numreactivo" maxlength="30" value="<?=$ROW[0]['numreactivo']?>" <?=$disabled?>></td>
</tr>
<tr>
	<td>Fecha de preparaci&oacute;n agua dura 342ppm:</td>
	<td><input type="text" id="fechaP" class="fecha" readonly onClick="show_calendar(this.id);" value="<?=$ROW[0]['fechaP']?>" <?=$disabled?>></td>
	<td>Fecha de comprobaci&oacute;n agua dura 342ppm:</td>
	<td><input type="text" id="fechaD" class="fecha" readonly onClick="show_calendar(this.id);" value="<?=$ROW[0]['fechaD']?>" <?=$disabled?>></td>
</tr>
<tr>
	<td colspan="2"></td>
	<td>Resultado de la comprobaci&oacute;n agua dura 342ppm:</td>
	<td><input type="text" id="resultado" class="monto" onblur="Redondear(this)" value="<?=$ROW[0]['resultado']?>" <?=$disabled?>/></td>
</tr>
</table>
<br />
<table class="radius" style="font-size:12px" width="98%">
<tr><td class="titulo" colspan="5">Datos del an&aacute;lisis</td></tr>
<tr>
	<td align="center">Cantidad de muestra (mL):</td>
	<td><input type="text" id="cantidad" class="monto" onblur="Redondear(this)" value="<?=$ROW[0]['cantidad']?>" <?=$disabled?>/></td>
	<td>&nbsp;</td>
	<td colspan="2"><?php if($_GET['acc']=='I'){ ?><input type="button" id="btn" value="Duplicar muestras" class="boton" onClick="duplicar()" title="Utilizado para trabajar con una sola muestra"><?php } ?></td>
</tr>
<tr><td colspan="5"><hr /></td></tr>
<tr>
	<td align="center" colspan="5"><strong>Muestra 1</strong></td>
</tr>
<tr>
	<td align="center"><strong>Tiempo</strong></td>
	<td align="center"><strong>Cremado (mL)</strong></td>
	<td align="center"><strong>Aceite (mL)</strong></td>
	<td align="center"><strong>Clarificaci&oacute;n del agua</strong></td>
	<td align="center"><strong>Espontaneidad</strong></td>
</tr>
<tr>
	<td align="center">0,5 min</td>
	<td align="center"><input type="text" id="cremado1" class="monto" onblur="Redondear(this)" value="<?=$ROW[0]['cremado1']?>" <?=$disabled?>/></td>
	<td align="center"><input type="text" id="aceite1" class="monto" onblur="Redondear(this)" value="<?=$ROW[0]['aceite1']?>" <?=$disabled?>/></td>
	<td align="center"><select id="clari1" <?=$disabled?>>
		<option value="">...</option>
		<option <?php if($ROW[0]['clari1']=='1') echo 'selected';?> value="1">S&iacute;</option>
		<option <?php if($ROW[0]['clari1']=='0') echo 'selected';?> value="0">No</option>
	</select></td>
	<td align="center"><select id="espon1" <?=$disabled?>>
		<option value="">...</option>
		<option <?php if($ROW[0]['espon1']=='1') echo 'selected';?> value="1">S&iacute;</option>
		<option <?php if($ROW[0]['espon1']=='0') echo 'selected';?> value="0">No</option>
	</select></td>
</tr>
<tr>
	<td align="center">30 min</td>
	<td align="center"><input type="text" id="cremado2" class="monto" onblur="Redondear(this)" value="<?=$ROW[0]['cremado2']?>" <?=$disabled?>/></td>
	<td align="center"><input type="text" id="aceite2" class="monto" onblur="Redondear(this)" value="<?=$ROW[0]['aceite2']?>" <?=$disabled?>/></td>
	<td colspan="2"></td>
</tr>
<tr>
	<td align="center">120 min</td>
	<td align="center"><input type="text" id="cremado3" class="monto" onblur="Redondear(this)" value="<?=$ROW[0]['cremado3']?>" <?=$disabled?>/></td>
	<td align="center"><input type="text" id="aceite3" class="monto" onblur="Redondear(this)" value="<?=$ROW[0]['aceite3']?>" <?=$disabled?>/></td>
	<td colspan="2"></td>
</tr>
<tr>
	<td align="center">24,50 horas</td>
	<td align="center"><input type="text" id="cremado4" class="monto" onblur="Redondear(this)" value="<?=$ROW[0]['cremado4']?>" <?=$disabled?>/></td>
	<td align="center"><input type="text" id="aceite4" class="monto" onblur="Redondear(this)" value="<?=$ROW[0]['aceite4']?>" <?=$disabled?>/></td>
	<td colspan="2"></td>
</tr>
<tr><td colspan="5"><hr /></td></tr>
<tr>
	<td align="center" colspan="5"><strong>Muestra 2</strong></td>
</tr>
<tr>
	<td align="center"><strong>Tiempo</strong></td>
	<td align="center"><strong>Cremado (mL)</strong></td>
	<td align="center"><strong>Aceite (mL)</strong></td>
	<td align="center"><strong>Clarificaci&oacute;n del agua</strong></td>
	<td align="center"><strong>Espontaneidad</strong></td>
</tr>
<tr>
	<td align="center">0,5 min</td>
	<td align="center"><input type="text" id="cremado5" class="monto" onblur="Redondear(this)" value="<?=$ROW[0]['cremado5']?>" <?=$disabled?>/></td>
	<td align="center"><input type="text" id="aceite5" class="monto" onblur="Redondear(this)" value="<?=$ROW[0]['aceite5']?>" <?=$disabled?>/></td>
	<td align="center"><select id="clari2" <?=$disabled?>>
		<option value="">...</option>
		<option <?php if($ROW[0]['clari2']=='1') echo 'selected';?> value="1">S&iacute;</option>
		<option <?php if($ROW[0]['clari2']=='0') echo 'selected';?> value="0">No</option>
	</select></td>
	<td align="center"><select id="espon2" <?=$disabled?>>
		<option value="">...</option>
		<option <?php if($ROW[0]['espon2']=='1') echo 'selected';?> value="1">S&iacute;</option>
		<option <?php if($ROW[0]['espon2']=='0') echo 'selected';?> value="0">No</option>
	</select></td>
</tr>
<tr>
	<td align="center">30 min</td>
	<td align="center"><input type="text" id="cremado6" class="monto" onblur="Redondear(this)" value="<?=$ROW[0]['cremado6']?>" <?=$disabled?>/></td>
	<td align="center"><input type="text" id="aceite6" class="monto" onblur="Redondear(this)" value="<?=$ROW[0]['aceite6']?>" <?=$disabled?>/></td>
	<td colspan="2"></td>
</tr>
<tr>
	<td align="center">120 min</td>
	<td align="center"><input type="text" id="cremado7" class="monto" onblur="Redondear(this)" value="<?=$ROW[0]['cremado7']?>" <?=$disabled?>/></td>
	<td align="center"><input type="text" id="aceite7" class="monto" onblur="Redondear(this)" value="<?=$ROW[0]['aceite7']?>" <?=$disabled?>/></td>
	<td colspan="2"></td>
</tr>
<tr>
	<td align="center">24,50 horas</td>
	<td align="center"><input type="text" id="cremado8" class="monto" onblur="Redondear(this)" value="<?=$ROW[0]['cremado8']?>" <?=$disabled?>/></td>
	<td align="center"><input type="text" id="aceite8" class="monto" onblur="Redondear(this)" value="<?=$ROW[0]['aceite8']?>" <?=$disabled?>/></td>
	<td colspan="2"></td>
</tr>
<tr><td class="titulo" colspan="5">Promedios</td></tr>
<tr>
	<td align="center"><strong>0,5 min</strong></td>
	<td align="center" id="promA1"></td>
	<td align="center" id="promB1"></td>
	<td colspan="2"></td>
</tr>
<tr>
	<td align="center"><strong>30 min</strong></td>
	<td align="center" id="promA2"></td>
	<td align="center" id="promB2"></td>
	<td colspan="2"></td>
</tr>
<tr>
	<td align="center"><strong>120 min</strong></td>
	<td align="center" id="promA3"></td>
	<td align="center" id="promB3"></td>
	<td colspan="2"></td>
</tr>
<tr>
	<td align="center"><strong>24,50 horas</strong></td>
	<td align="center" id="promA4"></td>
	<td align="center" id="promB4"></td>
	<td colspan="2"></td>
</tr>
<tr><td colspan="5"><hr /></td></tr>
<tr><td colspan="5">
	<font size="-1"><strong>&Aacute;mbito de aplicaci&oacute;n de prueba: Para productos con dosis de aplicaci&oacute;n mayor o igual al 5%</strong></font>    
	<br /><br /><strong>Tolerancias:</strong><br />
	En lo que se refiere a cremado en reposo en el desarrollo del an&aacute;lisis, los l&iacute;mites son:<br />
	&bull; A los 0 minutos Emulsificaci&oacute;n completa y espont&aacute;nea<br>
	&bull; A los 30 minutos	2,0 mL de cremado m&aacute;ximo<br>		
	&bull; A las 2 horas 4,0 mL de cremado m&aacute;ximo, aceite libre: 0,5 ml<br>		
	&bull; A las 24 horas Reemulsificaci&oacute;n completa (S&oacute;lo si hay duda de la lectura a 30 min. y a las 2 horas)<br>		
	&bull; A las 24,5 horas	4,0 mL de cremado m&aacute;ximo y 2 ml de aceite como l&iacute;mite m&aacute;ximo<br>
	Durante el proceso de an&aacute;lisis (24,5 horas), no debe presentarse en ning&uacute;n momento separaci&oacute;n de agua.
</td>
</tr>
</table>
<br />
<table class="radius" style="font-size:12px" width="98%">
<tr><td class="titulo">Observaciones</td></tr>
<tr>
	<td><textarea id="obs" style="width:98%" <?=$disabled?>><?=$ROW[0]['obs']?></textarea></td>
</tr>
 <tr><td class="titulo" colspan="6">Formulas</td></tr>
         <tr><td colspan="6">
                 <p>Resultado de comprobaci&oacute;n de agua dura 342 ppm = Concentraci&oacute;n del agua dura despu&eacute;s de la comprobaci&oacute;n.</p>
<p>Cantidad de muestra = Cantidad de muestra que se mide y agrega a la probeta.</p>
<p>Cremado = Cantidad de fase medida en el fondo de la probeta, en el tiempo correspondiente.</p>
<p>Aceite = Cantidad de fase medida en el tope de la probeta, en el tiempo correspondiente.</p>
<p>Espontaneidad = En caso de que se forme cremado y/o aceite al momento de agregar el agua dura a la probeta con la muestra, se indica SI.</p>


     </td>
</tr>
</table>
<br />
<br />
<?php if($_GET['acc']=='V'){ ?>
	<script>__calcula();</script>
	<input type="button" value="Imprimir" class="boton" onClick="window.print()">
<?php }else{ ?>
<input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
<?php } ?>
</center>
<?=$Gestor->Encabezado('H0007','p','')?>
<?=$Gestor->Footer(2)?>
</body>
</html>