<?php
//OK
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_estabilidadD();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

if ($_GET['acc'] == 'V') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="xanalizar" value="<?= $_GET['xanalizar'] ?>"/>
<input type="hidden" id="tipo" value="<?= $_GET['tipo'] ?>"/>
<center>
    <?php $Gestor->Incluir('h6', 'hr', 'An&aacute;lisis :: Determinaci&oacute;n de Estabilidad de la Diluci&oacute;n') ?>
    <?= $Gestor->Encabezado('H0006', 'e', 'Determinaci&oacute;n de Estabilidad de la Diluci&oacute;n') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="3">Datos de la muestra</td>
        </tr>
        <tr>
            <td>N&uacute;mero:</td>
            <td><?= $ROW[0]['ref'] ?></td>
            <td>Ingrediente Activo:</td>
        </tr>
        <tr>
            <td>Fecha de ingreso:</td>
            <td><?= $ROW[0]['fechaI'] ?></td>
            <td><input type="text" id="ingrediente" value="<?= $ROW[0]['ingrediente'] ?>" maxlength="30" disabled/></td>
        </tr>
        <tr>
            <td>Fecha de an&aacute;lisis:</td>
            <td><input type="text" id="fechaA" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaA'] ?>" <?= $disabled ?>></td>
            <td><strong>Tipo de formulaci&oacute;n:</strong></td>
        </tr>
        <tr>
            <td>Fecha de conclusi&oacute;n del an&aacute;lisis:</td>
            <td><?= $ROW[0]['fechaC'] ?></td>
            <td><?= $ROW[0]['tipo_form'] ?><input type="hidden" id="tipo_form" value="<?= $ROW[0]['tipo_form'] ?>"/>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="hidden" id="dosis" size="10" maxlength="20" value="<?= $ROW[0]['dosis'] ?>"
                       <?= $disabled ?>></td>
            <td><?php if ($_GET['acc'] == 'V') { ?><strong>Creado por:</strong> <?= $ROW[0]['analista'] ?><?php } ?>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Datos del m&eacute;todo</td>
        </tr>
        <tr>
            <td>Temperatura del agua del ba&ntilde;o mar&iacute;a (�C):</td>
            <td><input type="text" id="maria" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['maria'] ?>"
                       <?= $disabled ?>/></td>
            <td>N&uacute;mero de reactivo agua dura 342ppm:</td>
            <td><input type="text" id="numreactivo" maxlength="30" value="<?= $ROW[0]['numreactivo'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td>Fecha de preparaci&oacute;n agua dura 342ppm:</td>
            <td><input type="text" id="fechaP" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaP'] ?>" <?= $disabled ?>></td>
            <td>Fecha de comprobaci&oacute;n agua dura 342ppm:</td>
            <td><input type="text" id="fechaD" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaD'] ?>" <?= $disabled ?>></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td>Resultado de la comprobaci&oacute;n agua dura 342ppm:</td>
            <td><input type="text" id="resultado" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['resultado'] ?>" <?= $disabled ?>/></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Datos del an&aacute;lisis</td>
        </tr>
        <tr>
            <td>Cantidad de muestra (mL):</td>
            <td><input type="text" id="cantidad" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['cantidad'] ?>" <?= $disabled ?>/></td>
            <td>Volumen de sedimento (mL):</td>
            <td><input type="text" id="volumen" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['volumen'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td>Tiempo (horas):</td>
            <td><input type="text" id="tiempo" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['tiempo'] ?>"
                       <?= $disabled ?>/></td>
            <td>Volumen de aceite (mL):</td>
            <td><input type="text" id="aceite" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['aceite'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td colspan="4" align="center"><br/><strong>Nota:</strong> No debe aparecer ning&uacute;n aceite, sedimento
                o part&iacute;culas visibles, en la parte superior o en el fondo de la diluci&oacute;n al final de la
                lectura 18 horas despu&eacute;s.
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo">Observaciones</td>
        </tr>
        <tr>
            <td><textarea id="obs" style="width:98%" <?= $disabled ?>><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
        <tr>
            <td class="titulo" colspan="6">Formulas</td>
        </tr>
        <tr>
            <td colspan="6">
                <p>Resultado de comprobaci&oacute;n de agua dura 342 ppm = Concentraci&oacute;n del agua dura despu&eacute;s
                    de la comprobaci&oacute;n.</p>
                <p>Cantidad de muestra = Cantidad de muestra que se mide y agrega a la probeta (nominal 5 mL).</p>
                <p>Tiempo = Cantidad de tiempo que la disoluci&oacute;n se deja en reposo en el ba&#241o
                    mar&iacute;a.</p>
                <p>Volumen de sedimento = Volumen de sedimento medido en el fondo de la probeta, luego de las 18
                    horas.</p>
                <p>Volumen de aceite = Volumen de aceite medido en el tope de la probeta, luego de las 18 horas.</p>

            </td>
        </tr>
    </table>
    <br/>
    <br/>
    <?php if ($_GET['acc'] == 'V') { ?>
        <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php } else { ?>
        <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('H0006', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>