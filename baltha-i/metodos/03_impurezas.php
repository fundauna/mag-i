<?php
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_impurezas();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW)
    die('Registro inexistente');

if ($_GET['acc'] == 'V')
    $disabled = 'disabled';
else
    $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('graficador', 'gfx'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="xanalizar" value="<?= $_GET['xanalizar'] ?>"/>
<input type="hidden" id="tipo" value="<?= $_GET['tipo'] ?>"/>
<center>
    <?php $Gestor->Incluir('h22', 'hr', 'An&aacute;lisis :: Reporte de Resultados de Impurezas en Plaguicidas por Cromatograf&iacute;a Con Curva de Calibraci&oacute;�n') ?>
    <?= $Gestor->Encabezado('H0022', 'e', 'Reporte de Resultados de Impurezas en Plaguicidas por Cromatograf&iacute;a Con Curva de Calibraci&oacute;�n') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="3">Datos de la muestra</td>
        </tr>
        <tr>
            <td><strong>N&uacute;mero:</strong></td>
            <td><?= $ROW[0]['ref'] ?></td>
            <td><strong>Ingrediente Activo:</strong></td>
        </tr>
        <tr>
            <td><strong>Fecha de ingreso:</strong></td>
            <td><?= $ROW[0]['fechaI'] ?></td>
            <td><?= $ROW[0]['ingrediente'] ?><input type="hidden" id="ingrediente" maxlength="30"
                                                    value="<?= $ROW[0]['ingrediente'] ?>" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>Fecha de an&aacute;lisis:</strong></td>
            <td><input type="text" id="fechaA" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaA'] ?>" <?= $disabled ?>></td>
            <td><strong>Impureza:</strong></td>
        </tr>
        <tr>
            <td><strong>Fecha de conclusi&oacute;n del an&aacute;lisis:</strong></td>
            <td><?= $ROW[0]['fechaC'] ?></td>
            <td>
                <select id="impureza" disabled>
                    <option value="">...</option>
                    <?php
                    $ROW2 = $Gestor->SubAnalisisGet();
                    for ($x = 0; $x < count($ROW2); $x++) {
                        ?>
                        <option value="<?= $ROW2[$x]['id'] ?>"
                                <?= $ROW[0]['impureza'] == $ROW2[$x]['id'] ? 'selected' : '' ?>><?= $ROW2[$x]['nombre'] ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td><strong>Concentraci&oacute;n declarada ing activo:</strong></td>
            <td><input type="text" id="rango" maxlength="20" class="monto" value="<?= $ROW[0]['rango'] ?>" disabled>&nbsp;
                <select id="unidad" <?php if ($ROW[0]['unidad'] != '' or $_GET['acc'] == 'V') echo 'disabled'; ?>
                        onchange="__calcula()">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['unidad'] == '0') echo 'selected'; ?>>%m/m</option>
                    <option value="1" <?php if ($ROW[0]['unidad'] == '1') echo 'selected'; ?>>%m/v</option>
                </select>
            </td>
            <td><?php if ($_GET['acc'] == 'V') { ?><strong>Creado por:</strong> <?= $ROW[0]['analista'] ?><?php } ?>
            </td>
        </tr>
        <tr>
            <td><strong>Densidad de la muestra, &rho; (g/mL):</strong></td>
            <td colspan="2"><input type="text" id="densidad" class="monto" value="<?= $ROW[0]['densidad'] ?>"
                                   <?= $disabled ?> onblur="Redondear(this)"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Datos del m&eacute;todo</td>
        </tr>
        <tr>
            <td><strong>Referencia del m&eacute;todo:</strong></td>
            <td><input type="text" id="metodo" maxlength="50" value="<?= $ROW[0]['metodo'] ?>" <?= $disabled ?>/></td>
            <td><strong>Disolvente de la muestra:</strong></td>
            <td><input type="text" id="disolvente1" maxlength="50" size="40" value="<?= $ROW[0]['disolvente1'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>M&eacute;todo validado:</strong></td>
            <td><select id="validado" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['validado'] == '0') echo 'selected'; ?>>No</option>
                    <option value="1" <?php if ($ROW[0]['validado'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select></td>
            <td><strong>Disolvente del Est&aacute;ndar de Impureza:</strong></td>
            <td><input type="text" id="disolvente2" maxlength="50" size="40" value="<?= $ROW[0]['disolvente2'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>Tipo de Cromatograf&iacute;a:</strong></td>
            <td><select id="croma" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['croma'] == '0') echo 'selected'; ?>>HPLC</option>
                    <option value="1" <?php if ($ROW[0]['croma'] == '1') echo 'selected'; ?>>Gases</option>
                    <option value="2" <?php if ($ROW[0]['croma'] == '2') echo 'selected'; ?>>Iones</option>
                    <option value="3" <?php if ($ROW[0]['croma'] == '3') echo 'selected'; ?>>HPLC-Masas</option>
                    <option value="4" <?php if ($ROW[0]['croma'] == '4') echo 'selected'; ?>>Gases-Masas</option>
                </select></td>
            <td><strong>Disolvente del Est&aacute;ndar Interno:</strong></td>
            <td><input type="text" id="disolvente3" maxlength="50" size="40" value="<?= $ROW[0]['disolvente3'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
            <td><strong>Nombre del est&aacute;ndar de impureza:</strong></td>
            <td><input type="text" id="estandar" maxlength="50" value="<?= $ROW[0]['estandar'] ?>" <?= $disabled ?>/>
            </td>
        </tr>
        <tr>
            <td><strong>Tiempo retenci&oacute;n muestra (min):</strong></td>
            <td><input type="text" id="tiempo1" class="monto" value="<?= $ROW[0]['tiempo1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><strong>Origen y c&oacute;digo del est&aacute;ndar de impurezas:</strong></td>
            <td><input type="text" id="origen1" maxlength="50" value="<?= $ROW[0]['origen1'] ?>" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>Tiempo retenci&oacute;n Est&aacute;ndar Impurezas (min):</strong></td>
            <td><input type="text" id="tiempo2" class="monto" value="<?= $ROW[0]['tiempo2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><strong>Est&aacute;ndar de impureza tiene certificado?:</strong></td>
            <td><select id="certificado1" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['certificado1'] == '0') echo 'selected'; ?>>No</option>
                    <option value="1" <?php if ($ROW[0]['certificado1'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td><strong>Tiempo retenci&oacute;n est&aacute;ndar interno (min):</strong></td>
            <td><input type="text" id="tiempo3" class="monto" value="<?= $ROW[0]['tiempo3'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><strong>Pureza del est&aacute;ndar de impureza, muestra 1 (%):</strong></td>
            <td><input type="text" id="pureza1" class="monto" value="<?= $ROW[0]['pureza1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
            <td><strong>Utiliza Est&aacute;ndar Interno:</strong></td>
            <td><select name="certificado2" id="certificado2" <?= $disabled ?> onchange="__calcula()">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['certificado2'] == '0') echo 'selected'; ?>>No</option>
                    <option value="1" <?php if ($ROW[0]['certificado2'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
            <td><strong>Nombre del est&aacute;ndar interno:</strong></td>
            <td><input type="text" id="interno2" maxlength="50" value="<?= $ROW[0]['interno2'] ?>" <?= $disabled ?>/>
            </td>
        </tr>
        <tr>
            <td><strong>Volumen de inyecci&oacute;n (&micro;L):</strong></td>
            <td><input type="text" id="inyeccion" class="monto" value="<?= $ROW[0]['inyeccion'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><strong>Origen y c&oacute;digo del est&aacute;ndar interno:</strong></td>
            <td><input type="text" id="origen2" maxlength="50" value="<?= $ROW[0]['origen2'] ?>" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>Volumen del loop (&micro;L) * (Cromatograf&iacute;a L&iacute;quida):</strong></td>
            <td><input type="text" id="loop" class="monto" value="<?= $ROW[0]['loop'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><strong>Est&aacute;ndar interno tiene certificado de pureza:</strong></td>
            <td><select name="certificado3" id="certificado3" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['certificado3'] == '0') echo 'selected'; ?>>No</option>
                    <option value="1" <?php if ($ROW[0]['certificado3'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td><strong>Pureza del est&aacute;ndar interno (%):</strong></td>
            <td><input type="text" id="pureza2" class="monto" value="<?= $ROW[0]['pureza2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"/></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%" align="center" cellpadding="4" cellspacing="4">
        <tr>
            <td class="titulo" colspan="3">Condiciones del equipo</td>
        </tr>
        <tr>
            <td><strong>C&oacute;digo de equipo:</strong>&nbsp;&nbsp;<input type="text" id="tmp_equipo1" class="lista"
                                                                            value="<?= $ROW[0]['tmp_equipo1'] ?>"
                                                                            readonly onclick="EquiposLista(1)"
                                                                            <?= $disabled ?>/><input type="hidden"
                                                                                                     id="equipo"
                                                                                                     value="<?= $ROW[0]['equipo'] ?>"/>
            </td>
            <td><strong>Marca y Modelo de equipo:</strong></td>
            <td id="nommarmo"><?= $ROW[0]['marca'] ?> / <?= $ROW[0]['modelo'] ?></td>
        </tr>
        <tr>
            <td colspan="4">
                <hr>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="98%">
                    <tr>
                        <td colspan="3" style="background-color: #B7B7B7;color: #000;text-align: center;"><b>Columna</b>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Tipo:</strong></td>
                        <td>
                            <select id="tipo_c" <?= $disabled ?>>
                                <option value="">...</option>
                                <option value="0" <?php if ($ROW[0]['tipo_c'] == '0') echo 'selected'; ?>>Capilar
                                </option>
                                <option value="1" <?php if ($ROW[0]['tipo_c'] == '1') echo 'selected'; ?>>Empacado
                                </option>
                                <option value="2" <?php if ($ROW[0]['tipo_c'] == '2') echo 'selected'; ?>>HPLC</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>C&oacute;digo:</strong></td>
                        <td><input type="text" id="codigo_c" maxlength="20" value="<?= $ROW[0]['codigo_c'] ?>"
                                   <?= $disabled ?> class="lista" onclick="ColumnasLista()"/></td>
                    </tr>
                    <tr>
                        <td><strong>Marca y fase:</strong></td>
                        <td><input type="text" id="marca_c" maxlength="50" value="<?= $ROW[0]['marca_c'] ?>"
                                   <?= $disabled ?>/></td>
                    </tr>
                    <tr>
                        <td><strong>Temperatura de trabajo (�C):</strong></td>
                        <td><input type="text" id="temp_c" value="<?= $ROW[0]['temp_c'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td><strong>Dimensiones:</strong></td>
                        <td><input type="text" id="dim_c" maxlength="50" value="<?= $ROW[0]['dim_c'] ?>"
                                   <?= $disabled ?>/></td>
                    </tr>
                    <tbody id="cromaGas">
                    <tr>
                        <td colspan="3" style="background-color: #B7B7B7;color: #000;text-align: center;">Inyector
                            (Cromatograf&iacute;a Gaseosa)
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Tipo:</strong></td>
                        <td><select id="tipo_i" <?= $disabled ?>>
                                <option value="">...</option>
                                <option value="">N/A</option>
                                <option value="0" <?php if ($ROW[0]['tipo_i'] == '0') echo 'selected'; ?>>COC</option>
                                <option value="1" <?php if ($ROW[0]['tipo_i'] == '1') echo 'selected'; ?>>Capilar
                                </option>
                                <option value="2" <?php if ($ROW[0]['tipo_i'] == '2') echo 'selected'; ?>>Empacado
                                </option>
                                <option value="3" <?php if ($ROW[0]['tipo_i'] == '3') echo 'selected'; ?>>PTV</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td><strong>Temperatura (�C):</strong></td>
                        <td><input type="text" id="temp_i" class="monto" value="<?= $ROW[0]['temp_i'] ?>"
                                   <?= $disabled ?> onblur="Redondear(this)"></td>
                    </tr>
                    <tr>
                        <td><strong>Modo de inyecci&oacute;n:</strong></td>
                        <td><select id="modo_i" <?= $disabled ?>>
                                <option value="">...</option>
                                <option value="">N/A</option>
                                <option value="0" <?php if ($ROW[0]['modo_i'] == '0') echo 'selected'; ?>>Split</option>
                                <option value="1" <?php if ($ROW[0]['modo_i'] == '1') echo 'selected'; ?>>Splitless
                                </option>
                            </select></td>
                    </tr>
                    <tr>
                        <td><strong>Relaci&oacute;n split:</strong></td>
                        <td><input type="text" id="condicion_i" value="<?= $ROW[0]['condicion_i'] ?>" size="15"
                                   maxlength="20" <?= $disabled ?>/></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="background-color: #B7B7B7;color: #000;text-align: center;">Fase m&oacute;vil
                            (Cromatograf&iacute;a Gaseosa)
                        </td>
                    </tr>
                    <tr align="center">
                        <td><strong>Gas de arrastre</strong></td>
                        <td><strong>Flujo (mL/min)</strong></td>
                        <td><strong>Presi&oacute;n (psi)</strong></td>
                    </tr>
                    <tr align="center">
                        <td><select id="gas1" <?= $disabled ?>>
                                <option value="">...</option>
                                <option value="">N/A</option>
                                <option value="0" <?php if ($ROW[0]['gas1'] == '0') echo 'selected'; ?>>
                                    Hidr&oacute;geno
                                </option>
                                <option value="1" <?php if ($ROW[0]['gas1'] == '1') echo 'selected'; ?>>Helio</option>
                                <option value="2" <?php if ($ROW[0]['gas1'] == '2') echo 'selected'; ?>>
                                    Nitr&oacute;geno
                                </option>
                            </select></td>
                        <td><input type="text" id="flujo1" class="monto" value="<?= $ROW[0]['flujo1'] ?>"
                                   <?= $disabled ?> onblur="Redondear(this)"></td>
                        <td><input type="text" id="presion1" class="monto" value="<?= $ROW[0]['presion1'] ?>"
                                   <?= $disabled ?> onblur="Redondear(this)"></td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td colspan="2">
                <table width="98%">
                    <tbody id="cromaIon">
                    <tr>
                        <td colspan="3" style="background-color: #B7B7B7;color: #000;text-align: center;">Detector</td>
                    </tr>
                    <tr>
                        <td><strong>Tipo:</strong></td>
                        <td><select id="tipo_d" <?= $disabled ?> onchange="escondeDetector(this.value);">
                                <option value="">...</option>
                                <option value="0" <?php if ($ROW[0]['tipo_d'] == '0') echo 'selected'; ?>>Arreglo de
                                    Diodos
                                </option>
                                <option value="1" <?php if ($ROW[0]['tipo_d'] == '1') echo 'selected'; ?>>
                                    Conductividad
                                </option>
                                <option value="2" <?php if ($ROW[0]['tipo_d'] == '2') echo 'selected'; ?>>FID</option>
                                <option value="3" <?php if ($ROW[0]['tipo_d'] == '3') echo 'selected'; ?>>Masas</option>
                                <option value="4" <?php if ($ROW[0]['tipo_d'] == '4') echo 'selected'; ?>>Masas -
                                    Masas
                                </option>
                            </select></td>
                    </tr>
                    <tr id="tempTrab">
                        <td><strong>Temperatura de trabajo (�C):</strong></td>
                        <td><input type="text" id="temp_d" class="monto" value="<?= $ROW[0]['temp_d'] ?>"
                                   <?= $disabled ?> onblur="Redondear(this)"></td>
                    </tr>
                    <tr id="LongOnda">
                        <td><strong>Longitud de onda (nm):</strong></td>
                        <td><input type="text" id="long_d" class="monto" value="<?= $ROW[0]['long_d'] ?>"
                                   <?= $disabled ?> onblur="Redondear(this)"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr/>
                        </td>
                    </tr>
                    <tr id="condicionesFID">
                        <td align="center"><strong>Gases del FID:</strong></td>
                        <td align="center"><strong>Flujo (mL/min)</strong></td>
                    </tr>
                    <tr align="center" id="condicionesFID1">
                        <td><select id="fid" disabled>
                                <option value="0" selected>Hidr&oacute;geno</option>
                            </select></td>
                        <td><input type="text" id="flujofid1" class="monto" value="<?= $ROW[0]['flujofid1'] ?>"
                                   <?= $disabled ?> onblur="Redondear(this)"></td>
                    </tr>
                    <tr align="center" id="condicionesFID2">
                        <td><select id="fid1" disabled>
                                <option value="1" selected>Aire</option>
                            </select></td>
                        <td><input type="text" id="flujofid2" class="monto" value="<?= $ROW[0]['flujofid2'] ?>"
                                   <?= $disabled ?> onblur="Redondear(this)"></td>
                    </tr>
                    <tr align="center" id="condicionesFID3">
                        <td><select id="fid2" disabled>
                                <option value="2" selected>Make-Up (Nitr&oacute;geno)</option>
                            </select></td>
                        <td><input type="text" id="flujofid3" class="monto" value="<?= $ROW[0]['flujofid3'] ?>"
                                   <?= $disabled ?> onblur="Redondear(this)"></td>
                    </tr>
                    </tbody>
                    <tbody id="cromaLiq">
                    <tr>
                        <td colspan="3" style="background-color: #B7B7B7;color: #000;text-align: center;">Fase m&oacute;vil
                            (Cromatograf&iacute;a L&iacute;quida)
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Tipo de bomba:</strong></td>
                        <td colspan="2"><select id="bomba_d" <?= $disabled ?>>
                                <option value="">N/A</option>
                                <option value="1" <?php if ($ROW[0]['bomba_d'] == '1') echo 'selected'; ?>>Binaria
                                </option>
                                <option value="2" <?php if ($ROW[0]['bomba_d'] == '2') echo 'selected'; ?>>Cuaternaria
                                </option>
                            </select></td>
                    </tr>
                    <tr>
                        <td><strong>Eluente(s):</strong></td>
                        <td colspan="2"><input type="text" id="elu_d" maxlength="50" value="<?= $ROW[0]['elu_d'] ?>"
                                               <?= $disabled ?>/></td>
                    </tr>
                    <tr align="center">
                        <td><strong>Canal</strong></td>
                        <td><strong>Flujo (mL/min)</strong></td>
                        <td><strong>Presi&oacute;n (psi)</strong></td>
                    </tr>
                    <tr align="center">
                        <td><select id="canal_crom1" <?= $disabled ?>>
                                <option value="">...</option>
                                <option value="">N/A</option>
                                <option value="0" <?php if ($ROW[0]['canal_crom1'] == '0') echo 'selected'; ?>>A
                                </option>
                                <option value="1" <?php if ($ROW[0]['canal_crom1'] == '1') echo 'selected'; ?>>B
                                </option>
                                <option value="2" <?php if ($ROW[0]['canal_crom1'] == '2') echo 'selected'; ?>>C
                                </option>
                                <option value="3" <?php if ($ROW[0]['canal_crom1'] == '3') echo 'selected'; ?>>D
                                </option>
                            </select></td>
                        <td><input type="text" id="flujo_crom1" class="monto" value="<?= $ROW[0]['flujo_crom1'] ?>"
                                   <?= $disabled ?> onblur="Redondear(this)"></td>
                        <td><input type="text" id="presion_crom1" class="monto" value="<?= $ROW[0]['presion_crom1'] ?>"
                                   <?= $disabled ?> onblur="Redondear(this)"></td>
                    </tr>
                    <tr align="center">
                        <td><select id="canal_crom2" <?= $disabled ?>>
                                <option value="">...</option>
                                <option value="">N/A</option>
                                <option value="0" <?php if ($ROW[0]['canal_crom2'] == '0') echo 'selected'; ?>>A
                                </option>
                                <option value="1" <?php if ($ROW[0]['canal_crom2'] == '1') echo 'selected'; ?>>B
                                </option>
                                <option value="2" <?php if ($ROW[0]['canal_crom2'] == '2') echo 'selected'; ?>>C
                                </option>
                                <option value="3" <?php if ($ROW[0]['canal_crom2'] == '3') echo 'selected'; ?>>D
                                </option>
                            </select></td>
                        <td><input type="text" id="flujo_crom2" class="monto" value="<?= $ROW[0]['flujo_crom2'] ?>"
                                   <?= $disabled ?> onblur="Redondear(this)"></td>
                        <td><input type="text" id="presion_crom2" class="monto" value="<?= $ROW[0]['presion_crom2'] ?>"
                                   <?= $disabled ?> onblur="Redondear(this)"></td>
                    </tr>
                    <tr align="center">
                        <td><select id="canal_crom3" <?= $disabled ?>>
                                <option value="">...</option>
                                <option value="">N/A</option>
                                <option value="0" <?php if ($ROW[0]['canal_crom3'] == '0') echo 'selected'; ?>>A
                                </option>
                                <option value="1" <?php if ($ROW[0]['canal_crom3'] == '1') echo 'selected'; ?>>B
                                </option>
                                <option value="2" <?php if ($ROW[0]['canal_crom3'] == '2') echo 'selected'; ?>>C
                                </option>
                                <option value="3" <?php if ($ROW[0]['canal_crom3'] == '3') echo 'selected'; ?>>D
                                </option>
                            </select></td>
                        <td><input type="text" id="flujo_crom3" class="monto" value="<?= $ROW[0]['flujo_crom3'] ?>"
                                   <?= $disabled ?> onblur="Redondear(this)"></td>
                        <td><input type="text" id="presion_crom3" class="monto" value="<?= $ROW[0]['presion_crom3'] ?>"
                                   <?= $disabled ?> onblur="Redondear(this)"></td>
                    </tr>
                    <tr align="center">
                        <td><select id="canal_crom4" <?= $disabled ?>>
                                <option value="">...</option>
                                <option value="">N/A</option>
                                <option value="0" <?php if ($ROW[0]['canal_crom4'] == '0') echo 'selected'; ?>>A
                                </option>
                                <option value="1" <?php if ($ROW[0]['canal_crom4'] == '1') echo 'selected'; ?>>B
                                </option>
                                <option value="2" <?php if ($ROW[0]['canal_crom4'] == '2') echo 'selected'; ?>>C
                                </option>
                                <option value="3" <?php if ($ROW[0]['canal_crom4'] == '3') echo 'selected'; ?>>D
                                </option>
                            </select></td>
                        <td><input type="text" id="flujo_crom4" class="monto" value="<?= $ROW[0]['flujo_crom4'] ?>"
                                   <?= $disabled ?> onblur="Redondear(this)"></td>
                        <td><input type="text" id="presion_crom4" class="monto" value="<?= $ROW[0]['presion_crom4'] ?>"
                                   <?= $disabled ?> onblur="Redondear(this)"></td>
                    </tr>
                    </tbody>
                    <tr>
                        <td colspan="3" style="background-color: #B7B7B7;color: #000;text-align: center;">Detector de
                            masas, masas-masas
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Modo de adquisici&oacute;n:</strong></td>
                        <td><select id="modo" <?= $disabled ?>>
                                <option value="">...</option>
                                <option value="0" <?php if ($ROW[0]['modo'] == '0') echo 'selected'; ?>>SIM</option>
                                <option value="1" <?php if ($ROW[0]['modo'] == '1') echo 'selected'; ?>>SCAN</option>
                                <option value="2" <?php if ($ROW[0]['modo'] == '2') echo 'selected'; ?>>MRM</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td><strong>Temperatura de cuadrupolo (&deg;C):</strong></td>
                        <td><input type="text" id="temp1_g" class="monto" value="<?= $ROW[0]['temp1_g'] ?>"
                                   <?= $disabled ?> onblur="Redondear(this)"></td>
                    </tr>
                    <tr>
                        <td><strong>Temperatura de fuente de ionizaci&oacute;n (&deg;C):</strong></td>
                        <td><input type="text" id="temp2_g" class="monto" value="<?= $ROW[0]['temp2_g'] ?>"
                                   <?= $disabled ?> onblur="Redondear(this)"></td>
                    </tr>
                    <tr>
                        <td><strong>Iones monitoreados (+m/z):</strong></td>
                        <td><input type="text" id="temp3_g" value="<?= $ROW[0]['temp3_g'] ?>" <?= $disabled ?>></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="9">Curva de Calibraci&oacute;n</td>
        </tr>
        <tr>
            <td><strong>Dato</strong></td>
            <td><strong>Masa (mg)</strong></td>
            <td><strong>Vol B. Afo. 1 (mL)</strong></td>
            <td><strong>Vol. Al&iacute;cuota 1 (mL)</strong></td>
            <td><strong>Vol B. Afo. 2 (mL)</strong></td>
            <td><strong>Vol. Al&iacute;cuota 2 (mL)</strong></td>
            <td><strong>Vol B. Afo. 3 (mL)</strong></td>
            <td><strong>Conc. (mg/mL)</strong></td>
        </tr>
        <tr>
            <td>Est&aacute;ndar Impureza</td>
            <td><input type="text" id="masa1" class="monto" value="<?= $ROW[0]['masa1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><select id="aforado11" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['aforado11'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">
                        10.00
                    </option>
                    <option <?php if ($ROW[0]['aforado11'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                        IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforado11'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                        IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforado11'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                         IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforado11'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                         IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforado11'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                         IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforado11'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                         IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforado11'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                          IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforado11'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                          IR="0.0003">2000.00
                    </option>
                </select></td>
            <td><input type="text" id="alicuota11" class="monto" value="<?= $ROW[0]['alicuota11'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><select id="aforado12" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['aforado12'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">
                        10.00
                    </option>
                    <option <?php if ($ROW[0]['aforado12'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                        IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforado12'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                        IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforado12'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                         IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforado12'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                         IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforado12'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                         IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforado12'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                         IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforado12'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                          IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforado12'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                          IR="0.0003">2000.00
                    </option>
                </select></td>
            <td><input type="text" id="alicuota12" class="monto" value="<?= $ROW[0]['alicuota12'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><select id="aforado13" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['aforado13'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">
                        10.00
                    </option>
                    <option <?php if ($ROW[0]['aforado13'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                        IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforado13'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                        IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforado13'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                         IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforado13'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                         IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforado13'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                         IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforado13'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                         IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforado13'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                          IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforado13'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                          IR="0.0003">2000.00
                    </option>
                </select></td>
            <td align="center" id="con1"></td>
        </tr>
        <tr>
            <td>Est&aacute;ndar Interno</td>
            <td><input type="text" id="masa2" class="monto" value="<?= $ROW[0]['masa2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><select id="aforado21" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['aforado21'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">
                        10.00
                    </option>
                    <option <?php if ($ROW[0]['aforado21'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                        IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforado21'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                        IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforado21'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                         IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforado21'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                         IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforado21'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                         IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforado21'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                         IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforado21'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                          IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforado21'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                          IR="0.0003">2000.00
                    </option>
                </select></td>
            <td><input type="text" id="alicuota21" class="monto" value="<?= $ROW[0]['alicuota21'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><select id="aforado22" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['aforado22'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">
                        10.00
                    </option>
                    <option <?php if ($ROW[0]['aforado22'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                        IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforado22'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                        IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforado22'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                         IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforado22'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                         IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforado22'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                         IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforado22'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                         IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforado22'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                          IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforado22'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                          IR="0.0003">2000.00
                    </option>
                </select></td>
            <td><input type="text" id="alicuota22" class="monto" value="<?= $ROW[0]['alicuota22'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><select id="aforado23" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['aforado23'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">
                        10.00
                    </option>
                    <option <?php if ($ROW[0]['aforado23'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                        IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforado23'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                        IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforado23'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                         IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforado23'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                         IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforado23'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                         IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforado23'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                         IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforado23'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                          IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforado23'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                          IR="0.0003">2000.00
                    </option>
                </select></td>
            <td align="center" id="con2"></td>
        </tr>
        <tr>
            <td colspan="9">
                <hr/>
            </td>
        </tr>
        <tr>
            <td><strong>Patr&oacute;n</strong></td>
            <td><strong>Vol. de Al&iacute;cuota DM (&micro;L)</strong></td>
            <td><strong>Vol B. Aforado 1 (mL)</strong></td>
            <td><strong>Vol. de Al&iacute;cuota 1 (mL)</strong></td>
            <td><strong>Vol B. Aforado 2 (mL)</strong></td>
            <td><strong>Concentraci&oacute;n (mg/L)</strong></td>
            <td><strong>&Aacute;rea promedio Est&aacute;ndar Impureza</strong></td>
            <td><strong>&Aacute;rea promedio Estandar Interno</strong></td>
            <td><strong>&Aacute;rea Est. impureza
                    <hr/>&Aacute;rea Est. Interno</strong></td>
        </tr>
        <tr>
            <td>1</td>
            <td><input type="text" id="alicuota31" class="monto" value="<?= $ROW[0]['alicuota31'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><select id="aforado31" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['aforado31'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">
                        10.00
                    </option>
                    <option <?php if ($ROW[0]['aforado31'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                        IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforado31'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                        IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforado31'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                         IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforado31'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                         IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforado31'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                         IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforado31'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                         IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforado31'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                          IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforado31'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                          IR="0.0003">2000.00
                    </option>
                </select></td>
            <td><input type="text" id="alicuota32" class="monto" value="<?= $ROW[0]['alicuota32'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><select id="aforado32" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['aforado32'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">
                        10.00
                    </option>
                    <option <?php if ($ROW[0]['aforado32'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                        IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforado32'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                        IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforado32'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                         IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforado32'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                         IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforado32'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                         IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforado32'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                         IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforado32'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                          IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforado32'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                          IR="0.0003">2000.00
                    </option>
                </select></td>
            <td><input type="text" id="concentracion3" class="monto" value="<?= $ROW[0]['concentracion3'] ?>" disabled>
            </td>
            <td><input type="text" id="areaimp3" class="monto" value="<?= $ROW[0]['areaimp3'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaint3" class="monto" value="<?= $ROW[0]['areaint3'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td align="center" id="area3"></td>
        </tr>
        <tr>
            <td>2</td>
            <td><input type="text" id="alicuota41" class="monto" value="<?= $ROW[0]['alicuota41'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><select id="aforado41" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['aforado41'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">
                        10.00
                    </option>
                    <option <?php if ($ROW[0]['aforado41'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                        IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforado41'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                        IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforado41'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                         IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforado41'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                         IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforado41'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                         IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforado41'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                         IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforado41'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                          IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforado41'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                          IR="0.0003">2000.00
                    </option>
                </select></td>
            <td><input type="text" id="alicuota42" class="monto" value="<?= $ROW[0]['alicuota42'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><select id="aforado42" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['aforado42'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">
                        10.00
                    </option>
                    <option <?php if ($ROW[0]['aforado42'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                        IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforado42'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                        IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforado42'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                         IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforado42'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                         IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforado42'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                         IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforado42'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                         IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforado42'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                          IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforado42'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                          IR="0.0003">2000.00
                    </option>
                </select></td>
            <td><input type="text" id="concentracion4" class="monto" value="<?= $ROW[0]['concentracion4'] ?>" disabled>
            </td>
            <td><input type="text" id="areaimp4" class="monto" value="<?= $ROW[0]['areaimp4'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaint4" class="monto" value="<?= $ROW[0]['areaint4'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td align="center" id="area4"></td>
        </tr>
        <tr>
            <td>3</td>
            <td><input type="text" id="alicuota51" class="monto" value="<?= $ROW[0]['alicuota51'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><select id="aforado51" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['aforado51'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">
                        10.00
                    </option>
                    <option <?php if ($ROW[0]['aforado51'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                        IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforado51'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                        IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforado51'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                         IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforado51'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                         IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforado51'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                         IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforado51'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                         IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforado51'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                          IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforado51'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                          IR="0.0003">2000.00
                    </option>
                </select></td>
            <td><input type="text" id="alicuota52" class="monto" value="<?= $ROW[0]['alicuota52'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><select id="aforado52" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['aforado52'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">
                        10.00
                    </option>
                    <option <?php if ($ROW[0]['aforado52'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                        IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforado52'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                        IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforado52'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                         IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforado52'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                         IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforado52'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                         IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforado52'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                         IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforado52'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                          IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforado52'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                          IR="0.0003">2000.00
                    </option>
                </select></td>
            <td><input type="text" id="concentracion5" class="monto" value="<?= $ROW[0]['concentracion5'] ?>" disabled>
            </td>
            <td><input type="text" id="areaimp5" class="monto" value="<?= $ROW[0]['areaimp5'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaint5" class="monto" value="<?= $ROW[0]['areaint5'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td align="center" id="area5"></td>
        </tr>
        <tr>
            <td>4</td>
            <td><input type="text" id="alicuota61" class="monto" value="<?= $ROW[0]['alicuota61'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><select id="aforado61" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['aforado61'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">
                        10.00
                    </option>
                    <option <?php if ($ROW[0]['aforado61'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                        IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforado61'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                        IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforado61'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                         IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforado61'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                         IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforado61'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                         IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforado61'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                         IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforado61'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                          IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforado61'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                          IR="0.0003">2000.00
                    </option>
                </select></td>
            <td><input type="text" id="alicuota62" class="monto" value="<?= $ROW[0]['alicuota62'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><select id="aforado62" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['aforado62'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">
                        10.00
                    </option>
                    <option <?php if ($ROW[0]['aforado62'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                        IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforado62'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                        IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforado62'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                         IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforado62'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                         IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforado62'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                         IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforado62'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                         IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforado62'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                          IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforado62'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                          IR="0.0003">2000.00
                    </option>
                </select></td>
            <td><input type="text" id="concentracion6" class="monto" value="<?= $ROW[0]['concentracion6'] ?>" disabled>
            </td>
            <td><input type="text" id="areaimp6" class="monto" value="<?= $ROW[0]['areaimp6'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaint6" class="monto" value="<?= $ROW[0]['areaint6'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td align="center" id="area6"></td>
        </tr>
        <tr>
            <td colspan="9">
                <hr/>
            </td>
        </tr>
        <tr>
            <td><strong>Pendiente (m):</strong></td>
            <td id="m"></td>
            <td><strong>Intercepto (b):</strong></td>
            <td id="b"></td>
            <td><strong>r:</strong></td>
            <td id="r"></td>
        </tr>
    </table>
    <br/>
    <center><input type="button" value="Graficar" class="boton" onClick="Generar()"></center>
    <br/>
    <table>
        <tr>
            <td></td>
        </tr>
        <tr style="display:none;">
            <td id="td_tabla"></td>
        </tr>
        <tr align="center" id="tr_grafico" style="display:none;">
            <td>
                <div id="container" style="min-width: 98%; height: 400px; margin: 0 auto"></div>
            </td>
        </tr>
    </table>

    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="8">Datos del An&aacute;lisis</td>
        </tr>
        <tr>
            <td><strong>Dato</strong></td>
            <td><strong>Masa (mg)</strong></td>
            <td><strong>Vol. Extracci&oacute;n 1 (mL)</strong></td>
            <td><strong>Vol. de Al&iacute;cuota 1 (mL)</strong></td>
            <td><strong>Vol. Diluci&oacute;n 2 (mL)</strong></td>
            <td><strong>Vol. de Al&iacute;cuota 2 (mL)</strong></td>
            <td><strong>Vol B. Aforado 3 (mL)</strong></td>
            <td><strong>Concentraci&oacute;n (mg/mL)</strong></td>
        </tr>
        <tr>
            <td>Muestra 1</td>
            <td><input type="text" id="muestramasa1" class="monto"
                       value="<?= $ROW[0]['muestramasa1'] == 0 ? '' : $ROW[0]['muestramasa1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="muestraaforado11" class="monto"
                       value="<?= $ROW[0]['muestraaforado11'] == 0 ? '' : $ROW[0]['muestraaforado11'] ?>"
                       <?= $disabled ?> onblur="Redondear(this)"></td>
            <td><input type="text" id="muestraalicuota11" class="monto"
                       value="<?= $ROW[0]['muestraalicuota11'] == 0 ? '' : $ROW[0]['muestraalicuota11'] ?>"
                       <?= $disabled ?> onblur="Redondear(this)"></td>
            <td><input type="text" id="muestraaforado12" class="monto"
                       value="<?= $ROW[0]['muestraaforado12'] == 0 ? '' : $ROW[0]['muestraaforado12'] ?>"
                       <?= $disabled ?> onblur="Redondear(this)"></td>
            <td><input type="text" id="muestraalicuota12" class="monto"
                       value="<?= $ROW[0]['muestraalicuota12'] == 0 ? '' : $ROW[0]['muestraalicuota12'] ?>"
                       <?= $disabled ?> onblur="Redondear(this)"></td>
            <td><input type="text" id="muestraaforado13" class="monto"
                       value="<?= $ROW[0]['muestraaforado13'] == 0 ? '' : $ROW[0]['muestraaforado13'] ?>"
                       <?= $disabled ?> onblur="Redondear(this)"></td>
            <td align="center" id="mcon1"></td>
        </tr>
        <tr>
            <td>Est&aacute;ndar Interno</td>
            <td><input type="text" id="muestramasa2" class="monto"
                       value="<?= $ROW[0]['muestramasa2'] == 0 ? '' : $ROW[0]['muestramasa2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="muestraaforado21" class="monto"
                       value="<?= $ROW[0]['muestraaforado21'] == 0 ? '' : $ROW[0]['muestraaforado21'] ?>"
                       <?= $disabled ?> onblur="Redondear(this)"></td>
            <td><input type="text" id="muestraalicuota21" class="monto"
                       value="<?= $ROW[0]['muestraalicuota21'] == 0 ? '' : $ROW[0]['muestraalicuota21'] ?>"
                       <?= $disabled ?> onblur="Redondear(this)"></td>
            <td><input type="text" id="muestraaforado22" class="monto"
                       value="<?= $ROW[0]['muestraaforado22'] == 0 ? '' : $ROW[0]['muestraaforado22'] ?>"
                       <?= $disabled ?> onblur="Redondear(this)"></td>
            <td><input type="text" id="muestraalicuota22" class="monto"
                       value="<?= $ROW[0]['muestraalicuota22'] == 0 ? '' : $ROW[0]['muestraalicuota22'] ?>"
                       <?= $disabled ?> onblur="Redondear(this)"></td>
            <td><input type="text" id="muestraaforado23" class="monto"
                       value="<?= $ROW[0]['muestraaforado23'] == 0 ? '' : $ROW[0]['muestraaforado23'] ?>"
                       <?= $disabled ?> onblur="Redondear(this)"></td>
            <td align="center" id="mcon2"></td>
        </tr>
        <tr>
            <td colspan="8">
                <hr/>
            </td>
        </tr>
        <tr>
            <td><strong>Dato</strong></td>
            <td><strong>&Aacute;rea 1</strong></td>
            <td><strong>&Aacute;rea 2</strong></td>
            <td><strong>&Aacute;rea promedio</strong></td>
            <td><strong>Desviaci&oacute;n Est&aacute;ndar</strong></td>
            <td><strong>% C.V.</strong></td>
            <td><strong>% C.V. te&oacute;rico muestra</strong></td>
        </tr>
        <tr>
            <td>&Aacute;rea Muestra 1</td>
            <td><input type="text" id="areaA1" class="monto" value="<?= $ROW[0]['areaA1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaB1" class="monto" value="<?= $ROW[0]['areaB1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td align="center" id="promArea1"></td>
            <td align="center" id="desvArea1"></td>
            <td align="center" id="cvArea1"></td>
            <td align="center" id="cvt" rowspan="4"></td>
        </tr>
        <tr>
            <td>&Aacute;rea Est. Interno en la muestra 1</td>
            <td><input type="text" id="areaA2" class="monto" value="<?= $ROW[0]['areaA2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaB2" class="monto" value="<?= $ROW[0]['areaB2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td align="center" id="promArea2"></td>
            <td align="center" id="desvArea2"></td>
            <td align="center" id="cvArea2"></td>
        </tr>
        <tr>
            <td>Concentraci&oacute;n a partir de curva sin Est. Int. (mg/mL)</td>
            <td align="center" id="concurArea1"></td>
            <td align="center" id="concurArea2"></td>
            <td align="center" id="concurProm1"></td>
            <td align="center" id="concurdesv"></td>
            <td align="center" id="concurcv"></td>
        </tr>
        <tr>
            <td>Concentraci&oacute;n a partir de curva con Est. Int. (mg/mL)</td>
            <td align="center" id="concurIArea1"></td>
            <td align="center" id="concurIArea2"></td>
            <td align="center" id="concurIProm1"></td>
            <td align="center" id="concurIdesv"></td>
            <td align="center" id="concurIcv"></td>
        </tr>
        <tr>
            <td colspan="8">
                <hr/>
            </td>
        </tr>
        <tr>
            <td><strong>Dato</strong></td>
            <td><strong>Masa (mg)</strong></td>
            <td><strong>Vol. Extracci&oacute;n 1 (mL)</strong></td>
            <td><strong>Vol. de Al&iacute;cuota 1 (mL)</strong></td>
            <td><strong>Vol. Extracci&oacute;n 2 (mL)</strong></td>
            <td><strong>Vol. de Al&iacute;cuota 2 (mL)</strong></td>
            <td><strong>Vol B. Aforado 3 (mL)</strong></td>
            <td><strong>Concentraci&oacute;n (mg/mL)</strong></td>
        </tr>
        <tr>
            <td>Muestra 2</td>
            <td><input type="text" id="muestramasa3" class="monto"
                       value="<?= $ROW[0]['muestramasa3'] == 0 ? '' : $ROW[0]['muestramasa3'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="muestraaforado31" class="monto"
                       value="<?= $ROW[0]['muestraaforado31'] == 0 ? '' : $ROW[0]['muestraaforado31'] ?>"
                       <?= $disabled ?> onblur="Redondear(this)"></td>
            <td><input type="text" id="muestraalicuota31" class="monto"
                       value="<?= $ROW[0]['muestraalicuota31'] == 0 ? '' : $ROW[0]['muestraalicuota31'] ?>"
                       <?= $disabled ?> onblur="Redondear(this)"></td>
            <td><input type="text" id="muestraaforado32" class="monto"
                       value="<?= $ROW[0]['muestraaforado32'] == 0 ? '' : $ROW[0]['muestraaforado32'] ?>"
                       <?= $disabled ?> onblur="Redondear(this)"></td>
            <td><input type="text" id="muestraalicuota32" class="monto"
                       value="<?= $ROW[0]['muestraalicuota32'] == 0 ? '' : $ROW[0]['muestraalicuota32'] ?>"
                       <?= $disabled ?> onblur="Redondear(this)"></td>
            <td><input type="text" id="muestraaforado33" class="monto"
                       value="<?= $ROW[0]['muestraaforado33'] == 0 ? '' : $ROW[0]['muestraaforado33'] ?>"
                       <?= $disabled ?> onblur="Redondear(this)"></td>
            <td align="center" id="mcon3"></td>
        </tr>
        <tr>
            <td>Est&aacute;ndar Interno</td>
            <td><input type="text" id="muestramasa4" class="monto"
                       value="<?= $ROW[0]['muestramasa4'] == 0 ? '' : $ROW[0]['muestramasa4'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="muestraaforado41" class="monto"
                       value="<?= $ROW[0]['muestraaforado41'] == 0 ? '' : $ROW[0]['muestraaforado41'] ?>"
                       <?= $disabled ?> onblur="Redondear(this)"></td>
            <td><input type="text" id="muestraalicuota41" class="monto"
                       value="<?= $ROW[0]['muestraalicuota41'] == 0 ? '' : $ROW[0]['muestraalicuota41'] ?>"
                       <?= $disabled ?> onblur="Redondear(this)"></td>
            <td><input type="text" id="muestraaforado42" class="monto"
                       value="<?= $ROW[0]['muestraaforado42'] == 0 ? '' : $ROW[0]['muestraaforado42'] ?>"
                       <?= $disabled ?> onblur="Redondear(this)"></td>
            <td><input type="text" id="muestraalicuota42" class="monto"
                       value="<?= $ROW[0]['muestraalicuota42'] == 0 ? '' : $ROW[0]['muestraalicuota42'] ?>"
                       <?= $disabled ?> onblur="Redondear(this)"></td>
            <td><input type="text" id="muestraaforado43" class="monto"
                       value="<?= $ROW[0]['muestraaforado43'] == 0 ? '' : $ROW[0]['muestraaforado43'] ?>"
                       <?= $disabled ?> onblur="Redondear(this)"></td>
            <td align="center" id="mcon4"></td>
        </tr>
        <tr>
            <td colspan="8">
                <hr/>
            </td>
        </tr>
        <tr>
            <td><strong>Dato</strong></td>
            <td><strong>&Aacute;rea 1</strong></td>
            <td><strong>&Aacute;rea 2</strong></td>
            <td><strong>&Aacute;rea promedio</strong></td>
            <td><strong>Desviaci&oacute;n Est&aacute;ndar</strong></td>
            <td><strong>% C.V.</strong></td>
            <td><strong>% C.V. te&oacute;rico muestra</strong></td>
        </tr>
        <tr>
            <td>&Aacute;rea Muestra 2</td>
            <td><input type="text" id="areaA3" class="monto" value="<?= $ROW[0]['areaA3'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaB3" class="monto" value="<?= $ROW[0]['areaB3'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td align="center" id="promArea3"></td>
            <td align="center" id="desvArea3"></td>
            <td align="center" id="cvArea3"></td>
            <td align="center" id="cvt2" rowspan="4"></td>
        </tr>
        <tr>
            <td>&Aacute;rea Est. Interno en la muestra 2</td>
            <td><input type="text" id="areaA4" class="monto" value="<?= $ROW[0]['areaA4'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaB4" class="monto" value="<?= $ROW[0]['areaB4'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td align="center" id="promArea4"></td>
            <td align="center" id="desvArea4"></td>
            <td align="center" id="cvArea4"></td>
        </tr>
        <tr>
            <td>Concentraci&oacute;n a partir de curva sin Est. Int. (mg/mL)</td>
            <td align="center" id="concurArea3"></td>
            <td align="center" id="concurArea4"></td>
            <td align="center" id="concurProm2"></td>
            <td align="center" id="concurdesv2"></td>
            <td align="center" id="concurcv2"></td>
        </tr>
        <tr>
            <td>Concentraci&oacute;n a partir de curva con Est. Int. (mg/mL)</td>
            <td align="center" id="concurIArea3"></td>
            <td align="center" id="concurIArea4"></td>
            <td align="center" id="concurIProm2"></td>
            <td align="center" id="concurIdesv2"></td>
            <td align="center" id="concurIcv2"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="5">Reporte de resultados</td>
        </tr>
        <tr align="center">
            <td rowspan="2"><strong>Muestra</strong></td>
            <td colspan="2"><strong>Concentraci&oacute;n Impureza sin Est&aacute;ndar Interno</strong></td>
            <td colspan="2"><strong>Concentraci&oacute;n Impureza con Est&aacute;ndar Interno</strong></td>
        </tr>
        <tr align="center">
            <td><strong>% m/m</strong></td>
            <td><strong>% m/v</strong></td>
            <td><strong>% m/m</strong></td>
            <td><strong>% m/v</strong></td>
        </tr>
        <tr align="center">
            <td>1</td>
            <td id="mue1A"></td>
            <td id="mue1B"></td>
            <td id="mue1C"></td>
            <td id="mue1D"></td>
        </tr>
        <tr align="center">
            <td>2</td>
            <td id="mue2A"></td>
            <td id="mue2B"></td>
            <td id="mue2C"></td>
            <td id="mue2D"></td>
        </tr>
        <tr align="center">
            <td>Contenido Promedio</td>
            <td id="mue1AProm"></td>
            <td id="mue1BProm"></td>
            <td id="mue1CProm"></td>
            <td id="mue1DProm"></td>
        </tr>
        <tr align="center">
            <td>Incertidumbre expandida +/-</td>
            <td id="mue1AInc"></td>
            <td id="mue1BInc"></td>
            <td id="mue1CInc"></td>
            <td id="mue1DInc"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="8">Control de calidad con enriquecimiento</td>
        </tr>
        <tr align="center">
            <td><strong>Dato</strong></td>
            <td><strong>Masa(mg)</strong></td>
            <td><strong>Vol. Extracci&oacute;n 1 (mL)</strong></td>
            <td><strong>Vol. de Al&iacute;cuota 1 (mL)</strong></td>
            <td><strong>Vol. de Diluci&oacute;n 2 (mL)</strong></td>
            <td><strong>Vol. de Al&iacute;cuota 2 (mL)</strong></td>
            <td><strong>Vol. de Diluci&oacute;n 3 (mL)</strong></td>
            <td><strong>Concentraci&oacute;n (mg/mL)</strong></td>
        </tr>
        <tr align="center">
            <td>Muestra enriquecida</td>
            <td><input type="text" id="CCRmuestramasa" class="monto"
                       value="<?= $ROW[0]['CCRmuestramasa'] == 0 ? '' : $ROW[0]['CCRmuestramasa'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="CCRmuestradil1" class="monto"
                       value="<?= $ROW[0]['CCRmuestradil1'] == 0 ? '' : $ROW[0]['CCRmuestradil1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="CCRmuestraali1" class="monto"
                       value="<?= $ROW[0]['CCRmuestraali1'] == 0 ? '' : $ROW[0]['CCRmuestraali1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="CCRmuestradil2" class="monto"
                       value="<?= $ROW[0]['CCRmuestradil2'] == 0 ? '' : $ROW[0]['CCRmuestradil2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="CCRmuestraali2" class="monto"
                       value="<?= $ROW[0]['CCRmuestraali2'] == 0 ? '' : $ROW[0]['CCRmuestraali2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="CCRmuestradil3" class="monto"
                       value="<?= $ROW[0]['CCRmuestradil3'] == 0 ? '' : $ROW[0]['CCRmuestradil3'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td id="CCRcon1"></td>
        </tr>
        <tr align="center">
            <td>Est&aacute;ndar Interno</td>
            <td><input type="text" id="CCRinternomasa" class="monto"
                       value="<?= $ROW[0]['CCRinternomasa'] == 0 ? '' : $ROW[0]['CCRinternomasa'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="CCRinternodil1" class="monto"
                       value="<?= $ROW[0]['CCRinternodil1'] == 0 ? '' : $ROW[0]['CCRinternodil1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="CCRinternoali1" class="monto"
                       value="<?= $ROW[0]['CCRinternoali1'] == 0 ? '' : $ROW[0]['CCRinternoali1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="CCRinternodil2" class="monto"
                       value="<?= $ROW[0]['CCRinternodil2'] == 0 ? '' : $ROW[0]['CCRinternodil2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="CCRinternoali2" class="monto"
                       value="<?= $ROW[0]['CCRinternoali2'] == 0 ? '' : $ROW[0]['CCRinternoali2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="CCRinternodil3" class="monto"
                       value="<?= $ROW[0]['CCRinternodil3'] == 0 ? '' : $ROW[0]['CCRinternodil3'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td id="CCRcon2"></td>
        </tr>
        <tr align="center">
            <td>Est&aacute;ndar enriquecimiento</td>
            <td><input type="text" id="CCRenrimasa" class="monto"
                       value="<?= $ROW[0]['CCRenrimasa'] == 0 ? '' : $ROW[0]['CCRenrimasa'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="CCRenridil1" class="monto"
                       value="<?= $ROW[0]['CCRenridil1'] == 0 ? '' : $ROW[0]['CCRenridil1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="CCRenriali1" class="monto"
                       value="<?= $ROW[0]['CCRenriali1'] == 0 ? '' : $ROW[0]['CCRenriali1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="CCRenridil2" class="monto"
                       value="<?= $ROW[0]['CCRenridil2'] == 0 ? '' : $ROW[0]['CCRenridil2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="CCRenriali2" class="monto"
                       value="<?= $ROW[0]['CCRenriali2'] == 0 ? '' : $ROW[0]['CCRenriali2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="CCRenridil3" class="monto"
                       value="<?= $ROW[0]['CCRenridil3'] == 0 ? '' : $ROW[0]['CCRenridil3'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td id="CCRcon3"></td>
        </tr>
        <tr>
            <td colspan="8">
                <hr/>
            </td>
        </tr>
        <tr align="center">
            <td><strong>Dato</strong></td>
            <td><strong>&Aacute;rea 1</strong></td>
            <td><strong>&Aacute;rea 2</strong></td>
            <td><strong>PROMEDIO</strong></td>
            <td><strong>Desv. Est&aacute;ndar</strong></td>
            <td><strong>% C.V.</strong></td>
            <td><strong>% C.V. te&oacute;rico muestra</strong></td>
        </tr>
        <tr align="center">
            <td>&Aacute;rea Muestra 1</td>
            <td><input type="text" id="CCRAreaA1" class="monto" value="<?= $ROW[0]['CCRAreaA1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="CCRAreaB1" class="monto" value="<?= $ROW[0]['CCRAreaB1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td id="CCRProm1"></td>
            <td id="CCRDesv1"></td>
            <td id="CCRCV1"></td>
            <td id="CCRCVT" rowspan="4"></td>
        </tr>
        <tr align="center">
            <td>&Aacute;rea Est&aacute;ndar Interno en la muestra 1</td>
            <td><input type="text" id="CCRAreaA2" class="monto" value="<?= $ROW[0]['CCRAreaA2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="CCRAreaB2" class="monto" value="<?= $ROW[0]['CCRAreaB2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td id="CCRProm2"></td>
            <td id="CCRDesv2"></td>
            <td id="CCRCV2"></td>
        </tr>
        <tr align="center">
            <td><strong>Concentraci&oacute;n a partir de curva sin Est. Int. (mg/mL)</strong></td>
            <td id="CCRAreaA3"></td>
            <td id="CCRAreaB3"></td>
            <td id="CCRProm3"></td>
            <td id="CCRDesv3"></td>
            <td id="CCRCV3"></td>
        </tr>
        <tr align="center">
            <td><strong>Concentraci&oacute;n a partir de curva con Est. Int. (mg/mL)</strong></td>
            <td id="CCRAreaA4"></td>
            <td id="CCRAreaB4"></td>
            <td id="CCRProm4"></td>
            <td id="CCRDesv4"></td>
            <td id="CCRCV4"></td>
        </tr>
        <tr>
            <td colspan="8">
                <hr/>
            </td>
        </tr>
        <tr align="center">
            <td rowspan="2"><strong>Muestra</strong></td>
            <td colspan="2"><strong>Concentraci&oacute;n Impureza sin Est&aacute;ndar Interno</strong></td>
            <td colspan="2"><strong>Concentraci&oacute;n Impureza con Est&aacute;ndar Interno</strong></td>
        </tr>
        <tr align="center">
            <td><strong>% m/m</strong></td>
            <td><strong>% m/v</strong></td>
            <td><strong>% m/m</strong></td>
            <td><strong>% m/v</strong></td>
        </tr>
        <tr align="center">
            <td>Enriquecida total</td>
            <td id="CCRmue1A"></td>
            <td id="CCRmue1B"></td>
            <td id="CCRmue1C"></td>
            <td id="CCRmue1D"></td>
        </tr>
        <tr align="center">
            <td>Enriquecimiento calculado</td>
            <td id="CCRmue2A"></td>
            <td id="CCRmue2B"></td>
            <td id="CCRmue2C"></td>
            <td id="CCRmue2D"></td>
        </tr>
        <tr align="center">
            <td><strong>Porcentaje de recuperaci&oacute;n*</strong></td>
            <td id="CCRmue3A"></td>
            <td id="CCRmue3B"></td>
            <td id="CCRmue3C"></td>
            <td id="CCRmue3D"></td>
        </tr>
        <tr>
            <td colspan="8">*El control de calidad con enriquecimiento es satisfactorio si el porcentaje de recuperaci&oacute;n
                est&aacute; entre 70% y 120%
            </td>
        </tr>
        <tr>
            <td class="titulo" colspan="8">Observaciones</td>
        </tr>
        <tr>
            <td colspan="8"><textarea style="width:98%" id="obs"></textarea></td>
        </tr>
        <tr>
            <td class="titulo" colspan="8">Aprobaci&oacute;n</td>
        </tr>
        <tr>
            <td><br/></td>
        </tr>
        <tr>
            <td>Realizado por:</td>
            <td>_______________________________</td>
            <td>__________________________________</td>
        </tr>
        <tr>
            <td><br/></td>
        </tr>
        <tr>
            <td>Aprobado por:</td>
            <td>_______________________________</td>
            <td>__________________________________</td>
        </tr>
        <tr>
            <td class="titulo" colspan="8">Formulas</td>
        </tr>
        <tr>
            <td colspan="8">
                <p><b>Preparaci&oacute;n de la curva de calibraci&oacute;n</b></p>
                <p>Disoluci&oacute;n madre:</p>
                <p style="text-align: center">
                    <img src="imagenes/H0022/f1.PNG" width="725" height="342" alt="f1"/>
                </p>
                <p>Patrones de la curva de calibraci&oacute;n:</p>
                <p style="text-align: center">
                    <img src="imagenes/H0022/f2.PNG" alt="f2"/>
                </p>

                <p><b>M&iacute;nimos cuadrados, obtenci&oacute;n de la ecuaci&oacute;n de la recta de mejor ajuste,
                        curva de calibraci&oacute;n:</b></p>
                <b>Obtenci&oacute;n de pendiente de la recta de mejor ajuste de curva de calibraci&oacute;n:</b>
                <p style="text-align: center">
                    <img src="imagenes/H0022/f3.PNG" width="699" height="361" alt="f3"/>
                </p>
                <p>M&iacute;nimos cuadrados, obtenci&oacute;n del intercepto de la recta de mejor ajuste de curva de
                    calibraci&oacute;n:</p>
                <p style="text-align: center">
                    <img src="imagenes/H0022/f4.PNG" width="311" height="46" alt="f4"/>
                </p>
                <p>
                    Ecuaci&oacute;n de la recta de mejor ajuste, curva de calibraci&oacute;n:
                </p>
                <p style="text-align: center">
                    <img src="imagenes/H0022/f5.PNG" width="302" height="64" alt="f5"/>
                </p>
                <p>M&iacute;nimos cuadrados, obtenci&oacute;n de la ecuaci&oa&oacute;n de la recta de mejor ajuste,
                    curva de calibraci&oacute;n mediante el uso de est&aacute;ndar interno</p>
                <p>Obtenci&oacute;n de pendiente de la recta de mejor ajuste de curva de calibraci&oacute;n:</p>
                <p style="text-align: center">
                    <img src="imagenes/H0022/f6.PNG" width="715" height="468" alt="f6"/>
                </p>
                <p>M&iacute;nimos cuadrados, obtenci&oa&oacute;n del intercepto de la recta de mejor ajuste de curva de
                    calibraci&oacute;n:</p>
                <p style="text-align: center">
                    <img src="imagenes/H0022/f7.PNG" width="394" height="84" alt="f7"/>
                </p>
                <p>Ecuaci&oacute;n de la recta de mejor ajuste, curva de calibraci&oacute;n:</p>
                <p style="text-align: center">
                    <img src="imagenes/H0022/f8.PNG" width="378" height="78" alt="f8"/>
                </p>
                <p>Obtenci&oacute;n de la concentraci&oacute;n de impureza en la muestra de plaguicida</p>
                <p>Sin usar est&aacute;ndar interno:</p>
                <p style="text-align: center">
                    <img src="imagenes/H0022/f9.PNG" width="622" height="274" alt="f9"/>
                </p>
                <p>Utilizando est&aacute;ndar interno:</p>
                <p style="text-align: center">
                    <img src="imagenes/H0022/f10.PNG" width="622" height="288" alt="f10"/>
                </p>
                <p style="text-align: center">
                    <img src="imagenes/H0022/f11.PNG" width="728" height="467" alt="f11"/>
                </p>
                <p>
                <p><b>Control de calidad con enriquecimiento</b></p>
                </p>
                <p style="text-align: center">
                    <img src="imagenes/H0022/f12.PNG" width="758" height="357" alt="f12"/>
                </p>
                <p style="text-align: center">
                    <img src="imagenes/H0022/f13.PNG" width="752" height="319" alt="f13"/>
                </p>

                <p><b>Donde:</b></p>
                <p>Cn est enriquecimiento calc = Concentraci&oacute;n te&oacute;rica (calculada) del "spike" de est&a&aacute;ndar
                    en la muestra.</p>
                <p>Cn enriq total = Concentraci&oacute;n total de muestra enriquecida (contenido de impureza de muestra
                    m&aacute;s la adicionada de est&aacute;ndar).</p>
                <p>Cn enriq calculada = Concentraci&oacute;n enriquecida experimental, obtenida del an&aacute;lisis.</p>
                <p>Vol dil est final = Volumen final de extracci&oacute;n o diluci&oacute;n del spike de est&aacute;ndar.</p>
                <p>M muestra = Masa de muestra utilizada para la prueba de enriquecimiento.</p>


            </td>
        </tr>
    </table>
    <br/>
    <input type="hidden" id="final_P"/>
    <input type="hidden" id="final_I"/>
    <br/>
    <?php if ($_GET['acc'] == 'V') { ?>
        <script>__calcula();</script>
    <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php } else { ?>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('H0022', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>