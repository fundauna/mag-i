<?php
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_kjeldahl();
$ROW = $Gestor->ObtieneDatos();

if (!$ROW) die('Registro inexistente');

if ($_GET['acc'] == 'V') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="xanalizar" value="<?= $_GET['xanalizar'] ?>"/>
<input type="hidden" id="tipo" value="<?= $_GET['tipo'] ?>"/>
<center>
    <?php $Gestor->Incluir('h10', 'hr', 'An&aacute;lisis :: Determinaci&oacute;n de Nitr&oacute;geno en Fertilizantes por el M&eacute;todo de Kjeldahl') ?>
    <?= $Gestor->Encabezado('H0010', 'e', 'Determinaci&oacute;n de Nitr&oacute;geno en Fertilizantes por el M&eacute;todo de Kjeldahl') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Datos de la muestra</td>
        </tr>
        <tr>
            <td><strong>N&uacute;mero de muestra:</strong></td>
            <td><?= $ROW[0]['ref'] ?></td>
            <td><strong>Solicitud:</strong></td>
            <td><?= $ROW[0]['solicitud'] ?></td>
        </tr>
        <tr>
            <td><strong>Fecha de ingreso:</strong></td>
            <td><?= $ROW[0]['fechaI'] ?></td>
            <td><strong>Fecha de an&aacute;lisis:</strong></td>
            <td><input type="text" id="fechaA" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaA'] ?>" <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Fecha de conclusi&oacute;n del an&aacute;lisis:</strong></td>
            <td><?= $ROW[0]['fechaC'] ?></td>
            <td><strong>% Nitr&oacute;geno declarado:</strong></td>
            <td><input type="text" id="rango" maxlength="20" class="monto" value="<?= $ROW[0]['rango'] ?>"
                    <?= $disabled ?>>&nbsp;
                <select id="unidad" <?php if ($ROW[0]['unidad'] != '' or $_GET['acc'] == 'V') echo 'disabled'; ?>
                        onchange="__calcula()">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['unidad'] == '0') echo 'selected'; ?>>%m/m</option>
                    <option value="1" <?php if ($ROW[0]['unidad'] == '1') echo 'selected'; ?>>%m/v</option>
                </select>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="3">Masa</td>
        </tr>


        <td>&nbsp;</td>
        <td colspan="2" width="100%">
            <!-- -->
        <td class="radius" width="100%">
            <tr align="center">
                <td colspan="2"><strong>Incertidumbre por masa</strong></td>
                <td>Componente</td>
            </tr>
            <tr align="center">
                <td>Repetibilidad</td>
                <td><input type="text" id="repetibilidad" class="monto" onblur="Redondear(this);"
                           value="<?= isset($ROW[0]['rep']) == true ? $ROW[0]['rep'] : '0' ?>" <?= $disabled ?>>
                </td>
                <td id="rep_comp"></td>
            </tr>
            <tr align="center">
                <td>Resoluci&oacute;n</td>
                <td><input type="text" id="resolucion" class="monto" onblur="Redondear(this);"
                           value="<?= isset($ROW[0]['resM']) == true ? $ROW[0]['resM'] : '0' ?>" <?= $disabled ?>>
                </td>
                <td id="res_comp"></td>
            </tr>
            <tr align="center">
                <td>Linealidad</td>
                <td><input type="text" id="inc_cer" class="monto" onblur="Redondear(this);"
                           value="<?= isset($ROW[0]['lilM']) == true ? $ROW[0]['lilM'] : '0' ?>" <?= $disabled ?>></td>
                <td id="cer_comp"></td>
            </tr>
            <tr align="center">
                <td>Excentricidad</td>
                <td><input type="text" id="emp" class="monto" onblur="Redondear(this);"
                           value="<?= isset($ROW[0]['exM']) == true ? $ROW[0]['exM'] : '0' ?>" <?= $disabled ?>></td>
                <td id="emp_comp"></td>
            </tr>
            <tr align="center">
                <td colspan="2"><strong>IC Balanza</strong></td>
                <td id="inc_balanza" style="font-weight:bold"></td>
            </tr>
        </td>
    </table>
    <br/>

    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="3">Densidad</td>
        </tr>


        <td>&nbsp;</td>
        <td colspan="2" width="100%">
            <!-- -->
        <td class="radius" width="100%">
            <tr align="center">
                <td colspan="2"><strong>Incertidumbre por densidad</strong></td>
                <td>Componente</td>
            </tr>
            <tr align="center">
                <td>Certificado</td>
                <td><input type="text" id="certificado" class="monto" onblur="Redondear(this);"
                           value="<?= isset($ROW[0]['icCert']) == true ? $ROW[0]['icCert'] : '0' ?>" <?= $disabled ?>>
                </td>
                <td id="lcertificado"></td>
            </tr>
            <tr align="center">
                <td>Resoluci&oacute;n</td>
                <td><input type="text" id="resolucion1" class="monto" onblur="Redondear(this);"
                           value="<?= isset($ROW[0]['icRes']) == true ? $ROW[0]['icRes'] : '0' ?>" <?= $disabled ?>>
                </td>
                <td id="lresolucion1"></td>
            </tr>
            <tr align="center">
                <td colspan="2"><strong>IC Combinada</strong></td>
                <td id="inc_Combinada" style="font-weight:bold"></td>
            </tr>
        </td>
    </table>

    <br/>


    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="3">C&aacute;lculos</td>
        </tr>
        <tr valign="top">

            <td>
                <!-- -->
                <table class="radius" style="font-size:12px" width="98%">
                    <tr align="center">
                        <td colspan="2"><strong>Datos del an&aacute;lisis</strong></td>
                    </tr>
                    <tr>
                        <td>Masa KHC<sub>8</sub>H<sub>4</sub>O<sub>4</sub> (g)</td>
                        <td><input type="text" id="na" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['na'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td>Masa Molar KHC<sub>8</sub>H<sub>4</sub>O<sub>4</sub> (g/mol)</td>
                        <td><input type="text" id="molar" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['molar'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td>Volumen consumido NaOH (mL)</td>
                        <td><input type="text" id="valorado" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['valorado'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td>Normalidad NaOH (equivalentes/L)</td>
                        <td id="con"></td>
                    </tr>
                    <tr>
                        <td>Volumen valorado NaOH (mL)</td>
                        <td><input type="text" id="repetibilidad2" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['repetibilidad2'] ?>" <?= $disabled ?>></td>


                       <!-- <td align="left" id="cmbinc5">
                            <select id="repetibilidad2" onchange="__calcula()">-->
                                <!--<option value="10" >10</option>
                                <option value="25" >25</option>
                                <option value="50" >50</option>
                                <option value="100" >100</option>-->
                               <!-- <option value=0.5>0.50</option>
                                <option value="1">1.00</option>
                                <option value="2">2.00</option>
                                <option value="3">3.00</option>
                                <option value="4">4.00</option>
                                <option value="5">5.00</option>
                                <option value="6">6.00</option>
                                <option value="7">7.00</option>
                                <option value="8">8.00</option>
                                <option value="9">9.00</option>
                                <option value="10">10.00</option>
                                <option value="15">15.00</option>
                                <option value="20">20.00</option>
                                <option value="25">25.00</option>
                                <option value="50">50.00</option>
                                <option value="100">100.00</option>
                            </select></td>-->
                    </tr>
                    <tr>
                        <td>Volumen consumido H<sub>2</sub>SO<sub>4</sub> (mL)</td>
                        <td><input type="text" id="resolucion2" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['resolucion2'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td>Normalidad H<sub>2</sub>SO<sub>4</sub> (equivalentes/L)</td>
                        <td id="con2"></td>
                    </tr>
                    <tr>
                        <td>Densidad (g/mL)</td>
                        <td><input type="text" id="densidad" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['densidad'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Diluci&oacute;n</strong></td>
                    </tr>
                    <tr>
                        <td>Volumen al&iacute;cuota</td>
                        <td><select id="alicuota" onchange="__calcula()">
                                <option value="0.5" <?php if ($ROW[0]['alicuota'] == '0.50') echo 'selected'; ?>
                                        inc="0.006">
                                    0.50
                                </option>
                                <option value="1" <?php if ($ROW[0]['alicuota'] == '1') echo 'selected'; ?> inc="0.005">
                                    1.00
                                </option>
                                <option value="2" <?php if ($ROW[0]['alicuota'] == '2') echo 'selected'; ?> inc="0.01">
                                    2.00
                                </option>
                                <option value="3" <?php if ($ROW[0]['alicuota'] == '3') echo 'selected'; ?> inc="0.01">
                                    3.00
                                </option>
                                <option value="4" <?php if ($ROW[0]['alicuota'] == '4') echo 'selected'; ?> inc="0.01">
                                    4.00
                                </option>
                                <option value="5" <?php if ($ROW[0]['alicuota'] == '5') echo 'selected'; ?> inc="0.01">
                                    5.00
                                </option>
                                <option value="6" <?php if ($ROW[0]['alicuota'] == '6') echo 'selected'; ?> inc="0.02">
                                    6.00
                                </option>
                                <option value="7" <?php if ($ROW[0]['alicuota'] == '7') echo 'selected'; ?> inc="0.02">
                                    7.00
                                </option>
                                <option value="8" <?php if ($ROW[0]['alicuota'] == '8') echo 'selected'; ?> inc="0.02">
                                    8.00
                                </option>
                                <option value="9" <?php if ($ROW[0]['alicuota'] == '9') echo 'selected'; ?> inc="0.02">
                                    9.00
                                </option>
                                <option value="10" <?php if ($ROW[0]['alicuota'] == '10') echo 'selected'; ?>
                                        inc="0.02">10.00
                                </option>
                                <option value="15" <?php if ($ROW[0]['alicuota'] == '15') echo 'selected'; ?>
                                        inc="0.03">15.00
                                </option>
                                <option value="20" <?php if ($ROW[0]['alicuota'] == '20') echo 'selected'; ?>
                                        inc="0.03">20.00
                                </option>
                                <option value="25" <?php if ($ROW[0]['alicuota'] == '25') echo 'selected'; ?>
                                        inc="0.03">25.00
                                </option>
                                <option value="50" <?php if ($ROW[0]['alicuota'] == '50') echo 'selected'; ?>
                                        inc="0.05">50.00
                                </option>
                                <option value="100" <?php if ($ROW[0]['alicuota'] == '100') echo 'selected'; ?>
                                        inc="0.08">100.00
                                </option>
                            </select></td>
                    </tr>
                    <tr>
                        <td>Volumen bal&oacute;n</td>
                        <td><select id="balon" onchange="__calcula()">
                                <option value="5" <?php if ($ROW[0]['balon'] == '5') echo 'selected'; ?> inc="0.025">
                                    5.00
                                </option>
                                <option value="10" <?php if ($ROW[0]['balon'] == '10') echo 'selected'; ?> inc="0.025">
                                    10.00
                                </option>
                                <option value="25" <?php if ($ROW[0]['balon'] == '25') echo 'selected'; ?> inc="0.03">
                                    25.00
                                </option>
                                <option value="50" <?php if ($ROW[0]['balon'] == '50') echo 'selected'; ?> inc="0.05">
                                    50.00
                                </option>
                                <option value="100" <?php if ($ROW[0]['balon'] == '100') echo 'selected'; ?> inc="0.08">
                                    100.00
                                </option>
                                <option value="200" <?php if ($ROW[0]['balon'] == '200') echo 'selected'; ?> inc="0.1">
                                    200.00
                                </option>
                                <option value="250" <?php if ($ROW[0]['balon'] == '250') echo 'selected'; ?> inc="0.11">
                                    250.00
                                </option>
                                <option value="300" <?php if ($ROW[0]['balon'] == '300') echo 'selected'; ?> inc="0.12">
                                    300.00
                                </option>
                                <option value="500" <?php if ($ROW[0]['balon'] == '500') echo 'selected'; ?> inc="0.15">
                                    500.00
                                </option>
                                <option value="1000" <?php if ($ROW[0]['balon'] == '1000') echo 'selected'; ?>
                                        inc="0.3">1000.00
                                </option>
                                <option value="2000" <?php if ($ROW[0]['balon'] == '2000') echo 'selected'; ?>
                                        inc="0.5">2000.00
                                </option>
                            </select></td>
                    </tr>
                </table>
                <!-- -->
            </td>
            <td>
                <!-- -->
                <table class="radius" style="font-size:15px;" width="98%" cellpadding="1.85" >
                    <tr align="center">
                        <td style="font-size: 13px" width="50px"><strong>Incertidumbre</strong></td>
                        <td align="left" width="5px" style="font-size: 13px"><strong>Bureta</strong></td>
                    </tr>
                    <tr align="center">
                        <td id="inc1"></td>
                    </tr>
                    <tr align="center">
                        <td id="inc2"></td>
                    </tr>

                    <tr>
                        <td align="center" id="inc3"></td>
                        <td align="left">
                            <select id="cbinc3" onchange="__calcula()">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select></td>
                    </tr>
                    <!-- <tr align="center">
                         <td id="inc3"></td>
                     </tr>-->
                    <tr align="center">
                        <td id="inc4"></td>
                    </tr>

                    <tr align="center">
                        <td id="inc5"></td>

                    <tr align="center">
                        <td align="center" id="inc6"></td>
                        <td align="left">
                            <select id="cbinc6" onchange="__calcula()">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select></td>
                    </tr>


                    <tr align="center">
                        <td id="inc7"></td>
                    </tr>
                    <tr align="center">
                        <td id="inc8"></td>
                    </tr>
                    <tr align="center">
                        <td><br></td>
                    </tr>
                    <tr align="center">
                        <td id="inc9"></td>
                    </tr>
                    <tr align="center">
                        <td id="inc10">
                            <?php if ('inc10' > -1) {
                                "mas que 10";
                            } ?>


                        </td>
                    </tr>
                    <tr align="center" hidden>
                        <td><input type="text" id="inc2" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['inc2'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr align="center" hidden>
                        <td><input type="text" id="inc3" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['inc3'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr align="center" hidden>
                        <td><input type="text" id="inc5" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['inc5'] ?>" <?= $disabled ?>></td>
                    </tr>
                </table>
                <!-- -->
            </td>
            <td>
                <!-- -->
                <table class="radius" style="font-size:12px" width="98%" hidden>
                    <tr align="center">
                        <td colspan="2"><strong>Incertidumbre por masa</strong></td>
                        <td>Componente</td>
                    </tr>

                    <tr align="center">
                        <td>Linealidad</td>
                        <td><input type="text" id="inc_cer" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['inc_cer'] ?>" <?= $disabled ?>></td>
                        <td id="cer_comp"></td>
                    </tr>
                    <tr align="center">
                        <td>Excentricidad</td>
                        <td><input type="text" id="emp" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['emp'] ?>" <?= $disabled ?>></td>
                        <td id="emp_comp"></td>
                    </tr>
                    <tr align="center">
                        <td colspan="2"><strong>IC Balanza</strong></td>
                        <td id="inc_balanza" style="font-weight:bold"></td>
                    </tr>
                </table>
                <!-- -->
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="5">Titulaci&oacute;n de muestras</td>
        </tr>
        <tr align="center">
            <td>Muestra</td>
            <td>Masa (g)</td>
            <td>mL H<sub>2</sub>SO<sub>4</sub></td>
            <td>%m/m N</td>
            <td>% m/v N</td>
        </tr>
        <tr align="center">
            <td>1</td>
            <td><input type="text" id="masaA1" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['masaA1'] ?>"
                    <?= $disabled ?>></td>
            <td><input type="text" id="masaB1" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['masaB1'] ?>"
                    <?= $disabled ?>></td>
            <td id="mmA1"></td>
            <td id="mmB1"></td>
        </tr>
        <tr align="center">
            <td>2</td>
            <td><input type="text" id="masaA2" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['masaA2'] ?>"
                    <?= $disabled ?>></td>
            <td><input type="text" id="masaB2" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['masaB2'] ?>"
                    <?= $disabled ?>></td>
            <td id="mmA2"></td>
            <td id="mmB2"></td>
        </tr>
        <tr align="center">
            <td colspan="2"></td>
            <td><strong>Promedio</strong></td>
            <td id="promA"></td>
            <td id="promB"></td>
        </tr>
        <tr align="center">
            <td colspan="2"></td>
            <td><strong>Desviaci&oacute;n Est&aacute;ndar</strong></td>
            <td id="desvA"></td>
            <td id="desvB"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="5">C&aacute;lculo de Incertidumbre</td>
        </tr>
        <tr align="center">
            <td></td>
            <td>Valor</td>
            <td>Incertidumbre</td>
            <td>Inc. Estandarizada</td>
            <td>Cuadrados</td>
        </tr>
        <tr align="center">
            <td>mL H<sub>2</sub>SO<sub>4</sub> promedio</td>
            <td id="val1"></td>
            <td><input type="text" id="des1" class="monto"  onblur="Redondear(this);" value="<?= $ROW[0]['des1'] ?>"
                    <?= $disabled ?>></td>
            <td id="est1"></td>
            <td id="cua1"></td>
        </tr>
        <tr align="center">
            <td>Concentraci&oacute;n H<sub>2</sub>SO<sub>4</sub></td>
            <td id="val2"></td>
            <td id="des2"></td>
            <td id="est2"></td>
            <td id="cua2"></td>
        </tr>
        <tr align="center">
            <td>Masa muestra promedio</td>
            <td id="val3"></td>
            <td id="des3"></td>
            <td id="est3"></td>
            <td id="cua3"></td>
        </tr>
        <tr align="center">
            <td>Volumen al&iacute;cuota</td>
            <td id="val4"></td>
            <td id="des4"></td>
            <td id="est4"></td>
            <td id="cua4"></td>
        </tr>
        <tr align="center">
            <td>Volumen bal&oacute;n</td>
            <td id="val5"></td>
            <td id="des5"></td>
            <td id="est5"></td>
            <td id="cua5"></td>
        </tr>
        <tr align="center">
            <td>MM nitr&oacute;geno</td>
            <td><input type="text" id="val6" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['val6'] ?>"
                    <?= $disabled ?>></td>
            <td><input type="text" id="des6" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['des6'] ?>"
                    <?= $disabled ?>></td>
            <td id="est6"></td>
            <td id="cua6"></td>
        </tr>
        <tr align="center">
            <td>Dispersi&oacute;n muestras</td>
            <td id="val7"></td>
            <td id="des7"></td>
            <td id="est7"></td>
            <td id="cua7"></td>
        </tr>
        <tr align="center">
            <td>Densidad</td>
            <td id="val8"></td>
            <td id="des8"></td>
            <td id="est8"></td>
            <td id="cua8"></td>
        </tr>
        <tr align="center">
            <td colspan="3"></td>
            <td>Inc formula % m/m</td>
            <td id="suma1"></td>
        </tr>
        <tr align="center">
            <td colspan="3"></td>
            <td>Inc formula % m/v</td>
            <td id="suma2"></td>
        </tr>
        <tr align="center">
            <td colspan="3"></td>
            <td>IFC % m/m</td>
            <td id="suma3"></td>
        </tr>
        <tr align="center">
            <td colspan="3"></td>
            <td>IFC % m/v</td>
            <td id="suma4"></td>
        </tr>
        <tr align="center">
            <td colspan="3"></td>
            <td>IE % m/m</td>
            <td id="suma5"></td>
        </tr>
        <tr align="center">
            <td colspan="3"></td>
            <td>IE % m/v</td>
            <td id="suma6"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo">Observaciones</td>
        </tr>
        <tr>
            <td><textarea id="obs" style="width:98%" <?= $disabled ?>><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
        <tr>
            <td class="titulo" colspan="6">Formulas</td>
        </tr>
        <tr>
            <td colspan="6">
                <p style="text-align: center">
                    <img src="imagenes/H0010/f1.PNG" width="401" height="155" alt="f1"/>
                </p>

                <p><b>Donde:</b></p>
                <p>V<sub>H<sub>2</sub> SO<sub>4</sub> </sub>: Volumen consumido de soluci&oacute;n de &aacute;cido sulf&uacute;rico
                    en mL</p>
                <p>C<sub>H <sub>2</sub> SO<sub>4</sub> </sub>: Concentraci&oacute;n en normalidad de soluci&oacute;n de
                    &aacute;cido sulf&uacute;rico</p>
                <p>D: factor de diluci&oacute;n</p>
                <p>m: masa de la muestra en gramos (g)</p>
                <p>&#961;: densidad en g / mL</p>
                <p>% m/m = g / 100 g </p>
                <p>% m/v = g / 100 mL </p>
                <p>MM = Masa Molar </p>

            </td>
        </tr>
    </table>
    <br/>
    <?php
    if ($_GET['acc'] == 'V') {
        $ROW2 = $Gestor->Analista();
        echo "<br /><table class='radius' width='98%'><tr><td><strong>Realizado por:</strong> {$ROW2[0]['analista']}</td></tr></table>";
    }
    ?>
    <br/>
    <script>__calcula();</script>
    <?php if ($_GET['acc'] == 'V') { ?>
        <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php } else { ?>
        <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('H0010', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>