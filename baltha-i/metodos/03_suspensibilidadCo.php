<?php
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_suspensibilidadCo();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

if ($_GET['acc'] == 'V') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="xanalizar" value="<?= $_GET['xanalizar'] ?>"/>
<input type="hidden" id="tipo" value="<?= $_GET['tipo'] ?>"/>
<center>
    <?php $Gestor->Incluir('h17', 'hr', 'An&aacute;lisis :: Determinaci&oacute;n de Suspensibilidad de Cobre') ?>
    <?= $Gestor->Encabezado('H0017', 'e', 'Determinaci&oacute;n de Suspensibilidad de Cobre') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="3">Datos de la muestra</td>
        </tr>
        <tr>
            <td>N&uacute;mero:</td>
            <td><?= $ROW[0]['ref'] ?></td>
            <td>Ingrediente Activo:</td>
        </tr>
        <tr>
            <td>Fecha de ingreso:</td>
            <td><?= $ROW[0]['fechaI'] ?></td>
            <td><input type="text" id="ingrediente" maxlength="30" value="<?= $ROW[0]['ingrediente'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td>Fecha de an&aacute;lisis:</td>
            <td><input type="text" id="fechaA" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaA'] ?>" <?= $disabled ?>></td>
            <td>Tipo de formulaci&oacute;n:</td>
        </tr>
        <tr>
            <td>Fecha de conclusi&oacute;n del an&aacute;lisis:</td>
            <td><?= $ROW[0]['fechaC'] ?></td>
            <td><?= $ROW[0]['tipo_form'] ?><input type="hidden" id="tipo_form" value="<?= $ROW[0]['tipo_form'] ?>"/>
            </td>
        </tr>
        <tr>
            <td>Dosis recomendada:</td>
            <td><input type="text" id="dosis" size="10" value="<?= $ROW[0]['dosis'] ?>" <?= $disabled ?>></td>
            <td><?php if ($_GET['acc'] == 'V') { ?><strong>Creado por:</strong> <?= $ROW[0]['analista'] ?><?php } ?>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Datos del m&eacute;todo</td>
        </tr>
        <tr>
            <td>Temperatura del agua del ba&ntilde;o mar&iacute;a (�C):</td>
            <td><input type="text" id="maria" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['maria'] ?>"
                       <?= $disabled ?>/></td>
            <td>N&uacute;mero de reactivo agua dura 342ppm:</td>
            <td><input type="text" id="numreactivo" maxlength="30" value="<?= $ROW[0]['numreactivo'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td>Fecha de preparaci&oacute;n agua dura 342ppm:</td>
            <td><input type="text" id="fechaP" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaP'] ?>" <?= $disabled ?>></td>
            <td>Fecha de comprobaci&oacute;n agua dura 342ppm:</td>
            <td><input type="text" id="fechaD" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaD'] ?>" <?= $disabled ?>></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td>Resultado de la comprobaci&oacute;n agua dura 342ppm:</td>
            <td><input type="text" id="resultado" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['resultado'] ?>" <?= $disabled ?>/></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Datos del an&aacute;lisis</td>
        </tr>
        <tr>
            <td>Volumen de muestra (mL)</td>
            <td><input type="text" id="masa" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['valA1'] ?>"
                       <?= $disabled ?>/></td>
            <td>C. Masa de I.A en la muestra</td>
            <td id="res1"></td>
        </tr>
        <tr>
            <td>A. Masa de muestra (g)</td>
            <td><input type="text" id="muestra" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['valA2'] ?>"
                       <?= $disabled ?>/></td>
            <td>Q. Masa de I.A. en los 25 mL</td>
            <td id="res2"></td>
        </tr>
        <tr>
            <td>B. % m/m de I.A. encontrado</td>
            <td><input type="text" id="encontrado" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['valA3'] ?>" <?= $disabled ?>/></td>
            <td><strong>% S.I.A</strong></td>
            <td id="res3"></td>
        </tr>
        <tr>
            <td>Conc. Disln Tiosulfato (mol/L)</td>
            <td><input type="text" id="yodo" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['valA4'] ?>"
                       <?= $disabled ?>/></td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td>Vol. Tiosulfato consumido (mL)</td>
            <td><input type="text" id="consumido" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['valA5'] ?>"
                       <?= $disabled ?>/></td>
            <td colspan="2">&nbsp;</td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo">Observaciones</td>
        </tr>
        <tr>
            <td><textarea id="obs" style="width:98%" <?= $disabled ?>><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
        <tr>
            <td class="titulo" colspan="6">Formulas</td>
        </tr>
        <tr>
            <td colspan="6">
                <p><b>Determinaci&oacute;n de la suspensibilidad de cobre:</b></p>
                <p style="text-align: center">
                    <img src="imagenes/H0017/f1.PNG" width="246" height="61" alt="f1"/>
                </p>
                <p>
                    <b>Q = Masa de cobre en la porci&oacute;n de 25 mL de la probeta de suspensibilidad</b>
                </p>
                <p style="text-align: center">
                    <img src="imagenes/H0017/f2.PNG" width="675" height="162" alt="f2"/>
                </p>
                <p>Donde:</p>
                <p>MM cobre = Masa molar de cobre = 63,546 g/mol</p>
                <p>Cn tiosulfato = Concentraci&oacute;n de disoluci&oacute;n valorante de tiosulfato de sodio.</p>
                <p>Vol tiosulfato = Volumen gastado de tiosulfato de sodio como valorante.</p>
                <p>Vol alic = Volumen de al&iacute;cuota tomada a partir de la porci&oacute;n de 25 mL, de la prueba de
                    suspensibilidad (en caso de no haber diluci&oacute;n, se expresa como 1).</p>
                <p>Vol matraz = Volumen de matraz volum&eacute;trico en el que se diluye la al&iacute;cuota tomada a
                    partir de la porci&oacute;n de 25 mL, de la prueba de suspensibilidad (en caso de no haber diluci&oacute;n,
                    se expresa como 1).</p>
                <p>FG = Factor gravim&eacute;trico = </p>
                <table border="1">
                    <thead>
                    <tr>
                        <th>Compuesto de cobre</th>
                        <th>Factor gravim&eacute;trico</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Carbonato de cobre</td>
                        <td>1,74</td>
                    </tr>
                    <tr>
                        <td>Hidr&oacute;xido de cobre (II)</td>
                        <td>1,54</td>
                    </tr>
                    <tr>
                        <td>Oxicloruro de cobre</td>
                        <td>1,68</td>
                    </tr>
                    <tr>
                        <td>Oxido de cobre (I)</td>
                        <td>1,13</td>
                    </tr>
                    <tr>
                        <td>Sulfato de cobre (II)</td>
                        <td>3,93</td>
                    </tr>
                    </tbody>
                </table>
                <p><b>C= Masa de ingrediente activo en la muestra</b></p>
                <p style="text-align: center">
                    <img src="imagenes/H0017/f3.PNG" width="423" height="64" alt="f3"/>
                </p>

                <p><b>Donde:</b></p>
                <p>M muestra = Masa de muestra pesada para la prueba de suspensibilidad</p>
                <p>Cn cobre muestra = Concentraci&oacute;n de cobre (elemental o compuesto, seg&uacute;n corresponda)
                    como ingrediente activo encontrada en la muestra de prueba. </p>


            </td>
        </tr>
    </table>
    <br/>
    <br/>
    <?php if ($_GET['acc'] == 'V') { ?>
        <script>__calcula();</script>
    <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php }else{ ?>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('H0017', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>