<?php
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _01_quechers();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<form>
    <input type="hidden" id="xanalizar" value="<?= $_GET['xanalizar'] ?>"/>
    <input type="hidden" id="tipo" value="<?= $_GET['tipo'] ?>"/>
    <input type="hidden" id="matriz" value="<?= $ROW[0]['matriz'] ?>"/>
    <center>
        <?php $Gestor->Incluir('h0', 'hr', 'An&aacute;lisis :: Hoja de ingreso de resultados de m&eacute;todo QuEChERS') ?>
        <?= $Gestor->Encabezado('H0000', 'e', 'Hoja de ingreso de resultados de m&eacute;todo QuEChERS') ?>
        <br>
        <table class="radius" style="font-size:12px" width="500px">
            <tr>
                <td class="titulo" colspan="2">Informaci&oacute;n General</td>
            </tr>
            <tr>
                <td><strong>No. de muestra:</strong></td>
                <!-- <td><?= str_replace('--0', '', $ROW[0]['ref']); ?></td>-->
                <td><?=
                    $mu = str_replace('--0', '', str_replace('-1 ', '-01', str_replace('-2 ', '-02', str_replace('-3 ', '-03', str_replace('-4 ', '-04', str_replace('-5 ', '-05', str_replace('-6 ', '-06', str_replace('-7 ', '-07',
                        str_replace('-8 ', '-08', str_replace('-9 ', '-09', $ROW[0]['ref']))))))))));
                    ?></td>
            </tr>
            <tr>
                <td><strong>Matriz:</strong></td>
                <td><?= $ROW[0]['nombre'] ?></td>
            </tr>
            <?php
            if ($_GET['acc'] == 'V') {
                $ROW2 = $Gestor->Analista();
                ?>
                <tr>
                    <td><strong>Realizado por:</strong></td>
                    <td><?= $ROW2[0]['analista'], ', ', $ROW2[0]['fecha'] ?></td>
                </tr>
                <?php
            }
            ?>
        </table>
        <br/>
        <?php
        if ($_GET['acc'] == 'I') {
            ?>
            <table class="radius" style="font-size:12px" width="500px">
                <tr>
                    <td class="titulo" colspan="4">Detalle de resultados</td>
                </tr>
                <tr>
                    <td><strong>Analito </strong></td>
                    <td><strong>T&eacute;cnica</strong></td>
                    <td><strong>Resultado (mg/kg)</strong></td>
                    <td align="center"><strong>Reportar</strong></td>
                </tr>
                <?php

                $Q = $Gestor->MAAsignados($ROW[0]['matriz']);

                $ROW = $Gestor->Analitos();

                for ($x = 0; $x < count($ROW); $x++) {
                    $hide = 'hidden';

                    foreach ($Q as $dato) {
                        if ($ROW[$x]['id'] == $dato['ida']) {
                            $hide = '';
                            break;
                        }
                    }
                    if ($ROW[$x]['tipoCroma'] == 1) {
                        $type = 'CL EM/EM';
                    } else if ($ROW[$x]['tipoCroma'] == 2) {
                        $type = 'CG EM/EM';
                    } else if ($ROW[$x]['tipoCroma'] == '') {
                        $type = '';
                    }
                    ?>
                    <tr <?= $hide ?>>
                        <td style="display: none"><?= $ROW[$x]['id'] ?><input type="hidden" name="id" id="id"
                                                                              value="<?=

                                                                              $ROW[$x]['id'] ?>"/></td>
                        <td><?= $ROW[$x]['nombre'] ?><input type="hidden" name="analito"
                                                            value="<?= $ROW[$x]['nombre'] ?>"/></td>
                        <td><?= $type ?><input type="hidden" id="tecnica" name="tecnica"
                                               value="<?= $ROW[$x]['tipoCroma'] ?>"/></td>
                        <!--<td><input type="text" name="resultado" class="monto" onblur="Redondear(this)" value="ND"></td>-->
                        <td><input type="text" name="resultado" class="monto" value="ND"></td>
                        <td align="center"><input type="checkbox" name="reportar" checked/></td>
                    </tr>
                    <tr <?= $hide ?>>
                        <td colspan="4">
                            <hr/>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                <tr>
                    <td align="center" colspan="3"><br/><input type="button" id="btn" value="Aceptar" class="boton"
                                                               onclick="datos();"></td>
                </tr>
            </table>
            <?php
        } elseif ($_GET['acc'] == 'V') {
            ?>
            <table class="radius" style="font-size:12px" width="400px">
                <tr>
                    <td class="titulo" colspan="4">Detalle de resultados</td>
                </tr>
                <tr>
                    <td><strong>Analito</strong></td>
                    <td><strong>T&eacute;cnica</strong></td>
                    <td align="center"><strong>Resultado (mg/kg)</strong></td>
                    <td align="center"><strong>Reportar</strong></td>
                </tr>
                <?php
                $Q = $Gestor->MAAsignados($ROW[0]['matriz']);
                $ROW = $Gestor->Detalle();
                for ($x = 0; $x < count($ROW); $x++) {
                    $hide = 'hidden';
                    foreach ($Q as $dato) {
                       // if (str_replace(' ', '', $ROW[$x]['analito']) == str_replace(' ', '', strtolower(utf8_decode($dato['analito'])))) {
                        if (str_replace(' ', '', $ROW[$x]['analito']) == str_replace(' ', '', (utf8_decode($dato['analito'])))) {
                            $hide = '';
                        }
                    }

                    if ($ROW[$x]['tecnica'] == 1) {
                        $type = 'CL EM/EM';
                    } else if ($ROW[$x]['tecnica'] == 2) {
                        $type = 'CG EM/EM';
                    } else if ($ROW[$x]['tecnica'] == '') {
                        $type = '';
                    }
                    ?>
                    <tr <?= $hide ?>>
                        <td><?= $ROW[$x]['analito'] ?></td>
                        <td><?= $type ?></td>
                        <td align="center"><?= $Gestor->Formato($ROW[$x]['conc']) ?></td>
                        <td align="center"><input type="checkbox"
                                                  disabled <?php if ($ROW[$x]['reportar'] == '1') echo 'checked'; ?> />
                        </td>
                    </tr>
                    <tr <?= $hide ?>>
                        <td colspan="4">
                            <hr/>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <br/><input type="button" value="Imprimir" class="boton" onclick="window.print();">
            <?php
        }
        ?>
    </center>
    <?= $Gestor->Encabezado('H0000', 'p', '') ?>
    <?= $Gestor->Footer(2) ?>
</form>
</body>
</html>