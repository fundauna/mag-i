<?php
//OK
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_suspensibilidadDi();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW)
    die('Registro inexistente');

if ($_GET['acc'] == 'V')
    $disabled = 'disabled';
else
    $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="xanalizar" value="<?= $_GET['xanalizar'] ?>"/>
<input type="hidden" id="tipo" value="<?= $_GET['tipo'] ?>"/>
<center>
    <?php $Gestor->Incluir('h18', 'hr', 'An&aacute;lisis :: Determinaci&oacute;n de Suspensibilidad de Ditiocarbamatos') ?>
    <?= $Gestor->Encabezado('H0018', 'e', 'Determinaci&oacute;n de Suspensibilidad de Ditiocarbamatos') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="3">Datos de la muestra</td>
        </tr>
        <tr>
            <td>N&uacute;mero:</td>
            <td><?= $ROW[0]['ref'] ?></td>
            <td>Ingrediente Activo:</td>
        </tr>
        <tr>
            <td>Fecha de ingreso:</td>
            <td><?= $ROW[0]['fechaI'] ?></td>
            <td><input type="text" id="ingrediente" maxlength="30" value="<?= $ROW[0]['ingrediente'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td>Fecha de an&aacute;lisis:</td>
            <td><input type="text" id="fechaA" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaA'] ?>" <?= $disabled ?>></td>
            <td>Tipo de formulaci&oacute;n:</td>
        </tr>
        <tr>
            <td>Fecha de conclusi&oacute;n del an&aacute;lisis:</td>
            <td><?= $ROW[0]['fechaC'] ?></td>
            <td><?= $ROW[0]['tipo_form'] ?><input type="hidden" id="tipo_form" value="<?= $ROW[0]['tipo_form'] ?>"/>
            </td>
        </tr>
        <tr>
            <td>Dosis recomendada:</td>
            <td><input type="text" id="dosis" size="10" value="<?= $ROW[0]['dosis'] ?>" <?= $disabled ?>></td>
            <td><?php if ($_GET['acc'] == 'V') { ?><strong>Creado por:</strong> <?= $ROW[0]['analista'] ?><?php } ?>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Datos del m&eacute;todo</td>
        </tr>
        <tr>
            <td>Temperatura del agua del ba&ntilde;o mar&iacute;a (�C):</td>
            <td><input type="text" id="maria" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['maria'] ?>"
                       <?= $disabled ?>/></td>
            <td>N&uacute;mero de reactivo agua dura 342ppm:</td>
            <td><input type="text" id="numreactivo" maxlength="30" value="<?= $ROW[0]['numreactivo'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td>Fecha de preparaci&oacute;n agua dura 342ppm:</td>
            <td><input type="text" id="fechaP" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaP'] ?>" <?= $disabled ?>></td>
            <td>Fecha de comprobaci&oacute;n agua dura 342ppm:</td>
            <td><input type="text" id="fechaD" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaD'] ?>" <?= $disabled ?>></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td>Resultado de la comprobaci&oacute;n agua dura 342ppm:</td>
            <td><input type="text" id="resultado" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['resultado'] ?>" <?= $disabled ?>/></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Datos del an&aacute;lisis</td>
        </tr>
        <tr>
            <td>Volumen de muestra (mL)</td>
            <td><input type="text" id="masa" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['valA1'] ?>"
                       <?= $disabled ?>/></td>
            <td>Factor Gravim&eacute;trico</td>
            <td><select id="factor" <?= $disabled ?> onchange="__calcula()">
                    <option value="13.88">Ferban (13.88)</option>
                    <option value="12.82" <?php if ($ROW[0]['valB1'] == '12.82') echo 'selected'; ?>>Naban (12.82)
                    </option>
                    <option value="13.55" <?php if ($ROW[0]['valB1'] == '13.55') echo 'selected'; ?>>Mancozeb (13.55)
                    </option>
                    <option value="13.27" <?php if ($ROW[0]['valB1'] == '13.27') echo 'selected'; ?>>Maneb (13.27)
                    </option>
                    <option value="14.50" <?php if ($ROW[0]['valB1'] == '14.50') echo 'selected'; ?>>Propineb (14.50)
                    </option>
                    <option value="13.79" <?php if ($ROW[0]['valB1'] == '13.79') echo 'selected'; ?>>Zineb (13.79)
                    </option>
                    <option value="15.29" <?php if ($ROW[0]['valB1'] == '15.29') echo 'selected'; ?>>Ziran (15.29)
                    </option>
                    <option value="17.89" <?php if ($ROW[0]['valB1'] == '17.89') echo 'selected'; ?>>Metiram (17.89)
                    </option>
                </select>
            </td>
        </tr>
        <tr>
            <td>A. Masa de muestra (g)</td>
            <td><input type="text" id="muestra" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['valA2'] ?>"
                       <?= $disabled ?>/></td>
            <td>C. Masa de I.A en la muestra</td>
            <td id="res1"></td>
        </tr>
        <tr>
            <td>B. % m/m de I.A. encontrado</td>
            <td><input type="text" id="encontrado" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['valA3'] ?>" <?= $disabled ?>/></td>
            <td>Q. Masa de I.A. en los 25 mL</td>
            <td id="res2"></td>
        </tr>
        <tr>
            <td>Conc. Disln Yodo (mol/L)</td>
            <td><input type="text" id="yodo" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['valA4'] ?>"
                       <?= $disabled ?>/></td>
            <td><strong>% S.I.A</strong></td>
            <td id="res3"></td>
        </tr>
        <tr>
            <td>Vol. disln I<sub>2</sub> consumido (mL)</td>
            <td><input type="text" id="consumido" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['valA5'] ?>"
                       <?= $disabled ?>/></td>
            <td colspan="2">&nbsp;</td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo">Observaciones</td>
        </tr>
        <tr>
            <td><textarea id="obs" style="width:98%" <?= $disabled ?>><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
        <tr>
            <td class="titulo" colspan="6">Formulas</td>
        </tr>
        <tr>
            <td colspan="6">
                <p><b>Determinaci&oacute;n de la suspensibilidad del ditiocarbamato:</b></p>
                <p style="text-align: center">
                    <img src="imagenes/H0018/f1.PNG" width="274" height="74" alt="f1"/>
                </p>
                <p><b>Q = Masa de ditiocarbamato en la porci&oacute;n de 25 mL de la probeta de suspensibilidad</b></p>
                <p style="text-align: center">
                    <img src="imagenes/H0018/f2.PNG" width="516" height="85" alt="f2"/>
                </p>

                <p><b>Donde:</b></p>
                <p>Vol I<sub>2</sub> consumido = Volumen de valorante de disoluci&oacute;n de Yodo consumido.</p>
                <p>Cn I<sub>2</sub> = Concentraci&oacute;n de valorante de disoluci&oacute;n de Yodo.</p>
                <p>Vol alic = Volumen de al&iacute;cuota tomada a partir de la porci&oacute;n de 25 mL, de la prueba de
                    suspensibilidad (en caso de no haber diluci&oacute;n, se expresa como 1).</p>
                <p>Vol matraz = Volumen de matraz volum&eacute;trico en el que se diluye la al&iacute;cuota tomada a
                    partir de la porci&oacute;n de 25 mL, de la prueba de suspensibilidad (en caso de no haber diluci&oacute;n,
                    se expresa como 1).</p>
                <p>FG = Factor gravim&eacute;trico correspondiente al ditiocarbamato analizado =</p>
                <table border="1">
                    <thead>
                    <tr>
                        <th>Ditiocarbamato</th>
                        <th>Factor gravim&eacute;trico</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td>Ferbam</td>
                        <td>138,82</td>
                    </tr>
                    <tr>
                        <td>Mancozeb</td>
                        <td>135,50</td>
                    </tr>
                    <tr>
                        <td>Maneb</td>
                        <td>132,65</td>
                    </tr>
                    <tr>
                        <td>Metam Sodio</td>
                        <td>129,20</td>
                    </tr>
                    <tr>
                        <td>Metiram</td>
                        <td>136,08</td>
                    </tr>
                    <tr>
                        <td>Nabam</td>
                        <td>128,18</td>
                    </tr>
                    <tr>
                        <td>Propineb</td>
                        <td>144,90</td>
                    </tr>
                    <tr>
                        <td>Zineb</td>
                        <td>137,87</td>
                    </tr>
                    <tr>
                        <td>Ziram</td>
                        <td>152,91</td>
                    </tr>

                    </tbody>
                </table>
                <p><b>C= Masa de ingrediente activo en la muestra</b></p>
                <p><b>Donde:</b></p>
                <p>M muestra = Masa de muestra pesada para la prueba de suspensibilidad</p>
                <p>Cn ditiocarbamato muestra = Concentraci&oacute;n de ditiocarbamato como ingrediente activo encontrada
                    en la muestra de prueba. </p>

            </td>
        </tr>
    </table>
    <br/>
    <br/>
    <?php if ($_GET['acc'] == 'V') { ?>
        <script>__calcula();</script>
    <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php } else { ?>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('H0018', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>