<?php
//OK
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_inccc();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

if ($_GET['acc'] == 'V') $disabled = 'disabled';
else $disabled = '';
$ing = ['boro', 'calcio', 'cobre', 'fosforo', 'hierro', 'magnesio', 'manganeso', 'potasio', 'zinc', 'nitrogeno', 'azufre', 'arsenico', 'cadmio', 'cromo', 'plomo', 'mercurio'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<center>
    <?php $Gestor->Incluir('h23', 'hr', 'An&aacute;lisis :: Incertidumbre por curvas de calibraci&oacute;n') ?>
    <?= $Gestor->Encabezado('H0023', 'e', 'Incertidumbre por curvas de calibraci&oacute;n') ?>
    <?php for ($z = 0; $z < count($ing); $z++) { ?>
        <br>
        <table class="radius" style="font-size:11px; min-width:2200px;" width="98%">
            <tr>
                <td class="titulo" colspan="3"><?= $ing[$z] ?></td>
            </tr>
            <tr>
                <td>
                    <table class="radius" width="100%" cellspacing="0" cellpadding="0">
                        <tr align="center">
                            <td colspan="14"><b>Absorbancia</b></td>
                        </tr>
                        <tr align="center">
                            <td><b>Concentraci&oacute;n (mg/L)</b></td>
                            <td><b>Inc</b></td>
                            <td><b>Inc Rel (%)</b></td>
                            <td><b>A1</b></td>
                            <td><b>A2</b></td>
                            <td><b>A3</b></td>
                            <td><b>A4</b></td>
                            <td><b>C1</b></td>
                            <td><b>C2</b></td>
                            <td><b>C3</b></td>
                            <td><b>C4</b></td>
                            <td><b>Cp</b></td>
                            <td><b>A</b></td>
                            <td><b>C</b></td>
                        </tr>
                        <?php for ($i = 1; $i < 6; $i++) { ?>
                            <tr align="center">
                                <td>
                                    <input type="number" id="<?= $ing[$z] ?>con<?= $i ?>" class="monto"
                                           onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                           step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'con' . $i] ?>"
                                           disabled/>
                                </td>
                                <td>
                                    <input type="number" id="<?= $ing[$z] ?>inc<?= $i ?>" class="monto"
                                           onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                           step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'inc' . $i] ?>"
                                           disabled/>
                                </td>
                                <td>
                                    <input type="number" id="<?= $ing[$z] ?>increl<?= $i ?>" class="monto"
                                           onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                           step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'increl' . $i] ?>"
                                           disabled/>
                                </td>
                                <td>
                                    <input type="number" id="<?= $ing[$z] ?>A1<?= $i ?>" class="monto"
                                           onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                           step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'AA1' . $i] ?>"
                                           <?= $disabled ?>/>
                                </td>
                                <td>
                                    <input type="number" id="<?= $ing[$z] ?>A2<?= $i ?>" class="monto"
                                           onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                           step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'A2' . $i] ?>"
                                           <?= $disabled ?>/>
                                </td>
                                <td>
                                    <input type="number" id="<?= $ing[$z] ?>A3<?= $i ?>" class="monto"
                                           onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                           step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'A3' . $i] ?>"
                                           <?= $disabled ?>/>
                                </td>
                                <td>
                                    <input type="number" id="<?= $ing[$z] ?>A4<?= $i ?>" class="monto"
                                           onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                           step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'A4' . $i] ?>"
                                           <?= $disabled ?>/>
                                </td>
                                <td>
                                    <input type="number" id="<?= $ing[$z] ?>C1<?= $i ?>" class="monto"
                                           onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                           step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'CC1' . $i] ?>"
                                           <?= $disabled ?>/>
                                </td>
                                <td>
                                    <input type="number" id="<?= $ing[$z] ?>C2<?= $i ?>" class="monto"
                                           onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                           step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'C2' . $i] ?>"
                                           <?= $disabled ?>/>
                                </td>
                                <td>
                                    <input type="number" id="<?= $ing[$z] ?>C3<?= $i ?>" class="monto"
                                           onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                           step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'C3' . $i] ?>"
                                           <?= $disabled ?>/>
                                </td>
                                <td>
                                    <input type="number" id="<?= $ing[$z] ?>C4<?= $i ?>" class="monto"
                                           onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                           step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'C4' . $i] ?>"
                                           <?= $disabled ?>/>
                                </td>
                                <td>
                                    <input type="number" id="<?= $ing[$z] ?>cp<?= $i ?>" class="monto"
                                           onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                           step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'cp' . $i] ?>"
                                           disabled/>
                                </td>
                                <td>
                                    <input type="number" id="<?= $ing[$z] ?>a<?= $i ?>" class="monto"
                                           onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                           step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'a' . $i] ?>"
                                           disabled/>
                                </td>
                                <td>
                                    <input type="number" id="<?= $ing[$z] ?>c<?= $i ?>" class="monto"
                                           onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                           step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'c' . $i] ?>"
                                           disabled/>
                                </td>
                            </tr>
                        <?php } ?>
                        <tr align="center">
                            <td><b>M&aacute;ximo</b></td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>maxinc" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'maxinc'] ?>" disabled/>
                            </td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>maxincrel" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'maxincrel'] ?>" disabled/>
                            </td>
                            <td colspan="9"></td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>a6" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'a6'] ?>" disabled/>
                            </td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>c6" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'c6'] ?>" disabled/>
                            </td>
                        </tr>
                        <tr align="center">
                            <td><b>N&uacute;mero de mediciones por muestra (p)</b></td>
                            <td colspan="2"></td>
                            <td><input type="number" id="<?= $ing[$z] ?>nummedxmuestra" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'nummedxmuestra'] ?>"
                                       <?= $disabled ?>/>
                            </td>
                            <td colspan="8"></td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>a7" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'a7'] ?>" disabled/>
                            </td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>c7" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'c7'] ?>" disabled/>
                            </td>
                        </tr>
                        <tr align="center">
                            <td><b>No. de mediciones por punto de curva</b></td>
                            <td colspan="2"></td>
                            <td><input type="number" id="<?= $ing[$z] ?>nummedxpuncurva" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'nummedxpuncurva'] ?>"
                                       <?= $disabled ?>/>
                            </td>
                            <td colspan="8"></td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>a8" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'a8'] ?>" disabled/>
                            </td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>c8" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'c8'] ?>" disabled/>
                            </td>
                        </tr>
                        <tr align="center">
                            <td><b>No. de mediciones de la curva (n)</b></td>
                            <td colspan="2"></td>
                            <td><input type="number" id="<?= $ing[$z] ?>nummedcurva" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'nummedcurva'] ?>" disabled/>
                            </td>
                            <td colspan="8"></td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>a9" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'a9'] ?>" disabled/>
                            </td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>c9" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'c9'] ?>" disabled/>
                            </td>
                        </tr>
                        <tr align="center">
                            <td><b>Concentraci&oacute;n de la muestra 1 (C0)</b></td>
                            <td colspan="2"></td>
                            <td><input type="number" id="<?= $ing[$z] ?>conmuestra1" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'conmuestra1'] ?>"/>
                            </td>
                            <td colspan="8"></td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>a10" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'a10'] ?>" disabled/>
                            </td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>c10" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'c10'] ?>" disabled/>
                            </td>
                        </tr>
                        <tr align="center">
                            <td><b>Concentraci&oacute;n de la muestra 2 (C0)</b></td>
                            <td colspan="2"></td>
                            <td><input type="number" id="<?= $ing[$z] ?>conmuestra2" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'conmuestra2'] ?>"/>
                            </td>
                            <td colspan="8"></td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>a11" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'a11'] ?>" disabled/>
                            </td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>c11" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'c11'] ?>" disabled/>
                            </td>
                        </tr>
                        <tr align="center">
                            <td><b>Prom. de los patrones de calibraci&oacute;n ( C )</b></td>
                            <td colspan="2"></td>
                            <td><input type="number" id="<?= $ing[$z] ?>prompatcalibracion" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'prompatcalibracion'] ?>"
                                       disabled/></td>
                            <td colspan="8"></td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>a12" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'a12'] ?>" disabled/>
                            </td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>c12" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'c12'] ?>" disabled/>
                            </td>
                        </tr>
                        <tr align="center">
                            <td><b>Desviaci&oacute;n est&aacute;ndar de residuales (S)</b></td>
                            <td colspan="2"></td>
                            <td><input type="number" id="<?= $ing[$z] ?>desvestresidual" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'desvestresidual'] ?>"
                                       disabled/></td>
                            <td colspan="8"></td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>a13" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'a13'] ?>" disabled/>
                            </td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>c13" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'c13'] ?>" disabled/>
                            </td>
                        </tr>
                        <tr align="center">
                            <td><b>Desv. est. de la curva de calibraci&oacute;n (Sxx)</b></td>
                            <td colspan="2"></td>
                            <td><input type="number" id="<?= $ing[$z] ?>desvestcurvacali" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'desvestcurvacali'] ?>"
                                       disabled/></td>
                            <td colspan="8"></td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>a14" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'a14'] ?>" disabled/>
                            </td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>c14" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'c14'] ?>" disabled/>
                            </td>
                        </tr>
                        <tr align="center">
                            <td colspan="2"></td>
                            <td><b>Datos est&aacute;ndar</b></td>
                            <td></td>
                            <td colspan="8"></td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>a15" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'a15'] ?>" disabled/>
                            </td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>c15" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'c15'] ?>" disabled/>
                            </td>
                        </tr>
                        <tr align="center">
                            <td colspan="2"></td>
                            <td><b>NP</b></td>
                            <td>
                                <input type="text" id="<?= $ing[$z] ?>np" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'np'] ?>"/>
                            </td>
                            <td colspan="8"></td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>a16" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'a16'] ?>" disabled/>
                            </td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>c16" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'c16'] ?>" disabled/>
                            </td>
                        </tr>
                        <tr align="center">
                            <td colspan="2"></td>
                            <td><b>Lote</b></td>
                            <td>
                                <input type="text" id="<?= $ing[$z] ?>lote" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'lote'] ?>"/>
                            </td>
                            <td colspan="8"></td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>a17" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'a17'] ?>" disabled/>
                            </td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>c17" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'c17'] ?>" disabled/>
                            </td>
                        </tr>
                        <tr align="center">
                            <td colspan="2"></td>
                            <td><b>Vence</b></td>
                            <td>
                                <input type="text" id="<?= $ing[$z] ?>vence" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'vence'] ?>"/>
                            </td>
                            <td colspan="8"></td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>a18" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'a18'] ?>" disabled/>
                            </td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>c18" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'c18'] ?>" disabled/>
                            </td>
                        </tr>
                        <tr align="center">
                            <td colspan="12"></td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>a19" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'a19'] ?>" disabled/>
                            </td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>c19" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'c19'] ?>" disabled/>
                            </td>
                        </tr>
                        <tr align="center">
                            <td colspan="12"></td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>a20" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'a20'] ?>" disabled/>
                            </td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>c20" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'c20'] ?>" disabled/>
                            </td>
                        </tr>
                        <tr align="center">
                            <td colspan="6"></td>
                            <td rowspan="7" colspan="9">
                                <table class="radius">
                                    <tr>
                                        <td colspan="8" align="center">Diluciones</td>
                                    </tr>
                                    <tr>
                                        <td>Pureza Est&aacute;ndar</td>
                                        <td></td>
                                        <td></td>
                                        <td>Est&aacute;ndar</td>
                                        <td>Pipeta</td>
                                        <td>Vol Corr Pipeta</td>
                                        <td>INC pip</td>
                                        <td>Bal&oacute;n</td>
                                        <td>Inc</td>
                                    </tr>
                                    <tr>
                                        <td>Valor</td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>valor" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'valor'] ?>"/>
                                        </td>
                                        <td>mg/L</td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>estandar1" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'estandar1'] ?>"/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>pipeta1" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'pipeta1'] ?>"/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>volcorrpipeta1" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0"
                                                   value="<?= $ROW[0][$ing[$z] . 'volcorrpipeta1'] ?>" disabled/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>incpip1" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'incpip1'] ?>"
                                                   disabled/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>balon1" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'balon1'] ?>"
                                                   disabled/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>dilinc1" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'inc1'] ?>"
                                                   disabled/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Inc exp</td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>incexp" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'incexp'] ?>"/>
                                        </td>
                                        <td>mg/L</td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>estandar2" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'estandar2'] ?>"/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>pipeta2" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'pipeta2'] ?>"/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>volcorrpipeta2" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0"
                                                   value="<?= $ROW[0][$ing[$z] . 'volcorrpipeta2'] ?>" disabled/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>incpip2" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'incpip2'] ?>"/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>balon2" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'balon2'] ?>"/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>dilinc2" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'inc2'] ?>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>k</td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>k" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'k'] ?>"/>
                                        </td>
                                        <td></td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>estandar3" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'estandar3'] ?>"/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>pipeta3" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'pipeta3'] ?>"/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>volcorrpipeta3" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0"
                                                   value="<?= $ROW[0][$ing[$z] . 'volcorrpipeta3'] ?>" disabled/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>incpip3" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'incpip3'] ?>"/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>balon3" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'balon3'] ?>"/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>dilinc3" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'inc3'] ?>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Inc est</td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>incest" class="monto" readonly
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'incest'] ?>"/>
                                        </td>
                                        <td></td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>estandar4" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'estandar4'] ?>"/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>pipeta4" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'pipeta4'] ?>"/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>volcorrpipeta4" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0"
                                                   value="<?= $ROW[0][$ing[$z] . 'volcorrpipeta4'] ?>" disabled/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>incpip4" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'incpip4'] ?>"/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>balon4" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'balon4'] ?>"/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>dilinc4" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'inc4'] ?>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>estandar5" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'estandar5'] ?>"/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>pipeta5" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'pipeta5'] ?>"/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>volcorrpipeta5" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0"
                                                   value="<?= $ROW[0][$ing[$z] . 'volcorrpipeta5'] ?>" disabled/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>incpip5" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'incpip5'] ?>"/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>balon5" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'balon5'] ?>"/>
                                        </td>
                                        <td>
                                            <input type="number" id="<?= $ing[$z] ?>dilinc5" class="monto"
                                                   onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                                   step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'inc5'] ?>"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr align="center">
                            <td colspan="6"></td>
                        </tr>
                        <tr align="center">
                            <td colspan="3"></td>
                            <td>CC</td>
                            <td>Est</td>
                            <td>CC+Est</td>
                        </tr>
                        <tr align="center">
                            <td>Incertidumbre por CC y est&aacute;ndar 1</td>
                            <td colspan="2"></td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>cc1" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'cc1'] ?>" disabled/>
                            </td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>est1" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'est1'] ?>" disabled/>
                            </td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>ccest1" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'ccest1'] ?>" disabled/>
                            </td>
                        </tr>
                        <tr align="center">
                            <td>Incertidumbre por CC y est&aacute;ndar 2</td>
                            <td colspan="2"></td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>cc2" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'cc2'] ?>" disabled/>
                            </td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>est2" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'est2'] ?>" disabled/>
                            </td>
                            <td>
                                <input type="number" id="<?= $ing[$z] ?>ccest2" class="monto"
                                       onblur="Redondear(this, '<?= $ing[$z] ?>')"
                                       step="0.1" min="0" value="<?= $ROW[0][$ing[$z] . 'ccest2'] ?>" disabled/>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table width="100%" style="font-size:10px;">
                        <tr>
                            <td>Resumen<br><br></td>
                        </tr>
                        <tr>
                            <td><b>Estad&iacute;sticas de la regresi&oacute;n</b></td>
                        </tr>
                        <tr>
                            <td>Coeficiente de correlaci&oacute;n m&uacute;ltiple</td>
                            <td>0,99986734484807</td>
                        </tr>
                        <tr>
                            <td>Coeficiente de determinaci&oacute;n R<sup>2</sup></td>
                            <td>0,999734707293529</td>
                        </tr>
                        <tr>
                            <td>R<sup>2</sup> ajustado</td>
                            <td>0,999719968809836</td>
                        </tr>
                        <tr>
                            <td>Error t&iacute;pico</td>
                            <td>0,00442194920212979</td>
                        </tr>
                        <tr>
                            <td>Observaciones</td>
                            <td>20</td>
                        </tr>
                        <tr>
                            <td><br><br><b>An&aacute;lisis de varianza</b></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Grados de libertad</td>
                            <td>Suma de cuadrados</td>
                            <td>Promedio de los cuadrados</td>
                            <td>F</td>
                            <td>Valor cr&iacute;tico de F</td>
                        </tr>
                        <tr>
                            <td>Regresi&oacute;n</td>
                            <td>1</td>
                            <td>1,32635403457457</td>
                            <td>1,32635403457457</td>
                            <td>67831,5848582178</td>
                            <td>1,20740975097279E-33</td>
                        </tr>
                        <tr>
                            <td>Residuos</td>
                            <td>18</td>
                            <td>0,000351965425431894</td>
                            <td>0,0000195536347462163</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Total</td>
                            <td>19</td>
                            <td>1,326706</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><br><br></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Coeficientes</td>
                            <td>Error t&iacute;pico</td>
                            <td>Estad&iacute;stico t</td>
                            <td>Probabilidad</td>
                            <td>Inferior 95%</td>
                            <td>Superior 95%</td>
                            <td>Inferior 95.0%</td>
                            <td>Superior 95.0%</td>
                        </tr>
                        <tr>
                            <td>Intercepci&oacute;n</td>
                            <td>0,000295760103396547</td>
                            <td>0,00171108653382409</td>
                            <td>0,172849296368171</td>
                            <td>0,864698652584455</td>
                            <td>-0,00329909930827413</td>
                            <td>0,00389061951506722</td>
                            <td>-0,00329909930827413</td>
                            <td>0,00389061951506722</td>
                        </tr>
                        <tr>
                            <td>Variable X 1</td>
                            <td>0,0911311693526688</td>
                            <td>0,0003499056545428</td>
                            <td>260,444974722527</td>
                            <td>1,20740975097279E-33</td>
                            <td>0,0903960448510348</td>
                            <td>0,0918662938543027</td>
                            <td>0,0903960448510348</td>
                            <td>0,0918662938543027</td>
                        </tr>
                        <tr>
                            <td><br><br><b>An&aacute;lisis de los residuales</b></td>
                        </tr>
                        <tr>
                            <td>Observaci&oacute;n</td>
                            <td>Pron&oacute;stico para Y</td>
                            <td>Residuos</td>
                            <td>Cuadrados</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>0,000295760103396547</td>
                            <td>-0,000295760103396547</td>
                            <td>8,74740387611364E-08</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>0,000295760103396547</td>
                            <td>-0,000295760103396547</td>
                            <td>8,74740387611364E-08</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>0,000295760103396547</td>
                            <td>-0,000295760103396547</td>
                            <td>8,74740387611364E-08</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>0,000295760103396547</td>
                            <td>-0,000295760103396547</td>
                            <td>8,74740387611364E-08</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>0,18084719312219</td>
                            <td>0,00415280687780981</td>
                            <td>0,0000172458049643845</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>0,18084719312219</td>
                            <td>0,00515280687780981</td>
                            <td>0,0000265514187200041</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>0,18084719312219</td>
                            <td>0,00415280687780981</td>
                            <td>0,0000172458049643845</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>0,18084719312219</td>
                            <td>0,00415280687780981</td>
                            <td>0,0000172458049643845</td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>0,364552083559679</td>
                            <td>-0,0075520835596789</td>
                            <td>0,0000570339660923723</td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>0,364552083559679</td>
                            <td>-0,0085520835596789</td>
                            <td>0,0000731381332117302</td>
                        </tr>
                        <tr>
                            <td>11</td>
                            <td>0,364552083559679</td>
                            <td>-0,0065520835596789</td>
                            <td>0,0000429297989730145</td>
                        </tr>
                        <tr>
                            <td>12</td>
                            <td>0,364552083559679</td>
                            <td>-0,0075520835596789</td>
                            <td>0,0000570339660923723</td>
                        </tr>
                        <tr>
                            <td>13</td>
                            <td>0,546218652245373</td>
                            <td>0,00278134775462668</td>
                            <td>0,0000077358953321669</td>
                        </tr>
                        <tr>
                            <td>14</td>
                            <td>0,546218652245373</td>
                            <td>0,00378134775462668</td>
                            <td>0,0000142985908414203</td>
                        </tr>
                        <tr>
                            <td>15</td>
                            <td>0,546218652245373</td>
                            <td>0,00378134775462668</td>
                            <td>0,0000142985908414203</td>
                        </tr>
                        <tr>
                            <td>16</td>
                            <td>0,546218652245373</td>
                            <td>0,00178134775462668</td>
                            <td>3,17319982291353E-06</td>
                        </tr>
                        <tr>
                            <td>17</td>
                            <td>0,728086310969361</td>
                            <td>0,000913689030638598</td>
                            <td>8,34827644709301E-07</td>
                        </tr>
                        <tr>
                            <td>18</td>
                            <td>0,728086310969361</td>
                            <td>-0,0010863109693614</td>
                            <td>1,18007152215491E-06</td>
                        </tr>
                        <tr>
                            <td>19</td>
                            <td>0,728086310969361</td>
                            <td>0,000913689030638598</td>
                            <td>8,34827644709301E-07</td>
                        </tr>
                        <tr>
                            <td>20</td>
                            <td>0,728086310969361</td>
                            <td>0,000913689030638598</td>
                            <td>8,34827644709301E-07</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    <?php } ?>
    <br/>
    <br/>
    <?php if ($_GET['acc'] == 'V') { ?>
        <script>__calcula();</script>
    <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php }else{ ?>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('H0019', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>