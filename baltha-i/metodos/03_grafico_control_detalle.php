<?php
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_grafico_control_detalle();
$ROW = $Gestor->GrafControllMuestraEnc();
$_id = $ROW[0]['id'];
$ROW2 = $Gestor->GrafControllMuestraDet($_id);
$ROW3 = $Gestor->GrafControllMuestraObs($_id);
$CVHorwitz = pow(2, (1 - (0.5 * log10(($_GET['D'])/100))))*0.67;
$SHorwitz = $CVHorwitz / 100 * $_GET['D'];
$lm = $SHorwitz;
$lb = $lm * 0.67;
//$la = $lb * 2;
$la = $lb * 2;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('graficador', 'gfx'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('', 'hr', 'Servicios :: Gr&aacute;fico Control') ?>
<center>
    <table class="radius" style="font-size:12px" width="75%">
        <tr>
            <td><strong>Ingrediente Activo:</strong></td>
            <td><?= $_GET['I'] ?></td>
            <td><strong>Concentraci&oacute;n Declarada:</strong></td>
            <td><?= $_GET['D'] ?></td>
        </tr>
        <tr>
            <td><strong>Formulaci&oacute;n:</strong></td>
            <td colspan="3"><?= utf8_decode($ROW[0]['formulacion']) ?></td>
        </tr>
        <tr>
            <td><strong>CV Horwitz:</strong></td>
            <td><?= number_format($CVHorwitz, 3) ?></td>
            <td><strong>S Horwitz:</strong></td>
            <td><?= number_format($SHorwitz, 3) ?></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="75%">
        <tr>
            <td align="center"><strong>CONTROL N&Uacute;MERO</strong></td>
            <td align="center"><strong>FECHA DEL AN�LISIS</strong></td>
            <td align="center"><strong>ANALISTA</strong></td>
            <td align="center"><strong>MUESTRA</strong></td>
            <td align="center"><strong>R�PLICA 1(% I.A.)</strong></td>
            <td align="center"><strong>R�PLICA 2(% I.A.)</strong></td>
            <td align="center"><strong>PROMEDIO(% I.A.)</strong></td>
            <td align="center"><strong>DESV. EST(% I.A.)</strong></td>
        </tr>
        <?php for ($i = 0; $i < count($ROW2); $i++) {
            $prom = ($ROW2[$i]['replica1'] + $ROW2[$i]['replica2']) / 2;
            $desv = sqrt(pow(abs($ROW2[$i]['replica1'] - $ROW2[$i]['replica2']), 2) / 2);
            ?>
            <tr>
                <td align="center"><?= $i + 1 ?></td>
                <td align="center"><?= $ROW2[$i]['fecha'] ?></td>
                <td align="center"><?= $ROW2[$i]['analista'] ?></td>
                <td align="center"><?= $ROW2[$i]['muestra'] ?></td>
                <td align="center"><?= $ROW2[$i]['replica1'] ?></td>
                <td align="center"><?= $ROW2[$i]['replica2'] ?></td>
                <td align="center"><?= number_format($prom, 4) ?></td>
                <td align="center"><?= number_format($desv, 4) ?></td>
            </tr>
        <?php } ?>
    </table>
    <br/>
    <table width="75%">
        <tr>
            <td align="center">
                <input type="button" value="Graficar" class="boton"
                       onClick="GeneraGrafico('<?= $_GET['I'] ?>', '<?= $la ?>', '<?= $lm ?>', '<?= $lb ?>')">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="button" value="Ver D&oacute;cimas" class="boton" onClick="Docimas()">
            </td>
        </tr>
        <tr style="display:none;">
            <td id="td_tabla">
                <table id="datatable" style="display:none;">
                    <thead>
                    <tr>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php for ($i = 0; $i < count($ROW2); $i++) {
                        $desv = sqrt(pow(abs($ROW2[$i]['replica1'] - $ROW2[$i]['replica2']), 2) / 4);
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td><?= $desv?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr align="center" id="tr_grafico" style="display:none;">
            <td>
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="75%">
        <tr>
            <td align="center" colspan="2"><strong>OBSERVACIONES</strong></td>
        </tr>
        <tr>
            <td align="center"><strong>N&Uacute;MERO</strong></td>
            <td align="center"><strong>DETALLE</strong></td>
        </tr>
        <?php for ($i = 0; $i < count($ROW3); $i++) { ?>
            <tr>
                <td align="center"><?= $i + 1 ?></td>
                <td align="center"><?= $ROW3[$i]['observacion'] ?></td>
            </tr>
        <?php } ?>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="75%">
        <tr>
            <td class="titulo" colspan="8">Observaciones</td>
        </tr>
        <tr>
            <td colspan="8"><textarea style="width:98%" id="observacion"></textarea></td>
        </tr>
    </table>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos('<?= $_id ?>')">
</center>
<script>GeneraGrafico('<?=$_GET['I']?>', '<?=$la?>', '<?=$lm?>', '<?=$lb?>');</script>
</body>
</html>