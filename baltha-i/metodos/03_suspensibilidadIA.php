<?php
//OK
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_suspensibilidadIA();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

if ($_GET['acc'] == 'V') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="xanalizar" value="<?= $_GET['xanalizar'] ?>"/>
<input type="hidden" id="tipo" value="<?= $_GET['tipo'] ?>"/>
<center>
    <?php $Gestor->Incluir('h19', 'hr', 'An&aacute;lisis :: Determinaci&oacute;n de Suspensibilidad en Ingrediente Activo por Cromatograf&iacute;a') ?>
    <?= $Gestor->Encabezado('H0019', 'e', 'Determinaci&oacute;n de Suspensibilidad en Ingrediente Activo por Cromatograf&iacute;a') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="3">Datos de la muestra</td>
        </tr>
        <tr>
            <td>N&uacute;mero:</td>
            <td><?= $ROW[0]['ref'] ?></td>
            <td>Ingrediente Activo:</td>
        </tr>
        <tr>
            <td>Fecha de ingreso:</td>
            <td><?= $ROW[0]['fechaI'] ?></td>
            <td><input type="text" id="ingrediente" maxlength="30" value="<?= $ROW[0]['ingrediente'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td>Fecha de an&aacute;lisis:</td>
            <td><input type="text" id="fechaA" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaA'] ?>" <?= $disabled ?>></td>
            <td>Tipo de formulaci&oacute;n:</td>
        </tr>
        <tr>
            <td>Fecha de conclusi&oacute;n del an&aacute;lisis:</td>
            <td><?= $ROW[0]['fechaC'] ?></td>
            <td><?= $ROW[0]['tipo_form'] ?><input type="hidden" id="tipo_form" value="<?= $ROW[0]['tipo_form'] ?>"/>
            </td>
        </tr>
        <tr>
            <td>Dosis recomendada:</td>
            <td><input type="text" id="dosis" size="10" value="<?= $ROW[0]['dosis'] ?>" <?= $disabled ?>></td>
            <td><?php if ($_GET['acc'] == 'V') { ?><strong>Creado por:</strong> <?= $ROW[0]['analista'] ?><?php } ?>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Datos del m&eacute;todo</td>
        </tr>
        <tr>
            <td>Temperatura del agua del ba&ntilde;o mar&iacute;a (�C):</td>
            <td><input type="text" id="maria" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['maria'] ?>"
                       <?= $disabled ?>/></td>
            <td>N&uacute;mero de reactivo agua dura 342ppm:</td>
            <td><input type="text" id="numreactivo" maxlength="30" value="<?= $ROW[0]['numreactivo'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td>Fecha de preparaci&oacute;n agua dura 342ppm:</td>
            <td><input type="text" id="fechaP" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaP'] ?>" <?= $disabled ?>></td>
            <td>Fecha de comprobaci&oacute;n agua dura 342ppm:</td>
            <td><input type="text" id="fechaD" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaD'] ?>" <?= $disabled ?>></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td>Resultado de la comprobaci&oacute;n agua dura 342ppm:</td>
            <td><input type="text" id="resultado" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['resultado'] ?>" <?= $disabled ?>/></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="6">Datos del an&aacute;lisis</td>
        </tr>
        <tr>
            <td>Origen y n&uacute;mero del est&aacute;ndar:</td>
            <td colspan="2"><input type="text" id="origen" maxlength="50" value="<?= $ROW[0]['origen'] ?>"
                                   <?= $disabled ?>/></td>
            <td colspan="3" rowspan="7">
                <table class="radius" width="100%">
                    <tr align="center">
                        <td><strong>R&eacute;plica</strong></td>
                        <td><strong>&Aacute;rea muestra</strong></td>
                        <td><strong>&Aacute;rea est&aacute;ndar</strong></td>
                    </tr>
                    <tr align="center">
                        <td>1</td>
                        <td><input type="text" id="1A" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['1A'] ?>" <?= $disabled ?>/></td>
                        <td><input type="text" id="1B" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['1B'] ?>" <?= $disabled ?>/></td>
                    </tr>
                    <tr align="center">
                        <td>2</td>
                        <td><input type="text" id="2A" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['2A'] ?>" <?= $disabled ?>/></td>
                        <td><input type="text" id="2B" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['2B'] ?>" <?= $disabled ?>/></td>
                    </tr>
                    <tr align="center">
                        <td><strong>Promedio</strong></td>
                        <td id="promA"></td>
                        <td id="promB"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>Masa del est&aacute;ndar (mg):</td>
            <td><input type="text" id="masa" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['masa'] ?>"
                       <?= $disabled ?>/></td>
            <td></td>
        </tr>
        <tr>
            <td>Pureza del est&aacute;ndar:</td>
            <td><input type="text" id="pureza" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['pureza'] ?>"
                       <?= $disabled ?>/></td>
            <td></td>
        </tr>
        <tr>
            <td>Existe certificado de pureza:</td>
            <td><select id="existe" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['existe'] == '0') echo 'selected'; ?>>No</option>
                    <option value="1" <?php if ($ROW[0]['existe'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select></td>
            <td></td>
        </tr>
        <tr>
            <td>Disolvente utilizado:</td>
            <td><input type="text" id="disol" value="<?= $ROW[0]['disol'] ?>" <?= $disabled ?>/></td>
            <td></td>
        </tr>
        <tr>
            <td>A. Masa de muestra (g):</td>
            <td><input type="text" id="muestra" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['muestra'] ?>"
                       <?= $disabled ?>/></td>
            <td></td>
        </tr>
        <tr>
            <td>B. % m/m de I.A. encontrado:</td>
            <td><input type="text" id="encontrado" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['encontrado'] ?>" <?= $disabled ?>/></td>
            <td></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="3">Resultados del an&aacute;lisis</td>
        </tr>
        <tr>
            <td rowspan="6">
                <table class="radius" width="100%">
                    <tr align="center">
                        <td></td>
                        <td><strong>Diluci&oacute;n del Est&aacute;ndar</strong></td>
                        <td><strong>Diluci&oacute;n de los 25 mL remanentes</strong></td>
                    </tr>
                    <tr align="center">
                        <td>Vol. Bal&oacute;n aforado 1 (mL)</td>
                        <td><input type="text" id="valA1" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['valA1'] ?>" <?= $disabled ?>/></td>
                        <td><input type="text" id="valB1" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['valB1'] ?>" <?= $disabled ?>/></td>
                    </tr>
                    <tr align="center">
                        <td>Vol. Primera al&iacute;cuota (mL)</td>
                        <td><input type="text" id="valA2" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['valA2'] ?>" <?= $disabled ?>/></td>
                        <td><input type="text" id="valB2" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['valB2'] ?>" <?= $disabled ?>/></td>
                    </tr>
                    <tr align="center">
                        <td>Vol. Bal&oacute;n aforado 2 (mL)</td>
                        <td><input type="text" id="valA3" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['valA3'] ?>" <?= $disabled ?>/></td>
                        <td><input type="text" id="valB3" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['valB3'] ?>" <?= $disabled ?>/></td>
                    </tr>
                    <tr align="center">
                        <td>Vol. Segunda al&iacute;cuota (mL)</td>
                        <td><input type="text" id="valA4" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['valA4'] ?>" <?= $disabled ?>/></td>
                        <td><input type="text" id="valB4" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['valB4'] ?>" <?= $disabled ?>/></td>
                    </tr>
                    <tr align="center">
                        <td>Vol. Bal&oacute;n aforado 3 (mL)</td>
                        <td><input type="text" id="valA5" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['valA5'] ?>" <?= $disabled ?>/></td>
                        <td><input type="text" id="valB5" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['valB5'] ?>" <?= $disabled ?>/></td>
                    </tr>
                </table>
            </td>
            <td>C. Masa de I.A en la muestra</td>
            <td id="res1"></td>
        </tr>
        <tr>
            <td>Q. Masa de I.A. en los 25 mL</td>
            <td id="res2"></td>
        </tr>
        <tr>
            <td>Relaci&oacute;n de &aacute;reas</td>
            <td id="res3"></td>
        </tr>
        <tr>
            <td>Conc. Est&aacute;ndar (mg/mL)</td>
            <td id="res4"></td>
        </tr>
        <tr>
            <td>Factor de diluci&oacute;n</td>
            <td id="res5"></td>
        </tr>
        <tr>
            <td><strong>% S.I.A</strong></td>
            <td id="res6" style="font-weight:bold"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo">Observaciones</td>
        </tr>
        <tr>
            <td><textarea id="obs" style="width:98%" <?= $disabled ?>><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>

        <tr>
            <td class="titulo" colspan="6">Formulas</td>
        </tr>
        <tr>
            <td colspan="6">
                <p><b>Determinaci&oacute;n de la suspensibilidad del ingrediente activo:</b></p>
                <p>
                    <img src="imagenes/H0019/f1.PNG" width="259" height="65" alt="f1"/>
                </p>
                <p><b>Q = Masa del ingrediente activo en la porci&oacute;n de 25 mL de la probeta de suspensibilidad</b>
                </p>
                <p><b>M&eacute;todo est&aacute;ndar externo:</b></p>
                <p style="text-align: center">
                    <img src="imagenes/H0019/f2.PNG" width="689" height="180" alt="f2"/>
                </p>
                <p><b>M&eacute;todo est&aacute;ndar interno:</b></p>
                <p style="text-align: center">
                    <img src="imagenes/H0019/f3.PNG" width="749" height="164" alt="f3"/>
                </p>

                <p><b>Donde:</b></p>
                <p>M est&aacute;ndar = Masa de est&aacute;ndar anal&iacute;tico pesada.</p>
                <p>P est&aacute;ndar = Pureza del est&aacute;ndar anal&iacute;tico.</p>
                <p>Vol mat 1, 2, 3 = Volumen de matraz de diluci&iacute;n correspondiente, ya sea de muestra o est&aacute;ndar.</p>
                <p>Alic 1, 2, 3 = Al&iacute;cuota tomada del bal&oacute;n correspondiente, para luego diluir.</p>
                <p>P &aacute;rea M = Promedio de 3 r&eacute;plicas de inyecci&oacute;n de muestra diluida.</p>
                <p>P &aacute;rea est = Promedio de 3 r&eacute;plicas de inyecci&oacute;n de est&aacute;ndar diluido.</p>
                <p>P &aacute;rea est int M = Promedio del &aacute;rea de est&a&aacute;ndar interno insertado en la
                    muestra diluida.</p>
                <p>P &aacute;rea est int est = Promedio del &aacute;rea de est&aacute;ndar interno insertado en el est&aacute;ndar.</p>
                <p><b>C= Masa de ingrediente activo en la muestra</b></p>
                <p style="text-align: center">
                    <img src="imagenes/H0019/f4.PNG" width="434" height="92" alt="f4"/>
                </p>

                <p><b>Donde:</b></p>
                <p>M muestra = Masa de muestra pesada para la prueba de suspensibilidad</p>
                <p>Cn ing act muestra = Concentraci&oacute;n de ingrediente activo encontrada en la muestra de
                    prueba. </p>

            </td>
        </tr>
    </table>
    <br/>
    <br/>
    <?php if ($_GET['acc'] == 'V') { ?>
        <script>__calcula();</script>
    <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php }else{ ?>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('H0019', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>