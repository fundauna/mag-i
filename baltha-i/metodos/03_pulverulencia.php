<?php
//OK
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_pulverulencia();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

if ($_GET['acc'] == 'V') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="xanalizar" value="<?= $_GET['xanalizar'] ?>"/>
<input type="hidden" id="tipo" value="<?= $_GET['tipo'] ?>"/>
<center>
    <?php $Gestor->Incluir('h15', 'hr', 'An&aacute;lisis :: Determinaci&oacute;n de Pulverulencia') ?>
    <?= $Gestor->Encabezado('H0015', 'e', 'Determinaci&oacute;n de Pulverulencia') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="3">Datos de la muestra</td>
        </tr>
        <tr>
            <td>N&uacute;mero:</td>
            <td><?= $ROW[0]['ref'] ?></td>
            <td style="display:none">Incrediente Activo:</td>
        </tr>
        <tr>
            <td>Fecha de ingreso:</td>
            <td><?= $ROW[0]['fechaI'] ?></td>
            <td style="display:none"><input type="hidden" id="ingrediente" maxlength="30" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td>Fecha de an&aacute;lisis:</td>
            <td><input type="text" id="fechaA" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaA'] ?>" <?= $disabled ?>></td>
            <td>Tipo de formulaci&oacute;n:</td>
        </tr>
        <tr>
            <td>Fecha de conclusi&oacute;n del an&aacute;lisis:</td>
            <td><?= $ROW[0]['fechaC'] ?></td>
            <td><?= $ROW[0]['tipo_form'] ?><input type="hidden" id="tipo_form" value="<?= $ROW[0]['tipo_form'] ?>"/>
            </td>
        </tr>
        <?php if ($_GET['acc'] == 'V') { ?>
            <tr>
                <td><strong>Creado por:</strong></td>
                <td colspan="2"><?= $ROW[0]['analista'] ?></td>
            </tr>
        <?php } ?>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="2">Datos de la an&aacute;lisis</td>
        </tr>
        <tr>
            <td>
                <!-- -->
                <table class="radius" width="100%">
                    <tr>
                        <td colspan="2"><strong>Densidad Aparente</strong></td>
                    </tr>
                    <tr>
                        <td>Masa (g):</td>
                        <td><input type="text" id="masa" class="monto" onblur="Redondear(this);densidadAparente();"
                                   value="<?= $ROW[0]['masa'] ?>" <?= $disabled ?>/></td>
                    </tr>
                    <tr>
                        <td>Volumen (mL):</td>
                        <td><input type="text" id="volumen" class="monto" onblur="Redondear(this);densidadAparente();"
                                   value="<?= $ROW[0]['volumen'] ?>" <?= $disabled ?>/></td>
                    </tr>
                    <tr>
                        <td><strong>Densidad Aparente (g/mL):</strong></td>
                        <td id="aparente"></td>
                    </tr>
                </table>
                <!-- -->
            </td>
            <td>
                <!-- -->
                <table class="radius" width="100%">
                    <tr align="center">
                        <td><strong>Densidad Aparente (g/mL)</strong></td>
                        <td><strong>Masa de muestra (g)</strong></td>
                    </tr>
                    <tr align="center">
                        <td>&rho; > 1,0</td>
                        <td>> 300</td>
                    </tr>
                    <tr align="center">
                        <td>0,7 < &rho; < 1,0</td>
                        <td>210 a 340</td>
                    </tr>
                    <tr align="center">
                        <td>0,4 < &rho; < 0,7</td>
                        <td>120 a 240</td>
                    </tr>
                    <tr align="center">
                        <td colspan="2"><font size="-1">Masa de muestra a tomar seg&uacute;n la densidad aparente</font>
                        </td>
                    </tr>
                </table>
                <!-- -->
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="3">Datos de la an&aacute;lisis</td>
        </tr>
        <tr align="center">
            <td></td>
            <td><strong>Masa (g)</strong></td>
            <td rowspan="2"><strong>Porcentaje de masa</strong></td>
        </tr>
        <tr align="center">
            <td>Masa Tamiz inicial</td>
            <td><input type="text" id="tamiz1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['tamiz1'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr align="center">
            <td>Tamiz de 850 mm</td>
            <td><input type="text" id="tamiz2" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['tamiz2'] ?>"
                       <?= $disabled ?>/></td>
            <td id="porc2"></td>
        </tr>
        <tr align="center">
            <td>Tamiz de 250 mm</td>
            <td><input type="text" id="tamiz3" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['tamiz3'] ?>"
                       <?= $disabled ?>/></td>
            <td id="porc3"></td>
        </tr>
        <tr align="center">
            <td>Tamiz de 150 mm</td>
            <td><input type="text" id="tamiz4" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['tamiz4'] ?>"
                       <?= $disabled ?>/></td>
            <td id="porc4"></td>
        </tr>
        <tr align="center">
            <td>Plato receptor</td>
            <td><input type="text" id="tamiz5" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['tamiz5'] ?>"
                       <?= $disabled ?>/></td>
            <td id="porc5"></td>
        </tr>
        <tr align="center">
            <td><strong>P&eacute;rdida</strong></td>
            <td id="perdida1"></td>
            <td id="perdida2"></td>
        </tr>
        <tr align="center">
            <td align="right" colspan="2"><strong>% Total</strong></td>
            <td align="center" id="total"></td>
        </tr>
        <tr>
            <td align="right" colspan="2"><strong>% Acumulativo</strong></td>
            <td align="center" id="acumulativo"></td>
        </tr>
        <tr>
            <td align="right" colspan="2"><strong>% Fracci&oacute;n Retenida en el Plato Receptor</strong></td>
            <td align="center" id="fraccion"></td>
        </tr>
        <tr>
            <td colspan="3">
                <hr/>
            </td>
        </tr>
        <tr>
            <td colspan="3">Nota 1. El l&iacute;mite m&aacute;ximo aceptado para el porcentaje acumulativo es igual a
                4,4 % m/m
            </td>
        </tr>
        <tr>
            <td colspan="3">Nota 2. No m&aacute;s de 1,1 % m/m podr&aacute; pasar al recipiente receptor (% Fracci&oacute;n
                retenida en el plato receptor).
            </td>
        </tr>
        <tr>
            <td colspan="3">Nota 3. La muestra debe cumplir con las dos especificaciones anteriores.</td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo">Observaciones</td>
        </tr>
        <tr>
            <td><textarea id="obs" style="width:98%" <?= $disabled ?>><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
        <tr>
            <td class="titulo" colspan="6">Formulas</td>
        </tr>
        <tr>
            <td colspan="6">
                <p><b>Determinaci&oacute;n de densidad aparente:</b></p>
                <p>Masa = Masa de muestra tomada para medir la densidad aparente</p>
                <p>Volumen = Volumen medido con una probeta de la masa medida de la muestra</p>
                <p style="text-align: center">
                    <img src="imagenes/H0015/f1.PNG" width="497" height="79" alt="f1"/>
                </p>
                <p>Prueba de tamiz</p>
                <p>Masa en tamiz inicial = Masa de muestra, tomada de acuerdo con la densidad aparente calculada:</p>
                <table border="1">
                    <thead>
                    <tr>
                        <th>Densidad Aparente (g/mL)</th>
                        <th>Masa de muestra (g)</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td> &#961;?> 1,0</td>
                        <td>> 300</td>
                    </tr>
                    <tr>
                        <td>0,7 < &#961;?< 1,0</td>
                        <td>210 a 340</td>
                    </tr>
                    <tr>
                        <td>0,4 < &#961;?< 0,7</td>
                        <td>120 a 240</td>
                    </tr>
                    </tbody>
                </table>
                <p>Tamiz de 850 um = Masa de muestra medida en tamiz de 850 um, luego del proceso de tamizado.</p>
                <p>Porcentaje de masa, tamiz de 850 um = </p>
                <p>Tamiz de 250 um = Masa de muestra medida en tamiz de 250 um, luego del proceso de tamizado.</p>
                <p>Tamiz de 150 um = Masa de muestra medida en tamiz de 150 um, luego del proceso de tamizado.</p>
                <p>Plato receptor = Masa de muestra obtenida del plato receptor final, luego del tamizado.</p>
                <p>Perdida = es la sumatoria de las masas de cada tamiz y el plato receptor, menos la masa inicial.</p>
                <p>% Acumulativo = es la sumatoria del porcentaje de masa del tamiz de 150 &#181;m, el plato receptor y
                    la perdida.</p>
                <p>% Fracci&oacute;n retenida en el plato receptor = es la sumatoria del porcentaje de masa del plato
                    receptor y la perdida.</p>

            </td>
        </tr>
    </table>
    <br/>
    <br/>
    <?php if ($_GET['acc'] == 'V') { ?>
        <script>densidadAparente();
            __calcula();</script>
    <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php }else{ ?>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('H0015', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>