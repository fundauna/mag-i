<?php
//OK
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_azufrecombustion();
$ROW = $Gestor->ObtieneDatos();
$ROW2 = $Gestor->ObtieneDatos2();
if (!$ROW) die('Registro inexistente');

if ($_GET['acc'] == 'V') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="xanalizar" value="<?= $_GET['xanalizar'] ?>"/>
<input type="hidden" id="tipo" value="<?= $_GET['tipo'] ?>"/>
<input type="hidden" id="ingrediente" value="<?= $ROW[0]['ingrediente'] ?>"/>
<center>
    <?php $Gestor->Incluir('h21', 'hr', 'An&aacute;lisis :: Determinaci&oacute;n de azufre total por el m&eacute;todo de combusti&oacute;n') ?>
    <?= $Gestor->Encabezado('H0021', 'e', 'Determinaci&oacute;n de azufre total por el m&eacute;todo de combusti&oacute;n') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="3">Datos de la muestra</td>
        </tr>
        <tr>
            <td><strong>N&uacute;mero:</strong> <?= $ROW[0]['ref'] ?></td>
            <td><strong>Ingrediente Activo:</strong> <?= $ROW[0]['ingrediente'] ?></td>
            <td><strong>Creado por:</strong> <?= $ROW[0]['analista'] ?></td>
        </tr>
        <tr>
            <td><strong>Fecha de ingreso:</strong> <?= $ROW[0]['fechaI'] ?></td>
            <td><strong>Fecha de an&aacute;lisis:</strong> <input type="text" id="fechaA" class="fecha" readonly
                                                                  onClick="show_calendar(this.id);"
                                                                  value="<?= $ROW[0]['fechaA'] ?>" <?= $disabled ?>>
            </td>
            <td><strong>Fecha de conclusi&oacute;n del an&aacute;lisis:</strong> <?= $ROW[0]['fechaC'] ?></td>
        </tr>
        <tr>
            <td colspan="3"><strong>Concentraci&oacute;n declarada:</strong> <input type="text" class="monto" id="rango"
                                                                                    value="<?= $ROW[0]['rango'] ?>"
                                                                                    <?= $disabled ?>></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px;" width="98%" >
        <tr>
            <td></td>
            <td align="center"><strong>Masa de muestra (mg)</strong></td>
            <td align="center"><strong>Error de medida relativo (%, certificado)</strong></td>
            <td align="center"><strong>Masa de muestra corregida (mg)</strong></td>
            <td align="center"><strong>Densidad</strong></td>
            <td align="center"><strong>Incertidumbre Densidad</strong></td>
        </tr>
        <tr>
            <td align="center">1.</td>
            <td align="center"><input type="text" name="masa" id="masa1" class="monto" onblur="Redondear(this,1)"
                                      value="<?= $ROW[0]['masa1'] ?>" <?= $disabled ?>></td>
            <td align="center"><input type="text" name="error1" id="error1" class="monto" onblur="Redondear(this,1)"
                                      value="<?= $ROW[0]['error1'] ?>" <?= $disabled ?>></td>
            <td align="center" id="corregida1"></td>
            <td align="center"><input type="text" name="densidad" id="densidad" class="monto" onblur="Redondear(this,2)"
                                      value="<?= $ROW[0]['densidad'] ?>" <?= $disabled ?>></td>

            <td align="center" id="ixd" </td>

        </tr>
        <tr>
            <td align="center">2.</td>
            <td align="center"><input type="text" name="masa" id="masa2" class="monto" onblur="Redondear(this,1)"
                                      value="<?= $ROW[0]['masa2'] ?>" <?= $disabled ?>></td>
            <td align="center"><input type="text" name="error2" id="error2" class="monto" onblur="Redondear(this,1)"
                                      value="<?= $ROW[0]['error2'] ?>" <?= $disabled ?>></td>
            <td align="center" id="corregida2"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>

            <td align="center">3.</td>
            <td align="center"><input type="text" name="masa" id="masa3" class="monto" onblur="Redondear(this,1)"
                                      value="<?= isset($ROW[0]['masa3']) == true ? $ROW[0]['masa3'] : '' ?>" <?= $disabled ?>></td>
            <td align="center"><input type="text" name="error3" id="error3" class="monto" onblur="Redondear(this,1)"
                                      value="<?= $ROW[0]['error3'] ?>" <?= $disabled ?>></td>
            <td align="center" id="corregida3"></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px;" width="98%" >
        <tr>
            <td></td>
            <td align="center"><strong>Masa de <br/>azufre (mg)</strong></td>
            <td align="center"><strong>Masa de <br/>muestra corregida (mg)</strong></td>
            <td align="center"><strong>% m/m</strong></td>
            <td align="center"><strong>Inc. C&aacute;lculo <br/> m/m</strong></td>
            <td align="center"><strong>DE</strong></td>
            <td align="center"><strong>Inc. por <br/>DE</strong></td>
            <td align="center"><strong>% m/v</strong></td>
            <td align="center"><strong>Inc. C&aacute;lculo<br/>m/v</strong></td>
            <td align="center"><strong>DE</strong></td>
            <td align="center"><strong>Inc. por<br/> DE</strong></td>
        </tr>
        <tr>
            <td align="center">1.</td>
            <td align="center"><input type="text" id="masaA1" class="monto" onblur="Redondear(this,2)"
                                      value="<?= $ROW[0]['masaA1'] ?>" <?= $disabled ?>></td>
            <td align="center" id="mmuestra1"></td>
            <td align="center" id="mm1"></td>
            <td align="center" id="incmm1"></td>
            <td align="center" id="de11"></td>
            <td align="center" id="incde11"></td>
            <td align="center" id="mv1"></td>
            <td align="center" id="incmv1"></td>
            <td align="center" id="de21"></td>
            <td align="center" id="incde21"></td>
        </tr>
        <tr>
            <td align="center">2.</td>
            <td align="center"><input type="text" id="masaA2" class="monto" onblur="Redondear(this,2)"
                                      value="<?= $ROW[0]['masaA2'] ?>" <?= $disabled ?>></td>
            <td align="center" id="mmuestra2"></td>
            <td align="center" id="mm2"></td>
            <td align="center" id="incmm2"></td>
            <td align="center" id="de12"></td>
            <td align="center" id="incde12"></td>
            <td align="center" id="mv2"></td>
            <td align="center" id="incmv2"></td>
            <td align="center" id="de22"></td>
            <td align="center" id="incde22"></td>
        </tr>
        <tr>
            <td align="center">3.</td>
            <td align="center"><input type="text" id="masaA3" class="monto" onblur="Redondear(this,2)"
                                      value="<?= isset($ROW[0]['masaA3']) == true ? $ROW[0]['masaA3']: ''  ?>" <?= $disabled ?>></td>

            <td align="center" id="mmuestra3"></td>
            <td align="center" id="mm3"></td>
            <td align="center" id="incmm3"></td>
            <td align="center" id="de13"></td>
            <td align="center" id="incde13"></td>
            <td align="center" id="mv3"></td>
            <td align="center" id="incmv3"></td>
            <td align="center" id="de23"></td>
            <td align="center" id="incde23"></td>
        </tr>
        <tr>
            <td class="titulo" colspan="12">Promedio</td>
        </tr>
        <tr>
            <td colspan="2"></td>


            <td align="center" colspan="2"><strong>%m/m Azufre</strong></td>

            <td align="center" colspan="2"><strong>Inc. combinada</strong></td>


            <td align="center" colspan="2"><strong>%m/v Azufre</strong></td>
            <td align="center" colspan="2"><strong>Inc. combinada</strong></td>

            <td colspan="3"></td>


        </tr>
        <tr>
            <td colspan="2"></td>

            <td align="center" id="mmazufre" colspan="2"></td>
            <td align="center" id="incc1" colspan="2"></td>

            <td align="center" id="mvazufre"colspan="2"></td>
            <td align="center" id="incc2"colspan="2" ></td>

            <td colspan="3"></td>

        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px;" width="98%">
        <tr>
            <td class="titulo" colspan="9">Componentes de incertidumbre</td>
        </tr>
        <tr>
            <td align="center"><strong>Lectura de Equipo</strong></td>
            <td align="center"></td>
            <td align="center"><strong>Componente</strong></td>
            <td align="center"><strong>Masa de Muestra</strong></td>
            <td align="center"></td>
            <td align="center"><strong>Componente</strong></td>
            <td align="center"><strong>Incertidumbre por Densidad</strong></td>
            <td align="center"></td>
            <td align="center"><strong>Componente</strong></td>

        </tr>
        <tr>
            <td align="center">Repetibilidad</td>
            <td align="center"><input type="text" id="rep1" class="monto" onblur=" Redondear(this,3), Redondear(this,2)"
                                      value="<?= $ROW[0]['rep1'] ?>0.15" <?= $disabled ?>></td>
            <td align="center" id="rep11"></td>
            <td align="center">Repetibilidad</td>
            <td align="center"><input type="text" id="rep2" class="monto" onblur="__Redondear(this,3), Redondear(this,2)"
                                      value="<?= $ROW[0]['rep2'] ?>0.000014" <?= $disabled ?>></td>
            <td align="center" id="rep21"></td>

            <td align="center">Inc. de Certificado</td>
            <td align="center"><input type="text" id="incCertificado" class="monto" onblur="Redondear(this,3), Redondear(this,2)"
                                      value="<?= isset($ROW[0]['res_incCertificado']) == true ? $ROW[0]['res_incCertificado'] : 0.0008 ?>" <?= $disabled ?>></td>
            <td align="center" id="res_incCertificado"></td>
        </tr>
        <tr>
            <td align="center">Resoluci&oacute;n</td>
            <td align="center"><input type="text" id="resol1" class="monto" onblur="Redondear(this,3), Redondear(this,2)"
                                      value="<?= $ROW[0]['resol1'] ?>0.001" <?= $disabled ?>></td>
            <td align="center" id="resol11"></td>
            <td align="center">Resoluci&oacute;n</td>
            <td align="center"><input type="text" id="resol2" class="monto" onblur="Redondear(this,3), Redondear(this,2)"
                                      value="<?= $ROW[0]['resol2'] ?>0.00001" <?= $disabled ?>></td>
            <td align="center" id="resol21"></td>

            <td align="center">Resoluci&oacute;n</td>
            <td align="center"><input type="text" id="incResolucion" class="monto" onblur="Redondear(this), Redondear(this,2)"
                                      value="<?= isset($ROW[0]['res_incResolucion']) == true ? $ROW[0]['res_incResolucion'] : 0.001 ?>" <?= $disabled ?>></td>
            <td align="center" id="res_incResolucion"></td>

        </tr>

        <tr>
            <td align="center">Curva de calibraci&oacute;n</td>
            <td align="center"><input type="text" id="ccur" class="monto" onblur="Redondear(this,3), Redondear(this,2)"
                                      value="<?= $ROW[0]['ccur'] ?>4.754617 " <?= $disabled ?>></td>
            <td align="center" id="ccur1"></td>

             <td align="center">Inc. Linealidad</td>
            <td align="center"><input type="text" id="incLin" class="monto" onblur="Redondear(this,3);Redondear(this,2)"
                                      value=" <?= isset($ROW[0]['incLin1']) == true ? $ROW[0]['incLin1'] : 0.000073 ?> " <?= $disabled ?>></td>
            <td align="center" id="incLin1"></td>


        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td align="center">Inc. excentricidad</td>
            <td align="center"><input type="text" id="incex" class="monto" onblur="Redondear(this,3);Redondear(this,3)"
                                      value="<?= $ROW[0]['incex'] ?>0.000049" <?= $disabled ?>></td>
            <td align="center" id="incex1"></td>
        </tr>
        <tr>
            <td align="center">IC equipo</td>
            <td align="center"></td>
            <td align="center" id="icequipo1"></td>
            <td align="center">IC balanza</td>
            <td align="center"></td>
            <td align="center" id="icbalanza1"></td>
            <td align="center">IC Combinada</td>
            <td align="center"></td>
            <td align="center" id="res_incCombinada"></td>
        </tr>

        <tr>
            <td class="titulo" colspan="10">Formulas</td>
        </tr>
        <tr>
            <td colspan="6">
                <p style="text-align: center;"><img src="imagenes/H0021/f1.PNG" width="192" height="137" alt="f1"/></p>
                <p><b>Donde:</b></p>
                <p>m<sub>S</sub>: masa de azufre identificada en el equipo en miligramos (mg).</p>
                <p>m: masa de muestra en mg.</p>
                <p>&#961; : densidad en g/mL</p>
                <p>% m / m = g / 100 g </p>
                <p>% m / v = g / 100 mL </p>
            </td>
        </tr>
    </table>
    <br/>
    <br/>
    <?php if ($_GET['acc'] == 'V') { ?>
        <script>
            __calcula();
            __calcula2();
            __calcula3();

        </script>
    <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php }else{ ?>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('H0021', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>