<?php
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_grafico_control_docimas();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('', 'hr', 'Servicios :: D&oacute;cimas Gr&aacute;fico Control') ?>
<center>
    <table class="radius" style="font-size:12px" width="75%">
        <tr>
            <td><img src="../../caspha-i/imagenes/docimas/1.png"/></td>
            <td><img src="../../caspha-i/imagenes/docimas/2.png"/></td>
        </tr>
        <tr>
            <td><img src="../../caspha-i/imagenes/docimas/3.png"/></td>
            <td><img src="../../caspha-i/imagenes/docimas/4.png"/></td>
        </tr>
        <tr>
            <td><img src="../../caspha-i/imagenes/docimas/5.png"/></td>
            <td><img src="../../caspha-i/imagenes/docimas/6.png"/></td>
        </tr>
        <tr>
            <td><img src="../../caspha-i/imagenes/docimas/7.png"/></td>
            <td><img src="../../caspha-i/imagenes/docimas/8.png"/></td>
        </tr>
    </table>
</center>
</body>
</html>