<?php
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_cromaN();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

if ($_GET['acc'] == 'V') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="xanalizar" value="<?= $_GET['xanalizar'] ?>"/>
<input type="hidden" id="tipo" value="<?= $_GET['tipo'] ?>"/>
<center>
    <?php $Gestor->Incluir('h3', 'hr', 'An&aacute;lisis :: Determinaci&oacute;n de Ingrediente Activo en Formulaciones de Plaguicidas por Cromatograf&iacute;a') ?>
    <?= $Gestor->Encabezado('H0003', 'e', 'Determinaci&oacute;n de Ingrediente Activo en Formulaciones de Plaguicidas por Cromatograf&iacute;a') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="3">Datos de la muestra</td>
        </tr>
        <tr>
            <td><strong>N&uacute;mero:</strong></td>
            <td><?= $ROW[0]['ref'] ?></td>
            <td><strong>Ingrediente Activo:</strong></td>
        </tr>
        <tr>
            <td><strong>Fecha de ingreso:</strong></td>
            <td><?= $ROW[0]['fechaI'] ?></td>
            <td><?= $ROW[0]['ingrediente'] ?><input type="hidden" id="ingrediente" maxlength="30"
                                                    value="<?= $ROW[0]['ingrediente'] ?>" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>Fecha de an&aacute;lisis:</strong></td>
            <td><input type="text" id="fechaA" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaA'] ?>" <?= $disabled ?>></td>
            <td><strong>Tipo de formulaci&oacute;n:</strong></td>
        </tr>
        <tr>
            <td><strong>Fecha de conclusi&oacute;n del an&aacute;lisis:</strong></td>
            <td><?= $ROW[0]['fechaC'] ?></td>
            <td><?= $ROW[0]['tipo_form'] ?><input type="hidden" id="tipo_form" value="<?= $ROW[0]['tipo_form'] ?>"/>
            </td>
        </tr>
        <tr>
            <td><strong>Concentraci&oacute;n declarada:</strong></td>
            <td><input type="text" id="rango" maxlength="20" class="monto" value="<?= $ROW[0]['rango'] ?>"
                       <?= $disabled ?>>&nbsp;
                <select id="unidad" <?php if ($ROW[0]['unidad'] != '' or $_GET['acc'] == 'V') echo 'disabled'; ?>
                        onchange="__calcula()">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['unidad'] == '0') echo 'selected'; ?>>%m/m</option>
                    <option value="1" <?php if ($ROW[0]['unidad'] == '1') echo 'selected'; ?>>%m/v</option>
                </select>
            </td>
            <td><?php if ($_GET['acc'] == 'V') { ?><strong>Creado por:</strong> <?= $ROW[0]['analista'] ?><?php } ?>
            </td>
        </tr>
        <tr>
            <td><strong>Densidad de la muestra, &rho; (g/mL):</strong></td>
            <td colspan="2"><input type="text" id="densidad" class="monto" value="<?= $ROW[0]['densidad'] ?>"
                                   <?= $disabled ?> onblur="Redondear(this)"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Datos del m&eacute;todo</td>
        </tr>
        <tr>
            <td><strong>Referencia del m&eacute;todo:</strong></td>
            <td><input type="text" id="metodo" maxlength="50" value="<?= $ROW[0]['metodo'] ?>" <?= $disabled ?>/></td>
            <td><strong>Disolvente de la muestra:</strong></td>
            <td><input type="text" id="disolvente1" maxlength="50" size="40" value="<?= $ROW[0]['disolvente1'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>M&eacute;todo validado:</strong></td>
            <td><select id="validado" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['validado'] == '0') echo 'selected'; ?>>No</option>
                    <option value="1" <?php if ($ROW[0]['validado'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select></td>
            <td><strong>Disolvente del Est. de Plag.:</strong></td>
            <td><input type="text" id="disolvente2" maxlength="50" size="40" value="<?= $ROW[0]['disolvente2'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>Tipo de Cromatograf&iacute;a:</strong></td>
            <td><select id="croma" <?= $disabled ?> onchange="escondeCroma(this.value);">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['croma'] == '0') echo 'selected'; ?>>HPLC</option>
                    <option value="1" <?php if ($ROW[0]['croma'] == '1') echo 'selected'; ?>>Gases</option>
                    <option value="2" <?php if ($ROW[0]['croma'] == '2') echo 'selected'; ?>>Iones</option>
                    <option value="3" <?php if ($ROW[0]['croma'] == '3') echo 'selected'; ?>>HPLC-Masas</option>
                    <option value="4" <?php if ($ROW[0]['croma'] == '4') echo 'selected'; ?>>Gases-Masas</option>
                </select></td>
            <td><strong>Disolvente del Est. Interno:</strong></td>
            <td><input type="text" id="disolvente3" maxlength="50" size="40" value="<?= $ROW[0]['disolvente3'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
            <td><strong>Nombre del est&aacute;ndar de plaguicida:</strong></td>
            <td><input type="text" id="estandar" maxlength="50" value="<?= $ROW[0]['estandar'] ?>" <?= $disabled ?>/>
            </td>
        </tr>
        <tr>
            <td><strong>Tiempo retenci&oacute;n muestra (min):</strong></td>
            <td><input type="text" id="tiempo1" class="monto" value="<?= $ROW[0]['tiempo1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><strong>Origen y c&oacute;digo del est&aacute;ndar de plaguicida:</strong></td>
            <td><input type="text" id="origen1" maxlength="50" value="<?= $ROW[0]['origen1'] ?>" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>Tiempo retenci&oacute;n Est&aacute;ndar Plaguicida (min):</strong></td>
            <td><input type="text" id="tiempo2" class="monto" value="<?= $ROW[0]['tiempo2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><strong>Est&aacute;ndar de plaguicida tiene certificado?:</strong></td>
            <td><select id="certificado1" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['certificado1'] == '0') echo 'selected'; ?>>No</option>
                    <option value="1" <?php if ($ROW[0]['certificado1'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td><strong>Tiempo retenci&oacute;n est&aacute;ndar interno (min):</strong></td>
            <td><input type="text" id="tiempo3" class="monto" value="<?= $ROW[0]['tiempo3'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><strong>Pureza del est&aacute;ndar de plaguicida, muestra 1 (%):</strong></td>
            <td><input type="text" id="pureza1" class="monto" value="<?= $ROW[0]['pureza1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
            <td><strong>Pureza del est&aacute;ndar de plaguicida, muestra 2 (%):</strong></td>
            <td><input type="text" id="purezas1" class="monto" value="<?= $ROW[0]['purezas1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
            <td><strong>Utiliza Est&aacute;ndar Interno:</strong></td>
            <td><select id="interno1" <?= $disabled ?> onchange="__calcula();">
                    <option value="0">No</option>
                    <option value="1" <?php if ($ROW[0]['interno1'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td><strong>Volumen de inyecci&oacute;n (&micro;L):</strong></td>
            <td><input type="text" id="inyeccion" class="monto" value="<?= $ROW[0]['inyeccion'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><strong>Nombre del est&aacute;ndar interno:</strong></td>
            <td><input type="text" id="interno2" maxlength="50" value="<?= $ROW[0]['interno2'] ?>" <?= $disabled ?>/>
            </td>
        </tr>
        <tr>
            <td><strong>Volumen del loop (&micro;L) * (Cromatograf&iacute;a L&iacute;quida):</strong></td>
            <td><input type="text" id="loop" class="monto" value="<?= $ROW[0]['loop'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><strong>Origen y c&oacute;digo del est&aacute;ndar interno:</strong></td>
            <td><input type="text" id="origen2" maxlength="50" value="<?= $ROW[0]['origen2'] ?>" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>M&eacute;todo con curva de calibraci&oacute;n</strong></td>
            <td><select id="metcalibra" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['metcalibra'] == '0') echo 'selected'; ?>>No</option>
                    <option value="1" <?php if ($ROW[0]['metcalibra'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select></td>
            <td><strong>Est&aacute;ndar interno tiene certificado de pureza:</strong></td>
            <td><select id="certificado2" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['certificado2'] == '0') echo 'selected'; ?>>No</option>
                    <option value="1" <?php if ($ROW[0]['certificado2'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
            <td><strong>Pureza del est&aacute;ndar interno (%):</strong></td>
            <td><input type="text" id="pureza2" class="monto" value="<?= $ROW[0]['pureza2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>
    </table>
    <br>
    <input type="hidden" id="linealidad1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['linealidad1'] ?>"
           disabled>
    <input type="hidden" id="linealidad2" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['linealidad2'] ?>"
           disabled>
    <input type="hidden" id="IECA" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['IECA'] ?>" disabled>
    <input type="hidden" id="repeti1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['repeti1'] ?>" disabled>
    <input type="hidden" id="repeti2" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['repeti2'] ?>" disabled>
    <input type="hidden" id="IECB" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['IECB'] ?>" disabled>
    <table class="radius" style="font-size:12px" width="98%" align="center" cellpadding="4" cellspacing="4">
        <tr>
            <td class="titulo" colspan="3">Condiciones del equipo</td>
        </tr>
        <tr>
            <td><strong>C&oacute;digo de equipo:</strong>&nbsp;&nbsp;<input type="text" id="tmp_equipo1" class="lista"
                                                                            value="<?= $ROW[0]['tmp_equipo1'] ?>"
                                                                            readonly onclick="EquiposLista(1)"
                                                                            <?= $disabled ?>/><input type="hidden"
                                                                                                     id="equipo"
                                                                                                     value="<?= $ROW[0]['equipo'] ?>"/>
            </td>
            <td><strong>Marca y Modelo de equipo:</strong></td>
            <td id="nommarmo"><?= $ROW[0]['nombre'] ?></td>
        </tr>
        <tr>
            <td class="titulo" colspan="3">Columna</td>
        </tr>
        <tr>
            <td><strong>Tipo:</strong></td>
            <td><select id="tipo_c" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['tipo_c'] == '0') echo 'selected'; ?>>Capilar</option>
                    <option value="1" <?php if ($ROW[0]['tipo_c'] == '1') echo 'selected'; ?>>Empacado</option>
                    <option value="2" <?php if ($ROW[0]['tipo_c'] == '2') echo 'selected'; ?>>HPLC</option>
                </select></td>
        </tr>
        <tr>
            <td><strong>C&oacute;digo:</strong></td>
            <td><input type="text" id="codigo_c" maxlength="20" value="<?= $ROW[0]['codigo_c'] ?>" <?= $disabled ?>
                       class="lista" onclick="ColumnasLista()"/></td>
        </tr>
        <tr>
            <td><strong>Marca y fase:</strong></td>
            <td><input type="text" id="marca_c" maxlength="50" value="<?= $ROW[0]['marca_c'] ?>" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>Temperatura de trabajo (�C):</strong></td>
            <td><input type="text" id="temp_c" class="monto" value="<?= $ROW[0]['temp_c'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>
        <tr>
            <td><strong>Dimensiones:</strong></td>
            <td><input type="text" id="dim_c" maxlength="50" value="<?= $ROW[0]['dim_c'] ?>" <?= $disabled ?>/></td>
        </tr>
        <tbody id="cromaGas">
        <tr>
            <td class="titulo" colspan="3">Inyector (Cromatograf&iacute;a Gaseosa)</td>
        </tr>
        <tr>
            <td><strong>Tipo:</strong></td>
            <td><select id="tipo_i" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="">N/A</option>
                    <option value="0" <?php if ($ROW[0]['tipo_i'] == '0') echo 'selected'; ?>>COC</option>
                    <option value="1" <?php if ($ROW[0]['tipo_i'] == '1') echo 'selected'; ?>>Capilar</option>
                    <option value="2" <?php if ($ROW[0]['tipo_i'] == '2') echo 'selected'; ?>>Empacado</option>
                    <option value="3" <?php if ($ROW[0]['tipo_i'] == '3') echo 'selected'; ?>>PTV</option>
                </select></td>
        </tr>
        <tr>
            <td><strong>Temperatura (�C):</strong></td>
            <td><input type="text" id="temp_i" class="monto" value="<?= $ROW[0]['temp_i'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>
        <tr>
            <td><strong>Modo de inyecci&oacute;n:</strong></td>
            <td><select id="modo_i" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="">N/A</option>
                    <option value="0" <?php if ($ROW[0]['modo_i'] == '0') echo 'selected'; ?>>Split</option>
                    <option value="1" <?php if ($ROW[0]['modo_i'] == '1') echo 'selected'; ?>>Splitless</option>
                </select></td>
        </tr>
        <tr>
            <td><strong>Relaci&oacute;n split:</strong></td>
            <td><input type="text" id="condicion" value="<?= $ROW[0]['condicion'] ?>" size="15" maxlength="20"
                       <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td class="titulo" colspan="3">Fase m&oacute;vil (Cromatograf&iacute;a Gaseosa)</td>
        </tr>
        <tr align="center">
            <td><strong>Gas de arrastre</strong></td>
            <td><strong>Flujo (mL/min)</strong></td>
            <td><strong>Presi&oacute;n (psi)</strong></td>
        </tr>
        <tr align="center">
            <td><select id="gas1" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="">N/A</option>
                    <option value="0" <?php if ($ROW[0]['gas1'] == '0') echo 'selected'; ?>>Hidr&oacute;geno</option>
                    <option value="1" <?php if ($ROW[0]['gas1'] == '1') echo 'selected'; ?>>Helio</option>
                    <option value="2" <?php if ($ROW[0]['gas1'] == '2') echo 'selected'; ?>>Nitr&oacute;geno</option>
                </select></td>
            <td><input type="text" id="flujo1" class="monto" value="<?= $ROW[0]['flujo1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="presion1" class="monto" value="<?= $ROW[0]['presion1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>
        <tr align="center">
            <td><select id="gas2" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="">N/A</option>
                    <option value="0" <?php if ($ROW[0]['gas2'] == '0') echo 'selected'; ?>>Hidr&oacute;geno</option>
                    <option value="1" <?php if ($ROW[0]['gas2'] == '1') echo 'selected'; ?>>Helio</option>
                    <option value="2" <?php if ($ROW[0]['gas2'] == '2') echo 'selected'; ?>>Nitr&oacute;geno</option>
                </select></td>
            <td><input type="text" id="flujo2" class="monto" value="<?= $ROW[0]['flujo2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="presion2" class="monto" value="<?= $ROW[0]['presion2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>
        <tr align="center" style="visibility:hidden">
            <td><strong>Make-up</strong></td>
            <td><strong>Flujo (mL/min)</strong></td>
            <td><strong>Presi&oacute;n (psi)</strong></td>
        </tr>
        <tr align="center" style="visibility:hidden">
            <td><select id="gas3" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="">N/A</option>
                    <option value="0" <?php if ($ROW[0]['gas3'] == '0') echo 'selected'; ?>>Hidr&oacute;geno</option>
                    <option value="1" <?php if ($ROW[0]['gas3'] == '1') echo 'selected'; ?>>Helio</option>
                    <option value="2" <?php if ($ROW[0]['gas3'] == '2') echo 'selected'; ?>>Nitr&oacute;geno</option>
                </select></td>
            <td><input type="text" id="flujo3" class="monto" value="<?= $ROW[0]['flujo3'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="presion3" class="monto" value="<?= $ROW[0]['presion3'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>
        </tbody>
        <tbody id="cromaLiq">
        <tr>
            <td class="titulo" colspan="3">Fase m&oacute;vil (Cromatograf&iacute;a L&iacute;quida)</td>
        </tr>
        <tr>
            <td><strong>Tipo de bomba:</strong></td>
            <td colspan="2"><select id="bomba_d" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="">N/A</option>
                    <option value="0" <?php if ($ROW[0]['bomba_d'] == '0') echo 'selected'; ?>>Binaria</option>
                    <option value="1" <?php if ($ROW[0]['bomba_d'] == '1') echo 'selected'; ?>>Cuaternaria</option>
                </select></td>
        </tr>
        <tr>
            <td><strong>Eluente(s):</strong></td>
            <td colspan="2"><textarea id="elu_d" <?= $disabled ?>><?= $ROW[0]['elu_d'] ?></textarea></td>
        </tr>
        <tr align="center">
            <td><strong>Canal</strong></td>
            <td><strong>Flujo (mL/min)</strong></td>
            <td><strong>Presi&oacute;n (psi)</strong></td>
        </tr>
        <tr align="center">
            <td><select id="gas4" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="">N/A</option>
                    <option value="0" <?php if ($ROW[0]['gas4'] == '0') echo 'selected'; ?>>A</option>
                    <option value="1" <?php if ($ROW[0]['gas4'] == '1') echo 'selected'; ?>>B</option>
                    <option value="2" <?php if ($ROW[0]['gas4'] == '2') echo 'selected'; ?>>C</option>
                    <option value="3" <?php if ($ROW[0]['gas4'] == '3') echo 'selected'; ?>>D</option>
                </select></td>
            <td><input type="text" id="flujo4" class="monto" value="<?= $ROW[0]['flujo4'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="presion4" class="monto" value="<?= $ROW[0]['presion4'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>
        <tr align="center">
            <td><select id="gas5" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="">N/A</option>
                    <option value="0" <?php if ($ROW[0]['gas5'] == '0') echo 'selected'; ?>>A</option>
                    <option value="1" <?php if ($ROW[0]['gas5'] == '1') echo 'selected'; ?>>B</option>
                    <option value="2" <?php if ($ROW[0]['gas5'] == '2') echo 'selected'; ?>>C</option>
                    <option value="3" <?php if ($ROW[0]['gas5'] == '3') echo 'selected'; ?>>D</option>
                </select></td>
            <td><input type="text" id="flujo5" class="monto" value="<?= $ROW[0]['flujo5'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="presion5" class="monto" value="<?= $ROW[0]['presion5'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>
        <tr align="center">
            <td><select id="gas6" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="">N/A</option>
                    <option value="0" <?php if ($ROW[0]['gas6'] == '0') echo 'selected'; ?>>A</option>
                    <option value="1" <?php if ($ROW[0]['gas6'] == '1') echo 'selected'; ?>>B</option>
                    <option value="2" <?php if ($ROW[0]['gas6'] == '2') echo 'selected'; ?>>C</option>
                    <option value="3" <?php if ($ROW[0]['gas6'] == '3') echo 'selected'; ?>>D</option>
                </select></td>
            <td><input type="text" id="flujo6" class="monto" value="<?= $ROW[0]['flujo6'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="presion6" class="monto" value="<?= $ROW[0]['presion6'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>
        <tr align="center">
            <td><select id="gas7" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="">N/A</option>
                    <option value="0" <?php if ($ROW[0]['gas7'] == '0') echo 'selected'; ?>>A</option>
                    <option value="1" <?php if ($ROW[0]['gas7'] == '1') echo 'selected'; ?>>B</option>
                    <option value="2" <?php if ($ROW[0]['gas7'] == '2') echo 'selected'; ?>>C</option>
                    <option value="3" <?php if ($ROW[0]['gas7'] == '3') echo 'selected'; ?>>D</option>
                </select></td>
            <td><input type="text" id="flujo7" class="monto" value="<?= $ROW[0]['flujo7'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="presion7" class="monto" value="<?= $ROW[0]['presion7'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>
        </tbody>
        <tbody id="cromaIon">
        <tr>
            <td class="titulo" colspan="3">Detector</td>
        </tr>
        <tr>
            <td><strong>Tipo:</strong></td>
            <td><select id="tipo_d" <?= $disabled ?> onchange="escondeDetector(this.value);">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['tipo_d'] == '0') echo 'selected'; ?>>Arreglo de Diodos</option>
                    <option value="1" <?php if ($ROW[0]['tipo_d'] == '1') echo 'selected'; ?>>Conductividad</option>
                    <option value="2" <?php if ($ROW[0]['tipo_d'] == '2') echo 'selected'; ?>>FID</option>
                    <!--<option value="3" <?php if ($ROW[0]['tipo_d'] == '3') echo 'selected'; ?>>Masas</option>
				<option value="4" <?php if ($ROW[0]['tipo_d'] == '4') echo 'selected'; ?>>PTV</option>-->
                </select></td>
        </tr>
        <tr id="tempTrab">
            <td><strong>Temperatura de trabajo (�C):</strong></td>
            <td><input type="text" id="temp_d" class="monto" value="<?= $ROW[0]['temp_d'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>
        <tr id="LongOnda">
            <td><strong>Longitud de onda (nm):</strong></td>
            <td><input type="text" id="long_d" class="monto" value="<?= $ROW[0]['long_d'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>
        <tr>
            <td colspan="2">
                <hr/>
            </td>
        </tr>
        <tr id="condicionesFID">
            <td align="center"><strong>Gases del FID:</strong></td>
            <td align="center"><strong>Flujo (mL/min)</strong></td>
        </tr>
        <tr align="center" id="condicionesFID1">
            <td><select id="fid" disabled>
                    <option value="0" <?php if ($ROW[0]['fid'] == '0') echo 'selected'; ?>>Hidr&oacute;geno</option>
                </select></td>
            <td><input type="text" id="flujofid1" class="monto" value="<?= $ROW[0]['flujofid1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>
        <tr align="center" id="condicionesFID2">
            <td><select id="fid1" disabled>
                    <option value="1" <?php if ($ROW[0]['fid1'] == '1') echo 'selected'; ?>>Aire</option>
                </select></td>
            <td><input type="text" id="flujofid2" class="monto" value="<?= $ROW[0]['flujofid2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>
        <tr align="center" id="condicionesFID3">
            <td><select id="fid2" disabled>
                    <option value="2" <?php if ($ROW[0]['fid2'] == '2') echo 'selected'; ?>>Make-Up (Nitr&oacute;geno)
                    </option>
                </select></td>
            <td><input type="text" id="flujofid3" class="monto" value="<?= $ROW[0]['flujofid3'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>

        <tr>
            <td class="titulo" colspan="3">Para cromatograf&iacute;a de iones</td>
        </tr>
        <tr style="visibility:hidden;">
            <td><strong>Modo de adquisici&oacute;n:</strong></td>
            <td><select id="modo" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['modo'] == '0') echo 'selected'; ?>>SIM</option>
                    <option value="1" <?php if ($ROW[0]['modo'] == '1') echo 'selected'; ?>>SCAN</option>
                    <option value="2" <?php if ($ROW[0]['modo'] == '2') echo 'selected'; ?>>SIM/SCAN</option>
                </select></td>
        </tr>
        <tr>
            <td><strong>Corriente del supresor (mA):</strong></td>
            <td><input type="text" id="temp1_g" class="monto" value="<?= $ROW[0]['temp1_g'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>
        <tr>
            <td><strong>Modelo del supresor:</strong></td>
            <td><input type="text" id="temp2_g" class="monto" value="<?= $ROW[0]['temp2_g'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>
        <tr>
            <td><strong>Presi�n (psi):</strong></td>
            <td><input type="text" id="temp3_g" class="monto" value="<?= $ROW[0]['temp3_g'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
        </tr>
        </td>
        </tr>
        </tbody>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="8">Datos del an&aacute;lisis: Sin Curva de Calibraci&oacute;n</td>
        </tr>
        <tr>
            <td><strong>Dato</strong></td>
            <td><strong>Masa (mg)</strong></td>
            <td><strong>Vol B. Afo. 1 (mL)</strong></td>
            <td><strong>Vol. Al&iacute;cuota 1 (mL)</strong></td>
            <td><strong>Vol B. Afo. 2 (mL)</strong></td>
            <td><strong>Vol. Al&iacute;cuota 2 (mL)</strong></td>
            <td><strong>Vol B. Afo. 3 (mL)</strong></td>
            <td><strong>Conc. (mg/mL)</strong></td>
        </tr>
        <tr>
            <td>Muestra 1</td>
            <td><input type="text" id="masa1" class="monto" value="<?= $ROW[0]['masa1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><select id="aforadoA1" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="0" IR="0">...</option>
                    <option <?php if ($ROW[0]['aforadoA1'] == '10') echo 'selected'; ?> value="10" IEC="0.01190"
                                                                                        IR="0.00119">10.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA1'] == '25') echo 'selected'; ?> value="25" IEC="0.01660"
                                                                                        IR="0.00066">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA1'] == '50') echo 'selected'; ?> value="50" IEC="0.02780"
                                                                                        IR="0.00056">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA1'] == '100') echo 'selected'; ?> value="100" IEC="0.04690"
                                                                                         IR="0.00047">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA1'] == '200') echo 'selected'; ?> value="200" IEC="0.08180"
                                                                                         IR="0.00041">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA1'] == '250') echo 'selected'; ?> value="250" IEC="0.09880"
                                                                                         IR="0.00040">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA1'] == '500') echo 'selected'; ?> value="500" IEC="0.18700"
                                                                                         IR="0.00037">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA1'] == '1000') echo 'selected'; ?> value="1000" IEC="0.34760"
                                                                                          IR="0.00035">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA1'] == '2000') echo 'selected'; ?> value="2000" IEC="0.68190"
                                                                                          IR="0.00034">2000.00
                    </option>
                </select></td>
            <td><select id="muestraalicuota1" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="0" IR="0">...</option>
                    <option <?php if ($ROW[0]['muestraalicuota1'] == '0.5') echo 'selected'; ?> value="0.5"
                                                                                                IEC="0.00310"
                                                                                                IR="0.0062">0.50
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota1'] == '1') echo 'selected'; ?> value="1" IEC="0.00390"
                                                                                              IR="0.0039">1.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota1'] == '2') echo 'selected'; ?> value="2" IEC="0.00390"
                                                                                              IR="0.0020">2.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota1'] == '3') echo 'selected'; ?> value="3" IEC="0.00580"
                                                                                              IR="0.0019">3.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota1'] == '4') echo 'selected'; ?> value="4" IEC="0.00590"
                                                                                              IR="0.0015">4.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota1'] == '5') echo 'selected'; ?> value="5" IEC="0.00590"
                                                                                              IR="0.0012">5.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota1'] == '6') echo 'selected'; ?> value="6" IEC="0.00600"
                                                                                              IR="0.0010">6.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota1'] == '7') echo 'selected'; ?> value="7" IEC="0.00620"
                                                                                              IR="0.0009">7.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota1'] == '10') echo 'selected'; ?> value="10" IEC="0.00970"
                                                                                               IR="0.0010">10.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota1'] == '15') echo 'selected'; ?> value="15" IEC="0.00620"
                                                                                               IR="0.0009">15.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota1'] == '20') echo 'selected'; ?> value="20" IEC="0.01470"
                                                                                               IR="0.0007">20.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota1'] == '25') echo 'selected'; ?> value="25" IEC="0.01540"
                                                                                               IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota1'] == '50') echo 'selected'; ?> value="50" IEC="0.02690"
                                                                                               IR="0.0005">50.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota1'] == '100') echo 'selected'; ?> value="100"
                                                                                                IEC="0.04650"
                                                                                                IR="0.0005">100.00
                    </option>
                </select></td>
            <td><select id="muestraaforadoA2" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="0" IR="0">...</option>
                    <option <?php if ($ROW[0]['muestraaforadoA2'] == '10') echo 'selected'; ?> value="10" IEC="0.01190"
                                                                                               IR="0.00119">10.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA2'] == '25') echo 'selected'; ?> value="25" IEC="0.01660"
                                                                                               IR="0.00066">25.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA2'] == '50') echo 'selected'; ?> value="50" IEC="0.02780"
                                                                                               IR="0.00056">50.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA2'] == '100') echo 'selected'; ?> value="100"
                                                                                                IEC="0.04690"
                                                                                                IR="0.00047">100.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA2'] == '200') echo 'selected'; ?> value="200"
                                                                                                IEC="0.08180"
                                                                                                IR="0.00041">200.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA2'] == '250') echo 'selected'; ?> value="250"
                                                                                                IEC="0.09880"
                                                                                                IR="0.00040">250.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA2'] == '500') echo 'selected'; ?> value="500"
                                                                                                IEC="0.18700"
                                                                                                IR="0.00037">500.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA2'] == '1000') echo 'selected'; ?> value="1000"
                                                                                                 IEC="0.34760"
                                                                                                 IR="0.00035">1000.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA2'] == '2000') echo 'selected'; ?> value="2000"
                                                                                                 IEC="0.68190"
                                                                                                 IR="0.00034">2000.00
                    </option>
                </select></td>
            <td><select id="muestraalicuota2" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="0" IR="0">...</option>
                    <option <?php if ($ROW[0]['muestraalicuota2'] == '0.5') echo 'selected'; ?> value="0.5"
                                                                                                IEC="0.00310"
                                                                                                IR="0.0062">0.50
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota2'] == '1') echo 'selected'; ?> value="1" IEC="0.00390"
                                                                                              IR="0.0039">1.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota2'] == '2') echo 'selected'; ?> value="2" IEC="0.00390"
                                                                                              IR="0.0020">2.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota2'] == '3') echo 'selected'; ?> value="3" IEC="0.00580"
                                                                                              IR="0.0019">3.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota2'] == '4') echo 'selected'; ?> value="4" IEC="0.00590"
                                                                                              IR="0.0015">4.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota2'] == '5') echo 'selected'; ?> value="5" IEC="0.00590"
                                                                                              IR="0.0012">5.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota2'] == '6') echo 'selected'; ?> value="6" IEC="0.00600"
                                                                                              IR="0.0010">6.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota2'] == '7') echo 'selected'; ?> value="7" IEC="0.00620"
                                                                                              IR="0.0009">7.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota2'] == '10') echo 'selected'; ?> value="10" IEC="0.00970"
                                                                                               IR="0.0010">10.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota2'] == '15') echo 'selected'; ?> value="15" IEC="0.00620"
                                                                                               IR="0.0009">15.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota2'] == '20') echo 'selected'; ?> value="20" IEC="0.01470"
                                                                                               IR="0.0007">20.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota2'] == '25') echo 'selected'; ?> value="25" IEC="0.01540"
                                                                                               IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota2'] == '50') echo 'selected'; ?> value="50" IEC="0.02690"
                                                                                               IR="0.0005">50.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota2'] == '100') echo 'selected'; ?> value="100"
                                                                                                IEC="0.04650"
                                                                                                IR="0.0005">100.00
                    </option>
                </select></td>
            <td><select id="muestraaforadoA3" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="0" IR="0">...</option>
                    <option <?php if ($ROW[0]['muestraaforadoA3'] == '10') echo 'selected'; ?> value="10" IEC="0.01190"
                                                                                               IR="0.00119">10.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA3'] == '25') echo 'selected'; ?> value="25" IEC="0.01660"
                                                                                               IR="0.00066">25.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA3'] == '50') echo 'selected'; ?> value="50" IEC="0.02780"
                                                                                               IR="0.00056">50.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA3'] == '100') echo 'selected'; ?> value="100"
                                                                                                IEC="0.04690"
                                                                                                IR="0.00047">100.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA3'] == '200') echo 'selected'; ?> value="200"
                                                                                                IEC="0.08180"
                                                                                                IR="0.00041">200.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA3'] == '250') echo 'selected'; ?> value="250"
                                                                                                IEC="0.09880"
                                                                                                IR="0.00040">250.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA3'] == '500') echo 'selected'; ?> value="500"
                                                                                                IEC="0.18700"
                                                                                                IR="0.00037">500.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA3'] == '1000') echo 'selected'; ?> value="1000"
                                                                                                 IEC="0.34760"
                                                                                                 IR="0.00035">1000.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA3'] == '2000') echo 'selected'; ?> value="2000"
                                                                                                 IEC="0.68190"
                                                                                                 IR="0.00034">2000.00
                    </option>
                </select></td>
            <td align="center" id="con1"></td>
        </tr>
        <tr>
            <td>Est&aacute;ndar Plaguicida</td>
            <td><input type="text" id="masa2" class="monto" value="<?= $ROW[0]['masa2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><select id="aforadoA2" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="0" IR="0">...</option>
                    <option <?php if ($ROW[0]['aforadoA2'] == '10') echo 'selected'; ?> value="10" IEC="0.01190"
                                                                                        IR="0.00119">10.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA2'] == '25') echo 'selected'; ?> value="25" IEC="0.01660"
                                                                                        IR="0.00066">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA2'] == '50') echo 'selected'; ?> value="50" IEC="0.02780"
                                                                                        IR="0.00056">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA2'] == '100') echo 'selected'; ?> value="100" IEC="0.04690"
                                                                                         IR="0.00047">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA2'] == '200') echo 'selected'; ?> value="200" IEC="0.08180"
                                                                                         IR="0.00041">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA2'] == '250') echo 'selected'; ?> value="250" IEC="0.09880"
                                                                                         IR="0.00040">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA2'] == '500') echo 'selected'; ?> value="500" IEC="0.18700"
                                                                                         IR="0.00037">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA2'] == '1000') echo 'selected'; ?> value="1000" IEC="0.34760"
                                                                                          IR="0.00035">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA2'] == '2000') echo 'selected'; ?> value="2000" IEC="0.68190"
                                                                                          IR="0.00034">2000.00
                    </option>
                </select></td>
            <td><select id="plaguicidaalicuota1" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="0" IR="0">...</option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota1'] == '0.5') echo 'selected'; ?> value="0.5"
                                                                                                   IEC="0.00310"
                                                                                                   IR="0.0062">0.50
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota1'] == '1') echo 'selected'; ?> value="1" IEC="0.00390"
                                                                                                 IR="0.0039">1.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota1'] == '2') echo 'selected'; ?> value="2" IEC="0.00390"
                                                                                                 IR="0.0020">2.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota1'] == '3') echo 'selected'; ?> value="3" IEC="0.00580"
                                                                                                 IR="0.0019">3.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota1'] == '4') echo 'selected'; ?> value="4" IEC="0.00590"
                                                                                                 IR="0.0015">4.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota1'] == '5') echo 'selected'; ?> value="5" IEC="0.00590"
                                                                                                 IR="0.0012">5.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota1'] == '6') echo 'selected'; ?> value="6" IEC="0.00600"
                                                                                                 IR="0.0010">6.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota1'] == '7') echo 'selected'; ?> value="7" IEC="0.00620"
                                                                                                 IR="0.0009">7.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota1'] == '10') echo 'selected'; ?> value="10"
                                                                                                  IEC="0.00970"
                                                                                                  IR="0.0010">10.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota1'] == '15') echo 'selected'; ?> value="15"
                                                                                                  IEC="0.00620"
                                                                                                  IR="0.0009">15.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota1'] == '20') echo 'selected'; ?> value="20"
                                                                                                  IEC="0.01470"
                                                                                                  IR="0.0007">20.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota1'] == '25') echo 'selected'; ?> value="25"
                                                                                                  IEC="0.01540"
                                                                                                  IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota1'] == '50') echo 'selected'; ?> value="50"
                                                                                                  IEC="0.02690"
                                                                                                  IR="0.0005">50.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota1'] == '100') echo 'selected'; ?> value="100"
                                                                                                   IEC="0.04650"
                                                                                                   IR="0.0005">100.00
                    </option>
                </select></td>
            <td><select id="plaguicidaaforadoA2" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="0" IR="0">...</option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA2'] == '10') echo 'selected'; ?> value="10"
                                                                                                  IEC="0.01190"
                                                                                                  IR="0.00119">10.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA2'] == '25') echo 'selected'; ?> value="25"
                                                                                                  IEC="0.01660"
                                                                                                  IR="0.00066">25.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA2'] == '50') echo 'selected'; ?> value="50"
                                                                                                  IEC="0.02780"
                                                                                                  IR="0.00056">50.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA2'] == '100') echo 'selected'; ?> value="100"
                                                                                                   IEC="0.04690"
                                                                                                   IR="0.00047">100.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA2'] == '200') echo 'selected'; ?> value="200"
                                                                                                   IEC="0.08180"
                                                                                                   IR="0.00041">200.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA2'] == '250') echo 'selected'; ?> value="250"
                                                                                                   IEC="0.09880"
                                                                                                   IR="0.00040">250.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA2'] == '500') echo 'selected'; ?> value="500"
                                                                                                   IEC="0.18700"
                                                                                                   IR="0.00037">500.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA2'] == '1000') echo 'selected'; ?> value="1000"
                                                                                                    IEC="0.34760"
                                                                                                    IR="0.00035">1000.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA2'] == '2000') echo 'selected'; ?> value="2000"
                                                                                                    IEC="0.68190"
                                                                                                    IR="0.00034">2000.00
                    </option>
                </select></td>
            <td><select id="plaguicidaalicuota2" <?= $disabled ?> onchange="__calcula()">
                    <option value="">...</option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota2'] == '0.5') echo 'selected'; ?> value="0.5">0.50
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota2'] == '1') echo 'selected'; ?> value="1">1.00</option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota2'] == '2') echo 'selected'; ?> value="2">2.00</option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota2'] == '3') echo 'selected'; ?> value="3">3.00</option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota2'] == '4') echo 'selected'; ?> value="4">4.00</option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota2'] == '5') echo 'selected'; ?> value="5">5.00</option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota2'] == '6') echo 'selected'; ?> value="6">6.00</option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota2'] == '7') echo 'selected'; ?> value="7">7.00</option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota2'] == '10') echo 'selected'; ?> value="10">10.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota2'] == '15') echo 'selected'; ?> value="15">15.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota2'] == '20') echo 'selected'; ?> value="20">20.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota2'] == '25') echo 'selected'; ?> value="25">25.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota2'] == '50') echo 'selected'; ?> value="50">50.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota2'] == '100') echo 'selected'; ?> value="100">100.00
                    </option>
                </select></td>
            <td><select id="plaguicidaaforadoA3" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA3'] == '10') echo 'selected'; ?> value="10" IEC="0"
                                                                                                  IR="0">10.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA3'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                                  IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA3'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                                  IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA3'] == '100') echo 'selected'; ?> value="100"
                                                                                                   IEC="0.047"
                                                                                                   IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA3'] == '200') echo 'selected'; ?> value="200"
                                                                                                   IEC="0.082"
                                                                                                   IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA3'] == '250') echo 'selected'; ?> value="250"
                                                                                                   IEC="0.099"
                                                                                                   IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA3'] == '500') echo 'selected'; ?> value="500"
                                                                                                   IEC="0.180"
                                                                                                   IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA3'] == '1000') echo 'selected'; ?> value="1000"
                                                                                                    IEC="0.348"
                                                                                                    IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA3'] == '2000') echo 'selected'; ?> value="2000"
                                                                                                    IEC="0.674"
                                                                                                    IR="0.0003">2000.00
                    </option>
                </select></td>
            <td align="center" id="con2"></td>
        </tr>
        <tr>
            <td>Est&aacute;ndar Interno</td>
            <td><input type="text" id="masa3" class="monto" value="<?= $ROW[0]['masa3'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><select id="aforadoA3" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['aforadoA3'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">
                        10.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA3'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                        IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA3'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                        IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA3'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                         IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA3'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                         IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA3'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                         IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA3'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                         IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA3'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                          IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA3'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                          IR="0.0003">2000.00
                    </option>
                </select></td>
            <td><select id="alicuota" <?= $disabled ?> onchange="__calcula()">
                    <option value="">...</option>
                    <option <?php if ($ROW[0]['alicuota'] == '0.5') echo 'selected'; ?> value="0.5">0.50</option>
                    <option <?php if ($ROW[0]['alicuota'] == '1') echo 'selected'; ?> value="1">1.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '2') echo 'selected'; ?> value="2">2.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '3') echo 'selected'; ?> value="3">3.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '4') echo 'selected'; ?> value="4">4.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '5') echo 'selected'; ?> value="5">5.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '6') echo 'selected'; ?> value="6">6.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '7') echo 'selected'; ?> value="7">7.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '10') echo 'selected'; ?> value="10">10.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '15') echo 'selected'; ?> value="15">15.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '20') echo 'selected'; ?> value="20">20.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '25') echo 'selected'; ?> value="25">25.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '50') echo 'selected'; ?> value="50">50.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '100') echo 'selected'; ?> value="100">100.00</option>
                </select></td>
            <td><select id="aforadoB3" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['aforadoB3'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">
                        10.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoB3'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                        IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoB3'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                        IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoB3'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                         IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoB3'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                         IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoB3'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                         IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoB3'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                         IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoB3'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                          IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoB3'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                          IR="0.0003">2000.00
                    </option>
                </select></td>
            <td><select id="internoalicuota" <?= $disabled ?> onchange="__calcula()">
                    <option value="">...</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '0.5') echo 'selected'; ?> value="0.5">0.50</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '1') echo 'selected'; ?> value="1">1.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '2') echo 'selected'; ?> value="2">2.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '3') echo 'selected'; ?> value="3">3.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '4') echo 'selected'; ?> value="4">4.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '5') echo 'selected'; ?> value="5">5.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '6') echo 'selected'; ?> value="6">6.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '7') echo 'selected'; ?> value="7">7.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '10') echo 'selected'; ?> value="10">10.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '15') echo 'selected'; ?> value="15">15.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '20') echo 'selected'; ?> value="20">20.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '25') echo 'selected'; ?> value="25">25.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '50') echo 'selected'; ?> value="50">50.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '100') echo 'selected'; ?> value="100">100.00
                    </option>
                </select></td>
            <td><select id="internoaforadoB3" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['internoaforadoB3'] == '10') echo 'selected'; ?> value="10" IEC="0"
                                                                                               IR="0">10.00
                    </option>
                    <option <?php if ($ROW[0]['internoaforadoB3'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                               IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['internoaforadoB3'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                               IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['internoaforadoB3'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                                IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['internoaforadoB3'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                                IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['internoaforadoB3'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                                IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['internoaforadoB3'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                                IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['internoaforadoB3'] == '1000') echo 'selected'; ?> value="1000"
                                                                                                 IEC="0.348"
                                                                                                 IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['internoaforadoB3'] == '2000') echo 'selected'; ?> value="2000"
                                                                                                 IEC="0.674"
                                                                                                 IR="0.0003">2000.00
                    </option>
                </select></td>
            <td align="center" id="con3"></td>
        </tr>
        <tr>
            <td colspan="8">
                <hr/>
            </td>
        </tr>
        <tr>
            <td><strong>Dato</strong></td>
            <td><strong>&Aacute;rea 1</strong></td>
            <td><strong>&Aacute;rea 2</strong></td>
            <td><strong>&Aacute;rea 3</strong></td>
            <td><strong>&Aacute;rea promedio</strong></td>
            <td><strong>Desviaci&oacute;n Est&aacute;ndar</strong></td>
            <td><strong>% C.V.</strong></td>
        </tr>
        <tr>
            <td>&Aacute;rea Muestra 1</td>
            <td><input type="text" id="areaA1" class="monto" value="<?= $ROW[0]['areaA1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaB1" class="monto" value="<?= $ROW[0]['areaB1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaC1" class="monto" value="<?= $ROW[0]['areaC1'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td align="center" id="prom1"></td>
            <td align="center" id="desv1"></td>
            <td align="center" id="cv1"></td>
        </tr>
        <tr>
            <td>&Aacute;rea Est. Interno en la muestra 1</td>
            <td><input type="text" id="areaA2" class="monto" value="<?= $ROW[0]['areaA2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaB2" class="monto" value="<?= $ROW[0]['areaB2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaC2" class="monto" value="<?= $ROW[0]['areaC2'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td align="center" id="prom2"></td>
            <td align="center" id="desv2"></td>
            <td align="center" id="cv2"></td>
        </tr>
        <tr>
            <td>&Aacute;rea Est&aacute;ndar Plaguicida 1</td>
            <td><input type="text" id="areaA3" class="monto" value="<?= $ROW[0]['areaA3'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaB3" class="monto" value="<?= $ROW[0]['areaB3'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaC3" class="monto" value="<?= $ROW[0]['areaC3'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td align="center" id="prom3"></td>
            <td align="center" id="desv3"></td>
            <td align="center" id="cv3"></td>
        </tr>
        <tr>
            <td>&Aacute;rea Est. Interno 1 en el est&aacute;ndar de plaguicida 1</td>
            <td><input type="text" id="areaA4" class="monto" value="<?= $ROW[0]['areaA4'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaB4" class="monto" value="<?= $ROW[0]['areaB4'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaC4" class="monto" value="<?= $ROW[0]['areaC4'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td align="center" id="prom4"></td>
            <td align="center" id="desv4"></td>
            <td align="center" id="cv4"></td>
        </tr>
        <tr>
            <td>Relaci&oacute;n &Aacute;rea Muestra 1 / &Aacute;rea Est&aacute;ndar plaguicida 1</td>
            <td align="center" id="resA1"></td>
            <td align="center" id="resB1"></td>
            <td align="center" id="resC1"></td>
            <td align="center" id="resD1"></td>
            <td align="center" id="desv5"></td>
            <td align="center" id="resE1"></td>
        </tr>
        <tr>
            <td>Relaci&oacute;n &Aacute;rea Muestra 1 / &Aacute;rea Est. interno en muestra 1</td>
            <td align="center" id="resA2"></td>
            <td align="center" id="resB2"></td>
            <td align="center" id="resC2"></td>
            <td align="center" id="resD2"></td>
            <td align="center" id="desv6"></td>
            <td align="center" id="resE2"></td>
        </tr>
        <tr>
            <td>Relaci&oacute;n &Aacute;rea Est. Plag 1 ./ &Aacute;rea Est. interno en Est. Plag. 1</td>
            <td align="center" id="resA3"></td>
            <td align="center" id="resB3"></td>
            <td align="center" id="resC3"></td>
            <td align="center" id="resD3"></td>
            <td align="center" id="desv7"></td>
            <td align="center" id="resE3"></td>
        </tr>
        <tr>
            <td colspan="8">
                <hr/>
            </td>
        </tr>
        <tr>
            <td><strong>Dato</strong></td>
            <td><strong>Masa (mg)</strong></td>
            <td><strong>Vol B. Afo. 1 (mL)</strong></td>
            <td><strong>Vol. Al&iacute;cuota 1 (mL)</strong></td>
            <td><strong>Vol B. Afo. 2 (mL)</strong></td>
            <td><strong>Vol. Al&iacute;cuota 2 (mL)</strong></td>
            <td><strong>Vol B. Afo. 3 (mL)</strong></td>
            <td><strong>Conc. (mg/mL)</strong></td>
        </tr>
        <tr>
            <td>Muestra 2</td>
            <td><input type="text" id="masa4" class="monto" value="<?= $ROW[0]['masa4'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><select id="aforadoA4" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="0" IR="0">...</option>
                    <option <?php if ($ROW[0]['aforadoA4'] == '10') echo 'selected'; ?> value="10" IEC="0.01190"
                                                                                        IR="0.00119">10.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA4'] == '25') echo 'selected'; ?> value="25" IEC="0.01660"
                                                                                        IR="0.00066">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA4'] == '50') echo 'selected'; ?> value="50" IEC="0.02780"
                                                                                        IR="0.00056">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA4'] == '100') echo 'selected'; ?> value="100" IEC="0.04690"
                                                                                         IR="0.00047">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA4'] == '200') echo 'selected'; ?> value="200" IEC="0.08180"
                                                                                         IR="0.00041">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA4'] == '250') echo 'selected'; ?> value="250" IEC="0.09880"
                                                                                         IR="0.00040">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA4'] == '500') echo 'selected'; ?> value="500" IEC="0.18700"
                                                                                         IR="0.00037">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA4'] == '1000') echo 'selected'; ?> value="1000" IEC="0.34760"
                                                                                          IR="0.00035">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA4'] == '2000') echo 'selected'; ?> value="2000" IEC="0.68190"
                                                                                          IR="0.00034">2000.00
                    </option>
                </select></td>
            <td><select id="muestraalicuota3" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="0" IR="0">...</option>
                    <option <?php if ($ROW[0]['muestraalicuota3'] == '0.5') echo 'selected'; ?> value="0.5"
                                                                                                IEC="0.00310"
                                                                                                IR="0.0062">0.50
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota3'] == '1') echo 'selected'; ?> value="1" IEC="0.00390"
                                                                                              IR="0.0039">1.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota3'] == '2') echo 'selected'; ?> value="2" IEC="0.00390"
                                                                                              IR="0.0020">2.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota3'] == '3') echo 'selected'; ?> value="3" IEC="0.00580"
                                                                                              IR="0.0019">3.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota3'] == '4') echo 'selected'; ?> value="4" IEC="0.00590"
                                                                                              IR="0.0015">4.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota3'] == '5') echo 'selected'; ?> value="5" IEC="0.00590"
                                                                                              IR="0.0012">5.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota3'] == '6') echo 'selected'; ?> value="6" IEC="0.00600"
                                                                                              IR="0.0010">6.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota3'] == '7') echo 'selected'; ?> value="7" IEC="0.00620"
                                                                                              IR="0.0009">7.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota3'] == '10') echo 'selected'; ?> value="10" IEC="0.00970"
                                                                                               IR="0.0010">10.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota3'] == '15') echo 'selected'; ?> value="15" IEC="0.00620"
                                                                                               IR="0.0009">15.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota3'] == '20') echo 'selected'; ?> value="20" IEC="0.01470"
                                                                                               IR="0.0007">20.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota3'] == '25') echo 'selected'; ?> value="25" IEC="0.01540"
                                                                                               IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota3'] == '50') echo 'selected'; ?> value="50" IEC="0.02690"
                                                                                               IR="0.0005">50.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota3'] == '100') echo 'selected'; ?> value="100"
                                                                                                IEC="0.04650"
                                                                                                IR="0.0005">100.00
                    </option>
                </select></td>
            <td><select id="muestraaforadoA4" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="0" IR="0">...</option>
                    <option <?php if ($ROW[0]['muestraaforadoA4'] == '10') echo 'selected'; ?> value="10" IEC="0.01190"
                                                                                               IR="0.00119">10.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA4'] == '25') echo 'selected'; ?> value="25" IEC="0.01660"
                                                                                               IR="0.00066">25.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA4'] == '50') echo 'selected'; ?> value="50" IEC="0.02780"
                                                                                               IR="0.00056">50.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA4'] == '100') echo 'selected'; ?> value="100"
                                                                                                IEC="0.04690"
                                                                                                IR="0.00047">100.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA4'] == '200') echo 'selected'; ?> value="200"
                                                                                                IEC="0.08180"
                                                                                                IR="0.00041">200.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA4'] == '250') echo 'selected'; ?> value="250"
                                                                                                IEC="0.09880"
                                                                                                IR="0.00040">250.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA4'] == '500') echo 'selected'; ?> value="500"
                                                                                                IEC="0.18700"
                                                                                                IR="0.00037">500.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA4'] == '1000') echo 'selected'; ?> value="1000"
                                                                                                 IEC="0.34760"
                                                                                                 IR="0.00035">1000.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA4'] == '2000') echo 'selected'; ?> value="2000"
                                                                                                 IEC="0.68190"
                                                                                                 IR="0.00034">2000.00
                    </option>
                </select></td>
            <td><select id="muestraalicuota4" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="0" IR="0">...</option>
                    <option <?php if ($ROW[0]['muestraalicuota4'] == '0.5') echo 'selected'; ?> value="0.5"
                                                                                                IEC="0.00310"
                                                                                                IR="0.0062">0.50
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota4'] == '1') echo 'selected'; ?> value="1" IEC="0.00390"
                                                                                              IR="0.0039">1.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota4'] == '2') echo 'selected'; ?> value="2" IEC="0.00390"
                                                                                              IR="0.0020">2.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota4'] == '3') echo 'selected'; ?> value="3" IEC="0.00580"
                                                                                              IR="0.0019">3.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota4'] == '4') echo 'selected'; ?> value="4" IEC="0.00590"
                                                                                              IR="0.0015">4.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota4'] == '5') echo 'selected'; ?> value="5" IEC="0.00590"
                                                                                              IR="0.0012">5.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota4'] == '6') echo 'selected'; ?> value="6" IEC="0.00600"
                                                                                              IR="0.0010">6.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota4'] == '7') echo 'selected'; ?> value="7" IEC="0.00620"
                                                                                              IR="0.0009">7.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota4'] == '10') echo 'selected'; ?> value="10" IEC="0.00970"
                                                                                               IR="0.0010">10.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota4'] == '15') echo 'selected'; ?> value="15" IEC="0.00620"
                                                                                               IR="0.0009">15.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota4'] == '20') echo 'selected'; ?> value="20" IEC="0.01470"
                                                                                               IR="0.0007">20.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota4'] == '25') echo 'selected'; ?> value="25" IEC="0.01540"
                                                                                               IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota4'] == '50') echo 'selected'; ?> value="50" IEC="0.02690"
                                                                                               IR="0.0005">50.00
                    </option>
                    <option <?php if ($ROW[0]['muestraalicuota4'] == '100') echo 'selected'; ?> value="100"
                                                                                                IEC="0.04650"
                                                                                                IR="0.0005">100.00
                    </option>
                </select></td>
            <td><select id="muestraaforadoA5" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="0" IR="0">...</option>
                    <option <?php if ($ROW[0]['muestraaforadoA5'] == '10') echo 'selected'; ?> value="10" IEC="0.01190"
                                                                                               IR="0.00119">10.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA5'] == '25') echo 'selected'; ?> value="25" IEC="0.01660"
                                                                                               IR="0.00066">25.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA5'] == '50') echo 'selected'; ?> value="50" IEC="0.02780"
                                                                                               IR="0.00056">50.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA5'] == '100') echo 'selected'; ?> value="100"
                                                                                                IEC="0.04690"
                                                                                                IR="0.00047">100.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA5'] == '200') echo 'selected'; ?> value="200"
                                                                                                IEC="0.08180"
                                                                                                IR="0.00041">200.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA5'] == '250') echo 'selected'; ?> value="250"
                                                                                                IEC="0.09880"
                                                                                                IR="0.00040">250.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA5'] == '500') echo 'selected'; ?> value="500"
                                                                                                IEC="0.18700"
                                                                                                IR="0.00037">500.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA5'] == '1000') echo 'selected'; ?> value="1000"
                                                                                                 IEC="0.34760"
                                                                                                 IR="0.00035">1000.00
                    </option>
                    <option <?php if ($ROW[0]['muestraaforadoA5'] == '2000') echo 'selected'; ?> value="2000"
                                                                                                 IEC="0.68190"
                                                                                                 IR="0.00034">2000.00
                    </option>
                </select></td>
            <td align="center" id="con4"></td>
        </tr>
        <tr>
            <td>Est&aacute;ndar Plaguicida</td>
            <td><input type="text" id="masa5" class="monto" value="<?= $ROW[0]['masa5'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><select id="aforadoA5" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="0" IR="0">...</option>
                    <option <?php if ($ROW[0]['aforadoA5'] == '10') echo 'selected'; ?> value="10" IEC="0.01190"
                                                                                        IR="0.00119">10.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA5'] == '25') echo 'selected'; ?> value="25" IEC="0.01660"
                                                                                        IR="0.00066">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA5'] == '50') echo 'selected'; ?> value="50" IEC="0.02780"
                                                                                        IR="0.00056">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA5'] == '100') echo 'selected'; ?> value="100" IEC="0.04690"
                                                                                         IR="0.00047">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA5'] == '200') echo 'selected'; ?> value="200" IEC="0.08180"
                                                                                         IR="0.00041">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA5'] == '250') echo 'selected'; ?> value="250" IEC="0.09880"
                                                                                         IR="0.00040">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA5'] == '500') echo 'selected'; ?> value="500" IEC="0.18700"
                                                                                         IR="0.00037">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA5'] == '1000') echo 'selected'; ?> value="1000" IEC="0.34760"
                                                                                          IR="0.00035">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA5'] == '2000') echo 'selected'; ?> value="2000" IEC="0.68190"
                                                                                          IR="0.00034">2000.00
                    </option>
                </select></td>
            <td><select id="plaguicidaalicuota3" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="0" IR="0">...</option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota3'] == '0.5') echo 'selected'; ?> value="0.5"
                                                                                                   IEC="0.00310"
                                                                                                   IR="0.0062">0.50
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota3'] == '1') echo 'selected'; ?> value="1" IEC="0.00390"
                                                                                                 IR="0.0039">1.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota3'] == '2') echo 'selected'; ?> value="2" IEC="0.00390"
                                                                                                 IR="0.0020">2.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota3'] == '3') echo 'selected'; ?> value="3" IEC="0.00580"
                                                                                                 IR="0.0019">3.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota3'] == '4') echo 'selected'; ?> value="4" IEC="0.00590"
                                                                                                 IR="0.0015">4.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota3'] == '5') echo 'selected'; ?> value="5" IEC="0.00590"
                                                                                                 IR="0.0012">5.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota3'] == '6') echo 'selected'; ?> value="6" IEC="0.00600"
                                                                                                 IR="0.0010">6.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota3'] == '7') echo 'selected'; ?> value="7" IEC="0.00620"
                                                                                                 IR="0.0009">7.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota3'] == '10') echo 'selected'; ?> value="10"
                                                                                                  IEC="0.00970"
                                                                                                  IR="0.0010">10.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota3'] == '15') echo 'selected'; ?> value="15"
                                                                                                  IEC="0.00620"
                                                                                                  IR="0.0009">15.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota3'] == '20') echo 'selected'; ?> value="20"
                                                                                                  IEC="0.01470"
                                                                                                  IR="0.0007">20.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota3'] == '25') echo 'selected'; ?> value="25"
                                                                                                  IEC="0.01540"
                                                                                                  IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota3'] == '50') echo 'selected'; ?> value="50"
                                                                                                  IEC="0.02690"
                                                                                                  IR="0.0005">50.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota3'] == '100') echo 'selected'; ?> value="100"
                                                                                                   IEC="0.04650"
                                                                                                   IR="0.0005">100.00
                    </option>
                </select></td>
            <td><select id="plaguicidaaforadoA4" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="0" IR="0">...</option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA4'] == '10') echo 'selected'; ?> value="10"
                                                                                                  IEC="0.01190"
                                                                                                  IR="0.00119">10.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA4'] == '25') echo 'selected'; ?> value="25"
                                                                                                  IEC="0.01660"
                                                                                                  IR="0.00066">25.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA4'] == '50') echo 'selected'; ?> value="50"
                                                                                                  IEC="0.02780"
                                                                                                  IR="0.00056">50.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA4'] == '100') echo 'selected'; ?> value="100"
                                                                                                   IEC="0.04690"
                                                                                                   IR="0.00047">100.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA4'] == '200') echo 'selected'; ?> value="200"
                                                                                                   IEC="0.08180"
                                                                                                   IR="0.00041">200.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA4'] == '250') echo 'selected'; ?> value="250"
                                                                                                   IEC="0.09880"
                                                                                                   IR="0.00040">250.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA4'] == '500') echo 'selected'; ?> value="500"
                                                                                                   IEC="0.18700"
                                                                                                   IR="0.00037">500.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA4'] == '1000') echo 'selected'; ?> value="1000"
                                                                                                    IEC="0.34760"
                                                                                                    IR="0.00035">1000.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA4'] == '2000') echo 'selected'; ?> value="2000"
                                                                                                    IEC="0.68190"
                                                                                                    IR="0.00034">2000.00
                    </option>
                </select></td>
            <td><select id="plaguicidaalicuota4" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="0" IR="0">...</option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota4'] == '0.5') echo 'selected'; ?> value="0.5"
                                                                                                   IEC="0.00310"
                                                                                                   IR="0.0062">0.50
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota4'] == '1') echo 'selected'; ?> value="1" IEC="0.00390"
                                                                                                 IR="0.0039">1.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota4'] == '2') echo 'selected'; ?> value="2" IEC="0.00390"
                                                                                                 IR="0.0020">2.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota4'] == '3') echo 'selected'; ?> value="3" IEC="0.00580"
                                                                                                 IR="0.0019">3.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota4'] == '4') echo 'selected'; ?> value="4" IEC="0.00590"
                                                                                                 IR="0.0015">4.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota4'] == '5') echo 'selected'; ?> value="5" IEC="0.00590"
                                                                                                 IR="0.0012">5.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota4'] == '6') echo 'selected'; ?> value="6" IEC="0.00600"
                                                                                                 IR="0.0010">6.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota4'] == '7') echo 'selected'; ?> value="7" IEC="0.00620"
                                                                                                 IR="0.0009">7.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota4'] == '10') echo 'selected'; ?> value="10"
                                                                                                  IEC="0.00970"
                                                                                                  IR="0.0010">10.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota4'] == '15') echo 'selected'; ?> value="15"
                                                                                                  IEC="0.00620"
                                                                                                  IR="0.0009">15.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota4'] == '20') echo 'selected'; ?> value="20"
                                                                                                  IEC="0.01470"
                                                                                                  IR="0.0007">20.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota4'] == '25') echo 'selected'; ?> value="25"
                                                                                                  IEC="0.01540"
                                                                                                  IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota4'] == '50') echo 'selected'; ?> value="50"
                                                                                                  IEC="0.02690"
                                                                                                  IR="0.0005">50.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaalicuota4'] == '100') echo 'selected'; ?> value="100"
                                                                                                   IEC="0.04650"
                                                                                                   IR="0.0005">100.00
                    </option>
                </select></td>
            <td><select id="plaguicidaaforadoA5" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="0" IR="0">...</option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA5'] == '10') echo 'selected'; ?> value="10"
                                                                                                  IEC="0.01190"
                                                                                                  IR="0.00119">10.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA5'] == '25') echo 'selected'; ?> value="25"
                                                                                                  IEC="0.01660"
                                                                                                  IR="0.00066">25.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA5'] == '50') echo 'selected'; ?> value="50"
                                                                                                  IEC="0.02780"
                                                                                                  IR="0.00056">50.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA5'] == '100') echo 'selected'; ?> value="100"
                                                                                                   IEC="0.04690"
                                                                                                   IR="0.00047">100.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA5'] == '200') echo 'selected'; ?> value="200"
                                                                                                   IEC="0.08180"
                                                                                                   IR="0.00041">200.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA5'] == '250') echo 'selected'; ?> value="250"
                                                                                                   IEC="0.09880"
                                                                                                   IR="0.00040">250.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA5'] == '500') echo 'selected'; ?> value="500"
                                                                                                   IEC="0.18700"
                                                                                                   IR="0.00037">500.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA5'] == '1000') echo 'selected'; ?> value="1000"
                                                                                                    IEC="0.34760"
                                                                                                    IR="0.00035">1000.00
                    </option>
                    <option <?php if ($ROW[0]['plaguicidaaforadoA5'] == '2000') echo 'selected'; ?> value="2000"
                                                                                                    IEC="0.68190"
                                                                                                    IR="0.00034">2000.00
                    </option>
                </select></td>
            <td align="center" id="con5"></td>
        </tr>
        <tr>
            <td>Est&aacute;ndar Interno</td>
            <td><input type="text" id="masa6" class="monto" value="<?= $ROW[0]['masa6'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><select id="aforadoA6" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['aforadoA6'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">
                        10.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA6'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                        IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA6'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                        IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA6'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                         IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA6'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                         IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA6'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                         IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA6'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                         IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA6'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                          IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoA6'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                          IR="0.0003">2000.00
                    </option>
                </select></td>
            <td><select id="alicuota1" <?= $disabled ?> onchange="__calcula()">
                    <option value="">...</option>
                    <option <?php if ($ROW[0]['alicuota'] == '0.5') echo 'selected'; ?> value="0.5">0.50</option>
                    <option <?php if ($ROW[0]['alicuota'] == '1') echo 'selected'; ?> value="1">1.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '2') echo 'selected'; ?> value="2">2.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '3') echo 'selected'; ?> value="3">3.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '4') echo 'selected'; ?> value="4">4.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '5') echo 'selected'; ?> value="5">5.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '6') echo 'selected'; ?> value="6">6.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '7') echo 'selected'; ?> value="7">7.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '10') echo 'selected'; ?> value="10">10.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '15') echo 'selected'; ?> value="15">15.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '20') echo 'selected'; ?> value="20">20.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '25') echo 'selected'; ?> value="25">25.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '50') echo 'selected'; ?> value="50">50.00</option>
                    <option <?php if ($ROW[0]['alicuota'] == '100') echo 'selected'; ?> value="100">100.00</option>
                </select></td>
            <td><select id="aforadoB4" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['aforadoB3'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">
                        10.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoB3'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                        IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoB3'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                        IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoB3'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                         IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoB3'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                         IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoB3'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                         IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoB3'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                         IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoB3'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                          IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['aforadoB3'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                          IR="0.0003">2000.00
                    </option>
                </select></td>
            <td><select id="internoalicuota1" <?= $disabled ?> onchange="__calcula()">
                    <option value="">...</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '0.5') echo 'selected'; ?> value="0.5">0.50</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '1') echo 'selected'; ?> value="1">1.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '2') echo 'selected'; ?> value="2">2.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '3') echo 'selected'; ?> value="3">3.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '4') echo 'selected'; ?> value="4">4.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '5') echo 'selected'; ?> value="5">5.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '6') echo 'selected'; ?> value="6">6.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '7') echo 'selected'; ?> value="7">7.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '10') echo 'selected'; ?> value="10">10.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '15') echo 'selected'; ?> value="15">15.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '20') echo 'selected'; ?> value="20">20.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '25') echo 'selected'; ?> value="25">25.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '50') echo 'selected'; ?> value="50">50.00</option>
                    <option <?php if ($ROW[0]['internoalicuota'] == '100') echo 'selected'; ?> value="100">100.00
                    </option>
                </select></td>
            <td><select id="internoaforadoB4" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['internoaforadoB3'] == '10') echo 'selected'; ?> value="10" IEC="0"
                                                                                               IR="0">10.00
                    </option>
                    <option <?php if ($ROW[0]['internoaforadoB3'] == '25') echo 'selected'; ?> value="25" IEC="0.015"
                                                                                               IR="0.0006">25.00
                    </option>
                    <option <?php if ($ROW[0]['internoaforadoB3'] == '50') echo 'selected'; ?> value="50" IEC="0.028"
                                                                                               IR="0.0006">50.00
                    </option>
                    <option <?php if ($ROW[0]['internoaforadoB3'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                                IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['internoaforadoB3'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                                IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['internoaforadoB3'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                                IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['internoaforadoB3'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                                IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['internoaforadoB3'] == '1000') echo 'selected'; ?> value="1000"
                                                                                                 IEC="0.348"
                                                                                                 IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['internoaforadoB3'] == '2000') echo 'selected'; ?> value="2000"
                                                                                                 IEC="0.674"
                                                                                                 IR="0.0003">2000.00
                    </option>
                </select></td>
            <td align="center" id="con6"></td>
        </tr>
        <tr>
            <td colspan="8">
                <hr/>
            </td>
        </tr>
        <tr>
            <td><strong>Dato</strong></td>
            <td><strong>&Aacute;rea 1</strong></td>
            <td><strong>&Aacute;rea 2</strong></td>
            <td><strong>&Aacute;rea 3</strong></td>
            <td><strong>&Aacute;rea promedio</strong></td>
            <td><strong>Desviaci&oacute;n Est&aacute;ndar</strong></td>
            <td><strong>% C.V.</strong></td>
        </tr>
        <tr>
            <td>&Aacute;rea Muestra 2</td>
            <td><input type="text" id="areaA5" class="monto" value="<?= $ROW[0]['areaA5'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaB5" class="monto" value="<?= $ROW[0]['areaB5'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaC5" class="monto" value="<?= $ROW[0]['areaC5'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td align="center" id="prom5"></td>
            <td align="center" id="desv8"></td>
            <td align="center" id="cv5"></td>
        </tr>
        <tr>
            <td>&Aacute;rea Est. Interno en la muestra 2</td>
            <td><input type="text" id="areaA6" class="monto" value="<?= $ROW[0]['areaA6'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaB6" class="monto" value="<?= $ROW[0]['areaB6'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaC6" class="monto" value="<?= $ROW[0]['areaC6'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td align="center" id="prom6"></td>
            <td align="center" id="desv9"></td>
            <td align="center" id="cv6"></td>
        </tr>
        <tr>
            <td>&Aacute;rea Est&aacute;ndar Plaguicida</td>
            <td><input type="text" id="areaA7" class="monto" value="<?= $ROW[0]['areaA7'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaB7" class="monto" value="<?= $ROW[0]['areaB7'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaC7" class="monto" value="<?= $ROW[0]['areaC7'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td align="center" id="prom7"></td>
            <td align="center" id="desv10"></td>
            <td align="center" id="cv7"></td>
        </tr>
        <tr>
            <td>&Aacute;rea Est. Interno en el est&aacute;ndar de plaguicida 2</td>
            <td><input type="text" id="areaA8" class="monto" value="<?= $ROW[0]['areaA8'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaB8" class="monto" value="<?= $ROW[0]['areaB8'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td><input type="text" id="areaC8" class="monto" value="<?= $ROW[0]['areaC8'] ?>" <?= $disabled ?>
                       onblur="Redondear(this)"></td>
            <td align="center" id="prom8"></td>
            <td align="center" id="desv11"></td>
            <td align="center" id="cv8"></td>
        </tr>
        <tr>
            <td>Relaci&oacute;n &Aacute;rea Muestra 2 / &Aacute;rea Est&aacute;ndar plaguicida 2</td>
            <td align="center" id="resA4"></td>
            <td align="center" id="resB4"></td>
            <td align="center" id="resC4"></td>
            <td align="center" id="resD4"></td>
            <td align="center" id="desv12"></td>
            <td align="center" id="resE4"></td>
        </tr>
        <tr>
            <td>Relaci&oacute;n &Aacute;rea Muestra 2 / &Aacute;rea Est. interno en muestra 2</td>
            <td align="center" id="resA5"></td>
            <td align="center" id="resB5"></td>
            <td align="center" id="resC5"></td>
            <td align="center" id="resD5"></td>
            <td align="center" id="desv13"></td>
            <td align="center" id="resE5"></td>
        </tr>
        <tr>
            <td>Relaci&oacute;n &Aacute;rea Est. Plag. 2 / &Aacute;rea Est. interno en Est. Plag. 2</td>
            <td align="center" id="resA6"></td>
            <td align="center" id="resB6"></td>
            <td align="center" id="resC6"></td>
            <td align="center" id="resD6"></td>
            <td align="center" id="desv14"></td>
            <td align="center" id="resE6"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Reporte de resultados: Sin Curva de Calibraci&oacute;n</td>
        </tr>
        <tr align="center">
            <td><strong>Muestra</strong></td>
            <td><strong>Ingrediente activo sin<br/>est&aacute;ndar interno</strong></td>
            <td><strong>Ingrediente activo con<br/>est&aacute;ndar interno</strong></td>
            <td><strong>Observaciones</strong></td>
        </tr>
        <tr align="center">
            <td>1</td>
            <td id="mue1A"></td>
            <td id="mue1B"></td>
            <td rowspan="4"><textarea id="obs" style="width:90%" <?= $disabled ?>><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
        <tr align="center">
            <td>2</td>
            <td id="mue2A"></td>
            <td id="mue2B"></td>
        </tr>
        <tr align="center">
            <td>Contenido Promedio de I.A.</td>
            <td id="promA"></td>
            <td id="promB"></td>
        </tr>
        <tr align="center">
            <td>Incertidumbre expandida</td>
            <td id="ieA"></td>
            <td id="ieB"></td>
        </tr>
        <tr>
            <td class="titulo" colspan="6">Formulas</td>
        </tr>
        <tr>
            <td colspan="6">
                <p>
                <h3>M&eacute;todo est&aacute;ndar externo:</h3></p>
                <p style="text-align: center"><img src="imagenes/H0003/f1.PNG" width="698" height="323" alt="f1"/>
                </p>
                </br>
                <p>
                <h3>M&eacute;todo est&aacute;ndar interno:</h3></p>
                <p style="text-align: center">
                    <img src="imagenes/H0003/f2.PNG" width="704" height="308" alt="f2"/>
                </p>

                <p><b>Donde:</b></p>
                <p>M muestra = Masa de muestra pesada.</p>
                <p>M est&aacute;ndar = Masa de est&aacute;ndar anal&iacute;tico pesada.</p>
                <p>P est&aacute;ndar = Pureza est&aacute;ndar.</p>
                <p>Vol matraz 1, 2, 3 = Volumen de diluci&oacute;n correspondiente, ya sea de muestra o
                    est&aacute;ndar.</p>
                <p>Prom &aacute;rea muestra = Promedio de 3 r&eacute;plicas de inyecci&oacute;n de muestra diluida.</p>
                <p>Prom &aacute;rea est&aacute;ndar = Promedio de 3 r&eacute;plicas de inyecci&oacute;n de est&aacute;ndar
                    diluido.</p>
                <p>&#961; muestra = Densidad de la muestra.</p>
                <p>Prom &aacute;rea est int muestra = Promedio del &aacute;rea de est&aacute;ndar interno insertado en
                    la muestra diluida.</p>
                <p>Prom &aacute;rea est int est&aacute;ndar = Promedio del &aacute;rea de est&a&aacute;ndar interno
                    insertado en el est&aacute;ndar.</p>

            </td>
        </tr>
    </table>
    <br/>
    <input type="hidden" id="final_P"/>
    <input type="hidden" id="final_I"/>
    <br/>
    <?php if ($_GET['acc'] == 'V') { ?>
        <script>__calcula();</script>
        <script>escondeDetector(<?=$ROW[0]['tipo_d']?>);</script>
    <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php }else{ ?>

    <input type="button" id="btn" value="Aceptar" class="boton"  onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('H0003', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
<?php if ($_GET['acc'] == 'I') { ?>
    <script>escondeDetector('');</script>
<?php } ?>
</body>
</html>