<?php
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_dumas();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

if ($_GET['acc'] == 'V') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="xanalizar" value="<?= $_GET['xanalizar'] ?>"/>
<input type="hidden" id="tipo" value="<?= $_GET['tipo'] ?>"/>
<center>
    <?php $Gestor->Incluir('h5', 'hr', 'An&aacute;lisis :: Determinaci&oacute;n de Nitr&oacute;geno por M&eacute;todo de Dumas') ?>
    <?= $Gestor->Encabezado('H0005', 'e', 'Determinaci&oacute;n de Nitr&oacute;geno por M&eacute;todo de Dumas') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Datos de la muestra</td>
        </tr>
        <tr>
            <td><strong>N&uacute;mero de muestra:</strong></td>
            <td><?= $ROW[0]['ref'] ?></td>
            <td><strong>Solicitud:</strong></td>
            <td><?= $ROW[0]['solicitud'] ?></td>
        </tr>
        <tr>
            <td><strong>Fecha de ingreso:</strong></td>
            <td><?= $ROW[0]['fechaI'] ?></td>
            <td><strong>Fecha de an&aacute;lisis:</strong></td>
            <td><input type="text" id="fechaA" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaA'] ?>" <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Fecha de conclusi&oacute;n del an&aacute;lisis:</strong></td>
            <td><?= $ROW[0]['fechaC'] ?></td>
            <td><strong>Concentraci&oacute;n declarada:</strong></td>
            <td><input type="text" id="rango" maxlength="20" class="monto" value="<?= $ROW[0]['rango'] ?>"
                    <?= $disabled ?>>&nbsp;
                <select id="unidad" <?php if ($ROW[0]['unidad'] != '' or $_GET['acc'] == 'V') echo 'disabled'; ?>
                        onchange="__calcula()">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['unidad'] == '0') echo 'selected'; ?>>%m/m</option>
                    <option value="1" <?php if ($ROW[0]['unidad'] == '1') echo 'selected'; ?>>%m/v</option>
                </select>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="6">C&aacute;lculos</td>
        </tr>
        <tr align="center">
            <td></td>
            <td><strong>Muestra 1</strong></td>
            <td><strong>Muestra 2</strong></td>
            <td><strong>Muestra 3</strong></td>
            <td><strong>Inc./Desv. Est.</strong></td>

        </tr>
        <tr align="center">
            <td>Masa de nitr&oacute;geno total(mg)</td>
            <td><input type="text" id="nitro1" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['nitro1'] ?>"
                    <?= $disabled ?>></td>
            <td><input type="text" id="nitro2" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['nitro2'] ?>"
                    <?= $disabled ?>></td>
            <td><input type="text" id="nitro3" class="monto" onblur="Redondear(this);" value="<?= isset($ROW[0]['nitro3']) == true ? $ROW[0]['nitro3'] :'0' ?>"
                    <?= $disabled ?>></td>
            <td id="desv1"></td>

        </tr>
        <tr align="center">
            <td>Masa de muestra (mg)</td>
            <td><input type="text" id="masa1" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['masa1'] ?>"
                    <?= $disabled ?>></td>
            <td><input type="text" id="masa2" class="monto" onblur="Redondear(this);" value="<?= $ROW[0]['masa2'] ?>"
                    <?= $disabled ?>></td>
            <td><input type="text" id="masa3" class="monto" onblur="Redondear(this);" value="<?= isset($ROW[0]['masa3']) == true ? $ROW[0]['masa3'] :'0'?>"
                    <?= $disabled ?>></td>
            <td id="desv2"></td>

        </tr>
        <tr align="center">
            <td>% m/m</td>
            <td id="mm1"></td>
            <td id="mm2"></td>
            <td id="mm3"></td>
            <td colspan="1"></td>
        </tr>
        <tr align="center">
            <td>Inc. C&aacute;lculo m/m</td>
            <td id="incCal1"></td>
            <td id="incCal2"></td>
            <td id="incCal3"></td>
            <td colspan="1"></td>
        </tr>


        <tr align="center">
            <td>DE</td>
            <td id="desv3"></td>
            <td colspan="2"></td>
        </tr>
        <tr align="center">
            <td>Inc. por DE</td>
            <td id="desv4"></td>
            <td colspan="2"></td>
        </tr>
        <tr align="center">
            <td>d</td>
            <td><input type="text" id="delta1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['delta1'] ?>"
                    <?= $disabled ?>></td>
            <td></td>
            <td></td>
            <td id="d_inc"</td>

            <!--    <td id="delta3"></td>-->
        </tr>
        <tr align="center">
            <td>% m/v</td>
            <td id="mv1"></td>
            <td id="mv2"></td>
            <td id="mv3"></td>
            <td colspan="1"></td>
        </tr>


        <tr align="center">
            <td>Inc. C&aacute;lculo m/v</td>
            <td id="incCal4"></td>
            <td id="incCal5"></td>
            <td id="incCal6"></td>
            <td colspan="1"></td>
        </tr>


        <tr align="center">
            <td>DE</td>
            <td id="desv5"></td>
            <td colspan="2"></td>
        </tr>
        <tr align="center">
            <td>Inc. por DE</td>
            <td id="desv6"></td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="5">
                <hr/>
            </td>
        </tr>
        <tr align="center">
            <td></td>
            <td><strong>Promedio</strong></td>
            <td colspan="3" rowspan="7"></td>
        </tr>
        <tr align="center" style="color:#009900">
            <td>%m/m Nitr&oacute;geno</td>
            <td id="prom1"></td>
        </tr>
        <tr align="center" style="color:#009900">
            <td>Incertidumbre por DE</td>
            <td id="prom2"></td>
        </tr>
        <tr align="center">
            <td>I.F.C</td>
            <td id="prom3"></td>
        </tr>
        <tr align="center" style="color:#009900">
            <td>%m/v Nitr&oacute;geno</td>
            <td id="prom4"></td>
        </tr>
        <tr align="center" style="color:#009900">
            <td>Incertidumbre por DE</td>
            <td id="prom5"></td>
        </tr>
        <tr align="center">
            <td>I.F.C</td>
            <td id="prom6"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="10">Componentes de incertidumbre</td>
        </tr>
        <tr align="center">
            <td colspan="2" width="35%">
                <!-- -->
                <table class="radius" width="100%">
                    <tr align="center">
                        <td colspan="2"><strong>Lectura de equipo</strong></td>
                        <td>Componente</td>
                    </tr>
                    <tr align="center">
                        <td>Repetibilidad</td>
                        <td><input type="text" id="repetibilidad1" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['repetibilidad1'] ?>" <?= $disabled ?>></td>
                        <td id="rep_comp1"></td>
                    </tr>
                    <tr align="center">
                        <td>Resoluci&oacute;n</td>
                        <td><input type="text" id="resolucion1" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['resolucion1'] ?>" <?= $disabled ?>></td>
                        <td id="res_comp1"></td>
                    </tr>
                    <tr align="center">
                        <td>EMP</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr align="center" hidden>
                        <td>Precisi&oacute;n</td>
                        <td id="precision"></td>
                        <td id="pre_comp"></td>
                    </tr>
                    <tr align="center">
                        <td>Curva de calibraci&oacute;n</td>
                        <td><input type="text" id="curva" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['curva'] ?>" <?= $disabled ?>></td>
                        <td id="cur_comp"></td>
                    </tr>
                    <tr align="center">
                        <td colspan="2"><strong>IC Equipo</strong></td>
                        <td id="inc_equipo" style="font-weight:bold"></td>
                    </tr>
                </table>
                <!-- -->
            </td>
            <td>&nbsp;</td>
            <td colspan="2" width="35%">
                <!-- -->
                <table class="radius" width="100%">
                    <tr align="center">
                        <td colspan="2"><strong>Masa de muestra</strong></td>
                        <td>Componente</td>
                    </tr>
                    <tr align="center">
                        <td>Repetibilidad</td>
                        <td><input type="text" id="repetibilidad2" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['rep'] ?>" <?= $disabled ?>></td>
                        <td id="rep_comp2"></td>
                    </tr>
                    <tr align="center">
                        <td>Resoluci&oacute;n</td>
                        <td><input type="text" id="resolucion2" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['resM'] ?>" <?= $disabled ?>></td>
                        <td id="res_comp2"></td>
                    </tr>
                    <tr align="center">
                        <td>Linealidad</td>
                        <td><input type="text" id="inc_cer" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['lilM'] ?>" <?= $disabled ?>></td>
                        <td id="cer_comp"></td>
                    </tr>
                    <tr align="center">
                        <td>Excentricidad</td>
                        <td><input type="text" id="emp" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['exM'] ?>" <?= $disabled ?>></td>
                        <td id="emp_comp"></td>
                    </tr>
                    <tr align="center">
                        <td colspan="2"><strong>IC Balanza</strong></td>
                        <td id="inc_balanza" style="font-weight:bold"></td>
                    </tr>
                </table>
                <!-- -->
            </td>
            <td>&nbsp;</td>

            <td colspan="2" width="25%" height="">
                <!-- -->
                <table class="radius" width="100%">
                    <tr align="center">
                        <td colspan="2"><strong>Incertidumbre por Densidad</strong></td>
                        <td>Componente</td>
                    </tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr align="center">
                        <td>Incertidumbre por Certificado</td>
                        <td><input type="text" id="incCertificado" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['icCert'] ?>" <?= $disabled ?>></td>
                        <td id="res_incCertificado"></td>
                    </tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr align="center">
                        <td>Resoluci&oacute;n</td>
                        <td><input type="text" id="incResolucion" class="monto" onblur="Redondear(this);"
                                   value="<?= $ROW[0]['icRes'] ?>" <?= $disabled ?>></td>
                        <td id="res_incResolucion"></td>
                    </tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>



                    <tr align="center">
                        <td colspan="2"><strong>IC Combinada</strong></td>
                        <td id="res_incCombinada" style="font-weight:bold"></td>
                    </tr>
                </table>
                <!-- -->
            </td>
        </tr>
    </table>

    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo">Observaciones</td>
        </tr>
        <tr>
            <td><textarea id="obs" style="width:98%" <?= $disabled ?>><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
        <tr>
            <td class="titulo" colspan="6">Formulas</td>
        </tr>
        <tr>
            <td colspan="6">
                <p style="text-align: center">
                    <img src="imagenes/H0005/f1.PNG" width="195" height="143" alt="f1"/>
                </p>
                <p><b>Donde:</b></p>
                <p>m<sub>N</sub>: masa de nitr&oacute;geno identificada en el equipo en mg.</p>
                <p>m: masa de muestra en mg.</p>
                <p>&#961;: densidad en g/mL</p>
                <p>% m/m = g / 100 g </p>
                <p>% m/v = g / 100 mL </p>

            </td>
        </tr>
    </table>
    <br/>
    <?php
    if ($_GET['acc'] == 'V') {
        $ROW2 = $Gestor->Analista();
        echo "<br /><table class='radius' width='98%'><tr><td><strong>Realizado por:</strong> {$ROW2[0]['analista']}</td></tr></table>";
    }
    ?>
    <br/>
    <script>__calcula();</script>
    <?php if ($_GET['acc'] == 'V') { ?>
        <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php } else { ?>
        <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('H0005', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>