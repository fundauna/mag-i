<?php
//OK
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_etefon();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

if ($_GET['acc'] == 'V') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="xanalizar" value="<?= $_GET['xanalizar'] ?>"/>
<input type="hidden" id="tipo" value="<?= $_GET['tipo'] ?>"/>
<center>
    <?php $Gestor->Incluir('h8', 'hr', 'An&aacute;lisis :: Determinaci&oacute;n de Etef&oacute;n en Formulaciones de Plaguicidas por Volumetr&iacute;a') ?>
    <?= $Gestor->Encabezado('H0008', 'e', 'Determinaci&oacute;n de Etef&oacute;n en Formulaciones de Plaguicidas por Volumetr&iacute;a') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="3">Datos de la muestra</td>
        </tr>
        <tr>
            <td><strong>N&uacute;mero:</strong></td>
            <td><?= $ROW[0]['ref'] ?></td>
            <td><strong>Ingrediente Activo:</strong></td>
        </tr>
        <tr>
            <td><strong>Fecha de ingreso:</strong></td>
            <td><?= $ROW[0]['fechaI'] ?></td>
            <td><?= $ROW[0]['ingrediente'] ?><input type="hidden" id="ingrediente" maxlength="30"
                                                    value="<?= $ROW[0]['ingrediente'] ?>" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>Fecha de an&aacute;lisis:</strong></td>
            <td><input type="text" id="fechaA" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaA'] ?>" <?= $disabled ?>></td>
            <td><strong>Tipo de formulaci&oacute;n:</strong></td>
        </tr>
        <tr>
            <td><strong>Fecha de conclusi&oacute;n del an&aacute;lisis:</strong></td>
            <td><?= $ROW[0]['fechaC'] ?></td>
            <td><?= $ROW[0]['tipo_form'] ?><input type="hidden" id="tipo_form" value="<?= $ROW[0]['tipo_form'] ?>"/>
            </td>
        </tr>
        <tr>
            <td><strong>Concentraci&oacute;n declarada:</strong></td>
            <td><input type="text" id="rango" maxlength="20" class="monto" value="<?= $ROW[0]['rango'] ?>"
                       <?= $disabled ?>>&nbsp;
                <select id="unidad" <?php if ($ROW[0]['unidad'] != '' or $_GET['acc'] == 'V') echo 'disabled'; ?>
                        onchange="__calcula()">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['unidad'] == '0') echo 'selected'; ?>>%m/m</option>
                    <option value="1" <?php if ($ROW[0]['unidad'] == '1') echo 'selected'; ?>>%m/v</option>
                </select>
            </td>
            <td><?php if ($_GET['acc'] == 'V') { ?><strong>Creado por:</strong> <?= $ROW[0]['analista'] ?><?php } ?>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px; visibility:hidden" width="98%">
        <tr>
            <td class="titulo" colspan="6">Par&aacute;metros del equipo</td>
        </tr>
        <tr>
            <td><strong>Linealidad muestra 1:</strong></td>
            <td><input type="text" id="linealidad1" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['linealidad1'] ?>" <?= $disabled ?>></td>
            <td><strong>Linealidad muestra 2:</strong></td>
            <td><input type="text" id="linealidad2" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['linealidad2'] ?>" <?= $disabled ?>></td>
            <td><strong>IEC Bureta:</strong></td>
            <td><input type="text" id="IECB" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['IECB'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Repetibilidad muestra 1:</strong></td>
            <td><input type="text" id="repeti1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['repeti1'] ?>"
                       <?= $disabled ?>></td>
            <td><strong>Repetibilidad muestra 2:</strong></td>
            <td><input type="text" id="repeti2" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['repeti2'] ?>"
                       <?= $disabled ?>></td>
            <td colspan="2"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="6">Datos de an&aacute;lisis</td>
        </tr>
        <tr>
            <td>Masa Molar Etef&oacute;n (g/mol):</td>
            <td><input type="text" id="masa" class="monto" value="<?= $ROW[0]['masa'] ?>" <?= $disabled ?>></td>
            <td></td>
            <td><strong>Dato</strong></td>
            <td><strong>Muestra 1</strong></td>
            <td><strong>Muestra 2</strong></td>
        </tr>
        <tr>
            <td>N&uacute;mero de reactivo:</td>
            <td><input type="text" id="numreactivo" maxlength="30" value="<?= $ROW[0]['numreactivo'] ?>"
                       <?= $disabled ?>></td>
            <td></td>
            <td>Masa muestra (g)</td>
            <td><input type="text" id="muestra1" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['muestra1'] ?>" <?= $disabled ?>></td>
            <td><input type="text" id="muestra2" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['muestra2'] ?>" <?= $disabled ?>></td>
        </tr>
        <tr>
            <td>Fecha de preparaci&oacute;n:</td>
            <td><input type="text" id="fechaP" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaP'] ?>" <?= $disabled ?>></td>
            <td></td>
            <td>Vol. NaOH consumido (mL)</td>
            <td><input type="text" id="consumido1" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['consumido1'] ?>" <?= $disabled ?>></td>
            <td><input type="text" id="consumido2" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['consumido2'] ?>" <?= $disabled ?>></td>
        </tr>
        <tr>
            <td>Conc. Disln NaOH (mol/L):</td>
            <td><input type="text" id="yodo" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['yodo'] ?>"
                       <?= $disabled ?>></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Inc Conc. Disln NaOH (mol/L):</td>
            <td><input type="text" id="yodo2" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['yodo2'] ?>"
                       <?= $disabled ?>></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Densidad muestra, p (g/mL):</td>
            <td><input type="text" id="densidad" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['densidad'] ?>" <?= $disabled ?>></td>
            <td></td>
            <td id="lbl1"></td>
            <td><input type="text" readonly id="contenido1" class="monto"></td>
            <td><input type="text" readonly id="contenido2" class="monto"></td>
        </tr>
        <tr>
            <td colspan="6">
                <hr/>
            </td>
        </tr>
        <tr>
            <td colspan="3">Observaciones</td>
            <td><strong>Contenido de Etef&oacute;n como</strong></td>
            <td><strong>Promedio</strong></td>
            <td><strong>Inc. expandida</strong></td>
        </tr>
        <tr valign="top">
            <td colspan="3" rowspan="2"><textarea id="obs" style="width:80%"
                                                  <?= $disabled ?>><?= $ROW[0]['obs'] ?></textarea></td>
            <td id="lbl2"></td>
            <td><input type="text" readonly id="promedio" class="monto"></td>
            <td><input type="text" readonly id="incertidumbre" class="monto"></td>
        </tr>
        <tr></tr>
        <tr>
            <td class="titulo" colspan="6">Formulas</td>
        </tr>
        <tr>
            <td colspan="6">
                <p><b>En caso de que se deba estandarizar la disoluci&oacute;n dehidr&oacute;xido de sodio:</b></p>
                <p style="text-align: center">
                    <img src="imagenes/H0008/f1.PNG" width="461" height="100" alt="f1"/>
                </p>

                <p><b>Donde:</b></p>
                <p>Masa KHP = Masa de fosfato &aacute;cido de potasio, patr&&oacute;n primario, pesada para estandarizar
                    la disoluci&oacute;n de hidr&oacute;xido de sodio.</p>
                <p>P KHP = Pureza del fosfato &aacute;cido de potasio, patr&o&oacute; primario.</p>
                <p>Vol NaOH = Volumen consumido de la disoluci&oacute;n de hidr&oacute;xido de sodio que se desea
                    estandarizar.</p>
                <p>MM KHP = Masa molar del fosfato &aacute;cido de potasio, patr&oacute;n primario.</p>

                <p><b>Para calcular la concentraci&oacute;n de etef&oacute;n:</b></p>

                <p style="text-align: center">
                    <img src="imagenes/H0008/f2.PNG" width="688" height="161" alt="f2"/>
                </p>
                <p><b>Donde:</b></p>
                <p>VolNaOH = Volumen consumido de la disoluci&oacute;n de hidr&oacute;xido en la valoraci&oacute;n del
                    etef&oacute;n.</p>
                <p>CnNaOH = Concentraci&o&oacute;n de la disoluci&oacute;n estandarizada de hidr&oacute;xido de
                    sodio.</p>
                <p>MM etef&oacute;n = Masa molar del etef&oacute;n.</p>
                <p>M muestra = Masa de muestra.</p>
                <p>&#961; = Densidad de la muestra.</p>


            </td>
        </tr>
    </table>
    <br/>
    <br/>
    <?php if ($_GET['acc'] == 'V') { ?>
        <script>__calcula();</script>
    <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php }else{ ?>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('H0008', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>