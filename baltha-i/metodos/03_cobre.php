<?php
//OK
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_cobre();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW)
    die('Registro inexistente');

if ($_GET['acc'] == 'V')
    $disabled = 'disabled';
else
    $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="xanalizar" value="<?= $_GET['xanalizar'] ?>"/>
<input type="hidden" id="tipo" value="<?= $_GET['tipo'] ?>"/>
<center>
    <?php $Gestor->Incluir('h2', 'hr', 'An&aacute;lisis :: Determinaci&oacute;n de Cobre en Formulaciones de Plaguicidas por Volumetr&iacute;a') ?>
    <?= $Gestor->Encabezado('H0002', 'e', 'Determinaci&oacute;n de Cobre en Formulaciones de Plaguicidas por Volumetr&iacute;a') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="3">Datos de la muestra</td>
        </tr>
        <tr>
            <td><strong>N&uacute;mero:</strong></td>
            <td><?= $ROW[0]['ref'] ?></td>
            <td><strong>Ingrediente Activo:</strong></td>
        </tr>
        <tr>
            <td><strong>Fecha de ingreso:</strong></td>
            <td><?= $ROW[0]['fechaI'] ?></td>
            <td><input type="text" id="ingrediente" maxlength="30" value="<?= $ROW[0]['ingrediente'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>Fecha de an&aacute;lisis:</strong></td>
            <td><input type="text" id="fechaA" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaA'] ?>" <?= $disabled ?>></td>
            <td></td>
        </tr>
        <tr>
            <td><strong>Fecha de conclusi&oacute;n del an&aacute;lisis:</strong></td>
            <td><?= $ROW[0]['fechaC'] ?></td>
            <td><strong>Tipo de formulaci&oacute;n:</strong></td>
        </tr>
        <tr>
            <td><strong>Concentraci&oacute;n declarada:</strong></td>
            <td><input type="text" id="rango" class="monto" value="<?= $ROW[0]['rango'] ?>" <?= $disabled ?>>&nbsp;
                <select id="unidad" <?php if ($ROW[0]['unidad'] != '' or $_GET['acc'] == 'V') echo 'disabled'; ?>
                        onchange="__calcula()">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['unidad'] == '0') echo 'selected'; ?>>%m/m</option>
                    <option value="1" <?php if ($ROW[0]['unidad'] == '1') echo 'selected'; ?>>%m/v</option>
                </select>
            </td>
            <td><?= $ROW[0]['tipo_form'] ?><input type="hidden" id="tipo_form" value="<?= $ROW[0]['tipo_form'] ?>"/>
            </td>
        </tr>
        <?php if ($_GET['acc'] == 'V') { ?>
            <tr>
                <td><strong>Creado por:</strong></td>
                <td colspan="2"><?= $ROW[0]['analista'] ?></td>
            </tr>
        <?php } ?>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="3">Imformaci&oacute;n adicional</td>
        </tr>
        <tr align="center">
            <td>Metodolog&iacute;a:</td>
            <td>
                <select id="metodo" <?= $disabled ?> onchange="__calcula()" style="width:100px;">
                    <option value="A">Metodo A: Muestras que contienen m&aacute;s de 20% de Cu y que no contienen Cu2O
                    </option>
                    <option value="B" <?php if ($ROW[0]['metodo'] == 'B') echo 'selected'; ?>>Metodo B: Muestras que NO
                        contienen m&aacute;s de 20% de Cu y que s&iacute; contienen Cu2O
                    </option>
                    <option value="C" <?php if ($ROW[0]['metodo'] == 'C') echo 'selected'; ?>>Metodo C: Muestras que
                        contienen m&aacute;s de 20% de Cu y/o contienen Cu2O
                    </option>
                    <option value="D" <?php if ($ROW[0]['metodo'] == 'D') echo 'selected'; ?>>Cobre Met&aacute;lico
                    </option>
                </select>
            </td>
            <td>
                <table class="radius" width="90%">
                    <tr>
                        <td colspan="4" align="center"><strong>Plaguicidas</strong></td>
                    </tr>
                    <tr align="center">
                        <td><strong>Nombre</strong></td>
                        <td><strong>F&oacute;rmula</strong></td>
                        <td><strong>Masa molecular (g/mol)</strong></td>
                        <td><strong>Factor gravim&eacute;trico</strong></td>
                    </tr>
                    <tr align="center">
                        <td>Cobre</td>
                        <td>Cu</td>
                        <td>63,546</td>
                        <td>1,00</td>
                    </tr>
                    <tr align="center">
                        <td>Carbonato de cobre</td>
                        <td>CuCO<sub>3</sub> &bull; Cu(OH)<sub>2</sub></td>
                        <td>221,102</td>
                        <td>1,74</td>
                    </tr>
                    <tr align="center">
                        <td>Oxicloruro de cobre</td>
                        <td>CuCl<sub>2</sub> &bull; 3(Cu(OH)<sub>2</sub>)</td>
                        <td>424,107</td>
                        <td>1,68</td>
                    </tr>
                    <tr align="center">
                        <td>Sulfato de cobre (II)</td>
                        <td>CuSO<sub>4</sub> &bull; 5 H<sub>2</sub>O</td>
                        <td>249,686</td>
                        <td>3,93</td>
                    </tr>
                    <tr align="center">
                        <td>&Oacute;xido de cobre (I)</td>
                        <td>Cu<sub>2</sub>O</td>
                        <td>143,079</td>
                        <td>1,13</td>
                    </tr>
                    <tr align="center">
                        <td>Hidr&oacute;xido de cobre (II)</td>
                        <td>Cu(OH)<sub>2</sub></td>
                        <td>97,554</td>
                        <td>1,54</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="6">Par&aacute;metros del equipo</td>
        </tr>
        <tr>
            <td><strong>Linealidad muestra 1:</strong></td>
            <td><input type="text" id="linealidad1" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['linealidad1'] ?>" <?= $disabled ?>></td>
            <td><strong>Linealidad muestra 2:</strong></td>
            <td><input type="text" id="linealidad2" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['linealidad2'] ?>" <?= $disabled ?>></td>
            <td><strong>IEC Bureta:</strong></td>
            <td><input type="text" id="IECB" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['IECB'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Repetibilidad muestra 1:</strong></td>
            <td><input type="text" id="repeti1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['repeti1'] ?>"
                       <?= $disabled ?>></td>
            <td><strong>Repetibilidad muestra 2:</strong></td>
            <td><input type="text" id="repeti2" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['repeti2'] ?>"
                       <?= $disabled ?>></td>
            <td colspan="2"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Datos y resultados del an&aacute;lisis</td>
        </tr>
        <tr>
            <td>N&uacute;mero de Reactivo:</td>
            <td><input type="text" id="numreactivo" maxlength="30" value="<?= $ROW[0]['numreactivo'] ?>"
                       <?= $disabled ?>></td>
            <td>Conc. Disln Na<sub>2</sub>S<sub>2</sub>O<sub>3</sub> (mol/L) :</td>
            <td id="res1"></td>
        </tr>
        <tr>
            <td>Fecha de preparaci&oacute;n:</td>
            <td><input type="text" id="fechaP" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaP'] ?>" <?= $disabled ?>></td>
            <td>Capacidad de la Bureta (mL)</td>
            <td><select id="bureta" <?= $disabled ?> onchange="__calcula()">
                    <option value="">...</option>
                    <option <?php if ($ROW[0]['bureta'] == '10') echo 'selected'; ?> value="10">10.00</option>
                    <option <?php if ($ROW[0]['bureta'] == '25') echo 'selected'; ?> value="25">25.00</option>
                    <option <?php if ($ROW[0]['bureta'] == '50') echo 'selected'; ?> value="50">50.00</option>
                    <option <?php if ($ROW[0]['bureta'] == '100') echo 'selected'; ?> value="100">100.00</option>
                </select></td>
        </tr>
        <tr>
            <td>Cn de Na<sub>2</sub>S<sub>2</sub>O<sub>3</sub> en la ampolla:</td>
            <td><input type="text" id="ampolla1" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['ampolla1'] ?>" <?= $disabled ?>/></td>
            <td>Densidad muestra, r (g/mL):</td>
            <td><input type="text" id="densidad" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['densidad'] ?>" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td>Inc. Cn de Na<sub>2</sub>S<sub>2</sub>O<sub>3</sub> en ampolla:</td>
            <td><input type="text" id="ampolla2" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['ampolla2'] ?>" <?= $disabled ?>/></td>
            <td>Factor gravim&eacute;trico:</td>
            <td><select id="factor" <?= $disabled ?> onchange="__calcula()">
                    <option value="">...</option>
                    <option <?php if ($ROW[0]['factor'] == '1') echo 'selected'; ?> value="1">1.00</option>
                    <option <?php if ($ROW[0]['factor'] == '1.74') echo 'selected'; ?> value="1.74">1.74</option>
                    <option <?php if ($ROW[0]['factor'] == '1.6807') echo 'selected'; ?> value="1.6807">1.68</option>
                    <option <?php if ($ROW[0]['factor'] == '3.929') echo 'selected'; ?> value="3.929">3.93</option>
                    <option <?php if ($ROW[0]['factor'] == '1.1307') echo 'selected'; ?> value="1.1307">1.13</option>
                    <option <?php if ($ROW[0]['factor'] == '1.5396') echo 'selected'; ?> value="1.5396">1.54</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Vol B. Aforado (mL):</td>
            <td><select id="balon" <?= $disabled ?> onchange="__calcula()">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['balon'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">10.00
                    </option>
                    <option <?php if ($ROW[0]['balon'] == '25') echo 'selected'; ?> value="25" IEC="0.015" IR="0.0006">
                        25.00
                    </option>
                    <option <?php if ($ROW[0]['f'] == '50') echo 'selected'; ?> value="50" IEC="0.028" IR="0.0006">
                        50.00
                    </option>
                    <option <?php if ($ROW[0]['balon'] == '100') echo 'selected'; ?> value="100" IEC="0.047"
                                                                                     IR="0.0005">100.00
                    </option>
                    <option <?php if ($ROW[0]['balon'] == '200') echo 'selected'; ?> value="200" IEC="0.082"
                                                                                     IR="0.0004">200.00
                    </option>
                    <option <?php if ($ROW[0]['balon'] == '250') echo 'selected'; ?> value="250" IEC="0.099"
                                                                                     IR="0.0004">250.00
                    </option>
                    <option <?php if ($ROW[0]['balon'] == '500') echo 'selected'; ?> value="500" IEC="0.180"
                                                                                     IR="0.0004">500.00
                    </option>
                    <option <?php if ($ROW[0]['balon'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                      IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['balon'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                      IR="0.0003">2000.00
                    </option>
                </select></td>
            <td>Factor diluci&oacute;n:</td>
            <td id="res2"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr align="center">
            <td rowspan="2"><strong>Dato</strong></td>
            <td colspan="2"><strong>Metodolog&iacute;as A y C</strong></td>
            <td colspan="2"><strong>Metodolog&iacute;a B</strong></td>
            <td colspan="2"><strong>Metodolog&iacute;a Cobre met&aacute;lico</strong></td>
        </tr>
        <tr align="center">
            <td>Muestra 1</td>
            <td>Muestra 2</td>
            <td>Muestra 1</td>
            <td>Muestra 2</td>
            <td>Muestra 1</td>
            <td>Muestra 2</td>
        </tr>
        <tr align="center">
            <td>Masa muestra (g)</td>
            <td><input type="text" id="masaA" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['masaA'] ?>"
                       <?= $disabled ?>/></td>
            <td><input type="text" id="masaB" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['masaB'] ?>"
                       <?= $disabled ?>/></td>
            <td><input type="text" id="masaC" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['masaC'] ?>"
                       <?= $disabled ?>/></td>
            <td><input type="text" id="masaD" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['masaD'] ?>"
                       <?= $disabled ?>/></td>
            <td><input type="text" id="masaE" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['masaE'] ?>"
                       <?= $disabled ?>/></td>
            <td><input type="text" id="masaF" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['masaF'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr align="center">
            <td>Vol. Na<sub>2</sub>S<sub>2</sub>O<sub>3</sub> consumido (mL)</td>
            <td><input type="text" id="consuA" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['consuA'] ?>"
                       <?= $disabled ?>/></td>
            <td><input type="text" id="consuB" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['consuB'] ?>"
                       <?= $disabled ?>/></td>
            <td><input type="text" id="consuC" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['consuC'] ?>"
                       <?= $disabled ?>/></td>
            <td><input type="text" id="consuD" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['consuD'] ?>"
                       <?= $disabled ?>/></td>
            <td><input type="text" id="consuE" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['consuE'] ?>"
                       <?= $disabled ?>/></td>
            <td><input type="text" id="consuF" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['consuF'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr align="center">
            <td>Vol. de Al&iacute;cuota (mL)</td>
            <td><select id="alicuotaA" onchange="__calcula()" <?= $disabled ?> style="width:70px">
                    <option value="" inc="">...</option>
                    <option value="0.5" <?php if ($ROW[0]['alicuotaA'] == '0.5') echo 'selected'; ?> inc="0.0049">0.5
                    </option>
                    <option value="1" <?php if ($ROW[0]['alicuotaA'] == '1') echo 'selected'; ?> inc="0.0056">1.00
                    </option>
                    <option value="2" <?php if ($ROW[0]['alicuotaA'] == '2') echo 'selected'; ?> inc="0.0020">2.00
                    </option>
                    <option value="3" <?php if ($ROW[0]['alicuotaA'] == '3') echo 'selected'; ?> inc="0.0020">3.00
                    </option>
                    <option value="4" <?php if ($ROW[0]['alicuotaA'] == '4') echo 'selected'; ?> inc="0.0020">4.00
                    </option>
                    <option value="5" <?php if ($ROW[0]['alicuotaA'] == '5') echo 'selected'; ?> inc="0.0020">5.00
                    </option>
                    <option value="6" <?php if ($ROW[0]['alicuotaA'] == '6') echo 'selected'; ?> inc="0.0020">6.00
                    </option>
                    <option value="7" <?php if ($ROW[0]['alicuotaA'] == '7') echo 'selected'; ?> inc="0.0020">7.00
                    </option>
                    <option value="10" <?php if ($ROW[0]['alicuotaA'] == '10') echo 'selected'; ?> inc="0.0020">10.00
                    </option>
                    <option value="15" <?php if ($ROW[0]['alicuotaA'] == '15') echo 'selected'; ?> inc="0.0020">15.00
                    </option>
                    <option value="20" <?php if ($ROW[0]['alicuotaA'] == '20') echo 'selected'; ?> inc="0.0020">20.00
                    </option>
                    <option value="25" <?php if ($ROW[0]['alicuotaA'] == '25') echo 'selected'; ?> inc="0.0020">25.00
                    </option>
                    <option value="50" <?php if ($ROW[0]['alicuotaA'] == '50') echo 'selected'; ?> inc="0.0020">50.00
                    </option>
                    <option value="100" <?php if ($ROW[0]['alicuotaA'] == '100') echo 'selected'; ?> inc="0.0020">
                        100.00
                    </option>
                </select></td>
            <td><select id="alicuotaB" onchange="__calcula()" <?= $disabled ?> style="width:70px">
                    <option value="" inc="">...</option>
                    <option value="0.5" <?php if ($ROW[0]['alicuotaB'] == '0.5') echo 'selected'; ?> inc="0.0049">0.5
                    </option>
                    <option value="1" <?php if ($ROW[0]['alicuotaB'] == '1') echo 'selected'; ?> inc="0.0056">1.00
                    </option>
                    <option value="2" <?php if ($ROW[0]['alicuotaB'] == '2') echo 'selected'; ?> inc="0.0020">2.00
                    </option>
                    <option value="3" <?php if ($ROW[0]['alicuotaB'] == '3') echo 'selected'; ?> inc="0.0020">3.00
                    </option>
                    <option value="4" <?php if ($ROW[0]['alicuotaB'] == '4') echo 'selected'; ?> inc="0.0020">4.00
                    </option>
                    <option value="5" <?php if ($ROW[0]['alicuotaB'] == '5') echo 'selected'; ?> inc="0.0020">5.00
                    </option>
                    <option value="6" <?php if ($ROW[0]['alicuotaB'] == '6') echo 'selected'; ?> inc="0.0020">6.00
                    </option>
                    <option value="7" <?php if ($ROW[0]['alicuotaB'] == '7') echo 'selected'; ?> inc="0.0020">7.00
                    </option>
                    <option value="10" <?php if ($ROW[0]['alicuotaB'] == '10') echo 'selected'; ?> inc="0.0020">10.00
                    </option>
                    <option value="15" <?php if ($ROW[0]['alicuotaB'] == '15') echo 'selected'; ?> inc="0.0020">15.00
                    </option>
                    <option value="20" <?php if ($ROW[0]['alicuotaB'] == '20') echo 'selected'; ?> inc="0.0020">20.00
                    </option>
                    <option value="25" <?php if ($ROW[0]['alicuotaB'] == '25') echo 'selected'; ?> inc="0.0020">25.00
                    </option>
                    <option value="50" <?php if ($ROW[0]['alicuotaB'] == '50') echo 'selected'; ?> inc="0.0020">50.00
                    </option>
                    <option value="100" <?php if ($ROW[0]['alicuotaB'] == '100') echo 'selected'; ?> inc="0.0020">
                        100.00
                    </option>
                </select></td>
            <td colspan="2">&nbsp;</td>
            <td><select id="alicuotaE" onchange="__calcula()" <?= $disabled ?> style="width:70px">
                    <option value="" inc="">...</option>
                    <option value="0.5" <?php if ($ROW[0]['alicuotaE'] == '0.5') echo 'selected'; ?> inc="0.0049">0,5
                    </option>
                    <option value="1" <?php if ($ROW[0]['alicuotaE'] == '1') echo 'selected'; ?> inc="0.0056">1.00
                    </option>
                    <option value="2" <?php if ($ROW[0]['alicuotaE'] == '2') echo 'selected'; ?> inc="0.0020">2.00
                    </option>
                    <option value="3" <?php if ($ROW[0]['alicuotaE'] == '3') echo 'selected'; ?> inc="0.0020">3.00
                    </option>
                    <option value="4" <?php if ($ROW[0]['alicuotaE'] == '4') echo 'selected'; ?> inc="0.0020">4.00
                    </option>
                    <option value="5" <?php if ($ROW[0]['alicuotaE'] == '5') echo 'selected'; ?> inc="0.0020">5.00
                    </option>
                    <option value="6" <?php if ($ROW[0]['alicuotaE'] == '6') echo 'selected'; ?> inc="0.0020">6.00
                    </option>
                    <option value="7" <?php if ($ROW[0]['alicuotaE'] == '7') echo 'selected'; ?> inc="0.0020">7.00
                    </option>
                    <option value="10" <?php if ($ROW[0]['alicuotaE'] == '10') echo 'selected'; ?> inc="0.0020">10.00
                    </option>
                    <option value="15" <?php if ($ROW[0]['alicuotaE'] == '15') echo 'selected'; ?> inc="0.0020">15.00
                    </option>
                    <option value="20" <?php if ($ROW[0]['alicuotaE'] == '20') echo 'selected'; ?> inc="0.0020">20.00
                    </option>
                    <option value="25" <?php if ($ROW[0]['alicuotaE'] == '25') echo 'selected'; ?> inc="0.0020">25.00
                    </option>
                    <option value="50" <?php if ($ROW[0]['alicuotaE'] == '50') echo 'selected'; ?> inc="0.0020">50.00
                    </option>
                    <option value="100" <?php if ($ROW[0]['alicuotaE'] == '100') echo 'selected'; ?> inc="0.0020">
                        100.00
                    </option>
                </select></td>
            <td><select id="alicuotaF" onchange="__calcula()" <?= $disabled ?> style="width:70px">
                    <option value="" inc="">...</option>
                    <option value="0.5" <?php if ($ROW[0]['alicuotaF'] == '0.5') echo 'selected'; ?> inc="0.0049">0.5
                    </option>
                    <option value="1" <?php if ($ROW[0]['alicuotaF'] == '1') echo 'selected'; ?> inc="0.0056">1.00
                    </option>
                    <option value="2" <?php if ($ROW[0]['alicuotaF'] == '2') echo 'selected'; ?> inc="0.0020">2.00
                    </option>
                    <option value="3" <?php if ($ROW[0]['alicuotaF'] == '3') echo 'selected'; ?> inc="0.0020">3.00
                    </option>
                    <option value="4" <?php if ($ROW[0]['alicuotaF'] == '4') echo 'selected'; ?> inc="0.0020">4.00
                    </option>
                    <option value="5" <?php if ($ROW[0]['alicuotaF'] == '5') echo 'selected'; ?> inc="0.0020">5.00
                    </option>
                    <option value="6" <?php if ($ROW[0]['alicuotaF'] == '6') echo 'selected'; ?> inc="0.0020">6.00
                    </option>
                    <option value="7" <?php if ($ROW[0]['alicuotaF'] == '7') echo 'selected'; ?> inc="0.0020">7.00
                    </option>
                    <option value="10" <?php if ($ROW[0]['alicuotaF'] == '10') echo 'selected'; ?> inc="0.0020">10.00
                    </option>
                    <option value="15" <?php if ($ROW[0]['alicuotaF'] == '15') echo 'selected'; ?> inc="0.0020">15.00
                    </option>
                    <option value="20" <?php if ($ROW[0]['alicuotaF'] == '20') echo 'selected'; ?> inc="0.0020">20.00
                    </option>
                    <option value="25" <?php if ($ROW[0]['alicuotaF'] == '25') echo 'selected'; ?> inc="0.0020">25.00
                    </option>
                    <option value="50" <?php if ($ROW[0]['alicuotaF'] == '50') echo 'selected'; ?> inc="0.0020">50.00
                    </option>
                    <option value="100" <?php if ($ROW[0]['alicuotaF'] == '100') echo 'selected'; ?> inc="0.0020">
                        100.00
                    </option>
                </select></td>
        </tr>
        <tr align="center">
            <td>Vol Bal&oacute;n Aforado (mL)</td>
            <td><select id="balA" <?= $disabled ?> onchange="__calcula()" style="width:70px">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['balA'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">10.00
                    </option>
                    <option <?php if ($ROW[0]['balA'] == '25') echo 'selected'; ?> value="25" IEC="0.015" IR="0.0006">
                        25.00
                    </option>
                    <option <?php if ($ROW[0]['balA'] == '50') echo 'selected'; ?> value="50" IEC="0.028" IR="0.0006">
                        50.00
                    </option>
                    <option <?php if ($ROW[0]['balA'] == '100') echo 'selected'; ?> value="100" IEC="0.047" IR="0.0005">
                        100.00
                    </option>
                    <option <?php if ($ROW[0]['balA'] == '200') echo 'selected'; ?> value="200" IEC="0.082" IR="0.0004">
                        200.00
                    </option>
                    <option <?php if ($ROW[0]['balA'] == '250') echo 'selected'; ?> value="250" IEC="0.099" IR="0.0004">
                        250.00
                    </option>
                    <option <?php if ($ROW[0]['balA'] == '500') echo 'selected'; ?> value="500" IEC="0.180" IR="0.0004">
                        500.00
                    </option>
                    <option <?php if ($ROW[0]['balA'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                     IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['balA'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                     IR="0.0003">2000.00
                    </option>
                </select></td>
            <td><select id="balB" <?= $disabled ?> onchange="__calcula()" style="width:70px">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['balB'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">10.00
                    </option>
                    <option <?php if ($ROW[0]['balB'] == '25') echo 'selected'; ?> value="25" IEC="0.015" IR="0.0006">
                        25.00
                    </option>
                    <option <?php if ($ROW[0]['balB'] == '50') echo 'selected'; ?> value="50" IEC="0.028" IR="0.0006">
                        50.00
                    </option>
                    <option <?php if ($ROW[0]['balB'] == '100') echo 'selected'; ?> value="100" IEC="0.047" IR="0.0005">
                        100.00
                    </option>
                    <option <?php if ($ROW[0]['balB'] == '200') echo 'selected'; ?> value="200" IEC="0.082" IR="0.0004">
                        200.00
                    </option>
                    <option <?php if ($ROW[0]['balB'] == '250') echo 'selected'; ?> value="250" IEC="0.099" IR="0.0004">
                        250.00
                    </option>
                    <option <?php if ($ROW[0]['balB'] == '500') echo 'selected'; ?> value="500" IEC="0.180" IR="0.0004">
                        500.00
                    </option>
                    <option <?php if ($ROW[0]['balB'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                     IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['balB'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                     IR="0.0003">2000.00
                    </option>
                </select></td>
            <td colspan="2">&nbsp;</td>
            <td><select id="balE" <?= $disabled ?> onchange="__calcula()" style="width:70px">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['balE'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">10.00
                    </option>
                    <option <?php if ($ROW[0]['balE'] == '25') echo 'selected'; ?> value="25" IEC="0.015" IR="0.0006">
                        25.00
                    </option>
                    <option <?php if ($ROW[0]['balE'] == '50') echo 'selected'; ?> value="50" IEC="0.028" IR="0.0006">
                        50.00
                    </option>
                    <option <?php if ($ROW[0]['balE'] == '100') echo 'selected'; ?> value="100" IEC="0.047" IR="0.0005">
                        100.00
                    </option>
                    <option <?php if ($ROW[0]['balE'] == '200') echo 'selected'; ?> value="200" IEC="0.082" IR="0.0004">
                        200.00
                    </option>
                    <option <?php if ($ROW[0]['balE'] == '250') echo 'selected'; ?> value="250" IEC="0.099" IR="0.0004">
                        250.00
                    </option>
                    <option <?php if ($ROW[0]['balE'] == '500') echo 'selected'; ?> value="500" IEC="0.180" IR="0.0004">
                        500.00
                    </option>
                    <option <?php if ($ROW[0]['balE'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                     IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['balE'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                     IR="0.0003">2000.00
                    </option>
                </select></td>
            <td><select id="balF" <?= $disabled ?> onchange="__calcula()" style="width:70px">
                    <option value="" IEC="" IR="">...</option>
                    <option <?php if ($ROW[0]['balF'] == '10') echo 'selected'; ?> value="10" IEC="0" IR="0">10.00
                    </option>
                    <option <?php if ($ROW[0]['balF'] == '25') echo 'selected'; ?> value="25" IEC="0.015" IR="0.0006">
                        25.00
                    </option>
                    <option <?php if ($ROW[0]['balF'] == '50') echo 'selected'; ?> value="50" IEC="0.028" IR="0.0006">
                        50.00
                    </option>
                    <option <?php if ($ROW[0]['balF'] == '100') echo 'selected'; ?> value="100" IEC="0.047" IR="0.0005">
                        100.00
                    </option>
                    <option <?php if ($ROW[0]['balF'] == '200') echo 'selected'; ?> value="200" IEC="0.082" IR="0.0004">
                        200.00
                    </option>
                    <option <?php if ($ROW[0]['balF'] == '250') echo 'selected'; ?> value="250" IEC="0.099" IR="0.0004">
                        250.00
                    </option>
                    <option <?php if ($ROW[0]['balF'] == '500') echo 'selected'; ?> value="500" IEC="0.180" IR="0.0004">
                        500.00
                    </option>
                    <option <?php if ($ROW[0]['balF'] == '1000') echo 'selected'; ?> value="1000" IEC="0.348"
                                                                                     IR="0.0003">1000.00
                    </option>
                    <option <?php if ($ROW[0]['balF'] == '2000') echo 'selected'; ?> value="2000" IEC="0.674"
                                                                                     IR="0.0003">2000.00
                    </option>
                </select></td>
        </tr>
        <tr align="center">
            <td><strong>% Cu</strong></td>
            <td id="cuA"></td>
            <td id="cuB"></td>
            <td id="cuC"></td>
            <td id="cuD"></td>
            <td id="cuE"></td>
            <td id="cuF"></td>
        </tr>
        <tr align="center">
            <td><strong>Compuesto de Cu</strong></td>
            <td id="coA"></td>
            <td id="coB"></td>
            <td id="coC"></td>
            <td id="coD"></td>
            <td id="coE"></td>
            <td id="coF"></td>
        </tr>

    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Reporte de resultados</td>
        </tr>
        <tr align="center">
            <td>Contenido Promedio de Cu:</td>
            <td>Incertidumbre expandida:</td>
            <td>Contenido Prom. Comp de Cu:</td>
            <td>Incertidumbre expandida:</td>
        </tr>
        <tr align="center">
            <td id="finA"></td>
            <td id="finB"></td>
            <td id="finC"></td>
            <td id="finD"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Observaciones</td>
        </tr>
        <tr>
            <td colspan="4"><textarea id="obs" style="width:98%" <?= $disabled ?>><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
        <tr>
            <td class="titulo" colspan="6">Formulas</td>
        </tr>
        <tr>
            <td colspan="6">

                <p>Si el ingrediente activo es &oacute;xido de cobre (I) y su concentraci&oacute;n declarada es menor o
                    igual a 20 (%m/m):</p>
                <p style="text-align: center"><img src="imagenes/H0002/f1.PNG" width="706" height="314" alt="f1"/>
                </p>
                <br/>
                <p>Si el ingrediente activo es &oacute;xido de cobre (I) con una concentraci&oacute;n declarada mayor a
                    20 (%m/m), o cualquier otra especie de cobre (incluido cobre elemental):</p>
                <p style="text-align: center"><img src="imagenes/H0002/f2.PNG" width="720" height="312" alt="f2"/>
                </p>


                <p><b>Donde:</b></p>
                <p>MM cobre = Masa molar de cobre = 63,546 g/mol</p>
                <p>Cn tiosulfato = Concentraci&oacute;n de disoluci&oacute;n valorante de tiosulfato de sodio.</p>
                <p>Vol tiosulfato = Volumen gastado de tiosulfato de sodio como valorante.</p>
                <p>M muestra = Masa de muestra pesada.</p>
                <p>&#961; muestra = Densidad de la muestra.</p>
                <p>Fact. Grav. = Factor gravim&eacute;trico:</p>
            </td>
        </tr>

        <tr>
            <td colspan="6"><h3>Factor gravim&eacute;trico</h3></td>
        </tr>
        <tr>
            <td><b>Compuesto de cobre</b></td>
            <td><b>Factor gravim&eacute;trico</b></td>
        </tr>
        <tr>
            <td>Carbonato de cobre</td>
            <td>1,74</td>
        </tr>
        <tr>
            <td>Hidr&oacute;xido de cobre (II)</td>
            <td>1,54</td>
        </tr>
        <tr>
            <td>Oxicloruro de cobre</td>
            <td>1,68</td>
        </tr>
        <tr>
            <td>Oxido de cobre (I)</td>
            <td>1,13</td>
        </tr>
        <tr>
            <td>Sulfato de cobre (II)</td>
            <td>3,93</td>
        </tr>

    </table>
    <br/>
    <br/>
    <?php if ($_GET['acc'] == 'V') { ?>
        <script>__calcula();</script>
    <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php } else { ?>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('H0002', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>