<?php
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_general();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

if ($_GET['acc'] == 'V') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<form name="form" id="form" method="post" enctype="multipart/form-data"
      action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
    <input type="hidden" id="xanalizar" name="xanalizar" value="<?= $_GET['xanalizar'] ?>"/>
    <input type="hidden" id="tipo" name="tipo" value="<?= $_GET['tipo'] ?>"/>
    <input type="hidden" id="rango" name="rango"/>
    <input type="hidden" id="unidad" name="unidad"/>
    <input type="hidden" id="densidad" name="densidad"/>
    <center>
        <?php $Gestor->Incluir('h20', 'hr', 'An&aacute;lisis :: Formulario de ingreso manual de resultados') ?>
        <?= $Gestor->Encabezado('H0020', 'e', 'Formulario de ingreso manual de resultados') ?>
        <br>
        <table class="radius" style="font-size:12px" width="98%">
            <tr>
                <td class="titulo" colspan="3">Datos de la muestra</td>
            </tr>
            <tr>
                <td><strong>N&uacute;mero:</strong></td>
                <td><?= $ROW[0]['ref'] ?></td>
                <td><strong>Fecha de conclusi&oacute;n del an&aacute;lisis:</strong></td>
            </tr>
            <tr>
                <td><strong>Fecha de ingreso:</strong></td>
                <td><?= $ROW[0]['fechaI'] ?></td>
                <td><?= $ROW[0]['fechaC'] ?></td>
            </tr>
            <tr>
                <td><strong>Fecha de an&aacute;lisis:</strong></td>
                <td><input type="text" id="fechaA" name="fechaA" class="fecha" readonly
                           onClick="show_calendar(this.id);" value="<?= $ROW[0]['fechaA'] ?>" <?= $disabled ?>></td>
                <td><strong>Tipo de formulaci&oacute;n:</strong><?= $ROW[0]['tipo_form'] ?>
                    <input type="hidden" id="tipo_form" name="tipo_form" value="<?= $ROW[0]['tipo_form'] ?>"/></td>
            </tr>
            <tr>
                <td><strong>Sustancia analizada</strong></td>
                <td><input type="text" id="ingrediente" name="ingrediente" maxlength="30"
                           value="<?= $ROW[0]['ingrediente'] ?>" <?= $disabled ?>/></td>
                <td><strong>Tipo de an&aacute;lisis:</strong>&nbsp;&nbsp;<select id="tA" name="tA">
                        <option value="">...</option>
                        <option
                            <?= isset($ROW[0]['clasificacion']) && $ROW[0]['clasificacion'] == '0' ? 'selected' : '' ?>
                            value="0">Elemento
                        </option>
                        <option
                            <?= isset($ROW[0]['clasificacion']) && $ROW[0]['clasificacion'] == '2' ? 'selected' : '' ?>
                            value="2">Impureza
                        </option>
                    </select></td>
            </tr>
            <tr>
                <td><strong>Concentraci&oacute;n declarada:</strong></td>
                <td><input type="text" id="rango" name="rango" maxlength="20" class="monto"
                           value="<?= $ROW[0]['rango'] ?>" <?= $disabled ?>>&nbsp;
                    <select id="unidad" name="unidad" <?php if ($_GET['acc'] == 'V') echo 'disabled'; ?>
                            onchange="Densidad(this.value);">
                        <option value="">...</option>
                        <option value="0" <?php if ($ROW[0]['unidad'] == '0') echo 'selected'; ?>>%m/m</option>
                        <option value="1" <?php if ($ROW[0]['unidad'] == '1') echo 'selected'; ?>>%m/v</option>
                    </select></td>
                <td>&nbsp;</td>
            </tr>
            <?php if ($_GET['acc'] == 'V') { ?>
                <tr>
                    <td><strong>Creado por:</strong></td>
                    <td><?= $ROW[0]['analista'] ?></td>
                    <td>&nbsp;</td>
                </tr>
            <?php } ?>
        </table>
        <br>
        <table class="radius" style="font-size:12px" width="98%">
            <tr>
                <td class="titulo" colspan="6">Resultados</td>
            </tr>
            <tr>
                <td align="center" id="oculta1"><strong>Densidad:</strong></td>
                <td align="center" id="oculto1"><strong>Incertidumbre de la densidad:</strong></td>
                <td align="center"><strong>Concentraci&oacute;n encontrada:</strong></td>
                <td align="center"><strong>Incertidumbre Combinada:</strong></td>
            <tr>
                <td align="center" id="oculta2"><input type="text" id="densidad" name="densidad" class="monto"
                                                       onblur="Redondear(this)" value="<?= $ROW[0]['densidad'] ?>"
                                                       <?= $disabled ?>></td>
                <td align="center" id="oculto2"><input type="text" id="incden" name="incden" class="monto"
                                                       onblur="Redondear(this)" value="<?= $ROW[0]['incden'] ?>"
                                                       <?= $disabled ?>></td>
                <td align="center"><input type="text" id="ce" name="ce" class="monto" onblur="FormatoResultados()"
                                          value="<?= $ROW[0]['ce'] ?>" <?= $disabled ?>></td>
                <td align="center"><input type="text" id="ie" name="ie" class="monto" onblur="FormatoResultados()"
                                          value="<?= $ROW[0]['ie'] ?>" <?= $disabled ?>></td>
            </tr>
            <tr>
                <td colspan="6">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td colspan="2"><strong>Hoja electr&oacute;nica:</strong></td>
                <?php if ($_GET['acc'] == 'I') { ?>
                    <td colspan="4"><input type="file" id="archivo" name="archivo"/></td>
                <?php } else { ?>
                    <td colspan="4"><img onclick="DocumentosZip('<?= $_GET['xanalizar'] ?>', '<?= __MODULO__ ?>')"
                                         src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Documento adjunto"
                                         class="tab2"/></td>
                <?php } ?>
            </tr>
        </table>
        <br/>
        <br/>
        <?php if ($_GET['acc'] == 'V') { ?>
            <input type="button" value="Imprimir" class="boton" onClick="window.print()">
        <?php } else { ?>
            <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
        <?php } ?>
    </center>
    <?= $Gestor->Encabezado('H0020', 'p', '') ?>
    <?= $Gestor->Footer(2) ?>
</form>
<script>Densidad(<?=$ROW[0]['unidad']?>)</script>
</body>
</html>
