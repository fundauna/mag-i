<?php
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_macro();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

if ($_GET['acc'] == 'V') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="xanalizar" value="<?= $_GET['xanalizar'] ?>"/>
<input type="hidden" id="tipo" value="<?= $_GET['tipo'] ?>"/>
<input type="hidden" id="base" value="<?= $ROW[0]['base'] ?>"/>
<center>
    <?php $Gestor->Incluir('h11', 'hr', 'An&aacute;lisis :: Determinaci&oacute;n de los Macro y Microelementos en Fertilizantes por Espectrofotometr&iacute;a de Absorci&oacute;n At&oacute;mica y Ultravioleta Visible') ?>
    <?= $Gestor->Encabezado('H0011', 'e', 'Determinaci&oacute;n de los Macro y Microelementos en Fertilizantes<br />por Espectrofotometr&iacute;a de Absorci&oacute;n At&oacute;mica y Ultravioleta Visible') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="5">Datos de la muestra</td>
        </tr>
        <tr>
            <td><strong>N&uacute;mero:</strong></td>
            <td><?= $ROW[0]['ref'] ?></td>
            <td><strong>Solicitud:</strong></td>
            <td colspan="2"><?= $ROW[0]['solicitud'] ?></td>
        </tr>
        <tr>
            <td><strong>Fecha de ingreso:</strong></td>
            <td><?= $ROW[0]['fechaI'] ?></td>
            <td><strong>Elemento:</strong></td>
            <td colspan="2"><?= strtoupper($ROW[0]['base']) ?></td>
        </tr>
        <tr>
            <td><strong>Fecha de an&aacute;lisis:</strong></td>
            <td><input type="text" id="fechaA" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaA'] ?>" <?= $disabled ?>></td>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td><strong>Fecha de conclusi&oacute;n del an&aacute;lisis:</strong></td>
            <td><?= $ROW[0]['fechaC'] ?></td>
            <td colspan="2"></td>
            <td id="inc_masa" align="center">Incertidumbre</td>
        </tr>
        <tr>
            <td><strong>Concentraci&oacute;n declarada:</strong></td>
            <td><input type="text" id="rango" maxlength="20" class="monto" value="<?= $ROW[0]['rango'] ?>"
                    <?= $disabled ?>>&nbsp;
                <select id="unidad" <?php if ($ROW[0]['unidad'] != '' or $_GET['acc'] == 'V') echo 'disabled'; ?>
                        onchange="__calcula()">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['unidad'] == '0') echo 'selected'; ?>>%m/m</option>
                    <option value="1" <?php if ($ROW[0]['unidad'] == '1') echo 'selected'; ?>>%m/v</option>
                </select>
            </td>
            <td><strong>Bal&oacute;n Muestra1 (mL):</strong></td>
            <td><select id="balon1" onchange="__calcula()" <?= $disabled ?>>
                    <option value="5" <?php if ($ROW[0]['balon1'] == '5') echo 'selected'; ?> inc="0.025">5.00</option>
                    <option value="10" <?php if ($ROW[0]['balon1'] == '10') echo 'selected'; ?> inc="0.025">10.00
                    </option>
                    <option value="25" <?php if ($ROW[0]['balon1'] == '25') echo 'selected'; ?> inc="0.03">25.00
                    </option>
                    <option value="50" <?php if ($ROW[0]['balon1'] == '50') echo 'selected'; ?> inc="0.05">50.00
                    </option>
                    <option value="100" <?php if ($ROW[0]['balon1'] == '100') echo 'selected'; ?> inc="0.08">100.00
                    </option>
                    <option value="200" <?php if ($ROW[0]['balon1'] == '200') echo 'selected'; ?> inc="0.1">200.00
                    </option>
                    <option value="250" <?php if ($ROW[0]['balon1'] == '250') echo 'selected'; ?> inc="0.11">250.00
                    </option>
                    <option value="300" <?php if ($ROW[0]['balon1'] == '300') echo 'selected'; ?> inc="0.12">300.00
                    </option>
                    <option value="500" <?php if ($ROW[0]['balon1'] == '500') echo 'selected'; ?> inc="0.15">500.00
                    </option>
                    <option value="1000" <?php if ($ROW[0]['balon1'] == '1000') echo 'selected'; ?> inc="0.3">1000.00
                    </option>
                    <option value="2000" <?php if ($ROW[0]['balon1'] == '2000') echo 'selected'; ?> inc="0.5">2000.00
                    </option>
                </select></td>
            <td id="inc_balon" align="center" rowspan="2"></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td><strong>Bal&oacute;n Muestra2 (mL):</strong></td>
            <td><select id="balon2" onchange="__calcula()" <?= $disabled ?>>
                    <option value="5" <?php if ($ROW[0]['balon2'] == '5') echo 'selected'; ?> inc="0.025">5.00</option>
                    <option value="10" <?php if ($ROW[0]['balon2'] == '10') echo 'selected'; ?> inc="0.025">10.00
                    </option>
                    <option value="25" <?php if ($ROW[0]['balon2'] == '25') echo 'selected'; ?> inc="0.03">25.00
                    </option>
                    <option value="50" <?php if ($ROW[0]['balon2'] == '50') echo 'selected'; ?> inc="0.05">50.00
                    </option>
                    <option value="100" <?php if ($ROW[0]['balon2'] == '100') echo 'selected'; ?> inc="0.08">100.00
                    </option>
                    <option value="200" <?php if ($ROW[0]['balon2'] == '200') echo 'selected'; ?> inc="0.1">200.00
                    </option>
                    <option value="250" <?php if ($ROW[0]['balon2'] == '250') echo 'selected'; ?> inc="0.11">250.00
                    </option>
                    <option value="300" <?php if ($ROW[0]['balon2'] == '300') echo 'selected'; ?> inc="0.12">300.00
                    </option>
                    <option value="500" <?php if ($ROW[0]['balon2'] == '500') echo 'selected'; ?> inc="0.15">500.00
                    </option>
                    <option value="1000" <?php if ($ROW[0]['balon2'] == '1000') echo 'selected'; ?> inc="0.3">1000.00
                    </option>
                    <option value="2000" <?php if ($ROW[0]['balon2'] == '2000') echo 'selected'; ?> inc="0.5">2000.00
                    </option>
                </select></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="5">Datos de an&aacute;lisis</td>
        </tr>
        <tr align="center">
            <td colspan="2" width="48%">
                <!-- -->
                <table class="radius" width="100%">
                    <tr align="center">
                        <td style="font-weight:bold"><?= $ROW[0]['elemento'] ?></td>
                        <td>Pipeta</td>
                        <td>Bal&oacute;n</td>
                    </tr>
                    <tr align="center">
                        <td>D1</td>
                        <td><select id="pipetaD1" onchange="__calcula()" <?= $disabled ?>>
                                <option value="-1" <?php if ($ROW[0]['pipetaD1'] == '-1') echo 'selected'; ?>  inc="0">...</option>
                                <option value="0" <?php if ($ROW[0]['pipetaD1'] == '0') echo 'selected'; ?> inc="0.005">
                                    0.5
                                </option>
                                <option value="1" <?php if ($ROW[0]['pipetaD1'] == '1') echo 'selected'; ?> inc="0.005">
                                    1.00
                                </option>
                                <option value="2" <?php if ($ROW[0]['pipetaD1'] == '2') echo 'selected'; ?> inc="0.01">
                                    2.00
                                </option>
                                <option value="3" <?php if ($ROW[0]['pipetaD1'] == '3') echo 'selected'; ?> inc="0.01">
                                    3.00
                                </option>
                                <option value="4" <?php if ($ROW[0]['pipetaD1'] == '4') echo 'selected'; ?> inc="0.01">
                                    4.00
                                </option>
                                <option value="5" <?php if ($ROW[0]['pipetaD1'] == '5') echo 'selected'; ?> inc="0.01">
                                    5.00
                                </option>
                                <option value="6" <?php if ($ROW[0]['pipetaD1'] == '6') echo 'selected'; ?> inc="0.02">
                                    6.00
                                </option>
                                <option value="7" <?php if ($ROW[0]['pipetaD1'] == '7') echo 'selected'; ?> inc="0.02">
                                    7.00
                                </option>
                                <option value="8" <?php if ($ROW[0]['pipetaD1'] == '8') echo 'selected'; ?> inc="0.02">
                                    8.00
                                </option>
                                <option value="9" <?php if ($ROW[0]['pipetaD1'] == '9') echo 'selected'; ?> inc="0.02">
                                    9.00
                                </option>
                                <option value="10" <?php if ($ROW[0]['pipetaD1'] == '10') echo 'selected'; ?> inc="0.02">
                                    10.00
                                </option>
                                <option value="15" <?php if ($ROW[0]['pipetaD1'] == '15') echo 'selected'; ?> inc="0.03">
                                    15.00
                                </option>
                                <option value="20" <?php if ($ROW[0]['pipetaD1'] == '20') echo 'selected'; ?> inc="0.03">
                                    20.00
                                </option>
                                <option value="25" <?php if ($ROW[0]['pipetaD1'] == '25') echo 'selected'; ?> inc="0.03">
                                    25.00
                                </option>
                                <option value="50" <?php if ($ROW[0]['pipetaD1'] == '50') echo 'selected'; ?> inc="0.05">
                                    50.00
                                </option>   
                                <option value="100" <?php if ($ROW[0]['pipetaD1'] == '100') echo 'selected'; ?>
                                        inc="0.08">100.00
                                </option>
                            </select></td>
                            
                        <td><select id="balonD1" onchange="__calcula()" <?= $disabled ?>>
                                <option value="-1" <?php if ($ROW[0]['balonD1'] == '-1') echo 'selected'; ?>  inc="0">...</option>
                                <option value="5" <?php if ($ROW[0]['balonD1'] == '5') echo 'selected'; ?> inc="0.025">
                                    5.00
                                </option>
                                <option value="10" <?php if ($ROW[0]['balonD1'] == '10') echo 'selected'; ?> inc="0.025">
                                    10.00
                                </option>
                                <option value="25" <?php if ($ROW[0]['balonD1'] == '25') echo 'selected'; ?> inc="0.03">
                                    25.00
                                </option>
                                <option value="50" <?php if ($ROW[0]['balonD1'] == '50') echo 'selected'; ?> inc="0.05">
                                    50.00
                                </option>
                                <option value="100" <?php if ($ROW[0]['balonD1'] == '100') echo 'selected'; ?>
                                        inc="0.08">100.00
                                </option>
                                <option value="200" <?php if ($ROW[0]['balonD1'] == '200') echo 'selected'; ?> inc="0.1">
                                    200.00
                                </option>
                                <option value="250" <?php if ($ROW[0]['balonD1'] == '250') echo 'selected'; ?>
                                        inc="0.11">250.00
                                </option>
                                <option value="300" <?php if ($ROW[0]['balonD1'] == '300') echo 'selected'; ?>
                                        inc="0.12">300.00
                                </option>
                                <option value="500" <?php if ($ROW[0]['balonD1'] == '500') echo 'selected'; ?>
                                        inc="0.15">500.00
                                </option>
                                <option value="1000" <?php if ($ROW[0]['balonD1'] == '1000') echo 'selected'; ?>
                                        inc="0.3">1000.00
                                </option>
                                <option value="2000" <?php if ($ROW[0]['balonD1'] == '2000') echo 'selected'; ?>
                                        inc="0.5">2000.00
                                </option>
                            </select></td>
                    </tr>
                    <tr align="center">
                        <td>Incert</td>
                        <td id="inc_pipD1"></td>
                        <td id="inc_balD1"></td>
                    </tr>
                    <tr align="center">
                        <td>D2</td>
                        <td><select id="pipetaD2" onchange="__calcula()" <?= $disabled ?>>
                                <option value="-1"<?php if ($ROW[0]['pipetaD2'] == '-1') echo 'selected'; ?>    inc="0">...</option>
                                <option value="0" <?php if ($ROW[0]['pipetaD2'] == '0') echo 'selected'; ?> inc="0.005">
                                    0.5
                                </option>
                                <option value="1" <?php if ($ROW[0]['pipetaD2'] == '1') echo 'selected'; ?> inc="0.005">
                                    1.00
                                </option>
                                <option value="2" <?php if ($ROW[0]['pipetaD2'] == '2') echo 'selected'; ?> inc="0.01">
                                    2.00
                                </option>
                                <option value="3" <?php if ($ROW[0]['pipetaD2'] == '3') echo 'selected'; ?> inc="0.01">
                                    3.00
                                </option>
                                <option value="4" <?php if ($ROW[0]['pipetaD2'] == '4') echo 'selected'; ?> inc="0.01">
                                    4.00
                                </option>
                                <option value="5" <?php if ($ROW[0]['pipetaD2'] == '5') echo 'selected'; ?> inc="0.01">
                                    5.00
                                </option>
                                <option value="6" <?php if ($ROW[0]['pipetaD2'] == '6') echo 'selected'; ?> inc="0.02">
                                    6.00
                                </option>
                                <option value="7" <?php if ($ROW[0]['pipetaD2'] == '7') echo 'selected'; ?> inc="0.02">
                                    7.00
                                </option>
                                <option value="8" <?php if ($ROW[0]['pipetaD2'] == '8') echo 'selected'; ?> inc="0.02">
                                    8.00
                                </option>
                                <option value="9" <?php if ($ROW[0]['pipetaD2'] == '9') echo 'selected'; ?> inc="0.02">
                                    9.00
                                </option>
                                <option value="10" <?php if ($ROW[0]['pipetaD2'] == '10') echo 'selected'; ?> inc="0.02">
                                    10.00
                                </option>
                                <option value="15" <?php if ($ROW[0]['pipetaD2'] == '15') echo 'selected'; ?> inc="0.03">
                                    15.00
                                </option>
                                <option value="20" <?php if ($ROW[0]['pipetaD2'] == '20') echo 'selected'; ?> inc="0.03">
                                    20.00
                                </option>
                                <option value="25" <?php if ($ROW[0]['pipetaD2'] == '25') echo 'selected'; ?> inc="0.03">
                                    25.00
                                </option>
                                <option value="50" <?php if ($ROW[0]['pipetaD2'] == '50') echo 'selected'; ?> inc="0.05">
                                    50.00
                                </option>
                                <option value="100" <?php if ($ROW[0]['pipetaD2'] == '100') echo 'selected'; ?>
                                        inc="0.08">100.00
                                </option>
                            </select></td>
                        <td><select id="balonD2" onchange="__calcula()" <?= $disabled ?>>
                                <option value="-1" <?php if ($ROW[0]['balonD2'] == '-1') echo 'selected'; ?>  inc="0">...</option>
                                <option value="5" <?php if ($ROW[0]['balonD2'] == '5') echo 'selected'; ?> inc="0.025">
                                    5.00
                                </option>
                                <option value="10" <?php if ($ROW[0]['balonD2'] == '10') echo 'selected'; ?> inc="0.025">
                                    10.00
                                </option>
                                <option value="25" <?php if ($ROW[0]['balonD2'] == '25') echo 'selected'; ?> inc="0.03">
                                    25.00
                                </option>
                                <option value="50" <?php if ($ROW[0]['balonD2'] == '50') echo 'selected'; ?> inc="0.05">
                                    50.00
                                </option>
                                <option value="100" <?php if ($ROW[0]['balonD2'] == '100') echo 'selected'; ?>
                                        inc="0.08">100.00
                                </option>
                                <option value="200" <?php if ($ROW[0]['balonD2'] == '200') echo 'selected'; ?> inc="0.1">
                                    200.00
                                </option>
                                <option value="250" <?php if ($ROW[0]['balonD2'] == '250') echo 'selected'; ?>
                                        inc="0.11">250.00
                                </option>
                                <option value="300" <?php if ($ROW[0]['balonD2'] == '300') echo 'selected'; ?>
                                        inc="0.12">300.00
                                </option>
                                <option value="500" <?php if ($ROW[0]['balonD2'] == '500') echo 'selected'; ?>
                                        inc="0.15">500.00
                                </option>
                                <option value="1000" <?php if ($ROW[0]['balonD2'] == '1000') echo 'selected'; ?>
                                        inc="0.3">1000.00
                                </option>
                                <option value="2000" <?php if ($ROW[0]['balonD2'] == '2000') echo 'selected'; ?>
                                        inc="0.5">2000.00
                                </option>
                            </select></td>
                    </tr>
                    <tr align="center">
                        <td>Incert</td>
                        <td id="inc_pipD2"></td>
                        <td id="inc_balD2"></td>
                    </tr>
                    <tr align="center">
                        <td>D3</td>
                        <td><select id="pipetaD3" onchange="__calcula()" <?= $disabled ?>>
                                <option value="-1" <?php if ($ROW[0]['pipetaD3'] == '-1') echo 'selected'; ?>  inc="0">...</option>
                                <option value="0" <?php if ($ROW[0]['pipetaD3'] == '0') echo 'selected'; ?> inc="0.005">
                                    0.5
                                </option>
                                <option value="1" <?php if ($ROW[0]['pipetaD3'] == '1') echo 'selected'; ?> inc="0.005">
                                    1.00
                                </option>
                                <option value="2" <?php if ($ROW[0]['pipetaD3'] == '2') echo 'selected'; ?> inc="0.01">
                                    2.00
                                </option>
                                <option value="3" <?php if ($ROW[0]['pipetaD3'] == '3') echo 'selected'; ?> inc="0.01">
                                    3.00
                                </option>
                                <option value="4" <?php if ($ROW[0]['pipetaD3'] == '4') echo 'selected'; ?> inc="0.01">
                                    4.00
                                </option>
                                <option value="5" <?php if ($ROW[0]['pipetaD3'] == '5') echo 'selected'; ?> inc="0.01">
                                    5.00
                                </option>
                                <option value="6" <?php if ($ROW[0]['pipetaD3'] == '6') echo 'selected'; ?> inc="0.02">
                                    6.00
                                </option>
                                <option value="7" <?php if ($ROW[0]['pipetaD3'] == '7') echo 'selected'; ?> inc="0.02">
                                    7.00
                                </option>
                                <option value="8" <?php if ($ROW[0]['pipetaD3'] == '8') echo 'selected'; ?> inc="0.02">
                                    8.00
                                </option>
                                <option value="9" <?php if ($ROW[0]['pipetaD3'] == '9') echo 'selected'; ?> inc="0.02">
                                    9.00
                                </option>
                                <option value="10" <?php if ($ROW[0]['pipetaD3'] == '10') echo 'selected'; ?>inc="0.02">
                                    10.00
                                </option>
                                <option value="15" <?php if ($ROW[0]['pipetaD3'] == '15') echo 'selected'; ?> inc="0.03">
                                    15.00
                                </option>
                                <option value="20" <?php if ($ROW[0]['pipetaD3'] == '20') echo 'selected'; ?> inc="0.03">
                                    20.00
                                </option>
                                <option value="25" <?php if ($ROW[0]['pipetaD3'] == '25') echo 'selected'; ?> inc="0.03">
                                    25.00
                                </option>
                                <option value="50" <?php if ($ROW[0]['pipetaD3'] == '50') echo 'selected'; ?> inc="0.05">
                                    50.00
                                </option>
                                <option value="100" <?php if ($ROW[0]['pipetaD3'] == '100') echo 'selected'; ?>
                                        inc="0.08">100.00
                                </option>
                            </select></td>
                        <td><select id="balonD3" onchange="__calcula()" <?= $disabled ?>>
                                <option value="-1" <?php if ($ROW[0]['balonD3'] == '-1') echo 'selected'; ?> inc="0">...</option>
                                <option value="5" <?php if ($ROW[0]['balonD3'] == '5') echo 'selected'; ?> inc="0.025">
                                    5.00
                                </option>
                                <option value="10" <?php if ($ROW[0]['balonD3'] == '10') echo 'selected'; ?> inc="0.025">
                                    10.00
                                </option>
                                <option value="25" <?php if ($ROW[0]['balonD3'] == '25') echo 'selected'; ?> inc="0.03">
                                    25.00
                                </option>
                                <option value="50" <?php if ($ROW[0]['balonD3'] == '50') echo 'selected'; ?> inc="0.05">
                                    50.00
                                </option>
                                <option value="100" <?php if ($ROW[0]['balonD3'] == '100') echo 'selected'; ?>
                                        inc="0.08">100.00
                                </option>
                                <option value="200" <?php if ($ROW[0]['balonD3'] == '200') echo 'selected'; ?> inc="0.1">
                                    200.00
                                </option>
                                <option value="250" <?php if ($ROW[0]['balonD3'] == '250') echo 'selected'; ?>
                                        inc="0.11">250.00
                                </option>
                                <option value="300" <?php if ($ROW[0]['balonD3'] == '300') echo 'selected'; ?>
                                        inc="0.12">300.00
                                </option>
                                <option value="500" <?php if ($ROW[0]['balonD3'] == '500') echo 'selected'; ?>
                                        inc="0.15">500.00
                                </option>
                                <option value="1000" <?php if ($ROW[0]['balonD3'] == '1000') echo 'selected'; ?>
                                        inc="0.3">1000.00
                                </option>
                                <option value="2000" <?php if ($ROW[0]['balonD3'] == '2000') echo 'selected'; ?>
                                        inc="0.5">2000.00
                                </option>
                            </select></td>
                    </tr>
                    <tr align="center">
                        <td>Incert</td>
                        <td id="inc_pipD3"></td>
                        <td id="inc_balD3"></td>
                    </tr>
                </table>
                <!-- -->
            </td>
            <td>&nbsp;</td>
            <td colspan="2" width="48%">
                <!-- -->
                <table class="radius" width="100%">
                    <tr align="center">
                        <td colspan="2"><strong>Incertidumbre por masa</strong></td>
                        <td>Componente</td>
                    </tr>
                    <tr align="center">
                        <td>Repetibilidad</td>
                        <td><input type="text" id="repetibilidad" class="monto" onblur="Redondear(this);"
                                   value="<?= isset($ROW[0]['repetibilidad']) == true ? $ROW[0]['repetibilidad'] : '0' ?>" <?= $disabled ?> ></td>
                        <td id="rep_comp"></td>
                    </tr>
                    <tr align="center">
                        <td>Resoluci&oacute;n</td>
                        <td><input type="text" id="resolucion" class="monto" onblur="Redondear(this);"
                                   value="<?= isset($ROW[0]['resolucion']) == true ? $ROW[0]['resolucion'] : '0' ?>" <?= $disabled ?>></td>
                        <td id="res_comp"></td>
                    </tr>
                    <tr align="center">
                        <td>Linealidad</td>
                        <td><input type="text" id="inc_cer" class="monto" onblur="Redondear(this);"
                                   value="<?= isset($ROW[0]['inc_cer']) == true ? $ROW[0]['inc_cer'] : '0' ?>" <?= $disabled ?>></td>
                        <td id="cer_comp"></td>
                    </tr>
                    <tr align="center">
                        <td>Excentricidad</td>
                        <td><input type="text" id="emp" class="monto" onblur="Redondear(this);"
                                   value="<?= isset($ROW[0]['emp']) == true ? $ROW[0]['emp'] : '0' ?>" <?= $disabled ?>></td>
                        <td id="emp_comp"></td>
                    </tr>
                    <tr align="center">
                        <td colspan="2"><strong>IC Balanza</strong></td>
                        <td id="inc_balanza" style="font-weight:bold"></td>
                    </tr>
                </table>
                <!-- -->
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="6">Datos de los duplicados</td>
        </tr>
        <tr align="center">
            <td></td>
            <td>Masa (g)</td>
            <td>Cn / mg/L</td>
            <td>Incertidumbre</td>
            <td>&delta; (g/mL)</td>
            <td>Inc. certificado</td>
        </tr>
        <tr align="center">
            <td><strong>Muestra 1</strong></td>
            <td><input type="text" id="masa1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['masa1'] ?>"
                    <?= $disabled ?>></td>
            <td><input type="text" id="cn1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['cn1'] ?>"
                    <?= $disabled ?>></td>
            <td><input type="text" id="dumbre1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['dumbre1'] ?>"
                    <?= $disabled ?>></td>
            <td><input type="text" id="delta1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['delta1'] ?>"
                    <?= $disabled ?>></td>
            <td><input type="text" id="certi1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['certi1'] ?>"
                    <?= $disabled ?>></td>
        </tr>
        <tr align="center">
            <td><strong>Muestra 2</strong></td>
            <td><input type="text" id="masa2" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['masa2'] ?>"
                    <?= $disabled ?>></td>
            <td><input type="text" id="cn2" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['cn2'] ?>"
                    <?= $disabled ?>></td>
            <td><input type="text" id="dumbre2" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['dumbre2'] ?>"
                    <?= $disabled ?>></td>
            <td><input type="text" id="delta2" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['delta2'] ?>"
                    <?= $disabled ?>></td>
            <td><input type="text" id="certi2" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['certi2'] ?>"
                    <?= $disabled ?>></td>
        </tr>
    </table>
    <br/>
    <?php
    //$ele es si es ca,mg o k entra al oxido
    if($ROW[0]['base'] == "f\xc3\xb3sforo"){
        $ele = "fosforo";
    }else {
        $ele = $ROW[0]['base'];
    }

    if (($ele == 'potasio') || ($ele == 'calcio') || ($ele == 'magnesio')) {
        $ele = 1;
  /*  } else if ($ele == "f\xc3\xb3sforo") {*/
    } else if (($ele == "fosforo") || ($ele == "f�sforo")) {
        
        $ele = 3;
    } else {
        $ele = 0;
    }

    ?>

    <table class="radius" <?= $ele == 0 || $ele == 3 ? 'style="visibility:hidden" "font-size:12px" ' : '' ?>width="98%">
        <tr>
            <td class="titulo" colspan="6">Factores de correcci&oacute;n</td>
        </tr>
        <tr align="center">
            <td></td>
            <td><strong>MM elem</strong></td>
            <td></td>
            <td><strong>MM &oacute;xido</strong></td>
            <td><strong>F.C.</strong></td>
            <td><strong>Inc. F.C</strong></td>
        </tr>
        <tr align="center">
            <td id="simbolA"></td>
            <td id="simbolB"></td>
            <td></td>
            <td id="simbolC"></td>
            <td id="simbolD"></td>
            <td id="incFC"></td>
            <!-- <td><input type="text" id="incFC" class="monto" onblur="Redondear(this)" value=""></td>-->

        </tr>
        <tr align="center">
            <td>O:</td>
            <td id="simbolE">16.000</td>
            <td colspan="4"></td>
        </tr>

    </table>

    <br/>
    <?php if ($ele == 0) { ?>
        <table class="radius" style="font-size:12px" width="98%">
            <tr>
                <td class="titulo" colspan="7">Resultados finales</td>
            </tr>


            <tr align="center">
                <td></td>
                <td></td>
                <td></td>
                <td><strong>%m/m</strong></td>
                <td><strong>%m/v</strong></td>
                <td><strong>&Delta; %m/m</strong></td>
                <td><strong>&Delta; %m/v</strong></td>
                <!-- <td <?= $ele == 0 ? 'style="visibility:hidden"' : '' ?>><strong>% m/m
                    (&oacute;xidos)</strong></td>
            <td <?= $ele == 0 ? 'style="visibility:hidden"' : '' ?>><strong>% m/v
                    (&oacute;xidos)</strong></td>
            <td <?= $ele == 0 ? 'style="visibility:hidden"' : '' ?>><strong>&Delta; % m/m
                    (&oacute;xidos)</strong></td>
            <td <?= $ele == 0 ? 'style="visibility:hidden"' : '' ?>><strong>&Delta; % m/v
                    (&oacute;xidos)</strong></td>-->
            </tr>
            <tr align="center">
                <td></td>
                <td></td>
                <td><strong>Muestra 1</strong></td>
                <td id="masa_normal1"></td>
                <td id="volu_normal1"></td>
                <td id="masa_delta1"></td>
                <td id="volu_delta1"></td>
                <td id="masa_doble1" <?= $ele == 0 ? 'style="visibility:hidden"' : '' ?>></td>
                <td id="volu_doble1" <?= $ele == 0 ? 'style="visibility:hidden"' : '' ?>></td>
                <td id="delta_oxido_masa1" <?= $ele == 0 ? 'style="visibility:hidden"' : '' ?>></td>
                <td id="delta_oxido_volumen1" <?= $ele == 0 ? 'style="visibility:hidden"' : '' ?>></td>
            </tr>
            <tr align="center">
                <td></td>
                <td></td>
                <td><strong>Muestra 2</strong></td>
                <td id="masa_normal2"></td>
                <td id="volu_normal2"></td>
                <td id="masa_delta2"></td>
                <td id="volu_delta2"></td>
                <td id="masa_doble2" <?= $ele == 0 ? 'style="visibility:hidden"' : '' ?>></td>
                <td id="volu_doble2" <?= $ele == 0 ? 'style="visibility:hidden"' : '' ?>></td>
                <td id="delta_oxido_masa2" <?= $ele == 0 ? 'style="visibility:hidden"' : '' ?>></td>
                <td id="delta_oxido_volumen2" <?= $ele == 0 ? 'style="visibility:hidden"' : '' ?>></td>
            </tr>
            <tr>
                <td colspan="7">
                    <hr/>
                </td>
            </tr>
            <tr align="center">
                <td colspan="2"></td>
                <td style="color:#009900"><strong>Promedio % m/m</strong></td>
                <td style="color:#009900"><strong>Promedio % m/v</strong></td>
                <td><strong>Desv. Est.</strong></td>
                <td><strong>Inc. por DE</strong></td>
                <td style="color:#009900"><strong>Inc. final combinada</strong></td>
            </tr>
            <tr align="center">
                <td colspan="2"></td>
                <td id="prom_masa"></td>
                <td id="prom_volu"></td>
                <td id="desv"></td>
                <td id="DE"></td>
                <td id="final"></td>
            </tr>
        </table>
    <?php } elseif($ele == 3)  { ?>

        <table class="radius" style="font-size:12px" width="98%" rules="none" >
            <tr>
                <td class="titulo" colspan="9">Resultados finales</td>
            </tr>
            <tr align="center">
                <td></td>
                <td></td>
                <td><strong>% m/m (&oacute;xidos)</strong></td>
                <td><strong>% m/v (&oacute;xidos)</strong></td>
                <td><strong>&Delta; % m/m (&oacute;xidos)</strong></td>
                <td><strong>&Delta; % m/v (&oacute;xidos)</strong></td>


            </tr>
            <tr align="center">

                <td colspan="2"><strong>Muestra 1</strong></td>
                <td id="masa_normal1"></td>
                <td id="volu_normal1"></td>
                <td id="delta_oxido_masa1"></td>
                <td id="delta_oxido_volumen1"></td>

            </tr>
            <tr align="center">

                <td colspan="2"><strong>Muestra 2</strong></td>
                <td id="masa_normal2"></td>
                <td id="volu_normal2"></td>
                <td id="delta_oxido_masa2"></td>
                <td id="delta_oxido_volumen2"></td>
            </tr>
            <tr>
                <td colspan="9">
                    <hr/>
                </td>
            </tr>
            <tr align="center">
                <td></td>
                <td style="color:#009900"><strong>Promedio % m/m</strong></td>
                <td style="color:#009900"><strong>Promedio % m/v</strong></td>
                <td><strong>Desv. Est.</strong></td>
                <td><strong>Inc. por DE</strong></td>
                <td style="color:#009900"><strong>Inc. final combinada</strong></td>
                <td style="color:#009900"><strong></strong></td>
            </tr>
            <tr align="center">
                <td></td>
                <td id="prom_masa"></td>
                <td id="prom_volu"></td>
                <td id="desvP"></td>
                <td id="DEP"></td>
                <td id="final"></td>
                <td id="finalOxidos"></td>
            </tr>
        </table>


    <?php } else { ?>
        <table class="radius" style="font-size:12px" width="98%">
            <tr>
                <td class="titulo" colspan="9">Resultados finales</td>
            </tr>
            <tr align="center">
                <td></td>
                <td><strong>% m/m</strong></td>
                <td><strong>% m/v</strong></td>
                <td><strong>&Delta; % m/m</strong></td>
                <td><strong>&Delta; % m/v</strong></td>
                <td><strong>% m/m (&oacute;xidos)</strong></td>
                <td><strong>% m/v (&oacute;xidos)</strong></td>
                <td><strong>&Delta; % m/m (&oacute;xidos)</strong></td>
                <td><strong>&Delta; % m/v (&oacute;xidos)</strong></td>
            </tr>
            <tr align="center">
                <td><strong>Muestra 1</strong></td>
                <td id="masa_normal1"></td>
                <td id="volu_normal1"></td>
                <td id="masa_delta1"></td>
                <td id="volu_delta1"></td>
                <td id="masa_doble1"></td>
                <td id="volu_doble1"></td>
                <td id="delta_oxido_masa1"></td>
                <td id="delta_oxido_volumen1"></td>
            </tr>
            <tr align="center">
                <td><strong>Muestra 2</strong></td>
                <td id="masa_normal2"></td>
                <td id="volu_normal2"></td>
                <td id="masa_delta2"></td>
                <td id="volu_delta2"></td>
                <td id="masa_doble2"></td>
                <td id="volu_doble2"></td>
                <td id="delta_oxido_masa2"></td>
                <td id="delta_oxido_volumen2"></td>
            </tr>
            <tr>
                <td colspan="9">
                    <hr/>
                </td>
            </tr>
            <tr align="center">
                <td colspan="2"></td>
                <td style="color:#009900"><strong>Promedio % m/m</strong></td>
                <td style="color:#009900"><strong>Promedio % m/v</strong></td>
                <td><strong>Desv. Est.</strong></td>
                <td><strong>Inc. por DE</strong></td>
                <td style="color:#009900"><strong>Inc. final combinada</strong></td>
                <td style="color:#009900"><strong>Inc. final combinada (&oacute;xidos)</strong></td>
            </tr>
            <tr align="center">
                <td colspan="2"></td>
                <td id="prom_masa"></td>
                <td id="prom_volu"></td>
                <td id="desv"></td>
                <td id="DE"></td>
                <td id="final"></td>
                <td id="finalOxidos"></td>
            </tr>
        </table>
    <?php } ?>

    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo">Observaciones</td>
        </tr>
        <tr>
            <td><textarea id="obs" style="width:98%" <?= $disabled ?>><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
        <tr>
            <td class="titulo" colspan="6">Formulas</td>
        </tr>
        <tr>
            <td colspan="6">
                <p style="text-align: center">
                    <img src="imagenes/H0011/f1.PNG" width="280" height="149" alt="f1"/>
                </p>
                <p><b>Donde:</b></p>
                <p>C<sub>muestra</sub>: concentraci&oacute;n de la muestra emitida por el equipo en mg/L.</p>
                <p>FC: factor de correcci&oacute;n en caso de requerir reportarse como &oacute;xido.</p>
                <p>B: volumen del bal&oacute;n aforado utilizado despu&eacute;s de la digesti&oacute;n en mL.</p>
                <p>D: factor de diluci&oacute;n.</p>
                <p>m: masa de muestra en g</p>
                <p>&#961;: densidad en g/mL</p>
                <p>% m/m = g / 100 g </p>
                <p>% m/v = g / 100 mL </p>
            </td>
        </tr>
    </table>
    <br/>
    <?php
    if ($_GET['acc'] == 'V') {
        $ROW2 = $Gestor->Analista();
        echo "<br /><table class='radius' width='98%'><tr><td><strong>Realizado por:</strong> {$ROW2[0]['analista']}</td></tr></table>";
    }
    ?>
    <br/>
    <script>__calcula();</script>
    <?php if ($_GET['acc'] == 'V') { ?>
        <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php } else { ?>
        <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('H0011', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>