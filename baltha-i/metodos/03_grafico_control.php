<?php
define('__MODULO__', 'metodos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _03_grafico_control();
$ROW = $Gestor->GrafControllMuestraEnc();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('', 'hr', 'Servicios :: Gr&aacute;fico Control') ?>
<center>
    <div id="container" style="width:850px">

        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>Ingrediente Activo</th>
                <th>Concentraci&oacute;n Declarada</th>
                <th>Formulaci&oacute;n</th>
                <th>Opciones</th>
            </tr>
            </thead>
            <tbody>
            <?php for ($x = 0; $x < count($ROW); $x++) { ?>
                <tr class="gradeA" align="center">
                    <td><?= $ROW[$x]['ingrediente'] ?></td>
                    <td><?= $ROW[$x]['declarada'] ?></td>
                    <td><?= utf8_decode($ROW[$x]['formulacion']) ?></td>
                    <td>
                        <img onclick="GrafControl('<?= $ROW[$x]['ingrediente'] ?>', '<?= $ROW[$x]['declarada'] ?>', '<?= $ROW[$x]['formulacion'] ?>')"

                             src="<?php $Gestor->Incluir('estadistica', 'bkg') ?>" title="Mostrar Gr&aacute;fico"
                             class="tab2"/></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</center>
</body>
</html>