<?php
define('__MODULO__', 'inventario');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _inventario_historial();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW)
    die('Registro inexistente');

$nombre = $ROW[0]['codigo'] . ' ' . $ROW[0]['unids'] . ' ' . $ROW[0]['nombre'] . ' - ' . $ROW[0]['marca'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('f7', 'hr', 'Inventario :: Historial de movimientos') ?>
<?= $Gestor->Encabezado('F0007', 'e', 'Historial de movimientos') ?>
<center>
    <form name="formulario" action="<?= basename(__FILE__) ?>" method="post">
        <input type="hidden" name="articulo" value="<?= $_GET['ID'] ?>"/>
        <table class="radius" align="center" width="98%">
            <tr>
                <td class="titulo" colspan="3">Filtro</td>
            </tr>
            <tr>
                <td colspan="3"><?= $nombre ?>
                    <hr/>
                </td>
            </tr>
            <tr>
                <td>Tipo de movimiento:<br/>
                    <select name="tipo">
                        <option value='E'>Entradas</option>
                        <option value='T' <?php if ($_POST['tipo'] == 'T') echo 'selected'; ?>>Traslados</option>
                        <option value='' <?php if ($_POST['tipo'] == '') echo 'selected'; ?>>Todos los movimientos
                        </option>
                        <optgroup label="Salidas"></optgroup>
                        <option value='x' <?php if ($_POST['tipo'] == 'x') echo 'selected'; ?>>Todas las salidas
                        </option>
                        <option value='0' <?php if ($_POST['tipo'] == '0') echo 'selected'; ?>>Consumo</option>
                        <option value='1' <?php if ($_POST['tipo'] == '1') echo 'selected'; ?>>Da&ntilde;o</option>
                        <option value='2' <?php if ($_POST['tipo'] == '2') echo 'selected'; ?>>Donaci&oacute;n</option>
                        <option value='3' <?php if ($_POST['tipo'] == '3') echo 'selected'; ?>>Mantenimiento</option>
                        <option value='4' <?php if ($_POST['tipo'] == '4') echo 'selected'; ?>>Obsolescencia</option>
                        <option value='5' <?php if ($_POST['tipo'] == '5') echo 'selected'; ?>>Pr&eacute;stamo</option>
                    </select>
                </td>
                <td>Fecha:<br/>
                    <input type="text" id="desde" name="desde" value="<?= $_POST['desde'] ?>" class="fecha" readonly
                           onClick="show_calendar(this.id);">&nbsp;
                    <input type="text" id="hasta" name="hasta" value="<?= $_POST['hasta'] ?>" class="fecha" readonly
                           onClick="show_calendar(this.id);">
                </td>
                <td><br/><input type="button" value="Buscar" class="boton2" onclick="Buscar();"/></td>
            </tr>
        </table>
        <?php if ($_POST['desde'] != '') { ?>
            <br/>
            <div id="container" style="width:98%">
                <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                    <thead>
                    <tr>
                        <th title="C�digo interno">Cod. Int.</th>
                        <th>Tipo</th>
                        <th>Fecha</th>
                        <th title="Movimiento a bodega central">Bod. Central</th>
                        <th title="Movimiento a bodega auxiliar">Bod. Auxiliar</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $ROW = $Gestor->Movimientos();
                    for ($x = 0; $x < count($ROW); $x++) {
                        if ($ROW[$x]['tipo'] == 'E') {
                            $simbolo = '+';
                            if ($ROW[$x]['bodega1'] > 0)
                                $ROW[$x]['bodega1'] = '+' . $ROW[$x]['bodega1'];
                            if ($ROW[$x]['bodega2'] > 0)
                                $ROW[$x]['bodega2'] = '+' . $ROW[$x]['bodega2'];
                            $str = "<a href='#' onclick='MovimientoDetalle(\"{$ROW[$x]['id']}\")'>{$ROW[$x]['codigo']}</a>";
                            $title = 'Ver detalle';
                        } else {
                            if ($ROW[$x]['tipo'] != 'T') { //SI ES SALIDA
                                if ($ROW[$x]['bodega1'] > 0)
                                    $ROW[$x]['bodega1'] = '-' . $ROW[$x]['bodega1'];
                                if ($ROW[$x]['bodega2'] > 0)
                                    $ROW[$x]['bodega2'] = '-' . $ROW[$x]['bodega2'];
                            }
                            $str = $ROW[$x]['codigo'];
                            $title = "Realizado por: {$ROW[$x]['usuario']}";
                        }
                        ?>
                        <tr class="gradeA" align="center">
                            <td title="<?= $title ?>"><?= $str ?></td>
                            <td><?= $Gestor->Tipo($ROW[$x]['tipo']) ?></td>
                            <td><?= $ROW[$x]['fecha1'] ?></td>
                            <td><?= $ROW[$x]['bodega1'] ?></td>
                            <td><?= $ROW[$x]['bodega2'] ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <br/>
            <input type="button" value="Imprimir" class="boton2" onclick="window.print();"/>
        <?php } ?>
</center>
</form>
<?= $Gestor->Encabezado('F0007', 'p', '') ?>
</body>
</html>