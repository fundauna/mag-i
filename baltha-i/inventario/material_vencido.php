<?php
define('__MODULO__', 'inventario');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _material_vencido();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('f13', 'hr', 'Inventario :: Control de material vencido u obsoleto') ?>
<?= $Gestor->Encabezado('F0013', 'e', 'Control de material vencido u obsoleto') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" style="width:1050px">
            <tr>
                <td class="titulo" colspan="6">Filtro</td>
            </tr>
            <tr>
                <td>C&oacute;digo o n&uacute;mero de parte:<br/><input type="text" id="codigo" name="codigo"
                                                                       value="<?= $_POST['codigo'] ?>"/></td>
                <td>Material:<br/><input type="text" name="material" id="material" value="<?= $_POST['material'] ?>"/>
                </td>
                <td>Costo:<br/><input type="text" name="costo" id="costo" value="<?= $_POST['costo'] ?>"/></td>
                <td>Vencimiento:<br/><input type="text" name="vencimiento" id="vencimiento"
                                            value="<?= $_POST['vencimiento'] ?>"/></td>
                <td><br/><input type="button" value="Buscar" class="boton2" onclick="InventarioBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:1050px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>C&oacute;digo o n&uacute;mero de parte</th>
                <th>Material</th>
                <th>Costo Unitario</th>
                <th>Fecha Vencimiento</th>
                <th>Cantidad Vencida</th>
                <th>Unidad</th>
                <th>Costo Total</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->InventarioMuestra();
            $cantidadt = 0;
            $cantidad = 0;
            $cantidadreg = count($ROW) + 1;
            for ($x = 0; $x < count($ROW); $x++) {
                $cantidad += $ROW[$x]['cantidad'];
                $cantidadt += $ROW[$x]['total'];
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#" onclick="InventarioModifica('<?= $ROW[$x]['id'] ?>')"><?= $ROW[$x]['codigo'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['material'] ?></td>
                    <td><?= $ROW[$x]['costo'] ?></td>
                    <td><?= $ROW[$x]['vencimiento'] ?></td>
                    <td><?= $ROW[$x]['cantidad'] ?></td>
                    <td><?= $ROW[$x]['unidad'] ?></td>
                    <td><?= $ROW[$x]['total'] ?></td>
                    <td>
                        <img onclick="InventarioModifica('<?= $ROW[$x]['id'] ?>')"
                             src="<?php $Gestor->Incluir('previa', 'bkg') ?>" title="Ver detalle" class="tab3"/>&nbsp;
                        <img onclick="InventarioElimina('<?= $ROW[$x]['id'] ?>')"
                             src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar" class="tab2"/>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <p><b>Cantidad total vencida: <?= number_format($cantidad, 2, '.', ',') ?></b></p>
        <p><b>Cantidad total de Costo: <?= number_format($cantidadt, 2, '.', ',') ?></b></p>
        <p><b>Cantidad total de Registros: <?= number_format($cantidadreg, 2, '.', ',') ?></b></p>
    </div>
    <br/><input type="button" value="Agregar" class="boton" onClick="InventarioAgrega();">
</center>
<?= $Gestor->Encabezado('F0013', 'p', '') ?>
</body>
</html>