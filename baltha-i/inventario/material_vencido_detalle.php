<?php
define('__MODULO__', 'inventario');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _material_vencido_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) {
    die('Registro inexistente');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <script>
        img1 = '<?php $Gestor->Incluir('closed', 'bkg') ?>';
        img2 = '<?php $Gestor->Incluir('downboxed', 'bkg') ?>';
    </script>
</head>
<body>
<?php $Gestor->Incluir('f13', 'hr', 'Inventario :: Control de material vencido u obsoleto') ?>
<?= $Gestor->Encabezado('F0013', 'e', 'Control de material vencido u obsoleto') ?>
<center>
    <input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
    <input type="hidden" id="id" value="<?= $ROW[0]['id'] ?>"/>
    <table class="radius" width="95%">
        <tr>
            <td class="titulo" colspan="2">Datos Generales</td>
        </tr>
        <tr>
            <td>C&oacute;digo o n&uacute;mero de parte:</td>
            <td><input type="text" id="codigo" value="<?= $ROW[0]['codigo'] ?>"></td>
        </tr>
        <tr>
            <td>Material:</td>
            <td><input type="text" id="material" value="<?= $ROW[0]['material'] ?>"></td>
        </tr>
        <tr>
            <td>Costo Unitario:</td>
            <td><input type="text" id="costo" value="<?= $ROW[0]['costo'] ?>" onblur="_FLOAT(this)"></td>
        </tr>
        <tr>
            <td>Fecha Vencimiento:</td>
            <td><input type="text" id="vencimiento" value="<?= $ROW[0]['vencimiento'] ?>" class="fecha" readonly
                       onClick="show_calendar(this.id);"></td>
        </tr>
        <tr>
            <td>Cantidad Vencida:</td>
            <td><input type="text" id="cantidad" value="<?= $ROW[0]['cantidad'] ?>" onblur="_FLOAT(this)"></td>
        </tr>
        <tr>
            <td>Unidad:</td>
            <td><input type="text" id="unidad" value="<?= $ROW[0]['unidad'] ?>"></td>
        </tr>
        <tr>
            <td>Costo Total:</td>
            <td><input type="text" id="total" value="<?= $ROW[0]['total'] ?>" onblur="_FLOAT(this)"></td>
        </tr>
        <tr>
            <td>Observaciones:</td>
            <td><textarea id="obs"><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
    </table>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
</center>
<?= $Gestor->Encabezado('F0013', 'p', '') ?>
</body>
</html>