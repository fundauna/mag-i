<?php
define('__MODULO__', 'inventario');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _inventario_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) {
    die('Registro inexistente');
}
$ROW3 = $Gestor->inventarioAdicionales($ROW[0]['familia']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <script>
        img1 = '<?php $Gestor->Incluir('closed', 'bkg')?>';
        img2 = '<?php $Gestor->Incluir('downboxed', 'bkg')?>';
    </script>
</head>
<body>
<?php $Gestor->Incluir('f4', 'hr', 'Inventario :: Ficha T&eacute;cnica') ?>
<?= $Gestor->Encabezado('F0004', 'e', 'Ficha T&eacute;cnica') ?>
<center>
    <input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
    <input type="hidden" id="id" value="<?= $ROW[0]['id'] ?>"/>
    <table class="radius" width="95%">
        <tr>
            <td class="titulo" colspan="2">Datos Generales</td>
        </tr>
        <tr>
            <td>Tipo:</td>
            <td>
                <select id="tipo" style="width:100px" onchange="CambiaTipo(this.value)">
                    <option value=''>...</option>
                    <?php
                    $ROW2 = $Gestor->TiposMuestra();
                    for ($x = 0; $x < count($ROW2); $x++) {
                        ?>
                        <option <?= $ROW[0]['familia'] == $ROW2[$x]['cs'] ? 'selected' : '' ?>
                                value="<?= $ROW2[$x]['cs'] ?>"><?= $ROW2[$x]['nombre'] ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>N&uacute;mero de cat&aacute;logo:</td>
            <td><input onchange="verificaCatalogo(this.value)" type="text" id="codigo" value="<?= $ROW[0]['codigo'] ?>"
                       size="13" maxlength="15" title="Alfanumérico (3/15)"></td>
        </tr>
        <tr>
            <td>Orden de Compra:</td>
            <td><input type="text" id="orden" value="<?= $ROW[0]['orden'] ?>" size="30" maxlength="100"
                       title="Alfanumérico (2/100)"></td>
        </tr>
        <tr>
            <td>Factura:</td>
            <td><input type="text" id="factura" value="<?= $ROW[0]['factura'] ?>" size="30" maxlength="100"
                       title="Alfanumérico (2/100)"></td>
        </tr>
        <tr>
            <td>Proveedor:</td>
            <td><input class="lista" type="text" id="proveedor" value="<?= $ROW[0]['proveedor'] ?>" size="30"
                       maxlength="100" onclick="ProveeedorLista()" readonly></td>
        </tr>
        <tr>
            <td>N&uacute;mero de Acta:</td>
            <td><input type="text" id="acta" value="<?= $ROW[0]['acta'] ?>" size="30" maxlength="100"
                       title="Alfanumérico (2/100)"></td>
        </tr>
        <tr>
            <td>Nombre:</td>
            <td><input type="text" id="nombre" value="<?= $ROW[0]['nombre'] ?>" size="30" maxlength="200"
                       title="Alfanumérico (2/100)"></td>
        </tr>
        <tr>
            <td>Marca:</td>
            <td><input type="text" id="marca" value="<?= $ROW[0]['marca'] ?>" size="20" maxlength="200"
                       title="Alfanumérico (0/20)"></td>
        </tr>

        <tr>
            <td>Marca Comercial:</td>
            <td><input type="text" id="marcacomercial" value="<?= $ROW[0]['marcacomercial'] ?>" size="20"
                       maxlength="200" title="Alfanumérico (0/20)"></td>
        </tr>
        <tr>
            <td>Presentaci&oacute;n:</td>
            <td>
                <select id="presentacion" style="width:100px">
                    <option value=''>...</option>
                    <?php
                    $ROW2 = $Gestor->PresentacionesMuestra();
                    for ($x = 0; $x < count($ROW2); $x++) {
                        ?>
                        <option <?= $ROW[0]['presentacion'] == $ROW2[$x]['cs'] ? 'selected' : '' ?>
                                value="<?= $ROW2[$x]['cs'] ?>"><?= $ROW2[$x]['nombre'] ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Registra E/S:</td>
            <td>
                <select id="es" style="width:100px" onchange="CambiaEs(this.value)">
                    <option value=''>...</option>
                    <option value='0' <?php if ($ROW[0]['es'] == '0') echo 'selected'; ?>>No</option>
                    <option value='1' <?php if ($ROW[0]['es'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select>&nbsp;<img src="<?= $Gestor->Incluir('olvido', 'bkg') ?>" width="20" height="20"
                                    title="Indicar si registra entredas/salidas, Eje: Reactivos"/>
            </td>
        </tr>
        <tr>
            <td class="titulo" colspan="2">Datos Bodega en Laboratorio</td>
        </tr>
        <tr>
            <td>Cantidad m&iacute;nima:</td>
            <td><input type="text" id="minimo1" value="<?= $ROW[0]['minimo1'] ?>" size="4" maxlength="5"
                       onblur="ValidaEs();" title="Numérico (1/5)" class="cantidad">&nbsp;<img
                        src="<?= $Gestor->Incluir('olvido', 'bkg') ?>" width="20" height="20"
                        title="Digitar -1 si no aplica"/></td>
        </tr>
        <tr>
            <td>Stock:</td>
            <td>
                <?php
                //SI ESTOY MODIFICANDO, NO PUEDO TOCAR EL INDICE DE STOCK DIRECTAMENTE
                $estilo = '';
                if ($_GET['acc'] == 'M') {
                    $estilo = 'style="display:none"';
                    echo $ROW[0]['stock1'];
                }
                ?>
                <input <?= $estilo ?> type="text" id="stock1" value="<?= $ROW[0]['stock1'] ?>" size="4" maxlength="5"
                       title="Numérico (1/5)" class="cantidad">
            </td>
        </tr>
        <tr>
            <td>Ubicaci&oacute;n:</td>
            <td><input type="text" id="ubicacion1" value="<?= $ROW[0]['ubicacion1'] ?>" size="35" maxlength="100"
                       title="Alfanumérico (0/100)"></td>
        </tr>
        <tr>
            <td class="titulo" colspan="2">Datos Bodega Pavas</td>
        </tr>
        <tr>
            <td>Cantidad m&iacute;nima:</td>
            <td><input type="text" id="minimo2" value="<?= $ROW[0]['minimo2'] ?>" size="4" maxlength="5"
                       onblur="ValidaEs();" title="Numérico (1/5)" class="cantidad">&nbsp;<img
                        src="<?= $Gestor->Incluir('olvido', 'bkg') ?>" width="20" height="20"
                        title="Digitar -1 si no aplica"/></td>
        </tr>
        <tr>
            <td>Stock:</td>
            <td>
                <?php
                //SI ESTOY MODIFICANDO, NO PUEDO TOCAR EL INDICE DE STOCK DIRECTAMENTE
                $estilo = '';
                if ($_GET['acc'] == 'M') {
                    $estilo = 'style="display:none"';
                    echo $ROW[0]['stock2'];
                }
                ?>
                <input <?= $estilo ?> type="text" id="stock2" value="<?= $ROW[0]['stock2'] ?>" size="4" maxlength="5"
                       title="Numérico (1/5)" class="cantidad">
            </td>
        </tr>
        <tr>
            <td>Ubicaci&oacute;n:</td>
            <td><input type="text" id="ubicacion2" value="<?= $ROW[0]['ubicacion2'] ?>" size="35" maxlength="100"
                       title="Alfanumérico (0/100)"></td>
        </tr>
        <tr>
            <td class="titulo" colspan="2" title="Puede llenar vaciós haciendo doble clic">Datos Adicionales</td>
        </tr>
        <tr>
            <td colspan="2" id="pantalla_opcionales">
                <table width="100%">
                    <?php foreach ($ROW3 as $dato): ?>
                        <tr>
                            <td><?= $dato['nombre'] ?></td>
                            <td><input type="hidden" name="idcat[]" value="<?= $dato['idcat'] ?>"/> <input type="text"
                                                                                                           name="valorinv[]"
                                                                                                           value="<?= $dato['valor'] ?>"/>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
                <br/> <br/> <br/>
            </td>
        </tr>
    </table>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
</center>
<?= $Gestor->Encabezado('F0004', 'p', '') ?>
</body>
</html>