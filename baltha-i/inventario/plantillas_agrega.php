<?php
define('__MODULO__', 'inventario');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _plantillas_agrega();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('', 'hr', 'Inventario :: Plantillas Agregar Nuevo') ?>
<br/>
<table class="radius" align="center">
    <tr>
        <td class="titulo">Campo Nuevo</td>
    </tr>
    <tr>
        <td align="center">
            <input type="text" id="nombre" autofocus="autofocus"/>&nbsp;<input type="button" id="btn_buscar"
                                                                               value="Agregar" class="boton2"
                                                                               onClick="Agregar();">
        </td>
    </tr>
</table>
</body>
</html>