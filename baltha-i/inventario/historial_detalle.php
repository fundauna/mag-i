<?php
define('__MODULO__', 'inventario');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _historial_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('f2', 'hr', 'Inventario :: Detalle de Entradas') ?>
<?= $Gestor->Encabezado('F0002', 'e', 'Detalle de Entradas') ?>
<center>
    <input type="hidden" id="ES" value="<?= $_GET['ID'] ?>"/>
    <table class="radius" width="100%">
        <tr>
            <td class="titulo" colspan="4">Detalle</td>
        </tr>
        <tr>
            <td><strong>C&oacute;digo:</strong></td>
            <td><?= $ROW[0]['codigo'] ?></td>
            <td><strong>Observaciones:</strong></td>
            <td><?= $ROW[0]['obs'] ?></td>
        </tr>
        <tr>
            <td><strong>Orden de inicio:</strong></td>
            <td><?= $ROW[0]['oc_inicio'] ?></td>
            <td><strong>Tr&aacute;mite:</strong></td>
            <td><?= $ROW[0]['tramite'] ?></td>
        </tr>
        <tr>
            <td><strong>Requisici&oacute;n:</strong></td>
            <td><?= $ROW[0]['requisicion'] ?></td>
            <td><strong>Orden de compra:</strong></td>
            <td><?= $ROW[0]['oc'] ?></td>
        </tr>
        <tr>
            <td><strong>Factura:</strong></td>
            <td><?= $ROW[0]['factura'] ?></td>
            <td><strong>Pureza:</strong></td>
            <td><?= $ROW[0]['pureza'] ?>%</td>
        </tr>
        <tr>
            <td><strong>Ubicaci&oacute;n:</strong></td>
            <td><?= $ROW[0]['ubicacion'] ?></td>
            <td><strong>Proveedor:</strong></td>
            <td><?= $ROW[0]['proveedor'] ?></td>
        </tr>
        <tr>
            <td colspan="2">
                <?php
                if (file_exists("../../caspha-i/docs/inventario/{$_GET['ID']}.zip")) {
                    ?>
                    <strong>Adjunto:</strong>&nbsp;
                    <img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar"
                         onclick="DocumentosZip('<?= $_GET['ID'] ?>', '<?= __MODULO__ ?>')" class="tab3"/>
                    <?php
                }
                ?>
            </td>
            <td colspan="2">
                <?php
                if ($ROW[0]['certificado'] == '1') {
                    ?>
                    <strong>Material certificado</strong>&nbsp;<img src="<?= $Gestor->Incluir('aprobar', 'bkg') ?>"
                                                                    class="tab3"/>
                    <?php
                }
                ?>
            </td>
        </tr>
        <?php
        if ($ROW[0]['vence'] == '1') {
            ?>
            <tr>
                <td colspan="4">
                    <hr/>
                </td>
            </tr>
            <tr bgcolor="#FF9900">
                <td colspan="3"><?= "<strong>Fecha de vencimiento:</strong> {$ROW[0]['vencimiento']}, <strong>Fecha de aviso:</strong> {$ROW[0]['aviso']}" ?></td>
                <td align="center">
                    <?php
                    if ($ROW[0]['aviso'] != '') {
                        ?>
                        <img src="<?= $Gestor->Incluir('del', 'bkg') ?>" class="tab2" title="Marcar como atendido"
                             onclick="Atender()"/>
                        <?php
                    }
                    ?>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
    <br/>
    <input type="button" id="btn" value="Imprimir" class="boton" onClick="window.print()">
</center>
<?= $Gestor->Encabezado('F0002', 'p', '') ?>
</body>
</html>