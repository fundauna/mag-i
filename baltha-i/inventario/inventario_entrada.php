<?php
define('__MODULO__', 'inventario');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _inventario_entrada();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('f5', 'hr', 'Inventario :: Entrada de inventario') ?>
<?= $Gestor->Encabezado('F0005', 'e', 'Entrada de inventario') ?>
<center>
    <table class="radius" width="98%">
        <tr>
            <td class="titulo" colspan="4">Informaci&oacute;n general</td>
        </tr>
        <tr>
            <td><strong>C&oacute;digo:</strong></td>
            <td><?= $ROW[0]['codigo'], ' ', $ROW[0]['unids'] ?></td>
            <td><strong>Stock bodega laboratorio:</strong></td>
            <td><?= $ROW[0]['stock1'] ?></td>
        </tr>
        <tr>
            <td><strong>Nombre:</strong></td>
            <td><?= $ROW[0]['nombre'] ?></td>
            <td><strong>Stock bodega auxiliar:</strong></td>
            <td><?= $ROW[0]['stock2'] ?></td>
        </tr>
        <tr>
            <td><strong>Marca:</strong></td>
            <td><?= $ROW[0]['marca'] ?></td>
            <td><strong>Presentaci&oacute;n:</strong></td>
            <td><?= $ROW[0]['unids'] ?></td>
        </tr>
        <tr>
            <td><strong>Marca Comercial:</strong></td>
            <td><?= $ROW[0]['marcacomercial'] ?></td>
        </tr>
    </table>
    <br/>
    <form name="form" id="form" method="post" enctype="multipart/form-data"
          action="../../caspha-i/shell/<?= __MODULO__ ?>/_<?= basename(__FILE__) ?>">
        <input type="hidden" id="prov" name="prov"/>
        <input type="hidden" id="producto" name="producto" value="<?= $_GET['ID'] ?>"/>
        <table class="radius" width="98%">
            <tr>
                <td class="titulo" colspan="2">Detalle de la entrada</td>
            </tr>
            <tr>
                <td>C&oacute;digo interno:</td>
                <td><input type="text" id="codigo" name="codigo" size="20" maxlength="20"
                           onblur="ValidaCodigo(this.value)"></td>
            </tr>
            <tr>
                <td># Lote:</td>
                <td><input type="text" id="lote" name="lote" size="20" maxlength="100"></td>
            </tr>
            <tr>
                <td># de solicitud de compra:</td>
                <td><input type="text" id="oc_inicio" name="oc_inicio" size="20" maxlength="100"></td>
            </tr>
            <tr>
                <td>Tr&aacute;mite:</td>
                <td><input type="text" id="tramite" name="tramite" size="20" maxlength="100"></td>
            </tr>
            <tr>
                <td># Orden de compra:</td>
                <td><input type="text" id="oc" name="oc" size="20" maxlength="100"></td>
            </tr>
            <tr>
                <td># Requisici&oacute;n:</td>
                <td><input type="text" id="req" name="req" size="20" maxlength="100"></td>
            </tr>
            <tr>
                <td># Factura:</td>
                <td><input type="text" id="factura" name="factura" size="20" maxlength="100"></td>
            </tr>
            <tr>
                <td>Proveedor:</td>
                <td><input type="text" id="proveedor" class="lista" readonly onclick="ProveedoresLista(0)"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td>Cantidad bodega laboratorio:</td>
                <td><input type="text" id="stock1" name="stock1" maxlength="6" value="0.00" onblur="Total();"
                           class="cantidad"></td>
            </tr>
            <tr>
                <td>Cantidad bodega auxiliar:</td>
                <td><input type="text" id="stock2" name="stock2" maxlength="6" value="0.00" onblur="Total();"
                           class="cantidad"></td>
            </tr>
            <tr>
                <td>Costo total:</td>
                <td><input type="text" id="costo" name="costo" value="0.00" onblur="Total();" class="monto">
                    <span> CRC</span></td>
            </tr>
            <tr>
                <td>Costo por unidad:</td>
                <td><input type="text" id="unidad" value="0.00" class="monto" disabled> <span> CRC</span></td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td>Tiene vencimiento?:</td>
                <td><select id="vence" name="vence" onchange="ValidaVencimiento(this.value)">
                        <option value="">...</option>
                        <option value="0">No</option>
                        <option value="1">S&iacute;</option>
                    </select>
                </td>
            </tr>
            <tr id="tvencimiento1" style="display:none">
                <td>Vencimiento:</td>
                <td><input type="text" id="vencimiento" name="vencimiento" class="fecha" readonly
                           onClick="show_calendar(this.id);"></td>
            </tr>
            <tr id="tvencimiento2" style="display:none">
                <td>Fecha aviso vencimiento:</td>
                <td><input type="text" id="aviso" name="aviso" class="fecha" readonly onClick="show_calendar(this.id);">
                </td>
            </tr>
            <tr>
                <td>Certificado?:</td>
                <td><select id="certificado" name="certificado" onchange="CambiaCertificado(this.value)">
                        <option value="">...</option>
                        <option value="0">No</option>
                        <option value="1">S&iacute;</option>
                    </select>
                </td>
            </tr>
            <tr id="tr1" style="display:none">
                <td>% Pureza:</td>
                <td><input type="text" id="pureza" name="pureza" maxlength="3" onblur="ValidaPureza(this);"
                           class="cantidad"></td>
            </tr>
            <tr id="tr2" style="display:none">
                <td>Adjuntar Certificado:</td>
                <td><input type="file" name="archivo" id="archivo"></td>
            </tr>
        </table>
        <br/>
        <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    </form>
</center>
<?= $Gestor->Encabezado('F0005', 'p', '') ?>
</body>
</html>