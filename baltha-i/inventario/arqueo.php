<?php
define('__MODULO__', 'inventario');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _arqueo();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('f0', 'hr', 'Inventario :: Arqueo Inventario<->M�todos') ?>
<?= $Gestor->Encabezado('F0000', 'e', 'Arqueo Inventario<->M�todos') ?>
<br>
<table class="radius" align="center">
    <tr>
        <td class="titulo">Tipo de inventario</td>
        <td class="titulo">Elementos e Ingredientes Activos registrados</td>
    </tr>
    <tr>
        <td>
            <select id="tipo" style="width:340px;" onchange="LimpiaDisponibles();">
                <?php
                $ROW = $Gestor->TiposMuestra();
                for ($x = 0; $x < count($ROW); $x++) {
                    echo "<option value='{$ROW[$x]['cs']}'>{$ROW[$x]['nombre']}</option>";
                }
                ?>
            </select>
            <br/>
            <input type="button" id="btn_buscar1" value="Buscar" class="boton2" onClick="ProductoBuscarTipos();">
        </td>
        <td>
            <select id="analisis" style="width:340px;" onchange="LimpiaAsignados();">
                <?php
                $ROW = $Gestor->MetodosMuestra();
                for ($x = 0; $x < count($ROW); $x++) {
                    echo "<option value='{$ROW[$x]['id']}'>{$ROW[$x]['nombre']}</option>";
                }

                unset($ROW);
                ?>
            </select>
            <br/>
            <input type="button" id="btn_buscar2" value="Buscar" class="boton2" onClick="ProductoBuscarAsignados();">
        </td>
    </tr>
    <tr>
        <td class="titulo">Inventario Disponible</td>
        <td class="titulo">Inventario Asignado</td>
    </tr>
    <tr>
        <td id="td_disponibles"></td>
        <td id="td_asignados"></td>
    </tr>
    <tr>
        <td>
            <input id="btn_agregar" type="button" value="Agregar" class="boton" onClick="ProductoAgrega();" disabled>&nbsp;
            <strong>Cantidad:</strong>&nbsp;<input type="text" id="cantidad" size="4" maxlength="4"
                                                   onblur="this.value=this.value.replace(',','.')"/>&nbsp;
            <strong>Obs:</strong>&nbsp;<input type="text" id="obs" size="20" maxlength="20"/>
        </td>
        <td><input id="btn_eliminar" type="button" value="Eliminar" class="boton" onClick="ProductoElimina();" disabled>
        </td>
    </tr>
</table>
<script>
    LimpiaDisponibles();
    LimpiaAsignados();
</script>
<?= $Gestor->Encabezado('F0000', 'p', '') ?>
</body>
</html>