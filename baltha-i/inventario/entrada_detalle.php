<?php
define('__MODULO__', 'inventario');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _entrada_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW)
    die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('f5', 'hr', 'Inventario :: Entrada de inventario') ?>
<?= $Gestor->Encabezado('F0005', 'e', 'Entrada de inventario') ?>
<center>
    <table class="radius" width="98%">
        <tr>
            <td class="titulo" colspan="4">Detalle de la entrada</td>
        </tr>
        <tr>
            <td><strong>Inventario:</strong></td>
            <td colspan="3"><?= $ROW[0]['producto'] ?></td>
        </tr>
        <tr>
            <td><strong>C&oacute;digo interno:</strong></td>
            <td><?= $ROW[0]['codigo'] ?></td>
            <td><strong># Lote:</strong></td>
            <td><?= $ROW[0]['lote'] ?></td>

        </tr>
        <tr>
            <td title="Orden de inicio"><strong># solicitud de compra:</strong></td>
            <td><?= $ROW[0]['oc_inicio'] ?></td>
            <td><strong>Tr&aacute;mite:</strong></td>
            <td><?= $ROW[0]['tramite'] ?></td>
        </tr>
        <tr>
            <td><strong># Orden de compra:</strong></td>
            <td><?= $ROW[0]['oc'] ?></td>
            <td><strong># Requisici&oacute;n:</strong></td>
            <td><?= $ROW[0]['requisicion'] ?></td>
        </tr>
        <tr>
            <td><strong># Factura:</strong></td>
            <td><?= $ROW[0]['factura'] ?></td>
            <td><strong>Proveedor:</strong></td>
            <td colspan="3"><?= $ROW[0]['proveedor'] ?></td>
        </tr>
        <tr>
            <td colspan="4">
                <hr/>
            </td>
        </tr>
        <tr>
            <td><strong>Cantidad bodega central:</strong></td>
            <td><?= $ROW[0]['stock1'] ?></td>
            <td><strong>Cantidad bodega auxiliar:</strong></td>
            <td><?= $ROW[0]['stock2'] ?></td>
        </tr>
        <tr>
            <td><strong>Costo total:</strong></td>
            <td><?= number_format($ROW[0]['costo'], 2, '.', ',') ?> CRC</td>
            <td><strong>Costo por unidad:</strong></td>
            <td><?= number_format(($ROW[0]['costo'] / ($ROW[0]['stock1'] + $ROW[0]['stock2'])), 2, '.', ',') ?> CRC</td>
        </tr>
        <tr>
            <td colspan="4">
                <hr/>
            </td>
        </tr>
        <tr>
            <td><strong>Tiene vencimiento?:</strong></td>
            <td><select disabled>
                    <option value="0">No</option>
                    <option value="1" <?php if ($ROW[0]['vence'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select>
            </td>
            <td><strong>Vencimiento:</strong></td>
            <td><?= $ROW[0]['vencimiento'] ?></td>
        </tr>
        <tr>
            <td><strong>Fecha aviso vencimiento:</strong></td>
            <td><?= $ROW[0]['aviso'] ?></td>
            <td><strong>Certificado?:</strong></td>
            <td><select disabled>
                    <option value="0">No</option>
                    <option value="1" <?php if ($ROW[0]['certificado'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><strong>% Pureza:</strong></td>
            <td colspan="3"><?= $ROW[0]['pureza'] ?></td>
        </tr>
    </table>
    <font>Registrado por: <?= $ROW[0]['usuario'] ?></font>
    <br/><br/>
    <input type="button" id="btn" value="Imprimir" class="boton" onClick="window.print()">
</center>
<?= $Gestor->Encabezado('F0005', 'p', '') ?>
</body>
</html>