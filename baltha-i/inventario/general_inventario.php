<?php
define('__MODULO__', 'inventario');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _general_inventario();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('f11', 'hr', 'Inventario :: Registro General de Inventario') ?>
<?= $Gestor->Encabezado('F0011', 'e', 'Registro General de Inventario') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" style="width:1050px">
            <tr>
                <td class="titulo" colspan="6">Filtro</td>
            </tr>
            <tr>
                <td>C&oacute;digo:<br/><input type="text" id="codigo" name="codigo" value="<?= $_POST['codigo'] ?>"/>
                </td>
                <td>Descripci&oacute;n:<br/><input type="text" name="descripcion" id="descripcion"
                                                   value="<?= $_POST['descripcion'] ?>"/></td>
                <td>N&uacute;mero de Parte:<br/><input type="text" name="parte" id="parte"
                                                       value="<?= $_POST['parte'] ?>"/></td>
                <td>Estado:<br/><input type="text" name="estado" id="estado" value="<?= $_POST['estado'] ?>"/></td>
                <td><br/><input type="button" value="Buscar" class="boton2" onclick="InventarioBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:98%">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>C&oacute;digo</th>
                <th>Descripci&oacute;n</th>
                <th>Cantidad LCC</th>
                <th>Cantidad Pavas</th>
                <th>Punto de reorden</th>
                <th>Fecha de vencimiento</th>
                <th>Estado</th>
                <th>Clasificaci&oacute;n ABC</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->InventarioMuestra();
            $cantidad = 0;
            $cantidadpavas = 0;
            $cantidadreg = 0;
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#" onclick="InventarioModifica('<?= $ROW[$x]['id'] ?>')"><?= $ROW[$x]['codigo'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['descripcion'] ?></td>
                    <td><?= $ROW[$x]['cantidadlcc'] ?></td>
                    <?php $cantidad += intval($ROW[$x]['cantidadlcc']) ?>
                    <td><?= $ROW[$x]['cantidadpavas'] ?></td>
                    <?php $cantidad += intval($ROW[$x]['cantidadpavas']) ?>
                    <td><?= $ROW[$x]['reorden'] ?></td>
                    <td><?= $ROW[$x]['vencimiento'] ?></td>
                    <td><?= $ROW[$x]['estado'] ?></td>
                    <td><?= $ROW[$x]['abc'] ?></td>
                    <td>
                        <img onclick="InventarioModifica('<?= $ROW[$x]['id'] ?>')"
                             src="<?php $Gestor->Incluir('previa', 'bkg') ?>" title="Ver detalle" class="tab3"/>&nbsp;
                        <img onclick="InventarioElimina('<?= $ROW[$x]['id'] ?>')"
                             src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar" class="tab2"/>
                    </td>
                </tr>
            <?php }
            $cantidadreg = $x; ?>
            </tbody>
        </table>
        <p><b>Cantidad total LLC: <?= number_format($cantidad, 2, '.', ',') ?></b></p>
        <p><b>Cantidad total Pavas: <?= number_format($cantidadpavas, 2, '.', ',') ?></b></p>
        <p><b>Cantidad total de Registros: <?= number_format($cantidadreg, 2, '.', ',') ?></b></p>
    </div>
    <br/><input type="button" value="Agregar" class="boton" onClick="InventarioAgrega();">
</center>
<?= $Gestor->Encabezado('F0011', 'p', '') ?>
</body>
</html>