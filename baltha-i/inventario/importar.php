<?php
define('__MODULO__', 'inventario');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _importar();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('f3', 'hr', 'Inventario :: Importar Inventario') ?>
<?= $Gestor->Encabezado('F0003', 'e', 'Importar Inventario') ?>
<center>
    <form action="../../caspha-i/shell/inventario/_importa.php" method="post" enctype="multipart/form-data"
          id="formulario" name="formulario">
        <input type="hidden" id="token" name="token"/>
        <input type="hidden" id="lab" name="lab" value="<?= $Gestor->getLID() ?>"
        <table class="radius" align="center" style="width:580px">
            <tr>
                <td class="titulo" colspan="5"><b>Seleccione el Tipo de Inventario a Importar: </b></td>
            </tr>
            <tr>
                <td><input type="radio" name="inventario" id="inventario" value="inventario" checked> Inventario</td>
                <td><input type="radio" name="inventario" id="inventario1" value="registro"> Registro General de
                    Inventario
                </td>
                <td><input type="radio" name="inventario" id="inventario2" value="io"> Control de Ingreso y Salida de
                    Materiales
                </td>
                <td><input type="radio" name="inventario" id="inventario3" value="vencido"> Control Material Vencido
                </td>
            </tr>
            <tr>
                <td><br><br><br><br><b>Archivo en Excel: </b><input type="file" name="archivo" id="archivo"></td>
                <td><br><br><br><br><input type="button" value="Importar" class="boton2"
                                           onclick="InventarioImportar()"/></td>
            </tr>
        </table>
    </form>
    <br/><br/><br/>
    <a href="../../caspha-i/docs/formato.xlsx">Descargar Formato Inventario SILAB</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="../../caspha-i/docs/formatoRGI.xlsx">Descargar Formato Registro General de Inventario</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="../../caspha-i/docs/formatoCIS.xlsx">Descargar Formato Control de Ingreso y Salida de Materiales</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="../../caspha-i/docs/formatoCMV.xlsx">Descargar Formato Control Material Vencido </a>
    <br/><br/><br/><br/><b>Nota: las hojas electronicas a importar, NO deben contener f&oacute;rmulas.</b>
</center>
<?= $Gestor->Encabezado('F0003', 'p', '') ?>
</body>
</html>