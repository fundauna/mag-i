<?php
define('__MODULO__', 'inventario');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _ingreso_salida();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('f13', 'hr', 'Inventario :: Control de ingreso y salida de materiales') ?>
<?= $Gestor->Encabezado('F0013', 'e', 'Control de ingreso y salida de materiales') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" style="width:1150px">
            <tr>
                <td class="titulo" colspan="9">Filtro</td>
            </tr>
            <tr>
                <td>C&oacute;digo:<br/><input type="text" id="codigo" name="codigo" value="<?= $_POST['codigo'] ?>"/>
                </td>
                <td>Tipo:<br/>
                    <select id="tipo" name="tipo">
                        <option value="">...</option>
                        <option value="1" <?= $_POST['tipo'] == 1 ? 'selected' : '' ?>>Entrada</option>
                        <option value="2" <?= $_POST['tipo'] == 2 ? 'selected' : '' ?>>Salida</option>
                    </select>
                </td>
                <td>Desde:<br/> <input type="text" id="desde" name="desde" class="fecha" readonly
                                       onClick="show_calendar(this.id);" value="<?= $_POST['desde'] ?>"></td>
                <td>Hasta:<br/> <input type="text" id="hasta" name="hasta" class="fecha" readonly
                                       onClick="show_calendar(this.id);" value="<?= $_POST['hasta'] ?>"></td>
                <td>Descripci&oacute;n:<br/><input type="text" name="descripcion" id="descripcion"
                                                   value="<?= $_POST['descripcion'] ?>"/></td>
                <td>Orden de compra:<br/><input type="text" name="orden" id="orden" value="<?= $_POST['orden'] ?>"/>
                </td>
                <td>Factura:<br/><input type="text" name="factura" id="factura" value="<?= $_POST['factura'] ?>"/></td>
                <td>Proveedor:<br/><input type="text" name="proveedor" id="proveedor"
                                          value="<?= $_POST['proveedor'] ?>"/></td>
                <td><br/><input type="button" value="Buscar" class="boton2" onclick="InventarioBuscar(this)"/>&nbsp;&nbsp;<input
                            type="button" value="Limpiar" class="boton2" onclick="location.href='ingreso_salida.php'"/>
                </td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:1050px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>C&oacute;digo</th>
                <th>Descripci&oacute;n</th>
                <th>Tipo</th>
                <th>Cantidad</th>
                <th>Unidad</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->InventarioMuestra();
            $cantidad = 0;
            $cantidadreg = 0;
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#" onclick="InventarioModifica('<?= $ROW[$x]['id'] ?>')"><?= $ROW[$x]['codigo'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['descripcion'] ?></td>
                    <td><?= $ROW[$x]['tipo'] == 1 ? 'Entrada' : 'Salida' ?></td>
                    <td><?= $ROW[$x]['cantidad'] ?></td>
                    <?php $cantidad += intval($ROW[$x]['cantidad']) ?>
                    <td><?= $ROW[$x]['unidad'] ?></td>
                    <td>
                        <img onclick="InventarioModifica('<?= $ROW[$x]['id'] ?>')"
                             src="<?php $Gestor->Incluir('previa', 'bkg') ?>" title="Ver detalle" class="tab3"/>&nbsp;
                        <img onclick="InventarioElimina('<?= $ROW[$x]['id'] ?>')"
                             src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar" class="tab2"/>
                    </td>
                </tr>
            <?php }
            $cantidadreg = $x; ?>
            </tbody>
        </table>
        <p><b>Cantidad total: <?= number_format($cantidad, 2, '.', ',') ?></b></p>
        <p><b>Cantidad total de Registros: <?= number_format($cantidadreg, 2, '.', ',') ?></b></p>
    </div>
    <br/><input type="button" value="Agregar" class="boton" onClick="InventarioAgrega();">
</center>
<?= $Gestor->Encabezado('F0013', 'p', '') ?>
</body>
</html>