<?php
define('__MODULO__', 'inventario');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _inventario();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('f3', 'hr', 'Inventario :: Cat�logo') ?>
<?= $Gestor->Encabezado('F0003', 'e', 'Cat�logo') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" style="width:1050px">
            <tr>
                <td class="titulo" colspan="6">Filtro</td>
            </tr>
            <tr>
                <td>Tipo:<br/>
                    <select name="tipo" style="width:150px;">
                        <option value="">...</option>
                        <?php
                        $ROW = $Gestor->TiposMuestra();
                        for ($x = 0; $x < count($ROW); $x++) {
                            ?>
                            <option <?= $_POST['tipo'] == $ROW[$x]['cs'] ? 'selected' : '' ?>
                                    value="<?= $ROW[$x]['cs'] ?>"><?= $ROW[$x]['nombre'] ?></option>
                        <?php } ?>
                        <option <?= $_POST['tipo'] == -99 ? 'selected' : '' ?> value="-99">Todos</option>
                    </select>
                </td>
                <td><input type="radio" id="lolo1" name="lolo" onclick="marca(1)"
                           value="1" <?php if ($_POST['lolo'] == 1) echo 'checked'; ?> />&nbsp;C&oacute;digo:<br/><input
                            type="text" id="codigo"
                            name="codigo" <?php if ($_POST['lolo'] == 2 || $_POST['lolo'] == 3 || $_POST['lolo'] == 4) echo 'readonly'; ?>
                            value="<?= $_POST['codigo'] ?>"/></td>
                <td><input type="radio" id="lolo2" name="lolo" onclick="marca(2)"
                           value="2" <?php if ($_POST['lolo'] == 2) echo 'checked'; ?>/>&nbsp;Nombre:<br/><input
                            type="text" name="nombre"
                            id="nombre" <?php if ($_POST['lolo'] == 1 || $_POST['lolo'] == 3 || $_POST['lolo'] == 4) echo 'readonly'; ?>
                            value="<?= $_POST['nombre'] ?>"/></td>
                <td><input type="radio" id="lolo3" name="lolo" onclick="marca(3)"
                           value="3" <?php if ($_POST['lolo'] == 3) echo 'checked'; ?>/>&nbsp;N&uacute;mero de
                    Parte:<br/><input type="text" name="parte"
                                      id="parte" <?php if ($_POST['lolo'] == 1 || $_POST['lolo'] == 2 || $_POST['lolo'] == 4) echo 'readonly'; ?>
                                      value="<?= $_POST['parte'] ?>"/></td>
                <td><input type="radio" id="lolo4" name="lolo" onclick="marca(4)"
                           value="4" <?php if ($_POST['lolo'] == 4) echo 'checked'; ?>/>&nbsp;Estado:<br/>
                    <input type="hidden" name="estado" id="estado"
                           value="<?= $_POST['estado'] == '' ? '1' : $_POST['estado'] ?>"/>
                    <select name="estado2" id="estado2" style="width:150px;"
                            onchange="refrescaEstado(this.value);" <?php if ($_POST['lolo'] == 1 || $_POST['lolo'] == 2 || $_POST['lolo'] == 3) echo 'disabled'; ?>>
                        <option <?= $_POST['estado'] == '' ? 'selected' : '' ?> value="1">...</option>
                        <option <?= $_POST['estado'] == '1' ? 'selected' : '' ?> value="1">Activos</option>
                        <option <?= $_POST['estado'] == '0' ? 'selected' : '' ?> value="0">Eliminados</option>
                    </select>
                </td>
                <td><br/><input type="button" value="Buscar" class="boton2" onclick="InventarioBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:1050px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>N&uacute;mero de Cat&aacute;logo</th>
                <?= $_POST['tipo'] == 39 ? '<th>Presentacion</th>' : '<th>Marca</th>' ?>
                <th>Nombre</th>
                <th>Stock</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->InventarioMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                if (isset($ROW[$x]['alerta'])) {
                    if ($ROW[$x]['alerta'] == '1') {
                        $str = 'style="color:#FF6600;cursor:help;" title="Stock m�nimo alcanzado"';
                    } elseif ($ROW[$x]['alerta'] == '2') {
                        $str = 'style="color:#993399;cursor:help;" title="Fecha de aviso alcanzada"';
                    } else {
                        $str = 'style="color:#FF0000;cursor:help;" title="**VENCIDO**"';
                    }
                } else {
                    if ($ROW[$x]['stock1'] <= $ROW[$x]['minimo1'] or $ROW[$x]['stock2'] <= $ROW[$x]['minimo2']) {
                        $str = 'style="color:#FF6600;cursor:help;" title="Stock m�nimo alcanzado"';
                    } else {
                        $str = '';
                    }
                }
                ?>
                <tr class="gradeA" <?= $str ?> align="center">
                    <td><a href="#" onclick="InventarioModifica('<?= $ROW[$x]['id'] ?>')"><?= $ROW[$x]['codigo'] ?></a>
                    </td>
                    <td width="120px"><?= $_POST['tipo'] == 39 ? $ROW[$x]['unidades'] : $ROW[$x]['marca'] ?></td>
                    <td><?= $ROW[$x]['nombre'] ?></td>
                    <td style="cursor:help"
                        title="Lab. :<?= $ROW[$x]['stock1'] ?>, Auxiliar:<?= $ROW[$x]['stock2'] ?>"><?= $ROW[$x]['stock1'] + $ROW[$x]['stock2'] ?></td>
                    <td>
                        <?php if ($ROW[$x]['estado'] == '1') { ?>
                            <?php if ($ROW[$x]['es'] == '1') { ?>
                                <img onclick="InventarioEntrada('<?= $ROW[$x]['id'] ?>')"
                                     src="<?php $Gestor->Incluir('mas', 'bkg') ?>" title="Registrar entrada"
                                     class="tab3"/>&nbsp;
                                <img onclick="InventarioSalida('<?= $ROW[$x]['id'] ?>')"
                                     src="<?php $Gestor->Incluir('menos', 'bkg') ?>" title="Registrar salida"
                                     class="tab3"/>&nbsp;
                                <img onclick="InventarioES_Historial('<?= $ROW[$x]['id'] ?>')"
                                     src="<?php $Gestor->Incluir('previa', 'bkg') ?>" title="Historial de movimientos"
                                     class="tab3"/>&nbsp;
                            <?php } ?>
                            <img onclick="InventarioElimina('<?= $ROW[$x]['id'] ?>')"
                                 src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar" class="tab2"/>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/><input type="button" value="Agregar" class="boton" onClick="InventarioAgrega();">
</center>
<?= $Gestor->Encabezado('F0003', 'p', '') ?>
</body>
</html>