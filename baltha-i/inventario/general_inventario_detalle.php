<?php
define('__MODULO__', 'inventario');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _general_inventario_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) {
    die('Registro inexistente');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <script>
        img1 = '<?php $Gestor->Incluir('closed', 'bkg') ?>';
        img2 = '<?php $Gestor->Incluir('downboxed', 'bkg') ?>';
    </script>
</head>
<body>
<?php $Gestor->Incluir('f12', 'hr', 'Inventario :: Registro General de Inventario') ?>
<?= $Gestor->Encabezado('F0012', 'e', 'Registro General de Inventario') ?>
<center>
    <input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
    <input type="hidden" id="id" value="<?= $ROW[0]['id'] ?>"/>
    <table class="radius" width="95%">
        <tr>
            <td class="titulo" colspan="2">Datos Generales</td>
        </tr>
        <tr>
            <td>C&oacute;digo:</td>
            <td><input type="text" id="codigo" value="<?= $ROW[0]['codigo'] ?>"></td>
        </tr>
        <tr>
            <td>Descripci&oacute;n:</td>
            <td><input type="text" id="descripcion" value="<?= $ROW[0]['descripcion'] ?>"></td>
        </tr>
        <tr>
            <td>Ubicaci&oacute;n (n&uacute;mero de estante):</td>
            <td><input type="text" id="ubicacion" value="<?= $ROW[0]['ubicacion'] ?>"></td>
        </tr>
        <tr>
            <td>Cantidad en LCC:</td>
            <td><input type="text" id="cantidadlcc" value="<?= $ROW[0]['cantidadlcc'] ?>" onblur="total(1)"></td>
        </tr>
        <tr>
            <td>Cantidad en Pavas:</td>
            <td><input type="text" id="cantidadpavas" value="<?= $ROW[0]['cantidadpavas'] ?>" onblur="total(1)"></td>
        </tr>
        <tr>
            <td>Cantidad total:</td>
            <td><input type="text" id="cantidadtotal" value="<?= $ROW[0]['cantidadtotal'] ?>" readonly></td>
        </tr>
        <tr>
            <td>Cantidad en tr&aacute;nsito:</td>
            <td><input type="text" id="cantidadtransito" value="<?= $ROW[0]['cantidadtransito'] ?>" onblur="total(2)">
            </td>
        </tr>
        <tr>
            <td>Nivel de inventario:</td>
            <td><input type="text" id="nivel" value="<?= $ROW[0]['nivel'] ?>" readonly></td>
        </tr>
        <tr>
            <td>Costo unitario (USD):</td>
            <td><input type="text" id="costounitariod" value="<?= $ROW[0]['costounitariod'] ?>" onblur="_FLOAT(this)">
            </td>
        </tr>
        <tr>
            <td>Costo unitario (CRC):</td>
            <td><input type="text" id="costounitarioc" value="<?= $ROW[0]['costounitarioc'] ?>" onblur="_FLOAT(this)">
            </td>
        </tr>
        <tr>
            <td>Costo total (CRC):</td>
            <td><input type="text" id="costototal" value="<?= $ROW[0]['costototal'] ?>" onblur="_FLOAT(this)"></td>
        </tr>
        <tr>
            <td>Presentaci&oacute;n:</td>
            <td><input type="text" id="presentacion" value="<?= $ROW[0]['presentacion'] ?>"></td>
        </tr>
        <tr>
            <td>N&uacute;mero de parte o cat&aacute;logo:</td>
            <td><input type="text" id="parte" value="<?= $ROW[0]['parte'] ?>"></td>
        </tr>
        <tr>
            <td>Equipo para el que se usa (solamente aplica para repuestos y consumibles):</td>
            <td><input type="text" id="equipo" value="<?= $ROW[0]['equipo'] ?>"></td>
        </tr>
        <tr>
            <td>A&ntilde;o de ingreso de equipo (si aplica):</td>
            <td><input type="text" id="ingreso" value="<?= $ROW[0]['ingreso'] ?>"></td>
        </tr>
        <tr>
            <td>An&aacute;lisis en el que se emplea:</td>
            <td><input type="text" id="analisis" value="<?= $ROW[0]['analisis'] ?>"></td>
        </tr>
        <tr>
            <td>Consumo por an&aacute;lisis (unidades):</td>
            <td><input type="text" id="consumoanalisis" value="<?= $ROW[0]['consumoanalisis'] ?>" onblur="_FLOAT(this)">
            </td>
        </tr>
        <tr>
            <td>Cantidad de an&aacute;lisis por a&ntilde;o:</td>
            <td><input type="text" id="cantidadanalisis" value="<?= $ROW[0]['cantidadanalisis'] ?>"
                       onblur="_FLOAT(this)"></td>
        </tr>
        <tr>
            <td>Consumo por a&ntilde;o (unidades):</td>
            <td><input type="text" id="consumoanual" value="<?= $ROW[0]['consumoanual'] ?>" onblur="total(3)"></td>
        </tr>
        <tr>
            <td>Unidad de medida:</td>
            <td><input type="text" id="medida" value="<?= $ROW[0]['medida'] ?>"></td>
        </tr>
        <tr>
            <td>Inventario de seguridad m&aacute;x (a&ntilde;os):</td>
            <td><input type="text" id="seguridadmax" value="<?= $ROW[0]['seguridadmax'] ?>" onblur="total(3)"></td>
        </tr>
        <tr>
            <td>Punto de reorden (Unidades):</td>
            <td><input type="text" id="reorden" value="<?= $ROW[0]['reorden'] ?>" readonly></td>
        </tr>
        <tr>
            <td>Fecha de vencimiento (si aplica):</td>
            <td><input type="text" id="vencimiento" value="<?= $ROW[0]['vencimiento'] ?>" class="fecha" readonly
                       onClick="show_calendar(this.id);"></td>
        </tr>
        <tr>
            <td>Estado de inventario:</td>
            <td><input type="text" id="estado" value="<?= $ROW[0]['estado'] ?>" readonly></td>
        </tr>
        <tr>
            <td>Cantidad a pedir para 1 a&ntilde;o de abastecimiento:</td>
            <td><input type="text" id="pedir" value="<?= $ROW[0]['pedir'] ?>" readonly></td>
        </tr>
        <tr>
            <td>Clasificaci&oacute;n ABC para control peri&oacute;dico:</td>
            <td><input type="text" id="abc" value="<?= $ROW[0]['abc'] ?>"></td>
        </tr>
        <tr>
            <td>Observaciones:</td>
            <td><input type="text" id="obs" value="<?= $ROW[0]['obs'] ?>"></td>
        </tr>
        <tr>
            <td>Encargado:</td>
            <td><input type="text" id="encargado" value="<?= $ROW[0]['encargado'] ?>"></td>
        </tr>
    </table>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
</center>
<?= $Gestor->Encabezado('F0012', 'p', '') ?>
</body>
</html>