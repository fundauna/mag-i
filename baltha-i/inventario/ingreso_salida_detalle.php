<?php
define('__MODULO__', 'inventario');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _ingreso_salida_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) {
    die('Registro inexistente');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <script>
        img1 = '<?php $Gestor->Incluir('closed', 'bkg') ?>';
        img2 = '<?php $Gestor->Incluir('downboxed', 'bkg') ?>';
    </script>
</head>
<body>
<?php $Gestor->Incluir('f13', 'hr', 'Inventario :: Control de ingreso y salida de materiales') ?>
<?= $Gestor->Encabezado('F0013', 'e', 'Control de ingreso y salida de materiales') ?>
<center>
    <input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
    <input type="hidden" id="id" value="<?= $ROW[0]['id'] ?>"/>
    <table class="radius" width="95%">
        <tr>
            <td class="titulo" colspan="2">Datos Generales</td>
        </tr>
        <tr>
            <td>Tipo:</td>
            <td>
                <select id="tipo" onchange="cambia();">
                    <option value="1" <?= $ROW[0]['tipo'] == '1' ? 'selected' : '' ?>>Entrada</option>
                    <option value="2" <?= $ROW[0]['tipo'] == '2' ? 'selected' : '' ?>>Salida</option>

                </select>
            </td>
        </tr>
        <tr>
            <td>C&oacute;digo material:</td>
            <td><input type="text" id="codigo" value="<?= $ROW[0]['codigo'] ?>"></td>
        </tr>
        <tr>
            <td>Descripci&oacute;n:</td>
            <td><input type="text" id="descripcion" value="<?= $ROW[0]['descripcion'] ?>"></td>
        </tr>
        <tr id="e1">
            <td>Orden de compra:</td>
            <td><input type="text" id="orden" value="<?= $ROW[0]['orden'] ?>"></td>
        </tr>
        <tr id="e2">
            <td>Factura:</td>
            <td><input type="text" id="factura" value="<?= $ROW[0]['factura'] ?>"></td>
        </tr>
        <tr id="e3">
            <td>Proveedor:</td>
            <td><textarea id="proveedor"><?= $ROW[0]['proveedor'] ?></textarea></td>
        </tr>
        <tr id="e4">
            <td>Costo:</td>
            <td><input type="text" id="costo" value="<?= $ROW[0]['costo'] ?>" onblur="_FLOAT(this)"></td>
        </tr>
        <tr id="e5">
            <td>Fecha de ingreso BI:</td>
            <td><input type="text" id="ingresobi" value="<?= $ROW[0]['ingresobi'] ?>" class="fecha" readonly
                       onClick="show_calendar(this.id);"></td>
        </tr>
        <tr id="e6">
            <td>Fecha de ingreso LCC:</td>
            <td><input type="text" id="ingresolcc" value="<?= $ROW[0]['ingresolcc'] ?>" class="fecha" readonly
                       onClick="show_calendar(this.id);"></td>
        </tr>
        <tr id="s1">
            <td>Fecha de salida:</td>
            <td><input type="text" id="salida" value="<?= $ROW[0]['salida'] ?>" class="fecha" readonly
                       onClick="show_calendar(this.id);"></td>
        </tr>
        <tr>
            <td>Cantidad:</td>
            <td><input type="text" id="cantidad" value="<?= $ROW[0]['cantidad'] ?>" onblur="_FLOAT(this)"></td>
        </tr>
        <tr>
            <td>Unidades:</td>
            <td><input type="text" id="unidad" value="<?= $ROW[0]['unidad'] ?>"></td>
        </tr>
        <tr>
            <td>Observaciones:</td>
            <td><textarea id="obs"><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
    </table>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
</center>
<script>cambia();</script>
<?= $Gestor->Encabezado('F0013', 'p', '') ?>
</body>
</html>