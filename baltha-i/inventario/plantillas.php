<?php
define('__MODULO__', 'inventario');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _plantillas();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('f10', 'hr', 'Inventario :: Plantillas') ?>
<?= $Gestor->Encabezado('F0010', 'e', 'Plantillas') ?>
<br/>
<table class="radius" align="center">
    <tr>
        <td class="titulo">Tipos Registrados</td>
    </tr>
    <tr>
        <td align="center">
            <select id="perfiles">
                <option value="">...</option>
                <?php
                $ROW = $Gestor->Tipos();
                for ($x = 0; $x < count($ROW); $x++) {
                    echo "<option value='{$ROW[$x]['cs']}'>{$ROW[$x]['nombre']}</option>";
                }

                unset($ROW);
                ?>
            </select>&nbsp;<input type="button" id="btn_buscar" value="Buscar" class="boton2"
                                  onClick="PermisosBuscar();">
        </td>
    </tr>
</table>
<br/>
<br>
<table class="radius" align="center">
    <tr>
        <td class="titulo">Campos Disponibles</td>
        <td class="titulo">Campos Asignados</td>
    </tr>
    <tr>
        <td>
            <select id="permisos" style="width:340px;" size="10">
                <?php
                $ROW = $Gestor->Campos();
                for ($x = 0; $x < count($ROW); $x++) {
                    echo "<option value='{$ROW[$x]['id']}'>{$ROW[$x]['nombre']}</option>";
                }
                ?>
            </select>
        </td>
        <td id="td_asignados">
            <select id="asignados" style="width:340px;" size="10"></select>
        </td>
    </tr>
    <tr>
        <td>
            <input id="btn_agregar" type="button" value="Agregar" class="boton" onClick="PermisosAgrega();" disabled>
            <input id="btn_agrega" type="button" value="Nuevo" class="boton" onClick="PermisosNuevo();">
        </td>
        <td><input id="btn_eliminar" type="button" value="Eliminar" class="boton" onClick="PermisosElimina();" disabled>
        </td>
    </tr>
</table>
<?= $Gestor->Encabezado('F0010', 'p', '') ?>
</body>
</html>