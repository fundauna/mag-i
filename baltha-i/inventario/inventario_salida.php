<?php
define('__MODULO__', 'inventario');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _inventario_salida();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

$nombre = $ROW[0]['codigo'] . ' ' . $ROW[0]['unids'] . ' ' . $ROW[0]['nombre'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('f6', 'hr', 'Inventario :: Salida de inventario') ?>
<?= $Gestor->Encabezado('F0006', 'e', 'Salida de inventario') ?>
<center>
    <table class="radius" width="98%">
        <tr>
            <td class="titulo" colspan="4">Informaci&oacute;n general</td>
        </tr>
        <tr>
            <td><strong>C&oacute;digo:</strong></td>
            <td><?= $ROW[0]['codigo'], ' ', $ROW[0]['unids'] ?></td>
            <td><strong>Stock bodega laboratorio:</strong></td>
            <td><?= $ROW[0]['stock1'] ?></td>
        </tr>
        <tr>
            <td><strong>Nombre:</strong></td>
            <td><?= $ROW[0]['nombre'] ?></td>
            <td><strong>Stock bodega auxiliar:</strong></td>
            <td><?= $ROW[0]['stock2'] ?></td>
        </tr>
        <tr>
            <td><strong>Marca:</strong></td>
            <td><?= $ROW[0]['marca'] ?></td>
            <td><strong>Presentaci&oacute;n:</strong></td>
            <td><?= $ROW[0]['unids'] ?></td>
        </tr>
    </table>
    <br/>
    <div id="container" style="width:98%">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>&nbsp;</th>
                <th title="C�digo interno">Cod. Int.</th>
                <th>Fecha</th>
                <th title="Movimiento a bodega laboratorio">Bod. Laboratorio</th>
                <th title="Movimiento a bodega auxiliar">Bod. Auxiliar</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->Disponibles();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td>
                        <img onclick="Marca('<?= $ROW[$x]['id'] ?>', '<?= $ROW[$x]['codigo'] ?>', '<?= $ROW[$x]['bodega1'] ?>', '<?= $ROW[$x]['bodega2'] ?>')"
                             src="<?php $Gestor->Incluir('mod', 'bkg') ?>" class="tab3"/></td>
                    <td><?= $ROW[$x]['codigo'] ?></td>
                    <td><?= $ROW[$x]['fecha1'] ?></td>
                    <td><?= $ROW[$x]['bodega1'] ?></td>
                    <td><?= $ROW[$x]['bodega2'] ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/>
    <table id="tabla" class="radius" width="98%">
        <input type="hidden" id="id"/><!-- codigo unico del movimiento -->
        <input type="hidden" id="bodega1"/>
        <input type="hidden" id="bodega2"/>
        <thead>
        <tr>
            <td class="titulo" colspan="3">Opciones</td>
        </tr>
        <tr>
            <td id="ref"></td>
        </tr>
        <tr>
            <td><strong>Tipo de salida:</strong></td>
            <td colspan="2"><select id="tipo" onchange="CambiaTipo(this.value)">
                    <option value="">Seleccione el tipo de salida</option>
                    <option value='T'>Traslado</option>
                    <optgroup label="Salidas"></optgroup>
                    <option value='0'>Consumo</option>
                    <option value='1'>Da&ntilde;o</option>
                    <option value='2'>Donaci&oacute;n</option>
                    <option value='3'>Mantenimiento</option>
                    <option value='4'>Obsolescencia</option>
                    <option value='5'>Pr&eacute;stamo</option>
                </select></td>
        </tr>
        </thead>
        <tr id="tr_translado" style="display:none">
            <td><strong>Tipo de traslado:</strong></td>
            <td><select id="tipo_tras">
                    <option value="A">Bodega Auxiliar hacia Laboratorio</option>
                    <option value="C">Bodega Laboratorio hacia Auxiliar</option>
                </select></td>
            <td><strong>Cantidad:</strong> <input type="text" id="cant_tras" maxlength="6" value="0"
                                                  onblur="Entero(this);" class="cantidad"></td>
        </tr>
        <tr id="tr_salida" style="display:none">
            <td><strong>Rebajar de:</strong></td>
            <td>Bod. Laboratorio: <input type="text" id="cant_bod1" maxlength="6" value="0" onblur="Entero(this);"
                                         class="cantidad"></td>
            <td>Bod. Auxiliar: <input type="text" id="cant_bod2" maxlength="6" value="0" onblur="Entero(this);"
                                      class="cantidad"></td>
        </tr>
        <tr id="tr_boton" style="display:none">
            <td align="center" colspan="3"><br/><input type="button" id="btn" value="Aceptar" class="boton"
                                                       onClick="datos(<?= $_GET['ID'] ?>)"></td>
        </tr>
    </table>
</center>
<?= $Gestor->Encabezado('F0006', 'p', '') ?>
</body>
</html>