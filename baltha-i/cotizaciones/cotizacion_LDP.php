<?php
define('__MODULO__', 'cotizaciones');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _cotizacion_LDP();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <script>
        img1 = '<?php $Gestor->Incluir('closed', 'bkg')?>';
        img2 = '<?php $Gestor->Incluir('downboxed', 'bkg')?>';
    </script>
</head>
<body>
<form name="form" id="form" method="post" enctype="multipart/form-data"
      action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
    <input type="hidden" name="ID" id="ID" value="<?= $ROW[0]['numero'] ?>"/>
    <input type="hidden" name="accion" value="<?= $_GET['acc'] ?>"/>
    <center>
        <?php $Gestor->Incluir('d8', 'hr', 'Cotizaciones :: Cotizaci&oacute;n de An&aacute;lisis') ?>
        <?= $Gestor->Encabezado('D0008', 'e', 'Cotizaci&oacute;n de An&aacute;lisis') ?>
        <br>
        <table class="radius" style="font-size:12px" width="600px">
            <tr>
                <td class="titulo" colspan="2">Informaci&oacute;n General</td>
            </tr>
            <tr>
                <td colspan="2">Solicitud de origen:&nbsp;<input type="text" id="solicitud" name="solicitud"
                                                                 value="<?= $ROW[0]['solicitud'] ?>" size="12"
                                                                 maxlength="12" onblur="SolicitudCarga(this.value)">&nbsp;&nbsp;o
                    &nbsp;&nbsp;Apertura de origen:&nbsp;<input type="text" id="apertura" name="apertura"
                                                                value="<?= $ROW[0]['apertura'] ?>" size="12"
                                                                maxlength="12" onblur="AperturaCarga(this.value)"></td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td width="89">Fecha:</td>
                <td width="499"><?= $ROW[0]['fecha'] ?></td>
            </tr>
            <tr>
                <td>Estado:</td>
                <td><?= $Gestor->Estado($ROW[0]['estado']) ?></td>
            </tr>
            <tr>
                <td>Cliente:</td>
                <td><?= $ROW[0]['tmp'] ?>
                    <input type="hidden" id="cliente" name="cliente" value="<?= $ROW[0]['cliente'] ?>"/></td>
            </tr>
            <tr>
                <td>Solicitante:</td>
                <td><?= $ROW[0]['solicitante'] ?><input type="hidden" name="solicitante"
                                                        value="<?= $ROW[0]['solicitante'] ?>"/></td>
            </tr>
            <tr>
                <td>Cultivo:</td>
                <td><?= $ROW[0]['cultivo'] ?></td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td colspan="2"><input type="checkbox" id="desc1"
                                       name="desc1" <?php if ($ROW[0]['descuento']) echo 'checked'; ?>
                                       onclick="Totales()"/>&nbsp;Aplicar descuento para cultivos especiales(20% de
                    descuento, c&oacute;digos 8)
                </td>
            </tr>
        </table>
        <br/>
        <table class="radius" style="font-size:12px" width="600px">
            <thead>
            <tr>
                <td class="titulo" colspan="<?= $_GET['acc'] == 'I' ? '6' : '5' ?>">
                    Detalle<?php if ($ROW[0]['estado'] == '' or $ROW[0]['estado'] == '0') { ?>&nbsp;<img
                        onclick="AnalisisMas()" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                        class="tab" /><?php } ?></td>
            </tr>
            <tr>
                <td><strong>Item</strong></td>
                <td><strong>Descripci&oacute;n</strong></td>
                <td title="Cantidad de muestras"><strong>Cant.</strong></td>
                <td><strong>P. Unitario</strong></td>
                <td><strong>P. Total</strong></td>
                <?php if ($_GET['acc'] == 'I'): ?>
                    <td align="center"><strong>Opciones</strong></td><?php endif; ?>
            </tr>
            </thead>
            <tbody id="lolo">
            <?php
            $ROW2 = $Gestor->ObtieneLineas();
            for ($x = 0, $total = 0; $x < count($ROW2); $x++) {
                $total += $ROW2[$x]['cantidad'];
                ?>
                <tr>
                    <td><input type="text" id="item<?= $x ?>" name="item[]" size="3" readonly
                               value="<?= $ROW2[$x]['tarifa'] ?>"/></td>
                    <td><input type="text" id="analisis<?= $x ?>" name="analisis[]" class="lista2" readonly
                               onclick="AnalisisLista(<?= $x ?>)" value="<?= $ROW2[$x]['analisis'] ?>"/>
                        <input type="hidden" id="codigo<?= $x ?>" name="codigo[]" value="<?= $ROW2[$x]['codigo'] ?>"/>
                    </td>
                    <td><input type="text" id="cant<?= $x ?>" name="cant[]" maxlength="3"
                               value="<?= $ROW2[$x]['cantidad'] ?>" onblur="_INT(this);Totales();" class="cantidad">
                    </td>
                    <td><input type="text" id="punit<?= $x ?>" name="punit[]"
                               value="<?= $Gestor->Formato($ROW2[$x]['monto']) ?>" class="monto"
                               onblur="_FLOAT(this);Totales();"></td>
                    <td><input type="text" id="ptotal<?= $x ?>" name="ptotal[]"
                               value="<?= $Gestor->Formato($ROW2[$x]['total']) ?>" class="monto" readonly></td>
                    <?php if ($_GET['acc'] == 'I'): ?>
                        <td>
                            <img onclick="EliminaLinea($( this ))" src="../../caspha-i/imagenes/del.png"
                                 title="Eliminar" class="tab2"/>
                        </td>
                    <?php endif; ?>
                </tr>
                <?php
            }
            ?>
            </tbody>
            <tr>
                <td colspan="5">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td align="right"><strong>N&uacute;mero de muestras a realizar:</strong></td>
                <td colspan="3"><input type="text" id="total" name="total" value="<?= $total ?>" class="cantidad"
                                       readonly>
                    <input type="hidden" id="moneda" name="moneda" value="0"/></td>
            </tr>
            <tr>
                <td colspan="4" align="right"><strong>Total &cent;:</strong></td>
                <td><input type="text" id="monto" name="monto" value="<?= $Gestor->Formato($ROW[0]['monto']) ?>"
                           class="monto" readonly></td>
            </tr>
            <?php if ($ROW[0]['estado'] == '1') { ?>
                <tr>
                    <td colspan="5">
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td colspan="5"><strong># de recibo:</strong>&nbsp;<input type="text" name="deposito" id="deposito"
                                                                              size="20" maxlength="50"/>&nbsp;
                        <input type="file" name="archivo" id="archivo" style="width:200px;"/>
                        &nbsp;<input type="button" value="Finalizar" class="boton" onclick="SolicitudCancela(this)">
                    </td>
                </tr>
            <?php } elseif (($ROW[0]['estado'] == '0' or $ROW[0]['estado'] == '3') && $_GET['acc'] == 'M') { ?>
                <tr>
                    <td id="td_obs" colspan="5"></td>
                </tr>
            <?php } elseif ($ROW[0]['estado'] == '2' or $ROW[0]['estado'] == '5') { ?>
                <tr>
                    <td colspan="5">
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td id="td_obs" colspan="5"><strong>Observaciones:</strong>&nbsp;<?= $ROW[0]['obs'] ?>
                        <?php
                        $ruta = "../../caspha-i/docs/cotizaciones/" . $_GET['ID'] . '.zip';
                        if (file_exists($ruta)) {
                            ?>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            Descargar adjunto:&nbsp;<img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>"
                                                         title="Descargar"
                                                         onclick="DocumentosZip('<?= $_GET['ID'] ?>', '<?= __MODULO__ ?>')"
                                                         class="tab2"/>
                        <?php } ?>
                    </td>
                </tr>
                <?php
            }//elseif
            ?>
            <?php
            if ($_GET['acc'] == 'M') {
                ?>
                <tr>
                    <td colspan="5"><br/><img id="i_historial" src="<?= $Gestor->Incluir('closed', 'bkg') ?>"
                                              onclick="Oculta()" title="Mostrar/Ocultar"
                                              style="vertical-align:baseline;cursor:pointer"/>&nbsp;<strong>Historial</strong>
                        <hr/>
                    </td>
                </tr>
                <tfoot id="t_historial" style="display:none">
                <tr>
                    <td><strong>Fecha</strong></td>
                    <td colspan="2"><strong>Usuario</strong></td>
                    <td><strong>Acci&oacute;n</strong></td>
                </tr>
                <?php
                $ROW2 = $Gestor->Historial();
                for ($x = 0; $x < count($ROW2); $x++) {
                    ?>
                    <tr>
                        <td><?= $ROW2[$x]['fecha1'] ?></td>
                        <td colspan="2"><?= $ROW2[$x]['nombre'] . ' ' . $ROW2[$x]['ap1'] . ' ' . $ROW2[$x]['ap2'] ?></td>
                        <td><?= $Gestor->Accion($ROW2[$x]['accion']) ?></td>
                    </tr>
                    <?php
                }//FOR
                ?>
                </tfoot>
                <?php
            }
            ?>
        </table>
        <br/>
        <?php if ($ROW[0]['estado'] == '' || $ROW[0]['estado'] == '0' && $_GET['acc'] != 'M') { ?>
            <input type="button" value="Aceptar" class="boton" onclick="SolicitudGenerar(this)">&nbsp;&nbsp;
        <?php } ?>
        <?php if ($ROW[0]['estado'] == '0' && $_GET['acc'] == 'M') { ?>
            <input type="button" value="Modificar" class="boton" onclick="SolicitudGenerar(this)">&nbsp;&nbsp;
            <input type="button" value="Procesar" class="boton" onclick="SolicitudModificar(this, '', '3')">&nbsp;&nbsp;
        <?php } ?>
        <?php if ($ROW[0]['estado'] == '3' && $Gestor->Aprobar()) { ?>
            <input type="button" value="Aprobar" class="boton" onclick="SolicitudModificar(this, '', '1')">&nbsp;&nbsp;
        <?php } ?>
        <?php if ($ROW[0]['estado'] != '5' && $ROW[0]['estado'] != '2' && $_GET['acc'] == 'M') { ?>
            <input type="button" value="Anular" class="boton" onclick="SolicitudAnular(this)">&nbsp;&nbsp;
        <?php } ?>
        <?php if ($_GET['acc'] == 'M') { ?>
            <input type="button" value="Imprimir" class="boton" onclick="window.print()">
        <?php } ?>
    </center>
</form>
<script>
    Totales();
    <?php if($ROW[0]['estado'] != '' && $ROW[0]['estado'] != '0'){?>Deshabilita();<?php } ?>
</script>
<?= $Gestor->Encabezado('D0008', 'p', '') ?>
<?= $Gestor->Footer() ?>
</body>
</html>