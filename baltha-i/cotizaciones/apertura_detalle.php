<?php
define('__MODULO__', 'cotizaciones');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _apertura_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<center>
    <?php $Gestor->Incluir('d1', 'hr', 'Cotizaciones :: Solicitud de Apertura') ?>
    <?= $Gestor->Encabezado('D0001', 'e', 'Solicitud de Apertura') ?>
    <br>
    <table class="radius" style="font-size:12px" width="600px">
        <tr>
            <td class="titulo" colspan="2">1. Informaci&oacute;n general</td>
        </tr>
        <tr>
            <td>Estado:</td>
            <td><?= $Gestor->Estado($ROW[0]['estado']) ?></td>
        </tr>
        <tr>
            <td>Funcionario que solicita:</td>
            <td><?= $ROW[0]['funcionario'] ?></td>
        </tr>
        <tr>
            <td>Estaci&oacute;n de inter&eacute;s:</td>
            <td><select disabled>
                    <option>Puerto Lim&oacute;n</option>
                    <option <?php if ($ROW[0]['estacion'] == '1') echo 'selected'; ?>>Puerto Caldera</option>
                    <option <?php if ($ROW[0]['estacion'] == '2') echo 'selected'; ?>>Pe&ntilde;as Blancas</option>
                    <option <?php if ($ROW[0]['estacion'] == '3') echo 'selected'; ?>>Paso Canoas</option>
                    <option <?php if ($ROW[0]['estacion'] == '4') echo 'selected'; ?>>Aeropuerto de Liberia</option>
                    <option <?php if ($ROW[0]['estacion'] == '5') echo 'selected'; ?>>Aeropuerto Juan
                        Santamar&iacute;a
                    </option>
                    <option <?php if ($ROW[0]['estacion'] == '6') echo 'selected'; ?>>Los Chiles</option>
                    <option <?php if ($ROW[0]['estacion'] == '7') echo 'selected'; ?>>Aeropuerto de Pavas</option>
                    <option <?php if ($ROW[0]['estacion'] == '8') echo 'selected'; ?>>Sixaola</option>
                    <option <?php if ($ROW[0]['estacion'] == '9') echo 'selected'; ?>>Otra:</option>
                </select>&nbsp;&nbsp;<?= $ROW[0]['otro'] ?></td>
        </tr>
        <tr>
            <td>Fecha deseada para apertura:</td>
            <td><?= $ROW[0]['fecha1'] ?></td>
        </tr>
        <tr>
            <td>Nombre del cultivo:</td>
            <td><?= $ROW[0]['cultivo'] ?></td>
        </tr>
        <tr>
            <td>Observaciones:</td>
            <td id="td_obs"><?= $ROW[0]['obs'] ?></td>
        </tr>
        <tr>
            <td>Carta de solicitud de apertura:</td>
            <td><?php
                $ruta = "../../caspha-i/docs/aperturas/" . $_GET['ID'] . '.zip';
                if (file_exists($ruta)) {
                    ?>
                    &nbsp;<img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar"
                               onclick="DocumentosZip('<?= $_GET['ID'] ?>', 'aperturas')"
                               class="tab2"/>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php } ?></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="600px">
        <thead>
        <tr>
            <td class="titulo" colspan="5">2. Servicio(s) o producto(s) requerido(s)</td>
        </tr>
        <tr>
            <td><strong>#</strong></td>
            <td><strong>Tarifa</strong></td>
            <td><strong>An&aacute;lisis</strong></td>
            <td><strong># de muestras</strong></td>
            <td><strong>Departamento</strong></td>
        </tr>
        </thead>
        <?php
        $ROW2 = $Gestor->ObtieneDetalle();
        for ($x = 0, $total = 0; $x < count($ROW2); $x++) {
            $total += $ROW2[$x]['cantidad'];
            ?>
            <tr>
                <td><?= $x + 1 ?></td>
                <td><?= $ROW2[$x]['tarifa'] ?></td>
                <td><?= $ROW2[$x]['nombre'] ?></td>
                <td><?= $ROW2[$x]['cantidad'] ?></td>
                <td><select disabled>
                        <option>Entomolog&iacute;a</option>
                        <option <?php if ($ROW2[$x]['dpto'] == '1') echo 'selected'; ?>>Fitopatolog&iacute;a</option>
                        <option <?php if ($ROW2[$x]['dpto'] == '2') echo 'selected'; ?>>Nematolog&iacute;a</option>
                        <option <?php if ($ROW2[$x]['dpto'] == '3') echo 'selected'; ?>>Biolog&iacute;a molecular
                        </option>
                    </select></td>
            </tr>
            <?php
        }
        unset($ROW2);
        ?>
        <tr>
            <td colspan="5">
                <hr/>
            </td>
        </tr>
        <tr>
            <td colspan="3"><strong>N&uacute;mero de muestras a realizar:</strong></td>
            <td colspan="2"><?= $total ?></td>
        </tr>
    </table>
    <br/>
    <?php
    if ($ROW[0]['estado'] == '0' && $_GET['admin'] == '1' && $Gestor->Aprobar()) {
        ?>
        <input type="button" value="Aprobar" class="boton" onclick="LabAprobar('<?= $_GET['ID'] ?>')">&nbsp;
        <input type="button" value="Anular" class="boton" onclick="LabAnular('<?= $_GET['ID'] ?>')">&nbsp;
        <?php
    }
    ?>
    <input type="button" value="Imprimir" class="boton" onclick="window.print()">
</center>
</form>
<br><?= $Gestor->Encabezado('D0001', 'p', '') ?>
<?= $Gestor->Footer() ?>
</body>
</html>