<?php
define('__MODULO__', 'cotizaciones');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _aperturas();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('d2', 'hr', 'Aperturas :: Historial') ?>
<?= $Gestor->Encabezado('D0002', 'e', 'Historial') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" width="550px">
            <tr>
                <td class="titulo" colspan="4">Filtro</td>
            </tr>
            <tr>
                <td>Desde: <input type="text" id="desde" name="desde" class="fecha" value="<?= $_POST['desde'] ?>"
                                  readonly onClick="show_calendar(this.id);">&nbsp;
                    Hasta: <input type="text" id="hasta" name="hasta" class="fecha" value="<?= $_POST['hasta'] ?>"
                                  readonly onClick="show_calendar(this.id);"></td>
                <td>Estado:</td>
                <td><select name="estado">
                        <option value="0">Generada</option>
                        <option value="1" <?php if ($_POST['estado'] == '1') echo 'selected'; ?>>Aprobada</option>
                        <option value="a" <?php if ($_POST['estado'] == 'a') echo 'selected'; ?>>Anulada</option>
                        <option value="" <?php if ($_POST['estado'] == '') echo 'selected'; ?>>Todos</option>
                    </select></td>
                <td><input type="button" value="Buscar" class="boton" onclick="SolicitudesBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:550px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>No.</th>
                <th>Fecha</th>
                <th>Cultivo</th>
                <th>Estado</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->SolicitudesMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#" onclick="SolicitudesDetalle('<?= $ROW[$x]['id'] ?>')"><?= $ROW[$x]['id'] ?></a></td>
                    <td><?= $ROW[$x]['fecha1'] ?></td>
                    <td><?= $ROW[$x]['cultivo'] ?></td>
                    <td><?= $Gestor->Estado($ROW[$x]['estado']) ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/><input type="button" value="Agregar" class="boton" onClick="AperturaCrear();">
</center>
<?= $Gestor->Encabezado('D0002', 'p', '') ?>
</body>
</html>