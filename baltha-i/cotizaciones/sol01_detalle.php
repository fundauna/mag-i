<?php
define('__MODULO__', 'cotizaciones');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _sol01_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<center>
    <?php $Gestor->Incluir('d17', 'hr', 'Cotizaciones :: Solicitud de Cotizaci&oacute;n de An&aacute;lisis de Fertilizantes') ?>
    <?= $Gestor->Encabezado('D0017', 'e', 'Solicitud de Cotizaci&oacute;n de An&aacute;lisis de Fertilizantes') ?>
    <table width="100%">
        <tr align="right">
            <td colspan="2"><font size='-2'>[D0017]</font></td>
        </tr>
    </table>
    <br>
    <table class="radius" style="font-size:12px" width="600px">
        <tr>
            <td class="titulo" colspan="2">1. Datos de la persona, departamento o empresa solicitante</td>
        </tr>
        <tr>
            <td><strong>Nombre:</strong> <?= $ROW[0]['nom_cliente'] ?></td>
            <td><strong>Estado: </strong> <?= $Gestor->Estado($ROW[0]['estado']) ?></td>
        </tr>
        <tr>
            <td><strong>Tel&eacute;fono:</strong> <?= $ROW[0]['tel'] ?></td>
            <td><strong>Fax:</strong> <?= $ROW[0]['fax'] ?></td>
        </tr>
        <tr>
            <td><strong>Fecha </strong> <?= $ROW[0]['fecha'] ?></td>
            <td><strong>Nombre del representante legal:</strong> <?= $ROW[0]['representante'] ?></td>
        </tr>
    </table>
    <br>
    <table class="radius" style="font-size:12px" width="600px">
        <tr>
            <td class="titulo" colspan="2">2. Datos sobre el producto</td>
        </tr>
        <tr>
            <td width="206"><strong>F&oacute;rmula del fertilizante:</strong></td>
            <td width="382"><?= $ROW[0]['formula'] ?></td>
        </tr>
        <tr>
            <td><strong>Nombre comercial:</strong></td>
            <td><?= $ROW[0]['comercial'] ?></td>
        </tr>
        <tr>
            <td><strong>Observaciones:</strong></td>
            <td id="td_obs"><?= $ROW[0]['obs'] ?></td>
        </tr>
        <tr>
            <td><strong>Tipo de formulaci&oacute;n:</strong></td>
            <td>
                <select disabled>
                    <option value="">...</option>
                    <?php
                    $ROW2 = $Gestor->Formulaciones();
                    for ($x = 0; $x < count($ROW2); $x++) {
                        ?>
                        <option value="<?= $ROW2[$x]['id'] ?>" <?php if ($ROW[0]['tipo'] === $ROW2[$x]['id']) echo 'selected'; ?>><?= $ROW2[$x]['nombre'] ?></option>
                        <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td><strong>M&eacute;todo de an&aacute;lisis:</strong></td>
            <td><select disabled>
                    <option>Laboratorio</option>
                    <option <?php if ($ROW[0]['metodo'] == '1') echo 'selected' ?>>Registro</option>
                    <option <?php if ($ROW[0]['metodo'] == '2') echo 'selected' ?>>Cliente</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><strong>Tipo de mezcla:</strong></td>
            <td><select disabled>
                    <option>Mezcla f&iacute;sica</option>
                    <option <?php if ($ROW[0]['mezcla'] == '1') echo 'selected' ?>>Fertilizante qu&iacute;mico</option>
                    <option <?php if ($ROW[0]['mezcla'] == '2') echo 'selected' ?>>Fertilizante l&iacute;quido</option>
                    <option <?php if ($ROW[0]['mezcla'] == '3') echo 'selected' ?>>Fertilizante hidrosoluble</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><strong>Cliente aporta documentaci&oacute;n:</strong></td>
            <td><select disabled>
                    <option>N/A</option>
                    <option <?php if ($ROW[0]['aporta'] == '0') echo 'selected' ?>>No</option>
                    <option <?php if ($ROW[0]['aporta'] == '1') echo 'selected' ?>>S&iacute;</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><strong>Suministro del est&aacute;ndar:</strong></td>
            <td><select disabled>
                    <option value="0">Laboratorio</option>
                    <option <?php if ($ROW[0]['suministro'] == '1') echo 'selected' ?>>Cliente</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><strong>Muestra de discrepancia:</strong></td>
            <td><select disabled>
                    <option value="0">No</option>
                    <option <?php if ($ROW[0]['discre'] == '1') echo 'selected' ?>>S&iacute;</option>
                </select>
            </td>
        </tr>
        <?php if ($ROW[0]['discre'] == '1') { ?>
            <tr>
                <td><strong># Solicitud:</strong> <?= $ROW[0]['num_sol'] ?></td>
                <td><strong>C&oacute;digo(s) externo:</strong> <?= $ROW[0]['num_mue'] ?></td>
            </tr>
        <?php } ?>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="600px">
        <thead>
        <tr>
            <td class="titulo" colspan="6">3. An&aacute;lisis Solicitados</td>
        </tr>
        <tr>
            <td><strong>#</strong></td>
            <td><strong>An&aacute;lisis</strong></td>
            <td><strong>Concentraci&oacute;n declarada</strong></td>
            <td><strong>Tipo</strong></td>
            <td><strong>Unidad</strong></td>
            <td><strong>Fuente</strong></td>
        </tr>
        </thead>
        <?php
        $ROW2 = $Gestor->ObtieneDetalle(1);
        for ($x = 0, $total = 0; $x < count($ROW2); $x++) {
            ?>
            <tr>
                <td><?= $x + 1 ?></td>
                <td><?= $ROW2[$x]['nombre'] ?></td>
                <td><?= $ROW2[$x]['rango'] ?></td>
                <td><select disabled>
                        <option>N/A</option>
                        <option <?php if ($ROW2[$x]['quela'] == '0') echo 'selected'; ?>>Disponible</option>
                        <option <?php if ($ROW2[$x]['quela'] == '1') echo 'selected'; ?>>Total</option>
                        <option <?php if ($ROW2[$x]['quela'] == '2') echo 'selected'; ?>>Quelatado</option>
                        <option <?php if ($ROW2[$x]['quela'] == '3') echo 'selected'; ?>>No-Quelatado</option>
                    </select></td>
                <td><select disabled>
                        <option>%m/m</option>
                        <option <?php if ($ROW2[$x]['tipo'] == '1') echo 'selected'; ?>>%m/v</option>
                        <option <?php if ($ROW2[$x]['tipo'] == '2') echo 'selected'; ?>>ppm</option>
                    </select></td>
                <td><?= $ROW2[$x]['fuente'] ?></td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td colspan="6">
                <hr/>
            </td>
        </tr>
        <tr>
            <td colspan="3"><strong>N&uacute;mero de muestras a realizar:</strong></td>
            <td colspan="3"><?= $ROW[0]['muestras'] ?></td>
        </tr>
        <tr>
            <td colspan="6">
                <hr/>
                <strong>Impurezas:</strong></td>
        </tr>
        <?php
        $ROW2 = $Gestor->ObtieneDetalle(2);
        for ($x = 0, $total = 0; $x < count($ROW2); $x++) {
            ?>
            <tr>
                <td><?= $x + 1 ?></td>
                <td><?= $ROW2[$x]['nombre'] ?></td>
                <td colspan="4"></td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td colspan="6">
                <hr/>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <input type="checkbox" <?php if ($Gestor->Existe('E', $ROW[0]['tipo_impu'])) echo 'checked'; ?>
                       disabled/>&nbsp;Densidad&nbsp;
                <input type="checkbox" <?php if ($Gestor->Existe('F', $ROW[0]['tipo_impu'])) echo 'checked'; ?>
                       disabled/>&nbsp;Granulometr&iacute;a
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="600px">
        <tr>
            <td class="titulo" colspan="2">4. Nombre del solicitante</td>
        </tr>
        <tr>
            <td width="100"><strong>Nombre:</strong></td>
            <td><?= $ROW[0]['nombre_sol'] ?></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="600px">
        <tr>
            <td class="titulo" colspan="2">5. Nombre del encargado de entregar las muestras en el laboratorio</td>
        </tr>
        <tr>
            <td width="100"><strong>Nombre:</strong></td>
            <td><?= $ROW[0]['encargado'] ?></td>
        </tr>
    </table>
    <?= $Gestor->Historial($ROW[0]['estado']) ?>
    <br/><?= $Gestor->Encabezado('D0017', 'p', '') ?>
    <br/>
    <?php
    if ($ROW[0]['estado'] == 'r') {
        //ANULAR POR CLIENTE
        if ($_GET['admin'] == '') { ?>
            <input type="button" value="Anular" class="boton" onclick="ClienteAnular('<?= $_GET['ID'] ?>')">&nbsp;
            <?php
            //ANULAR POR EL LABORATORIO
        } elseif ($_GET['admin'] == '1' && $Gestor->Aprobar()) { ?>
            <input type="button" value="Aprobar" class="boton" onclick="LabAprobar('<?= $_GET['ID'] ?>')">&nbsp;
            <input type="button" value="Anular" class="boton" onclick="LabAnular('<?= $_GET['ID'] ?>')">&nbsp;
            <?php
        }
    }
    ?>
    <input type="button" value="Imprimir" class="boton2" onclick="window.print()">
</center>
<?= $Gestor->Footer() ?>
</body>
</html>