<?php
define('__MODULO__', 'cotizaciones');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _sol01l_crear();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<form name="form" id="form" method="post"
      action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
    <?php $Gestor->Incluir('p1', 'hr', 'Cotizaciones :: Solicitud de cotización de análisis de fertilizantes') ?>
    <?= $Gestor->Encabezado('P0001', 'e', 'Solicitud de cotización de análisis de fertilizantes') ?>
    <center>
        <table class="radius" style="font-size:12px" width="600px">
            <tr>
                <td class="titulo" colspan="2">1. Datos sobre el producto</td>
            </tr>
            <tr>
                <td>Cliente:</td>
                <td><input type="text" id="nomcliente" name="nomcliente" class="lista" readonly
                           onclick="ClienteLista()"/>
                    <input type="hidden" id="cliente" name="cliente"/>
                </td>
            </tr>
            <tr>
                <td>F&oacute;rmula del fertilizante:</td>
                <td><input type="text" id="formula" name="formula" size="30" maxlength="50"/></td>
            </tr>
            <tr>
                <td>Nombre comercial:</td>
                <td><input type="text" id="comercial" name="comercial" size="30" maxlength="50"/></td>
            </tr>
            <tr>
                <td>Observaciones:</td>
                <td><input type="text" id="obs" name="obs" size="30" maxlength="50"/></td>
            </tr>
            <tr>
                <td>Tipo de formulaci&oacute;n:</td>
                <td><select id="tipo_form" name="tipo_form">
                        <option value="">...</option>
                        <?php
                        $ROW2 = $Gestor->Formulaciones();
                        for ($x = 0; $x < count($ROW2); $x++) {
                            ?>
                            <option value="<?= $ROW2[$x]['id'] ?>"><?= $ROW2[$x]['nombre'] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>M&eacute;todo de an&aacute;lisis:</td>
                <td><select id="metodo" name="metodo" onchange="CambiaTipoMetodo(this.value)">
                        <option value="">...</option>
                        <option value="0">Laboratorio</option>
                        <option value="1">Registro</option>
                        <option value="2">Cliente</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Tipo de mezcla:</td>
                <td><select id="mezcla" name="mezcla">
                        <option value="">...</option>
                        <option value="0">Mezcla f&iacute;sica</option>
                        <option value="1">Fertilizante qu&iacute;mico</option>
                        <option value="2">Fertilizante l&iacute;quido</option>
                        <option value="3">Fertilizante hidrosoluble</option>
                    </select>
                </td>
            </tr>
            <tr id="tr_metodo" style="display:none">
                <td>Cliente aporta documentaci&oacute;n:</td>
                <td><select id="aporta" name="aporta">
                        <option value="">...</option>
                        <option value="0">No</option>
                        <option value="1">S&iacute;</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Suministro del est&aacute;ndar:</td>
                <td><select id="suministro" name="suministro">
                        <option value="">...</option>
                        <option value="0">Laboratorio</option>
                        <option value="1">Cliente</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Muestra de discrepancia:</td>
                <td><select id="discre" name="discre" onchange="CambiaTipoDiscre(this.value)">
                        <option value="0">No</option>
                        <option value="1">S&iacute;</option>
                    </select>
                </td>
            </tr>
            <tr id="tr_discre" style="display:none">
                <td># Solicitud: <input type="text" id="num_sol" name="num_sol" size="15" maxlength="20"/></td>
                <td>C&oacute;digo(s) externo: <input type="text" id="num_mue" name="num_mue" maxlength="30"/></td>
            </tr>
        </table>
        <br/>
        <table class="radius" style="font-size:12px" width="600px">
            <thead>
            <tr>
                <td class="titulo" colspan="6">2.A Elementos Solicitados&nbsp;<img onclick="AnalisisMas()"
                                                                                   src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                                                                                   title="Agregar línea" class="tab"/>
                </td>
            </tr>
            <tr>
                <td><strong>#</strong></td>
                <td><strong>An&aacute;lisis</strong></td>
                <td><strong>Concentraci&oacute;n declarada&nbsp;<img src="<?php $Gestor->Incluir('olvido', 'bkg') ?>"
                                                                     title="Si no conoce la concentración digite 0"
                                                                     border="0" width="15" height="15"
                                                                     style="cursor:pointer"/></strong></td>
                <td><strong>Tipo</strong></td>
                <td><strong>* Unidad</strong></td>
                <td><strong>Fuente</strong></td>
            </tr>
            </thead>
            <tbody id="lolo">
            <tr>
                <td>1.</td>
                <td><input type="text" id="analisis0" name="analisis[]" class="lista" readonly
                           onclick="MacroAnalisisLista(0)"/>
                    <input type="hidden" id="codigo0" name="codigo[]"/>
                </td>
                <td><input type="text" id="rango0" name="rango[]" size="20" maxlength="20"></td>
                <td><select id="quela0" name="quela[]">
                        <option value="">...</option>
                        <option value="0">Disponible</option>
                        <option value="1">Total</option>
                        <option value="2">Quelatado</option>
                        <option value="3">No-Quelatado</option>
                    </select></td>
                <td><select id="tipo0" name="tipo[]" onchange="CambiaTipoAnalisis()">
                        <option value="">...</option>
                        <option value="0">%m/m</option>
                        <option value="1">%m/v</option>
                    </select></td>
                <td><input type="text" name="fuente[]" size="20" maxlength="30"></td>
            </tr>
            </tbody>
        </table>
        <br/>
        <table class="radius" style="font-size:12px" width="600px">
            <thead>
            <tr>
                <td class="titulo" colspan="2">2.B Impurezas&nbsp;<img onclick="ImpurezasMas()"
                                                                       src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                                                                       title="Agregar línea" class="tab"/></td>
            </tr>
            <tr>
                <td><strong>#</strong></td>
                <td><strong>Nombre</strong></td>
            </tr>
            </thead>
            <tbody id="lolo2">
            <tr>
                <td>1.</td>
                <td><input type="text" id="impurezas0" name="impurezas[]" class="lista2" readonly
                           onclick="ImpurezasLista(0)"/>
                    <input type="hidden" id="codigoB0" name="codigoB[]"/>
                </td>
            </tr>
            </tbody>
        </table>
        <br/>
        <table class="radius" style="font-size:12px" width="600px">
            <tr>
                <td>
                    <strong>N&uacute;mero de muestras a realizar:</strong>&nbsp;
                    <input type="text" id="total" name="total" value="0" class="cantidad" onblur="_INT(this)">&nbsp;
                    <input type="checkbox" id="tipoE" name="tipoE" value="E"/>&nbsp;Densidad&nbsp;
                    <input type="checkbox" id="tipoF" name="tipoF" value="F"/>&nbsp;Granulometr&iacute;a
                </td>
            </tr>
            <tr>
                <td><font size="-1">* Si la concentraci&oacute;n est&aacute; declarada como %m/v, debe solicitar la
                        determinaci&oacute;n de la densidad</font></td>
            </tr>
        </table>
        <br/>
        <table class="radius" style="font-size:12px" width="600px">
            <tr>
                <td class="titulo" colspan="2">3. Nombre del solicitante</td>
            </tr>
            <tr>
                <td>Nombre:</td>
                <td><input type="text" id="nombre_sol" name="nombre_sol" size="50" maxlength="60"/></td>
            </tr>
        </table>
        <br/>
        <table class="radius" style="font-size:12px" width="600px">
            <tr>
                <td class="titulo" colspan="2">4. Nombre del encargado de entregar las muestras en el laboratorio</td>
            </tr>
            <tr>
                <td>Nombre:</td>
                <td><input type="text" id="encargado" name="encargado" size="50" maxlength="60"/></td>
            </tr>
        </table>
        <br/>
        <input type="button" value="Aceptar" class="boton" onclick="SolicitudGenerar(this)">
    </center>
</form>
<?= $Gestor->Encabezado('P0001', 'p', '') ?>
</body>
</html>