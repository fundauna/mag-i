<?php
define('__MODULO__', 'cotizaciones');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _cotizaciones();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('d10', 'hr', 'Cotizaciones :: Historial') ?>
<?= $Gestor->Encabezado('D0010', 'e', 'Historial') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center">
            <tr>
                <td class="titulo" colspan="2">Filtro</td>
            </tr>
            <tr>
                <td>No. de cotizaci&oacute;n:</td>
                <td><input type="text" id="cotizacion" name="cotizacion" value="<?= $_POST['cotizacion'] ?>" size="13"
                           maxlength="13"/></td>
            </tr>
            <tr>
                <td>Cliente:</td>
                <td><input type="text" id="tmp" name="tmp" class="lista" readonly onclick="ClientesLista()"
                           value="<?= $_POST['tmp'] ?>"/>
                    <input type="hidden" id="cliente" name="cliente" value="<?= $_POST['cliente'] ?>"/></td>
            </tr>
            <tr>
                <td>Fecha:</td>
                <td><input type="text" id="desde" name="desde" class="fecha" value="<?= $_POST['desde'] ?>" readonly
                           onClick="show_calendar(this.id);">&nbsp;
                    <input type="text" id="hasta" name="hasta" class="fecha" value="<?= $_POST['hasta'] ?>" readonly
                           onClick="show_calendar(this.id);"></td>
            </tr>
            <?php if ($_GET['LID'] == '3') { ?>
                <tr>
                    <td>Tipo:</td>
                    <td><select name="tipo" style="width:90px">
                            <option value="">Todas</option>
                            <option value="1" <?php if ($_POST['tipo'] == '1') echo 'selected'; ?>>Fertilizantes
                            </option>
                            <option value="2" <?php if ($_POST['tipo'] == '2') echo 'selected'; ?>>Plaguicidas</option>
                        </select></td>
                </tr>
            <?php } ?>
            <tr>
                <td>Estado:</td>
                <td><select name="estado">
                        <option value="0">Generada</option>
                        <option value="3" <?php if ($_POST['estado'] == '3') echo 'selected'; ?>>Pendiente</option>
                        <option value="1" <?php if ($_POST['estado'] == '1') echo 'selected'; ?>>Aprobada</option>
                        <option value="2" <?php if ($_POST['estado'] == '2') echo 'selected'; ?>>Finalizada</option>
                        <option value="5" <?php if ($_POST['estado'] == '5') echo 'selected'; ?>>Anulada</option>
                        <option value="" <?php if ($_POST['estado'] == '') echo 'selected'; ?>>Todos</option>
                    </select>&nbsp;&nbsp;<input type="button" value="Buscar" class="boton"
                                                onclick="CotizacionBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:970px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>A&ntilde;o</th>
                <th>N&uacute;mero</th>
                <th>Item</th>
                <th>No.</th>
                <th>Fecha</th>
                <th>Cliente</th>
                <th>Monto</th>
                <th>Estado</th>
                <th>Adjuntos</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->CotizacionesMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><?= $ROW[$x]['orden2'] ?></td>
                    <td><?= $ROW[$x]['orden1'] ?></td>
                    <td><?= $ROW[$x]['Row'] ?></td>
                    <td><a href="#"
                           onclick="CotizacionDetalle('<?= $_GET['LID'] ?>', '<?= $ROW[$x]['numero'] ?>')"><?= $ROW[$x]['numero'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['fecha1'] ?></td>
                    <td><?= $ROW[$x]['nombre'] ?></td>
                    <td><?= $Gestor->Moneda($ROW[$x]['moneda']) . $Gestor->Formato($ROW[$x]['monto']) ?></td>
                    <td><?= $Gestor->Estado($ROW[$x]['estado']) ?></td>
                    <td>
                        <?php $ROW[$x]['numero'] = trim($ROW[$x]['numero']);
                        if (file_exists("../../caspha-i/docs/cotizaciones/{$ROW[$x]['numero']}.zip")): ?>
                        <a href="../../caspha-i/docs/cotizaciones/<?= $ROW[$x]['numero'] ?>.zip" target="_blank"><img
                                    src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar" onclick=""
                                    class="tab2"/>
                            <?php endif; ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/><input type="button" value="Generar" class="boton" onClick="CotizacionCrear('<?= $_GET['LID'] ?>');">
</center>
<br><?= $Gestor->Encabezado('D0010', 'p', '') ?>
</body>
</html>