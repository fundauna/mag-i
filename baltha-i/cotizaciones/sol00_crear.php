<?php
define('__MODULO__', 'cotizaciones');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _sol00_crear();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<form name="form" id="form" method="post"
      action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
    <?php $Gestor->Incluir('p3', 'hr', 'Cotizaciones :: Solicitud de cotizaci�n de an�lisis de residuos de agroqu�micos') ?>
    <?= $Gestor->Encabezado('P0003', 'e', 'Solicitud de cotizaci�n de an�lisis de residuos de agroqu�micos') ?>
    <center>
        <table class="radius" style="font-size:12px" width="600px">
            <tr>
                <td class="titulo" colspan="2">1. Datos sobre el producto</td>
            </tr>
            <tr>
                <td>Nombre del producto:</td>
                <td><input type="text" id="comercial" name="comercial" size="30" maxlength="50"/></td>
            </tr>
            <tr>
                <td>Observaciones:</td>
                <td><input type="text" id="obs" name="obs" size="30" maxlength="50"/></td>
            </tr>
        </table>
        <br/>
        <table class="radius" style="font-size:12px" width="600px">
            <thead>
            <tr>
                <td class="titulo" colspan="3">2. An&aacute;lisis Solicitados&nbsp;<img onclick="AnalisisMas()"
                                                                                        src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                                                                                        title="Agregar l�nea"
                                                                                        class="tab"/></td>
            </tr>
            <tr>
                <td><strong>#</strong></td>
                <td><strong>An&aacute;lisis</strong></td>
                <td title="Cantidad de muestras"><strong>Cant.</strong></td>
            </tr>
            </thead>
            <tbody id="lolo">
            <tr>
                <td>1.</td>
                <td><input type="text" id="analisis0" name="analisis[]" class="lista" readonly
                           onclick="AnalisisLista(0)"/>
                    <input type="hidden" id="codigo0" name="codigo[]"/>
                </td>
                <td><input type="text" id="cant0" name="cant[]" maxlength="3" value="0" onblur="_INT(this);Totales();"
                           class="cantidad"></td>
            </tr>
            </tbody>
            <tr>
                <td colspan="3">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><strong>N&uacute;mero de muestras a realizar:</strong></td>
                <td><input type="text" id="total" name="total" value="0" class="cantidad" readonly></td>
            </tr>
        </table>
        <br/>
        <input type="button" value="Aceptar" class="boton" onclick="SolicitudGenerar(this)">
    </center>
</form>
<?= $Gestor->Encabezado('P0003', 'p', '') ?>
</body>
</html>