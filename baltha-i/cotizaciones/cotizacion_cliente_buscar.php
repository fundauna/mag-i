<?php
define('__MODULO__', 'cotizaciones');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _cotizacion_cliente_buscar();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('p6', 'hr', 'Cotizaciones :: Historial') ?>
<?= $Gestor->Encabezado('P0006', 'e', 'Historial') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" width="550px">
            <tr>
                <td class="titulo" colspan="2">Filtro</td>
            </tr>
            <tr>
                <td>Desde: <input type="text" id="desde" name="desde" class="fecha" value="<?= $_POST['desde'] ?>"
                                  readonly onClick="show_calendar(this.id);">&nbsp;
                    Hasta: <input type="text" id="hasta" name="hasta" class="fecha" value="<?= $_POST['hasta'] ?>"
                                  readonly onClick="show_calendar(this.id);"></td>
                <td><input type="button" value="Buscar" class="boton" onclick="CotizacionesBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:550px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>No.</th>
                <th>Fecha</th>
                <th>Tipo</th>
                <th>Estado</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->CotizacionesMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#"
                           onclick="CotizacionesDetalle('<?= $ROW[$x]['numero'] ?>', '<?= $ROW[$x]['tipo'] ?>')"><?= $ROW[$x]['numero'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['fecha1'] ?></td>
                    <td><?= $Gestor->Tipo($ROW[$x]['tipo']) ?></td>
                    <td><?= $Gestor->Estado($ROW[$x]['estado']) ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</center>
<br><?= $Gestor->Encabezado('P0006', 'p', '') ?>
</body>
</html>