<?php
define('__MODULO__', 'cotizaciones');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _certificaciones_detalle();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css', 1) ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js', 5) ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('d5', 'hr', 'Certificaciones :: Ingresar Certificación') ?>
<?= $Gestor->Encabezado('D0005', 'e', 'Ingresar Certificación') ?>
<center>
    <form name="form" id="form" method="post" enctype="multipart/form-data"
          action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
        <input type="hidden" name="token" id="token" value="1"/>
        <table class="radius" style="font-size:12px; width:450px">
            <tr>
                <td class="titulo">Adjuntar Documento</td>
            </tr>
            <tr>
                <td><input type="file" name="archivo" id="archivo"></td>
            </tr>
        </table>
        <br/>
        <input type="button" id="btn" value="Aceptar" class="boton" onclick="Valida(this)">
    </form>
</center>
<br><?= $Gestor->Encabezado('D0005', 'p', '') ?>
</body>
</html>