<?php
define('__MODULO__', 'cotizaciones');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _sol00_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<center>
    <?php $Gestor->Incluir('d15', 'hr', 'Cotizaciones :: Solicitud de An&aacute;lisis') ?>
    <?= $Gestor->Encabezado('D0015', 'e', 'Solicitud de An&aacute;lisis') ?>
    <br>
    <table class="radius" style="font-size:12px" width="600px">
        <tr>
            <td class="titulo" colspan="2">1. Datos de la persona, departamento o empresa solicitante</td>
        </tr>
        <tr>
            <td><strong>Nombre:</strong> <?= $ROW[0]['nom_cliente'] ?></td>
            <td><strong>Estado: </strong> <?= $Gestor->Estado($ROW[0]['estado']) ?></td>
        </tr>
        <tr>
            <td><strong>Tipo de cliente:</strong> <?= $ROW[0]['tipo'] == '1' ? 'Externo' : 'Interno' ?></td>
            <td><strong>Correo Electr&oacute;nico:</strong> <?= $ROW[0]['email'] ?></td>
        </tr>
        <tr>
            <td><strong>Tel&eacute;fono:</strong> <?= $ROW[0]['telefono'] ?></td>
            <td><strong>Fax:</strong> <?= $ROW[0]['fax'] ?></td>
        </tr>
        <tr>
            <td><strong>Fecha </strong> <?= $ROW[0]['fecha'] ?></td>
            <td><strong>Nombre del representante legal:</strong> <?= $ROW[0]['representante'] ?></td>
        </tr>
    </table>
    <br>
    <table class="radius" style="font-size:12px" width="600px">
        <tr>
            <td class="titulo" colspan="2">2. Datos sobre el producto</td>
        </tr>
        <tr>
            <td width="150"><strong>Nombre del producto:</strong></td>
            <td width="450"><?= $ROW[0]['producto'] ?></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="600px">
        <thead>
        <tr>
            <td class="titulo" colspan="3">3. An&aacute;lisis Solicitados</td>
        </tr>
        <tr>
            <td><strong>#</strong></td>
            <td><strong>An&aacute;lisis</strong></td>
            <td title="Cantidad de muestras"><strong>Cant.</strong></td>
        </tr>
        </thead>
        <?php
        $ROW2 = $Gestor->ObtieneDetalle();
        for ($x = 0, $total = 0; $x < count($ROW2); $x++) {
            $total += $ROW2[$x]['cantidad'];
            ?>
            <tr>
                <td><?= $x + 1 ?></td>
                <td><?= $ROW2[$x]['nombre'] ?></td>
                <td><?= $ROW2[$x]['cantidad'] ?></td>
            </tr>
            <?php
        }
        unset($ROW2);
        ?>
        <tr>
            <td colspan="3">
                <hr/>
            </td>
        </tr>
        <tr>
            <td colspan="2"><strong>N&uacute;mero de muestras a realizar:</strong></td>
            <td><?= $total ?></td>
        </tr>
    </table>
    <br/>
    <?php
    if ($ROW[0]['estado'] == 'a') $ROW2 = $Gestor->ObtieneAnulacion();
    else $ROW2[0]['obs'] = '';
    ?>
    <table id="justi" class="radius" style="font-size:12px; display:none">
        <tr>
            <td class="titulo" colspan="2">Justificaci&oacute;n de la anulaci&oacute;n</td>
        </tr>
        <tr align="center">
            <td><strong>Disponibilidad de</strong></td>
            <td><strong>Capacidad</strong></td>
        </tr>
        <tr>
            <td>Reactivos y Solventes:</td>
            <td><select id="Jreactivos">
                    <option value="">...</option>
                    <option value="0">No</option>
                    <option value="1">S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td>Material de Laboratorio:</td>
            <td><select id="Jmaterial">
                    <option value="">...</option>
                    <option value="0">No</option>
                    <option value="1">S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td>Personal de Laboratorio:</td>
            <td><select id="Jpersonal">
                    <option value="">...</option>
                    <option value="0">No</option>
                    <option value="1">S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td>Tiempo de trabajo:</td>
            <td><select id="Jtiempo">
                    <option value="">...</option>
                    <option value="0">No</option>
                    <option value="1">S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td>Equipos de rutina y trabajo:</td>
            <td><select id="Jequipos">
                    <option value="">...</option>
                    <option value="0">No</option>
                    <option value="1">S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td>Viabilidad del a&aacute;lisis:</td>
            <td><select id="Jviabilidad">
                    <option value="">...</option>
                    <option value="0">No</option>
                    <option value="1">S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td>Observaciones:</td>
            <td><textarea id="obs"><?= $ROW2[0]['obs'] ?></textarea></td>
        </tr>
    </table>
    <?php
    if ($ROW[0]['estado'] == 'a') {
        echo "<script>MuestraAnulacion('{$ROW2[0]['reactivos']}','{$ROW2[0]['material']}','{$ROW2[0]['personal']}','{$ROW2[0]['tiempo']}','{$ROW2[0]['equipos']}','{$ROW2[0]['viabilidad']}')</script>";
    }
    ?>
    <br/>
    <?= $Gestor->Historial($ROW[0]['estado']) ?>
    <br/>
    <?php
    if ($ROW[0]['estado'] == 'r') {
        //ANULAR POR CLIENTE
        if ($_GET['admin'] == '') { ?>
            <input type="button" value="Anular" class="boton" onclick="ClienteAnular('<?= $_GET['ID'] ?>')">&nbsp;
            <?php
            //ANULAR POR EL LABORATORIO
        } elseif ($_GET['admin'] == '1' && $Gestor->Aprobar()) { ?>
            <input type="button" value="Aprobar" class="boton" onclick="LabAprobar('<?= $_GET['ID'] ?>')">&nbsp;
            <input type="button" value="Anular" class="boton" onclick="LabAnular('<?= $_GET['ID'] ?>')">&nbsp;
            <?php
        }
    }
    ?>
    <input type="button" value="Imprimir" class="boton2" onclick="window.print()">
</center>
<?= $Gestor->Encabezado('D0015', 'p', '') ?>
<?= $Gestor->Footer() ?>
</body>
</html>