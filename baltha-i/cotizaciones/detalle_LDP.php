<?php
define('__MODULO__', 'cotizaciones');
require '../../caspha-i/shell/'.__MODULO__.'/_'.basename(__FILE__);

$Gestor = new _detalle_LDP();
$ROW = $Gestor->ObtieneDatos();
if(!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=$Gestor->Title()?></title>
<?php $Gestor->Incluir('estilo', 'css')?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<center>
    <?php $Gestor->Incluir('d12', 'hr', 'Cotizaciones :: Cotizaci&oacute;n de An&aacute;lisis') ?>
    <?= $Gestor->Encabezado('D0012', 'e', 'Cotizaci&oacute;n de An&aacute;lisis') ?>

<br>
<table class="radius" style="font-size:12px" width="600px">
<tr><td class="titulo" colspan="3">Informaci&oacute;n General</td></tr>
<tr>
	<td width="89">Fecha:</td>
	<td width="499"><?=$ROW[0]['fecha']?></td>
</tr>
<tr>
	<td>Estado:</td>
	<td><?=$Gestor->Estado($ROW[0]['estado'])?></td>
</tr>
<tr>
	<td>Cliente:</td>
	<td><?=$ROW[0]['tmp']?></td>
</tr>
<tr>
	<td>Solicitante:</td>
	<td><?=$ROW[0]['solicitante']?></td>
</tr>
<tr>
	<td>Cultivo:</td>
	<td><?=$ROW[0]['cultivo']?></td>
</tr>
</table>
<br />
<table class="radius" style="font-size:12px" width="600px">
<thead>
<tr><td class="titulo" colspan="5">Detalle</td></tr>
<tr>
	<td><strong>Item</strong></td>
	<td><strong>Descripci&oacute;n</strong></td>
	<td title="Cantidad de muestras"><strong>Cant.</strong></td>
	<td><strong>P. Unitario</strong></td>
	<td><strong>P. Total</strong></td>
</tr>
</thead>
<tbody id="lolo">
<?php
$ROW2 = $Gestor->ObtieneLineas();
for($x=0,$total=0;$x<count($ROW2);$x++){
	$total += $ROW2[$x]['cantidad'];
?>
<tr>
	<td><?=$ROW2[$x]['tarifa']?></td>
	<td><?=$ROW2[$x]['analisis']?></td>
	<td><?=$ROW2[$x]['cantidad']?></td>
	<td><?=$Gestor->Formato($ROW2[$x]['monto'])?></td>
	<td><?=$Gestor->Formato($ROW2[$x]['total'])?></td>
</tr>
<?php
}
?>
</tbody>
<tr><td colspan="5"><hr /></td></tr>
<tr>
	<td></td>
	<td align="right"><strong>N&uacute;mero de muestras a realizar:</strong></td>
	<td><?=$total?></td>
	<td align="right"></td>
	<td></td>
</tr>
<tr>
	<td colspan="4" align="right"><strong>Total:</strong></td>
	<td>&cent; <?=$Gestor->Formato($ROW[0]['monto'])?></td>
</tr>
<tr><td colspan="5" style="text-align:justify">
<br />
<font size="-1"><strong>
El costo de los an&aacute;lisis queda sujeto a cualquier variaci&oacute;n en las tarifas; las cuales se publican en el diario oficial La Gaceta.
<br /><br />
Forma de pago:
</strong></font>&nbsp;Por adelantado, con el siguiente sistema de pago:
<br /><br />
Banco de Costa Rica. N&ordm; de Cuenta: 001-0273982-8 (Colones)<br />
Banco Nacional de Costa Rica: N&ordm; de Cuenta: 100-02-000-620983-0 (D&oacute;lares)
<br /><br />
El depositante debe presentar el dep&oacute;sito o comprobante de la transferencia electr&oacute;nica en la caja del Departamento Administrativo y Financiero
del Servicio Fitosanitario del Estado.
<br /><br />
<strong>Importante:</strong> Las muestras se reciben, &uacute;nica y estrictamente. contra la entrega de la copia del recibo de pago emitido por
el Departamento Administrativo y Financiero del Servicio Fitosanitario del Estado, correspondiente al costo de los an&aacute;lisis aqu&iacute; cotizados.
</td></tr>
</table>
<br />
<input type="button" value="Imprimir" class="boton" onclick="window.print()">
</center>
<?= $Gestor->Encabezado('D0012', 'p', '') ?>
<?=$Gestor->Footer()?>
</body>
</html>