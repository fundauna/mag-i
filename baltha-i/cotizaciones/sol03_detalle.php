<?php
define('__MODULO__', 'cotizaciones');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _sol03_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<center>
    <?php $Gestor->Incluir('d21', 'hr', 'Cotizaciones :: Solicitud de Cotizaci&oacute;n de An&aacute;lisis') ?>
    <?= $Gestor->Encabezado('D0021', 'e', 'Solicitud de Cotizaci&oacute;n de An&aacute;lisis') ?>
    <br>
    <table class="radius" style="font-size:12px" width="600px">
        <tr>
            <td class="titulo" colspan="2">1. Datos de la persona, departamento o empresa solicitante</td>
        </tr>
        <tr>
            <td><strong>Nombre:</strong> <?= $ROW[0]['nom_cliente'] ?></td>
            <td><strong>Estado: </strong> <?= $Gestor->Estado($ROW[0]['estado']) ?></td>
        </tr>
        <tr>
            <td><strong>Tel&eacute;fono:</strong> <?= $ROW[0]['telefono'] ?></td>
            <td><strong>Fax:</strong> <?= $ROW[0]['fax'] ?></td>
        </tr>
        <tr>
            <td><strong>Fecha: </strong> <?= $ROW[0]['fecha'] ?></td>
            <td><strong>Nombre del representante legal:</strong> <?= $ROW[0]['representante'] ?></td>
        </tr>
    </table>
    <br>
    <table class="radius" style="font-size:12px" width="600px">
        <thead>
        <tr>
            <td class="titulo" colspan="5">1. Servicio(s) o producto(s) requerido(s)</td>
        </tr>
        <tr>
            <td><strong>#</strong></td>
            <td><strong>Tarifa</strong></td>
            <td><strong>An&aacute;lisis</strong></td>
            <td><strong># de muestras</strong></td>
            <td><strong>&Aacute;rea</strong></td>
        </tr>
        </thead>
        <?php
        $ROW2 = $Gestor->ObtieneAnalisis();
        for ($x = 0, $total = 0; $x < count($ROW2); $x++) {
            $total += $ROW2[$x]['cantidad'];
            ?>
            <tr>
                <td><?= $x + 1 ?></td>
                <td><?= $ROW2[$x]['tarifa'] ?></td>
                <td><?= $ROW2[$x]['nombre'] ?></td>
                <td><?= $ROW2[$x]['cantidad'] ?></td>
                <td><select disabled>
                        <option>Entomolog&iacute;a</option>
                        <option <?php if ($ROW2[$x]['dpto'] == '1') echo 'selected'; ?>>Fitopatolog&iacute;a</option>
                        <option <?php if ($ROW2[$x]['dpto'] == '2') echo 'selected'; ?>>Nematolog&iacute;a</option>
                        <option <?php if ($ROW2[$x]['dpto'] == '3') echo 'selected'; ?>>Biolog&iacute;a molecular
                        </option>
                    </select></td>
            </tr>
            <?php
        }
        unset($ROW2);
        ?>
        <tr>
            <td colspan="5">
                <hr/>
            </td>
        </tr>
        <tr>
            <td colspan="3"><strong>N&uacute;mero de muestras a realizar:</strong></td>
            <td colspan="2"><?= $total ?></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="600px">
        <tr>
            <td class="titulo" colspan="2">2. Informaci&oacute;n de la muestra</td>
        </tr>
        <?php
        $ROW2 = $Gestor->Apertura();
        if ($ROW2) {
            $estado_ap = $ROW2[0]['estado'];
            ?>
            <tr>
                <td>No. de apertua asociada:</td>
                <td><?= $ROW2[0]['id'] ?></td>
            </tr>
            <tr>
                <td>Solicitante:</td>
                <td><?= $ROW2[0]['funcionario'] ?></td>
            </tr>
            <tr>
                <td>Estado:</td>
                <td <?php if ($estado_ap != '1') echo 'style="color:#FF0000; cursor:pointer;" title="Es requerido aprobar la apertura"'; ?>><?= $Gestor->AperturaEstado($estado_ap) ?></td>
            </tr>
            <tr>
                <td>Estaci&oacute;n de inter&eacute;s:</td>
                <td><select disabled>
                        <option>Puerto Lim&oacute;n</option>
                        <option <?php if ($ROW2[0]['estacion'] == '1') echo 'selected'; ?>>Puerto Caldera</option>
                        <option <?php if ($ROW2[0]['estacion'] == '2') echo 'selected'; ?>>Pe&ntilde;as Blancas</option>
                        <option <?php if ($ROW2[0]['estacion'] == '3') echo 'selected'; ?>>Paso Canoas</option>
                        <option <?php if ($ROW2[0]['estacion'] == '4') echo 'selected'; ?>>Aeropuerto de Liberia
                        </option>
                        <option <?php if ($ROW2[0]['estacion'] == '5') echo 'selected'; ?>>Aeropuerto Juan Santamar&iacute;a</option>
                        <option <?php if ($ROW2[0]['estacion'] == '6') echo 'selected'; ?>>Los Chiles</option>
                        <option <?php if ($ROW2[0]['estacion'] == '7') echo 'selected'; ?>>Aeropuerto de Pavas</option>
                        <option <?php if ($ROW2[0]['estacion'] == '8') echo 'selected'; ?>>Sixaola</option>
                        <option <?php if ($ROW2[0]['estacion'] == '9') echo 'selected'; ?>>Otra:</option>
                    </select>&nbsp;&nbsp;<?= $ROW2[0]['otro'] ?></td>
            </tr>
            <tr>
                <td>Fecha deseada para apertura:</td>
                <td><?= $ROW2[0]['fecha'] ?></td>
            </tr>
            <tr>
                <td>Carta de solicitud de apertura:</td>
                <td><?php
                    $ROW2[0]['id'] = str_replace(' ', '', $ROW2[0]['id']);
                    $ruta = "../../caspha-i/docs/aperturas/{$ROW2[0]['id']}.zip";
                    if (file_exists($ruta)) {
                        ?>
                        &nbsp;<img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar"
                                   onclick="DocumentosZip('<?= $ROW2[0]['id'] ?>', 'aperturas')"
                                   class="tab2"/>&nbsp;&nbsp;&nbsp;&nbsp;
                    <?php } ?></td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td>Nombre del cultivo:</td>
            <td><?= $ROW[0]['cultivo'] ?></td>
        </tr>
        <tr>
            <td>Nombre del cient&iacute;fico cultivo:</td>
            <td><?= $ROW[0]['cientifico'] ?></td>
        </tr>
        <tr>
            <td>Sintomatolog&iacute;a observada:</td>
            <td><?= $ROW[0]['sintomas'] ?></td>
        </tr>
        <tr>
            <td>Plaga de inter&eacute;s a detectar:</td>
            <td><?= $ROW[0]['plaga'] ?></td>
        </tr>
        <tr>
            <td>Tipo de muestra:</td>
            <td><select disabled style="width:120px;">
                    <option value="B">Planta Completa</option>
                    <option value="C" <?php if ($ROW[0]['tipo'] == 'C') echo 'selected' ?>>Hoja</option>
                    <option value="D" <?php if ($ROW[0]['tipo'] == 'D') echo 'selected' ?>>Tallo o tronco</option>
                    <option value="E" <?php if ($ROW[0]['tipo'] == 'E') echo 'selected' ?>>Fruto</option>
                    <option value="F" <?php if ($ROW[0]['tipo'] == 'F') echo 'selected' ?>>Bulbo</option>
                    <option value="G" <?php if ($ROW[0]['tipo'] == 'G') echo 'selected' ?>>Cultivo celular</option>
                    <option value="H" <?php if ($ROW[0]['tipo'] == 'H') echo 'selected' ?>>Medio enraizamiento</option>
                    <option value="I" <?php if ($ROW[0]['tipo'] == 'I') echo 'selected' ?>>Ra&iacute;z</option>
                    <option value="J" <?php if ($ROW[0]['tipo'] == 'J') echo 'selected' ?>>Flor</option>
                    <option value="K" <?php if ($ROW[0]['tipo'] == 'K') echo 'selected' ?>>Tub&eacute;rculo</option>
                    <option value="L" <?php if ($ROW[0]['tipo'] == 'L') echo 'selected' ?>>Esqueje</option>
                    <option value="M" <?php if ($ROW[0]['tipo'] == 'M') echo 'selected' ?>>Semilla</option>
                    <option value="N" <?php if ($ROW[0]['tipo'] == 'N') echo 'selected' ?>>Rizoma</option>
                    <option value="O" <?php if ($ROW[0]['tipo'] == 'O') echo 'selected' ?>>Artr&oacute;podos y Gastr&oacute;podo
                        de importancia agr&iacute;cola
                    </option>
                    <option value="P" <?php if ($ROW[0]['tipo'] == 'P') echo 'selected' ?>>Rama</option>
                    <option value="Q" <?php if ($ROW[0]['tipo'] == 'Q') echo 'selected' ?>>Otro</option>
                </select>
                &nbsp;
                <?= $ROW[0]['otro'] ?></td>
        </tr>
        <tr>
            <td>Observaciones:</td>
            <td id="td_obs"><?= $ROW[0]['obs'] ?></td>
        </tr>
        <tr>
            <td colspan="2">Partes afectadas:</td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="100%">
                    <tr>
                        <td>
                            <input type="checkbox" <?php if ($Gestor->Existe('A', $ROW[0]['afectada'])) echo 'checked'; ?>
                                   disabled/>&nbsp;Planta Completa
                        </td>
                        <td>
                            <input type="checkbox" <?php if ($Gestor->Existe('B', $ROW[0]['afectada'])) echo 'checked'; ?>
                                   disabled/>&nbsp;Fruto
                        </td>
                        <td>
                            <input type="checkbox" <?php if ($Gestor->Existe('C', $ROW[0]['afectada'])) echo 'checked'; ?>
                                   disabled/>&nbsp;Bulbo
                        </td>
                        <td>
                            <input type="checkbox" <?php if ($Gestor->Existe('D', $ROW[0]['afectada'])) echo 'checked'; ?>
                                   disabled/>&nbsp;Hoja
                        </td>
                        <td>
                            <input type="checkbox" <?php if ($Gestor->Existe('E', $ROW[0]['afectada'])) echo 'checked'; ?>
                                   disabled/>&nbsp;Tallo o tronco
                        </td>
                        <td>
                            <input type="checkbox" <?php if ($Gestor->Existe('F', $ROW[0]['afectada'])) echo 'checked'; ?>
                                   disabled/>&nbsp;Ra&iacute;z
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" <?php if ($Gestor->Existe('G', $ROW[0]['afectada'])) echo 'checked'; ?>
                                   disabled/>&nbsp;Flor
                        </td>
                        <td>
                            <input type="checkbox" <?php if ($Gestor->Existe('H', $ROW[0]['afectada'])) echo 'checked'; ?>
                                   disabled/>&nbsp;Tub&eacute;rculo
                        </td>
                        <td>
                            <input type="checkbox" <?php if ($Gestor->Existe('I', $ROW[0]['afectada'])) echo 'checked'; ?>
                                   disabled/>&nbsp;Esqueje
                        </td>
                        <td>
                            <input type="checkbox" <?php if ($Gestor->Existe('J', $ROW[0]['afectada'])) echo 'checked'; ?>
                                   disabled/>&nbsp;Semilla
                        </td>
                        <td>
                            <input type="checkbox" <?php if ($Gestor->Existe('K', $ROW[0]['afectada'])) echo 'checked'; ?>
                                   disabled/>&nbsp;Rizoma
                        </td>
                        <td>
                            <input type="checkbox" <?php if ($Gestor->Existe('L', $ROW[0]['afectada'])) echo 'checked'; ?>
                                   disabled/>&nbsp;Rama
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="600px">
        <tr>
            <td class="titulo" colspan="2">3. Informaci&oacute;n del cultivo</td>
        </tr>
        <tr>
            <td colspan="2"><strong>Ubicaci&oacute;n del cultivo</strong></td>
        </tr>
        <tr>
            <td width="253">Provincia:</td>
            <td width="335"><select disabled>
                    <option>San Jos&eacute;</option>
                    <option <?php if ($ROW[0]['provincia'] == '2') echo 'selected' ?>>Alajuela</option>
                    <option <?php if ($ROW[0]['provincia'] == '3') echo 'selected' ?>>Cartago</option>
                    <option <?php if ($ROW[0]['provincia'] == '4') echo 'selected' ?>>Heredia</option>
                    <option <?php if ($ROW[0]['provincia'] == '5') echo 'selected' ?>>Guanacaste</option>
                    <option <?php if ($ROW[0]['provincia'] == '6') echo 'selected' ?>>Puntarenas</option>
                    <option <?php if ($ROW[0]['provincia'] == '7') echo 'selected' ?>>Lim&oacute;n</option>
                </select></td>
        </tr>
        <tr>
            <td>Cant&oacute;n:</td>
            <td><?= $ROW[0]['canton'] ?></td>
        </tr>
        <tr>
            <td>Direcci&oacute;n:</td>
            <td><?= $ROW[0]['direccion'] ?></td>
        </tr>
        <tr>
            <td colspan="2">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Gravedad de da&ntilde;o:</td>
            <td><select disabled>
                    <option value="">...</option>
                    <option <?php if ($ROW[0]['gravedad'] == '1') echo 'selected' ?>>Leve</option>
                    <option <?php if ($ROW[0]['gravedad'] == '2') echo 'selected' ?>>Moderado</option>
                    <option <?php if ($ROW[0]['gravedad'] == '3') echo 'selected' ?>>Severo</option>
                    <option <?php if ($ROW[0]['gravedad'] == '4') echo 'selected' ?>>Perdida Total</option>
                </select></td>
        </tr>
        <tr>
            <td>Etapa de desarrollo o edad del cultivo:</td>
            <td><?= $ROW[0]['etapa'] ?></td>
        </tr>
        <tr>
            <td>&Aacute;rea del cultivo y &aacute;rea afectada:</td>
            <td><?= $ROW[0]['area_cultivo'] ?>&nbsp;&nbsp;<?= $ROW[0]['area_afectada'] ?></td>
        </tr>
        <tr>
            <td>Ha aplicado plaguicidas:</td>
            <td><select disabled>
                    <option>No</option>
                    <option <?php if ($ROW[0]['aplicado'] == '1') echo 'selected' ?>>S&iacute;</option>
                </select></td>
        </tr>
        <?php
        if ($ROW[0]['aplicado'] == '1') {
            ?>
            <tr>
                <td colspan="2">
                    <!-- -->
                    <table width="100%" style="font-size:12px">
                        <thead>
                        <tr>
                            <td><strong>Nombre del plaguicida</strong></td>
                            <td><strong>Tipo</strong></td>
                            <td><strong>Frecuencia</strong></td>
                            <td><strong>Dosis</strong></td>
                            <td><strong>&Uacute;ltima aplicaci&oacute;n</strong></td>
                        </tr>
                        </thead>
                        <?php
                        $ROW2 = $Gestor->ObtienePlaguicidas();
                        for ($x = 0, $total = 0; $x < count($ROW2); $x++) {
                            ?>
                            <tr>
                                <td><?= $ROW2[$x]['plaguicida'] ?></td>
                                <td><?= $ROW2[$x]['tipo'] ?></td>
                                <td><?= $ROW2[$x]['frecuencia'] ?></td>
                                <td><?= $ROW2[$x]['dosis'] ?></td>
                                <td><?= $ROW2[$x]['fecha'] ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                    <!-- -->
                </td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td>Tipo de riego:</td>
            <td><?= $ROW[0]['riego'] ?></td>
        </tr>
        <tr>
            <td>Tipo de drenaje:</td>
            <td><?= $ROW[0]['drenaje'] ?></td>
        </tr>
        <tr>
            <td>Tipo de suelo:</td>
            <td><?= $ROW[0]['suelo'] ?></td>
        </tr>
        <tr>
            <td>Programa de fertilizaci&oacute;n:</td>
            <td><?= $ROW[0]['programa'] ?></td>
        </tr>
        <tr>
            <td>Densidad de la plantaci&oacute;n o distancia de siembra:</td>
            <td><?= $ROW[0]['densidad'] ?></td>
        </tr>
    </table>
    <?= $Gestor->Historial($ROW[0]['estado']) ?>
    <br/>
    <?php
    if ($ROW[0]['estado'] == 'r') {
        //ANULAR POR CLIENTE
        if ($_GET['admin'] == '') { ?>
            <input type="button" value="Anular" class="boton" onclick="ClienteAnular('<?= $_GET['ID'] ?>')">&nbsp;
            <?php
            //ANULAR POR EL LABORATORIO
        } elseif ($_GET['admin'] == '1' && $Gestor->Aprobar()) { ?>
            <input type="button" value="Aprobar" class="boton" onclick="LabAprobar('<?= $_GET['ID'] ?>')">&nbsp;
            <input type="button" value="Anular" class="boton" onclick="LabAnular('<?= $_GET['ID'] ?>')">&nbsp;
            <?php
        }
    }
    ?>
    <input type="button" value="Imprimir" class="boton2" onclick="window.print()">
</center>
<?= $Gestor->Encabezado('D0021', 'p', '') ?>
<?= $Gestor->Footer() ?>
</body>
</html>