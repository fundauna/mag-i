<?php
define('__MODULO__', 'cotizaciones');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _solicitud_cliente_crear();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('P0', 'hr', 'Solicitudes :: Generar solicitud de cotización') ?>
<?= $Gestor->Encabezado('P0000', 'e', 'Generar solicitud de cotización') ?>
<center>

    <table class="radius" align="center" width="350px">
        <tr>
            <td class="titulo">Laboratorio de Control de Calidad de Agroqu&iacute;micos</td>
        </tr>
        <tr>
            <td>&bull;&nbsp;<a href="sol01_crear.php">Crear solicitud de cotizaci&oacute;n an&aacute;lisis de
                    fertilizantes</a></td>
        </tr>
        <tr>
            <td>&bull;&nbsp;<a href="sol02_crear.php">Crear solicitud de cotizaci&oacute;n an&aacute;lisis de
                    plaguicidas</a></td>
        </tr>
        <tr>
            <td class="titulo">Laboratorio de An&aacute;lisis de Residuos de Agroqu&iacute;micos</td>
        </tr>
        <tr>
            <td>&bull;&nbsp;<a href="sol00_crear.php">Crear solicitud de cotizaci&oacute;n an&aacute;lisis</a></td>
        </tr>
        <tr>
            <td class="titulo">Laboratorio Central de Diagn&oacute;stico de Plagas</td>
        </tr>
        <tr>
            <td>&bull;&nbsp;<a href="sol03_crear.php">Crear solicitud de cotizaci&oacute;n an&aacute;lisis</a></td>
        </tr>
    </table>
</center>
<?= $Gestor->Encabezado('P0000', 'p', '') ?>
</body>
</html>