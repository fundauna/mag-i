<?php
define('__MODULO__', 'cotizaciones');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _apertura_crear();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('d0', 'hr', 'Cotizaciones :: Apertura de Solicitud de cotización') ?>
<?= $Gestor->Encabezado('D0000', 'e', 'Apertura de Solicitud de cotización') ?>
<form name="form" id="form" method="post" enctype="multipart/form-data"
      action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
    <center>
        <table class="radius" style="font-size:12px" width="600px">
            <tr>
                <td class="titulo" colspan="2">1. Informaci&oacute;n general</td>
            </tr>
            <tr>
                <td>Tipo de cliente que requiere la apertura:</td>
                <td><select id="cliente" name="cliente">
                        <option value="0">Interno</option>
                        <option value="1">Externo</option>
                    </select></td>
            </tr>
            <tr>
                <td>Estaci&oacute;n de inter&eacute;s:</td>
                <td><select id="estacion" name="estacion" onchange="CambiaEstacion(this.value)">
                        <option value="">...</option>
                        <option value="0">Puerto Lim&oacute;n</option>
                        <option value="1">Puerto Caldera</option>
                        <option value="2">Pe&ntilde;as Blancas</option>
                        <option value="3">Paso Canoas</option>
                        <option value="4">Aeropuerto de Liberia</option>
                        <option value="5">Aeropuerto Juan Santamar&iacute;a</option>
                        <option value="6">Los Chiles</option>
                        <option value="7">Aeropuerto de Pavas</option>
                        <option value="8">Sixaola</option>
                        <option value="9">Otra:</option>
                    </select>&nbsp;
                    <input type="text" id="otro" name="otro" size="20" maxlength="30" style="display:none"/>
                </td>
            </tr>
            <tr>
                <td>Fecha deseada para apertura:</td>
                <td><input type="text" id="fecha" name="fecha" class="fecha" readonly
                           onClick="show_calendar(this.id);"/></td>
            </tr>
            <tr>
                <td>Nombre del cultivo:</td>
                <td><input type="text" id="cultivo" name="cultivo" size="30" maxlength="50" value="cultivo1"/></td>
            </tr>
            <tr>
                <td>Observaciones:</td>
                <td><input type="text" id="obs" name="obs" size="30" maxlength="100" value="obs1"/></td>
            </tr>
            <tr>
                <td>Carta de solicitud de apertura:</td>
                <td><input type="file" name="archivo" id="archivo"></td>
            </tr>
        </table>
        <br/>
        <table class="radius" style="font-size:12px" width="600px">
            <thead>
            <tr>
                <td class="titulo" colspan="5">2. Servicio(s) o producto(s) requerido(s)&nbsp;<img
                            onclick="AnalisisMas()" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar línea"
                            class="tab"/></td>
            </tr>
            <tr>
                <td><strong>#</strong></td>
                <td><strong>Cod.</strong></td>
                <td><strong>An&aacute;lisis</strong></td>
                <td><strong># de muestras</strong></td>
                <td><strong>Departamento</strong></td>
            </tr>
            </thead>
            <tbody id="lolo">
            <tr>
                <td>1.</td>
                <td><input type="text" size="3" id="cod0" readonly/><input type="hidden" id="tarifa0"/></td>
                <td><input type="text" id="analisis0" name="analisis[]" class="lista" readonly
                           onclick="AnalisisLista(0)"/>
                    <input type="hidden" id="codigo0" name="codigo[]"/>
                </td>
                <td><input type="text" id="cant0" name="cant[]" maxlength="3" value="0" onblur="_INT(this);Totales();"
                           class="cantidad"></td>
                <td><select id="dpto0" name="dpto[]">
                        <option value="">...</option>
                        <option value="0">Entomolog&iacute;a</option>
                        <option value="1">Fitopatolog&iacute;a</option>
                        <option value="2">Nematolog&iacute;a</option>
                        <option value="3">Biolog&iacute;a molecular</option>
                    </select></td>
            </tr>
            </tbody>
            <tr>
                <td colspan="5">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td colspan="2"></td>
                <td><strong>N&uacute;mero de muestras a realizar:</strong></td>
                <td colspan="2"><input type="text" id="total" name="total" value="0" class="cantidad" readonly></td>
            </tr>
        </table>
        <br/>
        <input type="button" value="Aceptar" class="boton" onclick="SolicitudGenerar(this)">
    </center>
</form>
<br><?= $Gestor->Encabezado('D0000', 'p', '') ?>
</body>
</html>