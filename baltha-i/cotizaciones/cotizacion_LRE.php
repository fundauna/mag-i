<?php
define('__MODULO__', 'cotizaciones');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _cotizacion_LRE();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <script>
        img1 = '<?php $Gestor->Incluir('closed', 'bkg')?>';
        img2 = '<?php $Gestor->Incluir('downboxed', 'bkg')?>';
    </script>
</head>
<body>
<form name="form" id="form" method="post" enctype="multipart/form-data"
      action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
    <input type="hidden" name="ID" id="ID" value="<?= $ROW[0]['numero'] ?>"/>
    <input type="hidden" name="accion" value="<?= $_GET['acc'] ?>"/>
    <center>
        <?php $Gestor->Incluir('d9', 'hr', 'Cotizaciones :: Cotizaci&oacute;n de An&aacute;lisis') ?>
        <?= $Gestor->Encabezado('D0009', 'e', 'Cotizaci&oacute;n de An&aacute;lisis') ?>
        <br>
        <table class="radius" style="font-size:12px" width="600px">
            <tr>
                <td class="titulo" colspan="2">Informaci&oacute;n General</td>
            </tr>
            <tr>
                <td>Solicitud de origen:</td>
                <td><input type="text" id="solicitud" name="solicitud" value="<?= $ROW[0]['solicitud'] ?>" size="12"
                           maxlength="12" onblur="SolicitudCarga(this.value)"></td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td>Fecha:</td>
                <td><?= $ROW[0]['fecha'] ?></td>
            </tr>
            <tr>
                <td>Estado:</td>
                <td><?= $Gestor->Estado($ROW[0]['estado']) ?></td>
            </tr>
            <tr>
                <td>Cliente:</td>
                <td><input type="text" id="tmp" name="tmp" class="lista" readonly onclick="ClientesLista()"
                           value="<?= $ROW[0]['tmp'] ?>"/>
                    <input type="hidden" id="cliente" name="cliente" value="<?= $ROW[0]['cliente'] ?>"/></td>
            </tr>
            <tr>
                <td>Producto:</td>
                <td><input type="text" id="nombre" name="nombre" size="20" maxlength="20"
                           value="<?= $ROW[0]['comercial'] ?>"/></td>
            </tr>
        </table>
        <?php if ($ROW[0]['estado'] == '1' || $ROW[0]['estado'] == '2' || $ROW[0]['estado'] == '3' || $ROW[0]['estado'] == '5') {
            $ROW2 = $Gestor->ObtieneAnulacion(); ?>
            <br/>
            <table class="radius" style="font-size:12px;" width="600px">
                <tr>
                    <td class="titulo" colspan="4">Evaluaci&oacute;n de Revisi&oacute;n</td>
                </tr>
                <tr align="center">
                    <td><strong>Disponibilidad de</strong></td>
                    <td><strong>Capacidad</strong></td>
                </tr>
                <tr>
                    <td align="center">Reactivos y Solventes:</td>
                    <td align="center">
                        <select id="Jreactivos">
                            <option value="">...</option>
                            <option <?= $ROW2[0]['reactivos'] == '0' ? 'selected' : '' ?> value="0">No</option>
                            <option <?= $ROW2[0]['reactivos'] == '1' ? 'selected' : '' ?> value="1">S&iacute;</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align="center">Material de Laboratorio:</td>
                    <td align="center">
                        <select id="Jmaterial">
                            <option value="">...</option>
                            <option <?= $ROW2[0]['material'] == '0' ? 'selected' : '' ?> value="0">No</option>
                            <option <?= $ROW2[0]['material'] == '1' ? 'selected' : '' ?> value="1">S&iacute;</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align="center">Personal de Laboratorio:</td>
                    <td align="center">
                        <select id="Jpersonal">
                            <option value="">...</option>
                            <option <?= $ROW2[0]['personal'] == '0' ? 'selected' : '' ?> value="0">No</option>
                            <option <?= $ROW2[0]['personal'] == '1' ? 'selected' : '' ?> value="1">S&iacute;</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align="center">Tiempo de trabajo:</td>
                    <td align="center">
                        <select id="Jtiempo">
                            <option value="">...</option>
                            <option <?= $ROW2[0]['tiempo'] == '0' ? 'selected' : '' ?> value="0">No</option>
                            <option <?= $ROW2[0]['tiempo'] == '1' ? 'selected' : '' ?> value="1">S&iacute;</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align="center">Equipos de rutina y trabajo:</td>
                    <td align="center">
                        <select id="Jequipos">
                            <option value="">...</option>
                            <option <?= $ROW2[0]['equipos'] == '0' ? 'selected' : '' ?> value="0">No</option>
                            <option <?= $ROW2[0]['equipos'] == '1' ? 'selected' : '' ?> value="1">S&iacute;</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align="center">Viabilidad del an&aacute;lisis:</td>
                    <td align="center">
                        <select id="Jviabilidad">
                            <option value="">...</option>
                            <option <?= $ROW2[0]['viabilidad'] == '0' ? 'selected' : '' ?> value="0">No</option>
                            <option <?= $ROW2[0]['viabilidad'] == '1' ? 'selected' : '' ?> value="1">S&iacute;</option>
                        </select>
                    </td>
                </tr>
            </table>
        <?php } ?>
        <br/>
        <table class="radius" style="font-size:12px" width="600px">
            <thead>
            <tr>
                <td class="titulo" colspan="<?= $_GET['acc'] == 'I' ? '8' : '7' ?>">
                    Detalle<?php if ($ROW[0]['estado'] == '' or $ROW[0]['estado'] == '0') { ?>&nbsp;<img
                        onclick="AnalisisMas()" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                        class="tab" /><?php } ?></td>
            </tr>
            <tr>
                <td align="center"><strong>Item</strong></td>
                <td align="center"><strong>Cantidad</strong></td>
                <td align="center"><strong>Descripci&oacute;n</strong></td>
                <td align="center"><strong>P. Unitario</strong></td>
                <td align="center"><strong>Subtotal</strong></td>
                <td align="center"><strong>Descuento</strong></td>
                <td align="center"><strong>P. Total</strong></td>
                <?php if ($_GET['acc'] == 'I'): ?>
                    <td align="center"><strong>Opciones</strong></td><?php endif; ?>
            </tr>
            </thead>
            <tbody id="lolo">
            <?php
            $ROW2 = $Gestor->ObtieneLineas();
            for ($x = 0, $total = 0; $x < count($ROW2); $x++) {
                $total += $ROW2[$x]['cantidad'];
                ?>
                <tr>
                    <td><input type="text" id="item<?= $x ?>" name="item[]" size="3" readonly
                               value="<?= $ROW2[$x]['tarifa'] ?>"/><input type="hidden" id="descuento<?= $x ?>"
                                                                          value="<?= $ROW2[$x]['descuento'] ?>"/></td>
                    <td><input type="text" id="cant<?= $x ?>" name="cant[]" maxlength="3"
                               value="<?= $ROW2[$x]['cantidad'] ?>" onblur="_INT(this);Totales();" class="cantidad">
                    </td>
                    <td><input type="text" id="analisis<?= $x ?>" name="analisis[]" class="lista2" readonly
                               onclick="AnalisisLista(this.id.split('analisis')[1])"
                               value="<?= $ROW2[$x]['analisis'] ?>"/>
                        <input type="hidden" id="codigo<?= $x ?>" name="codigo[]" value="<?= $ROW2[$x]['codigo'] ?>"/>
                    </td>
                    <td><input type="text" id="punit<?= $x ?>" name="punit[]"
                               value="<?= $Gestor->Formato($ROW2[$x]['monto']) ?>" class="monto" readonly></td>
                    <td><input type="text" id="sub<?= $x ?>" name="sub[]" class="monto" readonly></td>
                    <td><input type="text" id="desc<?= $x ?>" name="desc[]" class="monto" readonly></td>
                    <td><input type="text" id="ptotal<?= $x ?>" name="ptotal[]"
                               value="<?= $Gestor->Formato($ROW2[$x]['total']) ?>" class="monto" readonly></td>
                    <?php if ($_GET['acc'] == 'I'): ?>
                        <td>
                            <img onclick="EliminaLinea($( this ))" src="../../caspha-i/imagenes/del.png"
                                 title="Eliminar" class="tab2"/>
                        </td>
                    <?php endif; ?>
                </tr>
                <?php
            }
            ?>
            </tbody>
            <tr>
                <td colspan="7">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="5"><strong>N&uacute;mero de muestras a realizar:&nbsp;</strong><input
                            type="text" id="total" name="total" value="<?= $ROW[0]['muestras'] ?>" class="cantidad"
                            onblur="_INT(this);Totales();">
                    <input type="hidden" id="moneda" name="moneda" value="0"/>
                </td>
            </tr>
            <tr>
                <td colspan="6" align="right"><strong>Descuento total: &nbsp;&cent;</strong></td>
                <td><input type="text" id="descT" name="descT" class="monto" readonly></td>
            </tr>
            <tr>
                <td colspan="6" align="right"><strong>Monto a cancelar: &nbsp;&cent;</strong></td>
                <td><input type="text" id="monto" name="monto" value="<?= $Gestor->Formato($ROW[0]['monto']) ?>"
                           class="monto" readonly></td>
            </tr>
            <?php if ($ROW[0]['estado'] == '1') { ?>
                <tr>
                    <td colspan="7">
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td colspan="5"><strong># de recibo:</strong>&nbsp;<input type="text" name="deposito" id="deposito"
                                                                              size="20" maxlength="50"/>&nbsp;
                        <input type="file" name="archivo" id="archivo" style="width:200px;"/>
                    </td>
                </tr>
            <?php } elseif ($ROW[0]['estado'] == '0' && $_GET['acc'] == 'M') { ?>
                <tr>
                    <td id="td_obs" colspan="5"></td>
                </tr>
            <?php } elseif ($ROW[0]['estado'] == '2' or $ROW[0]['estado'] == '5') { ?>
                <tr>
                    <td colspan="7">
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td id="td_obs" colspan="7"><strong>Observaciones:</strong>&nbsp;<?= $ROW[0]['obs'] ?>
                        <?php
                        $ruta = "../../caspha-i/docs/cotizaciones/" . $_GET['ID'] . '.zip';
                        if (file_exists($ruta)) {
                            ?>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            Descargar adjunto:&nbsp;<img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>"
                                                         title="Descargar"
                                                         onclick="DocumentosZip('<?= $_GET['ID'] ?>', '<?= __MODULO__ ?>')"
                                                         class="tab2"/>
                        <?php } ?>
                    </td>
                </tr>
                <?php
            }//elseif
            ?>
            <tr>
                <td colspan="7"><br/><img id="i_historial" src="<?= $Gestor->Incluir('closed', 'bkg') ?>"
                                          style="vertical-align:baseline;cursor:pointer"/>&nbsp;<strong>Notas</strong>
                    <hr/>
                </td>
            </tr>
            <tr>
                <td colspan="7" align="center">
                    <strong>Descuentos aplicados de acuerdo con la cantidad de muestras</strong><br/><br/>
                    <table border="1px">
                        <tr>
                            <td><strong>Cantidad de muestras</strong></td>
                            <td><strong>Descuento (%)</strong></td>
                        </tr>
                        <tr>
                            <td align="center">6 a 10</td>
                            <td align="center">5</td>
                        </tr>
                        <tr>
                            <td align="center">11 en adelante</td>
                            <td align="center">10</td>
                        </tr>
                    </table>
                    <br/>
                    <strong>Forma de pago</strong><br/>Por adelantado, mediante dep�sito bancario a nombre del
                    Ministerio de Agricultura, en alguna de las siguientes cuentas:<br/><br/>
                    <table>
                        <tr>
                            <td><strong>Banco de Costa Rica:</strong></td>
                            <td><strong>01-0273982-8</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Banco Nacional de Costa Rica:</strong></td>
                            <td><strong>100-01-000-219147-7</strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <ul>
                        <li>Despues de realizar el dep�sito bancario debe obtener el recibo de pago en la Unidad
                            Administrativa del Servicio Fitosanitario del Estado.
                        </li>
                        <li>El costo de los an�lisis queda sujeto a cualquier variaci�n en las tarifas, las cuales se
                            publican anualmente mediante decreto en el diario oficial la Gaceta. Las muestras se reciben
                            �nicamente contra la presentaci�n del recibo de pago, correspondiente al costo de los
                            an�lisis.
                        </li>
                    </ul>
                </td>
            </tr>
            <?php
            if ($_GET['acc'] == 'M') {
                ?>
                <tr>
                    <td colspan="7"><br/><img id="i_historial" src="<?= $Gestor->Incluir('closed', 'bkg') ?>"
                                              onclick="Oculta()" title="Mostrar/Ocultar"
                                              style="vertical-align:baseline;cursor:pointer"/>&nbsp;<strong>Historial</strong>
                        <hr/>
                    </td>
                </tr>
                <tfoot id="t_historial" style="display:none">
                <tr>
                    <td colspan="3"><strong>Fecha</strong></td>
                    <td colspan="2"><strong>Usuario</strong></td>
                    <td colspan="2"><strong>Acci&oacute;n</strong></td>
                </tr>
                <?php
                $ROW2 = $Gestor->Historial();
                for ($x = 0; $x < count($ROW2); $x++) {
                    ?>
                    <tr>
                        <td colspan="3"><?= $ROW2[$x]['fecha1'] ?></td>
                        <td colspan="2"><?= $ROW2[$x]['nombre'] . ' ' . $ROW2[$x]['ap1'] . ' ' . $ROW2[$x]['ap2'] ?></td>
                        <td colspan="2"><?= $Gestor->Accion($ROW2[$x]['accion']) ?></td>
                    </tr>
                    <?php
                }//FOR
                ?>
                </tfoot>
                <?php
            }
            ?>
        </table>
        <br/>
        <?php
        if ($ROW[0]['estado'] == '5' && ($_GET['acc'] != 'R' && $_GET['acc'] != 'I')) $ROW2 = $Gestor->ObtieneAnulacion();
        else $ROW2[0]['obs'] = '';
        ?>
        <table id="justi" class="radius" style="font-size:12px; display:none">
            <tr>
                <td class="titulo" colspan="2">Justificaci&oacute;n de la anulaci&oacute;n</td>
            </tr>
            <tr>
                <td>Observaciones:</td>
                <td><textarea id="obs"><?= $ROW2[0]['obs'] ?></textarea></td>
            </tr>
        </table>
        <?php
        if ($ROW[0]['estado'] == '5') {
            echo "<script>MuestraAnulacion('{$ROW2[0]['reactivos']}','{$ROW2[0]['material']}','{$ROW2[0]['personal']}','{$ROW2[0]['tiempo']}','{$ROW2[0]['equipos']}','{$ROW2[0]['viabilidad']}')</script>";
        }
        ?>
        <br/>
        <?php if ($ROW[0]['estado'] == '' || $ROW[0]['estado'] == '0' && $_GET['acc'] != 'M') { ?>
            <input type="button" value="Aceptar" class="boton" onclick="SolicitudGenerar(this)">&nbsp;&nbsp;
        <?php } ?>
        <?php if ($ROW[0]['estado'] == '0' && $_GET['acc'] == 'M') { ?>
            <input type="button" value="Modificar" class="boton" onclick="SolicitudGenerar(this)">&nbsp;&nbsp;
            <input type="button" value="Procesar" class="boton" onclick="SolicitudModificar(this, '', '3')">&nbsp;&nbsp;
        <?php } ?>
        <?php if ($ROW[0]['estado'] == '3' && $Gestor->Aprobar()) { ?>
            <input type="button" value="Aprobar" class="boton" onclick="SolicitudModificar2(this, '', '1')">&nbsp;&nbsp;
            <input type="button" value="Anular" class="boton" onclick="SolicitudAnular(this)">&nbsp;&nbsp;
        <?php } ?>
        <?php if ($ROW[0]['estado'] == '1') { ?>
            <input type="button" value="Finalizar" class="boton" onclick="SolicitudCancela(this)">&nbsp;&nbsp;
        <?php } ?>
        <?php if ($_GET['acc'] == 'M') { ?>
            <input type="button" value="Imprimir" class="boton" onclick="window.print()">
        <?php } ?>
    </center>
</form>
<script>
    Totales();
    <?php if($ROW[0]['estado'] != '' && $ROW[0]['estado'] != '0'){?>Deshabilita();<?php } ?>
</script>
<?= $Gestor->Encabezado('D0009', 'p', '') ?>
<?= $Gestor->Footer() ?>
</body>
</html>