<?php
define('__MODULO__', 'cotizaciones');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _sol02l_crear();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<form name="form" id="form" method="post"
      action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
    <?php $Gestor->Incluir('p2', 'hr', 'Cotizaciones :: Solicitud de cotización de análisis de plaguicidas') ?>
    <?= $Gestor->Encabezado('P0002', 'e', 'Solicitud de cotización de análisis de plaguicidas') ?>
    <center>
        <table class="radius" style="font-size:12px" width="600px">
            <tr>
                <td class="titulo" colspan="2">1. Datos sobre el producto</td>
            </tr>
            <tr style="display:none">
                <td>El producto contiene:</td>
                <td><select id="contiene" name="contiene">
                        <!--<option value="">...</option>-->
                        <option value="0">Un ingrediente activo</option>
                        <option value="1">Una mezcla de ingredientes activos</option>
                    </select>
                </td>
            </tr>

            <tr style="display:none">
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td>Cliente:</td>
                <td><input type="text" id="nomcliente" name="nomcliente" class="lista" readonly
                           onclick="ClienteLista()"/>
                    <input type="hidden" id="cliente" name="cliente"/>
                </td>
            </tr>
            <tr>
                <td>Tipo de formulaci&oacute;n:</td>
                <td><select id="tipo_form" name="tipo_form" onchange="BorraIngredientes()">
                        <option value="">...</option>
                        <?php
                        $ROW2 = $Gestor->Formulaciones();
                        for ($x = 0; $x < count($ROW2); $x++) {
                            ?>
                            <option value="<?= $ROW2[$x]['id'] ?>"><?= $ROW2[$x]['nombre'] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>M&eacute;todo de an&aacute;lisis:</td>
                <td><select id="metodo" name="metodo" onchange="CambiaTipoMetodo(this.value)">
                        <option value="">...</option>
                        <option value="0">Laboratorio</option>
                        <option value="1">Registro</option>
                        <option value="2">Cliente</option>
                    </select>
                </td>
            </tr>
            <tr id="tr_metodo" style="display:none">
                <td>Cliente aporta documentaci&oacute;n:</td>
                <td><select id="aporta" name="aporta">
                        <option value="">...</option>
                        <option value="0">No</option>
                        <option value="1">S&iacute;</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Suministro del est&aacute;ndar:</td>
                <td><select id="suministro" name="suministro">
                        <option value="">...</option>
                        <option value="0">Laboratorio</option>
                        <option value="1">Cliente</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Muestra de discrepancia:</td>
                <td><select id="discre" name="discre" onchange="CambiaTipoDiscre(this.value)">
                        <option value="0">No</option>
                        <option value="1">S&iacute;</option>
                    </select>
                </td>
            </tr>
            <tr id="tr_discre" style="display:none">
                <td># Solicitud: <input type="text" id="num_sol" name="num_sol" size="15" maxlength="20"/></td>
                <td>C&oacute;digo(s) externo: <input type="text" id="num_mue" name="num_mue" maxlength="30"/></td>
            </tr>
            <tr>
                <td>Observaciones:</td>
                <td><input type="text" id="obs" name="obs" size="30" maxlength="50"/></td>
            </tr>
        </table>
        <br/>
        <table class="radius" style="font-size:12px" width="600px">
            <thead>
            <tr>
                <td class="titulo" colspan="5">2.A Ingredientes Activos&nbsp;<img onclick="ElementosMas()"
                                                                                  src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                                                                                  title="Agregar línea" class="tab"/>
                </td>
            </tr>
            <tr>
                <td><strong>#</strong></td>
                <td><strong>Ingrediente Activo</strong></td>
                <td><strong>Concentraci&oacute;n declarada&nbsp;<img src="<?php $Gestor->Incluir('olvido', 'bkg') ?>"
                                                                     title="Si no conoce la concentración digite 0"
                                                                     border="0" width="15" height="15"
                                                                     style="cursor:pointer"/></strong></td>
                <td>* <strong>Unidad</strong></td>
                <td><strong>Fuente</strong></td>
            </tr>
            </thead>
            <tbody id="lolo">
            <tr>
                <td>1.</td>
                <td><input type="text" id="elementos0" name="elementos[]" class="lista" readonly
                           onclick="ElementosLista(0)"/>
                    <input type="hidden" id="codigoE0" name="codigoE[]"/>
                </td>
                <td><input type="text" id="rangoE0" name="rangoE[]" size="20" maxlength="20"></td>
                <td><select id="tipoE0" name="tipoE[]" onchange="CambiaTipoAnalisis()">
                        <option value="">...</option>
                        <option value="0">%m/m</option>
                        <option value="1">%m/v</option>
                    </select></td>
                <td><input type="text" name="fuenteE[]" size="20" maxlength="30"></td>
            </tr>
            </tbody>
        </table>
        <br/>
        <table class="radius" style="font-size:12px" width="600px">
            <thead>
            <tr>
                <td class="titulo" colspan="5">2.B An&aacute;lisis Solicitados&nbsp;<img onclick="AnalisisMas()"
                                                                                         src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                                                                                         title="Agregar línea"
                                                                                         class="tab"/></td>
            </tr>
            <tr>
                <td><strong>#</strong></td>
                <td><strong>An&aacute;lisis</strong></td>
                <td colspan="3"></td>
                <!--<td><strong>Concentraci&oacute;n declarada&nbsp;<img src="<?php $Gestor->Incluir('olvido', 'bkg') ?>" title="Si no conoce la concentración digite 0" border="0" width="15" height="15" style="cursor:pointer"/></strong></td>
	<td>* <strong>Unidad</strong></td>
	<td><strong>Fuente</strong></td>-->
            </tr>
            </thead>
            <tbody id="lolo2">
            <tr>
                <td>1.</td>
                <td><input type="text" id="analisis0" name="analisis[]" class="lista" readonly
                           onclick="MacroAnalisisLista(0)"/>
                    <input type="hidden" id="codigo0" name="codigo[]"/>
                </td>
                <td><input type="hidden" id="rango0" name="rango[]" size="20" maxlength="20"></td>
                <td><select id="tipo0" name="tipo[]" onchange="CambiaTipoAnalisis()" style="visibility:hidden">
                        <option value="">...</option>
                        <option value="0">%m/m</option>
                        <option value="1">%m/v</option>
                    </select></td>
                <td><input type="hidden" name="fuente[]" size="20" maxlength="30"></td>
            </tr>
            </tbody>
        </table>
        <br/>
        <table class="radius" style="font-size:12px" width="600px">
            <thead>
            <tr>
                <td class="titulo" colspan="2">2.C Impurezas&nbsp;<img onclick="ImpurezasMas()"
                                                                       src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                                                                       title="Agregar línea" class="tab"/></td>
            </tr>
            <tr>
                <td><strong>#</strong></td>
                <td><strong>Nombre</strong></td>
            </tr>
            <tbody id="lolo3">
            <tr>
                <td>1.</td>
                <td><input type="text" id="impurezas0" name="impurezas[]" class="lista" readonly
                           onclick="ImpurezasLista(0)"/>
                    <input type="hidden" id="codigoB0" name="codigoB[]"/>
                </td>
            </tr>
            </tbody>
        </table>
        <br/>
        <table class="radius" style="font-size:12px" width="600px">
            <tr>
                <td>
                    <strong>N&uacute;mero de muestras a realizar:</strong>&nbsp;
                    <input type="text" id="total" name="total" value="0" class="cantidad" onblur="_INT(this)">&nbsp;
                    <input type="checkbox" id="densidad" name="densidad" value="E"/>&nbsp;Densidad&nbsp;
                </td>
            </tr>
            <tr>
                <td><font size="-1">* Si la concentraci&oacute;n est&aacute; declarada como %m/v, debe solicitar la
                        determinaci&oacute;n de la densidad</font></td>
            </tr>
        </table>
        <br/>
        <table class="radius" style="font-size:12px" width="600px">
            <tr>
                <td class="titulo" colspan="2">3. Nombre del solicitante</td>
            </tr>
            <tr>
                <td>Nombre:</td>
                <td><input type="text" id="nombre_sol" name="nombre_sol" size="50" maxlength="60"/></td>
            </tr>
        </table>
        <br/>
        <table class="radius" style="font-size:12px" width="600px">
            <tr>
                <td class="titulo" colspan="2">4. Nombre del encargado de entregar las muestras en el laboratorio</td>
            </tr>
            <tr>
                <td>Nombre:</td>
                <td><input type="text" id="encargado" name="encargado" size="50" maxlength="60"/></td>
            </tr>
        </table>
        <br/>
        <input type="button" value="Aceptar" class="boton" onclick="SolicitudGenerar(this)">
    </center>
</form>
<?= $Gestor->Encabezado('P0002', 'p', '') ?>
</body>
</html>