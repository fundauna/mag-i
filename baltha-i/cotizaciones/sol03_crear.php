<?php
define('__MODULO__', 'cotizaciones');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _sol03_crear();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<form name="form" id="form" method="post" enctype="multipart/form-data"
      action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
    <input type="hidden" id="cliente" value="<?= $Gestor->Tipo() ?>"/><!-- GUARDA SI EL CLIENTE ES INTERNO O EXTERNO-->
    <?php $Gestor->Incluir('p4', 'hr', 'Cotizaciones :: Solicitud de cotizaci�n de an�lisis de plagas') ?>
    <?= $Gestor->Encabezado('P0004', 'e', 'Solicitud de cotizaci�n de an�lisis de plagas') ?>
    <center>
        <table class="radius" style="font-size:12px" width="600px">
            <thead>
            <tr>
                <td class="titulo" colspan="5">1. Servicio(s) o producto(s) requerido(s)&nbsp;<img
                            onclick="AnalisisMas()" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                            class="tab"/></td>
            </tr>
            <tr>
                <td><strong>#</strong></td>
                <td><strong>Cod.</strong></td>
                <td><strong>An&aacute;lisis</strong></td>
                <td><strong># de muestras</strong></td>
                <td><strong>&Aacute;rea</strong></td>
            </tr>
            </thead>
            <tbody id="lolo">
            <tr>
                <td>1.</td>
                <td><input type="text" size="3" id="cod0" readonly/><input type="hidden" id="tarifa0"/></td>
                <td><input type="text" id="analisis0" name="analisis[]" class="lista" readonly
                           onclick="AnalisisLista(0)"/>
                    <input type="hidden" id="codigo0" name="codigo[]"/>
                </td>
                <td><input type="text" id="cant0" name="cant[]" maxlength="3" value="0" onblur="_INT(this);Totales();"
                           class="cantidad"></td>
                <td><select id="dpto0" name="dpto[]">
                        <option value="">...</option>
                        <!--<option value="0">Entomolog&iacute;a</option>
                        <option value="1">Fitopatolog&iacute;a</option>
                        <option value="2">Nematolog&iacute;a</option>-->
                        <option value="3">Biolog&iacute;a molecular</option>
                    </select></td>
            </tr>
            </tbody>
            <tr>
                <td colspan="5">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td colspan="2"></td>
                <td><strong>N&uacute;mero de muestras a realizar:</strong></td>
                <td><input type="text" id="total" name="total" value="0" class="cantidad" readonly></td>
                <td></td>
            </tr>
        </table>
        <br/>
        <table class="radius" style="font-size:12px" width="600px">
            <tr>
                <td class="titulo" colspan="2">2. Informaci&oacute;n de la muestra</td>
            </tr>
            <tbody id="td_estacion" style="display:none">
            <tr>
                <td>Nombre del solicitante:</td>
                <td><input type="text" id="solicitante" name="solicitante" size="30" maxlength="60"/></td>
            </tr>
            <tr>
                <td>Estaci&oacute;n de inter&eacute;s:</td>
                <td><select id="estacion" name="estacion" onchange="CambiaEstacion(this.value)">
                        <option value="">...</option>
                        <option value="0">Puerto Lim&oacute;n</option>
                        <option value="1">Puerto Caldera</option>
                        <option value="2">Pe&ntilde;as Blancas</option>
                        <option value="3">Paso Canoas</option>
                        <option value="4">Aeropuerto de Liberia</option>
                        <option value="5">Aeropuerto Juan Santamar&iacute;a</option>
                        <option value="6">Los Chiles</option>
                        <option value="7">Aeropuerto de Pavas</option>
                        <option value="8">Sixaola</option>
                        <option value="9">Otra:</option>
                    </select>&nbsp;
                    <input type="text" id="otro2" name="otro2" size="20" maxlength="30" style="display:none"/>
            </tr>
            <tr>
                <td>Fecha deseada para apertura:</td>
                <td><input type="text" id="fecha" name="fecha" class="fecha" readonly
                           onClick="show_calendar(this.id);"/></td>
            </tr>
            <tr>
                <td>Carta de solicitud de apertura:</td>
                <td><input type="file" name="archivo" id="archivo"></td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            </tbody>
            <tr>
                <td>Nombre del cultivo:</td>
                <td><input type="text" id="cultivo" name="cultivo" size="30" maxlength="50"/></td>
            </tr>
            <tr>
                <td>Nombre del cient&iacute;fico cultivo:</td>
                <td><input type="text" id="cientifico" name="cientifico" size="30" maxlength="50"/></td>
            </tr>
            <tr>
                <td>Sintomatolog&iacute;a observada:</td>
                <td><input type="text" id="sintomas" name="sintomas" size="50" maxlength="100"/></td>
            </tr>
            <tr>
                <td>Plaga de inter&eacute;s a detectar:</td>
                <td><input type="text" id="plaga" name="plaga" size="30" maxlength="30"/></td>
            </tr>
            <tr>
                <td>Tipo de muestra:</td>
                <td><select id="tipo" name="tipo" onchange="CambiaTipo(this.value)" style="width:120px;">
                        <option value="">...</option>
                        <option value="B">Planta Completa</option>
                        <option value="C">Hoja</option>
                        <option value="D">Tallo o tronco</option>
                        <option value="E">Fruto</option>
                        <option value="F">Bulbo</option>
                        <option value="G">Cultivo celular</option>
                        <option value="H">Medio enraizamiento</option>
                        <option value="I">Ra&iacute;z</option>
                        <option value="J">Flor</option>
                        <option value="K">Tub&eacute;rculo</option>
                        <option value="L">Esqueje</option>
                        <option value="M">Semilla</option>
                        <option value="N">Rizoma</option>
                        <option value="O">Artr&oacute;podos y Gastr&oacute;podo de importancia agr&iacute;cola</option>
                        <option value="P">Rama</option>
                        <option value="Q">Otro</option>
                    </select>
                    &nbsp;
                    <input type="text" id="otro" name="otro" size="30" maxlength="50" style="display:none"/></td>
            </tr>
            <tr>
                <td>Observaciones:</td>
                <td><input type="text" id="obs" name="obs" size="50" maxlength="100"/></td>
            </tr>
            <tr>
                <td colspan="2">Partes afectadas:</td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%">
                        <tr>
                            <td><input type="checkbox" id="afectadaA" name="afectadaA" value="1"
                                       onclick="Marca(this.checked)"/>&nbsp;Planta Completa
                            </td>
                            <td><input type="checkbox" id="afectadaB" name="afectadaB" value="1"
                                       onclick="Marca(this.checked)"/>&nbsp;Fruto
                            </td>
                            <td><input type="checkbox" id="afectadaC" name="afectadaC" value="1"
                                       onclick="Marca(this.checked)"/>&nbsp;Bulbo
                            </td>
                            <td><input type="checkbox" id="afectadaD" name="afectadaD" value="1"
                                       onclick="Marca(this.checked)"/>&nbsp;Hoja
                            </td>
                            <td><input type="checkbox" id="afectadaE" name="afectadaE" value="1"
                                       onclick="Marca(this.checked)"/>&nbsp;Tallo o tronco
                            </td>
                            <td><input type="checkbox" id="afectadaF" name="afectadaF" value="1"
                                       onclick="Marca(this.checked)"/>&nbsp;Ra&iacute;z
                            </td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="afectadaG" name="afectadaG" value="1"
                                       onclick="Marca(this.checked)"/>&nbsp;Flor
                            </td>
                            <td><input type="checkbox" id="afectadaH" name="afectadaH" value="1"
                                       onclick="Marca(this.checked)"/>&nbsp;Tub&eacute;rculo
                            </td>
                            <td><input type="checkbox" id="afectadaI" name="afectadaI" value="1"
                                       onclick="Marca(this.checked)"/>&nbsp;Esqueje
                            </td>
                            <td><input type="checkbox" id="afectadaJ" name="afectadaJ" value="1"
                                       onclick="Marca(this.checked)"/>&nbsp;Semilla
                            </td>
                            <td><input type="checkbox" id="afectadaK" name="afectadaK" value="1"
                                       onclick="Marca(this.checked)"/>&nbsp;Rizoma
                            </td>
                            <td><input type="checkbox" id="afectadaL" name="afectadaL" value="1"
                                       onclick="Marca(this.checked)"/>&nbsp;Rama
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br/>
        <table class="radius" style="font-size:12px" width="600px">
            <tr>
                <td class="titulo" colspan="2">3. Informaci&oacute;n del cultivo</td>
            </tr>
            <tr>
                <td colspan="2"><strong>Ubicaci&oacute;n del cultivo</strong></td>
            </tr>
            <tr>
                <td>Provincia:</td>
                <td><select id="provincia" name="provincia">
                        <option value="">...</option>
                        <option value="1">San Jos&eacute;</option>
                        <option value="2">Alajuela</option>
                        <option value="3">Cartago</option>
                        <option value="4">Heredia</option>
                        <option value="5">Guanacaste</option>
                        <option value="6">Puntarenas</option>
                        <option value="7">Lim&oacute;n</option>
                    </select></td>
            </tr>
            <tr>
                <td>Cant&oacute;n:</td>
                <td><input type="text" name="canton" id="canton" size="30" maxlength="30"/></td>
            </tr>
            <tr>
                <td>Direcci&oacute;n:</td>
                <td><input type="text" name="direccion" id="direccion" size="50" maxlength="100"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td>Gravedad de da&ntilde;o:</td>
                <td><select id="gravedad" name="gravedad">
                        <option value="">...</option>
                        <option value="1">Leve</option>
                        <option value="2">Moderado</option>
                        <option value="3">Severo</option>
                        <option value="4">Perdida Total</option>
                    </select></td>
            </tr>
            <tr>
                <td>Etapa de desarrollo o edad del cultivo:</td>
                <td><input type="text" name="etapa" id="etapa" size="30" maxlength="30"/></td>
            </tr>
            <tr>
                <td>&Aacute;rea del cultivo y &aacute;rea afectada:</td>
                <td><input type="text" name="area_cultivo" id="area_cultivo" size="20" maxlength="30"/>&nbsp;<input
                            type="text" name="area_afectada" id="area_afectada" size="20" maxlength="30"/></td>
            </tr>
            <tr>
                <td>Ha aplicado plaguicidas:</td>
                <td><select id="aplicado" name="aplicado" onchange="CambiaTipo2(this.value)">
                        <option value="0">No</option>
                        <option value="1">S&iacute;</option>
                    </select></td>
            </tr>
            <tr id="tr_plaguicidas" style="display:none">
                <td colspan="2">
                    <!-- -->
                    <table>
                        <thead>
                        <tr>
                            <td><strong>Nombre del plaguicida</strong></td>
                            <td><strong>Tipo</strong></td>
                            <td><strong>Frecuencia</strong></td>
                            <td><strong>Dosis</strong></td>
                            <td><strong>&Uacute;ltima aplicaci&oacute;n</strong></td>
                        </tr>
                        </thead>
                        <tbody id="lolo2">
                        <!--<tr>
                            <td><input type="text" id="plag0" name="plag[]" size="20" maxlength="30" /></td>
                            <td><input type="text" id="tip0" name="tip[]" size="20" maxlength="20" /></td>
                            <td><input type="text" id="frecuencia0" name="frecuencia[]" size="10" maxlength="20" /></td>
                            <td><input type="text" id="dosis0" name="dosis[]" size="10" maxlength="20" /></td>
                            <td><input type="text" id="fecha0" name="fecha[]" class="fecha" readonly onClick="show_calendar(this.id);" /></td>
                        </tr>-->
                        </tbody>
                        <tr>
                            <td colspan="5"><input type="button" value="Agregar plaguicida" onclick="PlaguicidasMas()"
                                                   title="Agregar l�nea"/></td>
                        </tr>
                    </table>
                    <!-- -->
                </td>
            </tr>
            <tr>
                <td>Tipo de riego:</td>
                <td><input type="text" name="riego" id="riego" size="20" maxlength="20"/></td>
            </tr>
            <tr>
                <td>Tipo de drenaje:</td>
                <td><input type="text" name="drenaje" id="drenaje" size="20" maxlength="20"/></td>
            </tr>
            <tr>
                <td>Tipo de suelo:</td>
                <td><input type="text" name="suelo" id="suelo" size="20" maxlength="20"/></td>
            </tr>
            <tr>
                <td>Programa de fertilizaci&oacute;n:</td>
                <td><input type="text" name="programa" id="programa" size="20" maxlength="20"/></td>
            </tr>
            <tr>
                <td>Densidad de la plantaci&oacute;n o distancia de siembra:</td>
                <td><input type="text" name="densidad" id="densidad" size="20" maxlength="30"/></td>
            </tr>
        </table>
        <br/>
        <input type="button" value="Aceptar" class="boton" onclick="SolicitudGenerar(this)">
    </center>
</form>
<?= $Gestor->Encabezado('P0004', 'p', '') ?>
</body>
</html>