<?php
define('__MODULO__', 'cotizaciones');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _detalle_LRE();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <script>
        function MuestraAnulacion(_reactivos, _material, _personal, _tiempo, _equipos, _viabilidad) {
            document.getElementById('justi').style.display = '';
            document.getElementById('Jreactivos').selectedIndex = parseInt(_reactivos) + 1;
            document.getElementById('Jmaterial').selectedIndex = parseInt(_material) + 1;
            document.getElementById('Jpersonal').selectedIndex = parseInt(_personal) + 1;
            document.getElementById('Jtiempo').selectedIndex = parseInt(_tiempo) + 1;
            document.getElementById('Jequipos').selectedIndex = parseInt(_equipos) + 1;
            document.getElementById('Jviabilidad').selectedIndex = parseInt(_viabilidad) + 1;

            document.getElementById('Jreactivos').disabled = true;
            document.getElementById('Jmaterial').disabled = true;
            document.getElementById('Jpersonal').disabled = true;
            document.getElementById('Jtiempo').disabled = true;
            document.getElementById('Jequipos').disabled = true;
            document.getElementById('Jviabilidad').disabled = true;
            document.getElementById('obs').readOnly = true;
        }
    </script>
</head>
<body>
<center>
    <?php $Gestor->Incluir('d13', 'hr', 'Cotizaciones :: Cotizaci&oacute;n de An&aacute;lisis') ?>
    <?= $Gestor->Encabezado('D0013', 'e', 'Cotizaci&oacute;n de An&aacute;lisis') ?>
    <br>
    <table class="radius" style="font-size:12px" width="600px">
        <tr>
            <td class="titulo" colspan="3">Informaci&oacute;n General</td>
        </tr>
        <tr>
            <td width="145">Fecha:</td>
            <td width="443"><?= $ROW[0]['fecha'] ?></td>
        </tr>
        <tr>
            <td>Estado:</td>
            <td><?= $Gestor->Estado($ROW[0]['estado']) ?></td>
        </tr>
        <tr>
            <td>Cliente:</td>
            <td><?= $ROW[0]['tmp'] ?></td>
        </tr>
        <tr>
            <td>Producto:</td>
            <td><?= $ROW[0]['nombre'] ?></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="600px">
        <thead>
        <tr>
            <td class="titulo" colspan="5">Detalle</td>
        </tr>
        <tr>
            <td><strong>Item</strong></td>
            <td><strong>Descripci&oacute;n</strong></td>
            <td title="Cantidad de muestras"><strong>Cant.</strong></td>
            <td><strong>P. Unitario</strong></td>
            <td><strong>P. Total</strong></td>
        </tr>
        </thead>
        <tbody id="lolo">
        <?php
        $ROW2 = $Gestor->ObtieneLineas();
        for ($x = 0, $total = 0; $x < count($ROW2); $x++) {
            $total += $ROW2[$x]['cantidad'];
            ?>
            <tr>
                <td><?= $ROW2[$x]['tarifa'] ?></td>
                <td><?= $ROW2[$x]['analisis'] ?></td>
                <td><?= $ROW2[$x]['cantidad'] ?></td>
                <td><?= $Gestor->Formato($ROW2[$x]['monto']) ?></td>
                <td><?= $Gestor->Formato($ROW2[$x]['total']) ?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
        <tr>
            <td colspan="5">
                <hr/>
            </td>
        </tr>
        <tr>
            <td></td>
            <td align="right"><strong>N&uacute;mero de muestras a realizar:</strong></td>
            <td><?= $total ?></td>
            <td align="right"></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="4" align="right"><strong>Total:</strong></td>
            <td>&cent; <?= $Gestor->Formato($ROW[0]['monto']) ?></td>
        </tr>
        <?php if ($ROW[0]['estado'] != '5') { ?>
            <tr>
                <td colspan="5" style="text-align:justify">
                    <br/>
                    <center>
                        <font size="-1"><strong>Descuentos aplicados de acuerdo con la cantidad de
                                an&aacute;lisis</strong></font>
                        <table border="1" cellspacing="0">
                            <tr>
                                <td><strong>Cantidad de muestras</strong></td>
                                <td><strong>Descuento (%)</strong></td>
                            </tr>
                            <tr>
                                <td>6 a 10</td>
                                <td>5</td>
                            </tr>
                            <tr>
                                <td>11 en adelante</td>
                                <td>10</td>
                            </tr>
                        </table>
                    </center>
                    <br/>
                    <font size="-1"><strong>
                            El costo de los an&aacute;lisis queda sujeto a cualquier variaci&oacute;n en las tarifas;
                            las cuales se publican en el diario oficial La Gaceta.
                            <br/><br/>
                            Forma de pago:
                        </strong></font>&nbsp;Por adelantado, con el siguiente sistema de pago:
                    <br/><br/>
                    Banco de Costa Rica. N&ordm; de Cuenta: 001-0273982-8 (Colones)<br/>
                    Banco Nacional de Costa Rica: N&ordm; de Cuenta: 100-02-000-620983-0 (D&oacute;lares)
                    <br/><br/>
                    El depositante debe presentar el dep&oacute;sito o comprobante de la transferencia electr&oacute;nica
                    en la caja del Departamento Administrativo y Financiero
                    del Servicio Fitosanitario del Estado.
                    <br/><br/>
                    <strong>Importante:</strong> Las muestras se reciben, &uacute;nica y estrictamente. contra la
                    entrega de la copia del recibo de pago emitido por
                    el Departamento Administrativo y Financiero del Servicio Fitosanitario del Estado, correspondiente
                    al costo de los an&aacute;lisis aqu&iacute; cotizados.
                </td>
            </tr>
        <?php } ?>
    </table>

    <?php
    if ($ROW[0]['estado'] == '5') {
        $ROW2 = $Gestor->ObtieneAnulacion();
        ?>
        <br/>
        <table id="justi" class="radius" style="font-size:12px; display:none">
            <tr>
                <td class="titulo" colspan="2">Justificaci&oacute;n de la anulaci&oacute;n</td>
            </tr>
            <tr align="center">
                <td><strong>Disponibilidad de</strong></td>
                <td><strong>Capacidad</strong></td>
            </tr>
            <tr>
                <td>Reactivos y Solventes:</td>
                <td><select id="Jreactivos">
                        <option value="">...</option>
                        <option value="0">No</option>
                        <option value="1">S&iacute;</option>
                    </select></td>
            </tr>
            <tr>
                <td>Material de Laboratorio:</td>
                <td><select id="Jmaterial">
                        <option value="">...</option>
                        <option value="0">No</option>
                        <option value="1">S&iacute;</option>
                    </select></td>
            </tr>
            <tr>
                <td>Personal de Laboratorio:</td>
                <td><select id="Jpersonal">
                        <option value="">...</option>
                        <option value="0">No</option>
                        <option value="1">S&iacute;</option>
                    </select></td>
            </tr>
            <tr>
                <td>Tiempo de trabajo:</td>
                <td><select id="Jtiempo">
                        <option value="">...</option>
                        <option value="0">No</option>
                        <option value="1">S&iacute;</option>
                    </select></td>
            </tr>
            <tr>
                <td>Equipos de rutina y trabajo:</td>
                <td><select id="Jequipos">
                        <option value="">...</option>
                        <option value="0">No</option>
                        <option value="1">S&iacute;</option>
                    </select></td>
            </tr>
            <tr>
                <td>Viabilidad del a&aacute;lisis:</td>
                <td><select id="Jviabilidad">
                        <option value="">...</option>
                        <option value="0">No</option>
                        <option value="1">S&iacute;</option>
                    </select></td>
            </tr>
            <tr>
                <td>Observaciones:</td>
                <td><textarea id="obs"><?= $ROW2[0]['obs'] ?></textarea></td>
            </tr>
        </table>
        <?php
        echo "<script>MuestraAnulacion('{$ROW2[0]['reactivos']}','{$ROW2[0]['material']}','{$ROW2[0]['personal']}','{$ROW2[0]['tiempo']}','{$ROW2[0]['equipos']}','{$ROW2[0]['viabilidad']}')</script>";
    }
    ?>
    <br/>


    <br/>
    <input type="button" value="Imprimir" class="boton" onclick="window.print()">
</center>
<?= $Gestor->Encabezado('D0013', 'p', '') ?>
<?= $Gestor->Footer() ?>
</body>
</html>