<?php
define('__MODULO__', 'cotizaciones');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _cotizacion_LCC();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

$ntxt = $ROW[0]['tipo'] == '1' ? 'Fertilizantes' : 'Plaguicidas';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<center>
    <?php $Gestor->Incluir('d11', 'hr', 'Cotizaciones :: Cotizaci&oacute;n de An&aacute;lisis') ?>
    <?= $Gestor->Encabezado('D0011', 'e', 'Cotizaci&oacute;n de An&aacute;lisis de ' . $ntxt) ?>

    <br>
    <table class="radius" style="font-size:12px" width="600px">
        <tr>
            <td class="titulo" colspan="3">Informaci&oacute;n General</td>
        </tr>
        <tr>
            <td>Solicitud de origen:</td>
            <td><?= $ROW[0]['solicitud'] ?></td>
        </tr>
        <tr>
            <td>Fecha:</td>
            <td><?= $ROW[0]['fecha'] ?></td>
        </tr>
        <tr>
            <td>Estado:</td>
            <td><?= $Gestor->Estado($ROW[0]['estado']) ?></td>
        </tr>
        <tr>
            <td>Cliente:</td>
            <td><?= $ROW[0]['tmp'] ?></td>
        </tr>
        <tr>
            <td id="td_tipo">...</td>
            <td><?= $ROW[0]['nombre'] ?></td>
        </tr>
        <tr>
            <td>Observaciones:</td>
            <td><strong><?= $ROW[0]['obs'] ?></strong></td>
        </tr>
        <tr>
            <td>Tipo:</td>
            <td><select disabled>
                    <option>Fertilizantes</option>
                    <option <?php if ($ROW[0]['tipo'] == '2') echo 'selected'; ?>>Plaguicidas</option>
                </select></td>
        </tr>
        <tr>
            <td>Informaci&oacute;n adicional:</td>
            <td>
                <select disabled>
                    <option>Muestra de servicio</option>
                    <option <?php if ($ROW[0]['tipo2'] == '1') echo 'selected'; ?>>Muestra para fiscalizaci&oacute;n
                    </option>
                </select>&nbsp;<input
                        type="checkbox" <?php if ($Gestor->Existe('1', $ROW[0]['insumos'])) echo 'checked'; ?>disabled/>&nbsp;Muestra
                para el registro de insumos
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="600px">
        <thead>
        <tr>
            <td class="titulo" colspan="5">Detalle</td>
        </tr>
        <tr>
            <td><strong>Item</strong></td>
            <td><strong>Descripci&oacute;n</strong></td>
            <td title="Cantidad de muestras"><strong>Cant.</strong></td>
            <td><strong>P. Unitario</strong></td>
            <td><strong>P. Total</strong></td>
        </tr>
        </thead>
        <tbody id="lolo">
        <?php
        $ROW2 = $Gestor->ObtieneLineas();
        $muestras = $subtotal = $descuento = 0; //VARIABLES GENERALES
        $cant = $tmp = $porcent = 0;

        for ($x = 0; $x < count($ROW2); $x++) {
            $muestras += $ROW2[$x]['cantidad'];
            //
            if ($ROW2[$x]['descuento'] == '1') {
                if ($ROW2[$x]['cantidad'] > 3 && $ROW2[$x]['cantidad'] < 11) $porcent = 0.05;
                else if ($ROW2[$x]['cantidad'] > 10) $porcent = 0.1;
                else $porcent = 0;
            } else $porcent = 0;
            //
            $tmp = $ROW2[$x]['cantidad'] * $ROW2[$x]['monto'];
            $subtotal += $tmp;
            $descuento += ($tmp * $porcent);
            ?>
            <tr>
                <td><?= $ROW2[$x]['tarifa'] ?></td>
                <td><?= $ROW2[$x]['analisis'] ?></td>
                <td><?= $ROW2[$x]['cantidad'] ?></td>
                <td><?= $Gestor->Formato($ROW2[$x]['monto']) ?></td>
                <td><?= $Gestor->Formato($ROW2[$x]['total']) ?></td>
            </tr>
            <?php
            if ($porcent > 0) echo "<tr style='font-size:11px'><td colspan='4' align='right'>Descuento:</td><td>", $Gestor->Formato($tmp * $porcent), "</td></tr>";
        }
        ?>
        </tbody>
        <tr>
            <td colspan="5">
                <hr/>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2"><strong>N&uacute;mero de muestras a realizar:</strong></td>
            <td><?= $muestras ?></td>
            <td align="right"></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="4" align="right"><strong>Sub Total &cent;:</strong></td>
            <td><?= $Gestor->Formato($subtotal) ?></td>
        </tr>
        <tr>
            <td colspan="4" align="right"><strong>Descuento &cent;:</strong></td>
            <td><?= $Gestor->Formato($descuento) ?></td>
        </tr>
        <tr>
            <td colspan="4" align="right"><strong>Total:</strong></td>
            <td><?= $Gestor->Formato($ROW[0]['monto']) ?></td>
        </tr>
        <tr>
            <td colspan="5" style="text-align:justify">
                <br/>
                <font size="-1"><strong>
                        El costo de los analisis queda sujeto a cualquier variacion en las tarifas; las cuales se
                        publican en el diario oficial La Gaceta.
                        <br/><br/>
                        Forma de pago:
                    </strong></font>&nbsp;Por adelantado, con el siguiente sistema de pago:
                <br/><br/>
                Banco de Costa Rica. N&ordm; de Cuenta: 001-0273982-8 (Colones)<br/>
                Banco Nacional de Costa Rica: N&ordm; de Cuenta: 100-02-000-620983-0 (D&oacute;lares)
                <br/><br/>
                El depositante debe presentar el dep&oacute;sito o comprobante de la transferencia electr&oacute;nica en
                la caja del Departamento Administrativo y Financiero
                del Servicio Fitosanitario del Estado.
                <br/><br/>
                <strong>Importante:</strong> Las muestras se reciben, &uacute;nica y estrictamente. contra la entrega de
                la copia del recibo de pago emitido por
                el Departamento Administrativo y Financiero del Servicio Fitosanitario del Estado, correspondiente al
                costo de los an&aacute;lisis aqu&iacute; cotizados.
            </td>
        </tr>
    </table>
    <br/>
    <input type="button" value="Imprimir" class="boton" onclick="window.print()">
</center>
<script>CambiaTipo('<?=$ROW[0]['tipo']?>');</script>
<?= $Gestor->Encabezado('D0011', 'p', '') ?>
<?= $Gestor->Footer() ?>
</body>
</html>