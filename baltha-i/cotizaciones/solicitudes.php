<?php
define('__MODULO__', 'cotizaciones');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _solicitudes();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<center>
<?php $Gestor->Incluir('d24', 'hr', 'Cotizaciones :: Solicitudes') ?>
<?= $Gestor->Encabezado('D0024', 'e', 'Solicitudes') ?>
<BR>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center">
            <tr>
                <td class="titulo" colspan="2">Filtro</td>
            </tr>
            <tr>
                <td>No. de solicitud:</td>
                <td><input type="text" id="solicitud" name="solicitud" value="<?= $_POST['solicitud'] ?>" size="12"
                           maxlength="12"/></td>
            </tr>
            <tr>
                <td>Cliente:</td>
                <td><input type="text" id="tmp" name="tmp" class="lista" readonly onclick="ClientesLista()"
                           value="<?= $_POST['tmp'] ?>"/>
                    <input type="hidden" id="cliente" name="cliente" value="<?= $_POST['cliente'] ?>"/></td>
            </tr>
            <tr>
                <td>Fecha:</td>
                <td><input type="text" id="desde" name="desde" class="fecha" value="<?= $_POST['desde'] ?>" readonly
                           onClick="show_calendar(this.id);">&nbsp;
                    <input type="text" id="hasta" name="hasta" class="fecha" value="<?= $_POST['hasta'] ?>" readonly
                           onClick="show_calendar(this.id);"></td>
            </tr>
            <?php if ($_POST['LID'] == '3') { ?>
                <tr>
                    <td>Tipo:</td>
                    <td><select name="tipo" style="width:90px">
                            <option value="">Todas</option>
                            <option value="1" <?php if ($_POST['tipo'] == '1') echo 'selected'; ?>>Fertilizantes
                            </option>
                            <option value="2" <?php if ($_POST['tipo'] == '2') echo 'selected'; ?>>Plaguicidas</option>
                        </select></td>
                </tr>
            <?php } ?>
            <tr>
                <td>Estado:</td>
                <td><select name="estado" style="width:90px">
                        <option value="r">Generada</option>
                        <option value="2" <?php if ($_POST['estado'] == '2') echo 'selected'; ?>>Aprobada</option>
                        <option value="a" <?php if ($_POST['estado'] == 'a') echo 'selected'; ?>>Anulada</option>
                        <option value="f" <?php if ($_POST['estado'] == 'f') echo 'selected'; ?>>Finalizada</option>
                        <option value="" <?php if ($_POST['estado'] == '') echo 'selected'; ?>>Todos</option>
                    </select>&nbsp;<input type="button" value="Buscar" class="boton" onclick="SolicitudesBuscar(this)"/>
                </td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:650px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>No.</th>
                <th>Fecha</th>
                <th>Cliente</th>
                <th>Tipo</th>
                <th>Estado</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->SolicitudesMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#"
                           onclick="SolicitudesDetalle('<?= $ROW[$x]['numero'] ?>', '<?= $ROW[$x]['tipo'] ?>')"><?= $ROW[$x]['numero'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['fecha1'] ?></td>
                    <td><?= $ROW[$x]['cliente'] ?></td>
                    <td><?= $Gestor->Tipo($ROW[$x]['tipo']) ?></td>
                    <td><?= $Gestor->Estado($ROW[$x]['estado']) ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <?php if ($Gestor->LCC()) { ?>
            <br/><input type="button" value="Generar Sol. Fertilizantes" class="boton"
                        onClick="SolicitudCrear(1);">&nbsp;&nbsp;&nbsp;&nbsp;<input type="button"
                                                                                    value="Generar Sol. Plaguicidas"
                                                                                    class="boton"
                                                                                    onClick="SolicitudCrear(2);">
        <?php } ?>
    </div>
</center>
<?= $Gestor->Encabezado('D0024', 'p', '') ?>
</body>
</html>