<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _inventario_general();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <script>
        function Cambia(_ctr) {
            document.formulario.nombre.value = _ctr.options[_ctr.selectedIndex].text;
        }
    </script>
</head>
<body>
<?php $Gestor->Incluir('k10', 'hr', 'Reportes :: Inventario, Listado General') ?>
<?= $Gestor->Encabezado('K0010', 'e', 'Inventario, Listado General') ?>
<center>
    <form name="formulario" action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>"
          method="post" target="_blank">
        <table class="radius" align="center" width="450px">
            <tr>
                <td class="titulo" colspan="4">Filtro</td>
            </tr>
            <tr>
                <td>Tipo:<br/>
                    <select name="tipo" style="width:150px;" onchange="Cambia(this);">
                        <option value="">...</option>
                        <?php
                        $ROW = $Gestor->TiposMuestra();
                        for ($x = 0; $x < count($ROW); $x++) {
                            ?>
                            <option value="<?= $ROW[$x]['cs'] ?>"><?= $ROW[$x]['nombre'] ?></option>
                        <?php } ?>
                        <option <?= isset($_POST['tipo']) && $_POST['tipo'] == -99 ? 'selected' : '' ?> value="-99">
                            Todos
                        </option>
                    </select>
                    <input type="hidden" name="nombre" value="<?= $ROW[0]['nombre'] ?>"/>
                </td>
                <td>Registra E/S:<br/>
                    <select name="es">
                        <option value='0'>No</option>
                        <option value='1'>S&iacute;</option>
                    </select>
                </td>
                <td>Formato:<br/>
                    <select name="formato">
                        <option value='0'>Texto</option>
                        <option value='1'>Excel</option>
                    </select>
                </td>
                <td><br/><input type="button" value="Buscar" class="boton2" onclick="this.form.submit();"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <a href="#" onclick="location.href='menu.php'">[Atr&aacute;s]</a>
</center>
<?= $Gestor->Encabezado('K0010', 'p', '') ?>
</body>
</html>