<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _solicitudesXcliente();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('k26', 'hr', 'Reportes :: Solicitudes por cliente') ?>
<?= $Gestor->Encabezado('K0026', 'e', 'Solicitudes por cliente') ?>
<center>
    <form name="formulario" action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>"
          method="post" target="_blank">
        <input type="hidden" name="LAB" id="LAB" value="<?= $_POST['LAB'] ?>"/>
        <input type="hidden" name="cliente" id="cliente"/>
        <table class="radius" align="center">
            <tr>
                <td class="titulo" colspan="9">Filtro</td>
            </tr>
            <tr>
                <td>Desde: <input type="text" id="desde" name="desde" class="fecha" readonly
                                  onClick="show_calendar(this.id);">&nbsp;
                    Hasta: <input type="text" id="hasta" name="hasta" class="fecha" readonly
                                  onClick="show_calendar(this.id);"></td>
                <?php if ($_POST['LAB'] == '3') { ?>
                    <td>Tipo:</td>
                    <td><select name="tipo" style="width:90px">
                            <option value="">Todas</option>
                            <option value="1">Fertilizantes</option>
                            <option value="2">Plaguicidas</option>
                        </select></td>
                <?php } ?>
                <td>Cliente:</td>
                <td><input type="text" name="nomcliente" id="nomcliente" class="lista" readonly
                           onclick="ClientesLista()"></td>
                <td>Estado:</td>
                <td><select name="estado">
                        <option value="0">Generada</option>
                        <option value="1">Pendiente</option>
                        <option value="2">Aprobada</option>
                        <option value="5">Anulada</option>
                        <option value="7">Finalizada</option>
                        <option value="">Todos</option>
                    </select></td>
                <td>Formato:&nbsp;
                    <select name="formato">
                        <option value=0>Texto</option>
                        <option value=1>Excel</option>
                    </select>
                </td>
                <td><input type="button" value="Buscar" class="boton" onclick="SolicitudesBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <a href="#" onclick="location.href='menu.php'">[Atr&aacute;s]</a>
</center>
<?= $Gestor->Encabezado('K0026', 'p', '') ?>
</body>
</html>