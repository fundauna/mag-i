<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _rep_usuarios();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('k24', 'hr', 'Reportes :: Usuarios') ?>
<?= $Gestor->Encabezado('K0024', 'e', 'Usuarios') ?>
<center>
    <form action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>" method="post"
          target="_blank">
        <table class="radius" align="center">
            <tr>
                <td class="titulo" colspan="5">Filtro</td>
            </tr>
            <tr>
                <td><strong>Laboratorio:</strong> <select name="lab"><?= $Gestor->getUsuario(); ?></select></td>
                <td><strong>Usuario:</strong> <select name="usuario">
                        <option value="">Todos</option>
                        <?php
                        $ROW = $Gestor->UsuariosMuestra();
                        for ($x = 0; $x < count($ROW); $x++) {
                            ?>
                            <option value="<?= $ROW[$x]['id'] ?>"><?= $ROW[$x]['nombre'], ' ', $ROW[$x]['ap1'] ?></option>
                            <?php
                        }
                        ?>
                    </select></td>
                <td><strong>Estado:</strong> <select name="estado">
                        <option value='1'>Activo</option>
                        <option value='0'>Inactivo</option>
                        <option value='2'>Cond. Especial</option>
                        <option value="">Todos</option>
                    </select></td>
                <td>Formato:&nbsp;
                    <select name="formato">
                        <option value='0'>Texto</option>
                        <option value='1'>Excel</option>
                    </select>
                </td>
                <td><input type="submit" value="Buscar" class="boton2"/></td>
            </tr>
        </table>
    </form>
</center>
<?= $Gestor->Encabezado('K0024', 'p', '') ?>
</body>
</html>