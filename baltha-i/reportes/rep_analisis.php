<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _rep_analisis();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('k15', 'hr', 'Reportes :: An�lisis realizados') ?>
<?= $Gestor->Encabezado('K0015', 'e', 'An�lisis realizados') ?>
<center>
    <form action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>" method="post"
          target="_blank">
        <input type="hidden" name="lab" value="<?= $_POST['LID'] ?>"/>
        <table class="radius" align="center">
            <tr>
                <td class="titulo" colspan="5">Filtro</td>
            </tr>
            <tr>
                <td>Desde: <input type="text" id="desde" name="desde" class="fecha" readonly
                                  onClick="show_calendar(this.id);">&nbsp;
                    Hasta: <input type="text" id="hasta" name="hasta" class="fecha" readonly
                                  onClick="show_calendar(this.id);"></td>
                <td>An&aacute;lisis: <select name="analisis" style="width:130px">
                        <option value="-1">Todos</option>
                        <option value="-2">Todos Fertilizantes</option>
                        <option value="-3">Todos Plaguicidas</option>
                        <?php
                        $ROW = $Gestor->Analisis();
                        for ($x = 0, $anterior = ''; $x < count($ROW); $x++) {
                            if ($anterior != $ROW[$x]['tipo']) {
                                $anterior = $ROW[$x]['tipo'];
                                echo "<optgroup label='", $Gestor->Tipo($ROW[$x]['tipo']), "'></optgroup>";
                            }
                            ?>
                            <option value="<?= $ROW[$x]['id'] ?>"><?= $ROW[$x]['nombre'] ?></option>
                            <?php
                        }
                        ?>
                    </select></td>
                <td>Estado:&nbsp;
                    <select name="estado">
                        <option value="0">Generada</option>
                        <option value="1">Pendiente</option>
                        <option value="2">Aprobada</option>
                        <option value="5">Anulada</option>
                        <option value="7">Finalizada</option>
                        <option value="" selected="">Todos</option>
                    </select>
                </td>
                <td>Formato:&nbsp;
                    <select name="formato">
                        <option value='0'>Texto</option>
                        <option value='1'>Excel</option>
                    </select>
                </td>
                <td><input type="button" value="Buscar" class="boton2" onclick="SolicitudesBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <a href="#" onclick="location.href='menu.php'">[Atr&aacute;s]</a>
</center>
<?= $Gestor->Encabezado('K0015', 'p', '') ?>
</body>
</html>