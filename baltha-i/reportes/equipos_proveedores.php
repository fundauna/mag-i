<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _equipos_proveedores();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('k27', 'hr', 'Reportes :: Equipos, Proveedores') ?>
<?= $Gestor->Encabezado('K0027', 'e', 'Equipos, Proveedores') ?>
<center>
    <form action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>" method="post"
          target="_blank">
        <table class="radius" align="center" width="550px">
            <tr>
                <td class="titulo" colspan="5">Filtro</td>
            </tr>
            <tr>
                <td>C&oacute;digo del equipo:<br/>
                    <input type="text" name="codigo"/>
                </td>
                <td>Nombre del equipo:<br/>
                    <input type="text" name="nombre"/>
                </td>
                <td>Nombre del proveedor:<br/>
                    <input type="text" name="proveedor"/>
                </td>
                <td>Formato:<br/>
                    <select name="formato">
                        <option value='0'>Texto</option>
                        <option value='1'>Excel</option>
                    </select>
                </td>
                <td><br/><input type="button" value="Buscar" class="boton2" onclick="this.form.submit();"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <a href="#" onclick="location.href='menu.php'">[Atr&aacute;s]</a>
</center>
<?= $Gestor->Encabezado('K0027', 'p', '') ?>
</body>
</html>