<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _final01_detalle();
$ROW = $Gestor->Encabezado();
if (!$ROW) {
    die('Registro inexistente');
}
$_POST['ensayo'] = $ROW[0]['ensayo'];
$_estado = $ROW[0]['estado'];

//EN VEZ DE # DE MUESTRA SE PONE # DE LOTE MAS CONSECUTIVO
//list(,, $ROW[0]['muestra']) = explode('-', $ROW[0]['muestra']);
$ROW[0]['muestra'] = $ROW[0]['lote'] . '-' . $ROW[0]['muestra'];

//VERFICACION DE ACREDITACION
$acreditado1 = $ROW[0]['acreditado'];
$acreditado2 = false;

//FECHA DE LAS FIRMAS
$firma = $Gestor->Mes();

$QW = $Gestor->obtieneFirma();
$ER = $Gestor->obtieneFechas($ROW[0]['solicitud']);
if (!$ER) {
    $ER[0]['flab'] = $ER[0]['hlab'] = $ER[0]['fmuestreo'] = $ER[0]['hmuestreo'] = '-';
}


if ($_estado == '2' or $_estado == '5') {
    $disabled = 'disabled';
} else {
    $disabled = '';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body style="background-image:none;">
<input type="hidden" id="id" value="<?= $ROW[0]['cs'] ?>"/>
<input type="hidden" id="cliente" value="<?= $ROW[0]['id_cliente'] ?>"/>
<script>ALFA('Por favor espere....');</script>
<center>
    <table align="center" width="100%" border="1" bordercolor="#000000" cellpadding="0" cellspacing="0"
           style="font-size:12px">
        <tr align="center">
            <td colspan="2"><?php $Gestor->Incluir('mag', 'png'); ?></td>
            <td><strong>Ministerio de Agricultura y Ganader&iacute;a<br/>Servicio Fitosanitario del Estado<br/>Laboratorio
                    de An&aacute;lisis de Residuos de Agroqu&iacute;micos</strong><br/>Sabana sur, contiguo al edificio
                del MAG en "La Salle"<br/>Tel: 2549-3536
            </td>
            <td colspan="2"><?php $Gestor->Incluir('agro', 'png'); ?></td>
        </tr>
        <tr align="center">
            <td><strong>R-06-LAB-LRE-PO-01</strong></td>
            <td><strong>Rige a partir de</strong><br/>Setiembre 2014</td>
            <td><strong>Informe de Ensayos</strong></td>
            <td colspan="2"><strong>Versi&oacute;n:</strong> 03<br/><strong>Revisi&oacute;n:</strong> 01</td>
        </tr>
        <tr align="center">
            <td colspan="5">
                <!-- DATOS GENERALES -->
                <font style="font-size:14px; font-weight:bold;">INFORME DE ENSAYO <?= $ROW[0]['cs'] ?></font><br/>
                <table width="95%" style="font-size:11px">
                    <tr>
                        <td><strong>Cliente:</strong></td>
                        <td><?= $ROW[0]['cliente'] ?></td>
                        <td colspan="2"></td>
                        <td rowspan="9">
                            <img id="eca1" src="<?= $Gestor->Incluir('eca1', 'bkg') ?>" style="display:none"/>
                            <img id="eca0" src="<?= $Gestor->Incluir('eca0', 'bkg') ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Direcci&oacute;n:</strong></td>
                        <td colspan="3"><?= $ROW[0]['direccion'] ?></td>
                    </tr>
                    <tr>
                        <td><strong>Fecha solicitud:</strong></td>
                        <td><?= $ROW[0]['fecha_sol'] ?></td>
                        <td><strong>No. Solicitud:</strong></td>
                        <td><?= $ROW[0]['solicitud'] ?></td>
                    </tr>
                    <tr>
                        <td><strong>Solicitante:</strong></td>
                        <td colspan="3"><?= $ROW[0]['solicitante'] ?></td>
                    </tr>
                    <tr>
                        <td><strong>Fecha conclusi&oacute;n:</strong></td>
                        <td><?= $ROW[0]['fecha'] ?></td>
                        <td><strong>Fecha emisi&oacute;n:</strong></td>
                        <td><?= $ROW[0]['emision'] ?></td>
                    </tr>
                    <tr>
                        <td><strong>No. Lote:</strong></td>
                        <td><?= $ROW[0]['solicitud'] ?></td>
                        <td><strong>No. Muestra:</strong></td>
                        <td><?= $ROW[0]['muestra'] ?></td>
                    </tr>
                    <!--<tr>
                                <td><strong>Fecha de muestreo:</strong></td>
                                <td><?= $ER[0]['fmuestreo'] ?></td>
                                <td><strong>Hora de muestreo:</strong></td>
                                <td><?= $ER[0]['hmuestreo'] ?></td>
                            </tr>
                            <tr>
                                <td><strong>Fecha de recepci&oacute;n laboratorio:</strong></td>
                                <td><?= $ER[0]['flab'] ?></td>
                                <td><strong>Hora de recepci&oacute;n laboratorio:</strong></td>
                                <td><?= $ER[0]['hlab'] ?></td>
                            </tr>-->
                    <tr>
                        <td><strong>C&oacute;digo externo:</strong></td>
                        <td><?= $ROW[0]['codigo'] ?></td>
                        <td><strong>Item ensayado:</strong></td>
                        <td><?= $ROW[0]['matriz'] ?></td>
                    </tr>
                    <tr>
                        <td><strong>M&eacute;todo utilizado:</strong></td>
                        <?php if ($disabled != '') { ?>
                            <?= $ROW[0]['procedimiento'] ?>
                        <?php } else { ?>
                            <td colspan="3"><input type="text" id="proc" size="50"
                                                   value="<?= $ROW[0]['procedimiento'] ?>" <?= $disabled ?>/></td>
                        <?php } ?>
                    </tr>
                    <tr>
                        <td><strong>Observaciones:</strong></td>
                        <?php if ($disabled != '') { ?>
                            <?= $Gestor->obtieneFRF($ROW[0]['solicitud']) . ' ' . $ROW[0]['obs'] ?>
                        <?php } else { ?>
                            <td colspan="3"><textarea id="obs" style="height:40px; width:50%"
                                                      <?= $disabled ?>><?= $ROW[0]['obs'] ?></textarea></td>
                        <?php } ?>
                    </tr>
                </table>
                <!-- DATOS GENERALES -->
            </td>
        </tr>
        <tr>
            <td colspan="5"><br/>
                <!-- NOTAS -->
                <table width="100%">
                    <td>
                        <font style="font-size:14px; font-weight:bold;">
                            &nbsp;1) Los analitos marcados con *, se encuentran acreditados. Ver alcance en
                            www.eca.or.cr<br/>
                            &nbsp;2) Los analitos sin marca o sin *, no se encuentran dentro del alcance.
                        </font><br/>
                        <font style="font-size:11px;">
                            &nbsp;3) LC = L&iacute;mite de Cuantificaci&oacute;n; LD= L&iacute;mite de Detecci&oacute;n;
                            NA = No Aplica; ND = No Detectable; NC = No Cuantificable.<br/>
                            &nbsp;4) La unidad de medida utilizada en los resultados es mg/kg.<br/>
                            &nbsp;5) Este informe se emite a solicitud del cliente con el prop&oacute;sito de detectar
                            residuos. Queda prohibida su reproducci&oacute;n parcial o total.<br/>
                            &nbsp;6) El laboratorio no realiza el muestreo del producto. Los resultados corresponden
                            &uacute;nicamente a los items ensayados.<br/>
                            &nbsp;7) Para que el informe se considere v&aacute;lido debe contar con el sello del
                            laboratorio y con el nombre y firma del qu&iacute;mico responsable.<br/>
                            &nbsp;8) El tiempo m&aacute;ximo para la realizaci&oacute;n de reclamos es de 15 d&iacute;as
                            naturales, una vez recibido el informe por el cliente.<br/>
                            &nbsp;9) La incertidumbre de la medici&oacute;n se expresa como incertidumbre expandida con
                            un coeficiente de cobertura k=2 para un nivel de confianza de 95 %.<br>
                            &nbsp;10) Los resultados o valores de la concentraci&oacute;n se expresan con cifras
                            significativas, a saber: <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Concentraciones &le; 0,1 mg/kg una cifra significativa y
                            concentraciones > 0,1 mg/kg dos cifras significativas.
                            </br>
                        </font>
                    </td>
                    <td align="center" style="font-size:11px;border:none"></td>
                </table>
                <!-- NOTAS -->
            </td>
        </tr>

        <tr align="center">
            <td colspan="5">
                <!-- RESULTADOS -->
                <table width="100%" style="font-size:11px" cellpadding="0" cellspacing="0" border="1">
                    <tr align="center">
                        <td><strong>Plaquicida</strong></td>
                        <td><strong>&nbsp;Concentraci&oacute;n&nbsp;<br/>(mg/kg)</strong></td>
                        <td><strong>&nbsp;Incertidumbre&nbsp;<br/>(mg/kg)</strong></td>
                        <td><strong>Plaquicida</strong></td>
                        <td><strong>&nbsp;Concentraci&oacute;n&nbsp;<br/>(mg/kg)</strong></td>
                        <td><strong>&nbsp;Incertidumbre&nbsp;<br/>(mg/kg)</strong></td>
                        <td><strong>Plaquicida</strong></td>
                        <td><strong>&nbsp;Concentraci&oacute;n&nbsp;<br/>(mg/kg)</strong></td>
                        <td><strong>&nbsp;Incertidumbre&nbsp;<br/>(mg/kg)</strong></td>
                    </tr>
                    <?php
                    $ROW = $Gestor->Resultados();
                    for ($x = 0; $x < count($ROW); $x += 3) {
                        if (isset($ROW[$x]['analito'])) {
                            if ($ROW[$x]['conc'] == '-1') {
                                $ROW[$x]['conc'] = 'ND';
                                $ROW[$x]['inc'] = 'NA';
                            } elseif ($ROW[$x]['conc'] == '-2') {
                                $ROW[$x]['conc'] = 'NC';
                                $ROW[$x]['inc'] = 'NA';
                            } else {
                                $ROW[$x]['inc'] = $ROW[$x]['conc'] / 2;
                            }

                            if ($Gestor->Asterisco($ROW[$x]['analito'])) {
                                $acreditado2 = true;
                            }
                        } else {
                            $ROW[$x]['analito'] = $ROW[$x]['conc'] = $ROW[$x]['inc'] = '-----';
                        }
                        //+1
                        if (isset($ROW[$x + 1]['analito'])) {
                            if ($ROW[$x + 1]['conc'] == '-1') {
                                $ROW[$x + 1]['conc'] = 'ND';
                                $ROW[$x + 1]['inc'] = 'NA';
                            } elseif ($ROW[$x + 1]['conc'] == '-2') {
                                $ROW[$x + 1]['conc'] = 'NC';
                                $ROW[$x + 1]['inc'] = 'NA';
                            } else {
                                $ROW[$x + 1]['inc'] = $ROW[$x + 1]['conc'] / 2;
                            }

                            if ($Gestor->Asterisco($ROW[$x + 1]['analito'])) {
                                $acreditado2 = true;
                            }
                        } else {
                            $ROW[$x + 1]['analito'] = $ROW[$x + 1]['conc'] = $ROW[$x + 1]['inc'] = '-----';
                        }
                        //+2
                        if (isset($ROW[$x + 2]['analito'])) {
                            if ($ROW[$x + 2]['conc'] == '-1') {
                                $ROW[$x + 2]['conc'] = 'ND';
                                $ROW[$x + 2]['inc'] = 'NA';
                            } elseif ($ROW[$x + 2]['conc'] == '-2') {
                                $ROW[$x + 2]['conc'] = 'NC';
                                $ROW[$x + 2]['inc'] = 'NA';
                            } else {
                                $ROW[$x + 2]['inc'] = $ROW[$x + 2]['conc'] / 2;
                            }

                            if ($Gestor->Asterisco($ROW[$x + 2]['analito'])) {
                                $acreditado2 = true;
                            }
                        } else {
                            $ROW[$x + 2]['analito'] = $ROW[$x + 2]['conc'] = $ROW[$x + 2]['inc'] = '-----';
                        }
                        ?>
                        <tr align="center">
                            <td><?= $Gestor->Etiqueta(strtoupper($ROW[$x]['analito']), $acreditado1) ?></td>
                            <td><?= $Gestor->Formato($ROW[$x]['conc']) ?></td>
                            <td><?= $Gestor->Formato($ROW[$x]['inc'], 1) ?></td>
                            <td><?= $Gestor->Etiqueta(strtoupper($ROW[$x + 1]['analito']), $acreditado1) ?></td>
                            <td><?= $Gestor->Formato($ROW[$x + 1]['conc']) ?></td>
                            <td><?= $Gestor->Formato($ROW[$x + 1]['inc'], 1) ?></td>
                            <td><?= $Gestor->Etiqueta(strtoupper($ROW[$x + 2]['analito']), $acreditado1) ?></td>
                            <td><?= $Gestor->Formato($ROW[$x + 2]['conc']) ?></td>
                            <td><?= $Gestor->Formato($ROW[$x + 2]['inc'], 1) ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                <!-- RESULTADOS -->
            </td>
        </tr>
        <tr>
            <td colspan="5"><br/>
                <!-- NOTAS -->
                <table width="100%">
                    <td width="70%">
                    </td>
                    <td align="center" style="font-size:11px;border:none">
                        <br/><br/>_________________________<br/><?= $QW[0]['nombre'] ?><br/><?= $QW[0]['cargo'] ?></td>
                    <tr>
                        <td colspan="5" align="center">--------------------&Uacute;LTIMA L&Iacute;NEA--------------------</td>
                    </tr>
                </table>
                <!-- NOTAS -->
            </td>
        </tr>
        <tr style="font-size:10px; padding-left:2px">
            <td colspan="5">
                <table width="100%">
                    <tr>
                        <td colspan="3" align="center">RECIBIDO</td>
                    </tr>
                    <tr>
                        <td>
                            Guillermo Arrieta Quesada <input type="checkbox"><br>
                            Pedro S&aacute;nchez Carballo <input type="checkbox"><br>
                            Secretaria <input type="checkbox"><br>
                            Esa&uacute; Miranda Vargas <input type="checkbox"><br>
                            Otro <input type="checkbox">
                            ______________________________________________________________________________
                        </td>
                        <td></td>
                        <td align="center">Fecha: _______________________________ Hora:
                            ___________________</br></br></br></br></br>Firma: _________________________________________
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="font-size:10px; padding-left:2px">
            <!-- FIRMAS -->
            <td colspan="2">
                <table width="100%">
                    <tr>
                        <td>
                            <strong>Elaborado por:</strong>&nbsp;Gestor de Calidad Laboratorios<br/>
                            <strong>Nombre:</strong>&nbsp;Kattia Murillo Alfaro<br/>
                            <strong>Fecha:</strong>&nbsp;<?= $firma ?>
                        </td>
                        <td align="center"><img src="<?= $Gestor->Incluir('firma1', 'bkg') ?>"/></td>
                    </tr>
                </table>
            </td>
            <td>
                <table width="100%">
                    <tr>
                        <td>
                            <strong>Revisado por:</strong>&nbsp;Qu&iacute;mico<br/>
                            <strong>Nombre:</strong>&nbsp;Ver&oacute;nica Picado Pomar<br/>
                            <strong>Fecha:</strong>&nbsp;<?= $firma ?>
                        </td>
                        <td align="center"><img src="<?= $Gestor->Incluir('firma2', 'bkg') ?>"/></td>
                    </tr>
                </table>
            </td>
            <td colspan="2">
                <table width="100%">
                    <tr>
                        <td>
                            <strong>Aprobado por:</strong>&nbsp;Jefatura de Departamento<br/>
                            <strong>Nombre:</strong>&nbsp;German Carranza Castillo<br/>
                            <strong>Fecha:</strong>&nbsp;<?= $firma ?>
                        </td>
                        <td align="center"><img src="<?= $Gestor->Incluir('firma3', 'bkg') ?>"/></td>
                    </tr>
                </table>
            </td>
            <!-- FIRMAS -->
        </tr>
    </table>
    <br/>
    <?php if ($_estado == 't') { ?>
        <input type="button" value="Generar" class="boton" onClick="Convertir()">&nbsp;
    <?php } ?>
    <?php if ($_estado == '1' && $Gestor->Aprobar()) { ?>
        <input type="button" value="Aprobar" class="boton" onClick="Aprobar()">&nbsp;
        <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
    <?php } ?>
    <input type="button" value="Imprimir" class="boton2" onClick="window.print()">
    <input type="button" value="Eliminar" class="boton" onClick="Eliminarlo()">&nbsp;
    <br/>
</center>
<script>
    <?php if ($acreditado1 && $acreditado2) echo 'ActivaSello();'; ?>
    BETA();
</script>
</body>
</html>