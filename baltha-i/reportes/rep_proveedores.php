<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _rep_proveedores();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>Delfos</title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('k21', 'hr', 'Reportes :: Cat�logo de proveedores') ?>
<?= $Gestor->Encabezado('K0021', 'e', 'Cat�logo de proveedores') ?>
<center>
    <form action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>" method="post"
          target="_blank">
        <input type="hidden" name="lab" value="<?= $_POST['LID'] ?>"/>
        <table class="radius" align="center" style="font-size:12px;">
            <tr>
                <td class="titulo" colspan="4">Filtro</td>
            </tr>
            <tr>
                <td>Tipo:&nbsp;
                    <select name="tipo">
                        <option value=''>Todos</option>
                        <option value='0'>F&iacute;sicos</option>
                        <option value='1'>Jur&iacute;dicos</option>
                    </select>
                </td>
                <td>Clasificaci&oacute;n:&nbsp;
                    <select name="clasificacion">
                        <option value="-1">Todos</option>
                        <?php
                        $ROW = $Gestor->ProveedoresFiltro();
                        for ($x = 0; $x < count($ROW); $x++) {
                            ?>
                            <option value="<?= $ROW[$x]['cs'] ?>"><?= $ROW[$x]['nombre'] ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td>Formato:&nbsp;
                    <select name="formato">
                        <option value='0'>Texto</option>
                        <option value='1'>Excel</option>
                    </select>
                </td>
                <td><input type="submit" value="Buscar" class="boton2"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <a href="#" onclick="location.href='menu.php'">[Atr&aacute;s]</a>
</center>
<?= $Gestor->Encabezado('K0021', 'p', '') ?>
</body>
</html>