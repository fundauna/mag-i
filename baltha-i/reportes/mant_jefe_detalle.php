<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _mant_jefe_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) {
    die('Registro inexistente');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <script>
        img1 = '<?php $Gestor->Incluir('closed', 'bkg') ?>';
        img2 = '<?php $Gestor->Incluir('downboxed', 'bkg') ?>';
    </script>
</head>
<body>
<?php $Gestor->Incluir('f13', 'hr', 'Inventario :: Mantenimiento para firma de jefe') ?>
<?= $Gestor->Encabezado('F0013', 'e', 'Mantenimiento para firma de jefe') ?>
<center>
    <input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
    <input type="hidden" id="id" value="<?= $ROW[0]['id'] ?>"/>
    <table class="radius" width="95%">
        <tr>
            <td class="titulo" colspan="2">Datos Generales</td>
        </tr>
        <tr>
            <td>Nombre:</td>
            <td><input type="text" id="nombre" value="<?= $ROW[0]['nombre'] ?>"></td>
        </tr>
        <tr>
            <td>Cargo:</td>
            <td><input type="text" id="cargo" value="<?= $ROW[0]['cargo'] ?>"></td>
        </tr>
        <tr>
            <td>Estado:</td>
            <td>
                <select id="estado">
                    <option value="1" <?= $ROW[0]['estado'] == '1' ? 'selected' : '' ?>>Activo</option>
                    <option value="0" <?= $ROW[0]['estado'] == '0' ? 'selected' : '' ?>>Inactivo</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Observaciones:</td>
            <td><textarea id="obs"><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
    </table>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
</center>
<?= $Gestor->Encabezado('F0013', 'p', '') ?>
</body>
</html>