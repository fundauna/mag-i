<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _mant_jefe();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('f13', 'hr', 'Reportes :: Mantenimiento para firma de jefe') ?>
<?= $Gestor->Encabezado('F0013', 'e', 'Mantenimiento para firma de jefe') ?>
<center>
    <br/>
    <div id="container" style="width:550px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Cargo</th>
                <th>Estado</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->InventarioMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#" onclick="InventarioModifica('<?= $ROW[$x]['id'] ?>')"><?= $ROW[$x]['nombre'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['cargo'] ?></td>
                    <td><?= $ROW[$x]['estado'] == 1 ? 'Activo' : 'Inactivo' ?></td>
                    <td>
                        <img onclick="InventarioModifica('<?= $ROW[$x]['id'] ?>')"
                             src="<?php $Gestor->Incluir('previa', 'bkg') ?>" title="Ver detalle" class="tab3"/>&nbsp;
                        <img onclick="InventarioElimina('<?= $ROW[$x]['id'] ?>')"
                             src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar" class="tab2"/>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/><input type="button" value="Agregar" class="boton" onClick="InventarioAgrega();">
</center>
<?= $Gestor->Encabezado('F0013', 'p', '') ?>
</body>
</html>