<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _final_anexar();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('estilo', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('k7', 'hr', 'Reportes :: Anexar informe de resultados') ?>
<?= $Gestor->Encabezado('K0007', 'e', 'Anexar informe de resultados') ?>
<center>
    <form name="form" method="post" enctype="multipart/form-data"
          action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
        <input type="hidden" name="id" value="<?= $_GET['ID'] ?>"/>
        <table class="radius">
            <tr>
                <td class="titulo">Anexar archivo f&iacute;sico</td>
            </tr>
            <tr>
                <td><strong>Informe:</strong>&nbsp;<?= $_GET['ID'] ?></td>
            </tr>
            <tr>
                <td><input type="file" name="archivo" id="archivo"/></td>
            </tr>
            <tr>
                <td align="center"><input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()"></td>
            </tr>
        </table>
    </form>
</center>
<?= $Gestor->Encabezado('K0007', 'p', '') ?>
</body>
</html>