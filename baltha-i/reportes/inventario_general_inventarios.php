<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _inventario_vencimientos();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('k11', 'hr', 'Reportes :: Inventario, Registro General de Inventarios') ?>
<?= $Gestor->Encabezado('K0011', 'e', 'Inventario, Registro General de Inventarios') ?>
<center>
    <form action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>" method="post"
          target="_blank">
        <table class="radius" align="center">
            <tr>
                <td class="titulo" colspan="3">Filtro</td>
            </tr>
            <tr>
                <td>Formato:&nbsp;
                    <select name="formato">
                        <option value='0'>Texto</option>
                        <option value='1'>Excel</option>
                    </select>
                </td>
                <td><input type="button" value="Buscar" class="boton2" onclick="this.form.submit();"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <a href="#" onclick="location.href='menu.php'">[Atr&aacute;s]</a>
</center>
<?= $Gestor->Encabezado('K0011', 'p', '') ?>
</body>
</html>