<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _personal_capacitacionTotal();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('k14', 'hr', 'Reportes :: Capacitaciónes por funcionario') ?>
<?= $Gestor->Encabezado('K0014', 'e', 'Capacitaciónes por funcionario') ?>
<center>
    <form action="graficador.php" method="post" target="_blank">
        <input type="hidden" name="_TIPO" value="<?= $_GET['tipo'] ?>"/>
        <table class="radius" align="center">
            <tr>
                <td class="titulo" colspan="4">Filtro</td>
            </tr>
            <tr>
                <td><strong>Unidad:</strong> <select name="unidad">
                        <option value='1'>LRE</option>
                        <option value='2'>LDP</option>
                        <option value='3'>LCC</option>
                        <option value=''>General</option>
                    </select>
                </td>
                <td>
                    <?php if ($_GET['tipo'] == '1') { ?><strong>Tipo:</strong> <select name="tipo">
                        <option value="S">SGC</option>
                        <option value="T">T&eacute;cnica</option>
                        <option value="O">Otras</option>
                        <option value="I">Internacionales</option>
                    </select>
                    <?php } ?>
                </td>
                <td><strong>Desde:</strong> <input type="text" id="desde" name="desde" class="fecha" readonly
                                                   onClick="show_calendar(this.id);"></td>
                <td><input type="button" value="Buscar" class="boton2" onclick="Generar(this)"/></td>
            </tr>
        </table>
    </form>
</center>
<?= $Gestor->Encabezado('K0014', 'p', '') ?>
</body>
</html>