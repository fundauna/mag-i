<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _rep_informes();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('k29', 'hr', 'Reportes :: Historial Informes de Resultados') ?>
<?= $Gestor->Encabezado('K0029', 'e', 'Historial Informes de Resultados') ?>
<center>
    <form action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>" method="post"
          target="_blank">
        <table class="radius" align="center">
            <tr>
                <td class="titulo" colspan="8">Filtro</td>
            </tr>
            <tr>
                <td>Buscar por:<br/>
                    <select id="tipo" name="tipo">
                        <option value="0">No. de Informe</option>
                        <option value="1">No. de Solicitud</option>
                        <option value="2">No. de Muestra</option>
                        <option value="4">No. de Lote</option>
                        <option value="3">No. de Registro</option>
                        <option value="5">An&aacute;lisis</option>
                    </select>
                </td>
                <td>Valor:<br/>
                    <input type="text" id="valor" name="valor" size="15" maxlength="20"/>
                </td>
                <td>Fecha:<br/>
                    <input type="text" id="desde" name="desde" class="fecha" readonly onClick="show_calendar(this.id);">&nbsp;
                    <input type="text" id="hasta" name="hasta" class="fecha" readonly onClick="show_calendar(this.id);">
                </td>
                <?php if ($_POST['LAB'] == '3') { ?>
                    <td>Tipo:</td>
                    <td><select name="tipo" style="width:90px">
                            <option value="">Todas</option>
                            <option value="1">Fertilizantes</option>
                            <option value="2">Plaguicidas</option>
                        </select></td>
                <?php } ?>
                <td>Estado:<br/>
                    <select name="estado">
                        <option value="">Todos</option>
                        <option value="t">Preliminar</option>
                        <option value="1">Generado</option>
                        <option value="2">Aprobado</option>
                        <option value="5">Anulado</option>
                    </select></td>
                <td>Formato:<br/>
                    <select name="formato">
                        <option value='0'>Texto</option>
                        <option value='1'>Excel</option>
                    </select>
                </td>
                <td><br/><input type="button" value="Buscar" class="boton2" onclick="SolicitudesBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <a href="#" onclick="location.href='menu.php'">[Atr&aacute;s]</a>
</center>
<?= $Gestor->Encabezado('K0029', 'p', '') ?>
</body>
</html>