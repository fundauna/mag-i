<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _final03_crear();
$ROW = $Gestor->GetEncabezado();
if (!$ROW) die('Registro inexistente');
/**/
if ($ROW[0]['tipo'] == '2') {
    header("location: final03P_crear.php?ID={$_GET['ID']}");
    exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="id" value="<?= $ROW[0]['cs'] ?>"/>
<input type="hidden" id="cliente" value="<?= $ROW[0]['id_cliente'] ?>"/>
<script>ALFA('Por favor espere....');</script>
<center>
    <?php $Gestor->Incluir('k29', 'hr', 'Informe de resultados :: Informe de Resultados de An&aacute;lisis de Fertilizantes') ?>
    <?= $Gestor->MEncabezado('K0029', 'e', 'Informe de Resultados de An&aacute;lisis de Fertilizantes') ?>
    <br/>
    <table class="radius" style="font-size:16px" width="98%">
        <tr align="center">
            <td>
                <table align="center" width="100%" border="1" bordercolor="#000000" cellpadding="0" cellspacing="0"
                       style="font-size:16px">
                    <tr align="center">
                        <td><strong>Informe de An&aacute;lisis No:</strong> <?= $ROW[0]['cs'] ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="98%" style="font-size:12px">
        <tr>
            <td class="titulo" colspan="6">Datos Generales</td>
        </tr>
        <tr>
            <td><strong>No. Solicitud:</strong></td>
            <td><?= $ROW[0]['solicitud'] ?></td>
            <td><strong>Dependencia:</strong></td>
            <td><?= $Gestor->Dependencia($ROW[0]['dependencia']) ?></td>
            <td><strong>Solicitante:</strong></td>
            <td><?= $ROW[0]['solicitante'] ?></td>

        </tr>
        <tr>
            <td><strong>Ingreso de la muestra:</strong></td>
            <td><?= $ROW[0]['fecha_sol'] ?></td>
            <td><strong>Conclusi&oacute;n del an&aacute;lisis:</strong></td>
            <td><?= $ROW[0]['conclusion'] ?></td>
            <td><strong>Reporte:</strong></td>
            <td><?= $ROW[0]['fecha'] ?></td>
        </tr>
        <tr>
            <td><strong>Nombre comercial del producto:</strong></td>
            <td><?= $ROW[0]['producto'] ?></td>
            <td><strong>Tipo de formulaci&oacute;n:</strong></td>
            <td><?= $ROW[0]['tipo_form'] ?></td>
            <td><strong>No. Registro:</strong></td>
            <td><?= $ROW[0]['registro'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td><strong>Observaciones:</strong></td>
            <td colspan="4"><textarea style="height:40px; width:70%" id="obs"></textarea></td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="98%" style="font-size:12px">
        <?php
        $ROW2 = $Gestor->SolicitudMuestras($ROW[0]['solicitud']);
        for ($x = 0, $linea = 0; $x < count($ROW2); $x++) {
            $linea++;
            $_POST['densidad'] = 0;
            $tmp = str_replace('LCC-', '', $ROW[0]['solicitud']) . '-' . ($x + 1) . '-' . $_POST['tipo'];
            $cod_int = '3-' . str_replace(' ', '', $tmp);
            ?>
            <tr style="background-color:#CCCCCC">
                <td><img onclick="ObsMuestra(<?= $linea ?>)" src="<?php $Gestor->Incluir('closed', 'bkg') ?>"
                         title="Agregar Observaciones" class="tab"/>&nbsp;<strong>No. Muestra:</strong> <?= $tmp ?>
                    <input type="hidden" id="muestra" name="muestra" value="<?= $cod_int ?>"/></td>
                <td style="background-color:#CCCCCC">Cinta: <?= $ROW2[$x]['cod_ext'] ?></td>
                <td>Sellada: <?= $ROW2[$x]['sellado'] == '1' ? 'S�' : 'No' ?></td>
                <td>Lote: <?= $ROW2[$x]['lote'] ?></td>
                <td colspan="2">Recipiente: <?= $Gestor->Recipiente($ROW2[$x]['recipiente']) ?></td>
            </tr>
            <tr id="tr_<?= $linea ?>" style="display:none">
                <td colspan="6">Observaciones:&nbsp;<input type="text" id="obs<?= $linea ?>" name="obs"
                                                           style="width:60%" maxlength="200"/></td>
            </tr>
            <?php
            //Analisis Quimicos
            $ROW3 = $Gestor->Resultados($cod_int, 0);
            if ($ROW3) {
                echo '<tr><td colspan="6"><strong>An�lisis Qu�micos RTCR 228</strong></td></tr><tr style="text-decoration:underline"><td>Elemento</td><td align="center">C.D.</td><td align="center">C.E.</td><td align="center">I.C.</td><td align="center">U.C</td><td>M&eacute;todo</td></tr>';
                for ($y = 0; $y < count($ROW3); $y++) {
                    $linea++;
                    list($res1, $res2) = explode(',', $ROW3[$y]['resultado']);
                    $tildes = array(' ', 'Cn=', 'IC=', 'CE=', 'CP=');
                    $solas = array('', '', '', '', '');
                    $res1 = str_replace($tildes, $solas, $res1);
                    $res2 = str_replace($tildes, $solas, $res2);
                    /*if($res1==0){
                        $res1 = 'N.D.';
                        $res2 = 'N.A.';
                    }*/
                    ?>
                    <tr>
                        <td><img onclick="ObsMuestra(<?= $linea ?>)" src="<?php $Gestor->Incluir('closed', 'bkg') ?>"
                                 title="Agregar Observaciones" class="tab"/>&nbsp;<?= $ROW3[$y]['elemento'] ?></td>
                        <td align="center"><?= $ROW3[$y]['declarada'] ?></td>
                        <td align="center"><?= $res1 ?></td>
                        <td align="center"><?= $res2 ?></td>
                        <td align="center"><?= $Gestor->Unidad($ROW3[$y]['unidad']) ?></td>
                        <td>
                            <?= $Gestor->Combo(0) ?>
                            <input placeholder="Indique" type="text" name="metodootro" value="" hidden=""/>
                        </td>
                    </tr>
                    <tr id="tr_<?= $linea ?>" style="display:none">
                        <td colspan="6">Observaciones:&nbsp;<input type="text" id="obs<?= $linea ?>" name="obs"
                                                                   style="width:60%" maxlength="200"/></td>
                    </tr>
                    <?php
                }//FOR 2
                if ($_POST['densidad'] == 1) {
                    echo "<tr align='center'>
				<td></td>
				<td style='text-decoration:underline'>Densidad</td>
				<td style='text-decoration:underline'>Incertidumbre</td>
				<td colspan='3'></td>
			</tr>
			<tr align='center'>
				<td></td>
				<td>{$ROW3[0]['densidad']} g/mL</td>
				<td>{$ROW3[0]['incertidumbre']}</td>
				<td colspan='3'></td>
			</tr>";
                }
            }//IF
            //Impurezas
            $ROW3 = $Gestor->Resultados($cod_int, 2);
            if ($ROW3) {
                echo '<tr><td colspan="6"><strong>Impurezas</strong></td></tr><tr style="text-decoration:underline"><td>Impureza</td><td></td><td align="center">C.E.</td><td align="center">I.C.</td><td align="center">U.C</td><td>M&eacute;todo</td></tr>';
                for ($y = 0; $y < count($ROW3); $y++) {
                    $linea++;
                    list($res1, $res2) = explode(',', $ROW3[$y]['resultado']);
                    $tildes = array(' ', 'Cn=', 'IC=', 'CE=', 'CP=');
                    $solas = array('', '', '', '', '');
                    $res1 = str_replace($tildes, $solas, $res1);
                    $res2 = str_replace($tildes, $solas, $res2);
                    if ($res1 == 0) {
                        $res1 = 'N.D.';
                        $res2 = 'N.A.';
                    } else {
                        $res1 *= 1;
                        $res2 *= 1;
                    }
                    ?>
                    <tr>
                        <td><img onclick="ObsMuestra(<?= $linea ?>)" src="<?php $Gestor->Incluir('closed', 'bkg') ?>"
                                 title="Agregar Observaciones" class="tab"/>&nbsp;<?= $ROW3[$y]['elemento'] ?></td>
                        <td align="center"></td>
                        <td align="center"><?= $res1 ?></td>
                        <td align="center"><?= $res2 ?></td>
                        <td align="center"><?= $Gestor->Unidad($ROW3[$y]['unidad']) ?></td>
                        <td>
                            <?= $Gestor->Combo(2) ?>
                            <input placeholder="Indique" type="text" name="metodootro" value="" hidden=""/>
                        </td>
                    </tr>
                    <tr id="tr_<?= $linea ?>" style="display:none">
                        <td colspan="6">Observaciones:&nbsp;<input type="text" id="obs<?= $linea ?>" name="obs"
                                                                   style="width:60%" maxlength="200"/></td>
                    </tr>
                    <?php
                }//FOR 2
            }//IF
        }//FOR 1
        ?>
    </table>
    <br/>
    <table class="radius" style="font-size:12px">
        <tr>
            <td class="titulo" colspan="4">Equipos utilizados&nbsp;<img onclick="LineasMas()"
                                                                        src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                                                                        title="Agregar L�nea" class="tab"/></td>
        </tr>
        <tr>
            <td>#</td>
            <td>C&oacute;digo</td>
            <td>Analista</td>
            <td>Horas utilizadas</td>
        </tr>
        <tbody id="lolo">
        <tr>
            <td>1.</td>
            <td><input type="text" id="tmp_equipo0" name="tmp_equipo" class="lista2" readonly
                       onclick="EquiposLista(0)"/>
                <input type="hidden" id="equipo0" name="equipo"/>
            </td>
            <td><input type="text" id="tmp_usuario0" name="tmp_usuario" class="lista2" readonly
                       onclick="UsuariosLista(0)"/>
                <input type="hidden" id="usuario0" name="usuario"/>
            </td>
            <td><input type="text" name="horas" class="monto2" onblur="_FLOAT(this)"/></td>
        </tr>
        </tbody>
    </table>
    <br/>
    <input type="button" value="Generar" class="boton" onClick="Convertir()">&nbsp;
    <input type="button" value="Eliminar" class="boton" onClick="Eliminar()">&nbsp;
    <input type="button" value="Imprimir" class="boton2" onClick="window.print()">
</center>
<script>BETA();</script>
<?= $Gestor->MEncabezado('K0029', 'p', '') ?>
</body>
</html>