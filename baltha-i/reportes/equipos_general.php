<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _equipos_general();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('k6', 'hr', 'Reportes :: Equipos, Listado General') ?>
<?= $Gestor->Encabezado('K0006', 'e', 'Equipos, Listado General') ?>
<center>
    <form action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>" method="post"
          target="_blank">
        <table class="radius" align="center" width="550px">
            <tr>
                <td class="titulo" colspan="5">Filtro</td>
            </tr>
            <tr>
                <td>Nombre:<br/>
                    <input type="text" name="nombre"/>
                </td>
                <td>Sujeto a control metrol&oacute;gico:<br/>
                    <select name="control_metro">
                        <option value="-1">Todos</option>
                        <option value="0">No</option>
                        <option value="1">S&iacute;</option>
                    </select>
                </td>
                <td>Estado:<br/>
                    <select name="estado">
                        <option value="">Todos</option>
                        <option value="1">Habilitado</option>
                        <option value="0">Deshabilitado</option>
                        <option value="2">Almacenado</option>
                    </select>
                </td>
                <td>Formato:<br/>
                    <select name="formato">
                        <option value='0'>Texto</option>
                        <option value='1'>Excel</option>
                    </select>
                </td>
                <td><br/><input type="button" value="Buscar" class="boton2" onclick="this.form.submit();"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <a href="#" onclick="location.href='menu.php'">[Atr&aacute;s]</a>
</center>
<?= $Gestor->Encabezado('K0006', 'p', '') ?>
</body>
</html>