<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _rep_quejas();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('k22', 'hr', 'Reportes :: Quejas') ?>
<?= $Gestor->Encabezado('K0022', 'e', 'Quejas') ?>
<center>
    <form action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>" method="post"
          target="_blank">
        <table class="radius" align="center">
            <tr>
                <td class="titulo" colspan="6">Filtro</td>
            </tr>
            <tr>
                <td><strong>Desde:</strong> <input type="text" id="desde" name="desde" class="fecha" readonly
                                                   onClick="show_calendar(this.id);"></td>
                <td><strong>Hasta:</strong> <input type="text" id="hasta" name="hasta" class="fecha" readonly
                                                   onClick="show_calendar(this.id);"></td>
                <td><strong>Instancia:</strong> <select name="instancia">
                        <option value="">Todas</option>
                        <option value="0">No Procede</option>
                        <option value="1">LRE</option>
                        <option value="2">LDP</option>
                        <option value="3">LCC</option>
                        <option value="4">Jefatura</option>
                    </select></td>
                <td><strong>Estado:</strong> <select name="estado">
                        <option value="">Todas</option>
                        <option value="0">Cerradas</option>
                        <option value="1">Abiertas</option>
                    </select></td>
                <td>Formato:&nbsp;
                    <select name="formato">
                        <option value='0'>Texto</option>
                        <option value='1'>Excel</option>
                    </select>
                </td>
                <td><input type="button" value="Buscar" class="boton2" onclick="Generar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <a href="#" onclick="location.href='menu.php'">[Atr&aacute;s]</a>
</center>
<?= $Gestor->Encabezado('K0022', 'p', '') ?>
</body>
</html>