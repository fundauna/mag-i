<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _final01_detalle();
$ROW = $Gestor->Encabezado();
if (!$ROW) {
    die('Registro inexistente');
}
$_POST['ensayo'] = $ROW[0]['ensayo'];
$_estado = $ROW[0]['estado'];


$_matriz = $ROW[0]['matriz'];
$_muestra = $ROW[0]['muestra'];


//VERFICACION DE ACREDITACION
$acreditado1 = $ROW[0]['acreditado'];
$acreditado2 = false;


//FECHA DE LAS FIRMAS
$firma = $Gestor->Mes();
$QW = $Gestor->obtieneFirma();

$fir = $Gestor->Firmado($ROW[0]['cs']);

$tip = $Gestor->obtieneTipo($ROW[0]['solicitud']);

$ER = $Gestor->obtieneFechas($ROW[0]['solicitud']);
if (!$ER) {
    $ER[0]['flab'] = $ER[0]['hlab'] = $ER[0]['fmuestreo'] = $ER[0]['hmuestreo'] = '-';
}

$FM = $Gestor->obtieneFechasMuestreo($ROW[0]['solicitud']);
if (!$FM) {
    $FM[0]['fmuestra'] = $FM[0]['hmuestra'] = '-';
}
$ob = $Gestor->obtieneObservacion($ROW[0]['cs']);


if ($_estado == '2' or $_estado == '5') {
    $disabled = 'disabled';
} else {
    $disabled = '';
}

if ($_estado == 's'){
  $csS=  $ROW[0]['cs']. '- 2';
}




?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/estilo.css'>
</head>
<body style="background-image:none;">
<input type="hidden" id="id" name="id" value="<?= $ROW[0]['cs'] ?>"/>
<input type="hidden" id="cliente" value="<?= $ROW[0]['id_cliente'] ?>"/>
<script>ALFA('Por favor espere....');</script>
<tr align="center">

    <center>
        <?php $Gestor->Incluir('k29', 'hr', 'Informe de resultados :: Informe de Ensayos') ?>
        <!-- <header>-->
        <?= $Gestor->MEncabezado('K0029', 'e', 'Informe de Ensayos') ?>
        <!-- </header>-->
        <br/>
        <table align="center" width="98%" border="0" bordercolor="#000000" cellpadding="0" cellspacing="0"
               style="font-size:12px" rules="none">
            <tr align="center">
                <td colspan="5">
                    <!-- DATOS GENERALES -->
                    <font style="font-size:13px; font-weight:bold;">INFORME DE
                        ENSAYO
                        <?php
                        if($_estado == 's'){
                           $csS= $ROW[0]['cs'] . " - 2";
                        } else {
                            $csS=  $ROW[0]['cs'];
                        }

                        ?>

                        <?= $csS ?></font><br/><br/>
                    <table width="98%" style="font-size:10px" rules="none" border="0" align="left">
                        <tr>
                            <td width="13%" align="left"><strong>Cliente:</strong></td>
                            <td width="35%" align="left"><?= $ROW[0]['cliente'] ?></td>
                            <td width="25%" colspan="5" align="left"></td>
                            <td rowspan="8" align="left">
                                <img align="right" id="eca1" src="<?= $Gestor->Incluir('eca1', 'bkg') ?>"
                                     style="display:none"/>
                                <img id="eca0" src="<?= $Gestor->Incluir('eca0', 'bkg') ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td align="left"><strong>Direcci&oacute;n:</strong></td>
                            <td align="left" colspan="4" align="left"><?= $ROW[0]['direccion'] ?></td>
                        </tr>
                        <tr>
                            <td align="left"><strong>Fecha muestreo:</strong></td>
                            <td colspan="4" width="25%"
                                align="left"><?= $FM[0]['fmuestra'] . ' ' . $FM[0]['hmuestra'] . ':00' ?></td>
                        </tr>
                        <tr>
                            <td align="left"><strong>Fecha solicitud:</strong></td>
                            <td align="left"><?= date('d-m-Y H:i:s', strtotime($ROW[0]['fecha_sol'])) ?></td>
                            <td align="left" width="15%"><strong>N&#176 Solicitud:</strong></td>
                            <td align="left"><?= $ROW[0]['solicitud'] ?></td>
                        </tr>
                        <tr>
                            <td align="left"><strong>Solicitante:</strong></td>
                            <td colspan="4" align="left"><?= $ROW[0]['solicitante'] ?></td>
                        </tr>
                        <tr>
                            <td align="left"><strong>Fecha conclusi&oacute;n:</strong></td>
                            <td colspan="1" align="left"><?= date('d-m-Y H:i:s', strtotime($ROW[0]['fecha'])) ?></td>
                            <td align="left"><strong>Fecha emisi&oacute;n:</strong></td>
                            <td colspan="1" align="left"><?= date('d-m-Y H:i:s', strtotime($ROW[0]['emision'])) ?></td>
                        </tr>
                        <tr>
                            <td align="left"><strong>N&#176 Lote:</strong></td>
                            <td colspan="1" align="left"><?= $cod_int = str_replace('-0', '-', $ROW[0]['solicitud'])
                                /* $ROW[0]['solicitud']*/ ?></td>
                            <td align="left"><strong>N&#176 Muestra:</strong></td>
                            <td colspan="1" align="left"><?=

                                $mu = str_replace('--0', '', str_replace('-1 ', '-01', str_replace('-2 ', '-02', str_replace('-3 ', '-03', str_replace('-4 ', '-04', str_replace('-5 ', '-05', str_replace('-6 ', '-06', str_replace('-7 ', '-07',
                                    str_replace('-8 ', '-08', str_replace('-9 ', '-09', $ROW[0]['muestra']))))))))));
                                ?></td>
                        </tr>

                        <tr>
                            <td align="left"><strong>C&oacute;digo externo:</strong></td>
                            <td colspan="1" align="left"><?= $ROW[0]['codigo'] ?></td>
                            <td align="left"><strong>Item ensayado:</strong></td>
                            <td colspan="1" align="left"><?= $ROW[0]['matriz'] . ", " . $tip[0]['tipo'] ?></td>
                        </tr>

                        <tr>
                            <td align="left"><strong>M&eacute;todo utilizado:</strong></td>
                            <?php

                            if (($ROW[0]['matriz'] == 'agua') || ($ROW[0]['matriz'] == 'Agua') || ($ROW[0]['matriz'] == 'AGUA')) {
                                $met = 'M&eacute;todo de Referencia EPA (2012) 525.3 y EPA(2009)538';
                            } else if (($ROW[0]['matriz'] == 'suelo') || ($ROW[0]['matriz'] == 'Suelo') || ($ROW[0]['matriz'] == 'SUELO')) {
                                $met = 'M&eacute;todo de Referencia AOAC 2007.01';
                            } else {
                                $met = 'M&eacute;todo de Referencia AOAC 2007.01';
                            } ?>
                            <td colspan="4" align="left"><?= $met ?></td>
                        </tr>
                        <tr>
                            <td align="left"><strong>Observaciones:</strong></td>

                            <?php
                            $pres = $Gestor->obtieneFRF($ROW[0]['solicitud']);
                            $presentacion = '';

                            if ($pres[0]['presentacion'] == 0) {
                                $presentacion = 'congelada';
                            } elseif ($pres[0]['presentacion'] == 1) {
                                $presentacion = 'descongelada';
                            } elseif ($pres[0]['presentacion'] == 2) {
                                $presentacion = 'fresca, no refiregerada';
                            } elseif ($pres[0]['presentacion'] == 3) {
                                $presentacion = 'fresca, refiregerada';
                            } else {
                                $presentacion = '';
                            }
                            ?>

                            <td colspan="4" align="left"> La muestra <?= $mu ?> viene <?= $presentacion .".  ".$ob[0]['observaciones']  ?></td>
                        </tr>
                    </table>
                    <!-- DATOS GENERALES -->
                </td>
            </tr>
            <tr>
                <td colspan="5"><br/>
                    <!-- NOTAS -->
                    <div align="left" style="text-align: justify">
                        <font style="font-size:11px; font-weight:bold">
                            &nbsp;1) Los analitos marcados con *, se encuentran acreditados. Ver alcance en
                            www.eca.or.cr </font><br/>
                        <font style="font-size:11px; font-weight:bold">
                            &nbsp;2) Los analitos sin marca o sin *, no se encuentran dentro del alcance de
                            acreditaci&oacute;n.
                        </font><br/>
                        <font style="font-size:10px">
                            &nbsp;3) LC = L&iacute;mite de Cuantificaci&oacute;n; NA = No Aplica.
                        </font><br/>
                        <font style="font-size:10px">
                            &nbsp;4) CL EM/EM = Cromatograf&iacute;a l&iacute;quida con detector de masas; CG EM/EM =
                            Cromatograf&iacute;a de gases con detector de masas masas.</font><br/>
                        <font style="font-size:10px">
                            <?php if (($ROW[0]['matriz'] == 'agua') || ($ROW[0]['matriz'] == 'Agua') || ($ROW[0]['matriz'] == 'AGUA')) { ?>
                                &nbsp;5) La unidad de medida utilizada en los resultados es �g/L para aguas.
                            <?php } else if (($ROW[0]['matriz'] == 'suelo') || ($ROW[0]['matriz'] == 'Suelo') || ($ROW[0]['matriz'] == 'SUELO')) { ?>
                                &nbsp;5) La unidad de medida utilizada en los resultados es mg/kg para suelos.
                            <?php } else { ?>
                                &nbsp;5) La unidad de medida utilizada en los resultados es mg/kg para vegetales.
                            <?php } ?>
                        </font><br/>
                        <font style="font-size:10px">
                            &nbsp;6) Este informe se emite a solicitud del cliente con el prop&oacute;sito de
                            detectar residuos. Queda prohibida su reproducci&oacute;n parcial o total.</font><br/>
                        <font style="font-size:10px">
                            &nbsp;7) El laboratorio no realiza el muestreo del producto. Los resultados corresponden
                            &uacute;nicamente a los items ensayados, tal como los recibi&oacute; el
                            laboratorio</font><br/>
                        <font style="font-size:10px">
                            &nbsp;8) La informaci&oacute;n indicada en las casillas: cliente, direcci&oacute;n, fecha de
                            muestreo (incluida la hora), solicitante, c&oacute;digo externo es proporcionada por el
                            cliente.
                            El laboratorio no se hace responsable por errores asociados con esta
                            informaci&oacute;n que puedan afectar la validez de los resultados.</font><br/>
                        <font style="font-size:10px">
                            &nbsp;9) Para que el informe se considere v&aacute;lido debe contar con el sello del
                            laboratorio y con el nombre y firma del qu&iacute;mico responsable.
                            Los informes electr&oacute;nicos deben contar con la firma digital del
                            qu&iacute;mico</font><br/>
                        <font style="font-size:10px">
                            &nbsp;10) El tiempo m&aacute;ximo para la realizaci&oacute;n de reclamos es de 15 d&iacute;as
                            naturales, una vez recibido el informe por el cliente.</font><br/>
                        <font style="font-size:10px">
                            <?php if (($ROW[0]['matriz'] == 'agua') || ($ROW[0]['matriz'] == 'Agua') || ($ROW[0]['matriz'] == 'AGUA')) { ?>
                                &nbsp;11) La incertidumbre de medici&oacute;n corresponde a: Muestras Aguas un 34 % del valor de la concentraci&oacute;n reportada para los
                                plaguicidas obtenidos por la t&eacute;cnica de an&aacute;lisis CL EM/EM y un 49 % del valor de la concentraci&oacute;n
                                reportada para los plaguicidas obtenidos por la t&eacute;cnica de an&aacute;lisis CG EM/EM.

                            <?php } else if (($ROW[0]['matriz'] == 'suelo') || ($ROW[0]['matriz'] == 'Suelo') || ($ROW[0]['matriz'] == 'SUELO')) { ?>
                                &nbsp;11) La incertidumbre de medici&oacute;n corresponde a: Muestras Aguas un 63 % del valor de la concentraci&oacute;n reportada para los
                                plaguicidas obtenidos por la t&eacute;cnica de an&aacute;lisis CL EM/EM y un 63 % del valor de la concentraci&oacute;n
                                reportada para los plaguicidas obtenidos por la t&eacute;cnica de an&aacute;lisis CG EM/EM.

                            <?php } else { ?>
                                &nbsp;11) La incertidumbre de medici&oacute;n corresponde a: Muestras Vegetales un 50 % del valor
                                de la concentraci&oacute;n reportada para los plaguicidas obtenidos por la t&eacute;cnica de an&aacute;lisis CL EM/EM y un 50 % del valor
                                de la concentraci&oacute;n reportada para los plaguicidas obtenidos por la t&eacute;cnica de an&aacute;lisis CG EM/EM.
                            <?php } ?>
                        </font><br/>
                        <font style="font-size:10px">
                            &nbsp;12) La incertidumbre de la medici&oacute;n se expresa como incertidumbre expandida
                            con
                            un coeficiente de cobertura k=2 para un nivel de confianza del 95 %.</font><br/>
                        <font style="font-size:10px">
                            <?php if (($ROW[0]['matriz'] == 'agua') || ($ROW[0]['matriz'] == 'Agua') || ($ROW[0]['matriz'] == 'AGUA')) { ?>
                                &nbsp;13) Los resultados o valores de la concentraci&oacute;n se expresan con cifras
                                significativas, a saber: Concentraciones &le; 0,1  �g/L una cifra significativa y
                                concentraciones > 0,1 mg/l dos cifras significativas.

                            <?php } else if (($ROW[0]['matriz'] == 'suelo') || ($ROW[0]['matriz'] == 'Suelo') || ($ROW[0]['matriz'] == 'SUELO')) { ?>
                                &nbsp;13) Los resultados o valores de la concentraci&oacute;n se expresan con cifras
                                significativas, a saber: Concentraciones &le; 0,1 mg/kg una cifra significativa y
                                concentraciones > 0,1 mg/kg dos cifras significativas.

                            <?php } else { ?>
                                &nbsp;13) Los resultados o valores de la concentraci&oacute;n se expresan con cifras
                                significativas, a saber: Concentraciones &le; 0,1 mg/kg una cifra significativa y
                                concentraciones > 0,1 mg/kg dos cifras significativas.
                            <?php } ?>
                        </font><br/>
                        <font style="font-size:10px">
                            &nbsp;14) El resultado expresado como Fenvalerato corresponde a
                            Fenvalerato/Esfenvalerato.</font><br/>

                        </font>
                    </div>
                </td>
            </tr>

            <tr>
                <td colspan="5"> &nbsp;</td>
            </tr>
            <tr align="center">
                <td colspan="5">
                    <!-- RESULTADOS -->

            <tr>
                <td colspan="12" cellpadding="2" align="center" style="font-size: 16px; background: #999999"><strong>Plaguicidas
                        con
                        presencia de residuos
                    </strong></td>
            </tr>

            <?php

            $MGRP = $Gestor->obtieneGrupo($ROW[0]['matriz']);// devuelve el numero de grupo
            $AGRP = $Gestor->MAAsignados1($MGRP);// $AGRP devuelve la lista de analitos
            //devuelve analito y su concentracion
            $ROW = $Gestor->Resultados($MGRP);//devuelve lista cotejada de analitos


            //inician los arryas diferenciando cada analito
            $vecTabla = array();// arreglo para mostrar
            $vecCL = array();//arreglo para liquidos
            $vecCG = array();//arreglo para gases
            $vecDiferenciado = array();// arreglo para los diferenciados
            $vec = array();// municipal

            for ($x = 0; $x < count($ROW) - 1; $x += 2) {
                //carga array sencillos

                str_replace(' ', '', $ROW[$x]['analito']);


                /*  if (($ROW[$x]['conc'] == '-1') && ($ROW[$x]['tecnica'] == 1)) {
                      array_push($vecCL, $ROW[$x]['analito']);
                  } else if (($ROW[$x]['conc'] == '-1') && ($ROW[$x]['tecnica'] == 2)) {
                      array_push($vecCG, $ROW[$x]['analito']);
                  }*/


                if ($ROW[$x]['tecnica'] == 1) {
                    $typeD = 'CL EM/EM';
                } else if ($ROW[$x]['tecnica'] == 2) {
                    $typeD = 'CG EM/EM';
                } else if ($ROW[$x]['tecnica'] == '') {
                    $typeD = '';
                }


                if (($ROW[$x]['conc'] == '-1') && ($ROW[$x]['tecnica'] == 1)) {
                    if ($ROW[$x]['cuantificador'] != 0.01) {
                        array_push($vecDiferenciado, $ROW[$x]['analito'], $ROW[$x]['cuantificador'], $typeD);
                    } else {
                        array_push($vecCL, $ROW[$x]['analito']);
                    }
                } else if (($ROW[$x]['conc'] == '-1') && ($ROW[$x]['tecnica'] == 2)) {
                    if ($ROW[$x]['cuantificador'] != 0.01) {
                        array_push($vecDiferenciado, $ROW[$x]['analito'], $ROW[$x]['cuantificador'], $typeD);
                    } else {
                        array_push($vecCG, $ROW[$x]['analito']);
                    }
                }


                if (($ROW[$x]['analito']) && in_array(str_replace(' ', '', $ROW[$x]['analito']), $AGRP)) {
                    $ROW[$x]['conc'] = str_replace('.', ',', $ROW[$x]['conc']);
                    $ROW[$x]['cuantificador'] = str_replace('.', ',', $ROW[$x]['cuantificador']);
                    if ($ROW[$x]['conc'] == '-1') {
                        $ROW[$x]['conc'] = 'ND';
                    }
                    if ($Gestor->Asterisco($ROW[$x]['analito'])) {
                        $acreditado2 = true;
                    }
                    //define el tipo de tecnica
                    if ($ROW[$x]['tecnica'] == 1) {
                        $type = 'CL EM/EM';
                    } else if ($ROW[$x]['tecnica'] == 2) {
                        $type = 'CG EM/EM';
                    } else if ($ROW[$x]['tecnica'] == '') {
                        $type = '';
                    }
                    if ($ROW[$x]['conc'] != 'ND') {
                        array_push($vecTabla, $ROW[$x]['analito'], $ROW[$x]['conc'], $ROW[$x]['cuantificador'], $type);
                    }
                }//cierra rl row

                //carga array del + 1
                str_replace(' ', '', $ROW[$x + 1]['analito']);



                if (($ROW[$x + 1]['conc'] == '-1') && ($ROW[$x + 1]['tecnica'] == 1)) {
                    if ($ROW[$x + 1]['cuantificador'] != 0.01) {

                        array_push($vecDiferenciado, $ROW[$x + 1]['analito'], $ROW[$x + 1]['cuantificador'], $typeD);
                    } else {
                        array_push($vecCL, $ROW[$x + 1]['analito']);
                    }
                } else if (($ROW[$x + 1]['conc'] == '-1') && ($ROW[$x + 1]['tecnica'] == 2)) {
                    if ($ROW[$x + 1]['cuantificador'] != 0.01) {
                        array_push($vecDiferenciado, $ROW[$x + 1]['analito'], $ROW[$x + 1]['cuantificador'], $typeD);
                    } else {
                        array_push($vecCG, $ROW[$x + 1]['analito']);
                    }
                }


                if (($ROW[$x + 1]['analito']) && in_array(str_replace(' ', '', $ROW[$x + 1]['analito']), $AGRP)) {
                    $ROW[$x + 1]['cuantificador'] = str_replace('.', ',', $ROW[$x + 1]['cuantificador']);
                    $ROW[$x + 1]['conc'] = str_replace('.', ',', $ROW[$x + 1]['conc']);
                    if ($ROW[$x + 1]['conc'] == '-1') {
                        $ROW[$x + 1]['conc'] = 'ND';
                    }

                    if ($Gestor->Asterisco($ROW[$x + 1]['analito'])) {
                        $acreditado2 = true;
                    }
                    if ($ROW[$x + 1]['tecnica'] == 1) {
                        $type1 = 'CL EM/EM';
                    } else if ($ROW[$x + 1]['tecnica'] == 2) {
                        $type1 = 'CG EM/EM';
                    } else if ($ROW[$x + 1]['tecnica'] == '') {
                        $type1 = '';
                    }
                    if ($ROW[$x + 1]['conc'] != 'ND') {
                        array_push($vecTabla, $ROW[$x + 1]['analito'], $ROW[$x + 1]['conc'], $ROW[$x + 1]['cuantificador'], $type1);
                    }
                } //cierra row x +1
                ?>
                <?php
            }//cierra for ?>
            <?php
            //consultar el PA para resultados

            if (!empty($vecTabla)) { ?>
            <table id="res" class="table123" width="98%" style="font-size:11.5px" border="1" cellpadding="2">
                <tr align="center">
                    <td style="background: #aaaaaa" width="20%"><strong>Plaguicida</strong></td>
                    <td style="background: #aaaaaa">
                        <strong>&nbsp;Concentraci&oacute;n&nbsp;<br/>(mg/kg)</strong></td>
                    <td style="background: #aaaaaa" width="10%"><strong>&nbsp;LC&nbsp;<br/>(mg/kg)</strong></td>
                    <td style="background: #aaaaaa"><strong>T&eacute;cnica de An&aacute;lisis</strong></td>
                    <td style="background: #aaaaaa" width="20%"><strong>Plaguicida</strong></td>
                    <td style="background: #aaaaaa">
                        <strong>&nbsp;Concentraci&oacute;n&nbsp;<br/>(mg/kg)</strong></td>
                    <td style="background: #aaaaaa" width="10%"><strong>&nbsp;LC&nbsp;<br/>(mg/kg)</strong></td>
                    <td style="background: #aaaaaa"><strong>&nbsp;T&eacute;cnica de An&aacute;lisis&nbsp;</strong></td>
                </tr>
                <br/>


                    <?php
                    $dtResultado = $Gestor->ResultadosDeInforme($csS);

                    foreach ($dtResultado as $dato) {
                    ?>
                    <tr >
                        <td><?= $dato['analito1'] ?></td>
                        <td><?= $dato['conc1'] ?></td>
                        <td><?= $dato['tecnica1'] ?></td>
                        <td><?= $dato['cuantificador1'] ?></td>
                        <td><?= $dato['analito2'] ?></td>
                        <td><?= $dato['conc2'] ?></td>
                        <td><?= $dato['tecnica2'] ?></td>
                        <td><?= $dato['cuantificador2'] ?></td>
                    </tr>
                  <?php  
                }
             } else { ?>

                <table id="res" class="table123" width="98%" style="font-size:10px" border="1">
                    <tr align="center">
                        <br/>
                        <p align="left" style="padding-left: 25px"> No hay presencia de residuo</p>
                        <br/>

                        <?php }

                        ?>
                </table>
                <!-- RESULTADOS -->

                <br/>

                <table width="98%" cellpadding="2" cellspacing="0" border="0" style="border-color: #111111">
                    <tr>
                        <td colspan="12" align="center" style="font-size: 14px; background: #999999"><b>Plaguicidas
                                analizados y con resultado < LC</b>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" width="25%">T&eacute;cnicas de an&aacute;lisis:</td>
                        <td align="left" width="25%"><b>CL EM/EM</b></td>
                        <td align="left" width="25%"><b>LC&nbsp;(mg/kg): 0.01</b></td>
                        <td align="center"></b></td>
                    </tr>
                    <tr>
                        <td colspan="4"> &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="font-size:10px; text-align: justify" colspan="4" align="left"><?php
                            $ss = '';
                            $cl = '';
                            $count = count($vecCL);
                            for ($i = 0;
                                 $i < $count;
                                 $i++) {
                                $ss .= "  " . $vecCL[$i] . '    ;   ';
                                $cl = str_replace('   ', '', $ss);
                            }
                            if ($cl == 'null') {
                                echo '';
                            } else {
                                echo $cl;
                            }
                            ?>- &Uacute;ltimo
                        </td>
                    </tr>
                </table>
                <br/>
                <table width="98%" cellpadding="2" cellspacing="0" border="0" style="border-color: #111111">
                    <tr>
                        <td colspan="12" align="center" style="font-size: 14px; background: #999999"><b>Plaguicidas
                                analizados y con resultado < LC </b>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" width="25%">T&eacute;cnicas de an&aacute;lisis:</td>
                        <td align="left" width="25%"><b>CG EM/EM</b></td>
                        <td align="left" width="25%"><b>LC&nbsp;(mg/kg): 0.01</b></td>
                        <td align="center"></b></td>
                    </tr>
                    <tr>
                        <td colspan="4"> &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="font-size:10px; text-align: justify" colspan="4" align="left"><?php
                            $ss = '';
                            $cg = '';
                            $count = count($vecCG);
                            for ($i = 0;
                                 $i < $count;
                                 $i++) {
                                $ss .= "  " . $vecCG[$i] . ';  ';
                                $cg = str_replace('   ', '', $ss);
                            }
                            if (($cg == '')) {
                                echo '';
                            } else {
                                echo $cg;
                            }
                            ?>- &Uacute;ltimo
                        </td>
                    </tr>


                    <tr>
                        <td colspan="4"> &nbsp;</td>
                    </tr>

                    <tr>
                        <td colspan="12" align="center" style="font-size: 14px; background: #999999"><b>Plaguicidas
                                con Diferente LC&nbsp;(Plaguicida; LC; en mg/kg; t&eacute;cnica de an&aacute;lisis) y
                                con resultado < LC</b>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4"> &nbsp;</td>
                    </tr>

                    <tr>
                        <td style="font-size:10px; text-align: justify" colspan="4" align="left"><?php
                            $ss = '';
                            $c = '';
                            $count = count($vecDiferenciado);
                            for ($i = 0;
                                 $i < $count;
                                 $i++) {

                                if ($i % 3 == 0) {
                                    $ss .= " ( " . $vecDiferenciado[$i] . ' -  ';

                                } elseif ($i % 3 == 1) {
                                    $ss .= "  " . $vecDiferenciado[$i] . ' ; ';

                                } else {
                                    $ss .= "  " . $vecDiferenciado[$i] . ' )  -  ';
                                }
                                $c = $ss;
                            }
                            echo $c ?>  &Uacute;ltimo
                        </td>
                    </tr>
                </table>
                <br/>


                <tr>
                    <td colspan="4" align="center"><br/>
                        _______________________________________________________________
                        <br/>
                        <br/>
                        <?php
                        if ($_estado == 't') { ?>
                            ****  Debe generar el informe para poder asignarle una firma  *****
                            <br/>
                            <?php
                        } else if ($_estado == 1) { ?>

                            <select id="firmante">
                                <option value="">...</option>

                                <?php
                                $ROWF = $Gestor->Firmantes();
                                for ($x = 0;
                                     $x < count($ROWF);
                                     $x++) {
                                    echo "<option value='{$ROWF[$x]['id']}' label='{$ROWF[$x]['nombre']}' label='{$ROWF[$x]['nombre']}'>{$ROWF[$x]['nombre']}</option>";
                                }
                                unset($firm);
                                ?>
                            </select>&nbsp;<br>
                        <?php } else { ?>
                            <br/>
                            <div style="font-weight:bold;font-size: 14px"><?= $fir[0]['nombre'] ?></div>
                        <?php } ?>
                        <br/>
                        <div style="font-size: 12px">Laboratorio de An&aacute;lisis de Residuos Agroqu&iacute;micos
                        </div>
                        <br/> <br/>
                        <br/>
                    </td>
                <tr>
                    <td style="font-size:11px"> C. Archivo <br/> <br/></td>
                </tr>
                <br/>
                <br/>
                <tr>
                    <td align="center" style="font-size:12px"> &Uacute;ltima L&iacute;nea</td>
                </tr>
            </table>


            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <table align="center" border="4" rules="none" width="60%" cellpadding="10">

                <tr>
                    <td colspan="4" rowspan="1" align="center" style="font-size:14px;border:none">
                        <strong>Recibido</strong>
                    </td>
                </tr>
                <tr>
                    <td style="font-size:12px">Nombre</td>
                    <td style="font-size:12px">______________________</td>
                    <td style="font-size:12px">Firma</td>
                    <td style="font-size:12px">______________________</td>
                </tr>
                <tr>
                    <td style="font-size:12px">Fecha</td>
                    <td style="font-size:12px">______________________</td>
                    <td style="font-size:12px">Hora</td>
                    <td style="font-size:12px">_______________________</td>
                </tr>
            </table>
            </td>
            </tr>
        </table>

        <br/>
        <br/>
        <br/>
        <?php if ($_estado == 't') { ?>
            <input type="button" value="Generar" class="boton" onClick="Convertir()">&nbsp;
        <?php } ?>
        <?php if ($_estado == '1' && $Gestor->Aprobar()) { ?>
            <input type="button" value="Firmar" class="boton" onClick="Firmar()">&nbsp;
            <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
        <?php } else if ($_estado == '9' && $Gestor->Aprobar()) { ?>
            <input type="button" value="Aprobar" class="boton" onClick="Aprobar()">&nbsp;
            <input type="button" value="Cambiar Firma" class="boton" onClick="CambiaFirma()">
            <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
        <?php } ?>
        <input type="button" value="Imprimir" class="boton2" onclick="imprimirlo()">

        <!--  <input type="button" value="Eliminar" class="boton" onClick="Eliminarlo()">&nbsp;-->
        <br/>
    </center>
    <script>
        <?php if ($acreditado1 && $acreditado2) echo 'ActivaSello();'; ?>
        BETA();
    </script>
    <!--* revisar* -->
    <?= $Gestor->MEncabezado('K0029', 'p', '') ?>

</body>
</html>