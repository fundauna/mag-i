<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _menu();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('k12', 'hr', 'Reportes :: Reportes Estadísticos') ?>
<center>
    <table class="radius" align="center" width="300px">
        <tr>
            <td class="titulo">Reportes Disponibles</td>
        </tr>
        <?php if ($_POST['LID'] == '1' or $_POST['LID'] == '3') { ?>
            <tr>
                <td>&bull;&nbsp;<a href="cotizacionesXcliente.php">Cotizaciones por cliente</a></td>
            </tr>
            <tr>
                <td>&bull;&nbsp;<a href="solicitudesXcliente.php">Solicitudes por cliente</a></td>
            </tr>
            <tr>
                <td>&bull;&nbsp;<a href="muestrasXcliente.php">Muestras por cliente</a></td>
            </tr>
            <tr>
                <td>&bull;&nbsp;<a href="rep_muestras.php">Muestras reportadas</a></td>
            </tr>
            <tr>
                <td>&bull;&nbsp;<a href="rep_analisis.php">An&aacute;lisis realizados</a></td>
            </tr>
            <tr>
                <td>&bull;&nbsp;<a href="rep_ensayos.php">Ensayos realizados</a></td>
            </tr>
            <tr>
                <td>&bull;&nbsp;<a href="rep_validaciones.php">Validaciones realizadas</a></td>
            </tr>
            <?php if ($_POST['LID'] == '3') { ?>
                <tr>
                    <td>&bull;&nbsp;<a href="rep_mueingact.php">Muestras por ingrediente activo</a></td>
                </tr>
                <tr>
                    <td>&bull;&nbsp;<a href="rep_informes.php">Informes de resultados</a></td>
                </tr>
            <?php } ?>
            <tr>
                <td>
                    <hr/>
                </td>
            </tr>
        <?php } ?>
        <tr>
            <td>&bull;&nbsp;<a href="rep_analisisE.php">Proveedores: An&aacute;lisis externos</a></td>
        </tr>
        <tr>
            <td>&bull;&nbsp;<a href="rep_evaluaciones.php">Proveedores: Evaluaciones</a></td>
        </tr>
        <tr>
            <td>&bull;&nbsp;<a href="rep_pContratos.php">Proveedores: Contratos de servicios externos</a></td>
        </tr>
        <tr>
            <td>&bull;&nbsp;<a href="rep_proveedores.php">Proveedores: Listado General</a></td>
        </tr>
        <tr>
            <td>
                <hr/>
            </td>
        </tr>
        <tr>
            <td>&bull;&nbsp;<a href="rep_quejas.php">Quejas</a></td>
        </tr>
        <tr>
            <td>&bull;&nbsp;<a href="rep_tramites.php">Tr&aacute;mites</a></td>
        </tr>

        <!--<tr><td><hr /></td></tr>
        <tr><td>&bull;&nbsp;<a href="personal_capacitacionTotal.php?tipo=0">Personal:Horas totales de capacitaci&oacute;n por funcionario</a></td></tr>
        <tr><td>&bull;&nbsp;<a href="personal_capacitacionTotal.php?tipo=1">Personal:Horas por tipo de capacitaci&oacute;n por funcionario</a></td></tr>
        -->
        <tr>
            <td>
                <hr/>
            </td>
        </tr>
        <tr>
            <td>&bull;&nbsp;<a href="inventario_general.php">Inventario:General</a></td>
        </tr>
        <tr>
            <td>&bull;&nbsp;<a href="inventario_vencimientos.php">Inventario:Vencimientos</a></td>
        </tr>
        <tr>
            <td>&bull;&nbsp;<a href="inventario_general_datos_opcionales.php">Inventario:General con datos
                    opcionales</a></td>
        </tr>
        <?php if ($_POST['LID'] == '3') { ?>
            <tr>
                <td>&bull;&nbsp;<a href="inventario_general_inventarios.php">Inventario:Registro General de
                        Inventarios</a></td>
            </tr>
            <tr>
                <td>&bull;&nbsp;<a href="inventario_ingreso_salida_materiales.php">Inventario:Control Ingreso y Salida
                        de Materiales</a></td>
            </tr>
            <tr>
                <td>&bull;&nbsp;<a href="inventario_material_vencido.php">Inventario:Control Material Vencido</a></td>
            </tr>
        <?php } ?>
        <!--<tr><td>&bull;&nbsp;<a href="#">**Inventario:Proveedores</a></td></tr>-->
        <tr>
            <td>
                <hr/>
            </td>
        </tr>
        <tr>
            <td>&bull;&nbsp;<a href="equipos_contratos.php">Equipos:Contratos</a></td>
        </tr>
        <tr>
            <td>&bull;&nbsp;<a href="equipos_cronograma.php">Equipos:Cronograma</a></td>
        </tr>
        <tr>
            <td>&bull;&nbsp;<a href="equipos_general.php">Equipos:General</a></td>
        </tr>
        <tr>
            <td>&bull;&nbsp;<a href="equipos_proveedores.php">Equipos:Proveedores</a></td>
        </tr>
        <?php if ($_POST['LID'] == '1') { ?>
            <tr>
                <td>
                    <hr/>
                </td>
            </tr>
            <tr>
                <td>&bull;&nbsp;<a href="01_BDmuestras.php">BD Muestras recibidas</a></td>
            </tr>
            <tr>
                <td>&bull;&nbsp;<a href="01_MatrizAnalito.php">Arqueo Matrices<=>Analitos</a></td>
            </tr>
            <tr>
                <td>&bull;&nbsp;<a href="01_Validaciones.php">Informe de validaci&oacute;n para an&aacute;lisis
                        multiresiduos</a></td>
            </tr>
        <?php } ?>
    </table>
</center>
</body>
</html>