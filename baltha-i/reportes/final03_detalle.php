<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _final03_detalle();
$ROW = $Gestor->GetEncabezado();
if (!$ROW) die('Registro inexistente');
/**/
if ($ROW[0]['tipo'] == '2') {
    header("location: final03P_detalle.php?ID={$_GET['ID']}");
    exit;
}

$_estado = $ROW[0]['estado'];
if ($_estado != '1') $readonly = 'readonly';
else $readonly = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="id" value="<?= $ROW[0]['cs'] ?>"/>
<input type="hidden" id="cliente" value="<?= $ROW[0]['id_cliente'] ?>"/>
<script>ALFA('Por favor espere....');</script>
<center>
    <?php $Gestor->Incluir('k29', 'hr', 'An&aacute;lisis :: Informe de Resultados de An&aacute;lisis de Fertilizantes') ?>
    <?= $Gestor->MEncabezado('K0029', 'e', 'Informe de Resultados de An&aacute;lisis de Fertilizantes') ?>
    <br/>
    <table class="radius" style="font-size:16px" width="98%">
        <tr align="center">
            <td>
                <table align="center" width="100%" border="1" bordercolor="#000000" cellpadding="0" cellspacing="0"
                       style="font-size:16px">
                    <tr align="center">
                        <td><strong>Informe de An&aacute;lisis No:</strong> <?= $ROW[0]['cs'] ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="98%" style="font-size:12px">
        <tr>
            <td class="titulo" colspan="6">Datos Generales</td>
        </tr>
        <tr>
            <td><strong>No. Solicitud:</strong></td>
            <td><?= $ROW[0]['solicitud'] ?></td>
            <td><strong>Dependencia:</strong></td>
            <td><?= $Gestor->Dependencia($ROW[0]['dependencia']) ?></td>
            <td><strong>Solicitante:</strong></td>
            <td><?= $ROW[0]['solicitante'] ?></td>

        </tr>
        <tr>
            <td><strong>Fecha Ingreso de la muestra:</strong></td>
            <td><?= $ROW[0]['fecha_sol'] ?></td>
            <td><strong>Fecha Conclusi&oacute;n del an&aacute;lisis:</strong></td>
            <td><?= $ROW[0]['conclusion'] ?></td>
            <td><strong>Fecha Reporte:</strong></td>
            <td><?= $ROW[0]['fecha'] ?></td>
        </tr>
        <tr>
            <td><strong>Nombre comercial del producto:</strong></td>
            <td><?= $ROW[0]['producto'] ?></td>
            <td><strong>Tipo de formulaci&oacute;n:</strong></td>
            <td><?= $ROW[0]['tipo_form'] ?></td>
            <td>
            <td></td>
            </td>
        </tr>
        <tr>
            <td><strong>No. Registro:</strong></td>
            <td><?= $ROW[0]['registro'] ?></td>
            <td><strong>Observaciones:</strong></td>
            <td colspan="3"><textarea style="height:40px; width:70%" id="obs"
                                      <?= $readonly ?>><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="98%" style="font-size:12px">
        <?php
        $metodosTodos = $Gestor->MetodosTodos();
        $ROW2 = $Gestor->SolicitudMuestras($ROW[0]['solicitud']);
        for ($x = 0, $metodo = 0, $linea = 0; $x < count($ROW2); $x++) {
            $linea++;
            $_POST['densidad'] = 0;
            $tmp = str_replace('LCC-', '', $ROW[0]['solicitud']) . '-' . ($x + 1) . '-' . $_POST['tipo'];
            $cod_int = '3-' . str_replace(' ', '', $tmp);
            ?>
            <tr style="background-color:#CCCCCC">
                <td><strong>No. Muestra:</strong> <?= $tmp ?></td>
                <td style="background-color:#CCCCCC">Cinta: <?= $ROW2[$x]['cod_ext'] ?></td>
                <td>Sellada: <?= $ROW2[$x]['sellado'] == '1' ? 'S�' : 'No' ?></td>
                <td>Lote: <?= $ROW2[$x]['lote'] ?></td>
                <td colspan="2">Recipiente: <?= $Gestor->Recipiente($ROW2[$x]['recipiente']) ?></td>
            </tr>
            <tr id="tr_<?= $linea ?>" style="display:none">
                <td colspan="6" id="obs<?= $linea ?>"></td>
            </tr>
            <?php
            //Analisis Quimicos
            $ROW3 = $Gestor->Resultados($cod_int, 0);
            if ($ROW3) {
                echo '<tr><td colspan="6"><strong>An�lisis Qu�micos RTCR 228</strong></td></tr><tr style="text-decoration:underline"><td>Elemento</td><td align="center">C.D.</td><td align="center">C.E.</td><td align="center">I.C.</td><td align="center">U.C</td><td>M&eacute;todo</td></tr>';
                for ($y = 0; $y < count($ROW3); $y++) {
                    $linea++;
                    $metodo++;
                    list($res1, $res2) = explode(',', $ROW3[$y]['resultado']);
                    $tildes = array(' ', 'Cn=', 'IC=', 'CE=', 'CP=');
                    $solas = array('', '', '', '', '');
                    $res1 = str_replace($tildes, $solas, $res1);
                    $res2 = str_replace($tildes, $solas, $res2);
                    /*if($res1==0){
                        $res1 = 'N.D.';
                        $res2 = 'N.A.';
                    }*/
                    ?>
                    <tr>
                        <td><?= $ROW3[$y]['elemento'] ?></td>
                        <td align="center"><?= $ROW3[$y]['declarada'] ?></td>
                        <td align="center"><?= $res1 ?></td>
                        <td align="center"><?= $res2 ?></td>
                        <td align="center"><?= $Gestor->Unidad($ROW3[$y]['unidad']) ?></td>
                        <td>
                            <select id="metodo<?= $metodo ?>" onchange="muestraMetodosOtros()" name="metodo">
                                <?php foreach ($metodosTodos as $metodov): ?>
                                    <?php if ($metodov['clasificacion'] == '0' || $metodov['id'] == '15'): ?>
                                        <option value="<?= $metodov['id'] ?>"  ><?= $metodov['resumen'] ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                            <input placeholder="Indique" type="text" id="metodootro<?= $metodo ?>" name="metodootro"
                                   value="" hidden=""/>
                        </td>
                    </tr>
                    <tr id="tr_<?= $linea ?>" style="display:none">
                        <td colspan="6" id="obs<?= $linea ?>"></td>
                    </tr>
                    <?php
                }//FOR 2
                if ($_POST['densidad'] == 1) {
                    echo "<tr align='center'>
				<td></td>
				<td style='text-decoration:underline'>Densidad</td>
				<td style='text-decoration:underline'>Incertidumbre</td>
				<td colspan='3'></td>
			</tr>
			<tr align='center'>
				<td></td>
				<td>{$ROW3[0]['densidad']} g/cm<sup>3</sup></td>
				<td>{$ROW3[0]['incertidumbre']}</td>
				<td colspan='3'></td>
			</tr>";
                }
            }//IF
            //Impurezas
            $ROW3 = $Gestor->Resultados($cod_int, 2);
            if ($ROW3) {
                echo '<tr><td colspan="6"><strong>Impurezas</strong></td></tr><tr style="text-decoration:underline"><td>Nombre</td><td></td><td align="center">C.E.</td><td align="center">I.C.</td><td align="center">U.C</td><td>M&eacute;todo</td></tr>';
                for ($y = 0; $y < count($ROW3); $y++) {
                    $linea++;
                    $metodo++;
                    list($res1, $res2) = explode(',', $ROW3[$y]['resultado']);
                    $tildes = array(' ', 'Cn=', 'IC=', 'CE=', 'CP=');
                    $solas = array('', '', '', '', '');
                    $res1 = str_replace($tildes, $solas, $res1);
                    $res2 = str_replace($tildes, $solas, $res2);
                    if ($res1 == 0) {
                        $res1 = 'N.D.';
                        $res2 = 'N.A.';
                    } else {
                        $res1 *= 1;
                        $res2 *= 1;
                    }
                    ?>
                    <tr>
                        <td><?= $ROW3[$y]['elemento'] ?></td>
                        <td align="center"></td>
                        <td align="center"><?= $res1 ?></td>
                        <td align="center"><?= $res2 ?></td>
                        <td align="center"><?= 'mg/kg' //$Gestor->Unidad($ROW3[$y]['unidad']) ?></td>
                        <td>
                            <select id="metodo<?= $metodo ?>" onchange="muestraMetodosOtros()" name="metodo">
                                <?php foreach ($metodosTodos as $metodov): ?>
                                    <?php if ($metodov['clasificacion'] == '2' || $metodov['id'] == '15'): ?>
                                        <option value="<?= $metodov['id'] ?>"><?= $metodov['resumen'] ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                            <input placeholder="Indique" type="text" id="metodootro<?= $metodo ?>" name="metodootro"
                                   value="" hidden=""/>
                        </td>
                    </tr>
                    <tr id="tr_<?= $linea ?>" style="display:none">
                        <td colspan="6" id="obs<?= $linea ?>"></td>
                    </tr>
                    <?php
                }//FOR 2
            }//IF
        }//FOR 1

        //CARGA METODOS EN CADA LINEA
        $ROW = $Gestor->Metodos();
        echo '<script>';
        for ($x = 0, $str = ''; $x < count($ROW); $x++) {
            if ($ROW[$x]['metodo'] == '15') {
                $otro = $Gestor->MetodoOtro($_GET['ID'], $ROW[$x]['linea']);
                echo "EscribeMetodosOtros({$ROW[$x]['linea']}, '{$otro[0]['descripcion']}');";
            }
            $str .= $ROW[$x]['texto'] . '<br>';
            echo "EscribeMetodos({$ROW[$x]['linea']}, '{$ROW[$x]['metodo']}');";
        }
        echo '</script>';
        ?>
        <tr>
            <td colspan="6" align="right">
                <hr/>
                <font size="-2"><?= $str ?></font></td>
        </tr>
        <tr>
            <td colspan="6">
                <hr/>
                <font size="-2">U.C.: Unidades de concentraci&oacute;n. C.D.: Concentraci&oacute;n declarada. C.E.:
                    Concentraci&oacute;n encontrada. I.C.: Incertidumbre Combinada. N.D.: No detectable. N.C.: No
                    cuantificable. N.A.: No Aplica.</font></td>
        </tr>
    </table>
    <?php
    //CARGA HISTORIAL
    $ROW = $Gestor->Historial();
    echo '<br /><table width="98%" class="radius" style="font-size:12px" cellpadding="8" cellspacing="8"></tr>';
    for ($x = 0; $x < count($ROW); $x++) {
        if ($ROW[$x]['accion'] != '1') {
            $ROW[$x]['accion'] = $Gestor->Accion($ROW[$x]['accion']);
            echo "<tr>
			<td><strong>{$ROW[$x]['accion']}</strong> {$ROW[$x]['nombre']} {$ROW[$x]['ap1']}, <strong>{$ROW[$x]['rol']}</strong>, {$ROW[$x]['fecha1']}</td>
			<td>Firma:____________________</td>
		</tr>";
        } else {
            $ROW[$x]['accion'] = $Gestor->Accion($ROW[$x]['accion']);
            echo "<tr class='lolo'>
			<td><strong>{$ROW[$x]['accion']}</strong> {$ROW[$x]['nombre']} {$ROW[$x]['ap1']}, <strong>{$ROW[$x]['rol']}</strong>, {$ROW[$x]['fecha1']}</td>
			<td></td>
		</tr>";
        }
    }
    echo '</table>';

    //CARGA OBSERVACIONES EN CADA LINEA
    $ROW = $Gestor->Observaciones();
    echo '<script>';
    for ($x = 0; $x < count($ROW); $x++) {
        echo "EscribeObs({$ROW[$x]['linea']}, '{$ROW[$x]['obs']}');";
    }
    echo '</script>';
    ?>
    <?php if (file_exists('../../caspha-i/docs/reportes/' . $_GET['ID'] . '.zip')) { ?>
        <br/>
        <table width="98%" class="radius" style="font-size:12px" cellpadding="8" cellspacing="8"></tr>
            <tr>
                <td><strong>Documentos Anexos:</strong></td>
                <td><a href="../../caspha-i/docs/reportes/<?= $_GET['ID'] ?>.zip"><?= $_GET['ID'] ?></a></td>
            </tr>
        </table>
    <?php } ?>
    <br/>
    <table width="98%" class="radius" style="font-size:12px" cellpadding="8" cellspacing="8"></tr>
        <tr>
            <td><strong>Recibido por:</strong> ________________________________________</td>
            <td><strong>Identificaci&oacute;n:</strong> ____________________</td>
            <td><strong>Fecha:</strong> ____________________</td>
            <td><strong>Sello:</strong>____________________</td>
        </tr>
    </table>
    <br/><?= $Gestor->Encabezado('IF', 'p', '', '') ?>
    <br/><br/>
    <?php if ($_estado == '1' && $Gestor->Aprobar()) { ?>
        <input type="button" value="Aprobar" class="boton" onClick="Aprobar();Guardar();">&nbsp;
        <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
        <input type="button" value="Devolver" class="boton" onClick="Eliminar()">&nbsp;
    <?php } ?>
    <input type="button" value="Guardar M&eacute;todos" class="boton" onClick="Guardar()">&nbsp;
    <input type="button" value="Imprimir" class="boton2" onClick="window.print()">
</center>
<script>BETA();
    muestraMetodosOtros();</script>
<?= $Gestor->MEncabezado('K0029', 'p', '') ?>
</body>
</html>