<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _final_historial();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('k9', 'hr', 'Reportes :: Historial Informes de Resultados') ?>
<?= $Gestor->Encabezado('K0009', 'e', 'Historial Informes de Resultados') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" style="width:560px">
            <tr>
                <td class="titulo" colspan="4">Filtro</td>
            </tr>
            <tr>
                <td>Buscar por:<br/>
                    <select id="tipo" name="tipo">
                        <option value="0">No. de Informe</option>
                        <option value="1" <?php if ($_POST['tipo'] == '1') echo 'selected'; ?>>No. de Solicitud</option>
                        <option value="2" <?php if ($_POST['tipo'] == '2') echo 'selected'; ?>>No. de Muestra</option>
                        <option value="4" <?php if ($_POST['tipo'] == '4') echo 'selected'; ?>>No. de Lote</option>
                        <?php if ($_POST['LID'] == '3') { ?>
                            <option value="3" <?php if ($_POST['tipo'] == '3') echo 'selected'; ?>>No. de Registro
                            </option>
                            <option value="5" <?php if ($_POST['tipo'] == '5') echo 'selected'; ?>>Elementos</option>
                            <option value="6" <?php if ($_POST['tipo'] == '6') echo 'selected'; ?>>I.A.</option>
                            <option value="7" <?php if ($_POST['tipo'] == '7') echo 'selected'; ?>>An&aacute;lisis
                            </option>
                            <option value="8" <?php if ($_POST['tipo'] == '8') echo 'selected'; ?>>Impurezas</option>
                        <?php } ?>
                    </select>
                </td>
                <td>Valor:<br/>
                    <input type="text" id="valor" name="valor" size="15" maxlength="20" value="<?= $_POST['valor'] ?>"/>
                </td>
                <td>Fecha:<br/>
                    <input type="text" id="desde" name="desde" class="fecha" value="<?= $_POST['desde'] ?>" readonly
                           onClick="show_calendar(this.id);">&nbsp;
                    <input type="text" id="hasta" name="hasta" class="fecha" value="<?= $_POST['hasta'] ?>" readonly
                           onClick="show_calendar(this.id);">
                </td>
                <td>Estado:<br/>
                    <select name="estado">
                        <option value="">Todos</option>
                        <option value="t" <?php if ($_POST['estado'] == 't') echo 'selected'; ?>>Preliminar</option>
                        <option value="1" <?php if ($_POST['estado'] == '1') echo 'selected'; ?>>Generado</option>
                        <option value="2" <?php if ($_POST['estado'] == '2') echo 'selected'; ?>>Aprobado</option>
                        <option value="5" <?php if ($_POST['estado'] == '5') echo 'selected'; ?>>Anulado</option>
                        <option value="9" <?php if ($_POST['estado'] == '9') echo 'selected'; ?>>Firmado</option>
                        <option value="s" <?php if ($_POST['estado'] == 's') echo 'selected'; ?>>Sustituido</option>
                        <!-- <option value="-1" <?php if ($_POST['estado'] == '-1') echo 'selected'; ?>>Eliminado</option> -->
                    </select></td>
            </tr>
            <tr align="center">
                <td colspan="4"><input type="button" value="Buscar" class="boton2" onclick="SolicitudesBuscar(this)"/>
                </td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:760px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th width="10%">No. de informe</th>
                <th width="5%">Fecha</th>
                <th width="20%">Referencia</th>
                <th width="5%">Estado</th>
                <th width="10%">Acciones</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->Historial();
            for ($x = 0; $x < count($ROW); $x++) {
                $ROW[$x]['id'] = str_replace(' ', '', $ROW[$x]['id']);
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#"
                           onclick="InformeDetalle('<?= $ROW[$x]['id'] ?>', '<?= $ROW[$x]['estado'] ?>', '<?= $ROW[$x]['lab'] ?>')"><?= $ROW[$x]['id'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['fecha1'] ?></td>
                    <td><?= str_replace('--', '-', $ROW[$x]['ref']);     $ROW[$x]['ref'] ?></td>
                    <td><?= $Gestor->Estado($ROW[$x]['estado']) ?></td>
                    <td>
                        <?php
                        if (($ROW[$x]['estado'] == '2') || ($ROW[$x]['estado'] == 's')) {
                        $ruta = "../../caspha-i/docs/reportes/{$ROW[$x]['id']}.zip";
                        if (file_exists($ruta)) {
                            ?>
                            <img src="<?php $Gestor->Incluir('bajar2', 'bkg') ?>" title="Descargar"
                                 onclick="DocumentosZip('<?= $ROW[$x]['id'] ?>', '<?= __MODULO__ ?>')"
                                 class="tab2"/>&nbsp;
                            <img src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar anexo"
                                 onclick="DocElimina('<?= $ROW[$x]['id'] ?>')" class="tab2"/>&nbsp;
                            <?php
                        }
                        ?>
                        <img src="<?php $Gestor->Incluir('subir', 'bkg') ?>" title="Anexar informe" class="tab3"
                             onclick="Anexar('<?= $ROW[$x]['id'] ?>')"/>

                        <img  src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Sustituir informe" class="tab3"
                             onclick="Sustituir('<?= $ROW[$x]['id'] ?>')"/>
                    </td>

                    <?php
                    }
                    ?>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</center>
<?= $Gestor->Encabezado('K0009', 'p', '') ?>
</body>
</html>