<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _graficador();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('jquery-1.9.1.min', 'js'); ?>
    <?php $Gestor->Incluir('graficador', 'gfx'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body style="font-size:12px;">
<table class="radius" align="center" width="100%">
    <tr align='center'>
        <td width='25%'><?= $Gestor->Incluir('mag', 'png') ?></td>
        <td width='50%'><strong>Ministerio de Agricultura y Ganader&iacute;a<br/>Servicio Fitosanitario del Estado<br/>Departamento
                de Laboratorios</strong></td>
        <td width='25%'><?= $Gestor->Incluir('agro', 'png') ?></td>
    </tr>
    <tr align='center'>
        <td><?= date('d/m/Y') ?></td>
        <td><?= $_POST['titulo'] ?></td>
        <td><?= $Gestor->Title() ?></td>
    <tr>
</table>
<br>
<?php
$ROW = $Gestor->TablaCapacitaciones();
?>
<table class='radius' align='center' width='100%' border="1">
    <tr align="center">
        <td width="25%"><strong>Funcionario</strong></td>
        <td><strong><?= $_POST['desde'] ?></strong></td>
        <td><strong><?= (date('Y') - 1) ?></strong></td>
        <td><strong><?= (date('Y') - 2) ?></strong></td>
        <td><strong><?= (date('Y') - 3) ?></strong></td>
        <td><strong><?= (date('Y') - 4) ?></strong></td>
        <td width="25%"><strong>A&ntilde;o de ingreso al SFE</strong></td>
    </tr>
    <?php
    $totA = $tot1 = $tot2 = $tot3 = $tot4 = 0;

    for ($x = 0; $x < count($ROW); $x++) {
        $totA += $ROW[$x]['actual'];
        $tot1 += $ROW[$x]['menos1'];
        $tot2 += $ROW[$x]['menos2'];
        $tot3 += $ROW[$x]['menos3'];
        $tot4 += $ROW[$x]['menos4'];
        ?>
        <tr align="center">
            <td><?= $ROW[$x]['nombre'] ?></td>
            <td><?= $ROW[$x]['actual'] ?></td>
            <td><?= $ROW[$x]['menos1'] ?></td>
            <td><?= $ROW[$x]['menos2'] ?></td>
            <td><?= $ROW[$x]['menos3'] ?></td>
            <td><?= $ROW[$x]['menos4'] ?></td>
            <td><?= $ROW[$x]['ingreso'] ?></td>
        </tr>
    <?php } ?>
    <tr align="center">
        <td><strong>Total en horas</strong></td>
        <td><?= $totA ?></td>
        <td><?= $tot1 ?></td>
        <td><?= $tot2 ?></td>
        <td><?= $tot3 ?></td>
        <td><?= $tot4 ?></td>
        <td></td>
    </tr>
</table>
<br/>
<table id="tabla" style="display:none">
    <thead>
    <tr>
        <th></th>
        <th><strong><?= $_POST['desde'] ?></strong></th>
        <th><strong><?= (date('Y') - 1) ?></strong></th>
        <th><strong><?= (date('Y') - 2) ?></strong></th>
        <th><strong><?= (date('Y') - 3) ?></strong></th>
        <th><strong><?= (date('Y') - 4) ?></strong></th>
    </tr>
    </thead>
    <tbody>
    <?php
    for ($x = 0; $x < count($ROW); $x++) {
        ?>
        <tr align="center">
            <th><?= $ROW[$x]['nombre'] ?></th>
            <td><?= $ROW[$x]['actual'] ?></td>
            <td><?= $ROW[$x]['menos1'] ?></td>
            <td><?= $ROW[$x]['menos2'] ?></td>
            <td><?= $ROW[$x]['menos3'] ?></td>
            <td><?= $ROW[$x]['menos4'] ?></td>
        </tr>
        <?php
    }
    $Gestor->LimpiaTabla();
    ?>
    </tbody>
</table>
<br/>
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<script type="text/javascript">
    $(function () {
        $('#container').highcharts({
            data: {
                table: document.getElementById('tabla')
            },
            chart: {
                type: 'column',
                zoomType: 'xy'
            },
            title: {
                text: '<?=$_POST['titulo']?>'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Cantidad de horas'
                }/*,
			tickInterval: 50*/
            },
            tooltip: {
                formatter: function () {
                    return this.y;
                }
            }
        });
    });
</script>
</body>
</html>