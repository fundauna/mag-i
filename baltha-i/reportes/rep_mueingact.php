<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _rep_mueingact();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('k15', 'hr', 'Reportes :: Muestras por ingrediente activo') ?>
<?= $Gestor->Encabezado('K0015', 'e', 'Muestras por ingrediente activo') ?>
<center>
    <form action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>" method="post"
          target="_blank">
        <input type="hidden" name="lab" value="<?= $_POST['LID'] ?>"/>
        <table class="radius" align="center">
            <tr>
                <td class="titulo" colspan="4">Filtro</td>
            </tr>
            <tr>
                <td>Desde: <input type="text" id="desde" name="desde" class="fecha" readonly
                                  onClick="show_calendar(this.id);">&nbsp;
                    Hasta: <input type="text" id="hasta" name="hasta" class="fecha" readonly
                                  onClick="show_calendar(this.id);"></td>
                <td>An&aacute;lisis: <select name="analisis" style="width:130px">
                        <option value="-2">Todos Fertilizantes</option>
                        <option value="-3">Todos Plaguicidas</option>
                        <?php
                        $ROW = $Gestor->Analisis();
                        for ($x = 0, $anterior = ''; $x < count($ROW); $x++) {
                            if ($anterior != $ROW[$x]['tipo']) {
                                $anterior = $ROW[$x]['tipo'];
                                echo "<optgroup label='", $Gestor->Tipo($ROW[$x]['tipo']), "'></optgroup>";
                            }
                            ?>
                            <option value="<?= $ROW[$x]['id'] ?>"><?= $ROW[$x]['nombre'] ?></option>
                            <?php
                        }
                        ?>
                    </select></td>
                <td>Formato:&nbsp;
                    <select name="formato">
                        <option value='0'>Texto</option>
                        <option value='1'>Excel</option>
                    </select>
                </td>
                <td><input type="button" value="Buscar" class="boton2" onclick="SolicitudesBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <a href="#" onclick="location.href='menu.php'">[Atr&aacute;s]</a>
</center>
<?= $Gestor->Encabezado('K0015', 'p', '') ?>
</body>
</html>