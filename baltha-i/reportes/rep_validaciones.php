<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _rep_validaciones();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('k25', 'hr', 'Reportes :: Validaciones') ?>
<?= $Gestor->Encabezado('K0025', 'e', 'Validaciones') ?>
<center>
    <form action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>" method="post"
          target="_blank">
        <input type="hidden" name="lab" value="<?= $_POST['LID'] ?>"/>
        <table class="radius" align="center">
            <tr>
                <td class="titulo" colspan="6">Filtro</td>
            </tr>
            <tr>
                <td><strong>Desde:</strong> <input type="text" id="desde" name="desde" class="fecha" readonly
                                                   onClick="show_calendar(this.id);"></td>
                <td><strong>Hasta:</strong> <input type="text" id="hasta" name="hasta" class="fecha" readonly
                                                   onClick="show_calendar(this.id);"></td>
                <td><strong>Tipo:</strong> <select name="tipo">
                        <option value=''>Todos</option>
                        <?php if ($_POST['LID'] == '3') { ?>
                            <option value="1">Linealidad</option>
                            <option value="2">Repetibilidad</option>
                            <option value="3">Reproducibilidad</option>
                            <option value="7">Recuperaci&oacute;n</option>
                            <option value="8">Robustez</option>
                        <?php } elseif ($_POST['LID'] == '2') { ?>
                            <option value="4">Biolog&iacute;a Molecular</option>
                        <?php } elseif ($_POST['LID'] == '1') { ?>
                            <option value="5">C&aacute;lculo para validaciones</option>
                        <?php } ?>
                    </select></td>
                <td>Formato:&nbsp;
                    <select name="formato">
                        <option value='0'>Texto</option>
                        <option value='1'>Excel</option>
                    </select>
                </td>
                <td><input type="button" value="Buscar" class="boton2" onclick="Generar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <a href="#" onclick="location.href='menu.php'">[Atr&aacute;s]</a>
</center>
<?= $Gestor->Encabezado('K0025', 'p', '') ?>
</body>
</html>