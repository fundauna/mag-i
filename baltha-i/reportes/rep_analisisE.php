<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _rep_analisisE();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('k16', 'hr', 'Reportes :: Proveedores de an�lisis externos') ?>
<?= $Gestor->Encabezado('K0016', 'e', 'Proveedores de an�lisis externos') ?>
<center>
    <form action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>" method="post"
          target="_blank">
        <input type="hidden" name="lab" value="<?= $_POST['LID'] ?>"/>
        <table class="radius" align="center">
            <tr>
                <td class="titulo" colspan="4">Filtro</td>
            </tr>
            <tr>
                <td>Tipo de Contrato:&nbsp;
                    <select name="tipo">
                        <option value="">Todos</option>
                        <option value="1">An&aacute;lisis Ambiental</option>
                        <option value="2">Salud Ocupacional</option>
                        <option value="3">Otro</option>
                    </select>
                </td>
                <td>Proveedor:&nbsp;<input type="text" name="proveedor"/></td>
                <td>Formato:&nbsp;
                    <select name="formato">
                        <option value='0'>Texto</option>
                        <option value='1'>Excel</option>
                    </select>
                </td>
                <td><input type="submit" value="Buscar" class="boton2"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <a href="#" onclick="location.href='menu.php'">[Atr&aacute;s]</a>
</center>
<?= $Gestor->Encabezado('K0016', 'p', '') ?>
</body>
</html>