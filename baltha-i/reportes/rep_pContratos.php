<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _rep_pContratos();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('k19', 'hr', 'Reportes :: Contratos de mantenimiento y servicios externos') ?>
<?= $Gestor->Encabezado('K0019', 'e', 'Contratos de mantenimiento y servicios externos') ?>
<center>
    <form action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>" method="post"
          target="_blank">
        <input type="hidden" name="lab" value="<?= $_POST['LID'] ?>"/>
        <table class="radius" align="center">
            <tr>
                <td class="titulo" colspan="4">Filtro</td>
            </tr>
            <tr>
                <td>Desde: <input type="text" id="desde" name="desde" class="fecha" readonly
                                  onClick="show_calendar(this.id);">&nbsp;
                    Hasta: <input type="text" id="hasta" name="hasta" class="fecha" readonly
                                  onClick="show_calendar(this.id);"></td>
                <td>Tipo de Contrato:&nbsp;
                    <select name="tipo">
                        <option value="-1">Todos</option>
                        <?php
                        $ROW2 = $Gestor->TiposMuestra();
                        for ($x = 0; $x < count($ROW2); $x++) {
                            ?>
                            <option value="<?= $ROW2[$x]['cs'] ?>"><?= $ROW2[$x]['nombre'] ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td>Formato:&nbsp;
                    <select name="formato">
                        <option value='0'>Texto</option>
                        <option value='1'>Excel</option>
                    </select>
                </td>
                <td><input type="button" value="Buscar" class="boton2" onclick="Generar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <a href="#" onclick="location.href='menu.php'">[Atr&aacute;s]</a>
</center>
<?= $Gestor->Encabezado('K0019', 'p', '') ?>
</body>
</html>