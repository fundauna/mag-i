<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _rep_ensayos();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('k17', 'hr', 'Reportes :: Ensayos realizados') ?>
<?= $Gestor->Encabezado('K0017', 'e', 'Ensayos realizados') ?>
<center>
    <form action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>" method="post"
          target="_blank">
        <input type="hidden" name="lab" value="<?= $_POST['LID'] ?>"/>
        <table class="radius" align="center">
            <tr>
                <td class="titulo" colspan="6">Filtro</td>
            </tr>
            <tr>
                <td>Desde: <input type="text" id="desde" name="desde" class="fecha" readonly
                                  onClick="show_calendar(this.id);">&nbsp;
                    Hasta: <input type="text" id="hasta" name="hasta" class="fecha" readonly
                                  onClick="show_calendar(this.id);"></td>
                <td>Ensayo: <select name="ensayo" style="width:130px">
                        <option value="-1">Todos</option>
                        <?php
                        $ROW = $Gestor->Ensayos();
                        for ($x = 0; $x < count($ROW); $x++) {
                            ?>
                            <option value="<?= $ROW[$x]['id'] ?>"><?= $ROW[$x]['nombre'] ?></option>
                            <?php
                        }
                        ?>
                    </select></td>
                <td>
                    <?php if ($_POST['LID'] == '3') { ?>
                        Tipo: <select name="tipo">
                            <option value='F'>Fertilizantes</option>
                            <option value='P'>Plaguicidas</option>
                            <option value="">Todos</option>
                        </select>
                    <?php } ?>
                </td>
                <td>Estado: <select name="cumple">
                        <option value="">Todos</option>
                        <option value='0'>Inv&aacute;lido</option>
                        <option value='1'>V&aacute;lido</option>
                    </select></td>
                <td>Formato:&nbsp;
                    <select name="formato">
                        <option value='0'>Texto</option>
                        <option value='1'>Excel</option>
                    </select>
                </td>
                <td><input type="button" value="Buscar" class="boton2" onclick="SolicitudesBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <a href="#" onclick="location.href='menu.php'">[Atr&aacute;s]</a>
</center>
<?= $Gestor->Encabezado('K0017', 'p', '') ?>
</body>
</html>