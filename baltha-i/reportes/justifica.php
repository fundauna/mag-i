<?php
define('__MODULO__', 'reportes');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _justifica();
$ROW = $Gestor->ObtieneDatos();
$ROW2 = $Gestor->ObtieneDatosEncabezado();
$ROW3 = $Gestor->Solicitud01DetalleGet($ROW2[0]['solicitud'], $_GET['ID'] );
//$ROW4 = $Gestor->Solicitud01DetalleGet($ROW2[0]['solicitud']);

if ($ROW[0]['estado'] != '' && $ROW[0]['estado'] != '0') {
    $disabled = 'disabled';
} else {
    $disabled = '';
}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    <script type="text/javascript"></script>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>

    <script>
        img1 = '<?php $Gestor->Incluir('closed', 'bkg') ?>';
        img2 = '<?php $Gestor->Incluir('downboxed', 'bkg') ?>';
    </script>
</head>
<body>
<?php $Gestor->Incluir('f13', 'hr', 'Inventario :: Control de informes de ensayo modificados') ?>
<?= $Gestor->Encabezado('F0013', 'e', 'Control de informes de ensayo modificados') ?>
<center>
    <input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
    <input type="hidden" id="numInfo" name="numInfo" value="<?= $_GET['ID'] ?>"/>
    <input type="hidden" id="usuario" name="usuario" value="<?= $Gestor->Get('UNAME') ?>"/>
    <input type="hidden" id="numSolicitud" name="numSolicitud" value="<?= $ROW2[0]['solicitud'] ?>"/>


    <input type="hidden" id="id_cliente" name="id_cliente" value="<?= $ROW2[0]['id_cliente'] ?>"/>



    <table class="radius" width="95%">
        <tr>
            <td class="titulo" colspan="2">Datos Generales</td>
        </tr>
        <!-- <tr>
            <td>Consecutivo:</td>
            <td><input type="text" id="codigo" value="<?= $ROW[0]['nombre'] ?>"></td>
        </tr>-->
        <tr>
            <td>Nuevo n&uacute;mero de informe:</td>
            <td> <?= $_GET['ID'] ?> - 2 </td>
        </tr>
        <tr>
            <td>Justificaci&oacute;n:</td>
            <td><textarea id="obs">Este documento sustituye y anula el informe de ensayo <?= $_GET['ID'];
                    $ROW[0]['obs'] ?>.</textarea></td>
        </tr>
        <tr>
            <td>Informe que sustituye:</td>
            <td id="informe" name="informe"><?= $_GET['ID'] ?></td>
        </tr>
        <tr>
            <td> &nbsp;</td>
        </tr>
        <tr>
            <input type="hidden" id="pry" name="pry" value="<?= $_GET['ID'] ?>">
            <td> Usuario:</td>
            <td ><?= $Gestor->Get('UNAME') ?></td>
        </tr>
    </table>
    <br/>
    <hr>
    <table class="radius" width="95%" cellspacing="10" cellpadding="5">
        <tr>
            <td class="titulo" colspan="4">Encabezado de la muestra</td>
            <input type="hidden" id="cliente" value="<?= $ROW2[0]['cliente'] ?>"/>
        </tr>
        <tr>
            <td>N&uacute;mero de solicitud:</td>
            <td><?= $ROW2[0]['solicitud'] ?></td>
            <td>Fecha de creaci&oacute;n:</td>
            <td><?= $ROW2[0]['fecha'] ?></td>
        </tr>
        <tr>
            <td>Cliente:</td>
            <td><input type="text" name="nomcliente" id="nomcliente" value="
                <?= $ROW2[0]['cliente'] ?>" class="listaChav"
                       readonly onclick="ClientesLista()" <?= $disabled ?>></td>
            <td>N&uacute;mero de lote:</td>
            <td><?= $ROW2[0]['lote'] ?></td>
        </tr>
        <tr>
            <td>Solicitante:</td>
            <td><input type="text" name="solicita" id="solicita" size="47" value="<?= $ROW2[0]['solicitante'] ?>"></td>
            <td>N&uacute;mero de muestra:</td>
            <td><?= str_replace('--', '-', $ROW2[0]['muestra']) ?></td>
        </tr>
        <tr>
            <td> Fecha y hora de muestreo:</td>
            <td><?= $ROW2[0]['emision'] ?></td>
            <td> Fecha y hora de Recepci&oacute;n de la muestra :</td>
            <td><?= $ROW2[0]['fecha_sol'] ?></td>
        </tr>
    </table>
    <br/>
    <hr>
    <table class="radius" width="95%" cellspacing="5" cellpadding="5">
        <tr>
            <td class="titulo" colspan="4">Datos de la muestra</td>
        </tr>
        <tr>
            <td>Fecha:</td>
            <td><?= $ROW3[0]['fecha'] ?></td>
            <td>Hora:</td>
            <td><?= $ROW3[0]['hora'] ?></td>
        </tr>

        <tr>
            <td>Matriz:</td>
            <td><?= $ROW3[0]['nombre'] ?></td>
            </td>
            <td>Tipo:</td>
            <td><?= $ROW3[0]['tipo'] ?></td>
        </tr>
        <tr>
            <td>Nombre Grupo:</td>
            <td><?= $ROW3[0]['nombreGrupo'] ?></td>
            <td>Grupo Acreditado:</td>
            <td><?= ($ROW3[0]['acreditado'] == 1) ? 'Si' : 'No' ?></td>
        </tr>
        <tr>
            <td>Nombre del Analisis :</td>
            <td><?= $ROW3[0]['nom_analisis'] ?></td>
            <td>Masa:</td>
            <td><?= $ROW3[0]['masa'] ?>&nbsp;g</td>
        </tr>
        <tr>
            <?php
            $part = '';
            if ($ROW3[0]['parte'] == '0') {
                $part = '&Iacute;ntegra';
            } elseif ($ROW3[0]['parte'] == '1') {
                $part = 'C&aacute;scara';
            } elseif ($ROW3[0]['parte'] == '2') {
                $part = 'Pulpa';
            }
            ?>
            <td>Parte:</td>
            <td><?= $part ?></td>

            <?php
            $emp = '';
            if ($ROW3[0]['empaque'] == '0') {
                $emp = 'Bolsa Intacta';
            } elseif ($ROW3[0]['empaque'] == '1') {
                $emp = 'Bolsa Da&ntilde;ada';
            } elseif ($ROW3[0]['empaque'] == '2') {
                $emp = 'Botella Vidrio Ambar';
            } elseif ($ROW3[0]['empaque'] == '3') {
                $emp = 'Otro';
            }
            ?>
            <td>Empaque:</td>
            <td><?= $emp ?></td>
        </tr>
        <tr>
            <?php
            $form = '';
            if ($ROW3[0]['forma'] == '0') {
                $form = 'Entero';
            } elseif ($ROW3[0]['forma'] == '1') {
                $form = 'En Cuartos';
            } elseif ($ROW3[0]['forma'] == '2') {
                $form = 'Rebanadas';
            } elseif ($ROW3[0]['forma'] == '3') {
                $form = 'Podrida';
            } elseif ($ROW3[0]['forma'] == '4') {
                $form = 'Golpeada';
            } elseif ($ROW3[0]['forma'] == '5') {
                $form = 'Otro';
            }
            ?>
            <td>Forma:</td>
            <td><?= $form ?></td>

            <td>Presentaci&oacute;n:</td>
            <?php
            $pres = '';
            if ($ROW3[0]['presentacion'] == '0') {
                $pres = 'Congelada';
            } elseif ($ROW3[0]['presentacion'] == '1') {
                $pres = 'Descongelada';
            } elseif ($ROW3[0]['presentacion'] == '2') {
                $pres = 'Fresca, no refrigerada';
            } elseif ($ROW3[0]['presentacion'] == '3') {
                $pres = 'Fresca, refrigerada';
            }
            ?>
            <td><?= $pres ?></td>
        </tr>

        <tr>
            <td>C&oacute;digo externo:</td>
            <td><input type="text" id="codigo" value="<?= $ROW3[0]['codigo'] ?>"></td>
            <td>FRF:</td>
            <td><input type="text" id="obsfrf" value="<?= $ROW3[0]['obsfrf'] ?>"></td>
        </tr>
        <tr>
            <td>Usa Metodo del cliente:</td>
            <td><input type="text" id="metodo" value="<?= $ROW3[0]['metodo'] ?>"></td>
            <td>Observaciones:</td>
            <td><input type="text" id="observaciones" value="<?= $ROW3[0]['observaciones'] ?>"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="95%" cellspacing="10" cellpadding="5">
        <tr>
            <td class="titulo" colspan="4">Documento antiguo</td>
        </tr>
        <tr>
            <td>Por favor adjuntar el informe anterior en formato pdf, con todas las firmas y sellos correspondientes.
            </td>
            <td align="right"><input type="file" name="archivo" id="archivo"></td>
        </tr>
        <tr>
            <td>
                <button type="submit" class="boton" id="btnEnviar" title="Cargar"
                        onclick="valida()"> Cargar Archivo
                </button>
            </td>
        </tr>
    </table>
    <br/>

    <input type="button" id="btn" value="Aceptar" class="boton" onclick="salvar()">
</center>
<?= $Gestor->Encabezado('F0013', 'p', '') ?>
</body>
</html>