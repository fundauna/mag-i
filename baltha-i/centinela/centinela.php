<?php
define('__MODULO__', 'centinela');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _centinela();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('z01', 'hr', 'Centinela :: Informaci&oacute;n Centinela') ?>
<?= $Gestor->Encabezado('Z0001', 'e', 'Informaci&oacute;n Centinela') ?>

<center>
    <form action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_reportes_especifico.php' ?>" method="post"
          target="_blank">
        <table class="radius" align="center">
            <tr>
                <td class="titulo" colspan="5">B&uacute;squeda individual por n&uacute;mero de solicitud o n&uacute;mero
                    de informe
                </td>
            </tr>
            <tr>
                <td>
                    N&uacute;mero de solicitud: <input type="text" id="solicitud" name="solicitud">&nbsp;
                    N&uacute;mero de informe: <input type="text" id="informe" name="informe">
                </td>
                <td><input type="submit" value="Buscar" class="boton2"/></td>
            </tr>
        </table>
    </form>
    <br><br>
    <form action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_reporte_general.php' ?>" method="post" target="_blank">
        <table class="radius" align="center">
            <tr>
                <td class="titulo" colspan="5">B&uacute;squeda general</td>
            </tr>
            <tr>
                <td>
                    Desde: <input type="text" id="desde" name="desde" class="fecha" readonly
                                  onClick="show_calendar(this.id);">&nbsp;
                    Hasta: <input type="text" id="hasta" name="hasta" class="fecha" readonly
                                  onClick="show_calendar(this.id);">
                </td>
                <td>N&uacute;mero de registro: <input type="text" id="registro" name="registro"></td>
                <td>Producto/elemento: <select id="elemento" name="elemento" style="width:130px">
                        <option value="-1">Todos</option>
                        <?php
                        $ROW = $Gestor->obtieneProductosCentinela();
                        for ($x = 0; $x < count($ROW); $x++) {
                            ?>
                            <option value="<?= $ROW[$x]['producto'] ?>"><?= $ROW[$x]['producto'] ?></option>
                            <?php
                        }
                        ?>
                    </select></td>
            </tr>
            <tr>
                <td>N&uacute;mero de solicitud: <input type="text" id="solicitud" name="solicitud"></td>
                <td>N&uacute;mero de informe: <input type="text" id="informe" name="informe"></td>
                <td>Ingrediente: <select id="ingrediente" name="ingrediente" style="width:130px">
                        <option value="-1">Todos</option>
                        <?php
                        $ROW = $Gestor->obtieneIngredientesCentinela();
                        for ($x = 0; $x < count($ROW); $x++) {
                            ?>
                            <option value="<?= $ROW[$x]['ingrediente'] ?>"><?= $ROW[$x]['ingrediente'] ?></option>
                            <?php
                        }
                        ?>
                    </select></td>
                <td>Tipo:&nbsp;
                    <select id="tipo" name="tipo">
                        <option value="-1">Todos</option>
                        <option value="FERT">Fertilizantes</option>
                        <option value="PLAG">Plaguicidas</option>
                    </select>
                </td>
                <td><input type="submit" value="Buscar" class="boton2"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <a href="#" onclick="location.href = 'menu.php'">[Atr&aacute;s]</a>
</center>
<br><?= $Gestor->Encabezado('Z0001', 'p', '') ?>
</body>
</html>