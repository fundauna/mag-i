<?php
define('__MODULO__', 'mantenimientos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _tramites_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW)
    die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js', 5) ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('jquery.maskedinput.min', 'js'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('g7', 'hr', 'Trámites y Servicios Externos :: Detalle Trámites') ?>
<?= $Gestor->Encabezado('G0007', 'e', 'Detalle Trámites') ?>
<center>
    <input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
    <input type="hidden" id="cs" value="<?= $ROW[0]['cs'] ?>"/>
    <table class="radius">
        <tr>
            <td class="titulo" colspan="2">Detalle</td>
        </tr>
        <tr>
            <td>Tipo:</td>
            <td><select id="tipo">
                    <option value="">...</option>
                    <option value="1" <?= ($ROW[0]['tipo'] == '1') ? 'selected' : '' ?>>Permiso sanitario de
                        funcionamiento
                    </option>
                    <option value="2" <?= ($ROW[0]['tipo'] == '2') ? 'selected' : '' ?>>Precursores ICD</option>
                    <option value="3" <?= ($ROW[0]['tipo'] == '3') ? 'selected' : '' ?>>Permiso de circulaci&oacute;n(transporte
                        de sustancias peligrosas)
                    </option>
                    <option value="4" <?= ($ROW[0]['tipo'] == '4') ? 'selected' : '' ?>>Colegio qu&iacute;micos</option>
                    <option value="5" <?= ($ROW[0]['tipo'] == '5') ? 'selected' : '' ?>>Pr&eacute;stamo de equipo
                    </option>
                    <option value="6" <?= ($ROW[0]['tipo'] == '6') ? 'selected' : '' ?>>Renovaci&oacute;n de acreditaci&oacute;n</option>
                    <option value="9" <?= ($ROW[0]['tipo'] == '9') ? 'selected' : '' ?>>Otros</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Descripci&oacute;n:</td>
            <td><input type="text" id="descripcion" value="<?= $ROW[0]['descripcion'] ?>" size="30" maxlength="100">
            </td>
        </tr>
        <tr>
            <td>Requisitos:</td>
            <td><textarea id="requisitos" title="Alfanumérico (5/200)"><?= $ROW[0]['requisitos'] ?></textarea></td>
        </tr>
        <tr>
            <td colspan="2">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Fecha de renovaci&oacute;n:</td>
            <td><input type="text" id="fecha" class="fecha" value="<?= $ROW[0]['fecha'] ?>" readonly
                       onClick="show_calendar(this.id);"></td>
        </tr>
        <tr>
            <td>Fecha de aviso:</td>
            <td><input type="text" id="aviso" class="fecha" value="<?= $ROW[0]['aviso'] ?>" readonly
                       onClick="show_calendar(this.id);"></td>
        </tr>
        <tr>
            <td>Vigencia:</td>
            <td><input type="text" id="vigencia" value="<?= $ROW[0]['vigencia'] ?>" size="30" maxlength="50"></td>
        </tr>
        <tr>
            <td>Adjuntar documento:</td>
            <td><input type="file" name="archivo" id="archivo" class="boton"></td>
        </tr>
        <?php if ($_GET['acc'] == 'M' && file_exists('../../caspha-i/docs/mantenimientos/tramites/' . $_GET['ID'] . '.zip')) { ?>
            <tr>
                <td>Descargar documentos adjuntos:</td>
                <td><a href="../../caspha-i/docs/mantenimientos/tramites/<?= $_GET['ID'] ?>.zip"><img
                                src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar" class="tab2"/></a>
                </td>
            </tr>
        <?php } ?>
    </table>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
</center>
<?= $Gestor->Encabezado('G0007', 'p', '') ?>
</body>
</html>