<?php
define('__MODULO__', 'mantenimientos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _contratos_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>Delfos</title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js', 5) ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('g3', 'hr', 'Tr�mites y Servicios Externos :: Detalle de contrato') ?>
<?= $Gestor->Encabezado('G0003', 'e', 'Detalle de contrato') ?>
<center>
    <form name="form" id="form" method="post" enctype="multipart/form-data"
          action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
        <input type="hidden" id="accion" name="accion" value="<?= $_GET['acc'] ?>"/>
        <input type="hidden" id="cs" name="cs" value="<?= $ROW[0]['cs'] ?>"/>
        <input type="hidden" id="prov" name="prov" value="<?= $ROW[0]['proveedor'] ?>"/>
        <table class="radius" width="80%">
            <tr>
                <td class="titulo" colspan="2">Detalle</td>
            </tr>
            <tr>
                <td>N&uacute;mero de contrato:</td>
                <td><input type="text" id="contrato" name="contrato" value="<?= $ROW[0]['contrato'] ?>" size="20"
                           maxlength="20"></td>
            </tr>
            <tr>
                <td>Tipo de Contrato:</td>
                <td><select id="tipo" name="tipo">
                        <option value="">...</option>
                        <?php
                        $ROW2 = $Gestor->TiposMuestra();
                        for ($x = 0; $x < count($ROW2); $x++) {
                            ?>
                            <option value="<?= $ROW2[$x]['cs'] ?>"
                                    <?= ($ROW[0]['tipo'] == $ROW2[$x]['cs']) ? 'selected' : '' ?>><?= $ROW2[$x]['nombre'] ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Proveedor:</td>
                <td><input type="text" id="proveedor" name="proveedor" value="<?= $ROW[0]['nombre'] ?>" class="lista"
                           readonly onclick="ProveedoresLista(0)"/></td>
            </tr>
            <tr>
                <td class="titulo" colspan="2">Datos de los equipos&nbsp;<img onclick="ActivoAgregar()"
                                                                              src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                                                                              title="Agregar l�nea" class="tab"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%" style="font-size:12px">
                        <tr>
                            <td></td>
                            <td><strong>Equipo</strong></td>
                            <td><strong>Nombre</strong></td>
                            <td><strong>Patrimonio</strong></td>
                            <td><strong>N&uacute;mero de Serie</strong></td>
                        </tr>
                        <tbody id="lolo">
                        <?php
                        $ROW2 = $Gestor->EquiposMuestra();
                        for ($x = 0; $x < count($ROW2); $x++) {
                            ?>
                            <tr>
                                <td><?= $x + 1 ?>.</td>
                                <td><input type="text" id="tmp_equipo<?= $x ?>" value="<?= $ROW2[$x]['codigo'] ?>"
                                           class="lista" readonly onclick="EquiposLista(<?= $x ?>)"/>
                                    <input type="hidden" id="equipo<?= $x ?>" name="equipo[]"
                                           value="<?= $ROW2[$x]['id'] ?>"/></td>
                                <td><?= $ROW2[$x]['nombre'] ?></td>
                                <td><?= $ROW2[$x]['patrimonio'] ?></td>
                                <td><?= $ROW2[$x]['serie'] ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="titulo" colspan="2">Montos del contrato&nbsp;<img onclick="MontoAgregar()"
                                                                             src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                                                                             title="Agregar l�nea" class="tab"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%" style="font-size:12px">
                        <tr>
                            <td><strong>A&ntilde;o</strong></td>
                            <td><strong>Monto</strong></td>
                            <td><strong>Moneda</strong></td>
                            <td><strong>Observaciones</strong></td>
                        </tr>
                        <tbody id="lolo1">
                        <?php
                        $ROW2 = $Gestor->MontosMuestra();
                        for ($x = 0; $x < count($ROW2); $x++) {
                            ?>
                            <tr>
                                <td><input type="text" name="ano[]" size="4" maxlength="4"
                                           value="<?= $ROW2[$x]['ano'] ?>"/></td>
                                <td><input type="text" name="monto[]"
                                           value="<?= $Gestor->Formato($ROW2[$x]['monto']) ?>" onblur="_FLOAT(this);"
                                           class="monto"/></td>
                                <td><select name="moneda[]">
                                        <option value="">...</option>
                                        <option value="0" <?= ($ROW2[$x]['moneda'] == '0') ? 'selected' : '' ?>>
                                            Colones
                                        </option>
                                        <option value="1" <?= ($ROW2[$x]['moneda'] == '1') ? 'selected' : '' ?>>D&oacute;lares</option>
                                    </select>
                                </td>
                                <td><input type="text" name="obs[]" maxlength="50" value="<?= $ROW2[$x]['obs'] ?>"/>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td>Fecha de vencimiento:</td>
                <td><input type="text" id="fecha" name="fecha" value="<?= $ROW[0]['fecha'] ?>" class="fecha" readonly
                           onClick="show_calendar(this.id);"></td>
            </tr>
            <tr>
                <td>Fecha de aviso:</td>
                <td><input type="text" id="aviso" name="aviso" value="<?= $ROW[0]['aviso'] ?>" class="fecha" readonly
                           onClick="show_calendar(this.id);"></td>
            </tr>
            <tr>
                <td>Adjuntar documento:</td>
                <td><input type="file" name="archivo" id="archivo" class="boton"></td>
            </tr>
            <?php
            if (file_exists("../../caspha-i/docs/mantenimientos/contratos/{$ROW[0]['cs']}.zip")) {
                ?>
                <tr>
                    <td>Ver documento:</td>
                    <td><img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar"
                             onclick="DocumentosZip('<?= $ROW[0]['cs'] ?>', '<?= __MODULO__ ?>/contratos')"
                             class="tab3"/></td>
                </tr>
            <?php } ?>
        </table>
        <br/>
        <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos('<?= $_GET['acc'] ?>')">
    </form>
</center>
<?= $Gestor->Encabezado('G0003', 'p', '') ?>
</body>
</html>