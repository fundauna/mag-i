<?php
define('__MODULO__', 'mantenimientos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _tramites();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title><?= $Gestor->Title() ?></title>
        <?php $Gestor->Incluir('estilo', 'css'); ?>
        <?php $Gestor->Incluir('window', 'js'); ?>
        <?php $Gestor->Incluir('calendario', 'js'); ?>
        <?php $Gestor->Incluir('validaciones', 'js'); ?>
        <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
        <?php $Gestor->Incluir('', 'jtables', 2); ?>
        <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    </head>
    <body>
        <?php $Gestor->Incluir('g6', 'hr', 'Tr�mites y Servicios Externos :: Tr�mites') ?>
        <?= $Gestor->Encabezado('G0006', 'e', 'Tr�mites') ?>
        <center>
            <form name="formulario" action="<?= basename(__FILE__) ?>" method="post">
                <table class="radius" align="center" style="font-size:12px;width:650px">
                    <tr><td class="titulo" colspan="4">Filtro</td></tr>
                    <tr>
                        <td>Tipo:&nbsp;
                            <select name="tipo" style="width:100px">
                                <option value="">...</option>
                                <option value="1" <?= ($_POST['tipo'] == '1') ? 'selected' : '' ?>>Permiso sanitario de funcionamiento</option>
                                <option value="2" <?= ($_POST['tipo'] == '2') ? 'selected' : '' ?>>Precursores ICD</option>
                                <option value="3" <?= ($_POST['tipo'] == '3') ? 'selected' : '' ?>>Permiso de Circulaci&oacute;n(transporte de sustancias peligrosas)</option>
                                <option value="4" <?= ($_POST['tipo'] == '4') ? 'selected' : '' ?>>Colegio Qu&iacute;micos</option>
                                <option value="5" <?= ($_POST['tipo'] == '5') ? 'selected' : '' ?>>Pr&eacute;stamo de equipo</option>
                                <option value="6" <?= ($_POST['tipo'] == '6') ? 'selected' : '' ?>>Renovaci&oacute;n de acreditaci&oacute;n</option>
                                <option value="9" <?= ($_POST['tipo'] == '9') ? 'selected' : '' ?>>Otros</option>	
                                <option value="-99" <?= ($_POST['tipo'] == '-99') ? 'selected' : '' ?>>Todos</option>	
                            </select>	
                        </td>
                        <td>
                            Fecha:
                            <input type="text" id="desde" name="desde" class="fecha" value="<?= $_POST['desde'] ?>" readonly onClick="show_calendar(this.id);">&nbsp;
                            <input type="text" id="hasta" name="hasta" class="fecha" value="<?= $_POST['hasta'] ?>" readonly onClick="show_calendar(this.id);">
                        </td>
                        <td>Descripci&oacute;n: <input type="text" name="nombre" id="nombre" value="<?= $_POST['nombre'] ?>"/></td>		
                        <td><input type="button" value="Buscar" class="boton2" onclick="TramitesBuscar(this)" /></td>	
                    </tr>
                </table>
            </form>
            <br />
            <div id="container" style="width:650px">
                <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                    <thead>
                        <tr>
                            <th>Descripci&oacute;n</th>
                            <th>Tipo</th>	
                            <th>Renovaci&oacute;n</th>
                            <th>Plazo</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $ROW = $Gestor->TramitesMuestra();
                        for ($x = 0; $x < count($ROW); $x++) {
                            ?>
                            <tr class="gradeA" align="center">
                                <td><a href="#" onclick="TramitesModifica('<?= $ROW[$x]['cs'] ?>')"><?= $ROW[$x]['descripcion'] ?></a></td>
                                <td><?= $ROW[$x]['tipoD'] ?></td>
                                <td><?= $ROW[$x]['fecha1'] ?></td>	
                                <td <?= $ROW[$x]['plazo'] < 1 ? 'style="color:red"' : '' ?>><?= $ROW[$x]['plazo'] ?> d&iacute;as</td>
                                <td>
                                    <?php if (file_exists('../../caspha-i/docs/mantenimientos/tramites/'.$ROW[$x]['cs'].'.zip')) { ?>
                                    <a href="../../caspha-i/docs/mantenimientos/tramites/<?=$ROW[$x]['cs']?>.zip"><img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar" class="tab2"/></a>
                                    <?php } ?>
                                    <img onclick="TramitesElimina('<?= $ROW[$x]['cs'] ?>')" src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar" class="tab2"/>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <br /><input type="button" value="Agregar" class="boton" onClick="TramitesAgrega();">
        </center>
        <?= $Gestor->Encabezado('G0006', 'p', '') ?>
    </body>
</html>