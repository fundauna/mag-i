<?php
define('__MODULO__', 'mantenimientos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _tipos_contratos();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>Delfos</title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('g5', 'hr', 'Tr�mites y Servicios Externos :: Tipos de contrato') ?>
<?= $Gestor->Encabezado('G0005', 'e', 'Tipos de contrato') ?>
<table class="radius" align="center" width="255px">
    <tr>
        <td class="titulo" colspan="2">Tipos Registrados</td>
    </tr>
    <tr>
        <td colspan="2">
            <select size="10" style="width:250px" onclick="marca(this);">
                <?php
                $ROW = $Gestor->TiposMuestra();
                for ($x = 0; $x < count($ROW); $x++) {
                    echo "<option cs='{$ROW[$x]['cs']}' nombre='{$ROW[$x]['nombre']}'>{$ROW[$x]['nombre']}</option>";
                }
                unset($ROW);
                ?>
            </select>
        </td>
    </tr>
</table>
<br/>
<input type="hidden" id="id">
<table class="radius" align="center" width="255px">
    <tr>
        <td class="titulo" colspan="2">Detalle</td>
    </tr>
    <tr>
        <td><b>Nombre:&nbsp;</b></td>
        <td><input type="text" id="nombre" maxlength="60"></td>
    </tr>
</table>
<br/>
<center>
    <input id="new" type="button" class="boton" value="Limpiar" onclick="desmarca();" disabled>&nbsp;
    <input id="add" type="button" class="boton" value="Agregar" onclick="modificar('I');">&nbsp;
    <input id="mod" type="button" class="boton" value="Modificar" onclick="modificar('M');" disabled>&nbsp;
    <input id="del" type="button" class="boton" value="Eliminar" onclick="modificar('D');" disabled>
</center>
<?= $Gestor->Encabezado('G0005', 'p', '') ?>
</body>
</html>