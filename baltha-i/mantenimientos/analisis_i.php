<?php
define('__MODULO__', 'mantenimientos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _analisis_i();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('g1', 'hr', 'Cotizaciones :: An&aacute;lisis Externos') ?>
<?= $Gestor->Encabezado('G0001', 'e', 'An&aacute;lisis Externos') ?>
<br/>
<table class="radius" align="center" width="410px">
    <tr>
        <td class="titulo">An&aacute;lisis Registrados</td>
    </tr>
    <tr>
        <td align="center">
            <select size="10" style="width:400px" onclick="marca(this);">
                <?php
                $ROW = $Gestor->AnalisisMuestra();
                for ($x = 0; $x < count($ROW); $x++) {
                    $ROW[$x]['costo'] = $Gestor->Formato($ROW[$x]['costo']);
                    echo "<option id='{$ROW[$x]['id']}' nombre='{$ROW[$x]['nombre']}' tarifa='{$ROW[$x]['tarifa']}' costo='{$ROW[$x]['costo']}' descuento='{$ROW[$x]['descuento']}'>{$ROW[$x]['id']} - {$ROW[$x]['nombre']}</option>";
                }

                unset($ROW);
                ?>
            </select>
        </td>
    </tr>
</table>
<br/>
<table class="radius" align="center" width="410px">
    <tr>
        <td class="titulo" colspan="2">Detalle</td>
    </tr>
    <tr>
        <td><b>Id:&nbsp;</b></td>
        <td><input type="text" id="id" maxlength="3" onBlur="_INT(this);" size="2"></td>
    </tr>
    <tr>
        <td><b>Nombre:&nbsp;</b></td>
        <td><input type="text" id="nombre" size="40" maxlength="100"></td>
    </tr>
    <tr>
        <td><b>Cod. Tarifa:&nbsp;</b></td>
        <td><input type="text" id="tarifa" size="4" maxlength="4"></td>
    </tr>
    <tr>
        <td><b>Costo:&nbsp;</b></td>
        <td><input type="text" id="costo" maxlength="10" value="0.0" onblur="_FLOAT(this);" class="monto"></td>
    </tr>
    <tr>
        <td><strong>Aplica descuento?:</strong></td>
        <td><select id="descuento" name="descuento">
                <option value="">...</option>
                <option value="0">No</option>
                <option value="1">S&iacute;</option>
            </select>
        </td>
    </tr>
</table>
<br/>
<center>
    <input id="new" type="button" class="boton" value="Limpiar" onclick="desmarca();" disabled>&nbsp;
    <input id="add" type="button" class="boton" value="Agregar" onclick="modificar('I');">&nbsp;
    <input id="mod" type="button" class="boton" value="Modificar" onclick="modificar('M');" disabled>&nbsp;
    <input id="del" type="button" class="boton" value="Eliminar" onclick="modificar('D');" disabled>
</center>
<?= $Gestor->Encabezado('G0001', 'p', '') ?>
</body>
</html>