<?php
define('__MODULO__', 'mantenimientos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _analisis_e();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('g0', 'hr', 'Tr�mites y Servicios Externos :: Servicios Externos') ?>
<?= $Gestor->Encabezado('G0000', 'e', 'Servicios Externos') ?>
<table class="radius" align="center" width="32%">
    <tr>
        <td class="titulo">An&aacute;lisis Registrados</td>
    </tr>
    <tr>
        <td align="center">
            <select id="combo" size="10" style="width:100%" onclick="marca(this);">
                <?php
                $ROW = $Gestor->AnalisisExtMuestra();
                for ($x = 0; $x < count($ROW); $x++) {
                    echo "<option id='{$ROW[$x]['id']}' nombre='{$ROW[$x]['nombre']}' tipo='{$ROW[$x]['tipo']}' fecha='{$ROW[$x]['fecha']}' proveedor='{$ROW[$x]['proveedor']}' prov='{$ROW[$x]['prov']}' descripcion='{$ROW[$x]['descripcion']}'>{$ROW[$x]['nombre']}</option>";
                }

                unset($ROW);
                ?>
            </select>
        </td>
    </tr>
</table>
<br/>
<form name="form" id="form" method="post" enctype="multipart/form-data"
      action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
    <table class="radius" align="center" width="32%">
        <tr>
            <td class="titulo" colspan="2">Detalle</td>
        </tr>
        <tr>
            <td><b>Nombre:&nbsp;</b></td>
            <td><input type="text" id="nombre" name="nombre" size="20" maxlength="30"></td>
        </tr>
        <tr>
            <td><b>Tipo:&nbsp;</b></td>
            <td><select id="tipo" name="tipo" style="width:100px">
                    <option value="">...</option>
                    <option value="1">An&aacute;lisis Ambiental</option>
                    <option value="2">Salud Ocupacional</option>
                    <option value="3">Otro</option>
                </select></td>
        </tr>
        <tr>
            <td><b>Fecha de An&aacute;lisis:&nbsp;</b></td>
            <td><input type="text" id="fecha" name="fecha" class="fecha" readonly onClick="show_calendar(this.id);">
            </td>
        </tr>
        <tr>
            <td><b>Proveedor:&nbsp;</b></td>
            <td><input type="text" id="proveedor" name="proveedor" class="lista" readonly
                       onclick="ProveedoresLista(0)"/></td>
        </tr>
        <tr>
            <td>Descripci&oacute;n:</td>
            <td><textarea id="descripcion" name="descripcion"></textarea></td>
        </tr>
        <tr class="blokeado">
            <td>Adjuntar documento:</td>
            <td><input type="file" name="archivo" id="archivo" class="boton"></td>
        </tr>
        <tr id="lolo" style="display:none">
            <td>Ver documento:</td>
            <td><img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar"
                     onclick="DocumentosZip('<?= __MODULO__ ?>')" class="tab3"/></td>
        </tr>
        <input type="hidden" id="id" name="id"/>
        <input type="hidden" id="prov" name="prov"/>
        <input type="hidden" id="accion" name="accion"/>
    </table>
</form>
<br/>
<center>
    <input id="new" type="button" class="boton" value="Limpiar" onclick="desmarca();" disabled>&nbsp;
    <input id="add" type="button" class="boton" value="Agregar" onclick="modificar('I');">&nbsp;
    <input id="mod" type="button" class="boton" value="Modificar" onclick="modificar('M');" disabled>&nbsp;
    <input id="del" type="button" class="boton" value="Eliminar" onclick="modificar('D');" disabled>
</center>
<?= $Gestor->Encabezado('G0000', 'p', '') ?>
</body>
</html>