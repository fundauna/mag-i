<?php
define('__MODULO__', 'mantenimientos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _contratos();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>Delfos</title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('g2', 'hr', 'Tr�mites y Servicios Externos :: Cat�logo de contratos') ?>
<?= $Gestor->Encabezado('G0002', 'e', 'Cat�logo de contratos') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" style="font-size:12px;width:650px;">
            <tr>
                <td class="titulo" colspan="3">Filtro</td>
            </tr>
            <tr>
                <td><input type="radio" id="lolo1" name="lolo" onclick="marca(1)"
                           value="1" <?php if ($_POST['lolo'] == 1) echo 'checked'; ?> />&nbsp;Contrato: <input
                            type="text" id="id" name="id" value="<?= $_POST['id'] ?>" size="20" maxlength="20"/></td>
                <td><input type="radio" id="lolo2" name="lolo" onclick="marca(2)"
                           value="2" <?php if ($_POST['lolo'] == 2) echo 'checked'; ?>/>&nbsp;Proveedor: <input
                            type="text" name="nombre" id="nombre" readonly value="<?= $_POST['nombre'] ?>"/></td>
                <td><input type="button" value="Buscar" class="boton2" onclick="ContratosBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:650px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>No. Contrato</th>
                <th>Tipo</th>
                <th>Proveedor</th>
                <th>Fecha</th>
                <th>Plazo</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->ContratosMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#" onclick="ContratosModifica('<?= $ROW[$x]['cs'] ?>')"><?= $ROW[$x]['contrato'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['tipo'] ?></td>
                    <td><?= $ROW[$x]['nombre'] ?></td>
                    <td><?= $ROW[$x]['fecha'] ?></td>
                    <td <?= $ROW[$x]['plazo'] < 1 ? 'style="color:red"' : '' ?>><?= $ROW[$x]['plazo'] ?>d&iacute;as
                    </td>
                    <td><img onclick="ContratosElimina('<?= $ROW[$x]['cs'] ?>')"
                             src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar" class="tab2"/></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/><input type="button" value="Agregar" class="boton" onClick="ContratosAgrega();">
</center>
<?= $Gestor->Encabezado('G0002', 'p', '') ?>
</body>
</html>