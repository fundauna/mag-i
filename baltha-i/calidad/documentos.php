<?php
define('__MODULO__', 'calidad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _documentos();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('b3', 'hr', 'Gesti�n de Calidad :: Documentos') ?>
<?= $Gestor->Encabezado('B0003', 'e', 'Documentos') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" style="font-size:12px;width:750px;">
            <tr>
                <td class="titulo" colspan="5">Filtro</td>
            </tr>
            <tr>
                <td>Tipo:&nbsp;
                    <select name="tipo">
                        <option value="">...</option>
                        <?php
                        $ROW = $Gestor->DocumentosFiltro();
                        for ($x = 0; $x < count($ROW); $x++) {
                            ?>
                            <option <?= $_POST['tipo'] == $ROW[$x]['id'] ? 'selected' : '' ?>
                                    value="<?= $ROW[$x]['id'] ?>"><?= $ROW[$x]['sigla'] ?>(<?= $ROW[$x]['nombre'] ?>)
                            </option>
                        <?php } ?>
                    </select>
                </td>
                <td>Laboratorio:&nbsp;
                    <select name="lab">
                        <option value="">...</option>
                        <option <?= $_POST['lab'] == '1' ? 'selected' : '' ?> value="1">LRE</option>
                        <option <?= $_POST['lab'] == '2' ? 'selected' : '' ?> value="2">LDP</option>
                        <option <?= $_POST['lab'] == '3' ? 'selected' : '' ?> value="3">LCC</option>
                        <option <?= $_POST['lab'] == '4' ? 'selected' : '' ?> value="4">LAB</option>
                    </select>

                </td>
                <td><input type="radio" id="lolo1" name="lolo" onclick="marca(1)"
                           value="1" <?php if ($_POST['lolo'] == 1) echo 'checked'; ?> />&nbsp;C&oacute;digo: <input
                            type="text" id="codigo" name="codigo" value="<?= $_POST['codigo'] ?>"/></td>
                <td><input type="radio" id="lolo2" name="lolo" onclick="marca(2)"
                           value="2" <?php if ($_POST['lolo'] == 2) echo 'checked'; ?>/>&nbsp;Nombre: <input type="text"
                                                                                                             name="nombre"
                                                                                                             id="nombre"
                                                                                                             readonly
                                                                                                             value="<?= $_POST['nombre'] ?>"/>
                </td>
                <td><input type="button" value="Buscar" class="boton2" onclick="DocumentosBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:750px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>C&oacute;digo</th>
                <th>Nombre</th>
                <th>Versi&oacute;n</th>
                <th>Estado</th>
                <th>Descargar</th>
                <th>Anular</th>
                <th>Eliminar</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->DocumentosMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#" onclick="DocumentosModifica('<?= $ROW[$x]['cs'] ?>')"><?= $ROW[$x]['codigo'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['descripcion'] ?></td>
                    <td><?= $ROW[$x]['version'] ?></td>
                    <td <?= $ROW[$x]['invisible'] == '0' ? 'style="color:red"' : '' ?>><?= $ROW[$x]['estado'] ?></td>
                    <td>
                        <?php if (!$Gestor->Incluir("{$ROW[$x]['cs']}-{$ROW[$x]['version']}", 'doc', __MODULO__)) { ?>
                            <img src="<?php $Gestor->Incluir('unk', 'bkg') ?>"/>
                        <?php } else { ?>
                            <img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar"
                                 onclick="DocumentosZip('<?= "{$ROW[$x]['cs']}-{$ROW[$x]['version']}" ?>', '<?= __MODULO__ ?>')"
                                 class="tab3"/>
                        <?php } ?>
                    </td>
                    <td><img onclick="DocumentosAnula('<?= $ROW[$x]['cs'] ?>','<?= $ROW[$x]['version'] ?>', 1)"
                             src="<?php $Gestor->Incluir('menos', 'bkg') ?>" title="Anular" class="tab2"/></td>
                    <td><img onclick="DocumentosElimina('<?= $ROW[$x]['cs'] ?>','<?= $ROW[$x]['version'] ?>', 3)"
                             src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar" class="tab2"/></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/><input type="button" value="Agregar" class="boton" onClick="DocumentosAgrega();">
</center>
<br/><?= $Gestor->Encabezado('B0003', 'p', '') ?>
</body>
</html>