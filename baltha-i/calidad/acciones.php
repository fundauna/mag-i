<?php
define('__MODULO__', 'calidad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _acciones();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('b0', 'hr', 'Gesti�n de Calidad :: Acciones Correctivas/Preventivas') ?>
<?= $Gestor->Encabezado('B0000', 'e', 'Acciones Correctivas/Preventivas') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" style="width:500px">
            <tr>
                <td class="titulo" colspan="4">Filtro</td>
            </tr>
            <tr>
                <td>Tipo: <select name="tipo">
                        <option value="C">AC</option>
                        <option value="P" <?= ($_POST['tipo'] == 'P') ? 'selected' : '' ?>>AP</option>
                        <option value="" <?= ($_POST['tipo'] == '') ? 'selected' : '' ?>>Todas</option>
                    </select>
                </td>
                <td>Desde: <input type="text" id="desde" name="desde" class="fecha" value="<?= $_POST['desde'] ?>"
                                  readonly onClick="show_calendar(this.id);">&nbsp;
                    Hasta: <input type="text" id="hasta" name="hasta" class="fecha" value="<?= $_POST['hasta'] ?>"
                                  readonly onClick="show_calendar(this.id);">&nbsp;
                </td>
                <td>Estado: <select name="estado">
                        <option value="A">Abierta</option>
                        <option value="C" <?= ($_POST['estado'] == 'C') ? 'selected' : '' ?>>Cerrada</option>
                        <option value="" <?= ($_POST['estado'] == '') ? 'selected' : '' ?>>Todas</option>
                    </select>
                </td>
                <td><input type="button" value="Buscar" class="boton2" onclick="AccionesBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:500px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>C&oacute;digo</th>
                <th>Fecha</th>
                <th>Tipo</th>
                <th>Estado</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->AccionesMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#" onclick="AccionesModificar('<?= $ROW[$x]['cs'] ?>')"><?= $ROW[$x]['codigo'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['fecha'] ?></td>
                    <td><?= $ROW[$x]['tipo'] == 'C' ? 'AC' : 'AP' ?></td>
                    <td><?= $ROW[$x]['estado'] == 'C' ? 'Cerrada' : 'Abierta' ?></td>
                    <td><img onclick="AccionesElimina('<?= $ROW[$x]['cs'] ?>')"
                             src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar" class="tab2"/></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/>
    <input type="button" class="boton" value="Agregar" onclick="AccionesAgregar()"/>
</center>
<br/><?= $Gestor->Encabezado('B0000', 'p', '') ?>
</body>
</html>