<?php
define('__MODULO__', 'calidad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _firmas();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js', 5) ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('b5', 'hr', 'Gesti�n de calidad :: Transladar documentos') ?>
<?= $Gestor->Encabezado('B0015', 'e', 'Transladar documentos') ?>
<center>
    <input type="hidden" id="padre" value="<?= $_GET['ID'] ?>"/>
    <input type="hidden" id="yo" value="<?= $_GET['YO'] ?>"/>
    <table class="radius" align="center" width="400px">
        <tr>
            <td class="titulo" colspan="2">Documento padre</td>
        </tr>
        <tr>
            <td><strong>C&oacute;digo:</strong></td>
            <td><?= $_GET['COD'] ?></td>
        </tr>
        <tr>
            <td><strong>Nombre:</strong></td>
            <td><?= $_GET['NOMBRE'] ?></td>
        </tr>
    </table>
    <br/>
    <table class="radius" align="center" width="400px">
        <tr>
            <td><strong>Enviar a usuario:</strong></td>
            <td><input type="text" id="tmp" name="tmp" class="lista" readonly onclick="UsuariosLista()"/>
                <input type="hidden" id="usuario" name="usuario"/></td>
        </tr>
        <tr>
            <td><strong>Petici&oacute;n de:</strong></td>
            <td><select id="peticion" name="peticion">
                    <option value=''>...</option>
                    <!-- PERMISOS DEFINIDOS EN SEG003, MAG001-->
                    <option value="99">Revisar</option>
                    <option value="93">Aprobar</option>
                    <!-- -->
                </select>
            </td>
        </tr>
        <tr>
            <td><strong>Detalle de la notificaci&oacute;n:</strong></td>
            <td><input type="text" id="detalle" maxlength="50" style="width:95%"/></td>
        </tr>
    </table>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <br/><br/>
    <table class="radius" align="center" width="400px">
        <tr>
            <td class="titulo" colspan="4">Historial de Firmas</td>
        </tr>
        <tr>
            <td><strong>Usuario:</strong></td>
            <td><strong>Fecha:</strong></td>
            <td><strong>Acci&oacute;n:</strong></td>
        </tr>
        <?php
        $ROW = $Gestor->DocumentosFirmas();
        for ($x = 0; $x < count($ROW); $x++) {
            ?>
            <tr>
                <td><?= $ROW[$x]['nombre'] . ' ' . $ROW[$x]['ap1'] ?></td>
                <td><?= $ROW[$x]['fecha1'] ?></td>
                <td>
                    <?php
                    echo $Gestor->Tipo($ROW[$x]['accion']);
                    if ($ROW[$x]['accion'] == '-1') echo " <font title='{$ROW[$x]['jus']}' style='cursor:help;font-weight:bold'>[?]</font>";
                    ?>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
</center>
<br><?= $Gestor->Encabezado('B0015', 'p', '') ?>
</body>
</html>