<?php
define('__MODULO__', 'calidad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _encabezados_detalle();
$ROW = $Gestor->ObtieneDatos();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('q9', 'hr', 'Gesti&oacute;n de Calidad :: Mantenimiento de Encabezados') ?>
<?= $Gestor->Encabezado('Q0009', 'e', 'Mantenimiento de Encabezados') ?>
<br/>
<form name="form" id="form" method="post" enctype="multipart/form-data"
      action="../../caspha-i/shell/calidad/_encabezados_detalle.php">
    <input type="hidden" id="id" name="id" value="<?= $_GET['id'] ?>"/>
    <input type="hidden" id="acc" name="acc" value="<?= $_GET['acc'] ?>"/>
    <br/>
    <table class="radius" align="center" width="400">
        <tr>
            <td class="titulo" colspan="2">Datos Registradas</td>
        </tr>
        <tr>
            <td>Hoja a modificar</td>
            <td><?= $_GET['id'] ?></td>
        </tr>
        <tr>
            <td>Plantilla de Encabezado</td>
            <td>
                <select id="encabezado" name="encabezado" required>
                    <option value="">...</option>
                    <?php
                    $ROW2 = $Gestor->PlantillaEncabezadosMuestra('1', '', 'e');
                    for ($y = 0; $y < count($ROW2); $y++) {
                        ?>
                        <option <?= $ROW2[$y]['id'] == $ROW[0]['encabezado'] ? 'selected' : '' ?>
                                value="<?= $ROW2[$y]['id'] ?>"><?= $ROW2[$y]['nombre'] ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Plantilla de Pie de P&aacute;gina</td>
            <td>
                <select id="pie" name="pie" required>
                    <option value="">...</option>
                    <?php
                    $ROW2 = $Gestor->PlantillaEncabezadosMuestra('1', '', 'p');
                    for ($y = 0; $y < count($ROW2); $y++) {
                        ?>
                        <option <?= $ROW2[$y]['id'] == $ROW[0]['pie'] ? 'selected' : '' ?>
                                value="<?= $ROW2[$y]['id'] ?>"><?= $ROW2[$y]['nombre'] ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr>
            </td>
        </tr>
        <tr>
            <td>C&oacute;digo de p&aacute;gina</td>
            <td><input type="text" name="codigo" value="<?= $ROW[0]['codigo'] ?>" required/></td>
        </tr>
        <tr>
            <td>Versi&oacute;n de p&aacute;gina</td>
            <td><input type="text" name="version" value="<?= $ROW[0]['version'] ?>" required/></td>
        </tr>
        <tr>
            <td>Nombre de p&aacute;gina</td>
            <td><textarea name="nombre" style="width:98%" required><?= $ROW[0]['nombre'] ?></textarea></td>
        </tr>
        <tr>
            <td>Rige a partir de</td>
            <td><input type="text" name="rige" value="<?= $ROW[0]['rige'] ?>" required/></td>
        </tr>
        <tr>
            <td colspan="2">
                <hr>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <b>Elabor&oacute;</b>
            </td>
        </tr>
        <tr>
            <td>Nombre:</td>
            <td>
                <textarea name="enombre" style="width:98%"><?= $ROW[0]['enombre'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td>Puesto:</td>
            <td>
                <textarea name="epuesto" style="width:98%"><?= $ROW[0]['epuesto'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td>Fecha:</td>
            <td>
                <input name="efecha" value="<?= $ROW[0]['efecha'] ?>"/>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <b>Revis&oacute;</b>
            </td>
        </tr>
        <tr>
            <td>Nombre:</td>
            <td>
                <textarea name="rnombre" style="width:98%"><?= $ROW[0]['rnombre'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td>Puesto:</td>
            <td>
                <textarea name="rpuesto" style="width:98%"><?= $ROW[0]['rpuesto'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td>Fecha:</td>
            <td>
                <input name="rfecha" value="<?= $ROW[0]['rfecha'] ?>"/>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <b>Aprob&oacute;</b>
            </td>
        </tr>
        <tr>
            <td>Nombre:</td>
            <td>
                <textarea name="anombre" style="width:98%"><?= $ROW[0]['anombre'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td>Puesto:</td>
            <td>
                <textarea name="apuesto" style="width:98%"><?= $ROW[0]['apuesto'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td>Fecha:</td>
            <td>
                <input name="afecha" value="<?= $ROW[0]['afecha'] ?>"/>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center"><br><br><input type="submit" value="Aceptar" class="boton"></td>
        </tr>
    </table>
</form>
<br/>
<br/><?= $Gestor->Encabezado('Q0009', 'p', '') ?>
</body>
</html>