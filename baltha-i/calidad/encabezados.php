<?php
define('__MODULO__', 'calidad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _encabezados();
$ROW = $Gestor->PlantillaEncabezadosMuestra();
$ROW2 = $Gestor->EncabezadoMuestra();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>

</head>
<body>
<?php $Gestor->Incluir('q9', 'hr', 'Gesti&oacute;n de Calidad :: Mantenimiento de Encabezados') ?>
<?= $Gestor->Encabezado('Q0009', 'e', 'Mantenimiento de Encabezados') ?>
<br/>
<center>
    <table class="radius" align="center" width="300">
        <tr>
            <td class="titulo" colspan="2">Plantillas Registradas</td>
        </tr>
        <tr>
            <td align="center">
                <select id="enc">
                    <option value="">...</option>
                    <?php for ($x = 0; $x < count($ROW); $x++) { ?>
                        <option tipo='<?= $ROW[$x]['tipo'] ?>'
                                value='<?= $ROW[$x]['id'] ?>'><?= $ROW[$x]['nombre'] ?></option>
                    <?php } ?>
                </select>
            </td>
            <td align="right"><input type="button" value="Modificar" class="boton2" onClick="LabB('M');">&nbsp;<input
                        type="button" value="Agregar" class="boton2" onClick="LabB('I');"></td>
        </tr>
    </table>
    <br/>
    <div id="container" style="width:750px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>Hoja</th>
                <th>Encabezado</th>
                <th>Pie de P&aacute;gina</th>
                <th>Opciones</th>
            </tr>
            </thead>
            <tbody>
            <?php for ($y = 0; $y < count($ROW2); $y++) {
                $ceros = '00';
                if ($ROW2[$y]['codigo'] < 10) {
                    $ceros .= '0';
                }
                $sigla = $ROW2[$y]['sigla'] . $ceros . $ROW2[$y]['codigo'];
                ?>
                <tr class="gradeA" align="center">
                    <td><?= $sigla ?></td>
                    <td><?= $Gestor->obtieneNombreEncPie($ROW2[$y]['encabezado']) ?></td>
                    <td><?= $Gestor->obtieneNombreEncPie($ROW2[$y]['pie']) ?></td>
                    <td><img onclick="LabA('M', '<?= $ROW2[$y]['sigla'] ?>', '<?= $ROW2[$y]['codigo'] ?>')"
                             src="<?php $Gestor->Incluir('previa', 'bkg') ?>" title="Modificar Cambios" class="tab2"/>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</center>
<br/><?= $Gestor->Encabezado('Q0009', 'p', 'Mantenimiento de Encabezados') ?>
</body>
</html>