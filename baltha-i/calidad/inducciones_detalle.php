<?php
define('__MODULO__', 'calidad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _inducciones_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW)
    die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('graficador', 'gfx'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <script>
        img1 = '<?php $Gestor->Incluir('closed', 'bkg') ?>';
        img2 = '<?php $Gestor->Incluir('downboxed', 'bkg') ?>';
        rutaimg = '<?php $Gestor->Incluir('mod', 'bkg') ?>';
    </script>
</head>
<body>
<?php $Gestor->Incluir('b7', 'hr', 'Gesti�n de Calidad :: Necesidades de formaci�n') ?>
<?= $Gestor->Encabezado('B0007', 'e', 'Necesidades de formaci�n') ?>
<center>
    <input type="hidden" id="_ID" name="" value="<?= $_GET['ID'] ?>"/>
    <input type="hidden" id="id" name="id" value="<?= $ROW[0]['id'] ?>"/>
    <table width="95%" class="radius">
        <tr>
            <td colspan="2" class="titulo">Informaci&oacute;n</td>
            <td colspan="2" class="titulo">Requisitos</td>
        </tr>
        <tr>
            <td>Nombre:</td>
            <td><?= $ROW[0]['nombre'], ' ', $ROW[0]['ap1'], ' ', $ROW[0]['ap2'] ?></td>
            <td align="right">Fotocopia c&eacute;dula</td>
            <td><select id="req_cedula">
                    <option value="">NA</option>
                    <option value="0" <?= ($ROW[0]['req_cedula'] == '0') ? 'selected' : '' ?>>No</option>
                    <option value="1" <?= ($ROW[0]['req_cedula'] == '1') ? 'selected' : '' ?>>S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td>Siglas:</td>
            <td><input type="text" id="siglas" name="siglas" size="4" maxlength="4" value="<?= $ROW[0]['siglas'] ?>">
            </td>
            <td align="right">T&iacute;tulo profesional</td>
            <td><select id="req_titulo">
                    <option value="">NA</option>
                    <option value="0" <?= ($ROW[0]['req_titulo'] == '0') ? 'selected' : '' ?>>No</option>
                    <option value="1" <?= ($ROW[0]['req_titulo'] == '1') ? 'selected' : '' ?>>S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td>Fecha actualizaci&oacute;n:</td>
            <td><?= $ROW[0]['fecha'] ?></td>
            <td align="right">Incorporaci&oacute;n al colegio</td>
            <td><select id="req_colegio">
                    <option value="">NA</option>
                    <option value="0" <?= ($ROW[0]['req_colegio'] == '0') ? 'selected' : '' ?>>No</option>
                    <option value="1" <?= ($ROW[0]['req_colegio'] == '1') ? 'selected' : '' ?>>S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td>Pr&oacute;xima actualizaci&oacute;n:</td>
            <td><input type="text" id="prox" name="prox" class="fecha" value="<?= $ROW[0]['prox'] ?>" readonly
                       onClick="show_calendar(this.id);"></td>
            <td align="right">Curriculum vitae</td>
            <td><select id="req_hv">
                    <option value="">NA</option>
                    <option value="0" <?= ($ROW[0]['req_hv'] == '0') ? 'selected' : '' ?>>No</option>
                    <option value="1" <?= ($ROW[0]['req_hv'] == '1') ? 'selected' : '' ?>>S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td align="right">Compromiso de confidencialidad, objetividad e imparcialidad</td>
            <td><select id="req_compromiso">
                    <option value="">NA</option>
                    <option value="0" <?= ($ROW[0]['req_compromiso'] == '0') ? 'selected' : '' ?>>No</option>
                    <option value="1" <?= ($ROW[0]['req_compromiso'] == '1') ? 'selected' : '' ?>>S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td align="right">Personal competente</td>
            <td><select id="req_competente">
                    <option value="">NA</option>
                    <option value="0" <?= ($ROW[0]['req_competente'] == '0') ? 'selected' : '' ?>>No</option>
                    <option value="1" <?= ($ROW[0]['req_competente'] == '1') ? 'selected' : '' ?>>S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td align="right">Perfil</td>
            <td><select id="req_perfil">
                    <option value="">NA</option>
                    <option value="0" <?= ($ROW[0]['req_perfil'] == '0') ? 'selected' : '' ?>>No</option>
                    <option value="1" <?= ($ROW[0]['req_perfil'] == '1') ? 'selected' : '' ?>>S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td align="right">Puesto de trabajo por clase</td>
            <td><select id="req_clase">
                    <option value="">NA</option>
                    <option value="0" <?= ($ROW[0]['req_clase'] == '0') ? 'selected' : '' ?>>No</option>
                    <option value="1" <?= ($ROW[0]['req_clase'] == '1') ? 'selected' : '' ?>>S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td align="right">Puesto de trabajo especialidad</td>
            <td><select id="req_especialidad">
                    <option value="">NA</option>
                    <option value="0" <?= ($ROW[0]['req_especialidad'] == '0') ? 'selected' : '' ?>>No</option>
                    <option value="1" <?= ($ROW[0]['req_especialidad'] == '1') ? 'selected' : '' ?>>S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td align="right">Inducci&oacute;n</td>
            <td><select id="req_induccion">
                    <option value="">NA</option>
                    <option value="0" <?= ($ROW[0]['req_induccion'] == '0') ? 'selected' : '' ?>>No</option>
                    <option value="1" <?= ($ROW[0]['req_induccion'] == '1') ? 'selected' : '' ?>>S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td align="right">Autorizaciones</td>
            <td><select id="req_autorizacion">
                    <option value="">NA</option>
                    <option value="0" <?= ($ROW[0]['req_autorizacion'] == '0') ? 'selected' : '' ?>>No</option>
                    <option value="1" <?= ($ROW[0]['req_autorizacion'] == '1') ? 'selected' : '' ?>>S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td colspan="4" align="center"><input type="button" id="btn1" value="Actualizar" class="boton"
                                                  onClick="datos()"></td>
        </tr>
    </table>
    <br/>
    <table width="95%" class="radius" style="font-size:12px">
        <tr>
            <td class="titulo">Opciones</td>
        </tr>
        <tr>
            <td>
                <!---->
                <table border="1" width="100%" style="font-size:12px">
                    <tr>
                        <td>Tipo: <select id="cmb_tipo">
                                <option value="">Todos</option>
                                <option value="S">SGC</option>
                                <option value="T">T&eacute;cnica</option>
                                <option value="O">Otras</option>
                                <option value="I">Internacionales</option>
                            </select>
                            &nbsp;Desde: <input type="text" id="desde" name="desde" class="fecha" readonly
                                                onClick="show_calendar(this.id);">&nbsp;
                            Hasta: <input type="text" id="hasta" name="hasta" class="fecha" readonly
                                          onClick="show_calendar(this.id);">
                            &nbsp;
                            <input type="button" value="Buscar" class="boton" onclick="Buscar(3)"/>
                        </td>
                        <td><input type="button" id="lolo2" class="boton" value="Agregar" onclick="CursosMas()"
                                   title="Agregar l�nea"/>&nbsp;<input type="button" id="btn3" value="Actualizar"
                                                                       class="boton" onClick="Paso2()"
                                                                       disabled>&nbsp;<img
                                    src="<?= $Gestor->Incluir('olvido', 'bkg') ?>" height="20" width="20"
                                    title="Si deja alg�n campo vac�o la l�nea no ser� tomada en cuenta"/></td>
                    </tr>
                </table>
                <!---->
            </td>
        </tr>
    </table>
    <br/>


    <table width="95%" class="radius" style="font-size:12px">
        <thead>
        <tr>
            <td class="titulo" colspan="7"><img id="i_3" src="<?= $Gestor->Incluir('closed', 'bkg') ?>"
                                                onclick="Oculta(3)" title="Mostrar/Ocultar"
                                                style="vertical-align:baseline;cursor:pointer"/>&nbsp;Capacitaciones
            </td>
        </tr>
        <tr>
            <td></td>
            <td><strong>Tipo</strong></td>
            <td><strong>Nombre de la capacitaci&oacute;n/curso</strong></td>
            <td><strong>Informaci&oacute;n Empresa</strong></td>
            <td><strong>Fecha</strong></td>
            <td><strong>Horas</strong></td>
            <td><strong>Adjunto</strong></td>
        </tr>
        </thead>
        <tbody id="t3" style="display:none"></tbody>
    </table>
    <br/>
    <table width="95%" class="radius" style="font-size:12px">
        <thead>
        <tr>
            <td class="titulo" colspan="3"><img id="i_4" src="<?= $Gestor->Incluir('closed', 'bkg') ?>"
                                                onclick="Oculta(4)" title="Mostrar/Ocultar"
                                                style="vertical-align:baseline;cursor:pointer"/>&nbsp;Gr&aacute;ficos
            </td>
        </tr>
        <tr>
            <td colspan="3"><input type="button" id="btnG" class="boton" value="Generar" onclick="GeneraTabla()"/>&nbsp;&nbsp;
                <input type="button" id="btnt1" style="background-image:url(<?= $Gestor->Incluir('tabla', 'bkg') ?>)"
                       disabled title="Ver tabla" onclick="Muestra(1)"/>&nbsp;
                <input type="button" id="btnt2"
                       style="background-image:url(<?= $Gestor->Incluir('estadistica', 'bkg') ?>)" disabled
                       title="Ver gr&aacute;fico por tipo" onclick="Muestra(2)"/>&nbsp;
                <input type="button" id="btnt3"
                       style="background-image:url(<?= $Gestor->Incluir('estadistica2', 'bkg') ?>)" disabled
                       title="Ver gr&aacute;fico por totales" onclick="Muestra(3)"/>
            </td>
        </tr>
        </thead>
        <tbody id="t4" style="display:none">
        <tr>
            <td colspan="3">
                <hr/>
            </td>
        </tr>
        <tr align="center">
            <td id="grafico_1"></td>
            <td id="grafico_2" style="display:none">
                <div id="container2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </td>
            <td id="grafico_3" style="display:none">
                <div id="container3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </td>
        </tr>
        </tbody>
    </table>
</center>
<br> <?= $Gestor->Encabezado('B0007', 'p', '') ?>
</body>
</html>