<?php
define('__MODULO__', 'calidad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _plantilla_encpie_detalle();
$ROW = $Gestor->ObtieneDatos();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <!-- include libraries(jQuery, bootstrap) -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.js"></script>
</head>
<body>
<?php $Gestor->Incluir('q9', 'hr', 'Gesti&oacute;n de Calidad :: Mantenimiento de Encabezados') ?>
<?= $Gestor->Encabezado('Q0009', 'e', 'Mantenimiento de Encabezados') ?>
<br/>
<form name="form" id="form" method="post" action="../../caspha-i/shell/calidad/_plantilla_encpie_detalle.php">
    <input type="hidden" id="id" name="id" value="<?= $_GET['id'] ?>"/>
    <input type="hidden" id="acc" name="acc" value="<?= $_GET['acc'] ?>"/>
    <table class="radius" align="center">
        <tr>
            <td class="titulo">Tipo de Plantilla</td>
        </tr>
        <tr>
            <td align="center">
                <select id="tipo" name="tipo" required
                        onchange="location.href='plantilla_encpie_detalle.php?acc=<?= $_GET['acc'] ?>&id=<?= $_GET['id'] ?>&tipo='+this.value;">
                    <option value="">...</option>
                    <option <?= $_GET['tipo'] == 'e' ? 'selected' : '' ?> value='e'>Encabezado</option>
                    <option <?= $_GET['tipo'] == 'p' ? 'selected' : '' ?> value='p'>Pie P&aacute;gina</option>
                </select>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" align="center" width="98%">
        <tr>
            <td class="titulo" colspan="2">Datos Registradas</td>
        </tr>
        <tr>
            <td>
                <br><b>Nombre de <?= $_GET['tipo'] == 'e' ? 'Encabezado' : 'Pie P&aacute;gina' ?></b><br>
                <textarea name="nombre" style="width:98%" required><?= $ROW[0]['nombre'] ?></textarea>
            </td>
        </tr>
        <tr hidden>
            <td>
                <br><br><b>Laboratorio del <?= $_GET['tipo'] == 'e' ? 'Encabezado' : 'Pie P&aacute;gina' ?></b><br>
                <select id="lab" name="lab">
                    <option <?= $ROW[0]['lab'] == '3' ? 'selected' : '' ?> value='3'>LCC</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <br><br><b>Detalle</b><br>
                <textarea id="summernote" name="detalle"><?= $ROW[0]['detalle'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <hr>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <br><b> Gu&iacute;a de uso</b>:
            </td>
        </tr>
        <?php if ($_GET['tipo'] == 'e') { ?>
            <tr>
                <td colspan="2" align="center">
                    <img src="../../caspha-i/imagenes/ejemplo_enc.png?!">
                    <br>Utilice la forma <b>@nombre_variable</b> para intercambiar el
                    texto de la plantilla con los datos reales.
                    <br>En caso de los n&uacute;meros de p&aacute;gina, se debe dejar el s&iacute;mbolo #, ya que la numeraci&oacute;n en controlada por el sistema.
                </td>
            </tr>
        <?php } else { ?>
            <tr>
                <td colspan="2" align="center">
                    <img src="../../caspha-i/imagenes/ejemplo_pie.png?1">
                    <br>Utilice la forma <b>@elaboro</b>, <b>@reviso</b>, <b>@aprobo</b> para intercambiar el
                    texto de la plantilla con los datos reales.
                    <br>El texto se sustituir&aacute; de forma vertical, separados por un cambio de l&iacute;nea, tal como lo muestra el ejemplo.
                </td>
            </tr>
        <?php } ?>
        <tr>
            <td colspan="2" align="center"><br><br><input type="submit" value="Aceptar" class="boton"></td>
        </tr>
    </table>
</form>
<br/>
<br><?= $Gestor->Encabezado('Q0009', 'p', '') ?>
</body>
</html>