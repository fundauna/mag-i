<?php
define('__MODULO__', 'calidad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _pendientes();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('b8', 'hr', 'Gestión de Calidad :: Documentos pendientes de revisión/aprobación') ?>
<?= $Gestor->Encabezado('B0008', 'e', 'Documentos pendientes de revisión/aprobación') ?>
<center>
    <br/>
    <div id="container" style="width:750px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>C&oacute;digo</th>
                <th>Nombre</th>
                <th>Versi&oacute;n</th>
                <th>Revisi&oacute;n</th>
                <th>Estado</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->DocumentosMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#"
                           onclick="DocumentosModifica('<?= $ROW[$x]['cs'] ?>', '<?= $_GET['tablon'] ?>')"><?= $ROW[$x]['codigo'] ?></a>
                    </td>
                    <td><?= substr($ROW[$x]['descripcion'], 0, 30), '...' ?></td>
                    <td><?= $ROW[$x]['version'] ?></td>
                    <td><?= $ROW[$x]['revision'] ?></td>
                    <td>Pend. <?= $ROW[$x]['accion'] == '99' ? 'Revisión' : 'Aprobación' ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</center>
<br><?= $Gestor->Encabezado('B0008', 'p', '') ?>
</body>
</html>