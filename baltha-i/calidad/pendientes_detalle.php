<?php
define('__MODULO__', 'calidad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _pendientes_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Documento inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js', 5) ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('b9', 'hr', 'Gesti�n de Calidad :: Revisar/Aprobar Documentos') ?>
<?= $Gestor->Encabezado('B0009', 'e', 'Revisar/Aprobar Documentos') ?>
<center>
    <input type="hidden" id="tablon" name="tablon" value="<?= $_GET['tablon'] ?>"/>
    <input type="hidden" id="cs" name="cs" value="<?= $ROW[0]['cs'] ?>"/>
    <table class="radius" style="font-size:12px; width:450px">
        <tr>
            <td class="titulo" colspan="2">Datos</td>
        </tr>
        <tr>
            <td><strong>Tipo de documento:</strong></td>
            <td><?= $ROW[0]['nombre'] ?></td>
        </tr>
        <tr>
            <td><strong>C&oacute;digo del documento:</strong></td>
            <td><?= $ROW[0]['codigo'] ?></td>
        </tr>
        <tr>
            <td><strong>Nombre:</strong></td>
            <td><?= $ROW[0]['descripcion'] ?></td>
        </tr>
        <tr>
            <td><strong>Versi&oacute;n:</strong></td>
            <td><?= $ROW[0]['version'] ?></td>
        </tr>
        <tr>
            <td><strong>Revisi&oacute;n:</strong></td>
            <td><?= $ROW[0]['revision'] ?></td>
        </tr>
        <tr id="tr1" style="display:none">
            <td colspan="2" align="center"><strong>Justificaci&oacute;n:</strong></td>
        </tr>
        <tr id="tr2" style="display:none">
            <td colspan="2" align="center"><textarea id="jus" style="width:400px"></textarea></td>
        </tr>
    </table>
    <br/>
    <table class="radius" align="center" style="font-size:12px; width:450px">
        <tr>
            <td class="titulo" colspan="5">Hist&oacute;rico</td>
        </tr>
        <tr>
            <td align="center"><b>Versi&oacute;n</b></td>
            <td align="center"><b>Revisi&oacute;n</b></td>
            <td align="center"><b>&Uacute;ltima Actualizaci&oacute;n</b></td>
            <td align="center"><strong>Descargar</strong></td>
        </tr>
        <?php
        $ROW2 = $Gestor->DocumentosVersiones();
        for ($x = 0; $x < count($ROW2); $x++) {
            ?>
            <tr>
                <td align="center"><?= $ROW2[$x]['versionP'] == $ROW[0]['version'] ? $ROW2[$x]['versionP'] . ' (Actual)' : $ROW2[$x]['versionP'] ?></td>
                <td align="center"><?= $ROW2[$x]['revision'] ?></td>
                <td align="center"><?= $ROW2[$x]['fecha'] ?></td>
                <td align="center"><img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar"
                                        onclick="DocumentosZip('<?= "{$ROW[0]['cs']}-{$ROW2[$x]['versionP']}" ?>', '<?= __MODULO__ ?>')"
                                        class="tab2"/></td>
            </tr>
        <?php } ?>
    </table>
    <br/>
    <?php if ($Gestor->PuedeRevisar()) { ?>
        <input type="button" id="btn1" value="Revisar" class="boton" onclick="Revisar();">&nbsp;
        <input type="button" id="btn2" value="Denegar" class="boton" onclick="Denegar();">
    <?php } elseif ($Gestor->PuedeAprobar()) { ?>
        <input type="button" id="btn1" value="Aprobar" class="boton" onclick="Aprobar();">&nbsp;
        <input type="button" id="btn2" value="Denegar" class="boton" onclick="Denegar();">
    <?php } ?>
</center>
<br><?= $Gestor->Encabezado('B0009', 'p', '') ?>
</body>
</html>