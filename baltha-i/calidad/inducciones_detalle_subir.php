<?php
define('__MODULO__', 'calidad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);
$Gestor = new _inducciones_detalle_subir();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('graficador', 'gfx'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<form name="form" id="form" method="post" enctype="multipart/form-data"
      action="../../caspha-i/shell/calidad/_inducciones_detalle_subir.php">
    <input type="hidden" name="ID" value="<?= $_GET['ID'] ?>"/>
    <input type="hidden" name="l" value="<?= $_GET['l'] ?>"/>
    <center>
        <input required type="file" name="archivo" value=""/>
        <br/><br/><br/>
        <div>
            <input type="submit" id="btn1" value="Subir" class="boton" onClick="datos()"/>
        </div>
    </center>
</form>
</body>
</html>