<?php
define('__MODULO__', 'calidad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _referencias();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('b11', 'hr', 'Gesti�n de Calidad :: Documentos SGC') ?>
<?= $Gestor->Encabezado('B0011', 'e', 'Documentos SGC') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" style="font-size:12px;width:300px;">
            <tr>
                <td class="titulo" colspan="3">Filtro</td>
            </tr>
            <tr>
                <td>M&oacute;dulo:</td>
                <td><select id="modulo" name="modulo" style="width:150px">
                        <?php
                        $ROW2 = $Gestor->DocumentosModulo();
                        for ($x = 0; $x < count($ROW2); $x++) {
                            ?>
                            <option <?= $_POST['modulo'] == $ROW2[$x]['id'] ? 'selected' : '' ?>
                                    value="<?= $ROW2[$x]['id'] ?>"><?= $ROW2[$x]['nombre'] ?></option>
                        <?php } ?>
                        <option value="77">Todos</option>
                    </select></td>
                <td><input type="button" value="Buscar" class="boton2" onclick="DocumentosBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:750px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>C&oacute;digo</th>
                <th>Descripci&oacute;n</th>
                <th>Descargar</th>
                <th>V&iacute;nculos</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->DocumentosMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><?= $ROW[$x]['codigo'] ?></td>
                    <td><?= $ROW[$x]['descripcion'] ?></td>
                    <td><img onclick="DocumentosZip('<?= $ROW[$x]['cs'] ?>-<?= $ROW[$x]['version'] ?>','calidad')"
                             src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar" class="tab2"/></td>
                    <td><img onclick="DocumentosVinculos('<?= $ROW[$x]['cs'] ?>')"
                             src="<?php $Gestor->Incluir('vinculos', 'bkg') ?>" title="Documentos vinculados"
                             class="tab2"/></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</center>
<br><?= $Gestor->Encabezado('B0011', 'p', '') ?>
</body>
</html>