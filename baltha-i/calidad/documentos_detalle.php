<?php
define('__MODULO__', 'calidad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _documentos_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Documento inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css', 1) ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js', 5) ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('b4', 'hr', 'Gestión de Calidad :: Detalle de Documentos') ?>
<?= $Gestor->Encabezado('B0004', 'e', 'Detalle de Documentos') ?>
<center>
    <form name="form" id="form" method="post" enctype="multipart/form-data"
          action="../../caspha-i/shell/calidad/_documentos_detalle.php">
        <input type="hidden" id="accion" name="accion" value="<?= $_GET['acc'] ?>"/>
        <input type="hidden" id="cs" name="cs" value="<?= $ROW[0]['cs'] ?>"/>
        <input type="hidden" id="versionO" name="versionO" value="<?= $ROW[0]['version'] ?>"/>
        <table class="radius" style="font-size:12px; width:450px">
            <tr>
                <td class="titulo" colspan="2">Datos</td>
            </tr>
            <tr>
                <td>Tipo de documento:</td>
                <td>
                    <select id="tipo" name="tipo" style="width:150px">
                        <option value=''>...</option>
                        <?php
                        $ROW2 = $Gestor->DocumentosFiltro();
                        for ($x = 0; $x < count($ROW2); $x++) {
                            ?>
                            <option <?= $ROW[0]['tipo'] == $ROW2[$x]['id'] ? 'selected' : '' ?>
                                    value="<?= $ROW2[$x]['id'] ?>"><?= $ROW2[$x]['sigla'] ?>(<?= $ROW2[$x]['nombre'] ?>)
                            </option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>C&oacute;digo del documento:</td>
                <td><input type="text" id="codigo" name="codigo" value="<?= $ROW[0]['codigo'] ?>" size="20"
                           maxlength="20"></td>
            </tr>
            <tr>
                <td>Nombre:</td>
                <td><textarea id="descripcion" name="descripcion"
                              style="width:98%"><?= $ROW[0]['descripcion'] ?></textarea></td>
            </tr>
            <tr>
                <td>Versi&oacute;n:</td>
                <td><input type="text" id="version" name="version"
                           value="<?= $_GET['acc'] == 'I' ? '01' : $ROW[0]['version'] ?>" size="2" maxlength="2"
                           onblur="formateaVersion(this.value)"></td>
            </tr>
            <tr>
                <!--	<td>Revisi&oacute;n:</td>-->
                <td><input type="hidden" id="revision" name="revision" value="<?= $ROW[0]['revision'] ?>" size="10"
                           maxlength="10"></td>
            </tr>
            <tr>
                <td>Unidad:</td>
                <td><select id="unidad" name="unidad" style="width:150px">
                        <option <?= $ROW[0]['lid'] == '' ? 'selected' : '' ?> value=''>...</option>
                        <option <?= $ROW[0]['lid'] == '1' ? 'selected' : '' ?> value='1'>LRE</option>
                        <option <?= $ROW[0]['lid'] == '2' ? 'selected' : '' ?> value='2'>LDP</option>
                        <option <?= $ROW[0]['lid'] == '3' ? 'selected' : '' ?> value='3'>LCC</option>
                        <option <?= $ROW[0]['lid'] == '4' ? 'selected' : '' ?> value='4'>LAB</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Naturaleza:</td>
                <td><select id="naturaleza" name="naturaleza" style="width:150px">
                        <option value=''>...</option>
                        <?php
                        $ROW2 = $Gestor->DocumentosNaturaleza();
                        for ($x = 0; $x < count($ROW2); $x++) {
                            ?>
                            <option <?= $ROW[0]['naturaleza'] == $ROW2[$x]['id'] ? 'selected' : '' ?>
                                    value="<?= $ROW2[$x]['id'] ?>"
                                    title="<?= $ROW2[$x]['descripcion'] ?>"><?= $ROW2[$x]['nombre'] ?></option>
                        <?php } ?>
                    </select></td>
            </tr>
            <tr>
                <td>M&oacute;dulo:</td>
                <td><select id="modulo" name="modulo" style="width:150px">
                        <option value=''>...</option>
                        <?php
                        $ROW2 = $Gestor->DocumentosModulo();
                        for ($x = 0; $x < count($ROW2); $x++) {
                            ?>
                            <option <?= $ROW[0]['modulo'] == $ROW2[$x]['id'] ? 'selected' : '' ?>
                                    value="<?= $ROW2[$x]['id'] ?>"
                                    title="<?= $ROW2[$x]['nombre'] ?>"><?= $ROW2[$x]['nombre'] ?></option>
                        <?php } ?>
                    </select></td>
            </tr>
            <tr>
                <td>Estado:</td>
                <td><select id="estado" name="estado" style="width:150px">
                        <option value=''>...</option>
                        <?php
                        $ROW2 = $Gestor->DocumentosEstado();
                        for ($x = 0; $x < count($ROW2); $x++) {
                            ?>
                            <option <?= $ROW[0]['estado'] == $ROW2[$x]['id'] ? 'selected' : '' ?>
                                    value="<?= $ROW2[$x]['id'] ?>"
                                    title="<?= $ROW2[$x]['descripcion'] ?>"><?= $ROW2[$x]['nombre'] ?></option>
                        <?php } ?>
                    </select></td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr class="blokeado">
                <td>Adjuntar documento:</td>
                <td><input type="file" name="archivo" id="archivo" class="boton"></td>
            </tr>
            <?php if ($_GET['acc'] == 'M') { ?>
                <tr>
                    <td colspan="2">
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="2"><strong>Opciones adicionales:</strong>&nbsp;
                        <img onclick="DocumentosTrasladar('<?= $ROW[0]['cs'] ?>', '<?= $ROW[0]['codigo'] ?>', '<?= $ROW[0]['descripcion'] ?>')"
                             src="<?php $Gestor->Incluir('firmas', 'bkg', 1) ?>"
                             title="Transladar documento para aprobación" class="tab2"/>&nbsp;&nbsp;
                        <img onclick="DocumentosVinculados('<?= $ROW[0]['cs'] ?>', '<?= $ROW[0]['codigo'] ?>', '<?= $ROW[0]['descripcion'] ?>')"
                             src="<?php $Gestor->Incluir('vinculos', 'bkg') ?>" title="Ver documentos vinculados"
                             class="tab3"/>&nbsp;&nbsp;
                    </td>
                </tr>
            <?php } ?>
        </table>
        <br/>
        <input type="button" id="btn" value="Aceptar" class="boton" onclick="datos('<?= $_GET['acc'] ?>');">
    </form>
</center>
<?php if ($_GET['acc'] == 'M') { ?>
    <br/>
    <table class="radius" align="center" style="font-size:12px; width:450px">
        <tr>
            <td class="titulo" colspan="5">Hist&oacute;rico</td>
        </tr>
        <tr>
            <td align="center"><b>Versi&oacute;n</b></td>
            <td align="center"><b>&Uacute;ltima Actualizaci&oacute;n</b></td>
            <td colspan="2" align="center"><strong>Opciones</strong></td>
        </tr>
        <?php
        $ROW2 = $Gestor->DocumentosVersiones();
        for ($x = 0; $x < count($ROW2); $x++) {
            ?>
            <tr>
                <td align="center"><?= $ROW2[$x]['versionP'] == $ROW[0]['version'] ? $ROW2[$x]['versionP'] . ' (Actual)' : $ROW2[$x]['versionP'] ?></td>
                <td align="center"><?= $ROW2[$x]['fecha'] ?></td>
                <td align="center">
                    <img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar"
                         onclick="DocumentosZip('<?= "{$ROW[0]['cs']}-{$ROW2[$x]['versionP']}" ?>', '<?= __MODULO__ ?>')"
                         class="tab3"/>
                    <img onclick="DocumentosElimina('<?= $ROW[0]['cs'] ?>','<?= $ROW2[$x]['versionP'] ?>',2)"
                         src="<?php $Gestor->Incluir('del', 'bkg') ?>" border="0" title="Eliminar" class="tab2"/>
                </td>
            </tr>
        <?php } ?>
    </table>
<?php } ?>
<br><?= $Gestor->Encabezado('B0004', 'p', '') ?>
</body>
</html>