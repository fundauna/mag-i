<?php
define('__MODULO__', 'calidad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _clasificaciones();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('b2', 'hr', 'Gesti�n de Calidad :: Clasificaciones') ?>
<?= $Gestor->Encabezado('B0002', 'e', 'Gesti�n de Calidad :: Clasificaciones') ?>
<table class="radius" align="center">
    <tr>
        <td class="titulo" colspan="2">Opciones</td>
    </tr>
    <tr>
        <td>Objeto:&nbsp;</td>
        <td><select id="tipo" onchange="location.href='clasificaciones.php?tipo='+this.value;ALFA('Cargando');">
                <option value="">...</option>
                <option <?= $_GET['tipo'] == '1' ? 'selected' : '' ?> value="1">Tipo</option>
                <option <?= $_GET['tipo'] == '2' ? 'selected' : '' ?> value="2">Naturaleza</option>
                <option <?= $_GET['tipo'] == '3' ? 'selected' : '' ?> value="3">Estado</option>
            </select>
        </td>
    </tr>
</table>
<br/>
<table class="radius" align="center" width="255px">
    <tr>
        <td class="titulo" colspan="2">Clasificaciones Registradas</td>
    </tr>
    <tr>
        <td colspan="2">
            <select size="10" style="width:250px" onclick="marca(this, <?= $_GET['tipo'] ?>);">
                <?php
                $ROW = $Gestor->ClasificacionesMuestra();
                for ($x = 0; $x < count($ROW); $x++) {
                    if ($_GET['tipo'] == '1')
                        echo "<option id='{$ROW[$x]['id']}' sigla='{$ROW[$x]['sigla']}' nombre='{$ROW[$x]['nombre']}'>{$ROW[$x]['id']} - {$ROW[$x]['nombre']}</option>";
                    elseif ($_GET['tipo'] == '3') { //PARA ESTADOS
                        $color = '';
                        if ($ROW[$x]['tipo'] == '2') $color = "style='color:blue'";    //NOTIFICACION
                        elseif ($ROW[$x]['tipo'] == '0') $color = "style='color:red'";    //NO VISIBILIDAD
                        echo "<option {$color} id='{$ROW[$x]['id']}' sigla='{$ROW[$x]['tipo']}' nombre='{$ROW[$x]['nombre']}' descripcion='{$ROW[$x]['descripcion']}'>{$ROW[$x]['id']} - {$ROW[$x]['nombre']}</option>";

                    } else //PARA NATURALEZAS
                        echo "<option id='{$ROW[$x]['id']}' nombre='{$ROW[$x]['nombre']}' descripcion='{$ROW[$x]['descripcion']}'>{$ROW[$x]['id']} - {$ROW[$x]['nombre']}</option>";
                }

                unset($ROW);
                ?>
            </select>
        </td>
    </tr>
</table>
<br/>
<table class="radius" align="center" width="255px">
    <input readonly type="hidden" id="tipo" value='<?= $_GET['tipo'] ?>'>
    <tr>
        <td class="titulo" colspan="2">Detalle</td>
    </tr>
    <?php if ($_GET['tipo'] == '1') { ?>
        <tr>
            <td><b>Id:&nbsp;</b></td>
            <td><input type="text" id="id" maxlength="2" onBlur="_INT(this);" size="2"></td>
        </tr>
        <tr>
            <td><b>Sigla:&nbsp;</b></td>
            <td><input type="text" id="sigla" size="4" maxlength="3"></td>
        </tr>
        <tr>
            <td><b>Nombre:&nbsp;</b></td>
            <td><input type="text" id="nombre" size="20" maxlength="50"></td>
        </tr>
        <input type="hidden" id="descripcion" value="">
    <?php } elseif ($_GET['tipo'] == '2') { ?>
        <tr>
            <td><b>Id:&nbsp;</b></td>
            <td><input type="text" id="id" maxlength="2" onBlur="_INT(this);" size="2"></td>
        </tr>
        <tr>
            <td><b>Nombre:&nbsp;</b></td>
            <td><input type="text" id="nombre" size="20" maxlength="50"></td>
        </tr>
        <tr>
            <td><b>Descripci�n:&nbsp;</b></td>
            <td><input type="text" id="descripcion" size="20" maxlength="100"></td>
        </tr>
        <input type="hidden" id="sigla" value="">
    <?php } elseif ($_GET['tipo'] == '3') { ?>
        <tr>
            <td><b>Id:&nbsp;</b></td>
            <td><input type="text" id="id" maxlength="2" onBlur="_INT(this);" size="2"></td>
        </tr>
        <tr>
            <td><b>Nombre:&nbsp;</b></td>
            <td><input type="text" id="nombre" size="20" maxlength="50"></td>
        </tr>
        <tr>
            <td><b>Descripci�n:&nbsp;</b></td>
            <td><input type="text" id="descripcion" size="20" maxlength="100"></td>
        </tr>
        <tr>
            <td><b>Tipo:&nbsp;</b></td>
            <td>
                <select id="sigla">
                    <option value="1">Normal</option>
                    <!--<option value="2">Notificaci&oacute;n</option>-->
                    <option value="0">No visibilidad</option>
                </select>
            </td>
        </tr>

    <?php } else { ?>
        <tr>
            <td>Seleccione un objeto en las opciones</td>
        </tr>
    <?php } ?>
</table>
<br/>
<center>
    <input id="new" type="button" class="boton" value="Limpiar" onclick="desmarca();" disabled>&nbsp;
    <input id="add" type="button" class="boton" value="Agregar" onclick="modificar('I');">&nbsp;
    <input id="mod" type="button" class="boton" value="Modificar" onclick="modificar('M');" disabled>&nbsp;
    <input id="del" type="button" class="boton" value="Eliminar" onclick="modificar('D');" disabled>
</center>
<br/><?= $Gestor->Encabezado('B0002', 'p', '') ?>
</body>
</html>