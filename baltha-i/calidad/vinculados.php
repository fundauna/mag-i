<?php
define('__MODULO__', 'calidad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _vinculados();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js', 5) ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('b10', 'hr', 'Gesti�n de calidad :: Documentos vinculados') ?>
<?= $Gestor->Encabezado('B0010', 'e', 'Documentos vinculados') ?>
<center>
    <input type="hidden" id="padre" value="<?= $_GET['ID'] ?>"/>
    <input type="hidden" id="cod_padre" value="<?= str_replace(' ', '', $_GET['COD']) ?>"/>
    <table class="radius" align="center" width="400px">
        <tr>
            <td class="titulo" colspan="2">Documento padre</td>
        </tr>
        <tr>
            <td><strong>C&oacute;digo:</strong></td>
            <td><?= $_GET['COD'] ?></td>
        </tr>
        <tr>
            <td><strong>Nombre:</strong></td>
            <td><?= $_GET['NOMBRE'] ?></td>
        </tr>
    </table>
    <br/>
    <table class="radius" align="center" width="400px">
        <tr>
            <td class="titulo" colspan="4">Documentos Vinculados</td>
        </tr>
        <tr>
            <td><strong>C&oacute;digo:</strong></td>
            <td><strong>Nombre:</strong></td>
            <td colspan="2" align="center"><strong>Opciones</strong></td>
        </tr>
        <?php
        $ROW = $Gestor->DocumentosVinculados();
        for ($x = 0; $x < count($ROW); $x++) {
            ?>
            <tr>
                <td><?= $ROW[$x]['codigo'] ?></td>
                <td><?= $ROW[$x]['descripcion'] ?></td>
                <td align="center"><img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar"
                                        onclick="DocumentosZip('<?= "{$ROW[$x]['cs']}-{$ROW[$x]['version']}" ?>', '<?= __MODULO__ ?>')"
                                        class="tab3"/></td>
                <td align="center"><img src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar v�nculo"
                                        class="tab2" onclick="javascript:datos('D', '<?= $ROW[$x]['cs'] ?>')"/></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <br/>
    <table class="radius" align="center">
        <tr>
            <td class="titulo" colspan="2">Ingresar nuevo v&iacute;nculo</td>
        </tr>
        <tr>
            <td>C&oacute;digo:</td>
            <td><input type="text" id="codigo" name="codigo" size="20" maxlength="20" class="blokeado"></td>
        </tr>
    </table>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos('I', '-1')">
</center>
<br><?= $Gestor->Encabezado('B0010', 'p', '') ?>
</body>
</html>