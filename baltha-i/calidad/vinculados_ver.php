<?php
define('__MODULO__', 'calidad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _vinculados_ver();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Documento inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('b12', 'hr', 'Gesti�n de calidad :: Documentos vinculados') ?>
<?= $Gestor->Encabezado('B0012', 'e', 'Documentos vinculados') ?>
<center>
    <table class="radius" align="center" width="400px">
        <tr>
            <td class="titulo" colspan="2">Documento padre</td>
        </tr>
        <tr>
            <td><strong>C&oacute;digo:</strong></td>
            <td><?= $ROW[0]['codigo'] ?></td>
        </tr>
        <tr>
            <td><strong>Nombre:</strong></td>
            <td><?= $ROW[0]['descripcion'] ?></td>
        </tr>
    </table>
    <br/>
    <table class="radius" align="center" width="400px">
        <tr>
            <td class="titulo" colspan="3">Documentos Vinculados</td>
        </tr>
        <tr>
            <td><strong>C&oacute;digo:</strong></td>
            <td><strong>Nombre:</strong></td>
            <td><strong>Descargar</strong></td>
        </tr>
        <?php
        $ROW = $Gestor->DocumentosVinculados();
        for ($x = 0; $x < count($ROW); $x++) {
            ?>
            <tr>
                <td><?= $ROW[$x]['codigo'] ?></td>
                <td><?= $ROW[$x]['descripcion'] ?></td>
                <td align="center"><img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar"
                                        onclick="DocumentosZip('<?= "{$ROW[$x]['cs']}-{$ROW[$x]['version']}" ?>', '<?= __MODULO__ ?>')"
                                        class="tab3"/></td>
            </tr>
            <?php
        }
        ?>
    </table>
</center>
<br><?= $Gestor->Encabezado('B0012', 'p', '') ?>
</body>
</html>