<?php
define('__MODULO__', 'calidad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _acciones_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Documento inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<form name="form" id="form" method="post" enctype="multipart/form-data"
      action="../../caspha-i/shell/calidad/_acciones_detalle.php">
    <input type="hidden" id="accion" name="accion" value="<?= $_GET['acc'] ?>"/>
    <input type="hidden" id="cs" name="cs" value="<?= $ROW[0]['cs'] ?>"/>
    <center>
        <?php $Gestor->Incluir('b1', 'hr', 'Gesti�n de Calidad :: Control de Acciones Correctivas y/o Preventivas') ?>
        <?= $Gestor->Encabezado('B0001', 'e', 'Control de Acciones Correctivas y/o Preventivas') ?>
        <br/>
        <table class="radius" style="font-size:12px; width:98%">
            <tr>
                <td class="titulo" colspan="8" align="center"><strong>Informaci&oacute;n General</strong></td>
            </tr>
            <tr>
                <td><strong>Tipo de documento:</strong></td>
                <td><strong>C&oacute;digo:</strong></td>
                <td><strong>Fecha de presentaci&oacute;n:</strong></td>
                <td><strong>Tipo de hallazgo:</strong></td>
                <td><strong>Se aplica<br/>correci&oacute;n<br/>inmediata?:</strong></td>
                <td><strong>Fecha de correci&oacute;n<br/>inmediata:</strong></td>
                <td><strong>Responsable de<br/>correci&oacute;ninmediata:</strong></td>
                <td><strong>Estado:</strong></td>
            </tr>
            <tr>
                <td><select id="tipo" name="tipo">
                        <option value=''>...</option>
                        <option value="C" <?= ($ROW[0]['tipo'] == 'C') ? 'selected' : '' ?>>Acci&oacute;n Correctiva
                        </option>
                        <option value="P" <?= ($ROW[0]['tipo'] == 'P') ? 'selected' : '' ?>>Acci&oacute;n Preventiva
                        </option>
                    </select></td>
                <td><input type="text" id="codigo" name="codigo" value="<?= str_replace(' ', '', $ROW[0]['codigo']) ?>"
                           size="15" maxlength="15"></td>
                <td><input type="text" id="fec_pre" name="fec_pre" class="fecha2" value="<?= $ROW[0]['fec_pre'] ?>"
                           readonly onClick="show_calendar(this.id);"></td>
                <td><select id="hallazgo" name="hallazgo">
                        <option value=''>...</option>
                        <option value="0" <?= ($ROW[0]['hallazgo'] == '0') ? 'selected' : '' ?>>TNC</option>
                        <option value="1" <?= ($ROW[0]['hallazgo'] == '1') ? 'selected' : '' ?>>NCM</option>
                        <option value="2" <?= ($ROW[0]['hallazgo'] == '2') ? 'selected' : '' ?>>NCm</option>
                    </select></td>
                <td><select id="correccion" name="correccion" onchange="CambiaCorreccion(this.value)">
                        <option value=''>...</option>
                        <option value="0" <?= $ROW[0]['correccion'] == '0' ? 'selected' : '' ?>>No</option>
                        <option value="1" <?= $ROW[0]['correccion'] == '1' ? 'selected' : '' ?>>S&iacute;</option>
                    </select>
                </td>
                <td><input type="text" id="fec_corr" name="fec_corr" class="fecha2 correccion"
                           style="visibility: hidden" readonly onClick="show_calendar(this.id);"
                           value="<?= $ROW[0]['fecha_corr'] ?>"></td>
                <td>
                    <input type="text" id="tmpA0" class="lista2 correccion" style="visibility: hidden" readonly
                           onclick="UsuariosLista('A0')" value="<?= $ROW[0]['tmpA'] ?>"/>
                    <input type="hidden" id="usuarioA0" name="usuarioA" value="<?= $ROW[0]['usuarioA'] ?>"/>
                </td>
                <td><?= $ROW[0]['estado'] == 'A' ? 'Abierta' : 'Cerrada' ?></td>
            </tr>
        </table>
        <br/>
        <table class="radius" style="font-size:12px; width:98%">
            <tr>
                <td class="titulo" colspan="9" align="center"><strong>Detalle</strong>&nbsp;<img onclick="AccionMas()"
                                                                                                 src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                                                                                                 title="Agregar l�nea"
                                                                                                 class="tab"/></td>
            </tr>

            <tr align="center">
                <td>N&ordm; Hallazgo</td>
                <td>Fecha en que<br/>se realizar<br/>acci&oacute;n</td>
                <td>Responsable de<br/>la acci&oacute;n</td>
                <td>Fecha seguimiento<br/>propuesta</td>
                <td>Responsable<br/>seguimiento</td>
                <td>Fecha seguimiento<br/>real</td>
                <td>Estado de<br/>la acci&oacute;n</td>
                <td>Fecha de<br/>cierre</td>
                <td>Responsable<br/>del cierre</td>
            </tr>
            </thead>
            <tbody id="lolo">
            <?php
            $ROW = $Gestor->ObtieneLineas();
            for ($x = 0; $x < count($ROW); $x++) {
                $ROW[$x]['numero'] = str_replace(' ', '', $ROW[$x]['numero']);
                $ROW2 = $Gestor->ObtieneResponsables($ROW[$x]['numero']);
                ?>
                <tr>
                    <td><input type="text" id="numero<?= $x ?>" name="numero[]" value="<?= $ROW[$x]['numero'] ?>"
                               size="15" maxlength="15"></td>
                    <td><input type="text" id="fec_acc<?= $x ?>" name="fec_acc[]" class="fecha2" readonly
                               onClick="show_calendar(this.id);" value="<?= $ROW[$x]['fec_acc'] ?>"></td>
                    <td><input type="text" id="tmpB<?= $x ?>" class="lista2" readonly
                               onclick="UsuariosLista('B<?= $x ?>')" value="<?= $ROW2[0]['tmpB'] ?>"/><input
                                type="hidden" id="usuarioB<?= $x ?>" name="usuarioB[]"
                                value="<?= $ROW2[0]['usuarioB'] ?>"/></td>
                    <td><input type="text" id="fec_seg<?= $x ?>" name="fec_seg[]" class="fecha2" readonly
                               onClick="show_calendar(this.id);" value="<?= $ROW[$x]['fec_seg'] ?>"></td>
                    <td><input type="text" id="tmpC<?= $x ?>" class="lista2" readonly
                               onclick="UsuariosLista('C<?= $x ?>')" value="<?= $ROW2[0]['tmpC'] ?>"/><input
                                type="hidden" id="usuarioC<?= $x ?>" name="usuarioC[]"
                                value="<?= $ROW2[0]['usuarioC'] ?>"/></td>
                    <td><input type="text" id="fec_rea<?= $x ?>" name="fec_rea[]" class="fecha2" readonly
                               onClick="show_calendar(this.id);" value="<?= $ROW[$x]['fec_rea'] ?>"></td>
                    <td><select id="estado<?= $x ?>" name="estado[]"
                                <?= $ROW[$x]['estado'] == 'A' ? 'style="color:#FF0000"' : '' ?>>
                            <option value=''>...</option>
                            <option value="A" <?= $ROW[$x]['estado'] == 'A' ? 'selected' : '' ?>>Abierta</option>
                            <option value="C" <?= $ROW[$x]['estado'] == 'C' ? 'selected' : '' ?>>Cerrada</option>
                        </select>
                    </td>
                    <td><input type="text" id="fec_cie<?= $x ?>" name="fec_cie[]" class="fecha2" readonly
                               onClick="show_calendar(this.id);" value="<?= $ROW[$x]['fec_cie'] ?>"></td>
                    <td><input type="text" id="tmpD<?= $x ?>" class="lista2" readonly
                               onclick="UsuariosLista('D<?= $x ?>')" value="<?= $ROW2[0]['tmpD'] ?>"/><input
                                type="hidden" id="usuarioD<?= $x ?>" name="usuarioD[]"
                                value="<?= $ROW2[0]['usuarioD'] ?>"/></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <br/><?= $Gestor->Encabezado('B0001', 'p', '') ?>
        <br/>
        <input type="button" value="Aceptar" class="boton" onclick="AccionAgregar(this)">
    </center>
</form>
</body>
</html>