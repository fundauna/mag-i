<?php
define('__MODULO__', 'biologia');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _proc();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('a0008', 'hr', 'Inventario :: Mantenimiento de procedimientos') ?>
<?= $Gestor->Encabezado('A0008', 'e', 'Mantenimiento de procedimientos') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" style="font-size:12px;width:450px;">
            <tr>
                <td class="titulo" colspan="3">Filtro</td>
            </tr>
            <tr>
                <td>C&oacute;digo:<br/><input type="text" name="codigo" id="codigo" maxlength="20"
                                              value="<?= $_POST['codigo'] ?>"/></td>
                <td>Nombre:<br/><input type="text" name="nombre" id="nombre" maxlength="100"
                                       value="<?= $_POST['nombre'] ?>"/></td>
                <td><br/><input type="button" value="Buscar" class="boton2" onclick="ResBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:450px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>C&oacute;digo</th>
                <th>Nombre</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->ProcMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#" onclick="ResModifica('<?= $ROW[$x]['cs'] ?>')"><?= $ROW[$x]['cs'] ?></a></td>
                    <td><?= $ROW[$x]['nombre'] ?></td>
                    <td><img onclick="ResElimina('<?= $ROW[$x]['cs'] ?>')" src="<?php $Gestor->Incluir('del', 'bkg') ?>"
                             title="Eliminar" class="tab2"/></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/>
    <input type="button" value="Agregar" class="boton" onClick="ResAgrega();">
</center>
<br/><?= $Gestor->Encabezado('A0008', 'p', '') ?>
</body>
</html>