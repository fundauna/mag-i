<?php
define('__MODULO__', 'biologia');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _resuspension_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('estilo', 'js') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('a0011', 'hr', 'Inventario :: Detalle Control de resuspensión') ?>
<?= $Gestor->Encabezado('A0011', 'e', 'Control de resuspensión') ?>
<center>
    <input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
    <table class="radius" width="98%" style="font-size:12px;">
        <tr>
            <td class="titulo" colspan="4">Informaci&oacute;n general</td>
        </tr>
        <tr>
            <td>C&oacute;digo &uacute;nico:</td>
            <td><input type="text" id="codigo" value="<?= $ROW[0]['codigo'] ?>" size="15" maxlength="20"
                       <?= ($_GET['acc'] == 'M') ? 'readonly' : 'onblur="Sort()"' ?>></td>
            <td>Fecha de resuspensi&oacute;n:</td>
            <td><input type="text" id="fecha_r" class="fecha" value="<?= $ROW[0]['fecha_r'] ?>" readonly
                       onClick="show_calendar(this.id);"></td>
        </tr>
        <tr>
            <td>Nombre de reactivo:</td>
            <td><input type="text" id="reactivo" value="<?= $ROW[0]['reactivo'] ?>" size="15" maxlength="20"/></td>
            <td>Analista:</td>
            <td><?= $ROW[0]['analista'] ?></td>
        </tr>
        <tr>
            <td>C&oacute;digo reactivo disolvente:</td>
            <td colspan="3"><input type="text" id="disolvente" value="<?= $ROW[0]['disolvente'] ?>" size="15"
                                   maxlength="15"/></td>
        </tr>
        <tr>
            <td>Concentraci&oacute;n/Cantidad inicial:</td>
            <td><input type="text" id="concentracionI" value="<?= $Gestor->Formato($ROW[0]['concentracionI']) ?>"
                       onblur="_FLOAT(this);Calcula();" class="monto"/></td>
            <td colspan="2"><select id="medida1">
                    <option value="0">M</option>
                    <option value="1" <?php if ($ROW[0]['med1'] == '1') echo 'selected' ?>>&mu;M</option>
                    <option value="2" <?php if ($ROW[0]['med1'] == '2') echo 'selected' ?>>nM</option>
                    <option value="3" <?php if ($ROW[0]['med1'] == '3') echo 'selected' ?>>mM</option>
                    <option value="4" <?php if ($ROW[0]['med1'] == '4') echo 'selected' ?>>pM</option>
                    <option value="5" <?php if ($ROW[0]['med1'] == '5') echo 'selected' ?>>&mu;L</option>
                    <option value="6" <?php if ($ROW[0]['med1'] == '6') echo 'selected' ?>>mL</option>
                    <option value="7" <?php if ($ROW[0]['med1'] == '7') echo 'selected' ?>>L</option>
                    <option value="8" <?php if ($ROW[0]['med1'] == '8') echo 'selected' ?>>g</option>
                    <option value="9" <?php if ($ROW[0]['med1'] == '9') echo 'selected' ?>>&mu;g</option>
                    <option value="A" <?php if ($ROW[0]['med1'] == 'A') echo 'selected' ?>>ng</option>
                </select></td>
        </tr>
        <tr>
            <td>Volumen Disolvente:</td>
            <td><input type="text" id="vol_disolvente" value="<?= $Gestor->Formato($ROW[0]['vol_disolvente']) ?>"
                       onblur="_FLOAT(this);Calcula();" class="monto"/></td>
            <td colspan="2"><select id="medida2">
                    <option value="0">&mu;l</option>
                    <option value="1" <?php if ($ROW[0]['med2'] == '1') echo 'selected' ?>>&mu;M</option>
                    <option value="2" <?php if ($ROW[0]['med2'] == '2') echo 'selected' ?>>nM</option>
                    <option value="3" <?php if ($ROW[0]['med2'] == '3') echo 'selected' ?>>mM</option>
                    <option value="4" <?php if ($ROW[0]['med2'] == '4') echo 'selected' ?>>pM</option>
                    <option value="5" <?php if ($ROW[0]['med2'] == '5') echo 'selected' ?>>&mu;L</option>
                    <option value="6" <?php if ($ROW[0]['med2'] == '6') echo 'selected' ?>>mL</option>
                    <option value="7" <?php if ($ROW[0]['med2'] == '7') echo 'selected' ?>>L</option>
                    <option value="8" <?php if ($ROW[0]['med2'] == '8') echo 'selected' ?>>g</option>
                    <option value="9" <?php if ($ROW[0]['med2'] == '9') echo 'selected' ?>>&mu;g</option>
                    <option value="A" <?php if ($ROW[0]['med2'] == 'A') echo 'selected' ?>>ng</option>
                </select></td>
        </tr>
        <tr>
            <td>Concentraci&oacute;n Madre:</td>
            <td><input type="text" id="concentracionM" value="<?= $Gestor->Formato($ROW[0]['concentracionM']) ?>"
                       onblur="_FLOAT(this);Calcula();" class="monto"/></td>
            <td colspan="2"><select id="medida3">
                    <option value="0">&mu;l</option>
                    <option value="1" <?php if ($ROW[0]['med3'] == '1') echo 'selected' ?>>&mu;M</option>
                    <option value="2" <?php if ($ROW[0]['med3'] == '2') echo 'selected' ?>>nM</option>
                    <option value="3" <?php if ($ROW[0]['med3'] == '3') echo 'selected' ?>>mM</option>
                    <option value="4" <?php if ($ROW[0]['med3'] == '4') echo 'selected' ?>>pM</option>
                    <option value="5" <?php if ($ROW[0]['med3'] == '5') echo 'selected' ?>>&mu;L</option>
                    <option value="6" <?php if ($ROW[0]['med3'] == '6') echo 'selected' ?>>mL</option>
                    <option value="7" <?php if ($ROW[0]['med3'] == '7') echo 'selected' ?>>L</option>
                    <option value="8" <?php if ($ROW[0]['med3'] == '8') echo 'selected' ?>>g</option>
                    <option value="9" <?php if ($ROW[0]['med3'] == '9') echo 'selected' ?>>&mu;g</option>
                    <option value="A" <?php if ($ROW[0]['med3'] == 'A') echo 'selected' ?>>ng</option>
                </select></td>
        </tr>
        <tr>
            <td>Volumen a tomar</td>
            <td colspan="3"><input type="text" readonly id="formula" class="monto"/></td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="98%">
        <tr>
            <td class="titulo" colspan="3">Detalle&nbsp;<img onclick="ResAgregarLinea()"
                                                             src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                                                             title="Agregar línea" class="tab"/></td>
        </tr>
        <tr align="center">
            <td><strong>C&oacute;digo al&iacute;cuota soluci&oacute;n madre</strong></td>
            <td><strong>Observaciones</strong></td>
            <td><strong>Estado</strong></td>
        </tr>
        <tbody id="lolo">
        <?php
        $ROW = $Gestor->ResLineasMuestra();
        for ($i = 0; $i < count($ROW); $i++) {
            ?>
            <tr align="center" onmouseout="mOut(this)" onmouseover="mOver(this)">
                <td id="linea<?= $i ?>"></td>
                <td><input type="text" name="observaciones" value="<?= $ROW[$i]['observaciones'] ?>"/></td>
                <td><select name="estado">
                        <option value="1" <?php if ($ROW[$i]['estado'] == '1') echo 'selected'; ?>>Disponible</option>
                        <option value="0" <?php if ($ROW[$i]['estado'] == '0') echo 'selected'; ?>>Agotado</option>
                    </select></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
</center>
<br/><?= $Gestor->Encabezado('A0011', 'p', '') ?>
<script>Calcula();
    Sort();</script>
</body>
</html>