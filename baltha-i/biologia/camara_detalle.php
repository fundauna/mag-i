<?php
define('__MODULO__', 'biologia');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);
$Gestor = new _camaras_detalle();
$ROW = $Gestor->obtieneDatos();
$LINEAS = $Gestor->obtieneLineas();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('', 'hr', 'Inventario :: Detalle C&aacute;mara') ?>
<center>
    <form name="form" id="form">
        <input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
        <table class="radius" width="98%">
            <tr>
                <td class="titulo" colspan="2">Detalle de la c&aacute;mara</td>
            </tr>
            <?php if ($_GET['acc'] == 'M'): ?>
                <tr>
                    <td>C&oacute;digo:</td>
                    <td><input readonly type="text" id="id" value="<?= $ROW[0]['id'] ?>" size="15" maxlength="15"
                               title="Alfanumérico (4/15)"/></td>
                </tr>
            <?php endif; ?>
            <tr>
                <td>Descripci&oacute;n:</td>
                <td><input type="text" id="descripcion" value="<?= $ROW[0]['descripcion'] ?>" size="15" maxlength="15"
                           title="Alfanumérico (4/15)"/></td>
            </tr>
            <tr>
                <td>Estado:</td>
                <td>
                    <select id="estado">
                        <option <?= $ROW[0]['estado'] == '1' ? 'selected' : '' ?> value="1">Activo</option>
                        <option <?= $ROW[0]['estado'] == '0' ? 'selected' : '' ?> value="0">Inactivo</option>
                    </select>
                </td>
            </tr>
        </table>
        <br/>
        <table class="radius" width="98%">
            <thead>
            <tr>
                <td class="titulo" colspan="6">Bandejas <img onclick="agregarBandeja()"
                                                             src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                                                             title="Agregar línea" class="tab"/></td>
            </tr>
            <tr>
                <td><strong>Descripci&oacute;n</strong></td>
                <td><strong>Dimensi&oacute;n X ?:</strong></td>
                <td title="Cantidad de muestras"><strong>Dimensi&oacute;n Y ?:</strong></td>
                <td><strong>Rotulo X ?:</strong></td>
                <td><strong>Rotulo Y ?:</strong></td>
                <td><strong>Opciones</strong></td>
            </tr>
            </thead>
            <tbody id="lolo">
            <?php foreach ($LINEAS as $dato): ?>
                <tr>
                    <td><input class="descripcion_v" value="<?= $dato['descripcion'] ?>" type="text"/></td>
                    <td><input class="dimensionx" value="<?= $dato['dimensionx'] ?>" type="number" step="0.01"/></td>
                    <td><input class="dimensiony" value="<?= $dato['dimensiony'] ?>" type="number" step="0.01"/></td>
                    <td>
                        <select class="rotulox">
                            <option <?= $dato['rotulox'] == 'l' ? 'selected' : '' ?> value="l">Letras</option>
                            <option <?= $dato['rotulox'] == 'n' ? 'selected' : '' ?> value="n">N&uacute;meros</option>
                        </select>
                    </td>
                    <td>
                        <select class="rotuloy">
                            <option <?= $dato['rotuloy'] == 'l' ? 'selected' : '' ?> value="l">Letras</option>
                            <option <?= $dato['rotuloy'] == 'n' ? 'selected' : '' ?> value="n">N&uacute;meros</option>
                        </select>
                    </td>
                    <td>
                        <input type="hidden" class="bandejalinea" value="<?= $dato['linea'] ?>"/>
                        <img onclick="eliminarBandeja(<?= $dato['linea'] ?>)"
                             src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar" class="tab2"/>
                        <img onclick="cajas(<?= $dato['id_camara'] ?>,<?= $dato['linea'] ?>)"
                             src="<?php $Gestor->Incluir('tabla', 'bkg') ?>" title="Ver/Modificar Cajas" class="tab2"/>
                    </td>
                </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
        <br/>
        <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()"/>
    </form>
</center>
</body>
</html>