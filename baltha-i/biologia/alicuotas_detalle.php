<?php
define('__MODULO__', 'biologia');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _alicuotas_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('a0003', 'hr', 'Inventario :: Detalle Control de al�cuotas') ?>
<?= $Gestor->Encabezado('A0003', 'e', 'Control de al�cuotas') ?>
<center>
    <input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
    <table class="radius" width="98%" style="font-size:12px;">
        <tr>
            <td class="titulo">Informaci&oacute;n general</td>
        </tr>
        <tr>
            <td><strong>C&oacute;digo &uacute;nico:</strong>&nbsp;<input type="text" id="cs" size="20" maxlength="30"
                                                                         value="<?= $ROW[0]['cs'] ?>"
                                                                         <?= ($_GET['acc'] == 'M') ? 'readonly' : 'onblur="Sort()"' ?>>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="98%">
        <tr>
            <td class="titulo" colspan="7">Detalle al&iacute;cuotas de trabajo&nbsp;<img onclick="ResAgregarLinea()"
                                                                                         src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                                                                                         title="Agregar l�nea"
                                                                                         class="tab"/></td>
        </tr>
        <tr>
            <td align="center"><strong>C&oacute;digo</strong></td>
            <td align="center"><strong>Tipo</strong></td>
            <td align="center"><strong>Nombre</strong></td>
            <td align="center"><strong>Estado</strong></td>
            <td align="center"><strong>Observaciones</strong></td>
            <td align="center"><strong>Fecha</strong></td>
            <td align="center"><strong>Analista</strong></td>
        </tr>
        <tbody id="lolo">
        <?php
        $ROW2 = $Gestor->Detalle();
        for ($i = 0; $i < count($ROW2); $i++) {
            ?>
            <tr align="center">
                <td id="linea<?= $i ?>"></td>
                <td><select id="tipo<?= $i ?>" name="tipo">
                        <option value="">...</option>
                        <option value="0" <?php if ($ROW2[$i]['tipo'] == '0') echo 'selected'; ?>>Agua</option>
                        <option value="1" <?php if ($ROW2[$i]['tipo'] == '1') echo 'selected'; ?>>Controles</option>
                        <option value="2" <?php if ($ROW2[$i]['tipo'] == '2') echo 'selected'; ?>>dNTP</option>
                        <option value="3" <?php if ($ROW2[$i]['tipo'] == '3') echo 'selected'; ?>>Primer</option>
                        <option value="4" <?php if ($ROW2[$i]['tipo'] == '4') echo 'selected'; ?>>Sonda</option>
                        <option value="5" <?php if ($ROW2[$i]['tipo'] == '5') echo 'selected'; ?>>Otro</option>
                    </select></td>
                <td><input type="text" id="nombre<?= $i ?>" name="nombre" size="20" maxlength="20"
                           value="<?= $ROW2[$i]['nombre'] ?>"/></td>
                <td><select id="estado<?= $i ?>" name="estado">
                        <option value="">...</option>
                        <option value="0" <?php if ($ROW2[$i]['estado'] == '0') echo 'selected'; ?>>Agotado</option>
                        <option value="1" <?php if ($ROW2[$i]['estado'] == '1') echo 'selected'; ?>>Disponible</option>
                    </select></td>
                <td><textarea name="observaciones"><?= $ROW2[$i]['obs'] ?></textarea></td>
                <td><?= $ROW2[$i]['fecha'] ?></td>
                <td><?= $ROW2[$i]['analista'] ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <br/>
    <br><?= $Gestor->Encabezado('A0003', 'p', '') ?>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
</center>
<script>Sort();</script>
</body>
</html>