<?php
define('__MODULO__', 'biologia');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _adenoteca();
$ROW = $Gestor->obtieneDatos();
$anterior = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('a0', 'hr', 'Inventario :: ADNoteca-ARNoteca') ?>
<?= $Gestor->Encabezado('A0000', 'e', 'ADNoteca-ARNoteca') ?>
<center>
    <br/>
    <div id="container" style="width:500px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>Plaga</th>
                <th>Cajas</th>

            </tr>
            </thead>
            <tbody>
            <?php foreach ($ROW as $i => $datos): ?>
                <?php if ($anterior == ''): ?>
                    <tr class="gradeA" align="center">
                    <td><?= $datos['nombre'] ?></td>

                    <td>
                    <select id="" name="">
                    <option value="">...</option>
                    <?php if ($datos['id_caja']): ?>
                        <option value="<?= $datos['id_caja'] ?>"><?= $datos['nombrecaja'] ?></option>
                    <?php endif; ?>

                <?php elseif ($anterior == $datos['id']): ?>
                    <option value="<?= $datos['id_caja'] ?>"><?= $datos['nombrecaja'] ?></option>
                <?php else: ?>
                    </select>
                    </td>
                    </tr>
                    <tr class="gradeA" align="center">
                    <td><?= $datos['nombre'] ?></td>

                    <td>
                    <select id="" name="">
                    <option value="">...</option>
                    <?php if ($datos['id_caja']): ?>
                        <option value="<?= $datos['id_caja'] ?>"><?= $datos['nombrecaja'] ?></option>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if ((count($ROW) - 1) == $i): ?>
                    </select>
                    </td>
                    </tr>
                <?php endif; ?>
                <?php $anterior = $datos['id']; ?>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <br/><?= $Gestor->Encabezado('A0000', 'p', '') ?>
    <br/>
</center>
</body>
</html>