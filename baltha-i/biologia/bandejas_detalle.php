<?php
define('__MODULO__', 'biologia');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _bandejas_detalle();
$ROW = $Gestor->BandejasMuestra();
if (!$ROW) die('Bandeja inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <script>
        function Escoge(rotulo, y, x) {
            //if(typeof window.dialogArguments!='undefined'){
            /*window.dialogArguments*/
            opener.padre(rotulo, y, x, '<?=$_GET['bandeja']?>');
            window.close();
            //}
        }
    </script>
</head>
<body>
<?php $Gestor->Incluir('a0005', 'hr', 'Inventario :: Distribución de bandejas') ?>
<?= $Gestor->Encabezado('A0005', 'e', 'Distribución de bandejas') ?>
<table class="radius" align="center" border="1">
    <?php
    for ($y = 0; $y < $ROW[0]['dimy']; $y++) {
        ?>
        <tr align="center">
            <?php
            for ($x = 0; $x < $ROW[0]['dimx']; $x++) {
                $rotulo = $Gestor->Label($ROW[0]['labely'], $y) . $Gestor->Label($ROW[0]['labelx'], $x)
                ?>
                <td style="height:40px;width:40px;cursor:pointer;" id="T<?= $y . $x ?>"
                    onclick="Escoge('<?= $rotulo ?>', '<?= $y ?>', '<?= $x ?>')"><?= $rotulo ?></td>
                <?php
            }
            ?>
        </tr>
        <?php
    }
    ?>
</table>
<script>
    <?php
    $ROW = $Gestor->BandejasMuestraCajas();
    for($x = 0;$x < count($ROW);$x++){
    ?>
    document.getElementById('T<?=$ROW[$x]['y'], $ROW[$x]['x']?>').title = '<?=$ROW[$x]['valor1'], '\n', $ROW[$x]['valor2']?>';
    document.getElementById('T<?=$ROW[$x]['y'], $ROW[$x]['x']?>').style.color = '#FF0000';
    document.getElementById('T<?=$ROW[$x]['y'], $ROW[$x]['x']?>').onclick = false;
    <?php
    }
    ?>
</script>
<br/><?= $Gestor->Encabezado('A0005', 'p', '') ?>
</body>
</html>