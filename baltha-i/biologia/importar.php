<?php
define('__MODULO__', 'biologia');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _importar();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('f3', 'hr', 'Inventario :: Importar ADN-ARNoteca') ?>
<?= $Gestor->Encabezado('F0013', 'e', 'Importar ADN-ARNoteca') ?>
<center>
    <form action="../../caspha-i/shell/biologia/_importa.php" method="post" enctype="multipart/form-data"
          id="formulario" name="formulario">
        <input type="hidden" id="token" name="token"/>
        <table class="radius" align="center" style="width:580px">
            <tr>
                <td class="titulo" colspan="5">Importar ADN-ARNoteca</td>
            </tr>
            <tr>
                <td>Archivo en Excel: <input type="file" name="archivo" id="archivo"></td>
                <td><input type="button" value="Importar" class="boton2" onclick="InventarioImportar()"/></td>
            </tr>
        </table>
    </form>
    <br/><br/><br/><br/><a href="../../caspha-i/docs/formatoBM.xlsx">Descargar Formato</a>
</center>
<br/><?= $Gestor->Encabezado('F0013', 'p', '') ?>
</body>
</html>