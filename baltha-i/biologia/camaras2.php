<?php
define('__MODULO__', 'biologia');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _camaras2();
$ROW = $Gestor->obtieneDatos();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('a0', 'hr', 'Inventario :: C&aacute;maras') ?>
<center>
    <br/>
    <div id="container" style="width:500px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>Id</th>
                <th>Descripci&oacute;n</th>
                <th>Opciones</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($ROW as $dato): ?>
                <tr class="gradeA" align="center">
                    <td><?= $dato['id'] ?></td>
                    <td><?= $dato['descripcion'] ?></td>
                    <td>
                        <a onclick="camara_detalle(<?= $dato['id'] ?>)" title="Detalle" href="#"
                           target="detalle"><span><?php $Gestor->Incluir('buscar', 'ico') ?></span></a>
                    </td>
                </tr>
            <?php endforeach; ?>

            </tbody>
        </table>

    </div>
    <br/><input type="button" value="Agregar C&aacute;mara" class="boton" onClick="camara_detalle();">

</center>
</body>
</html>