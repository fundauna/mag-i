<?php
define('__MODULO__', 'biologia');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _proc_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

if ($_GET['acc'] != 'I') $disabled = 'readonly';
else $disabled = '';

if ($_GET['acc'] == 'V') {
    $disabled2 = 'disabled';
    $display = 'style="display:none"';
} else {
    $disabled2 = '';
    $display = '';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('a0009', 'hr', 'Inventario :: Detalle Mantenimiento de procedimientos') ?>
<?= $Gestor->Encabezado('A0009', 'e', 'Mantenimiento de procedimientos') ?>
<center>
    <form name="formulario" method="post" enctype="multipart/form-data"
          action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
        <input type="hidden" name="_AJAX" value="1"/>
        <input type="hidden" id="accion" name="accion" value="<?= $_GET['acc'] ?>"/>
        <input type="hidden" id="tipo"/><!-- PARA VER CUAL LISTADOR EST� ACTIVO -->
        <table class="radius" width="98%">
            <tr>
                <td class="titulo" colspan="2">Informaci&oacute;n General</td>
            </tr>
            <tr>
                <td><strong>C&oacute;digo:</strong></td>
                <td><input type="text" id="cs" name="cs" size="15" maxlength="20" value="<?= $ROW[0]['cs'] ?>"
                           <?= $disabled ?> <?= $disabled2 ?>/></td>
            </tr>
            <tr>
                <td><strong>Nombre:</strong></td>
                <td><input type="text" id="nombre" name="nombre" size="50" maxlength="100"
                           value="<?= $ROW[0]['nombre'] ?>" <?= $disabled2 ?>/></td>
            </tr>
            <tr>
                <td><strong>Archivo del procedimiento:</strong>
                    <?php
                    $eliminar = false;
                    $ruta = "../../caspha-i/docs/biologia/procs/" . str_replace(' ', '', $ROW[0]['cs']) . '.zip';
                    if (file_exists($ruta)) {
                        $eliminar = true;
                        ?>
                        <img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar"
                             onclick="DocumentosZip('<?= $ROW[0]['cs'] ?>', '<?= __MODULO__ ?>/procs')" class="tab2"/>
                    <?php } ?>
                </td>
                <td><input type="file" name="archivo" <?= $disabled2 ?>/></td>
            </tr>
            <?php if ($eliminar) { ?>
                <tr>
                    <td><input type="checkbox" name="eliminar" <?= $disabled2 ?>/>&nbsp;Eliminar documento</td>
                </tr>
            <?php } ?>
        </table>
        <br/>
        <table class="radius" width="98%">
            <tr>
                <td class="titulo" colspan="3">Informaci&oacute;n de An&aacute;lisis</td>
            </tr>
            <tr align="center" style="vertical-align:top">
                <td>
                    <img onclick="AgregarAnalisis('5', 'A1')" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                         title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>An&aacute;lisis:</strong>
                    <table class="radius">
                        <tbody id="loloA1">
                        <?php
                        $ROW2 = $Gestor->DetalleAnalisis('1');
                        for ($i = 0; $i < count($ROW2); $i++) {
                            ?>
                            <tr>
                                <td><?= $i + 1 ?></td>
                                <td><input type="text" id="A1analisis<?= $i ?>" class="lista2" readonly
                                           onclick="MacroAnalisisLista('5', 'A1', '<?= $i ?>')"
                                           value="<?= $ROW2[$i]['nombre'] ?>" <?= $disabled2 ?>/><input type="hidden"
                                                                                                        id="A1cod_analisis<?= $i ?>"
                                                                                                        name="A1cod_analisis[]"
                                                                                                        value="<?= $ROW2[$i]['id'] ?>"/>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </td>
                <td>
                    <img onclick="AgregarAnalisis('7', 'A2')" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                         title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>Tipos de material:</strong>
                    <table class="radius">
                        <tbody id="loloA2">
                        <?php
                        $ROW2 = $Gestor->DetalleAnalisis('2');
                        for ($i = 0; $i < count($ROW2); $i++) {
                            ?>
                            <tr>
                                <td><?= $i + 1 ?></td>
                                <td><input type="text" id="A2analisis<?= $i ?>" class="lista2" readonly
                                           onclick="MacroAnalisisLista('7', 'A2', '<?= $i ?>')"
                                           value="<?= $ROW2[$i]['nombre'] ?>" <?= $disabled2 ?>/><input type="hidden"
                                                                                                        id="A2cod_analisis<?= $i ?>"
                                                                                                        name="A2cod_analisis[]"
                                                                                                        value="<?= $ROW2[$i]['id'] ?>"/>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </td>
                <td>
                    <img onclick="AgregarAnalisis('6', 'A3')" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                         title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>Plagas a detectar:</strong>
                    <table class="radius">
                        <tbody id="loloA3">
                        <?php
                        $ROW2 = $Gestor->DetalleAnalisis('3');
                        for ($i = 0; $i < count($ROW2); $i++) {
                            ?>
                            <tr>
                                <td><?= $i + 1 ?></td>
                                <td><input type="text" id="A3analisis<?= $i ?>" class="lista2" readonly
                                           onclick="MacroAnalisisLista('6', 'A3', '<?= $i ?>')"
                                           value="<?= $ROW2[$i]['nombre'] ?>" <?= $disabled2 ?>/><input type="hidden"
                                                                                                        id="A3cod_analisis<?= $i ?>"
                                                                                                        name="A3cod_analisis[]"
                                                                                                        value="<?= $ROW2[$i]['id'] ?>"/>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
        <br/>
        <table class="radius" width="98%">
            <tr>
                <td class="titulo" colspan="2">Extracci&oacute;n</td>
            </tr>
            <tr align="center" style="vertical-align:top">
                <td>
                    <img onclick="AgregarInsumos('C1')" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                         title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>Insumos utilizados:</strong>
                    <table class="radius">
                        <tr align="center">
                            <td><strong>Nombre</strong></td>
                            <td><strong>Cantidad</strong></td>
                        </tr>
                        <tbody id="loloC1">
                        <?php
                        $ROW2 = $Gestor->DetalleInsumos('C');
                        for ($i = 0; $i < count($ROW2); $i++) {
                            ?>
                            <tr>
                                <td><?= $i + 1 ?>&nbsp;&nbsp;(<?= $ROW2[$i]['codigo'] ?>)&nbsp;<input type="text"
                                                                                                      id="C1insumos<?= $i ?>"
                                                                                                      class="lista2"
                                                                                                      readonly
                                                                                                      onclick="InsumosLista('C1', <?= $i ?>)"
                                                                                                      value="<?= $ROW2[$i]['nombre'] ?>"
                                                                                                      <?= $disabled2 ?>/><input
                                            type="hidden" id="C1cod_insumos<?= $i ?>" name="C1cod_insumos[]"
                                            value="<?= $ROW2[$i]['id'] ?>"/></td>
                                <td><input type="text" id="C1total<?= $i ?>" name="C1total[]" class="monto2"
                                           value="<?= $ROW2[$i]['cantidad'] ?>" <?= $disabled2 ?>
                                           onblur="_FLOAT(this)"/></td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </td>
                <td>
                    <img onclick="AgregarEquipos('C2')" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                         title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>Equipos utilizados:</strong>
                    <table class="radius">
                        <tbody id="loloC2">
                        <?php
                        $ROW2 = $Gestor->DetalleEquipos('C');
                        for ($i = 0; $i < count($ROW2); $i++) {
                            ?>
                            <tr>
                                <td><?= $i + 1 ?></td>
                                <td>(<?= $ROW2[$i]['codigo'] ?>) <input type="text" id="C2equipos<?= $i ?>"
                                                                        class="lista2" readonly
                                                                        onclick="EquiposLista('C2', <?= $i ?>)"
                                                                        value="<?= $ROW2[$i]['nombre'] ?>"
                                                                        <?= $disabled2 ?>/><input type="hidden"
                                                                                                  id="C2cod_equipos<?= $i ?>"
                                                                                                  name="C2cod_equipos[]"
                                                                                                  value="<?= $ROW2[$i]['id'] ?>"/>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
        <br/>
        <table class="radius" width="98%">
            <tr>
                <td class="titulo" colspan="3">Cuantificaci&oacute;n</td>
            </tr>
            <tr>
                <td><strong>Concentraci&oacute;n normalizada (ng/&mu;L):</strong></td>
                <td colspan="2"><input type="text" id="Dnormalizada" name="Dnormalizada" class="monto"
                                       onblur="_FLOAT(this)" value="<?= $ROW[0]['Dnormalizada'] ?>" <?= $disabled2 ?>>
                </td>
            </tr>
            <tr>
                <td><strong>Analito:</strong></td>
                <td><select id="Danalito" name="Danalito" <?= $disabled2 ?>>
                        <option value="">...</option>
                        <option value="0" <?php if ($ROW[0]['Danalito'] == '0') echo 'selected'; ?>>ADN
                            gen&oacute;mico
                        </option>
                        <option value="1" <?php if ($ROW[0]['Danalito'] == '1') echo 'selected'; ?>>ARN total</option>
                        <option value="2" <?php if ($ROW[0]['Danalito'] == '2') echo 'selected'; ?>>ADN doble banda
                        </option>
                        <option value="3" <?php if ($ROW[0]['Danalito'] == '3') echo 'selected'; ?>>ADN simple banda
                        </option>
                        <option value="4" <?php if ($ROW[0]['Danalito'] == '4') echo 'selected'; ?>>ARN</option>
                    </select></td>
            </tr>
            <tr>
                <td><strong>Unidad:</strong></td>
                <td><select id="Dunidad" name="Dunidad" <?= $disabled2 ?>>
                        <option value="">...</option>
                        <option value="0" <?php if ($ROW[0]['Dunidad'] == '0') echo 'selected'; ?>>&mu;g/&mu;L</option>
                        <option value="1" <?php if ($ROW[0]['Dunidad'] == '1') echo 'selected'; ?>>ng/&mu;L</option>
                    </select></td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr/>
                </td>
            </tr>
            <tr style="vertical-align:top">
                <td colspan="2">
                    <img onclick="AgregarInsumos('D1')" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                         title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>Insumos utilizados:</strong>
                    <table class="radius">
                        <tr align="center">
                            <td><strong>Nombre</strong></td>
                            <td><strong>Cantidad</strong></td>
                        </tr>
                        <tbody id="loloD1">
                        <?php
                        $ROW2 = $Gestor->DetalleInsumos('D');
                        for ($i = 0; $i < count($ROW2); $i++) {
                            ?>
                            <tr>
                                <td><?= $i + 1 ?>&nbsp;&nbsp;(<?= $ROW2[$i]['codigo'] ?>)&nbsp;<input type="text"
                                                                                                      id="D1insumos<?= $i ?>"
                                                                                                      class="lista2"
                                                                                                      readonly
                                                                                                      onclick="InsumosLista('D1', <?= $i ?>)"
                                                                                                      value="<?= $ROW2[$i]['nombre'] ?>"
                                                                                                      <?= $disabled2 ?>/><input
                                            type="hidden" id="D1cod_insumos<?= $i ?>" name="D1cod_insumos[]"
                                            value="<?= $ROW2[$i]['id'] ?>"/></td>
                                <td><input type="text" id="D1total<?= $i ?>" name="D1total[]" class="monto2"
                                           value="<?= $ROW2[$i]['cantidad'] ?>" <?= $disabled2 ?>
                                           onblur="_FLOAT(this)"/></td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </td>
                <td>
                    <img onclick="AgregarEquipos('D2')" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                         title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>Equipos utilizados:</strong>
                    <table class="radius">
                        <tbody id="loloD2">
                        <?php
                        $ROW2 = $Gestor->DetalleEquipos('D');
                        for ($i = 0; $i < count($ROW2); $i++) {
                            ?>
                            <tr>
                                <td><?= $i + 1 ?></td>
                                <td>(<?= $ROW2[$i]['codigo'] ?>) <input type="text" id="D2equipos<?= $i ?>"
                                                                        class="lista2" readonly
                                                                        onclick="EquiposLista('D2', <?= $i ?>)"
                                                                        value="<?= $ROW2[$i]['nombre'] ?>"
                                                                        <?= $disabled2 ?>/><input type="hidden"
                                                                                                  id="D2cod_equipos<?= $i ?>"
                                                                                                  name="D2cod_equipos[]"
                                                                                                  value="<?= $ROW2[$i]['id'] ?>"/>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
        <br/>
        <table class="radius" width="98%">
            <tr>
                <td class="titulo" colspan="2">PCR</td>
            </tr>
            <tr>
                <td colspan="2">
                    <img onclick="AgregarPlantilla1()" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                         title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>Plantilla de mezcla de reacci&oacute;n:</strong>
                    <table class="radius">
                        <tr>
                            <td><strong>#</strong></td>
                            <td><strong>Tipo</strong></td>
                            <td><strong>Descripci&oacute;n</strong></td>
                            <td><strong>Conc. Stock</strong></td>
                            <td><strong>Conc. Final</strong></td>
                            <td><strong>Volumen 1x (&mu;L)</strong></td>
                        </tr>
                        <tbody id="loloE1">
                        <?php
                        $ROW2 = $Gestor->DetallePlantilla('1');
                        for ($i = 0; $i < count($ROW2); $i++) {
                            ?>
                            <tr>
                                <td><?= $i + 1 ?></td>
                                <td><select name="E1tipo[]" <?= $disabled2 ?>>
                                        <option value="">...</option>
                                        <option value="0" <?php if ($ROW2[$i]['tipo'] == '0') echo 'selected'; ?>>
                                            Reactivo
                                        </option>
                                        <option value="1" <?php if ($ROW2[$i]['tipo'] == '1') echo 'selected'; ?>>
                                            Alicuota
                                        </option>
                                    </select></td>
                                <td><input type="text" name="E1descr[]" size="20" maxlength="20"
                                           value="<?= $ROW2[$i]['descr'] ?>" <?= $disabled2 ?>/></td>
                                <td><input type="text" name="E1concS[]" size="10" maxlength="20"
                                           value="<?= $ROW2[$i]['concS'] ?>" <?= $disabled2 ?>/></td>
                                <td><input type="text" name="E1concF[]" size="10" maxlength="20"
                                           value="<?= $ROW2[$i]['concF'] ?>" <?= $disabled2 ?>/></td>
                                <td><input type="text" name="E1volumen[]" class="monto2"
                                           value="<?= $ROW2[$i]['volumen'] ?>" onblur="_FLOAT(this)" <?= $disabled2 ?>/>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <img onclick="AgregarEquipos('E2')" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                         title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>Equipos utilizados:</strong>
                    <table class="radius">
                        <tbody id="loloE2">
                        <?php
                        $ROW2 = $Gestor->DetalleEquipos('E');
                        for ($i = 0; $i < count($ROW2); $i++) {
                            ?>
                            <tr>
                                <td><?= $i + 1 ?></td>
                                <td>(<?= $ROW2[$i]['codigo'] ?>) <input type="text" id="E2equipos<?= $i ?>"
                                                                        class="lista2" readonly
                                                                        onclick="EquiposLista('E2', <?= $i ?>)"
                                                                        value="<?= $ROW2[$i]['nombre'] ?>"
                                                                        <?= $disabled2 ?>/><input type="hidden"
                                                                                                  id="E2cod_equipos<?= $i ?>"
                                                                                                  name="E2cod_equipos[]"
                                                                                                  value="<?= $ROW2[$i]['id'] ?>"/>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td colspan="2"><strong>Programa del termociclador:</strong>&nbsp;
                    <input type="text" id="Eprograma" name="Eprograma" size="10" maxlength="20"
                           value="<?= $ROW[0]['Eprograma'] ?>" <?= $disabled2 ?>/></td>
            </tr>
        </table>
        <br/>
        <table class="radius" width="98%">
            <tr>
                <td class="titulo" colspan="2">Electroforesis PCR</td>
            </tr>
            <tr style="vertical-align:top">
                <td>
                    <img onclick="AgregarInsumos('F1')" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                         title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>Insumos utilizados:</strong>
                    <table class="radius">
                        <tr align="center">
                            <td><strong>Nombre</strong></td>
                            <td><strong>Cantidad</strong></td>
                        </tr>
                        <tbody id="loloF1">
                        <?php
                        $ROW2 = $Gestor->DetalleInsumos('F');
                        for ($i = 0; $i < count($ROW2); $i++) {
                            ?>
                            <tr>
                                <td><?= $i + 1 ?>&nbsp;&nbsp;(<?= $ROW2[$i]['codigo'] ?>)&nbsp;<input type="text"
                                                                                                      id="F1insumos<?= $i ?>"
                                                                                                      class="lista2"
                                                                                                      readonly
                                                                                                      onclick="InsumosLista('F1', <?= $i ?>)"
                                                                                                      value="<?= $ROW2[$i]['nombre'] ?>"
                                                                                                      <?= $disabled2 ?>/><input
                                            type="hidden" id="F1cod_insumos<?= $i ?>" name="F1cod_insumos[]"
                                            value="<?= $ROW2[$i]['id'] ?>"/></td>
                                <td><input type="text" id="F1total<?= $i ?>" name="F1total[]" class="monto2"
                                           value="<?= $ROW2[$i]['cantidad'] ?>" onblur="_FLOAT(this)"
                                           <?= $disabled2 ?>/></td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </td>
                <td>
                    <img onclick="AgregarEquipos('F2')" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                         title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>Equipos utilizados:</strong>
                    <table class="radius">
                        <tbody id="loloF2">
                        <?php
                        $ROW2 = $Gestor->DetalleEquipos('F');
                        for ($i = 0; $i < count($ROW2); $i++) {
                            ?>
                            <tr>
                                <td><?= $i + 1 ?></td>
                                <td>(<?= $ROW2[$i]['codigo'] ?>) <input type="text" id="F2equipos<?= $i ?>"
                                                                        class="lista2" readonly
                                                                        onclick="EquiposLista('F2', <?= $i ?>)"
                                                                        value="<?= $ROW2[$i]['nombre'] ?>"
                                                                        <?= $disabled2 ?>/><input type="hidden"
                                                                                                  id="F2cod_equipos<?= $i ?>"
                                                                                                  name="F2cod_equipos[]"
                                                                                                  value="<?= $ROW2[$i]['id'] ?>"/>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td colspan="2"><strong>Tiempo (min):</strong>&nbsp;
                    <input type="text" id="Ftiempo" name="Ftiempo" class="monto" onblur="_FLOAT(this)"
                           value="<?= $ROW[0]['Ftiempo'] ?>" <?= $disabled2 ?>></td>
            </tr>
            <tr>
                <td colspan="2"><strong>Voltaje (V):</strong>&nbsp;
                    <input type="text" id="Fvoltaje" name="Fvoltaje" class="monto" onblur="_FLOAT(this)"
                           value="<?= $ROW[0]['Fvoltaje'] ?>" <?= $disabled2 ?>></td>
            </tr>
        </table>
        <br/>
        <table class="radius" width="98%">
            <tr>
                <td class="titulo" colspan="2">Digesti&oacute;n con enzimas de restricci&oacute;n</td>
            </tr>
            <tr>
                <td colspan="2">
                    <img onclick="AgregarPlantilla2()" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                         title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>Plantilla de mezcla de digesti&oacute;n:</strong>
                    <table class="radius">
                        <tr>
                            <td><strong>#</strong></td>
                            <td><strong>Tipo</strong></td>
                            <td><strong>Descripci&oacute;n</strong></td>
                            <td><strong>Volumen 1x (&mu;L)</strong></td>
                        </tr>
                        <tbody id="loloG1">
                        <?php
                        $ROW2 = $Gestor->DetallePlantilla('2');
                        for ($i = 0; $i < count($ROW2); $i++) {
                            ?>
                            <tr>
                                <td><?= $i + 1 ?></td>
                                <td><select name="G1tipo[]" <?= $disabled2 ?>>
                                        <option value="">...</option>
                                        <option value="0" <?php if ($ROW2[$i]['tipo'] == '0') echo 'selected'; ?>>
                                            Reactivo
                                        </option>
                                        <option value="1" <?php if ($ROW2[$i]['tipo'] == '1') echo 'selected'; ?>>
                                            Alicuota
                                        </option>
                                    </select></td>
                                <td><input type="text" name="G1descr[]" size="20" maxlength="20"
                                           value="<?= $ROW2[$i]['descr'] ?>" <?= $disabled2 ?>/></td>
                                <td><input type="text" name="G1volumen[]" class="monto2"
                                           value="<?= $ROW2[$i]['volumen'] ?>" onblur="_FLOAT(this)" <?= $disabled2 ?>/>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="2" align="center">Producto PCR:</td>
                            <td><input type="text" name="Gamplicon" class="monto2" value="<?= $ROW[0]['Gamplicon'] ?>"
                                       onblur="_FLOAT(this)" <?= $disabled2 ?>/></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr style="vertical-align:top">
                <td colspan="2">
                    <img onclick="AgregarEquipos('G2')" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                         title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>Equipos utilizados:</strong>
                    <table class="radius">
                        <tbody id="loloG2">
                        <?php
                        $ROW2 = $Gestor->DetalleEquipos('G');
                        for ($i = 0; $i < count($ROW2); $i++) {
                            ?>
                            <tr>
                                <td><?= $i + 1 ?></td>
                                <td>(<?= $ROW2[$i]['codigo'] ?>) <input type="text" id="G2equipos<?= $i ?>"
                                                                        class="lista2" readonly
                                                                        onclick="EquiposLista('G2', <?= $i ?>)"
                                                                        value="<?= $ROW2[$i]['nombre'] ?>"
                                                                        <?= $disabled2 ?>/><input type="hidden"
                                                                                                  id="G2cod_equipos<?= $i ?>"
                                                                                                  name="G2cod_equipos[]"
                                                                                                  value="<?= $ROW2[$i]['id'] ?>"/>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td colspan="2"><strong>Programa del termociclador:</strong>&nbsp;
                    <input type="text" id="Gprograma" name="Gprograma" size="10" maxlength="20"
                           value="<?= $ROW[0]['Gprograma'] ?>" <?= $disabled2 ?>/></td>
            </tr>
        </table>
        <br/>
        <table class="radius" width="98%">
            <tr>
                <td class="titulo" colspan="2">Electroforesis Digesti&oacute;n</td>
            </tr>
            <tr style="vertical-align:top">
                <td>
                    <img onclick="AgregarInsumos('H1')" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                         title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>Insumos utilizados:</strong>
                    <table class="radius">
                        <tr align="center">
                            <td><strong>Nombre</strong></td>
                            <td><strong>Cantidad</strong></td>
                        </tr>
                        <tbody id="loloH1">
                        <?php
                        $ROW2 = $Gestor->DetalleInsumos('H');
                        for ($i = 0; $i < count($ROW2); $i++) {
                            ?>
                            <tr>
                                <td><?= $i + 1 ?>&nbsp;&nbsp;(<?= $ROW2[$i]['codigo'] ?>)&nbsp;<input type="text"
                                                                                                      id="H1insumos<?= $i ?>"
                                                                                                      class="lista2"
                                                                                                      readonly
                                                                                                      onclick="InsumosLista('H1', <?= $i ?>)"
                                                                                                      value="<?= $ROW2[$i]['nombre'] ?>"
                                                                                                      <?= $disabled2 ?>/><input
                                            type="hidden" id="H1cod_insumos<?= $i ?>" name="H1cod_insumos[]"
                                            value="<?= $ROW2[$i]['id'] ?>"/></td>
                                <td><input type="text" id="H1total<?= $i ?>" name="H1total[]" class="monto2"
                                           value="<?= $ROW2[$i]['cantidad'] ?>" onblur="_FLOAT(this)"
                                           <?= $disabled2 ?>/></td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </td>
                <td>
                    <img onclick="AgregarEquipos('H2')" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                         title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>Equipos utilizados:</strong>
                    <table class="radius">
                        <tbody id="loloH2">
                        <?php
                        $ROW2 = $Gestor->DetalleEquipos('H');
                        for ($i = 0; $i < count($ROW2); $i++) {
                            ?>
                            <tr>
                                <td><?= $i + 1 ?></td>
                                <td>(<?= $ROW2[$i]['codigo'] ?>) <input type="text" id="H2equipos<?= $i ?>"
                                                                        class="lista2" readonly
                                                                        onclick="EquiposLista('H2', <?= $i ?>)"
                                                                        value="<?= $ROW2[$i]['nombre'] ?>"
                                                                        <?= $disabled2 ?>/><input type="hidden"
                                                                                                  id="H2cod_equipos<?= $i ?>"
                                                                                                  name="H2cod_equipos[]"
                                                                                                  value="<?= $ROW2[$i]['id'] ?>"/>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td colspan="2"><strong>Tiempo (min):</strong>&nbsp;
                    <input type="text" id="Htiempo" name="Htiempo" class="monto" onblur="_FLOAT(this)"
                           value="<?= $ROW[0]['Htiempo'] ?>" <?= $disabled2 ?>></td>
            </tr>
            <tr>
                <td colspan="2"><strong>Voltaje (V):</strong>&nbsp;
                    <input type="text" id="Hvoltaje" name="Hvoltaje" class="monto" onblur="_FLOAT(this)"
                           value="<?= $ROW[0]['Hvoltaje'] ?>" <?= $disabled2 ?>></td>
            </tr>
        </table>
        <br/>
        <input type="button" class="boton" value="Aceptar" onclick="datos()" <?= $display ?>/>
    </form>
</center>
<br/><?= $Gestor->Encabezado('A0009', 'p', '') ?>
</body>
</html>