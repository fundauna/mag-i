<?php
define('__MODULO__', 'biologia');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);
$Gestor = new _caja_detalle();
$ROW = $Gestor->obtieneDatos();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('', 'hr', 'Inventario :: Detalle C&aacute;mara') ?>
<center>
    <form name="form" id="form">
        <input type="hidden" id="ID" name="" value="<?= $_GET['ID'] ?>"/>
        <input type="hidden" id="L" name="" value="<?= $_GET['L'] ?>"/>
        <table class="radius" width="98%">
            <tr>
                <td class="titulo" colspan="3">Detalles de la caja</td>
            </tr>

            <tr>
                <td>Caja</td>
                <td>
                    <select id="caja" onchange="detalle(this.value)" name="">
                        <option value="">...</option>
                        <?php foreach ($ROW as $dato): ?>
                            <option value="<?= $dato['id'] ?>"><?= $dato['nombre'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>

            <tr>
                <td>Nombre</td>
                <td><input type="text" name="" id="nombre" value=""/></td>
                <td></td>
            </tr>

            <tr>
                <td>Ubicaci&oacute;n</td>
                <td><input type="text" name="" id="ubicacion" value=""/></td>
            </tr>

        </table>
        <br/>
        <input type="button" id="btn" value="Guardar" class="boton" onClick="datos()"/>
        <input type="button" id="btn" value="Agregar Caja" class="boton" onClick="agregar_caja()"/>
        <br/><br/>
        <table class="radius" width="98%">
            <thead>
            <tr>
                <td class="titulo" colspan="2">Plagas</td>
            </tr>
            <tr>
                <td><strong>Plaga</strong></td>
                <td><strong>Opciones</strong></td>
            </tr>
            </thead>
            <tbody id="lolo">
            </tbody>
        </table>
        <br/>
        <input type="button" id="btn" value="Agregar Plaga" class="boton" onClick="agrega_plaga()"/>
    </form>
</center>
<?php if (isset($_GET['C'])): ?>
    <script type="text/javascript">
        $(function () {
            detalle(<?= $_GET['C'] ?>);
            $("#caja").val(<?= $_GET['C'] ?>);
        });
    </script>
<?php endif; ?>
</body>
</html>