<?php
define('__MODULO__', 'biologia');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _trabajo_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

/*if($ROW[0]['estado']=='a' or $ROW[0]['estado']=='f'){
	$disabled = 'disabled';
	$display = 'style="display:none"';
}else{*/
$disabled = '';
$display = '';
//}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<center>
    <input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
    <input type="hidden" id="cs" value="<?= $ROW[0]['cs'] ?>"/>
    <input type="hidden" id="tipo" padre=""/><!-- PARA VER CUAL LISTADOR EST� ACTIVO -->
    <?= $Gestor->Encabezado('A0013', 'e', 'Hoja de trabajo Biolog&iacute;a Molecular') ?>
    <br>
    <table class="radius" width="90%">
        <tr>
            <td colspan="3">
                <table class="radius" width="100%">
                    <tr>
                        <td class="titulo" width="90%">Informaci&oacute;n General</td>
                        <td class="titulo">
                            <img onclick="ProcsConsulta()" src="<?php $Gestor->Incluir('previa', 'bkg') ?>"
                                 title="Consultar procedimiento" class="tab"/>&nbsp;
                            <img onclick="ProcsCarga('A')" src="<?php $Gestor->Incluir('bajar2', 'bkg') ?>"
                                 title="Cargar secci�n del procedimiento" class="tab" <?= $display ?>/>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3"><font size="-2"><strong>Analista:</strong>&nbsp;<?= $ROW[0]['fechaA'] ?></font></td>
        </tr>
        <tr>
            <td colspan="3">Procedimiento:&nbsp;
                <input type="text" id="procedimiento" class="lista" readonly onclick="ProcsLista()"
                       value="<?= $ROW[0]['procedimiento'] ?>" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td colspan="3">Observaciones:&nbsp;<input type="text" id="obsA" size="50" maxlength="100"
                                                       value="<?= $ROW[0]['obsA'] ?>" <?= $disabled ?>></td>
        </tr>
        <tr>
            <td colspan="3">
                <hr/>
            </td>
        </tr>
        <tr align="center" style="vertical-align:top">
            <td>
                <img onclick="AgregarAnalisis('5', 'A1')" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                     title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>An&aacute;lisis:</strong>
                <table class="radius">
                    <tbody id="loloA1">
                    <?php
                    $ROW2 = $Gestor->DetalleAnalisis('1');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td><input type="text" id="A1analisis<?= $i ?>" class="lista2" readonly
                                       onclick="MacroAnalisisLista('5', 'A1', '<?= $i ?>')"
                                       value="<?= $ROW2[$i]['nombre'] ?>" <?= $disabled ?>/><input type="hidden"
                                                                                                   id="A1cod_analisis<?= $i ?>"
                                                                                                   name="A1cod_analisis"
                                                                                                   value="<?= $ROW2[$i]['id'] ?>"/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
            <td>
                <img onclick="AgregarAnalisis('7', 'A2')" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                     title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>Tipos de material:</strong>
                <table class="radius">
                    <tbody id="loloA2">
                    <?php
                    $ROW2 = $Gestor->DetalleAnalisis('2');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td><input type="text" id="A2analisis<?= $i ?>" class="lista2" readonly
                                       onclick="MacroAnalisisLista('7', 'A2', '<?= $i ?>')"
                                       value="<?= $ROW2[$i]['nombre'] ?>" <?= $disabled ?>/><input type="hidden"
                                                                                                   id="A2cod_analisis<?= $i ?>"
                                                                                                   name="A2cod_analisis"
                                                                                                   value="<?= $ROW2[$i]['id'] ?>"/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
            <td>
                <img onclick="AgregarAnalisis('6', 'A3')" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                     title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>Plagas a detectar:</strong>
                <table class="radius">
                    <tbody id="loloA3">
                    <?php
                    $ROW2 = $Gestor->DetalleAnalisis('3');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td><input type="text" id="A3analisis<?= $i ?>" class="lista2" readonly
                                       onclick="MacroAnalisisLista('6', 'A3', '<?= $i ?>')"
                                       value="<?= $ROW2[$i]['nombre'] ?>" <?= $disabled ?>/><input type="hidden"
                                                                                                   id="A3cod_analisis<?= $i ?>"
                                                                                                   name="A3cod_analisis"
                                                                                                   value="<?= $ROW2[$i]['id'] ?>"/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td class="titulo" colspan="3">Muestras&nbsp;<img onclick="AgregarMuestras()"
                                                              src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                                                              title="Agregar l�nea" class="tab" <?= $display ?>/></td>
        </tr>
        <tr>
            <td colspan="3">
                <table class="radius">
                    <tr>
                        <td><strong>#</strong></td>
                        <td><strong>C&oacute;digo</strong></td>
                    </tr>
                    <tbody id="lolo">
                    <?php
                    $ROW2 = $Gestor->DetalleMuestras(0);
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td><input type="text" id="codigo0" name="codigo" size="10" maxlength="10"
                                       value="<?= $ROW2[$i]['codigo'] ?>" <?= $disabled ?>/></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
        </tr>
        <?php if ($ROW[0]['analistaA'] == '' or $ROW[0]['analistaA'] == $_POST['usuario']) { ?>
            <tr align="center">
                <td colspan="3"><input type="button" class="boton" value="Guardar" onclick="Guarda1()" <?= $display ?>/>
                </td>
            </tr>
        <?php } ?>
    </table>
    <?php if ($_GET['acc'] == 'I') exit; ?>
    <br/>
    <table class="radius" width="90%">
        <tr>
            <td colspan="4">
                <table class="radius" width="100%">
                    <tr>
                        <td class="titulo" width="90%">Preprocesamiento</td>
                        <td class="titulo">
                            <img onclick="Oculta('B')" src="<?php $Gestor->Incluir('downboxed', 'bkg') ?>"
                                 title="Mostrar/Ocultar secci�n" class="tab"/>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tbody id="tablaB" <?php if ($ROW[0]['humedadB'] == '0') echo 'style="display:none"'; ?>>
        <tr>
            <td colspan="4"><font size="-2"><strong>Analista:</strong>&nbsp;<?= $ROW[0]['fechaB'] ?></font></td>
        </tr>
        <tr>
            <td>Fecha:</td>
            <td><input type="text" id="fechaProc" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaProc'] == '' ? date('d-m-Y') : $ROW[0]['fechaProc'] ?>"
                       <?= $disabled ?>></td>
            <td>Condiciones ambientales:</td>
            <td>
                <!-- C.A.-->
                <table class="radius">
                    <tr>
                        <td>Temperatura (&ordm;C):</td>
                        <td><input type="text" id="tempB" class="monto2" onblur="_FLOAT(this)"
                                   value="<?= $ROW[0]['tempB'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td>Humedad (%):</td>
                        <td><input type="text" id="humedadB" class="monto2" onblur="_FLOAT(this)"
                                   value="<?= $ROW[0]['humedadB'] ?>" <?= $disabled ?>></td>
                    </tr>
                </table>
                <!-- C.A.-->
            </td>
        </tr>
        <tr>
            <td>Observaciones:</td>
            <td colspan="3"><input type="text" id="obsB" size="50" maxlength="50" value="<?= $ROW[0]['obsB'] ?>"
                                   <?= $disabled ?>></td>
        </tr>
        <?php if ($ROW[0]['analistaB'] == '' or $ROW[0]['analistaB'] == $_POST['usuario']) { ?>
            <tr align="center">
                <td colspan="4"><input type="button" class="boton" value="Guardar" onclick="Guarda2()" <?= $display ?>/>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <br/>
    <table class="radius" width="90%">
        <tr>
            <td colspan="5">
                <table class="radius" width="100%">
                    <tr>
                        <td class="titulo" width="90%">Germinaci&oacute;n</td>
                        <td class="titulo">
                            <img onclick="Oculta('W')" src="<?php $Gestor->Incluir('downboxed', 'bkg') ?>"
                                 title="Mostrar/Ocultar secci�n" class="tab"/>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tbody id="tablaW" <?php if ($ROW[0]['tempW'] == '0') echo 'style="display:none"'; ?>>
        <tr>
            <td colspan="5"><font size="-2"><strong>Analista:</strong>&nbsp;<?= $ROW[0]['fechaW'] ?></font></td>
        </tr>
        <tr>
            <td>Tiempo (min):</td>
            <td><input type="text" id="tiempoW" class="monto2" onblur="_FLOAT(this)" value="<?= $ROW[0]['tiempoW'] ?>"
                       <?= $disabled ?>></td>
            <td>Temperatura (&ordm;C):</td>
            <td><input type="text" id="tempW" class="monto2" onblur="_FLOAT(this)" value="<?= $ROW[0]['tempW'] ?>"
                       <?= $disabled ?>></td>
            <td>
                <img onclick="AgregarEquipos('W2')" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                     class="tab" <?= $display ?>/>&nbsp;<strong>Equipos utilizados:</strong>
                <table class="radius">
                    <tbody id="loloW2">
                    <?php
                    $ROW2 = $Gestor->DetalleEquipos('W');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td>(<?= $ROW2[$i]['codigo'] ?>) <input type="text" id="W2equipos<?= $i ?>" class="lista2"
                                                                    readonly onclick="EquiposLista('W2', <?= $i ?>)"
                                                                    value="<?= $ROW2[$i]['nombre'] ?>"
                                                                    <?= $disabled ?>/><input type="hidden"
                                                                                             id="W2cod_equipos<?= $i ?>"
                                                                                             name="W2cod_equipos"
                                                                                             value="<?= $ROW2[$i]['id'] ?>"/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="5">Observaciones:&nbsp;
                <input type="text" id="obsW" size="50" maxlength="50" value="<?= $ROW[0]['obsW'] ?>" <?= $disabled ?>>
            </td>
        </tr>
        <?php if ($ROW[0]['analistaW'] == '' or $ROW[0]['analistaW'] == $_POST['usuario']) { ?>
            <tr align="center">
                <td colspan="5"><input type="button" class="boton" value="Guardar" onclick="GuardaW()" <?= $display ?>/>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <br/>
    <table class="radius" width="90%">
        <tr>
            <td colspan="3">
                <table class="radius" width="100%">
                    <tr>
                        <td class="titulo" width="90%">Extracci&oacute;n</td>
                        <td class="titulo">
                            <img onclick="Oculta('C')" src="<?php $Gestor->Incluir('downboxed', 'bkg') ?>"
                                 title="Mostrar/Ocultar secci�n" class="tab"/>&nbsp;
                            <img onclick="ProcsConsulta()" src="<?php $Gestor->Incluir('previa', 'bkg') ?>"
                                 title="Consultar procedimiento" class="tab"/>&nbsp;
                            <img onclick="ProcsCarga('C')" src="<?php $Gestor->Incluir('bajar2', 'bkg') ?>"
                                 title="Cargar secci�n del procedimiento" class="tab" <?= $display ?>/>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tbody id="tablaC" <?php if ($ROW[0]['humedadC'] == '0') echo 'style="display:none"'; ?>>
        <tr>
            <td colspan="3"><font size="-2"><strong>Analista:</strong>&nbsp;<?= $ROW[0]['fechaC'] ?></font></td>
        </tr>
        <tr>
            <td>Condiciones ambientales:</td>
            <td colspan="2">
                <!-- C.A.-->
                <table class="radius">
                    <tr>
                        <td>Temperatura (&ordm;C):</td>
                        <td><input type="text" id="tempC" class="monto2" onblur="_FLOAT(this)"
                                   value="<?= $ROW[0]['tempC'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td>Humedad (%):</td>
                        <td><input type="text" id="humedadC" class="monto2" onblur="_FLOAT(this)"
                                   value="<?= $ROW[0]['humedadC'] ?>" <?= $disabled ?>></td>
                    </tr>
                </table>
                <!-- C.A.-->
            </td>
        </tr>
        <tr>
            <td>Observaciones:</td>
            <td colspan="2"><input type="text" id="obsC" size="50" maxlength="50" value="<?= $ROW[0]['obsC'] ?>"
                                   <?= $disabled ?>></td>
        </tr>
        <tr>
            <td colspan="3">
                <hr/>
            </td>
        </tr>
        <tr align="center" style="vertical-align:top">
            <td>
                <img onclick="AgregarControles1()" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                     class="tab" <?= $display ?>/>&nbsp;<strong>Controles:</strong>
                <table class="radius">
                    <tr align="center">
                        <td><strong>#</strong></td>
                        <td><strong>C&oacute;digo &uacute;nico </strong></td>
                    </tr>
                    <tbody id="loloC3">
                    <?php
                    $ROW3 = $Gestor->DetalleControles(1);
                    for ($i = 0; $i < count($ROW3); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td><input type="text" id="C3insumos<?= $i ?>" name="C3insumos" class="lista" readonly
                                       onclick="AlicuotasLista('C3', <?= $i ?>)" value="<?= $ROW3[$i]['control'] ?>"
                                       <?= $disabled ?>/>
                                <input type="hidden" id="C3cod_insumos<?= $i ?>" name="C3cod_insumos"
                                       value="<?= $ROW3[$i]['control'] ?>"/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
            <td>
                <img onclick="AgregarInsumos('C1')" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                     class="tab" <?= $display ?>/>&nbsp;<strong>Insumos utilizados:</strong>
                <table class="radius">
                    <tr align="center">
                        <td><strong>C&oacute;digo</strong></td>
                        <td><strong>Nombre</strong></td>
                        <td><strong>Cantidad</strong></td>
                    </tr>
                    <tbody id="loloC1">
                    <?php
                    $ROW2 = $Gestor->DetalleInsumos('C');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?>&nbsp;&nbsp;<input type="text" id="C1insumos<?= $i ?>" class="lista"
                                                                readonly onclick="InsumosLista('C1', <?= $i ?>, this)"
                                                                padre="<?= $ROW2[$i]['padre'] ?>"
                                                                value="<?= $ROW2[$i]['codigo'] ?>"
                                                                <?= $disabled ?>/><input type="hidden"
                                                                                         id="C1cod_insumos<?= $i ?>"
                                                                                         name="C1cod_insumos"
                                                                                         value="<?= $ROW2[$i]['id'] ?>"/>
                            </td>
                            <td><font size="-2"><?= $ROW2[$i]['nombre'] ?></font></td>
                            <td><input type="text" id="C1total<?= $i ?>" name="C1total" class="monto2"
                                       value="<?= $ROW2[$i]['cantidad'] ?>" onblur="_FLOAT(this)" <?= $disabled ?>/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
            <td>
                <img onclick="AgregarEquipos('C2')" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                     class="tab" <?= $display ?>/>&nbsp;<strong>Equipos utilizados:</strong>
                <table class="radius">
                    <tbody id="loloC2">
                    <?php
                    $ROW2 = $Gestor->DetalleEquipos('C');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td>(<?= $ROW2[$i]['codigo'] ?>) <input type="text" id="C2equipos<?= $i ?>" class="lista2"
                                                                    readonly onclick="EquiposLista('C2', <?= $i ?>)"
                                                                    value="<?= $ROW2[$i]['nombre'] ?>"
                                                                    <?= $disabled ?>/><input type="hidden"
                                                                                             id="C2cod_equipos<?= $i ?>"
                                                                                             name="C2cod_equipos"
                                                                                             value="<?= $ROW2[$i]['id'] ?>"/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
        </tr>
        <?php if ($ROW[0]['analistaC'] == '' or $ROW[0]['analistaC'] == $_POST['usuario']) { ?>
            <tr align="center">
                <td colspan="3"><input type="button" class="boton" value="Guardar" onclick="Guarda3()" <?= $display ?>/>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <br/>
    <table class="radius" width="90%">
        <tr>
            <td colspan="6">
                <table class="radius" width="100%">
                    <tr>
                        <td class="titulo" width="90%">Cuantificaci&oacute;n</td>
                        <td class="titulo">
                            <img onclick="Oculta('D')" src="<?php $Gestor->Incluir('downboxed', 'bkg') ?>"
                                 title="Mostrar/Ocultar secci�n" class="tab"/>&nbsp;
                            <img onclick="ProcsConsulta()" src="<?php $Gestor->Incluir('previa', 'bkg') ?>"
                                 title="Consultar procedimiento" class="tab"/>&nbsp;
                            <img onclick="ProcsCarga('D')" src="<?php $Gestor->Incluir('bajar2', 'bkg') ?>"
                                 title="Cargar secci�n del procedimiento" class="tab" <?= $display ?>/>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tbody id="tablaD" <?php if ($ROW[0]['humedadD'] == '0') echo 'style="display:none"'; ?>>
        <tr>
            <td colspan="6"><font size="-2"><strong>Analista:</strong>&nbsp;<?= $ROW[0]['fechaD'] ?></font></td>
        </tr>
        <tr>
            <td>Condiciones ambientales:</td>
            <td colspan="5">
                <!-- C.A.-->
                <table class="radius">
                    <tr>
                        <td>Temperatura (&ordm;C):</td>
                        <td><input type="text" id="tempD" class="monto2" onblur="_FLOAT(this)"
                                   value="<?= $ROW[0]['tempD'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td>Humedad (%):</td>
                        <td><input type="text" id="humedadD" class="monto2" onblur="_FLOAT(this)"
                                   value="<?= $ROW[0]['humedadD'] ?>" <?= $disabled ?>></td>
                    </tr>
                </table>
                <!-- C.A.-->
            </td>
        </tr>
        <tr id="loloDA">
            <td>Analito:</td>
            <td><select id="analito" name="analito" <?= $disabled ?>>
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['analito'] == '0') echo 'selected'; ?>>ADN gen&oacute;mico
                    </option>
                    <option value="1" <?php if ($ROW[0]['analito'] == '1') echo 'selected'; ?>>ARN total</option>
                    <option value="2" <?php if ($ROW[0]['analito'] == '2') echo 'selected'; ?>>ADN doble banda</option>
                    <option value="3" <?php if ($ROW[0]['analito'] == '3') echo 'selected'; ?>>ADN simple banda</option>
                    <option value="4" <?php if ($ROW[0]['analito'] == '4') echo 'selected'; ?>>ARN</option>
                </select></td>
            <td>Concentraci&oacute;n normalizada:</td>
            <td><input type="text" id="normalizada" class="monto" onblur="_FLOAT(this);CalculaReacciones();"
                       value="<?= $ROW[0]['normalizada'] ?>" <?= $disabled ?>></td>

            <td>Unidad:</td>
            <td><select id="unidad" name="unidad" <?= $disabled ?> onchange="CambiaUnidad()">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['unidad'] == '0') echo 'selected'; ?>>&mu;g/&mu;L</option>
                    <option value="1" <?php if ($ROW[0]['unidad'] == '1') echo 'selected'; ?>>ng/&mu;L</option>
                </select></td>
        </tr>
        <tr>
            <td colspan="6">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Volumen final deseado (&mu;l)</td>
            <td colspan="5"><input type="text" id="todos" class="monto2" onblur="_FLOAT(this);" value="1"/>&nbsp;
                <input type="button" class="boton" value="Aplicar" onclick="Aplica()" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td>Archivo de cuantificaci&oacute;n:
                <?php
                $ruta = "../../caspha-i/docs/biologia/cuantificacion/" . str_replace(' ', '', $ROW[0]['cs']) . '.zip';
                if (file_exists($ruta)) {
                    ?>
                    <img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar"
                         onclick="DocumentosZip('<?= $ROW[0]['cs'] ?>', '<?= __MODULO__ ?>/cuantificacion')"
                         class="tab2"/>
                <?php } ?>
            </td>
            <td colspan="5">
                <form name="formulario" method="post" enctype="multipart/form-data"
                      action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
                    <input type="hidden" name="_IMPORT" value="0"/>
                    <input type="hidden" name="paso" value="0"/>
                    <input type="hidden" name="cs" value="<?= $ROW[0]['cs'] ?>"/>
                    <input type="file" id="cuantificacion" name="cuantificacion" <?= $display ?>/>&nbsp;<input
                            type="button" class="boton" value="Adjuntar" onclick="AdjuntaPasoD()" <?= $display ?>/>
                </form>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <strong>C&aacute;lculo de diluci&oacute;n:</strong>
                <table class="radius" width="100%">
                    <tr align="center">
                        <td><strong>Muestra</strong></td>
                        <td><strong>Conc. Acido<br/>Nuc.<span id="lb_unidad"></span></strong></td>
                        <td><strong>Coeficiente<br/>260/280</strong></td>
                        <td><strong>Coeficiente<br/>260/230</strong></td>
                        <td><strong>Volumen final<br/>
                                deseado (&mu;L)</strong></td>
                        <td><strong>Volumen de la<br/>
                                muestra a tomar (&mu;L) </strong></td>
                        <td><strong>Cantidad de<br/>
                                diluyente (&mu;L)</strong></td>
                    </tr>
                    <?php
                    $ROW2 = $Gestor->DetallePlantilla('D');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr align="center">
                            <td><?= $ROW2[$i]['muestra'] ?><input type="hidden" name="Dmuestra"
                                                                  value="<?= $ROW2[$i]['muestra'] ?>" <?= $disabled ?>/>
                            </td>
                            <td><input type="text" id="CAN<?= $i ?>" name="CAN" class="monto2"
                                       value="<?= $ROW2[$i]['CAN'] ?>" onblur="_FLOAT(this);CalculaReacciones();"
                                       <?= $disabled ?>/></td>
                            <td><input type="text" id="coef1<?= $i ?>" name="coef1" class="monto2"
                                       value="<?= $ROW2[$i]['coef1'] ?>" onblur="_FLOAT(this)" <?= $disabled ?>/></td>
                            <td><input type="text" id="coef2<?= $i ?>" name="coef2" class="monto2"
                                       value="<?= $ROW2[$i]['coef2'] ?>" onblur="_FLOAT(this)" <?= $disabled ?>/></td>
                            <td><input type="text" id="vol1<?= $i ?>" name="vol1" class="monto2"
                                       value="<?= $ROW2[$i]['vol1'] ?>" onblur="_FLOAT(this);CalculaReacciones();"
                                       <?= $disabled ?>></td>
                            <td><input type="text" id="vol2<?= $i ?>" class="monto2" readonly/></td>
                            <td><input type="text" id="vol3<?= $i ?>" class="monto2" readonly/></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </td>
        </tr>
        <tr>
            <td>Observaciones:</td>
            <td colspan="5"><input type="text" id="obsD" size="50" maxlength="50" value="<?= $ROW[0]['obsD'] ?>"
                                   <?= $disabled ?>></td>
        </tr>
        <tr>
            <td colspan="6">
                <hr/>
            </td>
        </tr>
        <tr align="center" style="vertical-align:top">
            <td colspan="3">
                <img onclick="AgregarInsumos('D1')" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                     class="tab" <?= $display ?>/>&nbsp;<strong>Insumos utilizados:</strong>
                <table class="radius">
                    <tr align="center">
                        <td><strong>C&oacute;digo</strong></td>
                        <td><strong>Nombre</strong></td>
                        <td><strong>Cantidad</strong></td>
                    </tr>
                    <tbody id="loloD1">
                    <?php
                    $ROW2 = $Gestor->DetalleInsumos('D');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?>&nbsp;&nbsp;<input type="text" id="D1insumos<?= $i ?>" class="lista"
                                                                readonly onclick="InsumosLista('D1', <?= $i ?>, this)"
                                                                padre="<?= $ROW2[$i]['padre'] ?>"
                                                                value="<?= $ROW2[$i]['codigo'] ?>"
                                                                <?= $disabled ?>/><input type="hidden"
                                                                                         id="D1cod_insumos<?= $i ?>"
                                                                                         name="D1cod_insumos"
                                                                                         value="<?= $ROW2[$i]['id'] ?>"/>
                            </td>
                            <td><font size="-2"><?= $ROW2[$i]['nombre'] ?></font></td>
                            <td><input type="text" id="D1total<?= $i ?>" name="D1total" class="monto2"
                                       value="<?= $ROW2[$i]['cantidad'] ?>" onblur="_FLOAT(this)" <?= $disabled ?>/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
            <td colspan="3">
                <img onclick="AgregarEquipos('D2')" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                     class="tab" <?= $display ?>/>&nbsp;<strong>Equipos utilizados:</strong>
                <table class="radius">
                    <tbody id="loloD2">
                    <?php
                    $ROW2 = $Gestor->DetalleEquipos('D');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td>(<?= $ROW2[$i]['codigo'] ?>) <input type="text" id="D2equipos<?= $i ?>" class="lista2"
                                                                    readonly onclick="EquiposLista('D2', <?= $i ?>)"
                                                                    value="<?= $ROW2[$i]['nombre'] ?>"
                                                                    <?= $disabled ?>/><input type="hidden"
                                                                                             id="D2cod_equipos<?= $i ?>"
                                                                                             name="D2cod_equipos"
                                                                                             value="<?= $ROW2[$i]['id'] ?>"/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
        </tr>
        <?php if ($ROW[0]['analistaD'] == '' or $ROW[0]['analistaD'] == $_POST['usuario']) { ?>
            <tr align="center">
                <td colspan="6"><input type="button" class="boton" value="Guardar" onclick="Guarda4()" <?= $display ?>/>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <br/>
    <table class="radius" width="90%">
        <tr>
            <td colspan="6">
                <table class="radius" width="100%">
                    <tr>
                        <td class="titulo" width="90%">PCR</td>
                        <td class="titulo">
                            <img onclick="Oculta('E')" src="<?php $Gestor->Incluir('downboxed', 'bkg') ?>"
                                 title="Mostrar/Ocultar secci�n" class="tab"/>&nbsp;
                            <img onclick="ProcsConsulta()" src="<?php $Gestor->Incluir('previa', 'bkg') ?>"
                                 title="Consultar procedimiento" class="tab"/>&nbsp;
                            <img onclick="ProcsCarga('E')" src="<?php $Gestor->Incluir('bajar2', 'bkg') ?>"
                                 title="Cargar secci�n del procedimiento" class="tab" <?= $display ?>/>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tbody id="tablaE" <?php if ($ROW[0]['humedadE'] == '0') echo 'style="display:none"'; ?>>
        <tr>
            <td colspan="6"><font size="-2"><strong>Analista:</strong>&nbsp;<?= $ROW[0]['fechaE'] ?></font></td>
        </tr>
        <tr>
            <td>Fecha:</td>
            <td><input type="text" id="fechaPcr" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= ($ROW[0]['fechaPcr'] == '') ? date('d/m/Y') : $ROW[0]['fechaPcr'] ?>"
                       <?= $disabled ?>></td>
            <td>Condiciones ambientales:</td>
            <td colspan="3">
                <!-- C.A.-->
                <table class="radius">
                    <tr>
                        <td>Temperatura (&ordm;C):</td>
                        <td><input type="text" id="tempE" class="monto2" onblur="_FLOAT(this)"
                                   value="<?= $ROW[0]['tempE'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td>Humedad (%):</td>
                        <td><input type="text" id="humedadE" class="monto2" onblur="_FLOAT(this)"
                                   value="<?= $ROW[0]['humedadE'] ?>" <?= $disabled ?>></td>
                    </tr>
                </table>
                <!-- C.A.-->
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <hr/>
            </td>
        </tr>
        <tr align="center" style="vertical-align:top">
            <td colspan="2">
                <img onclick="AgregarAdicionales()" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                     class="tab" <?= $display ?>/>&nbsp;<strong>Muestras adicionales:</strong>
                <table class="radius">
                    <tr align="center">
                        <td><strong>#</strong></td>
                        <td><strong>C&oacute;digo</strong></td>
                    </tr>
                    <tbody id="loloE0">
                    <?php
                    $ROW2 = $Gestor->DetalleMuestras(1);
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td><input type="text" name="E0muestras" size="10" maxlength="10"
                                       value="<?= str_replace(' ', '', $ROW2[$i]['codigo']) ?>" <?= $disabled ?>/></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
            <td colspan="2">
                <img onclick="AgregarControles()" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                     class="tab" <?= $display ?>/>&nbsp;<strong>Controles:</strong>
                <table class="radius">
                    <tr align="center">
                        <td><strong>#</strong></td>
                        <td><strong>C&oacute;digo &uacute;nico </strong></td>
                    </tr>
                    <tbody id="loloE4">
                    <?php
                    $ROW3 = $Gestor->DetalleControles();
                    for ($i = 0; $i < count($ROW3); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td><input type="text" id="E4insumos<?= $i ?>" name="E4insumos" class="lista" readonly
                                       onclick="AlicuotasLista('E4', <?= $i ?>)" value="<?= $ROW3[$i]['control'] ?>"
                                       <?= $disabled ?>/>
                                <input type="hidden" id="E4cod_insumos<?= $i ?>" name="E4cod_insumos"
                                       value="<?= $ROW3[$i]['control'] ?>"/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
            <td colspan="2">
                <img onclick="AgregarEquipos('E2')" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                     class="tab" <?= $display ?>/>&nbsp;<strong>Equipos utilizados:</strong>
                <table class="radius">
                    <tbody id="loloE2">
                    <?php
                    $ROW2 = $Gestor->DetalleEquipos('E');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td>(<?= $ROW2[$i]['codigo'] ?>) <input type="text" id="E2equipos<?= $i ?>" class="lista2"
                                                                    readonly onclick="EquiposLista('E2', <?= $i ?>)"
                                                                    value="<?= $ROW2[$i]['nombre'] ?>"
                                                                    <?= $disabled ?>/><input type="hidden"
                                                                                             id="E2cod_equipos<?= $i ?>"
                                                                                             name="E2cod_equipos"
                                                                                             value="<?= $ROW2[$i]['id'] ?>"/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Cantidad de reacciones:</td>
            <td colspan="5"><input type="text" id="reaccionesE" class="monto2" onblur="_INT(this);CalculaReacciones();"
                                   value="<?= $ROW[0]['reaccionesE'] ?>" <?= $disabled ?>></td>
        </tr>
        <tr>
            <td colspan="6">
                <img onclick="AgregarPlantilla('E1')" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                     title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>Plantilla de mezcla de reacci&oacute;n:</strong>
                <table class="radius">
                    <tr>
                        <td><strong>#</strong></td>
                        <td><strong>Tipo</strong></td>
                        <td><strong>Descripci&oacute;n</strong></td>
                        <td><strong>C&oacute;digo</strong></td>
                        <td><strong>Conc. Stock</strong></td>
                        <td><strong>Conc. Final</strong></td>
                        <td align="center"><strong>Volumen 1x<br/>(&mu;L)</strong></td>
                        <td align="center"><strong>Reacciones<br/>(&mu;L)</strong></td>
                    </tr>
                    <tbody id="loloE1">
                    <?php
                    $ROW2 = $Gestor->DetallePlantilla('E');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td><select name="E1tipo" id="E1tipo<?= $i ?>" <?= $disabled ?>>
                                    <option value="">...</option>
                                    <option value="0" <?php if ($ROW2[$i]['tipo'] == '0') echo 'selected'; ?>>Reactivo
                                    </option>
                                    <option value="1" <?php if ($ROW2[$i]['tipo'] == '1') echo 'selected'; ?>>Alicuota
                                    </option>
                                </select></td>
                            <td><input type="text" name="E1descr" size="10" maxlength="20"
                                       value="<?= $ROW2[$i]['descr'] ?>" <?= $disabled ?>/></td>
                            <td><input type="text" name="E1insumos" id="E1insumos<?= $i ?>" class="lista" readonly
                                       onclick="DependeLista('E1', <?= $i ?>)" value="<?= $ROW2[$i]['codigo'] ?>"
                                       <?= $disabled ?>/><input type="hidden" id="E1cod_insumos<?= $i ?>"
                                                                name="E1cod_insumos"
                                                                value="<?= $ROW2[$i]['codigo'] ?>"/></td>
                            <td><input type="text" name="E1concS" size="10" maxlength="20"
                                       value="<?= $ROW2[$i]['concS'] ?>" <?= $disabled ?>/></td>
                            <td><input type="text" name="E1concF" size="10" maxlength="20"
                                       value="<?= $ROW2[$i]['concF'] ?>" <?= $disabled ?>/></td>
                            <td><input type="text" name="E1volumen" class="monto2" value="<?= $ROW2[$i]['volumen'] ?>"
                                       onblur="_FLOAT(this);CalculaReacciones();" <?= $disabled ?>/></td>
                            <td><input type="text" name="E1reacciones" class="monto2"
                                       value="<?= $ROW2[$i]['reacciones'] ?>" readonly <?= $disabled ?>/></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                    <tr>
                        <td colspan="6" align="right"><strong>Total:</strong></td>
                        <td id="tot1" align="center"></td>
                        <td id="tot2" align="center"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Programa del termociclador:</td>
            <td id="termo"><input type="text" id="programaE" size="10" maxlength="20"
                                  value="<?= $ROW[0]['programaE'] ?>" <?= $disabled ?>/></td>
            <td colspan="4">Nombre del archivo:&nbsp;
                <input type="text" id="nombreE" size="10" maxlength="20" value="<?= $ROW[0]['nombreE'] ?>"
                       <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td>Observaciones:</td>
            <td colspan="6"><input type="text" id="obsE" size="50" maxlength="50" value="<?= $ROW[0]['obsE'] ?>"
                                   <?= $disabled ?>></td>
        </tr>
        <?php if ($ROW[0]['analistaE'] == '' or $ROW[0]['analistaE'] == $_POST['usuario']) { ?>
            <tr align="center">
                <td colspan="6"><input type="button" class="boton" value="Guardar" onclick="Guarda5()" <?= $display ?>/>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <br/>
    <table class="radius" width="90%">
        <tr>
            <td colspan="6">
                <table class="radius" width="100%">
                    <tr>
                        <td class="titulo" width="90%">Electroforesis PCR</td>
                        <td class="titulo">
                            <img onclick="Oculta('X')" src="<?php $Gestor->Incluir('downboxed', 'bkg') ?>"
                                 title="Mostrar/Ocultar secci�n" class="tab"/>&nbsp;
                            <img onclick="ProcsConsulta()" src="<?php $Gestor->Incluir('previa', 'bkg') ?>"
                                 title="Consultar procedimiento" class="tab"/>&nbsp;
                            <img onclick="ProcsCarga('X')" src="<?php $Gestor->Incluir('bajar2', 'bkg') ?>"
                                 title="Cargar secci�n del procedimiento" class="tab"/>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tbody id="tablaX" <?php if ($ROW[0]['tiempoX'] == '0') echo 'style="display:none"'; ?>>
        <tr>
            <td colspan="4"><font size="-2"><strong>Analista:</strong>&nbsp;<?= $ROW[0]['fechaX'] ?></font></td>
        </tr>
        <tr>
            <td>Condiciones ambientales:</td>
            <td colspan="3">
                <!-- C.A.-->
                <table class="radius">
                    <tr>
                        <td>Temperatura (&ordm;C):</td>
                        <td><input type="text" id="tempX" class="monto2" onblur="_FLOAT(this)"
                                   value="<?= $ROW[0]['tempX'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td>Humedad (%):</td>
                        <td><input type="text" id="humedadX" class="monto2" onblur="_FLOAT(this)"
                                   value="<?= $ROW[0]['humedadX'] ?>" <?= $disabled ?>></td>
                    </tr>
                </table>
                <!-- C.A.-->
            </td>
        </tr>
        <tr id="loloX">
            <td>Tiempo (min):</td>
            <td><input type="text" id="tiempoX" class="monto" onblur="_FLOAT(this)" value="<?= $ROW[0]['tiempoX'] ?>"
                       <?= $disabled ?>></td>
            <td>Voltaje (V):</td>
            <td><input type="text" id="voltajeX" class="monto" onblur="_FLOAT(this)" value="<?= $ROW[0]['voltajeX'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td>Observaciones:</td>
            <td colspan="3"><input type="text" id="obsX" size="50" maxlength="50" value="<?= $ROW[0]['obsX'] ?>"
                                   <?= $disabled ?>></td>
        </tr>
        <tr>
            <td colspan="4">
                <hr/>
            </td>
        </tr>
        <tr align="center" style="vertical-align:top">
            <td colspan="2">
                <img onclick="AgregarInsumos('X1')" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                     class="tab"/>&nbsp;<strong>Insumos utilizados:</strong>
                <table class="radius">
                    <tr align="center">
                        <td><strong>C&oacute;digo</strong></td>
                        <td><strong>Nombre</strong></td>
                        <td><strong>Cantidad</strong></td>
                    </tr>
                    <tbody id="loloX1">
                    <?php
                    $ROW2 = $Gestor->DetalleInsumos('X');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?>&nbsp;&nbsp;<input type="text" id="X1insumos<?= $i ?>" class="lista"
                                                                readonly onclick="InsumosLista('X1', <?= $i ?>)"
                                                                value="<?= $ROW2[$i]['codigo'] ?>"
                                                                <?= $disabled ?>/><input type="hidden"
                                                                                         id="X1cod_insumos<?= $i ?>"
                                                                                         name="X1cod_insumos"
                                                                                         value="<?= $ROW2[$i]['id'] ?>"/>
                            </td>
                            <td><font size="-2"><?= $ROW2[$i]['nombre'] ?></font></td>
                            <td><input type="text" id="X1total<?= $i ?>" name="X1total" class="monto2"
                                       value="<?= $ROW2[$i]['cantidad'] ?>" onblur="_FLOAT(this)" <?= $disabled ?>/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
            <td colspan="2">
                <img onclick="AgregarEquipos('X2')" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                     class="tab"/>&nbsp;<strong>Equipos utilizados:</strong>
                <table class="radius">
                    <tbody id="loloX2">
                    <?php
                    $ROW2 = $Gestor->DetalleEquipos('X');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td>(<?= $ROW2[$i]['codigo'] ?>) <input type="text" id="X2equipos<?= $i ?>" class="lista2"
                                                                    readonly onclick="EquiposLista('X2', <?= $i ?>)"
                                                                    value="<?= $ROW2[$i]['nombre'] ?>"
                                                                    <?= $disabled ?>/><input type="hidden"
                                                                                             id="X2cod_equipos<?= $i ?>"
                                                                                             name="X2cod_equipos"
                                                                                             value="<?= $ROW2[$i]['id'] ?>"/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
        </tr>
        <?php if ($ROW[0]['analistaX'] == '' or $ROW[0]['analistaX'] == $_POST['usuario']) { ?>
            <tr align="center">
                <td colspan="6"><input type="button" class="boton" value="Guardar" onclick="GuardaX()" <?= $display ?>/>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <br/>
    <table class="radius" width="90%">
        <tr>
            <td colspan="6">
                <table class="radius" width="100%">
                    <tr>
                        <td class="titulo" width="90%">Digesti&oacute;n con enzimas de restricci&oacute;n</td>
                        <td class="titulo">
                            <img onclick="Oculta('F')" src="<?php $Gestor->Incluir('downboxed', 'bkg') ?>"
                                 title="Mostrar/Ocultar secci�n" class="tab"/>&nbsp;
                            <img onclick="ProcsConsulta()" src="<?php $Gestor->Incluir('previa', 'bkg') ?>"
                                 title="Consultar procedimiento" class="tab"/>&nbsp;
                            <img onclick="ProcsCarga('F')" src="<?php $Gestor->Incluir('bajar2', 'bkg') ?>"
                                 title="Cargar secci�n del procedimiento" class="tab"/>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tbody id="tablaF" <?php if ($ROW[0]['Famplicon1'] == '0') echo 'style="display:none"'; ?>>
        <tr>
            <td colspan="6"><font size="-2"><strong>Analista:</strong>&nbsp;<?= $ROW[0]['fechaF'] ?></font></td>
        </tr>
        <tr>
            <td>Cantidad de reacciones:</td>
            <td colspan="5"><input type="text" id="reaccionesF" class="monto2" onblur="_INT(this);CalculaReacciones();"
                                   value="<?= $ROW[0]['reaccionesF'] ?>" <?= $disabled ?>></td>
        </tr>
        <tr>
            <td colspan="6">
                <img onclick="AgregarPlantilla('F1')" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                     title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>Plantilla de mezcla de <span
                            class="titulo">digesti&oacute;n</span>:</strong>
                <table class="radius">
                    <tr>
                        <td><strong>#</strong></td>
                        <td><strong>Tipo</strong></td>
                        <td><strong>Descripci&oacute;n</strong></td>
                        <td><strong>C&oacute;digo</strong></td>
                        <td><strong>Volumen 1x (&mu;L)</strong></td>
                        <td><strong>Reacciones (&mu;L)</strong></td>
                    </tr>
                    <tbody id="loloF1">
                    <?php
                    $ROW2 = $Gestor->DetallePlantilla('F');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td><select name="F1tipo" id="F1tipo<?= $i ?>" <?= $disabled ?>>
                                    <option value="">...</option>
                                    <option value="0" <?php if ($ROW2[$i]['tipo'] == '0') echo 'selected'; ?>>Reactivo
                                    </option>
                                    <option value="1" <?php if ($ROW2[$i]['tipo'] == '1') echo 'selected'; ?>>Alicuota
                                    </option>
                                </select></td>
                            <td><input type="text" name="F1descr" size="10" maxlength="20"
                                       value="<?= $ROW2[$i]['descr'] ?>" <?= $disabled ?>/></td>
                            <td><input type="text" id="F1insumos<?= $i ?>" name="F1insumos" class="lista2" readonly
                                       onclick="DependeLista('F1', <?= $i ?>)" value="<?= $ROW2[$i]['codigo'] ?>"
                                       <?= $disabled ?>/><input type="hidden" id="F1cod_insumos<?= $i ?>"
                                                                name="F1cod_insumos"
                                                                value="<?= $ROW2[$i]['codigo'] ?>"/></td>
                            <td><input type="text" name="F1volumen" class="monto2" value="<?= $ROW2[$i]['volumen'] ?>"
                                       onblur="_FLOAT(this);CalculaReacciones();" <?= $disabled ?>/></td>
                            <td><input type="text" name="F1reacciones" class="monto2"
                                       value="<?= $ROW2[$i]['reacciones'] ?>" readonly/></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                        <td colspan="2" align="center">Producto PCR:</td>
                        <td id="amplicon"><input type="text" id="Famplicon1" class="monto2"
                                                 value="<?= $ROW[0]['Famplicon1'] ?>" onblur="_FLOAT(this)"
                                                 <?= $disabled ?>/></td>
                        <td><input type="text" id="Famplicon2" class="monto2" value="<?= $ROW[0]['Famplicon2'] ?>"
                                   readonly/></td>
                    </tr>
                    <tr align="center">
                        <td colspan="4" align="right"><strong>Total:</strong></td>
                        <td id="tot3" align="center"></td>
                        <td id="tot4" align="center"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <hr/>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <img onclick="AgregarEquipos('F2')" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                     class="tab" <?= $display ?>/>&nbsp;<strong>Equipos utilizados:</strong>
                <table class="radius">
                    <tbody id="loloF2">
                    <?php
                    $ROW2 = $Gestor->DetalleEquipos('F');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td>(<?= $ROW2[$i]['codigo'] ?>) <input type="text" id="F2equipos<?= $i ?>" class="lista2"
                                                                    readonly onclick="EquiposLista('F2', <?= $i ?>)"
                                                                    value="<?= $ROW2[$i]['nombre'] ?>"
                                                                    <?= $disabled ?>/><input type="hidden"
                                                                                             id="F2cod_equipos<?= $i ?>"
                                                                                             name="F2cod_equipos"
                                                                                             value="<?= $ROW2[$i]['id'] ?>"/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Programa del termociclador:</td>
            <td colspan="5" id="loloF"><input type="text" id="programaF" size="10" maxlength="20"
                                              value="<?= $ROW[0]['programaF'] ?>" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td>Observaciones:</td>
            <td colspan="5"><input type="text" id="obsF" size="50" maxlength="50" value="<?= $ROW[0]['obsF'] ?>"
                                   <?= $disabled ?>></td>
        </tr>
        <?php if ($ROW[0]['analistaF'] == '' or $ROW[0]['analistaF'] == $_POST['usuario']) { ?>
            <tr align="center">
                <td colspan="6"><input type="button" class="boton" value="Guardar" onclick="Guarda6()" <?= $display ?>/>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <br/>
    <table class="radius" width="90%">
        <tr>
            <td colspan="6">
                <table class="radius" width="100%">
                    <tr>
                        <td class="titulo" width="90%">Electroforesis Digesti&oacute;n</td>
                        <td class="titulo">
                            <img onclick="Oculta('G')" src="<?php $Gestor->Incluir('downboxed', 'bkg') ?>"
                                 title="Mostrar/Ocultar secci�n" class="tab"/>&nbsp;
                            <img onclick="ProcsConsulta()" src="<?php $Gestor->Incluir('previa', 'bkg') ?>"
                                 title="Consultar procedimiento" class="tab"/>&nbsp;
                            <img onclick="ProcsCarga('G')" src="<?php $Gestor->Incluir('bajar2', 'bkg') ?>"
                                 title="Cargar secci�n del procedimiento" class="tab"/>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tbody id="tablaG" <?php if ($ROW[0]['tiempoG'] == '0') echo 'style="display:none"'; ?>>
        <tr>
            <td colspan="4"><font size="-2"><strong>Analista:</strong>&nbsp;<?= $ROW[0]['fechaG'] ?></font></td>
        </tr>
        <tr>
            <td>Condiciones ambientales:</td>
            <td colspan="3">
                <!-- C.A.-->
                <table class="radius">
                    <tr>
                        <td>Temperatura (&ordm;C):</td>
                        <td><input type="text" id="tempG" class="monto2" onblur="_FLOAT(this)"
                                   value="<?= $ROW[0]['tempG'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td>Humedad (%):</td>
                        <td><input type="text" id="humedadG" class="monto2" onblur="_FLOAT(this)"
                                   value="<?= $ROW[0]['humedadG'] ?>" <?= $disabled ?>></td>
                    </tr>
                </table>
                <!-- C.A.-->
            </td>
        </tr>
        <tr id="loloG">
            <td>Tiempo (min):</td>
            <td><input type="text" id="tiempoG" class="monto" onblur="_FLOAT(this)" value="<?= $ROW[0]['tiempoG'] ?>"
                       <?= $disabled ?>></td>
            <td>Voltaje (V):</td>
            <td><input type="text" id="voltajeG" class="monto" onblur="_FLOAT(this)" value="<?= $ROW[0]['voltajeG'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td>Observaciones:</td>
            <td colspan="3"><input type="text" id="obsG" size="50" maxlength="50" value="<?= $ROW[0]['obsG'] ?>"
                                   <?= $disabled ?>></td>
        </tr>
        <tr>
            <td colspan="4">
                <hr/>
            </td>
        </tr>
        <tr align="center" style="vertical-align:top">
            <td colspan="2">
                <img onclick="AgregarInsumos('G1')" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                     class="tab"/>&nbsp;<strong>Insumos utilizados:</strong>
                <table class="radius">
                    <tr align="center">
                        <td><strong>C&oacute;digo</strong></td>
                        <td><strong>Nombre</strong></td>
                        <td><strong>Cantidad</strong></td>
                    </tr>
                    <tbody id="loloG1">
                    <?php
                    $ROW2 = $Gestor->DetalleInsumos('G');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?>&nbsp;&nbsp;<input type="text" id="G1insumos<?= $i ?>" class="lista"
                                                                readonly onclick="InsumosLista('G1', <?= $i ?>)"
                                                                value="<?= $ROW2[$i]['codigo'] ?>"
                                                                <?= $disabled ?>/><input type="hidden"
                                                                                         id="G1cod_insumos<?= $i ?>"
                                                                                         name="G1cod_insumos"
                                                                                         value="<?= $ROW2[$i]['id'] ?>"/>
                            </td>
                            <td><font size="-2"><?= $ROW2[$i]['nombre'] ?></font></td>
                            <td><input type="text" id="G1total<?= $i ?>" name="G1total" class="monto2"
                                       value="<?= $ROW2[$i]['cantidad'] ?>" onblur="_FLOAT(this)" <?= $disabled ?>/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
            <td colspan="2">
                <img onclick="AgregarEquipos('G2')" src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea"
                     class="tab"/>&nbsp;<strong>Equipos utilizados:</strong>
                <table class="radius">
                    <tbody id="loloG2">
                    <?php
                    $ROW2 = $Gestor->DetalleEquipos('G');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td>(<?= $ROW2[$i]['codigo'] ?>) <input type="text" id="G2equipos<?= $i ?>" class="lista2"
                                                                    readonly onclick="EquiposLista('G2', <?= $i ?>)"
                                                                    value="<?= $ROW2[$i]['nombre'] ?>"
                                                                    <?= $disabled ?>/><input type="hidden"
                                                                                             id="G2cod_equipos<?= $i ?>"
                                                                                             name="G2cod_equipos"
                                                                                             value="<?= $ROW2[$i]['id'] ?>"/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
        </tr>
        <?php if ($ROW[0]['analistaG'] == '' or $ROW[0]['analistaG'] == $_POST['usuario']) { ?>
            <tr align="center">
                <td colspan="4"><input type="button" class="boton" value="Guardar" onclick="Guarda7()" <?= $display ?>/>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <br/>
    <form name="formulario2" method="post" enctype="multipart/form-data"
          action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
        <table class="radius" width="90%">
            <tr>
                <td colspan="2">
                    <table class="radius" width="100%">
                        <tr>
                            <td class="titulo" width="90%">Resultados</td>
                            <td class="titulo">
                                <img onclick="Oculta('R')" src="<?php $Gestor->Incluir('downboxed', 'bkg') ?>"
                                     title="Mostrar/Ocultar secci�n" class="tab"/>&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tbody id="tablaR" <?php if ($ROW[0]['fechaR'] == '') echo 'style="display:none"'; ?>>
            <tr>
                <td colspan="2"><font size="-2"><strong>Analista:</strong>&nbsp;<?= $ROW[0]['fechaR'] ?>
                        &nbsp;<?php if ($ROW[0]['fechaM'] != '') echo ", Modificado: {$ROW[0]['fechaM']}"; ?></font>
                </td>
            </tr>
            <tr>
                <td colspan="2">Archivo de resultados:
                    <?php
                    $ruta = "../../caspha-i/docs/biologia/resultados/" . str_replace(' ', '', $ROW[0]['cs']) . '.zip';
                    if (file_exists($ruta)) {
                        ?>
                        <img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar"
                             onclick="DocumentosZip('<?= $ROW[0]['cs'] ?>', '<?= __MODULO__ ?>/resultados')"
                             class="tab2"/>
                    <?php } ?>
                    &nbsp;
                    <input type="hidden" name="_AJAX" value="1"/>
                    <input type="hidden" name="paso" value="R"/>
                    <input type="hidden" name="cs" value="<?= $ROW[0]['cs'] ?>"/>
                    <input type="file" id="archivo" name="archivo" <?= $display ?>/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr align="center" style="vertical-align:top">
                <td>
                    <strong>Controles:</strong>
                    <table class="radius">
                        <tr align="center">
                            <td><strong>C&oacute;digo &uacute;nico </strong></td>
                            <td><strong>Resultado</strong></td>
                        </tr>
                        <?php
                        for ($i = 0; $i < count($ROW3); $i++) {
                            ?>
                            <tr>
                                <td><?= $ROW3[$i]['control'] ?><input type="hidden" name="control[]"
                                                                      value="<?= $ROW3[$i]['control'] ?>"/></td>
                                <td><select name="cumple[]" <?= $disabled ?>>
                                        <option value="">...</option>
                                        <option value="1" <?php if ($ROW3[$i]['cumple'] == '1') echo 'selected'; ?>>
                                            Cumple
                                        </option>
                                        <option value="0" <?php if ($ROW3[$i]['cumple'] == '0') echo 'selected'; ?>>No
                                            cumple
                                        </option>
                                    </select></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </td>
                <td>
                    <strong>Muestras:</strong>
                    <table class="radius">
                        <tr align="center">
                            <td><strong>C&oacute;digo</strong></td>
                            <td><strong>Plaga</strong></td>
                            <td><strong>Resultado</strong></td>
                            <td><img src="<?php $Gestor->Incluir('adn', 'bkg') ?>" title="Incluir en ADR/ARN-teca?"
                                     class="tab"/></td>
                        </tr>
                        <?php
                        $ROW2 = $Gestor->DetalleResultados($ROW[0]['fechaR']);
                        for ($i = 0, $actual = ''; $i < count($ROW2); $i++) {
                            ?>
                            <tr>
                                <td><input type="hidden" name="muestra[]"
                                           value="<?= $ROW2[$i]['muestra'] ?>"/><?= $ROW2[$i]['muestra'] ?></td>
                                <td><input type="hidden" name="analisis[]"
                                           value="<?= $ROW2[$i]['id'] ?>"/><?= $ROW2[$i]['analisis'] ?></td>
                                <td><select name="result[]" id="result<?= $i ?>" <?= $disabled ?>>
                                        <option value="">...</option>
                                        <option value="0" <?php if ($ROW2[$i]['resultado'] == '0') echo 'selected'; ?>>
                                            -
                                        </option>
                                        <option value="1" <?php if ($ROW2[$i]['resultado'] == '1') echo 'selected'; ?>>
                                            +
                                        </option>
                                        <option value="2" <?php if ($ROW2[$i]['resultado'] == '2') echo 'selected'; ?>>
                                            NC
                                        </option>
                                    </select></td>
                                <td align="center">
                                    <?php
                                    if ($actual != $ROW2[$i]['muestra']) {
                                        $actual = $ROW2[$i]['muestra'];
                                        ?>
                                        <input type="checkbox" name="incluir[]" value="<?= $ROW2[$i]['muestra'] ?>"/>
                                        <?php
                                    }//if
                                    ?>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>

                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td colspan="2">An&aacute;lisis de resultados:<br/><textarea name="obsR"
                                                                             style="width:80%"><?= $ROW[0]['obsR'] ?></textarea>
                </td>
            </tr>
            <?php if ($ROW[0]['analistaR'] == '' or $ROW[0]['analistaR'] == $_POST['usuario']) { ?>
                <tr align="center">
                    <td colspan="2"><input type="button" class="boton" value="Guardar" onclick="Guarda8()"
                                           <?= $display ?>/></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </form>
    <br/>
    <input type="button" class="boton" value="Imprimir" onclick="Imprimir()"/>
</center>
<br/><?= $Gestor->Encabezado('A0013', 'p', '') ?>
<script>
    CambiaUnidad();
    CalculaReacciones();
</script>
</body>
</html>