<?php
define('__MODULO__', 'biologia');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _bandejas();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('a0004', 'hr', 'Inventario :: Bandejas') ?>
<?= $Gestor->Encabezado('A0004', 'e', 'Bandejas') ?>
<table class="radius" align="center" width="255px">
    <tr>
        <td class="titulo" colspan="2">Bandejas Registradas</td>
    </tr>
    <tr>
        <td colspan="2">
            <select size="10" style="width:250px" onclick="marca(this);">
                <?php
                $ROW = $Gestor->Bandejas();
                for ($x = 0; $x < count($ROW); $x++) {
                    echo "<option id='{$ROW[$x]['id']}' dimx='{$ROW[$x]['dimx']}' dimy='{$ROW[$x]['dimy']}' labelx='{$ROW[$x]['labelx']}' labely='{$ROW[$x]['labely']}'>{$ROW[$x]['id']}</option>";
                }
                unset($ROW);
                ?>
            </select>
        </td>
    </tr>
</table>
<br/>
<table class="radius" align="center" width="255px">
    <tr>
        <td class="titulo" colspan="2">Detalle</td>
    </tr>
    <tr>
        <td><b>Id:&nbsp;</b></td>
        <td><input type="text" id="id" size="10" maxlength="10"></td>
    </tr>
    <tr>
        <td><b>Dimensi&oacute;n X &rarr;:</b></td>
        <td><input type="text" id="dimx" size="2" maxlength="2" onblur="_INT(this)"></td>
    </tr>
    <tr>
        <td><b>Dimensi&oacute;n Y &darr;:</b></td>
        <td><input type="text" id="dimy" size="2" maxlength="2" onblur="_INT(this)"></td>
    </tr>
    <tr>
        <td><b>Rotulo X &rarr;:</b></td>
        <td>
            <select id="labelx">
                <option value="L">Letras</option>
                <option value="N">N&uacute;meros</option>
            </select>
        </td>
    </tr>
    <tr>
        <td><b>Rotulo Y &darr;:</b></td>
        <td>
            <select id="labely">
                <option value="L">Letras</option>
                <option value="N">N&uacute;meros</option>
            </select>
        </td>
    </tr>
</table>
<br/>
<br/><?= $Gestor->Encabezado('A0004', 'p', '') ?>
<center>
    <input id="new" type="button" class="boton" value="Limpiar" onclick="desmarca();" disabled>&nbsp;
    <input id="add" type="button" class="boton" value="Agregar" onclick="modificar('I');">&nbsp;
    <input id="mod" type="button" class="boton" value="Modificar" onclick="modificar('M');" disabled>&nbsp;
    <input id="del" type="button" class="boton" value="Eliminar" onclick="modificar('D');" disabled>
</center>
</body>
</html>