<?php
define('__MODULO__', 'biologia');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _diluciones_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('a0007', 'hr', 'Inventario :: Detalle Registro de diluciones') ?>
<?= $Gestor->Encabezado('A0007', 'e', 'Registro de diluciones') ?>
<center>
    <input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
    <table class="radius" width="98%">
        <tr>
            <td class="titulo" colspan="4">Informaci&oacute;n general</td>
        </tr>
        <tr>
            <td>C&oacute;digo de resuspenci&oacute;n:</td>
            <td><input type="text" id="codigo" class="lista2" value="<?= $ROW[0]['codigo'] ?>" readonly
                       onclick="ResuspencionesLista()"></td>
            <td>Nombre de reactivo:</td>
            <td><input type="text" id="reactivo" value="<?= $ROW[0]['reactivo'] ?>" size="15" maxlength="20"/></td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="98%">
        <tr>
            <td class="titulo" colspan="5">Detalle de diluciones&nbsp;<img onclick="AliAgregarLinea()"
                                                                           src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                                                                           title="Agregar l�nea" class="tab"/></td>
        </tr>
        <tbody id="lolo">
        <?php
        $ROW = $Gestor->AliLineasMuestra();
        for ($i = 0; $i < count($ROW); $i++) {
            if ($i != 0) echo '<tr><td colspan="5"><hr></td></tr>';
            ?>
            <tr>
                <td align="center" rowspan="2"><?= $i + 1 ?></td>
                <td>Analista:</td>
                <td><?= $ROW[$i]['analista'] ?></td>
                <td>Fecha:</td>
                <td><?= $ROW[$i]['fecha'] ?></td>
            </tr>
            <tr>
                <td>Observaciones:</td>
                <td colspan="4"><input type="text" name="observaciones" value="<?= $ROW[$i]['observaciones'] ?>"
                                       maxlength="100" style="width:90%"/></td>
            </tr>
            <tr align="center">
                <td><strong>C&oacute;digo disolvente</strong></td>
                <td><strong>Concentraci&oacute;n inicial</strong></td>
                <td><strong>Volumen disolvente</strong></td>
                <td><strong>Concentraci&oacute;n final</strong></td>
                <td><strong>Volumen a tomar</strong></td>
            </tr>
            <tr align="center">
                <td><input type="text" name="cod_disolvente" value="<?= $ROW[$i]['cod_disolvente'] ?>" size="15"
                           maxlength="20"/></td>
                <td><input type="text" name="concentracionI" id="concentracionI<?= $i ?>"
                           value="<?= $Gestor->Formato($ROW[$i]['concentracionI']) ?>" onblur="_FLOAT(this);Calcula();"
                           class="monto2"/>
                    <select name="medida1" id="medida1<?= $i ?>">
                        <option value="0">M</option>
                        <option value="1" <?php if ($ROW[$i]['med1'] == '1') echo 'selected' ?>>&mu;M</option>
                        <option value="2" <?php if ($ROW[$i]['med1'] == '2') echo 'selected' ?>>nM</option>
                        <option value="3" <?php if ($ROW[$i]['med1'] == '3') echo 'selected' ?>>mM</option>
                        <option value="4" <?php if ($ROW[$i]['med1'] == '4') echo 'selected' ?>>pM</option>
                        <option value="5" <?php if ($ROW[$i]['med1'] == '5') echo 'selected' ?>>&mu;L</option>
                        <option value="6" <?php if ($ROW[$i]['med1'] == '6') echo 'selected' ?>>mL</option>
                        <option value="7" <?php if ($ROW[$i]['med1'] == '7') echo 'selected' ?>>L</option>
                        <option value="8" <?php if ($ROW[$i]['med1'] == '8') echo 'selected' ?>>g</option>
                        <option value="9" <?php if ($ROW[$i]['med1'] == '9') echo 'selected' ?>>&mu;g</option>
                        <option value="A" <?php if ($ROW[$i]['med1'] == 'A') echo 'selected' ?>>ng</option>
                    </select></td>
                <td><input type="text" name="disolvente" id="disolvente<?= $i ?>"
                           value="<?= $Gestor->Formato($ROW[$i]['disolvente']) ?>" onblur="_FLOAT(this);Calcula();"
                           class="monto2"/>
                    <select name="medida2" id="medida2<?= $i ?>">
                        <option value="0">M</option>
                        <option value="1" <?php if ($ROW[$i]['med2'] == '1') echo 'selected' ?>>&mu;M</option>
                        <option value="2" <?php if ($ROW[$i]['med2'] == '2') echo 'selected' ?>>nM</option>
                        <option value="3" <?php if ($ROW[$i]['med2'] == '3') echo 'selected' ?>>mM</option>
                        <option value="4" <?php if ($ROW[$i]['med2'] == '4') echo 'selected' ?>>pM</option>
                        <option value="5" <?php if ($ROW[$i]['med2'] == '5') echo 'selected' ?>>&mu;L</option>
                        <option value="6" <?php if ($ROW[$i]['med2'] == '6') echo 'selected' ?>>mL</option>
                        <option value="7" <?php if ($ROW[$i]['med2'] == '7') echo 'selected' ?>>L</option>
                        <option value="8" <?php if ($ROW[$i]['med2'] == '8') echo 'selected' ?>>g</option>
                        <option value="9" <?php if ($ROW[$i]['med2'] == '9') echo 'selected' ?>>&mu;g</option>
                        <option value="A" <?php if ($ROW[$i]['med2'] == 'A') echo 'selected' ?>>ng</option>
                    </select></td>
                <td><input type="text" name="concentracionF" id="concentracionF<?= $i ?>"
                           value="<?= $Gestor->Formato($ROW[$i]['concentracionF']) ?>" onblur="_FLOAT(this);Calcula();"
                           class="monto2"/>
                    <select name="medida3" id="medida3<?= $i ?>">
                        <option value="0">M</option>
                        <option value="1" <?php if ($ROW[$i]['med3'] == '1') echo 'selected' ?>>&mu;M</option>
                        <option value="2" <?php if ($ROW[$i]['med3'] == '2') echo 'selected' ?>>nM</option>
                        <option value="3" <?php if ($ROW[$i]['med3'] == '3') echo 'selected' ?>>mM</option>
                        <option value="4" <?php if ($ROW[$i]['med3'] == '4') echo 'selected' ?>>pM</option>
                        <option value="5" <?php if ($ROW[$i]['med3'] == '5') echo 'selected' ?>>&mu;L</option>
                        <option value="6" <?php if ($ROW[$i]['med3'] == '6') echo 'selected' ?>>mL</option>
                        <option value="7" <?php if ($ROW[$i]['med3'] == '7') echo 'selected' ?>>L</option>
                        <option value="8" <?php if ($ROW[$i]['med3'] == '8') echo 'selected' ?>>g</option>
                        <option value="9" <?php if ($ROW[$i]['med3'] == '9') echo 'selected' ?>>&mu;g</option>
                        <option value="A" <?php if ($ROW[$i]['med3'] == 'A') echo 'selected' ?>>ng</option>
                    </select></td>
                <td><input type="text" readonly id="formula<?= $i ?>" class="monto2"/></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <br/>
    <br/><?= $Gestor->Encabezado('A0007', 'p', '') ?>
    <script>Calcula();</script>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
</center>
</body>
</html>