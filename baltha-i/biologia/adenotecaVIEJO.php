<?php
define('__MODULO__', 'biologia');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _adenoteca();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('a0', 'hr', 'Inventario :: ADNoteca-ARNoteca') ?>
<center>
    <br/>
    <div id="container" style="width:500px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>Plaga</th>
                <th>Caja</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->MuestraPlagas();
            $Gestor->VaciaTemporal();
            for ($x = 0; $x < count($ROW); $x++) {
                $ROW2 = $Gestor->ResMuestra($ROW[$x]['id']);
                ?>
                <tr class="gradeA" align="center">
                    <td><?= $ROW[$x]['nombre'] ?></td>
                    <td>
                        <select id="caja<?= $x ?>" dato1="<?= $ROW[$x]['id'] ?>" dato2="<?= $ROW[$x]['nombre'] ?>">
                            <option value="">...</option>
                            <?php for ($i = 0; $i < count($ROW2); $i++) {
                                if (isset($ROW2[0]['bandeja']) && $ROW2[0]['bandeja'] != '') {
                                    ?>
                                    <option value="<?= $ROW2[0]['bandeja'] ?>"><?= $ROW2[0]['bandeja'] ?></option>
                                <?php }
                            } ?>
                        </select>
                    </td>
                    <td><img onclick="ResVer('<?= $x ?>')" src="<?php $Gestor->Incluir('previa', 'bkg') ?>" title="Ver"
                             class="tab2"/></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/><input type="button" value="Agregar" class="boton" onClick="ResAgrega();">
    &nbsp;&nbsp;&nbsp;<input type="button" value="Historial Muestras Desechadas" class="boton"
                             onClick="window.open('historial_AdnArnteca.php?tk=')">
</center>
</body>
</html>