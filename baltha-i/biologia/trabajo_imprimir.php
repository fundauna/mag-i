<?php
define('__MODULO__', 'biologia');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _trabajo_imprimir();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<center>
    <?= $Gestor->Encabezado('A0014', 'e', 'Hoja de trabajo Biolog&iacute;a Molecular') ?>
    <br>
    <table class="radius" width="90%">
        <tr>
            <td colspan="3" class="titulo">Informaci&oacute;n General</td>
        </tr>
        <tr>
            <td colspan="3"><font size="-2"><strong>Analista:</strong>&nbsp;<?= $ROW[0]['fechaA'] ?></font></td>
        </tr>
        <tr>
            <td colspan="3"><strong>Procedimiento:</strong>&nbsp;<?= $ROW[0]['procedimiento'] ?></td>
        </tr>
        <tr>
            <td colspan="3"><strong>Observaciones:</strong>&nbsp;<?= $ROW[0]['obsA'] ?></td>
        </tr>
        <tr>
            <td colspan="3">
                <hr/>
            </td>
        </tr>
        <tr align="center" style="vertical-align:top">
            <td>
                <strong>An&aacute;lisis:</strong>
                <table class="radius">
                    <?php
                    $ROW2 = $Gestor->DetalleAnalisis('1');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td><?= $ROW2[$i]['nombre'] ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </td>
            <td>
                <strong>Tipos de material:</strong>
                <table class="radius">
                    <?php
                    $ROW2 = $Gestor->DetalleAnalisis('2');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td><?= $ROW2[$i]['nombre'] ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </td>
            <td>
                <strong>Plagas a detectar:</strong>
                <table class="radius">
                    <?php
                    $ROW2 = $Gestor->DetalleAnalisis('3');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td><?= $ROW2[$i]['nombre'] ?>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <hr/>
            </td>
        </tr>
        <tr>
            <td colspan="3"><strong>Muestras:</strong>&nbsp;
                <?php
                $ROW2 = $Gestor->DetalleMuestras(0);
                for ($i = 0; $i < count($ROW2); $i++) echo $ROW2[$i]['codigo'], ', ';
                ?>
            </td>
        </tr>
    </table>
    <?php if ($ROW[0]['humedadB'] != '0') { ?>
        <br/>
        <table class="radius" width="90%">
            <tr>
                <td class="titulo">Preprocesamiento</td>
            </tr>
            <tr>
                <td><font size="-2"><strong>Analista:</strong>&nbsp;<?= $ROW[0]['fechaB'] ?></font></td>
            </tr>
            <tr>
                <td><strong>Fecha de preprocesamiento:</strong>&nbsp;<?= $ROW[0]['fechaProc'] ?></td>
            </tr>
            <tr>
                <td><strong>Observaciones:</strong>&nbsp;<?= $ROW[0]['obsB'] ?></td>
            </tr>
        </table>
    <?php } ?>
    <?php if ($ROW[0]['humedadC'] != '0') { ?>
        <br/>
        <table class="radius" width="90%">
            <tr>
                <td class="titulo">Extracci&oacute;n</td>
            </tr>
            <tr>
                <td><font size="-2"><strong>Analista:</strong>&nbsp;<?= $ROW[0]['fechaC'] ?></font></td>
            </tr>
            <tr>
                <td>
                    <strong>Controles:</strong>
                    <table class="radius">
                        <tr align="center">
                            <td><strong>C&oacute;digo &uacute;nico </strong></td>
                        </tr>
                        <?php
                        $ROW3 = $Gestor->DetalleControles(1);
                        for ($i = 0; $i < count($ROW3); $i++) {
                            ?>
                            <tr>
                                <td><?= $ROW3[$i]['control'] ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </td>
            </tr>
            <tr>
                <td><strong>Observaciones:</strong>&nbsp;<?= $ROW[0]['obsC'] ?></td>
            </tr>
        </table>
    <?php } ?>
    <?php if ($ROW[0]['humedadD'] != '0') { ?>
        <br/>
        <table class="radius" width="90%">
            <tr>
                <td colspan="6" class="titulo">Cuantificaci&oacute;n</td>
            </tr>
            <tr>
                <td colspan="6"><font size="-2"><strong>Analista:</strong>&nbsp;<?= $ROW[0]['fechaD'] ?></font></td>
            </tr>
            <tr>
                <td>Concentraci&oacute;n normalizada (ng/&mu;L):</td>
                <td><input type="text" id="normalizada" class="monto" value="<?= $ROW[0]['normalizada'] ?>" readonly>
                </td>
                <td>Analito:</td>
                <td><select disabled>
                        <option value="0" <?php if ($ROW[0]['analito'] != '0') echo 'selected'; ?>>ADN gen&oacute;mico
                        </option>
                        <option value="1" <?php if ($ROW[0]['analito'] == '1') echo 'selected'; ?>>ARN total</option>
                        <option value="2" <?php if ($ROW[0]['analito'] == '2') echo 'selected'; ?>>ADN doble banda
                        </option>
                        <option value="3" <?php if ($ROW[0]['analito'] == '3') echo 'selected'; ?>>ADN simple banda
                        </option>
                        <option value="4" <?php if ($ROW[0]['analito'] == '4') echo 'selected'; ?>>ARN</option>
                    </select></td>
                <td>Unidad:</td>
                <td><select disabled>
                        <option value="0" <?php if ($ROW[0]['unidad'] == '0') echo 'selected'; ?>>&mu;g/&mu;L</option>
                        <option value="1" <?php if ($ROW[0]['unidad'] == '1') echo 'selected'; ?>>ng/&mu;L</option>
                    </select></td>
            </tr>
            <tr>
                <td colspan="6">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <strong>C&aacute;lculo de diluci&oacute;n:</strong>
                    <table class="radius" width="100%">
                        <tr align="center">
                            <td>Muestra</td>
                            <td>Conc. Acido<br/>Nuc. (<?= $ROW[0]['unidad'] == '0' ? '&mu;g/&mu;L' : 'ng/&mu;L' ?>)</td>
                            <td>Coeficiente<br/>260/280</td>
                            <td>Coeficiente<br/>260/230</td>
                            <td>Volumen final<br/>deseado (&mu;l)</td>
                            <td>Volumen de la<br/>muestra a tomar</td>
                            <td>Cantidad de<br/>diluyente</td>
                        </tr>
                        <?php
                        $ROW2 = $Gestor->DetallePlantilla('D');
                        for ($i = 0; $i < count($ROW2); $i++) {
                            ?>
                            <tr align="center">
                                <td><?= $ROW2[$i]['muestra'] ?></td>
                                <td><input type="text" id="CAN<?= $i ?>" name="CAN" class="monto2"
                                           value="<?= $ROW2[$i]['CAN'] ?>" readonly/></td>
                                <td><input type="text" id="coef1<?= $i ?>" name="coef1" class="monto2"
                                           value="<?= $ROW2[$i]['coef1'] ?>" readonly/></td>
                                <td><input type="text" id="coef2<?= $i ?>" name="coef2" class="monto2"
                                           value="<?= $ROW2[$i]['coef2'] ?>" readonly/></td>
                                <td><input type="text" id="vol1<?= $i ?>" name="vol1" class="monto2"
                                           value="<?= $ROW2[$i]['vol1'] ?>" readonly></td>
                                <td><input type="text" id="vol2<?= $i ?>" class="monto2" readonly/></td>
                                <td><input type="text" id="vol3<?= $i ?>" class="monto2" readonly/></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </td>
            </tr>
            <tr>
                <td><strong>Observaciones:</strong></td>
                <td colspan="5"><?= $ROW[0]['obsD'] ?></td>
            </tr>
        </table>
    <?php } ?>
    <?php if ($ROW[0]['humedadE'] != '0') { ?>
        <br/>
        <table class="radius" width="90%">
            <tr>
                <td colspan="2" class="titulo">PCR</td>
            </tr>
            <tr>
                <td colspan="2"><font size="-2"><strong>Analista:</strong>&nbsp;<?= $ROW[0]['fechaE'] ?></font></td>
            </tr>
            <tr>
                <td colspan="2"><strong>Fecha:</strong>&nbsp;<?= $ROW[0]['fechaPcr'] ?></td>
            </tr>
            <tr>
                <td width="33%"><strong>Cantidad de reacciones:</strong>&nbsp;
                    <input type="text" id="reaccionesE" class="monto2" value="<?= $ROW[0]['reaccionesE'] ?>" readonly>
                </td>
                <td width="67%">
                    <strong>Controles:</strong>
                    <table class="radius">
                        <tr align="center">
                            <td><strong>Nombre</strong></td>
                        </tr>
                        <?php
                        $ROW3 = $Gestor->DetalleControles();
                        for ($i = 0; $i < count($ROW3); $i++) {
                            ?>
                            <tr>
                                <td><?= $ROW3[$i]['control'] ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr>

                <td colspan="2">
                    <strong>Plantilla de mezcla de reacci&oacute;n:</strong>
                    <table class="radius" width="98%">
                        <tr align="center">
                            <td><strong>#</strong></td>
                            <td><strong>Tipo</strong></td>
                            <td><strong>Descripci&oacute;n</strong></td>
                            <td><strong>C&oacute;digo</strong></td>
                            <td><strong>Conc. Stock</strong></td>
                            <td><strong>Conc. Final</strong></td>
                            <td align="center"><strong>Volumen 1x<br/>(&mu;L)</strong></td>
                            <td align="center"><strong>Reacciones<br/>(&mu;L)</strong></td>
                        </tr>
                        <tbody>
                        <?php
                        $ROW2 = $Gestor->DetallePlantilla('E');
                        for ($i = 0, $total1 = 0, $total2 = 0; $i < count($ROW2); $i++) {
                            $total1 += $ROW2[$i]['volumen'];
                            $total2 += $ROW2[$i]['reacciones'];
                            ?>
                            <tr align="center">
                                <td><?= $i + 1 ?></td>
                                <td><select name="E1tipo" id="E1tipo<?= $i ?>" disabled>
                                        <option value="0" <?php if ($ROW2[$i]['tipo'] == '0') echo 'selected'; ?>>
                                            Reactivo
                                        </option>
                                        <option value="1" <?php if ($ROW2[$i]['tipo'] == '1') echo 'selected'; ?>>
                                            Alicuota
                                        </option>
                                    </select></td>
                                <td><?= $ROW2[$i]['descr'] ?></td>
                                <td><?= $ROW2[$i]['codigo'] ?></td>
                                <td><?= $ROW2[$i]['concS'] ?></td>
                                <td><?= $ROW2[$i]['concF'] ?></td>
                                <td><?= $ROW2[$i]['volumen'] ?></td>
                                <td><?= $ROW2[$i]['reacciones'] ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                        <tr>
                            <td colspan="6" align="right"><strong>Total:</strong></td>
                            <td id="tot1" align="center"><?= $total1 ?></td>
                            <td id="tot2" align="center"><?= $total2 ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td colspan="2"><strong>Observaciones:</strong>&nbsp;<?= $ROW[0]['obsE'] ?></td>
            </tr>
        </table>
    <?php } ?>
    <?php if ($ROW[0]['tiempoX'] != '0') { ?>
        <br/>
        <table class="radius" width="90%">
            <tr>
                <td class="titulo">Electroforesis PCR</td>
            </tr>
            <tr>
                <td><font size="-2"><strong>Analista:</strong>&nbsp;<?= $ROW[0]['fechaX'] ?></font></td>
            </tr>

            <tr>
                <td><strong>Observaciones:</strong>&nbsp;<?= $ROW[0]['obsX'] ?></td>
            </tr>
        </table>
    <?php } ?>
    <?php if ($ROW[0]['Famplicon1'] != '0') { ?>
        <br/>
        <table class="radius" width="90%">
            <tr>
                <td class="titulo">Digesti&oacute;n con enzimas de restricci&oacute;n</td>
            </tr>
            <tr>
                <td><font size="-2"><strong>Analista:</strong>&nbsp;<?= $ROW[0]['fechaF'] ?></font></td>
            </tr>
            <tr>
                <td>Cantidad de reacciones:&nbsp;<input type="text" id="reaccionesF" class="monto2"
                                                        value="<?= $ROW[0]['reaccionesF'] ?>" readonly></td>
            </tr>
            <tr>
                <td>
                    <strong>Plantilla de mezcla de digesti&oacute;n:</strong>
                    <table class="radius" width="90%">
                        <tr align="center">
                            <td><strong>#</strong></td>
                            <td><strong>Tipo</strong></td>
                            <td><strong>Descripci&oacute;n</strong></td>
                            <td><strong>C&oacute;digo</strong></td>
                            <td><strong>Volumen 1x (&mu;L)</strong></td>
                            <td><strong>Reacciones (&mu;L)</strong></td>
                        </tr>
                        <?php
                        $ROW2 = $Gestor->DetallePlantilla('F');
                        for ($i = 0, $total1 = 0, $total2 = 0; $i < count($ROW2); $i++) {
                            $total1 += $ROW2[$i]['volumen'];
                            $total2 += $ROW2[$i]['reacciones'];
                            ?>
                            <tr align="center">
                                <td><?= $i + 1 ?></td>
                                <td><select name="F1tipo" id="F1tipo<?= $i ?>" disabled>
                                        <option value="0" <?php if ($ROW2[$i]['tipo'] == '0') echo 'selected'; ?>>
                                            Reactivo
                                        </option>
                                        <option value="1" <?php if ($ROW2[$i]['tipo'] == '1') echo 'selected'; ?>>
                                            Alicuota
                                        </option>
                                    </select></td>
                                <td><?= $ROW2[$i]['descr'] ?></td>
                                <td><?= $ROW2[$i]['codigo'] ?></td>
                                <td><?= $ROW2[$i]['volumen'] ?></td>
                                <td><?= $ROW2[$i]['reacciones'] ?></td>
                            </tr>
                            <?php
                        }
                        $total1 += $ROW[0]['Famplicon1'];
                        $total2 += $ROW[0]['Famplicon2'];
                        ?>
                        <tr align="center">
                            <td colspan="2">&nbsp;</td>
                            <td colspan="2" align="center">Producto PCR:</td>
                            <td id="amplicon"><?= $ROW[0]['Famplicon1'] ?></td>
                            <td><?= $ROW[0]['Famplicon2'] ?></td>
                        </tr>
                        <tr align="center">
                            <td colspan="4" align="right"><strong>Total:</strong></td>
                            <td id="tot3" align="center"><?= $total1 ?></td>
                            <td id="tot4" align="center"><?= $total2 ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <hr/>
                </td>
            </tr>
            <tr>
                <td><strong>Observaciones:</strong>&nbsp;<?= $ROW[0]['obsF'] ?></td>
            </tr>
        </table>
    <?php } ?>
    <?php if ($ROW[0]['tiempoG'] != '0') { ?>
        <br/>
        <table class="radius" width="90%">
            <tr>
                <td class="titulo">Electroforesis Digesti&oacute;n</td>
            </tr>
            <tr>
                <td><font size="-2"><strong>Analista:</strong>&nbsp;<?= $ROW[0]['fechaG'] ?></font></td>
            </tr>
            <tr>
                <td><strong>Observaciones:</strong>&nbsp;<?= $ROW[0]['obsG'] ?></td>
            </tr>
        </table>
    <?php } ?>
    <br/>
    <table class="radius" width="90%">
        <tr>
            <td colspan="2" class="titulo">Resultados</td>
        </tr>
        <tr>
            <td colspan="2"><font size="-2"><strong>Analista:</strong>&nbsp;<?= $ROW[0]['fechaR'] ?>
                    &nbsp;<?php if ($ROW[0]['fechaM'] != '') echo ", Modificado: {$ROW[0]['fechaM']}"; ?></font></td>
        </tr>
        <tr>
            <td colspan="2">An&aacute;lisis de resultados:<br/><textarea style="width:80%"
                                                                         readonly><?= $ROW[0]['obsR'] ?></textarea></td>
        </tr>
    </table>
    <br/>
    <input type="button" value="Imprimir" class="boton2" onclick="window.print()">
</center>
<br/><?= $Gestor->Encabezado('A0014', 'p', '') ?>
<script>
    CalculaReacciones();
    window.print();
</script>
<?= $Gestor->Footer() ?>
</body>
</html>