<?php
define('__MODULO__', 'biologia');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);
$Gestor = new _agrega_caja();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('', 'hr', 'Inventario :: Agrega Plaga') ?>
<center>
    <form name="form" id="form">
        <input type="hidden" id="ID" name="" value="<?= $_GET['ID'] ?>"/>
        <input type="hidden" id="L" name="" value="<?= $_GET['L'] ?>"/>
        <table class="radius" width="98%">
            <tr>
                <td class="titulo" colspan="2">Detalle de la caja</td>
            </tr>
            <tr>
                <td>Nombre</td>
                <td><input type="text" name="" id="nombre" value=""/></td>
                <td></td>
            </tr>

            <tr>
                <td>Ubicaci&oacute;n</td>
                <td><input type="text" name="" id="ubicacion" value=""/></td>
            </tr>
        </table>
        <br/>
        <input type="button" id="btn" value="Agregar" class="boton" onClick="datos()"/>
    </form>
</center>
</body>
</html>