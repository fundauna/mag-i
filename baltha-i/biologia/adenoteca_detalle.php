<?php
define('__MODULO__', 'biologia');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _adenoteca_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

if ($ROW[0]['bandeja'] == 'c') {
    $disabled = 'disabled';
    $display = 'style="display:none"';
} else {
    $disabled = '';
    $display = '';
}

if ($ROW[0]['bandeja'] != '')
    @list($rotulo, $bandeja, $x, $y) = explode(';', $ROW[0]['bandeja']);
else
    list($rotulo, $bandeja, $x, $y) = array('', '', '', '');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('a1', 'hr', 'Inventario :: Detalle ADNoteca-ARNoteca') ?>
<?= $Gestor->Encabezado('A0001', 'e', 'Detalle ADNoteca-ARNoteca') ?>
<center>
    <input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
    <input type="hidden" name="bandeja" id="bandeja0" value="<?= $bandeja ?>"/>
    <input type="hidden" name="x" id="x0" value="<?= $x ?>"/>
    <input type="hidden" name="y" id="y0" value="<?= $y ?>"/>
    <input type="hidden" id="tipo"/><!-- PARA VER CUAL LISTADOR EST� ACTIVO -->
    <table class="radius" width="98%" style="font-size:12px;">
        <tr>
            <td class="titulo" colspan="6">Datos</td>
        </tr>
        <tr>
            <td>C&oacute;digo de muestra:</td>
            <td><input type="text" id="cs" size="10" maxlength="10"
                       value="<?= $ROW[0]['id'] ?>" <?php if ($_GET['acc'] != 'I') echo 'readonly'; ?> /></td>
            <td>&Aacute;cido nucleico:</td>
            <td><select id="analito">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['analito'] == '0') echo 'selected'; ?>>ADN gen&oacute;mico
                    </option>
                    <option value="1" <?php if ($ROW[0]['analito'] == '1') echo 'selected'; ?>>ARN total</option>
                    <option value="2" <?php if ($ROW[0]['analito'] == '2') echo 'selected'; ?>>ADN doble banda</option>
                    <option value="3" <?php if ($ROW[0]['analito'] == '3') echo 'selected'; ?>>ADN simple banda</option>
                    <option value="4" <?php if ($ROW[0]['analito'] == '4') echo 'selected'; ?>>ARN</option>
                </select></td>
            <td>Bandeja:</td>
            <td><input type="text" name="rotulo" id="rotulo0" class="lista" readonly onclick="BandejasLista('0')"
                       style="width:100px;" value="<?= $rotulo ?>"/></td>
        </tr>
        <tr>
            <td>Cultivo:</td>
            <td><input type="text" id="cultivo" size="10" maxlength="20" value="<?= $ROW[0]['cultivo'] ?>"/></td>
            <td>Cuantificaci&oacute;n &aacute;cido nucleico:</td>
            <td><input type="text" id="cuanti" class="monto2" onblur="_FLOAT(this)" value="<?= $ROW[0]['cuanti'] ?>">
            </td>
            <td>Hoja de trabajo:</td>
            <td><input type="text" id="hoja" size="10" maxlength="12" value="<?= $ROW[0]['hoja'] ?>"/></td>
        </tr>
        <tr>
            <td>Fecha de ingreso:</td>
            <td><?= $ROW[0]['fechaC'] ?></td>
            <td>Fecha de salida:</td>
            <td><input name="text" type="text" class="fecha" id="fechaA" onclick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fechaA'] ?>" readonly="readonly"/></td>
            <td>Analista:</td>
            <td><?= $ROW[0]['analista'] ?></td>
        </tr>
        <tr>
            <td>Observaciones:</td>
            <td colspan="5"><input type="text" id="observaciones" size="50" maxlength="50"
                                   value="<?= $ROW[0]['obs'] ?>"></td>
        </tr>
        <tr>
            <td colspan="6">
                <hr/>
            </td>
        </tr>
        <tr align="center" style="vertical-align:top">
            <td colspan="3">
                <img onclick="AgregarAnalisis('5', 'A1')" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                     title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>An&aacute;lisis:</strong>
                <table class="radius">
                    <tbody id="loloA1">
                    <?php
                    $ROW2 = $Gestor->DetalleAnalisis('1');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td><input type="text" id="A1analisis<?= $i ?>" class="lista2" readonly
                                       onclick="MacroAnalisisLista('5', 'A1', '<?= $i ?>')"
                                       value="<?= $ROW2[$i]['nombre'] ?>" <?= $disabled ?>/><input type="hidden"
                                                                                                   id="A1cod_analisis<?= $i ?>"
                                                                                                   name="A1cod_analisis"
                                                                                                   value="<?= $ROW2[$i]['id'] ?>"/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
            <td colspan="3">
                <img onclick="AgregarAnalisis('7', 'A2')" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                     title="Agregar l�nea" class="tab" <?= $display ?>/>&nbsp;<strong>Tipos de material:</strong>
                <table class="radius">
                    <tbody id="loloA2">
                    <?php
                    $ROW2 = $Gestor->DetalleAnalisis('2');
                    for ($i = 0; $i < count($ROW2); $i++) {
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td><input type="text" id="A2analisis<?= $i ?>" class="lista2" readonly
                                       onclick="MacroAnalisisLista('7', 'A2', '<?= $i ?>')"
                                       value="<?= $ROW2[$i]['nombre'] ?>" <?= $disabled ?>/><input type="hidden"
                                                                                                   id="A2cod_analisis<?= $i ?>"
                                                                                                   name="A2cod_analisis"
                                                                                                   value="<?= $ROW2[$i]['id'] ?>"/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="98%">
        <tr>
            <td class="titulo" colspan="3">Detalle&nbsp;<img onclick="AnalisisMas()"
                                                             src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                                                             title="Agregar l�nea" class="tab"/></td>
        </tr>
        <tr align="center">
            <td><strong>Pat&oacute;genos analizados</strong></td>
            <td><strong>Resultado</strong></td>
            <td><strong>Observaciones</strong></td>
        </tr>
        <tbody id="lolo">
        <?php
        $ROW = $Gestor->Detalle();
        for ($x = 0; $x < count($ROW); $x++) {
            ?>
            <tr align="center">
                <td><input type="text" id="A3analisis<?= $x ?>" class="lista2" readonly
                           onclick="MacroAnalisisLista('6', 'A3', '<?= $x ?>')" value="<?= $ROW[$x]['nombre'] ?>"
                           <?= $disabled ?>/><input type="hidden" id="A3cod_analisis<?= $x ?>" name="A3cod_analisis"
                                                    value="<?= $ROW[$x]['id'] ?>"/></td>
                <td><select id="resultado<?= $x ?>" name="resultado">
                        <option value="">...</option>
                        <option value="0" <?php if ($ROW[$x]['resultado'] == '0') echo 'selected'; ?>>-</option>
                        <option value="1" <?php if ($ROW[$x]['resultado'] == '1') echo 'selected'; ?>>+</option>
                        <option value="2" <?php if ($ROW[$x]['resultado'] == '2') echo 'selected'; ?>>NC</option>
                    </select></td>
                <td><textarea id="obs<?= $x ?>" name="obs" style="height:38px"><?= $ROW[$x]['obs'] ?></textarea></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <br/>
    <br/><?= $Gestor->Encabezado('A0001', 'p', '') ?>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php if ($_GET['acc'] == 'M') { ?>
        &nbsp;&nbsp;&nbsp;<input type="button" id="btn" value="Eliminar" class="boton" onClick="eliminar()">
    <?php } ?>
</center>
</body>
</html>