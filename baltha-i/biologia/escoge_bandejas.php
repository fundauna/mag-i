<?php
define('__MODULO__', 'biologia');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _escoge_bandejas();
$ROW = $Gestor->BandejasMuestra();
if (!$ROW) die('Caja inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <script>
        function Escoge() {
            var adn = document.getElementById('bandeja').value;
            if (adn != '') {
                window.location.href = 'muestra_bandejas.php?bandeja=' + adn;
            } else {
                OMEGA('Debe seleccionar una bandeja');
                return;
            }
        }
    </script>
</head>
<body>
<?php $Gestor->Incluir('a0005', 'hr', 'Inventario :: Selecci&oacute;n de bandeja') ?>
<?= $Gestor->Encabezado('A0005', 'e', 'Selecci&oacute;n de bandeja') ?>
<table class="radius" align="center" border="1" cellpadding="4" cellspacing="4">
    <tr>
        <td><strong>Nombre de plaga:</strong></td>
        <td><?= $_GET['nP'] ?></td>
    </tr>
    <tr>
        <td align="center"><strong>Caja:</strong></td>
        <td align="center"><?= $_GET['caja'] ?></td>
    </tr>
    <tr align="center">
        <td><strong>Bandeja:</strong></td>
        <td>
            <select id="bandeja">
                <option value="">...</option>
                <?php for ($x = 0; $x < count($ROW); $x++) { ?>
                    <option value="<?= $ROW[$x]['bandeja'] ?>"><?= $ROW[$x]['bandeja'] ?></option>
                <?php } ?>
            </select>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center"><input type="button" value="Aceptar" class="boton" onClick="Escoge();"></td>
    </tr>
</table>
<br/><?= $Gestor->Encabezado('A0005', 'p', '') ?>
</body>
</html>