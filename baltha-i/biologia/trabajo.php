<?php
define('__MODULO__', 'biologia');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _trabajo();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('a0012', 'hr', 'Inventario :: Hoja de trabajo') ?>
<?= $Gestor->Encabezado('A0012', 'e', 'Hoja de trabajo') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" style="font-size:12px;width:500px;">
            <tr>
                <td class="titulo" colspan="3">Filtro</td>
            </tr>
            <tr>
                <td><select name="estado">
                        <option value="">Todos</option>
                        <option value="r" <?php if ($_POST['estado'] == 'r') echo 'selected'; ?>>Pendiente</option>
                        <option value="a" <?php if ($_POST['estado'] == 'a') echo 'selected'; ?>>Anulada</option>
                        <option value="f" <?php if ($_POST['estado'] == 'f') echo 'selected'; ?>>Finalizada</option>
                    </select></td>
                <td>Procedimiento de referencia: <input type="text" name="codigo" id="codigo"
                                                        value="<?= $_POST['codigo'] ?>"/></td>
                <td><input type="button" value="Buscar" class="boton2" onclick="ResBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:500px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>No.</th>
                <th>Procedimiento</th>
                <th>Estado</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->ResMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#" onclick="ResModifica('<?= $ROW[$x]['id'] ?>')"><?= $ROW[$x]['id'] ?></a></td>
                    <td><?= $ROW[$x]['procedimiento'] ?></td>
                    <td><?= $Gestor->Estado($ROW[$x]['estado']) ?></td>
                    <td>
                        <?php if ($ROW[$x]['estado'] == 'r' and $ROW[$x]['analista'] == $_POST['usuario']) { ?>
                            <img onclick="ResElimina('<?= $ROW[$x]['id'] ?>')"
                                 src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Anular" class="tab2"/>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/><input type="button" value="Agregar" class="boton" onClick="ResAgrega();">
</center>
<br/><?= $Gestor->Encabezado('A0012', 'p', '') ?>
</body>
</html>