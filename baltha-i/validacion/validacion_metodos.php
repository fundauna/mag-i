<?php
define('__MODULO__', 'validacion');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _validacion_metodos();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('q8', 'hr', 'Validaci�n de m�todos :: Historial') ?>
<?= $Gestor->Encabezado('Q0008', 'e', 'Historial') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <input type="hidden" name="LAB" id="LAB" value="<?= $_POST['LAB'] ?>"/>
        <table class="radius" align="center" width="600px">
            <tr>
                <td class="titulo" colspan="6">Filtro</td>
            </tr>
            <tr>
                <td>Desde: <input type="text" id="desde" name="desde" class="fecha" value="<?= $_POST['desde'] ?>"
                                  readonly onClick="show_calendar(this.id);">&nbsp;
                    Hasta: <input type="text" id="hasta" name="hasta" class="fecha" value="<?= $_POST['hasta'] ?>"
                                  readonly onClick="show_calendar(this.id);"></td>
                <td>Tipo:</td>
                <td><select name="tipo" style="width:90px">
                        <option value="">Todos</option>
                        <?php if ($_POST['LAB'] == '3') { ?>
                            <option value="1" <?php if ($_POST['tipo'] == '1') echo 'selected'; ?>>Linealidad</option>
                            <option value="2" <?php if ($_POST['tipo'] == '2') echo 'selected'; ?>>Repetibilidad
                            </option>
                            <option value="3" <?php if ($_POST['tipo'] == '3') echo 'selected'; ?>>Reproducibilidad
                            </option>
                            <option value="7" <?php if ($_POST['tipo'] == '7') echo 'selected'; ?>>Recuperaci&oacute;n
                            </option>
                            <option value="8" <?php if ($_POST['tipo'] == '8') echo 'selected'; ?>>Robustez</option>
                        <?php } elseif ($_POST['LAB'] == '2') { ?>
                            <option value="4" <?php if ($_POST['tipo'] == '4') echo 'selected'; ?>>Biolog&iacute;a
                                Molecular
                            </option>
                        <?php } elseif ($_POST['LAB'] == '1') { ?>
                            <option value="5" <?php if ($_POST['tipo'] == '5') echo 'selected'; ?>>C&aacute;lculo para
                                validaciones
                            </option>
                            <option value="6" <?php if ($_POST['tipo'] == '6') echo 'selected'; ?>>Informe de validaci&oacute;n</option>
                        <?php } ?>
                    </select></td>
                <td>Estado:</td>
                <td><select name="estado" style="width:90px">
                        <option value="0">Generada</option>
                        <option value="1" <?php if ($_POST['estado'] == '1') echo 'selected'; ?>>Pend. Revisi&oacute;n
                        </option>
                        <option value="3" <?php if ($_POST['estado'] == '3') echo 'selected'; ?>>Pend.
                            Aprobaci&oacute;n
                        </option>
                        <option value="2" <?php if ($_POST['estado'] == '2') echo 'selected'; ?>>Aprobada</option>
                        <option value="5" <?php if ($_POST['estado'] == '5') echo 'selected'; ?>>Anulada</option>
                        <option value="" <?php if ($_POST['estado'] == '') echo 'selected'; ?>>Todos</option>
                    </select></td>
                <td><input type="button" value="Buscar" class="boton" onclick="SolicitudesBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:600px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>No.</th>
                <th>Fecha</th>
                <th>Tipo</th>
                <?php if ($_POST['LAB'] == '1') { ?>
                    <th>Matriz</th>
                <?php } ?>
                <th>Cumple</th>
                <th>Estado</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->SolicitudesMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#"
                           onclick="SolicitudesModifica('<?= $ROW[$x]['validacion'] ?>', '<?= $ROW[$x]['tipo'] ?>')"><?= $ROW[$x]['cs'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['fecha1'] ?></td>
                    <td><?= $Gestor->Tipo($ROW[$x]['tipo']) ?></td>
                    <?php if ($_POST['LAB'] == '1') { ?>
                        <td><?= $ROW[$x]['matriz'] ?></td>
                    <?php } ?>
                    <td><?= $ROW[$x]['cumple'] == '1' ? 'S�' : 'No' ?></td>
                    <td><?= $Gestor->Estado($ROW[$x]['estado']) ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/>
    Tipo:&nbsp;<select id="tipo2" style="width:160px">
        <?php if ($_POST['LAB'] == '3') { ?>
            <option value="1">Linealidad</option>
            <option value="2">Repetibilidad</option>
            <option value="3">Precisi&oacute;n Intermedia</option>
            <option value="7">Recuperaci&oacute;n</option>
            <option value="8">Robustez</option>
        <?php } elseif ($_POST['LAB'] == '2') { ?>
            <option value="4">Biolog&iacute;a Molecular</option>
        <?php } elseif ($_POST['LAB'] == '1') { ?>
            <option value="5">C&aacute;lculo para validaciones</option>
            <option value="6">Informe de validaci&oacute;n</option>
        <?php } ?>
    </select>&nbsp;
    <input type="button" value="Agregar" class="boton" onClick="SolicitudesAgrega();">
</center>
<?= $Gestor->Encabezado('Q0008', 'p', '') ?>
</body>
</html>