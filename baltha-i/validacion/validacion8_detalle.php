<?php
define('__MODULO__', 'validacion');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _validacion8_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
$estado = $ROW[0]['estado'];

if ($_GET['acc'] == 'M') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="cumple"/>
<input type="hidden" id="cs" value="<?= $ROW[0]['cs'] ?>"/>
<input type="hidden" id="equipo"/>
<center>
    <?php $Gestor->Incluir('q7', 'hr', 'Validación de métodos :: Validaci&oacute;n e implementaci&oacute;n de m&eacute;todos: Robustez (Trazas)') ?>
    <?= $Gestor->Encabezado('Q0007', 'e', 'Validaci&oacute;n e implementaci&oacute;n de m&eacute;todos: Robustez (Trazas)') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Datos del an&aacute;lisis</td>
        </tr>
        <tr>
            <td><strong>Referencia m&eacute;todo anal&iacute;tico</strong></td>
            <td colspan="3"><input type="text" id="metodo" style="width:98%" maxlength="250"
                                   value="<?= $ROW[0]['metodo'] ?>" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>Equipo</strong></td>
            <td><input type="text" id="tmp_equipo1" class="lista" value="<?= $ROW[0]['tmp_equipo1'] ?>" readonly
                       onclick="EquiposLista(1)" <?= $disabled ?>/></td>
            <td><strong>C&oacute;digo del estandar</strong></td>
            <td><input type="text" id="estandar" maxlength="20" value="<?= $ROW[0]['estandar'] ?>" <?= $disabled ?>/>
            </td>
        </tr>
        <tr>
            <td><strong>Nombre del equipo:</strong></td>
            <td><?= $ROW[0]['nombre'] ?></td>
            <td><strong>Masa estandar (mg)</strong></td>
            <td><input type="text" id="masa1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['masa'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Analito</strong></td>
            <td><input type="text" id="analito" maxlength="20" value="<?= $ROW[0]['analito'] ?>" <?= $disabled ?>/></td>
            <td><strong>Pureza (%)</strong></td>
            <td><input type="text" id="pureza" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['pureza'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Volumen de extracci&oacute;n (mL)</strong></td>
            <td><input type="text" id="vol_ext" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['vol_ext'] ?>"
                       <?= $disabled ?>></td>
            <td><strong>Coeficiente m curva</strong></td>
            <td><input type="text" id="coefM" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['coefM'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Alicuota de extracci&oacute;n (mL)</strong></td>
            <td><input type="text" id="ali_ext" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['ali_ext'] ?>"
                       <?= $disabled ?>></td>
            <td><strong>Coeficiente b curva</strong></td>
            <td><input type="text" id="coefB" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['coefB'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Volumen final (mL)</strong></td>
            <td><input type="text" id="vol_fin" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['vol_fin'] ?>"
                       <?= $disabled ?>></td>
            <td><strong>r de curva</strong></td>
            <td><input type="text" id="curva" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['curva'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Observaciones:</strong></td>
            <td><textarea id="obs" <?= $disabled ?> maxlength="250"
                          title="Alfanumérico (0-250)"><?= $ROW[0]['obs'] ?></textarea></td>
            <td><strong>Fecha:</strong></td>
            <td><?= $ROW[0]['fecha'] ?></td>
        </tr>
    </table>
    <br/>
    <table class="radius">
        <tr align="center">
            <td><strong>Factores</strong></td>
            <td><strong>Variable +</strong></td>
            <td><strong>Variable -</strong></td>
        </tr>
        <tr>
            <td><input type="text" id="fac1" maxlength="20" value="<?= $ROW[0]['fac1'] ?>" <?= $disabled ?>/></td>
            <td><input type="text" id="mas1" maxlength="20" value="<?= $ROW[0]['mas1'] ?>" <?= $disabled ?>/></td>
            <td><input type="text" id="men1" maxlength="20" value="<?= $ROW[0]['men1'] ?>" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><input type="text" id="fac2" maxlength="20" value="<?= $ROW[0]['fac2'] ?>" <?= $disabled ?>/></td>
            <td><input type="text" id="mas2" maxlength="20" value="<?= $ROW[0]['mas2'] ?>" <?= $disabled ?>/></td>
            <td><input type="text" id="men2" maxlength="20" value="<?= $ROW[0]['men2'] ?>" <?= $disabled ?>/></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="6">Matriz de experimentos (2 factor)</td>
        </tr>
        <tr align="center">
            <td><strong>Exp</strong></td>
            <td><strong>Variable 1</strong></td>
            <td><strong>Variable 2</strong></td>
            <td><strong>Masa (mg)</strong></td>
            <td><strong>Area</strong></td>
            <td><strong>Conc. (mg/kg)</strong></td>
        </tr>
        <tr align="center">
            <td>1</td>
            <td>V +</td>
            <td>V +</td>
            <td><input type="text" id="mass0" name="mass" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['mass0'] ?>" <?= $disabled ?>></td>
            <td><input type="text" id="area0" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['area0'] ?>"
                       <?= $disabled ?>></td>
            <td id="conc0"></td>
        </tr>
        <tr align="center">
            <td>2</td>
            <td>V -</td>
            <td>V +</td>
            <td><input type="text" id="mass1" name="mass" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['mass1'] ?>" <?= $disabled ?>></td>
            <td><input type="text" id="area1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['area1'] ?>"
                       <?= $disabled ?>></td>
            <td id="conc1"></td>
        </tr>
        <tr align="center">
            <td>3</td>
            <td>V +</td>
            <td>V -</td>
            <td><input type="text" id="mass2" name="mass" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['mass2'] ?>" <?= $disabled ?>></td>
            <td><input type="text" id="area2" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['area2'] ?>"
                       <?= $disabled ?>></td>
            <td id="conc2"></td>
        </tr>
        <tr align="center">
            <td>4</td>
            <td>V -</td>
            <td>V -</td>
            <td><input type="text" id="mass3" name="mass" class="monto" onblur="Redondear(this)"
                       value="<?= $ROW[0]['mass3'] ?>" <?= $disabled ?>></td>
            <td><input type="text" id="area3" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['area3'] ?>"
                       <?= $disabled ?>></td>
            <td id="conc3"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Resultados</td>
        </tr>
        <tr align="center">
            <td><strong>&Delta; Variable 1</strong></td>
            <td><strong>&Delta; Variable 2</strong></td>
            <td><strong>2^(1/2)*S </strong></td>
            <td><strong>Resultado final</strong></td>
        </tr>
        <tr align="center">
            <td id="resA"></td>
            <td id="resB"></td>
            <td id="resC"></td>
            <td id="resD"></td>
        </tr>
    </table>
    <br/>
    <?php
    if ($_GET['acc'] == 'M') {
        $ROW = $Gestor->Historial();
        echo '<table width="98%" class="radius" style="font-size:12px"></tr>';
        for ($x = 0; $x < count($ROW); $x++) {
            $ROW[$x]['accion'] = $Gestor->Accion($ROW[$x]['accion']);
            echo "<tr>
			<td><strong>{$ROW[$x]['accion']}</strong> {$ROW[$x]['nombre']} {$ROW[$x]['ap1']}, {$ROW[$x]['fecha1']}</td>
			<td>____________________</td>
		</tr>";
        }
        echo '</table><br />';
        ?>
        <?php if ($estado == '0') { ?>
            <input type="button" value="Procesar" class="boton" onClick="Procesar()">&nbsp;
            <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
        <?php } ?>
        <?php if ($estado == '1' and $Gestor->Revisar()) { ?>
            <input type="button" value="Revisar" class="boton" onClick="Revisar()"
                   title="Enviar notificación de aprobación al superior">&nbsp;
            <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
        <?php } ?>
        <?php if ($estado == '3' and $Gestor->Aprobar()) { ?>
            <input type="button" value="Aprobar" class="boton" onClick="Aprobar()">&nbsp;
            <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
        <?php } ?>
        <input type="button" value="Imprimir" class="boton" onClick="window.print()">
        <?php
    } else {
        ?>
        <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<script>__calcula();</script>
<?= $Gestor->Encabezado('Q0007', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>