<?php
define('__MODULO__', 'validacion');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _validacion1_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
$estado = $ROW[0]['estado'];

if ($_GET['acc'] == 'M') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('graficador', 'gfx'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="equipo"/>
<input type="hidden" id="cs" value="<?= $ROW[0]['cs'] ?>"/>
<center>
    <?php $Gestor->Incluir('q0', 'hr', 'Validación de métodos :: Validaci&oacute;n e implementaci&oacute;n de m&eacute;todos: Linealidad') ?>
    <?= $Gestor->Encabezado('Q0000', 'e', 'Validaci&oacute;n e implementaci&oacute;n de m&eacute;todos: Linealidad') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Datos del an&aacute;lisis</td>
        </tr>
        <tr>
            <td><strong>Referencia m&eacute;todo anal&iacute;tico:</strong></td>
            <td><input type="text" id="metodo" maxlength="30" value="<?= $ROW[0]['metodo'] ?>" <?= $disabled ?>/></td>
            <td><strong>Masa estandar (mg):</strong></td>
            <td><input type="text" id="masa" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['masa'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Equipo:</strong></td>
            <td><input type="text" id="tmp_equipo" name="tmp_equipo" class="lista" value="<?= $ROW[0]['tmp_equipo'] ?>"
                       readonly onclick="EquiposLista()" <?= $disabled ?>/></td>
            <td><strong>Pureza (%):</strong></td>
            <td><input type="text" id="pureza" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['pureza'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Nombre del equipo:</strong></td>
            <td><?= $ROW[0]['nombre'] ?></td>
            <td><strong>Volumen Int:</strong></td>
            <td><input type="text" id="volumen" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['volumen'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>C&oacute;digo del estandar:</strong></td>
            <td><input type="text" id="estandar" maxlength="20" value="<?= $ROW[0]['estandar'] ?>" <?= $disabled ?>/>
            </td>
            <td><strong>Volumen balon curva (mL):</strong></td>
            <td><input type="text" id="balon" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['balon'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Ingrediente activo:</strong></td>
            <td><input type="text" id="IA" maxlength="30" value="<?= $ROW[0]['IA'] ?>" <?= $disabled ?>/></td>
            <td><strong>Concentraci&oacute;n intermedia:</strong></td>
            <td id="conc"></td>
        </tr>
        <tr>
            <td><strong>Observaciones:</strong></td>
            <td><textarea id="obs" <?= $disabled ?> maxlength="250"
                          title="Alfanumérico (0-250)"><?= $ROW[0]['obs'] ?></textarea></td>
            <td><strong>Fecha:</strong></td>
            <td><?= $ROW[0]['fecha'] ?></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="8">Datos de an&aacute;lisis</td>
        </tr>
        <tr align="center">
            <td><strong>Alicuota de<br/>estandar (mL)</strong></td>
            <td><strong>Concentraci&oacute;n</strong></td>
            <td><strong>&Aacute;rea</strong></td>
            <td><strong>xi-xm</strong></td>
            <td><strong>(xi-xm)2</strong></td>
            <td><strong>yi-ym</strong></td>
            <td><strong>(yi-ym)2</strong></td>
            <td><strong>(xi-ym)(yi-ym)</strong></td>
        </tr>
        <tr>
            <td><input type="text" id="A0" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['A0'] ?>"
                       <?= $disabled ?>></td>
            <td><input type="text" id="B0" class="monto" readonly></td>
            <td><input type="text" id="C0" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['C0'] ?>"
                       <?= $disabled ?>></td>
            <td><input type="text" id="D0" class="monto" readonly></td>
            <td><input type="text" id="E0" class="monto" readonly></td>
            <td><input type="text" id="F0" class="monto" readonly></td>
            <td><input type="text" id="G0" class="monto" readonly></td>
            <td><input type="text" id="H0" class="monto" readonly></td>
        </tr>
        <tr>
            <td><input type="text" id="A1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['A1'] ?>"
                       <?= $disabled ?>></td>
            <td><input type="text" id="B1" class="monto" readonly></td>
            <td><input type="text" id="C1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['C1'] ?>"
                       <?= $disabled ?>></td>
            <td><input type="text" id="D1" class="monto" readonly></td>
            <td><input type="text" id="E1" class="monto" readonly></td>
            <td><input type="text" id="F1" class="monto" readonly></td>
            <td><input type="text" id="G1" class="monto" readonly></td>
            <td><input type="text" id="H1" class="monto" readonly></td>
        </tr>
        <tr>
            <td><input type="text" id="A2" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['A2'] ?>"
                       <?= $disabled ?>></td>
            <td><input type="text" id="B2" class="monto" readonly></td>
            <td><input type="text" id="C2" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['C2'] ?>"
                       <?= $disabled ?>></td>
            <td><input type="text" id="D2" class="monto" readonly></td>
            <td><input type="text" id="E2" class="monto" readonly></td>
            <td><input type="text" id="F2" class="monto" readonly></td>
            <td><input type="text" id="G2" class="monto" readonly></td>
            <td><input type="text" id="H2" class="monto" readonly></td>
        </tr>
        <tr>
            <td><input type="text" id="A3" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['A3'] ?>"
                       <?= $disabled ?>></td>
            <td><input type="text" id="B3" class="monto" readonly></td>
            <td><input type="text" id="C3" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['C3'] ?>"
                       <?= $disabled ?>></td>
            <td><input type="text" id="D3" class="monto" readonly></td>
            <td><input type="text" id="E3" class="monto" readonly></td>
            <td><input type="text" id="F3" class="monto" readonly></td>
            <td><input type="text" id="G3" class="monto" readonly></td>
            <td><input type="text" id="H3" class="monto" readonly></td>
        </tr>
        <tr>
            <td align="center">Suma:</td>
            <td><input type="text" id="B4" class="monto" readonly></td>
            <td><input type="text" id="C4" class="monto" readonly></td>
            <td><input type="text" id="D4" class="monto" readonly></td>
            <td><input type="text" id="E4" class="monto" readonly></td>
            <td><input type="text" id="F4" class="monto" readonly></td>
            <td><input type="text" id="G4" class="monto" readonly></td>
            <td><input type="text" id="H4" class="monto" readonly></td>
        </tr>
        <tr>
            <td align="center">Promedio:</td>
            <td><input type="text" id="B5" class="monto" readonly></td>
            <td><input type="text" id="C5" class="monto" readonly></td>
            <td colspan="5"></td>
        </tr>
        <tr>
            <td colspan="8">
                <hr/>
            </td>
        </tr>
        <tr align="center">
            <td colspan="2"></td>
            <td><strong>Coeficiente corr, r:</strong></td>
            <td id="_r"></td>
            <td><strong>Pendiente, m:</strong></td>
            <td id="_m"></td>
            <td><strong>Intercepto, b:</strong></td>
            <td id="_b"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="6">Datos de an&aacute;lisis</td>
        </tr>
        <tr align="center">
            <td><strong>xi</strong></td>
            <td><strong>xi2</strong></td>
            <td><strong>yi</strong></td>
            <td><strong>Yi</strong></td>
            <td><strong>yi-Yi</strong></td>
            <td><strong>(yi-Yi)2</strong></td>
        </tr>
        <tr align="center">
            <td id="M0"></td>
            <td id="N0"></td>
            <td id="R0"></td>
            <td id="O0"></td>
            <td id="P0"></td>
            <td id="Q0"></td>
        </tr>
        <tr align="center">
            <td id="M1"></td>
            <td id="N1"></td>
            <td id="R1"></td>
            <td id="O1"></td>
            <td id="P1"></td>
            <td id="Q1"></td>
        </tr>
        <tr align="center">
            <td id="M2"></td>
            <td id="N2"></td>
            <td id="R2"></td>
            <td id="O2"></td>
            <td id="P2"></td>
            <td id="Q2"></td>
        </tr>
        <tr align="center">
            <td id="M3"></td>
            <td id="N3"></td>
            <td id="R3"></td>
            <td id="O3"></td>
            <td id="P3"></td>
            <td id="Q3"></td>
        </tr>
        <tr align="center">
            <td><strong>Suma:</strong></td>
            <td id="N4"></td>
            <td></td>
            <td></td>
            <td><strong>Suma:</strong></td>
            <td id="Q4"></td>
        </tr>
        <tr>
            <td colspan="6">
                <hr/>
            </td>
        </tr>
        <tr align="center">
            <td><strong>Sy/x</strong></td>
            <td><strong>Sm</strong></td>
            <td><strong>Sb</strong></td>
            <td><strong>L&iacute;mite de detecci&oacute;n, LDD</strong></td>
            <td><strong>L&iacute;mite de cuantificaci&oacute;n, LDC</strong></td>
            <td><strong>r > 0,99</strong></td>
        </tr>
        <tr align="center">
            <td id="sxy"></td>
            <td id="sm"></td>
            <td id="sb"></td>
            <td id="ldd"></td>
            <td id="ldc"></td>
            <td id="cumple"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <?php
        if ($_GET['acc'] == 'I') {
            ?>
            <tr>
                <td class="titulo">Gr&aacute;fico</td>
            </tr>
            <tr>
                <td><input type="button" id="btn" value="Generar" class="boton" onClick="Generar()"></td>
            </tr>
            <?php
        }
        ?>
        <tr style="display:none;">
            <td id="td_tabla"></td>
        </tr>
        <tr align="center" id="tr_grafico" style="display:none;">
            <td>
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </td>
        </tr>
    </table>
    <br/>
    <?php
    if ($_GET['acc'] == 'M') {
        $ROW = $Gestor->Historial();
        echo '<table width="98%" class="radius" style="font-size:12px"></tr>';
        for ($x = 0; $x < count($ROW); $x++) {
            $ROW[$x]['accion'] = $Gestor->Accion($ROW[$x]['accion']);
            echo "<tr>
			<td><strong>{$ROW[$x]['accion']}</strong> {$ROW[$x]['mbre']} {$ROW[$x]['ap1']}, {$ROW[$x]['fecha1']}</td>
			<td>____________________</td>
		</tr>";
        }
        echo '</table><br />';
        ?>
        <?php if ($estado == '0') { ?>
    <input type="button" value="Procesar" class="boton" onClick="Procesar()">&nbsp;
    <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
    <?php } ?>
    <?php if ($estado == '1' and $Gestor->Revisar()){ ?>
    <input type="button" value="Revisar" class="boton" onClick="Revisar()"
           title="Enviar notificación de aprobación al superior">&nbsp;
    <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
    <?php } ?>
    <?php if ($estado == '3' and $Gestor->Aprobar()){ ?>
    <input type="button" value="Aprobar" class="boton" onClick="Aprobar()">&nbsp;
    <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
    <?php } ?>
        <script>__calcula();
            Generar();</script>
    <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php
    }else{
    ?>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('Q0000', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>