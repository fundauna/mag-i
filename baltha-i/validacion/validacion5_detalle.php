<?php
define('__MODULO__', 'validacion');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _validacion5_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
$estado = $ROW[0]['estado'];

if ($_GET['acc'] == 'M') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="cs" value="<?= $ROW[0]['cs'] ?>"/>
<center>
    <?php $Gestor->Incluir('q4', 'hr', 'Validación de métodos :: Hoja de c&aacute;lculo para validaciones') ?>
    <?= $Gestor->Encabezado('Q0004', 'e', 'Hoja de c&aacute;lculo para validaciones') ?>
    <br>
    <?php if ($_GET['acc'] == 'I') { ?>
        <table class="radius" style="font-size:12px" width="98%">
            <tr>
                <td class="titulo">Cargar desde un archivo de excel</td>
            </tr>
            <tr>
                <td>
                    <form name="form" method="post" enctype="multipart/form-data"
                          action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
                        <input type="hidden" name="_AJAX" value="1"/>
                        <input type="hidden" name="paso" value="2"/>
                        <input type="hidden" name="ruta"
                               value="<?= '../../baltha-i/' . __MODULO__ . '/' . basename(__FILE__) ?>"/>
                        <input type="file" name="archivo" id="archivo">
                        <input type="button" value="Cargar" class="boton" onclick="Cargar();">&nbsp;
                        <img onclick="DocumentosZip('validacion_LRE', '<?= __MODULO__ ?>')"
                             src="<?= $Gestor->Incluir('olvido', 'bkg') ?>" border="0" width="20" height="20"
                             title="Decargar plantilla" style="vertical-align:middle; cursor:pointer"/></a>
                    </form>
                </td>
            </tr>
        </table>
        <?php exit;
    } ?>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="6">Datos del an&aacute;lisis</td>
        </tr>
        <tr>
            <td><strong>Fecha de elaboraci&oacute;n:</strong></td>
            <td><input type="text" id="elaboracion" name="elaboracion" class="fecha"
                       value="<?= $ROW[0]['elaboracion'] ?>" readonly onClick="show_calendar(this.id);"
                       <?= $disabled ?>></td>
            <td><strong>No. de validaci&oacute;n asociada:</strong></td>
            <td><input type="text" id="asociada" maxlength="20" value="<?= $ROW[0]['asociada'] ?>" <?= $disabled ?>/>
            </td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td><strong>Matriz:</strong></td>
            <td>
                <input type="text" id="nommatriz0" value="<?= $ROW[0]['nommatriz'] ?>" class="lista" readonly
                       onclick="MatrizLista(0)" <?= $disabled ?>>
                <input type="hidden" id="matriz0" value="<?= $ROW[0]['matriz'] ?>">
            </td>
            <td><strong>Nivel bajo:</strong></td>
            <td><input type="text" id="bajo" name="bajo" class="monto2" onblur="Redondear(this)"
                       value="<?= $ROW[0]['bajo'] ?>" <?= $disabled ?>></td>
            <td><strong>Nivel medio:</strong></td>
            <td colspan="3"><input type="text" id="medio" name="medio" class="monto2" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['medio'] ?>" <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Observaciones:</strong></td>
            <td colspan="5"><textarea id="obs" <?= $disabled ?> maxlength="250"
                                      title="Alfanumérico (0-250)"><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
    </table>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr align="center">
            <td><strong>Nombre de la muestra</strong></td>
            <td><strong>Nombre del analito</strong></td>
            <td><strong>&Aacute;rea del pico</strong></td>
            <td><strong>Concentraci&oacute;n<br/>te&oacute;rica<br/>(mg/kg)</strong></td>
            <td><strong>Concentraci&oacute;n<br/>experimental<br/>(mg/kg)</strong></td>
        </tr>
        <?php
        if ($_GET['acc'] == 'R') {
            $ROW2 = $Gestor->Importacion();
            $oculto1 = 'style="display:none"'; //se ocultan los valores para impresion
            $oculto2 = ''; //se ocultan los campos de texto
        } else {
            $ROW2 = $Gestor->Lineas();
            $oculto1 = ''; //se ocultan los valores para impresion
            $oculto2 = 'style="display:none"'; //se ocultan los campos de texto
        }

        for ($x = 0, $i = 0, $tabla = 0; $x < count($ROW2); $x++, $i++) {
            if ($i > 18) {
                $i = 0;
                echo '<tr><td colspan="5"><hr /></td></tr>';
                $tabla++;
            }

            ?>
            <tr>
                <td><?= $Gestor->Labels($i) ?></td>
                <td align="center" id="ana<?= $x ?>"><input type="hidden" name="ana"
                                                            value="<?= $ROW2[$x]['ana'] ?>"/><?= $ROW2[$x]['ana'] ?>
                </td>
                <td align="center"><font <?= $oculto1 ?>><?= $Gestor->Formato($ROW2[$x]['pic']) ?></font><input
                        <?= $oculto2 ?> type="text" id="tabla<?= $tabla ?>_pic<?= $i ?>" name="pic" class="monto"
                        onblur="Redondear(this, '<?= $tabla ?>')" value="<?= $ROW2[$x]['pic'] ?>" <?= $disabled ?>></td>
                <td align="center"><font <?= $oculto1 ?>><?= $Gestor->Formato($ROW2[$x]['teo']) ?></font><input
                        <?= $oculto2 ?> type="text" id="tabla<?= $tabla ?>_teo<?= $i ?>" name="teo" class="monto"
                        onblur="Redondear(this, '<?= $tabla ?>')"
                        value="<?= $ROW2[$x]['teo'] ?>" <?php if ($ROW2[$x]['teo'] == '-1') echo ' style="display:none"'; ?>
                        <?= $disabled ?>></td>
                <td align="center"><font <?= $oculto1 ?>><?= $Gestor->Formato($ROW2[$x]['exp']) ?></font><input
                        <?= $oculto2 ?> type="text" id="tabla<?= $tabla ?>_exp<?= $i ?>" name="exp" class="monto"
                        onblur="Redondear(this, '<?= $tabla ?>')" value="<?= $ROW2[$x]['exp'] ?>" <?= $disabled ?>></td>
            </tr>
        <?php
        //TIRA TABLA DE RESULTADOS
        if ($i == 18){
        ?>
            <tr>
                <td colspan="5">
                    <table class='radius' width="98%">
                        <tr align="center">
                            <td colspan="5" id="tabla<?= $tabla ?>_cumple_1"><strong>Linealidad</strong><br/>Criterio:
                                Residual &le; 20 %
                            </td>
                            <td colspan="3" id="tabla<?= $tabla ?>_cumple_2"><strong>Nivel Bajo</strong></td>
                            <td colspan="3" id="tabla<?= $tabla ?>_cumple_3"><strong>Nivel Medio</strong></td>
                            <td rowspan="2" id="tabla<?= $tabla ?>_cumple_4"><strong>Selectividad</strong><br/>Criterio:<br/>&le;
                                30 % del L.C.
                            </td>
                            <td rowspan="2" id="tabla<?= $tabla ?>_cumple_5"><strong>Efecto matriz</strong><br/>Criterio:<br/>&le;
                                20 %
                            </td>
                        </tr>
                        <tr align="center">
                            <td><strong>P1</strong></td>
                            <td><strong>P2</strong></td>
                            <td><strong>P3</strong></td>
                            <td><strong>P4</strong></td>
                            <td><strong>P5</strong></td>
                            <td colspan="2"><strong>Veracidad</strong><br/>Criterio:<br/>En el intervalo (70-120) %</td>
                            <td><strong>Precisi&oacute;n</strong><br/>Criterio:<br/>RSDr % &le; 20 %</td>
                            <td colspan="2"><strong>Veracidad</strong><br/>Criterio:<br/>En el intervalo (70-120) %</td>
                            <td><strong>Precisi&oacute;n</strong><br/>Criterio:<br/>RSDr % &le; 20 %</td>
                        </tr>
                        <tr align="center">
                            <td id="tabla<?= $tabla ?>_P1"></td>
                            <td id="tabla<?= $tabla ?>_P2"></td>
                            <td id="tabla<?= $tabla ?>_P3"></td>
                            <td id="tabla<?= $tabla ?>_P4"></td>
                            <td id="tabla<?= $tabla ?>_P5"></td>
                            <td id="tabla<?= $tabla ?>_bajo1"></td>
                            <td id="tabla<?= $tabla ?>_bajo2"></td>
                            <td id="tabla<?= $tabla ?>_bajo3"></td>
                            <td id="tabla<?= $tabla ?>_alto1"></td>
                            <td id="tabla<?= $tabla ?>_alto2"></td>
                            <td id="tabla<?= $tabla ?>_alto3"></td>
                            <td id="tabla<?= $tabla ?>_final"></td>
                            <td id="tabla<?= $tabla ?>_final2"></td>
                        </tr>
                    </table>
                    <!-- RESULTADOS -->
                    <input type="hidden" name="analito" value="<?= $ROW2[$x]['ana'] ?>"/>
                    <input type="hidden" id="res<?= $tabla ?>_P1" name="P1"/>
                    <input type="hidden" id="res<?= $tabla ?>_P2" name="P2"/>
                    <input type="hidden" id="res<?= $tabla ?>_P3" name="P3"/>
                    <input type="hidden" id="res<?= $tabla ?>_P4" name="P4"/>
                    <input type="hidden" id="res<?= $tabla ?>_P5" name="P5"/>
                    <input type="hidden" id="res<?= $tabla ?>_bajo1" name="bajo1"/>
                    <input type="hidden" id="res<?= $tabla ?>_bajo2" name="bajo2"/>
                    <input type="hidden" id="res<?= $tabla ?>_bajo3" name="bajo3"/>
                    <input type="hidden" id="res<?= $tabla ?>_alto1" name="alto1"/>
                    <input type="hidden" id="res<?= $tabla ?>_alto2" name="alto2"/>
                    <input type="hidden" id="res<?= $tabla ?>_alto3" name="alto3"/>
                    <input type="hidden" id="res<?= $tabla ?>_final" name="final"/>
                    <input type="hidden" id="res<?= $tabla ?>_final2" name="final2"/>
                    <input type="hidden" id="cumple<?= $tabla ?>" name="cumple"/>
                </td>
            </tr>
            <script>__calcula('<?=$tabla?>')</script>
            <?php
        }//IF
        } //FOR
        ?>
    </table>
    <br/>
    <?php
    if ($_GET['acc'] == 'M') {
        $ROW = $Gestor->Historial();
        echo '<table width="98%" class="radius" style="font-size:12px"></tr>';
        for ($x = 0; $x < count($ROW); $x++) {
            $ROW[$x]['accion'] = $Gestor->Accion($ROW[$x]['accion']);
            echo "<tr>
			<td><strong>{$ROW[$x]['accion']}</strong> {$ROW[$x]['nombre']} {$ROW[$x]['ap1']}, {$ROW[$x]['fecha1']}</td>
			<td>____________________</td>
		</tr>";
        }
        echo '</table><br />';
        ?>
        <?php if ($estado == '0') { ?>
            <input type="button" value="Procesar" class="boton" onClick="Procesar()">&nbsp;
            <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
        <?php } ?>
        <?php if ($estado == '1' and $Gestor->Revisar()) { ?>
            <input type="button" value="Revisar" class="boton" onClick="Revisar()"
                   title="Enviar notificación de aprobación al superior">&nbsp;
            <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
        <?php } ?>
        <?php if ($estado == '3' and $Gestor->Aprobar()) { ?>
            <input type="button" value="Aprobar" class="boton" onClick="Aprobar()">&nbsp;
            <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
        <?php } ?>
        <input type="button" value="Imprimir" class="boton" onClick="window.print()">
        <?php
    } elseif ($_GET['acc'] == 'R') {
        $Gestor->LimpiaTabla();
        ?>
        <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
        <?php
    }
    ?>
</center>
<?= $Gestor->Encabezado('Q0004', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>