<?php
define('__MODULO__', 'validacion');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _validacion4_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

if ($_GET['acc'] == 'M') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('q3', 'hr', 'Validaci�n de m�todos :: Biolog�a Molecular') ?>
<?= $Gestor->Encabezado('Q0003', 'e', 'Biolog�a Molecular') ?>
<center>
    <form name="form" id="form" method="post" enctype="multipart/form-data"
          action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
        <input type="hidden" name="token" id="token" value="1"/>
        <input type="hidden" name="id" id="id" value="<?= $_GET['ID'] ?>"/>
        <input type="hidden" name="acc" id="acc" value="<?= $_GET['acc'] ?>"/>
        <table class="radius" style="font-size:12px;" width="300px">
            <tr>
                <td class="titulo" colspan="2">Datos Generales</td>
            </tr>
            <?php if ($_GET['acc'] == 'M') { ?>
                <tr>
                    <td>No. de Validaci&oacute;n:</td>
                    <td><?= $ROW[0]['cs'] ?></td>
                </tr>
                <tr>
                    <td>Fecha:</td>
                    <td><?= $ROW[0]['fecha'] ?></td>
                </tr>
                <tr>
                    <td>Generado por:</td>
                    <td><?= $ROW[0]['analista'] ?></td>
                </tr>
            <?php } ?>
            <tr>
                <td>Procedimiento:</td>
                <td><input type="text" name="proce" id="proce" maxlength="20" <?= $disabled ?>
                           value="<?= $ROW[0]['proce'] ?>"></td>
            </tr>
            <tr>
                <td>Nombre del m&eacute;todo:</td>
                <td><textarea name="nombre" id="nombre" <?= $disabled ?>><?= $ROW[0]['nombre'] ?></textarea></td>
            </tr>
            <?php if ($_GET['acc'] == 'M') { ?>
                <tr>
                    <td>Documento adjunto:</td>
                    <td><img onclick="DocumentosZip('<?= $ROW[0]['cs'] ?>', '<?= __MODULO__ ?>')"
                             src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Documento adjunto" class="tab2"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" id="aaa" style="display:none"><input type="file" name="archivo" id="archivo"></td>
                </tr>
            <?php } ?>
            <?php if ($_GET['acc'] == 'I') { ?>
                <tr>
                    <td colspan="2">
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><input type="file" name="archivo" id="archivo"></td>
                </tr>
            <?php } ?>
        </table>
        <br/>
        <?php if ($_GET['acc'] == 'I') { ?>
            <input type="button" id="btn" value="Aceptar" class="boton" onclick="Valida(this)">
        <?php } ?>
        <?php if ($_GET['acc'] == 'M') { ?>
            <input type="button" id="btn" value="Modificar" class="boton" onclick="Activa()">&nbsp;&nbsp;&nbsp;
            <input type="button" id="btn" value="Eliminar" class="boton" onclick="Elimina()">
        <?php } ?>
    </form>
</center>
<?= $Gestor->Encabezado('Q0003', 'p', '') ?>
</body>
</html>