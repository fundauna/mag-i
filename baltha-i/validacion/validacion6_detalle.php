<?php
define('__MODULO__', 'validacion');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _validacion6_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

$estado = $ROW[0]['estado'];

if ($estado == '' or $estado == '0') {
    $readonly = '';
} else {
    $readonly = 'disabled';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<center>
    <?php $Gestor->Incluir('q5', 'hr', 'Validación de métodos :: Informe de validaci&oacute;n para an&aacute;lisis multiresiduos de plaguicidas') ?>
    <?= $Gestor->Encabezado('Q0005', 'e', 'Informe de validaci&oacute;n para an&aacute;lisis multiresiduos de plaguicidas') ?>

    <br/>
    <input type="hidden" id="cs" value="<?= $ROW[0]['cs'] ?>"/>
    <input type="hidden" id="equipo" name="equipo" value="<?= $ROW[0]['equipo'] ?>"/>
    <!-- TABLA DE DATOS -->
    <table id="pagina1" class="radius" border="1" style="font-size:12px" width="98%">
        <tr>
            <td align="right" colspan="2"><strong>No. de validaci&oacute;n:</strong></td>
            <td colspan="4"><input type="text" id="validacion" maxlength="13" size="13"
                                   value="<?= $ROW[0]['validacion'] ?>" onblur="ValidaAprobada()" <?= $readonly ?>></td>
        </tr>
        <tr>
            <td class="titulo" colspan="6">Objetivo de la validaci&oacute;n</td>
        </tr>
        <tr>
            <td colspan="6"><textarea id="objetivo" style="width:98%; height:32px" maxlength="500"
                                      <?= $readonly ?>><?= $ROW[0]['objetivo'] ?></textarea></td>
        </tr>
        <tr>
            <td class="titulo" colspan="6">Alcance de la validaci&oacute;n</td>
        </tr>
        <tr align="center">
            <td><strong>Matriz:</strong></td>
            <td><?= $ROW[0]['matriz'] ?></td>
            <td><strong>Analitos:</strong></td>
            <td><input type="text" id="analitos" value="<?= $ROW[0]['analitos'] ?>" maxlength="50" <?= $readonly ?>/>
            </td>
            <td><strong>&Aacute;mbito:</strong></td>
            <td><input type="text" id="ambito" value="<?= $ROW[0]['ambito'] ?>" maxlength="50" <?= $readonly ?>/></td>
        </tr>
        <tr>
            <td class="titulo" colspan="6">Metodolog&iacute;a, equipos, patrones y reactivos</td>
        </tr>
        <tr align="center" style="vertical-align:top">
            <td colspan="2"><strong>Tipo de m&eacute;todo utilizado:</strong></td>
            <td colspan="2"><strong>Par&aacute;metros de desempe&ntilde;o y criterio de aceptaci&oacute;n:</strong></td>
            <td colspan="2" rowspan="2"><strong>Referencias utilizadas para calcular<br/>los par&aacute;metros de
                    desempe&ntilde;o<br/>y sus respectivos criterios de aceptaci&oacute;n:</strong></td>
        </tr>
        <tr align="center">
            <td colspan="2"><textarea id="tipo_metodo" style="width:98%; height:32px" maxlength="100"
                                      <?= $readonly ?>><?= $ROW[0]['tipo_metodo'] ?></textarea></td>
            <td><strong>Nombre del par&aacute;metro</strong></td>
            <td><strong>Criterio de aceptaci&oacute;n</strong></td>
        </tr>
        <tr align="center">
            <td colspan="2"><strong>C&oacute;digo y nombre del m&eacute;todo utilizado:</strong></td>
            <td>Precisi&oacute;n</td>
            <td><input type="text" id="precision" value="<?= $ROW[0]['precision'] ?>" maxlength="20" <?= $readonly ?>/>
            </td>
            <td colspan="2" rowspan="2"><textarea id="ref" style="width:98%; height:32px" maxlength="500"
                                                  <?= $readonly ?>><?= $ROW[0]['ref'] ?></textarea></td>
        </tr>
        <tr align="center">
            <td colspan="2"><textarea id="cod_metodo" style="width:98%; height:32px" maxlength="500"
                                      <?= $readonly ?>><?= $ROW[0]['cod_metodo'] ?></textarea></td>
            <td>Veracidad</td>
            <td><input type="text" id="veracidad" value="<?= $ROW[0]['veracidad'] ?>" maxlength="50" <?= $readonly ?>/>
            </td>
        </tr>
        <tr align="center">
            <td colspan="2"><strong>Modificaciones con respecto al m&eacute;todo utilizado:</strong></td>
            <td>L&iacute;mite de detecci&oacute;n (LD)</td>
            <td><input type="text" id="limite1" value="<?= $ROW[0]['limite1'] ?>" maxlength="20" <?= $readonly ?>/></td>
            <td colspan="2"><strong>Trazabilidad</strong></td>
        </tr>
        <tr align="center">
            <td colspan="2"><textarea id="modif" style="width:98%; height:32px" maxlength="500"
                                      <?= $readonly ?>><?= $ROW[0]['modif'] ?></textarea></td>
            <td>L&iacute;mite de cuantificaci&oacute;n (LC)</td>
            <td><input type="text" id="limite2" value="<?= $ROW[0]['limite2'] ?>" maxlength="20" <?= $readonly ?>/></td>
            <td align="right"><strong>C&oacute;digo Bit&aacute;cora:<br/>P&aacute;gina:</strong></td>
            <td><input type="text" id="bitacora" value="<?= $ROW[0]['bitacora'] ?>" size="10" maxlength="20"
                       <?= $readonly ?>/><br/>
                <input type="text" id="pagina" value="<?= $ROW[0]['pagina'] ?>" size="5" maxlength="10"
                       <?= $readonly ?>/>
            </td>
        </tr>
        <tr align="center">
            <td colspan="2"><strong>T&eacute;cnica de an&aacute;lisis:</strong></td>
            <td>Linealidad</td>
            <td><input type="text" id="linealidad" value="<?= $ROW[0]['linealidad'] ?>" maxlength="20"
                       <?= $readonly ?>/></td>
            <td><strong>C&oacute;digo equipo utilizado:</strong></td>
            <td><input type="text" id="tmp_equipo" class="lista" value="<?= $ROW[0]['tmp_equipo'] ?>" readonly
                       onclick="EquiposLista()" <?= $readonly ?>/></td>
        </tr>
        <tr align="center">
            <td colspan="2"><textarea id="tecnica" style="width:98%; height:32px" maxlength="500"
                                      <?= $readonly ?>><?= $ROW[0]['tecnica'] ?></textarea></td>
            <td>Especificidad</td>
            <td><input type="text" id="especificidad" value="<?= $ROW[0]['especificidad'] ?>" maxlength="20"
                       <?= $readonly ?>/></td>
            <td rowspan="4" align="right"><strong>MRC<br/>Reactivos<br/>Condiciones ambientales:<br/>Otros:</strong>
            </td>
            <td rowspan="4"><textarea id="otros" style="width:95%; height:70px" maxlength="500"
                                      <?= $readonly ?>><?= $ROW[0]['otros'] ?></textarea></td>
        </tr>
        <tr align="center">
            <td colspan="2"><strong>C&oacute;digo y nombre del procedimiento interno utilizado:</strong></td>
            <td>Efecto matriz</td>
            <td><input type="text" id="efecto" value="<?= $ROW[0]['efecto'] ?>" maxlength="20" <?= $readonly ?>/></td>
        </tr>
        <tr align="center">
            <td colspan="2" rowspan="2"><textarea id="interno" style="width:98%; height:32px" maxlength="500"
                                                  <?= $readonly ?>><?= $ROW[0]['interno'] ?></textarea></td>
            <td>Robustez</td>
            <td><input type="text" id="robustez" value="<?= $ROW[0]['robustez'] ?>" maxlength="50" <?= $readonly ?>/>
            </td>
        </tr>
        <tr align="center">
            <td>Incertidumbre</td>
            <td><input type="text" id="inc" value="<?= $ROW[0]['inc'] ?>" maxlength="20" <?= $readonly ?>/></td>
        </tr>
        <tr>
            <td class="titulo" colspan="6">RESULTADOS Y CONCLUSIONES</td>
        </tr>
        <tr align="center">
            <td><strong>Resumen de resultados:</strong></td>
            <td><input type="text" id="resumen" value="<?= $ROW[0]['resumen'] ?>" maxlength="50" <?= $readonly ?>/></td>
            <td><strong>Tratamiento estad&iacute;stico de resultados:</strong></td>
            <td colspan="3"><textarea id="tratamiento" style="width:98%; height:32px" maxlength="500"
                                      <?= $readonly ?>><?= $ROW[0]['tratamiento'] ?></textarea></td>
        </tr>
        <tr>
            <td class="titulo" colspan="6">Declaraci&oacute;n de la conformidad del m&eacute;todo:</td>
        </tr>
        <tr align="center">
            <td colspan="6"><textarea id="declaracion" style="width:98%; height:32px" maxlength="500"
                                      <?= $readonly ?>><?= $ROW[0]['declaracion'] ?></textarea></td>
        </tr>
    </table>
    <?php
    if ($_GET['acc'] == 'M') {
        ?>
        <!-- TABLA DE RESULTADOS-->
        <table id="pagina2" class="radius" border="1" style="font-size:12px; display:none" width="98%">
            <tr align="center">
                <td class="titulo" colspan="15">RESUMEN DE RESULTADOS DE LOS PAR&Aacute;METROS DE DESEMPE&Ntilde;O</td>
            </tr>
            <tr align="center">
                <td colspan="15"><strong>MATRIZ:</strong> <?= $ROW[0]['matriz'] ?></td>
            </tr>
            <tr>
                <td colspan="10"><strong>Incertidumbre:</strong> <?= $ROW[0]['inc'] ?></td>
                <td colspan="5"><strong>Robustez:</strong> <?= $ROW[0]['robustez'] ?></td>
            </tr>
            <tr align="center">
                <td rowspan="2"><strong>Analito</strong></td>
                <td colspan="5"><strong>Linealidad</strong><br/>Criterio: Residual &le; &plusmn; 20 %</td>
                <td colspan="2"><strong>L&iacute;mites (mg/kg)</td>
                <td rowspan="2"><strong>Especificidad</strong><br>Criterio:<br>&le; 30 % del L.C.</td>
                <td rowspan="2"><strong>Efecto matriz</strong><br>Criterio:<br>&le; 20 %</td>
                <td colspan="2"><strong>Nivel Bajo (mg/kg)</strong></td>
                <td colspan="2"><strong>Nivel Medio (mg/kg)</strong></td>
                <td rowspan="2"><strong>Conclusi&oacute;n de<br/>la evaluaci&oacute;n</strong></td>
            </tr>
            <tr align="center">
                <td><strong>P1</strong></td>
                <td><strong>P2</strong></td>
                <td><strong>P3</strong></td>
                <td><strong>P4</strong></td>
                <td><strong>P5</strong></td>
                <td><strong>Detecci&oacute;n<br/>Criterio:<br/>3/10 LC</strong></td>
                <td><strong>Cuantificaci&oacute;n<br/>Criterio: = 0,01<br/>mg/kg</strong></td>
                <td><strong>Veracidad</strong><br>Criterio:<br>En el intervalo<br>(70-120) %</td>
                <td><strong>Precisi&oacute;n</strong><br>Criterio:<br>RSDr% &le;20 %</td>
                <td><strong>Veracidad</strong><br>Criterio:<br>En el intervalo<br>(70-120) %</td>
                <td><strong>Precisi&oacute;n</strong><br>Criterio:<br>RSDr% &le;20 %</td>
            </tr>
            <?php
            $ROW = $Gestor->Detalle($ROW[0]['validacion']);
            for ($x = 0; $x < count($ROW); $x++) {
                if ($ROW[$x]['cumple'] == '1') $ROW[$x]['cumple'] = 'Conforme';
                else $ROW[$x]['cumple'] = 'No conforme';
                ?>
                <tr align='center'>
                    <td><?= $ROW[$x]['analito'] ?></td>
                    <td><?= $ROW[$x]['P1'] ?></td>
                    <td><?= $ROW[$x]['P2'] ?></td>
                    <td><?= $ROW[$x]['P3'] ?></td>
                    <td><?= $ROW[$x]['P4'] ?></td>
                    <td><?= $ROW[$x]['P5'] ?></td>
                    <td><?= $ROW[$x]['bajo'] ?></td>
                    <td><?= $ROW[$x]['medio'] ?></td>
                    <td><?= $ROW[$x]['final'] ?></td>
                    <td><?= $ROW[$x]['final2'] ?></td>
                    <td><?= $ROW[$x]['bajo2'] ?></td>
                    <td><?= $ROW[$x]['bajo3'] ?></td>
                    <td><?= $ROW[$x]['alto2'] ?></td>
                    <td><?= $ROW[$x]['alto3'] ?></td>
                    <td><?= $ROW[$x]['cumple'] ?></td>
                </tr>
            <?php }//FOR
            ?>
        </table>
    <?php }//IF ?>
    <br/>
    <?php
    if ($_GET['acc'] == 'M') {
        $ROW = $Gestor->Historial();
        echo '<table id="pagina3" width="98%" class="radius" style="font-size:12px"></tr>';
        for ($x = 0; $x < count($ROW); $x++) {
            $ROW[$x]['accion'] = $Gestor->Accion($ROW[$x]['accion']);
            echo "<tr>
			<td><strong>{$ROW[$x]['accion']}</strong> {$ROW[$x]['nombre']} {$ROW[$x]['ap1']}, {$ROW[$x]['fecha1']}</td>
			<td>____________________</td>
		</tr>";
        }
        echo '</table><br />';
    }
    ?>
    <?php if ($estado == '') { ?>
        <input type="button" value="Aceptar" class="boton2" onclick="Valida('I')"/>
    <?php } ?>
    <?php if ($estado == '0') { ?>
        <input type="button" value="Modificar" class="boton2" onclick="Valida('M')"/>&nbsp;
        <input type="button" value="Procesar" class="boton" onClick="Procesar()">&nbsp;
        <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
        <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php } ?>
    <?php if ($estado == '3') { ?>
        <input type="button" value="Aprobar" class="boton" onClick="Aprobar()">&nbsp;
        <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
    <?php } ?>
    <?php if ($estado == '2' or $estado == '5') { ?>
        <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('Q0005', 'p', '') ?>
</body>
</html>