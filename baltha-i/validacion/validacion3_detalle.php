<?php
define('__MODULO__', 'validacion');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _validacion3_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
$estado = $ROW[0]['estado'];

if ($_GET['acc'] == 'M') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="cumple"/>
<input type="hidden" id="equipo1"/>
<input type="hidden" id="equipo2"/>
<input type="hidden" id="equipo3"/>
<input type="hidden" id="columna1"/>
<input type="hidden" id="columna2"/>
<input type="hidden" id="columna3"/>
<input type="hidden" id="cs" value="<?= $ROW[0]['cs'] ?>"/>
<center>
    <?php $Gestor->Incluir('q2', 'hr', 'Validación de métodos :: Validaci&oacute;n e implementaci&oacute;n de m&eacute;todos: Precisi&oacute;n Intermedia') ?>
    <?= $Gestor->Encabezado('Q0002', 'e', 'Validaci&oacute;n e implementaci&oacute;n de m&eacute;todos: Precisi&oacute;n Intermedia') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Datos de Muestra</td>
        </tr>
        <tr>
            <td><strong>Fecha:</strong></td>
            <td><?= $ROW[0]['fecha'] ?></td>
            <td><strong>Volumen 1 Bal&oacute;n (mL):</strong></td>
            <td><input type="text" id="mue_vol1" class="monto2" value="<?= $ROW[0]['mue_vol1'] ?>" <?= $disabled ?>>
            </td>
        </tr>
        <tr>
            <td><strong>Referencia m&eacute;todo anal&iacute;tico:</strong></td>
            <td><input type="text" id="metodo" maxlength="30" value="<?= $ROW[0]['metodo'] ?>" <?= $disabled ?>/></td>
            <td><strong>Volumen 2 (mL):</strong></td>
            <td><input type="text" id="mue_vol2" class="monto2" value="<?= $ROW[0]['mue_vol2'] ?>" <?= $disabled ?>>
            </td>
        </tr>
        <tr>
            <td><strong>M&eacute;todo anal&iacute;tico:</strong></td>
            <td><input type="text" id="analitico" maxlength="30" value="<?= $ROW[0]['analitico'] ?>" <?= $disabled ?>/>
            </td>
            <td><strong>Al&iacute;cuota en volumen 2 (mL):</strong></td>
            <td><input type="text" id="mue_ali2" class="monto2" value="<?= $ROW[0]['mue_ali2'] ?>" <?= $disabled ?>>
            </td>
        </tr>
        <tr>
            <td><strong>Tipo de formulaci&oacute;n:</strong></td>
            <td><input type="text" id="formulado" maxlength="30" value="<?= $ROW[0]['formulado'] ?>" <?= $disabled ?>/>
            </td>
            <td><strong>Concentraci&oacute;n declarada:</strong></td>
            <td><input type="text" id="declarada" class="monto2" value="<?= $ROW[0]['declarada'] ?>" <?= $disabled ?>>&nbsp;
                <select id="unidad" onchange="__calcula();" <?= $disabled ?>>
                    <option value="0">%m/m</option>
                    <option value="1" <?php if ($ROW[0]['unidad'] == '1') echo 'selected'; ?>>%m/v</option>
                </select></td>
        </tr>
        <tr>
            <td><strong>Ingrediente activo:</strong></td>
            <td><input type="text" id="IA" maxlength="30" value="<?= $ROW[0]['IA'] ?>" <?= $disabled ?>/></td>
            <td><strong>Densidad (g/mL):</strong></td>
            <td><input type="text" id="densidad" class="monto2" value="<?= $ROW[0]['densidad'] ?>" <?= $disabled ?>>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="6">Datos del an&aacute;lisis</td>
        </tr>
        <tr>
            <td colspan="2" align="center"><strong>Datos del est&aacute;ndar serie 1</strong></td>
            <td colspan="2" align="center"><strong>Datos del est&aacute;ndar serie 2</strong></td>
            <td colspan="2" align="center"><strong>Datos del est&aacute;ndar serie 3</strong></td>
        </tr>
        <tr>
            <td><strong>Est&aacute;ndar, masa (mg):</strong></td>
            <td><input type="text" id="masa" class="monto2" value="<?= $ROW[0]['masa'] ?>" onblur="calcular()"
                       <?= $disabled ?>></td>
            <td><strong>Est&aacute;ndar, masa (mg):</strong></td>
            <td><input type="text" id="masa1" class="monto2" value="<?= $ROW[0]['masa1'] ?>" onblur="calcular()"
                       <?= $disabled ?>></td>
            <td><strong>Est&aacute;ndar, masa (mg):</strong></td>
            <td><input type="text" id="masa2" class="monto2" value="<?= $ROW[0]['masa2'] ?>" onblur="calcular()"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Pureza (%):</strong></td>
            <td><input type="text" id="pureza" class="monto2" value="<?= $ROW[0]['pureza'] ?>" onblur="calcular()"
                       <?= $disabled ?>></td>
            <td><strong>Pureza (%):</strong></td>
            <td><input type="text" id="pureza1" class="monto2" value="<?= $ROW[0]['pureza1'] ?>" onblur="calcular()"
                       <?= $disabled ?>></td>
            <td><strong>Pureza (%):</strong></td>
            <td><input type="text" id="pureza2" class="monto2" value="<?= $ROW[0]['pureza2'] ?>" onblur="calcular()"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Equipo:</strong></td>
            <td><input type="text" id="tmp_equipo1" class="lista" value="<?= $ROW[0]['tmp_equipo1'] ?>" readonly
                       onclick="EquiposLista(1)" <?= $disabled ?>/></td>
            <td><strong>Equipo:</strong></td>
            <td><input type="text" id="tmp_equipo2" class="lista" value="<?= $ROW[0]['tmp_equipo2'] ?>" readonly
                       onclick="EquiposLista(2)" <?= $disabled ?>/></td>
            <td><strong>Equipo:</strong></td>
            <td><input type="text" id="tmp_equipo3" class="lista" value="<?= $ROW[0]['tmp_equipo3'] ?>" readonly
                       onclick="EquiposLista(3)" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>Columna:</strong></td>
            <td><input type="text" id="tmp_columna1" class="lista" value="<?= $ROW[0]['tmp_columna1'] ?>" readonly
                       onclick="ColumnaLista(1)" <?= $disabled ?>/></td>
            <td><strong>Columna:</strong></td>
            <td><input type="text" id="tmp_columna2" class="lista" value="<?= $ROW[0]['tmp_columna2'] ?>" readonly
                       onclick="ColumnaLista(2)" <?= $disabled ?>/></td>
            <td><strong>Columna:</strong></td>
            <td><input type="text" id="tmp_columna3" class="lista" value="<?= $ROW[0]['tmp_columna3'] ?>" readonly
                       onclick="ColumnaLista(3)" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>Volumen 1 Bal&oacute;n (mL):</strong></td>
            <td><input type="text" id="vol1" class="monto2" value="<?= $ROW[0]['vol1'] ?>" onblur="calcular()"
                       <?= $disabled ?>/></td>
            <td><strong>Volumen 1 Bal&oacute;n (mL):</strong></td>
            <td><input type="text" id="vol11" class="monto2" value="<?= $ROW[0]['vol11'] ?>" onblur="calcular()"
                       <?= $disabled ?>/></td>
            <td><strong>Volumen 1 Bal&oacute;n (mL):</strong></td>
            <td><input type="text" id="vol12" class="monto2" value="<?= $ROW[0]['vol12'] ?>" onblur="calcular()"
                       <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>Al&iacute;cuota en volumen 2 (mL):</strong></td>
            <td><input type="text" id="ali2" class="monto2" value="<?= $ROW[0]['ali2'] ?>" onblur="calcular()"
                       <?= $disabled ?>/></td>
            <td><strong>Al&iacute;cuota en volumen 2 (mL):</strong></td>
            <td><input type="text" id="ali21" class="monto2" value="<?= $ROW[0]['ali21'] ?>" onblur="calcular()"
                       <?= $disabled ?>/></td>
            <td><strong>Al&iacute;cuota en volumen 2 (mL):</strong></td>
            <td><input type="text" id="ali22" class="monto2" value="<?= $ROW[0]['ali22'] ?>" onblur="calcular()"
                       <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>Volumen 2 (mL):</strong></td>
            <td><input type="text" id="vol2" class="monto2" value="<?= $ROW[0]['vol2'] ?>" onblur="calcular()"
                       <?= $disabled ?>/></td>
            <td><strong>Volumen 2 (mL):</strong></td>
            <td><input type="text" id="vol21" class="monto2" value="<?= $ROW[0]['vol21'] ?>" onblur="calcular()"
                       <?= $disabled ?>/></td>
            <td><strong>Volumen 2 (mL):</strong></td>
            <td><input type="text" id="vol22" class="monto2" value="<?= $ROW[0]['vol22'] ?>" onblur="calcular()"
                       <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>Concentraci&oacute;n (mg/mL):</strong></td>
            <td id="conc"></td>
            <td><strong>Concentraci&oacute;n (mg/mL):</strong></td>
            <td id="conc1"></td>
            <td><strong>Concentraci&oacute;n (mg/mL):</strong></td>
            <td id="conc2"></td>
        </tr>
        <tr>
            <td><strong>Observaciones:</strong></td>
            <td colspan="5"><textarea id="obs" <?= $disabled ?> style="width:90%" maxlength="250"
                                      title="Alfanumérico (0-250)"><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
        <?php if ($_GET['acc'] == 'I') { ?>
            <tr>
                <td colspan="6">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td colspan="4"><strong>Cargar repetibilidad:</strong>&nbsp;
                    <input type="text" id="repe" maxlength="13" size="13"/>&nbsp;<input type="button" value="Aceptar"
                                                                                        class="boton"
                                                                                        onClick="Cargar()"></td>
            </tr>
        <?php } ?>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <?php
        for ($y = 1, $z = 0; $y < 4; $y++) {
            echo "<tr align='center'>
		<td><strong>Serie {$y}</strong></td>
		<td><strong>Masa (mg)</strong></td>
		<td colspan='3' align='center'><strong>Area<br />Estandar</strong></td>
		<td><strong>Promedio</strong></td>
		<td><strong>CV</strong></td>
		<td colspan='3' align='center'><strong>Area<br />Muestra</strong></td>
		<td><strong>Promedio</strong></td>
		<td><strong>CV</strong></td>
		<td><strong>% (m/m)</strong></td>
		<td><strong>% (m/v)</strong></td>
		<td align='center' title='Utilizar?' style='cursor:help'><strong>?</strong></td>
	</tr>";

            $ROW2 = $Gestor->ObtieneSet($y);
            for ($x = 0; $x < count($ROW2); $x++, $z++) {
                ?>
                <tr id="tr_usar<?= $z ?>">
                    <td><?= $z + 1 ?></td>
                    <td><input type="text" id="A<?= $z ?>" name="_A" class="monto2" onblur="Redondear(this)"
                               value="<?= $ROW2[$x]['A'] ?>" <?= $disabled ?>></td>
                    <td><input type="text" id="B<?= $z ?>" name="_B" class="monto2" onblur="Redondear(this)"
                               value="<?= $ROW2[$x]['B'] ?>" <?= $disabled ?>></td>
                    <td><input type="text" id="C<?= $z ?>" name="_C" class="monto2" onblur="Redondear(this)"
                               value="<?= $ROW2[$x]['C'] ?>" <?= $disabled ?>></td>
                    <td><input type="text" id="D<?= $z ?>" name="_D" class="monto2" onblur="Redondear(this)"
                               value="<?= $ROW2[$x]['D'] ?>" <?= $disabled ?>></td>
                    <td><input type="text" id="E<?= $z ?>" class="monto2" readonly></td>
                    <td><input type="text" id="F<?= $z ?>" class="monto2" readonly></td>
                    <td><input type="text" id="G<?= $z ?>" name="_G" class="monto2" onblur="Redondear(this)"
                               value="<?= $ROW2[$x]['G'] ?>" <?= $disabled ?>></td>
                    <td><input type="text" id="H<?= $z ?>" name="_H" class="monto2" onblur="Redondear(this)"
                               value="<?= $ROW2[$x]['H'] ?>" <?= $disabled ?>></td>
                    <td><input type="text" id="I<?= $z ?>" name="_I" class="monto2" onblur="Redondear(this)"
                               value="<?= $ROW2[$x]['I'] ?>" <?= $disabled ?>></td>
                    <td><input type="text" id="J<?= $z ?>" class="monto2" readonly title="0"></td>
                    <td><input type="text" id="K<?= $z ?>" class="monto2" readonly title="0"></td>
                    <td><input type="text" id="L<?= $z ?>" class="monto2" readonly title="0"></td>
                    <td><input type="text" id="M<?= $z ?>" class="monto2" readonly title="0"></td>
                    <td><input type="checkbox" id="X<?= $z ?>" name="X"
                               onclick="__calcula()" <?php if ($ROW2[$x]['X'] == '1') echo 'checked'; ?>
                               <?= $disabled ?>/></td>
                </tr>
                <?php
            }
        }
        ?>
        <tbody id="cuerpo"></tbody>
        <?php
        $ROW2 = $Gestor->ObtieneImportados();
        for ($x = 0; $x < count($ROW2); $x++) {
            ?>
            <tr>
                <td colspan='12' align='center'>-- Registros importados --</td>
                <td><input type='text' id='MM<?= $x ?>' name='MM' class='monto2' value='<?= $ROW2[$x]['MM'] ?>'
                           <?= $disabled ?>></td>
                <td><input type='text' id='MV<?= $x ?>' name='MV' class='monto2' value='<?= $ROW2[$x]['MV'] ?>'
                           <?= $disabled ?>></td>
                <td><input type='checkbox' id='XX<?= $x ?>'
                           name='XX' <?php if ($ROW2[$x]['XX'] == '1') echo 'checked'; ?> <?= $disabled ?>/></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="10">Resultados</td>
        </tr>
        <tr align="center">
            <td id="tr_tabla" style="font-weight:bold">T tabla</td>
            <td><strong>Promedio m/m</strong></td>
            <td><strong>Promedio m/v</strong></td>
            <td><strong>DE m/m</strong></td>
            <td><strong>DE m/v</strong></td>
            <td><strong>CV m/m</strong></td>
            <td><strong>CV m/v</strong></td>
            <td><strong>RRSD Horwitz</strong></td>
            <td><strong>&gt; RRSD LCC</strong></td>
            <td><strong>Cumple?</strong></td>
        </tr>
        <tr align="center">
            <td id="_tabla"></td>
            <td id="promMM"></td>
            <td id="promMV"></td>
            <td id="desvMM"></td>
            <td id="desvMV"></td>
            <td id="cvMM"></td>
            <td id="cvMV"></td>
            <td id="horwitz"></td>
            <td id="lcc"></td>
            <td id="_cumple"></td>
        </tr>
    </table>
    <br/>
    <?php
    if ($_GET['acc'] == 'M') {
        $ROW = $Gestor->Historial();
        echo '<table width="98%" class="radius" style="font-size:12px"></tr>';
        for ($x = 0; $x < count($ROW); $x++) {
            $ROW[$x]['accion'] = $Gestor->Accion($ROW[$x]['accion']);
            echo "<tr>
			<td><strong>{$ROW[$x]['accion']}</strong> {$ROW[$x]['nombre']} {$ROW[$x]['ap1']}, {$ROW[$x]['fecha1']}</td>
			<td>____________________</td>
		</tr>";
        }
        echo '</table><br />';
        ?>
        <?php if ($estado == '0') { ?>
    <input type="button" value="Procesar" class="boton" onClick="Procesar()">&nbsp;
    <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
    <?php } ?>
    <?php if ($estado == '1' and $Gestor->Revisar()){ ?>
    <input type="button" value="Revisar" class="boton" onClick="Revisar()"
           title="Enviar notificación de aprobación al superior">&nbsp;
    <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
    <?php } ?>
    <?php if ($estado == '3' and $Gestor->Aprobar()){ ?>
    <input type="button" value="Aprobar" class="boton" onClick="Aprobar()">&nbsp;
    <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
    <?php } ?>
        <script>__calcula();
            Generar();</script>
    <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php
    }else{
    ?>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('Q0002', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>