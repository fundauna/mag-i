<?php
define('__MODULO__', 'validacion');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _validacion7_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
$estado = $ROW[0]['estado'];

if ($_GET['acc'] == 'M') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="cumple"/>
<input type="hidden" id="cs" value="<?= $ROW[0]['cs'] ?>"/>
<input type="hidden" id="equipo"/>
<center>
    <?php $Gestor->Incluir('q6', 'hr', 'Validación de métodos :: Validaci&oacute;n e implementaci&oacute;n de m&eacute;todos: Recuperaci&oacute;n (Trazas)') ?>
    <?= $Gestor->Encabezado('Q0006', 'e', 'Validaci&oacute;n e implementaci&oacute;n de m&eacute;todos: Recuperaci&oacute;n (Trazas)') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="4">Datos del an&aacute;lisis</td>
        </tr>
        <tr>
            <td><strong>Referencia m&eacute;todo anal&iacute;tico</strong></td>
            <td colspan="3"><input type="text" id="metodo" style="width:98%" maxlength="250"
                                   value="<?= $ROW[0]['metodo'] ?>" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td><strong>Equipo</strong></td>
            <td><input type="text" id="tmp_equipo1" class="lista" value="<?= $ROW[0]['tmp_equipo1'] ?>" readonly
                       onclick="EquiposLista(1)" <?= $disabled ?>/></td>
            <td><strong>C&oacute;digo del estandar</strong></td>
            <td><input type="text" id="estandar" maxlength="20" value="<?= $ROW[0]['estandar'] ?>" <?= $disabled ?>/>
            </td>
        </tr>
        <tr>
            <td><strong>Nombre del equipo:</strong></td>
            <td><?= $ROW[0]['nombre'] ?></td>
            <td><strong>Masa estandar (mg)</strong></td>
            <td><input type="text" id="masa1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['masa'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Analito</strong></td>
            <td><input type="text" id="analito" maxlength="20" value="<?= $ROW[0]['analito'] ?>" <?= $disabled ?>/></td>
            <td><strong>Pureza (%)</strong></td>
            <td><input type="text" id="pureza" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['pureza'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Volumen de extracci&oacute;n (mL)</strong></td>
            <td><input type="text" id="vol_ext" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['vol_ext'] ?>"
                       <?= $disabled ?>></td>
            <td><strong>Coeficiente m curva</strong></td>
            <td><input type="text" id="coefM" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['coefM'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Alicuota de extracci&oacute;n (mL)</strong></td>
            <td><input type="text" id="ali_ext" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['ali_ext'] ?>"
                       <?= $disabled ?>></td>
            <td><strong>Coeficiente b curva</strong></td>
            <td><input type="text" id="coefB" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['coefB'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Volumen final (mL)</strong></td>
            <td><input type="text" id="vol_fin" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['vol_fin'] ?>"
                       <?= $disabled ?>></td>
            <td><strong>r de curva</strong></td>
            <td><input type="text" id="curva" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['curva'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td><strong>Observaciones:</strong></td>
            <td><input type="text" id="obs" style="width:98%" maxlength="250" value="<?= $ROW[0]['obs'] ?>"
                       <?= $disabled ?>/></td>
            <td><strong>Fecha:</strong></td>
            <td><?= $ROW[0]['fecha'] ?></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="5">Concentraci&oacute;n de muestra sin enriquecer</td>
        </tr>
        <tr align="center">
            <td><strong># R&eacute;plica</strong></td>
            <td><strong>Masa muestra(mg)</strong></td>
            <td><strong>Area</strong></td>
            <td><strong>Resoluci&oacute;n cromatogr&aacute;fica</strong></td>
            <td><strong>Conc. (mg/kg muestra)</strong></td>
        </tr>
        <?php
        $ROW2 = $Gestor->ObtieneGrupo(1);
        for ($x = 0; $x < count($ROW2); $x++) {
            ?>
            <tr align="center">
                <td><?= $x + 1 ?></td>
                <td><input type="text" id="_masa<?= $x ?>" name="_masa" class="monto" onblur="Redondear(this)"
                           value="<?= $ROW2[$x]['masa'] ?>" <?= $disabled ?>></td>
                <td><input type="text" id="_area<?= $x ?>" name="_area" class="monto" onblur="Redondear(this)"
                           value="<?= $ROW2[$x]['area'] ?>" <?= $disabled ?>></td>
                <td><input type="text" id="_res<?= $x ?>" name="_res" class="monto" onblur="Redondear(this)"
                           value="<?= $ROW2[$x]['res'] ?>" <?= $disabled ?>></td>
                <td id="_conc<?= $x ?>"></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="8">Resultados muestras enriquecidas</td>
        </tr>
        <tr align="center">
            <td colspan="2"><strong>Nivel</strong></td>
            <td><strong>Masa muestra(mg)</strong></td>
            <td><strong>Area</strong></td>
            <td><strong>Resoluci&oacute;n cromatogr&aacute;fica</strong></td>
            <td><strong>Conc. Enriquecida (mg/kg muestra)</strong></td>
            <td><strong>Conc. Encontrada (mg/kg muestra)</strong></td>
            <td><strong>% Recuperaci&oacute;n</strong></td>
        </tr>
        <?php
        $ROW2 = $Gestor->ObtieneGrupo(2);
        for ($x = 0, $nivel = 0, $replica = 1; $x < count($ROW2); $x++, $replica++) {
            $fila = '';
            if ($replica == 4) $replica = 1;
            if ($replica == 1) {
                $nivel++;
                echo '<tr><td colspan="8"><hr /></td></tr>';
                $fila = "<td rowspan='3'><strong>{$nivel}</strong></td>";
            }
            ?>
            <tr align="center">
                <?= $fila ?>
                <td><?= $replica ?></td>
                <td><input type="text" id="_mass<?= $x ?>" name="_mass" class="monto" onblur="Redondear(this)"
                           value="<?= $ROW2[$x]['mass'] ?>" <?= $disabled ?>></td>
                <td><input type="text" id="_are<?= $x ?>" name="_are" class="monto" onblur="Redondear(this)"
                           value="<?= $ROW2[$x]['are'] ?>" <?= $disabled ?>></td>
                <td><input type="text" id="_reso<?= $x ?>" name="_reso" class="monto" onblur="Redondear(this)"
                           value="<?= $ROW2[$x]['reso'] ?>" <?= $disabled ?>></td>
                <td><input type="text" id="_enri<?= $x ?>" name="_enri" class="monto" onblur="Redondear(this)"
                           value="<?= $ROW2[$x]['enri'] ?>" <?= $disabled ?>></td>
                <td id="_enco<?= $x ?>"></td>
                <td id="_recu<?= $x ?>"></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="5">Resultados finales</td>
        </tr>
        <tr align="center">
            <td><strong>Nivel</strong></td>
            <td><strong>CV enriquecidas</strong></td>
            <td><strong>% Recuperaci&oacute;n promedio</strong></td>
            <td><strong>Varianza</strong></td>
            <td><strong>Desv. Est.</strong></td>
        </tr>
        <tr align="center">
            <td>1</td>
            <td id="cv1"></td>
            <td id="re1"></td>
            <td id="va1"></td>
            <td id="de1"></td>
        </tr>
        <tr align="center">
            <td>2</td>
            <td id="cv2"></td>
            <td id="re2"></td>
            <td id="va2"></td>
            <td id="de2"></td>
        </tr>
        <tr align="center">
            <td>3</td>
            <td id="cv3"></td>
            <td id="re3"></td>
            <td id="va3"></td>
            <td id="de3"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="3">Prueba Homocedasticidad</td>
        </tr>
        <tr align="center">
            <td><strong>Comparaci&oacute;n niveles</strong></td>
            <td><strong>F exp</strong></td>
            <td><strong>F te&oacute;rica</strong></td>
        </tr>
        <tr align="center">
            <td>Nivel 1 - 2</td>
            <td><input type="text" id="_fexp0" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['_fexp0'] ?>"
                       <?= $disabled ?>></td>
            <td><input type="text" id="_fteo0" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['_fteo0'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr align="center">
            <td>Nivel 2 - 3</td>
            <td><input type="text" id="_fexp1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['_fexp1'] ?>"
                       <?= $disabled ?>></td>
            <td><input type="text" id="_fteo1" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['_fteo1'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr align="center">
            <td>Nivel 1 - 3</td>
            <td><input type="text" id="_fexp2" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['_fexp2'] ?>"
                       <?= $disabled ?>></td>
            <td><input type="text" id="_fteo2" class="monto" onblur="Redondear(this)" value="<?= $ROW[0]['_fteo2'] ?>"
                       <?= $disabled ?>></td>
        </tr>
        <tr>
            <td colspan="3">La regresi&oacute;n es:&nbsp;<span id="regresion"></span>
        </tr>
    </table>
    <br/>
    <?php
    if ($_GET['acc'] == 'M') {
        $ROW = $Gestor->Historial();
        echo '<table width="98%" class="radius" style="font-size:12px"></tr>';
        for ($x = 0; $x < count($ROW); $x++) {
            $ROW[$x]['accion'] = $Gestor->Accion($ROW[$x]['accion']);
            echo "<tr>
			<td><strong>{$ROW[$x]['accion']}</strong> {$ROW[$x]['nombre']} {$ROW[$x]['ap1']}, {$ROW[$x]['fecha1']}</td>
			<td>____________________</td>
		</tr>";
        }
        echo '</table><br />';
        ?>
        <?php if ($estado == '0') { ?>
            <input type="button" value="Procesar" class="boton" onClick="Procesar()">&nbsp;
            <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
        <?php } ?>
        <?php if ($estado == '1' and $Gestor->Revisar()) { ?>
            <input type="button" value="Revisar" class="boton" onClick="Revisar()"
                   title="Enviar notificación de aprobación al superior">&nbsp;
            <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
        <?php } ?>
        <?php if ($estado == '3' and $Gestor->Aprobar()) { ?>
            <input type="button" value="Aprobar" class="boton" onClick="Aprobar()">&nbsp;
            <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
        <?php } ?>
        <input type="button" value="Imprimir" class="boton" onClick="window.print()">
        <?php
    } else {
        ?>
        <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<script>__calcula();</script>
<?= $Gestor->Encabezado('Q0006', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>