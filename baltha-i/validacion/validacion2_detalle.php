<?php
define('__MODULO__', 'validacion');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _validacion2_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
$estado = $ROW[0]['estado'];

if ($_GET['acc'] == 'M') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="cumple"/>
<input type="hidden" id="cs" value="<?= $ROW[0]['cs'] ?>"/>
<center>
    <?php $Gestor->Incluir('q1', 'hr', 'Validación de métodos :: Validaci&oacute;n e implementaci&oacute;n de m&eacute;todos: Repetibilidad') ?>
    <?= $Gestor->Encabezado('Q0001', 'e', 'Validaci&oacute;n e implementaci&oacute;n de m&eacute;todos: Repetibilidad') ?>
    <br>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="6">Datos del an&aacute;lisis</td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td colspan="6" align="center"><strong>Datos de la muestra</strong></td>
                    </tr>
                    <tr>
                        <td><strong>Volumen balon 1 (mL) </strong></td>
                        <td><input type="text" id="vol_bal" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['vol_bal'] ?>" <?= $disabled ?>></td>
                    <tr>
                    <tr>
                        <td><strong>Alicuota muestra en volumen 2 (mL)</strong></td>
                        <td><input type="text" id="ali_mue" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['ali_mue'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td><strong>Alicuota enriquecimiento (mL)</strong></td>
                        <td><input type="text" id="ali_enr" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['ali_enr'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td><strong>Volumen balon (mL)</strong></td>
                        <td><input type="text" id="vol_bal2" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['vol_bal2'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td><strong>% (m/m) Muestra</strong></td>
                        <td><input type="text" id="mm" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['mm'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td><strong>Ingrediente activo:</strong></td>
                        <td><input type="text" id="IA" maxlength="30" value="<?= $ROW[0]['IA'] ?>" <?= $disabled ?>/>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>% (m/v) Muestra</strong></td>
                        <td><input type="text" id="mv" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['mv'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td><strong>Densidad (g/mL)</strong></td>
                        <td><input type="text" id="densidad" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['densidad'] ?>" <?= $disabled ?>></td>
                    </tr>
                </table>
            </td>
            <td>
                <table>
                    <tr>
                        <td colspan="6" align="center"><strong>Datos del est&aacute;ndar</strong></td>
                    </tr>
                    <tr>
                        <td><strong>C&oacute;digo del Estandar</strong></td>
                        <td><input type="text" id="estandar" maxlength="20" value="<?= $ROW[0]['estandar'] ?>"
                                   <?= $disabled ?>/></td>
                    <tr>
                    <tr>
                        <td><strong>Masa estandar (mg)</strong></td>
                        <td><input type="text" id="masa" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['masa'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td><strong>Pureza (%)</strong></td>
                        <td><input type="text" id="pureza" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['pureza'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td><strong>Volumen Int (mL)</strong></td>
                        <td><input type="text" id="vol_int" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['vol_int'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td><strong>Concentraci&oacute;n Intemedia (mg/mL)</strong></td>
                        <td id="conc"></td>
                    </tr>
                    <tr>
                        <td><strong>Alicuota diluci&oacute;n STD</strong> (mL)</td>
                        <td><input type="text" id="ali_dil" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['ali_dil'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td><strong>Volumen diluci&oacute;n (mL)</strong></td>
                        <td><input type="text" id="vol_dil" class="monto" onblur="Redondear(this)"
                                   value="<?= $ROW[0]['vol_dil'] ?>" <?= $disabled ?>></td>
                    </tr>
                    <tr>
                        <td style="background-color:#CCCCCC"><strong>Unidad a validar:</strong></td>
                        <td><select id="tipo" onchange="__calcula();" <?= $disabled ?>>
                                <option value="0">%m/m</option>
                                <option value="1" <?php if ($ROW[0]['tipo'] == '1') echo 'selected'; ?>>%m/v</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td><strong>Fecha:</strong></td>
                        <td><?= $ROW[0]['fecha'] ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="7">Datos de an&aacute;lisis <?php if ($_GET['acc'] == 'I') { ?>&nbsp;<img
                    onclick="MuestrasMas(); __calcula();" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                    title="Agregar línea" class="tab" /><?php } ?></td>
        </tr>
        <tr>
            <td><strong># R&eacute;plica</strong></td>
            <td><strong>Masa muestra(mg)</strong></td>
            <td colspan="3" align="center"><strong>&Aacute;rea de Estandar</strong></td>
            <td><strong>Promedio</strong></td>
            <td><strong>CV</strong></td>
        </tr>
        <tbody id="lolo">
        <?php
        $ROW2 = $Gestor->ObtieneGrupo1();
        for ($x = 0; $x < count($ROW2); $x++) {
            ?>
            <tr>
                <td><?= $x + 1 ?></td>
                <td><input type="text" id="A<?= $x ?>" name="_A" class="monto" onblur="Redondear(this)"
                           value="<?= $ROW2[$x]['A'] ?>" <?= $disabled ?>></td>
                <td><input type="text" id="B<?= $x ?>" name="_B" class="monto" onblur="Redondear(this)"
                           value="<?= $ROW2[$x]['B'] ?>" <?= $disabled ?>></td>
                <td><input type="text" id="C<?= $x ?>" name="_C" class="monto" onblur="Redondear(this)"
                           value="<?= $ROW2[$x]['C'] ?>" <?= $disabled ?>></td>
                <td><input type="text" id="D<?= $x ?>" name="_D" class="monto" onblur="Redondear(this)"
                           value="<?= $ROW2[$x]['D'] ?>" <?= $disabled ?>></td>
                <td><input type="text" id="E<?= $x ?>" class="monto" readonly></td>
                <td><input type="text" id="F<?= $x ?>" class="monto" readonly></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td><strong># R&eacute;plica</strong></td>
            <td colspan="3" align="center"><strong>&Aacute;rea Muestra</strong></td>
            <td><strong>Promedio</strong></td>
            <td><strong>CV</strong></td>
            <td><strong>% (m/m)</strong></td>
            <td><strong>% (m/v)</strong></td>
            <td align="center" title="Utilizar?" style="cursor:help"><strong>?</strong></td>
        </tr>
        <tbody id="lolo2">
        <?php
        $ROW2 = $Gestor->ObtieneGrupo2();
        for ($x = 0; $x < count($ROW2); $x++) {
            ?>
            <tr id="tr_usar<?= $x ?>">
                <td><?= $x + 1 ?></td>
                <td><input type="text" id="G<?= $x ?>" name="_G" class="monto" onblur="Redondear(this)"
                           value="<?= $ROW2[$x]['G'] ?>" <?= $disabled ?>></td>
                <td><input type="text" id="H<?= $x ?>" name="_H" class="monto" onblur="Redondear(this)"
                           value="<?= $ROW2[$x]['H'] ?>" <?= $disabled ?>></td>
                <td><input type="text" id="I<?= $x ?>" name="_I" class="monto" onblur="Redondear(this)"
                           value="<?= $ROW2[$x]['I'] ?>" <?= $disabled ?>></td>
                <td><input type="text" id="J<?= $x ?>" class="monto" readonly></td>
                <td><input type="text" id="K<?= $x ?>" class="monto" readonly></td>
                <td><input type="text" id="L<?= $x ?>" name="_L" class="monto" readonly></td>
                <td><input type="text" id="M<?= $x ?>" name="_M" class="monto" readonly></td>
                <td><input type="checkbox" id="X<?= $x ?>" name="X"
                           onclick="__calcula()" <?php if ($ROW2[$x]['X'] == '1') echo 'checked'; ?> <?= $disabled ?>/>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="98%">
        <tr>
            <td class="titulo" colspan="8">Prueba de Grubbs para Outliers</td>
        </tr>
        <tr align="center">
            <td><strong>Promedio</strong></td>
            <td><strong>Desv. Est.</strong></td>
            <td><strong>CV</strong></td>
            <td id="tr_tabla" style="font-weight:bold">T tabla</td>
            <td><strong>RRSD Horwitz</strong></td>
            <td><strong>rRSD Horwitz</strong></td>
            <td><strong>> rRSD LCC</strong></td>
            <td><strong>Cumple?</strong></td>
        </tr>
        <tr align="center">
            <td id="_prom"></td>
            <td id="_desv"></td>
            <td id="_cv"></td>
            <td id="_tabla"></td>
            <td id="_horwitz1"></td>
            <td id="_horwitz2" bgcolor="#99CCCC"></td>
            <td bgcolor="#CCCCCC" id="_lcc"></td>
            <td id="_cumple"></td>
        </tr>
    </table>
    <br/>
    <?php
    if ($_GET['acc'] == 'M') {
        $ROW = $Gestor->Historial();
        echo '<table width="98%" class="radius" style="font-size:12px"></tr>';
        for ($x = 0; $x < count($ROW); $x++) {
            $ROW[$x]['accion'] = $Gestor->Accion($ROW[$x]['accion']);
            echo "<tr>
			<td><strong>{$ROW[$x]['accion']}</strong> {$ROW[$x]['nombre']} {$ROW[$x]['ap1']}, {$ROW[$x]['fecha1']}</td>
			<td>____________________</td>
		</tr>";
        }
        echo '</table><br />';
        ?>
        <?php if ($estado == '0') { ?>
    <input type="button" value="Procesar" class="boton" onClick="Procesar()">&nbsp;
    <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
    <?php } ?>
    <?php if ($estado == '1' and $Gestor->Revisar()){ ?>
    <input type="button" value="Revisar" class="boton" onClick="Revisar()"
           title="Enviar notificación de aprobación al superior">&nbsp;
    <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
    <?php } ?>
    <?php if ($estado == '3' and $Gestor->Aprobar()){ ?>
    <input type="button" value="Aprobar" class="boton" onClick="Aprobar()">&nbsp;
    <input type="button" value="Anular" class="boton" onClick="Anular()">&nbsp;
    <?php } ?>
        <script>__calcula();
            Generar();</script>
    <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php
    }else{
    ?>
        <script>
            MuestrasMas();
            MuestrasMas();
            MuestrasMas();
            MuestrasMas();
            MuestrasMas();
            MuestrasMas();
            MuestrasMas();
            __calcula();
            LimpiaColores();
        </script>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('Q0001', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>