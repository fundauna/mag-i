<?php
define('__MODULO__', 'seguridad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _login();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="LID" value="<?= $Gestor->LabId() ?>"/>
<br/><br/>
<center>
    <table class="radius" width="30%">
        <tr>
            <td><?= $Gestor->Incluir('sfe_logo', 'jpg') ?></td>
            <td><?= $Gestor->LabName() ?></td>
        </tr>
        <tr>
            <td colspan="2" align="right"><font size="-2">Sistema de Gesti&oacute;n
                    Laboratorial <?= $Gestor->VersionId() ?></font></td>
        </tr>
    </table>
    <br/><br/><br/>
    <table class="radius" width="150px">
        <tr>
            <td class="titulo">Login</td>
        </tr>
        <tr>
            <td align="center">Usuario:</td>
        </tr>
        <tr>
            <td align="center"><input type="text" size="13" maxlength="15" id="usuario"/></td>
        </tr>
        <tr>
            <td align="center">Clave:&nbsp;</td>
        </tr>
        <tr>
            <td align="center"><input type="password" size="13" id="clave"/></td>
        </tr>
    </table>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton2" onClick="login()">
    <br/><br/><br/><br/><br/><br/><br/><br/>
    <a href="index.php" class="none" title="Inicio"><?= $Gestor->Incluir('home', 'png') ?></a>
    <br/>
    <font size="-2">&copy; <a class="none" href="http://www.fundauna.org" target="_blank"><font
                    color="#0066CC">FUNDA</font><font color="#FF0000">SOFT</font></a> <?= $Gestor->VersionDate() ?>
    </font>
</center>
<div style="display: none;">
    <form id="frm1" method="post" action="usuarios_detalle2.php?acc=I">
        <input type="hidden" name="login" id="usuario2" value=""/>
        <input type="hidden" name="clave" id="clave2" value=""/>
        <input type="hidden" name="LID" value="<?= $Gestor->LabId() ?>"/>
        <input type="submit" value=""/>
    </form>
</div>
</body>
</html>