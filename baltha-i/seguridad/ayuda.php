<?php
define('__MODULO__', 'seguridad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _ayuda();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('m12', 'hr', 'Seguridad :: Manual de usuario') ?>
<center>
    <table class="radius" width="300px">
        <tr>
            <td class="titulo" colspan="2">Manuales disponibles</td>
        </tr>
        <tr>
            <td>&bull; Secci&oacute;n administrativa</td>
            <td><img src="<?php $Gestor->Incluir('previa', 'bkg') ?>" title="Ver Manual"
                     onclick="window.open('manual_admin/index.htm');" class="tab3"/></td>
        </tr>
        <tr>
            <td>&bull; Secci&oacute;n clientes</td>
            <td><img src="<?php $Gestor->Incluir('previa', 'bkg') ?>" title="Ver Manual"
                     onclick="window.open('manual_clientes/index.htm');" class="tab3"/></td>
        </tr>
    </table>
</center>
</body>
</html>