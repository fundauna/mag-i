<?php
define('__MODULO__', 'seguridad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _clientes();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<br/><br/>
<center>
    <table class="radius" width="30%">
        <tr>
            <td><?= $Gestor->Incluir('sfe_logo', 'jpg') ?></td>
            <td>Ingreso de Clientes</td>
        </tr>
        <tr>
            <td colspan="2" align="right"><font size="-2"
                                                ondblclick="lolo()"><?= $Gestor->Title() ?> <?= $Gestor->VersionId() ?></font>
            </td>
        </tr>
    </table>
    <br/><br/><br/>
    <table class="radius" width="150px">
        <tr>
            <td class="titulo">Login</td>
        </tr>
        <tr>
            <td align="center">Usuario:</td>
        </tr>
        <tr>
            <td align="center"><input type="text" size="13" maxlength="15" id="usuario"/></td>
        </tr>
        <tr>
            <td align="center">Clave:&nbsp;<a href="olvido2.php" target="_blank"><img
                            src="<?= $Gestor->Incluir('olvido', 'bkg') ?>" border="0" width="20" height="20"
                            title="Olvid� su clave?" style="vertical-align:middle; cursor:pointer"/></a></td>
        </tr>
        <tr>
            <td align="center"><input type="password" size="13" id="clave"/></td>
        </tr>
    </table>
    <br/>
    <input type="button" id="btn" value="Ingresar" class="boton2" onClick="login()">&nbsp;&nbsp;&nbsp;&nbsp;<input
            type="button" id="btn" value="Registrarse" class="boton2" onClick="registrar()">
</center>
</body>
</html>