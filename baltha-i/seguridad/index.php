<?php
define('__MODULO__', 'seguridad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _index();
$ip = $Gestor->Ip();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('index', 'css'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body style="background:none">
<br/><br/><br/><br/><br/><br/>
<table align="center">
    <tr align="center" valign="top">
        <td align="center">
            <div>
                <h1><img border="0" src="<?php $Gestor->Incluir('sfe', 'bkg') ?>"/><span>Sistema de Gesti&oacute;n Laboratorial <br/>v.<?= $Gestor->VersionId() ?></span>
                </h1>
                <div class="content">
                    <ul class="ca-menu">
                        <li style="visibility:hidden"></li>
                        <li id="Cir1">
                            <a href="#"
                               onclick="location.href='http://<?= $ip ?>/MAG-i/baltha-i/seguridad/login.php?LID=c4ca4238a0b923820dcc509a6f75849b';">
                                <span class="ca-icon"><img border="0"
                                                           src="<?php $Gestor->Incluir('residuos', 'bkg') ?>"/></span>
                                <div class="ca-content">
                                    <h2 class="ca-main">Laboratorio de An&aacute;lisis de Residuos de Agroqu&iacute;micos<br/>(LRE)
                                    </h2>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#"
                               onclick="location.href='http://<?= $ip ?>/MAG-i/baltha-i/seguridad/login.php?LID=c81e728d9d4c2f636f067f89cc14862c';">
                                <span class="ca-icon"><img border="0" src="<?php $Gestor->Incluir('plagas', 'bkg') ?>"/></span>
                                <div class="ca-content">
                                    <h2 class="ca-main">Laboratorio Central de Diagn&oacute;stico de Plagas<br/>(LDP)
                                    </h2>
                                </div>
                            </a>
                        </li>
                        <li id="Cir3">
                            <a href="#"
                               onclick="location.href='http://<?= $ip ?>/MAG-i/baltha-i/seguridad/login.php?LID=eccbc87e4b5ce2fe28308fd9f2a7baf3';">
                                <span class="ca-icon"><img border="0"
                                                           src="<?php $Gestor->Incluir('calidad', 'bkg') ?>"/></span>
                                <div class="ca-content">
                                    <h2 class="ca-main">Laboratorio de Control de Calidad de Agroqu&iacute;micos<br/>(LCC)
                                    </h2>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div><!-- content -->
            </div>
            <br/><br/><br/><br/><br/><br/><br/> <br/><br/><br/><br/><br/><br/><br/> <br/><br/><br/><br/><br/>
            <font size="-2">&copy; <a class="none" href="http://www.fundauna.org" target="_blank"><font color="#0066CC">FUNDA</font><font
                            color="#FF0000">SOFT</font></a> <?= $Gestor->VersionDate() ?></font>
        </td>
    </tr>
</table>
</body>
</html>