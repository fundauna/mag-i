<?php
define('__MODULO__', 'seguridad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _menu();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('menu', 'css'); ?>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body marginheight="0" marginwidth="0">
<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#000000" height="97px">
    <tr>
        <td background="<?php $Gestor->CargaBanner() ?>" style="padding-right:10px" width="100%">
            <table height="80px" align="right">
                <tr>
                    <td>
                        <div style="position: relative; z-index: 10; color:#FFFFFF; padding:4px; font-size:12px">
                            <div class="radius2"></div>
                            <table>
                                <tr>
                                    <td align="right"><strong>Usuario:</strong></td>
                                    <td><?= $Gestor->Get('UNAME') ?></td>
                                </tr>
                                <tr>
                                    <td align="right"><strong>Perfil:</strong></td>
                                    <td <?php if ($Gestor->EsAdmin()) echo ' class="admin"'; ?>><?= $Gestor->NombreRol() ?></td>
                                </tr>
                                <tr>
                                    <td align="right"><strong>Lab:</strong></td>
                                    <td><?= $Gestor->LabName() ?></td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="2">
                                        <hr/>
                                        <a href="login.php?LID=FIN" style="color:#FFFFFF; font-size:10px">[Cerrar Sesi&oacute;n]</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<?php $Gestor->CargaPermisos(); ?>
<div id="menu">
    <ul class="menu">
        <li><a href="tablon.php?ver=1" target="detalle"><span>Inicio</span></a></li>
        <?php if ($Gestor->TienePermiso('01')) { ?>
            <li><a href="#" class="parent"><span>Cotizaciones</span></a>
                <ul>
                    <!--  <li><a href="#" class="parent"><span>Sub Item 1</span></a>
                        <ul>
                             <li><a href="#" class="parent"><span><img src="../../caspha-i/imagenes/menu/icon_08.png" class="icon">&nbsp;Sub Item 1.1</span></a>
                                 <ul>
                                     <li><a href="#" class="parent"><span>Sub Item 1.1.1</span></a>
                                                                             <ul>
                                                                                     <li><a href="#"><span>Sub Item 1.1.1</span></a></li>
                                                                                     <li><a href="#"><span>Sub Item 1.1.2</span></a></li>
                                                                             </ul>
                                                                     </li>
                                     <li><a href="#"><span>Sub Item 1.1.2</span></a></li>
                                 </ul>
                             </li>
                             <li><a href="#"><span>Sub Item 1.2</span></a></li>
                             <li><a href="#"><span>Sub Item 1.3</span></a></li>
                             <li><a href="#"><span>Sub Item 1.4</span></a></li>
                             <li><a href="#"><span>Sub Item 1.5</span></a></li>
                             <li><a href="#"><span>Sub Item 1.6</span></a></li>
                             <li><a href="#" class="parent"><span>Sub Item 1.7</span></a>
                                 <ul>
                                     <li><a href="#"><span>Sub Item 1.7.1</span></a></li>
                                     <li><a href="#"><span>Sub Item 1.7.2</span></a></li>
                                 </ul>
                             </li>
                         </ul>
                     </li>-->
                    <?php if ($Gestor->TienePermiso('-1')) { ?>
                        <li><a href="../cotizaciones/solicitudes.php"
                               target="detalle"><span><?php $Gestor->Incluir('buscar', 'ico') ?>Solicitudes</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('-2')) { ?>
                        <li><a href="../cotizaciones/cotizaciones.php"
                               target="detalle"><span><?php $Gestor->Incluir('doc', 'ico') ?>Cotizaciones</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->EsLDP() && $Gestor->TienePermiso('-4')) { ?>
                        <li><a href="../cotizaciones/aperturas.php"
                               target="detalle"><span><?php $Gestor->Incluir('apertura', 'ico') ?>Aperturas</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->EsLDP() && $Gestor->TienePermiso('-6')) { ?>
                        <li><a href="../cotizaciones/certificaciones.php"
                               target="detalle"><span><?php $Gestor->Incluir('doc3', 'ico') ?>Certificaciones</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('-9')) { ?>
                        <li>
                            <hr/>
                        </li>
                        <li><a href="../mantenimientos/analisis_i.php"
                               target="detalle"><span><?php $Gestor->Incluir('inv_analisis1', 'ico') ?>An&aacute;lisis externos</span></a>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($Gestor->TienePermiso('02')) { ?>
            <li><a href="#" class="parent"><span>Servicios</span></a>
                <ul>
                    <?php if (!$Gestor->EsLDP() && $Gestor->TienePermiso('21')) { ?>
                        <li><a href="../servicios/solicitudes.php"
                               target="detalle"><span><?php $Gestor->Incluir('doc2', 'ico') ?>Solicitudes</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->EsLRE() && $Gestor->TienePermiso('23')) { ?>
                        <li>
                            <hr/>
                        </li>
                        <li><a href="../servicios/matriz.php"
                               target="detalle"><span><?php $Gestor->Incluir('cultivo', 'ico') ?>Matrices</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('23')) { ?>
                        <li><a href="#" class="parent"><span><?php $Gestor->Incluir('inv_analisis3', 'ico') ?>An&aacute;lisis internos</span></a>
                            <ul>
                                <li><a href="../servicios/subanalisis.php"
                                       target="detalle"><span><?php $Gestor->Incluir('inv_analisis3', 'ico') ?>An&aacute;lisis internos</span></a>
                                </li>
                                <?php if ($Gestor->EsLCC()) { ?>
                                    <li><a href="../servicios/estxing.php"
                                           target="detalle"><span><?php $Gestor->Incluir('inv_analisis3', 'ico') ?>Est. x Ing. Activo</span></a>
                                    </li>
                                <?php } ?>
                                <?php if ($Gestor->EsLRE()) { ?>
                                    <li><a href="../servicios/mantgrupos.php"
                                           target="detalle"><span><?php $Gestor->Incluir('inv_analisis3', 'ico') ?>Mant. Grupos</span></a>
                                    </li>
                                    <li><a href="../servicios/matrizanalitos.php"
                                           target="detalle"><span><?php $Gestor->Incluir('inv_analisis3', 'ico') ?>Rel. Grupos-Analitos</span></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>

                        <?php if (!$Gestor->EsLDP() and !$Gestor->EsLRE()) { ?>
                            <li><a href="#"
                                   class="parent"><span><?php $Gestor->Incluir('atom', 'ico') ?>Formulaciones</span></a>
                                <ul>
                                    <li><a href="../servicios/formulaciones_cat.php"
                                           target="detalle"><span><?php $Gestor->Incluir('clasi', 'ico') ?>Cat&aacute;logo</span></a>
                                    </li>
                                    <li><a href="../servicios/formulaciones_arq.php"
                                           target="detalle"><span><?php $Gestor->Incluir('arqueo', 'ico') ?>Arqueo</span></a>
                                    </li>
                                    <li><a href="../servicios/analisis_for.php"
                                           target="detalle"><span><?php $Gestor->Incluir('atom', 'ico') ?>An&aacute;lisis x Formulaci&oacute;n</span></a>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>
                    <?php } ?>
                    <?php if ($Gestor->EsLCC() && $Gestor->TienePermiso('27')) { ?>
                        <li>
                            <hr/>
                        </li>
                        <li><a href="../servicios/arqueo_metodos.php" target="detalle"
                               title="Hojas de c�lculo por An�lisis internos"><span><?php $Gestor->Incluir('arqueo', 'ico') ?>HC x An&aacute;lisis</span></a>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>
        <?php if (!$Gestor->EsLDP()) { ?>
            <li><a href="#" class="parent"><span>An&aacute;lisis</span></a>
                <ul>
                    <?php if ($Gestor->TienePermiso('26')) { ?>
                        <li><a target="detalle"
                               href="../servicios/muestras_asignar.php"><span><?php $Gestor->Incluir('usuarios', 'ico') ?>Asignar analistas</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('25')) { ?>
                        <li><a target="detalle"
                               href="../servicios/libro.php"><span><?php $Gestor->Incluir('book', 'ico') ?>Libro de custodia</span></a>
                        </li>
                        <li><a target="detalle"
                               href="../servicios/analisis_pendientes.php"><span><?php $Gestor->Incluir('metodos', 'ico') ?>An&aacute;lisis pendientes</span></a>
                        </li>
                        <li><a target="detalle"
                               href="../servicios/historial_ensayos.php"><span><?php $Gestor->Incluir('buscar', 'ico') ?>Historial de ensayos</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('29')) { ?>
                        <li><a target="detalle"
                               href="../servicios/cambiar_estado.php"><span><?php $Gestor->Incluir('retirar', 'ico') ?>Retirar muestras</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->EsLCC() && $Gestor->TienePermiso('27')) { ?>
                        <li>
                            <hr/>
                        </li>
                        <li><a target="detalle"
                               href="../metodos/03_inc_cc.php"><span><?php $Gestor->Incluir('atom', 'ico') ?>Incertidumbre por curvas de calibraci&oacute;n</span></a>
                        </li>
                    <?php } ?>
                    <li>
                        <hr/>
                    </li>
                    <li><a target="detalle"
                           href="../metodos/03_grafico_control.php"><span><?php $Gestor->Incluir('resultados', 'ico') ?>Gr&aacute;fico de control</span></a>
                    </li>
                </ul>
            </li>
        <?php } ?>

        <?php if ($Gestor->TienePermiso('96')) { ?>
            <li><a href="../validacion/validacion_metodos.php" target="detalle"><span>Validaci&oacute;n de m&eacute;todos</span></a>
            </li>
        <?php } ?>
        <?php if ($Gestor->EsLCC()) { ?>
            <li><a href="../centinela/centinela.php" target="detalle"><span>Centinela</span></a></li>
        <?php } ?>
        <?php if ($Gestor->TienePermiso('08')) { ?>
            <li><a href="#"><span>Reportes</span></a>
                <ul>
                    <li><a href="../reportes/menu.php"
                           target="detalle"><span><?php $Gestor->Incluir('reportes', 'ico') ?>Estad&iacute;sticos</span></a>
                    </li>
                    <li><a href="#" class="parent"><span><?php $Gestor->Incluir('resultados', 'ico') ?>Informe Resultados</span></a>
                        <ul>
                            <?php if ($Gestor->TienePermiso('84')) { ?>
                                <li><a href="../reportes/final_generar.php"
                                       target="detalle"><span><?php $Gestor->Incluir('autorizaciones', 'ico') ?>Generar</span></a>
                                </li>
                            <?php } ?>
                            <?php if ($Gestor->TienePermiso('85')) { ?>
                                <li><a href="../reportes/final_historial.php"
                                       target="detalle"><span><?php $Gestor->Incluir('buscar', 'ico') ?>Historial</span></a>
                                </li>
                            <?php } ?>
                            <?php if ($Gestor->EsLRE()) { ?>
                                <li><a href="../reportes/mant_jefe.php"
                                       target="detalle"><span><?php $Gestor->Incluir('bitacora', 'ico') ?>Mantenimiento Jefe</span></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php if ($Gestor->TienePermiso('86')) { ?>
                        <li><a href="#"
                               class="parent"><span><?php $Gestor->Incluir('bitacora', 'ico') ?>Seguridad</span></a>
                            <ul>
                                <li><a href="../reportes/rep_perfiles.php"
                                       target="detalle"><span><?php $Gestor->Incluir('perfiles', 'ico') ?>Perfiles</span></a>
                                </li>
                                <li><a href="../reportes/rep_usuarios.php"
                                       target="detalle"><span><?php $Gestor->Incluir('usuarios', 'ico') ?>Usuarios</span></a>
                                </li>
                                <li><a href="../reportes/bitacora.php"
                                       target="detalle"><span><?php $Gestor->Incluir('bitacora', 'ico') ?>Bit&aacute;cora</span></a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($Gestor->TienePermiso('06')) { ?>
            <li><a href="#"><span>Equipos</span></a>
                <ul>
                    <?php if ($Gestor->TienePermiso('61')) { ?>
                        <li><a href="../equipos/catalogo.php"
                               target="detalle"><span><?php $Gestor->Incluir('eki_catalogo', 'ico') ?>Ficha t&eacute;cnica</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('62')) { ?>
                        <li><a href="../equipos/calibraciones.php"
                               target="detalle"><span><?php $Gestor->Incluir('eki_calibra', 'ico') ?>Calibraciones</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('63')) { ?>
                        <li><a href="../equipos/mantenimientos.php"
                               target="detalle"><span><?php $Gestor->Incluir('eki_mante', 'ico') ?>Mantenimientos</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('64')) { ?>
                        <li><a href="../equipos/cronograma.php"
                               target="detalle"><span><?php $Gestor->Incluir('inv_tramites', 'ico') ?>Cronograma</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('66')) { ?>
                        <li>
                            <hr/>
                        </li>
                        <?php if (!$Gestor->EsLDP()) { ?>
                            <li><a href="../equipos/camaras.php"
                                   target="detalle"><span><?php $Gestor->Incluir('camara', 'ico') ?>Refrigeraci&oacute;n</span></a>
                            </li>
                        <?php } ?>
                        <li><a href="../equipos/masas.php"
                               target="detalle"><span><?php $Gestor->Incluir('pesas', 'ico') ?>Masas</span></a></li>
                        <li><a href="../equipos/termometros.php?tipo=1"
                               target="detalle"><span><?php $Gestor->Incluir('termo2', 'ico') ?>Term&oacute;metros</span></a>
                        </li>
                        <li><a href="../equipos/termometros.php?tipo=0"
                               target="detalle"><span><?php $Gestor->Incluir('termo', 'ico') ?>Termohigr&oacute;metro</span></a>
                        </li>
                        <?php if (!$Gestor->EsLDP()) { ?>
                            <li><a href="../equipos/balanzas.php"
                                   target="detalle"><span><?php $Gestor->Incluir('bal', 'ico') ?>Balanzas</span></a>
                            </li>
                            <li><a href="../equipos/micropipetas.php"
                                   target="detalle"><span><?php $Gestor->Incluir('syr', 'ico') ?>Micropipetas</span></a>
                            </li>
                        <?php } ?>
                        <li><a href="../equipos/cuarto.php"
                               target="detalle"><span><?php $Gestor->Incluir('room', 'ico') ?>Cuarto</span></a></li>
                    <?php } ?>
                    <?php if (($Gestor->EsLRE() or $Gestor->EsLCC()) && $Gestor->TienePermiso('67')) { ?>
                        <li>
                            <hr/>
                        </li>
                        <li><a href="../cartas/historial.php"
                               target="detalle"><span><?php $Gestor->Incluir('cartas', 'ico') ?>Cartas/Verificaci&oacute;n</span></a>
                        </li>
                    <?php } ?>
                    <?php if (!$Gestor->EsLDP() && $Gestor->TienePermiso('69')) { ?>
                        <li><a href="../equipos/uso.php" target="detalle"><span><?php $Gestor->Incluir('uso', 'ico') ?>Registro de uso</span></a>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($Gestor->TienePermiso('A0')) { ?>
            <li><a href="#"><span>Inventario</span></a>
                <ul>
                    <li><a href="#" class="parent"><span><?php $Gestor->Incluir('clasi', 'ico') ?>Clasificaciones</span></a>
                        <ul>
                            <?php if ($Gestor->TienePermiso('A1')) { ?>
                                <li><a href="../inventario/tipos.php"
                                       target="detalle"><span><?php $Gestor->Incluir('tipos', 'ico') ?>Tipos</span></a>
                                </li>
                                <li><a href="../inventario/plantillas.php"
                                       target="detalle"><span><?php $Gestor->Incluir('plantillas', 'ico') ?>Plantillas</span></a>
                                </li>
                            <?php } ?>
                            <?php if ($Gestor->TienePermiso('A2')) { ?>
                                <li><a href="../inventario/presentaciones.php"
                                       target="detalle"><span><?php $Gestor->Incluir('presentacion', 'ico') ?>Presentaciones</span></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>
                    <li>
                        <hr/>
                    </li>
                    <?php if ($Gestor->TienePermiso('A4')) { ?>
                        <li><a href="../inventario/inventario.php"
                               target="detalle"><span><?php $Gestor->Incluir('inv_inventario', 'ico') ?>Cat&aacute;logo</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->EsLCC()) { ?>
                        <li><a href="../inventario/general_inventario.php"
                               target="detalle"><span><?php $Gestor->Incluir('doc2', 'ico') ?>Registro general inventarios</span></a>
                        </li>
                        <li><a href="../inventario/ingreso_salida.php"
                               target="detalle"><span><?php $Gestor->Incluir('doc2', 'ico') ?>Control ingreso y salida de materiales</span></a>
                        </li>
                        <li><a href="../inventario/material_vencido.php"
                               target="detalle"><span><?php $Gestor->Incluir('doc2', 'ico') ?>Control material vencido</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('A4')) { ?>
                        <li><a href="../inventario/importar.php"
                               target="detalle"><span><?php $Gestor->Incluir('datos', 'ico') ?>Importar Inventario</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->EsLDP() && $Gestor->TienePermiso('A7')) { ?>
                        <li><a href="#"
                               class="parent"><span><?php $Gestor->Incluir('atom', 'ico') ?>Bio. Molecular</span></a>
                            <ul>
                                <!-- <li><a href="../biologia/bandejas.php" target="detalle"><span><?php $Gestor->Incluir('bandejas', 'ico') ?>Bandejas</span></a></li> -->
                                <li><a href="../biologia/camaras2.php"
                                       target="detalle"><span><?php $Gestor->Incluir('camara', 'ico') ?>Camaras</span></a>
                                </li>
                                <li><a href="../biologia/importar.php"
                                       target="detalle"><span><?php $Gestor->Incluir('datos', 'ico') ?>Importar ADN-ARN</span></a>
                                </li>
                                <li>
                                    <hr/>
                                </li>
                                <li><a href="../biologia/resuspension.php" target="detalle"
                                       title="Control de resuspensi�n"><span><?php $Gestor->Incluir('molecular', 'ico') ?>Ctrl. Resuspensi&oacute;n</span></a>
                                </li>
                                <li><a href="../biologia/diluciones.php"
                                       target="detalle"><span><?php $Gestor->Incluir('molecular2', 'ico') ?>Registro de diluciones</span></a>
                                </li>
                                <li><a href="../biologia/alicuotas.php"
                                       target="detalle"><span><?php $Gestor->Incluir('molecular4', 'ico') ?>Ctrl. Alicuotas</span></a>
                                </li>
                                <li><a href="../biologia/adenoteca.php" target="detalle"
                                       title="ADNoteca-ARNoteca"><span><?php $Gestor->Incluir('adn', 'ico') ?>ADNoteca-ARNoteca</span></a>
                                </li>
                                <li><a href="../biologia/proc.php"
                                       target="detalle"><span><?php $Gestor->Incluir('contratos', 'ico') ?>Procedimientos</span></a>
                                </li>
                                <li><a href="../biologia/trabajo.php"
                                       target="detalle"><span><?php $Gestor->Incluir('molecular3', 'ico') ?>Hoja de trabajo</span></a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('A8') && !$Gestor->EsLDP()) { ?>
                        <li><a href="../inventario/arqueo.php"
                               target="detalle"><span><?php $Gestor->Incluir('arqueo', 'ico') ?>Arqueo</span></a></li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($Gestor->TienePermiso('B0')) { ?>
            <li><a href="#"><span>Proveedores</span></a>
                <ul>
                    <?php if ($Gestor->TienePermiso('B1')) { ?>
                        <li><a href="../proveedores/clasificaciones.php"
                               target="detalle"><span><?php $Gestor->Incluir('clasi', 'ico') ?>Clasificaci&oacute;n</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('B2')) { ?>
                        <li><a href="../proveedores/proveedores.php"
                               target="detalle"><span><?php $Gestor->Incluir('proveedores', 'ico') ?>Cat&aacute;logo</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('B3')) { ?>
                        <li><a href="../proveedores/evaluaciones.php"
                               target="detalle"><span><?php $Gestor->Incluir('quality', 'ico') ?>Evaluaciones</span></a>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($Gestor->TienePermiso('07')) { ?>
            <li><a href="#"><span>Tra. y Serv. Externos</span></a>
                <ul>
                    <?php if ($Gestor->TienePermiso('71')) { ?>
                        <li><a href="../mantenimientos/analisis_e.php"
                               target="detalle"><span><?php $Gestor->Incluir('inv_analisis2', 'ico') ?>Servicios externos</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('72')) { ?>
                        <li><a href="#"
                               class="parent"><span><?php $Gestor->Incluir('contratos', 'ico') ?>Contratos</span></a>
                            <ul>
                                <li><a href="../mantenimientos/tipos_contratos.php"
                                       target="detalle"><span><?php $Gestor->Incluir('clasi', 'ico') ?>Tipos de contrato</span></a>
                                </li>
                                <li><a href="../mantenimientos/contratos.php"
                                       target="detalle"><span><?php $Gestor->Incluir('contratos', 'ico') ?>Cat&aacute;logo</span></a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('73')) { ?>
                        <li><a href="../mantenimientos/tramites.php"
                               target="detalle"><span><?php $Gestor->Incluir('inv_tramites', 'ico') ?>Tr&aacute;mites</span></a>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <li><a href="#"><span>Personal</span></a>
            <ul>
                <li><a href="../personal/curriculo.php" target="detalle"><span><?php $Gestor->Incluir('vida', 'ico') ?>Hoja de vida</span></a>
                </li>
                <?php if ($Gestor->TienePermiso('42')) { ?>
                    <li><a href="../personal/citas.php" target="detalle"><span><?php $Gestor->Incluir('citas', 'ico') ?>Citas/Chequeos</span></a>
                    </li>
                <?php } ?>
                <?php if ($Gestor->TienePermiso('43')) { ?>
                    <li><a href="../personal/autorizaciones.php?tipo=2"
                           target="detalle"><span><?php $Gestor->Incluir('autorizaciones', 'ico') ?>Autorizaciones</span></a>
                    </li>
                <?php } ?>
                <?php if ($Gestor->TienePermiso('44')) { ?>
                    <li>
                        <hr/>
                    </li>
                    <li><a href="../personal/parametros.php"
                           target="detalle"><span><?php $Gestor->Incluir('parametros', 'ico') ?>Par&aacute;metros</span></a>
                    </li>
                <?php } ?>
                <li><a href="../personal/sugerencias.php"
                       target="detalle"><span><?php $Gestor->Incluir('quality', 'ico') ?>Sugerencias</span></a></li>
            </ul>
        </li>

        <?php if ($Gestor->TienePermiso('05')) { ?>
            <li><a href="#"><span>Serv. Cliente</span></a>
                <ul>
                    <?php if ($Gestor->TienePermiso('51')) { ?>
                        <li><a href="../sc/quejas.php" target="detalle"><span><?php $Gestor->Incluir('quejas', 'ico') ?>Quejas</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('52')) { ?>
                        <li><a href="../sc/encuestas.php"
                               target="detalle"><span><?php $Gestor->Incluir('encuestas', 'ico') ?>Encuestas</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('53')) { ?>
                        <li><a href="../sc/clientes.php"
                               target="detalle"><span><?php $Gestor->Incluir('clientes', 'ico') ?>Clientes</span></a>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($Gestor->TienePermiso('09')) { ?>
            <li><a href="#"><span>Gesti&oacute;n de Calidad</span></a>
                <ul>
                    <?php if ($Gestor->TienePermiso('C0')) { ?>
                        <li><a href="../calidad/encabezados.php"
                               target="detalle"><span><?php $Gestor->Incluir('doc', 'ico') ?>Mant. Encabezados</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('91')) { ?>
                        <li><a href="../calidad/clasificaciones.php"
                               target="detalle"><span><?php $Gestor->Incluir('clasi', 'ico') ?>Clasificaciones</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('92')) { ?>
                        <li><a href="../calidad/documentos.php"
                               target="detalle"><span><?php $Gestor->Incluir('doc3', 'ico') ?>Documentos</span></a></li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('94')) { ?>
                        <li><a href="../calidad/acciones.php"
                               target="detalle"><span><?php $Gestor->Incluir('acciones', 'ico') ?>AC/AP</span></a></li>
                    <?php } ?>
                    <?php if ($Gestor->TienePermiso('95')) { ?>
                        <li><a href="#" class="parent"
                               title="Necesidades de formacion"><span><?php $Gestor->Incluir('inducciones', 'ico') ?>Nec. formaci&oacute;n</span></a>
                            <ul>
                                <li><a href="../calidad/inducciones.php"
                                       target="detalle"><span><?php $Gestor->Incluir('clasi', 'ico') ?>Expediente</span></a>
                                </li>
                                <li><a href="../reportes/personal_capacitacionTotal.php?tipo=0"
                                       target="detalle"><span><?php $Gestor->Incluir('reportes', 'ico') ?>Reporte total</span></a>
                                </li>
                                <li><a href="../reportes/personal_capacitacionTotal.php?tipo=1"
                                       target="detalle"><span><?php $Gestor->Incluir('reportes', 'ico') ?>Reporte tipos</span></a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <li><a href="../calidad/referencias.php"
                           target="detalle"><span><?php $Gestor->Incluir('doc2', 'ico') ?>Documentos SGC</span></a></li>
                </ul>
            </li>
        <?php } ?>

        <li class="last"><a href="#"><span>Seguridad</span></a>
            <ul>
                <li><a href="tablon.php?ver=1" target="detalle"><span><?php $Gestor->Incluir('tablon', 'ico') ?>Notificaciones</span></a>
                </li>
                <li><a href="datos.php"
                       target="detalle"><span><?php $Gestor->Incluir('datos', 'ico') ?>Cambiar datos</span></a></li>
                <li><a href="ayuda.php"
                       target="detalle"><span><?php $Gestor->Incluir('help', 'ico') ?>Manual de usuario</span></a></li>
                <li>
                    <hr/>
                </li>
                <?php if ($Gestor->TienePermiso('11')) { ?>
                    <li><a href="perfiles.php" target="detalle"><span><?php $Gestor->Incluir('perfiles', 'ico') ?>Perfiles</span></a>
                    </li>
                <?php } ?>
                <?php if ($Gestor->TienePermiso('12')) { ?>
                    <li><a href="permisos.php" target="detalle"><span><?php $Gestor->Incluir('permisos', 'ico') ?>Permisos</span></a>
                    </li>
                <?php } ?>
                <?php if ($Gestor->TienePermiso('13')) { ?>
                    <li><a href="usuarios.php" target="detalle"><span><?php $Gestor->Incluir('usuarios', 'ico') ?>Usuarios</span></a>
                    </li>
                <?php } ?>
                <?php if ($Gestor->TienePermiso('14')) { ?>
                    <li><a href="#" class="parent"><span><?php $Gestor->Incluir('autorizaciones', 'ico') ?>Autorizaciones</span></a>
                        <ul>
                            <li><a href="aut_cartas.php"
                                   target="detalle"><span><?php $Gestor->Incluir('cartas', 'ico') ?>Cartas/Verificaci&oacute;n</span></a>
                            </li>
                            <li><a href="aut_analisis.php"
                                   target="detalle"><span><?php $Gestor->Incluir('metodos', 'ico') ?>Hojas de c&aacute;lculo</span></a>
                            </li>
                            <li><a href="aut_equipos.php"
                                   target="detalle"><span><?php $Gestor->Incluir('eki_catalogo', 'ico') ?>Equipos</span></a>
                            </li>
                            <li><a href="aut_inventario.php"
                                   target="detalle"><span><?php $Gestor->Incluir('inv_inventario', 'ico') ?>Inventario</span></a>
                            </li>
                        </ul>
                    </li>
                <?php } ?>
                <li><a href="paginas.php" target="detalle"><span><?php $Gestor->Incluir('molecular3', 'ico') ?>Mapa del sitio</span></a>
                </li>
            </ul>
        </li>
    </ul>
</div>
<iframe name="detalle" id="detalle" width="100%" frameborder="0" height="800px" src="<?= $Gestor->Tablon() ?>"></iframe>
<!--document.getElementById("detalle").style.height = screen.availWidth-840 + "px";-->
</body>
</html>