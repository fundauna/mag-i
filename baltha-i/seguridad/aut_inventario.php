<?php
define('__MODULO__', 'seguridad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _aut_inventario();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('m2', 'hr', 'Seguridad :: Autorización de inventario') ?>
<br/>
<table class="radius" align="center">
    <tr>
        <td class="titulo">Perfiles Registrados</td>
    </tr>
    <tr>
        <td align="center">
            <select id="perfiles">
                <?php
                $ROW = $Gestor->Perfiles();
                for ($x = 0; $x < count($ROW); $x++) {
                    echo "<option value='{$ROW[$x]['id']}'>{$ROW[$x]['nombre']}</option>";
                }

                unset($ROW);
                ?>
            </select>&nbsp;<input type="button" id="btn_buscar" value="Buscar" class="boton2"
                                  onClick="PermisosBuscar();">
        </td>
    </tr>
</table>
<br/>
<br>
<table class="radius" align="center">
    <tr>
        <td class="titulo">Tipos de Inventario Disponibles</td>
        <td class="titulo">Tipos de Inventario Asignados</td>
    </tr>
    <tr>
        <td>
            <select id="permisos" style="width:340px;" size="10">
                <?php
                $ROW = $Gestor->Tipos();
                for ($x = 0; $x < count($ROW); $x++) {
                    echo "<option value='{$ROW[$x]['cs']}'>{$ROW[$x]['nombre']}</option>";
                }
                ?>
            </select>
        </td>
        <td id="td_asignados">
            <select id="asignados" style="width:340px;" size="10"></select>
        </td>
    </tr>
    <tr>
        <td><input id="btn_agregar" type="button" value="Agregar" class="boton" onClick="PermisosAgrega();" disabled>
        </td>
        <td><input id="btn_eliminar" type="button" value="Eliminar" class="boton" onClick="PermisosElimina();" disabled>
        </td>
    </tr>
</table>
</body>
</html>