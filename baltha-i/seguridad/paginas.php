<?php
define('__MODULO__', 'seguridad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _paginas();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('m11', 'hr', 'Seguridad :: Mapa del sitio') ?>
<center>
    <div id="container" style="width:650px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>Men&uacute;</th>
                <th>C&oacute;digo</th>
                <th align="left">Ref</th>
                <th>laboratorio</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->Paginas();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><?= $Gestor->Seccion($ROW[$x]['sigla']) ?></td>
                    <td><?= $Gestor->Codigo($ROW[$x]['sigla'], $ROW[$x]['codigo']) ?></td>
                    <td align="left"><?= $ROW[$x]['ref'] ?></td>
                    <td><?= $ROW[$x]['LCC'] == '1' ? 'LCC' : '' ?>  <?= $ROW[$x]['LDP'] == '1' ? 'LDP' : '' ?>  <?= $ROW[$x]['LRE'] == '1' ? 'LRE' : '' ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</center>
</body>
</html>