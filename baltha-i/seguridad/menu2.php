<?php
define('__MODULO__', 'seguridad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _menu2();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('menu', 'css'); ?>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body marginheight="0" marginwidth="0">
<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#000000" height="97px">
    <tr>
        <td background="<?php $Gestor->CargaBanner() ?>" style="padding-right:10px" width="100%">
            <table height="80px" align="right">
                <tr>
                    <td>
                        <div style="position: relative; z-index: 10; color:#FFFFFF; padding:4px; font-size:12px">
                            <div class="radius2"></div>
                            <table>
                                <tr>
                                    <td align="right"><strong>Usuario:</strong></td>
                                    <td><?= $Gestor->Get('UNAME') ?></td>
                                </tr>
                                <tr>
                                    <td align="right"><strong>Id:</strong></td>
                                    <td><?= $Gestor->Get('UCED') ?></td>
                                </tr>
                                <tr>
                                    <td align="right"><strong>Secci&oacute;n:</strong></td>
                                    <td>Clientes</td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="2">
                                        <hr/>
                                        <a href="clientes.php" style="color:#FFFFFF; font-size:10px">[Cerrar Sesi&oacute;n]</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div id="menu">
    <ul class="menu">
        <li><a href="#" class="parent"><span>Solicitudes</span></a>
            <ul>
                <li><a href="../cotizaciones/solicitud_cliente_crear.php"
                       target="detalle"><span><?php $Gestor->Incluir('doc', 'ico') ?>Crear</span></a></li>
                <li><a href="../cotizaciones/solicitud_cliente_buscar.php"
                       target="detalle"><span><?php $Gestor->Incluir('buscar', 'ico') ?>Historial</span></a></li>
                <li>
                    <hr/>
                </li>
                <li><a href="../cotizaciones/cotizacion_cliente_buscar.php"
                       target="detalle"><span><?php $Gestor->Incluir('doc2', 'ico') ?>Cotizaciones</span></a></li>
                <li><a href="../cotizaciones/certificaciones_cliente_buscar.php"
                       target="detalle"><span><?php $Gestor->Incluir('doc3', 'ico') ?>Certificaciones</span></a></li>
                <li>
                    <hr/>
                </li>
                <li><a href="../servicios/muestras_cliente_buscar.php"
                       target="detalle"><span><?php $Gestor->Incluir('muestreo', 'ico') ?>Muestras</span></a></li>
            </ul>
        </li>
        <li><a href="../sc/encuestas_usuario.php" target="detalle" class="parent"><span>Encuestas</span></a></li>
        <li><a href="../sc/quejas_usuario.php" target="detalle" class="parent"><span>Quejas</span></a></li>
        <li class="last"><a href="#"><span>Seguridad</span></a>
            <ul>
                <li><a href="datos2.php"
                       target="detalle"><span><?php $Gestor->Incluir('datos', 'ico') ?>Cambiar clave</span></a></li>
            </ul>
        </li>
    </ul>
</div>
<iframe name="detalle" id="detalle" width="100%" frameborder="0" height="500px"></iframe>
</body>
</html>