<?php
define('__MODULO__', 'seguridad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _usuarios_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Usuario inexistente');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js', 5) ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('m10', 'hr', 'Seguridad :: Detalle Usuarios') ?>
<center>
    <input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
    <input type="hidden" id="id" value="<?= $ROW[0]['id'] ?>"/>
    <table class="radius">
        <tr>
            <td class="titulo" colspan="2">Detalle</td>
        </tr>
        <tr>
            <td>C&eacute;dula:</td>
            <td><input type="text" id="cedula" value="<?= $ROW[0]['cedula'] ?>" size="13" maxlength="15"
                       title="Alfanumérico (9/15)"></td>
        </tr>
        <tr>
            <td>Nombre:</td>
            <td><input type="text" id="nombre" value="<?= $ROW[0]['nombre'] ?>" size="30" maxlength="30"
                       title="Alfanumérico (2/30)"></td>
        </tr>
        <tr>
            <td>Apellido 1:</td>
            <td><input type="text" id="ap1" value="<?= $ROW[0]['ap1'] ?>" size="30" maxlength="30"
                       title="Alfanumérico (4/30)"></td>
        </tr>
        <tr>
            <td>Apellido 2:</td>
            <td><input type="text" id="ap2" value="<?= $ROW[0]['ap2'] ?>" size="30" maxlength="30"
                       title="Alfanumérico (4/30)"></td>
        </tr>
        <tr>
            <td>Correo electr&oacute;nico:</td>
            <td><input type="text" id="email" value="<?= $ROW[0]['email'] ?>" size="30" maxlength="50"
                       title="Alfanumérico (4/50)"></td>
        </tr>
        <tr>
            <td colspan="2">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Perfil:</td>
            <td><select id="perfil">
                    <?php if ($Gestor->EsAdmin()) { ?>
                        <option value='-1'>Administrador</option>
                    <?php } else { ?>
                        <option value=''>N/A</option>
                        <?php
                        $ROW2 = $Gestor->Perfiles();
                        for ($x = 0; $x < count($ROW2); $x++) {
                            ?>
                            <option value='<?= $ROW2[$x]['id'] ?>'
                                    <?= ($ROW[0]['perfil'] == $ROW2[$x]['id']) ? 'selected' : '' ?>><?= $ROW2[$x]['nombre'] ?></option>
                            <?php
                        }

                        unset($ROW2);
                        ?>
                    <?php } ?>
                </select></td>
        </tr>
        <tr>
            <td>Estado:</td>
            <td><select id="estado">
                    <option value='1'>Activo</option>
                    <option value='0' <?= ($ROW[0]['estado'] == '0') ? 'selected' : '' ?>>Inactivo</option>
                    <option value='2' <?= ($ROW[0]['estado'] == '2') ? 'selected' : '' ?>>Cond. Especial</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Login:</td>
            <td><input type="text" id="login" value="<?= str_replace(' ', '', $ROW[0]['login']) ?>" size="12"
                       maxlength="15" title="Alfanumérico (4/10)"></td>
        </tr>
        <tr>
            <td>Clave:</td>
            <td><input type="password" id="clave" value="<?= base64_decode($ROW[0]['clave']) ?>" size="20"
                       maxlength="20" title="Alfanumérico (8/20)"></td>
        </tr>
    </table>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
</center>
</body>
</html>