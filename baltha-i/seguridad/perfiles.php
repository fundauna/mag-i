<?php
define('__MODULO__', 'seguridad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _perfiles();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('m5', 'hr', 'Seguridad :: Perfiles') ?>
<br/>
<table class="radius" align="center">
    <tr>
        <td class="titulo">Perfiles Registrados</td>
    </tr>
    <tr>
        <td align="center">
            <select size="10" style="width:250px" onclick="marca(this);">
                <?php
                $ROW = $Gestor->PerfilesMuestra();
                for ($x = 0; $x < count($ROW); $x++) {
                    echo "<option id='{$ROW[$x]['id']}' nombre='{$ROW[$x]['nombre']}'>{$ROW[$x]['nombre']}</option>";
                }

                unset($ROW);
                ?>
            </select>
        </td>
    </tr>
</table>
<br/>
<table class="radius" align="center" style="width:250px">
    <tr>
        <td class="titulo" colspan="2">Detalle</td>
    </tr>
    <tr style="display:none">
        <td><b>Id:&nbsp;</b></td>
        <td><input type="hidden" id="id"></td>
    </tr>
    <tr>
        <td><b>Nombre:&nbsp;</b></td>
        <td><input type="text" id="nombre" size="20" maxlength="30" title="Alfanumérico (2/30)"></td>
    </tr>
</table>
<br/>
<center>
    <input id="new" type="button" class="boton" value="Limpiar" onclick="desmarca();" disabled>&nbsp;
    <input id="add" type="button" class="boton" value="Agregar" onclick="modificar('I');">&nbsp;
    <input id="mod" type="button" class="boton" value="Modificar" onclick="modificar('M');" disabled>&nbsp;
    <input id="del" type="button" class="boton" value="Eliminar" onclick="modificar('D');" disabled>
</center>
</body>
</html>