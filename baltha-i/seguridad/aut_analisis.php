<?php
define('__MODULO__', 'seguridad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _aut_analisis();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('m0', 'hr', 'Seguridad :: Autorizaci�n de hojas de c�lculo') ?>
<br/>
<table class="radius" align="center">
    <tr>
        <td class="titulo">Usuario</td>
    </tr>
    <tr>
        <td align="center">
            <input type="text" id="tmp" name="tmp" class="lista" readonly onclick="UsuariosLista()"/>
            <input type="hidden" id="usuario" name="usuario"/>
            &nbsp;<input type="button" id="btn_buscar" value="Buscar" class="boton2" onClick="AutoBuscar();">
        </td>
    </tr>
</table>
<br/>
<br>
<table class="radius" align="center">
    <tr>
        <td class="titulo">HC Disponibles</td>
        <td class="titulo">HC Asignadas</td>
    </tr>
    <tr>
        <td>
            <select id="permisos" style="width:400px;" size="10">
                <?php
                $ROW = $Gestor->PermisosMuestra();
                for ($x = 0; $x < count($ROW); $x++) {
                    echo "<option value='{$ROW[$x]['id']}'>{$ROW[$x]['nombre']}</option>";
                }
                ?>
            </select>
        </td>
        <td id="td_asignados">
            <select id="asignados" style="width:400px;" size="10"></select>
        </td>
    </tr>
    <tr>
        <td><input id="btn_agregar" type="button" value="Agregar" class="boton" onClick="PermisosAgrega();" disabled>
        </td>
        <td><input id="btn_eliminar" type="button" value="Eliminar" class="boton" onClick="PermisosElimina();" disabled>
        </td>
    </tr>
</table>
</body>
</html>