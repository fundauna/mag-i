<?php
define('__MODULO__', 'seguridad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _clientes_registra();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js', 5) ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('jquery.maskedinput.min', 'js'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" name="accion" id="accion" value="I"/>
<?php $Gestor->Incluir('l1', 'hr', 'Servicio al Cliente :: Registro de Clientes') ?>
<center>
    <table class="radius" width="60%">
        <tr>
            <td class="titulo" colspan="2">Detalle</td>
        </tr>
        <tr>
            <td>Tipo:</td>
            <td><select id="tipo" name="tipo" onchange="cambiaMascara();">
                    <option value="0">F&iacute;sico</option>
                    <option value="1">Jur&iacute;dico</option>
                    <option value="2">Extranjero</option>
                </select></td>
        </tr>
        <tr>
            <td>Identificaci&oacute;n:</td>
            <td><input type="text" id="id" size="10" maxlength="15" title="Alfanumérico (3/15)"
                       onblur="validaUsuario();"></td>
        </tr>
        <tr>
            <td>Nombre:</td>
            <td><input type="text" id="nombre" size="30" maxlength="100" title="Alfanumérico (2/100)"></td>
        </tr>
        <tr>
            <td>Representante legal:</td>
            <td><input type="text" id="representante" size="30" maxlength="60" title="Alfanumérico (5/60)"></td>
        </tr>
        <tr>
            <td>Tel&eacute;fono:</td>
            <td><input type="text" id="tel" size="10" maxlength="15" title="Alfanumérico (8/15)"></td>
        </tr>
        <tr>
            <td>Correo para notificaciones de sistema:</td>
            <td><input type="text" id="email" maxlength="50"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Contactos:</td>
            <td>
                <table>
                    <tr>
                        <td></td>
                        <td>Nombre</td>
                        <td>Tel&eacute;fono</td>
                        <td>Email</td>
                    </tr>
                    <tbody id="lolo">
                    <tr>
                        <td>1.</td>
                        <td><input type="text" name="contacto" onblur="ValidaLinea()" maxlength="60"/></td>
                        <td><input type="text" name="telefono" size="9" maxlength="15"/></td>
                        <td><input type="text" name="correo" maxlength="50"/></td>
                    </tr>
                    </tbody>
                    <tr>
                        <td colspan="4" align="right"><input type="button" value="+" onclick="ContactoAgregar()"
                                                             title="Agregar línea"/></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Solicitante:</td>
            <td>
                <table>
                    <tr>
                        <td></td>
                        <td>Nombre</td>
                        <td>Tel&eacute;fono</td>
                        <td>Email</td>
                    </tr>
                    <tbody id="lolo1">
                    <tr>
                        <td>1.</td>
                        <td><input type="text" name="solicitante" onblur="ValidaLinea()" maxlength="60"/></td>
                        <td><input type="text" name="soltel" size="9" maxlength="15"/></td>
                        <td><input type="text" name="solemail" maxlength="50"/></td>
                    </tr>
                    </tbody>
                    <tr>
                        <td colspan="4" align="right"><input type="button" value="+" onclick="SolicitanteAgregar()"
                                                             title="Agregar línea"/></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Fax:</td>
            <td><input type="text" id="fax" size="10" maxlength="15" title="Alfanumérico (8/15)"></td>
        </tr>
        <tr>
            <td>Direcci&oacute;n:</td>
            <td><textarea id="direccion" title="Alfanumérico (5/500)"></textarea></td>
        </tr>
    </table>
    <br/>
    <input type="button" id="btn" value="Regresar" onClick="atras()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="button" id="btn" value="Registar" class="boton" onClick="datos()">
</center>
<script>cambiaMascara();</script>
</body>
</html>