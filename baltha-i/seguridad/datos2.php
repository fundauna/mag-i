<?php
define('__MODULO__', 'seguridad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _datos2();
$ROW = $Gestor->ObtieneDatos();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js', 5) ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('p10', 'hr', 'Seguridad :: Cambiar datos de cliente') ?>
<center>
    <table class="radius">
        <tr>
            <td class="titulo" colspan="2">Detalle</td>
        </tr>
        <tr>
            <td>Correo electr&oacute;nico:</td>
            <td><input type="text" id="email" value="<?= $ROW[0]['email'] ?>" size="30" maxlength="50"></td>
        </tr>
        <tr>
            <td colspan="2">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Actualizar clave?</td>
            <td><input type="checkbox" id="checkbox" onClick="clave(this)" value="1"></td>
        </tr>
        <tr>
            <td>Clave actual:</td>
            <td><input type="password" id="actual" disabled></td>
        </tr>
        <tr>
            <td>Nueva clave:</td>
            <td><input type="password" id="nueva" disabled></td>
        </tr>
        <tr>
            <td>Confirmaci&oacute;n:</td>
            <td><input type="password" id="confirmacion" disabled></td>
        </tr>
    </table>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
</center>
</body>
</html>