<?php
define('__MODULO__', 'seguridad');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _tablon();
$ROW = $Gestor->TablonMuestra();
//if(!$ROW) exit;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('m8', 'hr', 'Inicio :: Notificaciones') ?>
<center>
    Tareas pendientes
    <div id="container" style="width:650px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr align="center">
                <th>&nbsp;</th>
                <th>Fecha</th>
                <th>Descripci&oacute;n</th>
                <th>Eliminar</th>
            </tr>
            </thead>
            <tbody>
            <?php
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td>
                        <img onclick="Redirige('<?= $ROW[$x]['id'] ?>', '<?= $ROW[$x]['modulo'] ?>', '<?= $ROW[$x]['pagina'] ?>')"
                             src="<?php $Gestor->Incluir('previa', 'bkg') ?>" title="Ir" class="tab2"/></td>
                    <td><?= $ROW[$x]['fecha1'] ?></td>
                    <td><?= $ROW[$x]['descripcion'] ?></td>
                    <td><img onclick="Elimina('<?= $ROW[$x]['id'] ?>')" src="<?php $Gestor->Incluir('del', 'bkg') ?>"
                             title="Ir" class="tab2"/></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</center>
</body>
</html>