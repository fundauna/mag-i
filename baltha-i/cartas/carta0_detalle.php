<?php
define('__MODULO__', 'cartas');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _carta0_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
$estado = $ROW[0]['estado'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('graficador', 'gfx'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="cs" value="<?= $ROW[0]['cs'] ?>"/>
<input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
<center>
    <?php $Gestor->Incluir('c0', 'hr', 'Equipos :: Carta de Control de Balanzas Anal&iacute;ticas y Granatarias') ?>
    <?= $Gestor->Encabezado('C0000', 'e', 'Carta de Control de Balanzas Anal&iacute;ticas y Granatarias') ?>

    <br>
    <table class="radius" width="600px">
        <tr>
            <td class="titulo" colspan="4">Datos de la balanza y pesas certificadas</td>
        </tr>
        <tr>
            <td><strong>Tipo de balanza:</strong></td>
            <td colspan="3">
                <select id="tipo_bal">
                    <option value="">...</option>
                    <option value="0" <?= ($ROW[0]['tipo_bal'] == 0) ? 'selected' : '' ?>>Anal&iacute;tica</option>
                    <option value="1" <?= ($ROW[0]['tipo_bal'] == 1) ? 'selected' : '' ?>>Granataria</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><strong>Balanza:</strong></td>
            <td><input type="text" id="nom_bal" value="<?= $ROW[0]['nom_bal'] ?>" class="lista" readonly
                       onclick="EquiposLista()">
                <input type="hidden" id="cod_bal" value="<?= $ROW[0]['cod_bal'] ?>"/>
            </td>
            <td><strong>Marca/modelo:</strong></td>
            <td id="marca"><?= $ROW[0]['nombre'] ?></td>
        </tr>
        <tr>
            <td><strong>Rango de la pesada:</strong></td>
            <td colspan="3"><input type="text" id="rango" value="<?= $ROW[0]['rango'] ?>" title="Alfanumérico (2/20)"
                                   maxlength="20"></td>
        </tr>
        <tr>
            <td colspan="4">
                <hr/>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><strong>C&oacute;digo masas certificadas</strong></td>
            <td><strong>Valor nominal (g)</strong></td>
            <td><strong>No. Certificado de las masas</strong></td>
        </tr>
        <tr>
            <td align="right"><strong>Masa 1:</strong></td>
            <td><input type="text" id="pesa1" value="<?= $ROW[0]['pesa1'] ?>" class="lista" readonly
                       onclick="PesasLista(1)"></td>
            <td><input type="text" id="nominal1" class="monto" value="<?= $ROW[0]['nominal1'] ?>" readonly/></td>
            <td><input type="text" id="certificado1" value="<?= $ROW[0]['cert1'] ?>" readonly/></td>
        </tr>
        <tr>
            <td align="right"><strong>Masa 2:</strong></td>
            <td><input type="text" id="pesa2" value="<?= $ROW[0]['pesa2'] ?>" class="lista" readonly
                       onclick="PesasLista(2)"></td>
            <td><input type="text" id="nominal2" class="monto" value="<?= $ROW[0]['nominal2'] ?>" readonly/></td>
            <td><input type="text" id="certificado2" value="<?= $ROW[0]['cert2'] ?>" readonly/></td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="600px">
        <tr>
            <td class="titulo" colspan="7">Datos de la verificaci&oacute;n diaria</td>
        </tr>
        <tr align="center">
            <td width="14%"><strong>Fecha</strong></td>
            <td width="14%"></td>
            <td width="14%"><strong>Masa pesa 1 (g)</strong></td>
            <td width="14%"></td>
            <td width="14%"><strong>Masa pesa 2 (g)</strong></td>
            <td width="25%"><strong>Funcionario</strong></td>
            <td></td>
        </tr>
        <?php
        $ROW = $Gestor->ObtieneLineas();
        for ($x = 0; $x < count($ROW); $x++) {
            ?>
            <tr align="center">
                <td><?= $ROW[$x]['fecha1'] ?></td>
                <td></td>
                <td><?= $ROW[$x]['masaA'] ?></td>
                <td></td>
                <td><?= $ROW[$x]['masaB'] ?></td>
                <td><?= substr($ROW[$x]['nombre'], 0, 1) . substr($ROW[$x]['ap1'], 0, 1) . substr($ROW[$x]['ap2'], 0, 1) ?></td>
                <td><?php if ($estado == '' or $estado == '0') { ?><img src="<?php $Gestor->Incluir('del', 'bkg') ?>"
                                                                        onclick="EliminaLinea('<?= $_GET['ID'] ?>', '<?= $ROW[$x]['fecha2'] ?>')"
                                                                        title="Eliminar" class="tab3" /><?php } ?></td>
            </tr>
            <?php
        }
        ?>
        <?php if ($estado == '' or $estado == '0') { ?>
            <tr>
                <td colspan="7">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td><input type="text" id="fecha" name="fecha" class="fecha" readonly onClick="show_calendar(this.id);">
                </td>
                <td align="center"><input type="text" id="decimalesA" maxlength="1" value="4" onblur="_INT(this);"
                                          class="decimales"></td>
                <td><input type="text" id="masaA" name="masaA" class="monto" onblur="Redondear(this, 'A')"/></td>
                <td align="center"><input type="text" id="decimalesB" maxlength="1" value="4" onblur="_INT(this);"
                                          class="decimales"></td>
                <td><input type="text" id="masaB" name="masaB" class="monto" onblur="Redondear(this, 'B')"/></td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td align="center"></td>
                <td align="center"><font size="-2">Decimales<br/>a utilizar</font></td>
                <td align="right"></td>
                <td align="center"><font size="-2">Decimales<br/>a utilizar</font></td>
                <td align="right" colspan="3"></td>
            </tr>
        <?php } ?>
        <?php
        if ($estado != '' && $estado != '0') {
            $ROW = $Gestor->Historial();
            echo '<tr><td class="titulo" colspan="7">Historial</td></tr>';
            for ($x = 0; $x < count($ROW); $x++) {
                echo "<tr><td colspan='7'><strong>", $Gestor->Accion($ROW[$x]['accion']), "</strong> {$ROW[$x]['nombre']} {$ROW[$x]['ap1']}, {$ROW[$x]['fecha1']}</td></tr>";
            }
        }
        ?>
    </table>
    <br/><?= $Gestor->Encabezado('C0000', 'p', '') ?>
    <!-- BOTONES-->
    <br/>
    <?php if ($estado == '') { ?>
        <input type="button" value="Guardar" class="boton" onClick="datos()">&nbsp;
    <?php } ?>
    <?php if ($estado == '0') { ?>
        <input type="button" value="Guardar" class="boton" onClick="datos()">&nbsp;
        <input type="button" value="Procesar" class="boton" onClick="Procesar('1')">&nbsp;
        <input type="button" value="Anular" class="boton" onClick="Procesar('5')">&nbsp;
    <?php } ?>
    <?php if ($estado == '1' && $Gestor->Aprobar()) { ?>
        <input type="button" value="Anular" class="boton" onClick="Procesar('5')">&nbsp;
        <input type="button" value="Aprobar" class="boton2" onClick="Procesar('2')">&nbsp;
    <?php } ?>
    <?php if ($estado != '') { ?>
        <input type="button" value="Imprimir" class="boton2" onClick="window.print()">
    <?php } ?>
    <!-- BOTONES-->
    <?php if ($_GET['acc'] == 'M') { ?>
        <br/><br/>
        <table class="radius" width="600px">
            <tr>
                <td class="titulo">Gr&aacute;fico</td>
            </tr>
            <tr>
                <td>Tipo: <select id="graf_pesa">
                        <option value="A">Pesa 1</option>
                        <option value="B">Pesa 2</option>
                    </select>&nbsp;
                    Desde: <input type="text" id="graf_desde" class="fecha" readonly onClick="show_calendar(this.id);">&nbsp;
                    Hasta: <input type="text" id="graf_hasta" class="fecha" readonly onClick="show_calendar(this.id);">&nbsp;
                    <input type="button" id="btn" value="Generar" class="boton2" onClick="Generar()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a target="_blank" href="../../caspha-i/docs/cartas/carta0.xlsx">[D&oacute;cimas]</a>
                </td>
            </tr>
            <tr style="display:none;">
                <td id="td_tabla"></td>
            </tr>
            <tr align="center" id="tr_grafico" style="display:none;">
                <td>
                    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </td>
            </tr>
        </table>
    <?php } ?>
</center>
<?= $Gestor->Footer() ?>
</body>
</html>