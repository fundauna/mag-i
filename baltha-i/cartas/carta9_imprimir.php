<?php
define('__MODULO__', 'cartas');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _carta9_imprimir();
$ROW = $Gestor->GetEncabezado();
if (!$ROW) die('Registro inexistente');
$estado = $ROW[0]['estado'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('graficador', 'gfx'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="id" value="<?= $_GET['ID'] ?>"/>
<input type="hidden" id="acc" value="M"/>
<script>ALFA('Por favor espere....');</script>
<center>
    <?php $Gestor->Incluir('c10', 'hr', 'Equipos :: Verificaci&oacute;n de los Cromat&oacute;grafos de gases acoplado a detector de masas') ?>
    <?= $Gestor->Encabezado('C0010', 'e', 'Verificaci&oacute;n de los Cromat&oacute;grafos de gases acoplado a detector de masas') ?>
    <br/>
    <!-- CAMPOS PARA EL CALCULO DE R-->
    <div style="display:none">
        <span id="7pendiente"></span>
        <span id="7intercepto"></span>
        <span id="7r"></span>
        <span id="8pendiente"></span>
        <span id="8intercepto"></span>
        <span id="8r"></span>
        <span id="9pendiente"></span>
        <span id="9intercepto"></span>
        <span id="9r"></span>
        <table id="curva_7">
            <tr>
                <td>X</td>
                <td>Y</td>
            </tr>
            <tr>
                <td id="7xx0"></td>
                <td id="7yy0"></td>
            </tr>
            <tr>
                <td id="7xx1"></td>
                <td id="7yy1"></td>
            </tr>
            <tr>
                <td id="7xx2"></td>
                <td id="7yy2"></td>
            </tr>
            <tr>
                <td id="7xx3"></td>
                <td id="7yy3"></td>
            </tr>
        </table>
        <table id="curva_8">
            <tr>
                <td>X</td>
                <td>Y</td>
            </tr>
            <tr>
                <td id="8xx0"></td>
                <td id="8yy0"></td>
            </tr>
            <tr>
                <td id="8xx1"></td>
                <td id="8yy1"></td>
            </tr>
            <tr>
                <td id="8xx2"></td>
                <td id="8yy2"></td>
            </tr>
            <tr>
                <td id="8xx3"></td>
                <td id="8yy3"></td>
            </tr>
        </table>
        <table id="curva_9">
            <tr>
                <td>X</td>
                <td>Y</td>
            </tr>
            <tr>
                <td id="9xx0"></td>
                <td id="9yy0"></td>
            </tr>
            <tr>
                <td id="9xx1"></td>
                <td id="9yy1"></td>
            </tr>
            <tr>
                <td id="9xx2"></td>
                <td id="9yy2"></td>
            </tr>
            <tr>
                <td id="9xx3"></td>
                <td id="9yy3"></td>
            </tr>
        </table>
    </div>
    <!-- CAMPOS PARA EL CALCULO DE R-->
    <table class="radius" width="90%" style="font-size:12px">
        <tr>
            <td class="titulo" colspan="4">1- Datos Generales</td>
        </tr>
        <tr>
            <td><strong>Equipo:</strong></td>
            <td><?= $ROW[0]['nomequipo'] ?></td>
            <td><strong>Marca/Modelo:</strong></td>
            <td><?= $ROW[0]['marca'] ?></td>
        </tr>
        <tr>
            <td><strong>Columna:</strong></td>
            <td><?= $ROW[0]['nomcolumna'] ?></td>
            <td><strong>C&oacute;digo de Columna:</strong></td>
            <td><?= $ROW[0]['codcolumna'] ?></td>
        </tr>
        <tr>
            <td><strong>Estandar:</strong></td>
            <td><?= $ROW[0]['nomestandar'] ?></td>
            <td><strong>C&oacute;digo de Estandar:</strong></td>
            <td><?= $ROW[0]['codestandar'] ?></td>
        </tr>
        <tr>
            <td colspan="2"><strong>Concentraci&oacute;n Nominal (Disol. Madre):</strong></td>
            <td colspan="2"><?= $ROW[0]['connominal'] ?></td>
        </tr>
    </table>
    <br/>
    <table width="90%" class="radius" style="font-size:12px">
        <thead>
        <tr>
            <td class="titulo" colspan="3">2- Chequeo de Variables del Autotune</td>
        </tr>
        </thead>
        <tbody id="t2" style="display:none"></tbody>
        <tfoot id="f2" style="display:none">
        </tfoot>
    </table>
    <script>Oculta(2);</script>
    <br/>
    <table width="90%" class="radius" style="font-size:12px">
        <thead>
        <tr>
            <td class="titulo" colspan="7">3- Barrido en Modo Scan</td>
        </tr>
        <tr>
            <td></td>
        </tr>
        </thead>
        <tbody id="t3" style="display:none"></tbody>
        <tfoot id="f3" style="display:none">
        </tfoot>
    </table>
    <script>Oculta(3);</script>
    <br/>
    <table width="90%" class="radius" style="font-size:12px">
        <thead>
        <tr>
            <td class="titulo" colspan="4">4- Barrido en Modo SIM</td>
        </tr>
        </thead>
        <tbody id="t4" style="display:none"></tbody>
        <tfoot id="f4" style="display:none">
        <tr>
            <td colspan="4">
                <!-- GRAFICOS -->
                <table width="100%">
                    <tr style="display:none;">
                        <td id="td_tabla1"></td>
                    </tr>
                    <tr align="center" id="tr_grafico1" style="display:none;">
                        <td>
                            <div id="container1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                        </td>
                    </tr>
                </table>
                <!-- GRAFICOS -->
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <!-- GRAFICOS -->
                <table width="100%">
                    <tr style="display:none;">
                        <td id="td_tabla2"></td>
                    </tr>
                    <tr align="center" id="tr_grafico2" style="display:none;">
                        <td>
                            <div id="container2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                        </td>
                    </tr>
                </table>
                <!-- GRAFICOS -->
            </td>
        </tr>
        <tr>
            <td colspan="4" id="sobrepasa1"></td>
        </tr>
        <tr>
            <td colspan="4">
                <!-- GRAFICOS -->
                <table width="100%">
                    <tr style="display:none;">
                        <td id="td_tabla3"></td>
                    </tr>
                    <tr align="center" id="tr_grafico3" style="display:none;">
                        <td>
                            <div id="container3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                        </td>
                    </tr>
                </table>
                <!-- GRAFICOS -->
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <!-- GRAFICOS -->
                <table width="100%">
                    <tr style="display:none;">
                        <td id="td_tabla4"></td>
                    </tr>
                    <tr align="center" id="tr_grafico4" style="display:none;">
                        <td>
                            <div id="container4" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                        </td>
                    </tr>
                </table>
                <!-- GRAFICOS -->
            </td>
        </tr>
        <tr>
            <td colspan="4" id="sobrepasa2"></td>
        </tr>
        <tr>
            <td colspan="4">
                <!-- GRAFICOS -->
                <table width="100%">
                    <tr style="display:none;">
                        <td id="td_tabla5"></td>
                    </tr>
                    <tr align="center" id="tr_grafico5" style="display:none;">
                        <td>
                            <div id="container5" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                        </td>
                    </tr>
                </table>
                <!-- GRAFICOS -->
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <!-- GRAFICOS -->
                <table width="100%">
                    <tr style="display:none;">
                        <td id="td_tabla6"></td>
                    </tr>
                    <tr align="center" id="tr_grafico6" style="display:none;">
                        <td>
                            <div id="container6" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                        </td>
                    </tr>
                </table>
                <!-- GRAFICOS -->
            </td>
        </tr>
        <tr>
            <td colspan="4" id="sobrepasa3"></td>
        </tr>
        </tfoot>
    </table>
    <script>Oculta(4);</script>
    <br/>
    <table width="90%" class="radius" style="font-size:12px">
        <thead>
        <tr>
            <td class="titulo" colspan="5">5- Linealidad del Sistema</td>
        </tr>
        </thead>
        <tbody id="t5" style="display:none"></tbody>
        <tfoot id="f5" style="display:none">
        <tr>
            <td colspan="4">
                <!-- GRAFICOS -->
                <table width="100%">
                    <tr style="display:none">
                        <td id="td_tabla7"></td>
                    </tr>
                    <tr align="center" id="tr_grafico7" style="display:none;">
                        <td>
                            <div id="container7" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                        </td>
                    </tr>
                </table>
                <!-- GRAFICOS -->
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <!-- GRAFICOS -->
                <table width="100%">
                    <tr style="display:none">
                        <td id="td_tabla8"></td>
                    </tr>
                    <tr align="center" id="tr_grafico8" style="display:none;">
                        <td>
                            <div id="container8" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                        </td>
                    </tr>
                </table>
                <!-- GRAFICOS -->
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <!-- GRAFICOS -->
                <table width="100%">
                    <tr style="display:none">
                        <td id="td_tabla9"></td>
                    </tr>
                    <tr align="center" id="tr_grafico9" style="display:none;">
                        <td>
                            <div id="container9" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                        </td>
                    </tr>
                </table>
                <!-- GRAFICOS -->
            </td>
        </tr>
        <tr>
            <td colspan="4" id="sobrepasa4"></td>
        </tr>
        </tfoot>
    </table>
    <script>Oculta(5);</script>
    <br/>
    <table width="90%" class="radius" style="font-size:12px">
        <thead>
        <tr>
            <td class="titulo" colspan="3">6- Observaciones finales</td>
        </tr>
        </thead>
        <tbody id="t6" style="display:none"></tbody>
        <tfoot id="f6" style="display:none">
        </tfoot>
    </table>
    <br/><?= $Gestor->Encabezado('C0010', 'p', '') ?>
    <script>Oculta(6);</script>
    <?php
    if ($estado != '') {
        $ROW = $Gestor->Historial();
        echo '<br /><table width="90%" class="radius" style="font-size:12px"><tr><td class="titulo">Historial</td></tr>';
        for ($x = 0; $x < count($ROW); $x++) {
            echo "<tr><td><strong>", $Gestor->Accion($ROW[$x]['accion']), "</strong> {$ROW[$x]['nombre']} {$ROW[$x]['ap1']}, {$ROW[$x]['fecha1']}</td></tr>";
        }
        echo '</table>';
    }
    ?>
    <br/>
    <input type="button" value="Imprimir" class="boton2" onClick="window.print()">
    <br/>
</center>
<script>BETA();</script>
<?= $Gestor->Footer(2) ?>
</body>
</html>