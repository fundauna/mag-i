<?php
define('__MODULO__', 'cartas');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _carta6_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
$estado = $ROW[0]['estado'];
$creador = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('graficador', 'gfx'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
<center>
    <?php $Gestor->Incluir('c6', 'hr', 'Equipos :: Verificaci&oacute;n de los Espectrofot&oacute;metros de Absorci&oacute;n At&oacute;mica en modalidad') ?>
    <?= $Gestor->Encabezado('C0006', 'e', 'Verificaci&oacute;n de los Espectrofot&oacute;metros de Absorci&oacute;n At&oacute;mica en modalidad') ?>
    <br>
    <table class="radius" width="90%">
        <tr>
            <td class="titulo" colspan="8">Datos generales y par&aacute;metro del espectrofot&oacute;metro</td>
        </tr>
        <tr>
            <td><strong>Consecutivo:</strong></td>
            <td><?= $ROW[0]['codigoalterno'] ?><input type="hidden" id="cs" value="<?= $ROW[0]['cs'] ?>"></td>
            <td></td>
            <td>Modalidad del equipo:</td>
            <td>
                <select id="modalidad" onchange="CambiaEtiquetas(this.value)">
                    <option value="">...</option>
                    <option <?= $ROW[0]['modalidad'] == '1' ? 'selected' : '' ?> value="1">Llama</option>
                    <option <?= $ROW[0]['modalidad'] == '2' ? 'selected' : '' ?> value="2">Horno</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><strong>Fecha:</strong></td>
            <td><input type="text" id="fecha" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fecha'] ?>"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Equipo:</td>
            <td><input type="text" id="nom_bal" value="<?= $ROW[0]['nom_bal'] ?>" class="lista" readonly
                       onclick="EquiposLista()">
                <input type="hidden" id="cod_bal" value="<?= $ROW[0]['cod_bal'] ?>"/>
            </td>
            <td>Marca/modelo:</td>
            <td id="marca"><?= $ROW[0]['marca'] ?></td>
        </tr>
        <tr>
            <td colspan="2" align="center"><strong>Lampara</strong></td>
            <td></td>
            <td>Nombre del est&aacute;ndar:</td>
            <td><input type="text" id="nombre" value="<?= $ROW[0]['nombre'] ?>" maxlength="20"/></td>
        </tr>
        <tr>
            <td>Corriente (mA):</td>
            <td><input type="text" id="corriente" value="<?= $ROW[0]['corriente'] ?>" maxlength="11"></td>
            <td></td>
            <td>Origen y c&oacute;digo del est&aacute;ndar:</td>
            <td><input type="text" id="origen" value="<?= $ROW[0]['origen'] ?>" maxlength="50"/></td>
        </tr>
        <tr>
            <td>Longitud de onda (nm):</td>
            <td><input type="text" id="longitud" value="<?= $ROW[0]['longitud'] ?>" maxlength="11"></td>
            <td></td>
            <td>Est&aacute;ndar tiene certificado de pureza:</td>
            <td><select id="tiene">
                    <option value="0">No</option>
                    <option value="1" <?php if ($ROW[0]['tiene'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select></td>
        </tr>
        <tr>
            <td>Ancho de banda (nm):</td>
            <td><input type="text" id="ancho" value="<?= $ROW[0]['ancho'] ?>" maxlength="11"></td>
            <td></td>
            <td>Concentraci&oacute;n est&aacute;ndar (mg/ml):</td>
            <td><input type="text" id="conc" value="<?= $ROW[0]['conc'] ?>" class="monto" onblur="Redondear(this);">
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="90%">
        <tr>
            <td class="titulo" colspan="7">Curva de calibraci&oacute;n</td>
        </tr>
        <tr align="center">
            <td><strong>Medici&oacute;n</strong></td>
            <td><strong>Blanco</strong></td>
            <td><strong>Absorbancia del<br/>
                    Patr&oacute;n de <a id="label2"></a></strong></td>
            <td><strong>Absorbancia del<br/>
                    Patr&oacute;n de <a id="label3"></a></strong></td>
            <td><strong>Absorbancia del<br/>
                    Patr&oacute;n de <a id="label4"></a></strong></td>
            <td><strong>Absorbancia del<br/>
                    Patr&oacute;n de <a id="label5"></a></strong></td>
            <td><strong>Absorbancia de la<br/>
                    muestra de <a id="label6"></a></strong></td>
        </tr>
        <tbody id="lolo">
        <?php
        $ROW2 = $Gestor->ObtieneLineas();
        for ($x = 0; $x < count($ROW2); $x++) {
            if ($x < 4) {
                ?>
                <tr>
                    <td align="center">&nbsp;&nbsp;&nbsp;<?= ($x + 1) ?></td>
                    <td align="center"><input type="text" name="blanco" id="blanco<?= $x ?>" class="monto"
                                              value="<?= $ROW2[$x]['blanco'] ?>" onblur="Redondear(this, 4);"/></td>
                    <td align="center"><input type="text" name="abs1" id="abs1<?= $x ?>" class="monto"
                                              value="<?= $ROW2[$x]['abs1'] ?>" onblur="Redondear(this, 4);"/></td>
                    <td align="center"><input type="text" name="abs2" id="abs2<?= $x ?>" class="monto"
                                              value="<?= $ROW2[$x]['abs2'] ?>" onblur="Redondear(this, 4);"/></td>
                    <td align="center"><input type="text" name="abs3" id="abs3<?= $x ?>" class="monto"
                                              value="<?= $ROW2[$x]['abs3'] ?>" onblur="Redondear(this, 4);"/></td>
                    <td align="center"><input type="text" name="abs4" id="abs4<?= $x ?>" class="monto"
                                              value="<?= $ROW2[$x]['abs4'] ?>" onblur="Redondear(this, 4);"/></td>
                    <td align="center"><input type="text" name="abs5" id="abs5<?= $x ?>" class="monto"
                                              value="<?= $ROW2[$x]['abs5'] ?>" onblur="Redondear(this, 4);"/></td>
                </tr>
                <?php
            } else {
                ?>
                <tr>
                    <td align="center">&nbsp;&nbsp;&nbsp;<?= ($x + 1) ?></td>
                    <td align="center"></td>
                    <td align="center"></td>
                    <td align="center"></td>
                    <td align="center"></td>
                    <td align="center"></td>
                    <td align="center"><input type="text" name="abs5" id="abs5<?= $x ?>" class="monto"
                                              value="<?= $ROW2[$x]['abs5'] ?>" onblur="Redondear(this, 4);"/></td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>
    <br/>
    <table class="radius" width="90%">
        <tr align="center">
            <td><strong>Patr&oacute;n</strong></td>
            <td><strong>Vol. de Al&iacute;cuota (&micro;L)</strong></td>
            <td><strong>Vol B. Aforado (mL)</strong></td>
            <td><strong>Concentraci&oacute;n (<a id="label7"></a>)</strong></td>
            <td><strong>Absorbancia Promedio</strong></td>
        </tr>
        <?php
        $ROW2 = $Gestor->ObtieneVolumenes();
        for ($x = 0; $x < count($ROW2) - 1; $x++) {
            ?>
            <tr align="center">
                <td><?= ($x + 1) ?></td>
                <td><input type="text" name="volA" id="volA<?= $x ?>" class="monto" value="<?= $ROW2[$x]['volA'] ?>"
                           onblur="Redondear(this, 4);"/></td>
                <td><input type="text" name="volB" id="volB<?= $x ?>" class="monto" value="<?= $ROW2[$x]['volB'] ?>"
                           onblur="Redondear(this, 4);"/></td>
                <td><input type="text" id="volC<?= $x ?>" class="monto" readonly/></td>
                <td><input type="text" id="volP<?= $x ?>" class="monto" readonly/></td>
            </tr>
            <?php
        }
        ?>
        <tr align="center">
            <td>Muestra</td>
            <td><input type="text" name="volA" id="volA<?= $x ?>" class="monto" value="<?= $ROW2[$x]['volA'] ?>"
                       onblur="Redondear(this, 4);"/></td>
            <td><input type="text" name="volB" id="volB<?= $x ?>" class="monto" value="<?= $ROW2[$x]['volB'] ?>"
                       onblur="Redondear(this, 4);"/></td>
            <td><input type="text" id="volC<?= $x ?>" class="monto" readonly/></td>
            <td><input type="text" id="volP<?= $x ?>" class="monto" readonly/></td>
        </tr>
        <tr>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr>
            <td>Absorbancia esperada para la muestra:</td>
            <td><a id="label8"></a></td>
            <td></td>
            <td>Espectrofot&oacute;metro dentro de especificaci&oacute;n:</td>
            <td id="dentro"></td>
        </tr>
        <tr>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr align="center">
            <td><strong>Pendiente (m)</strong></td>
            <td><strong>Intercepto (b)</strong></td>
            <td><strong>r</strong></td>
            <td><strong>Sm</strong></td>
            <td><strong>Sb</strong></td>
        </tr>
        <tr align="center">
            <td id="pendiente"></td>
            <td id="intercepto"></td>
            <td id="r"></td>
            <td id="sm"></td>
            <td id="sb"></td>
        </tr>
        <?php
        if ($estado != '') {
            $ROW = $Gestor->Historial();
            echo '<tr><td class="titulo" colspan="5">Historial</td></tr>';
            for ($x = 0; $x < count($ROW); $x++) {
                echo "<tr><td colspan='5'><strong>", $Gestor->Accion($ROW[$x]['accion']), "</strong> {$ROW[$x]['nombre']} {$ROW[$x]['ap1']}, {$ROW[$x]['fecha1']}</td></tr>";
                if ($ROW[$x]['accion'] == '0') $creador = $ROW[$x]['id'];
            }
        }
        ?>
    </table>
    <table style="display:none">
        <?php
        for ($x = 0; $x < 7; $x++) {
            ?>
            <tr>
                <td id="_CN<?= $x ?>"></td>
                <td id="_AR<?= $x ?>"></td>
                <td id="_A<?= $x ?>"></td>
                <td id="_B<?= $x ?>"></td>
                <td id="_C<?= $x ?>"></td>
                <td id="_D<?= $x ?>"></td>
                <td id="_E<?= $x ?>"></td>
                <td id="_F<?= $x ?>"></td>
                <td id="_G<?= $x ?>"></td>
                <td id="_H<?= $x ?>"></td>
                <td id="_I<?= $x ?>"></td>
            </tr>
        <?php } ?>
    </table>
    <br/><?= $Gestor->Encabezado('C0006', 'p', '') ?>
    <!-- BOTONES-->
    <br/>
    <?php if ($estado == '') { ?>
        <input type="button" value="Guardar" class="boton" onClick="datos()">&nbsp;
    <?php } ?>
    <?php if ($estado == '0' && $Gestor->Modificar($creador)) { ?>
        <input type="button" value="Guardar" class="boton" onClick="datos()">&nbsp;
        <input type="button" value="Procesar" class="boton" onClick="Procesar('1')">&nbsp;
        <input type="button" value="Anular" class="boton" onClick="Procesar('5')">&nbsp;
    <?php } ?>
    <?php if ($estado == '1' && $Gestor->Aprobar()) { ?>
        <input type="button" value="Anular" class="boton" onClick="Procesar('5')">&nbsp;
        <input type="button" value="Aprobar" class="boton2" onClick="Procesar('2')">&nbsp;
    <?php } ?>
    <?php if ($estado != '') { ?>
        <input type="button" value="Imprimir" class="boton2" onClick="window.print()">
    <?php } ?>
    <!-- BOTONES-->
    <?php if ($_GET['acc'] == 'M') { ?>
        <br/><br/>
        <table class="radius" width="90%">
            <tr>
                <td class="titulo">Gr&aacute;fico</td>
            </tr>
            <tr>
                <td>
                    <input type="button" id="btn" value="Generar" class="boton" onClick="Generar()">&nbsp;
                    <a target="_blank" href="../../caspha-i/docs/cartas/carta6.xlsx">[D&oacute;cimas]</a>
                </td>
            </tr>
            <tr style="display:none;">
                <td id="td_tabla"></td>
            </tr>
            <tr align="center" id="tr_grafico" style="display:none;">
                <td>
                    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </td>
            </tr>
        </table>
    <?php } ?>
</center>
<script>
    <?php if($_GET['acc'] == 'I'){?>
    SolicitudAgregar(1);
    SolicitudAgregar(1);
    SolicitudAgregar(1);
    SolicitudAgregar(2);
    SolicitudAgregar(2);
    SolicitudAgregar(2);
    SolicitudAgregar(2);
    SolicitudAgregar(2);
    SolicitudAgregar(2);
    <?php }else{ ?>
    CalculaTotal();
    <?php } ?>
</script>
<?= $Gestor->Footer(2) ?>
</body>
</html>