<?php
define('__MODULO__', 'cartas');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _carta9_detalle();
$ROW = $Gestor->GetEncabezado();
if (!$ROW) die('Registro inexistente');
$estado = $ROW[0]['estado'];
$creador = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('estilo', 'js'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('graficador', 'gfx'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<script>
    img1 = '<?php $Gestor->Incluir('closed', 'bkg')?>';
    img2 = '<?php $Gestor->Incluir('downboxed', 'bkg')?>';
</script>
<center>
    <?php $Gestor->Incluir('c9', 'hr', 'Equipos :: Verificaci&oacute;n de los Cromat&oacute;grafos de gases acoplado a detector de masas') ?>
    <?= $Gestor->Encabezado('C0009', 'e', 'Verificaci&oacute;n de los Cromat&oacute;grafos de gases acoplado a detector de masas') ?>
    <input type="hidden" id="acc" value="<?= $_GET['acc'] ?>"/>
    <input type="hidden" id="id" value="<?= $_GET['ID'] ?>"/>
    <br/>
    <table class="radius" width="90%" style="font-size:12px">
        <tr>
            <td class="titulo" colspan="4">1- Datos Generales</td>
        </tr>
        <tr>
            <td><strong>Consecutivo:</strong></td>
            <td><?= $_GET['ID'] ?><input type="hidden" id="cs" value="<?= $_GET['ID'] ?>"></td>
            <td><strong>Fecha:</strong></td>
            <td><input type="text" id="fecha" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fecha'] ?>"></td>
        </tr>
        <tr>
            <td><strong>Equipo</strong></td>
            <td><input type="text" id="nomequipo" value="<?= $ROW[0]['nomequipo'] ?>" class="lista" readonly
                       onclick="EquiposLista()">
                <input type="hidden" id="equipo" value="<?= $ROW[0]['equipo'] ?>"/></td>
            <td><strong>Marca/Modelo:</strong></td>
            <td id="marca"><?= $ROW[0]['marca'] ?></td>
        </tr>
        <tr>
            <td><strong>Columna</strong></td>
            <td><input type="text" id="nomColumna" value="<?= $ROW[0]['nomcolumna'] ?>" class="lista" readonly
                       onclick="ColumnaLista()">
                <input type="hidden" id="idColumna" value="<?= $ROW[0]['idcolumna'] ?>"/></td>
            <td><strong>C&oacute;digo de Columna</strong></td>
            <td id="codColumna"><?= $ROW[0]['codcolumna'] ?></td>
        </tr>
        <tr>
            <td><strong>Estandar</strong></td>
            <td><input type="text" id="nomEstandar" value="<?= $ROW[0]['nomestandar'] ?>" class="lista" readonly
                       onclick="EstandarLista()">
                <input type="hidden" id="idEstandar" value="<?= $ROW[0]['idestandar'] ?>"/></td>
            <td><strong>C&oacute;digo de Estandar</strong></td>
            <td id="codEstandar"><?= $ROW[0]['codestandar'] ?></td>
        </tr>
        <tr>
            <td colspan="2"><strong>Concentraci&oacute;n Nominal (Disol. Madre):</strong></td>
            <td colspan="2"><input type="text" id="connominal" value="<?= $ROW[0]['connominal'] ?>" size="50"
                                   maxlength="50"></td>
        </tr>
        <tr>
            <td align="center" colspan="4"><br/><input type="button" id="btn1" value="Actualizar" class="boton"
                                                       onClick="Paso1()"></td>
        </tr>
    </table>
    <br/>
    <table width="90%" class="radius" style="font-size:12px">
        <thead>
        <tr>
            <td class="titulo" colspan="3"><img id="i_2" src="<?= $Gestor->Incluir('closed', 'bkg') ?>"
                                                onclick="Oculta(2)" title="Mostrar/Ocultar"
                                                style="vertical-align:baseline;cursor:pointer"/>&nbsp;2- Chequeo de
                Variables del Autotune
            </td>
        </tr>
        </thead>
        <tbody id="t2" style="display:none"></tbody>
        <tfoot id="f2" style="display:none">
        <tr>
            <td align="center" colspan="3"><br/><input type="button" id="btn2" value="Actualizar" class="boton"
                                                       onClick="Paso2()" <?php if ($_GET['acc'] == 'I') echo 'disabled'; ?>>
            </td>
        </tr>
        </tfoot>
    </table>
    <br/>
    <table width="90%" class="radius" style="font-size:12px">
        <thead>
        <tr>
            <td class="titulo" colspan="7"><img id="i_3" src="<?= $Gestor->Incluir('closed', 'bkg') ?>"
                                                onclick="Oculta(3)" title="Mostrar/Ocultar"
                                                style="vertical-align:baseline;cursor:pointer"/>&nbsp;3- Barrido en Modo
                Scan
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
        </thead>
        <tbody id="t3" style="display:none"></tbody>
        <tfoot id="f3" style="display:none">
        <tr>
            <td align="center" colspan="7"><br/><input type="button" id="btn3" value="Actualizar" class="boton"
                                                       onClick="Paso3()" <?php if ($_GET['acc'] == 'I') echo 'disabled'; ?>>
            </td>
        </tr>
        </tfoot>
    </table>
    <br/>
    <table width="90%" class="radius" style="font-size:12px">
        <thead>
        <tr>
            <td class="titulo" colspan="4"><img id="i_4" src="<?= $Gestor->Incluir('closed', 'bkg') ?>"
                                                onclick="Oculta(4)" title="Mostrar/Ocultar"
                                                style="vertical-align:baseline;cursor:pointer"/>&nbsp;4- Barrido en Modo
                SIM
            </td>
        </tr>
        </thead>
        <tbody id="t4" style="display:none"></tbody>
        <tfoot id="f4" style="display:none">
        <tr>
            <td align="center" colspan="4"><br/><input type="button" id="btn4" value="Actualizar" class="boton"
                                                       onClick="Paso4()" <?php if ($_GET['acc'] == 'I') echo 'disabled'; ?>>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <!-- GRAFICOS -->
                <table class="radius" width="100%">
                    <tr>
                        <td class="titulo">Gr&aacute;fico</td>
                    </tr>
                    <tr>
                        <td>Tipo: <select id="graf_pesa">
                                <option value="A">TR n-Undecano</option>
                                <option value="B">&Aacute;rea n-Undecano</option>
                                <option value="C">TR 2,6-Dimetilfenol</option>
                                <option value="D">&Aacute;rea 2,6-Dimetilfenol</option>
                                <option value="E">TR 2,6-Dimetilanilina</option>
                                <option value="F">&Aacute;rea 2,6-Dimetilanilina</option>
                            </select>&nbsp;
                            <input type="button" id="btn" value="Generar" class="boton" onClick="Generar()">
                        </td>
                    </tr>
                    <tr style="display:none;">
                        <td id="td_tabla"></td>
                    </tr>
                    <tr align="center" id="tr_grafico" style="display:none;">
                        <td>
                            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                        </td>
                    </tr>
                </table>
                <!-- GRAFICOS -->
            </td>
        </tr>
        </tfoot>
    </table>
    <br/>
    <table width="90%" class="radius" style="font-size:12px">
        <thead>
        <tr>
            <td class="titulo" colspan="5"><img id="i_5" src="<?= $Gestor->Incluir('closed', 'bkg') ?>"
                                                onclick="Oculta(5)" title="Mostrar/Ocultar"
                                                style="vertical-align:baseline;cursor:pointer"/>&nbsp;5- Linealidad del
                Sistema
            </td>
        </tr>
        </thead>
        <tbody id="t5" style="display:none"></tbody>
        <tfoot id="f5" style="display:none">
        <tr>
            <td align="center" colspan="5"><br/><input type="button" id="btn5" value="Actualizar" class="boton"
                                                       onClick="Paso5()" <?php if ($_GET['acc'] == 'I') echo 'disabled'; ?>>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <!-- GRAFICOS -->
                <table class="radius" width="100%">
                    <tr>
                        <td class="titulo">Gr&aacute;fico</td>
                    </tr>
                    <tr>
                        <td>Tipo: <select id="graf_pesa2">
                                <option value="G">Curva n-Undecano</option>
                                <option value="H">Curva 2,6-Dimetilfenol</option>
                                <option value="I">Curva 2,6-Dimetilanilina</option>
                            </select>&nbsp;
                            <input type="button" id="btn" value="Generar" class="boton" onClick="Generar2()">
                        </td>
                    </tr>
                    <tr style="display:none;">
                        <td id="td_tabla2"></td>
                    </tr>
                    <tr align="center" id="tr_grafico2" style="display:none;">
                        <td>
                            <div id="container2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                        </td>
                    </tr>
                </table>
                <!-- GRAFICOS -->
            </td>
        </tr>
        </tfoot>
    </table>
    <?php
    if ($estado != '') {
        $ROW = $Gestor->Historial();
        echo '<br /><table width="90%" class="radius" style="font-size:12px"><tr><td class="titulo">Historial</td></tr>';
        for ($x = 0; $x < count($ROW); $x++) {
            echo "<tr><td><strong>", $Gestor->Accion($ROW[$x]['accion']), "</strong> {$ROW[$x]['nombre']} {$ROW[$x]['ap1']}, {$ROW[$x]['fecha1']}</td></tr>";
        }
        if ($estado == '0' && $Gestor->Modificar($creador)) echo '<tr><td><strong>Agregar involucrado:</strong> <input type="text" id="tmp" name="tmp" class="lista"readonly onclick="UsuariosLista()" />
	<input type="hidden" id="usuario" name="usuario" />&nbsp;<input type="button" value="+" class="boton" onclick="AgregaInvolucrado()" /></td></tr>';
        echo '</table>';
    }
    ?>
    <br/><?= $Gestor->Encabezado('C0009', 'p', '') ?>
    <br/>
    <!-- BOTONES-->
    <?php if ($estado == '0' && $Gestor->Modificar($creador)) { ?>
        <input type="button" value="Procesar" class="boton" onClick="Procesar('1')">&nbsp;
        <input type="button" value="Anular" class="boton" onClick="Procesar('5')">&nbsp;
    <?php } ?>
    <?php if ($estado == '1' && $Gestor->Aprobar()) { ?>
        <input type="button" value="Anular" class="boton" onClick="Procesar('5')">&nbsp;
        <input type="button" value="Aprobar" class="boton2" onClick="Procesar('2')">&nbsp;
    <?php } ?>
    <?php if ($estado != '') { ?>
        <input type="button" value="Imprimir" class="boton2" onClick="VersionImprimible()">
    <?php } ?>
    <!-- BOTONES-->
    <br/>
</center>
</body>
</html>