<?php
define('__MODULO__', 'cartas');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _carta7_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
$estado = $ROW[0]['estado'];
$creador = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('graficador', 'gfx'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
<center>
    <?php $Gestor->Incluir('c7', 'hr', 'Equipos :: Verificaci&oacute;n de los Cromat&oacute;grafos L&iacute;quidos con Detector UV-Vis') ?>
    <?= $Gestor->Encabezado('C0007', 'e', 'Verificaci&oacute;n de los Cromat&oacute;grafos L&iacute;quidos con Detector UV-Vis') ?>
    <br>
    <table class="radius" width="90%">
        <input type="hidden" id="fecha_em" name="fecha_em" value="<?= $ROW[0]['fecha_em'] ?>">
        <tr>
            <td class="titulo" colspan="5">Datos generales</td>
        </tr>
        <tr>
            <td>Nombre del est&aacute;ndar:</td>
            <td><input type="text" id="nombre" value="<?= $ROW[0]['nombre'] ?>" maxlength="20"/></td>
            <td></td>
            <td>Masa del est&aacute;ndar (mg):</td>
            <td><input type="text" id="masa" value="<?= $ROW[0]['masa'] ?>" class="monto"
                       onblur="_FLOAT(this);CalculaTotal();"/></td>
        </tr>
        <tr>
            <td>Origen y c&oacute;digo del est&aacute;ndar:</td>
            <td><input type="text" id="origen" value="<?= $ROW[0]['origen'] ?>" maxlength="50"/></td>
            <td></td>
            <td>Volumen Bal&oacute;n aforado (mL):</td>
            <td><select id="aforado" onchange="CalculaTotal();">
                    <option value="">...</option>
                    <option value="10" <?php if ($ROW[0]['aforado'] == '10') echo 'selected'; ?>>10</option>
                    <option value="25" <?php if ($ROW[0]['aforado'] == '25') echo 'selected'; ?>>25</option>
                    <option value="50" <?php if ($ROW[0]['aforado'] == '50') echo 'selected'; ?>>50</option>
                    <option value="100 <?php if ($ROW[0]['aforado'] == '100') echo 'selected'; ?>">100</option>
                    <option value="200" <?php if ($ROW[0]['aforado'] == '200') echo 'selected'; ?>>200</option>
                    <option value="250" <?php if ($ROW[0]['aforado'] == '250') echo 'selected'; ?>>250</option>
                    <option value="500" <?php if ($ROW[0]['aforado'] == '500') echo 'selected'; ?>>500</option>
                    <option value="1000" <?php if ($ROW[0]['aforado'] == '1000') echo 'selected'; ?>>1000</option>
                    <option value="2000" <?php if ($ROW[0]['aforado'] == '2000') echo 'selected'; ?>>2000</option>
                </select></td>
        </tr>
        <tr>
            <td>Est&aacute;ndar tiene certificado de pureza:</td>
            <td><select id="tiene">
                    <option value="0">No</option>
                    <option value="1" <?php if ($ROW[0]['tiene'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select></td>
            <td></td>
            <td>Concentraci&oacute;n est&aacute;ndar (mg/mL):</td>
            <td id="conce"></td>
        </tr>
        <tr>
            <td>Pureza del est&aacute;ndar(%):</td>
            <td colspan="2"><input type="text" id="pureza" value="<?= $ROW[0]['pureza'] ?>" class="monto"
                                   onblur="_FLOAT(this);CalculaTotal();"/></td>
            <td><strong>Consecutivo:</strong></td>
            <td><?= $ROW[0]['codigoalterno'] ?><input type="hidden" id="cs" value="<?= $ROW[0]['cs'] ?>"></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"></td>
            <td><strong>Fecha:</strong></td>
            <td><input type="text" id="fecha" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fecha'] ?>"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="90%">
        <tr>
            <td class="titulo" colspan="6">Condiciones cromatogr&aacute;ficas</td>
        </tr>
        <td><strong>Equipo:</strong></td>
        <td><input type="text" id="nom_bal" value="<?= $ROW[0]['nom_bal'] ?>" class="lista" readonly
                   onclick="EquiposLista()">
            <input type="hidden" id="cod_bal" value="<?= $ROW[0]['cod_bal'] ?>"/>
        </td>
        <td><strong>Marca/Modelo:</strong></td>
        <td id="marca"><?= $ROW[0]['marca'] ?></td>
        <td><strong>Volumen de Inyecci&oacute;n (&mu;L):</strong></td>
        <td><input type="text" id="inyeccion" value="<?= $ROW[0]['inyeccion'] ?>" class="monto"
                   onblur="_FLOAT(this);CalculaTotal();"/></td>
        </tr>
        <tr>
            <td colspan="6">&nbsp;</td>
        </tr>
        <tr align="center">
            <td colspan="3"><strong>Fase m&oacute;vil (l&iacute;quida)</strong></td>
            <td></td>
            <td colspan="2"><strong>Inyector</strong></td>
        </tr>
        <tr>
            <td>Tipo de bomba:</td>
            <td colspan="2"><select id="tipo_bomba">
                    <option value="">N/A</option>
                    <option value="0" <?php if ($ROW[0]['tipo_bomba'] == '0') echo 'selected'; ?>>Binaria</option>
                    <option value="1" <?php if ($ROW[0]['tipo_bomba'] == '1') echo 'selected'; ?>>Cuartenaria</option>
                </select></td>
            <td></td>
            <td>Tipo:</td>
            <td><select id="tipo_inyector">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['tipo_inyector'] == '0') echo 'selected'; ?>>COC</option>
                    <option value="1" <?php if ($ROW[0]['tipo_inyector'] == '1') echo 'selected'; ?>>Capilar</option>
                    <option value="2" <?php if ($ROW[0]['tipo_inyector'] == '2') echo 'selected'; ?>>Empacado</option>
                    <option value="3" <?php if ($ROW[0]['tipo_inyector'] == '3') echo 'selected'; ?>>PTV</option>
                </select></td>
        </tr>
        <tr>
            <td>Eluente(s):</td>
            <td colspan="2"><input type="text" id="eluente" value="<?= $ROW[0]['eluente'] ?>"></td>
            <td></td>
            <td>Temperatura (&deg;C):</td>
            <td><input type="text" id="temp1" value="<?= $ROW[0]['temp1'] ?>" class="monto" onblur="_FLOAT(this);"/>
            </td>
        </tr>
        <tr>
            <td><strong>Canal</strong></td>
            <td><strong>Flujo (mL/min)</strong></td>
            <td><strong>Presi&oacute;n (psi)</strong></td>
            <td></td>
            <td>Modo de inyecci&oacute;n:</td>
            <td><select id="modo">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['modo'] == '0') echo 'selected'; ?>>Split</option>
                    <option value="1" <?php if ($ROW[0]['modo'] == '1') echo 'selected'; ?>>Splitless</option>
                </select></td>
        </tr>
        <tr>
            <td><select id="canalA">
                    <option value="">N/A</option>
                    <option value="0" <?php if ($ROW[0]['canalA'] == '0') echo 'selected'; ?>>A</option>
                    <option value="1" <?php if ($ROW[0]['canalA'] == '1') echo 'selected'; ?>>B</option>
                    <option value="2" <?php if ($ROW[0]['canalA'] == '2') echo 'selected'; ?>>C</option>
                    <option value="3" <?php if ($ROW[0]['canalA'] == '3') echo 'selected'; ?>>D</option>
                </select></td>
            <td><input type="text" id="flujoA" value="<?= $ROW[0]['flujoA'] ?>" class="monto" onblur="_FLOAT(this);"/>
            </td>
            <td><input type="text" id="presiA" value="<?= $ROW[0]['presiA'] ?>" class="monto" onblur="_FLOAT(this);"/>
            </td>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td><select id="canalB">
                    <option value="">N/A</option>
                    <option value="0" <?php if ($ROW[0]['canalB'] == '0') echo 'selected'; ?>>A</option>
                    <option value="1" <?php if ($ROW[0]['canalB'] == '1') echo 'selected'; ?>>B</option>
                    <option value="2" <?php if ($ROW[0]['canalB'] == '2') echo 'selected'; ?>>C</option>
                    <option value="3" <?php if ($ROW[0]['canalB'] == '3') echo 'selected'; ?>>D</option>
                </select></td>
            <td><input type="text" id="flujoB" value="<?= $ROW[0]['flujoB'] ?>" class="monto" onblur="_FLOAT(this);"/>
            </td>
            <td><input type="text" id="presiB" value="<?= $ROW[0]['presiB'] ?>" class="monto" onblur="_FLOAT(this);"/>
            </td>
            <td colspan="3" align="center"><strong>Columna</strong></td>
        </tr>
        <tr>
            <td><select id="canalC">
                    <option value="">N/A</option>
                    <option value="0" <?php if ($ROW[0]['canalC'] == '0') echo 'selected'; ?>>A</option>
                    <option value="1" <?php if ($ROW[0]['canalC'] == '1') echo 'selected'; ?>>B</option>
                    <option value="2" <?php if ($ROW[0]['canalC'] == '2') echo 'selected'; ?>>C</option>
                    <option value="3" <?php if ($ROW[0]['canalC'] == '3') echo 'selected'; ?>>D</option>
                </select></td>
            <td><input type="text" id="flujoC" value="<?= $ROW[0]['flujoC'] ?>" class="monto" onblur="_FLOAT(this);"/>
            </td>
            <td><input type="text" id="presiC" value="<?= $ROW[0]['presiC'] ?>" class="monto" onblur="_FLOAT(this);"/>
            </td>
            <td></td>
            <td>Tipo:</td>
            <td><select id="tipo_col">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['tipo_col'] == '0') echo 'selected'; ?>>Capilar</option>
                    <option value="1" <?php if ($ROW[0]['tipo_col'] == '1') echo 'selected'; ?>>Empacado</option>
                    <option value="2" <?php if ($ROW[0]['tipo_col'] == '2') echo 'selected'; ?>>HPLC</option>
                </select></td>
        </tr>
        <tr>
            <td><select id="canalD">
                    <option value="">N/A</option>
                    <option value="0" <?php if ($ROW[0]['canalD'] == '0') echo 'selected'; ?>>A</option>
                    <option value="1" <?php if ($ROW[0]['canalD'] == '1') echo 'selected'; ?>>B</option>
                    <option value="2" <?php if ($ROW[0]['canalD'] == '2') echo 'selected'; ?>>C</option>
                    <option value="3" <?php if ($ROW[0]['canalD'] == '3') echo 'selected'; ?>>D</option>
                </select></td>
            <td><input type="text" id="flujoD" value="<?= $ROW[0]['flujoD'] ?>" class="monto" onblur="_FLOAT(this);"/>
            </td>
            <td><input type="text" id="presiD" value="<?= $ROW[0]['presiD'] ?>" class="monto" onblur="_FLOAT(this);"/>
            </td>
            <td></td>
            <td>C&oacute;digo:</td>
            <td><input type="text" id="cod_col" value="<?= $ROW[0]['cod_col'] ?>" class="lista" readonly
                       onclick="ColumnaLista()"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td></td>
            <td>Marca/Fase</td>
            <td><textarea id="marcafase" style="width:100%"><?= $ROW[0]['marcafase'] ?></textarea></td>
        </tr>
        <tr>
            <td colspan="3" align="center"><strong>Detector</strong></td>
            <td></td>
            <!--<td>Fecha del empaque:</td>
            <td></td>-->
        </tr>
        <tr>
            <td>Tipo:</td>
            <td colspan="2"><select id="tipo_detector">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['tipo_detector'] == '0') echo 'selected'; ?>>Arreglo de diodos
                    </option>
                    <option value="1" <?php if ($ROW[0]['tipo_detector'] == '1') echo 'selected'; ?>>Conductividad
                    </option>
                    <option value="2" <?php if ($ROW[0]['tipo_detector'] == '2') echo 'selected'; ?>>FID</option>
                    <option value="3" <?php if ($ROW[0]['tipo_detector'] == '3') echo 'selected'; ?>>Masas</option>
                    <option value="4" <?php if ($ROW[0]['tipo_detector'] == '4') echo 'selected'; ?>>Masas-Masas
                    </option>
                </select></td>
            <td></td>
            <td>Temperatura (&deg;C):</td>
            <td><input type="text" id="temp2" value="<?= $ROW[0]['temp2'] ?>" class="monto" onblur="_FLOAT(this);"/>
            </td>
        </tr>
        <tr>
            <td>Temperatura (&deg;C):</td>
            <td colspan="2"><input type="text" id="temp3" value="<?= $ROW[0]['temp3'] ?>" class="monto"
                                   onblur="_FLOAT(this);"/></td>
            <td></td>
            <td>Dimensiones:</td>
            <td><textarea id="dimensiones" style="width:100%"><?= $ROW[0]['dimensiones'] ?></textarea></td>
        </tr>
        <tr>
            <td>Longitud de onda (nm):</td>
            <td colspan="2"><input type="text" id="longitud" value="<?= $ROW[0]['longitud'] ?>" class="monto"
                                   onblur="_FLOAT(this);"/></td>
            <td></td>
            <td rowspan="2">Otros par&aacute;metros:</td>
            <td rowspan="2"><textarea id="otros" style="width:100%"><?= $ROW[0]['otros'] ?></textarea></td>
        </tr>
        <tr>
            <td>Presi&oacute;n (bar):</td>
            <td colspan="2"><input type="text" id="bar" value="<?= $ROW[0]['bar'] ?>" class="monto"
                                   onblur="_FLOAT(this);"/></td>
            <td></td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="90%">
        <tr>
            <td class="titulo" colspan="5">Verificaci&oacute;n del flujo de las bombas</td>
        </tr>
        <tr>
            <td><strong>Medici&oacute;n</strong></td>
            <td><strong>Temperatura (�C)</strong></td>
            <td><strong>Densidad (g/mL)</strong></td>
            <td><strong>Masa (g)</strong></td>
            <td><strong>Volumen (mL)</strong></td>
        </tr>
        <tbody id="lolo">
        <?php
        $ROW2 = $Gestor->ObtieneLineas();
        for ($x = 0; $x < count($ROW2); $x++) {
            ?>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;<?= ($x + 1) ?></td>
                <td><select id="t<?= $x ?>" name="t" onchange="CalculaTotal()">
                        <option value="0" temp="0.99987">0.00</option>
                        <option value="3.98" temp="1" <?php if ($ROW2[$x]['t'] == '3.98') echo 'selected'; ?>>3.98
                        </option>
                        <option value="5" temp="0.99999" <?php if ($ROW2[$x]['t'] == '5') echo 'selected'; ?>>5</option>
                        <option value="10" temp="0.99973" <?php if ($ROW2[$x]['t'] == '10') echo 'selected'; ?>>10
                        </option>
                        <option value="15" temp="0.99913" <?php if ($ROW2[$x]['t'] == '15') echo 'selected'; ?>>15
                        </option>
                        <option value="18" temp="0.99862" <?php if ($ROW2[$x]['t'] == '18') echo 'selected'; ?>>18
                        </option>
                        <option value="20" temp="0.99823" <?php if ($ROW2[$x]['t'] == '20') echo 'selected'; ?>>20
                        </option>
                        <option value="25" temp="0.99707" <?php if ($ROW2[$x]['t'] == '25') echo 'selected'; ?>>25
                        </option>
                        <option value="30" temp="0.99567" <?php if ($ROW2[$x]['t'] == '30') echo 'selected'; ?>>30
                        </option>
                        <option value="35" temp="0.99406" <?php if ($ROW2[$x]['t'] == '35') echo 'selected'; ?>>35
                        </option>
                        <option value="38" temp="0.99299" <?php if ($ROW2[$x]['t'] == '38') echo 'selected'; ?>>38
                        </option>
                        <option value="40" temp="0.99224" <?php if ($ROW2[$x]['t'] == '40') echo 'selected'; ?>>40
                        </option>
                        <option value="45" temp="0.99025" <?php if ($ROW2[$x]['t'] == '45') echo 'selected'; ?>>45
                        </option>
                        <option value="50" temp="0.98807" <?php if ($ROW2[$x]['t'] == '50') echo 'selected'; ?>>50
                        </option>
                        <option value="55" temp="0.98573" <?php if ($ROW2[$x]['t'] == '55') echo 'selected'; ?>>55
                        </option>
                        <option value="60" temp="0.98324" <?php if ($ROW2[$x]['t'] == '60') echo 'selected'; ?>>60
                        </option>
                        <option value="65" temp="0.98059" <?php if ($ROW2[$x]['t'] == '65') echo 'selected'; ?>>65
                        </option>
                        <option value="70" temp="0.97781" <?php if ($ROW2[$x]['t'] == '70') echo 'selected'; ?>>70
                        </option>
                        <option value="75" temp="0.97489" <?php if ($ROW2[$x]['t'] == '75') echo 'selected'; ?>>75
                        </option>
                        <option value="80" temp="0.97183" <?php if ($ROW2[$x]['t'] == '80') echo 'selected'; ?>>80
                        </option>
                        <option value="85" temp="0.96865" <?php if ($ROW2[$x]['t'] == '85') echo 'selected'; ?>>85
                        </option>
                        <option value="90" temp="0.96534" <?php if ($ROW2[$x]['t'] == '90') echo 'selected'; ?>>90
                        </option>
                        <option value="95" temp="0.96192" <?php if ($ROW2[$x]['t'] == '95') echo 'selected'; ?>>95
                        </option>
                        <option value="100" temp="0.95838" <?php if ($ROW2[$x]['t'] == '100') echo 'selected'; ?>>100
                        </option>
                    </select></td>
                <td><input type="text" id="densidad<?= $x ?>" class="monto" readonly/></td>
                <td><input type="text" id="mass<?= $x ?>" name="mass" class="monto" value="<?= $ROW2[$x]['mass'] ?>"
                           onblur="Redondear(this, 4);"/></td>
                <td><input type="text" id="vol<?= $x ?>" class="monto" readonly/></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
        <tr>
            <td colspan="3" align="right"><strong>Promedio:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
            <td id="prom_mass"></td>
            <td id="prom_vol"></td>
        </tr>
        <tr>
            <td colspan="3" align="right"><strong>Desviaci&oacute;n
                    est&aacute;ndar:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
            <td id="desv_mass"></td>
            <td id="desv_vol"></td>
        </tr>
        <tr>
            <td colspan="3" align="right"><strong>% C.V.:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
            <td id="cv_mass"></td>
            <td id="cv_vol"></td>
        </tr>
        <tr>
            <td colspan="5"><strong>**Nota:</strong> El valor nominal del flujo debe estar 5% y el coeficiente de
                variaci�n debe ser menor o igual al 2%
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="90%">
        <tr>
            <td class="titulo" colspan="5">Verificaci&oacute;n de la linealidad del detector</td>
        </tr>
        <tr align="center">
            <td><strong>Patr&oacute;n</strong></td>
            <td><strong>Vol. de Al&iacute;cuota (mL)</strong></td>
            <td><strong>Vol B. Aforado (mL)</strong></td>
            <td><strong>Concentraci&oacute;n (mg/mL)</strong></td>
            <td><strong>&Aacute;rea</strong></td>
        </tr>
        <?php
        $ROW2 = $Gestor->ObtieneDetecciones();
        for ($x = 0; $x < count($ROW2); $x++) {
            ?>
            <tr align="center">
                <td><?= ($x + 1) ?></td>
                <td><select name="vol_ali" id="vol_ali<?= $x ?>" onchange="CalculaTotal()">
                        <option value="">...</option>
                        <option value="0.5" <?php if ($ROW2[$x]['vol_ali'] == '0.5') echo 'selected'; ?>>0.5</option>
                        <option value="1" <?php if ($ROW2[$x]['vol_ali'] == '1') echo 'selected'; ?>>1</option>
                        <option value="2" <?php if ($ROW2[$x]['vol_ali'] == '2') echo 'selected'; ?>>2</option>
                        <option value="3" <?php if ($ROW2[$x]['vol_ali'] == '3') echo 'selected'; ?>>3</option>
                        <option value="4" <?php if ($ROW2[$x]['vol_ali'] == '4') echo 'selected'; ?>>4</option>
                        <option value="5" <?php if ($ROW2[$x]['vol_ali'] == '5') echo 'selected'; ?>>5</option>
                        <option value="6" <?php if ($ROW2[$x]['vol_ali'] == '6') echo 'selected'; ?>>6</option>
                        <option value="7" <?php if ($ROW2[$x]['vol_ali'] == '7') echo 'selected'; ?>>7</option>
                        <option value="8" <?php if ($ROW2[$x]['vol_ali'] == '8') echo 'selected'; ?>>8</option>
                        <option value="9" <?php if ($ROW2[$x]['vol_ali'] == '9') echo 'selected'; ?>>9</option>
                        <option value="10" <?php if ($ROW2[$x]['vol_ali'] == '10') echo 'selected'; ?>>10</option>
                        <option value="12.5" <?php if ($ROW2[$x]['vol_ali'] == '12.5') echo 'selected'; ?>>12.5</option>
                        <option value="15" <?php if ($ROW2[$x]['vol_ali'] == '15') echo 'selected'; ?>>15</option>
                        <option value="20" <?php if ($ROW2[$x]['vol_ali'] == '20') echo 'selected'; ?>>20</option>
                        <option value="25" <?php if ($ROW2[$x]['vol_ali'] == '25') echo 'selected'; ?>>25</option>
                        <option value="50" <?php if ($ROW2[$x]['vol_ali'] == '50') echo 'selected'; ?>>50</option>
                        <option value="100" <?php if ($ROW2[$x]['vol_ali'] == '100') echo 'selected'; ?>>100</option>
                    </select></td>
                <td><select name="vol_afo" id="vol_afo<?= $x ?>" onchange="CalculaTotal()">
                        <option value="">...</option>
                        <option value="10" <?php if ($ROW2[$x]['vol_afo'] == '10') echo 'selected'; ?>>10</option>
                        <option value="25" <?php if ($ROW2[$x]['vol_afo'] == '25') echo 'selected'; ?>>25</option>
                        <option value="50" <?php if ($ROW2[$x]['vol_afo'] == '50') echo 'selected'; ?>>50</option>
                        <option value="100" <?php if ($ROW2[$x]['vol_afo'] == '100') echo 'selected'; ?>>100</option>
                        <option value="200" <?php if ($ROW2[$x]['vol_afo'] == '200') echo 'selected'; ?>>200</option>
                        <option value="250" <?php if ($ROW2[$x]['vol_afo'] == '250') echo 'selected'; ?>>250</option>
                        <option value="500" <?php if ($ROW2[$x]['vol_afo'] == '500') echo 'selected'; ?>>500</option>
                        <option value="1000" <?php if ($ROW2[$x]['vol_afo'] == '1000') echo 'selected'; ?>>1000</option>
                        <option value="2000" <?php if ($ROW2[$x]['vol_afo'] == '2000') echo 'selected'; ?>>2000</option>
                    </select></td>
                <td id="cn<?= $x ?>"></td>
                <td><input type="text" name="area" id="area<?= $x ?>" class="monto" value="<?= $ROW2[$x]['area'] ?>"
                           onblur="_RED(this, 2);CalculaTotal();"/></td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr align="center">
            <td><strong>Pendiente (m)</strong></td>
            <td><strong>Intercepto (b)</strong></td>
            <td><strong>r</strong></td>
            <td><strong>Sm</strong></td>
            <td><strong>Sb</strong></td>
        </tr>
        <tr align="center">
            <td id="pendiente"></td>
            <td id="intercepto"></td>
            <td id="r"></td>
            <td id="sm"></td>
            <td id="sb"></td>
        </tr>
    </table>
    <table style="display: none">
        <?php
        for ($x = 0; $x < 6; $x++) {
            ?>
            <tr>
                <td id="_CN<?= $x ?>"></td>
                <td id="_AR<?= $x ?>"></td>
                <td id="_A<?= $x ?>"></td>
                <td id="_B<?= $x ?>"></td>
                <td id="_C<?= $x ?>"></td>
                <td id="_D<?= $x ?>"></td>
                <td id="_E<?= $x ?>"></td>
                <td id="_F<?= $x ?>"></td>
                <td id="_G<?= $x ?>"></td>
                <td id="_H<?= $x ?>"></td>
                <td id="_I<?= $x ?>"></td>
            </tr>
        <?php } ?>
    </table>
    <br/>
    <table class="radius" width="90%">
        <tr>
            <td class="titulo" colspan="4">Verificaci&oacute;n de la reproducibilidad del inyector</td>
        </tr>
        <tr align="center">
            <td><strong>Patr&oacute;n</strong></td>
            <td><strong>Vol. de Al&iacute;cuota (mL)</strong></td>
            <td><strong>Vol B. Aforado (mL)</strong></td>
            <td><strong>Concentraci&oacute;n (mg/mL)</strong></td>
        </tr>
        <tr align="center">
            <td>1</td>
            <td><select id="pat_ali" onchange="CalculaTotal()">
                    <option value="">...</option>
                    <option value="0.5" <?php if ($ROW[0]['pat_ali'] == '0.5') echo 'selected'; ?>>0.5</option>
                    <option value="1" <?php if ($ROW[0]['pat_ali'] == '1') echo 'selected'; ?>>1</option>
                    <option value="2" <?php if ($ROW[0]['pat_ali'] == '2') echo 'selected'; ?>>2</option>
                    <option value="3" <?php if ($ROW[0]['pat_ali'] == '3') echo 'selected'; ?>>3</option>
                    <option value="4" <?php if ($ROW[0]['pat_ali'] == '4') echo 'selected'; ?>>4</option>
                    <option value="5" <?php if ($ROW[0]['pat_ali'] == '5') echo 'selected'; ?>>5</option>
                    <option value="6" <?php if ($ROW[0]['pat_ali'] == '6') echo 'selected'; ?>>6</option>
                    <option value="7" <?php if ($ROW[0]['pat_ali'] == '7') echo 'selected'; ?>>7</option>
                    <option value="10" <?php if ($ROW[0]['pat_ali'] == '10') echo 'selected'; ?>>10</option>
                    <option value="12.5" <?php if ($ROW[0]['pat_ali'] == '12.5') echo 'selected'; ?>>12.5</option>
                    <option value="15" <?php if ($ROW[0]['pat_ali'] == '15') echo 'selected'; ?>>15</option>
                    <option value="20" <?php if ($ROW[0]['pat_ali'] == '20') echo 'selected'; ?>>20</option>
                    <option value="25" <?php if ($ROW[0]['pat_ali'] == '25') echo 'selected'; ?>>25</option>
                    <option value="50" <?php if ($ROW[0]['pat_ali'] == '50') echo 'selected'; ?>>50</option>
                    <option value="100" <?php if ($ROW[0]['pat_ali'] == '100') echo 'selected'; ?>>100</option>
                </select></td>
            <td><select id="pat_afo" onchange="CalculaTotal()">
                    <option value="">...</option>
                    <option value="10" <?php if ($ROW[0]['pat_afo'] == '10') echo 'selected'; ?>>10</option>
                    <option value="25" <?php if ($ROW[0]['pat_afo'] == '25') echo 'selected'; ?>>25</option>
                    <option value="50" <?php if ($ROW[0]['pat_afo'] == '50') echo 'selected'; ?>>50</option>
                    <option value="100" <?php if ($ROW[0]['pat_afo'] == '100') echo 'selected'; ?>>100</option>
                    <option value="200" <?php if ($ROW[0]['pat_afo'] == '200') echo 'selected'; ?>>200</option>
                    <option value="250" <?php if ($ROW[0]['pat_afo'] == '250') echo 'selected'; ?>>250</option>
                    <option value="500" <?php if ($ROW[0]['pat_afo'] == '500') echo 'selected'; ?>>500</option>
                    <option value="1000" <?php if ($ROW[0]['pat_afo'] == '1000') echo 'selected'; ?>>1000</option>
                    <option value="2000" <?php if ($ROW[0]['pat_afo'] == '2000') echo 'selected'; ?>>2000</option>
                </select></td>
            <td id="pat_con"></td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td><strong>N&uacute;mero de Corrida</strong></td>
            <td><strong>&Aacute;rea</strong></td>
            <td><strong>TR</strong></td>
            <td><strong>Altura</strong></td>
        </tr>
        <tbody id="lolo2">
        <?php
        $ROW2 = $Gestor->ObtieneRepro();
        for ($x = 0; $x < count($ROW2); $x++) {
            ?>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;<?= ($x + 1) ?></td>
                <td><input type="text" id="are<?= $x ?>" name="are" class="monto" value="<?= $ROW2[$x]['are'] ?>"
                           onblur="Redondear(this, 2);"/></td>
                <td><input type="text" id="tr<?= $x ?>" name="tr" class="monto" value="<?= $ROW2[$x]['tr'] ?>"
                           onblur="Redondear(this, 3);"/></td>
                <td><input type="text" id="alt<?= $x ?>" name="alt" class="monto" value="<?= $ROW2[$x]['alt'] ?>"
                           onblur="Redondear(this, 2);"/></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
        <tr>
            <td align="center"><strong>Promedio:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
            <td id="prom_are"></td>
            <td id="prom_tr"></td>
            <td id="prom_alt"></td>
        </tr>
        <tr>
            <td align="center"><strong>Desviaci&oacute;n est&aacute;ndar:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
            <td id="desv_are"></td>
            <td id="desv_tr"></td>
            <td id="desv_alt"></td>
        </tr>
        <tr>
            <td align="center"><strong>% C.V.:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
            <td id="cv_are"></td>
            <td id="cv_tr"></td>
            <td id="cv_alt"></td>
        </tr>
        <tr>
            <td align="center"><strong>L&iacute;mite Superior 3s</strong></td>
            <td id="3s_are"></td>
            <td id="3s_tr"></td>
            <td id="3s_alt"></td>
        </tr>
        <tr>
            <td align="center"><strong>L&iacute;mite Superior 2s</strong></td>
            <td id="2s_are"></td>
            <td id="2s_tr"></td>
            <td id="2s_alt"></td>
        </tr>
        <tr>
            <td align="center"><strong>L&iacute;mite Superior 1s</strong></td>
            <td id="1s_are"></td>
            <td id="1s_tr"></td>
            <td id="1s_alt"></td>
        </tr>
        <tr>
            <td align="center"><strong>L&iacute;mite Inferior 1s</strong></td>
            <td id="1i_are"></td>
            <td id="1i_tr"></td>
            <td id="1i_alt"></td>
        </tr>
        <tr>
            <td align="center"><strong>L&iacute;mite Inferior 2s</strong></td>
            <td id="2i_are"></td>
            <td id="2i_tr"></td>
            <td id="2i_alt"></td>
        </tr>
        <tr>
            <td align="center"><strong>L&iacute;mite Inferior 3s</strong></td>
            <td id="3i_are"></td>
            <td id="3i_tr"></td>
            <td id="3i_alt"></td>
        </tr>
        <?php
        if ($estado != '') {
            $ROW = $Gestor->Historial();
            echo '<tr><td class="titulo" colspan="4">Historial</td></tr>';
            for ($x = 0; $x < count($ROW); $x++) {
                echo "<tr><td colspan='4'><strong>", $Gestor->Accion($ROW[$x]['accion']), "</strong> {$ROW[$x]['nombre']} {$ROW[$x]['ap1']}, {$ROW[$x]['fecha1']}</td></tr>";
                if ($ROW[$x]['accion'] == '0') $creador = $ROW[$x]['id'];
            }
        }
        ?>
    </table>
    <br/><?= $Gestor->Encabezado('C0007', 'p', '') ?>
    <!-- BOTONES-->
    <br/>
    <?php if ($estado == '') { ?>
        <input type="button" value="Guardar" class="boton" onClick="datos()">&nbsp;
    <?php } ?>
    <?php if ($estado == '0' && $Gestor->Modificar($creador)) { ?>
        <input type="button" value="Guardar" class="boton" onClick="datos()">&nbsp;
        <input type="button" value="Procesar" class="boton" onClick="Procesar('1')">&nbsp;
        <input type="button" value="Anular" class="boton" onClick="Procesar('5')">&nbsp;
    <?php } ?>
    <?php if ($estado == '1' && $Gestor->Aprobar()) { ?>
        <input type="button" value="Anular" class="boton" onClick="Procesar('5')">&nbsp;
        <input type="button" value="Aprobar" class="boton2" onClick="Procesar('2')">&nbsp;
    <?php } ?>
    <?php if ($estado != '') { ?>
        <input type="button" value="Imprimir" class="boton2" onClick="window.print()">
    <?php } ?>
    <!-- BOTONES-->
    <?php if ($_GET['acc'] == 'M') { ?>
        <br/><br/>
        <table class="radius" width="90%">
            <tr>
                <td class="titulo">Gr&aacute;fico</td>
            </tr>
            <tr>
                <td>Tipo: <select id="graf_pesa">
                        <option value="A">Curva de calibraci&oacute;n</option>
                        <option value="B">&Aacute;rea</option>
                        <option value="C">TR</option>
                        <option value="D">Altura</option>
                    </select>&nbsp;
                    <input type="button" id="btn" value="Generar" class="boton" onClick="Generar()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a target="_blank" href="../../caspha-i/docs/cartas/carta7.xlsx">[D&oacute;cimas]</a>
                </td>
            </tr>
            <tr style="display:none;">
                <td id="td_tabla"></td>
            </tr>
            <tr align="center" id="tr_grafico" style="display:none;">
                <td>
                    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </td>
            </tr>
        </table>
    <?php } ?>
</center>
<script>
    <?php if($_GET['acc'] == 'I'){?>
    inicio();
    <?php }else{ ?>
    CalculaTotal();
    <?php } ?>
</script>
<?= $Gestor->Footer(2) ?>
</body>
</html>