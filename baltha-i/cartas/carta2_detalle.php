<?php
define('__MODULO__', 'cartas');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _carta2_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
$estado = $ROW[0]['estado'];
$creador = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
<center>
    <?php $Gestor->Incluir('c2', 'hr', 'Equipos :: Verificaci&oacute;n del  pH metro marca Metler Toledo modelo SevenEasy') ?>
    <?= $Gestor->Encabezado('C0002', 'e', 'Verificaci&oacute;n del  pH metro marca Metler Toledo modelo SevenEasy') ?>
    <br>
    <table class="radius" width="600px">
        <tr>
            <td class="titulo" colspan="4">Datos</td>
        </tr>
        <tr>
            <td><strong>Consecutivo:</strong></td>
            <td><?= $ROW[0]['codigoalterno'] ?><input type="hidden" id="cs" value="<?= $ROW[0]['cs'] ?>"></td>
            <td><strong>Fecha:</strong></td>
            <td><input type="text" id="fecha" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fecha'] ?>"></td>
        </tr>
        <tr>
            <td colspan="4">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Equipo:</td>
            <td><input type="text" id="nomequipo" value="<?= $ROW[0]['nomequipo'] ?>" class="lista" readonly
                       onclick="EquiposLista(1)"></td>
            <input type="hidden" id="equipo" value="<?= $ROW[0]['equipo'] ?>"/>
            <td>Marca/modelo:</td>
            <td id="marca"><?= $ROW[0]['marca'] ?></td>
        </tr>
        <tr>
            <td colspan="4">
                <hr/>
            </td>
        </tr>
        <tr align="center">
            <td><strong>Buffer</strong></td>
            <td><strong>Fecha de vencimiento</strong></td>
            <td><strong>N&uacute;mero de lote</strong></td>
            <td><strong>C&oacute;digo del certificado</strong></td>
        </tr>
        <tr align="center">
            <td>pH 4,00</td>
            <td><input type="text" id="fechavence1" name="fechavence1" class="fecha"
                       value="<?= $ROW[0]['fechavence1'] ?>" readonly onClick="show_calendar(this.id);"></td>
            <td><input type="text" id="lote1" value="<?= $ROW[0]['lote1'] ?>"></td>
            <td><input type="text" id="certificado1" value="<?= $ROW[0]['certificado1'] ?>"></td>
        </tr>
        <tr align="center">
            <td>pH 7,00</td>
            <td><input type="text" id="fechavence2" name="fechavence2" class="fecha"
                       value="<?= $ROW[0]['fechavence2'] ?>" readonly onClick="show_calendar(this.id);"></td>
            <td><input type="text" id="lote2" value="<?= $ROW[0]['lote2'] ?>"></td>
            <td><input type="text" id="certificado2" value="<?= $ROW[0]['certificado2'] ?>"></td>
        </tr>
        <tr align="center">
            <td>pH 10,00</td>
            <td><input type="text" id="fechavence3" name="fechavence3" class="fecha"
                       value="<?= $ROW[0]['fechavence3'] ?>" readonly onClick="show_calendar(this.id);"></td>
            <td><input type="text" id="lote3" value="<?= $ROW[0]['lote3'] ?>"/></td>
            <td><input type="text" id="certificado3" value="<?= $ROW[0]['certificado3'] ?>"/></td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="600px">
        <tr>
            <td class="titulo" colspan="5">Resultados de la verificaci&oacute;n</td>
        </tr>
        <tr>
            <td colspan="3" align="right">Verificaci&oacute;n de Electrodo (Buffer 7,00):</td>
            <td align="center"><input type="text" id="electrodo" class="monto" value="<?= $ROW[0]['electrodo'] ?>"
                                      onblur="_RED(this,2);CalculaTotal();"/></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="5">
                <hr/>
            </td>
        </tr>
        <tr align="center">
            <td colspan="2"><strong>N&uacute;mero de lectura</strong></td>
            <td><strong>Buffer pH 4,00</strong></td>
            <td><strong>Buffer pH 7,00</strong></td>
            <td><strong>Buffer pH 10,00</strong></td>
        </tr>
        <?php
        $ROW2 = $Gestor->ObtieneLineas();
        for ($x = 0; $x < count($ROW2); $x++) {
            ?>
            <tr>
                <td colspan="2" align="center"><?= $x + 1 ?></td>
                <td align="center"><input type="text" id="ph4<?= $x + 1 ?>" name="ph4" class="monto"
                                          value="<?= $ROW2[$x]['ph4'] ?>" onblur="CalculaTotal();"/></td>
                <td align="center"><input type="text" id="ph7<?= $x + 1 ?>" name="ph7" class="monto"
                                          value="<?= $ROW2[$x]['ph7'] ?>" onblur="CalculaTotal()"/></td>
                <td align="center"><input type="text" id="ph10<?= $x + 1 ?>" name="ph10" class="monto"
                                          value="<?= $ROW2[$x]['ph10'] ?>" onblur="CalculaTotal();"/></td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td colspan="2" align="center">Promedio</td>
            <td align="center"><input type="text" id="promph4" class="monto" readonly/></td>
            <td align="center"><input type="text" id="promph7" class="monto" readonly/></td>
            <td align="center"><input type="text" id="promph10" class="monto" readonly/></td>
        </tr>
        <tr>
            <td colspan="2" align="center">Desviaci&oacute;n Estandar</td>
            <td align="center"><input type="text" id="desvph4" class="monto" readonly/></td>
            <td align="center"><input type="text" id="desvph7" class="monto" readonly/></td>
            <td align="center"><input type="text" id="desvph10" class="monto" readonly/></td>
        </tr>
        <tr>
            <td colspan="2" align="center">Coeficiente de Variaci&oacute;n</td>
            <td align="center"><input type="text" id="coevarph4" class="monto" readonly/></td>
            <td align="center"><input type="text" id="coevarph7" class="monto" readonly/></td>
            <td align="center"><input type="text" id="coevarph10" class="monto" readonly/></td>
        </tr>
        <tr>
            <td colspan="5">
                <hr/>
            </td>
        </tr>
        <tr align="center">
            <td><strong>Rubro</strong></td>
            <td><strong>L&iacute;mites de aceptaci&oacute;n</strong></td>
            <td><strong>Cumple Buffer pH 4,00</strong></td>
            <td><strong>Cumple Buffer pH 7,00</strong></td>
            <td><strong>Cumple Buffer pH 10,00</strong></td>
        </tr>
        <tr>
            <td>Lectura de pH:</td>
            <td align="center">&plusmn;<input type="text" id="limiteph" size="2" onblur="CalculaTotal();"
                                              value="<?= $ROW[0]['limiteph'] ?>"/></td>
            <td align="center"><input type="text" id="lectph4" class="monto" readonly/></td>
            <td align="center"><input type="text" id="lectph7" class="monto" readonly/></td>
            <td align="center"><input type="text" id="lectph10" class="monto" readonly/></td>
        </tr>
        <tr>
            <td>Desviaci&oacute;n estandar:</td>
            <td align="center">&plusmn;<input type="text" id="limitedesv" size="2" onblur="CalculaTotal();"
                                              value="<?= $ROW[0]['limitedesv'] ?>"/></td>
            <td align="center"><input type="text" id="desvest4" class="monto" readonly/></td>
            <td align="center"><input type="text" id="desvest7" class="monto" readonly/></td>
            <td align="center"><input type="text" id="desvest10" class="monto" readonly/></td>
        </tr>
        <tr>
            <td>Coeficiente de variaci&oacute;n:</td>
            <td align="center">&plusmn;<input type="text" id="limitecoef" size="2" onblur="CalculaTotal();"
                                              value="<?= $ROW[0]['limitecoef'] ?>"/></td>
            <td align="center"><input type="text" id="coeest4" class="monto" readonly/></td>
            <td align="center"><input type="text" id="coeest7" class="monto" readonly/></td>
            <td align="center"><input type="text" id="coeest10" class="monto" readonly/></td>
        </tr>
        <tr>
            <td>Lectura en mV<br/>con buffer pH 7:</td>
            <td align="center">&plusmn;<input type="text" id="limitemv" size="2" onblur="_INT(this);CalculaTotal();"
                                              value="<?= $ROW[0]['limitemv'] ?>"/></td>
            <td align="center"></td>
            <td align="center"><input type="text" id="lectmv7" class="monto" readonly/></td>
            <td align="center"></td>
        </tr>
        <?php
        if ($estado != '') {
            $ROW = $Gestor->Historial();
            echo '<tr><td class="titulo" colspan="5">Historial</td></tr>';
            for ($x = 0; $x < count($ROW); $x++) {
                echo "<tr><td colspan='5'><strong>", $Gestor->Accion($ROW[$x]['accion']), "</strong> {$ROW[$x]['nombre']} {$ROW[$x]['ap1']}, {$ROW[$x]['fecha1']}</td></tr>";
                if ($ROW[$x]['accion'] == '0') $creador = $ROW[$x]['id'];
            }
        }
        ?>
    </table>
    <br/><?= $Gestor->Encabezado('C0002', 'p', '') ?>
    <!-- BOTONES-->
    <br/>
    <?php if ($estado == '') { ?>
        <input type="button" value="Guardar" class="boton" onClick="datos()">&nbsp;
    <?php } ?>
    <?php if ($estado == '0' && $Gestor->Modificar($creador)) { ?>
        <input type="button" value="Guardar" class="boton" onClick="datos()">&nbsp;
        <input type="button" value="Procesar" class="boton" onClick="Procesar('1')">&nbsp;
        <input type="button" value="Anular" class="boton" onClick="Procesar('5')">&nbsp;
    <?php } ?>
    <?php if ($estado == '1' && $Gestor->Aprobar()) { ?>
        <input type="button" value="Anular" class="boton" onClick="Procesar('5')">&nbsp;
        <input type="button" value="Aprobar" class="boton2" onClick="Procesar('2')">&nbsp;
    <?php } ?>
    <?php if ($estado != '') { ?>
        <input type="button" value="Imprimir" class="boton2" onClick="window.print()">
    <?php } ?>
    <!-- BOTONES-->
</center>
<script>CalculaTotal();</script>
<?= $Gestor->Footer(2) ?>
</body>
</html>