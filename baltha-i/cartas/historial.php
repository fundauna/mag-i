<?php
define('__MODULO__', 'cartas');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _historial();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('c12', 'hr', 'Equipos :: Cartas de Control/Verificaci&oacute;n') ?>
<?= $Gestor->Encabezado('C0012', 'e', 'Cartas de Control/Verificaci&oacute;n') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" width="600px">
            <tr>
                <td class="titulo" colspan="6">Filtro</td>
            </tr>
            <tr>
                <td>Desde: <input type="text" id="desde" name="desde" class="fecha" value="<?= $_POST['desde'] ?>"
                                  readonly onClick="show_calendar(this.id);">&nbsp;
                    Hasta: <input type="text" id="hasta" name="hasta" class="fecha" value="<?= $_POST['hasta'] ?>"
                                  readonly onClick="show_calendar(this.id);"></td>
                <td>Tipo:</td>
                <td><select name="tipo" style="width:130px">
                        <?php
                        $ROW = $Gestor->Catalogo();
                        for ($x = 0; $x < count($ROW); $x++) {
                            ?>
                            <option value="<?= $ROW[$x]['id'] ?>" <?php if ($_POST['tipo'] == $ROW[$x]['id']) echo 'selected'; ?>><?= $ROW[$x]['desc'] ?></option>
                        <?php } ?>
                        <option value="" <?php if ($_POST['tipo'] == '') echo 'selected'; ?>>Todos</option>
                    </select></td>
                <td>Estado:</td>
                <td><select name="estado" style="width:100px">
                        <option value="0" <?php if ($_POST['estado'] == '0') echo 'selected'; ?>>Creada</option>
                        <option value="1" <?php if ($_POST['estado'] == '1') echo 'selected'; ?>>Pend. Aprob.</option>
                        <option value="2" <?php if ($_POST['estado'] == '2') echo 'selected'; ?>>Aprobada</option>
                        <option value="5" <?php if ($_POST['estado'] == '5') echo 'selected'; ?>>Anulada</option>
                        <option value="" <?php if ($_POST['estado'] == '') echo 'selected'; ?>>Todos</option>
                    </select></td>
                <td><input type="button" value="Buscar" class="boton2" onclick="CartasBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:850px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>No.</th>
                <th>Fecha</th>
                <th>Equipo</th>
                <th>Tipo</th>
                <th>Estado</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW2 = $Gestor->CartasMuestra();
            for ($x = 0; $x < count($ROW2); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#"
                           onclick="CartaDetalle('<?= $ROW2[$x]['consecutivo'] ?>', '<?= $ROW2[$x]['tipo'] ?>')"><?= $ROW2[$x]['codigoalterno'] ?></a>
                    </td>
                    <td><?= $ROW2[$x]['fecha1'] ?></td>
                    <td><?= $ROW2[$x]['equipo'] ?></td>
                    <td><?= $ROW2[$x]['desc'] ?></td>
                    <td><?= $Gestor->Estado($ROW2[$x]['estado']) ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/>
    Tipo:&nbsp;<select id="tipo2" style="width:160px">
        <?php
        for ($x = 0; $x < count($ROW); $x++) {
            ?>
            <option value="<?= $ROW[$x]['id'] ?>"><?= $ROW[$x]['desc'] ?></option>
        <?php } ?>
    </select>&nbsp;
    <input type="button" value="Agregar" class="boton" onClick="CartasAgrega();">
</center>
<br><?= $Gestor->Encabezado('C0012', 'p', '') ?>
</body>
</html>