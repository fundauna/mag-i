<?php
define('__MODULO__', 'cartas');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _carta1_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
$estado = $ROW[0]['estado'];
$creador = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
<center>
    <?php $Gestor->Incluir('c1', 'hr', 'Equipos :: Verificaci&oacute;n de Balanzas Anal&iacute;ticas y Granatarias') ?>
    <?= $Gestor->Encabezado('C0001', 'e', 'Verificaci&oacute;n de Balanzas Anal&iacute;ticas y Granatarias') ?>
    <br>
    <table class="radius" width="98%">
        <tr>
            <td class="titulo" colspan="8">Datos de la balanza y pesas certificadas</td>
        </tr>
        <tr>
            <td>Tipo de balanza:</td>
            <td>
                <select id="tipo_bal">
                    <option value="">...</option>
                    <option value="0" <?= ($ROW[0]['tipo_bal'] == 0) ? 'selected' : '' ?>>Anal&iacute;tica</option>
                    <option value="1" <?= ($ROW[0]['tipo_bal'] == 1) ? 'selected' : '' ?>>Granataria</option>
                </select>
            </td>
            <td align="center">Decimales</td>
            <td align="center">Certificado<br/>
                masas
            </td>
            <td align="center">C&oacute;digo<br/>
                masas
            </td>
            <td align="center">Valor nominal<br/>(g)</td>
            <td align="center">Valor real<br/>(g)</td>
            <td align="center">Incertidumbre<br/>(g)</td>
        </tr>
        <tr>
            <td>Balanza:</td>
            <td><input type="text" id="nom_bal" value="<?= $ROW[0]['nom_bal'] ?>" class="lista" readonly
                       onclick="EquiposLista()">
                <input type="hidden" id="cod_bal" value="<?= $ROW[0]['cod_bal'] ?>"/>
            </td>
            <td><input type="text" id="decimalesA" maxlength="1" value="<?= substr($ROW[0]['decimales'], 0, 1) ?>"
                       onblur="_INT(this);CalculaTotal();" class="cantidad"></td>
            <td><input type="text" id="cert1" value="<?= $ROW[0]['cert1'] ?>" size="15" readonly/></td>
            <td><input type="text" id="pesa1" value="<?= $ROW[0]['pesa1'] ?>" class="lista" readonly
                       onclick="PesasLista(1)"></td>
            <td><input type="text" id="nominal1" class="monto" value="<?= $ROW[0]['nominal1'] ?>" readonly/></td>
            <td><input type="text" id="real1" class="monto" value="<?= $ROW[0]['real1'] ?>" readonly/></td>
            <td><input type="text" id="inc1" class="monto" value="<?= $ROW[0]['inc1'] ?>" readonly/></td>
        </tr>
        <tr>
            <td>Marca/modelo:</td>
            <td id="marca"><?= $ROW[0]['nombre'] ?></td>
            <td><input type="text" id="decimalesB" maxlength="1" value="<?= substr($ROW[0]['decimales'], 1, 1) ?>"
                       onblur="_INT(this);CalculaTotal();" class="cantidad"></td>
            <td><input type="text" id="cert2" value="<?= $ROW[0]['cert2'] ?>" size="15" readonly/></td>
            <td><input type="text" id="pesa2" value="<?= $ROW[0]['pesa2'] ?>" class="lista" readonly
                       onclick="PesasLista(2)"></td>
            <td><input type="text" id="nominal2" class="monto" value="<?= $ROW[0]['nominal2'] ?>" readonly/></td>
            <td><input type="text" id="real2" class="monto" value="<?= $ROW[0]['real2'] ?>" readonly/></td>
            <td><input type="text" id="inc2" class="monto" value="<?= $ROW[0]['inc2'] ?>" readonly/></td>
        </tr>
        <tr>
            <td>Linealidad</td>
            <td><input type="text" id="linealidad" value="<?= $ROW[0]['linealidad'] ?>" class="monto"
                       onblur="Redondear(this, 0);"></td>
            <td><input type="text" id="decimalesC" maxlength="1" value="<?= substr($ROW[0]['decimales'], 2, 1) ?>"
                       onblur="_INT(this);CalculaTotal();" class="cantidad"></td>
            <td><input type="text" id="cert3" value="<?= $ROW[0]['cert3'] ?>" size="15" readonly/></td>
            <td><input type="text" id="pesa3" value="<?= $ROW[0]['pesa3'] ?>" class="lista" readonly
                       onclick="PesasLista(3)"></td>
            <td><input type="text" id="nominal3" class="monto" value="<?= $ROW[0]['nominal3'] ?>" readonly/></td>
            <td><input type="text" id="real3" class="monto" value="<?= $ROW[0]['real3'] ?>" readonly/></td>
            <td><input type="text" id="inc3" class="monto" value="<?= $ROW[0]['inc3'] ?>" readonly/></td>
        </tr>
        <tr>
            <td>Repetibilidad:</td>
            <td><input type="text" id="repetibilidad" value="<?= $ROW[0]['repetibilidad'] ?>" class="monto"
                       onblur="Redondear(this, 0);"></td>
            <td><input type="text" id="decimalesD" maxlength="1" value="<?= substr($ROW[0]['decimales'], 3, 1) ?>"
                       onblur="_INT(this);CalculaTotal();" class="cantidad"></td>
            <td><input type="text" id="cert4" value="<?= $ROW[0]['cert4'] ?>" size="15" readonly/></td>
            <td><input type="text" id="pesa4" value="<?= $ROW[0]['pesa4'] ?>" class="lista" readonly
                       onclick="PesasLista(4)"></td>
            <td><input type="text" id="nominal4" class="monto" value="<?= $ROW[0]['nominal4'] ?>" readonly/></td>
            <td><input type="text" id="real4" class="monto" value="<?= $ROW[0]['real4'] ?>" readonly/></td>
            <td><input type="text" id="inc4" class="monto" value="<?= $ROW[0]['inc4'] ?>" readonly/></td>
        </tr>
        <tr>
            <td><strong>Consecutivo:</strong></td>
            <td><?= $ROW[0]['codigoalterno'] ?><input type="hidden" id="cs" value="<?= $ROW[0]['cs'] ?>"></td>
            <td colspan="2" align="center">Rango de la pesada:</td>
            <td colspan="2"><input type="text" id="rango" value="<?= $ROW[0]['rango'] ?>" title="Alfanumérico (2/20)"
                                   maxlength="20"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="98%">
        <tr>
            <td class="titulo" colspan="8">Datos de la prueba de linealidad</td>
        </tr>
        <tr align="center">
            <td rowspan="2"><strong>Puntos comprobados</strong></td>
            <td colspan="2"><strong>Indicaci&oacute;n</strong></td>
            <td colspan="2"><strong>Indicaci&oacute;n</strong></td>
            <td rowspan="2"><strong>EMP &plusmn;</strong></td>
            <td colspan="2"><strong>Est&aacute; dentro de especificaci&oacute;n</strong></td>
        </tr>
        <tr align="center">
            <td>Ascendente (g)</td>
            <td>Descendente (g)</td>
            <td>Ascendente (g)</td>
            <td>Descendente (g)</td>
            <td>Ascendente (g)</td>
            <td>Descendente (g)</td>
        </tr>
        <tr>
            <td align="center" id="pto1">...</td>
            <td><input type="text" id="pto1_ind_asc" class="monto" value="<?= $ROW[0]['pto1_ind_asc'] ?>"
                       onblur="Redondear(this, 'A');"/></td>
            <td><input type="text" id="pto1_ind_dsc" class="monto" value="<?= $ROW[0]['pto1_ind_dsc'] ?>"
                       onblur="Redondear(this, 'A');"/></td>
            <td><input type="text" id="pto1_err_asc" class="monto" readonly/></td>
            <td><input type="text" id="pto1_err_dsc" class="monto" readonly/></td>
            <td rowspan="4" id="td_linealidad"></td>
            <td id="pto1_si_asc"></td>
            <td id="pto1_si_dsc"></td>
        </tr>
        <tr>
            <td align="center" id="pto2">...</td>
            <td><input type="text" id="pto2_ind_asc" class="monto" value="<?= $ROW[0]['pto2_ind_asc'] ?>"
                       onblur="Redondear(this, 'B');"/></td>
            <td><input type="text" id="pto2_ind_dsc" class="monto" value="<?= $ROW[0]['pto2_ind_dsc'] ?>"
                       onblur="Redondear(this, 'B');"/></td>
            <td><input type="text" id="pto2_err_asc" class="monto" readonly/></td>
            <td><input type="text" id="pto2_err_dsc" class="monto" readonly/></td>
            <td id="pto2_si_asc"></td>
            <td id="pto2_si_dsc"></td>
        </tr>
        <tr>
            <td align="center" id="pto3">...</td>
            <td><input type="text" id="pto3_ind_asc" class="monto" value="<?= $ROW[0]['pto3_ind_asc'] ?>"
                       onblur="Redondear(this, 'C');"/></td>
            <td><input type="text" id="pto3_ind_dsc" class="monto" value="<?= $ROW[0]['pto3_ind_dsc'] ?>"
                       onblur="Redondear(this, 'C');"/></td>
            <td><input type="text" id="pto3_err_asc" class="monto" readonly/></td>
            <td><input type="text" id="pto3_err_dsc" class="monto" readonly/></td>
            <td id="pto3_si_asc"></td>
            <td id="pto3_si_dsc"></td>
        </tr>
        <tr>
            <td align="center" id="pto4">...</td>
            <td><input type="text" id="pto4_ind_asc" class="monto" value="<?= $ROW[0]['pto4_ind_asc'] ?>"
                       onblur="Redondear(this, 'D');"/></td>
            <td><input type="text" id="pto4_ind_dsc" class="monto" value="<?= $ROW[0]['pto4_ind_dsc'] ?>"
                       onblur="Redondear(this, 'D');"/></td>
            <td><input type="text" id="pto4_err_asc" class="monto" readonly/></td>
            <td><input type="text" id="pto4_err_dsc" class="monto" readonly/></td>
            <td id="pto4_si_asc"></td>
            <td id="pto4_si_dsc"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="98%">
        <tr>
            <td class="titulo" colspan="5">Datos de la prueba de repetibilidad</td>
        </tr>
        <tr>
            <td rowspan="3"><strong>Repetici&oacute;n n</strong></td>
            <td align="center" colspan="4"><strong>Carga aplicada (g)</strong></td>
        </tr>
        <tr align="center">
            <td id="serie1">...</td>
            <td id="serie2">...</td>
            <td id="serie3">...</td>
            <td id="serie4">...</td>
        </tr>
        <tr>
            <td>Indicaci&oacute;n de carga (g)</td>
            <td>Indicaci&oacute;n de carga (g)</td>
            <td>Indicaci&oacute;n de carga (g)</td>
            <td>Indicaci&oacute;n de carga (g)</td>
        </tr>
        <tbody id="lolo">
        <?php
        $ROW2 = $Gestor->ObtieneLineas();
        for ($x = 0; $x < count($ROW2); $x++) {
            ?>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;<?= ($x + 1) ?></td>
                <td><input type="text" id="valA<?= $x ?>" name="valA" class="monto" value="<?= $ROW2[$x]['valA'] ?>"
                           onblur="Redondear(this, 0);"/></td>
                <td><input type="text" id="valB<?= $x ?>" name="valB" class="monto" value="<?= $ROW2[$x]['valB'] ?>"
                           onblur="Redondear(this, 0);"/></td>
                <td><input type="text" id="valC<?= $x ?>" name="valC" class="monto" value="<?= $ROW2[$x]['valC'] ?>"
                           onblur="Redondear(this, 0);"/></td>
                <td><input type="text" id="valD<?= $x ?>" name="valD" class="monto" value="<?= $ROW2[$x]['valD'] ?>"
                           onblur="Redondear(this, 0);"/></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
        <tr>
            <td>Desviaci&oacute;n estandar</td>
            <td id="desv1"></td>
            <td id="desv2"></td>
            <td id="desv3"></td>
            <td id="desv4"></td>
        </tr>
        <tr>
            <td>EMP &plusmn; (g)</td>
            <td id="rep1"></td>
            <td id="rep2"></td>
            <td id="rep3"></td>
            <td id="rep4"></td>
        </tr>
        <tr>
            <td>Est&aacute; dentro de especificaci&oacute;n</td>
            <td id="esta1"></td>
            <td id="esta2"></td>
            <td id="esta3"></td>
            <td id="esta4"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="98%">
        <tr>
            <td class="titulo" colspan="6">Datos de la prueba de excentricidad</td>
        </tr>
        <tr>
            <td>Tipo de platillo:</td>
            <td><select id="platillo">
                    <option value="0">Cuadrado</option>
                    <option value="1" <?= ($ROW[0]['platillo'] == 1) ? 'selected' : '' ?>>Redondo</option>
                </select></td>
            <td>&nbsp;</td>
            <td align="center"><strong>Posici&oacute;n</strong></td>
            <td align="center"><strong>Indicación de carga (g)</strong></td>
            <td align="center" rowspan="2"><strong>X_(i)-X Lectura del centro</strong></td>
        </tr>
        <tr>
            <td>C&oacute;digo de la pesa certificada:</td>
            <td><input type="text" id="pesa5" value="<?= $ROW[0]['pesa5'] ?>" class="lista" readonly
                       onclick="PesasLista(5)"></td>
            <td>&nbsp;</td>
            <td align="center"><strong>0 centro</strong></td>
            <td align="center"><input type="text" id="pos0" class="monto" value="<?= $ROW[0]['pos0'] ?>"
                                      onblur="Redondear(this, 0);"/></td>
        </tr>
        <tr>
            <td>Valor nominal de la pesa (g):</td>
            <td><input type="text" id="nominal5" class="monto" value="<?= $ROW[0]['nominal5'] ?>" readonly/></td>
            <td>&nbsp;</td>
            <td align="center"><strong>1</strong></td>
            <td align="center"><input type="text" id="pos1" class="monto" value="<?= $ROW[0]['pos1'] ?>"
                                      onblur="Redondear(this, 0);"/></td>
            <td align="center" id="lectura1"></td>
        </tr>
        <tr>
            <td>Valor real de la pesa (g):</td>
            <td><input type="text" id="real5" class="monto" value="<?= $ROW[0]['real5'] ?>" readonly/></td>
            <td>&nbsp;</td>
            <td align="center"><strong>2</strong></td>
            <td align="center"><input type="text" id="pos2" class="monto" value="<?= $ROW[0]['pos2'] ?>"
                                      onblur="Redondear(this, 0);"/></td>
            <td align="center" id="lectura2"></td>
        </tr>
        <tr>
            <td>EMP &plusmn; (g)</td>
            <td><input type="text" id="sensibilidad" value="<?= $ROW[0]['sensibilidad'] ?>" class="monto"
                       onblur="Redondear(this, 0);"></td>
            <td>&nbsp;</td>
            <td align="center"><strong>3</strong></td>
            <td align="center"><input type="text" id="pos3" class="monto" value="<?= $ROW[0]['pos3'] ?>"
                                      onblur="Redondear(this, 0);"/></td>
            <td align="center" id="lectura3"></td>
        </tr>
        <tr align="center">
            <td rowspan="4" colspan="3"><?php $Gestor->Incluir('pesas', 'png'); ?></td>
            <td><strong>4</strong></td>
            <td><input type="text" id="pos4" class="monto" value="<?= $ROW[0]['pos4'] ?>" onblur="Redondear(this, 0);"/>
            </td>
            <td id="lectura4"></td>
        </tr>
        <tr align="center">
            <td><strong>0 centro</strong></td>
            <td><input type="text" id="pos5" class="monto" value="<?= $ROW[0]['pos5'] ?>" onblur="Redondear(this, 0);"/>
            </td>
            <td><strong>Mayor diferencia</strong></td>
        </tr>
        <tr align="center">
            <td><strong>X_ Lectura del centro</strong></td>
            <td id="pos6"></td>
            <td id="mayor"></td>
        </tr>
        <tr align="center">
            <td colspan="2"><strong>Est&aacute; dentro de especificaci&oacute;n</strong></td>
            <td id="esta"></td>
        </tr>
        <tr>
            <td colspan="6">
                <hr/>
            </td>
        </tr>
        <tr>
            <td colspan="6"><strong>Observaciones:</strong>&nbsp;<input type="text" id="obs" size="100" maxlength="200"
                                                                        value="<?= $ROW[0]['obs'] ?>"/></td>
        </tr>
        <?php
        if ($estado != '') {
            $ROW = $Gestor->Historial();
            echo '<tr><td class="titulo" colspan="6">Historial</td></tr>';
            for ($x = 0; $x < count($ROW); $x++) {
                echo "<tr><td colspan='6'><strong>", $Gestor->Accion($ROW[$x]['accion']), "</strong> {$ROW[$x]['nombre']} {$ROW[$x]['ap1']}, {$ROW[$x]['fecha1']}</td></tr>";
                if ($ROW[$x]['accion'] == '0') $creador = $ROW[$x]['id'];
            }
        }
        ?>
    </table>
    <br/><?= $Gestor->Encabezado('C0001', 'p', '') ?>
    <!-- BOTONES-->
    <br/>
    <?php if ($estado == '') { ?>
        <input type="button" value="Guardar" class="boton" onClick="datos()">&nbsp;
    <?php } ?>
    <?php if ($estado == '0' && $Gestor->Modificar($creador)) { ?>
        <input type="button" value="Guardar" class="boton" onClick="datos()">&nbsp;
        <input type="button" value="Procesar" class="boton" onClick="Procesar('1')">&nbsp;
        <input type="button" value="Anular" class="boton" onClick="Procesar('5')">&nbsp;
    <?php } ?>
    <?php if ($estado == '1' && $Gestor->Aprobar()) { ?>
        <input type="button" value="Anular" class="boton" onClick="Procesar('5')">&nbsp;
        <input type="button" value="Aprobar" class="boton2" onClick="Procesar('2')">&nbsp;
    <?php } ?>
    <?php if ($estado != '') { ?>
        <input type="button" value="Imprimir" class="boton2" onClick="window.print()">
    <?php } ?>
    <!-- BOTONES-->
</center>
<script>
    <?php if($_GET['acc'] == 'I'){?>
    SolicitudAgregar();
    SolicitudAgregar();
    SolicitudAgregar();
    SolicitudAgregar();
    SolicitudAgregar();
    SolicitudAgregar();
    SolicitudAgregar();
    SolicitudAgregar();
    SolicitudAgregar();
    <?php }else{ ?>
    CalculaTotal();
    <?php } ?>
</script>
<?= $Gestor->Footer(2) ?>
</body>
</html>