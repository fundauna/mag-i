<?php
define('__MODULO__', 'cartas');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _carta4_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
$estado = $ROW[0]['estado'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('graficador', 'gfx'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="cs" value="<?= $ROW[0]['cs'] ?>"/>
<input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
<center>
    <?php $Gestor->Incluir('c4', 'hr', 'Equipos :: Carta de Control de Condiciones Ambientales (Temperatura y Humedad)') ?>
    <?= $Gestor->Encabezado('C0004', 'e', 'Carta de Control de Condiciones Ambientales (Temperatura y Humedad)') ?>
    <br>
    <table class="radius" width="620px">
        <tr>
            <td class="titulo" colspan="5">Datos del termohig&oacute;metro</td>
        </tr>
        <tr>
            <td>C&oacute;digo:</td>
            <td><input type="text" id="cod_ter" value="<?= $ROW[0]['cod_ter'] ?>" class="lista" readonly
                       onclick="TermosLista()"></td>
            <td></td>
            <td>Cuarto:</td>
            <td id="cuarto"><?= $ROW[0]['cuarto'] ?></td>
        </tr>
        <tr>
            <td>L&iacute;mites de Temperatura (&deg;C):</td>
            <td id="limites"><?= $ROW[0]['limite1'], ' a ', $ROW[0]['limite2'] ?></td>
            <td></td>
            <td>L&iacute;mites de Humedad relativa (%):</td>
            <td id="humedad"><?= $ROW[0]['humedad'] ?></td>
        </tr>
        <tr>
            <td colspan="5">
                <hr/>
            </td>
        </tr>
        <tr align="center">
            <td><strong>Indicaci&oacute;n (&deg;C):</strong></td>
            <td><strong>Error de indicaci&oacute;n (&deg;C):</strong></td>
            <td></td>
            <td><strong>Indicaci&oacute;n (% HR):</strong></td>
            <td><strong>Error de indicaci&oacute;n (% HR):</strong></td>
        </tr>
        <tr align="center">
            <td id="indicacion1"><?= $ROW[0]['indicacion1'] ?></td>
            <td id="error1"><?= $ROW[0]['error1'] ?></td>
            <td></td>
            <td id="hr1"><?= $ROW[0]['hr1'] ?></td>
            <td id="err_hr1"><?= $ROW[0]['err_hr1'] ?></td>
        </tr>
        <tr align="center">
            <td id="indicacion2"><?= $ROW[0]['indicacion2'] ?></td>
            <td id="error2"><?= $ROW[0]['error2'] ?></td>
            <td></td>
            <td id="hr2"><?= $ROW[0]['hr2'] ?></td>
            <td id="err_hr2"><?= $ROW[0]['err_hr2'] ?></td>
        </tr>
        <tr align="center">
            <td id="indicacion3"><?= $ROW[0]['indicacion3'] ?></td>
            <td id="error3"><?= $ROW[0]['error3'] ?></td>
            <td></td>
            <td id="hr3"><?= $ROW[0]['hr3'] ?></td>
            <td id="err_hr3"><?= $ROW[0]['err_hr3'] ?></td>
        </tr>
        <tr>
            <td colspan="5">
                <hr/>
            </td>
        </tr>
        <tr>
            <td><strong>Observaciones:</strong></td>
            <td colspan="4"><textarea id="obs" cols="40"><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="620px">
        <tr>
            <td class="titulo" colspan="8">Datos de control</td>
        </tr>
        <tr align="center">
            <td><strong>Fecha</strong></td>
            <td><strong>Hora</strong></td>
            <td><strong>Temp. (&deg;C)</strong></td>
            <td><strong>Temperatura<br/>
                    corregida (&deg;C)</strong></td>
            <td><strong>Humedad<br/>relativa (%)</strong></td>
            <td><strong>Humedad relativa<br/>corregida (%)</strong></td>
            <td><strong>Funcionario</strong></td>
            <td></td>
        </tr>
        <?php
        $ROW = $Gestor->ObtieneLineas();
        for ($x = 0; $x < count($ROW); $x++) {
            ?>
            <tr align="center">
                <td><?= $ROW[$x]['fecha1'] ?></td>
                <td><?= $ROW[$x]['hora'] ?></td>
                <td><?= $ROW[$x]['temp1'] ?></td>
                <td><?= $ROW[$x]['temp2'] ?></td>
                <td><?= $ROW[$x]['hume1'] ?></td>
                <td><?= $ROW[$x]['hume2'] ?></td>
                <td><?= substr($ROW[$x]['nombre'], 0, 1) . substr($ROW[$x]['ap1'], 0, 1) . substr($ROW[$x]['ap2'], 0, 1) ?></td>
                <td><?php if ($estado == '' or $estado == '0') { ?><img src="<?php $Gestor->Incluir('del', 'bkg') ?>"
                                                                        onclick="EliminaLinea('<?= $_GET['ID'] ?>', '<?= $ROW[$x]['fecha1'] ?>', '<?= $ROW[$x]['hora'] ?>')"
                                                                        title="Eliminar" class="tab3" /><?php } ?></td>
            </tr>
            <?php
        }
        ?>
        <?php if ($estado == '' or $estado == '0') { ?>
            <tr>
                <td colspan="8">
                    <hr/>
                </td>
            </tr>
            <tr align="center">
                <td><input type="text" id="fecha" name="fecha" class="fecha" readonly onClick="show_calendar(this.id);">
                </td>
                <td><input type="text" id="hora" name="hora" maxlength="5" size="4"
                           onclick="this.value = hora(this.value);"></td>
                <td><input type="text" id="temp1" name="temp1" class="monto" onblur="_FLOAT(this);CalculaTemp();"/></td>
                <td><input type="text" id="temp2" name="temp2" class="monto" readonly/></td>
                <td><input type="text" id="hume1" name="hume1" class="monto" onblur="_FLOAT(this);CalculaTemp();"/></td>
                <td><input type="text" id="hume2" name="hume2" class="monto" readonly/></td>
                <td colspan="2"></td>
            </tr>
        <?php } ?>
        <?php
        if ($estado != '' && $estado != '0') {
            $ROW = $Gestor->Historial();
            echo '<tr><td class="titulo" colspan="8">Historial</td></tr>';
            for ($x = 0; $x < count($ROW); $x++) {
                echo "<tr><td colspan='8'><strong>", $Gestor->Accion($ROW[$x]['accion']), "</strong> {$ROW[$x]['nombre']} {$ROW[$x]['ap1']}, {$ROW[$x]['fecha1']}</td></tr>";
            }
        }
        ?>
    </table>
    <!-- BOTONES-->
    <br/>
    <?php if ($estado == '') { ?>
        <input type="button" value="Guardar" class="boton" onClick="datos()">&nbsp;
    <?php } ?>
    <?php if ($estado == '0') { ?>
        <input type="button" value="Guardar" class="boton" onClick="datos()">&nbsp;
        <input type="button" value="Procesar" class="boton" onClick="Procesar('1')">&nbsp;
        <input type="button" value="Anular" class="boton" onClick="Procesar('5')">&nbsp;
    <?php } ?>
    <?php if ($estado == '1' && $Gestor->Aprobar()) { ?>
        <input type="button" value="Anular" class="boton" onClick="Procesar('5')">&nbsp;
        <input type="button" value="Aprobar" class="boton2" onClick="Procesar('2')">&nbsp;
    <?php } ?>
    <?php if ($estado != '') { ?>
        <input type="button" value="Imprimir" class="boton2" onClick="window.print()">
    <?php } ?>
    <!-- BOTONES-->
    <?php if ($_GET['acc'] == 'M') { ?>
        <br/><br/>
        <table class="radius" width="620px">
            <tr>
                <td class="titulo" colspan="8">Gr&aacute;fico</td>
            </tr>
            <tr>
                <td>Tipo:</td>
                <td><select id="graf_pesa">
                        <option value="temp2">Temperatura</option>
                        <option value="hume2">Humedad</option>
                    </select></td>
                <td>Desde:</td>
                <td><input type="text" id="graf_desde" class="fecha" readonly onClick="show_calendar(this.id);"></td>
                <td>Hasta:</td>
                <td><input type="text" id="graf_hasta" class="fecha" readonly onClick="show_calendar(this.id);"></td>
                <td style="color:#0066FF">Media:</td>
                <td><input type="text" id="graf_media" class="monto" onblur="_FLOAT(this);" value="0"></a></td>
            </tr>
            <tr>
                <td style="color:#FF0000">L&iacute;mite 1:</td>
                <td><input type="text" id="graf_limite1" class="monto" onblur="_FLOAT(this);" value="0"></td>
                <td style="color:#00FF00">L&iacute;mite 2:</td>
                <td><input type="text" id="graf_limite2" class="monto" onblur="_FLOAT(this);" value="0"></td>
                <td style="color:#00FF00">L&iacute;mite 3:</td>
                <td><input type="text" id="graf_limite3" class="monto" onblur="_FLOAT(this);" value="0">&nbsp;
                <td style="color:#FF0000">L&iacute;mite 4:</td>
                <td><input type="text" id="graf_limite4" class="monto" onblur="_FLOAT(this);" value="0">&nbsp;
            </tr>
            <tr>
                <td align="center" colspan="8"><input type="button" id="btn" value="Generar" class="boton"
                                                      onClick="Generar()"></td>
            </tr>
            <tr style="display:none;">
                <td colspan="8" id="td_tabla"></td>
            </tr>
            <tr align="center" colspan="8" id="tr_grafico" style="display:none;">
                <td colspan="8">
                    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </td>
            </tr>
        </table>
    <?php } ?>
    <br/><?= $Gestor->Encabezado('C0004', 'p', '') ?>
</center>
<?= $Gestor->Footer() ?>
</body>
</html>