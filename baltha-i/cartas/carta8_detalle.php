<?php
define('__MODULO__', 'cartas');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _carta8_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
$estado = $ROW[0]['estado'];
$creador = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('graficador', 'gfx'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
<center>
    <?php $Gestor->Incluir('c8', 'hr', 'Equipos :: Verificaci&oacute;n de los Cromat&oacute;grafos de Gases con Detector FID') ?>
    <?= $Gestor->Encabezado('C0008', 'e', 'Verificaci&oacute;n de los Cromat&oacute;grafos de Gases con Detector FID') ?>
    <br>
    <table class="radius" width="90%">
        <input type="hidden" id="fecha_em" name="fecha_em" value="<?= $ROW[0]['fecha_em'] ?>">
        <tr>
            <td class="titulo" colspan="5">Datos generales</td>
        </tr>
        <tr>
            <td>Nombre del est&aacute;ndar:</td>
            <td><input type="text" id="nombre" value="<?= $ROW[0]['nombre'] ?>" maxlength="20"/></td>
            <td></td>
            <td>Masa del est&aacute;ndar (mg):</td>
            <td><input type="text" id="masa" class="monto" value="<?= $ROW[0]['masa'] ?>"
                       onblur="_FLOAT(this);CalculaTotal();"/></td>
        </tr>
        <tr>
            <td>Origen y c&oacute;digo del est&aacute;ndar:</td>
            <td><input type="text" id="origen" value="<?= $ROW[0]['origen'] ?>" maxlength="50"/></td>
            <td></td>
            <td>Volumen Bal&oacute;n aforado (mL):</td>
            <td><select id="aforado" onchange="CalculaTotal();">
                    <option value="">...</option>
                    <option value="10" <?php if ($ROW[0]['aforado'] == '10') echo 'selected'; ?>>10</option>
                    <option value="25" <?php if ($ROW[0]['aforado'] == '25') echo 'selected'; ?>>25</option>
                    <option value="50" <?php if ($ROW[0]['aforado'] == '50') echo 'selected'; ?>>50</option>
                    <option value="100" <?php if ($ROW[0]['aforado'] == '100') echo 'selected'; ?>>100</option>
                    <option value="200" <?php if ($ROW[0]['aforado'] == '200') echo 'selected'; ?>>200</option>
                    <option value="250" <?php if ($ROW[0]['aforado'] == '250') echo 'selected'; ?>>250</option>
                    <option value="500" <?php if ($ROW[0]['aforado'] == '500') echo 'selected'; ?>>500</option>
                    <option value="1000" <?php if ($ROW[0]['aforado'] == '1000') echo 'selected'; ?>>1000</option>
                    <option value="2000" <?php if ($ROW[0]['aforado'] == '2000') echo 'selected'; ?>>2000</option>
                </select></td>
        </tr>
        <tr>
            <td>Est&aacute;ndar tiene certificado de pureza:</td>
            <td><select id="tiene">
                    <option value="0">No</option>
                    <option value="1" <?php if ($ROW[0]['tiene'] == '1') echo 'selected'; ?>>S&iacute;</option>
                </select></td>
            <td></td>
            <td>Concentraci&oacute;n est&aacute;ndar (mg/mL):</td>
            <td id="conce"></td>
        </tr>
        <tr>
            <td>Pureza del est&aacute;ndar(%):</td>
            <td colspan="2"><input type="text" id="pureza" value="<?= $ROW[0]['pureza'] ?>" class="monto"
                                   onblur="_FLOAT(this);CalculaTotal();"/></td>
            <td><strong>Consecutivo:</strong></td>
            <td><?= $ROW[0]['codigoalterno'] ?><input type="hidden" id="cs" value="<?= $ROW[0]['cs'] ?>"></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"></td>
            <td><strong>Fecha:</strong></td>
            <td><input type="text" id="fecha" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $ROW[0]['fecha'] ?>"></td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="90%">
        <tr>
            <td class="titulo" colspan="6">Condiciones cromatogr&aacute;ficas</td>
        </tr>
        <td>Equipo:</td>
        <td colspan="2"><input type="text" id="nom_bal" value="<?= $ROW[0]['nom_bal'] ?>" class="lista" readonly
                               onclick="EquiposLista()">
            <input type="hidden" id="cod_bal" value="<?= $ROW[0]['cod_bal'] ?>"/>
        </td>
        <td></td>
        <td colspan="2" align="center"><strong>Inyector</strong></td>
        </tr>
        <tr>
            <td>Marca/Modelo:</td>
            <td id="marca"><?= $ROW[0]['marca'] ?></td>
            <td colspan="2"></td>
            <td>Tipo:</td>
            <td><select id="tipo_inyector">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['tipo_inyector'] == '0') echo 'selected'; ?>>COC</option>
                    <option value="1" <?php if ($ROW[0]['tipo_inyector'] == '1') echo 'selected'; ?>>Capilar</option>
                    <option value="2" <?php if ($ROW[0]['tipo_inyector'] == '2') echo 'selected'; ?>>Empacado</option>
                    <option value="3" <?php if ($ROW[0]['tipo_inyector'] == '3') echo 'selected'; ?>>PTV</option>
                </select></td>
        </tr>
        <tr>
            <td colspan="2">Volumen de Inyecci&oacute;n (&micro;L)</td>
            <td><input type="text" id="inyeccion" class="monto" value="<?= $ROW[0]['inyeccion'] ?>"
                       onblur="_FLOAT(this);"/></td>
            <td></td>
            <td>Temperatura (&deg;C):</td>
            <td><input type="text" id="temp" class="monto" value="<?= $ROW[0]['temp1'] ?>" onblur="_FLOAT(this);"/></td>
        </tr>
        <tr>
            <td colspan="4"></td>
            <td>Modo de inyecci&oacute;n:</td>
            <td><select id="modo">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['modo'] == '0') echo 'selected'; ?>>Split</option>
                    <option value="1" <?php if ($ROW[0]['modo'] == '1') echo 'selected'; ?>>Splitless</option>
                </select></td>
        </tr>
        <tr>
            <td colspan="6">&nbsp;</td>
        </tr>
        <tr align="center">
            <td colspan="3"><strong>Fase m&oacute;vil</strong></td>
            <td></td>
            <td colspan="2"><strong>Columna</strong></td>
        </tr>
        <tr>
            <td><strong>Gas</strong></td>
            <td><strong>Flujo (mL/min)</strong></td>
            <td><strong>Presi&oacute;n (psi)</strong></td>
            <td></td>
            <td>Tipo:</td>
            <td><select id="tipo_col">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['tipo_col'] == '0') echo 'selected'; ?>>Capilar</option>
                    <option value="1" <?php if ($ROW[0]['tipo_col'] == '1') echo 'selected'; ?>>Empacada</option>
                </select></td>
        </tr>
        <tr>
            <td><select id="gas1">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['gas1'] == '0') echo 'selected'; ?>>Hidr&oacute;geno</option>
                    <option value="1" <?php if ($ROW[0]['gas1'] == '1') echo 'selected'; ?>>Helio</option>
                    <option value="2" <?php if ($ROW[0]['gas1'] == '2') echo 'selected'; ?>>Nitr&oacute;geno</option>
                    <option value="3" <?php if ($ROW[0]['gas1'] == '3') echo 'selected'; ?>>Aire</option>
                </select></td>
            <td><input type="text" id="flujo1" value="<?= $ROW[0]['flujo1'] ?>" class="monto" onblur="_FLOAT(this);">
            </td>
            <td><input type="text" id="presion1" value="<?= $ROW[0]['presion1'] ?>" class="monto"
                       onblur="_FLOAT(this);"></td>
            <td></td>
            <td>C&oacute;digo:</td>
            <td><input type="text" id="cod_col" value="<?= $ROW[0]['cod_col'] ?>" class="lista" readonly
                       onclick="ColumnaLista()"></td>
        </tr>
        <tr align="center">
            <td colspan="3"><strong>Condiciones del FID</strong></td>
        </tr>
        <tr>
            <td><strong>Gas</strong></td>
            <td><strong>Flujo (mL/min)</strong></td>
            <td></td>
        </tr>
        <tr>
            <td><select id="gas2" disabled>
                    <option value="0">Hidr&oacute;geno</option>
                </select></td>
            <td><input type="text" id="flujo2" value="<?= $ROW[0]['flujo2'] ?>" class="monto" onblur="_FLOAT(this);">
            </td>
            <td><input type="hidden" id="presion2" value="<?= $ROW[0]['presion2'] ?>" class="monto"
                       onblur="_FLOAT(this);"></td>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td><select id="gas3" disabled>
                    <option value="3">Aire</option>
                </select></td>
            <td><input type="text" id="flujo3" value="<?= $ROW[0]['flujo3'] ?>" class="monto" onblur="_FLOAT(this);">
            </td>
            <td><input type="hidden" id="presion3" value="<?= $ROW[0]['presion3'] ?>" class="monto"
                       onblur="_FLOAT(this);"></td>
            <td></td>
            <td>Marca y fase:</td>
            <td><textarea id="marcafase" style="width:97%"><?= $ROW[0]['marcafase'] ?></textarea></td>
        </tr>
        <tr>
            <td><select id="gas4" disabled>
                    <option value="2">Nitr&oacute;geno</option>
                </select></td>
            <td><input type="text" id="flujo4" value="<?= $ROW[0]['flujo4'] ?>" class="monto" onblur="_FLOAT(this);">
            </td>
            <td><input type="hidden" id="presion4" value="<?= $ROW[0]['presion4'] ?>" class="monto"
                       onblur="_FLOAT(this);"></td>
            <td></td>
            <!--<td>Fecha del empaque:</td>
            <td></td>-->
        </tr>
        <tr>
            <td colspan="4"></td>
            <td rowspan="2">Temperatura (&deg;C):</td>
            <td rowspan="2"><input type="text" id="temp2" value="<?= $ROW[0]['temp2'] ?>" class="monto"
                                   onblur="_FLOAT(this);"/></td>
        </tr>
        <tr>
            <td align="center" colspan="3"><strong>Detector</strong></td>
            <td></td>
        </tr>
        <tr>
            <td>Tipo:</td>
            <td colspan="2"><select id="tipo_detector">
                    <option value="">...</option>
                    <option value="0" <?php if ($ROW[0]['tipo_detector'] == '0') echo 'selected'; ?>>Arreglo de diodos
                    </option>
                    <option value="1" <?php if ($ROW[0]['tipo_detector'] == '1') echo 'selected'; ?>>Conductividad
                    </option>
                    <option value="2" <?php if ($ROW[0]['tipo_detector'] == '2') echo 'selected'; ?>>FID</option>
                    <option value="3" <?php if ($ROW[0]['tipo_detector'] == '3') echo 'selected'; ?>>Masas</option>
                    <option value="4" <?php if ($ROW[0]['tipo_detector'] == '4') echo 'selected'; ?>>Masas-Masas
                    </option>
                </select></td>
            <td></td>
            <td>Dimensiones:</td>
            <td><textarea id="dimensiones" style="width:97%"><?= $ROW[0]['dimensiones'] ?></textarea></td>
        </tr>
        <tr>
            <td>Temperatura (&deg;C):</td>
            <td colspan="2"><input type="text" id="temp3" value="<?= $ROW[0]['temp3'] ?>" class="monto"
                                   onblur="_FLOAT(this);"/></td>
            <td></td>
            <td>Otros par&aacute;metros:</td>
            <td><textarea id="otros" style="width:97%"><?= $ROW[0]['otros'] ?></textarea></td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="90%">
        <tr>
            <td class="titulo" colspan="5">Verificaci&oacute;n de linealidad del equipo</td>
        </tr>
        <tr align="center">
            <td><strong>Patr&oacute;n</strong></td>
            <td><strong>Vol. de Al&iacute;cuota (mL)</strong></td>
            <td><strong>Vol B. Aforado (mL)</strong></td>
            <td><strong>Concentraci&oacute;n (mg/mL)</strong></td>
            <td><strong>&Aacute;rea</strong></td>
        </tr>
        <?php
        $ROW2 = $Gestor->ObtieneDetecciones();
        for ($x = 0; $x < count($ROW2); $x++) {
            ?>
            <tr align="center">
                <td><?= ($x + 1) ?></td>
                <td><select name="vol_ali" id="vol_ali<?= $x ?>" onchange="CalculaTotal()">
                        <?php if ($x == '3') { ?>
                            <option value="0">N/A</option>
                        <?php } else { ?>
                            <option value="">...</option>
                            <option value="0.5" <?php if ($ROW2[$x]['vol_ali'] == '0.5') echo 'selected'; ?>>0.5
                            </option>
                            <option value="1" <?php if ($ROW2[$x]['vol_ali'] == '1') echo 'selected'; ?>>1</option>
                            <option value="2" <?php if ($ROW2[$x]['vol_ali'] == '2') echo 'selected'; ?>>2</option>
                            <option value="3" <?php if ($ROW2[$x]['vol_ali'] == '3') echo 'selected'; ?>>3</option>
                            <option value="4" <?php if ($ROW2[$x]['vol_ali'] == '4') echo 'selected'; ?>>4</option>
                            <option value="5" <?php if ($ROW2[$x]['vol_ali'] == '5') echo 'selected'; ?>>5</option>
                            <option value="6" <?php if ($ROW2[$x]['vol_ali'] == '6') echo 'selected'; ?>>6</option>
                            <option value="7" <?php if ($ROW2[$x]['vol_ali'] == '7') echo 'selected'; ?>>7</option>
                            <option value="10" <?php if ($ROW2[$x]['vol_ali'] == '10') echo 'selected'; ?>>10</option>
                            <option value="15" <?php if ($ROW2[$x]['vol_ali'] == '15') echo 'selected'; ?>>15</option>
                            <option value="20" <?php if ($ROW2[$x]['vol_ali'] == '20') echo 'selected'; ?>>20</option>
                            <option value="25" <?php if ($ROW2[$x]['vol_ali'] == '25') echo 'selected'; ?>>25</option>
                            <option value="50" <?php if ($ROW2[$x]['vol_ali'] == '50') echo 'selected'; ?>>50</option>
                            <option value="100" <?php if ($ROW2[$x]['vol_ali'] == '100') echo 'selected'; ?>>100
                            </option>
                        <?php } ?>
                    </select></td>
                <td><select name="vol_afo" id="vol_afo<?= $x ?>" onchange="CalculaTotal()">
                        <?php if ($x == '3') { ?>
                            <option value="0">N/A</option>
                        <?php } else { ?>
                            <option value="">...</option>
                            <option value="10" <?php if ($ROW2[$x]['vol_afo'] == '10') echo 'selected'; ?>>10</option>
                            <option value="25" <?php if ($ROW2[$x]['vol_afo'] == '25') echo 'selected'; ?>>25</option>
                            <option value="50" <?php if ($ROW2[$x]['vol_afo'] == '50') echo 'selected'; ?>>50</option>
                            <option value="100" <?php if ($ROW2[$x]['vol_afo'] == '100') echo 'selected'; ?>>100
                            </option>
                            <option value="200" <?php if ($ROW2[$x]['vol_afo'] == '200') echo 'selected'; ?>>200
                            </option>
                            <option value="250" <?php if ($ROW2[$x]['vol_afo'] == '250') echo 'selected'; ?>>250
                            </option>
                            <option value="500" <?php if ($ROW2[$x]['vol_afo'] == '500') echo 'selected'; ?>>500
                            </option>
                            <option value="1000" <?php if ($ROW2[$x]['vol_afo'] == '1000') echo 'selected'; ?>>1000
                            </option>
                            <option value="2000" <?php if ($ROW2[$x]['vol_afo'] == '2000') echo 'selected'; ?>>2000
                            </option>
                        <?php } ?>
                    </select></td>
                <td id="cn<?= $x ?>"></td>
                <td><input type="text" name="area" id="area<?= $x ?>" class="monto" value="<?= $ROW2[$x]['area'] ?>"
                           onblur="_RED(this, 2);CalculaTotal();"/></td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr align="center">
            <td><strong>Pendiente (m)</strong></td>
            <td><strong>Intercepto (b)</strong></td>
            <td><strong>r</strong></td>
            <td><strong>Sm</strong></td>
            <td><strong>Sb</strong></td>
        </tr>
        <tr align="center">
            <td id="pendiente"></td>
            <td id="intercepto"></td>
            <td id="r"></td>
            <td id="sm"></td>
            <td id="sb"></td>
        </tr>
    </table>
    <table style="display: none">
        <?php
        for ($x = 0; $x < 6; $x++) {
            ?>
            <tr>
                <td id="_CN<?= $x ?>"></td>
                <td id="_AR<?= $x ?>"></td>
                <td id="_A<?= $x ?>"></td>
                <td id="_B<?= $x ?>"></td>
                <td id="_C<?= $x ?>"></td>
                <td id="_D<?= $x ?>"></td>
                <td id="_E<?= $x ?>"></td>
                <td id="_F<?= $x ?>"></td>
                <td id="_G<?= $x ?>"></td>
                <td id="_H<?= $x ?>"></td>
                <td id="_I<?= $x ?>"></td>
            </tr>
        <?php } ?>
    </table>
    <br/>
    <table class="radius" width="90%">
        <tr>
            <td class="titulo" colspan="4">Verificaci&oacute;n de la repetibilidad del equipo</td>
        </tr>
        <tr>
            <td><strong>N&uacute;mero de Corrida</strong></td>
            <td><strong>&Aacute;rea</strong></td>
            <td><strong>TR</strong></td>
            <td><strong>Altura</strong></td>
        </tr>
        <tbody id="lolo2">
        <?php
        $ROW2 = $Gestor->ObtieneRepro();
        for ($x = 0; $x < count($ROW2); $x++) {
            ?>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;<?= ($x + 1) ?></td>
                <td><input type="text" id="are<?= $x ?>" name="are" class="monto" value="<?= $ROW2[$x]['are'] ?>"
                           onblur="Redondear(this, 2);"/></td>
                <td><input type="text" id="tr<?= $x ?>" name="tr" class="monto" value="<?= $ROW2[$x]['tr'] ?>"
                           onblur="Redondear(this, 3);"/></td>
                <td><input type="text" id="alt<?= $x ?>" name="alt" class="monto" value="<?= $ROW2[$x]['alt'] ?>"
                           onblur="Redondear(this, 2);"/></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
        <tr>
            <td align="center"><strong>Promedio:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
            <td id="prom_are"></td>
            <td id="prom_tr"></td>
            <td id="prom_alt"></td>
        </tr>
        <tr>
            <td align="center"><strong>Desviaci&oacute;n est&aacute;ndar:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
            <td id="desv_are"></td>
            <td id="desv_tr"></td>
            <td id="desv_alt"></td>
        </tr>
        <tr>
            <td align="center"><strong>% C.V.:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
            <td id="cv_are"></td>
            <td id="cv_tr"></td>
            <td id="cv_alt"></td>
        </tr>
        <tr>
            <td align="center"><strong>L&iacute;mite Superior 3s</strong></td>
            <td id="3s_are"></td>
            <td id="3s_tr"></td>
            <td id="3s_alt"></td>
        </tr>
        <tr>
            <td align="center"><strong>L&iacute;mite Superior 2s</strong></td>
            <td id="2s_are"></td>
            <td id="2s_tr"></td>
            <td id="2s_alt"></td>
        </tr>
        <tr>
            <td align="center"><strong>L&iacute;mite Superior 1s</strong></td>
            <td id="1s_are"></td>
            <td id="1s_tr"></td>
            <td id="1s_alt"></td>
        </tr>
        <tr>
            <td align="center"><strong>L&iacute;mite Inferior 1s</strong></td>
            <td id="1i_are"></td>
            <td id="1i_tr"></td>
            <td id="1i_alt"></td>
        </tr>
        <tr>
            <td align="center"><strong>L&iacute;mite Inferior 2s</strong></td>
            <td id="2i_are"></td>
            <td id="2i_tr"></td>
            <td id="2i_alt"></td>
        </tr>
        <tr>
            <td align="center"><strong>L&iacute;mite Inferior 3s</strong></td>
            <td id="3i_are"></td>
            <td id="3i_tr"></td>
            <td id="3i_alt"></td>
        </tr>
        <?php
        if ($estado != '') {
            $ROW = $Gestor->Historial();
            echo '<tr><td class="titulo" colspan="4">Historial</td></tr>';
            for ($x = 0; $x < count($ROW); $x++) {
                echo "<tr><td colspan='4'><strong>", $Gestor->Accion($ROW[$x]['accion']), "</strong> {$ROW[$x]['nombre']} {$ROW[$x]['ap1']}, {$ROW[$x]['fecha1']}</td></tr>";
                if ($ROW[$x]['accion'] == '0') $creador = $ROW[$x]['id'];
            }
        }
        ?>
    </table>
    <br/><?= $Gestor->Encabezado('C0008', 'p', '') ?>
    <!-- BOTONES-->
    <br/>
    <?php if ($estado == '') { ?>
        <input type="button" value="Guardar" class="boton" onClick="datos()">&nbsp;
    <?php } ?>
    <?php if ($estado == '0' && $Gestor->Modificar($creador)) { ?>
        <input type="button" value="Guardar" class="boton" onClick="datos()">&nbsp;
        <input type="button" value="Procesar" class="boton" onClick="Procesar('1')">&nbsp;
        <input type="button" value="Anular" class="boton" onClick="Procesar('5')">&nbsp;
    <?php } ?>
    <?php if ($estado == '1' && $Gestor->Aprobar()) { ?>
        <input type="button" value="Anular" class="boton" onClick="Procesar('5')">&nbsp;
        <input type="button" value="Aprobar" class="boton2" onClick="Procesar('2')">&nbsp;
    <?php } ?>
    <!-- BOTONES-->
    <?php if ($_GET['acc'] == 'M') { ?>
        <br/>
        <table class="radius" width="90%">
            <tr>
                <td class="titulo">Gr&aacute;fico</td>
            </tr>
            <tr>
                <td>Tipo: <select id="graf_pesa" disabled>
                        <option value="A">Curva de calibraci&oacute;n</option>
                    </select>&nbsp;
                    <input type="button" id="btn" value="Generar" class="boton" onClick="GenerarA()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a target="_blank" href="../../caspha-i/docs/cartas/carta8.xlsx">[D&oacute;cimas]</a>
                </td>
            </tr>
            <tr style="display:none;">
                <td id="td_tablaA"></td>
            </tr>
            <tr align="center" id="tr_graficoA" style="display:none;">
                <td>
                    <div id="containerA" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </td>
            </tr>
        </table>
        <br>
        <table class="radius" width="90%">
            <tr>
                <td class="titulo">Gr&aacute;fico</td>
            </tr>
            <tr>
                <td>Tipo: <select id="graf_pesa1" disabled>
                        <option value="B">&Aacute;rea</option>
                    </select>&nbsp;
                    <input type="button" id="btn" value="Generar" class="boton" onClick="GenerarB()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a target="_blank" href="../../caspha-i/docs/cartas/carta8.xlsx">[D&oacute;cimas]</a>
                </td>
            </tr>
            <tr style="display:none;">
                <td id="td_tablaB"></td>
            </tr>
            <tr align="center" id="tr_graficoB" style="display:none;">
                <td>
                    <div id="containerB" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </td>
            </tr>
        </table>
        <br>
        <table class="radius" width="90%">
            <tr>
                <td class="titulo">Gr&aacute;fico</td>
            </tr>
            <tr>
                <td>Tipo: <select id="graf_pesa2" disabled>
                        <option value="C">TR</option>
                        <option value="D">Altura</option>
                    </select>&nbsp;
                    <input type="button" id="btn" value="Generar" class="boton" onClick="GenerarC()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a target="_blank" href="../../caspha-i/docs/cartas/carta8.xlsx">[D&oacute;cimas]</a>
                </td>
            </tr>
            <tr style="display:none;">
                <td id="td_tablaC"></td>
            </tr>
            <tr align="center" id="tr_graficoC" style="display:none;">
                <td>
                    <div id="containerC" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </td>
            </tr>
        </table>
        <br>
        <table class="radius" width="90%">
            <tr>
                <td class="titulo">Gr&aacute;fico</td>
            </tr>
            <tr>
                <td>Tipo: <select id="graf_pesa3" disabled>
                        <option value="D">Altura</option>
                    </select>&nbsp;
                    <input type="button" id="btn" value="Generar" class="boton" onClick="GenerarD()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a target="_blank" href="../../caspha-i/docs/cartas/carta8.xlsx">[D&oacute;cimas]</a>
                </td>
            </tr>
            <tr style="display:none;">
                <td id="td_tablaD"></td>
            </tr>
            <tr align="center" id="tr_graficoD" style="display:none;">
                <td>
                    <div id="containerD" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </td>
            </tr>
        </table>
        <br/>
        <script>Generar();</script>
    <?php } ?>
    <?php if ($estado != '') { ?>
        <input type="button" value="Imprimir" class="boton2" onClick="window.print()">
    <?php } ?>
</center>
<script>
    <?php if($_GET['acc'] == 'I'){?>
    inicio();
    <?php }else{ ?>
    CalculaTotal();
    <?php } ?>
</script>
<?= $Gestor->Footer(2) ?>
</body>
</html>