<?php
define('__MODULO__', 'cartas');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _cartaA_detalle();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('graficador', 'gfx'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<center>
    <?php $Gestor->Incluir('c11', 'hr', 'Equipos :: Carta de control analitos positivos') ?>
    <?= $Gestor->Encabezado('C0011', 'e', 'Carta de control analitos positivos') ?>
    <div class="printable">
        <form name="form" method="post" enctype="multipart/form-data"
              action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
            <input type="hidden" name="subir" value="1"/>
            <br>
            <table class="radius" width="600px">
                <tr>
                    <td class="titulo">Adjuntar archivo de resultados</td>
                </tr>
                <tr>
                    <td><input type="file" name="archivo" id="archivo">&nbsp;
                        <input type="button" id="btn" value="Cargar" class="boton" onclick="datos();">
                        &nbsp;
                        <img onclick="DocumentosZip('carta_LRE', '<?= __MODULO__ ?>')"
                             src="<?= $Gestor->Incluir('olvido', 'bkg') ?>" border="0" width="20" height="20"
                             title="Decargar plantilla" style="vertical-align:middle; cursor:pointer"/></a></td>
                </tr>
            </table>
        </form>
    </div>
    <br/>
    <table class="radius" width="600px">
        <tr>
            <td class="titulo">Gr&aacute;fico</td>
        </tr>
        <tr>
            <td>Analito: <select id="graf_tipo">
                    <option value=''>Todos</option>
                    <?php
                    $ROW = $Gestor->Analitos();
                    for ($x = 0; $x < count($ROW); $x++) {
                        $ROW[$x]['nombre'] = strtolower($ROW[$x]['nombre']);
                        ?>
                        <option value='<?= $ROW[$x]['nombre'] ?>'><?= $ROW[$x]['nombre'] ?></option>
                        <?php
                    }
                    ?>
                </select>&nbsp;
                Desde: <input type="text" id="graf_desde" class="fecha" readonly onClick="show_calendar(this.id);">&nbsp;
                Hasta: <input type="text" id="graf_hasta" class="fecha" readonly onClick="show_calendar(this.id);">&nbsp;
                Promedio: <input type="text" id="graf_prom" class="cantidad" maxlength="3" onblur="_INT(this)">&nbsp;
                <input type="button" id="btn" value="Generar" class="boton2" onClick="Generar()">
            </td>
        </tr>
        <tr>
            <td id="td_tabla"></td>
        </tr>
        <tr align="center" id="tr_grafico" style="display:none;">
            <td>
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </td>
        </tr>
    </table>
    <br/>
    <input type="button" value="Imprimir" class="boton2" onClick="window.print()">
</center>
<br><?= $Gestor->Encabezado('C0011', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>