<?php
define('__MODULO__', 'personal');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _sugerencias();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('i7', 'hr', 'Personal :: Sugerencias') ?>
<?= $Gestor->Encabezado('I0007', 'e', 'Sugerencias') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" style="width:650px">
            <tr>
                <td class="titulo" colspan="2">Filtro</td>
            </tr>
            <tr>
                <td>Usuario: <input type="text" id="tmp" name="tmp" class="lista" value="<?= $_POST['tmp'] ?>" readonly
                                    onclick="UsuariosLista()"/>
                    <input type="hidden" id="usuario" name="usuario" value="<?= $_POST['usuario'] ?>"/>
                </td>
                <td>Desde: <input type="text" id="desde" name="desde" class="fecha" value="<?= $_POST['desde'] ?>"
                                  readonly onClick="show_calendar(this.id);">&nbsp;
                    Hasta: <input type="text" id="hasta" name="hasta" class="fecha" value="<?= $_POST['hasta'] ?>"
                                  readonly onClick="show_calendar(this.id);">&nbsp;
                    <input type="button" value="Buscar" class="boton2" onclick="CitasBuscar(this)"/>
                </td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:650px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>No.</th>
                <th>Nombre</th>
                <th>Fecha</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->SugerenciasMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#"
                           onclick="SugerenciasModificar('<?= $ROW[$x]['id'] ?>')">LAB-LCC-F-<?= $ROW[$x]['id'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['nombre'], ' ', $ROW[$x]['ap1'], ' ', $ROW[$x]['ap2'] ?></td>
                    <td><?= $ROW[$x]['fecha'] ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/>
    <input type="button" class="boton" value="Agregar" onclick="SugerenciasAgregar()"/>
</center>
<?= $Gestor->Encabezado('I0007', 'p', '') ?>
</body>
</html>