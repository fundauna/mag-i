<?php
define('__MODULO__', 'personal');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _curriculo_detalle();
$ROW2 = $Gestor->InfoPersonal();
if (!$ROW2) die('Registro inexistente');
$id = $ROW2[0]['id'];
$ROW = $Gestor->InfoLaboral();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('jquery.maskedinput.min', 'js'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <style media="print">
        .boton {
            display: none
        }
    </style>
</head>
<body>
<script>
    img1 = '<?php $Gestor->Incluir('closed', 'bkg')?>';
    img2 = '<?php $Gestor->Incluir('downboxed', 'bkg')?>';
</script>
<center>
    <input type="hidden" id="id" value="<?= $id ?>"/>
    <?php $Gestor->Incluir('i5', 'hr', 'Hoja de Vida') ?>
    <?= $Gestor->Encabezado('I0005', 'e', 'Hoja de Vida') ?>

    <br/>
    <table class="radius" width="100%" style="font-size:12px">
        <tr>
            <td class="titulo" colspan="3">1- Identificaci&oacute;n del funcionario</td>
        </tr>
        <tr>
            <td colspan="2"><strong>Nombre y Apellidos</strong></td>
            <td><strong>C&eacute;dula de identidad</strong></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" id="nombre" value="<?= $ROW2[0]['nombre'] ?>" size="20" maxlength="30">&nbsp;
                <input type="text" id="ap1" value="<?= $ROW2[0]['ap1'] ?>" size="20" maxlength="30">&nbsp;
                <input type="text" id="ap2" value="<?= $ROW2[0]['ap2'] ?>" size="20" maxlength="30">
            </td>
            <td><input type="text" id="cedula" value="<?= $ROW2[0]['cedula'] ?>" size="13" maxlength="15"></td>
        </tr>
        <tr>
            <td><strong>Fecha de Nacimiento</strong></td>
            <td><strong>Nacionalidad</strong></td>
            <td><strong>G&eacute;nero</strong></td>
        </tr>
        <tr>
            <td><input type="text" id="fnacimiento" class="fecha" value="<?= $ROW[0]['fnacimiento'] ?>" readonly
                       onClick="show_calendar(this.id);"></td>
            <td><input type="text" id="nacionalidad" value="<?= $ROW[0]['nacionalidad'] ?>" size="20" maxlength="50">
            </td>
            <td><select id="genero">
                    <option value="">...</option>
                    <option value='0' <?= ($ROW[0]['genero'] == '0') ? 'selected' : '' ?>>Femenino</option>
                    <option value='1' <?= ($ROW[0]['genero'] == '1') ? 'selected' : '' ?>>Masculino</option>
                </select></td>
        </tr>
        <tr>
            <td colspan="3"><strong>Domicilio</strong></td>
        </tr>
        <tr>
            <td colspan="3"><textarea id="domicilio"><?= $ROW[0]['domicilio'] ?></textarea></td>
        </tr>
        <tr>
            <td><strong>Tel&eacute;fono habitaci&oacute;n</strong></td>
            <td><strong>Tel&eacute;fono celular</strong></td>
            <td><strong>Correo electr&oacute;nico</strong></td>
        </tr>
        <tr>
            <td><input type="text" id="tel_hab" value="<?= $ROW[0]['tel_hab'] ?>" size="10" maxlength="9"></td>
            <td><input type="text" id="tel_cel" value="<?= $ROW[0]['tel_cel'] ?>" size="10" maxlength="9"></td>
            <td><input type="text" id="email" value="<?= $ROW2[0]['email'] ?>" size="30" maxlength="50"></td>
        </tr>
        <tr>
            <td colspan="2"><strong>Fecha de ingreso al SFE (MAG)</strong></td>
            <td><strong>Profesi&oacute;n</strong></td>
        </tr>
        <tr>
            <td colspan="2"><input type="text" id="fingreso" class="fecha" value="<?= $ROW[0]['fingreso'] ?>" readonly
                                   onClick="show_calendar(this.id);"></td>
            <td><input type="text" id="profesion" value="<?= $ROW[0]['profesion'] ?>" size="20" maxlength="50"></td>
        </tr>
        <tr>
            <td><strong>Cargo</strong></td>
            <td><strong>Clase de Puesto</strong></td>
            <td><strong>No. de Puesto</strong></td>
        </tr>
        <tr>
            <td><input type="text" id="cargo" value="<?= $ROW[0]['cargo'] ?>" size="20" maxlength="50"></td>
            <td><input type="text" id="clase_puesto" value="<?= $ROW[0]['clase_puesto'] ?>" size="20" maxlength="50">
            </td>
            <td><input type="text" id="num_puesto" value="<?= $ROW[0]['num_puesto'] ?>" size="10" maxlength="10"></td>
        </tr>
        <tr>
            <td colspan="2"><strong>Colegio profesional</strong></td>
            <td><strong>No. de Colegiado</strong></td>
        </tr>
        <tr>
            <td colspan="2"><input type="text" id="colegio" value="<?= $ROW[0]['colegio'] ?>" size="20" maxlength="80">
            </td>
            <td><input type="text" id="num_colegiado" value="<?= $ROW[0]['num_colegiado'] ?>" size="20" maxlength="20">
            </td>
        </tr>
        <tr class="printable">
            <td colspan="3">
                <hr/>
            </td>
        </tr>
        <tr class="printable">
            <td align="left" id="doc" colspan="3">
                <?php
                $ruta = "../../caspha-i/docs/personal/ced_{$id}.zip";

                if (file_exists($ruta)) {
                    ?>
                    Descargar adjuntos: <img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar"
                                             onclick="DocumentosZip('ced_<?= $id ?>', '<?= __MODULO__ ?>')"
                                             class="tab2"/><br/>
                    <?php
                }
                ?>
                <iframe id="ac" name="ac" frameborder="0" scrolling="0" src="docs.php?usuario=<?= $id ?>" width="500px"
                        height="65px;"></iframe>
            </td>
        </tr>
        <tr class="printable">
            <td align="center" colspan="3"><input type="button" id="btn1" value="Actualizar" class="boton"
                                                  onClick="Paso1()"></td>
        </tr>
    </table>
    <br/>
    <table width="100%" class="radius" style="font-size:12px">
        <thead>
        <tr>
            <td class="titulo" colspan="3"><img class="printable" id="i_2"
                                                src="<?= $Gestor->Incluir('closed', 'bkg') ?>" onclick="Oculta(2)"
                                                title="Mostrar/Ocultar" style="vertical-align:baseline;cursor:pointer"/>&nbsp;2-
                Formaci&oacute;n Acad&eacute;mica
            </td>
        </tr>
        <tr align="center">
            <td>T&iacute;tulo</td>
            <td>Instituci&oacute;n</td>
            <td>A&ntilde;o</td>
        </tr>
        </thead>
        <tbody id="t2" style="display:none"></tbody>
        <tfoot id="f2" style="display:none">
        <tr>
            <td align="right" colspan="3"><img class="printable" src="<?= $Gestor->Incluir('olvido', 'bkg') ?>"
                                               width="20" height="20"
                                               title="Si deja alg�n campo vac�o la l�nea no ser� tomada en cuenta"/>&nbsp;<input
                        type="button" id="btn2" value="Actualizar" class="boton" onClick="Paso2()">&nbsp;<input
                        type="button" id="lolo1" value="+" onclick="FormacionMas()" title="Agregar l�nea"
                        class="boton"/></td>
        </tr>
        </tfoot>
    </table>
    <br/>
    <table width="100%" class="radius" style="font-size:12px">
        <thead>
        <tr>
            <td class="titulo" colspan="4"><img class="printable" id="i_3"
                                                src="<?= $Gestor->Incluir('closed', 'bkg') ?>" onclick="Oculta(3)"
                                                title="Mostrar/Ocultar" style="vertical-align:baseline;cursor:pointer"/>&nbsp;3-
                Idiomas
            </td>
        </tr>
        <tr>
            <td>Idioma</td>
            <td>Habla (%)</td>
            <td>Lee (%)</td>
            <td>Escribe (%)</td>
        </tr>
        </thead>
        <tbody id="t3" style="display:none"></tbody>
        <tfoot id="f3" style="display:none">
        <tr>
            <td colspan="4" align="right"><img class="printable" src="<?= $Gestor->Incluir('olvido', 'bkg') ?>"
                                               width="20" height="20"
                                               title="Si deja alg�n campo vac�o la l�nea no ser� tomada en cuenta"/>&nbsp;<input
                        type="button" id="btn3" value="Actualizar" class="boton" onClick="Paso3()">&nbsp;<input
                        type="button" id="lolo2" value="+" onclick="IdiomasMas()" title="Agregar l�nea" class="boton"/>
            </td>
        </tr>
        </tfoot>
    </table>
    <br/>
    <table width="100%" class="radius" style="font-size:12px">
        <thead>
        <tr>
            <td class="titulo" colspan="3"><img class="printable" id="i_4"
                                                src="<?= $Gestor->Incluir('closed', 'bkg') ?>" onclick="Oculta(4)"
                                                title="Mostrar/Ocultar" style="vertical-align:baseline;cursor:pointer"/>&nbsp;4-
                Publicaciones
            </td>
        </tr>
        <tr align="center">
            <td>Nombre del art&iacute;culo</td>
            <td>Entidad donde se publica</td>
            <td>A&ntilde;o</td>
        </tr>
        </thead>
        <tbody id="t4" style="display:none"></tbody>
        <tfoot id="f4" style="display:none">
        <tr>
            <td align="right" colspan="3"><img class="printable" src="<?= $Gestor->Incluir('olvido', 'bkg') ?>"
                                               width="20" height="20"
                                               title="Si deja alg�n campo vac�o la l�nea no ser� tomada en cuenta"/>&nbsp;<input
                        type="button" id="btn4" value="Actualizar" class="boton" onClick="Paso4()">&nbsp;<input
                        type="button" id="lolo3" value="+" onclick="PublicacionesMas()" title="Agregar l�nea"
                        class="boton"/></td>
        </tr>
        </tfoot>
    </table>
    <br/>
    <center><strong>5. Experiencia</strong></center>
    <hr/>
    <table width="100%" class="radius" style="font-size:12px">
        <thead>
        <tr>
            <td class="titulo" colspan="2"><img id="i_51" src="<?= $Gestor->Incluir('closed', 'bkg') ?>"
                                                onclick="Oculta(51)" title="Mostrar/Ocultar" class="tab"/>&nbsp;5.1-
                Experiencia Laboral
            </td>
        </tr>
        <tr>
            <td>Empresa</td>
            <td>Periodo</td>
        </tr>
        </thead>
        <tbody id="t51" style="display:none"></tbody>
        <tfoot id="f51" style="display:none">
        <tr>
            <td colspan="2" align="right"><img class="printable" src="<?= $Gestor->Incluir('olvido', 'bkg') ?>"
                                               width="20" height="20"
                                               title="Si deja alg�n campo vac�o la l�nea no ser� tomada en cuenta"/>&nbsp;<input
                        type="button" id="btn51" value="Actualizar" class="boton" onClick="Paso51()">&nbsp;<input
                        type="button" id="lolo4" value="+" onclick="ExperienciaMas()" title="Agregar l�nea"
                        class="boton"/></td>
        </tr>
        </tfoot>
    </table>
    <br/>
    <table width="100%" class="radius" style="font-size:12px">
        <thead>
        <tr>
            <td class="titulo" colspan="3"><img class="printable" id="i_52"
                                                src="<?= $Gestor->Incluir('closed', 'bkg') ?>" onclick="Oculta(52)"
                                                title="Mostrar/Ocultar" style="vertical-align:baseline;cursor:pointer"/>&nbsp;5.2-
                Experiencia en an&aacute;lisis de laboratorio
            </td>
        </tr>
        <tr align="center">
            <td>T&eacute;cnica</td>
            <td>Empresa</td>
            <td>Periodo</td>
        </tr>
        </thead>
        <tbody id="t52" style="display:none"></tbody>
        <tfoot id="f52" style="display:none">
        <tr>
            <td align="right" colspan="3"><img class="printable" src="<?= $Gestor->Incluir('olvido', 'bkg') ?>"
                                               width="20" height="20"
                                               title="Si deja alg�n campo vac�o la l�nea no ser� tomada en cuenta"/>&nbsp;<input
                        type="button" id="btn52" value="Actualizar" class="boton" onClick="Paso52()">&nbsp;<input
                        type="button" id="lolo5" value="+" onclick="AnalisisMas()" title="Agregar l�nea" class="boton"/>
            </td>
        </tr>
        </tfoot>
    </table>
    <br/>
    <table width="100%" class="radius" style="font-size:12px">
        <thead>
        <tr>
            <td class="titulo" colspan="3"><img class="printable" id="i_53"
                                                src="<?= $Gestor->Incluir('closed', 'bkg') ?>" onclick="Oculta(53)"
                                                title="Mostrar/Ocultar" style="vertical-align:baseline;cursor:pointer"/>&nbsp;5.3-
                Experiencia en uso y manejo de equipos
            </td>
        </tr>
        <tr align="center">
            <td>Equipo</td>
            <td>Empresa</td>
            <td>Periodo</td>
        </tr>
        </thead>
        <tbody id="t53" style="display:none"></tbody>
        <tfoot id="f53" style="display:none">
        <tr>
            <td align="right" colspan="3"><img class="printable" src="<?= $Gestor->Incluir('olvido', 'bkg') ?>"
                                               width="20" height="20"
                                               title="Si deja alg�n campo vac�o la l�nea no ser� tomada en cuenta"/>&nbsp;<input
                        type="button" id="btn53" value="Actualizar" class="boton" onClick="Paso53()">&nbsp;<input
                        type="button" id="lolo6" value="+" onclick="EquiposMas()" title="Agregar l�nea" class="boton"/>
            </td>
        </tr>
        </tfoot>
    </table>
    <br/>
    <table width="100%" class="radius" style="font-size:12px">
        <thead>
        <tr>
            <td class="titulo" colspan="3"><img class="printable" id="i_54"
                                                src="<?= $Gestor->Incluir('closed', 'bkg') ?>" onclick="Oculta(54)"
                                                title="Mostrar/Ocultar" style="vertical-align:baseline;cursor:pointer"/>&nbsp;5.4-
                Experiencia en gesti&oacute;n de calidad
            </td>
        </tr>
        <tr align="center">
            <td>Actividad</td>
            <td>Empresa</td>
            <td>Periodo</td>
        </tr>
        </thead>
        <tbody id="t54" style="display:none"></tbody>
        <tfoot id="f54" style="display:none">
        <tr>
            <td align="right" colspan="3"><img class="printable" src="<?= $Gestor->Incluir('olvido', 'bkg') ?>"
                                               width="20" height="20"
                                               title="Si deja alg�n campo vac�o la l�nea no ser� tomada en cuenta"/>&nbsp;<input
                        type="button" id="btn54" value="Actualizar" class="boton" onClick="Paso54()">&nbsp;<input
                        type="button" id="lolo7" value="+" onclick="CalidadMas()" title="Agregar l�nea" class="boton"/>
            </td>
        </tr>
        </tfoot>
    </table>
    <br/>
    <table width="100%" class="radius" style="font-size:12px">
        <thead>
        <tr>
            <td class="titulo" colspan="3"><img class="printable" id="i_55"
                                                src="<?= $Gestor->Incluir('closed', 'bkg') ?>" onclick="Oculta(55)"
                                                title="Mostrar/Ocultar" style="vertical-align:baseline;cursor:pointer"/>&nbsp;5.5-
                Otro tipo de experiencia o habilidades
            </td>
        </tr>
        <tr align="center">
            <td>Actividad</td>
            <td>Empresa</td>
            <td>Periodo</td>
        </tr>
        </thead>
        <tbody id="t55" style="display:none"></tbody>
        <tfoot id="f55" style="display:none">
        <tr>
            <td align="right" colspan="3"><img class="printable" src="<?= $Gestor->Incluir('olvido', 'bkg') ?>"
                                               width="20" height="20"
                                               title="Si deja alg�n campo vac�o la l�nea no ser� tomada en cuenta"/>&nbsp;<input
                        type="button" id="btn55" value="Actualizar" class="boton" onClick="Paso55()">&nbsp;<input
                        type="button" id="lolo8" value="+" onclick="OtrosMas()" title="Agregar l�nea" class="boton"/>
            </td>
        </tr>
        </tfoot>
    </table>
    <br/>
    <table width="100%" class="radius" style="font-size:12px">
        <thead>
        <tr>
            <td class="titulo" colspan="7"><img class="printable" id="i_6"
                                                src="<?= $Gestor->Incluir('closed', 'bkg') ?>" onclick="Oculta(6)"
                                                title="Mostrar/Ocultar" style="vertical-align:baseline;cursor:pointer"/>&nbsp;6-
                Capacitaciones o cursos realizados
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
            <td>Tipo</td>
            <td>Nombre del curso</td>
            <td>Empresa o instituci&oacute;n</td>
            <td>Duraci&oacute;n</td>
            <td>Fecha</td>
        </tr>
        </thead>
        <tbody id="t6" style="display:none"></tbody>
        <tfoot id="f6" style="display:none">
        <tr>
            <td align="right" colspan="7"><img class="printable" src="<?= $Gestor->Incluir('olvido', 'bkg') ?>"
                                               width="20" height="20"
                                               title="Si deja alg�n campo vac�o la l�nea no ser� tomada en cuenta"/>&nbsp;<input
                        type="button" id="btn6" value="Actualizar" class="boton" onClick="Paso6()">&nbsp;<input
                        type="button" id="lolo9" value="+" onclick="CursosMas()" title="Agregar l�nea" class="boton"/>
            </td>
        </tr>
        </tfoot>
    </table>
    <br/>
    <input type="button" class="boton" value="Imprimir" onclick="window.print()"/>
    <br/>&nbsp;
    <?php $Gestor->SoyYo(); ?>
</center>
<?= $Gestor->Encabezado('I0005', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>