<?php
define('__MODULO__', 'personal');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _sugerencias_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js', 5) ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('i8', 'hr', 'Personal :: Sugerencias Detalle') ?>
<center>
    <?= $Gestor->Encabezado('I0008', 'e', 'Formulario de sugerencias para personal de laboratorio') ?>
    <br>
    <input type="hidden" id="accion" name="accion" value="<?= $_GET['acc'] ?>"/>
    <input type="hidden" id="id" name="id" value="<?= $ROW[0]['id'] ?>"/>
    <table class="radius">
        <tr>
            <td class="titulo" colspan="2">Detalle</td>
        </tr>
        <tr>
            <td align="center">Usuario:</td>
            <td><?= $ROW[0]['nombre'], ' ', $ROW[0]['ap1'], ' ', $ROW[0]['ap2'] ?></td>
        </tr>
        <tr>
            <td align="center">Fecha:</td>
            <td><?= $ROW[0]['fecha'] ?></td>
        </tr>
        <tr>
            <td colspan="2">
                <hr/>
            </td>
        </tr>
        <tr>
            <td colspan="2">Oportunidad de mejora (indique en que consiste de forma clara y sencilla, adem&aacute;s
                mencione c&oacute;mo influye en su trabajo, el de sus compa&ntilde;eros y el de los clientes; junto con
                los efectos en tiempo y recursos):
            </td>
        </tr>
        <tr>
            <td colspan="2"><textarea id="obs" name="obs" style="width:99%"><?= $ROW[0]['obs'] ?></textarea></td>
        </tr>
        <tr>
            <td colspan="2">
                <hr/>
            </td>
        </tr>
        <tr>
            <td colspan="2">Recursos necesarios (explique c&oacute;mo el el jefe de Laboratorio y el jefe del
                Departamento pueden ayudar a solventar la oportunidad de mejora, sugiera los recursos que resuelvan la
                necesidad):
            </td>
        </tr>
        <tr>
            <td align="center">Personal necesario:</td>
            <td><textarea id="personal" name="personal" style="width:95%"><?= $ROW[0]['personal'] ?></textarea></td>
        </tr>
        <tr>
            <td align="center">Materiales necesarios:</td>
            <td><textarea id="materiales" name="materiales" style="width:95%"><?= $ROW[0]['materiales'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td align="center">Equipos necesarios:</td>
            <td><textarea id="equipos" name="equipos" style="width:95%"><?= $ROW[0]['equipos'] ?></textarea></td>
        </tr>
        <tr>
            <td align="center">Presupuesto necesario:</td>
            <td><textarea id="presupuesto" name="presupuesto" style="width:95%"><?= $ROW[0]['presupuesto'] ?></textarea>
            </td>
        </tr>
        <tr>
            <td align="center">Otros recursos necesarios:</td>
            <td><textarea id="otros" name="otros" style="width:95%"><?= $ROW[0]['otros'] ?></textarea></td>
        </tr>
        <tr>
            <td align="center">Costo total estimado:</td>
            <td><textarea id="costo" name="costo" style="width:95%"><?= $ROW[0]['costo'] ?></textarea></td>
        </tr>
        <tr>
            <td colspan="2">
                <hr/>
            </td>
        </tr>
        <tr>
            <td colspan="2">Beneficio(s) deseado(s) (explique el beneficio total estimado de la aplicaci&oacute;�n de la
                mejora):
            </td>
        </tr>
        <tr>
            <td colspan="2"><textarea id="beneficio" name="beneficio"
                                      style="width:99%"><?= $ROW[0]['beneficio'] ?></textarea></td>
        </tr>
        <tr>
            <td colspan="2">
                <hr/>
            </td>
        </tr>
        <tr>
            <td colspan="2">Propuesta de acci&oacute;n para solventar la oportunidad de mejora (indique de forma breve
                los pasos y personas necesarias para llevar a cabo la sugerencia):
            </td>
        </tr>
        <tr>
            <td colspan="2"><textarea id="propuesta" name="propuesta"
                                      style="width:99%"><?= $ROW[0]['propuesta'] ?></textarea></td>
        </tr>
    </table>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos('<?= $_GET['acc'] ?>')">
    <?php if ($_GET['acc'] != 'I') { ?>
        &nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php } ?>
    </form>
</center>
<?= $Gestor->Encabezado('I0008', 'p', '') ?>
</body>
</html>