<?php
define('__MODULO__', 'personal');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _parametros();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('No se han inicializado los parámetros');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('i6', 'hr', 'Personal :: Parámetros') ?>
<?= $Gestor->Encabezado('I0006', 'e', 'Parámetros') ?>
<table class="radius" align="center">
    <tr>
        <td class="titulo" colspan="2">Laboratorio de an&aacute;lisis de residuos de agroqu&iacute;micos</td>
    </tr>
    <tr>
        <td><b>Fecha de aviso para actualizar hoja de vida:</b></td>
        <td><input type="text" id="personal1" name="personal1" class="fecha" value="<?= $ROW[0]['personal1'] ?>"
                   readonly onClick="show_calendar(this.id);"></td>
    </tr>
    <tr>
        <td class="titulo" colspan="2">Laboratorio central de diagn&oacute;stico de plagas</td>
    </tr>
    <tr>
        <td><b>Fecha de aviso para actualizar hoja de vida:</b></td>
        <td><input type="text" id="personal2" name="personal2" class="fecha" value="<?= $ROW[0]['personal2'] ?>"
                   readonly onClick="show_calendar(this.id);"></td>
    </tr>
    <tr>
        <td class="titulo" colspan="2">Laboratorio de control de calidad de agroqu&iacute;micos</td>
    </tr>
    <tr>
        <td><b>Fecha de aviso para actualizar hoja de vida:</b></td>
        <td><input type="text" id="personal3" name="personal3" class="fecha" value="<?= $ROW[0]['personal3'] ?>"
                   readonly onClick="show_calendar(this.id);"></td>
    </tr>
</table>
<br/>
<center>
    <input id="btn" type="button" class="boton" value="Aceptar" onclick="datos();">
</center>
<?= $Gestor->Encabezado('I0006', 'p', '') ?>
</body>
</html>