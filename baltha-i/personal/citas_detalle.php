<?php
define('__MODULO__', 'personal');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _citas_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW)
    die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js', 5) ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('i3', 'hr', 'Personal :: Detalle de Citas/Chequeos') ?>
<?= $Gestor->Encabezado('I0003', 'e', 'Detalle de Citas/Chequeos') ?>
<center>
    <form name="form" id="form" method="post" enctype="multipart/form-data"
          action="../../caspha-i/shell/personal/_citas_detalle.php">
        <input type="hidden" id="accion" name="accion" value="<?= $_GET['acc'] ?>"/>
        <input type="hidden" id="id" name="id" value="<?= $ROW[0]['id'] ?>"/>
        <table class="radius">
            <tr>
                <td class="titulo" colspan="2">Detalle</td>
            </tr>
            <tr>
                <td>Usuario:</td>
                <td><input type="text" id="tmp" name="tmp" class="lista"
                           value="<?= $ROW[0]['nombre'], ' ', $ROW[0]['ap1'], ' ', $ROW[0]['ap2'] ?>" readonly
                           onclick="UsuariosLista()"/>
                    <input type="hidden" id="usuario" name="usuario" value="<?= $ROW[0]['persona'] ?>"/></td>
            </tr>
            <tr>
                <td>Fecha:</td>
                <td><input type="text" id="fecha" name="fecha" class="fecha" value="<?= $ROW[0]['fecha'] ?>" readonly
                           onClick="show_calendar(this.id);"></td>
            </tr>
            <tr>
                <td>Lugar donde fue atendido:</td>
                <td><input type="text" id="lugar" name="lugar" value="<?= $ROW[0]['lugar'] ?>" size="50"
                           maxlength="100"></td>
            </tr>
            <tr>
                <td>Comprobante:</td>
                <td><input type="text" id="comprobante" name="comprobante" value="<?= $ROW[0]['comprobante'] ?>"
                           size="20" maxlength="20"></td>
            </tr>
            <tr>
                <td>Observaciones:</td>
                <td><textarea id="obs" name="obs"><?= $ROW[0]['obs'] ?></textarea></td>
            </tr>
            <tr>
                <td>Adjunto:</td>
                <td id="adjunto">
                    <?php

                    $ruta = "../../caspha-i/docs/citas/" . str_replace(' ', '', $ROW[0]['id']) . '.zip';
                    if ($ROW[0]['id'] != '' && file_exists($ruta)) {
                    ?>
                    <a href="<?= $ruta ?>" style="font-size:12px">[Descargar]</a>&nbsp;<img
                            onclick="DocElimina('<?= $_GET['ID'] ?>')" src="<?php $Gestor->Incluir('del', 'bkg') ?>"
                            border="0" title="Eliminar adjunto" style="vertical-align:middle; cursor:pointer"/></td>
                <?php } else { ?>
                    <input type="file" name="archivo" id="archivo">
                <?php } ?>
            </tr>
        </table>
        <br/>
        <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    </form>
</center>
<?= $Gestor->Encabezado('I0003', 'p', '') ?>
</body>
</html>