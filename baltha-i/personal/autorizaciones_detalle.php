<?php
define('__MODULO__', 'personal');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _autorizaciones_detalle();
$ROW = $Gestor->ObtieneDatos();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('i1', 'hr', 'Personal :: Detalle de Autorizaciones / Inducciones') ?>
<?= $Gestor->Encabezado('I0001', 'e', 'Detalle de Autorizaciones / Inducciones') ?>
<center>
    <table class="radius" width="90%">
        <tr>
            <td class="titulo" colspan="2">Informaci&oacute;n general</td>
        </tr>
        <tr>
            <td><strong>Nombre:</strong></td>
            <td><?= $_GET['nombre'] ?></td>
        </tr>
    </table>
    <br/>
    <table class="radius" width="90%">
        <tr>
            <td class="titulo" colspan="5">Historial</td>
        </tr>
        <tr align="center">
            <td><strong>Formulario</strong></td>
            <td><strong>Tipo</strong></td>
            <td><strong>Fecha</strong></td>
            <td colspan="2" align="center"><strong>Opciones</strong></td>
        </tr>
        <?php
        for ($x = 0; $x < count($ROW); $x++) {
            ?>
            <tr>
                <td><?= $ROW[$x]['formulario'] ?></td>
                <td align="center"><?= $Gestor->Tipo($ROW[$x]['tipo']) ?></td>
                <td align="center"><?= $ROW[$x]['fecha1'] ?></td>
                <td align="center"><img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar"
                                        onclick="DocumentosZip('<?= "ind_{$ROW[$x]['id']}" ?>', '<?= __MODULO__ ?>')"
                                        class="tab3"/></td>
                <td align="center"><img onclick="DocumentosElimina('<?= $ROW[0]['id'] ?>')"
                                        src="<?php $Gestor->Incluir('del', 'bkg') ?>" border="0" title="Eliminar"
                                        class="tab2"/></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <?php if ($Gestor->Escritura()) { ?>
        <br/>
        <form name="form" id="form" method="post" enctype="multipart/form-data"
              action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
            <input type="hidden" id="persona" name="persona" value="<?= $_GET['ID'] ?>"/>
            <table class="radius" width="90%">
                <tr>
                    <td class="titulo" colspan="2">Nuevo registro</td>
                </tr>

                <tr>
                    <td>Tipo:</td>
                    <td><select id="tipo" name="tipo">
                            <option value="0">Inducci&oacute;n General</option>
                            <option value="1">Inducci&oacute;n T&eacute;cnica</option>
                            <option value="2">Autorizaci&oacute;n</option>
                        </select></td>
                </tr>
                <tr>
                    <td>Formulario:</td>
                    <td><input type="text" id="codigo" name="codigo" maxlength="15" title="Alfanumérico (3/15)"></td>
                </tr>
                <tr>
                    <td>Fecha:</td>
                    <td><input type="text" id="fecha" name="fecha" class="fecha" readonly
                               onClick="show_calendar(this.id);"></td>
                </tr>
                <tr>
                    <td>Adjunto:</td>
                    <td><input type="file" name="archivo" id="archivo"></td>
                </tr>
            </table>
            <br/>
            <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos(this)">
        </form>
    <?php } ?>
</center>
<?= $Gestor->Encabezado('I0001', 'p', '') ?>
</body>
</html>