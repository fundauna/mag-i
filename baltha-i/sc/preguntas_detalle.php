<?php
define('__MODULO__', 'sc');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _preguntas_detalle();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js', 5) ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('l11', 'hr', 'Servicio al Cliente :: Opciones de Pregunta') ?>
<?= $Gestor->Encabezado('L0011', 'e', 'Opciones de Pregunta') ?>
<center>
    <input type="hidden" id="id" value="<?= $_GET['ID'] ?>"/>
    <table class="radius" align="center">
        <tr>
            <td class="titulo" colspan="3">Opciones Registradas</td>
        </tr>
        <tr>
            <td>Descripci&oacute;n</td>
            <td colspan="2" align="right">Opciones</td>
        </tr>
        <?php
        $ROW = $Gestor->OpcionesMuestra();
        for ($x = 0; $x < count($ROW); $x++) {
            ?>
            <tr>
                <td><input type="text" id="descr<?= $ROW[$x]['linea'] ?>" value="<?= $ROW[$x]['opcion'] ?>"/></td>
                <td align="center"><img src="<?php $Gestor->Incluir('guardar', 'bkg') ?>"
                                        onclick="datos('M', '<?= $_GET['ID'] ?>', '<?= $ROW[$x]['linea'] ?>')"
                                        title="Guardar Cambios" class="tab2"/></td>
                <td align="center"><img src="<?php $Gestor->Incluir('del', 'bkg') ?>"
                                        onclick="datos('D', '<?= $_GET['ID'] ?>', '<?= $ROW[$x]['linea'] ?>')"
                                        title="Eliminar Opci�n" class="tab2"/></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <br/>
    <table class="radius" align="center">
        <tr>
            <td class="titulo" colspan="2">Ingresar nueva opci&oacute;n</td>
        </tr>
        <tr>
            <td>Descripci&oacute;n:</td>
            <td><input type="text" id="descr" name="descr" size="40" maxlength="100"/></td>
        </tr>
    </table>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos('I','<?= $_GET['ID'] ?>','99')">
</center>
<?= $Gestor->Encabezado('L0011', 'p', '') ?>
</body>
</html>