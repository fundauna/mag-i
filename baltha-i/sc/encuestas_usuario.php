<?php
define('__MODULO__', 'sc');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);
$Gestor = new _encuestas_usuario();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('p9', 'hr', 'Servicio al Cliente :: Encuestas pendientes') ?>
<?= $Gestor->Encabezado('P0009', 'e', 'Encuestas pendientes') ?>
<table class="radius" align="center" style="font-size:12px; width:350px">
    <tr>
        <td class="titulo" colspan="3">Encuestas pendientes</td>
    </tr>
    <tr>
        <td><b>Nombre</b></td>
        <td align="center"><b>Fecha</b></td>
        <td></td>
    </tr>
    <?php
    $ROW = $Gestor->EncuestaMuestra();
    for ($x = 0; $x < count($ROW); $x++) {
        ?>
        <tr>
            <td><?= $ROW[$x]['nombre'] ?></td>
            <td align="center"><?= $ROW[$x]['fecha'] ?></td>
            <td align="center"><img onclick="EncuestaVer('<?= $ROW[$x]['id'] ?>')"
                                    src="<?php $Gestor->Incluir('mod', 'bkg') ?>" border="0" title="Completar encuesta"
                                    class="tab2"/>
            </td>
        </tr>
    <?php } ?>
</table>
<?= $Gestor->Encabezado('P0009', 'p', '') ?>
</body>
</html>