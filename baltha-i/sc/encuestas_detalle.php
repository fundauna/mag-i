<?php
define('__MODULO__', 'sc');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _encuestas_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Encuesta inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js', 5) ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('l3', 'hr', 'Servicio al Cliente :: Detalle Encuesta') ?>
<?= $Gestor->Encabezado('L0003', 'e', 'Detalle Encuesta') ?>
<center>
    <form name="form" id="form" method="post" enctype="multipart/form-data"
          action="../../caspha-i/shell/sc/_encuestas_detalle.php">
        <input type="hidden" id="accion" name="accion" value="<?= $_GET['acc'] ?>"/>
        <input type="hidden" id="id" name="id" value="<?= $ROW[0]['id'] ?>"/>
        <table class="radius">
            <tr>
                <td class="titulo" colspan="2">Datos</td>
            </tr>
            <tr>
                <td>Nombre:</td>
                <td><input type="text" id="nombre" name="nombre" value="<?= $ROW[0]['nombre'] ?>" size="30"
                           maxlength="50"></td>
            </tr>
            <tr>
                <td>Estado:</td>
                <td><select id="estado" name="estado">
                        <?php if ($_GET['acc'] == 'I') { ?>
                            <option value='0'>Inactiva</option>
                        <?php } else { ?>
                            <option value='0' <?= ($ROW[0]['estado'] == '0') ? 'selected' : '' ?>>Inactiva</option>
                            <option value='1' <?= ($ROW[0]['estado'] == '1') ? 'selected' : '' ?>>Activa</option>
                            <option value='2' <?= ($ROW[0]['estado'] == '2') ? 'selected' : '' ?>>Finalizada</option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr class="blokeado">
                <td>Adjuntar documento:</td>
                <td><input type="file" name="archivo" id="archivo" class="boton"></td>
            </tr>
            <?php
            $ruta = "../../caspha-i/docs/sc/encuestas/" . $_GET['ID'] . '.zip';
            if ($_GET['acc'] == 'M' && file_exists($ruta)) {
                ?>
                <tr>
                    <td colspan="2">
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td>Descargar documento:</td>
                    <td align="center"><img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar"
                                            onclick="DocumentosZip('<?= "{$ROW[0]['id']}" ?>', '<?= __MODULO__ ?>/encuestas')"
                                            class="tab3"/></td>
                </tr>
                <?php
            }
            ?>
        </table>
    </form>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos('<?= $_GET['acc'] ?>')">
</center>
<?= $Gestor->Encabezado('L0003', 'p', '') ?>
</body>
</html>