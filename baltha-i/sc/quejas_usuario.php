<?php
define('__MODULO__', 'sc');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _quejas();
$lab = isset($_POST['instancia']) ? $_POST['instancia'] : '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('l12', 'hr', 'Servicio al Cliente :: Control de quejas') ?>
<?= $Gestor->Encabezado('L0012', 'e', 'Control de quejas') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" style="width:470px">
            <tr>
                <td class="titulo" colspan="2">Filtro</td>
            </tr>
            <tr>
                <td>Desde: <input type="text" id="desde" name="desde" class="fecha" value="<?= $_POST['desde'] ?>"
                                  readonly onClick="show_calendar(this.id);">&nbsp;
                    Hasta: <input type="text" id="hasta" name="hasta" class="fecha" value="<?= $_POST['hasta'] ?>"
                                  readonly onClick="show_calendar(this.id);">&nbsp;
                    Instancia: <select id="instancia" name="instancia">
                        <option <?= $lab == '' ? 'selected' : '' ?> value="">...</option>
                        <option <?= $lab == '0' ? 'selected' : '' ?> value="0">No Procede</option>
                        <option <?= $lab == '1' ? 'selected' : '' ?> value="1">LRE</option>
                        <option <?= $lab == '2' ? 'selected' : '' ?> value="2">LDP</option>
                        <option <?= $lab == '3' ? 'selected' : '' ?> value="3">LCC</option>
                        <option <?= $lab == '4' ? 'selected' : '' ?> value="4">Jefatura</option>
                    </select>
                    <input type="button" value="Buscar" class="boton2" onclick="QuejasBuscar(this)"/>
                </td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:470px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>C&oacute;digo</th>
                <th>Fecha</th>
                <th>Instancia</th>
                <th>Estado</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->QuejasMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#" onclick="QuejasModificar('<?= $ROW[$x]['cs'] ?>')"><?= $ROW[$x]['codigo'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['fecha'] ?></td>
                    <td><?= $Gestor->Tipo($ROW[$x]['instancia']) ?></td>
                    <td><?= $ROW[$x]['estado'] == '1' ? 'Abierta' : 'Cerrada' ?></td>
                    <td>
                        <?php if ($ROW[$x]['estado'] == '1'): ?>
                            <img onclick="QuejasElimina('<?= $ROW[$x]['cs'] ?>')"
                                 src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Cerrar" class="tab2"/>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/>
    <input type="button" class="boton" value="Agregar" onclick="QuejasAgregar()"/>
</center>
<?= $Gestor->Encabezado('L0012', 'p', '') ?>
</body>
</html>