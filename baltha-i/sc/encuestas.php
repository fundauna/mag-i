<?php
define('__MODULO__', 'sc');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);
$Gestor = new _encuestas();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
    <script>
        function Recarga(_valor) {
            if (_valor == '') return;
            ALFA('Cargando');
            location.href = 'encuestas.php?estado=' + _valor;

        }
    </script>
</head>
<body>
<?php $Gestor->Incluir('l2', 'hr', 'Servicio al Cliente :: Encuestas') ?>
<?= $Gestor->Encabezado('L0002', 'e', 'Encuestas') ?>
<center>
    <table class="radius" align="center">
        <tr>
            <td class="titulo" colspan="2">Filtro</td>
        </tr>
        <tr>
            <td>Estado:&nbsp;</td>
            <td>
                <select id="tipo" onchange="Recarga(this.value)">
                    <option value="">...</option>
                    <option <?= $_GET['estado'] == '0' ? 'selected' : '' ?> value="0">Inactiva</option>
                    <option <?= $_GET['estado'] == '1' ? 'selected' : '' ?> value="1">Activa</option>
                    <option <?= $_GET['estado'] == '2' ? 'selected' : '' ?> value="2">Finalizada</option>
                </select>
            </td>
        </tr>
    </table>
    <br/>
    <div id="container" style="width:650px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Fecha</th>
                <th>Estado</th>
                <th>Opciones</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->EncuestaMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#" onclick="EncuestaModifica('<?= $ROW[$x]['id'] ?>')"><?= $ROW[$x]['nombre'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['fecha'] ?></td>
                    <td><?php
                        if ($ROW[$x]['estado'] == '0') echo 'Inactiva';
                        elseif ($ROW[$x]['estado'] == '1') echo 'Activa';
                        elseif ($ROW[$x]['estado'] == '2') echo 'Finalizada';
                        else echo '?'; ?>
                    </td>
                    <td>
                        <?php if ($ROW[$x]['estado'] == '0') { ?>
                            <img onclick="location.href = 'preguntas.php?id=<?= $ROW[$x]['id'] ?>';"
                                 src="<?php $Gestor->Incluir('mod', 'bkg') ?>" border="0"
                                 title="Agregar/Modificar Preguntas" class="tab2"/>
                            <img onclick="EncuestaEnvia('<?= $ROW[$x]['id'] ?>')"
                                 src="<?php $Gestor->Incluir('mail', 'bkg') ?>" border="0" title="Enviar encuesta"
                                 class="tab2"/>
                        <?php }
                        if ($ROW[$x]['estado'] == '1') { ?>
                            <img onclick="EncuestaVer('<?= $ROW[$x]['id'] ?>')"
                                 src="<?php $Gestor->Incluir('ver', 'bkg') ?>" border="0" title="Ver Encuesta"
                                 class="tab2"/>
                        <?php }
                        if ($ROW[$x]['estado'] == '2') { ?>
                            <img onclick="EncuestaExporta('<?= $ROW[$x]['id'] ?>')"
                                 src="<?php $Gestor->Incluir('estadistica', 'bkg') ?>" border="0"
                                 title="Exportar Resultados" class="tab2"/>
                        <?php }
                        if ($ROW[$x]['estado'] != '') { ?>
                            <img onclick="EncuestaMuestra('<?= $ROW[$x]['id'] ?>')"
                                 src="<?php $Gestor->Incluir('previa', 'bkg') ?>" border="0" title="Vista Previa"
                                 class="tab2"/>
                            <img onclick="EncuestaElimina('<?= $ROW[$x]['id'] ?>')"
                                 src="<?php $Gestor->Incluir('del', 'bkg') ?>" border="0" title="Eliminar"
                                 class="tab2"/>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/><input type="button" value="Agregar" class="boton" onClick="EncuestaAgrega();">
</center>
<?= $Gestor->Encabezado('L0002', 'p', '') ?>
</body>
</html>