<?php
define('__MODULO__', 'sc');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);
$Gestor = new _encuestas_vistas();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js', 5) ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('l9', 'hr', 'Servicio al Cliente :: Vista Previa');
$ROW = $Gestor->Nombre(); ?>
<?= $Gestor->Encabezado('L0009', 'e', 'Vista Previa') ?>
<center>
    <table class="radius" align="center" width="80%">
        <tr>
            <td class="titulo"><?= $ROW[0]['nombre'] ?></td>
        </tr>
        <?php
        $ROW = $Gestor->VistaPrevia();
        for ($x = 0; $x < count($ROW); $x++) {
            if ($ROW[$x]['tipo'] == 0) {
                ?>
                <tr>
                    <td><?= $ROW[$x]['descri'] ?>:</td>
                </tr>
                <tr>
                    <td><select style="width:200px">
                            <option>...</option>
                            <option>S&iacute;</option>
                            <option>No</option>
                        </select></td>
                </tr>
                <?php
            } elseif ($ROW[$x]['tipo'] == 1) {
                ?>
                <tr>
                    <td><?= $ROW[$x]['descri'] ?>:</td>
                </tr>
                <tr>
                    <td><input type="text" size="50px"/></td>
                </tr>
                <?php
            } elseif ($ROW[$x]['tipo'] == 2) {
                ?>
                <tr>
                    <td><?= $ROW[$x]['descri'] ?>:</td>
                </tr>
                <tr>
                    <td>
                        <select style="width:200px">
                            <option>...</option>
                            <?php
                            $ROW2 = $Gestor->GetOpciones($ROW[$x]['id']);
                            for ($y = 0; $y < count($ROW2); $y++) {
                                ?>
                                <option><?= $ROW2[$y]['opcion'] ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <?php
            } elseif ($ROW[$x]['tipo'] == 3) {
                ?>
                <tr>
                    <td><?= $ROW[$x]['descri'] ?>:</td>
                </tr>
                <tr>
                    <td>
                        <?php
                        $ROW2 = $Gestor->GetOpciones($ROW[$x]['id']);
                        for ($y = 0; $y < count($ROW2); $y++) {
                            ?>
                            <input type="checkbox"/><?= $ROW2[$y]['opcion'] ?>
                        <?php } ?>
                    </td>
                </tr>
                <?php
            } elseif ($ROW[$x]['tipo'] == 4) {
                ?>
                <tr>
                    <td><?= $ROW[$x]['descri'] ?>:</td>
                </tr>
                <tr>
                    <td><textarea></textarea></td>
                </tr>
                <?php
            }
        }
        ?>
    </table>
    <br/>
    <input type="button" id="btn" value="Imprimir" class="boton" onClick="print();">
</center>
<?= $Gestor->Encabezado('L0009', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>