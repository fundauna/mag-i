<?php
define('__MODULO__', 'sc');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);
$Gestor = new _encuestas_exporta();
$ROW = $Gestor->GetEncuesta($_GET['ID']);
if (isset($_GET['export'])) {
    header('Content-type: application/x-msdownload');
    header('Content-Disposition: attachment; filename=Encuesta_' . $ROW[0]['nombre'] . '.xls');
    header('Pragma: no-cache');
    header('Expires: 0');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('l5', 'hr', 'Servicio al Cliente :: Resultados de Encuesta') ?>
<?= $Gestor->Encabezado('L0005', 'e', 'Resultados de Encuesta') ?>
<center>
    <table class="radius" align="center" width="650px">
        <tr>
            <td class="titulo" align="center" colspan="3"><b><?= $ROW[0]['nombre'] ?></b></td>
        </tr>
        <?php
        $ROW = $Gestor->Paso0();
        if ($ROW) {
            ?>
            <tr>
                <td align="center" colspan="3"><strong><?= $ROW[0]['descri'] ?></strong></td>
            </tr>
            <tr>
                <td><b>Cliente</b></td>
                <td align="center"><b>S&iacute;</b></td>
                <td align="center"><b>No</b></td>
            </tr>
            <?php
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr>
                    <td><?= $ROW[$x]['nombre'] ?></td>
                    <td align="center"><?= $ROW[$x]['respuesta'] == 1 ? 'X' : '' ?></td>
                    <td align="center"><?= $ROW[$x]['respuesta'] == 0 ? 'X' : '' ?></td>
                </tr>
                <?php
            }
        }

        $ROW = $Gestor->Paso1('1');
        if ($ROW) {
            ?>
            <tr>
                <td colspan="3">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3"><strong><?= $ROW[0]['descri'] ?></strong></td>
            </tr>
            <tr>
                <td><b>Cliente</b></td>
                <td colspan="2"></td>
            </tr>
            <?php
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr>
                    <td><?= $ROW[$x]['nombre'] ?></td>
                    <td colspan="2"><?= $ROW[$x]['desarrollo'] ?></td>
                </tr>
                <?php
            }
        }

        $ROW = $Gestor->Paso1('4');
        if ($ROW) {
            ?>
            <tr>
                <td colspan="3">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3"><strong><?= $ROW[0]['descri'] ?></strong></td>
            </tr>
            <tr>
                <td><b>Cliente</b></td>
                <td colspan="2"></td>
            </tr>
            <?php
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr>
                    <td><?= $ROW[$x]['nombre'] ?></td>
                    <td colspan="2"><?= $ROW[$x]['desarrollo'] ?></td>
                </tr>
                <?php
            }
        }

        $ROW = $Gestor->Paso2();
        if ($ROW) {
            $anterior = '';
            for ($x = 0; $x < count($ROW); $x++) {
                if ($anterior != $ROW[$x]['descri']) {
                    $anterior = $ROW[$x]['descri'];
                    echo '<tr><td colspan="3"><hr /></td></tr>';
                } else $ROW[$x]['descri'] = '';
                ?>
                <tr>
                    <td><strong><?= $ROW[$x]['descri'] ?></strong></td>
                    <td><?= $ROW[$x]['opcion'] ?></td>
                    <td><?= $ROW[$x]['total'] ?></td>
                </tr>
                <?php
            }
        }
        ?>
    </table>
</center>
<?= $Gestor->Encabezado('L0005', 'p', '') ?>
</body>
</html>