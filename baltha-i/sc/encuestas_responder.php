<?php
define('__MODULO__', 'sc');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);
$Gestor = new _encuestas_responder();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Encuesta inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js', 1); ?>
    <?php $Gestor->Incluir('validaciones', 'js', 5) ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('p11', 'hr', 'Servicio al Cliente :: Completar encuesta'); ?>
<?= $Gestor->Encabezado('L0011', 'e', 'Completar encuesta') ?>
<center>
    <table class="radius" align="center" width="80%">
        <tr>
            <td class="titulo" colspan="3"><?= $ROW[0]['nombre'] ?></td>
        </tr>
        <?php
        $ROW = $Gestor->VistaPrevia();
        $str = ''; //preguntas normales
        $tipos = '';
        for ($x = 0; $x < count($ROW); $x++) {
            if ($ROW[$x]['tipo'] != 3) {
                $str .= ';' . $ROW[$x]['id'];
                $tipos .= ';' . $ROW[$x]['tipo'];
            }

            if ($ROW[$x]['tipo'] == 0) {
                ?>
                <tr>
                    <td><?= ($x + 1) ?>-&nbsp;<?= $ROW[$x]['descri'] ?>:</td>
                </tr>
                <tr>
                    <td><select name="pregunta" style="width:200px" tipo="0">
                            <option value="">...</option>
                            <option value="1">S&iacute;</option>
                            <option value="0">No</option>
                        </select>
                    </td>
                </tr>
                <?php
            } elseif ($ROW[$x]['tipo'] == 1) {
                ?>
                <tr>
                    <td><?= ($x + 1) ?>-&nbsp;<?= $ROW[$x]['descri'] ?>:</td>
                </tr>
                <tr>
                    <td><input type="text" name="pregunta" size="50px" tipo="0"/></td>
                </tr>
                <?php
            } elseif ($ROW[$x]['tipo'] == 2) {
                ?>
                <tr>
                    <td><?= ($x + 1) ?>-&nbsp;<?= $ROW[$x]['descri'] ?>:</td>
                </tr>
                <tr>
                    <td>
                        <select name="pregunta" style="width:200px" tipo="0">
                            <option value="">...</option>
                            <?php
                            $ROW2 = $Gestor->GetOpciones($ROW[$x]['id']);
                            for ($y = 0; $y < count($ROW2); $y++) {
                                ?>
                                <option value="<?= $ROW2[$y]['linea'] ?>"><?= $ROW2[$y]['opcion'] ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <?php
            } elseif ($ROW[$x]['tipo'] == 3) {
                //$str2 .= ';'.$ROW[$x]['id'];
                ?>
                <tr>
                    <td><?= ($x + 1) ?>-&nbsp;<?= $ROW[$x]['descri'] ?>:</td>
                </tr>
                <tr>
                    <td>
                        <input type="hidden" name="pregunta" tipo="1" id="<?= $ROW[$x]['id'] ?>"/>
                        <!-- DUMMY PARA QUE NO CONTBABILICE LOS CHECK COMO UNA PREGUNTA-->
                        <?php
                        $ROW2 = $Gestor->GetOpciones($ROW[$x]['id']);
                        for ($y = 0; $y < count($ROW2); $y++) {
                            ?>
                            <input type="checkbox" name="check<?= $ROW[$x]['id'] ?>"
                                   value="<?= $ROW2[$y]['linea'] ?>" /><?= $ROW2[$y]['opcion'] ?>
                        <?php } ?>
                    </td>
                </tr>
                <?php
            } elseif ($ROW[$x]['tipo'] == 4) {
                ?>
                <tr>
                    <td><?= ($x + 1) ?>-&nbsp;<?= $ROW[$x]['descri'] ?>:</td>
                </tr>
                <tr>
                    <td><textarea name="pregunta" tipo="0"></textarea></td>
                </tr>
                <?php
            }
        }
        ?>
    </table>
    <input type="hidden" id="tipos" value="<?= $tipos ?>"/>
    <input type="hidden" id="preguntas1" value="<?= $str ?>"/>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos();">
</center>
<?= $Gestor->Encabezado('L0011', 'p', '') ?>
</body>
</html>