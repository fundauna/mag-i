<?php
define('__MODULO__', 'sc');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _quejas_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Documento inexistente');
$ROW2 = $Gestor->ObtieneResponsables();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<form name="form" id="form" method="post" enctype="multipart/form-data"
      action="../../caspha-i/shell/sc/_quejas_detalle.php">
    <input type="hidden" id="accion" name="accion" value="<?= $_GET['acc'] ?>"/>
    <input type="hidden" id="cs" name="cs" value="<?= $ROW[0]['cs'] ?>"/>
    <center>
        <?php $Gestor->Incluir('l13', 'hr', 'Servicio al Cliente :: An&aacute;lisis :: Control de Quejas presentadas contra el Departamento de Laboratorios') ?>
        <?= $Gestor->Encabezado('L0013', 'e', 'Control de Quejas presentadas contra el Departamento de Laboratorios') ?>
        <br/>
        <table class="radius" style="font-size:12px" width="98%">
            <tr>
                <td class="titulo" colspan="4" align="center">Informaci&oacute;n General</td>
            </tr>
            <tr>
                <td>C&oacute;digo:</td>
                <td><input type="text" id="codigo" name="codigo" value="<?= str_replace(' ', '', $ROW[0]['codigo']) ?>"
                           size="15" maxlength="15"></td>
                <td>Fecha de presentaci&oacute;n:</td>
                <td><input type="text" id="fec_pre" name="fec_pre" class="fecha2" value="<?= $ROW[0]['fec_pre'] ?>"
                           readonly onClick="show_calendar(this.id);"></td>
            </tr>
            <tr>
                <td>Instancia Involucrada:</td>
                <td><select id="instancia" name="instancia">
                        <option value=''>...</option>
                        <option value="0" <?= $ROW[0]['instancia'] == '0' ? 'selected' : '' ?>>No Procede</option>
                        <option value="1" <?= $ROW[0]['instancia'] == '1' ? 'selected' : '' ?>>LRE</option>
                        <option value="2" <?= $ROW[0]['instancia'] == '2' ? 'selected' : '' ?>>LDP</option>
                        <option value="3" <?= $ROW[0]['instancia'] == '3' ? 'selected' : '' ?>>LCC</option>
                        <option value="4" <?= $ROW[0]['instancia'] == '4' ? 'selected' : '' ?>>Jefatura</option>
                    </select></td>
                <td>Estado:</td>
                <td><select id="estado" name="estado">
                        <option value=''>...</option>
                        <option value="1" <?= $ROW[0]['estado'] == '1' ? 'selected' : '' ?>>Abierta</option>
                        <option value="0" <?= $ROW[0]['estado'] == '0' ? 'selected' : '' ?>>Cerrada</option>
                    </select></td>
            </tr>
            <tr>
                <td>
                    <?php
                    $ruta = "../../caspha-i/docs/sc/{$_GET['ID']}.zip";
                    if (file_exists($ruta)) {
                        ?>
                        Descargar adjunto:&nbsp;<img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar"
                                                     onclick="DocumentosZip('<?= $_GET['ID'] ?>', '<?= __MODULO__ ?>')"
                                                     class="tab2"/>&nbsp;&nbsp;&nbsp;&nbsp;
                    <?php } ?>
                </td>
                <td colspan="3">Adjuntar documento:&nbsp;<input type="file" name="archivo" id="archivo"></td>
            </tr>
        </table>
        <br/>
        <table class="radius" style="font-size:12px" width="98%">
            <tr>
                <td class="titulo" colspan="4" align="center">Detalle</td>
            </tr>
            <tr>
                <td>Fecha l&iacute;mite de respuesta:</td>
                <td><input type="text" id="fec_lim" name="fec_lim" class="fecha2" readonly
                           onClick="show_calendar(this.id);" value="<?= $ROW[0]['fec_lim'] ?>"></td>
                <td>Responsable de atender queja:</td>
                <td><input type="text" id="tmpA" class="lista2" readonly onclick="UsuariosLista('A')"
                           value="<?= $ROW2[0]['tmpA'] ?>"/><input type="hidden" id="usuarioA" name="usuarioA"
                                                                   value="<?= $ROW2[0]['usuarioA'] ?>"/></td>
            </tr>
            <tr>
                <td>Fecha recibido G.C. y Jefatura de departamento:</td>
                <td><input type="text" id="fec_rec" name="fec_rec" class="fecha2" readonly
                           onClick="show_calendar(this.id);" value="<?= $ROW[0]['fec_rec'] ?>"></td>
                <td>Queja finiquitada por:</td>
                <td><input type="text" id="tmpB" class="lista2" readonly onclick="UsuariosLista('B')"
                           value="<?= $ROW2[0]['tmpB'] ?>"/><input type="hidden" id="usuarioB" name="usuarioB"
                                                                   value="<?= $ROW2[0]['usuarioB'] ?>"/></td>
            </tr>
            <tr>
                <td>Fecha recibido responsable:</td>
                <td><input type="text" id="fec_res" name="fec_res" class="fecha2" readonly
                           onClick="show_calendar(this.id);" value="<?= $ROW[0]['fec_res'] ?>"></td>
                <td>AC/AP asociada:</td>
                <td><input type="text" id="acap" name="acap" size="15" maxlength="15" value="<?= $ROW[0]['acap'] ?>"/>
                </td>
            </tr>
            <tr>
                <td>Fecha de notificaci&oacute;n a G.C.:</td>
                <td><input type="text" id="fec_cal" name="fec_cal" class="fecha2" readonly
                           onClick="show_calendar(this.id);" value="<?= $ROW[0]['fec_cal'] ?>"></td>
                <td colspan="2">Observaciones</td>
            </tr>
            <tr>
                <td>Fecha de notificaci&oacute;n a Contralor&iacute;a de Servicios:</td>
                <td><input type="text" id="fec_ser" name="fec_ser" class="fecha2" readonly
                           onClick="show_calendar(this.id);" value="<?= $ROW[0]['fec_ser'] ?>"></td>
                <td colspan="2" rowspan="2"><textarea id="obs" name="obs"
                                                      style="width:98%"><?= $ROW[0]['obs'] ?></textarea></td>
            </tr>
            <tr>
                <td>Fecha de notificaci&oacute;n al cliente:</td>
                <td><input type="text" id="fec_cli" name="fec_cli" class="fecha2" readonly
                           onClick="show_calendar(this.id);" value="<?= $ROW[0]['fec_cli'] ?>"></td>
            </tr>
        </table>
        <br/><?= $Gestor->Encabezado('L0013', 'p', '') ?>
        <br/>
        <input type="button" value="Aceptar" class="boton" onclick="QuejasAgregar(this)">
    </center>
</form>
</body>
</html>