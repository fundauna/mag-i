<?php
define('__MODULO__', 'sc');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _encuestas_enviar();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW)
    die('Encuesta inexistente');

if ($ROW[0]['estado'] != '0')
    die('Solo las encuestas inactivas pueden ser enviadas');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('l4', 'hr', 'Servicio al Cliente :: Enviar Encuesta') ?>
<?= $Gestor->Encabezado('L0004', 'e', 'Enviar Encuesta') ?>
<center>
    <input type="hidden" id="id" name="id" value="<?= $ROW[0]['id'] ?>"/>
    <table class="radius">
        <tr>
            <td class="titulo" colspan="2">Datos</td>
        </tr>
        <tr>
            <td>Nombre:</td>
            <td><?= $ROW[0]['nombre'] ?></td>
        </tr>
    </table>
    <br/>
    Clientes
    <div id="container" style="width:550px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example" style="font-size:12px;">
            <thead>
            <tr>
                <th>&nbsp;</th>
                <th>Id</th>
                <th>Nombre</th>
                <th>Estado</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->ClientesMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><input type="checkbox" name="id" value="<?= $ROW[$x]['id'] ?>"
                               <?= $_GET['T'] == 'T' ? 'checked' : '' ?>/></td>
                    <td><?= $ROW[$x]['id'] ?></td>
                    <td><?= $ROW[$x]['nombre'] ?></td>
                    <td><?= $ROW[$x]['estado'] ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/>
    <input type="button" id="btnSA" value="<?= $_GET['T'] == 'T' ? 'Desmarcar' : 'Marcar' ?> a todos los clientes"
           class="boton" onClick="todos('<?= $_GET['ID'] ?>','<?= $_GET['T'] ?>')">
    <input type="button" id="btn" value="Enviar" class="boton" onClick="datos()">
</center>
<?= $Gestor->Encabezado('L0004', 'p', '') ?>
</body>
</html>