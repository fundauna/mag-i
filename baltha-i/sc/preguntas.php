<?php
define('__MODULO__', 'sc');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);
$Gestor = new _preguntas();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('l10', 'hr', 'Servicio al Cliente :: Preguntas');
$ROW = $Gestor->GetEncuesta($_GET['id']); ?>
<?= $Gestor->Encabezado('L0010', 'e', 'Preguntas') ?>
<table class="radius" align="center">
    <tr>
        <td class="titulo" colspan="2">Preguntas Registradas Encuesta "<?= $ROW[0]['nombre'] ?>"</td>
    </tr>
    <tr>
        <td colspan="2">
            <select size="10" style="width:550px" onclick="marca(this);">
                <?php
                $ROW = $Gestor->PreguntasMuestra();
                for ($x = 0; $x < count($ROW); $x++) {
                    echo "<option id='{$ROW[$x]['id']}' descri='{$ROW[$x]['descri']}' tipo='{$ROW[$x]['tipo']}'>{$ROW[$x]['descri']}</option>";
                }
                unset($ROW);
                ?>
            </select>
        </td>
    </tr>
</table>
<br/>
<table class="radius" align="center">
    <input readonly type="hidden" id="enc" value="<?= $_GET['id'] ?>">
    <input readonly type="hidden" id="id">
    <tr>
        <td class="titulo" colspan="2">Datos</td>
    </tr>
    <tr>
        <td><b>Pregunta:&nbsp;</b></td>
        <td><textarea id="descripcion"></textarea></td>
    </tr>

    <tr>
        <td><b>Tipo:&nbsp;</b></td>
        <td>
            <select id="tipo">
                <option value="">...</option>
                <option value="0">S&iacute;/No</option>
                <option value="1">Respuesta Breve</option>
                <option value="2">Selecci&oacute;n &uacute;nica</option>
                <option value="3">Selecci&oacute;n m&uacute;ltiple</option>
                <option value="4">Desarrollo</option>
            </select>
        </td>
    </tr>
    <tr id="tr1" style="display: none;">
        <td><b>Opciones:&nbsp;</b></td>
        <td><input type="button" value="Modificar" class="boton"
                   onClick="OpcionIM(document.getElementById('id').value);"></td>
    </tr>
    </tr>
</table>
<br/>
<center>
    <input id="new" type="button" class="boton" value="Limpiar" onclick="desmarca();" disabled>&nbsp;
    <input id="add" type="button" class="boton" value="Agregar" onclick="modificar('I');">&nbsp;
    <input id="mod" type="button" class="boton" value="Modificar" onclick="modificar('M');" disabled>&nbsp;
    <input id="del" type="button" class="boton" value="Eliminar" onclick="modificar('D');" disabled>
    <br/><br/>
    <a href="encuestas.php">Regresar</a>
</center>
<?= $Gestor->Encabezado('L0010', 'p', '') ?>
</body>
</html>