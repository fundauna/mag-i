<?php
define('__MODULO__', 'sc');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _clientes();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('l0', 'hr', 'Servicio al Cliente :: Clientes') ?>
<?= $Gestor->Encabezado('L0000', 'e', 'Clientes') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" style="width:650px">
            <tr>
                <td class="titulo" colspan="4">Filtro</td>
            </tr>
            <tr>
                <td>Laboratorio: <select name="filtro">
                        <option value="1">Actual</option>
                        <option value="0" <?php if ($_POST['filtro'] == 0) echo 'selected'; ?>>Todos</option>
                    </select></td>
                <td><input type="radio" id="lolo1" name="lolo" onclick="marca(1)"
                           value="1" <?php if ($_POST['lolo'] == 1) echo 'checked'; ?> />&nbsp;Id: <input type="text"
                                                                                                          id="id"
                                                                                                          name="id"
                                                                                                          value="<?= $_POST['id'] ?>"/>
                </td>
                <td><input type="radio" id="lolo2" name="lolo" onclick="marca(2)"
                           value="2" <?php if ($_POST['lolo'] == 2) echo 'checked'; ?>/>&nbsp;Nombre: <input type="text"
                                                                                                             name="nombre"
                                                                                                             id="nombre"
                                                                                                             readonly
                                                                                                             value="<?= $_POST['nombre'] ?>"/>
                </td>
                <td><input type="button" value="Buscar" class="boton2" onclick="ClientesBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:650px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Tel&eacute;fono</th>
                <th>Estado</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->ClientesMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#" onclick="ClientesModifica('<?= $ROW[$x]['id'] ?>')"><?= $ROW[$x]['id'] ?></a></td>
                    <td><?= $ROW[$x]['nombre'] ?></td>
                    <td><?= $ROW[$x]['tel'] ?></td>
                    <td><?= $ROW[$x]['estado'] ?></td>
                    <td><img onclick="ClientesElimina('<?= $ROW[$x]['id'] ?>')"
                             src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar" class="tab2"/></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/><input type="button" value="Agregar" class="boton" onClick="ClientesAgrega();">&nbsp;&nbsp;&nbsp;&nbsp;<input
            type="button" value="Exportar" class="boton" onClick="ClientesExportar('<?= $Gestor->obtieneLAB() ?>');">
</center>
<?= $Gestor->Encabezado('L0000', 'p', '') ?>
</body>
</html>