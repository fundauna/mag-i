<?php
define('__MODULO__', 'sc');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);
$Gestor = new _encuestas_ver();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js', 5) ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('l8', 'hr', 'Servicio al Cliente :: Ver Encuesta');
$ROW = $Gestor->GetEncuesta($_GET['ID']); ?>
<?= $Gestor->Encabezado('L0008', 'e', 'Ver Encuesta') ?>
<center>
    <form action="<?= basename(__FILE__) ?>?ID=<?= $_GET['ID'] ?>" method="post">
        <table class="radius" align="center" style="font-size:12px;width:300px;">
            <tr>
                <td class="titulo" colspan="4">Filtro</td>
            </tr>
            <tr>
                <input type="hidden" id="client" name="client" value="<?= $_POST['client'] ?>"/>
                <td>Cliente:&nbsp;<input type="text" id="cliente" name="cliente" class="lista"
                                         value="<?= $_POST['cliente'] ?>" readonly onclick="ClientesLista(0)"/></td>
                <td><input type="button" value="Buscar" class="boton2" onclick="ClienteBuscar(this)"/></td>
            </tr>
        </table>
        <br/>
    </form>
    <table class="radius" align="center" width="650px">
        <tr>
            <td class="titulo">Encuesta de <?= $ROW[0]['nombre'] ?></td>
        </tr>
        <?php
        $ROW = $Gestor->Respuestas();
        for ($x = 0; $x < count($ROW); $x++) {
            if ($ROW[$x]['tipo'] == 0) {
                ?>
                <tr>
                    <td><?= $ROW[$x]['descri'] ?></td>
                </tr>
                <tr>
                    <td><select style="width:200px" disabled>
                            <option <?= $ROW[$x]['respuesta'] == '1' ? 'selected' : '' ?>>S&iacute;</option>
                            <option <?= $ROW[$x]['respuesta'] == '0' ? 'selected' : '' ?>>No</option>
                        </select>
                    </td>
                </tr>
                <?php
            } elseif ($ROW[$x]['tipo'] == 1) {
                ?>
                <tr>
                    <td><?= $ROW[$x]['descri'] ?></td>
                </tr>
                <tr>
                    <td><input readonly type="text" size="50px" value="<?= $ROW[$x]['desarrollo'] ?>"/></td>
                </tr>
                <?php
            } elseif ($ROW[$x]['tipo'] == 2) {
                ?>
                <tr>
                    <td><?= $ROW[$x]['descri'] ?></td>
                </tr>
                <tr>
                    <td><select style="width:200px" disabled>
                            <?php
                            $ROW2 = $Gestor->GetOpciones($ROW[$x]['id']);
                            for ($y = 0; $y < count($ROW2); $y++) {
                                ?>
                                <option
                                    <?= $ROW[$x]['respuesta'] == $ROW2[$y]['linea'] ? 'selected' : '' ?>><?= $ROW2[$y]['opcion'] ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <?php
            } elseif ($ROW[$x]['tipo'] == 3) {
                ?>
                <tr>
                    <td><?= $ROW[$x]['descri'] ?></td>
                </tr>
                <tr>
                    <td>
                        <?php
                        $ROW2 = $Gestor->GetOpciones($ROW[$x]['id']);
                        $ROW3 = $Gestor->GetCheckbox($ROW[$x]['id']);
                        //$token = 0;
                        for ($y = 0; $y < count($ROW2); $y++) {
                            ?>
                            <input disabled type="checkbox" <?php for ($z = 0; $z < count($ROW3); $z++) {
                                if ($ROW3[$z]['respuesta'] == $ROW2[$y]['linea']) {
                                    echo 'checked';
                                    //$token = 1;
                                }
                            } ?>/><?= $ROW2[$y]['opcion'] ?>
                        <?php } /*if($token == 1)$x++;*/
                        ?>
                    </td>
                </tr>
                <?php
            } elseif ($ROW[$x]['tipo'] == 4) {
                ?>
                <tr>
                    <td><?= $ROW[$x]['descri'] ?></td>
                </tr>
                <tr>
                    <td><textarea readonly><?= $ROW[$x]['desarrollo'] ?></textarea></td>
                </tr>
            <?php }
        } ?>
    </table>
    <br/>
    <input type="button" id="btn" value="Imprimir" class="boton" onClick="print();">
</center>
<?= $Gestor->Encabezado('L0008', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>