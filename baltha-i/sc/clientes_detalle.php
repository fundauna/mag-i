<?php
define('__MODULO__', 'sc');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _clientes_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js', 5) ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('l1', 'hr', 'Servicio al Cliente :: Detalle Clientes') ?>
<?= $Gestor->Encabezado('L0001', 'e', 'Detalle Clientes') ?>
<center>
    <input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
    <input type="hidden" id="idActual" value="<?= $_GET['ID'] ?>"/>
    <table class="radius" width="98%">
        <tr>
            <td class="titulo" colspan="2">Detalle</td>
        </tr>
        <tr>
            <td>Tipo:</td>
            <td><select id="tipo">
                    <option value='0'>Interno</option>
                    <option value='1' <?= ($ROW[0]['tipo'] == '1') ? 'selected' : '' ?>>Externo</option>
                </select>
            </td>

        </tr>
        <tr>
            <td>Identificaci&oacute;n:</td>
            <td><input type="text" id="id" value="<?= str_replace(" ", "", $ROW[0]['id']); ?>" size="10" maxlength="15"
                       title="Alfanumérico (3/15)" <?= $_GET['acc'] == 'M' ? 'readonly' : '' ?>>
                <?php if ($_GET['acc'] == 'M'): ?>
                    &nbsp;&nbsp;
                    <a id="btn_actualizarid" title="Editar Identificaci&oacute;n" href="#">
                        <img style="width: 15px; height: 15px;" src="../../caspha-i/imagenes/menu/datos.png" width="18"
                             height="18" alt="datos"/>
                    </a>

                    <a hidden id="btn_guardarid" title="Guardar Identificaci&oacute;n" href="#">
                        <img style="width: 15px; height: 15px;" src="../../caspha-i/imagenes/guardar.png" width="18"
                             height="18" alt="datos"/>
                    </a>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td>Nombre/Dpto:</td>
            <td><input type="text" id="nombre" value="<?= $ROW[0]['nombre'] ?>" size="30" maxlength="100"
                       title="Alfanumérico (2/100)"></td>
        </tr>
        <tr>
            <td>Unidad:</td>
            <td><input type="text" id="unidad" value="<?= $ROW[0]['unidad'] ?>" size="30" maxlength="50"
                       title="Alfanumérico (5/50)"></td>
        </tr>
        <tr>
            <td>Representante legal:</td>
            <td><input type="text" id="representante" value="<?= $ROW[0]['representante'] ?>" size="30" maxlength="60"
                       title="Alfanumérico (5/60)"></td>
        </tr>
        <tr>
            <td>Tel&eacute;fono:</td>
            <td><input type="text" id="tel" value="<?= $ROW[0]['tel'] ?>" size="10" maxlength="15"
                       title="Alfanumérico (8/15)"></td>
        </tr>
        <tr>
            <td>Correo de notificaciones de sistema:</td>
            <td><input type="text" id="email" value="<?= $ROW[0]['email'] ?>" maxlength="50"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Contactos:</td>
            <td>
                <table>
                    <tr>
                        <td></td>
                        <td>Nombre</td>
                        <td>Tel&eacute;fono</td>
                        <td>Email</td>
                    </tr>
                    <tbody id="lolo">
                    <?php
                    $ROW2 = $Gestor->ClientesContactos();
                    for ($x = 0; $x < count($ROW2); $x++) {
                        ?>
                        <tr>
                            <td><?= $x + 1 ?>.</td>
                            <td><input type="text" name="contacto" value="<?= $ROW2[$x]['contacto'] ?>"
                                       onblur="ValidaLinea()" maxlength="60"/></td>
                            <td><input type="text" name="telefono" value="<?= $ROW2[$x]['telefono'] ?>" size="9"
                                       maxlength="15"/></td>
                            <td><input type="text" name="correo" value="<?= $ROW2[$x]['email'] ?>" maxlength="50"/></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                    <tr>
                        <td colspan="4" align="right"><input type="button" value="+" onclick="ContactoAgregar()"
                                                             title="Agregar línea"/></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Solicitante:</td>
            <td>
                <table>
                    <tr>
                        <td></td>
                        <td>Nombre</td>
                        <td>Tel&eacute;fono</td>
                        <td>Email</td>
                    </tr>
                    <tbody id="lolo1">
                    <?php
                    $ROW2 = $Gestor->ClientesSolicitantes();
                    for ($x = 0; $x < count($ROW2); $x++) {
                        ?>
                        <tr>
                            <td><?= $x + 1 ?>.</td>
                            <td><input type="text" name="solicitante" value="<?= $ROW2[$x]['contacto'] ?>"
                                       onblur="ValidaLinea()" maxlength="60"/></td>
                            <td><input type="text" name="soltel" value="<?= $ROW2[$x]['telefono'] ?>" size="9"
                                       maxlength="15"/></td>
                            <td><input type="text" name="solemail" value="<?= $ROW2[$x]['email'] ?>" maxlength="50"/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                    <tr>
                        <td colspan="4" align="right"><input type="button" value="+" onclick="SolicitanteAgregar()"
                                                             title="Agregar línea"/></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Fax:</td>
            <td><input type="text" id="fax" value="<?= $ROW[0]['fax'] ?>" size="10" maxlength="15"
                       title="Alfanumérico (8/15)"></td>
        </tr>
        <tr>
            <td>Direcci&oacute;n:</td>
            <td><textarea id="direccion" title="Alfanumérico (5/500)"><?= $ROW[0]['direccion'] ?></textarea></td>
        </tr>
        <tr>
            <td>Observaciones:</td>
            <td><textarea id="actividad" title="Alfanumérico (5/200)"><?= $ROW[0]['actividad'] ?></textarea></td>
        </tr>
        <tr>
            <td colspan="2">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Laboratorio:</td>
            <td>
                <input type="checkbox" id="LRE" value="1" <?php if ($ROW[0]['LRE'] == '1') echo 'checked'; ?>/>&nbsp;LRE&nbsp;&nbsp;
                <input type="checkbox" id="LDP" value="1" <?php if ($ROW[0]['LDP'] == '1') echo 'checked'; ?> />&nbsp;LDP&nbsp;&nbsp;
                <input type="checkbox" id="LCC" value="1" <?php if ($ROW[0]['LCC'] == '1') echo 'checked'; ?> />&nbsp;LCC&nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td>Estado:</td>
            <td><select id="estado">
                    <option value='1'>Activo</option>
                    <option value='0' <?= ($ROW[0]['estado'] == '0') ? 'selected' : '' ?>>Inactivo</option>
                </select>
            </td>
        </tr>
    </table>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php if ($_GET['acc'] == 'M' and $Gestor->Clave()) { ?>
        &nbsp;&nbsp;<input type="button" value="Contraseña" class="boton" onclick="ClaveReenvia()"
                           title="Reenviar contraseña">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('L0001', 'p', '') ?>
</body>
</html>