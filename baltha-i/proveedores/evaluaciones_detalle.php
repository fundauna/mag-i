<?php
define('__MODULO__', 'proveedores');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _evaluaciones_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>Delfos</title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<center>
    <input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
    <input type="hidden" id="id" value="<?= $ROW[0]['id'] ?>"/>
    <input type="hidden" id="prov" value="<?= $ROW[0]['proveedor'] ?>"/>
    <input type="hidden" id="tipo_prov" value="<?= $ROW[0]['tipo_prov'] ?>"/>
    <?php $Gestor->Incluir('j2', 'hr', 'Proveedores :: Evaluaci&oacute;n de proveedores') ?>
    <?= $Gestor->Encabezado('J0002', 'e', 'Evaluaci&oacute;n de proveedores') ?>
    <br/>
    <table class="radius" width="100%">
        <tr>
            <td class="titulo" colspan="5">Informaci&oacute;n General</td>
        </tr>
        <tr>
            <td>Laboratorio:</td>
            <td colspan="2"><?= $Gestor->Lab() ?></td>
            <td style="color:#0066CC">Proveedor:</td>
            <td><input type="text" id="proveedor" value="<?= $ROW[0]['nombre'] ?>" class="lista" readonly
                       onclick="ProveedoresLista()"/></td>
        </tr>
        <tbody id="tgeneral0" style="display:none">
        <tr>
            <td colspan="2" <?= $_GET['acc'] == 'I' ? ' style="visibility:hidden"' : '' ?>>Funcionario que realiza la
                encuesta:
            </td>
            <td <?= $_GET['acc'] == 'I' ? ' style="visibility:hidden"' : '' ?>><?= $ROW[0]['funcionario'] ?></td>
            <td>Tipo de proveedor:</td>
            <td id="lbl_tipo"></td>
        </tr>
        <tr>
            <td style="color:#0066CC">No. orden de compra que se evalua:</td>
            <td><input type="text" id="orden" value="<?= $ROW[0]['orden'] ?>" size="20" maxlength="20"></td>
            <td>Fecha:<?= $ROW[0]['fecha'] ?></td>
            <td>No. de l&iacute;nea(s) evaluadas:</td>
            <td><input type="text" id="lineas" value="<?= $ROW[0]['lineas'] ?>" size="5" maxlength="5"></td>
        </tr>
        <tr>
            <td style="color:#00CC00"><strong>Calificaci&oacute;n Obtenida:</strong></td>
            <td><input type="text" id="total" size="5" readonly tabindex="-1"></td>
            <td colspan="2">Descripci&oacute;n del proveedor seg&uacute;n calificaci&oacute;n obtenida:</td>
            <td id="lbl_calificacion"></td>
        </tr>
        </tbody>
        <tbody id="tgeneral1" style="display:none">
        <tr>
            <td class="titulo" colspan="5">Precio (30% proveedor de bienes y suministros, 45% proveedor de servicios)
            </td>
        </tr>
        <tr>
            <td colspan="2" style="color:#0066CC">Precio total ofrecido por la oferta en evaluaci&oacute;n:</td>
            <td><input type="text" id="precio1" value="<?= $Gestor->Formato($ROW[0]['precio1']) ?>" maxlength="12"
                       onblur="_FLOAT(this);Calcula();" class="monto"></td>
            <td rowspan="3">Puntaje obtenido:</td>
            <td rowspan="3"><input type="text" id="puntos1" class="cantidad" readonly tabindex="-1"></td>
        </tr>
        <tr>
            <td colspan="2" style="color:#0066CC">Precio total de la oferta con el monto total m&aacute;s bajo:</td>
            <td><input type="text" id="precio2" value="<?= $Gestor->Formato($ROW[0]['precio2']) ?>" maxlength="12"
                       onblur="_FLOAT(this);Calcula();" class="monto"></td>
        </tr>
        <tr>
            <td colspan="2">Porcentaje correspondiente al criterio de evaluaci&oacute;n:</td>
            <td><input type="text" id="porc" value="<?= $ROW[0]['porc'] ?>" readonly class="cantidad"></td>
        </tr>
        </tbody>
        <tbody id="tbienes1" style="display:none">
        <tr>
            <td class="titulo" colspan="5">Calidad (25% proveedor de bienes y suministros, 30% proveedor de servicios)
            </td>
        </tr>
        <tr align="center">
            <td colspan="3"><strong>Criterio de evaluaci&oacute;n</strong></td>
            <td colspan="2"></td>
        </tr>
        <tr align="center">
            <td colspan="2"><strong>Descripci&oacute;n</strong></td>
            <td><strong>Calificaci&oacute;n</strong></td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2">Cumple (los bienes o suministros son aceptados por<br/>cumplir con las caracter&iacute;sticas
                solicitadas)
            </td>
            <td align="center">100%</td>
            <td rowspan="3">Puntaje obtenido:</td>
            <td rowspan="3"><input type="text" id="puntos2" class="monto" readonly tabindex="-1"></td>
        </tr>
        <tr>
            <td colspan="2">No cumple (son rechazados)</td>
            <td align="center">0%</td>
        </tr>
        <tr>
            <td colspan="2" align="right" style="color:#0066CC">Calificaci&oacute;n asignada:</td>
            <td align="center"><input type="text" id="calif1" value="<?= $ROW[0]['calif1'] ?>" maxlength="3"
                                      onblur="_INT(this);Calcula();" class="cantidad"></td>
            <td colspan="2"></td>
        </tr>
        </tbody>
        <tbody id="tservicios" style="display:none">
        <tr align="center">
            <td colspan="3"><strong>Criterio de evaluaci&oacute;n</strong></td>
            <td colspan="2"></td>
        </tr>
        <tr align="center">
            <td><strong>Subcriterio</strong></td>
            <td><strong>Criterio de evaluaci&oacute;n</strong></td>
            <td><strong>Criterio de evaluaci&oacute;n</strong></td>
            <td style="color:#0066CC">Calificaci&oacute;n asignada:</td>
            <td></td>
        </tr>
        <tr>
            <td>Trabajo o servicio realizado</td>
            <td align="center">8</td>
            <td>Muy bueno: 8 - Bueno: 6 - Regular: 4 - Malo: 2</td>
            <td><input type="text" id="calif2" value="<?= $ROW[0]['calif2'] ?>" maxlength="3"
                       onblur="_INT(this);Calcula();" class="cantidad"></td>
            <td>Puntaje obtenido:</td>
        </tr>
        <tr>
            <td>Calidad de materiales y suministros</td>
            <td align="center">8</td>
            <td>Muy bueno: 8 - Bueno: 6 - Regular: 4 - Malo: 2</td>
            <td><input type="text" id="calif3" value="<?= $ROW[0]['calif3'] ?>" maxlength="3"
                       onblur="_INT(this);Calcula();" class="cantidad"></td>
            <td rowspan="4"><input type="text" id="puntos3" class="cantidad" readonly tabindex="-1"></td>
        </tr>
        <tr>
            <td>Idoneidad del personal clave</td>
            <td align="center">6</td>
            <td>Muy bueno: 6 - Bueno: 4,5 - Regular: 3 - Malo: 1,5</td>
            <td><input type="text" id="calif4" value="<?= $ROW[0]['calif4'] ?>" maxlength="3"
                       onblur="_INT(this);Calcula();" class="cantidad"></td>
        </tr>
        <tr>
            <td>Programaci&oacute;n del trabajo</td>
            <td align="center">4</td>
            <td>Muy bueno: 4 - Bueno: 3 - Regular: 2 - Malo: 1</td>
            <td><input type="text" id="calif5" value="<?= $ROW[0]['calif5'] ?>" maxlength="3"
                       onblur="_INT(this);Calcula();" class="cantidad"></td>
        </tr>
        <tr>
            <td>Iniciativa y cooperaci&oacute;n</td>
            <td align="center">4</td>
            <td>Muy bueno: 4 - Bueno: 3 - Regular: 2 - Malo: 1</td>
            <td><input type="text" id="calif6" value="<?= $ROW[0]['calif6'] ?>" maxlength="3"
                       onblur="_INT(this);Calcula();" class="cantidad"></td>
        </tr>
        </tbody>
        <tbody id="tgeneral2" style="display:none">
        <tr>
            <td class="titulo" colspan="5">Cumplimiento de plazo (25% proveedor de bienes y suministros, 25% proveedor
                de servicios)
            </td>
        </tr>
        <tr align="center">
            <td colspan="3"><strong>Criterio de evaluaci&oacute;n</strong></td>
            <td colspan="2"></td>
        </tr>
        <tr align="center">
            <td colspan="2"><strong>D&iacute;as de atraso en la entrega</strong></td>
            <td><strong>Calificaci&oacute;n</strong></td>
            <td colspan="2"></td>
        </tr>
        <tr align="center">
            <td colspan="2">0</td>
            <td>100</td>
            <td rowspan="6">Puntaje obtenido:</td>
            <td rowspan="6"><input type="text" id="puntos4" class="cantidad" readonly tabindex="-1"></td>
        </tr>
        <tr align="center">
            <td colspan="2">< 3 d&iacute;as</td>
            <td>90</td>
        </tr>
        <tr align="center">
            <td colspan="2">4 � 5 d&iacute;as</td>
            <td>80</td>
        </tr>
        <tr align="center">
            <td colspan="2">6 � 7 d&iacute;as</td>
            <td>70</td>
        </tr>
        <tr align="center">
            <td colspan="2">8 � 9 d&iacute;as</td>
            <td>20</td>
        </tr>
        <tr align="center">
            <td colspan="2">&ge; 10 d&iacute;as</td>
            <td>0</td>
        </tr>
        <tr>
            <td colspan="2" align="right" style="color:#0066CC">Calificaci&oacute;n asignada:</td>
            <td align="center"><input type="text" id="calif7" value="<?= $ROW[0]['calif7'] ?>" maxlength="3"
                                      onblur="_INT(this);Calcula();" class="cantidad"></td>
            <td colspan="2"></td>
        </tr>
        </tbody>
        <tbody id="tbienes2" style="display:none">
        <tr>
            <td class="titulo" colspan="5">Cumplimiento de cantidad (20% proveedor de bienes y suministros, no aplica
                proveedor de servicios)
            </td>
        </tr>
        <tr align="center">
            <td colspan="3"><strong>Criterio de evaluaci&oacute;n</strong></td>
            <td colspan="2"></td>
        </tr>
        <tr align="center">
            <td colspan="2"><strong>% de entrega</strong></td>
            <td><strong>Calificaci&oacute;n</strong></td>
            <td colspan="2"></td>
        </tr>
        <tr align="center">
            <td colspan="2">0</td>
            <td>100</td>
            <td rowspan="3">Puntaje obtenido:</td>
            <td rowspan="3"><input type="text" id="puntos5" class="cantidad" readonly tabindex="-1"></td>
        </tr>
        <tr align="center">
            <td colspan="2">80 &le; x &le; 99</td>
            <td>80</td>
        </tr>
        <tr align="center">
            <td colspan="2">< 80</td>
            <td>0</td>
        </tr>
        <tr>
            <td colspan="2" align="right" style="color:#0066CC">Calificaci&oacute;n asignada:</td>
            <td align="center"><input type="text" id="calif8" value="<?= $ROW[0]['calif8'] ?>" maxlength="3"
                                      onblur="_INT(this);Calcula();" class="cantidad"></td>
            <td colspan="2"></td>
        </tr>
        </tbody>
    </table>
    <br/>
    <?php if ($_GET['acc'] == 'I') { ?>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php }else{ ?>
        <script>CambiaTipo()</script>
    <input type="button" value="Imprimir" class="boton" onClick="window.print()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('J0002', 'p', '') ?>
<?= $Gestor->Footer(2) ?>
</body>
</html>