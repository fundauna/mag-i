<?php
define('__MODULO__', 'proveedores');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _evaluaciones();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>Delfos</title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('j1', 'hr', 'Proveedores :: Evaluaciones') ?>
<?= $Gestor->Encabezado('J0001', 'e', 'Evaluaciones') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" style="font-size:12px;width:650px;">
            <tr>
                <td class="titulo" colspan="3">Filtro</td>
            </tr>
            <tr>
                <td>Desde: <input type="text" id="desde" name="desde" class="fecha" value="<?= $_POST['desde'] ?>"
                                  readonly onClick="show_calendar(this.id);">&nbsp;
                    Hasta: <input type="text" id="hasta" name="hasta" class="fecha" value="<?= $_POST['hasta'] ?>"
                                  readonly onClick="show_calendar(this.id);"></td>
                <td>Proveedor: <input type="text" name="nombre" id="nombre" value="<?= $_POST['nombre'] ?>"/></td>
                <td><input type="button" value="Buscar" class="boton2" onclick="EvasBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:650px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>&nbsp;</th>
                <th>Proveedor</th>
                <th>O.C. Evaluada</th>
                <th>Calificaci&oacute;n</th>
                <th>Fecha</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->EvasMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><img onclick="EvasModifica('<?= $ROW[$x]['id'] ?>')"
                             src="<?php $Gestor->Incluir('previa', 'bkg') ?>" border="0" title="Ver detalle"
                             class="tab2"/></td>
                    <td><?= $ROW[$x]['nombre'] ?></td>
                    <td><?= $ROW[$x]['orden'] ?></td>
                    <td><?= $ROW[$x]['calificacion'] ?></td>
                    <td><?= $ROW[$x]['fecha1'] ?></td>
                    <td><img onclick="EvasElimina('<?= $ROW[$x]['id'] ?>')"
                             src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar" class="tab2"/></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/><input type="button" value="Agregar" class="boton" onClick="EvasAgrega();">
</center>
<?= $Gestor->Encabezado('J0001', 'p', '') ?>
</body>
</html>