<?php
define('__MODULO__', 'proveedores');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _proveedores();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>Delfos</title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('j3', 'hr', 'Proveedores :: Cat�logo') ?>
<?= $Gestor->Encabezado('J0003', 'e', 'Cat�logo') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" style="font-size:12px;width:650px;">
            <tr>
                <td class="titulo" colspan="5">Filtro</td>
            </tr>
            <tr>
                <td>Laboratorio: <select name="filtro">
                        <option value="1">Actual</option>
                        <option value="0" <?php if ($_POST['filtro'] == 0) echo 'selected'; ?>>Todos</option>
                    </select></td>
                <td>Tipo:&nbsp;
                    <select name="tipo">
                        <option value="">...</option>
                        <?php
                        $ROW = $Gestor->ProveedoresFiltro();
                        for ($x = 0; $x < count($ROW); $x++) {
                            ?>
                            <option value="<?= $ROW[$x]['cs'] ?>"
                                    <?= ($_POST['tipo'] == $ROW[$x]['cs']) ? 'selected' : '' ?>><?= $ROW[$x]['nombre'] ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td><input type="radio" id="lolo1" name="lolo" onclick="marca(1)"
                           value="1" <?php if ($_POST['lolo'] == 1) echo 'checked'; ?> />&nbsp;Identificaci&oacute;n:
                    <input type="text" id="id" name="id" value="<?= $_POST['id'] ?>"/></td>
                <td><input type="radio" id="lolo2" name="lolo" onclick="marca(3)"
                           value="3" <?php if ($_POST['lolo'] == 3) echo 'checked'; ?>/>&nbsp;Nombre: <input type="text"
                                                                                                             name="nombre"
                                                                                                             id="nombre"
                                                                                                             readonly
                                                                                                             value="<?= $_POST['nombre'] ?>"/>
                </td>
                <td><input type="button" value="Buscar" class="boton2" onclick="ProveedoresBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:650px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>Identificaci&oacute;n</th>
                <th>Nombre</th>
                <th>Tel&eacute;fonos</th>
                <th>Fax</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->ProveedoresMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                $ROW[$x]['id'] = str_replace(' ', '', $ROW[$x]['id']);
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#"
                           onclick="ProveedoresModifica('<?= $ROW[$x]['cs'] ?>')"><?= $ROW[$x]['id'] == '' ? '-' : $ROW[$x]['id'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['nombre'] ?></td>
                    <td><?= $ROW[$x]['telefono'] ?></td>
                    <td><?= $ROW[$x]['fax'] ?></td>
                    <td><img onclick="ProveedoresElimina('<?= $ROW[$x]['cs'] ?>')"
                             src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar" class="tab2"/></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/><input type="button" value="Agregar" class="boton" onClick="ProveedoresAgrega();">
</center>
<?= $Gestor->Encabezado('J0003', 'p', '') ?>
</body>
</html>