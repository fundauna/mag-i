<?php
define('__MODULO__', 'proveedores');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _proveedores_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW)
    die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>Delfos</title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js', 5) ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('jquery.maskedinput.min', 'js'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('j4', 'hr', 'Proveedores :: Detalle de catálogo') ?>
<?= $Gestor->Encabezado('J0004', 'e', 'Detalle de catálogo') ?>
<center>
    <input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
    <input type="hidden" id="cs" value="<?= $ROW[0]['cs'] ?>"/>
    <table class="radius">
        <tr>
            <td class="titulo" colspan="5">Datos</td>
        </tr>
        <tr>
            <td>Tipo:</td>
            <?php
            if ($_GET['acc'] == 'M') {
                $reload = "&ID={$ROW[0]['cs']}&cambiar=1";
                if (isset($_GET['cambiar'])) {
                    if ($ROW[0]['tipo'] == '0')
                        $ROW[0]['tipo'] = '1';
                    else
                        $ROW[0]['tipo'] = '0';
                    $ROW[0]['id'] = '';
                }
            } else {
                $reload = '&cambiar=1';
                if (isset($_GET['cambiar'])) {
                    if ($_GET['tipo'] == '0')
                        $ROW[0]['tipo'] = '0';
                    else
                        $ROW[0]['tipo'] = '1';
                    $ROW[0]['id'] = '';
                }
            }
            ?>
            <td><select id="tipo"
                        onchange="location.href = 'proveedores_detalle.php?acc=<?= $_GET['acc'] . $reload ?>&tipo=' + this.value;">
                    <option value=''>...</option>
                    <option value='0' <?= ($ROW[0]['tipo'] == '0') ? 'selected' : '' ?>>F&iacute;sico</option>
                    <option value='1' <?= ($ROW[0]['tipo'] == '1') ? 'selected' : '' ?>>Jur&iacute;dico</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Naturaleza:</td>
            <td><select id="naturaleza">
                    <option value="">...</option>
                    <option value="0" <?= ($ROW[0]['naturaleza'] == '0') ? 'selected' : '' ?>>Bienes/Suministros
                    </option>
                    <option value="1" <?= ($ROW[0]['naturaleza'] == '1') ? 'selected' : '' ?>>Servicios</option>
                    <option value="2" <?= ($ROW[0]['naturaleza'] == '2') ? 'selected' : '' ?>>Bienes/Suministros y
                        Servicios
                    </option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Identificaci&oacute;n:</td>
            <td><input type="text" id="<?= ($ROW[0]['tipo'] == '1') ? 'ide' : 'idp' ?>" value="<?= $ROW[0]['id'] ?>"
                       maxlength="100" title="Alfanumérico"></td>
        </tr>
        <tr>
            <td>Nombre:</td>
            <td><input type="text" id="nombre" value="<?= $ROW[0]['nombre'] ?>" size="30" maxlength="100"
                       title="Alfanumérico (2/100)"></td>
        </tr>
        <tr>
            <td>Direcci&oacute;n:</td>
            <td><textarea id="direccion" title="Alfanumérico (5/200)"><?= $ROW[0]['direccion'] ?></textarea></td>
        </tr>
        <tr>
            <td>Tel&eacute;fono:</td>
            <td><input type="text" id="tel" value="<?= $ROW[0]['telefono'] ?>" size="9" maxlength="15"
                       title="Alfanumérico (8/15)"></td>
        </tr>
        <tr>
            <td>Fax:</td>
            <td><input type="text" id="fax" value="<?= $ROW[0]['fax'] ?>" size="9" maxlength="15"
                       title="Alfanumérico (8/15)"></td>
        </tr>
        <tr>
            <td>Otro:</td>
            <td><input type="text" id="otro" value="<?= $ROW[0]['otro'] ?>" size="9" maxlength="15"
                       title="Alfanumérico (8/15)"></td>
        </tr>
        <tr>
            <td>Email:</td>
            <td><input type="text" id="email" value="<?= $ROW[0]['correo'] ?>" size="30" maxlength="50"
                       title="Alfanumérico (5/50)"></td>
        </tr>
        <tr>
            <td>Cr&iacute;tico:</td>
            <td><input type="checkbox" id="critico" value="1" <?= $ROW[0]['critico'] == '1' ? 'checked' : '' ?>/></td>
        </tr>
        <tr>
            <td colspan="5">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Contactos:</td>
            <td>
                <table>
                    <tr>
                        <td></td>
                        <td>Nombre</td>
                        <td>&Aacute;rea</td>
                        <td>Tel&eacute;fono</td>
                        <td>Extensi&oacute;n</td>
                        <td>Directo</td>
                        <td>Otro</td>
                        <td>Email</td>
                        <td>Otro Email</td>
                        <td>Acciones</td>
                    </tr>
                    <tbody id="lolo">
                    <?php
                    $ROW2 = $Gestor->ProveedoresContactos();
                    for ($x = 0; $x < count($ROW2); $x++) {
                        ?>
                        <tr>
                            <td><?= $x + 1 ?>.</td>
                            <td><input type="text" name="contacto" value="<?= $ROW2[$x]['contacto'] ?>"
                                       onblur="ValidaLinea()" maxlength="60"/></td>
                            <td><input type="text" name="area" value="<?= $ROW2[$x]['area'] ?>" maxlength="60"/></td>
                            <td><input type="text" name="telefono" value="<?= $ROW2[$x]['telefono'] ?>" size="9"
                                       maxlength="15"/></td>
                            <td><input type="text" name="extension" value="<?= $ROW2[$x]['extension'] ?>" size="9"
                                       maxlength="15"/></td>
                            <td><input type="text" name="directo" value="<?= $ROW2[$x]['directo'] ?>" size="9"
                                       maxlength="15"/></td>
                            <td><input type="text" name="Otelefono" value="<?= $ROW2[$x]['Otelefono'] ?>" size="9"
                                       maxlength="15"/></td>
                            <td><input type="text" name="correo" value="<?= $ROW2[$x]['email'] ?>" maxlength="50"/></td>
                            <td><input type="text" name="Ocorreo" value="<?= $ROW2[$x]['Oemail'] ?>" maxlength="50"/>
                            </td>
                            <td><img onclick="EliminaLinea(<?= $x ?>)" src="<?php $Gestor->Incluir('del', 'bkg') ?>"
                                     title="Eliminar" class="tab2"/></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                    <tr>
                        <td colspan="9" align="right"><input type="button" value="+" onclick="ContactoAgregar()"
                                                             title="Agregar línea"/></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Notas:</td>
            <td><textarea id="notas" title="Alfanumérico (5/200)"><?= $ROW[0]['notas'] ?></textarea></td>
        </tr>
        <tr>
            <td colspan="5">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Laboratorio:</td>
            <td colspan="4">
                <?php if ($_GET['acc'] == 'I'): ?>
                    <input type="checkbox" id="LRE" value="1"
                           <?= $Gestor->get_ROW() == '1' ? 'checked' : '' ?>/>&nbsp;LRE&nbsp;&nbsp;
                    <input type="checkbox" id="LDP" value="1"
                           <?= $Gestor->get_ROW() == '2' ? 'checked' : '' ?>/>&nbsp;LDP&nbsp;&nbsp;
                    <input type="checkbox" id="LCC" value="1"
                           <?= $Gestor->get_ROW() == '3' ? 'checked' : '' ?>/>&nbsp;LCC&nbsp;&nbsp;
                <?php else: ?>
                    <input type="checkbox" id="LRE"
                           value="1" <?php if ($ROW[0]['LRE'] == '1') echo 'checked'; ?> />&nbsp;LRE&nbsp;&nbsp;
                    <input type="checkbox" id="LDP"
                           value="1" <?php if ($ROW[0]['LDP'] == '1') echo 'checked'; ?> />&nbsp;LDP&nbsp;&nbsp;
                    <input type="checkbox" id="LCC"
                           value="1" <?php if ($ROW[0]['LCC'] == '1') echo 'checked'; ?> />&nbsp;LCC&nbsp;&nbsp;
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td>Clasificaci&oacute;n:</td>
            <td><select id="clasificacion">
                    <option value="">...</option>
                    <?php
                    $ROW2 = $Gestor->ProveedoresFiltro();
                    for ($x = 0; $x < count($ROW2); $x++) {
                        ?>
                        <option value="<?= $ROW2[$x]['cs'] ?>"
                                <?= ($ROW[0]['clasificacion'] == $ROW2[$x]['cs']) ? 'selected' : '' ?>><?= $ROW2[$x]['nombre'] ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
    </table>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos(<?= $ROW[0]['tipo'] ?>)">
</center>
<?= $Gestor->Encabezado('J0004', 'p', '') ?>
</body>
</html>