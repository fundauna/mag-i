<?php
define('__MODULO__', 'equipos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _balanzas();
$ROW = $Gestor->BalanzasCalMuestra();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('e15', 'hr', 'Equipos :: Balanzas') ?>
<?= $Gestor->Encabezado('E0015', 'e', 'Balanzas') ?>
<center>
    <div id="container" style="width:750px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>Equipo</th>
                <th>Fecha</th>
                <th>Cap. Nominal del Equipo Calibrado</th>
                <th>Resultado de la Calibraci&oacute;n</th>
                <th>Error de medida promedio</th>
                <th>Incertidumbre Expandida</th>
                <th>Opciones</th>
            </tr>
            </thead>
            <tbody>
            <?php for ($x = 0; $x < count($ROW); $x++) { ?>
                <tr class="gradeA" align="center">
                    <td>(<?= $ROW[$x]['codigo'] ?>) <?= $ROW[$x]['nombre'] ?></td>
                    <td><?= $ROW[$x]['fecha'] ?></td>
                    <
                    <td><?= $ROW[$x]['nominal'] ?></td>
                    <td><?= $ROW[$x]['resultado'] ?></td>
                    <td><?= $ROW[$x]['error'] ?></td>
                    <td><?= $ROW[$x]['inc'] ?></td>
                    <td><img onclick="modificar('E','<?= $ROW[$x]['cs'] ?>')"
                             src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Elimina Registro" class="tab2"/></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/>
    <input type="hidden" id="equipo"/>
    <table class="radius" align="center" width="355px">
        <tr>
            <td class="titulo" colspan="2">Resultado de Calibraci&oacute;n</td>
        </tr>
        <tr>
            <td><b>Equipo:&nbsp;</b></td>
            <td><input type="text" class="lista" onclick="EquiposLista();" id="nomequipo"></td>
        </tr>
        <tr>
            <td><b>Cap. Nominal del Equipo Calibrado:&nbsp;</b></td>
            <td><input type="text" id="nominal"></td>
        </tr>
        <tr>
            <td><b>Resultado de la Calibraci&oacute;n:&nbsp;</b></td>
            <td><input type="text" id="resultado"></td>
        </tr>
        <tr>
            <td><b>Error de medida promedio:&nbsp;</b></td>
            <td><input type="text" id="error"></td>
        </tr>
        <tr>
            <td><b>Incertidumbre Expandida:&nbsp;</b></td>
            <td><input type="text" id="inc"></td>
        </tr>
    </table>
    <br/>
    <input id="add" type="button" class="boton" value="Agregar" onclick="modificar('I','');">
</center>
<?= $Gestor->Encabezado('E0015', 'p', '') ?>
</body>
</html>