<?php
define('__MODULO__', 'equipos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _catalogo_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('e3', 'hr', 'Equipos :: Detalle Ficha T�cnica') ?>
<?= $Gestor->Encabezado('E0003', 'e', 'Detalle Ficha T�cnica') ?>
<center>
    <form name="form" id="form">
        <input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
        <input type="hidden" id="id" value="<?= $ROW[0]['id'] ?>"/>
        <input type="hidden" id="proveedor" value="<?= $ROW[0]['proveedor'] ?>"/>
        <table class="radius" width="98%">
            <tr>
                <td class="titulo" colspan="2">Identificaci&oacute;n del equipo</td>
            </tr>
            <tr>
                <td>C&oacute;digo:</td>
                <td><input type="text" id="codigo" value="<?= str_replace(' ', '', $ROW[0]['codigo']) ?>" size="15"
                           maxlength="15" title="Alfanum�rico (4/15)"></td>
            </tr>
            <tr>
                <td>Nombre:</td>
                <td><textarea id="nombre"><?= $ROW[0]['nombre'] ?></textarea></td>
            </tr>
            <tr>
                <td>Patrimonio:</td>
                <td><input type="text" id="patrimonio" value="<?= $ROW[0]['patrimonio'] ?>"></td>
            </tr>
            <tr>
                <td>Marca:</td>
                <td><input type="text" id="marca" value="<?= $ROW[0]['marca'] ?>" size="20" maxlength="20"
                           title="Alfanum�rico (1/20)"></td>
            </tr>
            <tr>
                <td>Modelo:</td>
                <td><input type="text" id="modelo" value="<?= $ROW[0]['modelo'] ?>" size="20" maxlength="20"
                           title="Alfanum�rico (1/20)"></td>
            </tr>
            <tr>
                <td>Serie:</td>
                <td><input type="text" id="serie" value="<?= $ROW[0]['serie'] ?>" size="20" maxlength="20"
                           title="Alfanum�rico (0/20)"></td>
            </tr>
        </table>
        <br/>
        <table class="radius" width="98%">
            <tr>
                <td class="titulo">Componentes&nbsp;<img onclick="ComponenteAgregar()"
                                                         src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                                                         title="Agregar l�nea" class="tab"/></td>
            </tr>
            <tr>
                <td>
                    <table style="font-size:12px" width="100%">
                        <tr>
                            <td>C&oacute;digo</td>
                            <td>Nombre</td>
                            <td>Patrimonio</td>
                            <td>Marca</td>
                            <td>Modelo</td>
                            <td>Serie</td>
                        </tr>
                        <tbody id="lolo">
                        <?php
                        $ROW2 = $Gestor->ComponentesMuestra();
                        for ($x = 0; $x < count($ROW2); $x++) {
                            ?>
                            <tr>
                                <td><input type="text" name="c_codigo" value="<?= $ROW2[$x]['c_codigo'] ?>" size="22"/>
                                </td>
                                <td><input type="text" name="c_nombre" value="<?= $ROW2[$x]['c_nombre'] ?>"/></td>
                                <td><input type="text" name="c_patrimonio" value="<?= $ROW2[$x]['c_patrimonio'] ?>"/>
                                </td>
                                <td><input type="text" name="c_marca" value="<?= $ROW2[$x]['c_marca'] ?>" size="22"
                                           maxlength="20"/></td>
                                <td><input type="text" name="c_modelo" value="<?= $ROW2[$x]['c_modelo'] ?>" size="22"
                                           maxlength="20"/></td>
                                <td><input type="text" name="c_serie" value="<?= $ROW2[$x]['c_serie'] ?>" size="22"
                                           maxlength="20"/></td>
                                <td><img onclick="ComponentesEliminar('<?= $ROW2[$x]['c_codigo'] ?>')"
                                         src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar" class="tab2"/>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
        <br/>
        <table class="radius" width="98%">
            <tr>
                <td class="titulo" colspan="2">Informaci&oacute;n general</td>
            </tr>
            <tr>
                <td>Ubicaci&oacute;n y c&oacute;digo del manual del fabricante:</td>
                <td><textarea id="ubi_fabricante"><?= $ROW[0]['ubi_fabricante'] ?></textarea></td>
            </tr>
            <tr>
                <td>Fecha puesto en funcionamiento:</td>
                <td><input type="text" id="fecha" class="fecha" value="<?= $ROW[0]['fecha'] ?>"
                           onClick="show_calendar(this.id);"></td>
            </tr>
            <tr>
                <td>Condiciones ambientales recomendadas para el uso:</td>
                <td>Temperatura (&deg;C):
                    <input type="text" id="temp1" value="<?= $ROW[0]['temp1'] ?>" class="cantidad"/>&nbsp;
                    Humedad (%): <input type="text" id="hume1" value="<?= $ROW[0]['hume1'] ?>" class="cantidad"/></td>
            </tr>
            <tr>
                <td>Condiciones ambientales recomendadas para el almacenamiento:</td>
                <td>Temperatura (&deg;C):
                    <input type="text" id="temp2" value="<?= $ROW[0]['temp2'] ?>" class="cantidad"/>&nbsp;
                    Humedad (%): <input type="text" id="hume2" value="<?= $ROW[0]['hume2'] ?>" class="cantidad"/></td>
            </tr>
            <tr>
                <td>Ubicaci&oacute;n en el laboratorio:</td>
                <td><input type="text" id="ubi_equipo" value="<?= $ROW[0]['ubi_equipo'] ?>" size="20" maxlength="50"
                           title="Alfanum�rico (1/50)"></td>
            </tr>
            <tr>
                <td>Garant&iacute;a:</td>
                <td><input type="text" id="garantia" value="<?= $ROW[0]['garantia'] ?>" size="20" maxlength="20"
                           title="Alfanum�rico (0/20)"></td>
            </tr>
            <?php if ($_GET['acc'] == 'M') { ?>
                <tr>
                    <td>Documentos relacionados</td>
                    <td><img onclick="EquiposCertificados('<?= $ROW[0]['id'] ?>')"
                             src="<?php $Gestor->Incluir('certificados', 'bkg') ?>" title="Documentos relacionados"
                             class="tab2"/></td>
                </tr>
            <?php } ?>
        </table>
        <br/>
        <table class="radius" width="98%">
            <tr>
                <td class="titulo" colspan="2">Proveedor</td>
            </tr>
            <tr>
                <td>Nombre:</td>
                <td><input type="text" id="tmp_proveedor" value="<?= $ROW[0]['tmp_proveedor'] ?>" class="lista" readonly
                           onclick="ProveedoresLista(0)"/></td>
            </tr>
            <?php if ($_GET['acc'] == 'M') { ?>
                <tr>
                    <td>Tel&eacute;fono:</td>
                    <td><?= $ROW[0]['telefono'] ?></td>
                </tr>
                <tr>
                    <td>Correo:</td>
                    <td><?= $ROW[0]['correo'] ?></td>
                </tr>
                <tr>
                    <td colspan="2" align="right"><img onclick="HistorialMuestra()"
                                                       src="<?= $Gestor->Incluir('previa', 'bkg') ?>"
                                                       title="Mostrar historial" class="tab2"/>&nbsp;Historial de
                        mantenimientos
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" id="tbody"></td>
                </tr>
            <?php } ?>
        </table>
        <br/>
        <table class="radius" width="98%">
            <tr>
                <td class="titulo" colspan="2">Datos metrol&oacute;gicos</td>
            </tr>
            <tr>
                <td>Sometido a control metrol&oacute;gico?:</td>
                <td><select id="control_metro">
                        <option value="">....</option>
                        <option value="0" <?= ($ROW[0]['control_metro'] == '0') ? 'selected' : '' ?>>No</option>
                        <option value="1" <?= ($ROW[0]['control_metro'] == '1') ? 'selected' : '' ?>>S&iacute;</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Tipo de control metrol&oacute;gico?:</td>
                <td>
                    Calibraci&oacute;n<input type="checkbox" id="tipo_cali"
                                             <?= ($ROW[0]['tipo_cali'] == '1') ? 'checked' : '' ?>/>&nbsp;
                    Verificaci&oacute;n<input type="checkbox" id="tipo_veri"
                                              <?= ($ROW[0]['tipo_veri'] == '1') ? 'checked' : '' ?>/>
                </td>
            </tr>
            <tr>
                <td>Sometido a mantenimiento?:</td>
                <td><select id="tipo_mant">
                        <option value="0">No</option>
                        <option value="1" <?php if ($ROW[0]['tipo_mant'] == '1') echo 'selected'; ?>>S&iacute;</option>
                    </select></td>
            </tr>
            <tr>
                <td>Otro:</td>
                <td><input type="text" id="otro" value="<?= $ROW[0]['otro'] ?>" size="50" maxlength="50"
                           title="Alfanum�rico (0/50)"></td>
            </tr>
            <tr>
                <td>Unidad de medida:</td>
                <td><input type="text" id="unidad" value="<?= utf8_decode($ROW[0]['unidad']) ?>" size="5" maxlength="5"
                           title="Alfanum�rico (1/5)"></td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td>Rangos:&nbsp;<img onclick="RangosAgregar()" src="<?php $Gestor->Incluir('add', 'bkg') ?>"
                                      title="Agregar l�nea" class="tab"/></td>
                <td>
                    <table style="font-size:12px">
                        <tr align="center">
                            <td></td>
                            <td><strong>De</strong></td>
                            <td><strong>Hasta</strong></td>
                            <td><strong>Resoluci&oacute;n</strong></td>
                            <td><strong>Error m&aacute;ximo permisible</strong></td>
                        </tr>
                        <tbody id="lolo3">
                        <?php
                        $ROW2 = $Gestor->RangosMuestra();
                        for ($x = 0; $x < count($ROW2); $x++) {
                            ?>
                            <tr>
                                <td>Rango <?= $x + 1 ?>:</td>
                                <td><input type="text" name="rango1" value="<?= $ROW2[$x]['rango1'] ?>"
                                           onblur="_FLOAT(this);" class="monto"/></td>
                                <td><input type="text" name="rango2" value="<?= $ROW2[$x]['rango2'] ?>"
                                           onblur="_FLOAT(this);" class="monto"/></td>
                                <td><input type="text" name="resolucion" value="<?= $ROW2[$x]['resolucion'] ?>"
                                           class="monto"/></td>
                                <td><input type="text" name="error" value="<?= $ROW2[$x]['error'] ?>"
                                           onblur="this.value=_RED(this.value,6);" class="monto"/></td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>
            <tr>
                <td>Estado:</td>
                <td><select id="estado">
                        <option value="">....</option>
                        <option value="1" <?= ($ROW[0]['estado'] == '1') ? 'selected' : '' ?>>En uso</option>
                        <option value="0" <?= ($ROW[0]['estado'] == '0') ? 'selected' : '' ?>>Fuera de uso</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Condici&oacute;n:</td>
                <td><input type="text" id="justi" maxlength="50" value="<?= $ROW[0]['justi'] ?>"
                           title="En reserva, Da�ado, en respaldo, etc"/></td>
            </tr>
        </table>
        <br/>
        <?php if ($_GET['acc'] == 'M') { ?>
            <table class="radius" width="98%">
                <tr>
                    <td class="titulo">Historial del equipo&nbsp;<img
                                onclick="HEquipoAgregar('<?= $_GET['ID'] ?>', '', 'I')"
                                src="<?php $Gestor->Incluir('add', 'bkg') ?>" title="Agregar l�nea" class="tab"/></td>
                </tr>
                <tr>
                    <td>
                        <table style="font-size:12px" width="100%">
                            <tr>
                                <td>Fecha</td>
                                <td>Descripci&oacute;n</td>
                                <td>Detectado por</td>
                                <td>Opciones</td>
                            </tr>
                            <tbody id="lolos">
                            <?php
                            $ROW3 = $Gestor->CatalogoHistorialMuestra();
                            for ($x = 0; $x < count($ROW3); $x++) {
                                ?>
                                <tr>
                                    <td><?= $ROW3[$x]['fecha'] ?></td>
                                    <td><?= $ROW3[$x]['descripcion'] ?></td>
                                    <td><?= $ROW3[$x]['usuario'] ?></td>
                                    <td>
                                        <img onclick="HEquipoAgregar('<?= $ROW3[$x]['equipo'] ?>', '<?= $ROW3[$x]['linea'] ?>', 'M')"
                                             src="<?php $Gestor->Incluir('mod', 'bkg') ?>" title="Modificar"
                                             class="tab2"/>
                                        <img onclick="HEquipoElimina('<?= $ROW3[$x]['equipo'] ?>', '<?= $ROW3[$x]['linea'] ?>')"
                                             src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar"
                                             class="tab2"/></td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
            <br/>
        <?php } ?>
        <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    </form>
</center>
<?= $Gestor->Encabezado('E0003', 'p', '') ?>
</body>
</html>