<?php
define('__MODULO__', 'equipos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _historial();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('estilo', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('e7', 'hr', 'Equipos :: Historial') ?>
<?= $Gestor->Encabezado('E0007', 'e', 'Historial') ?>
<center>
    <input type="hidden" id="equipo" value="<?= $_GET['ID'] ?>"/>
    <input type="hidden" id="id"/>
    <table class="radius" width="85%">
        <tr>
            <td class="titulo" colspan="4">Detalle</td>
        </tr>
        <tr>
            <td><strong>Fecha</strong></td>
            <td><strong>Descripci&oacute;n u observaci&oacute;n</strong></td>
            <td><strong>Detectado por</strong></td>
            <td></td>
        </tr>
        <?php
        $ROW = $Gestor->HistorialMuestra();
        for ($x = 0; $x < count($ROW); $x++) {
            ?>
            <tr onmouseout="mOut(this)" onmouseover="mOver(this)">
                <td><?= $ROW[$x]['fecha1'] ?></td>
                <td><textarea readonly><?= $ROW[$x]['descr'] ?></textarea></td>
                <td><?= $ROW[$x]['usuario'] ?></td>
                <td><img onclick="HistorialElimina('<?= $ROW[$x]['id'] ?>')"
                         src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar" class="tab2"/></td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td colspan="4">
                <hr/>
            </td>
        </tr>
        <tr>
            <td><input type="text" id="fecha" name="fecha" class="fecha" readonly onClick="show_calendar(this.id);">
            </td>
            <td><textarea id="descr"></textarea></td>
            <td colspan="2"><input type="text" id="tmp" class="lista" readonly onclick="UsuariosLista()"/>
                <input type="hidden" id="usuario" name="usuario"/>
            </td>
        </tr>
    </table>
    <br/>
    <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
</center>
<?= $Gestor->Encabezado('E0007', 'p', '') ?>
</body>
</html>