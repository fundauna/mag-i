<?php
define('__MODULO__', 'equipos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _uso_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');

if ($_GET['acc'] == 'V') $disabled = 'disabled';
else $disabled = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('e13', 'hr', 'Equipos :: Detalle Registro de uso') ?>
<?= $Gestor->Encabezado('E0013', 'e', 'Detalle Registro de uso') ?>
<center>
    <input type="hidden" id="equipo" name="equipo" value="<?= $ROW[0]['equipo'] ?>"/>
    <table class="radius" style="font-size:12px">
        <tr>
            <td class="titulo" colspan="2">Datos</td>
        </tr>
        <?php
        if ($_GET['acc'] == 'V') {
            ?>
            <tr>
                <td>Analista:</td>
                <td><?= $ROW[0]['analista'] ?></td>
            </tr>
            <tr>
                <td>C&oacute;digo:</td>
                <td><?= $ROW[0]['equipo'] ?></td>
            </tr>
        <?php } ?>
        <tr>
            <td>Equipo:</td>
            <td><input type="text" id="tmp_equipo" class="lista" value="<?= $ROW[0]['tmp_equipo'] ?>" readonly
                       onclick="EquiposLista()" <?= $disabled ?>/></td>
        </tr>
        <tr>
            <td>Cantidad de Horas:</td>
            <td><input type="text" id="horas" name="horas" value="<?= $ROW[0]['horas'] ?>" class="monto"
                       onblur="_FLOAT(this)" <?= $disabled ?>></td>
        </tr>
        <tr>
            <td>Referencia:</td>
            <td><input type="text" id="referencia" name="referencia" value="<?= $ROW[0]['ref'] ?>" size="40"
                       maxlength="50" title="Alfanumérico (3/15)" <?= $disabled ?>></td>
        </tr>
        <tr>
            <td>Acci&oacute;n:</td>
            <td><select id="accion" name="accion" <?= $disabled ?>>
                    <option value="0">An&aacute;lisis</option>
                    <option value="1" <?= ($ROW[0]['accion'] == '1') ? 'selected' : '' ?>>Validaci&oacute;n</option>
                    <option value="2" <?= ($ROW[0]['accion'] == '2') ? 'selected' : '' ?>>Verificaci&oacute;n</option>
                    <?php
                    //SOLO SALE EN DETALLE Y SE CREA AL HACER EL INFORME
                    if ($_GET['acc'] == 'V') {
                        ?>
                        <option value="i" <?= ($ROW[0]['accion'] == 'i') ? 'selected' : '' ?>>Informe</option>
                        <?php
                    }
                    ?>
                </select>
            </td>
        </tr>

    </table>
    <?php
    if ($_GET['acc'] == 'I') {
        ?>
        <br/>
        <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    <?php } ?>
</center>
<?= $Gestor->Encabezado('E0013', 'p', '') ?>
</body>
</html>