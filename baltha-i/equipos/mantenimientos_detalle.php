<?php
define('__MODULO__', 'equipos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _mantenimientos_detalle();
$ROW = $Gestor->ObtieneDatos();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('e9', 'hr', 'Equipos :: Detalle del mantenimiento') ?>
<?= $Gestor->Encabezado('E0009', 'e', 'Detalle del mantenimiento') ?>
<center>
    <form name="form" id="form" method="post" enctype="multipart/form-data"
          action="<?= '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__) ?>">
        <input type="hidden" id="accion" name="accion" value="<?= $_GET['acc'] ?>"/>
        <input type="hidden" id="id" name="id" value="<?= $ROW[0]['id'] ?>"/>
        <input type="hidden" id="equipo" name="equipo" value="<?= $ROW[0]['equipo'] ?>"/>
        <input type="hidden" id="prov" name="prov" value="<?= $ROW[0]['prov'] ?>"/>
        <input type="hidden" id="usuario" name="usuario" value="<?= $ROW[0]['usuario'] ?>"/>
        <table class="radius" style="font-size:12px">
            <tr>
                <td class="titulo" colspan="2">Detalle</td>
            </tr>
            <tr>
                <td>C&oacute;digo de reporte de servicio:</td>
                <td><input type="text" id="reporte" name="reporte" value="<?= $ROW[0]['reporte'] ?>" size="13"
                           maxlength="15" title="Alfanumérico (3/15)"></td>
            </tr>
            <tr>
                <td>Equipo:</td>
                <td><input type="text" id="tmp_equipo" class="lista" value="<?= $ROW[0]['tmp_equipo'] ?>" readonly
                           onclick="EquiposLista()"/></td>
            </tr>
            <?php if ($_GET['acc'] != 'I') { ?>
                <tr>
                    <td>Nombre:</td>
                    <td><?= $ROW[0]['nombre'] ?></td>
                </tr>
            <?php } ?>
            <tr>
                <td>Fecha:</td>
                <td><input type="text" id="fecha" name="fecha" class="fecha" value="<?= $ROW[0]['fecha'] ?>" readonly
                           onClick="show_calendar(this.id);"></td>
            </tr>
            <tr>
                <td>Tipo:</td>
                <td><select id="tipo" name="tipo" onchange="CambiaTipo(this.value)">
                        <option value="">....</option>
                        <option value="0" <?= ($ROW[0]['tipo'] == '0') ? 'selected' : '' ?>>Correctivo</option>
                        <option value="1" <?= ($ROW[0]['tipo'] == '1') ? 'selected' : '' ?>>Preventivo</option>
                        <option value="2" <?= ($ROW[0]['tipo'] == '2') ? 'selected' : '' ?>>Rutinario</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Encargado del equipo:</td>
                <td><input type="text" id="tmp" name="tmp" class="lista" value="<?= $ROW[0]['tmp'] ?>" readonly
                           onclick="UsuariosLista()"/></td>
            </tr>
            <tr id="tr_prov" <?php if ($ROW[0]['tipo'] == '' or $ROW[0]['tipo'] == '2') echo 'style="display:none"'; ?> >
                <td>Proveedor:</td>
                <td><input type="text" id="proveedor" value="<?= $ROW[0]['proveedor'] ?>" class="lista" readonly
                           onclick="ProveedoresLista(0)"/></td>
            </tr>
            <tr>
                <td>C&oacute;digo de registro:</td>
                <td><input type="text" id="maestro" name="maestro"
                           value="<?= str_replace(' ', '', $ROW[0]['maestro']) ?>" size="20" maxlength="20"
                           title="Alfanumérico (3/20)"></td>
            </tr>
            <tr>
                <td>Observaciones:</td>
                <td><textarea id="detalle" name="detalle"><?= $ROW[0]['detalle'] ?></textarea></td>
            </tr>
            <tr>
                <td>Adjunto:</td>
                <td id="adjunto">
                    <?php
                    $ruta = "../../caspha-i/docs/equipos/man/" . str_replace(' ', '', $ROW[0]['id']) . '.zip';
                    if (file_exists($ruta)){
                    ?>
                    <img src="<?php $Gestor->Incluir('bajar', 'bkg') ?>" title="Descargar"
                         onclick="DocumentosZip('<?= $ROW[0]['id'] ?>', '<?= __MODULO__ ?>/man')" class="tab2"/>
                    &nbsp;<img id="btn2" onclick="DocElimina('<?= $ROW[0]['id'] ?>', '<?= __MODULO__ ?>/man')"
                               src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar adjunto" class="tab2"/>
                </td>
                <?php } else { ?>
                    <input type="file" name="archivo" id="archivo">
                <?php } ?>
            </tr>
        </table>
        <br/>
        <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    </form>
</center>
<?= $Gestor->Encabezado('E0009', 'p', '') ?>
</body>
</html>