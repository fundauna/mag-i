<?php
define('__MODULO__', 'equipos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _cronograma();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('cronograma', 'css') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('e5', 'hr', 'Equipos :: Cronograma') ?>
<?= $Gestor->Encabezado('E0005', 'e', 'Cronograma') ?>
<?php
echo $Gestor->Controles($_GET['m'], $_GET['y']);
echo $Gestor->Calendario($_GET['m'], $_GET['y']);
?>
<script>
    <?php
    $ROW = $Gestor->ObtieneDatos($_GET['m'], $_GET['y']);
    for($x = 0;$x < count($ROW);$x++){
    ?>
    colorea('<?=$ROW[$x]['fecha']?>');
    <?php
    }
    ?>
</script>
<br/>
<center>
    <input type="button" class="boton" value="Refrescar" onclick="location.reload()"/>
    <br/><br/>
    <img src='../../caspha-i/imagenes/0.png'/> Verificaci&oacute;n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <img src='../../caspha-i/imagenes/1.png'/> Mantenimienton&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <img src='../../caspha-i/imagenes/2.png'/> Calibraci&oacute;n
</center>
<?= $Gestor->Encabezado('E0005', 'p', '') ?>
</body>
</html>