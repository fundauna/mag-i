<?php
define('__MODULO__', 'equipos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _catalogo();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('e2', 'hr', 'Equipos :: Ficha T�cnica de Equipos') ?>
<?= $Gestor->Encabezado('E0002', 'e', 'Ficha T�cnica de Equipos') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <table class="radius" align="center" style="width:650px">
            <tr>
                <td class="titulo" colspan="3">Filtro</td>
            </tr>
            <tr>
                <td><input type="radio" id="lolo1" name="lolo" onclick="marca(1)"
                           value="1" <?php if ($_POST['lolo'] == 1) echo 'checked'; ?> />&nbsp;C&oacute;digo: <input
                            type="text" id="codigo" name="codigo" value="<?= $_POST['codigo'] ?>"/></td>
                <td><input type="radio" id="lolo2" name="lolo" onclick="marca(2)"
                           value="2" <?php if ($_POST['lolo'] == 2) echo 'checked'; ?>/>&nbsp;Nombre: <input type="text"
                                                                                                             name="nombre"
                                                                                                             id="nombre"
                                                                                                             readonly
                                                                                                             value="<?= $_POST['nombre'] ?>"/>
                </td>
                <td><input type="button" value="Buscar" class="boton2" onclick="EquiposBuscar(this)"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:650px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>C&oacute;digo</th>
                <th>Nombre</th>
                <th>Estado</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->EquiposMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                $ROW[$x]['codigo'] = str_replace(' ', '', $ROW[$x]['codigo']);
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#" onclick="EquiposModifica('<?= $ROW[$x]['id'] ?>')"><?= $ROW[$x]['codigo'] ?></a>
                    </td>
                    <td><?= $ROW[$x]['nombre'] ?></td>
                    <td <?= ($ROW[$x]['estado'] == 'Deshabilitado' ? 'style="color:#FF0000"' : '') ?>><?= $ROW[$x]['estado'] ?></td>
                    <td><img onclick="EquiposHistorial('<?= $ROW[$x]['id'] ?>')"
                             src="<?php $Gestor->Incluir('ver', 'bkg') ?>" title="Historial del equipo" class="tab2"/>
                    </td>
                    <td><img onclick="EquiposElimina('<?= $ROW[$x]['id'] ?>')"
                             src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar" class="tab2"/></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/><input type="button" value="Agregar" class="boton" onClick="EquiposAgrega();">
</center>
<?= $Gestor->Encabezado('E0002', 'p', '') ?>
</body>
</html>