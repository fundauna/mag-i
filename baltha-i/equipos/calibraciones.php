<?php
define('__MODULO__', 'equipos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _calibraciones();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables'); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('e0', 'hr', 'Equipos :: Calibraciones') ?>
<?= $Gestor->Encabezado('E0000', 'e', 'Calibraciones') ?>
<center>
    <form action="<?= basename(__FILE__) ?>" method="post">
        <input type="hidden" id="equipo" name="equipo" value="<?= $_POST['equipo'] ?>"/>
        <table class="radius" align="center" style="width:650px">
            <tr>
                <td class="titulo" colspan="2">Filtro</td>
            </tr>
            <tr>
                <td>Equipo: <input type="text" id="tmp_equipo" name="tmp_equipo" class="lista"
                                   value="<?= $_POST['tmp_equipo'] ?>" readonly onclick="EquiposLista()"/></td>
                <td>Desde: <input type="text" id="desde" name="desde" class="fecha" value="<?= $_POST['desde'] ?>"
                                  readonly onClick="show_calendar(this.id);">&nbsp;
                    Hasta: <input type="text" id="hasta" name="hasta" class="fecha" value="<?= $_POST['hasta'] ?>"
                                  readonly onClick="show_calendar(this.id);">&nbsp;
                    <input type="button" value="Buscar" class="boton2" onclick="CalisBuscar(this)"/>
                </td>
            </tr>
        </table>
    </form>
    <br/>
    <div id="container" style="width:650px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>C&oacute;digo</th>
                <th>Equipo</th>
                <th>Ente</th>
                <th>Fecha</th>
                <th>Opciones</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ROW = $Gestor->CalisMuestra();
            for ($x = 0; $x < count($ROW); $x++) {
                ?>
                <tr class="gradeA" align="center">
                    <td><a href="#" onclick="CalisModificar('<?= $ROW[$x]['id'] ?>')"><?= $ROW[$x]['codigo'] ?></a></td>
                    <td><?= $ROW[$x]['codequipo'] . ' | ' . $ROW[$x]['nombre'] ?></td>
                    <td><?= $ROW[$x]['ente'] ?></td>
                    <td><?= $ROW[$x]['fecha'] ?></td>
                    <td><img onclick="CalisElimina('<?= $ROW[$x]['id'] ?>');"
                             src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Eliminar" class="tab2"/></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/>
    <input type="button" class="boton" value="Agregar" onclick="CalisAgregar()"/>
</center>
<?= $Gestor->Encabezado('E0000', 'p', '') ?>
</body>
</html>