<?php
define('__MODULO__', 'equipos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _termometros();

if ($_GET['tipo'] == '1') $titulo = 'Term�metros';
else $titulo = 'Termohig�metros';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('e11', 'hr', "Equipos :: {$titulo}") ?>
<?= $Gestor->Encabezado('E0011', 'e', $titulo) ?>
<input type="hidden" id="tipo" value="<?= $_GET['tipo'] ?>"/>
<table class="radius" align="center" width="320px">
    <tr>
        <td class="titulo" colspan="2">Equipos Registradas</td>
    </tr>
    <tr>
        <td colspan="2">
            <select size="10" style="width:345px" onclick="marca(this);">
                <?php
                $ROW = $Gestor->Termos();
                for ($x = 0; $x < count($ROW); $x++) {
                    $ROW[$x]['codigo'] = str_replace(' ', '', $ROW[$x]['codigo']);

                    if ($ROW[$x]['activo'] == '1') $txt2 = '(Vigente)';
                    else $txt2 = '';

                    echo "<option codigo='{$ROW[$x]['codigo']}'
			  codigocer='{$ROW[$x]['codigocer']}' 
			  cuarto='{$ROW[$x]['cuarto']}' 
			  limite1='{$ROW[$x]['limite1']}' 
			  limite2='{$ROW[$x]['limite2']}' 
			  indicacion1='{$ROW[$x]['indicacion1']}' 
			  error1='{$ROW[$x]['error1']}' 
			  inc1='{$ROW[$x]['inc1']}' 
			  indicacion2='{$ROW[$x]['indicacion2']}' 
			  error2='{$ROW[$x]['error2']}' 
			  inc2='{$ROW[$x]['inc2']}'
			  indicacion3='{$ROW[$x]['indicacion3']}' 
			  error3='{$ROW[$x]['error3']}' 
			  inc3='{$ROW[$x]['inc3']}'
			  indicacion4='{$ROW[$x]['indicacion4']}' 
			  error4='{$ROW[$x]['error4']}' 
			  inc4='{$ROW[$x]['inc4']}'
			  indicacion5='{$ROW[$x]['indicacion5']}' 
			  error5='{$ROW[$x]['error5']}' 
			  inc5='{$ROW[$x]['inc5']}'
			  humedad1='{$ROW[$x]['humedad1']}'
			  humedad2='{$ROW[$x]['humedad2']}' 
			  hr1='{$ROW[$x]['hr1']}' 
			  err_hr1='{$ROW[$x]['err_hr1']}' 					
			  hr2='{$ROW[$x]['hr2']}' 
			  err_hr2='{$ROW[$x]['err_hr2']}' 
			  hr3='{$ROW[$x]['hr3']}' 
			  err_hr3='{$ROW[$x]['err_hr3']}'
			  fecha='{$ROW[$x]['fecha']}' 
			  activo='{$ROW[$x]['activo']}'>{$ROW[$x]['codigo']} {$txt2}</option>";
                }
                unset($ROW);
                ?>
            </select>
        </td>
    </tr>
</table>
<br/>
<table class="radius" align="center" width="450px">
    <tr>
        <td class="titulo" colspan="3">Detalle</td>
    </tr>
    <tr>
        <td><b>C&oacute;digo del equipo:&nbsp;</b></td>
        <td colspan="2"><input type="text" id="codigo" size="20" maxlength="20"></td>
    </tr>
    <tr>
        <td><b>C&oacute;digo del certificado:&nbsp;</b></td>
        <td colspan="2"><input type="text" id="codigocer" size="20" maxlength="20"></td>
    </tr>
    <tr>
        <td><b>Fecha del certificado:&nbsp;</b></td>
        <td colspan="2"><input type="text" id="fecha" name="fecha" class="fecha" readonly
                               onClick="show_calendar(this.id);"></td>
    </tr>
    <tr>
        <td><b>Vigente:&nbsp;</b></td>
        <td colspan="2">
            <select id="activo">
                <option value="0">No</option>
                <option value="1">S&iacute;</option>
            </select>
        </td>
    </tr>
    <tr>
        <td><b>Cuarto:</b></td>
        <td colspan="2"><input type="text" id="cuarto"></td>
    </tr>
    <tr>
        <td><b><?= $_GET['tipo'] == '1' ? 'Rango de trabajo:' : 'L&iacute;mite de temp del cuarto:' ?></b></td>
        <td colspan="2"><input type="text" id="limite1" class="monto" onblur="_RED(this,2)" value="0.00">�C a <input
                    type="text" id="limite2" class="monto" onblur="_RED(this,2)" value="0.00">�C
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <hr/>
        </td>
    </tr>
    <tr>
        <td><b>Indicaci&oacute;n (&deg;C):</b></td>
        <td><b>Error de indicaci&oacute;n (&deg;C):</b></td>
        <td><b>Incertidumbre(&deg;C):</b></td>
    </tr>
    <tr>
        <td><input type="text" id="indicacion1" class="monto" onblur="_RED(this,3)" value="0.00"></td>
        <td><input type="text" id="error1" class="monto" onblur="_RED(this,3)" value="0.00"></td>
        <td><input type="text" id="inc1" class="monto" onblur="_RED(this,3)" value="0.00"></td>
    </tr>
    <tr>
        <td><input type="text" id="indicacion2" class="monto" onblur="_RED(this,3)" value="0.00"></td>
        <td><input type="text" id="error2" class="monto" onblur="_RED(this,3)" value="0.00"></td>
        <td><input type="text" id="inc2" class="monto" onblur="_RED(this,3)" value="0.00"></td>
    </tr>
    <tr>
        <td><input type="text" id="indicacion3" class="monto" onblur="_RED(this,3)" value="0.00"></td>
        <td><input type="text" id="error3" class="monto" onblur="_RED(this,3)" value="0.00"></td>
        <td><input type="text" id="inc3" class="monto" onblur="_RED(this,3)" value="0.00"></td>
    </tr>
    <tr>
        <td><input type="<?= $_GET['tipo'] == '1' ? 'text' : 'hidden' ?>" id="indicacion4" class="monto"
                   onblur="_RED(this,3)" value="0.00"></td>
        <td><input type="<?= $_GET['tipo'] == '1' ? 'text' : 'hidden' ?>" id="error4" class="monto"
                   onblur="_RED(this,3)" value="0.00"></td>
        <td><input type="<?= $_GET['tipo'] == '1' ? 'text' : 'hidden' ?>" id="inc4" class="monto" onblur="_RED(this,3)"
                   value="0.00"></td>
    </tr>
    <tr>
        <td><input type="<?= $_GET['tipo'] == '1' ? 'text' : 'hidden' ?>" id="indicacion5" class="monto"
                   onblur="_RED(this,3)" value="0.00"></td>
        <td><input type="<?= $_GET['tipo'] == '1' ? 'text' : 'hidden' ?>" id="error5" class="monto"
                   onblur="_RED(this,3)" value="0.00"></td>
        <td><input type="<?= $_GET['tipo'] == '1' ? 'text' : 'hidden' ?>" id="inc5" class="monto" onblur="_RED(this,3)"
                   value="0.00"></td>
    </tr>
    <tbody id="higometro" <?php if ($_GET['tipo'] == '1') echo 'style="display:none"'; ?>>
    <tr>
        <td colspan="3">
            <hr/>
        </td>
    </tr>
    <tr>
        <td><b>L&iacute;mites Humedad Relativa del Cuarto:</b></td>
        <td colspan="2"><input type="text" id="humedad1" class="monto" onblur="_RED(this,2)" value="0.00">�C a <input
                    type="text" id="humedad2" class="monto" onblur="_RED(this,2)" value="0.00">�C
        </td>
    <tr>
        <td colspan="3">
            <hr/>
        </td>
    </tr>
    <tr>
        <td><b>Indicaci&oacute;n (%HR):</b></td>
        <td><b>Error de indicaci&oacute;n (%HR):</b></td>
        <td><b>Incertidumbre(%HR):</b></td>
    </tr>
    <tr>
        <td><input type="text" id="hr1" class="monto" onblur="_RED(this,3)" value="0.00"></td>
        <td><input type="text" id="err_hr1" class="monto" onblur="_RED(this,3)" value="0.00"></td>
        <td><input type="text" id="inc6" class="monto" onblur="_RED(this,3)" value="0.00"></td>
    </tr>
    <tr>
        <td><input type="text" id="hr2" class="monto" onblur="_RED(this,3)" value="0.00"></td>
        <td><input type="text" id="err_hr2" class="monto" onblur="_RED(this,3)" value="0.00"></td>
        <td><input type="text" id="inc7" class="monto" onblur="_RED(this,3)" value="0.00"></td>
    </tr>
    <tr>
        <td><input type="text" id="hr3" class="monto" onblur="_RED(this,3)" value="0.00"></td>
        <td><input type="text" id="err_hr3" class="monto" onblur="_RED(this,3)" value="0.00"></td>
        <td><input type="text" id="inc8" class="monto" onblur="_RED(this,3)" value="0.00"></td>
    </tr>
    </tbody>
</table>
<br/>
<center>
    <input id="new" type="button" class="boton" value="Limpiar" onclick="desmarca();" disabled>&nbsp;
    <input id="add" type="button" class="boton" value="Agregar" onclick="modificar('I');">&nbsp;
    <input id="mod" type="button" class="boton" value="Modificar" onclick="modificar('M');" disabled>&nbsp;
    <input id="del" type="button" class="boton" value="Eliminar" onclick="modificar('D');" disabled>
</center>
<?= $Gestor->Encabezado('E0011', 'p', '') ?>
</body>
</html>