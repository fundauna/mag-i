<?php
define('__MODULO__', 'equipos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _masas();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('e10', 'hr', 'Equipos :: Masas') ?>
<?= $Gestor->Encabezado('E0010', 'e', 'Masas') ?>
<table class="radius" align="center" width="255px">
    <tr>
        <td class="titulo" colspan="2">Masas Registradas</td>
    </tr>
    <tr>
        <td colspan="2">
            <select size="10" style="width:250px" onclick="marca(this);">
                <?php
                $ROW = $Gestor->Masas();
                for ($x = 0; $x < count($ROW); $x++) {
                    $ROW[$x]['codigo'] = str_replace(' ', '', $ROW[$x]['codigo']);
                    $ROW[$x]['nominal'] = str_replace(' ', '', $ROW[$x]['nominal']);
                    //$ROW[$x]['nominal'] = utf8_decode($ROW[$x]['nominal']);
                    if ($ROW[$x]['activo'] == '1')
                        $txt2 = '(Vigente)';
                    else
                        $txt2 = '';
                    echo "<option codigo='{$ROW[$x]['codigo']}' codigocer='{$ROW[$x]['codigocer']}' nominal='{$ROW[$x]['nominal']}' real='{$ROW[$x]['vreal']}' inc='{$ROW[$x]['inc']}' certificado='{$ROW[$x]['certificado']}' fecha='{$ROW[$x]['fecha']}' activo='{$ROW[$x]['activo']}'>{$ROW[$x]['nominal']}   {$ROW[$x]['codigo']} {$txt2}</option>";
                }
                unset($ROW);
                ?>
            </select>
        </td>
    </tr>
</table>
<br/>
<form id="frmmasas" method="post" action="../../caspha-i/shell/equipos/_masas.php">
    <table class="radius" align="center" width="255px">
        <input type="hidden" name="ocodigo" id="ocodigo"/>
        <input type="hidden" name="onominal" id="onominal"/>
        <input type="hidden" id="accion" name="accion" value=""/>
        <tr>
            <td class="titulo" colspan="2">Detalle</td>
        </tr>
        <tr>
            <td><b>C&oacute;digo del equipo:&nbsp;</b></td>
            <td><input type="text" name="codigo" id="codigo" size="20" maxlength="20"></td>
        </tr>
        <tr>
            <td><b>C&oacute;digo del certificado:&nbsp;</b></td>
            <td><input type="text" name="codigocer" id="codigocer" size="20" maxlength="20"></td>
        </tr>
        <tr>
            <td><b>Fecha del certificado:&nbsp;</b></td>
            <td><input type="text" id="fecha" name="fecha" class="fecha" readonly onClick="show_calendar(this.id);">
            </td>
        </tr>
        <tr>
            <td><b>Valor nominal (g):</b></td>
            <td><input type="text" id="nominal" name="nominal" class="monto" onblur="redondea(this)"></td>
        </tr>
        <tr>
            <td><b>Valor real: (g)</b></td>
            <td><input type="text" id="real" name="real" class="monto" onblur="redondea(this)"></td>
        </tr>
        <tr>
            <td><b>Incertidumbre (g):</b></td>
            <td><input type="text" id="inc" name="inc" class="monto" onblur="redondea(this)"></td>
        </tr>
        <tr>
            <td><b>Certificado:&nbsp;</b></td>
            <td><input type="text" id="certificado" name="certificado" size="20" maxlength="20"></td>
        </tr>
        <tr>
            <td><b>Vigente:&nbsp;</b></td>
            <td>
                <select name="activo" id="activo">
                    <option value="0">No</option>
                    <option value="1">S&iacute;</option>
                </select>
            </td>
        </tr>
    </table>
    <br/>
    <center>
        <input id="new" type="button" class="boton" value="Limpiar" onclick="desmarca();" disabled>&nbsp;
        <input id="add" type="button" class="boton" value="Agregar" onclick="modificar('I');">&nbsp;
        <input id="mod" type="button" class="boton" value="Modificar" onclick="modificar('M');" disabled>&nbsp;
        <input id="del" type="button" class="boton" value="Eliminar" onclick="modificar('D');" disabled>
    </center>
</form>
</body>
<?= $Gestor->Encabezado('E0010', 'p', '') ?>
</html>