<?php
define('__MODULO__', 'equipos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _cronograma_detalle();

$dias = array('Domingo', 'Lunes', 'Martes', 'Mi�rcoles', 'Jueves', 'Viernes', 'S�bado');
$meses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
$dia = date('w', mktime(0, 0, 0, $_GET['m'], $_GET['d'], $_GET['a']));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<input type="hidden" id="equipo" name="equipo"/>
<input type="hidden" id="cs" name="cs"/>
<input type="hidden" id="responsable" name="responsable"/>
<input type="hidden" id="fecha" name="fecha"
       value="<?= $_GET['d'] < 10 ? '0' . $_GET['d'] : $_GET['d'] ?>-<?= $_GET['m'] < 10 ? '0' . $_GET['m'] : $_GET['m'] ?>-<?= $_GET['a'] ?>"/>
<input type="hidden" id="hoy" value="<?= date('d/m/Y') ?>"/>
<input type="hidden" id="dia" value="<?= $_POST['dia'] ?>"/>

<?php $Gestor->Incluir('e6', 'hr', 'Equipos :: Modificar Cronograma del ' . $dias[$dia] . ' ' . $_GET['d'] . ' de ' . $meses[$_GET['m'] - 1] . ' del ' . $_GET['a'] . ''); ?>
<?= $Gestor->Encabezado('E0006', 'e', 'Modificar Cronograma') ?>
<center>
    <table class="radius" align="center" width="350px">
        <tr>
            <td class="titulo">Eventos Registrados</td>
        </tr>
        <tr>
            <td>
                <select size="10" style="width:100%" onclick="marca(this);">
                    <?php
                    $ROW = $Gestor->ObtieneDatos();
                    if ($ROW) {
                        for ($x = 0; $x < count($ROW); $x++) {
                            $Q = $Gestor->ObtieneEquipos($ROW[$x]['cs']);

                            $color = $Gestor->Color($ROW[$x]['tipo']);
                            $ROW[$x]['tmp_resp'] = $Gestor->Obtiene($ROW[$x]['responsable']);

                            if ($Q) {
                                $equ = '';
                                $txt = '';
                                foreach ($Q as $dato) {
                                    $txt .= $dato['codigo'] . ',';
                                    $equ .= $dato['equipo'] . ',';
                                }
                                echo "<option style='background-color:#{$color}' cs='{$ROW[$x]['cs']}' fecha='{$ROW[$x]['fecha']}' final='{$ROW[$x]['final']}' aviso='{$ROW[$x]['aviso']}' equipo='{$equ}' responsable='{$ROW[$x]['responsable']}' tmp_resp='{$ROW[$x]['tmp_resp']}' codigo='{$txt}' tipo='{$ROW[$x]['tipo']}' deshabilita='{$ROW[$x]['deshabilita']}' observaciones='{$ROW[$x]['observaciones']}'>{$txt}</option>";
                            } else {
                                $txt = $Q[$x]['codigo'] . ' ' . $ROW[$x]['nombre'];
                                echo "<option style='background-color:#{$color}' cs='{$ROW[$x]['cs']}' fecha='{$ROW[$x]['fecha']}' final='{$ROW[$x]['final']}' aviso='{$ROW[$x]['aviso']}' equipo='{$ROW[$x]['equipo']}' responsable='{$ROW[$x]['responsable']}' tmp_resp='{$ROW[$x]['tmp_resp']}' codigo='{$ROW[$x]['codigo']}' tipo='{$ROW[$x]['tipo']}' deshabilita='{$ROW[$x]['deshabilita']}' observaciones='{$ROW[$x]['observaciones']}'>{$txt}</option>";
                            }
                        }
                    }
                    unset($ROW);
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td align="center">
                <span style="background-color:#<?= $Gestor->Color(0) ?>">Verificaci&oacute;n</span>&nbsp;
                <span style="background-color:#<?= $Gestor->Color(1) ?>">Mantenimiento</span>&nbsp;
                <span style="background-color:#<?= $Gestor->Color(2) ?>">Calibraci&oacute;n</span>
            </td>
        </tr>
    </table>
    <br/>
    <table class="radius" style="font-size:12px" width="350px">
        <tr>
            <td class="titulo" colspan="2">Detalle</td>
        </tr>
        <tr id="tr_fecha" style="display:none;">
            <td>Fecha:</td>
            <td><input type="text" id="nueva" name="nueva" class="fecha" readonly onClick="show_calendar(this.id);"
                       value="<?= $_POST['dia'] ?>"></td>
        </tr>
        <tr>
            <td>Fecha final:</td>
            <td><input type="text" id="final" name="final" class="fecha" readonly onClick="show_calendar(this.id);">
            </td>
        </tr>
        <tr>
            <td>Equipos:</td>
            <td><textarea id="tmp_equipo" name="tmp_equipo" class="lista" readonly onclick="EquiposLista()"></textarea>
            </td>
        </tr>
        <tr>
            <td>Responsable:</td>
            <td><input type="text" id="tmp_resp" name="tmp_resp" class="lista" readonly onclick="UsuariosLista()"/></td>
        </tr>
        <tr>
            <td>Tipo:</td>
            <td><select id="tipo" name="tipo">
                    <option value="">...</option>
                    <option value="0">Verificaci&oacute;n</option>
                    <option value="1">Mantenimiento</option>
                    <option value="2">Calibraci&oacute;n</option>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Fecha de recordatorio:</td>
            <td><input type="text" id="aviso" name="aviso" class="fecha" readonly onClick="show_calendar(this.id);">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr/>
            </td>
        </tr>
        <tr>
            <td>Deshabilitar:</td>
            <td><select id="deshabilita" name="deshabilita">
                    <option value="">...</option>
                    <option value="0">No</option>
                    <option value="1">S&iacute;</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Observaciones:</td>
            <td><textarea id="observaciones" name="observaciones"></textarea></td>
        </tr>
    </table>
    <br/>
    <?php if ($_GET['t'] == 1) { ?>
        <input id="new" type="button" class="boton" value="Limpiar" onclick="desmarca();" disabled>&nbsp;
        <input id="add" type="button" class="boton" value="Agregar" onclick="modificar('I');">&nbsp;
        <input id="mod" type="button" class="boton" value="Modificar" onclick="modificar('M');" disabled>&nbsp;
        <input id="del" type="button" class="boton" value="Eliminar" onclick="modificar('D');" disabled>
    <?php } ?>
</center>
<?= $Gestor->Encabezado('E0006', 'p', '') ?>
</body>
</html>