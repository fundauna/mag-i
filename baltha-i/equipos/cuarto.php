<?php
define('__MODULO__', 'equipos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _cuarto();
$ROW = $Gestor->CuartoCalMuestra();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js', 2); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('', 'jtables', 2); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('e17', 'hr', 'Equipos :: Cuarto') ?>
<?= $Gestor->Encabezado('E0017', 'e', 'Cuarto') ?>
<center>
    <div id="container" style="width:750px">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>Cuarto</th>
                <th>Fecha</th>
                <th>L&iacute;mite de Temp del Cuarto</th>
                <th>L&iacute;mites Humedad Relativa del Cuarto</th>
                <th>Opciones</th>
            </tr>
            </thead>
            <tbody>
            <?php for ($x = 0; $x < count($ROW); $x++) { ?>
                <tr class="gradeA" align="center">
                    <td><?= $ROW[$x]['cuarto'] ?></td>
                    <td><?= $ROW[$x]['fecha'] ?></td>
                    <td><?= $ROW[$x]['temp1'] ?> a <?= $ROW[$x]['temp2'] ?></td>
                    <td><?= $ROW[$x]['hum1'] ?> a <?= $ROW[$x]['hum2'] ?></td>
                    <td><img onclick="modificar('E','<?= $ROW[$x]['id'] ?>')"
                             src="<?php $Gestor->Incluir('del', 'bkg') ?>" title="Elimina Registro" class="tab2"/></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <br/>
    <table class="radius" align="center" width="355px">
        <tr>
            <td class="titulo" colspan="2">Datos del Cuarto</td>
        </tr>
        <tr>
            <td><b>Cuarto:&nbsp;</b></td>
            <td><input type="text" id="cuarto"></td>
        </tr>
        <tr>
            <td><b>L&iacute;mite de Temp del Cuarto:&nbsp;</b></td>
            <td><input type="text" id="temp1" size="2"> a <input type="text" id="temp2" size="2"></td>
        </tr>
        <tr>
            <td><b>L&iacute;mites Humedad Relativa del Cuarto:&nbsp;</b></td>
            <td><input type="text" id="hum1" size="2"> a <input type="text" id="hum2" size="2"></td>
        </tr>
    </table>
    <br/>
    <input id="add" type="button" class="boton" value="Agregar" onclick="modificar('I','');">
</center>
<?= $Gestor->Encabezado('E0017', 'p', '') ?>
</body>
</html>