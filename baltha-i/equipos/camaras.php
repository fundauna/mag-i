<?php
define('__MODULO__', 'equipos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _camaras();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('estilo', 'css'); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('validaciones', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('e14', 'hr', 'Equipos :: Equipos de refrigeración') ?>
<?= $Gestor->Encabezado('E0014', 'e', 'Equipos de refrigeración') ?>
<table class="radius" align="center" width="255px">
    <tr>
        <td class="titulo" colspan="2">Equipos Registrados</td>
    </tr>
    <tr>
        <td colspan="2">
            <select size="10" style="width:250px" onclick="marca(this);">
                <?php
                $ROW = $Gestor->Camaras();
                for ($x = 0; $x < count($ROW); $x++) {
                    $ROW[$x]['codigo'] = str_replace(' ', '', $ROW[$x]['codigo']);
                    if ($ROW[$x]['tipo'] == '0') $txt = '(Cámara)';
                    else if ($ROW[$x]['tipo'] == '1') $txt = '(Congelador)';
                    else $txt = '(Refrigeradora)';
                    if ($ROW[$x]['activo'] == '1') $txt2 = '(Vigente)';
                    else $txt2 = '';

                    echo "<option codigo='{$ROW[$x]['codigo']}' tipo='{$ROW[$x]['tipo']}' limite1='{$ROW[$x]['limite1']}' limite2='{$ROW[$x]['limite2']}' ubicacion='{$ROW[$x]['ubicacion']}' fecha='{$ROW[$x]['fecha']}' activo='{$ROW[$x]['activo']}'>{$ROW[$x]['codigo']} {$txt} {$txt2}</option>";
                }
                unset($ROW);
                ?>
            </select>
        </td>
    </tr>
</table>
<br/>
<table class="radius" align="center">
    <tr>
        <td class="titulo" colspan="2">Detalle</td>
    </tr>
    <tr>
        <td><b>C&oacute;digo:&nbsp;</b></td>
        <td><input type="text" id="codigo" size="20" maxlength="20"></td>
    </tr>
    <tr>
        <td><b>Fecha:&nbsp;</b></td>
        <td><input type="text" id="fecha" name="fecha" class="fecha" readonly onClick="show_calendar(this.id);"></td>
    </tr>
    <tr>
        <td><b>Tipo:&nbsp;</b></td>
        <td>
            <select id="tipo">
                <option value="0">C&aacute;mara</option>
                <option value="1">Congelador</option>
                <option value="2">Refrigeradora</option>
            </select>
        </td>
    </tr>
    <tr>
        <td><b>Temp de funcion del equipo:</b></td>
        <td><input type="text" id="limite1" class="monto" onblur="_RED(this,2)" value="0.00"> a <input type="text"
                                                                                                       id="limite2"
                                                                                                       class="monto"
                                                                                                       onblur="_RED(this,2)"
                                                                                                       value="0.00">
        </td>
    </tr>
    <tr>
        <td><b>Ubicaci&oacute;n:&nbsp;</b></td>
        <td><input type="text" id="ubicacion" size="50" maxlength="50"></td>
    </tr>
    <tr>
        <td><b>Vigente:&nbsp;</b></td>
        <td>
            <select id="activo">
                <option value="0">No</option>
                <option value="1">S&iacute;</option>
            </select>
        </td>
    </tr>
</table>
<br/>
<center>
    <input id="new" type="button" class="boton" value="Limpiar" onclick="desmarca();" disabled>&nbsp;
    <input id="add" type="button" class="boton" value="Agregar" onclick="modificar('I');">&nbsp;
    <input id="mod" type="button" class="boton" value="Modificar" onclick="modificar('M');" disabled>&nbsp;
    <input id="del" type="button" class="boton" value="Eliminar" onclick="modificar('D');" disabled>
</center>
<?= $Gestor->Encabezado('E0014', 'p', '') ?>
</body>
</html>