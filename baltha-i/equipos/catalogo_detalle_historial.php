<?php
define('__MODULO__', 'equipos');
require '../../caspha-i/shell/' . __MODULO__ . '/_' . basename(__FILE__);

$Gestor = new _catalogo_detalle_historial();
$ROW = $Gestor->ObtieneDatos();
$ROW2 = $Gestor->ObtieneDatos2();
if (!$ROW) die('Registro inexistente');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('', 'fav'); ?>
    <?php $Gestor->Incluir('estilo', 'css') ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir('calendario', 'js') ?>
    <?php $Gestor->Incluir('validaciones', 'js') ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel='stylesheet' type='text/css' media='print' href='../../caspha-i/css/print.css'>
</head>
<body>
<?php $Gestor->Incluir('e3', 'hr', 'Equipos :: Detalle Historial Ficha T�cnica') ?>
<?= $Gestor->Encabezado('E0003', 'e', 'Detalle Historial Ficha T�cnica') ?>
<center>
    <form name="form" id="form">
        <input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
        <input type="hidden" id="id" value="<?= $_GET['id'] ?>"/>
        <input type="hidden" id="linea" value="<?= $_GET['l'] ?>"/>
        <table class="radius" width="98%">
            <tr>
                <td class="titulo" colspan="2">Historial del equipo</td>
            </tr>
            <tr>
                <td>Fecha:</td>
                <td><input type="text" id="fecha" class="fecha" value="<?= $ROW[0]['fecha'] ?>"
                           onClick="show_calendar(this.id);"></td>
            </tr>
            <tr>
                <td>Descripci&oacute;n:</td>
                <td><textarea id="descripcion"><?= $ROW[0]['descripcion'] ?></textarea></td>
            </tr>
            <tr>
                <td>Detectado por:</td>
                <td>
                    <select id="usuario">
                        <option value="">....</option>
                        <?php for ($i = 0; $i < count($ROW2); $i++) {
                            $nombre = $ROW2[$i]['nombre'] . ' ' . $ROW2[$i]['ap1']; ?>
                            <option value="<?= $nombre ?>"
                                    <?= ($ROW[0]['usuario'] == $nombre) ? 'selected' : '' ?>><?= $nombre ?></option>
                        <?php } ?>
                    </select>
                </td>
        </table>
        <br/>
        <input type="button" id="btn" value="Aceptar" class="boton" onClick="datos()">
    </form>
</center>
<?= $Gestor->Encabezado('E0003', 'p', '') ?>
</body>
</html>